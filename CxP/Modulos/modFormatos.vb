Imports Dipros.Windows.Forms
Module modFormatos

#Region "Acreedores"

    Public Sub for_acreedores_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Acreedor", "acreedor", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Rfc", "rfc", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Domicilio", "domicilio", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Estado", "nombre_estado", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Municipio", "nombre_municipio", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Ciudad", "nombre_ciudad", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Colonia", "nombre_colonia", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cp", "cp", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fonos", "telefonos", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "CURP", "curp", -1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Baja", "baja", 8, DevExpress.Utils.FormatType.None, "", 100)
    End Sub

#End Region

#Region "ConceptosCxp"

    Public Sub for_conceptos_cxp_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 210)
        AddColumns(oGrid, "Tipo", "tipo", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio", "folio", 3, DevExpress.Utils.FormatType.Numeric, "n0")

    End Sub

#End Region

#Region "Bancos"

    Public Sub for_bancos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Banco", "banco", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "sucursal", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Direcci�n", "direccion", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono", "tel1", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cuenta", "cuenta", 9, DevExpress.Utils.FormatType.None, "")

        AddColumns(oGrid, "Ejecutivo Cuenta", "ejecutivo_cuenta", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Gerente", "gerente", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fax", "fax", -1, DevExpress.Utils.FormatType.None, "")


    End Sub

#End Region

#Region "Chequeras"

    Public Sub for_chequeras_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Banco", "banco", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Banco", "nombre_banco", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cuenta Bancaria", "cuenta_bancaria", 1, DevExpress.Utils.FormatType.None, "", 180)
        AddColumns(oGrid, "Saldo", "saldo", 2, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Folio", "folio", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Predeterminada", "predeterminada", 4, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Cuenta Contable", "cuenta_contable", 5, DevExpress.Utils.FormatType.None, "", 180)
        AddColumns(oGrid, "Descripci�n de Cuenta", "descripcion_cuenta", 6, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

#Region "AcreedoresDiversos"

    Public Sub for_acreedores_diversos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)

        AddColumns(oGrid, "Folio Pago", "folio_pago", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Total", "importe", 1, DevExpress.Utils.FormatType.Numeric, "$###,###.00")
        AddColumns(oGrid, "Subtotal", "subtotal", 2, DevExpress.Utils.FormatType.Numeric, "$###,###.00")
        AddColumns(oGrid, "IVA", "iva", 3, DevExpress.Utils.FormatType.Numeric, "$###,###.00")
        AddColumns(oGrid, "Beneficiario", "beneficiario", 4, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "RFC", "rfc", 5, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Fecha Programaci�n", "fecha_programacion", 6, DevExpress.Utils.FormatType.DateTime, "", 150)
        AddColumns(oGrid, "N�mero Documento", "numero_documento", 7, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Fecha Documento", "fecha_documento", 8, DevExpress.Utils.FormatType.DateTime, "", 150)
        AddColumns(oGrid, "Tipo Pago", "tipo_pago", 9, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Fecha Pago", "fecha_pago", 10, DevExpress.Utils.FormatType.DateTime, "", 150)
        AddColumns(oGrid, "Cuenta Bancaria", "cuenta_bancaria", 11, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio Cheque", "folio_cheque", 12, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Referencia Transferencia", "referencia_transferencia", 13, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha Deposito", "fecha_deposito", 14, DevExpress.Utils.FormatType.DateTime, "", 150)
        AddColumns(oGrid, "Banco", "banco", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre Banco", "nombre_banco", 15, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Abono Cuenta", "abono_cuenta", 16, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Concepto", "concepto", 17, DevExpress.Utils.FormatType.None, "", 150)

    End Sub

#End Region

#Region "MovimientosPagar"

    Public Sub for_movimientos_pagar_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio", "folio", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Entrada", "entrada", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 3, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Fecha Vto", "fecha_vencimiento", 4, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Plazo", "plazo", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cargo", "cargo", 6, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Abono", "abono", 7, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Valor Factura Sin Descuentos", "importe_sin_descuentos", 8, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Cheque", "cheque", 9, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Status", "status", 10, DevExpress.Utils.FormatType.None, "")

        AddColumns(oGrid, "Proveedor", "proveedor", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Proveedor", "nombre_proveedor", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Concepto Referencia", "concepto_referencia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio Referencia", "folio_referencia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Banco", "banco", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cuenta Bancaria", "cuenta_bancaria", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio Factura", "folio_factura", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Saldo", "saldo", 28, DevExpress.Utils.FormatType.Numeric, "c2")

    End Sub

#End Region

#Region "MovimientosChequera"

    Public Sub for_movimientos_chequera_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Banco", "banco", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cuentabancaria", "cuentabancaria", -1, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Folio", "folio", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Concepto Movimiento", "descripcion", 4, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Estatus", "estatus", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tipo", "tipo", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cargo", "cargo", 7, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Abono", "abono", 8, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Concepto General ", "concepto", -1, DevExpress.Utils.FormatType.None, "", 200)
    End Sub

#End Region

#Region "PagoFacturasProveedores"

    Public Sub for_pago_facturas_proveedores_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Concepto", "concepto", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Banco", "banco", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cuenta Bancaria", "cuentabancaria", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cheque", "folio_cheque", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Importe", "importe", 6, DevExpress.Utils.FormatType.Numeric, "c2")
        AddColumns(oGrid, "Proveedor", "proveedor", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cancelado", "cancelado", 8, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "ConceptosMovimientosChequera"

    Public Sub for_conceptos_movimientos_chequera_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 550)
    End Sub

    Public Sub for_conceptos_movimientos_chequera_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "", 75)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 300)
    End Sub

    Public Sub for_movimientos_chequera_beneficiario_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Beneficiario", "beneficiario", 0, DevExpress.Utils.FormatType.None, "", 400)
    End Sub

#End Region

End Module
