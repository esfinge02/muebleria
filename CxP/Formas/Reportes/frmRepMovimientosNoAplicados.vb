Imports Dipros.Reports.Reports
Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepMovimientosNoAplicados
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepMovimientosNoAplicados))
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(316, 28)
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(96, 38)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = "(Ninguno)"
        Me.lkpProveedor.PopupWidth = CType(400, Long)
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(312, 20)
        Me.lkpProveedor.TabIndex = 59
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(16, 40)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 60
        Me.lblProveedor.Text = "Proveedor:"
        '
        'frmRepMovimientosNoAplicados
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(426, 72)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Name = "frmRepMovimientosNoAplicados"
        Me.Text = "frmRepMovimientosNoAplicados"
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Dim oReportes As New VillarrealBusiness.Reportes
    Dim oProveedores As New VillarrealBusiness.clsProveedores

    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)

        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la forma"

    Private Sub frmRepMovimientosNoAplicados_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.MovimientosNoAplicados(Proveedor)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oRepMovimientosNoAplicados As New rptMovimientosNoAplicados
                oRepMovimientosNoAplicados.DataSource = oDataSet.Tables(0)
                ShowReport(Me.MdiParent, "Reporte de Movimientos No aplicados", oRepMovimientosNoAplicados)
            Else
                ShowMessage(MessageType.MsgInformation, "El reporte no contiene información.")
            End If
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de los controles"
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim oEvent As New Events

        oEvent = oProveedores.Listado()
        If oEvent.ErrorFound Then
            oEvent.ShowError()
        Else
            Dim oDataSet As DataSet
            oDataSet = oEvent.Value
            lkpProveedor.DataSource = oDataSet.Tables(0)
        End If
        oEvent = Nothing
    End Sub
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(lkpProveedor)
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region



    Private Sub frmRepMovimientosNoAplicados_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oReportes.ValidacionMovimientosNoAplicados(lkpProveedor.Text)
    End Sub
End Class
