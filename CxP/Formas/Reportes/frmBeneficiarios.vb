Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports VillarrealBusiness
Public Class frmBeneficiarios
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblBeneficiarioDesde As System.Windows.Forms.Label
    Friend WithEvents gpoBeneficiario As System.Windows.Forms.GroupBox
    Friend WithEvents lblBeneficiarioHasta As System.Windows.Forms.Label
    Friend WithEvents lkpBeneficiarioInicial As Dipros.Editors.TINMultiLookup
    Friend WithEvents gpoFecha As System.Windows.Forms.GroupBox
    Friend WithEvents lblFechaHasta As System.Windows.Forms.Label
    Friend WithEvents lblFechaDesde As System.Windows.Forms.Label
    Friend WithEvents lkpBeneficiarioFinal As Dipros.Editors.TINMultiLookup
    Friend WithEvents dteFechaInicial As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFechaFinal As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNombreBanco As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpCuentaBancaria As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCuenta_Bancaria As System.Windows.Forms.Label
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents clcBanco As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBeneficiarios))
        Me.lblBeneficiarioDesde = New System.Windows.Forms.Label
        Me.gpoBeneficiario = New System.Windows.Forms.GroupBox
        Me.lkpBeneficiarioFinal = New Dipros.Editors.TINMultiLookup
        Me.lkpBeneficiarioInicial = New Dipros.Editors.TINMultiLookup
        Me.lblBeneficiarioHasta = New System.Windows.Forms.Label
        Me.gpoFecha = New System.Windows.Forms.GroupBox
        Me.dteFechaFinal = New DevExpress.XtraEditors.DateEdit
        Me.dteFechaInicial = New DevExpress.XtraEditors.DateEdit
        Me.lblFechaHasta = New System.Windows.Forms.Label
        Me.lblFechaDesde = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtNombreBanco = New DevExpress.XtraEditors.TextEdit
        Me.lkpCuentaBancaria = New Dipros.Editors.TINMultiLookup
        Me.lblCuenta_Bancaria = New System.Windows.Forms.Label
        Me.lblBanco = New System.Windows.Forms.Label
        Me.clcBanco = New Dipros.Editors.TINCalcEdit
        Me.gpoBeneficiario.SuspendLayout()
        Me.gpoFecha.SuspendLayout()
        CType(Me.dteFechaFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaInicial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtNombreBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(332, 28)
        '
        'lblBeneficiarioDesde
        '
        Me.lblBeneficiarioDesde.AutoSize = True
        Me.lblBeneficiarioDesde.Location = New System.Drawing.Point(16, 26)
        Me.lblBeneficiarioDesde.Name = "lblBeneficiarioDesde"
        Me.lblBeneficiarioDesde.Size = New System.Drawing.Size(43, 16)
        Me.lblBeneficiarioDesde.TabIndex = 0
        Me.lblBeneficiarioDesde.Text = "Desde:"
        '
        'gpoBeneficiario
        '
        Me.gpoBeneficiario.Controls.Add(Me.lkpBeneficiarioFinal)
        Me.gpoBeneficiario.Controls.Add(Me.lkpBeneficiarioInicial)
        Me.gpoBeneficiario.Controls.Add(Me.lblBeneficiarioHasta)
        Me.gpoBeneficiario.Controls.Add(Me.lblBeneficiarioDesde)
        Me.gpoBeneficiario.Location = New System.Drawing.Point(8, 40)
        Me.gpoBeneficiario.Name = "gpoBeneficiario"
        Me.gpoBeneficiario.Size = New System.Drawing.Size(440, 80)
        Me.gpoBeneficiario.TabIndex = 0
        Me.gpoBeneficiario.TabStop = False
        Me.gpoBeneficiario.Text = "Beneficiario:"
        '
        'lkpBeneficiarioFinal
        '
        Me.lkpBeneficiarioFinal.AllowAdd = False
        Me.lkpBeneficiarioFinal.AutoReaload = False
        Me.lkpBeneficiarioFinal.DataSource = Nothing
        Me.lkpBeneficiarioFinal.DefaultSearchField = ""
        Me.lkpBeneficiarioFinal.DisplayMember = "beneficiario"
        Me.lkpBeneficiarioFinal.EditValue = Nothing
        Me.lkpBeneficiarioFinal.Filtered = False
        Me.lkpBeneficiarioFinal.InitValue = Nothing
        Me.lkpBeneficiarioFinal.Location = New System.Drawing.Point(64, 48)
        Me.lkpBeneficiarioFinal.MultiSelect = False
        Me.lkpBeneficiarioFinal.Name = "lkpBeneficiarioFinal"
        Me.lkpBeneficiarioFinal.NullText = ""
        Me.lkpBeneficiarioFinal.PopupWidth = CType(400, Long)
        Me.lkpBeneficiarioFinal.Required = False
        Me.lkpBeneficiarioFinal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBeneficiarioFinal.SearchMember = ""
        Me.lkpBeneficiarioFinal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBeneficiarioFinal.SelectAll = False
        Me.lkpBeneficiarioFinal.Size = New System.Drawing.Size(360, 22)
        Me.lkpBeneficiarioFinal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBeneficiarioFinal.TabIndex = 3
        Me.lkpBeneficiarioFinal.ToolTip = Nothing
        Me.lkpBeneficiarioFinal.ValueMember = "beneficiario"
        '
        'lkpBeneficiarioInicial
        '
        Me.lkpBeneficiarioInicial.AllowAdd = False
        Me.lkpBeneficiarioInicial.AutoReaload = False
        Me.lkpBeneficiarioInicial.DataSource = Nothing
        Me.lkpBeneficiarioInicial.DefaultSearchField = ""
        Me.lkpBeneficiarioInicial.DisplayMember = "beneficiario"
        Me.lkpBeneficiarioInicial.EditValue = Nothing
        Me.lkpBeneficiarioInicial.Filtered = False
        Me.lkpBeneficiarioInicial.InitValue = Nothing
        Me.lkpBeneficiarioInicial.Location = New System.Drawing.Point(64, 24)
        Me.lkpBeneficiarioInicial.MultiSelect = False
        Me.lkpBeneficiarioInicial.Name = "lkpBeneficiarioInicial"
        Me.lkpBeneficiarioInicial.NullText = ""
        Me.lkpBeneficiarioInicial.PopupWidth = CType(400, Long)
        Me.lkpBeneficiarioInicial.Required = False
        Me.lkpBeneficiarioInicial.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBeneficiarioInicial.SearchMember = ""
        Me.lkpBeneficiarioInicial.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBeneficiarioInicial.SelectAll = False
        Me.lkpBeneficiarioInicial.Size = New System.Drawing.Size(360, 22)
        Me.lkpBeneficiarioInicial.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBeneficiarioInicial.TabIndex = 1
        Me.lkpBeneficiarioInicial.ToolTip = Nothing
        Me.lkpBeneficiarioInicial.ValueMember = "beneficiario"
        '
        'lblBeneficiarioHasta
        '
        Me.lblBeneficiarioHasta.AutoSize = True
        Me.lblBeneficiarioHasta.Location = New System.Drawing.Point(18, 48)
        Me.lblBeneficiarioHasta.Name = "lblBeneficiarioHasta"
        Me.lblBeneficiarioHasta.Size = New System.Drawing.Size(41, 16)
        Me.lblBeneficiarioHasta.TabIndex = 2
        Me.lblBeneficiarioHasta.Text = "Hasta:"
        '
        'gpoFecha
        '
        Me.gpoFecha.Controls.Add(Me.dteFechaFinal)
        Me.gpoFecha.Controls.Add(Me.dteFechaInicial)
        Me.gpoFecha.Controls.Add(Me.lblFechaHasta)
        Me.gpoFecha.Controls.Add(Me.lblFechaDesde)
        Me.gpoFecha.Location = New System.Drawing.Point(8, 128)
        Me.gpoFecha.Name = "gpoFecha"
        Me.gpoFecha.Size = New System.Drawing.Size(440, 56)
        Me.gpoFecha.TabIndex = 1
        Me.gpoFecha.TabStop = False
        Me.gpoFecha.Text = "Fecha:"
        '
        'dteFechaFinal
        '
        Me.dteFechaFinal.EditValue = New Date(2007, 6, 29, 0, 0, 0, 0)
        Me.dteFechaFinal.Location = New System.Drawing.Point(304, 24)
        Me.dteFechaFinal.Name = "dteFechaFinal"
        '
        'dteFechaFinal.Properties
        '
        Me.dteFechaFinal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaFinal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFechaFinal.Size = New System.Drawing.Size(88, 22)
        Me.dteFechaFinal.TabIndex = 3
        '
        'dteFechaInicial
        '
        Me.dteFechaInicial.EditValue = New Date(2007, 6, 29, 0, 0, 0, 0)
        Me.dteFechaInicial.Location = New System.Drawing.Point(96, 24)
        Me.dteFechaInicial.Name = "dteFechaInicial"
        '
        'dteFechaInicial.Properties
        '
        Me.dteFechaInicial.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaInicial.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFechaInicial.Size = New System.Drawing.Size(88, 22)
        Me.dteFechaInicial.TabIndex = 1
        '
        'lblFechaHasta
        '
        Me.lblFechaHasta.AutoSize = True
        Me.lblFechaHasta.Location = New System.Drawing.Point(248, 26)
        Me.lblFechaHasta.Name = "lblFechaHasta"
        Me.lblFechaHasta.Size = New System.Drawing.Size(41, 16)
        Me.lblFechaHasta.TabIndex = 2
        Me.lblFechaHasta.Text = "Hasta:"
        '
        'lblFechaDesde
        '
        Me.lblFechaDesde.AutoSize = True
        Me.lblFechaDesde.Location = New System.Drawing.Point(48, 26)
        Me.lblFechaDesde.Name = "lblFechaDesde"
        Me.lblFechaDesde.Size = New System.Drawing.Size(43, 16)
        Me.lblFechaDesde.TabIndex = 0
        Me.lblFechaDesde.Text = "Desde:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtNombreBanco)
        Me.GroupBox1.Controls.Add(Me.lkpCuentaBancaria)
        Me.GroupBox1.Controls.Add(Me.lblCuenta_Bancaria)
        Me.GroupBox1.Controls.Add(Me.lblBanco)
        Me.GroupBox1.Controls.Add(Me.clcBanco)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 192)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(440, 56)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cuenta Bancaria:"
        '
        'txtNombreBanco
        '
        Me.txtNombreBanco.EditValue = ""
        Me.txtNombreBanco.Location = New System.Drawing.Point(304, 24)
        Me.txtNombreBanco.Name = "txtNombreBanco"
        '
        'txtNombreBanco.Properties
        '
        Me.txtNombreBanco.Properties.Enabled = False
        Me.txtNombreBanco.Properties.MaxLength = 70
        Me.txtNombreBanco.Size = New System.Drawing.Size(96, 22)
        Me.txtNombreBanco.TabIndex = 3
        Me.txtNombreBanco.Tag = "nombre_banco"
        Me.txtNombreBanco.ToolTip = "Nombre Banco"
        '
        'lkpCuentaBancaria
        '
        Me.lkpCuentaBancaria.AllowAdd = False
        Me.lkpCuentaBancaria.AutoReaload = False
        Me.lkpCuentaBancaria.DataSource = Nothing
        Me.lkpCuentaBancaria.DefaultSearchField = ""
        Me.lkpCuentaBancaria.DisplayMember = "cuenta_bancaria"
        Me.lkpCuentaBancaria.EditValue = Nothing
        Me.lkpCuentaBancaria.Filtered = False
        Me.lkpCuentaBancaria.InitValue = Nothing
        Me.lkpCuentaBancaria.Location = New System.Drawing.Point(96, 24)
        Me.lkpCuentaBancaria.MultiSelect = False
        Me.lkpCuentaBancaria.Name = "lkpCuentaBancaria"
        Me.lkpCuentaBancaria.NullText = ""
        Me.lkpCuentaBancaria.PopupWidth = CType(400, Long)
        Me.lkpCuentaBancaria.Required = False
        Me.lkpCuentaBancaria.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCuentaBancaria.SearchMember = ""
        Me.lkpCuentaBancaria.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCuentaBancaria.SelectAll = False
        Me.lkpCuentaBancaria.Size = New System.Drawing.Size(152, 22)
        Me.lkpCuentaBancaria.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCuentaBancaria.TabIndex = 1
        Me.lkpCuentaBancaria.Tag = "cuenta_bancaria"
        Me.lkpCuentaBancaria.ToolTip = "Cuenta bancaria"
        Me.lkpCuentaBancaria.ValueMember = "cuenta_bancaria"
        '
        'lblCuenta_Bancaria
        '
        Me.lblCuenta_Bancaria.AutoSize = True
        Me.lblCuenta_Bancaria.Location = New System.Drawing.Point(40, 24)
        Me.lblCuenta_Bancaria.Name = "lblCuenta_Bancaria"
        Me.lblCuenta_Bancaria.Size = New System.Drawing.Size(48, 16)
        Me.lblCuenta_Bancaria.TabIndex = 0
        Me.lblCuenta_Bancaria.Tag = ""
        Me.lblCuenta_Bancaria.Text = "Cuenta:"
        Me.lblCuenta_Bancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(256, 24)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 2
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcBanco
        '
        Me.clcBanco.EditValue = "0"
        Me.clcBanco.Location = New System.Drawing.Point(408, 24)
        Me.clcBanco.MaxValue = 0
        Me.clcBanco.MinValue = 0
        Me.clcBanco.Name = "clcBanco"
        '
        'clcBanco.Properties
        '
        Me.clcBanco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcBanco.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBanco.Size = New System.Drawing.Size(24, 21)
        Me.clcBanco.TabIndex = 4
        Me.clcBanco.TabStop = False
        Me.clcBanco.Tag = "banco"
        Me.clcBanco.Visible = False
        '
        'frmBeneficiarios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(458, 255)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gpoFecha)
        Me.Controls.Add(Me.gpoBeneficiario)
        Me.Name = "frmBeneficiarios"
        Me.Text = "frmBeneficiarios"
        Me.Controls.SetChildIndex(Me.gpoBeneficiario, 0)
        Me.Controls.SetChildIndex(Me.gpoFecha, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.gpoBeneficiario.ResumeLayout(False)
        Me.gpoFecha.ResumeLayout(False)
        CType(Me.dteFechaFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaInicial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtNombreBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oChequeras As VillarrealBusiness.clsChequeras '@JGTO-18/04/2008: Se incluye el filtro de cuenta
    Private oReportes As New VillarrealBusiness.Reportes
    Private oMovimientosChequera As New VillarrealBusiness.clsMovimientosChequera

    '@JGTO-18/04/2008: Se incluye el filtro de cuenta
    Private ReadOnly Property CuentaBancaria() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpCuentaBancaria)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la forma"
    Private Sub frmBeneficiarios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        '@JGTO-18/04/2008: Se valida los nothing de los beneficiarios y se incluye filtro por cuenta
        Response = oReportes.Beneficiarios( _
        IIf(IsNothing(lkpBeneficiarioInicial.EditValue), "", lkpBeneficiarioInicial.EditValue), _
        IIf(IsNothing(lkpBeneficiarioFinal.EditValue), "", lkpBeneficiarioFinal.EditValue), _
        dteFechaInicial.EditValue, dteFechaFinal.EditValue, _
        IIf(IsNothing(lkpCuentaBancaria.EditValue), "", lkpCuentaBancaria.EditValue))

        If Not Response.ErrorFound Then
            Dim oDataSet As New DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oRepBeneficiarios As New rptBeneficiarios
                oRepBeneficiarios.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Reporte de Beneficiarios", oRepBeneficiarios)
            Else
                '@JGTO-18/04/2008: Se incluye el tipo de mensaje y se corrige el problema del string que no se pod�a convertir.
                ShowMessage(Dipros.Utils.Common.MessageType.MsgInformation, "El reporte no contiene informaci�n", "Reporte de Beneficiarios")
            End If
        End If
    End Sub
    Private Sub frmBeneficiarios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        dteFechaInicial.EditValue = Today()
        dteFechaInicial.EditValue = New Date(CType(dteFechaInicial.EditValue, Date).Year, CType(dteFechaInicial.EditValue, Date).Month, 1)
        '@JGTO-18/04/2008: Se a�ade para establecer la fecha final y no dejar su valor por omisi�n.
        dteFechaFinal.EditValue = CDate(dteFechaInicial.EditValue).AddMonths(1).AddDays(-1)
        lkpBeneficiarioInicial.PopupWidth = 410
        lkpBeneficiarioFinal.PopupWidth = 410

        '@JGTO-18/04/2008: Se incluye el filtro de cuenta
        oChequeras = New VillarrealBusiness.clsChequeras

    End Sub
    Private Sub frmBeneficiarios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        ' Response = oReportes.ValidacionBeneficiarios(lkpBeneficiarioInicial.EditValue, lkpBeneficiarioFinal.EditValue, dteFechaInicial.EditValue, dteFechaFinal.EditValue)
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de los controles"
    Private Sub lkpBeneficiarioInicial_Format() Handles lkpBeneficiarioInicial.Format
        for_movimientos_chequera_beneficiario_grl(Me.lkpBeneficiarioInicial)
    End Sub
    Private Sub lkpBeneficiarioInicial_LoadData(ByVal Initialize As Boolean) Handles lkpBeneficiarioInicial.LoadData
        CargaBeneficiarios(lkpBeneficiarioInicial)
    End Sub

    Private Sub lkpBeneficiarioFinal_Format() Handles lkpBeneficiarioFinal.Format
        for_movimientos_chequera_beneficiario_grl(Me.lkpBeneficiarioFinal)
    End Sub
    Private Sub lkpBeneficiarioFinal_LoadData(ByVal Initialize As Boolean) Handles lkpBeneficiarioFinal.LoadData
        CargaBeneficiarios(lkpBeneficiarioFinal)
    End Sub

    '@JGTO-18/04/2008: Se a�ade filtro por cuenta
    Private Sub lkpCuentaBancaria_Format() Handles lkpCuentaBancaria.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpCuentaBancaria)
    End Sub
    Private Sub lkpCuentaBancaria_LoadData(ByVal Initialize As Boolean) Handles lkpCuentaBancaria.LoadData
        Dim response As Events
        response = oChequeras.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCuentaBancaria.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpCuentaBancaria_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCuentaBancaria.EditValueChanged
        If Not lkpCuentaBancaria.EditValue Is Nothing Then
            Me.clcBanco.Text = Me.lkpCuentaBancaria.GetValue("banco")
            Me.clcBanco.Properties.Enabled = False
            Me.txtNombreBanco.Enabled = False
            Me.txtNombreBanco.EditValue = Me.clcBanco.EditValue
            Me.txtNombreBanco.Text = Me.txtNombreBanco.EditValue
            Me.txtNombreBanco.EditValue = Me.lkpCuentaBancaria.GetValue("nombre_banco")
        Else
            Me.clcBanco.Text = "-1"
            Me.txtNombreBanco.EditValue = ""
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function CargaBeneficiarios(ByRef Lookup As Dipros.Editors.TINMultiLookup)
        Dim oEvent As Events

        oEvent = oMovimientosChequera.ListadoBeneficiarios()
        If oEvent.ErrorFound Then
            oEvent.ShowError()
        Else
            Dim oDataSet As New DataSet
            oDataSet = oEvent.Value
            Lookup.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        oEvent = Nothing
    End Function
#End Region

End Class
