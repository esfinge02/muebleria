Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmRepRelacionPagoProveedores
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents grcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumentos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAPagar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblFechaCorte As System.Windows.Forms.Label
    Friend WithEvents grcFechaFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcProveedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents crcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grcDia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAnio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcAnioFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMesFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcProveedorFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConceptoFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio2doNivel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaFac As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioFactura2doNivel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte2doNivel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoAPagar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grvProveedores As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grPagoProveedores As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcDiaFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombre2doNivel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroMes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroMes2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grcModificado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents btnGuardarCambios As System.Windows.Forms.ToolBarButton
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Public WithEvents grcFechaPago2doNivel As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents RepositoryItemDateEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents grcFechaPago2doNivelFija As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepRelacionPagoProveedores))
        Me.grPagoProveedores = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcAnioFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroMes2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMesFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcProveedorFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDiaFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombre2doNivel = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConceptoFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio2doNivel = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaFac = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioFactura2doNivel = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaPago2doNivel = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.grcImporte2doNivel = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoAPagar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModificado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcFechaPago2doNivelFija = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvProveedores = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcAnio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroMes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcProveedor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcFolioFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaPago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.crcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumentos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAPagar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.lblFechaCorte = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.btnGuardarCambios = New System.Windows.Forms.ToolBarButton
        CType(Me.grPagoProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnGuardarCambios})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(6864, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'grPagoProveedores
        '
        '
        'grPagoProveedores.EmbeddedNavigator
        '
        Me.grPagoProveedores.EmbeddedNavigator.Name = ""
        Me.grPagoProveedores.LevelDefaults.Add("ProveedoresFacturas", Me.grvFacturas)
        Me.grPagoProveedores.Location = New System.Drawing.Point(8, 72)
        Me.grPagoProveedores.MainView = Me.grvProveedores
        Me.grPagoProveedores.Name = "grPagoProveedores"
        Me.grPagoProveedores.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1, Me.RepositoryItemDateEdit1, Me.RepositoryItemCheckEdit1, Me.RepositoryItemDateEdit2, Me.RepositoryItemDateEdit3})
        Me.grPagoProveedores.Size = New System.Drawing.Size(880, 424)
        Me.grPagoProveedores.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grPagoProveedores.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagoProveedores.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagoProveedores.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagoProveedores.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagoProveedores.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPagoProveedores.TabIndex = 2
        Me.grPagoProveedores.TabStop = False
        Me.grPagoProveedores.Text = "-1"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcAnioFactura, Me.grcNumeroMes2, Me.grcMesFactura, Me.grcProveedorFactura, Me.grcDiaFactura, Me.grcNombre2doNivel, Me.grcConceptoFactura, Me.grcFolio2doNivel, Me.grcFechaFac, Me.grcFolioFactura2doNivel, Me.grcFechaPago2doNivel, Me.grcImporte2doNivel, Me.grcSaldoAPagar, Me.grcModificado, Me.grcFechaPago2doNivelFija})
        Me.grvFacturas.GridControl = Me.grPagoProveedores
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        '
        'grcAnioFactura
        '
        Me.grcAnioFactura.Caption = "A�o"
        Me.grcAnioFactura.FieldName = "anio"
        Me.grcAnioFactura.Name = "grcAnioFactura"
        '
        'grcNumeroMes2
        '
        Me.grcNumeroMes2.Caption = "N�mero Mes"
        Me.grcNumeroMes2.FieldName = "numero_mes"
        Me.grcNumeroMes2.Name = "grcNumeroMes2"
        '
        'grcMesFactura
        '
        Me.grcMesFactura.Caption = "Mes"
        Me.grcMesFactura.FieldName = "mes"
        Me.grcMesFactura.Name = "grcMesFactura"
        '
        'grcProveedorFactura
        '
        Me.grcProveedorFactura.Caption = "Proveedor"
        Me.grcProveedorFactura.FieldName = "proveedor"
        Me.grcProveedorFactura.Name = "grcProveedorFactura"
        Me.grcProveedorFactura.Options = DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm
        '
        'grcDiaFactura
        '
        Me.grcDiaFactura.Caption = "D�a"
        Me.grcDiaFactura.FieldName = "dia"
        Me.grcDiaFactura.Name = "grcDiaFactura"
        '
        'grcNombre2doNivel
        '
        Me.grcNombre2doNivel.Caption = "Nombre"
        Me.grcNombre2doNivel.FieldName = "nombre"
        Me.grcNombre2doNivel.Name = "grcNombre2doNivel"
        Me.grcNombre2doNivel.Options = DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm
        '
        'grcConceptoFactura
        '
        Me.grcConceptoFactura.Caption = "Concepto"
        Me.grcConceptoFactura.FieldName = "concepto"
        Me.grcConceptoFactura.Name = "grcConceptoFactura"
        Me.grcConceptoFactura.Options = DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm
        Me.grcConceptoFactura.VisibleIndex = 0
        '
        'grcFolio2doNivel
        '
        Me.grcFolio2doNivel.Caption = "Folio"
        Me.grcFolio2doNivel.FieldName = "folio"
        Me.grcFolio2doNivel.Name = "grcFolio2doNivel"
        Me.grcFolio2doNivel.Options = DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm
        Me.grcFolio2doNivel.VisibleIndex = 1
        '
        'grcFechaFac
        '
        Me.grcFechaFac.Caption = "Fecha Factura"
        Me.grcFechaFac.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaFac.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaFac.FieldName = "fecha_factura"
        Me.grcFechaFac.Name = "grcFechaFac"
        Me.grcFechaFac.Options = DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm
        Me.grcFechaFac.VisibleIndex = 2
        '
        'grcFolioFactura2doNivel
        '
        Me.grcFolioFactura2doNivel.Caption = "Folio Factura"
        Me.grcFolioFactura2doNivel.FieldName = "folio_factura"
        Me.grcFolioFactura2doNivel.Name = "grcFolioFactura2doNivel"
        Me.grcFolioFactura2doNivel.Options = DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm
        Me.grcFolioFactura2doNivel.VisibleIndex = 3
        '
        'grcFechaPago2doNivel
        '
        Me.grcFechaPago2doNivel.Caption = "Fecha Pago"
        Me.grcFechaPago2doNivel.ColumnEdit = Me.RepositoryItemDateEdit3
        Me.grcFechaPago2doNivel.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaPago2doNivel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaPago2doNivel.FieldName = "fecha_pago"
        Me.grcFechaPago2doNivel.Name = "grcFechaPago2doNivel"
        Me.grcFechaPago2doNivel.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaPago2doNivel.VisibleIndex = 4
        '
        'RepositoryItemDateEdit3
        '
        Me.RepositoryItemDateEdit3.AutoHeight = False
        Me.RepositoryItemDateEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.Name = "RepositoryItemDateEdit3"
        '
        'grcImporte2doNivel
        '
        Me.grcImporte2doNivel.Caption = "Importe"
        Me.grcImporte2doNivel.DisplayFormat.FormatString = "$###,#0.00"
        Me.grcImporte2doNivel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte2doNivel.FieldName = "importe"
        Me.grcImporte2doNivel.Name = "grcImporte2doNivel"
        Me.grcImporte2doNivel.Options = DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm
        Me.grcImporte2doNivel.VisibleIndex = 5
        '
        'grcSaldoAPagar
        '
        Me.grcSaldoAPagar.Caption = "Saldo a Pagar"
        Me.grcSaldoAPagar.DisplayFormat.FormatString = "{0:c2}"
        Me.grcSaldoAPagar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoAPagar.FieldName = "saldo_a_pagar"
        Me.grcSaldoAPagar.Name = "grcSaldoAPagar"
        Me.grcSaldoAPagar.Options = DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm
        Me.grcSaldoAPagar.VisibleIndex = 6
        '
        'grcModificado
        '
        Me.grcModificado.Caption = "Modificado"
        Me.grcModificado.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcModificado.FieldName = "modificado"
        Me.grcModificado.Name = "grcModificado"
        Me.grcModificado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Enabled = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grcFechaPago2doNivelFija
        '
        Me.grcFechaPago2doNivelFija.Caption = "Fecha Pago Fija"
        Me.grcFechaPago2doNivelFija.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaPago2doNivelFija.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaPago2doNivelFija.FieldName = "fecha_factura_fijo"
        Me.grcFechaPago2doNivelFija.Name = "grcFechaPago2doNivelFija"
        Me.grcFechaPago2doNivelFija.VisibleIndex = 7
        '
        'grvProveedores
        '
        Me.grvProveedores.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcAnio, Me.grcNumeroMes, Me.grcMes, Me.grcProveedor, Me.grcDia, Me.grcNombre, Me.grcConcepto, Me.grcFolio, Me.grcFechaFactura, Me.grcFolioFactura, Me.grcFechaPago, Me.crcImporte, Me.grcDocumentos, Me.grcAPagar})
        Me.grvProveedores.GridControl = Me.grPagoProveedores
        Me.grvProveedores.GroupSummary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "saldo_a_pagar", Me.grcAPagar, "{0:c2}")})
        Me.grvProveedores.Name = "grvProveedores"
        Me.grvProveedores.OptionsBehavior.Editable = False
        Me.grvProveedores.OptionsCustomization.AllowFilter = False
        Me.grvProveedores.OptionsPrint.ExpandAllDetails = True
        Me.grvProveedores.OptionsView.ColumnAutoWidth = False
        Me.grvProveedores.OptionsView.ShowFooter = True
        Me.grvProveedores.OptionsView.ShowGroupPanel = False
        '
        'grcAnio
        '
        Me.grcAnio.Caption = "A�o"
        Me.grcAnio.FieldName = "anio"
        Me.grcAnio.Name = "grcAnio"
        Me.grcAnio.SortIndex = 3
        Me.grcAnio.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        '
        'grcNumeroMes
        '
        Me.grcNumeroMes.Caption = "N�mero Mes"
        Me.grcNumeroMes.FieldName = "numero_mes"
        Me.grcNumeroMes.Name = "grcNumeroMes"
        Me.grcNumeroMes.SortIndex = 2
        Me.grcNumeroMes.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        '
        'grcMes
        '
        Me.grcMes.Caption = "Mes"
        Me.grcMes.FieldName = "mes"
        Me.grcMes.GroupIndex = 0
        Me.grcMes.Name = "grcMes"
        Me.grcMes.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMes.SortIndex = 0
        Me.grcMes.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcMes.Width = 62
        '
        'grcProveedor
        '
        Me.grcProveedor.Caption = "Proveedor"
        Me.grcProveedor.FieldName = "proveedor"
        Me.grcProveedor.Name = "grcProveedor"
        '
        'grcDia
        '
        Me.grcDia.Caption = "D�a"
        Me.grcDia.FieldName = "dia"
        Me.grcDia.Name = "grcDia"
        '
        'grcNombre
        '
        Me.grcNombre.Caption = "Nombre"
        Me.grcNombre.FieldName = "nombre"
        Me.grcNombre.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left
        Me.grcNombre.Name = "grcNombre"
        Me.grcNombre.SortIndex = 1
        Me.grcNombre.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcNombre.VisibleIndex = 0
        Me.grcNombre.Width = 278
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        '
        'grcFechaFactura
        '
        Me.grcFechaFactura.Caption = "Fecha Factura"
        Me.grcFechaFactura.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcFechaFactura.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaFactura.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaFactura.FieldName = "fecha_factura"
        Me.grcFechaFactura.Name = "grcFechaFactura"
        Me.grcFechaFactura.VisibleIndex = 1
        Me.grcFechaFactura.Width = 104
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatString = "c2"
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.EditFormat.FormatString = "n2"
        Me.RepositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'grcFolioFactura
        '
        Me.grcFolioFactura.Caption = "Folio Factura"
        Me.grcFolioFactura.FieldName = "folio_factura"
        Me.grcFolioFactura.Name = "grcFolioFactura"
        Me.grcFolioFactura.VisibleIndex = 2
        Me.grcFolioFactura.Width = 86
        '
        'grcFechaPago
        '
        Me.grcFechaPago.Caption = "Fecha de Pago"
        Me.grcFechaPago.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.grcFechaPago.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaPago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaPago.FieldName = "fecha_pago"
        Me.grcFechaPago.Name = "grcFechaPago"
        Me.grcFechaPago.VisibleIndex = 3
        Me.grcFechaPago.Width = 117
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit1.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.RepositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'crcImporte
        '
        Me.crcImporte.Caption = "Importe"
        Me.crcImporte.DisplayFormat.FormatString = "$###,#0.00"
        Me.crcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.crcImporte.FieldName = "importe"
        Me.crcImporte.Name = "crcImporte"
        Me.crcImporte.VisibleIndex = 4
        Me.crcImporte.Width = 79
        '
        'grcDocumentos
        '
        Me.grcDocumentos.Caption = "Documentos"
        Me.grcDocumentos.DisplayFormat.FormatString = "$###,#0.00"
        Me.grcDocumentos.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDocumentos.FieldName = "documentos"
        Me.grcDocumentos.Name = "grcDocumentos"
        Me.grcDocumentos.VisibleIndex = 5
        Me.grcDocumentos.Width = 103
        '
        'grcAPagar
        '
        Me.grcAPagar.Caption = "A pagar"
        Me.grcAPagar.DisplayFormat.FormatString = "{0:c2}"
        Me.grcAPagar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcAPagar.FieldName = "saldo_a_pagar"
        Me.grcAPagar.MinWidth = 30
        Me.grcAPagar.Name = "grcAPagar"
        Me.grcAPagar.SummaryItem.DisplayFormat = "{0:c2}"
        Me.grcAPagar.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcAPagar.VisibleIndex = 6
        Me.grcAPagar.Width = 115
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'lblFechaCorte
        '
        Me.lblFechaCorte.AutoSize = True
        Me.lblFechaCorte.Location = New System.Drawing.Point(24, 40)
        Me.lblFechaCorte.Name = "lblFechaCorte"
        Me.lblFechaCorte.Size = New System.Drawing.Size(41, 16)
        Me.lblFechaCorte.TabIndex = 93
        Me.lblFechaCorte.Tag = ""
        Me.lblFechaCorte.Text = "&Fecha:"
        Me.lblFechaCorte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 10, 9, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(72, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "MMMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "MMMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha.Size = New System.Drawing.Size(112, 20)
        Me.dteFecha.TabIndex = 0
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(208, 40)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "Desglosado"
        Me.chkDesglosado.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(93, 22)
        Me.chkDesglosado.TabIndex = 1
        Me.chkDesglosado.Tag = ""
        Me.chkDesglosado.ToolTip = "desglosado"
        '
        'btnGuardarCambios
        '
        Me.btnGuardarCambios.Text = "Guardar Cambios"
        Me.btnGuardarCambios.ToolTipText = "Guardar cambios"
        '
        'frmRepRelacionPagoProveedores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(898, 503)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblFechaCorte)
        Me.Controls.Add(Me.grPagoProveedores)
        Me.Name = "frmRepRelacionPagoProveedores"
        Me.Text = "frmRepRelacionPagoProveedores"
        Me.Controls.SetChildIndex(Me.grPagoProveedores, 0)
        Me.Controls.SetChildIndex(Me.lblFechaCorte, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        CType(Me.grPagoProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As New VillarrealBusiness.Reportes
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepRelacionPagoProveedores_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oReportes.RelacionPagoProveedores(Me.dteFecha.EditValue, Me.chkDesglosado.EditValue)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Relaci�n de  Pagos a Proveedores no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oReport As New rptRelacionPagoProveedores
                oReport.oDesglosado = Me.chkDesglosado.Checked
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Relaci�n de  Pagos a Proveedores", oReport)
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub
    Private Sub frmRepRelacionPagoProveedores_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

        oReportes = New VillarrealBusiness.Reportes
        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)

    End Sub
    Private Sub frmRepRelacionPagoProveedores_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        ' Response = oReportes.Validacion(Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
    End Sub


    Private Sub frmRepRelacionPagoProveedores_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields


        Me.oReportes.TraerRelacionPagoProveedores(Me.dteFecha.EditValue)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            oDataSet.Tables(0).TableName = "Proveedores"
            oDataSet.Tables(1).TableName = "Facturas"

            oDataSet.EnforceConstraints = False

            oDataSet.Relations.Add("ProveedoresFacturas", New DataColumn() _
                                                                {oDataSet.Tables("Proveedores").Columns("anio"), _
                                                                oDataSet.Tables("Proveedores").Columns("mes"), _
                                                                oDataSet.Tables("Proveedores").Columns("proveedor")}, _
                                                            New DataColumn() _
                                                                {oDataSet.Tables("Facturas").Columns("anio"), _
                                                                oDataSet.Tables("Facturas").Columns("mes"), _
                                                                oDataSet.Tables("Facturas").Columns("proveedor")})

            grvProveedores.OptionsDetail.EnableMasterViewMode = True

            Me.grPagoProveedores.DataSource = oDataSet.Tables("Proveedores")


        End If
    End Sub


#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub dteFecha_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha.EditValueChanged
        Me.LlenarGrid()
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Public Sub LlenarGrid()

        Dim oDataSet As DataSet
        Dim response As New Events
        response = oReportes.TraerRelacionPagoProveedores(Me.dteFecha.EditValue)

        If Not response.ErrorFound Then

            oDataSet = response.Value
            Me.DataSource = oDataSet

            oDataSet.Tables(0).TableName = "Proveedores"
            oDataSet.Tables(1).TableName = "Facturas"

            oDataSet.EnforceConstraints = False

            oDataSet.Relations.Add("ProveedoresFacturas", New DataColumn() _
                                                                {oDataSet.Tables("Proveedores").Columns("anio"), _
                                                                oDataSet.Tables("Proveedores").Columns("mes"), _
                                                                oDataSet.Tables("Proveedores").Columns("proveedor")}, _
                                                            New DataColumn() _
                                                                {oDataSet.Tables("Facturas").Columns("anio"), _
                                                                oDataSet.Tables("Facturas").Columns("mes"), _
                                                                oDataSet.Tables("Facturas").Columns("proveedor")})
            grvProveedores.OptionsDetail.EnableMasterViewMode = True
            Me.grPagoProveedores.DataSource = oDataSet.Tables("Proveedores")
            Me.grvProveedores.ExpandAllGroups()
        End If
    End Sub
#End Region


    'Private Sub grvFacturas_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvFacturas.CellValueChanged
    '    If e.Column.FieldName = "fecha_pago" Then
    '        If IsDate(e.Value) Then
    '            Dim helpView As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
    '            helpView.SetRowCellValue(e.RowHandle, Me.grcModificado, True)
    '            helpView.UpdateCurrentRow()
    '        End If
    '    End If
    'End Sub
End Class
