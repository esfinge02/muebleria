
Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepMovimientosChequera
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblChequera As System.Windows.Forms.Label
    Friend WithEvents lkpChequera As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents lkpBanco As Dipros.Editors.TINMultiLookup
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepMovimientosChequera))
        Me.lblChequera = New System.Windows.Forms.Label
        Me.lkpChequera = New Dipros.Editors.TINMultiLookup
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lkpBanco = New Dipros.Editors.TINMultiLookup
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(23, 50)
        '
        'lblChequera
        '
        Me.lblChequera.AutoSize = True
        Me.lblChequera.Location = New System.Drawing.Point(74, 72)
        Me.lblChequera.Name = "lblChequera"
        Me.lblChequera.Size = New System.Drawing.Size(62, 16)
        Me.lblChequera.TabIndex = 2
        Me.lblChequera.Tag = ""
        Me.lblChequera.Text = "Chequera:"
        Me.lblChequera.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpChequera
        '
        Me.lkpChequera.AllowAdd = False
        Me.lkpChequera.AutoReaload = False
        Me.lkpChequera.DataSource = Nothing
        Me.lkpChequera.DefaultSearchField = ""
        Me.lkpChequera.DisplayMember = "cuenta_bancaria"
        Me.lkpChequera.EditValue = Nothing
        Me.lkpChequera.Enabled = False
        Me.lkpChequera.Filtered = False
        Me.lkpChequera.InitValue = Nothing
        Me.lkpChequera.Location = New System.Drawing.Point(144, 72)
        Me.lkpChequera.MultiSelect = False
        Me.lkpChequera.Name = "lkpChequera"
        Me.lkpChequera.NullText = "(Ninguno)"
        Me.lkpChequera.PopupWidth = CType(400, Long)
        Me.lkpChequera.Required = False
        Me.lkpChequera.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpChequera.SearchMember = ""
        Me.lkpChequera.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpChequera.SelectAll = False
        Me.lkpChequera.Size = New System.Drawing.Size(176, 20)
        Me.lkpChequera.TabIndex = 3
        Me.lkpChequera.Tag = "cuenta_bancaria"
        Me.lkpChequera.ToolTip = Nothing
        Me.lkpChequera.ValueMember = "cuenta_bancaria"
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(93, 48)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 0
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBanco
        '
        Me.lkpBanco.AllowAdd = False
        Me.lkpBanco.AutoReaload = False
        Me.lkpBanco.DataSource = Nothing
        Me.lkpBanco.DefaultSearchField = ""
        Me.lkpBanco.DisplayMember = "nombre"
        Me.lkpBanco.EditValue = Nothing
        Me.lkpBanco.Filtered = False
        Me.lkpBanco.InitValue = Nothing
        Me.lkpBanco.Location = New System.Drawing.Point(144, 48)
        Me.lkpBanco.MultiSelect = False
        Me.lkpBanco.Name = "lkpBanco"
        Me.lkpBanco.NullText = "(Ninguno)"
        Me.lkpBanco.PopupWidth = CType(400, Long)
        Me.lkpBanco.Required = False
        Me.lkpBanco.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBanco.SearchMember = ""
        Me.lkpBanco.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBanco.SelectAll = False
        Me.lkpBanco.Size = New System.Drawing.Size(176, 20)
        Me.lkpBanco.TabIndex = 1
        Me.lkpBanco.Tag = "banco"
        Me.lkpBanco.ToolTip = Nothing
        Me.lkpBanco.ValueMember = "banco"
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(29, 104)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(336, 43)
        Me.gpbFechas.TabIndex = 63
        Me.gpbFechas.TabStop = False
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(224, 13)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Fin.TabIndex = 7
        Me.dteFecha_Fin.ToolTip = "Fecha Hasta"
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(64, 13)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Ini.TabIndex = 5
        Me.dteFecha_Ini.ToolTip = "Fecha Desde"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "&Desde: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(184, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "&Hasta: "
        '
        'frmRepMovimientosChequera
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 167)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.lkpBanco)
        Me.Controls.Add(Me.lblChequera)
        Me.Controls.Add(Me.lkpChequera)
        Me.Name = "frmRepMovimientosChequera"
        Me.Text = "frmRepMovimientosChequera"
        Me.Controls.SetChildIndex(Me.lkpChequera, 0)
        Me.Controls.SetChildIndex(Me.lblChequera, 0)
        Me.Controls.SetChildIndex(Me.lkpBanco, 0)
        Me.Controls.SetChildIndex(Me.lblBanco, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oBancos As New VillarrealBusiness.clsBancos
    Private oChequeras As New VillarrealBusiness.clsChequeras


    'ReadOnly Property chequera(ByVal cuenta_bancaria As String) As Long
    '    Get
    '        Return CType(oChequeras.DespliegaDatos(cuenta_bancaria).Value, DataSet).Tables(0).Rows(0).Item("folio") + 1
    '    End Get
    'End Property
    Private ReadOnly Property Banco() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpBanco)

        End Get
    End Property
    Private ReadOnly Property Chequera() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpChequera)

        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepMovimientosChequera_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
'@ACH-25/06/07: Se modific� para que aceptara varias chequeras
        Response = oReportes.MovimientosChequera(Chequera, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)
'/@ACH-25/06/07
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Movimientos de Chequera no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oReport As New rptMovimientosChequera
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Movimientos de Chequera", oReport)
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub
    Private Sub frmRepMovimientosChequera_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

        oReportes = New VillarrealBusiness.Reportes
        oBancos = New VillarrealBusiness.clsBancos
        oChequeras = New VillarrealBusiness.clsChequeras

        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)

    End Sub
    Private Sub frmRepMovimientosChequera_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        '@ACH-25/06/07: Se modific� la validaci�n para que evalue varios bancos y chequeras
        Response = oReportes.Validacion(Me.lkpBanco.ToXML, Me.lkpChequera.ToXML, Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
        '/@ACH-25/06/07
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpChequera_Format() Handles lkpChequera.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpChequera)
    End Sub
    Private Sub lkpChequera_LoadData(ByVal Initialize As Boolean) Handles lkpChequera.LoadData
        '@ACH-25/06/07: Se modific� el codigo para permitir que se pueda seleccionar m�s de un banco
        Dim response As Events
        'Dim banco As Long


        'banco = Me.lkpBanco.EditValue '("banco")
        response = oChequeras.MultiLookup(lkpBanco.ToXML)
        'Banco = -1
        '/@ACH-25/06/07
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpChequera.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    'Private Sub lkpChequera_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpChequera.EditValueChanged

    'End Sub

    Private Sub lkpBanco_Format() Handles lkpBanco.Format
        Comunes.clsFormato.for_bancos_grl(Me.lkpBanco)
    End Sub
    Private Sub lkpBanco_LoadData(ByVal Initialize As Boolean) Handles lkpBanco.LoadData

        Dim response As Events
        response = oBancos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBanco.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpBanco_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBanco.EditValueChanged
        If lkpBanco.Text = "(Ninguno)" Then
            Me.lkpChequera.Enabled = False
            Me.lkpChequera.EditValue = Nothing
        Else
            Me.lkpChequera.Enabled = True
            Me.lkpChequera.EditValue = Nothing
            Me.lkpChequera_LoadData(True)
        End If

    End Sub


#End Region


End Class
