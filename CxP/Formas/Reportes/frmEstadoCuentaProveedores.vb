Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmEstadoCuentaProveedores
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents chkAmpliado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEstadoCuentaProveedores))
        Me.chkAmpliado = New DevExpress.XtraEditors.CheckEdit
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        CType(Me.chkAmpliado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(126, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'chkAmpliado
        '
        Me.chkAmpliado.Location = New System.Drawing.Point(255, 138)
        Me.chkAmpliado.Name = "chkAmpliado"
        '
        'chkAmpliado.Properties
        '
        Me.chkAmpliado.Properties.Caption = "Ampliado"
        Me.chkAmpliado.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkAmpliado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkAmpliado.Size = New System.Drawing.Size(96, 22)
        Me.chkAmpliado.TabIndex = 61
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(12, 74)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(339, 56)
        Me.gpbFechas.TabIndex = 60
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(71, 22)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(96, 22)
        Me.dteFecha_Ini.TabIndex = 1
        Me.dteFecha_Ini.ToolTip = "Fecha Inicial"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(217, 22)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(96, 22)
        Me.dteFecha_Fin.TabIndex = 3
        Me.dteFecha_Fin.ToolTip = "Fecha Final"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(177, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Ha&sta: "
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.BackColor = System.Drawing.SystemColors.Window
        Me.lblProveedor.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProveedor.ForeColor = System.Drawing.Color.Black
        Me.lblProveedor.Location = New System.Drawing.Point(17, 43)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 62
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "&Proveedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.BackColor = System.Drawing.SystemColors.Window
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpProveedor.ForeColor = System.Drawing.Color.Black
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(89, 43)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(300, Long)
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(256, 22)
        Me.lkpProveedor.TabIndex = 63
        Me.lkpProveedor.Tag = "proveedor"
        Me.lkpProveedor.ToolTip = "proveedor"
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'frmEstadoCuentaProveedores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 160)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.chkAmpliado)
        Me.Controls.Add(Me.gpbFechas)
        Me.Name = "frmEstadoCuentaProveedores"
        Me.Text = "frmEstadoCuentaProveedores"
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.chkAmpliado, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        CType(Me.chkAmpliado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private oReportes As VillarrealBusiness.Reportes
    Private oProveedores As VillarrealBusiness.clsProveedores

    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmEstadoCuentaProveedores_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.EstadoCuentaProveedor(Proveedor, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet

                If Me.chkAmpliado.Checked = False Then
                    Dim oReport As New rptEdoCuentaProveedor

                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    TINApp.ShowReport(Me.MdiParent, "Estado de Cuenta", oReport)
                    oReport = Nothing
                Else

                    Dim oReport As New rptEdoCuentaProveedorAmpliado

                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    TINApp.ShowReport(Me.MdiParent, "Estado de Cuenta", oReport)
                    oReport = Nothing

                End If
                oDataSet = Nothing
            Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub
    Private Sub frmEstadoCuentaProveedores_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oProveedores = New VillarrealBusiness.clsProveedores

        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
    End Sub
    Private Sub frmEstadoCuentaProveedores_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
        If Response.ErrorFound Then Exit Sub
        Response = oReportes.ValidaProveedor(Me.Proveedor)
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region

End Class
