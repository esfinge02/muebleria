Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepAntiguedadesSaldos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblFechaCorte As System.Windows.Forms.Label
    Friend WithEvents dteFechaCorte As DevExpress.XtraEditors.DateEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepAntiguedadesSaldos))
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.lblFechaCorte = New System.Windows.Forms.Label
        Me.dteFechaCorte = New DevExpress.XtraEditors.DateEdit
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(639, 28)
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.BackColor = System.Drawing.SystemColors.Window
        Me.lblProveedor.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProveedor.ForeColor = System.Drawing.Color.Black
        Me.lblProveedor.Location = New System.Drawing.Point(42, 40)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 1
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "&Proveedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.BackColor = System.Drawing.SystemColors.Window
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpProveedor.ForeColor = System.Drawing.Color.Black
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(113, 40)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(300, Long)
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(368, 20)
        Me.lkpProveedor.TabIndex = 2
        Me.lkpProveedor.Tag = "proveedor"
        Me.lkpProveedor.ToolTip = "proveedor"
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'lblFechaCorte
        '
        Me.lblFechaCorte.AutoSize = True
        Me.lblFechaCorte.Location = New System.Drawing.Point(17, 68)
        Me.lblFechaCorte.Name = "lblFechaCorte"
        Me.lblFechaCorte.Size = New System.Drawing.Size(90, 16)
        Me.lblFechaCorte.TabIndex = 3
        Me.lblFechaCorte.Tag = ""
        Me.lblFechaCorte.Text = "&Fecha de corte:"
        Me.lblFechaCorte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaCorte
        '
        Me.dteFechaCorte.EditValue = "31/05/2006"
        Me.dteFechaCorte.Location = New System.Drawing.Point(113, 68)
        Me.dteFechaCorte.Name = "dteFechaCorte"
        '
        'dteFechaCorte.Properties
        '
        Me.dteFechaCorte.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCorte.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCorte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaCorte.Size = New System.Drawing.Size(95, 20)
        Me.dteFechaCorte.TabIndex = 4
        Me.dteFechaCorte.Tag = "fechaCorte"
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(388, 66)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "Desglosado"
        Me.chkDesglosado.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(93, 22)
        Me.chkDesglosado.TabIndex = 5
        Me.chkDesglosado.Tag = "desglosado"
        Me.chkDesglosado.ToolTip = "desglosado"
        '
        'frmRepAntiguedadesSaldos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(498, 104)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.lblFechaCorte)
        Me.Controls.Add(Me.dteFechaCorte)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Name = "frmRepAntiguedadesSaldos"
        Me.Text = "frmRepAntiguedadesSaldos"
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        Me.Controls.SetChildIndex(Me.dteFechaCorte, 0)
        Me.Controls.SetChildIndex(Me.lblFechaCorte, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oProveedores As VillarrealBusiness.clsProveedores

    'Private ReadOnly Property Proveedor() As String
    '    Get
    '        Return Me.lkpProveedor.ToXML(Dipros.Editors.Encoding.ISO8859)
    '        'Return Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpProveedor.ToXML, "Proveedor")
    '    End Get
    'End Property
    Friend ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property

    'Friend ReadOnly Property NombreProveedor() As String
    '    Get
    '        Return IIf(Me.lkpProveedor.EditValue Is Nothing, "", CStr(Me.lkpProveedor.GetValue("nombre")))
    '    End Get
    'End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepAntiguedadesSaldos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept


        If Me.chkDesglosado.Checked = True Then
            'ANTIGUEDADES DE SALDOS DESGLOSADO
            Response = oReportes.AntiguedadesSaldosDesglosado(Proveedor, Me.dteFechaCorte.EditValue)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte de Antiguedades de Saldos Desglosado no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptAntiguedadesSaldosDesglosado
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Antiguedades de Saldos Desglosado", oReport)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If


        Else
            'ANTIGUEDADES DE SALDOS 
            Response = oReportes.AntiguedadesSaldos(Proveedor, Me.dteFechaCorte.EditValue)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte de Antiguedades de Saldos no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptAntiguedadesSaldos
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Antiguedades de Saldos", oReport)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If
        End If



    End Sub
    Private Sub frmRepAntiguedadesSaldos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

        oReportes = New VillarrealBusiness.Reportes
        oProveedores = New VillarrealBusiness.clsProveedores
        Me.dteFechaCorte.EditValue = CDate(TINApp.FechaServidor)

    End Sub
    Private Sub frmRepAntiguedadesSaldos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        ' Response = oReportes.Validacion(Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpProveedor_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpProveedor.EditValueChanged
    End Sub
#End Region

    Private Sub frmRepAntiguedadesSaldos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
