Imports Dipros.Utils.Common
Imports VillarrealBusiness.BusinessEnvironment

Public Class frmConceptosMovimientosChequera
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblConcepto As System.Windows.Forms.Label
		Friend WithEvents clcConcepto As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConceptosMovimientosChequera))
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.clcConcepto = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(409, 28)
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(28, 40)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 0
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "C&oncepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcConcepto
        '
        Me.clcConcepto.EditValue = "0"
        Me.clcConcepto.Location = New System.Drawing.Point(92, 40)
        Me.clcConcepto.MaxValue = 0
        Me.clcConcepto.MinValue = 0
        Me.clcConcepto.Name = "clcConcepto"
        '
        'clcConcepto.Properties
        '
        Me.clcConcepto.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcConcepto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcConcepto.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcConcepto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcConcepto.Properties.Enabled = False
        Me.clcConcepto.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcConcepto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcConcepto.Size = New System.Drawing.Size(36, 19)
        Me.clcConcepto.TabIndex = 1
        Me.clcConcepto.Tag = "concepto"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(10, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(92, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(340, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'frmConceptosMovimientosChequera
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(450, 96)
        Me.Controls.Add(Me.lblConcepto)
        Me.Controls.Add(Me.clcConcepto)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmConceptosMovimientosChequera"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcConcepto, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto, 0)
        CType(Me.clcConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oConceptosMovimientosChequera As VillarrealBusiness.clsConceptosMovimientosChequera
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
	Private Sub frmConceptosMovimientosChequera_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
		Select Case Action
			Case Actions.Insert
							Response = oConceptosMovimientosChequera.Insertar(Me.DataSource)

			Case Actions.Update
							Response = oConceptosMovimientosChequera.Actualizar(Me.DataSource)

			Case Actions.Delete
							Response = oConceptosMovimientosChequera.Eliminar(clcConcepto.value)

		End Select
	End Sub

	Private Sub frmConceptosMovimientosChequera_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
		Response = oConceptosMovimientosChequera.DespliegaDatos(OwnerForm.Value("concepto"))
		If Not Response.ErrorFound Then
			Dim oDataSet As DataSet
			oDataSet = Response.Value
			Me.DataSource = oDataSet
		End If

	End Sub

	Private Sub frmConceptosMovimientosChequera_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oConceptosMovimientosChequera = New VillarrealBusiness.clsConceptosMovimientosChequera

		Select Case Action
			Case Actions.Insert
			Case Actions.Update
			Case Actions.Delete
		End Select
	End Sub

    Private Sub frmConceptosMovimientosChequera_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim intControl As Long

        txtDescripcion.Text = Trim(txtDescripcion.Text)
        Response = oConceptosMovimientosChequera.Validacion(Action, txtDescripcion.Text, intControl)
        Select Case intControl
            Case 1
                txtDescripcion.Focus()
        End Select
    End Sub

    Private Sub frmConceptosMovimientosChequera_Localize() Handles MyBase.Localize
        Find("concepto", CType(clcConcepto.EditValue, Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
	Private Sub txtDescripcion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oConceptosMovimientosChequera.ValidaDescripcion(txtDescripcion.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
