
Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmChequeras
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents lkpBanco As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCuenta_Bancaria As System.Windows.Forms.Label
    Friend WithEvents txtCuenta_Bancaria As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents clcSaldo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkPredeterminada As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblCuentaContable As System.Windows.Forms.Label
    Friend WithEvents txtCuentaContable As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDescripcionCuenta As System.Windows.Forms.Label
    Friend WithEvents txtDescripcionCuenta As DevExpress.XtraEditors.TextEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmChequeras))
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lkpBanco = New Dipros.Editors.TINMultiLookup
        Me.lblCuenta_Bancaria = New System.Windows.Forms.Label
        Me.txtCuenta_Bancaria = New DevExpress.XtraEditors.TextEdit
        Me.lblSaldo = New System.Windows.Forms.Label
        Me.clcSaldo = New Dipros.Editors.TINCalcEdit
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.chkPredeterminada = New DevExpress.XtraEditors.CheckEdit
        Me.lblCuentaContable = New System.Windows.Forms.Label
        Me.txtCuentaContable = New DevExpress.XtraEditors.TextEdit
        Me.lblDescripcionCuenta = New System.Windows.Forms.Label
        Me.txtDescripcionCuenta = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtCuenta_Bancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPredeterminada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCuentaContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcionCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(135, 28)
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(104, 40)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 0
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBanco
        '
        Me.lkpBanco.AllowAdd = False
        Me.lkpBanco.AutoReaload = False
        Me.lkpBanco.DataSource = Nothing
        Me.lkpBanco.DefaultSearchField = ""
        Me.lkpBanco.DisplayMember = "Nombre"
        Me.lkpBanco.EditValue = Nothing
        Me.lkpBanco.Filtered = False
        Me.lkpBanco.InitValue = Nothing
        Me.lkpBanco.Location = New System.Drawing.Point(152, 39)
        Me.lkpBanco.MultiSelect = False
        Me.lkpBanco.Name = "lkpBanco"
        Me.lkpBanco.NullText = ""
        Me.lkpBanco.PopupWidth = CType(400, Long)
        Me.lkpBanco.Required = False
        Me.lkpBanco.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBanco.SearchMember = ""
        Me.lkpBanco.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBanco.SelectAll = False
        Me.lkpBanco.Size = New System.Drawing.Size(132, 20)
        Me.lkpBanco.TabIndex = 1
        Me.lkpBanco.Tag = "Banco"
        Me.lkpBanco.ToolTip = "Banco"
        Me.lkpBanco.ValueMember = "Banco"
        '
        'lblCuenta_Bancaria
        '
        Me.lblCuenta_Bancaria.AutoSize = True
        Me.lblCuenta_Bancaria.Location = New System.Drawing.Point(48, 63)
        Me.lblCuenta_Bancaria.Name = "lblCuenta_Bancaria"
        Me.lblCuenta_Bancaria.Size = New System.Drawing.Size(99, 16)
        Me.lblCuenta_Bancaria.TabIndex = 2
        Me.lblCuenta_Bancaria.Tag = ""
        Me.lblCuenta_Bancaria.Text = "&Cuenta bancaria:"
        Me.lblCuenta_Bancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCuenta_Bancaria
        '
        Me.txtCuenta_Bancaria.EditValue = ""
        Me.txtCuenta_Bancaria.Location = New System.Drawing.Point(152, 63)
        Me.txtCuenta_Bancaria.Name = "txtCuenta_Bancaria"
        '
        'txtCuenta_Bancaria.Properties
        '
        Me.txtCuenta_Bancaria.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuenta_Bancaria.Properties.MaxLength = 20
        Me.txtCuenta_Bancaria.Size = New System.Drawing.Size(132, 20)
        Me.txtCuenta_Bancaria.TabIndex = 3
        Me.txtCuenta_Bancaria.Tag = "cuenta_bancaria"
        Me.txtCuenta_Bancaria.ToolTip = "Cuenta bancaria"
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(104, 86)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(40, 16)
        Me.lblSaldo.TabIndex = 5
        Me.lblSaldo.Tag = ""
        Me.lblSaldo.Text = "&Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldo.Location = New System.Drawing.Point(152, 86)
        Me.clcSaldo.MaxValue = 0
        Me.clcSaldo.MinValue = 0
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSaldo.Properties.MaxLength = 15
        Me.clcSaldo.Properties.Precision = 2
        Me.clcSaldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldo.Size = New System.Drawing.Size(132, 19)
        Me.clcSaldo.TabIndex = 6
        Me.clcSaldo.Tag = "saldo"
        Me.clcSaldo.ToolTip = "Saldo"
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(112, 109)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 7
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio.Location = New System.Drawing.Point(152, 109)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.MaxLength = 8
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(60, 19)
        Me.clcFolio.TabIndex = 8
        Me.clcFolio.Tag = "folio"
        Me.clcFolio.ToolTip = "Folio"
        '
        'chkPredeterminada
        '
        Me.chkPredeterminada.Location = New System.Drawing.Point(288, 64)
        Me.chkPredeterminada.Name = "chkPredeterminada"
        '
        'chkPredeterminada.Properties
        '
        Me.chkPredeterminada.Properties.Caption = "Predeterminada"
        Me.chkPredeterminada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkPredeterminada.Size = New System.Drawing.Size(104, 19)
        Me.chkPredeterminada.TabIndex = 4
        Me.chkPredeterminada.Tag = "predeterminada"
        Me.chkPredeterminada.ToolTip = "Predeterminada"
        '
        'lblCuentaContable
        '
        Me.lblCuentaContable.AutoSize = True
        Me.lblCuentaContable.Location = New System.Drawing.Point(48, 134)
        Me.lblCuentaContable.Name = "lblCuentaContable"
        Me.lblCuentaContable.Size = New System.Drawing.Size(99, 16)
        Me.lblCuentaContable.TabIndex = 9
        Me.lblCuentaContable.Text = "C&uenta contable:"
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.EditValue = ""
        Me.txtCuentaContable.Location = New System.Drawing.Point(152, 132)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        '
        'txtCuentaContable.Properties
        '
        Me.txtCuentaContable.Properties.MaxLength = 40
        Me.txtCuentaContable.Size = New System.Drawing.Size(240, 20)
        Me.txtCuentaContable.TabIndex = 10
        Me.txtCuentaContable.Tag = "cuenta_contable"
        Me.txtCuentaContable.ToolTip = "Cuenta contable"
        '
        'lblDescripcionCuenta
        '
        Me.lblDescripcionCuenta.AutoSize = True
        Me.lblDescripcionCuenta.Location = New System.Drawing.Point(16, 160)
        Me.lblDescripcionCuenta.Name = "lblDescripcionCuenta"
        Me.lblDescripcionCuenta.Size = New System.Drawing.Size(131, 16)
        Me.lblDescripcionCuenta.TabIndex = 11
        Me.lblDescripcionCuenta.Text = "&Descripci�n de cuenta:"
        '
        'txtDescripcionCuenta
        '
        Me.txtDescripcionCuenta.EditValue = ""
        Me.txtDescripcionCuenta.Location = New System.Drawing.Point(152, 156)
        Me.txtDescripcionCuenta.Name = "txtDescripcionCuenta"
        '
        'txtDescripcionCuenta.Properties
        '
        Me.txtDescripcionCuenta.Properties.MaxLength = 100
        Me.txtDescripcionCuenta.Size = New System.Drawing.Size(240, 20)
        Me.txtDescripcionCuenta.TabIndex = 12
        Me.txtDescripcionCuenta.Tag = "descripcion_cuenta"
        Me.txtDescripcionCuenta.ToolTip = "Descripci�n de la cuenta"
        '
        'frmChequeras
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(410, 192)
        Me.Controls.Add(Me.txtDescripcionCuenta)
        Me.Controls.Add(Me.lblDescripcionCuenta)
        Me.Controls.Add(Me.txtCuentaContable)
        Me.Controls.Add(Me.lblCuentaContable)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.lkpBanco)
        Me.Controls.Add(Me.lblCuenta_Bancaria)
        Me.Controls.Add(Me.txtCuenta_Bancaria)
        Me.Controls.Add(Me.lblSaldo)
        Me.Controls.Add(Me.clcSaldo)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.chkPredeterminada)
        Me.Name = "frmChequeras"
        Me.Controls.SetChildIndex(Me.chkPredeterminada, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.clcSaldo, 0)
        Me.Controls.SetChildIndex(Me.lblSaldo, 0)
        Me.Controls.SetChildIndex(Me.txtCuenta_Bancaria, 0)
        Me.Controls.SetChildIndex(Me.lblCuenta_Bancaria, 0)
        Me.Controls.SetChildIndex(Me.lkpBanco, 0)
        Me.Controls.SetChildIndex(Me.lblBanco, 0)
        Me.Controls.SetChildIndex(Me.lblCuentaContable, 0)
        Me.Controls.SetChildIndex(Me.txtCuentaContable, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcionCuenta, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcionCuenta, 0)
        CType(Me.txtCuenta_Bancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPredeterminada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCuentaContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcionCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oChequeras As VillarrealBusiness.clsChequeras
    'creado por Christian Sequera 19-jul
    Private oBancos As VillarrealBusiness.clsBancos
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmChequeras_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert

                Response = oChequeras.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oChequeras.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oChequeras.Eliminar(txtCuenta_Bancaria.Text)

        End Select
    End Sub

    Private Sub frmChequeras_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oChequeras.DespliegaDatos(OwnerForm.Value("cuenta_bancaria"))

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmChequeras_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oChequeras = New VillarrealBusiness.clsChequeras
        oBancos = New VillarrealBusiness.clsBancos

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmChequeras_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oChequeras.Validacion(Action, Me.lkpBanco.EditValue, Me.txtCuenta_Bancaria.Text)
    End Sub

    Private Sub frmChequeras_Localize() Handles MyBase.Localize
        Find("cuenta_bancaria", Me.txtCuenta_Bancaria.Text)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpBanco_Format() Handles lkpBanco.Format
        Comunes.clsFormato.for_bancos_grl(Me.lkpBanco)
    End Sub

    Private Sub lkpBanco_LoadData(ByVal Initialize As Boolean) Handles lkpBanco.LoadData
        Dim response As Events
        response = oBancos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBanco.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub

   
    Private Sub chkPredeterminada_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPredeterminada.CheckedChanged

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

 
#End Region


  
End Class
