Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmAcreedores
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
    Private Entro_Despliega As Boolean = False
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblAcreedor As System.Windows.Forms.Label
    Friend WithEvents clcAcreedor As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblRfc As System.Windows.Forms.Label
    Friend WithEvents txtRfc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblMunicipio As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lblCp As System.Windows.Forms.Label
    Friend WithEvents clcCp As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTelefonos As System.Windows.Forms.Label
    Friend WithEvents txtTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpCiudad As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    Friend WithEvents txtCurp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkBaja As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCuenta As DevExpress.XtraEditors.TextEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAcreedores))
        Me.lblAcreedor = New System.Windows.Forms.Label
        Me.clcAcreedor = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblRfc = New System.Windows.Forms.Label
        Me.txtRfc = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblEstado = New System.Windows.Forms.Label
        Me.lblMunicipio = New System.Windows.Forms.Label
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lblCp = New System.Windows.Forms.Label
        Me.clcCp = New Dipros.Editors.TINCalcEdit
        Me.lblTelefonos = New System.Windows.Forms.Label
        Me.txtTelefonos = New DevExpress.XtraEditors.TextEdit
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.lkpCiudad = New Dipros.Editors.TINMultiLookup
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.txtCurp = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkBaja = New DevExpress.XtraEditors.CheckEdit
        Me.txtCuenta = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.clcAcreedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRfc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkBaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1255, 28)
        '
        'lblAcreedor
        '
        Me.lblAcreedor.AutoSize = True
        Me.lblAcreedor.Location = New System.Drawing.Point(21, 40)
        Me.lblAcreedor.Name = "lblAcreedor"
        Me.lblAcreedor.Size = New System.Drawing.Size(58, 16)
        Me.lblAcreedor.TabIndex = 0
        Me.lblAcreedor.Tag = ""
        Me.lblAcreedor.Text = "&Acreedor:"
        Me.lblAcreedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAcreedor
        '
        Me.clcAcreedor.EditValue = "0"
        Me.clcAcreedor.Location = New System.Drawing.Point(88, 40)
        Me.clcAcreedor.MaxValue = 0
        Me.clcAcreedor.MinValue = 0
        Me.clcAcreedor.Name = "clcAcreedor"
        '
        'clcAcreedor.Properties
        '
        Me.clcAcreedor.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcAcreedor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAcreedor.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcAcreedor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAcreedor.Properties.Enabled = False
        Me.clcAcreedor.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcAcreedor.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAcreedor.Size = New System.Drawing.Size(72, 19)
        Me.clcAcreedor.TabIndex = 1
        Me.clcAcreedor.Tag = "acreedor"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(26, 63)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(88, 62)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 100
        Me.txtNombre.Size = New System.Drawing.Size(472, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'lblRfc
        '
        Me.lblRfc.AutoSize = True
        Me.lblRfc.Location = New System.Drawing.Point(48, 86)
        Me.lblRfc.Name = "lblRfc"
        Me.lblRfc.Size = New System.Drawing.Size(31, 16)
        Me.lblRfc.TabIndex = 4
        Me.lblRfc.Tag = ""
        Me.lblRfc.Text = "&RFC:"
        Me.lblRfc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRfc
        '
        Me.txtRfc.EditValue = ""
        Me.txtRfc.Location = New System.Drawing.Point(88, 85)
        Me.txtRfc.Name = "txtRfc"
        '
        'txtRfc.Properties
        '
        Me.txtRfc.Properties.MaxLength = 20
        Me.txtRfc.Size = New System.Drawing.Size(120, 20)
        Me.txtRfc.TabIndex = 5
        Me.txtRfc.Tag = "rfc"
        '
        'lblDomicilio
        '
        Me.lblDomicilio.AutoSize = True
        Me.lblDomicilio.Location = New System.Drawing.Point(20, 134)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio.TabIndex = 8
        Me.lblDomicilio.Tag = ""
        Me.lblDomicilio.Text = "&Domicilio:"
        Me.lblDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(88, 131)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.MaxLength = 50
        Me.txtDomicilio.Size = New System.Drawing.Size(304, 20)
        Me.txtDomicilio.TabIndex = 9
        Me.txtDomicilio.Tag = "domicilio"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(33, 155)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 10
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "&Estado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMunicipio
        '
        Me.lblMunicipio.AutoSize = True
        Me.lblMunicipio.Location = New System.Drawing.Point(19, 174)
        Me.lblMunicipio.Name = "lblMunicipio"
        Me.lblMunicipio.Size = New System.Drawing.Size(60, 16)
        Me.lblMunicipio.TabIndex = 12
        Me.lblMunicipio.Tag = ""
        Me.lblMunicipio.Text = "&Municipio:"
        Me.lblMunicipio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(19, 198)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(60, 16)
        Me.lblCiudad.TabIndex = 14
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "Localidad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(29, 222)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 16
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "&Colonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCp
        '
        Me.lblCp.AutoSize = True
        Me.lblCp.Location = New System.Drawing.Point(55, 246)
        Me.lblCp.Name = "lblCp"
        Me.lblCp.Size = New System.Drawing.Size(24, 16)
        Me.lblCp.TabIndex = 18
        Me.lblCp.Tag = ""
        Me.lblCp.Text = "&Cp:"
        Me.lblCp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCp
        '
        Me.clcCp.EditValue = "0"
        Me.clcCp.Location = New System.Drawing.Point(88, 246)
        Me.clcCp.MaxValue = 0
        Me.clcCp.MinValue = 0
        Me.clcCp.Name = "clcCp"
        '
        'clcCp.Properties
        '
        Me.clcCp.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCp.Properties.MaxLength = 6
        Me.clcCp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCp.Size = New System.Drawing.Size(104, 19)
        Me.clcCp.TabIndex = 19
        Me.clcCp.Tag = "cp"
        '
        'lblTelefonos
        '
        Me.lblTelefonos.AutoSize = True
        Me.lblTelefonos.Location = New System.Drawing.Point(17, 270)
        Me.lblTelefonos.Name = "lblTelefonos"
        Me.lblTelefonos.Size = New System.Drawing.Size(62, 16)
        Me.lblTelefonos.TabIndex = 20
        Me.lblTelefonos.Tag = ""
        Me.lblTelefonos.Text = "&Telefonos:"
        Me.lblTelefonos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefonos
        '
        Me.txtTelefonos.EditValue = ""
        Me.txtTelefonos.Location = New System.Drawing.Point(88, 268)
        Me.txtTelefonos.Name = "txtTelefonos"
        '
        'txtTelefonos.Properties
        '
        Me.txtTelefonos.Properties.MaxLength = 50
        Me.txtTelefonos.Size = New System.Drawing.Size(300, 20)
        Me.txtTelefonos.TabIndex = 21
        Me.txtTelefonos.Tag = "telefonos"
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(88, 154)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.ReadOnlyControl = False
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(304, 20)
        Me.lkpEstado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEstado.TabIndex = 11
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'lkpCiudad
        '
        Me.lkpCiudad.AllowAdd = False
        Me.lkpCiudad.AutoReaload = True
        Me.lkpCiudad.DataSource = Nothing
        Me.lkpCiudad.DefaultSearchField = ""
        Me.lkpCiudad.DisplayMember = "descripcion"
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad.Filtered = False
        Me.lkpCiudad.InitValue = Nothing
        Me.lkpCiudad.Location = New System.Drawing.Point(88, 200)
        Me.lkpCiudad.MultiSelect = False
        Me.lkpCiudad.Name = "lkpCiudad"
        Me.lkpCiudad.NullText = ""
        Me.lkpCiudad.PopupWidth = CType(300, Long)
        Me.lkpCiudad.ReadOnlyControl = False
        Me.lkpCiudad.Required = False
        Me.lkpCiudad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCiudad.SearchMember = ""
        Me.lkpCiudad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCiudad.SelectAll = False
        Me.lkpCiudad.Size = New System.Drawing.Size(304, 20)
        Me.lkpCiudad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCiudad.TabIndex = 15
        Me.lkpCiudad.Tag = "ciudad"
        Me.lkpCiudad.ToolTip = Nothing
        Me.lkpCiudad.ValueMember = "ciudad"
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = True
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(88, 177)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(300, Long)
        Me.lkpMunicipio.ReadOnlyControl = False
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(304, 20)
        Me.lkpMunicipio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMunicipio.TabIndex = 13
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = True
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(88, 223)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(300, Long)
        Me.lkpColonia.ReadOnlyControl = False
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(304, 20)
        Me.lkpColonia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpColonia.TabIndex = 17
        Me.lkpColonia.Tag = "colonia"
        Me.lkpColonia.ToolTip = Nothing
        Me.lkpColonia.ValueMember = "colonia"
        '
        'txtCurp
        '
        Me.txtCurp.EditValue = ""
        Me.txtCurp.Location = New System.Drawing.Point(88, 108)
        Me.txtCurp.Name = "txtCurp"
        '
        'txtCurp.Properties
        '
        Me.txtCurp.Properties.MaxLength = 20
        Me.txtCurp.Size = New System.Drawing.Size(120, 20)
        Me.txtCurp.TabIndex = 7
        Me.txtCurp.Tag = "curp"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Tag = ""
        Me.Label2.Text = "CURP:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkBaja
        '
        Me.chkBaja.Location = New System.Drawing.Point(88, 312)
        Me.chkBaja.Name = "chkBaja"
        '
        'chkBaja.Properties
        '
        Me.chkBaja.Properties.Caption = "Baja"
        Me.chkBaja.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlText)
        Me.chkBaja.Size = New System.Drawing.Size(88, 19)
        Me.chkBaja.TabIndex = 24
        Me.chkBaja.Tag = "baja"
        '
        'txtCuenta
        '
        Me.txtCuenta.EditValue = ""
        Me.txtCuenta.Location = New System.Drawing.Point(88, 291)
        Me.txtCuenta.Name = "txtCuenta"
        '
        'txtCuenta.Properties
        '
        Me.txtCuenta.Properties.MaxLength = 40
        Me.txtCuenta.Size = New System.Drawing.Size(300, 20)
        Me.txtCuenta.TabIndex = 23
        Me.txtCuenta.Tag = "cuenta"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 293)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 22
        Me.Label3.Tag = ""
        Me.Label3.Text = "Cuenta:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmAcreedores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(570, 340)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCuenta)
        Me.Controls.Add(Me.chkBaja)
        Me.Controls.Add(Me.lkpCiudad)
        Me.Controls.Add(Me.lkpMunicipio)
        Me.Controls.Add(Me.lkpColonia)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lblAcreedor)
        Me.Controls.Add(Me.clcAcreedor)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblRfc)
        Me.Controls.Add(Me.txtRfc)
        Me.Controls.Add(Me.lblDomicilio)
        Me.Controls.Add(Me.txtDomicilio)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblMunicipio)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.lblCp)
        Me.Controls.Add(Me.clcCp)
        Me.Controls.Add(Me.lblTelefonos)
        Me.Controls.Add(Me.txtTelefonos)
        Me.Controls.Add(Me.txtCurp)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmAcreedores"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtCurp, 0)
        Me.Controls.SetChildIndex(Me.txtTelefonos, 0)
        Me.Controls.SetChildIndex(Me.lblTelefonos, 0)
        Me.Controls.SetChildIndex(Me.clcCp, 0)
        Me.Controls.SetChildIndex(Me.lblCp, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.lblCiudad, 0)
        Me.Controls.SetChildIndex(Me.lblMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.txtDomicilio, 0)
        Me.Controls.SetChildIndex(Me.lblDomicilio, 0)
        Me.Controls.SetChildIndex(Me.txtRfc, 0)
        Me.Controls.SetChildIndex(Me.lblRfc, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcAcreedor, 0)
        Me.Controls.SetChildIndex(Me.lblAcreedor, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        Me.Controls.SetChildIndex(Me.lkpColonia, 0)
        Me.Controls.SetChildIndex(Me.lkpMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lkpCiudad, 0)
        Me.Controls.SetChildIndex(Me.chkBaja, 0)
        Me.Controls.SetChildIndex(Me.txtCuenta, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        CType(Me.clcAcreedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRfc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkBaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oAcreedores As VillarrealBusiness.clsAcreedores

    Private oEstados As VillarrealBusiness.clsEstados
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private ociudades As VillarrealBusiness.clsCiudades
    'Private oVariables As New VillarrealBusiness.clsVariables
    Private oColonias As VillarrealBusiness.clsColonias

    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property
    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property
    Private ReadOnly Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCiudad)
        End Get
    End Property
    Private ReadOnly Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmAcreedores_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oAcreedores.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oAcreedores.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oAcreedores.Eliminar(clcAcreedor.value)

        End Select
    End Sub

    Private Sub frmAcreedores_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Entro_Despliega = True
        Response = oAcreedores.DespliegaDatos(OwnerForm.Value("acreedor"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            Me.lkpMunicipio.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad")), 0, oDataSet.Tables(0).Rows(0).Item("municipio"))
            Me.lkpCiudad.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad")), 0, oDataSet.Tables(0).Rows(0).Item("ciudad"))
            Me.lkpColonia.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia")), 0, oDataSet.Tables(0).Rows(0).Item("colonia"))

        End If

    End Sub

    Private Sub frmAcreedores_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oAcreedores = New VillarrealBusiness.clsAcreedores

        oEstados = New VillarrealBusiness.clsEstados
        oMunicipios = New VillarrealBusiness.clsMunicipios
        ociudades = New VillarrealBusiness.clsCiudades
        oColonias = New VillarrealBusiness.clsColonias

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmAcreedores_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oAcreedores.Validacion(Action, Me.txtNombre.Text, Me.txtDomicilio.Text, Estado, Municipio, Ciudad, Colonia)
    End Sub

    Private Sub frmAcreedores_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData
        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        'If Me.lkpMunicipio.DataSource Is Nothing Or Me.lkpMunicipio.EditValue Is Nothing Then Exit Sub
        If Entro_Despliega = True Then Exit Sub
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad_LoadData(True)

    End Sub

    Private Sub lkpCiudad_Format() Handles lkpCiudad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpCiudad)
    End Sub
    Private Sub lkpCiudad_LoadData(ByVal Initialize As Boolean) Handles lkpCiudad.LoadData
        Dim Response As New Events
        Response = ociudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCiudad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCiudad_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpCiudad.EditValueChanged
        If Me.lkpCiudad.EditValue Is Nothing Then Exit Sub

        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia_LoadData(True)
    End Sub

    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub
    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
