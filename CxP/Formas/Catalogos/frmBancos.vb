Imports Dipros.Utils.Common

Public Class frmBancos
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents clcBanco As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents txtSucursal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblEjecutivo_Cuenta As System.Windows.Forms.Label
    Friend WithEvents txtEjecutivo_Cuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblGerente As System.Windows.Forms.Label
    Friend WithEvents txtGerente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTel1 As System.Windows.Forms.Label
    Friend WithEvents txtTel1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTel2 As System.Windows.Forms.Label
    Friend WithEvents txtTel2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents clcCuenta As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBancos))
        Me.lblBanco = New System.Windows.Forms.Label
        Me.clcBanco = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.txtSucursal = New DevExpress.XtraEditors.TextEdit
        Me.lblDireccion = New System.Windows.Forms.Label
        Me.txtDireccion = New DevExpress.XtraEditors.TextEdit
        Me.lblEjecutivo_Cuenta = New System.Windows.Forms.Label
        Me.txtEjecutivo_Cuenta = New DevExpress.XtraEditors.TextEdit
        Me.lblGerente = New System.Windows.Forms.Label
        Me.txtGerente = New DevExpress.XtraEditors.TextEdit
        Me.lblTel1 = New System.Windows.Forms.Label
        Me.txtTel1 = New DevExpress.XtraEditors.TextEdit
        Me.lblTel2 = New System.Windows.Forms.Label
        Me.txtTel2 = New DevExpress.XtraEditors.TextEdit
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtFax = New DevExpress.XtraEditors.TextEdit
        Me.lblCuenta = New System.Windows.Forms.Label
        Me.clcCuenta = New Dipros.Editors.TINCalcEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEjecutivo_Cuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGerente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTel1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTel2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(255, 28)
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(73, 40)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 0
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcBanco
        '
        Me.clcBanco.EditValue = "0"
        Me.clcBanco.Location = New System.Drawing.Point(128, 40)
        Me.clcBanco.MaxValue = 0
        Me.clcBanco.MinValue = 0
        Me.clcBanco.Name = "clcBanco"
        '
        'clcBanco.Properties
        '
        Me.clcBanco.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcBanco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcBanco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.Enabled = False
        Me.clcBanco.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcBanco.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBanco.Size = New System.Drawing.Size(38, 19)
        Me.clcBanco.TabIndex = 1
        Me.clcBanco.Tag = "banco"
        Me.clcBanco.ToolTip = "Banco"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(63, 64)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(128, 64)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 30
        Me.txtNombre.Size = New System.Drawing.Size(208, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        Me.txtNombre.ToolTip = "Nombre"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(60, 88)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSucursal
        '
        Me.txtSucursal.EditValue = ""
        Me.txtSucursal.Location = New System.Drawing.Point(128, 88)
        Me.txtSucursal.Name = "txtSucursal"
        '
        'txtSucursal.Properties
        '
        Me.txtSucursal.Properties.MaxLength = 30
        Me.txtSucursal.Size = New System.Drawing.Size(208, 20)
        Me.txtSucursal.TabIndex = 5
        Me.txtSucursal.Tag = "sucursal"
        Me.txtSucursal.ToolTip = "Sucursal"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(56, 112)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(60, 16)
        Me.lblDireccion.TabIndex = 6
        Me.lblDireccion.Tag = ""
        Me.lblDireccion.Text = "&Direcci�n:"
        Me.lblDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDireccion
        '
        Me.txtDireccion.EditValue = ""
        Me.txtDireccion.Location = New System.Drawing.Point(128, 112)
        Me.txtDireccion.Name = "txtDireccion"
        '
        'txtDireccion.Properties
        '
        Me.txtDireccion.Properties.MaxLength = 35
        Me.txtDireccion.Size = New System.Drawing.Size(210, 20)
        Me.txtDireccion.TabIndex = 7
        Me.txtDireccion.Tag = "direccion"
        Me.txtDireccion.ToolTip = "Direcci�n"
        '
        'lblEjecutivo_Cuenta
        '
        Me.lblEjecutivo_Cuenta.AutoSize = True
        Me.lblEjecutivo_Cuenta.Location = New System.Drawing.Point(16, 136)
        Me.lblEjecutivo_Cuenta.Name = "lblEjecutivo_Cuenta"
        Me.lblEjecutivo_Cuenta.TabIndex = 8
        Me.lblEjecutivo_Cuenta.Tag = ""
        Me.lblEjecutivo_Cuenta.Text = "&Ejecutivo cuenta:"
        Me.lblEjecutivo_Cuenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEjecutivo_Cuenta
        '
        Me.txtEjecutivo_Cuenta.EditValue = ""
        Me.txtEjecutivo_Cuenta.Location = New System.Drawing.Point(128, 136)
        Me.txtEjecutivo_Cuenta.Name = "txtEjecutivo_Cuenta"
        '
        'txtEjecutivo_Cuenta.Properties
        '
        Me.txtEjecutivo_Cuenta.Properties.MaxLength = 35
        Me.txtEjecutivo_Cuenta.Size = New System.Drawing.Size(210, 20)
        Me.txtEjecutivo_Cuenta.TabIndex = 9
        Me.txtEjecutivo_Cuenta.Tag = "ejecutivo_cuenta"
        Me.txtEjecutivo_Cuenta.ToolTip = "Ejecutivo cuenta"
        '
        'lblGerente
        '
        Me.lblGerente.AutoSize = True
        Me.lblGerente.Location = New System.Drawing.Point(63, 160)
        Me.lblGerente.Name = "lblGerente"
        Me.lblGerente.Size = New System.Drawing.Size(53, 16)
        Me.lblGerente.TabIndex = 10
        Me.lblGerente.Tag = ""
        Me.lblGerente.Text = "&Gerente:"
        Me.lblGerente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtGerente
        '
        Me.txtGerente.EditValue = ""
        Me.txtGerente.Location = New System.Drawing.Point(128, 160)
        Me.txtGerente.Name = "txtGerente"
        '
        'txtGerente.Properties
        '
        Me.txtGerente.Properties.MaxLength = 35
        Me.txtGerente.Size = New System.Drawing.Size(210, 20)
        Me.txtGerente.TabIndex = 11
        Me.txtGerente.Tag = "gerente"
        Me.txtGerente.ToolTip = "Gerente"
        '
        'lblTel1
        '
        Me.lblTel1.AutoSize = True
        Me.lblTel1.Location = New System.Drawing.Point(49, 208)
        Me.lblTel1.Name = "lblTel1"
        Me.lblTel1.Size = New System.Drawing.Size(67, 16)
        Me.lblTel1.TabIndex = 14
        Me.lblTel1.Tag = ""
        Me.lblTel1.Text = "&Tel�fono 1:"
        Me.lblTel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTel1
        '
        Me.txtTel1.EditValue = ""
        Me.txtTel1.Location = New System.Drawing.Point(128, 208)
        Me.txtTel1.Name = "txtTel1"
        '
        'txtTel1.Properties
        '
        Me.txtTel1.Properties.MaxLength = 20
        Me.txtTel1.Size = New System.Drawing.Size(120, 20)
        Me.txtTel1.TabIndex = 15
        Me.txtTel1.Tag = "tel1"
        Me.txtTel1.ToolTip = "Tel�fono 1"
        '
        'lblTel2
        '
        Me.lblTel2.AutoSize = True
        Me.lblTel2.Location = New System.Drawing.Point(49, 232)
        Me.lblTel2.Name = "lblTel2"
        Me.lblTel2.Size = New System.Drawing.Size(67, 16)
        Me.lblTel2.TabIndex = 16
        Me.lblTel2.Tag = ""
        Me.lblTel2.Text = "&Tel�fono 2:"
        Me.lblTel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTel2
        '
        Me.txtTel2.EditValue = ""
        Me.txtTel2.Location = New System.Drawing.Point(128, 232)
        Me.txtTel2.Name = "txtTel2"
        '
        'txtTel2.Properties
        '
        Me.txtTel2.Properties.MaxLength = 20
        Me.txtTel2.Size = New System.Drawing.Size(120, 20)
        Me.txtTel2.TabIndex = 17
        Me.txtTel2.Tag = "tel2"
        Me.txtTel2.ToolTip = "Tel�fono 2"
        '
        'lblFax
        '
        Me.lblFax.AutoSize = True
        Me.lblFax.Location = New System.Drawing.Point(88, 256)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(28, 16)
        Me.lblFax.TabIndex = 18
        Me.lblFax.Tag = ""
        Me.lblFax.Text = "&Fax:"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFax
        '
        Me.txtFax.EditValue = ""
        Me.txtFax.Location = New System.Drawing.Point(128, 256)
        Me.txtFax.Name = "txtFax"
        '
        'txtFax.Properties
        '
        Me.txtFax.Properties.MaxLength = 20
        Me.txtFax.Size = New System.Drawing.Size(120, 20)
        Me.txtFax.TabIndex = 19
        Me.txtFax.Tag = "fax"
        Me.txtFax.ToolTip = "Fax"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(68, 184)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(48, 16)
        Me.lblCuenta.TabIndex = 12
        Me.lblCuenta.Tag = ""
        Me.lblCuenta.Text = "&Cuenta:"
        Me.lblCuenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCuenta
        '
        Me.clcCuenta.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCuenta.Location = New System.Drawing.Point(128, 184)
        Me.clcCuenta.MaxValue = 0
        Me.clcCuenta.MinValue = 0
        Me.clcCuenta.Name = "clcCuenta"
        '
        'clcCuenta.Properties
        '
        Me.clcCuenta.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCuenta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCuenta.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCuenta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCuenta.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCuenta.Properties.MaxLength = 8
        Me.clcCuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCuenta.Size = New System.Drawing.Size(62, 19)
        Me.clcCuenta.TabIndex = 13
        Me.clcCuenta.Tag = "cuenta"
        Me.clcCuenta.ToolTip = "Cuenta"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(27, 280)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 20
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(128, 280)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(208, 72)
        Me.txtObservaciones.TabIndex = 21
        Me.txtObservaciones.Tag = "observaciones"
        Me.txtObservaciones.ToolTip = "Observaciones"
        '
        'frmBancos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(354, 368)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.lblEjecutivo_Cuenta)
        Me.Controls.Add(Me.txtEjecutivo_Cuenta)
        Me.Controls.Add(Me.lblGerente)
        Me.Controls.Add(Me.txtGerente)
        Me.Controls.Add(Me.lblCuenta)
        Me.Controls.Add(Me.clcCuenta)
        Me.Controls.Add(Me.lblTel1)
        Me.Controls.Add(Me.txtTel1)
        Me.Controls.Add(Me.lblTel2)
        Me.Controls.Add(Me.txtTel2)
        Me.Controls.Add(Me.lblFax)
        Me.Controls.Add(Me.txtFax)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.clcBanco)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.txtSucursal)
        Me.Name = "frmBancos"
        Me.Controls.SetChildIndex(Me.txtSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcBanco, 0)
        Me.Controls.SetChildIndex(Me.lblBanco, 0)
        Me.Controls.SetChildIndex(Me.txtDireccion, 0)
        Me.Controls.SetChildIndex(Me.lblDireccion, 0)
        Me.Controls.SetChildIndex(Me.txtFax, 0)
        Me.Controls.SetChildIndex(Me.lblFax, 0)
        Me.Controls.SetChildIndex(Me.txtTel2, 0)
        Me.Controls.SetChildIndex(Me.lblTel2, 0)
        Me.Controls.SetChildIndex(Me.txtTel1, 0)
        Me.Controls.SetChildIndex(Me.lblTel1, 0)
        Me.Controls.SetChildIndex(Me.clcCuenta, 0)
        Me.Controls.SetChildIndex(Me.lblCuenta, 0)
        Me.Controls.SetChildIndex(Me.txtGerente, 0)
        Me.Controls.SetChildIndex(Me.lblGerente, 0)
        Me.Controls.SetChildIndex(Me.txtEjecutivo_Cuenta, 0)
        Me.Controls.SetChildIndex(Me.lblEjecutivo_Cuenta, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEjecutivo_Cuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGerente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTel1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTel2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oBancos As VillarrealBusiness.clsBancos
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmBancos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oBancos.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oBancos.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oBancos.Eliminar(clcBanco.Value)

        End Select
    End Sub

    Private Sub frmBancos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oBancos.DespliegaDatos(OwnerForm.Value("banco"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmBancos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oBancos = New VillarrealBusiness.clsBancos

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmBancos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oBancos.Validacion(Action, txtNombre.Text, Me.txtSucursal.EditValue, Me.txtDireccion.EditValue, Me.txtEjecutivo_Cuenta.EditValue, Me.txtGerente.EditValue, Me.clcCuenta.Value)
    End Sub

    Private Sub frmBancos_Localize() Handles MyBase.Localize
        Find("banco", Me.clcBanco.Value)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region



End Class
