Imports Dipros.Utils.Common
Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents mnuCatConceptosCxp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatBancos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatChequeras As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatProveedores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosPagar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosChequera As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProImpresionCheques As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProProgramacionPagos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProAcreedoresDiversos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepPagosFechaVencimiento As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepAntiguedadesSaldos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepVencimientosCxP As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMovimientosChequera As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepEstadoCuenta As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuProPagoVariasFacturas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepRelacionPagoProveedores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatAcreedores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatConceptosMovimientosChequera As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepBeneficiarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMovimientosNoAplicados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProDepositosBancarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDepositosBancarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatTiposDepositos As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatConceptosCxp = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatBancos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatChequeras = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatProveedores = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProAcreedoresDiversos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosPagar = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosChequera = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProImpresionCheques = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProProgramacionPagos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepPagosFechaVencimiento = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepAntiguedadesSaldos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepVencimientosCxP = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMovimientosChequera = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepEstadoCuenta = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuProPagoVariasFacturas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepRelacionPagoProveedores = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatAcreedores = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatConceptosMovimientosChequera = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepBeneficiarios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMovimientosNoAplicados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProDepositosBancarios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDepositosBancarios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatTiposDepositos = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatConceptosCxp, Me.mnuCatBancos, Me.mnuCatChequeras, Me.mnuCatProveedores, Me.mnuProAcreedoresDiversos, Me.mnuProMovimientosPagar, Me.mnuProMovimientosChequera, Me.mnuProImpresionCheques, Me.mnuProProgramacionPagos, Me.mnuRepPagosFechaVencimiento, Me.mnuRepAntiguedadesSaldos, Me.mnuRepVencimientosCxP, Me.mnuRepMovimientosChequera, Me.mnuRepEstadoCuenta, Me.BarStaticItem1, Me.mnuProPagoVariasFacturas, Me.mnuRepRelacionPagoProveedores, Me.mnuCatAcreedores, Me.mnuCatConceptosMovimientosChequera, Me.mnuRepBeneficiarios, Me.mnuRepMovimientosNoAplicados, Me.mnuProDepositosBancarios, Me.mnuRepDepositosBancarios, Me.mnuCatTiposDepositos})
        Me.BarManager.MaxItemId = 133
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatProveedores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatAcreedores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatConceptosCxp), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatBancos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatChequeras), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatConceptosMovimientosChequera), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatTiposDepositos)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProAcreedoresDiversos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProDepositosBancarios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosPagar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosChequera), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProProgramacionPagos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProImpresionCheques), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProPagoVariasFacturas)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepAntiguedadesSaldos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepBeneficiarios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepEstadoCuenta), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMovimientosChequera), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMovimientosNoAplicados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepPagosFechaVencimiento), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepVencimientosCxP), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepRelacionPagoProveedores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDepositosBancarios)})
        '
        'mnuCatConceptosCxp
        '
        Me.mnuCatConceptosCxp.Caption = "Conceptos Cxp"
        Me.mnuCatConceptosCxp.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatConceptosCxp.Id = 106
        Me.mnuCatConceptosCxp.ImageIndex = 3
        Me.mnuCatConceptosCxp.Name = "mnuCatConceptosCxp"
        '
        'mnuCatBancos
        '
        Me.mnuCatBancos.Caption = "Bancos"
        Me.mnuCatBancos.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatBancos.Id = 107
        Me.mnuCatBancos.ImageIndex = 1
        Me.mnuCatBancos.Name = "mnuCatBancos"
        '
        'mnuCatChequeras
        '
        Me.mnuCatChequeras.Caption = "Chequeras"
        Me.mnuCatChequeras.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatChequeras.Id = 108
        Me.mnuCatChequeras.ImageIndex = 2
        Me.mnuCatChequeras.Name = "mnuCatChequeras"
        '
        'mnuCatProveedores
        '
        Me.mnuCatProveedores.Caption = "Prov&eedores"
        Me.mnuCatProveedores.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatProveedores.Id = 109
        Me.mnuCatProveedores.ImageIndex = 0
        Me.mnuCatProveedores.Name = "mnuCatProveedores"
        '
        'mnuProAcreedoresDiversos
        '
        Me.mnuProAcreedoresDiversos.Caption = "Acreedores &Diversos"
        Me.mnuProAcreedoresDiversos.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProAcreedoresDiversos.Id = 110
        Me.mnuProAcreedoresDiversos.ImageIndex = 5
        Me.mnuProAcreedoresDiversos.Name = "mnuProAcreedoresDiversos"
        '
        'mnuProMovimientosPagar
        '
        Me.mnuProMovimientosPagar.Caption = "&Movimientos Pagar"
        Me.mnuProMovimientosPagar.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProMovimientosPagar.Id = 111
        Me.mnuProMovimientosPagar.ImageIndex = 11
        Me.mnuProMovimientosPagar.Name = "mnuProMovimientosPagar"
        '
        'mnuProMovimientosChequera
        '
        Me.mnuProMovimientosChequera.Caption = "Movimientos de C&hequera"
        Me.mnuProMovimientosChequera.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProMovimientosChequera.Id = 113
        Me.mnuProMovimientosChequera.ImageIndex = 4
        Me.mnuProMovimientosChequera.Name = "mnuProMovimientosChequera"
        '
        'mnuProImpresionCheques
        '
        Me.mnuProImpresionCheques.Caption = "&Impresi�n de Cheques"
        Me.mnuProImpresionCheques.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProImpresionCheques.Id = 114
        Me.mnuProImpresionCheques.ImageIndex = 7
        Me.mnuProImpresionCheques.Name = "mnuProImpresionCheques"
        '
        'mnuProProgramacionPagos
        '
        Me.mnuProProgramacionPagos.Caption = "Programaci�n de Pa&gos"
        Me.mnuProProgramacionPagos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProProgramacionPagos.Id = 115
        Me.mnuProProgramacionPagos.ImageIndex = 6
        Me.mnuProProgramacionPagos.Name = "mnuProProgramacionPagos"
        '
        'mnuRepPagosFechaVencimiento
        '
        Me.mnuRepPagosFechaVencimiento.Caption = "Pagos por Fecha de Vencimiento"
        Me.mnuRepPagosFechaVencimiento.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepPagosFechaVencimiento.Id = 116
        Me.mnuRepPagosFechaVencimiento.ImageIndex = 8
        Me.mnuRepPagosFechaVencimiento.Name = "mnuRepPagosFechaVencimiento"
        '
        'mnuRepAntiguedadesSaldos
        '
        Me.mnuRepAntiguedadesSaldos.Caption = "Antiguedades de &Saldos"
        Me.mnuRepAntiguedadesSaldos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepAntiguedadesSaldos.Id = 117
        Me.mnuRepAntiguedadesSaldos.ImageIndex = 10
        Me.mnuRepAntiguedadesSaldos.Name = "mnuRepAntiguedadesSaldos"
        '
        'mnuRepVencimientosCxP
        '
        Me.mnuRepVencimientosCxP.Caption = "Vencimientos de CxP"
        Me.mnuRepVencimientosCxP.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepVencimientosCxP.Id = 118
        Me.mnuRepVencimientosCxP.ImageIndex = 9
        Me.mnuRepVencimientosCxP.Name = "mnuRepVencimientosCxP"
        '
        'mnuRepMovimientosChequera
        '
        Me.mnuRepMovimientosChequera.Caption = "M&ovimientos de Chequera"
        Me.mnuRepMovimientosChequera.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMovimientosChequera.Id = 119
        Me.mnuRepMovimientosChequera.ImageIndex = 14
        Me.mnuRepMovimientosChequera.Name = "mnuRepMovimientosChequera"
        '
        'mnuRepEstadoCuenta
        '
        Me.mnuRepEstadoCuenta.Caption = "Estado de Cuenta"
        Me.mnuRepEstadoCuenta.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepEstadoCuenta.Id = 120
        Me.mnuRepEstadoCuenta.ImageIndex = 13
        Me.mnuRepEstadoCuenta.Name = "mnuRepEstadoCuenta"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(77, 475)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Offset = 5
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Id = 121
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'mnuProPagoVariasFacturas
        '
        Me.mnuProPagoVariasFacturas.Caption = "Pago a Varias &Facturas"
        Me.mnuProPagoVariasFacturas.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProPagoVariasFacturas.Id = 123
        Me.mnuProPagoVariasFacturas.ImageIndex = 15
        Me.mnuProPagoVariasFacturas.Name = "mnuProPagoVariasFacturas"
        '
        'mnuRepRelacionPagoProveedores
        '
        Me.mnuRepRelacionPagoProveedores.Caption = "Relaci�&n de Pago a Proveedores"
        Me.mnuRepRelacionPagoProveedores.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepRelacionPagoProveedores.Id = 124
        Me.mnuRepRelacionPagoProveedores.ImageIndex = 17
        Me.mnuRepRelacionPagoProveedores.Name = "mnuRepRelacionPagoProveedores"
        '
        'mnuCatAcreedores
        '
        Me.mnuCatAcreedores.Caption = "Acreedores Diversos"
        Me.mnuCatAcreedores.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatAcreedores.Id = 125
        Me.mnuCatAcreedores.ImageIndex = 16
        Me.mnuCatAcreedores.Name = "mnuCatAcreedores"
        '
        'mnuCatConceptosMovimientosChequera
        '
        Me.mnuCatConceptosMovimientosChequera.Caption = "C&onceptos de Movimientos"
        Me.mnuCatConceptosMovimientosChequera.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatConceptosMovimientosChequera.Id = 126
        Me.mnuCatConceptosMovimientosChequera.ImageIndex = 18
        Me.mnuCatConceptosMovimientosChequera.Name = "mnuCatConceptosMovimientosChequera"
        '
        'mnuRepBeneficiarios
        '
        Me.mnuRepBeneficiarios.Caption = "&Beneficiarios"
        Me.mnuRepBeneficiarios.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepBeneficiarios.Id = 128
        Me.mnuRepBeneficiarios.ImageIndex = 20
        Me.mnuRepBeneficiarios.Name = "mnuRepBeneficiarios"
        '
        'mnuRepMovimientosNoAplicados
        '
        Me.mnuRepMovimientosNoAplicados.Caption = "&Movimientos No Aplicados"
        Me.mnuRepMovimientosNoAplicados.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMovimientosNoAplicados.Id = 129
        Me.mnuRepMovimientosNoAplicados.ImageIndex = 21
        Me.mnuRepMovimientosNoAplicados.Name = "mnuRepMovimientosNoAplicados"
        '
        'mnuProDepositosBancarios
        '
        Me.mnuProDepositosBancarios.Caption = "&Dep�sitos Bancarios"
        Me.mnuProDepositosBancarios.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProDepositosBancarios.Id = 130
        Me.mnuProDepositosBancarios.ImageIndex = 19
        Me.mnuProDepositosBancarios.Name = "mnuProDepositosBancarios"
        '
        'mnuRepDepositosBancarios
        '
        Me.mnuRepDepositosBancarios.Caption = "Depositos Bancarios"
        Me.mnuRepDepositosBancarios.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDepositosBancarios.Id = 131
        Me.mnuRepDepositosBancarios.Name = "mnuRepDepositosBancarios"
        '
        'mnuCatTiposDepositos
        '
        Me.mnuCatTiposDepositos.Caption = "Tipos de D�positos"
        Me.mnuCatTiposDepositos.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatTiposDepositos.Id = 132
        Me.mnuCatTiposDepositos.Name = "mnuCatTiposDepositos"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim osucursales As New VillarrealBusiness.clsSucursales
        'Dim response As Dipros.Utils.Events
        'response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        'If Not response.ErrorFound Then
        '    Dim odataset As DataSet

        '    odataset = response.Value

        '    Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
        'End If
    End Sub

#End Region

#Region "Catalogos"
    Public Sub mnuCatAcreedores_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatAcreedores.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oAcreedores As New VillarrealBusiness.clsAcreedores
        With oGrid
            .Title = "Acreedores"
            .UpdateTitle = "Acreedores"
            .DataSource = AddressOf oAcreedores.Listado
            .UpdateForm = New frmAcreedores
            .Format = AddressOf for_acreedores_grs
            .MdiParent = Me
            .Show()
        End With
        oAcreedores = Nothing
    End Sub

    Public Sub mnuCatConceptosCxp_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatConceptosCxp.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oConceptosCxp As New VillarrealBusiness.clsConceptosCxp
        With oGrid
            .Title = "Conceptos de Cuentas por Pagar"
            .UpdateTitle = "Concepto de cuentas por pagar"
            .DataSource = AddressOf oConceptosCxp.Listado
            .UpdateForm = New frmConceptosCxp
            .Format = AddressOf for_conceptos_cxp_grs
            .Width = 600
            .MdiParent = Me
            .Show()
        End With
        oConceptosCxp = Nothing
    End Sub
    Public Sub mnuCatBancos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatBancos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oBancos As New VillarrealBusiness.clsBancos
        With oGrid
            .Title = "Bancos"
            .UpdateTitle = "Bancos"
            .DataSource = AddressOf oBancos.Listado
            .UpdateForm = New frmBancos
            .Format = AddressOf for_bancos_grs
            .MdiParent = Me
            .Show()
        End With
        oBancos = Nothing
    End Sub
    Public Sub mnuCatChequeras_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatChequeras.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oChequeras As New VillarrealBusiness.clsChequeras
        With oGrid
            .Title = "Chequeras"
            .UpdateTitle = "Chequeras"
            .DataSource = AddressOf oChequeras.Listado
            .UpdateForm = New frmChequeras
            .Format = AddressOf for_chequeras_grs
            .MdiParent = Me
            .Width = 600
            .Show()
        End With
        oChequeras = Nothing
    End Sub
    Public Sub mnuCatProveedores_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatProveedores.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oProveedores As New VillarrealBusiness.clsProveedores
        With oGrid
            .Title = "Proveedores"
            .UpdateTitle = "Proveedor"
            .DataSource = AddressOf oProveedores.Listado
            .UpdateForm = New Comunes.frmProveedores
            .Format = AddressOf Comunes.clsFormato.for_proveedores_grs
            .MdiParent = Me
            .Show()
        End With
        oProveedores = Nothing

    End Sub
    Public Sub mnuCatConceptosMovimientosChequera_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatConceptosMovimientosChequera.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oConceptosMovimientosChequera As New VillarrealBusiness.clsConceptosMovimientosChequera
        With oGrid
            .Title = "Conceptos de Movimientos de Chequera"
            .UpdateTitle = "Conceptos de Movimientos de Chequera"
            .DataSource = AddressOf oConceptosMovimientosChequera.Listado
            .UpdateFormType = GetType(frmConceptosMovimientosChequera)
            .Format = AddressOf for_conceptos_movimientos_chequera_grs
            .MdiParent = Me
            .Size = New Size(750, 500)
            .Show()
        End With
    End Sub
    Public Sub mnuCatTiposDepositos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatTiposDepositos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oTiposDepositos As New VillarrealBusiness.clsTiposDepositos
        With oGrid
            .Title = "Tipos de D�positos"
            .UpdateTitle = "Tipo de D�posito"
            .DataSource = AddressOf oTiposDepositos.Listado
            .UpdateForm = New Comunes.frmTiposDepositos
            .Format = AddressOf Comunes.clsFormato.for_tipos_depositos_grs
            .MdiParent = Me
            .Show()
        End With
        oTiposDepositos = Nothing

    End Sub
#End Region

#Region "Procesos"

    Public Sub mnuProMovimientosPagar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosPagar.ItemClick
        Dim oGrid As New brwMovimientosPagar
        With oGrid
            .MenuOption = e.Item
            .Title = "Movimientos por Pagar"
            .UpdateTitle = "Movimientos por Pagar"
            .UpdateForm = New frmMovimientosPagar
            .Format = AddressOf for_movimientos_pagar_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProAcreedoresDiversos_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProAcreedoresDiversos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oAcreedoresDiversos As New VillarrealBusiness.clsAcreedoresDiversos
        With oGrid
            .Title = "Acreedores Diversos"
            .UpdateTitle = "Acreedores Diversos"
            .DataSource = AddressOf oAcreedoresDiversos.Listado
            .UpdateForm = New frmAcreedoresDiversos
            .Format = AddressOf for_acreedores_diversos_grs
            .MdiParent = Me
            .Show()
        End With
        oAcreedoresDiversos = Nothing

    End Sub
    Public Sub mnuProMovimientosChequera_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosChequera.ItemClick
        Dim oGrid As New brwMovimientosChequera
        With oGrid
            .MenuOption = e.Item
            .Title = "Movimientos de Chequera"
            .UpdateTitle = "Movimientos de Chequera"
            .UpdateForm = New frmMovimientosChequera
            .Format = AddressOf for_movimientos_chequera_grs
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuProImpresionCheques_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProImpresionCheques.ItemClick
        Dim oForm As New frmImpresionCheques
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Impresi�n de Cheques"
        oForm.Action = Actions.Accept
        oForm.Show()
    End Sub
    Public Sub mnuProProgramacionPagos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProProgramacionPagos.ItemClick
        Dim oForm As New frmProgramacionPagos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Programaci�n de Pagos"
        oForm.Action = Actions.Accept
        oForm.Show()
    End Sub
    Public Sub mnuProPagoVariasFacturas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProPagoVariasFacturas.ItemClick

        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oPagos As New VillarrealBusiness.clsPagoFacturasProveedores
        With oGrid
            .MenuOption = e.Item
            .Title = "Pago a Varias Facturas"
            .UpdateTitle = "Pago a Varias Facturas"
            .UpdateForm = New frmPagosVariasFacturas
            .Format = AddressOf for_pago_facturas_proveedores_grs
            .DataSource = AddressOf oPagos.Listado
            .CanUpdate = False
            .MdiParent = Me
            .Show()
        End With

    End Sub
    Public Sub mnuProDepositosBancarios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProDepositosBancarios.ItemClick
        Dim oGrid As New Comunes.brwDepositosBancarios
        With oGrid
            .MenuOption = e.Item
            .Title = "Dep�sitos Bancarios"
            .MdiParent = Me
            .Show()
        End With

    End Sub
   
#End Region

#Region "Reportes"

    Public Sub mnuRepPagosFechaVencimiento_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepPagosFechaVencimiento.ItemClick
        Dim oForm As New frmPagosFechaVencimiento
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Pagos por Fecha de Vencimiento"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepAntiguedadesSaldos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepAntiguedadesSaldos.ItemClick
        Dim oForm As New frmRepAntiguedadesSaldos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Antiguedades de Saldos"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepVencimientosCxP_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepVencimientosCxP.ItemClick
        Dim oForm As New frmVencimientosCxP
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Vencimientos de CxP"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepMovimientosChequera_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMovimientosChequera.ItemClick
        Dim oForm As New frmRepMovimientosChequera
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Movimientos de Chequera"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepEstadoCuenta_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepEstadoCuenta.ItemClick
        Dim oForm As New frmEstadoCuentaProveedores
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Estado de Cuenta de Proveedores"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepRelacionPagoProveedores_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepRelacionPagoProveedores.ItemClick
        Dim oForm As New frmRepRelacionPagoProveedores
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Relacion de Pago a Proveedores"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    '@ACH-29/06/07: Nuevos reportes agregados
    Public Sub mnuRepBeneficiarios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepBeneficiarios.ItemClick
        Dim oForm As New frmBeneficiarios
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Reporte de Beneficiarios"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepMovimientosNoAplicados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMovimientosNoAplicados.ItemClick
        Dim oForm As New frmRepMovimientosNoAplicados
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Reporte de Movimientos No Aplicados"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    '/@ACH-29/06/07

    Public Sub mnuRepDepositosBancarios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDepositosBancarios.ItemClick
        Dim oForm As New Comunes.frmRepDepositosBancarios
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Depostios Bancarios"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
#End Region



   
   
End Class
