Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmAcreedoresDiversos
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
    Dim banCerrar As Boolean
    Private dImporte_anterior As Double = 0
    Private ActualizarCheque As Boolean
    Private Folio_Poliza_Cheque As Long = 0

#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio_Pago As System.Windows.Forms.Label
    Friend WithEvents lblFecha_Programacion As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Programacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblNumero_Documento As System.Windows.Forms.Label
    Friend WithEvents clcNumero_Documento As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha_Documento As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Documento As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblBeneficiario As System.Windows.Forms.Label
    Friend WithEvents txtBeneficiario As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblReferencia_Transferencia As System.Windows.Forms.Label
    Friend WithEvents clcReferencia_Transferencia As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents clcBanco As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha_Pago As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Pago As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCuenta_Bancaria As System.Windows.Forms.Label
    Friend WithEvents lblFolio_Cheque As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Cheque As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha_Deposito As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Deposito As DevExpress.XtraEditors.DateEdit
    Friend WithEvents chkAbono_Cuenta As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboTipoPago As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblTipoPago As System.Windows.Forms.Label
    Friend WithEvents lkpCuentaBancaria As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcFolio_Pago As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtNombreBanco As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnImprimeCheque As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents clcSubtotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblIva As System.Windows.Forms.Label
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents txtRFC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcIva As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lkpAcreedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoMovimiento As Dipros.Editors.TINMultiLookup
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nvrDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrPolizaCheque As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tmaPolizaCheque As Dipros.Windows.TINMaster
    Friend WithEvents grDatosPoliza As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDatosPoliza As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcConsecutivo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCuentaContable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepTxtConcepto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents grcCargo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepClcImporte As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepLkpCuentasContables As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAcreedoresDiversos))
        Me.lblFolio_Pago = New System.Windows.Forms.Label
        Me.lblFecha_Programacion = New System.Windows.Forms.Label
        Me.dteFecha_Programacion = New DevExpress.XtraEditors.DateEdit
        Me.lblNumero_Documento = New System.Windows.Forms.Label
        Me.clcNumero_Documento = New Dipros.Editors.TINCalcEdit
        Me.lblFecha_Documento = New System.Windows.Forms.Label
        Me.dteFecha_Documento = New DevExpress.XtraEditors.DateEdit
        Me.lblBeneficiario = New System.Windows.Forms.Label
        Me.txtBeneficiario = New DevExpress.XtraEditors.TextEdit
        Me.lblReferencia_Transferencia = New System.Windows.Forms.Label
        Me.clcReferencia_Transferencia = New Dipros.Editors.TINCalcEdit
        Me.lblBanco = New System.Windows.Forms.Label
        Me.clcBanco = New Dipros.Editors.TINCalcEdit
        Me.lblFecha_Pago = New System.Windows.Forms.Label
        Me.dteFecha_Pago = New DevExpress.XtraEditors.DateEdit
        Me.lblCuenta_Bancaria = New System.Windows.Forms.Label
        Me.lblFolio_Cheque = New System.Windows.Forms.Label
        Me.clcFolio_Cheque = New Dipros.Editors.TINCalcEdit
        Me.lblFecha_Deposito = New System.Windows.Forms.Label
        Me.dteFecha_Deposito = New DevExpress.XtraEditors.DateEdit
        Me.chkAbono_Cuenta = New DevExpress.XtraEditors.CheckEdit
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.txtConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lkpConceptoMovimiento = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNombreBanco = New DevExpress.XtraEditors.TextEdit
        Me.lkpCuentaBancaria = New Dipros.Editors.TINMultiLookup
        Me.cboTipoPago = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblTipoPago = New System.Windows.Forms.Label
        Me.clcFolio_Pago = New Dipros.Editors.TINCalcEdit
        Me.btnImprimeCheque = New System.Windows.Forms.ToolBarButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.clcSubtotal = New Dipros.Editors.TINCalcEdit
        Me.lblIva = New System.Windows.Forms.Label
        Me.lblImporte = New System.Windows.Forms.Label
        Me.txtRFC = New DevExpress.XtraEditors.TextEdit
        Me.clcIva = New Dipros.Editors.TINCalcEdit
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lkpAcreedor = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.nvrDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrPolizaCheque = New DevExpress.XtraNavBar.NavBarItem
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.tmaPolizaCheque = New Dipros.Windows.TINMaster
        Me.grDatosPoliza = New DevExpress.XtraGrid.GridControl
        Me.grvDatosPoliza = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcConsecutivo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCuentaContable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepTxtConcepto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.grcCargo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepClcImporte = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcAbono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepLkpCuentasContables = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        CType(Me.dteFecha_Programacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcNumero_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBeneficiario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcReferencia_Transferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Pago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio_Cheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Deposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbono_Cuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtNombreBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio_Pago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSubtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRFC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.grDatosPoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDatosPoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepTxtConcepto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepClcImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepLkpCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnImprimeCheque})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(8692, 28)
        '
        'lblFolio_Pago
        '
        Me.lblFolio_Pago.AutoSize = True
        Me.lblFolio_Pago.Location = New System.Drawing.Point(64, 16)
        Me.lblFolio_Pago.Name = "lblFolio_Pago"
        Me.lblFolio_Pago.Size = New System.Drawing.Size(83, 16)
        Me.lblFolio_Pago.TabIndex = 0
        Me.lblFolio_Pago.Tag = ""
        Me.lblFolio_Pago.Text = "&Folio de pago:"
        Me.lblFolio_Pago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha_Programacion
        '
        Me.lblFecha_Programacion.AutoSize = True
        Me.lblFecha_Programacion.Location = New System.Drawing.Point(344, 16)
        Me.lblFecha_Programacion.Name = "lblFecha_Programacion"
        Me.lblFecha_Programacion.Size = New System.Drawing.Size(138, 16)
        Me.lblFecha_Programacion.TabIndex = 2
        Me.lblFecha_Programacion.Tag = ""
        Me.lblFecha_Programacion.Text = "&Fecha de programaci�n:"
        Me.lblFecha_Programacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Programacion
        '
        Me.dteFecha_Programacion.EditValue = New Date(2006, 7, 21, 0, 0, 0, 0)
        Me.dteFecha_Programacion.Location = New System.Drawing.Point(488, 15)
        Me.dteFecha_Programacion.Name = "dteFecha_Programacion"
        '
        'dteFecha_Programacion.Properties
        '
        Me.dteFecha_Programacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Programacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Programacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Programacion.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Programacion.TabIndex = 3
        Me.dteFecha_Programacion.Tag = "fecha_programacion"
        Me.dteFecha_Programacion.ToolTip = "Fecha programaci�n"
        '
        'lblNumero_Documento
        '
        Me.lblNumero_Documento.AutoSize = True
        Me.lblNumero_Documento.Location = New System.Drawing.Point(32, 40)
        Me.lblNumero_Documento.Name = "lblNumero_Documento"
        Me.lblNumero_Documento.Size = New System.Drawing.Size(113, 16)
        Me.lblNumero_Documento.TabIndex = 4
        Me.lblNumero_Documento.Tag = ""
        Me.lblNumero_Documento.Text = "&N�mero de factura:"
        Me.lblNumero_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcNumero_Documento
        '
        Me.clcNumero_Documento.EditValue = "0"
        Me.clcNumero_Documento.Location = New System.Drawing.Point(152, 40)
        Me.clcNumero_Documento.MaxValue = 0
        Me.clcNumero_Documento.MinValue = 0
        Me.clcNumero_Documento.Name = "clcNumero_Documento"
        '
        'clcNumero_Documento.Properties
        '
        Me.clcNumero_Documento.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcNumero_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcNumero_Documento.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcNumero_Documento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcNumero_Documento.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcNumero_Documento.Properties.MaxLength = 8
        Me.clcNumero_Documento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcNumero_Documento.Size = New System.Drawing.Size(54, 19)
        Me.clcNumero_Documento.TabIndex = 5
        Me.clcNumero_Documento.Tag = "numero_documento"
        Me.clcNumero_Documento.ToolTip = "N�mero documento"
        '
        'lblFecha_Documento
        '
        Me.lblFecha_Documento.AutoSize = True
        Me.lblFecha_Documento.Location = New System.Drawing.Point(360, 40)
        Me.lblFecha_Documento.Name = "lblFecha_Documento"
        Me.lblFecha_Documento.Size = New System.Drawing.Size(124, 16)
        Me.lblFecha_Documento.TabIndex = 6
        Me.lblFecha_Documento.Tag = ""
        Me.lblFecha_Documento.Text = "&Fecha de documento:"
        Me.lblFecha_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Documento
        '
        Me.dteFecha_Documento.EditValue = New Date(2006, 7, 21, 0, 0, 0, 0)
        Me.dteFecha_Documento.Location = New System.Drawing.Point(488, 39)
        Me.dteFecha_Documento.Name = "dteFecha_Documento"
        '
        'dteFecha_Documento.Properties
        '
        Me.dteFecha_Documento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Documento.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Documento.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Documento.TabIndex = 7
        Me.dteFecha_Documento.Tag = "fecha_documento"
        Me.dteFecha_Documento.ToolTip = "Fecha documento"
        '
        'lblBeneficiario
        '
        Me.lblBeneficiario.AutoSize = True
        Me.lblBeneficiario.Location = New System.Drawing.Point(8, 88)
        Me.lblBeneficiario.Name = "lblBeneficiario"
        Me.lblBeneficiario.Size = New System.Drawing.Size(141, 16)
        Me.lblBeneficiario.TabIndex = 10
        Me.lblBeneficiario.Tag = ""
        Me.lblBeneficiario.Text = "Nombre del &Beneficiario:"
        Me.lblBeneficiario.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBeneficiario
        '
        Me.txtBeneficiario.EditValue = ""
        Me.txtBeneficiario.Location = New System.Drawing.Point(152, 88)
        Me.txtBeneficiario.Name = "txtBeneficiario"
        '
        'txtBeneficiario.Properties
        '
        Me.txtBeneficiario.Properties.Enabled = False
        Me.txtBeneficiario.Properties.MaxLength = 70
        Me.txtBeneficiario.Size = New System.Drawing.Size(430, 20)
        Me.txtBeneficiario.TabIndex = 11
        Me.txtBeneficiario.Tag = "beneficiario"
        Me.txtBeneficiario.ToolTip = "Beneficiario"
        '
        'lblReferencia_Transferencia
        '
        Me.lblReferencia_Transferencia.AutoSize = True
        Me.lblReferencia_Transferencia.Location = New System.Drawing.Point(344, 90)
        Me.lblReferencia_Transferencia.Name = "lblReferencia_Transferencia"
        Me.lblReferencia_Transferencia.Size = New System.Drawing.Size(143, 16)
        Me.lblReferencia_Transferencia.TabIndex = 31
        Me.lblReferencia_Transferencia.Tag = ""
        Me.lblReferencia_Transferencia.Text = "&Referencia transferencia:"
        Me.lblReferencia_Transferencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcReferencia_Transferencia
        '
        Me.clcReferencia_Transferencia.EditValue = "0"
        Me.clcReferencia_Transferencia.Location = New System.Drawing.Point(496, 88)
        Me.clcReferencia_Transferencia.MaxValue = 0
        Me.clcReferencia_Transferencia.MinValue = 0
        Me.clcReferencia_Transferencia.Name = "clcReferencia_Transferencia"
        '
        'clcReferencia_Transferencia.Properties
        '
        Me.clcReferencia_Transferencia.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcReferencia_Transferencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcReferencia_Transferencia.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcReferencia_Transferencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcReferencia_Transferencia.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcReferencia_Transferencia.Properties.MaxLength = 8
        Me.clcReferencia_Transferencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcReferencia_Transferencia.Size = New System.Drawing.Size(64, 19)
        Me.clcReferencia_Transferencia.TabIndex = 32
        Me.clcReferencia_Transferencia.Tag = "referencia_transferencia"
        Me.clcReferencia_Transferencia.ToolTip = "Referencia transferencia"
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(91, 42)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 23
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcBanco
        '
        Me.clcBanco.EditValue = "0"
        Me.clcBanco.Location = New System.Drawing.Point(242, 40)
        Me.clcBanco.MaxValue = 0
        Me.clcBanco.MinValue = 0
        Me.clcBanco.Name = "clcBanco"
        '
        'clcBanco.Properties
        '
        Me.clcBanco.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcBanco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcBanco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcBanco.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBanco.Size = New System.Drawing.Size(48, 19)
        Me.clcBanco.TabIndex = 25
        Me.clcBanco.Tag = "banco"
        Me.clcBanco.Visible = False
        '
        'lblFecha_Pago
        '
        Me.lblFecha_Pago.AutoSize = True
        Me.lblFecha_Pago.Location = New System.Drawing.Point(41, 67)
        Me.lblFecha_Pago.Name = "lblFecha_Pago"
        Me.lblFecha_Pago.Size = New System.Drawing.Size(93, 16)
        Me.lblFecha_Pago.TabIndex = 26
        Me.lblFecha_Pago.Tag = ""
        Me.lblFecha_Pago.Text = "&Fecha de  pago:"
        Me.lblFecha_Pago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Pago
        '
        Me.dteFecha_Pago.EditValue = New Date(2006, 7, 21, 0, 0, 0, 0)
        Me.dteFecha_Pago.Location = New System.Drawing.Point(138, 64)
        Me.dteFecha_Pago.Name = "dteFecha_Pago"
        '
        'dteFecha_Pago.Properties
        '
        Me.dteFecha_Pago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Pago.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Pago.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Pago.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Pago.TabIndex = 27
        Me.dteFecha_Pago.Tag = "fecha_pago"
        Me.dteFecha_Pago.ToolTip = "Fecha pago"
        '
        'lblCuenta_Bancaria
        '
        Me.lblCuenta_Bancaria.AutoSize = True
        Me.lblCuenta_Bancaria.Location = New System.Drawing.Point(35, 19)
        Me.lblCuenta_Bancaria.Name = "lblCuenta_Bancaria"
        Me.lblCuenta_Bancaria.Size = New System.Drawing.Size(99, 16)
        Me.lblCuenta_Bancaria.TabIndex = 21
        Me.lblCuenta_Bancaria.Tag = ""
        Me.lblCuenta_Bancaria.Text = "&Cuenta bancaria:"
        Me.lblCuenta_Bancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio_Cheque
        '
        Me.lblFolio_Cheque.AutoSize = True
        Me.lblFolio_Cheque.Location = New System.Drawing.Point(392, 66)
        Me.lblFolio_Cheque.Name = "lblFolio_Cheque"
        Me.lblFolio_Cheque.Size = New System.Drawing.Size(98, 16)
        Me.lblFolio_Cheque.TabIndex = 29
        Me.lblFolio_Cheque.Tag = ""
        Me.lblFolio_Cheque.Text = "&Folio del cheque:"
        Me.lblFolio_Cheque.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Cheque
        '
        Me.clcFolio_Cheque.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio_Cheque.Location = New System.Drawing.Point(496, 64)
        Me.clcFolio_Cheque.MaxValue = 0
        Me.clcFolio_Cheque.MinValue = 0
        Me.clcFolio_Cheque.Name = "clcFolio_Cheque"
        '
        'clcFolio_Cheque.Properties
        '
        Me.clcFolio_Cheque.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Cheque.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Cheque.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Cheque.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Cheque.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Cheque.Properties.MaxLength = 8
        Me.clcFolio_Cheque.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Cheque.Size = New System.Drawing.Size(64, 19)
        Me.clcFolio_Cheque.TabIndex = 30
        Me.clcFolio_Cheque.Tag = "folio_cheque"
        Me.clcFolio_Cheque.ToolTip = "Folio cheque"
        '
        'lblFecha_Deposito
        '
        Me.lblFecha_Deposito.AutoSize = True
        Me.lblFecha_Deposito.Location = New System.Drawing.Point(25, 91)
        Me.lblFecha_Deposito.Name = "lblFecha_Deposito"
        Me.lblFecha_Deposito.Size = New System.Drawing.Size(109, 16)
        Me.lblFecha_Deposito.TabIndex = 24
        Me.lblFecha_Deposito.Tag = ""
        Me.lblFecha_Deposito.Text = "&Fecha de dep�sito:"
        Me.lblFecha_Deposito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Deposito
        '
        Me.dteFecha_Deposito.EditValue = New Date(2006, 7, 21, 0, 0, 0, 0)
        Me.dteFecha_Deposito.Location = New System.Drawing.Point(138, 88)
        Me.dteFecha_Deposito.Name = "dteFecha_Deposito"
        '
        'dteFecha_Deposito.Properties
        '
        Me.dteFecha_Deposito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Deposito.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Deposito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Deposito.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Deposito.TabIndex = 28
        Me.dteFecha_Deposito.Tag = "fecha_deposito"
        Me.dteFecha_Deposito.ToolTip = "Fecha deposito"
        '
        'chkAbono_Cuenta
        '
        Me.chkAbono_Cuenta.EditValue = True
        Me.chkAbono_Cuenta.Location = New System.Drawing.Point(152, 304)
        Me.chkAbono_Cuenta.Name = "chkAbono_Cuenta"
        '
        'chkAbono_Cuenta.Properties
        '
        Me.chkAbono_Cuenta.Properties.Caption = "Abono A Cuenta"
        Me.chkAbono_Cuenta.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkAbono_Cuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAbono_Cuenta.Size = New System.Drawing.Size(110, 22)
        Me.chkAbono_Cuenta.TabIndex = 23
        Me.chkAbono_Cuenta.Tag = "abono_cuenta"
        Me.chkAbono_Cuenta.ToolTip = "Abono A cuenta"
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(88, 328)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 24
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "&Concepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(152, 328)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(430, 38)
        Me.txtConcepto.TabIndex = 25
        Me.txtConcepto.Tag = "concepto"
        Me.txtConcepto.ToolTip = "Concepto"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lkpConceptoMovimiento)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtNombreBanco)
        Me.GroupBox1.Controls.Add(Me.lkpCuentaBancaria)
        Me.GroupBox1.Controls.Add(Me.dteFecha_Pago)
        Me.GroupBox1.Controls.Add(Me.lblCuenta_Bancaria)
        Me.GroupBox1.Controls.Add(Me.lblFolio_Cheque)
        Me.GroupBox1.Controls.Add(Me.clcFolio_Cheque)
        Me.GroupBox1.Controls.Add(Me.lblFecha_Deposito)
        Me.GroupBox1.Controls.Add(Me.dteFecha_Deposito)
        Me.GroupBox1.Controls.Add(Me.lblReferencia_Transferencia)
        Me.GroupBox1.Controls.Add(Me.clcReferencia_Transferencia)
        Me.GroupBox1.Controls.Add(Me.lblBanco)
        Me.GroupBox1.Controls.Add(Me.clcBanco)
        Me.GroupBox1.Controls.Add(Me.lblFecha_Pago)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 184)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(568, 120)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        '
        'lkpConceptoMovimiento
        '
        Me.lkpConceptoMovimiento.AllowAdd = False
        Me.lkpConceptoMovimiento.AutoReaload = False
        Me.lkpConceptoMovimiento.DataSource = Nothing
        Me.lkpConceptoMovimiento.DefaultSearchField = ""
        Me.lkpConceptoMovimiento.DisplayMember = "descripcion"
        Me.lkpConceptoMovimiento.EditValue = Nothing
        Me.lkpConceptoMovimiento.Filtered = False
        Me.lkpConceptoMovimiento.InitValue = Nothing
        Me.lkpConceptoMovimiento.Location = New System.Drawing.Point(416, 16)
        Me.lkpConceptoMovimiento.MultiSelect = False
        Me.lkpConceptoMovimiento.Name = "lkpConceptoMovimiento"
        Me.lkpConceptoMovimiento.NullText = ""
        Me.lkpConceptoMovimiento.PopupWidth = CType(400, Long)
        Me.lkpConceptoMovimiento.Required = False
        Me.lkpConceptoMovimiento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoMovimiento.SearchMember = ""
        Me.lkpConceptoMovimiento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoMovimiento.SelectAll = False
        Me.lkpConceptoMovimiento.Size = New System.Drawing.Size(144, 20)
        Me.lkpConceptoMovimiento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoMovimiento.TabIndex = 82
        Me.lkpConceptoMovimiento.Tag = "concepto_movimiento"
        Me.lkpConceptoMovimiento.ToolTip = "Concepto del Movimiento de Chequera"
        Me.lkpConceptoMovimiento.ValueMember = "concepto"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(344, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 16)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Concepto:"
        '
        'txtNombreBanco
        '
        Me.txtNombreBanco.EditValue = ""
        Me.txtNombreBanco.Location = New System.Drawing.Point(138, 39)
        Me.txtNombreBanco.Name = "txtNombreBanco"
        '
        'txtNombreBanco.Properties
        '
        Me.txtNombreBanco.Properties.MaxLength = 70
        Me.txtNombreBanco.Size = New System.Drawing.Size(96, 20)
        Me.txtNombreBanco.TabIndex = 24
        Me.txtNombreBanco.Tag = "nombre_banco"
        Me.txtNombreBanco.ToolTip = "Nombre Banco"
        '
        'lkpCuentaBancaria
        '
        Me.lkpCuentaBancaria.AllowAdd = False
        Me.lkpCuentaBancaria.AutoReaload = False
        Me.lkpCuentaBancaria.DataSource = Nothing
        Me.lkpCuentaBancaria.DefaultSearchField = ""
        Me.lkpCuentaBancaria.DisplayMember = "cuenta_bancaria"
        Me.lkpCuentaBancaria.EditValue = Nothing
        Me.lkpCuentaBancaria.Filtered = False
        Me.lkpCuentaBancaria.InitValue = Nothing
        Me.lkpCuentaBancaria.Location = New System.Drawing.Point(138, 16)
        Me.lkpCuentaBancaria.MultiSelect = False
        Me.lkpCuentaBancaria.Name = "lkpCuentaBancaria"
        Me.lkpCuentaBancaria.NullText = ""
        Me.lkpCuentaBancaria.PopupWidth = CType(400, Long)
        Me.lkpCuentaBancaria.Required = False
        Me.lkpCuentaBancaria.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCuentaBancaria.SearchMember = ""
        Me.lkpCuentaBancaria.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCuentaBancaria.SelectAll = False
        Me.lkpCuentaBancaria.Size = New System.Drawing.Size(152, 20)
        Me.lkpCuentaBancaria.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCuentaBancaria.TabIndex = 22
        Me.lkpCuentaBancaria.Tag = "cuenta_bancaria"
        Me.lkpCuentaBancaria.ToolTip = "Cuenta bancaria"
        Me.lkpCuentaBancaria.ValueMember = "cuenta_bancaria"
        '
        'cboTipoPago
        '
        Me.cboTipoPago.EditValue = "P"
        Me.cboTipoPago.Location = New System.Drawing.Point(152, 136)
        Me.cboTipoPago.Name = "cboTipoPago"
        '
        'cboTipoPago.Properties
        '
        Me.cboTipoPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton("", DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoPago.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pendiente", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cheque", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Transferencia", "T", -1)})
        Me.cboTipoPago.Size = New System.Drawing.Size(150, 20)
        Me.cboTipoPago.TabIndex = 15
        Me.cboTipoPago.Tag = "tipo_pago"
        Me.cboTipoPago.ToolTip = "Tipo pago"
        '
        'lblTipoPago
        '
        Me.lblTipoPago.AutoSize = True
        Me.lblTipoPago.Location = New System.Drawing.Point(56, 136)
        Me.lblTipoPago.Name = "lblTipoPago"
        Me.lblTipoPago.Size = New System.Drawing.Size(92, 16)
        Me.lblTipoPago.TabIndex = 14
        Me.lblTipoPago.Tag = ""
        Me.lblTipoPago.Text = "&Forma de pago:"
        Me.lblTipoPago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Pago
        '
        Me.clcFolio_Pago.EditValue = "0"
        Me.clcFolio_Pago.Location = New System.Drawing.Point(152, 16)
        Me.clcFolio_Pago.MaxValue = 0
        Me.clcFolio_Pago.MinValue = 0
        Me.clcFolio_Pago.Name = "clcFolio_Pago"
        '
        'clcFolio_Pago.Properties
        '
        Me.clcFolio_Pago.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Pago.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Pago.Properties.Enabled = False
        Me.clcFolio_Pago.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Pago.Properties.MaxLength = 8
        Me.clcFolio_Pago.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Pago.Size = New System.Drawing.Size(54, 19)
        Me.clcFolio_Pago.TabIndex = 1
        Me.clcFolio_Pago.Tag = "folio_pago"
        Me.clcFolio_Pago.ToolTip = "Folio pago"
        '
        'btnImprimeCheque
        '
        Me.btnImprimeCheque.Enabled = False
        Me.btnImprimeCheque.Tag = ""
        Me.btnImprimeCheque.Text = "Imprimir Cheque"
        Me.btnImprimeCheque.ToolTipText = "Imprimir Cheque"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(120, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 16)
        Me.Label5.TabIndex = 12
        Me.Label5.Tag = ""
        Me.Label5.Text = "&RFC:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(432, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 16)
        Me.Label6.TabIndex = 16
        Me.Label6.Tag = ""
        Me.Label6.Text = "&Subtotal:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSubtotal
        '
        Me.clcSubtotal.EditValue = "0"
        Me.clcSubtotal.Location = New System.Drawing.Point(488, 112)
        Me.clcSubtotal.MaxValue = 0
        Me.clcSubtotal.MinValue = 0
        Me.clcSubtotal.Name = "clcSubtotal"
        '
        'clcSubtotal.Properties
        '
        Me.clcSubtotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSubtotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSubtotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSubtotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSubtotal.Size = New System.Drawing.Size(96, 19)
        Me.clcSubtotal.TabIndex = 17
        Me.clcSubtotal.Tag = "subtotal"
        Me.clcSubtotal.ToolTip = "Subtotal"
        '
        'lblIva
        '
        Me.lblIva.AutoSize = True
        Me.lblIva.Location = New System.Drawing.Point(456, 136)
        Me.lblIva.Name = "lblIva"
        Me.lblIva.Size = New System.Drawing.Size(27, 16)
        Me.lblIva.TabIndex = 18
        Me.lblIva.Tag = ""
        Me.lblIva.Text = "I&va:"
        Me.lblIva.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(448, 160)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(36, 16)
        Me.lblImporte.TabIndex = 20
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Total:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRFC
        '
        Me.txtRFC.EditValue = ""
        Me.txtRFC.Location = New System.Drawing.Point(152, 112)
        Me.txtRFC.Name = "txtRFC"
        '
        'txtRFC.Properties
        '
        Me.txtRFC.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRFC.Properties.Enabled = False
        Me.txtRFC.Properties.MaxLength = 70
        Me.txtRFC.Size = New System.Drawing.Size(96, 20)
        Me.txtRFC.TabIndex = 13
        Me.txtRFC.Tag = "rfc"
        Me.txtRFC.ToolTip = "RFC"
        '
        'clcIva
        '
        Me.clcIva.EditValue = "0"
        Me.clcIva.Location = New System.Drawing.Point(488, 136)
        Me.clcIva.MaxValue = 0
        Me.clcIva.MinValue = 0
        Me.clcIva.Name = "clcIva"
        '
        'clcIva.Properties
        '
        Me.clcIva.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcIva.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIva.Size = New System.Drawing.Size(96, 19)
        Me.clcIva.TabIndex = 19
        Me.clcIva.Tag = "iva"
        Me.clcIva.ToolTip = "Iva"
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(488, 160)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(96, 19)
        Me.clcImporte.TabIndex = 21
        Me.clcImporte.Tag = "importe"
        Me.clcImporte.ToolTip = "Total"
        '
        'lkpAcreedor
        '
        Me.lkpAcreedor.AllowAdd = False
        Me.lkpAcreedor.AutoReaload = False
        Me.lkpAcreedor.DataSource = Nothing
        Me.lkpAcreedor.DefaultSearchField = ""
        Me.lkpAcreedor.DisplayMember = "acreedor"
        Me.lkpAcreedor.EditValue = Nothing
        Me.lkpAcreedor.Filtered = False
        Me.lkpAcreedor.InitValue = Nothing
        Me.lkpAcreedor.Location = New System.Drawing.Point(152, 64)
        Me.lkpAcreedor.MultiSelect = False
        Me.lkpAcreedor.Name = "lkpAcreedor"
        Me.lkpAcreedor.NullText = ""
        Me.lkpAcreedor.PopupWidth = CType(400, Long)
        Me.lkpAcreedor.Required = False
        Me.lkpAcreedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAcreedor.SearchMember = ""
        Me.lkpAcreedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAcreedor.SelectAll = False
        Me.lkpAcreedor.Size = New System.Drawing.Size(94, 20)
        Me.lkpAcreedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpAcreedor.TabIndex = 9
        Me.lkpAcreedor.Tag = "acreedor"
        Me.lkpAcreedor.ToolTip = "Numero de Acreedor"
        Me.lkpAcreedor.ValueMember = "acreedor"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(88, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Tag = ""
        Me.Label2.Text = "Acreedor:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvrDatosGenerales, Me.nvrPolizaCheque})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 28)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(120, 404)
        Me.NavBarControl1.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.NavBarControl1.TabIndex = 95
        Me.NavBarControl1.Text = "Entrada al Almacen"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrPolizaCheque)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'nvrDatosGenerales
        '
        Me.nvrDatosGenerales.Caption = "Datos Generales"
        Me.nvrDatosGenerales.LargeImage = CType(resources.GetObject("nvrDatosGenerales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosGenerales.LargeImageIndex = 0
        Me.nvrDatosGenerales.Name = "nvrDatosGenerales"
        '
        'nvrPolizaCheque
        '
        Me.nvrPolizaCheque.Caption = "P�liza Cheque"
        Me.nvrPolizaCheque.Name = "nvrPolizaCheque"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.clcIva)
        Me.Panel1.Controls.Add(Me.clcImporte)
        Me.Panel1.Controls.Add(Me.dteFecha_Documento)
        Me.Panel1.Controls.Add(Me.txtBeneficiario)
        Me.Panel1.Controls.Add(Me.lblBeneficiario)
        Me.Panel1.Controls.Add(Me.lblNumero_Documento)
        Me.Panel1.Controls.Add(Me.chkAbono_Cuenta)
        Me.Panel1.Controls.Add(Me.lkpAcreedor)
        Me.Panel1.Controls.Add(Me.lblIva)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.dteFecha_Programacion)
        Me.Panel1.Controls.Add(Me.lblFolio_Pago)
        Me.Panel1.Controls.Add(Me.clcNumero_Documento)
        Me.Panel1.Controls.Add(Me.lblConcepto)
        Me.Panel1.Controls.Add(Me.lblFecha_Programacion)
        Me.Panel1.Controls.Add(Me.lblTipoPago)
        Me.Panel1.Controls.Add(Me.clcFolio_Pago)
        Me.Panel1.Controls.Add(Me.txtConcepto)
        Me.Panel1.Controls.Add(Me.cboTipoPago)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.lblFecha_Documento)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.clcSubtotal)
        Me.Panel1.Controls.Add(Me.lblImporte)
        Me.Panel1.Controls.Add(Me.txtRFC)
        Me.Panel1.Location = New System.Drawing.Point(128, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(592, 392)
        Me.Panel1.TabIndex = 96
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.tmaPolizaCheque)
        Me.Panel2.Controls.Add(Me.grDatosPoliza)
        Me.Panel2.Location = New System.Drawing.Point(128, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(592, 392)
        Me.Panel2.TabIndex = 97
        '
        'tmaPolizaCheque
        '
        Me.tmaPolizaCheque.BackColor = System.Drawing.Color.White
        Me.tmaPolizaCheque.CanDelete = True
        Me.tmaPolizaCheque.CanInsert = True
        Me.tmaPolizaCheque.CanUpdate = True
        Me.tmaPolizaCheque.Grid = Me.grDatosPoliza
        Me.tmaPolizaCheque.Location = New System.Drawing.Point(8, 8)
        Me.tmaPolizaCheque.Name = "tmaPolizaCheque"
        Me.tmaPolizaCheque.popTINGrid = Nothing
        Me.tmaPolizaCheque.Size = New System.Drawing.Size(576, 23)
        Me.tmaPolizaCheque.TabIndex = 13
        Me.tmaPolizaCheque.TabStop = False
        Me.tmaPolizaCheque.Title = ""
        Me.tmaPolizaCheque.UpdateTitle = "un Registro"
        '
        'grDatosPoliza
        '
        '
        'grDatosPoliza.EmbeddedNavigator
        '
        Me.grDatosPoliza.EmbeddedNavigator.Name = ""
        Me.grDatosPoliza.Location = New System.Drawing.Point(8, 33)
        Me.grDatosPoliza.MainView = Me.grvDatosPoliza
        Me.grDatosPoliza.Name = "grDatosPoliza"
        Me.grDatosPoliza.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepLkpCuentasContables, Me.RepClcImporte, Me.RepTxtConcepto})
        Me.grDatosPoliza.Size = New System.Drawing.Size(576, 352)
        Me.grDatosPoliza.Styles.AddReplace("Style2", New DevExpress.Utils.ViewStyleEx("Style2", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDatosPoliza.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDatosPoliza.TabIndex = 12
        Me.grDatosPoliza.TabStop = False
        Me.grDatosPoliza.Text = "GridControl1"
        '
        'grvDatosPoliza
        '
        Me.grvDatosPoliza.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcConsecutivo, Me.grcCuentaContable, Me.grcDescripcion, Me.grcCargo, Me.grcAbono})
        Me.grvDatosPoliza.GridControl = Me.grDatosPoliza
        Me.grvDatosPoliza.Name = "grvDatosPoliza"
        Me.grvDatosPoliza.OptionsBehavior.Editable = False
        Me.grvDatosPoliza.OptionsView.ShowFooter = True
        Me.grvDatosPoliza.OptionsView.ShowGroupPanel = False
        '
        'grcConsecutivo
        '
        Me.grcConsecutivo.Caption = "Consecutivo"
        Me.grcConsecutivo.FieldName = "partida"
        Me.grcConsecutivo.Name = "grcConsecutivo"
        Me.grcConsecutivo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCuentaContable
        '
        Me.grcCuentaContable.Caption = "Cuenta Contable"
        Me.grcCuentaContable.FieldName = "cuenta_contable"
        Me.grcCuentaContable.HeaderStyleName = "Style1"
        Me.grcCuentaContable.Name = "grcCuentaContable"
        Me.grcCuentaContable.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCuentaContable.VisibleIndex = 0
        Me.grcCuentaContable.Width = 140
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Concepto"
        Me.grcDescripcion.ColumnEdit = Me.RepTxtConcepto
        Me.grcDescripcion.FieldName = "concepto"
        Me.grcDescripcion.HeaderStyleName = "Style1"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 1
        Me.grcDescripcion.Width = 216
        '
        'RepTxtConcepto
        '
        Me.RepTxtConcepto.AutoHeight = False
        Me.RepTxtConcepto.MaxLength = 120
        Me.RepTxtConcepto.Name = "RepTxtConcepto"
        '
        'grcCargo
        '
        Me.grcCargo.Caption = "Cargo"
        Me.grcCargo.ColumnEdit = Me.RepClcImporte
        Me.grcCargo.FieldName = "cargo"
        Me.grcCargo.HeaderStyleName = "Style1"
        Me.grcCargo.Name = "grcCargo"
        Me.grcCargo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCargo.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcCargo.VisibleIndex = 2
        Me.grcCargo.Width = 110
        '
        'RepClcImporte
        '
        Me.RepClcImporte.AutoHeight = False
        Me.RepClcImporte.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepClcImporte.DisplayFormat.FormatString = "c2"
        Me.RepClcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepClcImporte.EditFormat.FormatString = "n2"
        Me.RepClcImporte.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepClcImporte.Name = "RepClcImporte"
        Me.RepClcImporte.Precision = 4
        '
        'grcAbono
        '
        Me.grcAbono.Caption = "Abono"
        Me.grcAbono.ColumnEdit = Me.RepClcImporte
        Me.grcAbono.FieldName = "abono"
        Me.grcAbono.HeaderStyleName = "Style1"
        Me.grcAbono.Name = "grcAbono"
        Me.grcAbono.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcAbono.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcAbono.VisibleIndex = 3
        Me.grcAbono.Width = 96
        '
        'RepLkpCuentasContables
        '
        Me.RepLkpCuentasContables.AutoHeight = False
        Me.RepLkpCuentasContables.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepLkpCuentasContables.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("cuentacontable", "Cuenta", 40, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Center), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripci�n", 50)})
        Me.RepLkpCuentasContables.DisplayMember = "cuentacontable"
        Me.RepLkpCuentasContables.Name = "RepLkpCuentasContables"
        Me.RepLkpCuentasContables.NullText = ""
        Me.RepLkpCuentasContables.PopupWidth = 200
        Me.RepLkpCuentasContables.ValueMember = "cuentacontable"
        '
        'frmAcreedoresDiversos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(730, 432)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "frmAcreedoresDiversos"
        Me.Controls.SetChildIndex(Me.Panel2, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        CType(Me.dteFecha_Programacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcNumero_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBeneficiario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcReferencia_Transferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Pago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio_Cheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Deposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbono_Cuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtNombreBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio_Pago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSubtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRFC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.grDatosPoliza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDatosPoliza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepTxtConcepto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepClcImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepLkpCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oAcreedoresDiversos As VillarrealBusiness.clsAcreedoresDiversos
    Private oChequeras As VillarrealBusiness.clsChequeras
    Private oAcreedores As VillarrealBusiness.clsAcreedores
    Private oConceptosMovimientosChequera As New VillarrealBusiness.clsConceptosMovimientosChequera
    Private oMovimientosChequera As New VillarrealBusiness.clsMovimientosChequera
    Private oPolizasCheques As VillarrealBusiness.clsPolizasCheques

    Private ReadOnly Property CuentaBancaria() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpCuentaBancaria)
        End Get
    End Property

    ReadOnly Property FolioChequera(ByVal cuenta_bancaria As String) As Long
        Get
            Return CType(oChequeras.DespliegaDatos(cuenta_bancaria).Value, DataSet).Tables(0).Rows(0).Item("folio") + 1
        End Get
    End Property

    Private ReadOnly Property Acreedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpAcreedor)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmAcreedoresDiversos_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmAcreedoresDiversos_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmAcreedoresDiversos_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmAcreedoresDiversos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Me.ObtenerFolioPolizaCheque(Response)
                Response = oAcreedoresDiversos.Insertar(Me.DataSource, ActualizarCheque, Me.clcSubtotal.EditValue, Me.clcIva.EditValue, Me.txtRFC.Text, Me.Folio_Poliza_Cheque)

            Case Actions.Update
                Me.ObtenerFolioPolizaCheque(Response)
                Response = oAcreedoresDiversos.Actualizar(Me.DataSource, ActualizarCheque, dImporte_anterior, Me.clcSubtotal.EditValue, Me.clcIva.EditValue, Me.txtRFC.Text, Me.Folio_Poliza_Cheque)

            Case Actions.Delete
                Response = oAcreedoresDiversos.Eliminar(clcFolio_Pago.Value)
        End Select
    End Sub
    Private Sub frmAcreedoresDiversos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oAcreedoresDiversos.DespliegaDatos(OwnerForm.Value("folio_pago"))

        Dim oDataSet As DataSet
        If Not Response.ErrorFound Then

            oDataSet = Response.Value
            Me.DataSource = oDataSet

            dImporte_anterior = Me.clcImporte.Value

            ' OBTENGO EL FOLIO DE LA POLIZA 
            Folio_Poliza_Cheque = oDataSet.Tables(0).Rows(0).Item("folio_poliza_cheque")
        End If
        If Me.clcFolio_Cheque.EditValue > 0 Then
            Me.EnabledEdit(False)

        End If

        'Obtengo los datos de la poliza del cheque
        Response = Me.oPolizasCheques.DespliegaDatos(Me.Folio_Poliza_Cheque)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaPolizaCheque.DataSource = oDataSet
        End If
    End Sub
    Private Sub frmAcreedoresDiversos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
        oAcreedoresDiversos = New VillarrealBusiness.clsAcreedoresDiversos
        oAcreedores = New VillarrealBusiness.clsAcreedores
        oChequeras = New VillarrealBusiness.clsChequeras
        oPolizasCheques = New VillarrealBusiness.clsPolizasCheques

        Me.cboTipoPago.Properties.Items.GetItem(0)
        Me.cboTipoPago.Properties.Items.GetItemDescription(0)
        Me.cboTipoPago.Value = ""

        With Me.tmaPolizaCheque
            .UpdateTitle = "Un Registro"
            .UpdateForm = New Comunes.frmPolizaChequeDetalle
            '.AddColumn("partida")
            .AddColumn("cuenta_contable", "System.String")
            .AddColumn("concepto", "System.String")
            .AddColumn("cargo", "System.Double")
            .AddColumn("abono", "System.Double")
        End With

        Select Case Action
            Case Actions.Insert
                Me.dteFecha_Deposito.EditValue = CDate(TINApp.FechaServidor)
                Me.dteFecha_Documento.EditValue = CDate(TINApp.FechaServidor)
                Me.dteFecha_Pago.EditValue = CDate(TINApp.FechaServidor)
                Me.dteFecha_Programacion.EditValue = CDate(TINApp.FechaServidor)
            Case Actions.Update
                If Me.cboTipoPago.Value <> "T" And Me.cboTipoPago.Value <> "C" Then
                    Me.cboTipoPago.Value = "P"
                End If
                Me.btnImprimeCheque.Enabled = True
            Case Actions.Delete
                If Me.cboTipoPago.Value <> "T" And Me.cboTipoPago.Value <> "C" Then
                    Me.cboTipoPago.Value = "P"
                End If
                Me.btnImprimeCheque.Enabled = True
        End Select
    End Sub
    Private Sub frmAcreedoresDiversos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oAcreedoresDiversos.Validacion(Action, clcNumero_Documento.Value, clcImporte.Value, Me.dteFecha_Documento.Text, Me.cboTipoPago.Value, CuentaBancaria, Me.clcReferencia_Transferencia.Value, Me.txtBeneficiario.EditValue, Me.txtRFC.Text, Me.clcSubtotal.EditValue, Me.clcIva.EditValue, Me.lkpConceptoMovimiento.EditValue) 'Me.clcFolio_Cheque.Value
    End Sub

    Private Sub frmAcreedoresDiversos_Localize() Handles MyBase.Localize
        Find("folio_pago", Me.clcFolio_Pago.Value)

    End Sub
    Private Sub frmAcreedoresDiversos_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        '  Tipo Pago
        '====================
        ' el 1 son movimientos pagar
        ' el 2 son pagos diversos
        ' el 3 son movimientos chequera

        Me.tmaPolizaCheque.MoveFirst()

        Do While Not Me.tmaPolizaCheque.EOF
            Select Case tmaPolizaCheque.CurrentAction
                Case Actions.Insert
                    Response = Me.oPolizasCheques.Insertar(Me.tmaPolizaCheque.SelectedRow, Me.Folio_Poliza_Cheque, 2)
                Case Actions.Update
                    Response = Me.oPolizasCheques.Actualizar(Me.tmaPolizaCheque.SelectedRow, Me.Folio_Poliza_Cheque)
                Case Actions.Delete
                    Response = Me.oPolizasCheques.Eliminar(Me.Folio_Poliza_Cheque, Me.tmaPolizaCheque.Item("partida"))
            End Select
            If Response.ErrorFound Then Exit Sub
            tmaPolizaCheque.MoveNext()
        Loop
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpCuentaBancaria_Format() Handles lkpCuentaBancaria.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpCuentaBancaria)
    End Sub
    Private Sub lkpCuentaBancaria_LoadData(ByVal Initialize As Boolean) Handles lkpCuentaBancaria.LoadData
        Dim response As Events
        response = oChequeras.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCuentaBancaria.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpCuentaBancaria_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCuentaBancaria.EditValueChanged
        Me.clcBanco.Text = Me.lkpCuentaBancaria.GetValue("banco")
        Me.clcBanco.Properties.Enabled = False
        Me.txtNombreBanco.Enabled = False
        Me.txtNombreBanco.EditValue = Me.clcBanco.EditValue
        Me.txtNombreBanco.Text = Me.txtNombreBanco.EditValue
        Me.txtNombreBanco.EditValue = Me.lkpCuentaBancaria.GetValue("nombre_banco")
        Me.clcFolio_Cheque.Enabled = False
    End Sub
    Private Sub cboTipoPago_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoPago.EditValueChanged

        Me.clcFolio_Cheque.Enabled = False
        Me.clcReferencia_Transferencia.Enabled = False

        Me.lkpCuentaBancaria.Enabled = False
        Me.lkpConceptoMovimiento.Enabled = False
        Me.txtNombreBanco.Enabled = False
        Me.dteFecha_Pago.Enabled = False
        Me.dteFecha_Deposito.Enabled = False

        '   Tipo de Pago: Cheque
        If Me.cboTipoPago.EditValue = "C" Then
            Me.clcReferencia_Transferencia.Enabled = False
            Me.clcReferencia_Transferencia.Value = 0
            '@ACH-27/06/07: Se deshabilit� el folio del cheque para que sea autom�tico
            Me.clcFolio_Cheque.Enabled = False
            '/@ACH-27/06/07
            '@ACH-27/06/07: Se agreg� un lookup nuevo asignado al campo Concepto Movimiento de Chequera
            Me.lkpConceptoMovimiento.Enabled = True
            '/@ACH-27/06/07
            Me.lkpCuentaBancaria.Enabled = True
            Me.txtNombreBanco.Enabled = True
            Me.dteFecha_Pago.Enabled = True
            Me.dteFecha_Deposito.Enabled = True
            If Action = Actions.Update Then
                Me.btnImprimeCheque.Enabled = True
            End If
        Else
            '   Tipo de Pago: Transferencia
            If Me.cboTipoPago.EditValue = "T" Then
                Me.clcFolio_Cheque.Enabled = False
                Me.clcFolio_Cheque.Value = 0
                Me.clcReferencia_Transferencia.Enabled = True
                Me.lkpCuentaBancaria.Enabled = True
                Me.txtNombreBanco.Enabled = True
                Me.dteFecha_Pago.Enabled = True
                Me.dteFecha_Deposito.Enabled = True
                Me.btnImprimeCheque.Enabled = False

            Else
                '   Tipo de Pago: Pendiente
                Me.clcReferencia_Transferencia.Enabled = False
                Me.clcReferencia_Transferencia.Value = 0
                Me.clcFolio_Cheque.Enabled = False
                Me.clcFolio_Cheque.Value = 0
                Me.lkpCuentaBancaria.Enabled = False
                Me.lkpCuentaBancaria.EditValue = ""
                Me.txtNombreBanco.Enabled = False
                Me.txtNombreBanco.Text = ""
                Me.dteFecha_Pago.Enabled = False
                Me.dteFecha_Deposito.Enabled = False
                Me.btnImprimeCheque.Enabled = False
            End If
        End If


    End Sub
    Public Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

        If e.Button Is Me.btnImprimeCheque And Action = Actions.Update Then
            Dim response As Events


            'Verificar que el tipo sea Cheque para generarlo
            If Me.cboTipoPago.EditValue <> "C" Then
                response = New Events
                response.Message = "No se Puede Imprimir Cheque, el Tipo de Pago debe ser Cheque"
                response.ShowMessage()
                Exit Sub
            End If


            'Verificar Poliza Cuadrada y Completa para la Generacion del Cheque
            If Not (Me.PolizaCuadrada And Me.PolizaCompleta(clcImporte.Value)) Then
                response = New Events
                response.Message = "La Poliza No esta Cuadrada o No esta Completa"
                response.ShowMessage()
                Exit Sub
            End If

            response = oAcreedoresDiversos.Validacion(Action, clcNumero_Documento.Value, clcImporte.Value, Me.dteFecha_Documento.Text, Me.cboTipoPago.Value, CuentaBancaria, Me.clcReferencia_Transferencia.Value, Me.txtBeneficiario.EditValue, Me.txtRFC.Text, Me.clcSubtotal.EditValue, Me.clcIva.EditValue, Me.lkpConceptoMovimiento.EditValue)

            If response.ErrorFound Then
                response.ShowMessage()
                Exit Sub
            End If
            'Abro la Transaccion para obtener el folio del cheque
            frmAcreedoresDiversos_BeginUpdate()

            If Me.clcFolio_Cheque.Value = 0 And Me.cboTipoPago.Value = "C" Then
                'asigno el valor al control para que lo actualice
                Me.clcFolio_Cheque.Value = Me.FolioChequera(Me.CuentaBancaria)
                ActualizarCheque = False
            Else
                ActualizarCheque = True
            End If

            'Actualizo los Valores
            frmAcreedoresDiversos_Accept(response)
            frmAcreedoresDiversos_Detail(response)

            'If Not response.ErrorFound Then
            ''@ACH-27/06/07: Se inserta el movimiento en la tabla de Movimientos de Chequera
            'Dim lngFolio As Long
            'Dim dblSaldoActual As Double
            'oMovimientosChequera.Insertar(Me.clcBanco.EditValue, Me.lkpCuentaBancaria.EditValue, lngFolio, Me.dteFecha_Pago.EditValue, Me.clcNumero_Documento.EditValue, Me.clcImporte.EditValue, "C", True, Me.txtBeneficiario.EditValue, Me.clcFolio_Cheque.EditValue, Me.chkAbono_Cuenta.EditValue, TINApp.Connection.User, TINApp.Connection.User, Me.txtConcepto.EditValue, dblSaldoActual, False, 0, Me.lkpConceptoMovimiento.EditValue)
            ''@ACH-27/06/07
            If Not response.ErrorFound Then
                'Termino Satisfactoriamente la transaccion
                frmAcreedoresDiversos_EndUpdate()
                'Imprimo el cheque
                ImprimirCheque(Me.CuentaBancaria, Me.clcFolio_Cheque.Value)
                Me.Close()
                'End If
            Else
                'Aborto o Cancelo la Transaccion
                frmAcreedoresDiversos_AbortUpdate()
                Me.clcFolio_Cheque.EditValue = 0
                response.ShowMessage()
            End If
        End If

    End Sub
    Private Sub clcSubtotal_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcSubtotal.EditValueChanged
        If IsNumeric(Me.clcSubtotal.EditValue) = True Then
            Me.clcImporte.EditValue = Me.clcSubtotal.EditValue + Me.clcIva.EditValue
        End If
    End Sub
    Private Sub clcIva_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcIva.EditValueChanged
        If IsNumeric(Me.clcIva.EditValue) = True Then
            Me.clcImporte.EditValue = Me.clcSubtotal.EditValue + Me.clcIva.EditValue
        End If
    End Sub


    Private Sub lkpAcreedor_Format() Handles lkpAcreedor.Format
        Comunes.clsFormato.for_acreedores_grl(Me.lkpAcreedor)
    End Sub
    Private Sub lkpAcreedor_LoadData(ByVal Initialize As Boolean) Handles lkpAcreedor.LoadData
        Dim response As Events
        response = Me.oAcreedores.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpAcreedor.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpAcreedor_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpAcreedor.EditValueChanged
        If Me.Acreedor > 0 Then
            Me.txtBeneficiario.Text = Me.lkpAcreedor.GetValue("nombre")
            Me.txtRFC.Text = Me.lkpAcreedor.GetValue("rfc")
        End If
    End Sub

    Private Sub nvrDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosGenerales.LinkPressed
        Me.Panel1.BringToFront()
    End Sub
    Private Sub nvrPolizaCheque_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrPolizaCheque.LinkPressed
        Me.Panel2.BringToFront()
    End Sub



#Region "Lookup Concepto Movimiento"
    Private Sub lkpConceptoMovimiento_InitData(ByVal Value As Object) Handles lkpConceptoMovimiento.InitData
        Dim response As Events

        response = oConceptosMovimientosChequera.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoMovimiento.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub


    Private Sub lkpConceptoMovimiento_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoMovimiento.LoadData
        Dim response As Events

        response = oConceptosMovimientosChequera.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoMovimiento.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpConceptoMovimiento_Format() Handles lkpConceptoMovimiento.Format
        for_conceptos_movimientos_chequera_grl(Me.lkpConceptoMovimiento)
    End Sub
#End Region

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub ImprimirCheque(ByVal cuentabancaria As String, ByVal folio As Integer)
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            response = oReportes.ImprimeChequeAcreedoresDiversos(Me.clcBanco.EditValue, cuentabancaria, folio)
            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Cheque no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptCheque

                    oDataSet = response.Value

                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Cheques", oReport)
                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
    Private Function ObtenerFolioPolizaCheque(ByRef response As Dipros.Utils.Events) As Long

        If Me.tmaPolizaCheque.View.RowCount > 0 And Folio_Poliza_Cheque = 0 Then
            response = Me.oPolizasCheques.ObtenerSiguienteFolio
            If Not response.ErrorFound Then
                Me.Folio_Poliza_Cheque = response.Value + 1
            End If
        End If
    End Function
    Public Function PolizaCuadrada() As Boolean
        Dim abono As Double = 0
        Dim cargo As Double = 0

        abono = CType(Me.grcAbono.SummaryItem.SummaryValue, Double)
        cargo = CType(Me.grcCargo.SummaryItem.SummaryValue, Double)

        If cargo = 0 And abono = 0 Then
            Me.tmaPolizaCheque.Title = "Sin Registros"
            Return False
        Else
            If cargo <> abono Then

                Me.tmaPolizaCheque.Title = "Descuadrada"
                Return False
            Else

                Me.tmaPolizaCheque.Title = "Cuadrada"
                Return True
            End If
        End If
    End Function
    Private Function PolizaCompleta(ByVal Total As Double) As Boolean
        Dim abono As Double = 0
        Dim cargo As Double = 0

        abono = CType(Me.grcAbono.SummaryItem.SummaryValue, Double)
        cargo = CType(Me.grcCargo.SummaryItem.SummaryValue, Double)

        If abono > 0 And cargo > 0 And (abono = cargo) And abono = Total Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region



End Class
