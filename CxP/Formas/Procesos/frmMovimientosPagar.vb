Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmMovimientosPagar
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys

    Private ActualizarCheque As Boolean
    Private FolioPagoProveedores As Long = 0

#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents lblFolio As System.Windows.Forms.Label
		Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblProveedor As System.Windows.Forms.Label
		Friend WithEvents clcProveedor As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblEntrada As System.Windows.Forms.Label
		Friend WithEvents clcEntrada As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblFecha As System.Windows.Forms.Label
		Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
		Friend WithEvents lblFecha_Vto As System.Windows.Forms.Label
		Friend WithEvents dteFecha_Vto As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblPlazo As System.Windows.Forms.Label
		Friend WithEvents clcPlazo As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblConcepto_Referencia As System.Windows.Forms.Label
    Friend WithEvents lblFolio_Referencia As System.Windows.Forms.Label

    Friend WithEvents lblSubtotal As System.Windows.Forms.Label
		Friend WithEvents clcSubtotal As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblIva As System.Windows.Forms.Label
		Friend WithEvents clcIva As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblTotal As System.Windows.Forms.Label
		Friend WithEvents clcTotal As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblSaldo As System.Windows.Forms.Label
		Friend WithEvents clcSaldo As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblBanco As System.Windows.Forms.Label

    Friend WithEvents lblReferencia_Transferencia As System.Windows.Forms.Label
		Friend WithEvents txtReferencia_Transferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCheque As System.Windows.Forms.Label
		Friend WithEvents clcCheque As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblCuenta_Bancaria As System.Windows.Forms.Label
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
		Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
		Friend WithEvents lblFormulo As System.Windows.Forms.Label
		Friend WithEvents txtFormulo As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblAutorizo As System.Windows.Forms.Label
		Friend WithEvents txtAutorizo As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblStatus As System.Windows.Forms.Label
		Friend WithEvents txtStatus As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblFolio_Factura As System.Windows.Forms.Label
		Friend WithEvents txtFolio_Factura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpChequera As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblNombreProveedor As System.Windows.Forms.Label
    Friend WithEvents txtNombreBanco As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpConcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpConceptoReferencia As Dipros.Editors.TINMultiLookup
    Friend WithEvents cboTipoPago As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents gpTipoPago As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblDrescripcionConcepto As System.Windows.Forms.Label
    Friend WithEvents lblDrescripcionConceptoRef As System.Windows.Forms.Label
    Friend WithEvents lkpFolioReferencia As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcSaldoTotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSaldoTotal As System.Windows.Forms.Label
    Friend WithEvents chkEsPadre As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents gpb1 As System.Windows.Forms.GroupBox
    Friend WithEvents gpb2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkAbono_Cuenta As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btnImprimeCheque As System.Windows.Forms.ToolBarButton
    Friend WithEvents clcBanco As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPagoVariasFacturas As System.Windows.Forms.Label
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nvrDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents grDescuentosProveedores As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDescuentosProveedor As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClaveDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPorcentaje As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAntesIVA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcProntoPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents nvrDescuentos As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrPolizaCheque As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents grDatosPoliza As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDatosPoliza As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcCuentaContable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepLkpCuentasContables As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCargo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConsecutivo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepClcImporte As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepTxtConcepto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents tmaPolizaCheque As Dipros.Windows.TINMaster

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosPagar))
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.clcProveedor = New Dipros.Editors.TINCalcEdit
        Me.lblEntrada = New System.Windows.Forms.Label
        Me.clcEntrada = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblFecha_Vto = New System.Windows.Forms.Label
        Me.dteFecha_Vto = New DevExpress.XtraEditors.DateEdit
        Me.lblPlazo = New System.Windows.Forms.Label
        Me.clcPlazo = New Dipros.Editors.TINCalcEdit
        Me.lblConcepto_Referencia = New System.Windows.Forms.Label
        Me.lblFolio_Referencia = New System.Windows.Forms.Label
        Me.lblSubtotal = New System.Windows.Forms.Label
        Me.clcSubtotal = New Dipros.Editors.TINCalcEdit
        Me.lblIva = New System.Windows.Forms.Label
        Me.clcIva = New Dipros.Editors.TINCalcEdit
        Me.lblTotal = New System.Windows.Forms.Label
        Me.clcTotal = New Dipros.Editors.TINCalcEdit
        Me.lblSaldo = New System.Windows.Forms.Label
        Me.clcSaldo = New Dipros.Editors.TINCalcEdit
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lblReferencia_Transferencia = New System.Windows.Forms.Label
        Me.txtReferencia_Transferencia = New DevExpress.XtraEditors.TextEdit
        Me.lblCheque = New System.Windows.Forms.Label
        Me.clcCheque = New Dipros.Editors.TINCalcEdit
        Me.lblCuenta_Bancaria = New System.Windows.Forms.Label
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblFormulo = New System.Windows.Forms.Label
        Me.txtFormulo = New DevExpress.XtraEditors.TextEdit
        Me.lblAutorizo = New System.Windows.Forms.Label
        Me.txtAutorizo = New DevExpress.XtraEditors.TextEdit
        Me.lblStatus = New System.Windows.Forms.Label
        Me.txtStatus = New DevExpress.XtraEditors.TextEdit
        Me.lblFolio_Factura = New System.Windows.Forms.Label
        Me.txtFolio_Factura = New DevExpress.XtraEditors.TextEdit
        Me.gpb1 = New System.Windows.Forms.GroupBox
        Me.clcSaldoTotal = New Dipros.Editors.TINCalcEdit
        Me.lblDrescripcionConcepto = New System.Windows.Forms.Label
        Me.lkpConcepto = New Dipros.Editors.TINMultiLookup
        Me.lkpConceptoReferencia = New Dipros.Editors.TINMultiLookup
        Me.lblDrescripcionConceptoRef = New System.Windows.Forms.Label
        Me.lkpFolioReferencia = New Dipros.Editors.TINMultiLookup
        Me.lblSaldoTotal = New System.Windows.Forms.Label
        Me.gpb2 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.gpTipoPago = New System.Windows.Forms.GroupBox
        Me.clcBanco = New Dipros.Editors.TINCalcEdit
        Me.txtNombreBanco = New DevExpress.XtraEditors.TextEdit
        Me.lkpChequera = New Dipros.Editors.TINMultiLookup
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.cboTipoPago = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblNombreProveedor = New System.Windows.Forms.Label
        Me.chkEsPadre = New DevExpress.XtraEditors.CheckEdit
        Me.chkAbono_Cuenta = New DevExpress.XtraEditors.CheckEdit
        Me.btnImprimeCheque = New System.Windows.Forms.ToolBarButton
        Me.lblPagoVariasFacturas = New System.Windows.Forms.Label
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.nvrDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDescuentos = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrPolizaCheque = New DevExpress.XtraNavBar.NavBarItem
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.grDescuentosProveedores = New DevExpress.XtraGrid.GridControl
        Me.grvDescuentosProveedor = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPorcentaje = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAntesIVA = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcProntoPago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.tmaPolizaCheque = New Dipros.Windows.TINMaster
        Me.grDatosPoliza = New DevExpress.XtraGrid.GridControl
        Me.grvDatosPoliza = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcConsecutivo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCuentaContable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepTxtConcepto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.grcCargo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepClcImporte = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcAbono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepLkpCuentasContables = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcEntrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Vto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSubtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReferencia_Transferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFormulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutorizo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFolio_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpb1.SuspendLayout()
        CType(Me.clcSaldoTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpb2.SuspendLayout()
        Me.gpTipoPago.SuspendLayout()
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.cboTipoPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEsPadre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbono_Cuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.grDescuentosProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDescuentosProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.grDatosPoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDatosPoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepTxtConcepto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepClcImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepLkpCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnImprimeCheque})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5731, 28)
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(70, 16)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 0
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "&Concepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(448, 16)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 7
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(488, 15)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(78, 21)
        Me.clcFolio.TabIndex = 8
        Me.clcFolio.Tag = "folio"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(16, 16)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 0
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "&Proveedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcProveedor
        '
        Me.clcProveedor.EditValue = "0"
        Me.clcProveedor.Location = New System.Drawing.Point(504, 16)
        Me.clcProveedor.MaxValue = 0
        Me.clcProveedor.MinValue = 0
        Me.clcProveedor.Name = "clcProveedor"
        '
        'clcProveedor.Properties
        '
        Me.clcProveedor.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcProveedor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcProveedor.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcProveedor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcProveedor.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcProveedor.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcProveedor.Size = New System.Drawing.Size(40, 21)
        Me.clcProveedor.TabIndex = 10
        Me.clcProveedor.Tag = "proveedor"
        Me.clcProveedor.Visible = False
        '
        'lblEntrada
        '
        Me.lblEntrada.AutoSize = True
        Me.lblEntrada.Location = New System.Drawing.Point(78, 88)
        Me.lblEntrada.Name = "lblEntrada"
        Me.lblEntrada.Size = New System.Drawing.Size(52, 16)
        Me.lblEntrada.TabIndex = 6
        Me.lblEntrada.Tag = ""
        Me.lblEntrada.Text = "&Entrada:"
        Me.lblEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEntrada
        '
        Me.clcEntrada.EditValue = "0"
        Me.clcEntrada.Location = New System.Drawing.Point(136, 87)
        Me.clcEntrada.MaxValue = 0
        Me.clcEntrada.MinValue = 0
        Me.clcEntrada.Name = "clcEntrada"
        '
        'clcEntrada.Properties
        '
        Me.clcEntrada.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcEntrada.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEntrada.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcEntrada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEntrada.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcEntrada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEntrada.Size = New System.Drawing.Size(64, 21)
        Me.clcEntrada.TabIndex = 7
        Me.clcEntrada.Tag = "entrada"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(50, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "20/07/2006"
        Me.dteFecha.Location = New System.Drawing.Point(96, 14)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 22)
        Me.dteFecha.TabIndex = 1
        Me.dteFecha.Tag = "fecha"
        '
        'lblFecha_Vto
        '
        Me.lblFecha_Vto.AutoSize = True
        Me.lblFecha_Vto.Location = New System.Drawing.Point(352, 16)
        Me.lblFecha_Vto.Name = "lblFecha_Vto"
        Me.lblFecha_Vto.Size = New System.Drawing.Size(113, 16)
        Me.lblFecha_Vto.TabIndex = 8
        Me.lblFecha_Vto.Tag = ""
        Me.lblFecha_Vto.Text = "&Fecha Vencimiento:"
        Me.lblFecha_Vto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Vto
        '
        Me.dteFecha_Vto.EditValue = "20/07/2006"
        Me.dteFecha_Vto.Location = New System.Drawing.Point(472, 14)
        Me.dteFecha_Vto.Name = "dteFecha_Vto"
        '
        'dteFecha_Vto.Properties
        '
        Me.dteFecha_Vto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Vto.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Vto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Vto.Properties.Enabled = False
        Me.dteFecha_Vto.Size = New System.Drawing.Size(95, 22)
        Me.dteFecha_Vto.TabIndex = 9
        Me.dteFecha_Vto.Tag = "fecha_vencimiento"
        '
        'lblPlazo
        '
        Me.lblPlazo.AutoSize = True
        Me.lblPlazo.Location = New System.Drawing.Point(424, 40)
        Me.lblPlazo.Name = "lblPlazo"
        Me.lblPlazo.Size = New System.Drawing.Size(38, 16)
        Me.lblPlazo.TabIndex = 10
        Me.lblPlazo.Tag = ""
        Me.lblPlazo.Text = "Pla&zo:"
        Me.lblPlazo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo
        '
        Me.clcPlazo.EditValue = "0"
        Me.clcPlazo.Location = New System.Drawing.Point(472, 39)
        Me.clcPlazo.MaxValue = 0
        Me.clcPlazo.MinValue = 0
        Me.clcPlazo.Name = "clcPlazo"
        '
        'clcPlazo.Properties
        '
        Me.clcPlazo.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPlazo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPlazo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo.Properties.Enabled = False
        Me.clcPlazo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPlazo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo.Size = New System.Drawing.Size(32, 21)
        Me.clcPlazo.TabIndex = 11
        Me.clcPlazo.Tag = "plazo"
        '
        'lblConcepto_Referencia
        '
        Me.lblConcepto_Referencia.AutoSize = True
        Me.lblConcepto_Referencia.Location = New System.Drawing.Point(8, 40)
        Me.lblConcepto_Referencia.Name = "lblConcepto_Referencia"
        Me.lblConcepto_Referencia.Size = New System.Drawing.Size(122, 16)
        Me.lblConcepto_Referencia.TabIndex = 2
        Me.lblConcepto_Referencia.Tag = ""
        Me.lblConcepto_Referencia.Text = "&Concepto Referencia:"
        Me.lblConcepto_Referencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio_Referencia
        '
        Me.lblFolio_Referencia.AutoSize = True
        Me.lblFolio_Referencia.Location = New System.Drawing.Point(33, 64)
        Me.lblFolio_Referencia.Name = "lblFolio_Referencia"
        Me.lblFolio_Referencia.Size = New System.Drawing.Size(97, 16)
        Me.lblFolio_Referencia.TabIndex = 4
        Me.lblFolio_Referencia.Tag = ""
        Me.lblFolio_Referencia.Text = "&Folio Referencia:"
        Me.lblFolio_Referencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSubtotal
        '
        Me.lblSubtotal.AutoSize = True
        Me.lblSubtotal.Location = New System.Drawing.Point(36, 40)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(55, 16)
        Me.lblSubtotal.TabIndex = 2
        Me.lblSubtotal.Tag = ""
        Me.lblSubtotal.Text = "&Subtotal:"
        Me.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSubtotal
        '
        Me.clcSubtotal.EditValue = "0"
        Me.clcSubtotal.Location = New System.Drawing.Point(96, 39)
        Me.clcSubtotal.MaxValue = 0
        Me.clcSubtotal.MinValue = 0
        Me.clcSubtotal.Name = "clcSubtotal"
        '
        'clcSubtotal.Properties
        '
        Me.clcSubtotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSubtotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSubtotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSubtotal.Properties.Precision = 2
        Me.clcSubtotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSubtotal.Size = New System.Drawing.Size(95, 21)
        Me.clcSubtotal.TabIndex = 3
        Me.clcSubtotal.Tag = "subtotal"
        '
        'lblIva
        '
        Me.lblIva.AutoSize = True
        Me.lblIva.Location = New System.Drawing.Point(64, 64)
        Me.lblIva.Name = "lblIva"
        Me.lblIva.Size = New System.Drawing.Size(27, 16)
        Me.lblIva.TabIndex = 4
        Me.lblIva.Tag = ""
        Me.lblIva.Text = "&Iva:"
        Me.lblIva.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIva
        '
        Me.clcIva.EditValue = "0"
        Me.clcIva.Location = New System.Drawing.Point(96, 63)
        Me.clcIva.MaxValue = 0
        Me.clcIva.MinValue = 0
        Me.clcIva.Name = "clcIva"
        '
        'clcIva.Properties
        '
        Me.clcIva.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcIva.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIva.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcIva.Properties.Precision = 2
        Me.clcIva.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIva.Size = New System.Drawing.Size(95, 21)
        Me.clcIva.TabIndex = 5
        Me.clcIva.Tag = "iva"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(55, 88)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(36, 16)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Tag = ""
        Me.lblTotal.Text = "&Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotal
        '
        Me.clcTotal.EditValue = "0"
        Me.clcTotal.Location = New System.Drawing.Point(96, 87)
        Me.clcTotal.MaxValue = 0
        Me.clcTotal.MinValue = 0
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.Enabled = False
        Me.clcTotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotal.Properties.Precision = 2
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Size = New System.Drawing.Size(95, 21)
        Me.clcTotal.TabIndex = 7
        Me.clcTotal.Tag = "total"
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(424, 88)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(40, 16)
        Me.lblSaldo.TabIndex = 13
        Me.lblSaldo.Tag = ""
        Me.lblSaldo.Text = "&Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = "0"
        Me.clcSaldo.Location = New System.Drawing.Point(472, 87)
        Me.clcSaldo.MaxValue = 0
        Me.clcSaldo.MinValue = 0
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSaldo.Properties.Precision = 2
        Me.clcSaldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldo.Size = New System.Drawing.Size(95, 21)
        Me.clcSaldo.TabIndex = 14
        Me.clcSaldo.Tag = "saldo"
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(48, 39)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 0
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblReferencia_Transferencia
        '
        Me.lblReferencia_Transferencia.AutoSize = True
        Me.lblReferencia_Transferencia.Location = New System.Drawing.Point(320, 19)
        Me.lblReferencia_Transferencia.Name = "lblReferencia_Transferencia"
        Me.lblReferencia_Transferencia.Size = New System.Drawing.Size(146, 16)
        Me.lblReferencia_Transferencia.TabIndex = 4
        Me.lblReferencia_Transferencia.Tag = ""
        Me.lblReferencia_Transferencia.Text = "&Referencia Transferencia:"
        Me.lblReferencia_Transferencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtReferencia_Transferencia
        '
        Me.txtReferencia_Transferencia.EditValue = ""
        Me.txtReferencia_Transferencia.Location = New System.Drawing.Point(472, 16)
        Me.txtReferencia_Transferencia.Name = "txtReferencia_Transferencia"
        '
        'txtReferencia_Transferencia.Properties
        '
        Me.txtReferencia_Transferencia.Properties.Enabled = False
        Me.txtReferencia_Transferencia.Properties.MaxLength = 50
        Me.txtReferencia_Transferencia.Size = New System.Drawing.Size(95, 22)
        Me.txtReferencia_Transferencia.TabIndex = 5
        Me.txtReferencia_Transferencia.Tag = "referencia_transferencia"
        '
        'lblCheque
        '
        Me.lblCheque.AutoSize = True
        Me.lblCheque.Location = New System.Drawing.Point(416, 41)
        Me.lblCheque.Name = "lblCheque"
        Me.lblCheque.Size = New System.Drawing.Size(51, 16)
        Me.lblCheque.TabIndex = 6
        Me.lblCheque.Tag = ""
        Me.lblCheque.Text = "&Cheque:"
        Me.lblCheque.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCheque
        '
        Me.clcCheque.EditValue = "0"
        Me.clcCheque.Location = New System.Drawing.Point(472, 39)
        Me.clcCheque.MaxValue = 0
        Me.clcCheque.MinValue = 0
        Me.clcCheque.Name = "clcCheque"
        '
        'clcCheque.Properties
        '
        Me.clcCheque.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCheque.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCheque.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCheque.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCheque.Properties.Enabled = False
        Me.clcCheque.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCheque.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCheque.Size = New System.Drawing.Size(95, 21)
        Me.clcCheque.TabIndex = 7
        Me.clcCheque.Tag = "cheque"
        '
        'lblCuenta_Bancaria
        '
        Me.lblCuenta_Bancaria.AutoSize = True
        Me.lblCuenta_Bancaria.Location = New System.Drawing.Point(29, 16)
        Me.lblCuenta_Bancaria.Name = "lblCuenta_Bancaria"
        Me.lblCuenta_Bancaria.Size = New System.Drawing.Size(62, 16)
        Me.lblCuenta_Bancaria.TabIndex = 2
        Me.lblCuenta_Bancaria.Tag = ""
        Me.lblCuenta_Bancaria.Text = "Chequera:"
        Me.lblCuenta_Bancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(32, 16)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 0
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(128, 16)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(440, 38)
        Me.txtObservaciones.TabIndex = 1
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblFormulo
        '
        Me.lblFormulo.AutoSize = True
        Me.lblFormulo.Location = New System.Drawing.Point(64, 56)
        Me.lblFormulo.Name = "lblFormulo"
        Me.lblFormulo.Size = New System.Drawing.Size(54, 16)
        Me.lblFormulo.TabIndex = 2
        Me.lblFormulo.Tag = ""
        Me.lblFormulo.Text = "&Formuló:"
        Me.lblFormulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFormulo
        '
        Me.txtFormulo.EditValue = ""
        Me.txtFormulo.Location = New System.Drawing.Point(128, 56)
        Me.txtFormulo.Name = "txtFormulo"
        '
        'txtFormulo.Properties
        '
        Me.txtFormulo.Properties.Enabled = False
        Me.txtFormulo.Properties.MaxLength = 30
        Me.txtFormulo.Size = New System.Drawing.Size(180, 22)
        Me.txtFormulo.TabIndex = 3
        Me.txtFormulo.Tag = "formulo"
        '
        'lblAutorizo
        '
        Me.lblAutorizo.AutoSize = True
        Me.lblAutorizo.Location = New System.Drawing.Point(64, 80)
        Me.lblAutorizo.Name = "lblAutorizo"
        Me.lblAutorizo.Size = New System.Drawing.Size(55, 16)
        Me.lblAutorizo.TabIndex = 4
        Me.lblAutorizo.Tag = ""
        Me.lblAutorizo.Text = "&Autorizó:"
        Me.lblAutorizo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAutorizo
        '
        Me.txtAutorizo.EditValue = ""
        Me.txtAutorizo.Location = New System.Drawing.Point(128, 80)
        Me.txtAutorizo.Name = "txtAutorizo"
        '
        'txtAutorizo.Properties
        '
        Me.txtAutorizo.Properties.MaxLength = 30
        Me.txtAutorizo.Size = New System.Drawing.Size(180, 22)
        Me.txtAutorizo.TabIndex = 5
        Me.txtAutorizo.Tag = "autorizo"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(504, 288)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 8
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "&Status:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblStatus.Visible = False
        '
        'txtStatus
        '
        Me.txtStatus.EditValue = ""
        Me.txtStatus.Location = New System.Drawing.Point(552, 288)
        Me.txtStatus.Name = "txtStatus"
        '
        'txtStatus.Properties
        '
        Me.txtStatus.Properties.MaxLength = 2
        Me.txtStatus.Size = New System.Drawing.Size(30, 22)
        Me.txtStatus.TabIndex = 9
        Me.txtStatus.Tag = "status"
        Me.txtStatus.Visible = False
        '
        'lblFolio_Factura
        '
        Me.lblFolio_Factura.AutoSize = True
        Me.lblFolio_Factura.Location = New System.Drawing.Point(400, 88)
        Me.lblFolio_Factura.Name = "lblFolio_Factura"
        Me.lblFolio_Factura.Size = New System.Drawing.Size(80, 16)
        Me.lblFolio_Factura.TabIndex = 9
        Me.lblFolio_Factura.Tag = ""
        Me.lblFolio_Factura.Text = "&Folio Factura:"
        Me.lblFolio_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFolio_Factura
        '
        Me.txtFolio_Factura.EditValue = ""
        Me.txtFolio_Factura.Location = New System.Drawing.Point(488, 86)
        Me.txtFolio_Factura.Name = "txtFolio_Factura"
        '
        'txtFolio_Factura.Properties
        '
        Me.txtFolio_Factura.Properties.MaxLength = 12
        Me.txtFolio_Factura.Size = New System.Drawing.Size(78, 22)
        Me.txtFolio_Factura.TabIndex = 10
        Me.txtFolio_Factura.Tag = "folio_factura"
        '
        'gpb1
        '
        Me.gpb1.Controls.Add(Me.clcSaldoTotal)
        Me.gpb1.Controls.Add(Me.lblDrescripcionConcepto)
        Me.gpb1.Controls.Add(Me.lkpConcepto)
        Me.gpb1.Controls.Add(Me.lblConcepto)
        Me.gpb1.Controls.Add(Me.lblFolio)
        Me.gpb1.Controls.Add(Me.lblFolio_Referencia)
        Me.gpb1.Controls.Add(Me.lblConcepto_Referencia)
        Me.gpb1.Controls.Add(Me.clcFolio)
        Me.gpb1.Controls.Add(Me.txtFolio_Factura)
        Me.gpb1.Controls.Add(Me.lblFolio_Factura)
        Me.gpb1.Controls.Add(Me.lkpConceptoReferencia)
        Me.gpb1.Controls.Add(Me.lblEntrada)
        Me.gpb1.Controls.Add(Me.clcEntrada)
        Me.gpb1.Controls.Add(Me.lblDrescripcionConceptoRef)
        Me.gpb1.Controls.Add(Me.lkpFolioReferencia)
        Me.gpb1.Controls.Add(Me.lblSaldoTotal)
        Me.gpb1.Location = New System.Drawing.Point(8, 40)
        Me.gpb1.Name = "gpb1"
        Me.gpb1.Size = New System.Drawing.Size(576, 120)
        Me.gpb1.TabIndex = 2
        Me.gpb1.TabStop = False
        '
        'clcSaldoTotal
        '
        Me.clcSaldoTotal.EditValue = "0"
        Me.clcSaldoTotal.Location = New System.Drawing.Point(488, 63)
        Me.clcSaldoTotal.MaxValue = 0
        Me.clcSaldoTotal.MinValue = 0
        Me.clcSaldoTotal.Name = "clcSaldoTotal"
        '
        'clcSaldoTotal.Properties
        '
        Me.clcSaldoTotal.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSaldoTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoTotal.Properties.EditFormat.FormatString = "c2"
        Me.clcSaldoTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoTotal.Properties.Enabled = False
        Me.clcSaldoTotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSaldoTotal.Properties.Precision = 2
        Me.clcSaldoTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldoTotal.Size = New System.Drawing.Size(78, 21)
        Me.clcSaldoTotal.TabIndex = 12
        Me.clcSaldoTotal.Tag = ""
        Me.clcSaldoTotal.Visible = False
        '
        'lblDrescripcionConcepto
        '
        Me.lblDrescripcionConcepto.Location = New System.Drawing.Point(208, 14)
        Me.lblDrescripcionConcepto.Name = "lblDrescripcionConcepto"
        Me.lblDrescripcionConcepto.Size = New System.Drawing.Size(240, 22)
        Me.lblDrescripcionConcepto.TabIndex = 11
        '
        'lkpConcepto
        '
        Me.lkpConcepto.AllowAdd = False
        Me.lkpConcepto.AutoReaload = False
        Me.lkpConcepto.DataSource = Nothing
        Me.lkpConcepto.DefaultSearchField = ""
        Me.lkpConcepto.DisplayMember = "concepto"
        Me.lkpConcepto.EditValue = Nothing
        Me.lkpConcepto.Filtered = False
        Me.lkpConcepto.InitValue = Nothing
        Me.lkpConcepto.Location = New System.Drawing.Point(136, 14)
        Me.lkpConcepto.MultiSelect = False
        Me.lkpConcepto.Name = "lkpConcepto"
        Me.lkpConcepto.NullText = ""
        Me.lkpConcepto.PopupWidth = CType(400, Long)
        Me.lkpConcepto.Required = False
        Me.lkpConcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConcepto.SearchMember = ""
        Me.lkpConcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConcepto.SelectAll = False
        Me.lkpConcepto.Size = New System.Drawing.Size(64, 22)
        Me.lkpConcepto.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConcepto.TabIndex = 1
        Me.lkpConcepto.Tag = "concepto"
        Me.lkpConcepto.ToolTip = Nothing
        Me.lkpConcepto.ValueMember = "concepto"
        '
        'lkpConceptoReferencia
        '
        Me.lkpConceptoReferencia.AllowAdd = False
        Me.lkpConceptoReferencia.AutoReaload = False
        Me.lkpConceptoReferencia.DataSource = Nothing
        Me.lkpConceptoReferencia.DefaultSearchField = ""
        Me.lkpConceptoReferencia.DisplayMember = "concepto"
        Me.lkpConceptoReferencia.EditValue = Nothing
        Me.lkpConceptoReferencia.Filtered = False
        Me.lkpConceptoReferencia.InitValue = Nothing
        Me.lkpConceptoReferencia.Location = New System.Drawing.Point(136, 38)
        Me.lkpConceptoReferencia.MultiSelect = False
        Me.lkpConceptoReferencia.Name = "lkpConceptoReferencia"
        Me.lkpConceptoReferencia.NullText = ""
        Me.lkpConceptoReferencia.PopupWidth = CType(400, Long)
        Me.lkpConceptoReferencia.Required = False
        Me.lkpConceptoReferencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoReferencia.SearchMember = ""
        Me.lkpConceptoReferencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoReferencia.SelectAll = False
        Me.lkpConceptoReferencia.Size = New System.Drawing.Size(64, 22)
        Me.lkpConceptoReferencia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoReferencia.TabIndex = 3
        Me.lkpConceptoReferencia.Tag = "concepto_referencia"
        Me.lkpConceptoReferencia.ToolTip = Nothing
        Me.lkpConceptoReferencia.ValueMember = "concepto"
        '
        'lblDrescripcionConceptoRef
        '
        Me.lblDrescripcionConceptoRef.Location = New System.Drawing.Point(208, 38)
        Me.lblDrescripcionConceptoRef.Name = "lblDrescripcionConceptoRef"
        Me.lblDrescripcionConceptoRef.Size = New System.Drawing.Size(240, 22)
        Me.lblDrescripcionConceptoRef.TabIndex = 11
        '
        'lkpFolioReferencia
        '
        Me.lkpFolioReferencia.AllowAdd = False
        Me.lkpFolioReferencia.AutoReaload = False
        Me.lkpFolioReferencia.DataSource = Nothing
        Me.lkpFolioReferencia.DefaultSearchField = ""
        Me.lkpFolioReferencia.DisplayMember = "folio_referencia"
        Me.lkpFolioReferencia.EditValue = Nothing
        Me.lkpFolioReferencia.Filtered = False
        Me.lkpFolioReferencia.InitValue = Nothing
        Me.lkpFolioReferencia.Location = New System.Drawing.Point(136, 62)
        Me.lkpFolioReferencia.MultiSelect = False
        Me.lkpFolioReferencia.Name = "lkpFolioReferencia"
        Me.lkpFolioReferencia.NullText = ""
        Me.lkpFolioReferencia.PopupWidth = CType(400, Long)
        Me.lkpFolioReferencia.Required = False
        Me.lkpFolioReferencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolioReferencia.SearchMember = ""
        Me.lkpFolioReferencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolioReferencia.SelectAll = False
        Me.lkpFolioReferencia.Size = New System.Drawing.Size(64, 22)
        Me.lkpFolioReferencia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolioReferencia.TabIndex = 3
        Me.lkpFolioReferencia.Tag = "folio_referencia"
        Me.lkpFolioReferencia.ToolTip = Nothing
        Me.lkpFolioReferencia.ValueMember = "folio_referencia"
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldoTotal.Location = New System.Drawing.Point(400, 64)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(80, 16)
        Me.lblSaldoTotal.TabIndex = 11
        Me.lblSaldoTotal.Text = "Saldo Total:"
        Me.lblSaldoTotal.Visible = False
        '
        'gpb2
        '
        Me.gpb2.Controls.Add(Me.dteFecha)
        Me.gpb2.Controls.Add(Me.lblFecha)
        Me.gpb2.Controls.Add(Me.lblPlazo)
        Me.gpb2.Controls.Add(Me.clcPlazo)
        Me.gpb2.Controls.Add(Me.clcSubtotal)
        Me.gpb2.Controls.Add(Me.lblSubtotal)
        Me.gpb2.Controls.Add(Me.clcTotal)
        Me.gpb2.Controls.Add(Me.lblTotal)
        Me.gpb2.Controls.Add(Me.clcIva)
        Me.gpb2.Controls.Add(Me.lblIva)
        Me.gpb2.Controls.Add(Me.clcSaldo)
        Me.gpb2.Controls.Add(Me.lblSaldo)
        Me.gpb2.Controls.Add(Me.dteFecha_Vto)
        Me.gpb2.Controls.Add(Me.lblFecha_Vto)
        Me.gpb2.Controls.Add(Me.Label3)
        Me.gpb2.Location = New System.Drawing.Point(8, 160)
        Me.gpb2.Name = "gpb2"
        Me.gpb2.Size = New System.Drawing.Size(576, 120)
        Me.gpb2.TabIndex = 3
        Me.gpb2.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(512, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 16)
        Me.Label3.TabIndex = 12
        Me.Label3.Tag = ""
        Me.Label3.Text = "Días"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpTipoPago
        '
        Me.gpTipoPago.Controls.Add(Me.clcBanco)
        Me.gpTipoPago.Controls.Add(Me.txtNombreBanco)
        Me.gpTipoPago.Controls.Add(Me.lblBanco)
        Me.gpTipoPago.Controls.Add(Me.clcCheque)
        Me.gpTipoPago.Controls.Add(Me.lblCuenta_Bancaria)
        Me.gpTipoPago.Controls.Add(Me.lblCheque)
        Me.gpTipoPago.Controls.Add(Me.lblReferencia_Transferencia)
        Me.gpTipoPago.Controls.Add(Me.txtReferencia_Transferencia)
        Me.gpTipoPago.Controls.Add(Me.lkpChequera)
        Me.gpTipoPago.Location = New System.Drawing.Point(8, 312)
        Me.gpTipoPago.Name = "gpTipoPago"
        Me.gpTipoPago.Size = New System.Drawing.Size(576, 72)
        Me.gpTipoPago.TabIndex = 6
        Me.gpTipoPago.TabStop = False
        '
        'clcBanco
        '
        Me.clcBanco.EditValue = "0"
        Me.clcBanco.Location = New System.Drawing.Point(240, 39)
        Me.clcBanco.MaxValue = 0
        Me.clcBanco.MinValue = 0
        Me.clcBanco.Name = "clcBanco"
        '
        'clcBanco.Properties
        '
        Me.clcBanco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcBanco.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBanco.Size = New System.Drawing.Size(48, 21)
        Me.clcBanco.TabIndex = 20
        Me.clcBanco.Tag = "banco"
        Me.clcBanco.Visible = False
        '
        'txtNombreBanco
        '
        Me.txtNombreBanco.EditValue = ""
        Me.txtNombreBanco.Location = New System.Drawing.Point(96, 39)
        Me.txtNombreBanco.Name = "txtNombreBanco"
        '
        'txtNombreBanco.Properties
        '
        Me.txtNombreBanco.Properties.Enabled = False
        Me.txtNombreBanco.Properties.MaxLength = 30
        Me.txtNombreBanco.Size = New System.Drawing.Size(136, 22)
        Me.txtNombreBanco.TabIndex = 1
        Me.txtNombreBanco.Tag = ""
        '
        'lkpChequera
        '
        Me.lkpChequera.AllowAdd = False
        Me.lkpChequera.AutoReaload = False
        Me.lkpChequera.DataSource = Nothing
        Me.lkpChequera.DefaultSearchField = ""
        Me.lkpChequera.DisplayMember = "cuenta_bancaria"
        Me.lkpChequera.EditValue = Nothing
        Me.lkpChequera.Enabled = False
        Me.lkpChequera.Filtered = False
        Me.lkpChequera.InitValue = Nothing
        Me.lkpChequera.Location = New System.Drawing.Point(96, 16)
        Me.lkpChequera.MultiSelect = False
        Me.lkpChequera.Name = "lkpChequera"
        Me.lkpChequera.NullText = ""
        Me.lkpChequera.PopupWidth = CType(400, Long)
        Me.lkpChequera.Required = False
        Me.lkpChequera.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpChequera.SearchMember = ""
        Me.lkpChequera.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpChequera.SelectAll = False
        Me.lkpChequera.Size = New System.Drawing.Size(136, 22)
        Me.lkpChequera.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpChequera.TabIndex = 3
        Me.lkpChequera.Tag = "cuenta_bancaria"
        Me.lkpChequera.ToolTip = Nothing
        Me.lkpChequera.ValueMember = "cuenta_bancaria"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblAutorizo)
        Me.GroupBox4.Controls.Add(Me.txtAutorizo)
        Me.GroupBox4.Controls.Add(Me.lblObservaciones)
        Me.GroupBox4.Controls.Add(Me.txtObservaciones)
        Me.GroupBox4.Controls.Add(Me.lblFormulo)
        Me.GroupBox4.Controls.Add(Me.txtFormulo)
        Me.GroupBox4.Location = New System.Drawing.Point(128, 432)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(576, 112)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        '
        'cboTipoPago
        '
        Me.cboTipoPago.EditValue = "P"
        Me.cboTipoPago.Location = New System.Drawing.Point(120, 288)
        Me.cboTipoPago.Name = "cboTipoPago"
        '
        'cboTipoPago.Properties
        '
        Me.cboTipoPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoPago.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pendiente", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Transferencia", "T", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cheque", "C", -1)})
        Me.cboTipoPago.Size = New System.Drawing.Size(95, 22)
        Me.cboTipoPago.TabIndex = 5
        Me.cboTipoPago.Tag = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 288)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "Forma de Pago:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNombreProveedor
        '
        Me.lblNombreProveedor.Location = New System.Drawing.Point(88, 16)
        Me.lblNombreProveedor.Name = "lblNombreProveedor"
        Me.lblNombreProveedor.Size = New System.Drawing.Size(328, 16)
        Me.lblNombreProveedor.TabIndex = 1
        '
        'chkEsPadre
        '
        Me.chkEsPadre.Location = New System.Drawing.Point(392, 288)
        Me.chkEsPadre.Name = "chkEsPadre"
        '
        'chkEsPadre.Properties
        '
        Me.chkEsPadre.Properties.Caption = "Es &padre"
        Me.chkEsPadre.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkEsPadre.Size = New System.Drawing.Size(75, 19)
        Me.chkEsPadre.TabIndex = 59
        Me.chkEsPadre.Tag = "es_padre"
        Me.chkEsPadre.Visible = False
        '
        'chkAbono_Cuenta
        '
        Me.chkAbono_Cuenta.Location = New System.Drawing.Point(248, 288)
        Me.chkAbono_Cuenta.Name = "chkAbono_Cuenta"
        '
        'chkAbono_Cuenta.Properties
        '
        Me.chkAbono_Cuenta.Properties.Caption = "Abono A Cuenta"
        Me.chkAbono_Cuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAbono_Cuenta.Size = New System.Drawing.Size(110, 19)
        Me.chkAbono_Cuenta.TabIndex = 60
        Me.chkAbono_Cuenta.Tag = "abono_cuenta"
        Me.chkAbono_Cuenta.ToolTip = "Abono A Cuenta"
        '
        'btnImprimeCheque
        '
        Me.btnImprimeCheque.Text = "Imprime Cheque"
        '
        'lblPagoVariasFacturas
        '
        Me.lblPagoVariasFacturas.AutoSize = True
        Me.lblPagoVariasFacturas.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPagoVariasFacturas.Location = New System.Drawing.Point(424, 16)
        Me.lblPagoVariasFacturas.Name = "lblPagoVariasFacturas"
        Me.lblPagoVariasFacturas.Size = New System.Drawing.Size(163, 18)
        Me.lblPagoVariasFacturas.TabIndex = 63
        Me.lblPagoVariasFacturas.Text = "Pago a Varias Facturas"
        Me.lblPagoVariasFacturas.Visible = False
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvrDescuentos, Me.nvrDatosGenerales, Me.nvrPolizaCheque})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 28)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(120, 524)
        Me.NavBarControl1.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.NavBarControl1.TabIndex = 94
        Me.NavBarControl1.Text = "Entrada al Almacen"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDescuentos), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrPolizaCheque)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'nvrDatosGenerales
        '
        Me.nvrDatosGenerales.Caption = "Datos Generales"
        Me.nvrDatosGenerales.LargeImage = CType(resources.GetObject("nvrDatosGenerales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosGenerales.LargeImageIndex = 0
        Me.nvrDatosGenerales.Name = "nvrDatosGenerales"
        '
        'nvrDescuentos
        '
        Me.nvrDescuentos.Caption = "Descuentos"
        Me.nvrDescuentos.LargeImage = CType(resources.GetObject("nvrDescuentos.LargeImage"), System.Drawing.Image)
        Me.nvrDescuentos.LargeImageIndex = 1
        Me.nvrDescuentos.Name = "nvrDescuentos"
        '
        'nvrPolizaCheque
        '
        Me.nvrPolizaCheque.Caption = "Póliza Cheque"
        Me.nvrPolizaCheque.Name = "nvrPolizaCheque"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkAbono_Cuenta)
        Me.Panel1.Controls.Add(Me.cboTipoPago)
        Me.Panel1.Controls.Add(Me.lblProveedor)
        Me.Panel1.Controls.Add(Me.gpb1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.lblNombreProveedor)
        Me.Panel1.Controls.Add(Me.chkEsPadre)
        Me.Panel1.Controls.Add(Me.gpTipoPago)
        Me.Panel1.Controls.Add(Me.lblPagoVariasFacturas)
        Me.Panel1.Controls.Add(Me.lblStatus)
        Me.Panel1.Controls.Add(Me.txtStatus)
        Me.Panel1.Controls.Add(Me.gpb2)
        Me.Panel1.Controls.Add(Me.clcProveedor)
        Me.Panel1.Location = New System.Drawing.Point(120, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(592, 392)
        Me.Panel1.TabIndex = 95
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.grDescuentosProveedores)
        Me.Panel2.Location = New System.Drawing.Point(120, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(592, 392)
        Me.Panel2.TabIndex = 96
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(24, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 18)
        Me.Label4.TabIndex = 64
        Me.Label4.Text = "Descuentos:"
        '
        'grDescuentosProveedores
        '
        '
        'grDescuentosProveedores.EmbeddedNavigator
        '
        Me.grDescuentosProveedores.EmbeddedNavigator.Name = ""
        Me.grDescuentosProveedores.Location = New System.Drawing.Point(24, 56)
        Me.grDescuentosProveedores.MainView = Me.grvDescuentosProveedor
        Me.grDescuentosProveedores.Name = "grDescuentosProveedores"
        Me.grDescuentosProveedores.Size = New System.Drawing.Size(544, 312)
        Me.grDescuentosProveedores.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grDescuentosProveedores.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.TabIndex = 3
        Me.grDescuentosProveedores.TabStop = False
        Me.grDescuentosProveedores.Text = "Cuentas Bancarias"
        '
        'grvDescuentosProveedor
        '
        Me.grvDescuentosProveedor.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveDescuento, Me.grcNombreDescuento, Me.grcPorcentaje, Me.grcAntesIVA, Me.grcProntoPago, Me.grcImporteDescuento})
        Me.grvDescuentosProveedor.GridControl = Me.grDescuentosProveedores
        Me.grvDescuentosProveedor.Name = "grvDescuentosProveedor"
        Me.grvDescuentosProveedor.OptionsBehavior.Editable = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowFilter = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowGroup = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowSort = False
        Me.grvDescuentosProveedor.OptionsView.ShowGroupPanel = False
        '
        'grcClaveDescuento
        '
        Me.grcClaveDescuento.Caption = "Descuento"
        Me.grcClaveDescuento.FieldName = "descuento"
        Me.grcClaveDescuento.Name = "grcClaveDescuento"
        Me.grcClaveDescuento.Width = 89
        '
        'grcNombreDescuento
        '
        Me.grcNombreDescuento.Caption = "Descripción"
        Me.grcNombreDescuento.FieldName = "nombre"
        Me.grcNombreDescuento.Name = "grcNombreDescuento"
        Me.grcNombreDescuento.VisibleIndex = 0
        Me.grcNombreDescuento.Width = 166
        '
        'grcPorcentaje
        '
        Me.grcPorcentaje.Caption = "Porcentaje"
        Me.grcPorcentaje.DisplayFormat.FormatString = "n2"
        Me.grcPorcentaje.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPorcentaje.FieldName = "porcentaje"
        Me.grcPorcentaje.Name = "grcPorcentaje"
        Me.grcPorcentaje.VisibleIndex = 3
        Me.grcPorcentaje.Width = 83
        '
        'grcAntesIVA
        '
        Me.grcAntesIVA.Caption = "Antes de IVA"
        Me.grcAntesIVA.FieldName = "antes_iva"
        Me.grcAntesIVA.Name = "grcAntesIVA"
        Me.grcAntesIVA.VisibleIndex = 1
        Me.grcAntesIVA.Width = 86
        '
        'grcProntoPago
        '
        Me.grcProntoPago.Caption = "Pronto Pago"
        Me.grcProntoPago.FieldName = "pronto_pago"
        Me.grcProntoPago.Name = "grcProntoPago"
        Me.grcProntoPago.VisibleIndex = 2
        Me.grcProntoPago.Width = 83
        '
        'grcImporteDescuento
        '
        Me.grcImporteDescuento.Caption = "Importe Descuento"
        Me.grcImporteDescuento.DisplayFormat.FormatString = "c2"
        Me.grcImporteDescuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteDescuento.FieldName = "importe_descuento"
        Me.grcImporteDescuento.Name = "grcImporteDescuento"
        Me.grcImporteDescuento.VisibleIndex = 4
        Me.grcImporteDescuento.Width = 112
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.tmaPolizaCheque)
        Me.Panel3.Controls.Add(Me.grDatosPoliza)
        Me.Panel3.Location = New System.Drawing.Point(120, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(592, 392)
        Me.Panel3.TabIndex = 97
        '
        'tmaPolizaCheque
        '
        Me.tmaPolizaCheque.BackColor = System.Drawing.Color.White
        Me.tmaPolizaCheque.CanDelete = True
        Me.tmaPolizaCheque.CanInsert = True
        Me.tmaPolizaCheque.CanUpdate = True
        Me.tmaPolizaCheque.Grid = Me.grDatosPoliza
        Me.tmaPolizaCheque.Location = New System.Drawing.Point(8, 8)
        Me.tmaPolizaCheque.Name = "tmaPolizaCheque"
        Me.tmaPolizaCheque.popTINGrid = Nothing
        Me.tmaPolizaCheque.Size = New System.Drawing.Size(576, 23)
        Me.tmaPolizaCheque.TabIndex = 11
        Me.tmaPolizaCheque.TabStop = False
        Me.tmaPolizaCheque.Title = ""
        Me.tmaPolizaCheque.UpdateTitle = "un Registro"
        '
        'grDatosPoliza
        '
        '
        'grDatosPoliza.EmbeddedNavigator
        '
        Me.grDatosPoliza.EmbeddedNavigator.Name = ""
        Me.grDatosPoliza.Location = New System.Drawing.Point(8, 33)
        Me.grDatosPoliza.MainView = Me.grvDatosPoliza
        Me.grDatosPoliza.Name = "grDatosPoliza"
        Me.grDatosPoliza.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepLkpCuentasContables, Me.RepClcImporte, Me.RepTxtConcepto})
        Me.grDatosPoliza.Size = New System.Drawing.Size(576, 352)
        Me.grDatosPoliza.Styles.AddReplace("Style2", New DevExpress.Utils.ViewStyleEx("Style2", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDatosPoliza.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDatosPoliza.TabIndex = 8
        Me.grDatosPoliza.TabStop = False
        Me.grDatosPoliza.Text = "GridControl1"
        '
        'grvDatosPoliza
        '
        Me.grvDatosPoliza.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcConsecutivo, Me.grcCuentaContable, Me.grcDescripcion, Me.grcCargo, Me.grcAbono})
        Me.grvDatosPoliza.GridControl = Me.grDatosPoliza
        Me.grvDatosPoliza.Name = "grvDatosPoliza"
        Me.grvDatosPoliza.OptionsBehavior.Editable = False
        Me.grvDatosPoliza.OptionsView.ShowFooter = True
        Me.grvDatosPoliza.OptionsView.ShowGroupPanel = False
        '
        'grcConsecutivo
        '
        Me.grcConsecutivo.Caption = "Consecutivo"
        Me.grcConsecutivo.FieldName = "partida"
        Me.grcConsecutivo.Name = "grcConsecutivo"
        Me.grcConsecutivo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCuentaContable
        '
        Me.grcCuentaContable.Caption = "Cuenta Contable"
        Me.grcCuentaContable.FieldName = "cuenta_contable"
        Me.grcCuentaContable.HeaderStyleName = "Style1"
        Me.grcCuentaContable.Name = "grcCuentaContable"
        Me.grcCuentaContable.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCuentaContable.VisibleIndex = 0
        Me.grcCuentaContable.Width = 140
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Concepto"
        Me.grcDescripcion.ColumnEdit = Me.RepTxtConcepto
        Me.grcDescripcion.FieldName = "concepto"
        Me.grcDescripcion.HeaderStyleName = "Style1"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 1
        Me.grcDescripcion.Width = 216
        '
        'RepTxtConcepto
        '
        Me.RepTxtConcepto.AutoHeight = False
        Me.RepTxtConcepto.MaxLength = 120
        Me.RepTxtConcepto.Name = "RepTxtConcepto"
        '
        'grcCargo
        '
        Me.grcCargo.Caption = "Cargo"
        Me.grcCargo.ColumnEdit = Me.RepClcImporte
        Me.grcCargo.FieldName = "cargo"
        Me.grcCargo.HeaderStyleName = "Style1"
        Me.grcCargo.Name = "grcCargo"
        Me.grcCargo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCargo.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcCargo.VisibleIndex = 2
        Me.grcCargo.Width = 110
        '
        'RepClcImporte
        '
        Me.RepClcImporte.AutoHeight = False
        Me.RepClcImporte.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepClcImporte.DisplayFormat.FormatString = "c2"
        Me.RepClcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepClcImporte.EditFormat.FormatString = "n2"
        Me.RepClcImporte.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepClcImporte.Name = "RepClcImporte"
        Me.RepClcImporte.Precision = 4
        '
        'grcAbono
        '
        Me.grcAbono.Caption = "Abono"
        Me.grcAbono.ColumnEdit = Me.RepClcImporte
        Me.grcAbono.FieldName = "abono"
        Me.grcAbono.HeaderStyleName = "Style1"
        Me.grcAbono.Name = "grcAbono"
        Me.grcAbono.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcAbono.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcAbono.VisibleIndex = 3
        Me.grcAbono.Width = 96
        '
        'RepLkpCuentasContables
        '
        Me.RepLkpCuentasContables.AutoHeight = False
        Me.RepLkpCuentasContables.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepLkpCuentasContables.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("cuentacontable", "Cuenta", 40, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Center), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripción", 50)})
        Me.RepLkpCuentasContables.DisplayMember = "cuentacontable"
        Me.RepLkpCuentasContables.Name = "RepLkpCuentasContables"
        Me.RepLkpCuentasContables.NullText = ""
        Me.RepLkpCuentasContables.PopupWidth = 200
        Me.RepLkpCuentasContables.ValueMember = "cuentacontable"
        '
        'frmMovimientosPagar
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(722, 552)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "frmMovimientosPagar"
        Me.Controls.SetChildIndex(Me.Panel2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox4, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        Me.Controls.SetChildIndex(Me.Panel3, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcEntrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Vto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSubtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReferencia_Transferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFormulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutorizo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFolio_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpb1.ResumeLayout(False)
        CType(Me.clcSaldoTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpb2.ResumeLayout(False)
        Me.gpTipoPago.ResumeLayout(False)
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.cboTipoPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEsPadre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbono_Cuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.grDescuentosProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDescuentosProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.grDatosPoliza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDatosPoliza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepTxtConcepto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepClcImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepLkpCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oMovimientosPagar As VillarrealBusiness.clsMovimientosPagar
    Private oChequeras As VillarrealBusiness.clsChequeras
    Private oConceptosCxp As VillarrealBusiness.clsConceptosCxp
    Private oEntradasProveedorDescuentos As VillarrealBusiness.clsEntradasProveedorDescuentos
    Private oPolizasCheques As VillarrealBusiness.clsPolizasCheques
    Private ovariables As New VillarrealBusiness.clsVariables


    Private ReadOnly Property Concepto() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConcepto)
        End Get
    End Property
    Private ReadOnly Property ConceptoReferencia() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoReferencia)
        End Get
    End Property
    Private ReadOnly Property Cargo() As Object
        Get
            Return IIf(CStr(Me.lkpConcepto.GetValue("tipo")) = "P" Or CStr(Me.lkpConcepto.GetValue("tipo")) = "A", System.DBNull.Value, Me.clcTotal.EditValue)
        End Get
    End Property
    Private ReadOnly Property Abono() As Object
        Get
            Return IIf(CStr(Me.lkpConcepto.GetValue("tipo")) = "P" Or CStr(Me.lkpConcepto.GetValue("tipo")) = "A", Me.clcTotal.EditValue, System.DBNull.Value)
        End Get
    End Property
    Private ReadOnly Property TipoPago() As Object
        Get
            Return IIf(Me.cboTipoPago.EditValue = "P", System.DBNull.Value, Me.cboTipoPago.EditValue)
        End Get
    End Property
    Private ReadOnly Property Banco() As Object
        Get
            If Me.lkpChequera.EditValue Is Nothing Then
                Return System.DBNull.Value
            Else
                Return Me.lkpChequera.GetValue("banco")
            End If
        End Get
    End Property
    'Private ReadOnly Property FolioReferencia() As String
    '    Get
    '        If Me.lkpFolioReferencia.EditValue Is Nothing Then
    '            Return System.DBNull.Value
    '        Else
    '            Return Me.lkpFolioReferencia.GetValue("banco")
    '    End Get
    'End Property
    ReadOnly Property FolioChequera(ByVal cuenta_bancaria As String) As Long
        Get
            Return CType(oChequeras.DespliegaDatos(cuenta_bancaria).Value, DataSet).Tables(0).Rows(0).Item("folio") + 1
        End Get
    End Property
    Private ReadOnly Property Impuesto() As Double
        Get
            If ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float) = -1 Then
                Return 0
            Else
                Return (CType(ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double) / 100)
            End If


        End Get
    End Property

    Private ReadOnly Property Status() As String
        Get
            If Action = Actions.Insert And CStr(Me.lkpConcepto.GetValue("tipo") = "P") Or CStr(Me.lkpConcepto.GetValue("tipo") = "A") Then
                Return ""
            Else
                Return Me.txtStatus.Text
            End If

        End Get
    End Property
    Private TipoConcepto As Char = ""

    Private Folio_Poliza_Cheque As Long = 0

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmMovimientosPagar_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmMovimientosPagar_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmMovimientosPagar_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmMovimientosPagar_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Me.ObtenerFolioPolizaCheque(Response)
                Response = oMovimientosPagar.Insertar(Me.DataSource, TipoPago, Cargo, Abono, Banco, Me.txtStatus.Text, Me.chkAbono_Cuenta.EditValue, Me.Folio_Poliza_Cheque)

            Case Actions.Update
                Me.ObtenerFolioPolizaCheque(Response)
                Response = oMovimientosPagar.Actualizar(Me.DataSource, TipoPago, Cargo, Abono, Banco, Status, ActualizarCheque, Me.Folio_Poliza_Cheque)

            Case Actions.Delete
                Response = oMovimientosPagar.Eliminar(Concepto, clcFolio.Value)

        End Select
    End Sub
    Private Sub frmMovimientosPagar_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim oDataSet As New DataSet
        Response = oMovimientosPagar.DespliegaDatos(OwnerForm.Value("concepto"), OwnerForm.Value("folio"))
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            'Obtengo el Folio del pago a proveedores si dicho movimiento fue generado desde Pago a Varias Factuas
            FolioPagoProveedores = oDataSet.Tables(0).Rows(0).Item("folio_pago_proveedores")

            ' OBTENGO EL FOLIO DE LA POLIZA 
            Folio_Poliza_Cheque = oDataSet.Tables(0).Rows(0).Item("folio_poliza_cheque")

            Me.cboTipoPago.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("tipo_pago")), "P", oDataSet.Tables(0).Rows(0).Item("tipo_pago"))
            Me.lkpChequera.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cuenta_bancaria")), Nothing, oDataSet.Tables(0).Rows(0).Item("cuenta_bancaria"))
            Me.clcSaldo.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("saldo")), 0, oDataSet.Tables(0).Rows(0).Item("saldo"))
            Me.txtStatus.Text = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("status")), "", oDataSet.Tables(0).Rows(0).Item("status"))
        End If

        If CStr(Me.lkpConcepto.GetValue("tipo")) = "F" Or CStr(Me.lkpConcepto.GetValue("tipo")) = "C" Then
            'Inhabilito el boton de imprimir cheque si es un Cargo  o Factura
            Me.btnImprimeCheque.Enabled = False

            If Me.clcTotal.EditValue = Me.clcSaldo.EditValue Then
                Me.clcSubtotal.Enabled = True
                Me.clcIva.Enabled = True

            Else
                Me.clcSubtotal.Enabled = False
                Me.clcIva.Enabled = False
                If Action = Actions.Delete Then
                    Me.tbrTools.Buttons(0).Enabled = False
                End If
            End If

            'LLENA EL GRID
            Response = oEntradasProveedorDescuentos.DespliegaDatos(Me.clcEntrada.EditValue)
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                Me.grDescuentosProveedores.DataSource = oDataSet.Tables(0)
            End If

            ''Inhabilito el Master de la Poliza
            'Me.tmaPolizaCheque.CanInsert = False
            'Me.tmaPolizaCheque.CanUpdate = False
            'Me.tmaPolizaCheque.CanDelete = False



        Else
            'Habilito el boton de imprimir cheque si es un Abono  o Pago
            Me.btnImprimeCheque.Enabled = True

            ''Habilito el Master de la Poliza
            'Me.tmaPolizaCheque.CanInsert = True
            'Me.tmaPolizaCheque.CanUpdate = True
            'Me.tmaPolizaCheque.CanDelete = True

            'si el Pago viene de un Pago a Varias Facturas No se puede Hacer nada con el Movimiento
            If Me.FolioPagoProveedores > 0 Then
                Me.tbrTools.Buttons(0).Enabled = False
                Me.btnImprimeCheque.Enabled = False
                Me.lblPagoVariasFacturas.Visible = True
            End If
        End If


        If Me.txtStatus.Text = "C" Then
            Me.tbrTools.Buttons(0).Enabled = False
        End If



        'Obtengo los datos de la poliza del cheque
        Response = Me.oPolizasCheques.DespliegaDatos(Me.Folio_Poliza_Cheque)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaPolizaCheque.DataSource = oDataSet
        End If

    End Sub
    Private Sub frmMovimientosPagar_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oMovimientosPagar = New VillarrealBusiness.clsMovimientosPagar
        oChequeras = New VillarrealBusiness.clsChequeras
        oConceptosCxp = New VillarrealBusiness.clsConceptosCxp
        oEntradasProveedorDescuentos = New VillarrealBusiness.clsEntradasProveedorDescuentos
        oPolizasCheques = New VillarrealBusiness.clsPolizasCheques


        Me.clcProveedor.EditValue = CType(OwnerForm, brwMovimientosPagar).Proveedor
        Me.lblNombreProveedor.Text = CType(OwnerForm, brwMovimientosPagar).NombreProveedor
        Me.btnImprimeCheque.Enabled = False


        With Me.tmaPolizaCheque
            .UpdateTitle = "Un Registro"
            .UpdateForm = New Comunes.frmPolizaChequeDetalle
            '.AddColumn("partida")
            .AddColumn("cuenta_contable", "System.String")
            .AddColumn("concepto", "System.String")
            .AddColumn("cargo", "System.Double")
            .AddColumn("abono", "System.Double")
        End With

        Select Case Action
            Case Actions.Insert
                Me.dteFecha.DateTime = CDate(TINApp.FechaServidor)
                Me.txtFormulo.Text = CStr(TINApp.Connection.UserName)

            Case Actions.Update
                Me.cboTipoPago.Enabled = True
                If Me.cboTipoPago.Value <> "T" And Me.cboTipoPago.Value <> "C" Then
                    Me.cboTipoPago.Value = "P"
                End If

                Me.gpb1.Enabled = False
                Me.dteFecha.Enabled = False
                Me.clcSubtotal.Enabled = False
                Me.clcIva.Enabled = False



            Case Actions.Delete
                Me.gpb1.Enabled = False
                Me.gpb2.Enabled = False
                Me.gpTipoPago.Enabled = False
                Me.GroupBox4.Enabled = False
        End Select
    End Sub
    Private Sub frmMovimientosPagar_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMovimientosPagar.Validacion(Action, Me.clcProveedor.EditValue, Concepto, Me.dteFecha.Text, Me.clcSubtotal.EditValue, Me.clcIva.EditValue, Me.clcTotal.EditValue, Me.cboTipoPago.EditValue, Banco, Me.txtReferencia_Transferencia.Text, Me.clcCheque.EditValue)
    End Sub

    Private Sub frmMovimientosPagar_Localize() Handles MyBase.Localize
    End Sub
    Private Sub frmMovimientosPagar_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        '  Tipo Pago
        '====================
        ' el 1 son movimientos pagar
        ' el 2 son pagos diversos
        ' el 3 son movimientos chequera

        Me.tmaPolizaCheque.MoveFirst()

        Do While Not Me.tmaPolizaCheque.EOF
            Select Case tmaPolizaCheque.CurrentAction
                Case Actions.Insert
                    Response = Me.oPolizasCheques.Insertar(Me.tmaPolizaCheque.SelectedRow, Me.Folio_Poliza_Cheque, 1)
                Case Actions.Update
                    Response = Me.oPolizasCheques.Actualizar(Me.tmaPolizaCheque.SelectedRow, Me.Folio_Poliza_Cheque)
                Case Actions.Delete
                    Response = Me.oPolizasCheques.Eliminar(Me.Folio_Poliza_Cheque, Me.tmaPolizaCheque.Item("partida"))
            End Select
            If Response.ErrorFound Then Exit Sub
            tmaPolizaCheque.MoveNext()
        Loop
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpChequera_Format() Handles lkpChequera.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpChequera)
    End Sub
    Private Sub lkpChequera_LoadData(ByVal Initialize As Boolean) Handles lkpChequera.LoadData
        Dim response As Events
        response = oChequeras.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpChequera.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpChequera_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpChequera.EditValueChanged
        If Me.lkpChequera.EditValue Is Nothing Then Exit Sub

        Me.txtNombreBanco.Text = CStr(Me.lkpChequera.GetValue("nombre_banco"))
        Me.clcBanco.Value = Me.Banco
        If Me.cboTipoPago.EditValue <> "C" Then
            Me.clcCheque.EditValue = 0
        End If
    End Sub

    Private Sub lkpConcepto_Format() Handles lkpConcepto.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConcepto)
    End Sub
    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpConcepto.LoadData
        Dim response As Events
        response = oConceptosCxp.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConcepto.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpConcepto_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConcepto.EditValueChanged
        If Me.lkpConcepto.EditValue Is Nothing Then Exit Sub

        Me.lblDrescripcionConcepto.Text = CStr(Me.lkpConcepto.GetValue("descripcion"))

        Me.TipoConcepto = CStr(Me.lkpConcepto.GetValue("tipo"))

        'CONCEPTO    P :  PAGO     Y   A :  ABONO
        If TipoConcepto = "P" Or TipoConcepto = "A" Then
            Me.chkEsPadre.Checked = False
            Me.clcSaldo.Visible = False
            Me.lblSaldo.Visible = False
            Me.lkpConceptoReferencia.Enabled = True
            Me.lkpConceptoReferencia.EditValue = ""
            Me.lblDrescripcionConceptoRef.Text = ""
            Me.lkpConceptoReferencia.AutoReaload = True ''nuevo
            Me.lkpFolioReferencia.Enabled = True
            Me.clcEntrada.Enabled = False
            Me.txtFolio_Factura.Enabled = False
            Me.clcPlazo.EditValue = 0
            Me.cboTipoPago.Enabled = True
        Else 'CONCEPTO    F :  FACTURA     Y   C :  CARGO
            Me.chkEsPadre.Checked = True
            Me.clcSaldo.Visible = True
            Me.lblSaldo.Visible = True

            Me.lkpConceptoReferencia.EditValue = ""
            Me.lkpConceptoReferencia.AutoReaload = True
            Me.lblDrescripcionConceptoRef.Text = ""
            Me.lblDrescripcionConceptoRef.Text = Me.lkpConceptoReferencia.GetValue("descripcion") '.EditValue ' Me.lkpConceptoReferencia.Text = Me.lkpConcepto.EditValue
            Me.lkpFolioReferencia.EditValue = Me.clcFolio.EditValue
            Me.clcEntrada.Enabled = True
            Me.txtFolio_Factura.Enabled = True
            Me.lblSaldoTotal.Visible = False
            Me.clcSaldoTotal.Visible = False
            'Me.clcPlazo.EditValue = PlazoMayor()
            Me.cboTipoPago.Enabled = False
            Me.lkpConceptoReferencia.Enabled = False
            Me.lkpFolioReferencia.Enabled = False

            LimpiarDatos()
        End If
    End Sub

    Private Sub lkpConceptoReferencia_Format() Handles lkpConceptoReferencia.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConceptoReferencia)
    End Sub
    Private Sub lkpConceptoReferencia_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoReferencia.LoadData
        Dim response As Events
        Dim tipo As Char = ""

        If Concepto.Length > 0 Then tipo = Me.lkpConcepto.GetValue("tipo")

        response = oConceptosCxp.LookupConceptoReferencia(tipo)
        tipo = ""
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoReferencia.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpConceptoReferencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConceptoReferencia.EditValueChanged
        If Me.lkpConceptoReferencia.EditValue Is Nothing Then Exit Sub
        Me.lkpFolioReferencia.Enabled = True
        Me.lkpFolioReferencia.EditValue = ""

        Me.lkpFolioReferencia.AutoReaload = True
        Me.lblDrescripcionConceptoRef.Text = CStr(Me.lkpConceptoReferencia.GetValue("descripcion"))
    End Sub

    Private Sub lkpFolioReferencia_LoadData(ByVal Initialize As Boolean) Handles lkpFolioReferencia.LoadData
        Dim response As Events

        Dim proveedor As Long


        proveedor = Me.clcProveedor.EditValue
        response = oMovimientosPagar.Lookup(ConceptoReferencia, proveedor)

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolioReferencia.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpFolioReferencia_Format() Handles lkpFolioReferencia.Format
        Comunes.clsFormato.for_movimientos_pagar_grl(Me.lkpFolioReferencia)
    End Sub
    Private Sub lkpFolioReferencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolioReferencia.EditValueChanged
        If Me.lkpFolioReferencia.EditValue Is Nothing Then Exit Sub

        CargarDatosMovimientoPagar()

        ''LLENA EL GRID
        'If Me.lkpFolioReferencia.EditValue > 0 Then
        '    Dim oDataSet As New DataSet
        '    Dim response As Dipros.Utils.Events
        '    response = oEntradasProveedorDescuentos.DespliegaDatos(Me.lkpFolioReferencia.GetValue("entrada")) '(Me.clcEntrada.EditValue)
        '    If Not response.ErrorFound Then
        '        oDataSet = response.Value
        '        Me.grDescuentosProveedores.DataSource = oDataSet.Tables(0)
        '    End If
        'End If

    End Sub

    Private Sub cboTipoPago_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoPago.SelectedIndexChanged
        Select Case CStr(Me.cboTipoPago.EditValue).ToUpper
            Case "P"
                Me.lkpChequera.EditValue = Nothing
                Me.txtReferencia_Transferencia.Text = ""
                Me.lkpChequera.Enabled = False
                Me.txtReferencia_Transferencia.Enabled = False
                Me.clcCheque.EditValue = 0
            Case "T"
                Me.lkpChequera.EditValue = Nothing
                Me.lkpChequera.Enabled = True
                Me.txtReferencia_Transferencia.Enabled = True
                Me.lkpChequera.EditValue = Comunes.clsUtilerias.uti_ChequeraPredeterminada
            Case "C"
                Me.lkpChequera.Enabled = True
                Me.txtReferencia_Transferencia.Text = ""
                Me.txtReferencia_Transferencia.Enabled = False
                Me.lkpChequera.EditValue = Comunes.clsUtilerias.uti_ChequeraPredeterminada
        End Select
    End Sub
    Private Sub dteFecha_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha.EditValueChanged
        If Not IsDate(Me.dteFecha.Text) Or Me.lkpConcepto.EditValue Is Nothing Then Exit Sub

        If CStr(Me.lkpConcepto.GetValue("tipo")) = "P" Or CStr(Me.lkpConcepto.GetValue("tipo")) = "A" Then
            Me.dteFecha_Vto.EditValue = Me.dteFecha.EditValue
        Else
            Me.dteFecha_Vto.EditValue = Me.dteFecha.DateTime '.AddDays(PlazoMayor)
        End If


    End Sub

    Private Sub clcSubtotal_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcSubtotal.EditValueChanged
        If Me.clcSubtotal.EditValue Is Nothing Then Exit Sub
        CalcularIva()
        CalcularTotal()
    End Sub
    Private Sub clcIva_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcIva.EditValueChanged
        If Me.clcIva.EditValue Is Nothing Then Exit Sub

        CalcularTotal()
    End Sub
    Private Sub clcTotal_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcTotal.EditValueChanged
        If Me.clcTotal.EditValue Is Nothing Or Not IsNumeric(Me.clcTotal.EditValue) Then Exit Sub

        Me.clcSaldo.EditValue = Me.clcTotal.EditValue


    End Sub

    Private Sub nvrDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosGenerales.LinkPressed
        Me.Panel1.BringToFront()
    End Sub
    Private Sub nvrDescuentos_LinkPressed(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDescuentos.LinkPressed
        Me.Panel2.BringToFront()
    End Sub
    Private Sub nvrPolizaCheque_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrPolizaCheque.LinkPressed
        Me.Panel3.BringToFront()
    End Sub


    Public Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

        If e.Button Is Me.btnImprimeCheque And Action = Actions.Update Then
            Dim response As Events

            'Verificar que el tipo sea Cheque para generarlo
            If Me.cboTipoPago.EditValue <> "C" Then
                response = New Events
                response.Message = "No se Puede Imprimir Cheque, el Tipo de Pago debe ser Cheque"
                response.ShowMessage()
                Exit Sub
            End If


            'Verificar Poliza Cuadrada y Completa para la Generacion del Cheque
            If Not (Me.PolizaCuadrada And Me.PolizaCompleta(clcTotal.Value)) Then
                response = New Events
                response.Message = "La Poliza No esta Cuadrada o No esta Completa"
                response.ShowMessage()
                Exit Sub
            End If

            response = oMovimientosPagar.Validacion(Action, Me.clcProveedor.EditValue, Concepto, Me.dteFecha.Text, Me.clcSubtotal.EditValue, Me.clcIva.EditValue, Me.clcTotal.EditValue, Me.cboTipoPago.EditValue, Me.clcBanco.Text, Me.txtReferencia_Transferencia.Text, Me.clcCheque.EditValue)
            If response.ErrorFound Then
                response.ShowMessage()
                Exit Sub
            End If
            'Abro la Transaccion para obtener el folio del cheque
            frmMovimientosPagar_BeginUpdate()
            If Me.clcCheque.Value = 0 And Me.cboTipoPago.Value = "C" Then
                'asigno el valor al control para que lo actualice
                Me.clcCheque.Value = Me.FolioChequera(Me.lkpChequera.EditValue)
                ActualizarCheque = False
            Else
                ActualizarCheque = True
            End If
            'Actualizo los Valores
            frmMovimientosPagar_Accept(response)
            frmMovimientosPagar_Detail(response)

            If Not response.ErrorFound Then
                'Termino Satisfactoriamente la transaccion
                frmMovimientosPagar_EndUpdate()
                'Imprimo el cheque
                ImprimirCheque(Me.lkpChequera.EditValue, Me.clcCheque.Value)

                Me.Close()
            Else
                'Aborto o Cancelo la Transaccion
                frmMovimientosPagar_AbortUpdate()
                response.ShowMessage()
            End If
        End If
    End Sub

    Private Sub clcEntrada_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcEntrada.EditValueChanged
        If Me.clcTotal.EditValue Is Nothing Or Not IsNumeric(Me.clcEntrada.EditValue) Then Exit Sub

        'LLENA EL GRID
        If Me.clcEntrada.EditValue > 0 Then
            Dim oDataSet As New DataSet
            Dim response As Dipros.Utils.Events
            response = oEntradasProveedorDescuentos.DespliegaDatos(Me.clcEntrada.EditValue)
            If Not response.ErrorFound Then
                oDataSet = response.Value
                Me.grDescuentosProveedores.DataSource = oDataSet.Tables(0)
            End If
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarDatosMovimientoPagar()
        Me.dteFecha.EditValue = Me.lkpFolioReferencia.GetValue("fecha")

        If Action = Actions.Insert Then
            '@JGTO-18/04/2008: Se corrige esta snetencia para que obtenga el subtotal a partir del saldo (calculo inverso)
            Me.clcSubtotal.EditValue = Me.lkpFolioReferencia.GetValue("saldo") / (1 + Impuesto)
            Me.clcIva.EditValue = Me.clcSubtotal.EditValue * Impuesto
            Me.clcTotal.EditValue = Me.lkpFolioReferencia.GetValue("saldo")
        Else
            Me.clcSubtotal.EditValue = Me.lkpFolioReferencia.GetValue("subtotal")
            Me.clcIva.EditValue = Me.lkpFolioReferencia.GetValue("iva")
            Me.clcTotal.EditValue = Me.lkpFolioReferencia.GetValue("total")
        End If

        If TipoConcepto.ToString.ToUpper = "A" Or TipoConcepto.ToString.ToUpper = "P" Then
            Me.clcEntrada.EditValue = 0
        Else
            Me.clcEntrada.EditValue = Me.lkpFolioReferencia.GetValue("entrada")
        End If


        Me.txtFolio_Factura.Text = Me.lkpFolioReferencia.GetValue("folio_factura")
        Me.dteFecha_Vto.EditValue = Me.dteFecha.EditValue
        Me.lblSaldoTotal.Visible = True
        Me.clcSaldoTotal.Visible = True
        Me.clcSaldoTotal.EditValue = Me.lkpFolioReferencia.GetValue("saldo")
    End Sub
    Private Sub LimpiarDatos()

        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
        Me.dteFecha_Vto.EditValue = Me.dteFecha.DateTime '.AddDays(PlazoMayor)
        Me.clcSubtotal.EditValue = 0
        Me.clcIva.EditValue = 0
        Me.clcTotal.EditValue = 0
        Me.clcEntrada.EditValue = 0
        Me.txtFolio_Factura.Text = ""

    End Sub
    Private Sub CalcularIva()
        If Me.clcSubtotal.EditValue Is Nothing Or Not IsNumeric(Me.clcSubtotal.EditValue) Then Exit Sub

        Me.clcIva.EditValue = Me.clcSubtotal.EditValue * Impuesto 'iva   Me.clcIva.EditValue
    End Sub
    Private Sub CalcularTotal()
        If Me.clcSubtotal.EditValue Is Nothing Or Me.clcIva.EditValue Is Nothing Or Not IsNumeric(Me.clcSubtotal.EditValue) Or Not IsNumeric(Me.clcIva.EditValue) Then Exit Sub

        Me.clcTotal.Value = Me.clcSubtotal.Value + Me.clcIva.Value
    End Sub
    'Private Function PlazoMayor() As Long
    '    Dim oEvents As Events
    '    Dim oProveedores As New VillarrealBusiness.clsProveedores
    '    Dim oDataSet As DataSet
    '    Dim plazo As Long = 0

    '    oEvents = oProveedores.TraerPlazoMayor(Me.clcProveedor.EditValue)
    '    If Not oEvents.ErrorFound Then

    '        oDataSet = oEvents.Value
    '        plazo = CLng(oDataSet.Tables(0).Rows(0).Item("plazo"))

    '    End If

    '    oEvents = Nothing
    '    oProveedores = Nothing
    '    oDataSet = Nothing

    '    Return plazo
    'End Function
    Private Sub ImprimirCheque(ByVal cuentabancaria As String, ByVal folio_cheque As Integer)
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try

            response = oReportes.ImprimeChequeMovimientosPagar(Me.lkpChequera.GetValue("banco"), cuentabancaria, folio_cheque)

            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Cheque no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptCheque

                    oDataSet = response.Value

                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Cheques", oReport)
                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

    Private Function ObtenerFolioPolizaCheque(ByRef response As Dipros.Utils.Events) As Long

        If Me.tmaPolizaCheque.View.RowCount > 0 And Folio_Poliza_Cheque = 0 Then
            response = Me.oPolizasCheques.ObtenerSiguienteFolio
            If Not response.ErrorFound Then
                Me.Folio_Poliza_Cheque = response.Value + 1
            End If
        End If
    End Function
    Public Function PolizaCuadrada() As Boolean
        Dim abono As Double = 0
        Dim cargo As Double = 0

        abono = CType(Me.grcAbono.SummaryItem.SummaryValue, Double)
        cargo = CType(Me.grcCargo.SummaryItem.SummaryValue, Double)

        If cargo = 0 And abono = 0 Then
            Me.tmaPolizaCheque.Title = "Sin Registros"
            Return False
        Else
            If cargo <> abono Then

                Me.tmaPolizaCheque.Title = "Descuadrada"
                Return False
            Else

                Me.tmaPolizaCheque.Title = "Cuadrada"
                Return True
            End If
        End If
    End Function
    Private Function PolizaCompleta(ByVal Total As Double) As Boolean
        Dim abono As Double = 0
        Dim cargo As Double = 0

        abono = CType(Me.grcAbono.SummaryItem.SummaryValue, Double)
        cargo = CType(Me.grcCargo.SummaryItem.SummaryValue, Double)

        If abono > 0 And cargo > 0 And (abono = cargo) And abono = Total Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

End Class
