Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias




Public Class brwMovimientosChequera
    Inherits Dipros.Windows.frmTINGridNet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents lkpBanco As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpCuentaBancaria As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCuenta_Bancaria As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lkpBanco = New Dipros.Editors.TINMultiLookup
        Me.lkpCuentaBancaria = New Dipros.Editors.TINMultiLookup
        Me.lblCuenta_Bancaria = New System.Windows.Forms.Label
        Me.lblSaldo = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FilterPanel.SuspendLayout()
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.Label3)
        Me.FilterPanel.Controls.Add(Me.lblSaldo)
        Me.FilterPanel.Controls.Add(Me.lkpCuentaBancaria)
        Me.FilterPanel.Controls.Add(Me.lblCuenta_Bancaria)
        Me.FilterPanel.Controls.Add(Me.lblBanco)
        Me.FilterPanel.Controls.Add(Me.lkpBanco)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(770, 61)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpBanco, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblBanco, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblCuenta_Bancaria, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpCuentaBancaria, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblSaldo, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label3, 0)
        '
        'FooterPanel
        '
        Me.FooterPanel.Name = "FooterPanel"
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(59, 8)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(39, 16)
        Me.lblBanco.TabIndex = 2
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBanco
        '
        Me.lkpBanco.AllowAdd = False
        Me.lkpBanco.AutoReaload = False
        Me.lkpBanco.DataSource = Nothing
        Me.lkpBanco.DefaultSearchField = ""
        Me.lkpBanco.DisplayMember = "Nombre"
        Me.lkpBanco.EditValue = Nothing
        Me.lkpBanco.Filtered = False
        Me.lkpBanco.InitValue = Nothing
        Me.lkpBanco.Location = New System.Drawing.Point(104, 8)
        Me.lkpBanco.MultiSelect = False
        Me.lkpBanco.Name = "lkpBanco"
        Me.lkpBanco.NullText = ""
        Me.lkpBanco.PopupWidth = CType(400, Long)
        Me.lkpBanco.Required = False
        Me.lkpBanco.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBanco.SearchMember = ""
        Me.lkpBanco.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBanco.SelectAll = False
        Me.lkpBanco.Size = New System.Drawing.Size(200, 20)
        Me.lkpBanco.TabIndex = 3
        Me.lkpBanco.Tag = "Banco"
        Me.lkpBanco.ToolTip = "Banco"
        Me.lkpBanco.ValueMember = "Banco"
        '
        'lkpCuentaBancaria
        '
        Me.lkpCuentaBancaria.AllowAdd = False
        Me.lkpCuentaBancaria.AutoReaload = True
        Me.lkpCuentaBancaria.DataSource = Nothing
        Me.lkpCuentaBancaria.DefaultSearchField = ""
        Me.lkpCuentaBancaria.DisplayMember = "cuenta_bancaria"
        Me.lkpCuentaBancaria.EditValue = Nothing
        Me.lkpCuentaBancaria.Filtered = False
        Me.lkpCuentaBancaria.InitValue = Nothing
        Me.lkpCuentaBancaria.Location = New System.Drawing.Point(104, 32)
        Me.lkpCuentaBancaria.MultiSelect = False
        Me.lkpCuentaBancaria.Name = "lkpCuentaBancaria"
        Me.lkpCuentaBancaria.NullText = ""
        Me.lkpCuentaBancaria.PopupWidth = CType(300, Long)
        Me.lkpCuentaBancaria.Required = False
        Me.lkpCuentaBancaria.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCuentaBancaria.SearchMember = ""
        Me.lkpCuentaBancaria.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCuentaBancaria.SelectAll = False
        Me.lkpCuentaBancaria.Size = New System.Drawing.Size(200, 20)
        Me.lkpCuentaBancaria.TabIndex = 18
        Me.lkpCuentaBancaria.Tag = "cuenta_bancaria"
        Me.lkpCuentaBancaria.ToolTip = "Cuenta bancaria"
        Me.lkpCuentaBancaria.ValueMember = "cuenta_bancaria"
        '
        'lblCuenta_Bancaria
        '
        Me.lblCuenta_Bancaria.AutoSize = True
        Me.lblCuenta_Bancaria.Location = New System.Drawing.Point(8, 32)
        Me.lblCuenta_Bancaria.Name = "lblCuenta_Bancaria"
        Me.lblCuenta_Bancaria.Size = New System.Drawing.Size(90, 16)
        Me.lblCuenta_Bancaria.TabIndex = 17
        Me.lblCuenta_Bancaria.Tag = ""
        Me.lblCuenta_Bancaria.Text = "&Cuenta bancaria:"
        Me.lblCuenta_Bancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSaldo
        '
        Me.lblSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldo.Location = New System.Drawing.Point(488, 16)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(248, 23)
        Me.lblSaldo.TabIndex = 19
        Me.lblSaldo.Text = "$ 0.00"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(416, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 23)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Saldo:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'brwMovimientosChequera
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(770, 397)
        Me.Name = "brwMovimientosChequera"
        Me.Text = "brwMovimientosChequera"
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FilterPanel.ResumeLayout(False)

    End Sub

#End Region

    Private oMovimientosChequera As VillarrealBusiness.clsMovimientosChequera
    Private oBancos As VillarrealBusiness.clsBancos
    Private oChequeras As VillarrealBusiness.clsChequeras

#Region "Declaraciones"
    Public ReadOnly Property Banco() As Long
        Get
            Return PreparaValorLookup(Me.lkpBanco)
        End Get
    End Property
    Public ReadOnly Property CuentaBancaria() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpCuentaBancaria)
        End Get
    End Property


#End Region

#Region "Eventos de la forma"
    Private Sub brwMovimientosChequera_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        Response = oMovimientosChequera.Listado(Banco, CuentaBancaria)
    End Sub
    Private Sub brwMovimientosChequera_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMovimientosChequera.ValidacionBrowse(Me.Banco, Me.CuentaBancaria)
    End Sub
    Private Sub brwMovimientosChequera_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oMovimientosChequera = New VillarrealBusiness.clsMovimientosChequera
        oBancos = New VillarrealBusiness.clsBancos
        oChequeras = New VillarrealBusiness.clsChequeras

    End Sub

#End Region

#Region "Eventos de los controles"
    Private Sub lkpBanco_Format() Handles lkpBanco.Format
        Comunes.clsFormato.for_bancos_grl(Me.lkpBanco)
    End Sub
    Private Sub lkpBanco_LoadData(ByVal Initialize As Boolean) Handles lkpBanco.LoadData
        Dim response As Events
        response = oBancos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBanco.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub

    Private Sub lkpCuentaBancaria_Format() Handles lkpCuentaBancaria.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpCuentaBancaria)
    End Sub
    Private Sub lkpCuentaBancaria_LoadData(ByVal Initialize As Boolean) Handles lkpCuentaBancaria.LoadData
        Dim response As Events
        response = oChequeras.Lookup(Me.Banco)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCuentaBancaria.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpCuentaBancaria_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCuentaBancaria.EditValueChanged
        ObtenerSaldo(CType(Me.lkpCuentaBancaria.GetValue("saldo"), Double))
        Me.Reload(True)
    End Sub

    Public Sub ObtenerSaldo(ByVal dSaldo As Double)
        If Me.CuentaBancaria.Length > 0 Then
            Dim color As New Color

            If dSaldo < 0 Then
                Me.lblSaldo.ForeColor = color.Red
            Else
                Me.lblSaldo.ForeColor = color.Blue
            End If

            Me.lblSaldo.Text = dSaldo.ToString("C")
        End If

    End Sub

#End Region


End Class
