Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmProgramacionPagos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpCuentaBancaria As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCuenta_Bancaria As System.Windows.Forms.Label
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents lkpBanco As Dipros.Editors.TINMultiLookup
    Friend WithEvents grvMovimientos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoMovimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepChkImprimir As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents tbrSelecccion As System.Windows.Forms.ToolBar
    Friend WithEvents optTodos As System.Windows.Forms.ToolBarButton
    Friend WithEvents optNinguno As System.Windows.Forms.ToolBarButton
    Friend WithEvents grMovimientos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClaveTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grcProgramar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcTotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents grcProveedorBeneficiario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmProgramacionPagos))
        Me.grMovimientos = New DevExpress.XtraGrid.GridControl
        Me.grvMovimientos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcProgramar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepChkImprimir = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcProveedorBeneficiario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoMovimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClaveTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lkpCuentaBancaria = New Dipros.Editors.TINMultiLookup
        Me.lblCuenta_Bancaria = New System.Windows.Forms.Label
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lkpBanco = New Dipros.Editors.TINMultiLookup
        Me.tbrSelecccion = New System.Windows.Forms.ToolBar
        Me.optTodos = New System.Windows.Forms.ToolBarButton
        Me.optNinguno = New System.Windows.Forms.ToolBarButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dteHasta = New DevExpress.XtraEditors.DateEdit
        Me.dteDesde = New DevExpress.XtraEditors.DateEdit
        Me.clcTotal = New Dipros.Editors.TINCalcEdit
        Me.lblTotal = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblSaldo = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepChkImprimir, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dteHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(6502, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'grMovimientos
        '
        Me.grMovimientos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grMovimientos.EmbeddedNavigator
        '
        Me.grMovimientos.EmbeddedNavigator.Name = ""
        Me.grMovimientos.Location = New System.Drawing.Point(13, 168)
        Me.grMovimientos.MainView = Me.grvMovimientos
        Me.grMovimientos.Name = "grMovimientos"
        Me.grMovimientos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepChkImprimir})
        Me.grMovimientos.Size = New System.Drawing.Size(728, 312)
        Me.grMovimientos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientos.TabIndex = 60
        Me.grMovimientos.Text = "GridControl1"
        '
        'grvMovimientos
        '
        Me.grvMovimientos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcProgramar, Me.grcProveedorBeneficiario, Me.grcConcepto, Me.grcImporte, Me.grcTipoMovimiento, Me.grcFolio, Me.grcClaveTipo, Me.grcFecha})
        Me.grvMovimientos.GridControl = Me.grMovimientos
        Me.grvMovimientos.Name = "grvMovimientos"
        Me.grvMovimientos.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvMovimientos.OptionsView.ShowGroupPanel = False
        Me.grvMovimientos.OptionsView.ShowIndicator = False
        '
        'grcProgramar
        '
        Me.grcProgramar.Caption = "Programar"
        Me.grcProgramar.ColumnEdit = Me.RepChkImprimir
        Me.grcProgramar.FieldName = "programar"
        Me.grcProgramar.HeaderStyleName = "Style1"
        Me.grcProgramar.Name = "grcProgramar"
        Me.grcProgramar.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcProgramar.VisibleIndex = 0
        Me.grcProgramar.Width = 65
        '
        'RepChkImprimir
        '
        Me.RepChkImprimir.Name = "RepChkImprimir"
        '
        'grcProveedorBeneficiario
        '
        Me.grcProveedorBeneficiario.Caption = "Beneficiario"
        Me.grcProveedorBeneficiario.FieldName = "nombre_proveedor_beneficiario"
        Me.grcProveedorBeneficiario.Name = "grcProveedorBeneficiario"
        Me.grcProveedorBeneficiario.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcProveedorBeneficiario.VisibleIndex = 3
        Me.grcProveedorBeneficiario.Width = 157
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.VisibleIndex = 5
        Me.grcConcepto.Width = 142
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 6
        Me.grcImporte.Width = 105
        '
        'grcTipoMovimiento
        '
        Me.grcTipoMovimiento.Caption = "Tipo de Movimiento"
        Me.grcTipoMovimiento.FieldName = "tipo_movimiento_despliege"
        Me.grcTipoMovimiento.Name = "grcTipoMovimiento"
        Me.grcTipoMovimiento.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipoMovimiento.VisibleIndex = 2
        Me.grcTipoMovimiento.Width = 115
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio"
        Me.grcFolio.FieldName = "folio_movimiento"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 1
        Me.grcFolio.Width = 48
        '
        'grcClaveTipo
        '
        Me.grcClaveTipo.Caption = "grcTipo"
        Me.grcClaveTipo.FieldName = "tipo_movimiento"
        Me.grcClaveTipo.Name = "grcClaveTipo"
        Me.grcClaveTipo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 4
        Me.grcFecha.Width = 94
        '
        'lkpCuentaBancaria
        '
        Me.lkpCuentaBancaria.AllowAdd = False
        Me.lkpCuentaBancaria.AutoReaload = True
        Me.lkpCuentaBancaria.DataSource = Nothing
        Me.lkpCuentaBancaria.DefaultSearchField = ""
        Me.lkpCuentaBancaria.DisplayMember = "cuenta_bancaria"
        Me.lkpCuentaBancaria.EditValue = Nothing
        Me.lkpCuentaBancaria.Filtered = False
        Me.lkpCuentaBancaria.InitValue = Nothing
        Me.lkpCuentaBancaria.Location = New System.Drawing.Point(138, 43)
        Me.lkpCuentaBancaria.MultiSelect = False
        Me.lkpCuentaBancaria.Name = "lkpCuentaBancaria"
        Me.lkpCuentaBancaria.NullText = ""
        Me.lkpCuentaBancaria.PopupWidth = CType(300, Long)
        Me.lkpCuentaBancaria.Required = False
        Me.lkpCuentaBancaria.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCuentaBancaria.SearchMember = ""
        Me.lkpCuentaBancaria.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCuentaBancaria.SelectAll = False
        Me.lkpCuentaBancaria.Size = New System.Drawing.Size(200, 20)
        Me.lkpCuentaBancaria.TabIndex = 64
        Me.lkpCuentaBancaria.Tag = "cuenta_bancaria"
        Me.lkpCuentaBancaria.ToolTip = "Cuenta bancaria"
        Me.lkpCuentaBancaria.ValueMember = "cuenta_bancaria"
        '
        'lblCuenta_Bancaria
        '
        Me.lblCuenta_Bancaria.AutoSize = True
        Me.lblCuenta_Bancaria.Location = New System.Drawing.Point(34, 43)
        Me.lblCuenta_Bancaria.Name = "lblCuenta_Bancaria"
        Me.lblCuenta_Bancaria.Size = New System.Drawing.Size(99, 16)
        Me.lblCuenta_Bancaria.TabIndex = 63
        Me.lblCuenta_Bancaria.Tag = ""
        Me.lblCuenta_Bancaria.Text = "&Cuenta bancaria:"
        Me.lblCuenta_Bancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(90, 19)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 61
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBanco
        '
        Me.lkpBanco.AllowAdd = False
        Me.lkpBanco.AutoReaload = False
        Me.lkpBanco.DataSource = Nothing
        Me.lkpBanco.DefaultSearchField = ""
        Me.lkpBanco.DisplayMember = "Nombre"
        Me.lkpBanco.EditValue = Nothing
        Me.lkpBanco.Filtered = False
        Me.lkpBanco.InitValue = Nothing
        Me.lkpBanco.Location = New System.Drawing.Point(138, 19)
        Me.lkpBanco.MultiSelect = False
        Me.lkpBanco.Name = "lkpBanco"
        Me.lkpBanco.NullText = ""
        Me.lkpBanco.PopupWidth = CType(400, Long)
        Me.lkpBanco.Required = False
        Me.lkpBanco.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBanco.SearchMember = ""
        Me.lkpBanco.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBanco.SelectAll = False
        Me.lkpBanco.Size = New System.Drawing.Size(200, 20)
        Me.lkpBanco.TabIndex = 62
        Me.lkpBanco.Tag = "Banco"
        Me.lkpBanco.ToolTip = "Banco"
        Me.lkpBanco.ValueMember = "Banco"
        '
        'tbrSelecccion
        '
        Me.tbrSelecccion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbrSelecccion.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.tbrSelecccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbrSelecccion.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.optTodos, Me.optNinguno})
        Me.tbrSelecccion.ButtonSize = New System.Drawing.Size(67, 22)
        Me.tbrSelecccion.Dock = System.Windows.Forms.DockStyle.None
        Me.tbrSelecccion.DropDownArrows = True
        Me.tbrSelecccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrSelecccion.ImageList = Me.ilsToolbar
        Me.tbrSelecccion.Location = New System.Drawing.Point(13, 136)
        Me.tbrSelecccion.Name = "tbrSelecccion"
        Me.tbrSelecccion.ShowToolTips = True
        Me.tbrSelecccion.Size = New System.Drawing.Size(728, 29)
        Me.tbrSelecccion.TabIndex = 66
        Me.tbrSelecccion.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right
        '
        'optTodos
        '
        Me.optTodos.ImageIndex = 6
        Me.optTodos.Text = "Autorizar Todas"
        Me.optTodos.ToolTipText = "Autorizar Todas"
        '
        'optNinguno
        '
        Me.optNinguno.ImageIndex = 7
        Me.optNinguno.Text = "Cancelar Todas"
        Me.optNinguno.ToolTipText = "Cancelar Todas"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblBanco)
        Me.GroupBox1.Controls.Add(Me.lkpBanco)
        Me.GroupBox1.Controls.Add(Me.lblCuenta_Bancaria)
        Me.GroupBox1.Controls.Add(Me.lkpCuentaBancaria)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 96)
        Me.GroupBox1.TabIndex = 67
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de la Chequera"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.dteHasta)
        Me.GroupBox2.Controls.Add(Me.dteDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(381, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(360, 56)
        Me.GroupBox2.TabIndex = 68
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Rango de Fechas del Vencimiento"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(200, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "&Hasta:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "&Desde:"
        '
        'dteHasta
        '
        Me.dteHasta.EditValue = New Date(2006, 9, 14, 0, 0, 0, 0)
        Me.dteHasta.Location = New System.Drawing.Point(248, 21)
        Me.dteHasta.Name = "dteHasta"
        '
        'dteHasta.Properties
        '
        Me.dteHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteHasta.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteHasta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteHasta.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteHasta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteHasta.Size = New System.Drawing.Size(96, 20)
        Me.dteHasta.TabIndex = 1
        '
        'dteDesde
        '
        Me.dteDesde.EditValue = New Date(2006, 9, 14, 0, 0, 0, 0)
        Me.dteDesde.Location = New System.Drawing.Point(72, 21)
        Me.dteDesde.Name = "dteDesde"
        '
        'dteDesde.Properties
        '
        Me.dteDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteDesde.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteDesde.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteDesde.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteDesde.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteDesde.Size = New System.Drawing.Size(96, 20)
        Me.dteDesde.TabIndex = 0
        '
        'clcTotal
        '
        Me.clcTotal.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcTotal.Location = New System.Drawing.Point(626, 143)
        Me.clcTotal.MaxValue = 0
        Me.clcTotal.MinValue = 0
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.DisplayFormat.FormatString = "C2"
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatString = "C2"
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.Enabled = False
        Me.clcTotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotal.Properties.Precision = 2
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.Color.Black)
        Me.clcTotal.Size = New System.Drawing.Size(112, 19)
        Me.clcTotal.TabIndex = 76
        Me.clcTotal.Tag = "importe"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(582, 144)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(36, 16)
        Me.lblTotal.TabIndex = 75
        Me.lblTotal.Tag = ""
        Me.lblTotal.Text = "&Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 23)
        Me.Label4.TabIndex = 78
        Me.Label4.Text = "Saldo:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSaldo
        '
        Me.lblSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldo.Location = New System.Drawing.Point(90, 12)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(248, 23)
        Me.lblSaldo.TabIndex = 77
        Me.lblSaldo.Text = "$ 0.00"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.lblSaldo)
        Me.GroupBox3.Location = New System.Drawing.Point(381, 88)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(360, 40)
        Me.GroupBox3.TabIndex = 79
        Me.GroupBox3.TabStop = False
        '
        'frmProgramacionPagos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(754, 488)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.clcTotal)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.grMovimientos)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.tbrSelecccion)
        Me.Name = "frmProgramacionPagos"
        Me.Text = "frmProgramacionPagos"
        Me.Controls.SetChildIndex(Me.tbrSelecccion, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.grMovimientos, 0)
        Me.Controls.SetChildIndex(Me.lblTotal, 0)
        Me.Controls.SetChildIndex(Me.clcTotal, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepChkImprimir, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dteHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oBancos As VillarrealBusiness.clsBancos
    Private oChequeras As VillarrealBusiness.clsChequeras
    Private oReportes As New VillarrealBusiness.Reportes
    ' MovimientosChequera La use para las validaciones ya que en esta clase se ocupan los mismos campos
    Private oMovimientosChequera As VillarrealBusiness.clsMovimientosChequera
    Private dSaldo_Chequera As Double


    Public ReadOnly Property Banco() As Long
        Get
            Return PreparaValorLookup(Me.lkpBanco)
        End Get
    End Property
    Public ReadOnly Property CuentaBancaria() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpCuentaBancaria)
        End Get
    End Property
    Private Property Saldo_Chequera() As Double
        Get
            Return dSaldo_Chequera
        End Get
        Set(ByVal Value As Double)
            dSaldo_Chequera = Value
        End Set
    End Property

    Public ReadOnly Property FolioChequera(ByVal cuenta_bancaria As String) As Long
        Get
            Return CType(oChequeras.DespliegaDatos(cuenta_bancaria).Value, DataSet).Tables(0).Rows(0).Item("folio") + 1
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmImpresionCheques_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
        oMovimientosChequera = New VillarrealBusiness.clsMovimientosChequera
        oBancos = New VillarrealBusiness.clsBancos
        oChequeras = New VillarrealBusiness.clsChequeras

        Me.dteDesde.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteHasta.EditValue = CDate(TINApp.FechaServidor)

        LLenaGridMovimientos()
    End Sub
    Private Sub frmImpresionCheques_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = Guardar()

        If Not Response.ErrorFound Then
            LLenaGridMovimientos()
        End If
    End Sub
    Private Sub frmProgramacionPagos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMovimientosChequera.ValidacionProgramacionPagos(Me.Banco, Me.CuentaBancaria, Me.clcTotal.EditValue, Me.Saldo_Chequera)
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpBanco_Format() Handles lkpBanco.Format
        Comunes.clsFormato.for_bancos_grl(Me.lkpBanco)
    End Sub
    Private Sub lkpBanco_LoadData(ByVal Initialize As Boolean) Handles lkpBanco.LoadData
        Dim response As Events
        response = oBancos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBanco.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpBanco_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBanco.EditValueChanged
        If Me.Banco > 0 Then
            Me.lkpCuentaBancaria.EditValue = Nothing
        End If
    End Sub
    Private Sub lkpCuentaBancaria_Format() Handles lkpCuentaBancaria.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpCuentaBancaria)
    End Sub
    Private Sub lkpCuentaBancaria_LoadData(ByVal Initialize As Boolean) Handles lkpCuentaBancaria.LoadData
        Dim response As Events
        response = oChequeras.Lookup(Me.Banco)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCuentaBancaria.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub

    Private Sub lkpCuentaBancaria_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCuentaBancaria.EditValueChanged
        If CuentaBancaria > 0 Then
            Saldo_Chequera = Me.lkpCuentaBancaria.GetValue("saldo")
            Me.ObtenerSaldo(Saldo_Chequera)
        End If

    End Sub
    Private Sub tbrSelecccion_ButtonClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrSelecccion.ButtonClick
        If e.Button Is Me.optTodos Or e.Button Is Me.optNinguno Then
            If e.Button Is Me.optTodos Then
                Marcar("AUTORIZAR")
            End If

            If e.Button Is Me.optNinguno Then
                Marcar("CANCELAR")
            End If

        End If
    End Sub
    Private Sub dteDesde_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteDesde.EditValueChanged
        If IsDate(Me.dteDesde.EditValue) And IsDate(Me.dteHasta.EditValue) Then
            LLenaGridMovimientos()
        End If

    End Sub
    Private Sub dteHasta_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteHasta.EditValueChanged
        If IsDate(Me.dteDesde.EditValue) And IsDate(Me.dteHasta.EditValue) Then
            LLenaGridMovimientos()
        End If

    End Sub
    Private Sub grvMovimientos_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvMovimientos.CellValueChanging
        Me.grvMovimientos.UpdateCurrentRow()
        Dim i As Long
        Dim response As Events
        Dim ImporteTotal As Double
        ImporteTotal = 0

        If e.Column Is Me.grcProgramar Then

            For i = 0 To Me.grvMovimientos.RowCount - 1
                If e.RowHandle = i And e.Value = True Then
                    ImporteTotal = ImporteTotal + Me.grvMovimientos.GetRowCellValue(i, Me.grcImporte)
                Else
                    If e.RowHandle <> i And grvMovimientos.GetRowCellValue(i, Me.grcProgramar) = True Then
                        ImporteTotal = ImporteTotal + Me.grvMovimientos.GetRowCellValue(i, Me.grcImporte)
                    End If
                End If
            Next

            Me.clcTotal.Value = ImporteTotal
            ImporteTotal = 0
        End If
        Me.grvMovimientos.SetRowCellValue(e.RowHandle, Me.grcProgramar, True)
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub Marcar(ByVal columna As String)
        Dim i As Long

        Select Case columna.Trim
            Case "AUTORIZAR"
                For i = 0 To Me.grvMovimientos.RowCount - 1
                    Me.grvMovimientos.SetRowCellValue(i, Me.grcProgramar, True)
                Next

            Case "CANCELAR"
                For i = 0 To Me.grvMovimientos.RowCount - 1
                    Me.grvMovimientos.SetRowCellValue(i, Me.grcProgramar, False)
                Next
        End Select

    End Sub
    Private Sub LLenaGridMovimientos()
        Dim response As Events
        response = oReportes.MovimientosProgramacionPagos(Me.dteDesde.DateTime, Me.dteHasta.DateTime)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = CType(response.Value, DataSet)
            grMovimientos.DataSource = odataset.Tables(0)

        Else
            response.ShowMessage()
        End If
    End Sub
    Private Function Guardar() As Events
        Me.grvMovimientos.CloseEditor()
        Me.grvMovimientos.UpdateCurrentRow()

        Dim response As New Events
        Dim i As Long
        Dim lfolio_movimiento As Long
        Dim lfolio_cheque As Long
        Dim sconcepto As String
        Dim ltipo As Long

        For i = 0 To Me.grvMovimientos.RowCount - 1
            If Me.grvMovimientos.GetRowCellValue(i, Me.grcProgramar) = True Then
                lfolio_movimiento = Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio)
                sconcepto = Me.grvMovimientos.GetRowCellValue(i, Me.grcConcepto)
                ltipo = Me.grvMovimientos.GetRowCellValue(i, Me.grcClaveTipo)

                'abro la transaccion
                TINApp.Connection.Begin()


                response = Me.oReportes.ActualizaProgramacionChequeraTipoMovimiento(Me.Banco, Me.CuentaBancaria, lfolio_movimiento, lfolio_cheque, sconcepto, ltipo)

                If Not response.ErrorFound Then
                    'Se Termino exitosa la transaccion
                    TINApp.Connection.Commit()
                Else
                    'Hubo un error en la transaccion
                    TINApp.Connection.Rollback()
                    response.ShowMessage()
                End If
            End If
        Next


        Return response
    End Function
    Public Sub ObtenerSaldo(ByVal dSaldo As Double)
        If Me.CuentaBancaria.Length > 0 Then
            Dim color As New Color

            If dSaldo < 0 Then
                Me.lblSaldo.ForeColor = color.Red
            Else
                Me.lblSaldo.ForeColor = color.Blue
            End If

            Me.lblSaldo.Text = dSaldo.ToString("C")
        End If

    End Sub
#End Region


  



End Class
