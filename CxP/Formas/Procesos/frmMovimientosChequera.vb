Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmMovimientosChequera
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Consecutivo As Long
    Dim Estatus As String
    Dim KS As Keys
    Private dImporte_anterior As Double = 0
    Private dSaldo_actual As Double = 0
    Private ActualizarCheque As Boolean
    Private FolioPagoProveedores As Long = 0
    Private Folio_Poliza_Cheque As Long = 0
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents clcBanco As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCuentabancaria As System.Windows.Forms.Label
    Friend WithEvents txtCuentabancaria As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFactura As System.Windows.Forms.Label
    Friend WithEvents txtFactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkGenerar_Cheque As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblBeneficiario As System.Windows.Forms.Label
    Friend WithEvents txtBeneficiario As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCheque As System.Windows.Forms.Label
    Friend WithEvents clcCheque As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkAbono_A_Cuenta As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblFormulo As System.Windows.Forms.Label
    Friend WithEvents txtFormulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblAutorizo As System.Windows.Forms.Label
    Friend WithEvents txtAutorizo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtconcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblNombre_Banco As System.Windows.Forms.Label
    Friend WithEvents picEliminado As System.Windows.Forms.PictureBox
    Friend WithEvents btnImprimircheque As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblPagoVariasFacturas As System.Windows.Forms.Label
    Friend WithEvents lblConceptoMovimiento As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoMovimiento As Dipros.Editors.TINMultiLookup
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nvrDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrPolizaCheque As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tmaPolizaCheque As Dipros.Windows.TINMaster
    Friend WithEvents grDatosPoliza As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDatosPoliza As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcConsecutivo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCuentaContable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepTxtConcepto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents grcCargo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepClcImporte As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepLkpCuentasContables As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosChequera))
        Me.lblBanco = New System.Windows.Forms.Label
        Me.clcBanco = New Dipros.Editors.TINCalcEdit
        Me.lblCuentabancaria = New System.Windows.Forms.Label
        Me.txtCuentabancaria = New DevExpress.XtraEditors.TextEdit
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblFactura = New System.Windows.Forms.Label
        Me.txtFactura = New DevExpress.XtraEditors.TextEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblTipo = New System.Windows.Forms.Label
        Me.cboTipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkGenerar_Cheque = New DevExpress.XtraEditors.CheckEdit
        Me.lblBeneficiario = New System.Windows.Forms.Label
        Me.txtBeneficiario = New DevExpress.XtraEditors.TextEdit
        Me.lblCheque = New System.Windows.Forms.Label
        Me.clcCheque = New Dipros.Editors.TINCalcEdit
        Me.chkAbono_A_Cuenta = New DevExpress.XtraEditors.CheckEdit
        Me.lblFormulo = New System.Windows.Forms.Label
        Me.txtFormulo = New DevExpress.XtraEditors.TextEdit
        Me.lblAutorizo = New System.Windows.Forms.Label
        Me.txtAutorizo = New DevExpress.XtraEditors.TextEdit
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblNombre_Banco = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lkpConceptoMovimiento = New Dipros.Editors.TINMultiLookup
        Me.lblConceptoMovimiento = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.txtconcepto = New DevExpress.XtraEditors.MemoEdit
        Me.picEliminado = New System.Windows.Forms.PictureBox
        Me.btnImprimircheque = New System.Windows.Forms.ToolBarButton
        Me.lblPagoVariasFacturas = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.nvrDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrPolizaCheque = New DevExpress.XtraNavBar.NavBarItem
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.tmaPolizaCheque = New Dipros.Windows.TINMaster
        Me.grDatosPoliza = New DevExpress.XtraGrid.GridControl
        Me.grvDatosPoliza = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcConsecutivo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCuentaContable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepTxtConcepto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.grcCargo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepClcImporte = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcAbono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepLkpCuentasContables = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCuentabancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkGenerar_Cheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBeneficiario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbono_A_Cuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFormulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutorizo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.txtconcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.grDatosPoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDatosPoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepTxtConcepto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepClcImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepLkpCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnImprimircheque})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4221, 28)
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(67, 16)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 0
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcBanco
        '
        Me.clcBanco.EditValue = "0"
        Me.clcBanco.Location = New System.Drawing.Point(113, 16)
        Me.clcBanco.MaxValue = 0
        Me.clcBanco.MinValue = 0
        Me.clcBanco.Name = "clcBanco"
        '
        'clcBanco.Properties
        '
        Me.clcBanco.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcBanco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcBanco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcBanco.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBanco.Size = New System.Drawing.Size(58, 19)
        Me.clcBanco.TabIndex = 1
        Me.clcBanco.Tag = "banco"
        '
        'lblCuentabancaria
        '
        Me.lblCuentabancaria.AutoSize = True
        Me.lblCuentabancaria.Location = New System.Drawing.Point(10, 40)
        Me.lblCuentabancaria.Name = "lblCuentabancaria"
        Me.lblCuentabancaria.TabIndex = 3
        Me.lblCuentabancaria.Tag = ""
        Me.lblCuentabancaria.Text = "&Cuenta Bancaria:"
        Me.lblCuentabancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCuentabancaria
        '
        Me.txtCuentabancaria.EditValue = ""
        Me.txtCuentabancaria.Location = New System.Drawing.Point(113, 40)
        Me.txtCuentabancaria.Name = "txtCuentabancaria"
        '
        'txtCuentabancaria.Properties
        '
        Me.txtCuentabancaria.Properties.MaxLength = 20
        Me.txtCuentabancaria.Size = New System.Drawing.Size(120, 20)
        Me.txtCuentabancaria.TabIndex = 4
        Me.txtCuentabancaria.Tag = "cuentabancaria"
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(75, 64)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 5
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(113, 64)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(58, 19)
        Me.clcFolio.TabIndex = 6
        Me.clcFolio.Tag = "folio"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(392, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 9, 8, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(440, 16)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 1
        Me.dteFecha.Tag = "fecha"
        '
        'lblFactura
        '
        Me.lblFactura.AutoSize = True
        Me.lblFactura.Location = New System.Drawing.Point(18, 64)
        Me.lblFactura.Name = "lblFactura"
        Me.lblFactura.Size = New System.Drawing.Size(50, 16)
        Me.lblFactura.TabIndex = 4
        Me.lblFactura.Tag = ""
        Me.lblFactura.Text = "&Factura:"
        Me.lblFactura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFactura
        '
        Me.txtFactura.EditValue = ""
        Me.txtFactura.Location = New System.Drawing.Point(80, 64)
        Me.txtFactura.Name = "txtFactura"
        '
        'txtFactura.Properties
        '
        Me.txtFactura.Properties.MaxLength = 25
        Me.txtFactura.Size = New System.Drawing.Size(159, 20)
        Me.txtFactura.TabIndex = 5
        Me.txtFactura.Tag = "factura"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(15, 88)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 6
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(80, 88)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "n2"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(96, 19)
        Me.clcImporte.TabIndex = 7
        Me.clcImporte.Tag = "importe"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(36, 16)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(32, 16)
        Me.lblTipo.TabIndex = 0
        Me.lblTipo.Tag = ""
        Me.lblTipo.Text = "&Tipo:"
        Me.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = "C"
        Me.cboTipo.Location = New System.Drawing.Point(80, 16)
        Me.cboTipo.Name = "cboTipo"
        '
        'cboTipo.Properties
        '
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Retiro", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Deposito", "A", -1)})
        Me.cboTipo.Size = New System.Drawing.Size(95, 20)
        Me.cboTipo.TabIndex = 1
        Me.cboTipo.Tag = "tipo"
        '
        'chkGenerar_Cheque
        '
        Me.chkGenerar_Cheque.EditValue = True
        Me.chkGenerar_Cheque.Location = New System.Drawing.Point(24, 168)
        Me.chkGenerar_Cheque.Name = "chkGenerar_Cheque"
        '
        'chkGenerar_Cheque.Properties
        '
        Me.chkGenerar_Cheque.Properties.Caption = "Generar Cheque"
        Me.chkGenerar_Cheque.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkGenerar_Cheque.Size = New System.Drawing.Size(112, 19)
        Me.chkGenerar_Cheque.TabIndex = 4
        Me.chkGenerar_Cheque.Tag = "generar_cheque"
        '
        'lblBeneficiario
        '
        Me.lblBeneficiario.AutoSize = True
        Me.lblBeneficiario.Location = New System.Drawing.Point(6, 24)
        Me.lblBeneficiario.Name = "lblBeneficiario"
        Me.lblBeneficiario.Size = New System.Drawing.Size(73, 16)
        Me.lblBeneficiario.TabIndex = 0
        Me.lblBeneficiario.Tag = ""
        Me.lblBeneficiario.Text = "&Beneficiario:"
        Me.lblBeneficiario.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBeneficiario
        '
        Me.txtBeneficiario.EditValue = ""
        Me.txtBeneficiario.Location = New System.Drawing.Point(82, 24)
        Me.txtBeneficiario.Name = "txtBeneficiario"
        '
        'txtBeneficiario.Properties
        '
        Me.txtBeneficiario.Properties.MaxLength = 100
        Me.txtBeneficiario.Size = New System.Drawing.Size(446, 20)
        Me.txtBeneficiario.TabIndex = 1
        Me.txtBeneficiario.Tag = "beneficiario"
        '
        'lblCheque
        '
        Me.lblCheque.AutoSize = True
        Me.lblCheque.Location = New System.Drawing.Point(28, 48)
        Me.lblCheque.Name = "lblCheque"
        Me.lblCheque.Size = New System.Drawing.Size(51, 16)
        Me.lblCheque.TabIndex = 2
        Me.lblCheque.Tag = ""
        Me.lblCheque.Text = "&Cheque:"
        Me.lblCheque.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCheque
        '
        Me.clcCheque.EditValue = "0"
        Me.clcCheque.Location = New System.Drawing.Point(82, 48)
        Me.clcCheque.MaxValue = 0
        Me.clcCheque.MinValue = 0
        Me.clcCheque.Name = "clcCheque"
        '
        'clcCheque.Properties
        '
        Me.clcCheque.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCheque.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCheque.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCheque.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCheque.Properties.Enabled = False
        Me.clcCheque.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCheque.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCheque.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcCheque.Size = New System.Drawing.Size(78, 19)
        Me.clcCheque.TabIndex = 3
        Me.clcCheque.Tag = "cheque"
        '
        'chkAbono_A_Cuenta
        '
        Me.chkAbono_A_Cuenta.Location = New System.Drawing.Point(264, 48)
        Me.chkAbono_A_Cuenta.Name = "chkAbono_A_Cuenta"
        '
        'chkAbono_A_Cuenta.Properties
        '
        Me.chkAbono_A_Cuenta.Properties.Caption = "Abono A Cuenta"
        Me.chkAbono_A_Cuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAbono_A_Cuenta.Size = New System.Drawing.Size(192, 19)
        Me.chkAbono_A_Cuenta.TabIndex = 4
        Me.chkAbono_A_Cuenta.Tag = "abono_a_cuenta"
        '
        'lblFormulo
        '
        Me.lblFormulo.AutoSize = True
        Me.lblFormulo.Location = New System.Drawing.Point(24, 24)
        Me.lblFormulo.Name = "lblFormulo"
        Me.lblFormulo.Size = New System.Drawing.Size(54, 16)
        Me.lblFormulo.TabIndex = 0
        Me.lblFormulo.Tag = ""
        Me.lblFormulo.Text = "&Formulo:"
        Me.lblFormulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFormulo
        '
        Me.txtFormulo.EditValue = ""
        Me.txtFormulo.Location = New System.Drawing.Point(80, 24)
        Me.txtFormulo.Name = "txtFormulo"
        '
        'txtFormulo.Properties
        '
        Me.txtFormulo.Properties.MaxLength = 100
        Me.txtFormulo.Size = New System.Drawing.Size(448, 20)
        Me.txtFormulo.TabIndex = 1
        Me.txtFormulo.Tag = "formulo"
        '
        'lblAutorizo
        '
        Me.lblAutorizo.AutoSize = True
        Me.lblAutorizo.Location = New System.Drawing.Point(16, 48)
        Me.lblAutorizo.Name = "lblAutorizo"
        Me.lblAutorizo.Size = New System.Drawing.Size(55, 16)
        Me.lblAutorizo.TabIndex = 2
        Me.lblAutorizo.Tag = ""
        Me.lblAutorizo.Text = "A&utoriz�:"
        Me.lblAutorizo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAutorizo
        '
        Me.txtAutorizo.EditValue = ""
        Me.txtAutorizo.Location = New System.Drawing.Point(80, 48)
        Me.txtAutorizo.Name = "txtAutorizo"
        '
        'txtAutorizo.Properties
        '
        Me.txtAutorizo.Properties.MaxLength = 100
        Me.txtAutorizo.Size = New System.Drawing.Size(448, 20)
        Me.txtAutorizo.TabIndex = 3
        Me.txtAutorizo.Tag = "autorizo"
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(16, 72)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 4
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "Conce&pto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblNombre_Banco)
        Me.GroupBox1.Controls.Add(Me.clcFolio)
        Me.GroupBox1.Controls.Add(Me.lblBanco)
        Me.GroupBox1.Controls.Add(Me.clcBanco)
        Me.GroupBox1.Controls.Add(Me.lblCuentabancaria)
        Me.GroupBox1.Controls.Add(Me.txtCuentabancaria)
        Me.GroupBox1.Controls.Add(Me.lblFolio)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(16, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(272, 115)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'lblNombre_Banco
        '
        Me.lblNombre_Banco.AutoSize = True
        Me.lblNombre_Banco.Location = New System.Drawing.Point(176, 16)
        Me.lblNombre_Banco.Name = "lblNombre_Banco"
        Me.lblNombre_Banco.Size = New System.Drawing.Size(0, 16)
        Me.lblNombre_Banco.TabIndex = 2
        Me.lblNombre_Banco.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lkpConceptoMovimiento)
        Me.GroupBox2.Controls.Add(Me.lblConceptoMovimiento)
        Me.GroupBox2.Controls.Add(Me.lblImporte)
        Me.GroupBox2.Controls.Add(Me.lblFactura)
        Me.GroupBox2.Controls.Add(Me.lblTipo)
        Me.GroupBox2.Controls.Add(Me.txtFactura)
        Me.GroupBox2.Controls.Add(Me.clcImporte)
        Me.GroupBox2.Controls.Add(Me.cboTipo)
        Me.GroupBox2.Location = New System.Drawing.Point(296, 40)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(248, 115)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'lkpConceptoMovimiento
        '
        Me.lkpConceptoMovimiento.AllowAdd = False
        Me.lkpConceptoMovimiento.AutoReaload = False
        Me.lkpConceptoMovimiento.DataSource = Nothing
        Me.lkpConceptoMovimiento.DefaultSearchField = ""
        Me.lkpConceptoMovimiento.DisplayMember = "descripcion"
        Me.lkpConceptoMovimiento.EditValue = Nothing
        Me.lkpConceptoMovimiento.Filtered = False
        Me.lkpConceptoMovimiento.InitValue = Nothing
        Me.lkpConceptoMovimiento.Location = New System.Drawing.Point(80, 40)
        Me.lkpConceptoMovimiento.MultiSelect = False
        Me.lkpConceptoMovimiento.Name = "lkpConceptoMovimiento"
        Me.lkpConceptoMovimiento.NullText = ""
        Me.lkpConceptoMovimiento.PopupWidth = CType(400, Long)
        Me.lkpConceptoMovimiento.Required = False
        Me.lkpConceptoMovimiento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoMovimiento.SearchMember = ""
        Me.lkpConceptoMovimiento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoMovimiento.SelectAll = False
        Me.lkpConceptoMovimiento.Size = New System.Drawing.Size(144, 20)
        Me.lkpConceptoMovimiento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoMovimiento.TabIndex = 3
        Me.lkpConceptoMovimiento.Tag = "concepto_movimiento"
        Me.lkpConceptoMovimiento.ToolTip = Nothing
        Me.lkpConceptoMovimiento.ValueMember = "concepto"
        '
        'lblConceptoMovimiento
        '
        Me.lblConceptoMovimiento.AutoSize = True
        Me.lblConceptoMovimiento.Location = New System.Drawing.Point(8, 42)
        Me.lblConceptoMovimiento.Name = "lblConceptoMovimiento"
        Me.lblConceptoMovimiento.Size = New System.Drawing.Size(60, 16)
        Me.lblConceptoMovimiento.TabIndex = 2
        Me.lblConceptoMovimiento.Text = "Concepto:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkAbono_A_Cuenta)
        Me.GroupBox3.Controls.Add(Me.lblBeneficiario)
        Me.GroupBox3.Controls.Add(Me.txtBeneficiario)
        Me.GroupBox3.Controls.Add(Me.lblCheque)
        Me.GroupBox3.Controls.Add(Me.clcCheque)
        Me.GroupBox3.Location = New System.Drawing.Point(16, 176)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(536, 80)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtconcepto)
        Me.GroupBox4.Controls.Add(Me.lblFormulo)
        Me.GroupBox4.Controls.Add(Me.txtFormulo)
        Me.GroupBox4.Controls.Add(Me.lblAutorizo)
        Me.GroupBox4.Controls.Add(Me.txtAutorizo)
        Me.GroupBox4.Controls.Add(Me.lblConcepto)
        Me.GroupBox4.Location = New System.Drawing.Point(16, 272)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(536, 128)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Datos Generales"
        '
        'txtconcepto
        '
        Me.txtconcepto.EditValue = ""
        Me.txtconcepto.Location = New System.Drawing.Point(80, 72)
        Me.txtconcepto.Name = "txtconcepto"
        Me.txtconcepto.Size = New System.Drawing.Size(448, 40)
        Me.txtconcepto.TabIndex = 5
        Me.txtconcepto.Tag = "concepto"
        '
        'picEliminado
        '
        Me.picEliminado.BackColor = System.Drawing.Color.Transparent
        Me.picEliminado.Image = CType(resources.GetObject("picEliminado.Image"), System.Drawing.Image)
        Me.picEliminado.Location = New System.Drawing.Point(192, 16)
        Me.picEliminado.Name = "picEliminado"
        Me.picEliminado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEliminado.TabIndex = 61
        Me.picEliminado.TabStop = False
        Me.picEliminado.Visible = False
        '
        'btnImprimircheque
        '
        Me.btnImprimircheque.Text = "Imprimir Cheque"
        Me.btnImprimircheque.ToolTipText = "Impresion del Cheque"
        '
        'lblPagoVariasFacturas
        '
        Me.lblPagoVariasFacturas.AutoSize = True
        Me.lblPagoVariasFacturas.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPagoVariasFacturas.Location = New System.Drawing.Point(24, 16)
        Me.lblPagoVariasFacturas.Name = "lblPagoVariasFacturas"
        Me.lblPagoVariasFacturas.Size = New System.Drawing.Size(163, 18)
        Me.lblPagoVariasFacturas.TabIndex = 62
        Me.lblPagoVariasFacturas.Text = "Pago a Varias Facturas"
        Me.lblPagoVariasFacturas.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.picEliminado)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.lblFecha)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.chkGenerar_Cheque)
        Me.Panel1.Controls.Add(Me.GroupBox4)
        Me.Panel1.Controls.Add(Me.dteFecha)
        Me.Panel1.Controls.Add(Me.lblPagoVariasFacturas)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Location = New System.Drawing.Point(123, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(560, 408)
        Me.Panel1.TabIndex = 63
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvrDatosGenerales, Me.nvrPolizaCheque})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 28)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(120, 420)
        Me.NavBarControl1.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.NavBarControl1.TabIndex = 96
        Me.NavBarControl1.Text = "Entrada al Almacen"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrPolizaCheque)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'nvrDatosGenerales
        '
        Me.nvrDatosGenerales.Caption = "Datos Generales"
        Me.nvrDatosGenerales.LargeImage = CType(resources.GetObject("nvrDatosGenerales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosGenerales.LargeImageIndex = 0
        Me.nvrDatosGenerales.Name = "nvrDatosGenerales"
        '
        'nvrPolizaCheque
        '
        Me.nvrPolizaCheque.Caption = "P�liza Cheque"
        Me.nvrPolizaCheque.Name = "nvrPolizaCheque"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.tmaPolizaCheque)
        Me.Panel2.Controls.Add(Me.grDatosPoliza)
        Me.Panel2.Location = New System.Drawing.Point(123, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(560, 408)
        Me.Panel2.TabIndex = 97
        '
        'tmaPolizaCheque
        '
        Me.tmaPolizaCheque.BackColor = System.Drawing.Color.White
        Me.tmaPolizaCheque.CanDelete = True
        Me.tmaPolizaCheque.CanInsert = True
        Me.tmaPolizaCheque.CanUpdate = True
        Me.tmaPolizaCheque.Grid = Me.grDatosPoliza
        Me.tmaPolizaCheque.Location = New System.Drawing.Point(8, 8)
        Me.tmaPolizaCheque.Name = "tmaPolizaCheque"
        Me.tmaPolizaCheque.popTINGrid = Nothing
        Me.tmaPolizaCheque.Size = New System.Drawing.Size(544, 23)
        Me.tmaPolizaCheque.TabIndex = 15
        Me.tmaPolizaCheque.TabStop = False
        Me.tmaPolizaCheque.Title = ""
        Me.tmaPolizaCheque.UpdateTitle = "un Registro"
        '
        'grDatosPoliza
        '
        '
        'grDatosPoliza.EmbeddedNavigator
        '
        Me.grDatosPoliza.EmbeddedNavigator.Name = ""
        Me.grDatosPoliza.Location = New System.Drawing.Point(8, 32)
        Me.grDatosPoliza.MainView = Me.grvDatosPoliza
        Me.grDatosPoliza.Name = "grDatosPoliza"
        Me.grDatosPoliza.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepLkpCuentasContables, Me.RepClcImporte, Me.RepTxtConcepto})
        Me.grDatosPoliza.Size = New System.Drawing.Size(544, 368)
        Me.grDatosPoliza.Styles.AddReplace("Style2", New DevExpress.Utils.ViewStyleEx("Style2", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDatosPoliza.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDatosPoliza.TabIndex = 14
        Me.grDatosPoliza.TabStop = False
        Me.grDatosPoliza.Text = "GridControl1"
        '
        'grvDatosPoliza
        '
        Me.grvDatosPoliza.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcConsecutivo, Me.grcCuentaContable, Me.grcDescripcion, Me.grcCargo, Me.grcAbono})
        Me.grvDatosPoliza.GridControl = Me.grDatosPoliza
        Me.grvDatosPoliza.Name = "grvDatosPoliza"
        Me.grvDatosPoliza.OptionsBehavior.Editable = False
        Me.grvDatosPoliza.OptionsView.ShowFooter = True
        Me.grvDatosPoliza.OptionsView.ShowGroupPanel = False
        '
        'grcConsecutivo
        '
        Me.grcConsecutivo.Caption = "Consecutivo"
        Me.grcConsecutivo.FieldName = "partida"
        Me.grcConsecutivo.Name = "grcConsecutivo"
        Me.grcConsecutivo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCuentaContable
        '
        Me.grcCuentaContable.Caption = "Cuenta Contable"
        Me.grcCuentaContable.FieldName = "cuenta_contable"
        Me.grcCuentaContable.HeaderStyleName = "Style1"
        Me.grcCuentaContable.Name = "grcCuentaContable"
        Me.grcCuentaContable.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCuentaContable.VisibleIndex = 0
        Me.grcCuentaContable.Width = 140
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Concepto"
        Me.grcDescripcion.ColumnEdit = Me.RepTxtConcepto
        Me.grcDescripcion.FieldName = "concepto"
        Me.grcDescripcion.HeaderStyleName = "Style1"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 1
        Me.grcDescripcion.Width = 216
        '
        'RepTxtConcepto
        '
        Me.RepTxtConcepto.AutoHeight = False
        Me.RepTxtConcepto.MaxLength = 120
        Me.RepTxtConcepto.Name = "RepTxtConcepto"
        '
        'grcCargo
        '
        Me.grcCargo.Caption = "Cargo"
        Me.grcCargo.ColumnEdit = Me.RepClcImporte
        Me.grcCargo.FieldName = "cargo"
        Me.grcCargo.HeaderStyleName = "Style1"
        Me.grcCargo.Name = "grcCargo"
        Me.grcCargo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCargo.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcCargo.VisibleIndex = 2
        Me.grcCargo.Width = 110
        '
        'RepClcImporte
        '
        Me.RepClcImporte.AutoHeight = False
        Me.RepClcImporte.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepClcImporte.DisplayFormat.FormatString = "c2"
        Me.RepClcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepClcImporte.EditFormat.FormatString = "n2"
        Me.RepClcImporte.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepClcImporte.Name = "RepClcImporte"
        Me.RepClcImporte.Precision = 4
        '
        'grcAbono
        '
        Me.grcAbono.Caption = "Abono"
        Me.grcAbono.ColumnEdit = Me.RepClcImporte
        Me.grcAbono.FieldName = "abono"
        Me.grcAbono.HeaderStyleName = "Style1"
        Me.grcAbono.Name = "grcAbono"
        Me.grcAbono.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcAbono.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcAbono.VisibleIndex = 3
        Me.grcAbono.Width = 96
        '
        'RepLkpCuentasContables
        '
        Me.RepLkpCuentasContables.AutoHeight = False
        Me.RepLkpCuentasContables.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepLkpCuentasContables.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("cuentacontable", "Cuenta", 40, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Center), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripci�n", 50)})
        Me.RepLkpCuentasContables.DisplayMember = "cuentacontable"
        Me.RepLkpCuentasContables.Name = "RepLkpCuentasContables"
        Me.RepLkpCuentasContables.NullText = ""
        Me.RepLkpCuentasContables.PopupWidth = 200
        Me.RepLkpCuentasContables.ValueMember = "cuentacontable"
        '
        'frmMovimientosChequera
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(690, 448)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "frmMovimientosChequera"
        Me.Controls.SetChildIndex(Me.Panel2, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCuentabancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkGenerar_Cheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBeneficiario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbono_A_Cuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFormulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutorizo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.txtconcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.grDatosPoliza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDatosPoliza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepTxtConcepto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepClcImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepLkpCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oMovimientosChequera As VillarrealBusiness.clsMovimientosChequera
    Private oChequeras As VillarrealBusiness.clsChequeras
    Private oReportes As VillarrealBusiness.Reportes
    Private oConceptoMovimientosChequera As VillarrealBusiness.clsConceptosMovimientosChequera
    Private oPolizasCheques As VillarrealBusiness.clsPolizasCheques

    ReadOnly Property FolioChequera(ByVal cuenta_bancaria As String) As Long
        Get
            Return CType(oChequeras.DespliegaDatos(cuenta_bancaria).Value, DataSet).Tables(0).Rows(0).Item("folio") + 1
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmMovimientosChequera_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmMovimientosChequera_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmMovimientosChequera_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmMovimientosChequera_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Me.ObtenerFolioPolizaCheque(Response)
                Response = oMovimientosChequera.Insertar(Me.DataSource, dSaldo_actual, ActualizarCheque, Me.Folio_Poliza_Cheque)

            Case Actions.Update
                Me.ObtenerFolioPolizaCheque(Response)
                Response = oMovimientosChequera.Actualizar(Me.DataSource, dImporte_anterior, dSaldo_actual, ActualizarCheque, Me.Folio_Poliza_Cheque)

            Case Actions.Delete
                Response = oMovimientosChequera.Eliminar(clcBanco.Value, txtCuentabancaria.Text, clcFolio.Value, dSaldo_actual)
        End Select

        CType(OwnerForm, brwMovimientosChequera).ObtenerSaldo(dSaldo_actual)
    End Sub
    Private Sub frmMovimientosChequera_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oMovimientosChequera.DespliegaDatos(OwnerForm.Value("banco"), OwnerForm.Value("cuentabancaria"), OwnerForm.Value("folio"))
        Dim oDataSet As DataSet

        If Not Response.ErrorFound Then

            oDataSet = Response.Value
            Me.DataSource = oDataSet

            tbrTools.Buttons(0).Enabled = True
            'Obtengo el Folio del pago a proveedores si dicho movimiento fue generado desde Pago a Varias Factuas
            FolioPagoProveedores = oDataSet.Tables(0).Rows(0).Item("folio_pago_proveedores")

            ' OBTENGO EL FOLIO DE LA POLIZA 
            Folio_Poliza_Cheque = oDataSet.Tables(0).Rows(0).Item("folio_poliza_cheque")

            If Me.clcCheque.EditValue > 0 Or FolioPagoProveedores > 0 Then
                Me.tbrTools.Buttons(0).Enabled = False
                Me.EnabledEdit(False)
            End If

            If oDataSet.Tables(0).Rows.Count > 0 Then
                If oDataSet.Tables(0).Rows(0).Item("cancelado") = True Then
                    Me.tbrTools.Buttons(0).Enabled = False
                    Me.EnabledEdit(False)
                    Me.picEliminado.Visible = True
                    Me.btnImprimircheque.Enabled = False
                    Me.btnImprimircheque.Visible = False
                End If
            End If
            dImporte_anterior = Me.clcImporte.Value
        End If

        If Action = Actions.Update Then
            Me.cboTipo.Enabled = False
        End If

        If Me.FolioPagoProveedores > 0 Then
            Me.tbrTools.Buttons(0).Enabled = False
            Me.lblPagoVariasFacturas.Visible = True
        End If

        'Obtengo los datos de la poliza del cheque
        Response = Me.oPolizasCheques.DespliegaDatos(Me.Folio_Poliza_Cheque)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaPolizaCheque.DataSource = oDataSet
        End If
    End Sub
    Private Sub frmMovimientosChequera_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oMovimientosChequera = New VillarrealBusiness.clsMovimientosChequera
        oConceptoMovimientosChequera = New VillarrealBusiness.clsConceptosMovimientosChequera
        Me.clcBanco.EditValue = CType(OwnerForm, brwMovimientosChequera).Banco
        Me.txtCuentabancaria.Text = CType(OwnerForm, brwMovimientosChequera).CuentaBancaria
        oChequeras = New VillarrealBusiness.clsChequeras
        oPolizasCheques = New VillarrealBusiness.clsPolizasCheques

        Me.chkGenerar_Cheque.Checked = False
        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)

        With Me.tmaPolizaCheque
            .UpdateTitle = "Un Registro"
            .UpdateForm = New Comunes.frmPolizaChequeDetalle
            '.AddColumn("partida")
            .AddColumn("cuenta_contable", "System.String")
            .AddColumn("concepto", "System.String")
            .AddColumn("cargo", "System.Double")
            .AddColumn("abono", "System.Double")
        End With

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
                Me.btnImprimircheque.Enabled = False
                Me.btnImprimircheque.Visible = False
                Me.EnabledEdit(False)
        End Select
    End Sub
    Private Sub frmMovimientosChequera_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim intControl As Long

        Response = oMovimientosChequera.Validacion(Action, Me.lkpConceptoMovimiento.EditValue, Me.clcImporte.EditValue, Me.chkGenerar_Cheque.Checked, Me.txtBeneficiario.Text, intControl)
        Select Case intControl
            Case 1
                Me.lkpConceptoMovimiento.Focus()
            Case 2
                Me.clcImporte.Focus()
            Case 3
                Me.txtBeneficiario.Focus()
        End Select
    End Sub

    Private Sub frmMovimientosChequera_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        '  Tipo Pago
        '====================
        ' el 1 son movimientos pagar
        ' el 2 son pagos diversos
        ' el 3 son movimientos chequera

        Me.tmaPolizaCheque.MoveFirst()

        Do While Not Me.tmaPolizaCheque.EOF
            Select Case tmaPolizaCheque.CurrentAction
                Case Actions.Insert
                    Response = Me.oPolizasCheques.Insertar(Me.tmaPolizaCheque.SelectedRow, Me.Folio_Poliza_Cheque, 2)
                Case Actions.Update
                    Response = Me.oPolizasCheques.Actualizar(Me.tmaPolizaCheque.SelectedRow, Me.Folio_Poliza_Cheque)
                Case Actions.Delete
                    Response = Me.oPolizasCheques.Eliminar(Me.Folio_Poliza_Cheque, Me.tmaPolizaCheque.Item("partida"))
            End Select
            If Response.ErrorFound Then Exit Sub
            tmaPolizaCheque.MoveNext()
        Loop
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub chkGenerar_Cheque_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGenerar_Cheque.CheckedChanged
        Me.txtBeneficiario.Enabled = Me.chkGenerar_Cheque.Checked
        Me.chkAbono_A_Cuenta.Enabled = Me.chkGenerar_Cheque.Checked


        If Me.chkGenerar_Cheque.Checked = False Then
            Me.txtBeneficiario.Text = ""
        End If
    End Sub

    Public Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.btnImprimircheque Then
            Dim response As Events

            'Verificar que el tipo sea Cheque para generarlo
            'If Me.cboTipoPago.EditValue <> "C" Then
            '    response = New Events
            '    response.Message = "No se Puede Imprimir Cheque, el Tipo de Pago debe ser Cheque"
            '    response.ShowMessage()
            '    Exit Sub
            'End If


            'Verificar Poliza Cuadrada y Completa para la Generacion del Cheque
            If Not (Me.PolizaCuadrada And Me.PolizaCompleta(clcImporte.Value)) Then
                response = New Events
                response.Message = "La Poliza No esta Cuadrada o No esta Completa"
                response.ShowMessage()
                Exit Sub
            End If


            Me.chkGenerar_Cheque.Checked = True
            Dim intControl As Long
            response = oMovimientosChequera.Validacion(Action, Me.lkpConceptoMovimiento.EditValue, Me.clcImporte.EditValue, Me.chkGenerar_Cheque.Checked, Me.txtBeneficiario.Text, intControl)
            Select Case intControl
                Case 1
                    Me.lkpConceptoMovimiento.Focus()
                Case 2
                    Me.clcImporte.Focus()
                Case 3
                    Me.txtBeneficiario.Focus()
            End Select

            If response.ErrorFound Then
                response.ShowMessage()
                Exit Sub
            End If
            'Abro la Transaccion para obtener el folio del cheque
            frmMovimientosChequera_BeginUpdate()

            If Me.clcCheque.Value = 0 Then
                'asigno el valor al control para que lo actualice
                Me.clcCheque.Value = Me.FolioChequera(Me.txtCuentabancaria.Text)
                ActualizarCheque = False
            Else
                ActualizarCheque = True
            End If
            'Actualizo los Valores
            frmMovimientosChequera_Accept(response)
            frmMovimientosChequera_Detail(response)

            If Not response.ErrorFound Then
                'Termino Satisfactoriamente la transaccion
                frmMovimientosChequera_EndUpdate()
                'Imprimo el cheque
                ImprimirCheque(Me.txtCuentabancaria.Text, Me.clcCheque.Value)

                'Actualizo el salod en el tinGrid 
                CType(OwnerForm, brwMovimientosChequera).ObtenerSaldo(dSaldo_actual)
                Me.Close()
            Else
                'Aborto o Cancelo la Transaccion
                frmMovimientosChequera_AbortUpdate()
                Me.clcCheque.EditValue = 0
                response.ShowMessage()
            End If
        End If
    End Sub

    Private Sub cboTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipo.SelectedIndexChanged
        If CType(Me.cboTipo.Value, String) = "A" Then
            Me.chkGenerar_Cheque.Enabled = False
            Me.chkGenerar_Cheque.Checked = False
            Me.tbrTools.Buttons(2).Enabled = False

        Else
            Me.chkGenerar_Cheque.Enabled = True
            Me.tbrTools.Buttons(2).Enabled = True
        End If
    End Sub

    Private Sub nvrDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosGenerales.LinkPressed
        Me.Panel1.BringToFront()
    End Sub
    Private Sub nvrPolizaCheque_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrPolizaCheque.LinkPressed
        Me.Panel2.BringToFront()
    End Sub

#Region "Lookup Concepto Movimiento"
    Private Sub lkpConceptoMovimiento_InitData(ByVal Value As Object) Handles lkpConceptoMovimiento.InitData
        Dim response As Events

        response = oConceptoMovimientosChequera.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoMovimiento.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub


    Private Sub lkpConceptoMovimiento_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoMovimiento.LoadData
        Dim response As Events

        response = oConceptoMovimientosChequera.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoMovimiento.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpConceptoMovimiento_Format() Handles lkpConceptoMovimiento.Format
        for_conceptos_movimientos_chequera_grl(Me.lkpConceptoMovimiento)
    End Sub
#End Region

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub ImprimirCheque(ByVal cuentabancaria As String, ByVal folio As Integer)
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            response = oReportes.ImprimeChequeMovimientosChequera(Me.clcBanco.EditValue, cuentabancaria, folio)
            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Cheque no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptCheque

                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Cheques", oReport)

                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
    Private Function ObtenerFolioPolizaCheque(ByRef response As Dipros.Utils.Events) As Long

        If Me.tmaPolizaCheque.View.RowCount > 0 And Folio_Poliza_Cheque = 0 Then
            response = Me.oPolizasCheques.ObtenerSiguienteFolio
            If Not response.ErrorFound Then
                Me.Folio_Poliza_Cheque = response.Value + 1
            End If
        End If
    End Function
    Public Function PolizaCuadrada() As Boolean
        Dim abono As Double = 0
        Dim cargo As Double = 0

        abono = CType(Me.grcAbono.SummaryItem.SummaryValue, Double)
        cargo = CType(Me.grcCargo.SummaryItem.SummaryValue, Double)

        If cargo = 0 And abono = 0 Then
            Me.tmaPolizaCheque.Title = "Sin Registros"
            Return False
        Else
            If cargo <> abono Then

                Me.tmaPolizaCheque.Title = "Descuadrada"
                Return False
            Else

                Me.tmaPolizaCheque.Title = "Cuadrada"
                Return True
            End If
        End If
    End Function
    Private Function PolizaCompleta(ByVal Total As Double) As Boolean
        Dim abono As Double = 0
        Dim cargo As Double = 0

        abono = CType(Me.grcAbono.SummaryItem.SummaryValue, Double)
        cargo = CType(Me.grcCargo.SummaryItem.SummaryValue, Double)

        If abono > 0 And cargo > 0 And (abono = cargo) And abono = Total Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region
End Class
