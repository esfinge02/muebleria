Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class brwMovimientosPagar
    Inherits Dipros.Windows.frmTINGridNet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents clcSaldo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents btnAsignarAnticipos As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblSaldo = New System.Windows.Forms.Label
        Me.clcSaldo = New Dipros.Editors.TINCalcEdit
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.btnAsignarAnticipos = New DevExpress.XtraEditors.SimpleButton
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FooterPanel.SuspendLayout()
        Me.FilterPanel.SuspendLayout()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        Me.tbrExtended.Size = New System.Drawing.Size(688, 22)
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'FooterPanel
        '
        Me.FooterPanel.Controls.Add(Me.btnAsignarAnticipos)
        Me.FooterPanel.Location = New System.Drawing.Point(0, 394)
        Me.FooterPanel.Name = "FooterPanel"
        Me.FooterPanel.Size = New System.Drawing.Size(800, 36)
        Me.FooterPanel.Controls.SetChildIndex(Me.btnAsignarAnticipos, 0)
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.lblProveedor)
        Me.FilterPanel.Controls.Add(Me.lkpProveedor)
        Me.FilterPanel.Controls.Add(Me.clcSaldo)
        Me.FilterPanel.Controls.Add(Me.lblSaldo)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(800, 37)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblSaldo, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.clcSaldo, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblProveedor, 0)
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldo.Location = New System.Drawing.Point(656, 8)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(38, 16)
        Me.lblSaldo.TabIndex = 1
        Me.lblSaldo.Text = "Saldo:"
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = "0"
        Me.clcSaldo.Location = New System.Drawing.Point(704, 7)
        Me.clcSaldo.MaxValue = 0
        Me.clcSaldo.MinValue = 0
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatString = "$###,##0.00"
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSaldo.Properties.Precision = 2
        Me.clcSaldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldo.Size = New System.Drawing.Size(80, 19)
        Me.clcSaldo.TabIndex = 56
        Me.clcSaldo.Tag = "saldo"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(16, 8)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(60, 16)
        Me.lblProveedor.TabIndex = 57
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "Prov&eedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = True
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(80, 6)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(380, Long)
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(296, 20)
        Me.lkpProveedor.TabIndex = 58
        Me.lkpProveedor.Tag = "Proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "Proveedor"
        '
        'btnAsignarAnticipos
        '
        Me.btnAsignarAnticipos.Location = New System.Drawing.Point(656, 7)
        Me.btnAsignarAnticipos.Name = "btnAsignarAnticipos"
        Me.btnAsignarAnticipos.Size = New System.Drawing.Size(128, 23)
        Me.btnAsignarAnticipos.TabIndex = 7
        Me.btnAsignarAnticipos.Text = "&Asignar Anticipos"
        '
        'brwMovimientosPagar
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(800, 430)
        Me.Name = "brwMovimientosPagar"
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FooterPanel.ResumeLayout(False)
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oProveedores As New VillarrealBusiness.clsProveedores
    Private oMovimientosPagar As New VillarrealBusiness.clsMovimientosPagar
    Private oConceptosCxp As New VillarrealBusiness.clsConceptosCxp
    Dim sConcepto As String = ""
    Dim sConceptoReferencia As String = ""
    Dim lFolioReferencia As Long = 0
    Dim lEntrada As Long = 0

    ' Private oVariables As New VillarrealBusiness.clsVariables

    Friend ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property
    Friend ReadOnly Property NombreProveedor() As String
        Get
            Return IIf(Me.lkpProveedor.EditValue Is Nothing, "", CStr(Me.lkpProveedor.GetValue("nombre")))
        End Get
    End Property

    Friend Property Concepto() As String
        Get
            Return sConcepto 'Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
        Set(ByVal Value As String)
            sConcepto = value
        End Set
    End Property
    Friend Property ConceptoReferencia() As String
        Get
            Return sConceptoReferencia 'Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
        Set(ByVal Value As String)
            sConceptoReferencia = Value
        End Set
    End Property
    Friend Property FolioReferencia() As Long
        Get
            Return lFolioReferencia
        End Get
        Set(ByVal Value As Long)
            lFolioReferencia = Value
        End Set
    End Property
    Friend Property Entrada() As Long
        Get
            Return lEntrada
        End Get
        Set(ByVal Value As Long)
            lEntrada = Value
        End Set
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub brwMovimientosPagar_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        Response = oMovimientosPagar.Listado(Proveedor)
    End Sub
    Private Sub brwMovimientosPagar_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMovimientosPagar.Validacion(Actions.None, Proveedor)
    End Sub
    Private Sub brwMovimientosPagar_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        ActuazalizaSaldo(Proveedor)
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpProveedor_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpProveedor.EditValueChanged
        If Me.lkpProveedor.EditValue Is Nothing Then Exit Sub

        ActuazalizaSaldo(Proveedor)
        Me.Reload(True)
    End Sub

    Private Sub btnAsignarAnticipos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsignarAnticipos.Click
        Dim concepto As String
        Dim folio As Integer
        Dim Tipo As String
        Dim TipoAbono As String

        'si no existen Movimientos
        If Me.View.RowCount = 0 Then Exit Sub

        folio = Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("folio"))

        'Si el tipo de concepto seleccionado es un Abono
        concepto = Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("concepto"))
        Tipo = CType(oConceptosCxp.DespliegaDatos(concepto).Value, DataSet).Tables(0).Rows(0).Item("tipo")
        Me.Concepto = concepto
        If Tipo.ToUpper = "A" Or Tipo.ToUpper = "P" Then

            If AbonoAsignado(concepto, folio) Then

                If ShowMessage(MessageType.MsgQuestion, "El movimiento ya ha sido Asignado. �Desea modificar su asignaci�n?") = Answer.MsgYes Then

                    AsignarAbono(folio)
                    'Dim frmModal As New frmAsignarAbonos
                    'With frmModal
                    '    .Text = "Asignaci�n de Abonos"
                    '    .Proveedor = Me.Proveedor
                    '    .Concepto = Me.Concepto
                    '    .Folio = folio 'Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("folio"))
                    '    .ConceptoReferencia = Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("concepto_referencia"))
                    '    .FolioReferencia = Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("folio_referencia"))
                    '    .OwnerForm = Me
                    '    .MdiParent = Me.MdiParent
                    '    .Show()
                    'End With
                    'Me.Enabled = False
                Else
                    Exit Sub
                End If

            Else
                AsignarAbono(folio)
                'Dim frmModal As New frmAsignarAbonos
                'With frmModal
                '    .Text = "Asignaci�n de Abonos"
                '.Proveedor = Me.Proveedor
                '.Concepto = Me.Concepto
                '    .Folio = folio 'Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("folio"))
                '    .OwnerForm = Me
                '    .MdiParent = Me.MdiParent
                '    .Show()
                'End With
                'Me.Enabled = False
            End If

        Else
            Exit Sub
        End If

        ''si el inventario esta en estatus de Aplicado
        'If Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("status")) = "A" Then
        '    ShowMessage(MessageType.MsgInformation, "El ajuste al inventario ya fue aplicado", "Inventario F�sico", Nothing, False)
        '    Exit Sub
        'End If

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Function AbonoAsignado(ByVal concepto As String, ByVal folio As Integer) As Boolean
        Dim Response As Dipros.Utils.Events
        Dim oDataset As DataSet
        'Dim ConceptoReferencia As String
        'Dim FolioReferencia As Integer
        'Dim Entrada As Integer

        Try
            Response = Me.oMovimientosPagar.DespliegaDatos(concepto, folio)
            If Response.ErrorFound = False Then
                oDataset = Response.Value
                ConceptoReferencia = oDataset.Tables(0).Rows(0).Item("concepto_referencia")
                FolioReferencia = oDataset.Tables(0).Rows(0).Item("folio_referencia")
                Entrada = oDataset.Tables(0).Rows(0).Item("entrada")
            End If

            If ConceptoReferencia.Length > 0 And FolioReferencia > 0 Then ' And Entrada > 0 Then
                AbonoAsignado = True
            Else
                AbonoAsignado = False
            End If
        Catch ex As Exception
            'AbonoAsignado = False
        End Try

    End Function

    Private Sub AsignarAbono(ByVal folio As Integer)
        Dim frmModal As New frmAsignarAbonos
        With frmModal
            .Text = "Asignaci�n de Abonos"
            '.Proveedor = Me.Proveedor
            '.Concepto = Me.Concepto
            .Folio = folio 'Me.View.GetRowCellValue(Me.View.FocusedRowHandle, Me.View.Columns("folio"))
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Show()
        End With
        Me.Enabled = False


    End Sub

    Private Sub ActuazalizaSaldo(ByVal proveedor As Long)
        If proveedor > 0 Then
            Dim Response As New Events
            Response = oProveedores.DespliegaDatos(proveedor)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.clcSaldo.Value = oDataSet.Tables(0).Rows(0).Item("saldo")
                oDataSet = Nothing
            End If
            Response = Nothing
        Else
            Me.clcSaldo.Value = 0
        End If
    End Sub

#End Region



   
End Class
