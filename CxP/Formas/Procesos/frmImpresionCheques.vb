Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmImpresionCheques
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpCuentaBancaria As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCuenta_Bancaria As System.Windows.Forms.Label
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents lkpBanco As Dipros.Editors.TINMultiLookup
    Friend WithEvents cboTipoMovimiento As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grvMovimientos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClaveBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCuentaBancaria As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBeneficiario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAbonoaCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoMovimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImprimir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepChkImprimir As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents tbrSelecccion As System.Windows.Forms.ToolBar
    Friend WithEvents optTodos As System.Windows.Forms.ToolBarButton
    Friend WithEvents optNinguno As System.Windows.Forms.ToolBarButton
    Friend WithEvents grMovimientos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClaveTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblTotalImporte As System.Windows.Forms.Label
    Friend WithEvents labelTotal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmImpresionCheques))
        Me.cboTipoMovimiento = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.grMovimientos = New DevExpress.XtraGrid.GridControl
        Me.grvMovimientos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcImprimir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepChkImprimir = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcClaveBanco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreBanco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCuentaBancaria = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBeneficiario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAbonoaCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoMovimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClaveTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lkpCuentaBancaria = New Dipros.Editors.TINMultiLookup
        Me.lblCuenta_Bancaria = New System.Windows.Forms.Label
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lkpBanco = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbrSelecccion = New System.Windows.Forms.ToolBar
        Me.optTodos = New System.Windows.Forms.ToolBarButton
        Me.optNinguno = New System.Windows.Forms.ToolBarButton
        Me.lblTotalImporte = New System.Windows.Forms.Label
        Me.labelTotal = New System.Windows.Forms.Label
        CType(Me.cboTipoMovimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepChkImprimir, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3340, 28)
        '
        'cboTipoMovimiento
        '
        Me.cboTipoMovimiento.EditValue = 0
        Me.cboTipoMovimiento.Location = New System.Drawing.Point(544, 40)
        Me.cboTipoMovimiento.Name = "cboTipoMovimiento"
        '
        'cboTipoMovimiento.Properties
        '
        Me.cboTipoMovimiento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoMovimiento.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todos", 0, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Movimientos de Chequera", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Movimientos a Pagar", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Acreedores Diversos", 3, -1)})
        Me.cboTipoMovimiento.Size = New System.Drawing.Size(200, 22)
        Me.cboTipoMovimiento.TabIndex = 5
        '
        'grMovimientos
        '
        Me.grMovimientos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grMovimientos.EmbeddedNavigator
        '
        Me.grMovimientos.EmbeddedNavigator.Name = ""
        Me.grMovimientos.Location = New System.Drawing.Point(12, 121)
        Me.grMovimientos.MainView = Me.grvMovimientos
        Me.grMovimientos.Name = "grMovimientos"
        Me.grMovimientos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepChkImprimir})
        Me.grMovimientos.Size = New System.Drawing.Size(731, 295)
        Me.grMovimientos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientos.TabIndex = 7
        Me.grMovimientos.Text = "GridControl1"
        '
        'grvMovimientos
        '
        Me.grvMovimientos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcImprimir, Me.grcClaveBanco, Me.grcNombreBanco, Me.grcCuentaBancaria, Me.grcBeneficiario, Me.grcConcepto, Me.grcImporte, Me.grcAbonoaCuenta, Me.grcTipoMovimiento, Me.grcFolio, Me.grcClaveTipo})
        Me.grvMovimientos.GridControl = Me.grMovimientos
        Me.grvMovimientos.Name = "grvMovimientos"
        Me.grvMovimientos.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvMovimientos.OptionsView.ShowGroupPanel = False
        Me.grvMovimientos.OptionsView.ShowIndicator = False
        '
        'grcImprimir
        '
        Me.grcImprimir.Caption = "Imprimir"
        Me.grcImprimir.ColumnEdit = Me.RepChkImprimir
        Me.grcImprimir.FieldName = "imprimir"
        Me.grcImprimir.HeaderStyleName = "Style1"
        Me.grcImprimir.Name = "grcImprimir"
        Me.grcImprimir.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImprimir.VisibleIndex = 0
        Me.grcImprimir.Width = 60
        '
        'RepChkImprimir
        '
        Me.RepChkImprimir.Name = "RepChkImprimir"
        '
        'grcClaveBanco
        '
        Me.grcClaveBanco.Caption = "Clave Banco"
        Me.grcClaveBanco.FieldName = "banco"
        Me.grcClaveBanco.Name = "grcClaveBanco"
        Me.grcClaveBanco.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombreBanco
        '
        Me.grcNombreBanco.Caption = "Banco"
        Me.grcNombreBanco.FieldName = "nombre_banco"
        Me.grcNombreBanco.Name = "grcNombreBanco"
        Me.grcNombreBanco.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCuentaBancaria
        '
        Me.grcCuentaBancaria.Caption = "Cuenta Bancaria"
        Me.grcCuentaBancaria.FieldName = "cuentabancaria"
        Me.grcCuentaBancaria.Name = "grcCuentaBancaria"
        Me.grcCuentaBancaria.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcBeneficiario
        '
        Me.grcBeneficiario.Caption = "Beneficiario"
        Me.grcBeneficiario.FieldName = "beneficiario"
        Me.grcBeneficiario.Name = "grcBeneficiario"
        Me.grcBeneficiario.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBeneficiario.VisibleIndex = 3
        Me.grcBeneficiario.Width = 280
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.Width = 166
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 5
        Me.grcImporte.Width = 100
        '
        'grcAbonoaCuenta
        '
        Me.grcAbonoaCuenta.Caption = "Abono a Cuenta"
        Me.grcAbonoaCuenta.ColumnEdit = Me.RepChkImprimir
        Me.grcAbonoaCuenta.FieldName = "abono_a_cuenta"
        Me.grcAbonoaCuenta.Name = "grcAbonoaCuenta"
        Me.grcAbonoaCuenta.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcAbonoaCuenta.VisibleIndex = 4
        Me.grcAbonoaCuenta.Width = 115
        '
        'grcTipoMovimiento
        '
        Me.grcTipoMovimiento.Caption = "Tipo de Movimiento"
        Me.grcTipoMovimiento.FieldName = "tipo_movimiento_despliege"
        Me.grcTipoMovimiento.Name = "grcTipoMovimiento"
        Me.grcTipoMovimiento.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipoMovimiento.VisibleIndex = 2
        Me.grcTipoMovimiento.Width = 116
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio"
        Me.grcFolio.FieldName = "folio_movimiento"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 1
        Me.grcFolio.Width = 57
        '
        'grcClaveTipo
        '
        Me.grcClaveTipo.Caption = "grcTipo"
        Me.grcClaveTipo.FieldName = "tipo_movimiento"
        Me.grcClaveTipo.Name = "grcClaveTipo"
        Me.grcClaveTipo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'lkpCuentaBancaria
        '
        Me.lkpCuentaBancaria.AllowAdd = False
        Me.lkpCuentaBancaria.AutoReaload = True
        Me.lkpCuentaBancaria.DataSource = Nothing
        Me.lkpCuentaBancaria.DefaultSearchField = ""
        Me.lkpCuentaBancaria.DisplayMember = "cuenta_bancaria"
        Me.lkpCuentaBancaria.EditValue = Nothing
        Me.lkpCuentaBancaria.Filtered = False
        Me.lkpCuentaBancaria.InitValue = Nothing
        Me.lkpCuentaBancaria.Location = New System.Drawing.Point(120, 64)
        Me.lkpCuentaBancaria.MultiSelect = False
        Me.lkpCuentaBancaria.Name = "lkpCuentaBancaria"
        Me.lkpCuentaBancaria.NullText = ""
        Me.lkpCuentaBancaria.PopupWidth = CType(300, Long)
        Me.lkpCuentaBancaria.Required = False
        Me.lkpCuentaBancaria.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCuentaBancaria.SearchMember = ""
        Me.lkpCuentaBancaria.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCuentaBancaria.SelectAll = False
        Me.lkpCuentaBancaria.Size = New System.Drawing.Size(200, 22)
        Me.lkpCuentaBancaria.TabIndex = 3
        Me.lkpCuentaBancaria.Tag = "cuenta_bancaria"
        Me.lkpCuentaBancaria.ToolTip = "Cuenta bancaria"
        Me.lkpCuentaBancaria.ValueMember = "cuenta_bancaria"
        '
        'lblCuenta_Bancaria
        '
        Me.lblCuenta_Bancaria.AutoSize = True
        Me.lblCuenta_Bancaria.Location = New System.Drawing.Point(16, 64)
        Me.lblCuenta_Bancaria.Name = "lblCuenta_Bancaria"
        Me.lblCuenta_Bancaria.Size = New System.Drawing.Size(99, 16)
        Me.lblCuenta_Bancaria.TabIndex = 2
        Me.lblCuenta_Bancaria.Tag = ""
        Me.lblCuenta_Bancaria.Text = "&Cuenta bancaria:"
        Me.lblCuenta_Bancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(72, 40)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 0
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBanco
        '
        Me.lkpBanco.AllowAdd = False
        Me.lkpBanco.AutoReaload = False
        Me.lkpBanco.DataSource = Nothing
        Me.lkpBanco.DefaultSearchField = ""
        Me.lkpBanco.DisplayMember = "Nombre"
        Me.lkpBanco.EditValue = Nothing
        Me.lkpBanco.Filtered = False
        Me.lkpBanco.InitValue = Nothing
        Me.lkpBanco.Location = New System.Drawing.Point(120, 40)
        Me.lkpBanco.MultiSelect = False
        Me.lkpBanco.Name = "lkpBanco"
        Me.lkpBanco.NullText = ""
        Me.lkpBanco.PopupWidth = CType(400, Long)
        Me.lkpBanco.Required = False
        Me.lkpBanco.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBanco.SearchMember = ""
        Me.lkpBanco.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBanco.SelectAll = False
        Me.lkpBanco.Size = New System.Drawing.Size(200, 22)
        Me.lkpBanco.TabIndex = 1
        Me.lkpBanco.Tag = "Banco"
        Me.lkpBanco.ToolTip = "Banco"
        Me.lkpBanco.ValueMember = "Banco"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(416, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(117, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "Tipo de Movimiento:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbrSelecccion
        '
        Me.tbrSelecccion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbrSelecccion.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.tbrSelecccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbrSelecccion.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.optTodos, Me.optNinguno})
        Me.tbrSelecccion.ButtonSize = New System.Drawing.Size(67, 22)
        Me.tbrSelecccion.Dock = System.Windows.Forms.DockStyle.None
        Me.tbrSelecccion.DropDownArrows = True
        Me.tbrSelecccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrSelecccion.ImageList = Me.ilsToolbar
        Me.tbrSelecccion.Location = New System.Drawing.Point(13, 93)
        Me.tbrSelecccion.Name = "tbrSelecccion"
        Me.tbrSelecccion.ShowToolTips = True
        Me.tbrSelecccion.Size = New System.Drawing.Size(731, 29)
        Me.tbrSelecccion.TabIndex = 6
        Me.tbrSelecccion.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right
        '
        'optTodos
        '
        Me.optTodos.ImageIndex = 6
        Me.optTodos.Text = "Autorizar Todas"
        Me.optTodos.ToolTipText = "Autorizar Todas"
        '
        'optNinguno
        '
        Me.optNinguno.ImageIndex = 7
        Me.optNinguno.Text = "Cancelar Todas"
        Me.optNinguno.ToolTipText = "Cancelar Todas"
        '
        'lblTotalImporte
        '
        Me.lblTotalImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblTotalImporte.Location = New System.Drawing.Point(638, 97)
        Me.lblTotalImporte.Name = "lblTotalImporte"
        Me.lblTotalImporte.Size = New System.Drawing.Size(96, 23)
        Me.lblTotalImporte.TabIndex = 62
        Me.lblTotalImporte.Text = "$ 0.00"
        Me.lblTotalImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'labelTotal
        '
        Me.labelTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.labelTotal.Location = New System.Drawing.Point(536, 97)
        Me.labelTotal.Name = "labelTotal"
        Me.labelTotal.Size = New System.Drawing.Size(96, 23)
        Me.labelTotal.TabIndex = 63
        Me.labelTotal.Text = "Importe Total :"
        Me.labelTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmImpresionCheques
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(754, 424)
        Me.Controls.Add(Me.labelTotal)
        Me.Controls.Add(Me.lblTotalImporte)
        Me.Controls.Add(Me.tbrSelecccion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpCuentaBancaria)
        Me.Controls.Add(Me.lblCuenta_Bancaria)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.lkpBanco)
        Me.Controls.Add(Me.grMovimientos)
        Me.Controls.Add(Me.cboTipoMovimiento)
        Me.Name = "frmImpresionCheques"
        Me.Text = "frmImpresionCheques"
        CType(Me.cboTipoMovimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepChkImprimir, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oBancos As VillarrealBusiness.clsBancos
    Private oChequeras As VillarrealBusiness.clsChequeras
    Private oReportes As VillarrealBusiness.Reportes
    Dim ImporteTotal As Integer
    Dim ImporteTotal2 As Integer

    Public ReadOnly Property Banco() As Long
        Get
            Return PreparaValorLookup(Me.lkpBanco)
        End Get
    End Property
    Public ReadOnly Property CuentaBancaria() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpCuentaBancaria)
        End Get
    End Property
    ReadOnly Property FolioChequera(ByVal cuenta_bancaria As String) As Long
        Get
            Return CType(oChequeras.DespliegaDatos(cuenta_bancaria).Value, DataSet).Tables(0).Rows(0).Item("folio") + 1
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmImpresionCheques_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
        oBancos = New VillarrealBusiness.clsBancos
        oChequeras = New VillarrealBusiness.clsChequeras
        oReportes = New VillarrealBusiness.Reportes
    End Sub
    Private Sub frmImpresionCheques_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = Guardar()

        If Not Response.ErrorFound Then
            LLenaGridMovimientos()
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpBanco_Format() Handles lkpBanco.Format
        Comunes.clsFormato.for_bancos_grl(Me.lkpBanco)
    End Sub
    Private Sub lkpBanco_LoadData(ByVal Initialize As Boolean) Handles lkpBanco.LoadData
        Dim response As Events
        response = oBancos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBanco.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpBanco_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBanco.EditValueChanged
        If Me.Banco > 0 Then
            Me.lkpCuentaBancaria.EditValue = Nothing
        End If
    End Sub
    Private Sub lkpCuentaBancaria_Format() Handles lkpCuentaBancaria.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpCuentaBancaria)
    End Sub
    Private Sub lkpCuentaBancaria_LoadData(ByVal Initialize As Boolean) Handles lkpCuentaBancaria.LoadData
        Dim response As Events
        response = oChequeras.Lookup(Me.Banco)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCuentaBancaria.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpCuentaBancaria_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCuentaBancaria.EditValueChanged
        LLenaGridMovimientos()
    End Sub

    Private Sub tbrSelecccion_ButtonClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrSelecccion.ButtonClick
        If e.Button Is Me.optTodos Or e.Button Is Me.optNinguno Then
            If e.Button Is Me.optTodos Then
                Marcar("AUTORIZAR")
            End If

            If e.Button Is Me.optNinguno Then
                Marcar("CANCELAR")
            End If

        End If
    End Sub
    Private Sub cboTipoMovimiento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoMovimiento.SelectedIndexChanged
        LLenaGridMovimientos()
    End Sub
    Private Sub grvMovimientos_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvMovimientos.CellValueChanging

        Me.grvMovimientos.UpdateCurrentRow()
        Dim i As Long
        Dim response As Events
        ImporteTotal = 0
        Me.lblTotalImporte.Text = ImporteTotal.ToString("C")

        If e.Column Is Me.grcImprimir Then

            For i = 0 To Me.grvMovimientos.RowCount - 1
                If e.RowHandle = i And e.Value = True Then
                    ImporteTotal = ImporteTotal + Me.grvMovimientos.GetRowCellValue(i, Me.grcImporte)
                Else
                    If e.RowHandle <> i And grvMovimientos.GetRowCellValue(i, Me.grcImprimir) = True Then
                        ImporteTotal = ImporteTotal + Me.grvMovimientos.GetRowCellValue(i, Me.grcImporte)
                    End If
                End If
            Next

            Me.lblTotalImporte.Text = ImporteTotal.ToString("C")
            ImporteTotal = 0
        End If

    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub Marcar(ByVal columna As String)
        Dim i As Long

        Select Case columna.Trim
            Case "AUTORIZAR"
                For i = 0 To Me.grvMovimientos.RowCount - 1
                    Me.grvMovimientos.SetRowCellValue(i, Me.grcImprimir, True)
                Next

            Case "CANCELAR"
                For i = 0 To Me.grvMovimientos.RowCount - 1
                    Me.grvMovimientos.SetRowCellValue(i, Me.grcImprimir, False)
                Next
        End Select

    End Sub
    Private Sub LLenaGridMovimientos()
        Dim response As Events

        response = oReportes.MovimientosImpresionCheque(Banco, Me.CuentaBancaria, Me.cboTipoMovimiento.Value)

        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = CType(response.Value, DataSet)
            grMovimientos.DataSource = odataset.Tables(0)

        Else
            response.ShowMessage()
        End If
    End Sub
    Private Function Guardar() As Events
        Me.grvMovimientos.CloseEditor()
        Me.grvMovimientos.UpdateCurrentRow()

        Dim response As New Events
        Dim i As Long
        Dim lbanco As Long
        Dim scuentabancaria As String
        Dim lfolio_movimiento As Long
        Dim lfolio_cheque As Long
        Dim sconcepto As String
        Dim ltipo As Long

        For i = 0 To Me.grvMovimientos.RowCount - 1
            If Me.grvMovimientos.GetRowCellValue(i, Me.grcImprimir) = True Then

                lbanco = Me.grvMovimientos.GetRowCellValue(i, Me.grcClaveBanco)
                scuentabancaria = Me.grvMovimientos.GetRowCellValue(i, Me.grcCuentaBancaria)
                lfolio_movimiento = Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio)
                sconcepto = Me.grvMovimientos.GetRowCellValue(i, Me.grcConcepto)
                ltipo = Me.grvMovimientos.GetRowCellValue(i, Me.grcClaveTipo)

                'abro la transaccion
                TINApp.Connection.Begin()

                'Obtengo el siguiente folio de la chequera
                lfolio_cheque = Me.FolioChequera(scuentabancaria)

                response = Me.oReportes.ActualizaChequeTipoMovimiento(lbanco, scuentabancaria, lfolio_movimiento, lfolio_cheque, sconcepto, ltipo)

                If Not response.ErrorFound Then
                    'Se Termino exitosa la transaccion
                    TINApp.Connection.Commit()

                    'Depende el tipo de Movimiento se utiliza la funcion
                    Select Case ltipo
                        Case 1
                            ImprimirChequeMovimientosChequera(lbanco, scuentabancaria, lfolio_cheque)
                        Case 2
                            ImprimirChequeAcreedoresDiversos(lbanco, scuentabancaria, lfolio_cheque)
                        Case 3
                            ImprimirChequeMovimientosPagar(lbanco, scuentabancaria, lfolio_cheque)
                    End Select

                Else
                    'Hubo un error en la transaccion
                    TINApp.Connection.Rollback()
                    response.ShowMessage()
                End If
            End If
        Next
        Return response
    End Function


#Region "DIPROS Systems, Funciones de Impresiones"

    Private Sub ImprimirChequeMovimientosChequera(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Integer)
        Dim response As New Events

        Try
            response = oReportes.ImprimeChequeMovimientosChequera(banco, cuentabancaria, folio)
            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Cheque no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptCheque

                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Cheques", oReport)

                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
    Private Sub ImprimirChequeAcreedoresDiversos(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Integer)
        Dim response As New Events


        Try
            response = oReportes.ImprimeChequeAcreedoresDiversos(banco, cuentabancaria, folio)
            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Cheque no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptCheque

                    oDataSet = response.Value

                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Cheques", oReport)
                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
    Private Sub ImprimirChequeMovimientosPagar(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Integer)
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try

            response = oReportes.ImprimeChequeMovimientosPagar(banco, cuentabancaria, folio)

            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Cheque no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptCheque

                    oDataSet = response.Value

                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Cheques", oReport)
                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
#End Region

#End Region

  
End Class
