Imports Dipros.Utils.Common
Imports Dipros.Utils
Imports Comunes.clsUtilerias
Public Class frmAsignarAbonos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents gpb1 As System.Windows.Forms.GroupBox
    Friend WithEvents clcSaldoTotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDrescripcionConcepto As System.Windows.Forms.Label
    Friend WithEvents lkpConcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents lblFolio_Referencia As System.Windows.Forms.Label
    Friend WithEvents lblConcepto_Referencia As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtFolio_Factura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFolio_Factura As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoReferencia As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblEntrada As System.Windows.Forms.Label
    Friend WithEvents clcEntrada As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDrescripcionConceptoRef As System.Windows.Forms.Label
    Friend WithEvents lkpFolioReferencia As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSaldoTotal As System.Windows.Forms.Label
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAsignarAbonos))
        Me.gpb1 = New System.Windows.Forms.GroupBox
        Me.clcSaldoTotal = New Dipros.Editors.TINCalcEdit
        Me.lblDrescripcionConcepto = New System.Windows.Forms.Label
        Me.lkpConcepto = New Dipros.Editors.TINMultiLookup
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.lblFolio = New System.Windows.Forms.Label
        Me.lblFolio_Referencia = New System.Windows.Forms.Label
        Me.lblConcepto_Referencia = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.txtFolio_Factura = New DevExpress.XtraEditors.TextEdit
        Me.lblFolio_Factura = New System.Windows.Forms.Label
        Me.lkpConceptoReferencia = New Dipros.Editors.TINMultiLookup
        Me.lblEntrada = New System.Windows.Forms.Label
        Me.clcEntrada = New Dipros.Editors.TINCalcEdit
        Me.lblDrescripcionConceptoRef = New System.Windows.Forms.Label
        Me.lkpFolioReferencia = New Dipros.Editors.TINMultiLookup
        Me.lblSaldoTotal = New System.Windows.Forms.Label
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.gpb1.SuspendLayout()
        CType(Me.clcSaldoTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFolio_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcEntrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1439, 28)
        '
        'gpb1
        '
        Me.gpb1.Controls.Add(Me.clcSaldoTotal)
        Me.gpb1.Controls.Add(Me.lblDrescripcionConcepto)
        Me.gpb1.Controls.Add(Me.lkpConcepto)
        Me.gpb1.Controls.Add(Me.lblConcepto)
        Me.gpb1.Controls.Add(Me.lblFolio)
        Me.gpb1.Controls.Add(Me.lblFolio_Referencia)
        Me.gpb1.Controls.Add(Me.lblConcepto_Referencia)
        Me.gpb1.Controls.Add(Me.clcFolio)
        Me.gpb1.Controls.Add(Me.txtFolio_Factura)
        Me.gpb1.Controls.Add(Me.lblFolio_Factura)
        Me.gpb1.Controls.Add(Me.lkpConceptoReferencia)
        Me.gpb1.Controls.Add(Me.lblEntrada)
        Me.gpb1.Controls.Add(Me.clcEntrada)
        Me.gpb1.Controls.Add(Me.lblDrescripcionConceptoRef)
        Me.gpb1.Controls.Add(Me.lkpFolioReferencia)
        Me.gpb1.Controls.Add(Me.lblSaldoTotal)
        Me.gpb1.Location = New System.Drawing.Point(8, 80)
        Me.gpb1.Name = "gpb1"
        Me.gpb1.Size = New System.Drawing.Size(576, 120)
        Me.gpb1.TabIndex = 59
        Me.gpb1.TabStop = False
        '
        'clcSaldoTotal
        '
        Me.clcSaldoTotal.EditValue = "0"
        Me.clcSaldoTotal.Location = New System.Drawing.Point(488, 63)
        Me.clcSaldoTotal.MaxValue = 0
        Me.clcSaldoTotal.MinValue = 0
        Me.clcSaldoTotal.Name = "clcSaldoTotal"
        '
        'clcSaldoTotal.Properties
        '
        Me.clcSaldoTotal.Properties.DisplayFormat.FormatString = "C2"
        Me.clcSaldoTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoTotal.Properties.EditFormat.FormatString = "C2"
        Me.clcSaldoTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoTotal.Properties.Enabled = False
        Me.clcSaldoTotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSaldoTotal.Properties.Precision = 2
        Me.clcSaldoTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldoTotal.Size = New System.Drawing.Size(78, 19)
        Me.clcSaldoTotal.TabIndex = 12
        Me.clcSaldoTotal.Tag = ""
        Me.clcSaldoTotal.Visible = False
        '
        'lblDrescripcionConcepto
        '
        Me.lblDrescripcionConcepto.Location = New System.Drawing.Point(208, 14)
        Me.lblDrescripcionConcepto.Name = "lblDrescripcionConcepto"
        Me.lblDrescripcionConcepto.Size = New System.Drawing.Size(240, 22)
        Me.lblDrescripcionConcepto.TabIndex = 11
        '
        'lkpConcepto
        '
        Me.lkpConcepto.AllowAdd = False
        Me.lkpConcepto.AutoReaload = False
        Me.lkpConcepto.DataSource = Nothing
        Me.lkpConcepto.DefaultSearchField = ""
        Me.lkpConcepto.DisplayMember = "concepto"
        Me.lkpConcepto.EditValue = Nothing
        Me.lkpConcepto.Enabled = False
        Me.lkpConcepto.Filtered = False
        Me.lkpConcepto.InitValue = Nothing
        Me.lkpConcepto.Location = New System.Drawing.Point(136, 14)
        Me.lkpConcepto.MultiSelect = False
        Me.lkpConcepto.Name = "lkpConcepto"
        Me.lkpConcepto.NullText = ""
        Me.lkpConcepto.PopupWidth = CType(400, Long)
        Me.lkpConcepto.Required = False
        Me.lkpConcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConcepto.SearchMember = ""
        Me.lkpConcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConcepto.SelectAll = False
        Me.lkpConcepto.Size = New System.Drawing.Size(64, 20)
        Me.lkpConcepto.TabIndex = 1
        Me.lkpConcepto.Tag = "concepto"
        Me.lkpConcepto.ToolTip = Nothing
        Me.lkpConcepto.ValueMember = "concepto"
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(70, 16)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 0
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "&Concepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(448, 16)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 7
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio_Referencia
        '
        Me.lblFolio_Referencia.AutoSize = True
        Me.lblFolio_Referencia.Location = New System.Drawing.Point(33, 64)
        Me.lblFolio_Referencia.Name = "lblFolio_Referencia"
        Me.lblFolio_Referencia.Size = New System.Drawing.Size(97, 16)
        Me.lblFolio_Referencia.TabIndex = 4
        Me.lblFolio_Referencia.Tag = ""
        Me.lblFolio_Referencia.Text = "&Folio Referencia:"
        Me.lblFolio_Referencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblConcepto_Referencia
        '
        Me.lblConcepto_Referencia.AutoSize = True
        Me.lblConcepto_Referencia.Location = New System.Drawing.Point(8, 40)
        Me.lblConcepto_Referencia.Name = "lblConcepto_Referencia"
        Me.lblConcepto_Referencia.Size = New System.Drawing.Size(122, 16)
        Me.lblConcepto_Referencia.TabIndex = 2
        Me.lblConcepto_Referencia.Tag = ""
        Me.lblConcepto_Referencia.Text = "&Concepto Referencia:"
        Me.lblConcepto_Referencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(488, 15)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(78, 19)
        Me.clcFolio.TabIndex = 8
        Me.clcFolio.Tag = "folio"
        '
        'txtFolio_Factura
        '
        Me.txtFolio_Factura.EditValue = ""
        Me.txtFolio_Factura.Location = New System.Drawing.Point(488, 86)
        Me.txtFolio_Factura.Name = "txtFolio_Factura"
        '
        'txtFolio_Factura.Properties
        '
        Me.txtFolio_Factura.Properties.Enabled = False
        Me.txtFolio_Factura.Properties.MaxLength = 12
        Me.txtFolio_Factura.Size = New System.Drawing.Size(78, 20)
        Me.txtFolio_Factura.TabIndex = 10
        Me.txtFolio_Factura.Tag = "folio_factura"
        Me.txtFolio_Factura.Visible = False
        '
        'lblFolio_Factura
        '
        Me.lblFolio_Factura.AutoSize = True
        Me.lblFolio_Factura.Enabled = False
        Me.lblFolio_Factura.Location = New System.Drawing.Point(400, 88)
        Me.lblFolio_Factura.Name = "lblFolio_Factura"
        Me.lblFolio_Factura.Size = New System.Drawing.Size(80, 16)
        Me.lblFolio_Factura.TabIndex = 9
        Me.lblFolio_Factura.Tag = ""
        Me.lblFolio_Factura.Text = "&Folio Factura:"
        Me.lblFolio_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblFolio_Factura.Visible = False
        '
        'lkpConceptoReferencia
        '
        Me.lkpConceptoReferencia.AllowAdd = False
        Me.lkpConceptoReferencia.AutoReaload = False
        Me.lkpConceptoReferencia.DataSource = Nothing
        Me.lkpConceptoReferencia.DefaultSearchField = ""
        Me.lkpConceptoReferencia.DisplayMember = "concepto"
        Me.lkpConceptoReferencia.EditValue = Nothing
        Me.lkpConceptoReferencia.Filtered = False
        Me.lkpConceptoReferencia.InitValue = Nothing
        Me.lkpConceptoReferencia.Location = New System.Drawing.Point(136, 38)
        Me.lkpConceptoReferencia.MultiSelect = False
        Me.lkpConceptoReferencia.Name = "lkpConceptoReferencia"
        Me.lkpConceptoReferencia.NullText = ""
        Me.lkpConceptoReferencia.PopupWidth = CType(400, Long)
        Me.lkpConceptoReferencia.Required = False
        Me.lkpConceptoReferencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoReferencia.SearchMember = ""
        Me.lkpConceptoReferencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoReferencia.SelectAll = False
        Me.lkpConceptoReferencia.Size = New System.Drawing.Size(64, 20)
        Me.lkpConceptoReferencia.TabIndex = 3
        Me.lkpConceptoReferencia.Tag = "concepto_referencia"
        Me.lkpConceptoReferencia.ToolTip = Nothing
        Me.lkpConceptoReferencia.ValueMember = "concepto"
        '
        'lblEntrada
        '
        Me.lblEntrada.AutoSize = True
        Me.lblEntrada.Enabled = False
        Me.lblEntrada.Location = New System.Drawing.Point(78, 88)
        Me.lblEntrada.Name = "lblEntrada"
        Me.lblEntrada.Size = New System.Drawing.Size(52, 16)
        Me.lblEntrada.TabIndex = 6
        Me.lblEntrada.Tag = ""
        Me.lblEntrada.Text = "&Entrada:"
        Me.lblEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEntrada.Visible = False
        '
        'clcEntrada
        '
        Me.clcEntrada.EditValue = "0"
        Me.clcEntrada.Location = New System.Drawing.Point(136, 87)
        Me.clcEntrada.MaxValue = 0
        Me.clcEntrada.MinValue = 0
        Me.clcEntrada.Name = "clcEntrada"
        '
        'clcEntrada.Properties
        '
        Me.clcEntrada.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEntrada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEntrada.Properties.Enabled = False
        Me.clcEntrada.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcEntrada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEntrada.Size = New System.Drawing.Size(64, 19)
        Me.clcEntrada.TabIndex = 7
        Me.clcEntrada.Tag = "entrada"
        Me.clcEntrada.Visible = False
        '
        'lblDrescripcionConceptoRef
        '
        Me.lblDrescripcionConceptoRef.Location = New System.Drawing.Point(208, 38)
        Me.lblDrescripcionConceptoRef.Name = "lblDrescripcionConceptoRef"
        Me.lblDrescripcionConceptoRef.Size = New System.Drawing.Size(240, 22)
        Me.lblDrescripcionConceptoRef.TabIndex = 11
        '
        'lkpFolioReferencia
        '
        Me.lkpFolioReferencia.AllowAdd = False
        Me.lkpFolioReferencia.AutoReaload = False
        Me.lkpFolioReferencia.DataSource = Nothing
        Me.lkpFolioReferencia.DefaultSearchField = ""
        Me.lkpFolioReferencia.DisplayMember = "folio_referencia"
        Me.lkpFolioReferencia.EditValue = Nothing
        Me.lkpFolioReferencia.Filtered = False
        Me.lkpFolioReferencia.InitValue = Nothing
        Me.lkpFolioReferencia.Location = New System.Drawing.Point(136, 62)
        Me.lkpFolioReferencia.MultiSelect = False
        Me.lkpFolioReferencia.Name = "lkpFolioReferencia"
        Me.lkpFolioReferencia.NullText = ""
        Me.lkpFolioReferencia.PopupWidth = CType(400, Long)
        Me.lkpFolioReferencia.Required = False
        Me.lkpFolioReferencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolioReferencia.SearchMember = ""
        Me.lkpFolioReferencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolioReferencia.SelectAll = False
        Me.lkpFolioReferencia.Size = New System.Drawing.Size(64, 20)
        Me.lkpFolioReferencia.TabIndex = 3
        Me.lkpFolioReferencia.Tag = "folio_referencia"
        Me.lkpFolioReferencia.ToolTip = Nothing
        Me.lkpFolioReferencia.ValueMember = "folio_referencia"
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.Enabled = False
        Me.lblSaldoTotal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldoTotal.Location = New System.Drawing.Point(400, 64)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(80, 16)
        Me.lblSaldoTotal.TabIndex = 11
        Me.lblSaldoTotal.Text = "Saldo Total:"
        Me.lblSaldoTotal.Visible = False
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(8, 58)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 60
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "Provee&dor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Enabled = False
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(80, 56)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(450, Long)
        Me.lkpProveedor.Required = True
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(504, 20)
        Me.lkpProveedor.TabIndex = 61
        Me.lkpProveedor.Tag = "Proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "Proveedor"
        '
        'frmAsignarAbonos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(594, 208)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.gpb1)
        Me.Name = "frmAsignarAbonos"
        Me.Text = "frmAsignarAbonos"
        Me.Controls.SetChildIndex(Me.gpb1, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        Me.gpb1.ResumeLayout(False)
        CType(Me.clcSaldoTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFolio_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcEntrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones de Variables"
    Dim KS As Keys

    Private ActualizarCheque As Boolean
    Private FolioPagoProveedores As Long = 0

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oMovimientosPagar As VillarrealBusiness.clsMovimientosPagar
    Private oChequeras As VillarrealBusiness.clsChequeras
    Private oConceptosCxp As VillarrealBusiness.clsConceptosCxp
    Private ovariables As New VillarrealBusiness.clsVariables
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private lFolio As Long = 0
    Private sConcepto As String = ""
    Private lFolioReferencia As Long = 0
    Private sConceptoReferencia As String = ""


    Public Property Concepto() As String
        Get
            'Return sConcepto '
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConcepto)
        End Get
        Set(ByVal Value As String)
            Me.lkpConcepto.EditValue = Value
        End Set

    End Property
    Public Property Folio() As Long
        Get
            Return lFolio
        End Get
        Set(ByVal Value As Long)
            Me.clcFolio.EditValue = Value
        End Set

    End Property
    Public Property Proveedor() As Long
        Get
            Return PreparaValorLookup(Me.lkpProveedor)
        End Get
        Set(ByVal Value As Long)
            Me.lkpProveedor.EditValue = Value
        End Set
    End Property
    'Public ReadOnly Property ConceptoReferencia() As String
    '    Get
    '        Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoReferencia)
    '    End Get
    'End Property
    Public Property ConceptoReferencia() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoReferencia)
        End Get
        Set(ByVal Value As String)
            Me.lkpConceptoReferencia.EditValue = Value
        End Set
    End Property
    Public Property FolioReferencia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolioReferencia)
        End Get
        Set(ByVal Value As Long)
            Me.lkpFolioReferencia.EditValue = Value
        End Set
    End Property

    ReadOnly Property FolioChequera(ByVal cuenta_bancaria As String) As Long
        Get
            Return CType(oChequeras.DespliegaDatos(cuenta_bancaria).Value, DataSet).Tables(0).Rows(0).Item("folio") + 1
        End Get
    End Property
    Private ReadOnly Property Impuesto() As Double
        Get
            If ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float) = -1 Then
                Return 0
            Else
                Return (CType(ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double) / 100)
            End If


        End Get
    End Property

    Private TipoConcepto As Char = ""

    'Private ReadOnly Property Cargo() As Object
    '    Get
    '        Return IIf(CStr(Me.lkpConcepto.GetValue("tipo")) = "P" Or CStr(Me.lkpConcepto.GetValue("tipo")) = "A", System.DBNull.Value, Me.clcTotal.EditValue)
    '    End Get
    'End Property

    'Private ReadOnly Property Abono() As Object
    '    Get
    '        Return IIf(CStr(Me.lkpConcepto.GetValue("tipo")) = "P" Or CStr(Me.lkpConcepto.GetValue("tipo")) = "A", Me.clcTotal.EditValue, System.DBNull.Value)
    '    End Get
    'End Property

    'Private ReadOnly Property TipoPago() As Object
    '    Get
    '        Return IIf(Me.cboTipoPago.EditValue = "P", System.DBNull.Value, Me.cboTipoPago.EditValue)
    '    End Get
    'End Property

    'Private ReadOnly Property Banco() As Object
    '    Get
    '        If Me.lkpChequera.EditValue Is Nothing Then
    '            Return System.DBNull.Value
    '        Else
    '            Return Me.lkpChequera.GetValue("banco")
    '        End If
    '    End Get
    'End Property

    'Private ReadOnly Property Status() As String
    '    Get
    '        If Action = Actions.Insert And CStr(Me.lkpConcepto.GetValue("tipo") = "P") Or CStr(Me.lkpConcepto.GetValue("tipo") = "A") Then
    '            Return ""
    '        Else
    '            Return Me.txtStatus.Text
    '        End If

    '    End Get
    'End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmAsignarAbonos_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmAsignarAbonos_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmAsignarAbonos_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmAsignarAbonos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oMovimientosPagar.AsignarAbonoPorComision(Me.lkpConcepto.EditValue, Me.clcFolio.EditValue, Me.Proveedor, Me.clcEntrada.EditValue, Me.ConceptoReferencia, Me.FolioReferencia)

    End Sub

    Private Sub frmAsignarAbonos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

        Me.lkpConceptoReferencia.Reload()

    End Sub

    Private Sub frmAsignarAbonos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

        oMovimientosPagar = New VillarrealBusiness.clsMovimientosPagar
        oProveedores = New VillarrealBusiness.clsProveedores
        oConceptosCxp = New VillarrealBusiness.clsConceptosCxp

        Me.lkpProveedor.EditValue = CType(OwnerForm, brwMovimientosPagar).Proveedor
        Me.lkpConcepto.EditValue = CType(OwnerForm, brwMovimientosPagar).Concepto

        If CType(OwnerForm, brwMovimientosPagar).ConceptoReferencia.Length > 0 And CType(OwnerForm, brwMovimientosPagar).FolioReferencia > 0 And Me.clcEntrada.EditValue = CType(OwnerForm, brwMovimientosPagar).Entrada > 0 Then
            Me.lkpConceptoReferencia.EditValue = CType(OwnerForm, brwMovimientosPagar).ConceptoReferencia
            Me.lkpFolioReferencia.EditValue = CType(OwnerForm, brwMovimientosPagar).FolioReferencia
            Me.clcEntrada.EditValue = CType(OwnerForm, brwMovimientosPagar).Entrada
        End If
    End Sub

    Private Sub frmAsignarAbonos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMovimientosPagar.Validacion(Me.lkpConceptoReferencia.EditValue, Me.lkpFolioReferencia.EditValue, Me.lkpConcepto.EditValue, Me.clcFolio.EditValue, Me.clcEntrada.EditValue)
    End Sub


#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpConcepto_Format() Handles lkpConcepto.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConcepto)
    End Sub

    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpConcepto.LoadData
        Dim response As Events
        response = oConceptosCxp.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConcepto.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpConcepto_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConcepto.EditValueChanged
        If Me.lkpConcepto.EditValue Is Nothing Then Exit Sub

        Me.lblDrescripcionConcepto.Text = CStr(Me.lkpConcepto.GetValue("descripcion"))
        Me.TipoConcepto = CStr(Me.lkpConcepto.GetValue("tipo"))

        'CONCEPTO    P :  PAGO     Y   A :  ABONO
        If TipoConcepto = "P" Or TipoConcepto = "A" Then

            Me.lkpConceptoReferencia.Reload()

        End If
    End Sub

    Private Sub lkpConceptoReferencia_Format() Handles lkpConceptoReferencia.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConceptoReferencia)
    End Sub

    Private Sub lkpConceptoReferencia_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoReferencia.LoadData
        Dim response As Events

        response = oConceptosCxp.LookupConceptoReferencia(TipoConcepto)

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoReferencia.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpConceptoReferencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConceptoReferencia.EditValueChanged
        If Me.lkpConceptoReferencia.EditValue Is Nothing Then Exit Sub
        'Me.lkpFolioReferencia.Enabled = True
        'Me.lkpFolioReferencia.EditValue = ""
        Me.lkpFolioReferencia.AutoReaload = True
        Me.lblDrescripcionConceptoRef.Text = CStr(Me.lkpConceptoReferencia.GetValue("descripcion"))
        LimpiarDatos()
    End Sub

    Private Sub lkpFolioReferencia_LoadData(ByVal Initialize As Boolean) Handles lkpFolioReferencia.LoadData
        Dim response As Events
        Dim proveedor As Long

        proveedor = Me.Proveedor
        response = oMovimientosPagar.Lookup(ConceptoReferencia, proveedor)

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolioReferencia.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpFolioReferencia_Format() Handles lkpFolioReferencia.Format
        Comunes.clsFormato.for_movimientos_pagar_grl(Me.lkpFolioReferencia)
    End Sub
    Private Sub lkpFolioReferencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolioReferencia.EditValueChanged
        If Me.lkpFolioReferencia.EditValue Is Nothing Then Exit Sub

        CargarDatosMovimientoPagar()

    End Sub

    'Private Sub cboTipoPago_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoPago.SelectedIndexChanged
    '    Select Case CStr(Me.cboTipoPago.EditValue).ToUpper
    '        Case "P"
    '            Me.lkpChequera.EditValue = Nothing
    '            Me.txtReferencia_Transferencia.Text = ""
    '            Me.lkpChequera.Enabled = False
    '            Me.txtReferencia_Transferencia.Enabled = False
    '            Me.clcCheque.EditValue = 0
    '        Case "T"
    '            Me.lkpChequera.EditValue = Nothing
    '            Me.lkpChequera.Enabled = True
    '            Me.txtReferencia_Transferencia.Enabled = True
    '            Me.lkpChequera.EditValue = Comunes.clsUtilerias.uti_ChequeraPredeterminada
    '        Case "C"
    '            Me.lkpChequera.Enabled = True
    '            Me.txtReferencia_Transferencia.Text = ""
    '            Me.txtReferencia_Transferencia.Enabled = False
    '            Me.lkpChequera.EditValue = Comunes.clsUtilerias.uti_ChequeraPredeterminada
    '    End Select
    'End Sub
    'Private Sub dteFecha_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha.EditValueChanged
    '    If Not IsDate(Me.dteFecha.Text) Or Me.lkpConcepto.EditValue Is Nothing Then Exit Sub

    '    If CStr(Me.lkpConcepto.GetValue("tipo")) = "P" Or CStr(Me.lkpConcepto.GetValue("tipo")) = "A" Then
    '        Me.dteFecha_Vto.EditValue = Me.dteFecha.EditValue
    '    Else
    '        Me.dteFecha_Vto.EditValue = Me.dteFecha.DateTime.AddDays(PlazoMayor)
    '    End If


    'End Sub

    'Private Sub clcSubtotal_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcSubtotal.EditValueChanged
    '    If Me.clcSubtotal.EditValue Is Nothing Then Exit Sub
    '    CalcularIva()
    '    CalcularTotal()
    'End Sub
    'Private Sub clcIva_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcIva.EditValueChanged
    '    If Me.clcIva.EditValue Is Nothing Then Exit Sub

    '    CalcularTotal()
    'End Sub
    'Private Sub clcTotal_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcTotal.EditValueChanged
    '    If Me.clcTotal.EditValue Is Nothing Or Not IsNumeric(Me.clcTotal.EditValue) Then Exit Sub

    '    Me.clcSaldo.EditValue = Me.clcTotal.EditValue
    'End Sub



    'Public Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

    '    If e.Button Is Me.btnImprimeCheque And Action = Actions.Update Then
    '        Dim response As Events

    '        response = oMovimientosPagar.Validacion(Action, Me.clcProveedor.EditValue, Concepto, Me.dteFecha.Text, Me.clcSubtotal.EditValue, Me.clcIva.EditValue, Me.clcTotal.EditValue, Me.cboTipoPago.EditValue, Me.clcBanco.Text, Me.txtReferencia_Transferencia.Text, Me.clcCheque.EditValue)
    '        If response.ErrorFound Then
    '            response.ShowMessage()
    '            Exit Sub
    '        End If
    '        'Abro la Transaccion para obtener el folio del cheque
    '        frmMovimientosPagar_BeginUpdate()
    '        If Me.clcCheque.Value = 0 And Me.cboTipoPago.Value = "C" Then
    '            'asigno el valor al control para que lo actualice
    '            Me.clcCheque.Value = Me.FolioChequera(Me.lkpChequera.EditValue)
    '            ActualizarCheque = False
    '        Else
    '            ActualizarCheque = True
    '        End If
    '        'Actualizo los Valores
    '        frmMovimientosPagar_Accept(response)
    '        If Not response.ErrorFound Then
    '            'Termino Satisfactoriamente la transaccion
    '            frmMovimientosPagar_EndUpdate()
    '            'Imprimo el cheque
    '            ImprimirCheque(Me.lkpChequera.EditValue, Me.clcCheque.Value)

    '            Me.Close()
    '        Else
    '            'Aborto o Cancelo la Transaccion
    '            frmMovimientosPagar_AbortUpdate()
    '            response.ShowMessage()
    '        End If
    '    End If
    'End Sub

    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim response As Events
        response = oProveedores.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarDatosMovimientoPagar()

        If TipoConcepto.ToString.ToUpper = "A" Or TipoConcepto.ToString.ToUpper = "P" Then
            Me.clcEntrada.EditValue = Me.lkpFolioReferencia.GetValue("entrada")
        Else
            Me.clcEntrada.EditValue = 0
        End If

        Me.txtFolio_Factura.Text = Me.lkpFolioReferencia.GetValue("folio_factura")
        Me.clcSaldoTotal.EditValue = Me.lkpFolioReferencia.GetValue("saldo")

        Me.lblSaldoTotal.Visible = True
        Me.clcSaldoTotal.Visible = True
        Me.lblEntrada.Visible = True
        Me.clcEntrada.Visible = True
        Me.lblFolio_Factura.Visible = True
        Me.txtFolio_Factura.Visible = True

    End Sub

    Private Sub LimpiarDatos()

        Me.lkpFolioReferencia.EditValue = Nothing
        Me.clcEntrada.EditValue = 0
        Me.clcSaldoTotal.EditValue = 0
        Me.txtFolio_Factura.Text = ""

        Me.lblSaldoTotal.Visible = False
        Me.clcSaldoTotal.Visible = False
        Me.lblEntrada.Visible = False
        Me.clcEntrada.Visible = False
        Me.lblFolio_Factura.Visible = False
        Me.txtFolio_Factura.Visible = False

    End Sub

    'Private Sub CalcularIva()
    '    If Me.clcSubtotal.EditValue Is Nothing Or Not IsNumeric(Me.clcSubtotal.EditValue) Then Exit Sub

    '    Me.clcIva.EditValue = Me.clcSubtotal.EditValue * Impuesto 'iva   Me.clcIva.EditValue
    'End Sub
    'Private Sub CalcularTotal()
    '    If Me.clcSubtotal.EditValue Is Nothing Or Me.clcIva.EditValue Is Nothing Or Not IsNumeric(Me.clcSubtotal.EditValue) Or Not IsNumeric(Me.clcIva.EditValue) Then Exit Sub

    '    Me.clcTotal.EditValue = Me.clcSubtotal.EditValue + Me.clcIva.EditValue
    'End Sub
    'Private Function PlazoMayor() As Long
    '    Dim oEvents As Events
    '    Dim oProveedores As New VillarrealBusiness.clsProveedores
    '    Dim oDataSet As DataSet
    '    Dim plazo As Long = 0

    '    oEvents = oProveedores.TraerPlazoMayor(Me.clcProveedor.EditValue)
    '    If Not oEvents.ErrorFound Then

    '        oDataSet = oEvents.Value
    '        plazo = CLng(oDataSet.Tables(0).Rows(0).Item("plazo"))

    '    End If

    '    oEvents = Nothing
    '    oProveedores = Nothing
    '    oDataSet = Nothing

    '    Return plazo
    'End Function


    'Private Sub ImprimirCheque(ByVal cuentabancaria As String, ByVal folio_cheque As Integer)
    '    Dim response As New Events
    '    Dim oReportes As New VillarrealBusiness.Reportes

    '    Try

    '        response = oReportes.ImprimeChequeMovimientosPagar(Me.lkpChequera.GetValue("banco"), cuentabancaria, folio_cheque)

    '        If response.ErrorFound Then
    '            ShowMessage(MessageType.MsgInformation, "El Cheque no se puede Mostrar")
    '        Else
    '            If response.Value.Tables(0).Rows.Count > 0 Then

    '                Dim oDataSet As DataSet
    '                Dim oReport As New rptCheque

    '                oDataSet = response.Value

    '                oReport.DataSource = oDataSet.Tables(0)
    '                TINApp.ShowReport(Me.MdiParent, "Cheques", oReport)
    '                oDataSet = Nothing
    '                oReport = Nothing

    '            Else

    '                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ShowMessage(MessageType.MsgError, ex.ToString, )
    '    End Try
    'End Sub

#End Region


End Class
