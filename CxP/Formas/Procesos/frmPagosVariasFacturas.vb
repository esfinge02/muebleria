Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmPagosVariasFacturas
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblDrescripcionConcepto As System.Windows.Forms.Label
    Friend WithEvents lkpConcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtNombreBanco As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents lblCuenta_Bancaria As System.Windows.Forms.Label
    Friend WithEvents lkpChequera As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcBanco As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcCheque As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCheque As System.Windows.Forms.Label
    Friend WithEvents clcTotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents chkAbono_Cuenta As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grcSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaVencimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEntrada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioMovimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkGeneraCheque As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents tbrImpresionPago As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcImportePagar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblConceptoMovimientoChequera As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoMovimiento As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPagosVariasFacturas))
        Me.lblDrescripcionConcepto = New System.Windows.Forms.Label
        Me.lkpConcepto = New Dipros.Editors.TINMultiLookup
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.txtNombreBanco = New DevExpress.XtraEditors.TextEdit
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lblCuenta_Bancaria = New System.Windows.Forms.Label
        Me.lkpChequera = New Dipros.Editors.TINMultiLookup
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.clcBanco = New Dipros.Editors.TINCalcEdit
        Me.clcCheque = New Dipros.Editors.TINCalcEdit
        Me.lblCheque = New System.Windows.Forms.Label
        Me.clcTotal = New Dipros.Editors.TINCalcEdit
        Me.lblTotal = New System.Windows.Forms.Label
        Me.chkAbono_Cuenta = New DevExpress.XtraEditors.CheckEdit
        Me.chkGeneraCheque = New DevExpress.XtraEditors.CheckEdit
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaVencimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEntrada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioMovimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImportePagar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbrImpresionPago = New System.Windows.Forms.ToolBarButton
        Me.lblConceptoMovimientoChequera = New System.Windows.Forms.Label
        Me.lkpConceptoMovimiento = New Dipros.Editors.TINMultiLookup
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbono_Cuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkGeneraCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbrImpresionPago})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4458, 28)
        '
        'lblDrescripcionConcepto
        '
        Me.lblDrescripcionConcepto.Location = New System.Drawing.Point(184, 160)
        Me.lblDrescripcionConcepto.Name = "lblDrescripcionConcepto"
        Me.lblDrescripcionConcepto.Size = New System.Drawing.Size(152, 23)
        Me.lblDrescripcionConcepto.TabIndex = 61
        '
        'lkpConcepto
        '
        Me.lkpConcepto.AllowAdd = False
        Me.lkpConcepto.AutoReaload = False
        Me.lkpConcepto.DataSource = Nothing
        Me.lkpConcepto.DefaultSearchField = ""
        Me.lkpConcepto.DisplayMember = "concepto"
        Me.lkpConcepto.EditValue = Nothing
        Me.lkpConcepto.Enabled = False
        Me.lkpConcepto.Filtered = False
        Me.lkpConcepto.InitValue = Nothing
        Me.lkpConcepto.Location = New System.Drawing.Point(112, 160)
        Me.lkpConcepto.MultiSelect = False
        Me.lkpConcepto.Name = "lkpConcepto"
        Me.lkpConcepto.NullText = ""
        Me.lkpConcepto.PopupWidth = CType(400, Long)
        Me.lkpConcepto.Required = False
        Me.lkpConcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConcepto.SearchMember = ""
        Me.lkpConcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConcepto.SelectAll = False
        Me.lkpConcepto.Size = New System.Drawing.Size(64, 20)
        Me.lkpConcepto.TabIndex = 60
        Me.lkpConcepto.Tag = "concepto"
        Me.lkpConcepto.ToolTip = Nothing
        Me.lkpConcepto.ValueMember = "concepto"
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(16, 160)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(85, 16)
        Me.lblConcepto.TabIndex = 59
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "C&oncepto CxP:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "20/07/2006"
        Me.dteFecha.Location = New System.Drawing.Point(112, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 63
        Me.dteFecha.Tag = "fecha"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(60, 42)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 62
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombreBanco
        '
        Me.txtNombreBanco.EditValue = ""
        Me.txtNombreBanco.Location = New System.Drawing.Point(112, 112)
        Me.txtNombreBanco.Name = "txtNombreBanco"
        '
        'txtNombreBanco.Properties
        '
        Me.txtNombreBanco.Properties.Enabled = False
        Me.txtNombreBanco.Properties.MaxLength = 30
        Me.txtNombreBanco.Size = New System.Drawing.Size(136, 20)
        Me.txtNombreBanco.TabIndex = 65
        Me.txtNombreBanco.Tag = ""
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(58, 112)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 64
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCuenta_Bancaria
        '
        Me.lblCuenta_Bancaria.AutoSize = True
        Me.lblCuenta_Bancaria.Location = New System.Drawing.Point(39, 66)
        Me.lblCuenta_Bancaria.Name = "lblCuenta_Bancaria"
        Me.lblCuenta_Bancaria.Size = New System.Drawing.Size(62, 16)
        Me.lblCuenta_Bancaria.TabIndex = 66
        Me.lblCuenta_Bancaria.Tag = ""
        Me.lblCuenta_Bancaria.Text = "Chequera:"
        Me.lblCuenta_Bancaria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpChequera
        '
        Me.lkpChequera.AllowAdd = False
        Me.lkpChequera.AutoReaload = False
        Me.lkpChequera.DataSource = Nothing
        Me.lkpChequera.DefaultSearchField = ""
        Me.lkpChequera.DisplayMember = "cuenta_bancaria"
        Me.lkpChequera.EditValue = Nothing
        Me.lkpChequera.Filtered = False
        Me.lkpChequera.InitValue = Nothing
        Me.lkpChequera.Location = New System.Drawing.Point(112, 64)
        Me.lkpChequera.MultiSelect = False
        Me.lkpChequera.Name = "lkpChequera"
        Me.lkpChequera.NullText = ""
        Me.lkpChequera.PopupWidth = CType(400, Long)
        Me.lkpChequera.Required = False
        Me.lkpChequera.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpChequera.SearchMember = ""
        Me.lkpChequera.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpChequera.SelectAll = False
        Me.lkpChequera.Size = New System.Drawing.Size(144, 20)
        Me.lkpChequera.TabIndex = 67
        Me.lkpChequera.Tag = "cuentabancaria"
        Me.lkpChequera.ToolTip = Nothing
        Me.lkpChequera.ValueMember = "cuenta_bancaria"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(36, 136)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 68
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "Prov&eedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(112, 136)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(380, Long)
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(304, 20)
        Me.lkpProveedor.TabIndex = 69
        Me.lkpProveedor.Tag = "Proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "Proveedor"
        '
        'clcBanco
        '
        Me.clcBanco.EditValue = "0"
        Me.clcBanco.Location = New System.Drawing.Point(256, 112)
        Me.clcBanco.MaxValue = 0
        Me.clcBanco.MinValue = 0
        Me.clcBanco.Name = "clcBanco"
        '
        'clcBanco.Properties
        '
        Me.clcBanco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBanco.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcBanco.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBanco.Size = New System.Drawing.Size(48, 19)
        Me.clcBanco.TabIndex = 70
        Me.clcBanco.Tag = "banco"
        Me.clcBanco.Visible = False
        '
        'clcCheque
        '
        Me.clcCheque.EditValue = "0"
        Me.clcCheque.Location = New System.Drawing.Point(560, 113)
        Me.clcCheque.MaxValue = 0
        Me.clcCheque.MinValue = 0
        Me.clcCheque.Name = "clcCheque"
        '
        'clcCheque.Properties
        '
        Me.clcCheque.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCheque.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCheque.Properties.Enabled = False
        Me.clcCheque.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCheque.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCheque.Size = New System.Drawing.Size(95, 19)
        Me.clcCheque.TabIndex = 72
        Me.clcCheque.Tag = "folio_cheque"
        '
        'lblCheque
        '
        Me.lblCheque.AutoSize = True
        Me.lblCheque.Location = New System.Drawing.Point(504, 114)
        Me.lblCheque.Name = "lblCheque"
        Me.lblCheque.Size = New System.Drawing.Size(51, 16)
        Me.lblCheque.TabIndex = 71
        Me.lblCheque.Tag = ""
        Me.lblCheque.Text = "C&heque:"
        Me.lblCheque.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotal
        '
        Me.clcTotal.EditValue = "0"
        Me.clcTotal.Location = New System.Drawing.Point(560, 137)
        Me.clcTotal.MaxValue = 0
        Me.clcTotal.MinValue = 0
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.DisplayFormat.FormatString = "C2"
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatString = "C2"
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.Enabled = False
        Me.clcTotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotal.Properties.Precision = 2
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Size = New System.Drawing.Size(95, 19)
        Me.clcTotal.TabIndex = 74
        Me.clcTotal.Tag = "importe"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(512, 138)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(36, 16)
        Me.lblTotal.TabIndex = 73
        Me.lblTotal.Tag = ""
        Me.lblTotal.Text = "&Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkAbono_Cuenta
        '
        Me.chkAbono_Cuenta.Location = New System.Drawing.Point(560, 89)
        Me.chkAbono_Cuenta.Name = "chkAbono_Cuenta"
        '
        'chkAbono_Cuenta.Properties
        '
        Me.chkAbono_Cuenta.Properties.Caption = "Abono A Cuenta"
        Me.chkAbono_Cuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAbono_Cuenta.Size = New System.Drawing.Size(104, 19)
        Me.chkAbono_Cuenta.TabIndex = 75
        Me.chkAbono_Cuenta.Tag = "abono_cuenta"
        Me.chkAbono_Cuenta.ToolTip = "Abono A Cuenta"
        '
        'chkGeneraCheque
        '
        Me.chkGeneraCheque.EditValue = True
        Me.chkGeneraCheque.Location = New System.Drawing.Point(560, 65)
        Me.chkGeneraCheque.Name = "chkGeneraCheque"
        '
        'chkGeneraCheque.Properties
        '
        Me.chkGeneraCheque.Properties.Caption = "Genera Cheque"
        Me.chkGeneraCheque.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkGeneraCheque.Size = New System.Drawing.Size(104, 19)
        Me.chkGeneraCheque.TabIndex = 76
        Me.chkGeneraCheque.Tag = ""
        Me.chkGeneraCheque.ToolTip = "Generar  Cheque"
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(8, 184)
        Me.grFacturas.MainView = Me.grvFacturas
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.grFacturas.Size = New System.Drawing.Size(656, 224)
        Me.grFacturas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFacturas.TabIndex = 77
        Me.grFacturas.Text = "GridControl1"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcConcepto, Me.grcFolio, Me.grcFecha, Me.grcFechaVencimiento, Me.grcImporte, Me.grcSaldo, Me.grcEntrada, Me.grcFolioMovimiento, Me.grcImportePagar})
        Me.grvFacturas.GridControl = Me.grFacturas
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.Caption = "Seleccionar"
        Me.grcSeleccionar.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.HeaderStyleName = "Style1"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 70
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Factura"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.HeaderStyleName = "Style1"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 2
        Me.grcFolio.Width = 77
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.HeaderStyleName = "Style1"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 93
        '
        'grcFechaVencimiento
        '
        Me.grcFechaVencimiento.Caption = "Fecha Vencimiento"
        Me.grcFechaVencimiento.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaVencimiento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaVencimiento.FieldName = "fecha_vencimiento"
        Me.grcFechaVencimiento.HeaderStyleName = "Style1"
        Me.grcFechaVencimiento.Name = "grcFechaVencimiento"
        Me.grcFechaVencimiento.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaVencimiento.VisibleIndex = 4
        Me.grcFechaVencimiento.Width = 113
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "total"
        Me.grcImporte.HeaderStyleName = "Style1"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 5
        Me.grcImporte.Width = 82
        '
        'grcSaldo
        '
        Me.grcSaldo.Caption = "Saldo"
        Me.grcSaldo.DisplayFormat.FormatString = "c2"
        Me.grcSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldo.FieldName = "saldo"
        Me.grcSaldo.HeaderStyleName = "Style1"
        Me.grcSaldo.Name = "grcSaldo"
        Me.grcSaldo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldo.VisibleIndex = 6
        Me.grcSaldo.Width = 108
        '
        'grcEntrada
        '
        Me.grcEntrada.Caption = "Entrada"
        Me.grcEntrada.FieldName = "entrada"
        Me.grcEntrada.Name = "grcEntrada"
        Me.grcEntrada.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFolioMovimiento
        '
        Me.grcFolioMovimiento.Caption = "Folio Movimiento"
        Me.grcFolioMovimiento.FieldName = "folio_movimiento"
        Me.grcFolioMovimiento.Name = "grcFolioMovimiento"
        Me.grcFolioMovimiento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolioMovimiento.VisibleIndex = 1
        Me.grcFolioMovimiento.Width = 99
        '
        'grcImportePagar
        '
        Me.grcImportePagar.Caption = "Saldo"
        Me.grcImportePagar.FieldName = "total_pagar"
        Me.grcImportePagar.Name = "grcImportePagar"
        Me.grcImportePagar.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(385, 41)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(64, 19)
        Me.clcFolio.TabIndex = 78
        Me.clcFolio.Tag = "folio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(344, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 16)
        Me.Label2.TabIndex = 79
        Me.Label2.Tag = ""
        Me.Label2.Text = "Folio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbrImpresionPago
        '
        Me.tbrImpresionPago.Text = "Reimprimir Reporte de Pago"
        '
        'lblConceptoMovimientoChequera
        '
        Me.lblConceptoMovimientoChequera.AutoSize = True
        Me.lblConceptoMovimientoChequera.Location = New System.Drawing.Point(41, 90)
        Me.lblConceptoMovimientoChequera.Name = "lblConceptoMovimientoChequera"
        Me.lblConceptoMovimientoChequera.Size = New System.Drawing.Size(60, 16)
        Me.lblConceptoMovimientoChequera.TabIndex = 80
        Me.lblConceptoMovimientoChequera.Text = "Co&ncepto:"
        '
        'lkpConceptoMovimiento
        '
        Me.lkpConceptoMovimiento.AllowAdd = False
        Me.lkpConceptoMovimiento.AutoReaload = False
        Me.lkpConceptoMovimiento.DataSource = Nothing
        Me.lkpConceptoMovimiento.DefaultSearchField = ""
        Me.lkpConceptoMovimiento.DisplayMember = "descripcion"
        Me.lkpConceptoMovimiento.EditValue = Nothing
        Me.lkpConceptoMovimiento.Filtered = False
        Me.lkpConceptoMovimiento.InitValue = Nothing
        Me.lkpConceptoMovimiento.Location = New System.Drawing.Point(112, 88)
        Me.lkpConceptoMovimiento.MultiSelect = False
        Me.lkpConceptoMovimiento.Name = "lkpConceptoMovimiento"
        Me.lkpConceptoMovimiento.NullText = ""
        Me.lkpConceptoMovimiento.PopupWidth = CType(400, Long)
        Me.lkpConceptoMovimiento.Required = False
        Me.lkpConceptoMovimiento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoMovimiento.SearchMember = ""
        Me.lkpConceptoMovimiento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoMovimiento.SelectAll = False
        Me.lkpConceptoMovimiento.Size = New System.Drawing.Size(144, 20)
        Me.lkpConceptoMovimiento.TabIndex = 81
        Me.lkpConceptoMovimiento.Tag = "concepto_movimiento"
        Me.lkpConceptoMovimiento.ToolTip = "Concepto del Movimiento de Chequera"
        Me.lkpConceptoMovimiento.ValueMember = "concepto"
        '
        'frmPagosVariasFacturas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(674, 416)
        Me.Controls.Add(Me.lkpConceptoMovimiento)
        Me.Controls.Add(Me.lblConceptoMovimientoChequera)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.chkGeneraCheque)
        Me.Controls.Add(Me.chkAbono_Cuenta)
        Me.Controls.Add(Me.clcTotal)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.clcCheque)
        Me.Controls.Add(Me.lblCheque)
        Me.Controls.Add(Me.clcBanco)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.txtNombreBanco)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.lblCuenta_Bancaria)
        Me.Controls.Add(Me.lkpChequera)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblDrescripcionConcepto)
        Me.Controls.Add(Me.lkpConcepto)
        Me.Controls.Add(Me.lblConcepto)
        Me.Name = "frmPagosVariasFacturas"
        Me.Text = "frmPagosVariasFacturas"
        Me.Controls.SetChildIndex(Me.lblConcepto, 0)
        Me.Controls.SetChildIndex(Me.lkpConcepto, 0)
        Me.Controls.SetChildIndex(Me.lblDrescripcionConcepto, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpChequera, 0)
        Me.Controls.SetChildIndex(Me.lblCuenta_Bancaria, 0)
        Me.Controls.SetChildIndex(Me.lblBanco, 0)
        Me.Controls.SetChildIndex(Me.txtNombreBanco, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        Me.Controls.SetChildIndex(Me.clcBanco, 0)
        Me.Controls.SetChildIndex(Me.lblCheque, 0)
        Me.Controls.SetChildIndex(Me.clcCheque, 0)
        Me.Controls.SetChildIndex(Me.lblTotal, 0)
        Me.Controls.SetChildIndex(Me.clcTotal, 0)
        Me.Controls.SetChildIndex(Me.chkAbono_Cuenta, 0)
        Me.Controls.SetChildIndex(Me.chkGeneraCheque, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblConceptoMovimientoChequera, 0)
        Me.Controls.SetChildIndex(Me.lkpConceptoMovimiento, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbono_Cuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkGeneraCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"

    Private oPagoFacturasProveedores As VillarrealBusiness.clsPagoFacturasProveedores
    Private oPagoFacturasProveedoresDetalle As VillarrealBusiness.clsPagoFacturasProveedoresDetalle

    Private oMovimientosPagar As VillarrealBusiness.clsMovimientosPagar
    Private oChequeras As VillarrealBusiness.clsChequeras
    Private oConceptosCxp As VillarrealBusiness.clsConceptosCxp
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private ovariables As New VillarrealBusiness.clsVariables
    Private oMovimientosChequera As VillarrealBusiness.clsMovimientosChequera
    Private oConceptosMovimientosChequera As VillarrealBusiness.clsConceptosMovimientosChequera
    Private oPolizasCheques As VillarrealBusiness.clsPolizasCheques

    Private Folio_Poliza_Cheque As Long = 0

    Private dImporte_anterior As Double = 0
    Private ActualizarCheque As Boolean
    Private dSaldo_actual As Double = 0

    Private EntroDisplay As Boolean
    Private bImprimirCheque As Boolean = False


    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property
    Private ReadOnly Property Concepto() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConcepto)
        End Get
    End Property
    Private ReadOnly Property Banco() As Object
        Get
            If Me.lkpChequera.EditValue Is Nothing Then
                Return System.DBNull.Value
            Else
                Return Me.lkpChequera.GetValue("banco")
            End If
        End Get
    End Property

    Private ReadOnly Property Chequera() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpChequera)
        End Get
    End Property
    Private ReadOnly Property Concepto_Pago_varias_Facturas() As String
        Get
            Return ovariables.TraeDatos("concepto_cxp_pago_varias_facturas", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property Impuesto() As Double
        Get
            Return ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float)
        End Get
    End Property
    ReadOnly Property FolioChequera(ByVal cuenta_bancaria As String) As Long
        Get
            Return CType(oChequeras.DespliegaDatos(cuenta_bancaria).Value, DataSet).Tables(0).Rows(0).Item("folio") + 1
        End Get
    End Property
    Private ReadOnly Property FilasSeleccionadas() As Long
        Get

            Dim i As Long
            Dim contador As Long
            Me.grvFacturas.CloseEditor()
            Me.grvFacturas.UpdateCurrentRow()


            For i = 0 To Me.grvFacturas.RowCount - 1
                If Me.grvFacturas.GetRowCellValue(i, Me.grcSeleccionar) = True Then
                    contador = contador + 1
                End If
            Next

            Return contador
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmPagosVariasFacturas_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmPagosVariasFacturas_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub
    Private Sub frmPagosVariasFacturas_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmPagosVariasFacturas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Guardar(Response)
        If Response.ErrorFound Then
            bImprimirCheque = False
        End If
    End Sub
    Private Sub frmPagosVariasFacturas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oPagoFacturasProveedores = New VillarrealBusiness.clsPagoFacturasProveedores
        oPagoFacturasProveedoresDetalle = New VillarrealBusiness.clsPagoFacturasProveedoresDetalle
        oMovimientosPagar = New VillarrealBusiness.clsMovimientosPagar
        oChequeras = New VillarrealBusiness.clsChequeras
        oConceptosCxp = New VillarrealBusiness.clsConceptosCxp
        oProveedores = New VillarrealBusiness.clsProveedores
        oMovimientosChequera = New VillarrealBusiness.clsMovimientosChequera
        oConceptosMovimientosChequera = New VillarrealBusiness.clsConceptosMovimientosChequera
        oPolizasCheques = New VillarrealBusiness.clsPolizasCheques

        Me.lkpConcepto.EditValue = Concepto_Pago_varias_Facturas
        Me.dteFecha.DateTime = CDate(TINApp.FechaServidor)

        If Concepto_Pago_varias_Facturas.Trim.Length = 0 Then
            Me.tbrTools.Buttons(0).Enabled = False
            Me.tbrTools.Buttons(0).Visible = False
            ShowMessage(MessageType.MsgInformation, "El Concepto de CXP de Pago a Varias Facturas No Est� Definido", "Variables del Sistema", Nothing, False)
        End If

        Select Case Action
            Case Actions.Insert
                Me.tbrImpresionPago.Enabled = False
                Me.tbrImpresionPago.Visible = False
            Case Actions.Update
                Me.grFacturas.Enabled = False

        End Select

    End Sub
    Private Sub frmPagosVariasFacturas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim intControl As Long
        Response = oPagoFacturasProveedores.Validacion_PagosVariasFacturas(Action, Chequera, Proveedor, FilasSeleccionadas, lkpConceptoMovimiento.EditValue, intControl)
        Select Case intControl
            Case 1
                lkpChequera.Focus()
            Case 2
                lkpProveedor.Focus()
            Case 3
                lkpConceptoMovimiento.Focus()
        End Select
    End Sub
    Private Sub frmPagosVariasFacturas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim odataset As DataSet
        Response = oPagoFacturasProveedores.DespliegaDatos(OwnerForm.Value("folio"), OwnerForm.Value("concepto"))
        If Not Response.ErrorFound Then
            EntroDisplay = True
            odataset = Response.Value
            Me.DataSource = odataset
            If Me.clcCheque.Value > 0 Then
                Me.chkGeneraCheque.Checked = True
            End If

            Response = oPagoFacturasProveedoresDetalle.Listado(OwnerForm.Value("folio"), OwnerForm.Value("concepto"))
            If Not Response.ErrorFound Then
                odataset = Response.Value
                Me.grFacturas.DataSource = odataset.Tables(0)
            End If
        End If

    End Sub
    Private Sub frmPagosVariasFacturas_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If bImprimirCheque Then
            Me.ImprimirCheque(Me.Chequera, Me.clcCheque.Value)
            ImprimeReporte(Me.clcCheque.Value)
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    Private Sub lkpChequera_Format() Handles lkpChequera.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpChequera)
    End Sub
    Private Sub lkpChequera_LoadData(ByVal Initialize As Boolean) Handles lkpChequera.LoadData
        Dim response As Events
        response = oChequeras.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpChequera.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpChequera_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpChequera.EditValueChanged
        If Me.lkpChequera.EditValue Is Nothing Then Exit Sub

        Me.txtNombreBanco.Text = CStr(Me.lkpChequera.GetValue("nombre_banco"))
        Me.clcBanco.Value = Me.Banco

    End Sub

    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpProveedor_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpProveedor.EditValueChanged
        LlenaGrid()
    End Sub

    Private Sub lkpConcepto_Format() Handles lkpConcepto.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConcepto)
    End Sub
    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpConcepto.LoadData
        Dim response As Events
        response = oConceptosCxp.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConcepto.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpConcepto_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConcepto.EditValueChanged
        If Me.lkpConcepto.EditValue Is Nothing Then Exit Sub
        Me.lblDrescripcionConcepto.Text = CStr(Me.lkpConcepto.GetValue("descripcion"))

    End Sub

    Private Sub grvFacturas_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvFacturas.CellValueChanging
        Me.grvFacturas.UpdateCurrentRow()
        Dim i As Long
        Dim response As Events
        Dim ImporteTotal As Double
        ImporteTotal = 0

        If e.Column Is Me.grcSeleccionar Then

            For i = 0 To Me.grvFacturas.RowCount - 1
                If e.RowHandle = i And e.Value = True Then
                    ImporteTotal = ImporteTotal + Me.grvFacturas.GetRowCellValue(i, Me.grcImportePagar)
                Else
                    If e.RowHandle <> i And grvFacturas.GetRowCellValue(i, Me.grcSeleccionar) = True Then
                        ImporteTotal = ImporteTotal + Me.grvFacturas.GetRowCellValue(i, Me.grcImportePagar)
                    End If
                End If
            Next

            Me.clcTotal.Value = ImporteTotal
            ImporteTotal = 0
        End If


    End Sub
    Public Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

        If e.Button Is Me.tbrImpresionPago Then
            ImprimeReporte(Me.clcCheque.Value)
        End If
    End Sub

#Region "Lookup Concepto Movimiento"
    Private Sub lkpConceptoMovimiento_InitData(ByVal Value As Object) Handles lkpConceptoMovimiento.InitData
        Dim response As Events

        response = oConceptosMovimientosChequera.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoMovimiento.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub


    Private Sub lkpConceptoMovimiento_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoMovimiento.LoadData
        Dim response As Events

        response = oConceptosMovimientosChequera.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoMovimiento.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpConceptoMovimiento_Format() Handles lkpConceptoMovimiento.Format
        for_conceptos_movimientos_chequera_grl(Me.lkpConceptoMovimiento)
    End Sub
#End Region

#End Region

#Region "DIPROS Systems, funcionalidad"
    Private Sub LlenaGrid()
        If Me.EntroDisplay = True Then Exit Sub
        Dim response As Events
        response = Me.oMovimientosPagar.llena_pago_varias_facturas(Proveedor)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.grFacturas.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub Guardar(ByRef response As Events)
        Dim i As Long
        Dim seleccionar As Boolean = False
        Dim importe_factura_detalle As Double
        Dim entrada As Long
        Dim concepto_factura As String
        Dim folio_referencia As Long
        Dim folio_factura As Long
        Dim Folio_chequera As Long
        Dim strPago As String = "Facturas: "
        Dim contadorfacturas As Long = 0


        Dim tipo_pago As Char


        'Genero la Cadena para las Facturas
        For i = 0 To Me.grvFacturas.RowCount - 1
            folio_factura = Me.grvFacturas.GetRowCellValue(i, Me.grcFolioMovimiento)
            If Me.grvFacturas.GetRowCellValue(i, Me.grcSeleccionar) = True Then
                contadorfacturas = contadorfacturas + 1
                If contadorfacturas = 1 Then
                    strPago = strPago + folio_factura.ToString
                Else
                    strPago = strPago + ", " + folio_factura.ToString
                End If
            End If

        Next

        ' Verifico Si hay que Imprimir el Cheque y que no tenga algun folio ya asignado
        If Me.clcCheque.Value = 0 And Me.chkGeneraCheque.Checked Then
            'asigno el valor al control para que lo actualice
            Me.clcCheque.Value = Me.FolioChequera(Me.lkpChequera.EditValue)
            ActualizarCheque = False
            'Actualizo la Bandera para imprimir el Cheque
            bImprimirCheque = True
            tipo_pago = "C" 'Tipo  Pago Cheque

        Else
            ActualizarCheque = True
            tipo_pago = ""
        End If


        Select Case Action
            Case Actions.Insert
                ' Paso 1 .- Pagos a Facturas de Proveedores
                response = oPagoFacturasProveedores.Insertar(Me.clcFolio.EditValue, Concepto, Me.dteFecha.DateTime, Banco, Me.Chequera, Me.clcCheque.Value, Me.clcTotal.Value, Proveedor, False, Me.chkAbono_Cuenta.Checked)

                If Not response.ErrorFound Then
                    ' Paso 2 .- Genero el Movimiento a la Chequera por el total del pago
                    '@ACH-26/06/07: Se agreg� el campo concepto_movimiento en la inserci�n
                    response = oMovimientosChequera.Insertar(Banco, Me.Chequera, Folio_chequera, Me.dteFecha.DateTime, strPago, Me.clcTotal.Value, "C", Me.chkGeneraCheque.Checked, Me.lkpProveedor.Text, Me.clcCheque.Value, Me.chkAbono_Cuenta.Checked, TINApp.Connection.User, TINApp.Connection.User, "Pago a Varias Facturas", dSaldo_actual, ActualizarCheque, Me.clcFolio.EditValue, Me.lkpConceptoMovimiento.EditValue, 0)
                    '/@ACH-26/06/07
                End If


            Case Actions.Delete
                ' Paso 1 .- Pagos a Facturas de Proveedores
                response = oPagoFacturasProveedores.Eliminar(Me.clcFolio.EditValue, Concepto)

                If Not response.ErrorFound Then
                    ' Paso 2 .- Genero el Movimiento a la Chequera por el total del pago
                    response = oMovimientosChequera.Eliminar(clcBanco.Value, Chequera, clcFolio.Value, dSaldo_actual)
                End If
        End Select

        If response.ErrorFound Then Exit Sub

        Dim folio_pagar As Long

        For i = 0 To Me.grvFacturas.RowCount - 1
            entrada = Me.grvFacturas.GetRowCellValue(i, Me.grcEntrada)
            concepto_factura = Me.grvFacturas.GetRowCellValue(i, Me.grcConcepto)
            folio_referencia = Me.grvFacturas.GetRowCellValue(i, Me.grcFolioMovimiento)
            seleccionar = Me.grvFacturas.GetRowCellValue(i, Me.grcSeleccionar)
            importe_factura_detalle = Me.grvFacturas.GetRowCellValue(i, Me.grcSaldo)

            If seleccionar = True And response.ErrorFound = False Then

                'Paso 3.-  Genera un pago para cada uno de los movimientos por pagar para que el trigger los salde
                '           automaticamente por medio de sql
                Select Case Action
                    Case Actions.Insert
                        'ObtenerFolioPolizaCheque(response)
                        response = oMovimientosPagar.Insertar(Concepto, folio_pagar, Proveedor, 0, Me.dteFecha.DateTime, Me.dteFecha.DateTime, 0, concepto_factura, folio_referencia, False, 0, importe_factura_detalle, importe_factura_detalle - CalculaIva(importe_factura_detalle), CalculaIva(importe_factura_detalle), importe_factura_detalle, 0, tipo_pago, Me.Banco, Me.Chequera, Me.clcCheque.Value, "Pago a Varias Facturas", TINApp.Connection.User, TINApp.Connection.User, "", Concepto + "-" + folio_referencia.ToString, Me.chkAbono_Cuenta.EditValue, Me.clcFolio.EditValue, Me.Folio_Poliza_Cheque)

                        'Paso 4.- Genero el Detalle del Pago a Varias Facturas
                        If Not response.ErrorFound Then
                            response = oPagoFacturasProveedoresDetalle.Insertar(Me.clcFolio.EditValue, Concepto, 0, folio_referencia, concepto_factura, importe_factura_detalle)
                        End If

                    Case Actions.Delete
                        'Cancelo un Pago por cada uno de los movimientos 
                        response = oMovimientosPagar.Eliminar(Concepto, clcFolio.Value)

                End Select


            End If
        Next i
    End Sub
    Private Function CalculaIva(ByVal Total As Double) As Double
        CalculaIva = Total * (Impuesto / 100)
    End Function
    Private Sub ImprimirCheque(ByVal cuentabancaria As String, ByVal folio_cheque As Integer)
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            response = oReportes.ImprimeChequePagoFacturasProveedores(Me.clcBanco.EditValue, cuentabancaria, folio_cheque)
            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Cheque no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptCheque

                    oDataSet = response.Value

                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Cheques", oReport)
                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

        oReportes = Nothing
        response = Nothing
    End Sub
    Private Sub ImprimeReporte(ByVal folio_cheque As Long)
        Dim response As New Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            response = oReportes.ImprimeReportePagoFacturas(Me.dteFecha.DateTime, folio_cheque)
            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptImpresionFacturasPagadas

                    oDataSet = response.Value


                    oReport.DataSource = oDataSet.Tables(0)
                    oReport.oDatatable = oDataSet.Tables(1)
                    TINApp.ShowReport(Me.MdiParent, "Reporte de Facturas Pagadas", oReport, True, , , False)
                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

        oReportes = Nothing
        response = Nothing
    End Sub

    'Private Function ObtenerFolioPolizaCheque(ByRef response As Dipros.Utils.Events) As Long
    '    response = Me.oPolizasCheques.ObtenerSiguienteFolio
    '    If Not response.ErrorFound Then
    '        Me.Folio_Poliza_Cheque = response.Value + 1
    '    End If
    'End Function

#End Region






End Class

