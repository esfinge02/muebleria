Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptEdoCuentaProveedorAmpliado
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghCliente As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghampliado As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfampliado As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfCliente As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape5 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblProveedor As DataDynamics.ActiveReports.Label = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtDireccion As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCiudad As DataDynamics.ActiveReports.Label = Nothing
	Private txtCiudad As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTelefonos As DataDynamics.ActiveReports.Label = Nothing
	Private txtTelefonos As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblRfc As DataDynamics.ActiveReports.Label = Nothing
	Private txtRfc As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private domicilio_proveedor1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private txtestado As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescripcionCorta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCantidad As DataDynamics.ActiveReports.Label = Nothing
	Private lblCosto As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Line15 As DataDynamics.ActiveReports.Line = Nothing
	Private Line16 As DataDynamics.ActiveReports.Line = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldo_Venta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "CxP.rptEdoCuentaProveedorAmpliado.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghCliente = CType(Me.Sections("ghCliente"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghampliado = CType(Me.Sections("ghampliado"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfampliado = CType(Me.Sections("gfampliado"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfCliente = CType(Me.Sections("gfCliente"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape5 = CType(Me.ghCliente.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.lblProveedor = CType(Me.ghCliente.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtProveedor = CType(Me.ghCliente.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDomicilio = CType(Me.ghCliente.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtDireccion = CType(Me.ghCliente.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblCiudad = CType(Me.ghCliente.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtCiudad = CType(Me.ghCliente.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.lblTelefonos = CType(Me.ghCliente.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtTelefonos = CType(Me.ghCliente.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.lblRfc = CType(Me.ghCliente.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtRfc = CType(Me.ghCliente.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.Label13 = CType(Me.ghCliente.Controls(11),DataDynamics.ActiveReports.Label)
		Me.domicilio_proveedor1 = CType(Me.ghCliente.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.ghCliente.Controls(13),DataDynamics.ActiveReports.Label)
		Me.txtestado = CType(Me.ghCliente.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.lblArticulo = CType(Me.ghCliente.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblDescripcionCorta = CType(Me.ghCliente.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblCantidad = CType(Me.ghCliente.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblCosto = CType(Me.ghCliente.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.ghCliente.Controls(19),DataDynamics.ActiveReports.Label)
		Me.TextBox1 = CType(Me.ghCliente.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.ghCliente.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.TextBox3 = CType(Me.ghCliente.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.TextBox4 = CType(Me.ghCliente.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.Label26 = CType(Me.ghCliente.Controls(24),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.ghCliente.Controls(25),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.ghCliente.Controls(26),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.ghCliente.Controls(27),DataDynamics.ActiveReports.Label)
		Me.Line15 = CType(Me.ghCliente.Controls(28),DataDynamics.ActiveReports.Line)
		Me.Line16 = CType(Me.ghCliente.Controls(29),DataDynamics.ActiveReports.Line)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.importe = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldo_Venta = CType(Me.gfampliado.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptEdoCuentaProveedorAmpliado_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
