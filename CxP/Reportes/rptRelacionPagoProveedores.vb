Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptRelacionPagoProveedores
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghAnio As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghMes As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghProveedor As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfProveedor As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfMes As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfAnio As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape2 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblFechaFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolioFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaPago As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoAPagar As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Private txtAnio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtMes As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombre As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaPago As DataDynamics.ActiveReports.TextBox = Nothing
	Private Foliofac As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo_a_pagar As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTotalProveedor As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalProveedorMes As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "CxP.rptRelacionPagoProveedores.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghAnio = CType(Me.Sections("ghAnio"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghMes = CType(Me.Sections("ghMes"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghProveedor = CType(Me.Sections("ghProveedor"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfProveedor = CType(Me.Sections("gfProveedor"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfMes = CType(Me.Sections("gfMes"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfAnio = CType(Me.Sections("gfAnio"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Shape2 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblFechaFactura = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblFolioFactura = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblFechaPago = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblSaldoAPagar = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblBodega = CType(Me.ghAnio.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtAnio = CType(Me.ghAnio.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtMes = CType(Me.ghMes.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtProveedor = CType(Me.ghProveedor.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombre = CType(Me.ghProveedor.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaFactura = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaPago = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Foliofac = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.saldo_a_pagar = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblTotalProveedor = CType(Me.gfProveedor.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalProveedorMes = CType(Me.gfProveedor.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalProveedor = CType(Me.gfMes.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.gfMes.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.gfAnio.Controls(0),DataDynamics.ActiveReports.Label)
		Me.importe1 = CType(Me.gfAnio.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region
    Public oDesglosado As Boolean = True
 

    Private Sub rptRelacionPagoProveedores_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.gfProveedor.Visible = oDesglosado
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
