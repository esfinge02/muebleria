Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptMovimientosChequera
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghBanco As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghChequera As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfChequera As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private lblConcepto As DataDynamics.ActiveReports.Label = Nothing
	Private lblCheque As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private lblAbonos As DataDynamics.ActiveReports.Label = Nothing
	Private lblCargos As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblBanco As DataDynamics.ActiveReports.Label = Nothing
	Private txtBanco As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreBanco As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblChequera As DataDynamics.ActiveReports.Label = Nothing
	Private txtChequera As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSaldoEnLibros As DataDynamics.ActiveReports.Label = Nothing
	Private txtSaldoEnLibros As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTipos As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtConcepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCheque As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEstatus As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtAbonos As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNumeroSeguimientos As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSumChequera As DataDynamics.ActiveReports.Label = Nothing
	Private txtSumatoriaSaldoChequera As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSaldoBanco As DataDynamics.ActiveReports.Label = Nothing
	Private txtSumatoriaSaldoBanco As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoTotal As DataDynamics.ActiveReports.Label = Nothing
	Private txtSaldoTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "CxP.rptMovimientosChequera.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghBanco = CType(Me.Sections("ghBanco"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghChequera = CType(Me.Sections("ghChequera"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfChequera = CType(Me.Sections("gfChequera"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape1 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.Label11 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblConcepto = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblCheque = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblAbonos = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblCargos = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblSaldo = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblBanco = CType(Me.ghBanco.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtBanco = CType(Me.ghBanco.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreBanco = CType(Me.ghBanco.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblChequera = CType(Me.ghChequera.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtChequera = CType(Me.ghChequera.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblSaldoEnLibros = CType(Me.ghChequera.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtSaldoEnLibros = CType(Me.ghChequera.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTipos = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtConcepto = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtCheque = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtEstatus = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtAbonos = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtNumeroSeguimientos = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldo = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.lblSumChequera = CType(Me.gfChequera.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtSumatoriaSaldoChequera = CType(Me.gfChequera.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblSaldoBanco = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtSumatoriaSaldoBanco = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblSaldoTotal = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtSaldoTotal = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Dim saldo_total As Double

    Private Sub GroupFooter1_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupFooter1.Format
        saldo_total = saldo_total + txtSumatoriaSaldoBanco.Value
    End Sub

    Private Sub ReportFooter_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportFooter.Format

    End Sub

    Private Sub ReportFooter_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportFooter.BeforePrint
        txtSaldoTotal.Value = saldo_total
    End Sub

    Private Sub rptMovimientosChequera_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
