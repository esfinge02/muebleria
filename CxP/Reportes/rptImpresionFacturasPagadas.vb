Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptImpresionFacturasPagadas
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private proveedor As DataDynamics.ActiveReports.Label = Nothing
	Private direccion As DataDynamics.ActiveReports.TextBox = Nothing
	Private colonia As DataDynamics.ActiveReports.TextBox = Nothing
	Private ciudad_estado As DataDynamics.ActiveReports.TextBox = Nothing
	Private fax As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private txtValorCheque As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private subMenos As DataDynamics.ActiveReports.SubReport = Nothing
	Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private txtGrandTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "CxP.rptImpresionFacturasPagadas.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.proveedor = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.direccion = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.colonia = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.ciudad_estado = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.fax = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.TextBox1 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label3 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label4 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtValorCheque = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.TextBox3 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.TextBox4 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label26 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Label)
		Me.TextBox7 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.subMenos = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.SubReport)
		Me.TextBox6 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label7 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Line)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(7),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageFooter.Controls(9),DataDynamics.ActiveReports.Line)
		Me.Label27 = CType(Me.PageFooter.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageFooter.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtGrandTotal = CType(Me.PageFooter.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label29 = CType(Me.PageFooter.Controls(13),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Public oDatatable As DataTable

    Private Sub PageFooter_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageFooter.Format
        Me.subMenos.Report = New rptImpresionFacturasPagadasSub
        CType(subMenos.Report, rptImpresionFacturasPagadasSub).DataSource = oDatatable

        Me.txtGrandTotal.Value = Me.txtValorCheque.Value
    End Sub

    Private Sub rptImpresionFacturasPagadas_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
