Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptPagosFechaVencimiento
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghTipoMovimiento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghcorte2 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfcorte2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfTipoMovimiento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnombre_concepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private n_col_41 As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_11 As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_31 As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_21 As DataDynamics.ActiveReports.Label = Nothing
	Private Shape2 As DataDynamics.ActiveReports.Shape = Nothing
	Private importe As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio_referencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_concepto1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private txtGranTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "CxP.rptPagosFechaVencimiento.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghTipoMovimiento = CType(Me.Sections("ghTipoMovimiento"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghcorte2 = CType(Me.Sections("ghcorte2"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfcorte2 = CType(Me.Sections("gfcorte2"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfTipoMovimiento = CType(Me.Sections("gfTipoMovimiento"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblBodega = CType(Me.ghTipoMovimiento.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtBodega = CType(Me.ghTipoMovimiento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtnombre_concepto = CType(Me.ghcorte2.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.n_col_41 = CType(Me.ghcorte2.Controls(1),DataDynamics.ActiveReports.Label)
		Me.n_col_11 = CType(Me.ghcorte2.Controls(2),DataDynamics.ActiveReports.Label)
		Me.n_col_31 = CType(Me.ghcorte2.Controls(3),DataDynamics.ActiveReports.Label)
		Me.n_col_21 = CType(Me.ghcorte2.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Shape2 = CType(Me.ghcorte2.Controls(5),DataDynamics.ActiveReports.Shape)
		Me.importe = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio_referencia = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.nombre_concepto1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.gfcorte2.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalGrupo = CType(Me.gfcorte2.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label5 = CType(Me.gfTipoMovimiento.Controls(0),DataDynamics.ActiveReports.Label)
		Me.importe1 = CType(Me.gfTipoMovimiento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Label6 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtGranTotal = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptPagosFechaVencimiento_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
