Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptBeneficiarios
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents rhReporte As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gpBeneficiario As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents ghBeneficiario As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents rfReporte As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblFechaCheque As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolio As DataDynamics.ActiveReports.Label = Nothing
	Private lblConcepto As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblBanco As DataDynamics.ActiveReports.Label = Nothing
	Private lblCuenta As DataDynamics.ActiveReports.Label = Nothing
	Private txtBeneficiario As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaMovimiento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtConceptoMovimientoChequera As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBanco As DataDynamics.ActiveReports.TextBox = Nothing
	Private banco_nombre1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteBeneficiario As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblImporteBeneficiario As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTotalImporte As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "CxP.rptBeneficiarios.rpx")
		Me.rhReporte = CType(Me.Sections("rhReporte"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gpBeneficiario = CType(Me.Sections("gpBeneficiario"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.ghBeneficiario = CType(Me.Sections("ghBeneficiario"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.rfReporte = CType(Me.Sections("rfReporte"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblFechaTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Picture)
		Me.lblFechaCheque = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblFolio = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblConcepto = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblBanco = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblCuenta = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtBeneficiario = CType(Me.gpBeneficiario.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaMovimiento = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtConceptoMovimientoChequera = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtBanco = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.banco_nombre1 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteBeneficiario = CType(Me.ghBeneficiario.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblImporteBeneficiario = CType(Me.ghBeneficiario.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtTotalImporte = CType(Me.rfReporte.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblTotalImporte = CType(Me.rfReporte.Controls(1),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptBeneficiarios_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
