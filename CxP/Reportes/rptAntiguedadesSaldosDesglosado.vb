Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptAntiguedadesSaldosDesglosado
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblSaldoActual As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoVencido As DataDynamics.ActiveReports.Label = Nothing
	Private lbl1_30 As DataDynamics.ActiveReports.Label = Nothing
	Private lblProveedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblNombre As DataDynamics.ActiveReports.Label = Nothing
	Private lbl_46_60 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private lbl_31_45 As DataDynamics.ActiveReports.Label = Nothing
	Private lbl61 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private Label35 As DataDynamics.ActiveReports.Label = Nothing
	Private Label36 As DataDynamics.ActiveReports.Label = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtguion As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private abono As DataDynamics.ActiveReports.TextBox = Nothing
	Private cargo As DataDynamics.ActiveReports.TextBox = Nothing
	Private total As DataDynamics.ActiveReports.TextBox = Nothing
	Private concepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtentrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoVencido As DataDynamics.ActiveReports.TextBox = Nothing
	Private txt1_30 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txt_31_45 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txt_46_60 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s46_601 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo_actual3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo_vencido3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s1_303 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s31_453 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s46_604 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s61_mas3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label37 As DataDynamics.ActiveReports.Label = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "CxP.rptAntiguedadesSaldosDesglosado.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader2 = CType(Me.Sections("GroupHeader2"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter2 = CType(Me.Sections("GroupFooter2"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape1 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblSaldoActual = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblSaldoVencido = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lbl1_30 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblProveedor = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblNombre = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lbl_46_60 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Line)
		Me.lbl_31_45 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lbl61 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label32 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Label33 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Label34 = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.Label35 = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.Label36 = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Label)
		Me.txtProveedor = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.GroupHeader1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtguion = CType(Me.GroupHeader1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.saldo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.abono = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.cargo = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.total = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.concepto = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtentrada = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtfolio = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreArticulo = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoVencido = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txt1_30 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txt_31_45 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txt_46_60 = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.s46_601 = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.saldo_actual3 = CType(Me.GroupFooter2.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.saldo_vencido3 = CType(Me.GroupFooter2.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.s1_303 = CType(Me.GroupFooter2.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.s31_453 = CType(Me.GroupFooter2.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.s46_604 = CType(Me.GroupFooter2.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.s61_mas3 = CType(Me.GroupFooter2.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Label37 = CType(Me.GroupFooter2.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Line3 = CType(Me.GroupFooter2.Controls(7),DataDynamics.ActiveReports.Line)
		Me.Label27 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptAntiguedadesSaldosDesglosado_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
