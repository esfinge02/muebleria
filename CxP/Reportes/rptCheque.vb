Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptCheque
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCheque As DataDynamics.ActiveReports.Label = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtA As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaActual As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteEnLetra As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtBanco As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCuentaBancaria As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDieccionBanco As DataDynamics.ActiveReports.TextBox = Nothing
	Private lbl As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox9 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox10 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "CxP.rptCheque.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.TextBox6 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblCheque = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtA = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaActual = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label26 = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.TextBox1 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.TextBox3 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteEnLetra = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.txtBanco = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.txtCuentaBancaria = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.txtDieccionBanco = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.lbl = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.TextBox7 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.TextBox8 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.TextBox9 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.TextBox10 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region
End Class
