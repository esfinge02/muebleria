Imports Dipros.Utils.Common
Imports Dipros.Utils


Public Class frmPuntosVenta
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lbl_ePuntoventa As System.Windows.Forms.Label
    Friend WithEvents lblPuntoventa As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPuntosVenta))
        Me.lbl_ePuntoventa = New System.Windows.Forms.Label
        Me.lblPuntoventa = New System.Windows.Forms.Label
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblSerie = New System.Windows.Forms.Label
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(359, 28)
        '
        'lbl_ePuntoventa
        '
        Me.lbl_ePuntoventa.AutoSize = True
        Me.lbl_ePuntoventa.Location = New System.Drawing.Point(31, 45)
        Me.lbl_ePuntoventa.Name = "lbl_ePuntoventa"
        Me.lbl_ePuntoventa.Size = New System.Drawing.Size(94, 16)
        Me.lbl_ePuntoventa.TabIndex = 0
        Me.lbl_ePuntoventa.Tag = ""
        Me.lbl_ePuntoventa.Text = "&Punto de Venta:"
        Me.lbl_ePuntoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPuntoventa
        '
        Me.lblPuntoventa.AutoSize = True
        Me.lblPuntoventa.Location = New System.Drawing.Point(127, 45)
        Me.lblPuntoventa.Name = "lblPuntoventa"
        Me.lblPuntoventa.Size = New System.Drawing.Size(11, 16)
        Me.lblPuntoventa.TabIndex = 1
        Me.lblPuntoventa.Tag = "puntoventa"
        Me.lblPuntoventa.Text = "0"
        Me.lblPuntoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(53, 67)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 4
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(127, 62)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 30
        Me.txtDescripcion.Size = New System.Drawing.Size(189, 22)
        Me.txtDescripcion.TabIndex = 5
        Me.txtDescripcion.Tag = "descripcion"
        Me.txtDescripcion.ToolTip = "descripción"
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(88, 111)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie.TabIndex = 6
        Me.lblSerie.Tag = ""
        Me.lblSerie.Text = "S&erie:"
        Me.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(127, 108)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.MaxLength = 3
        Me.txtSerie.Size = New System.Drawing.Size(33, 22)
        Me.txtSerie.TabIndex = 7
        Me.txtSerie.Tag = "serie"
        Me.txtSerie.ToolTip = "serie"
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(90, 133)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 8
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio.Location = New System.Drawing.Point(127, 131)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.MaxLength = 10
        Me.clcFolio.Properties.NullText = "0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(81, 21)
        Me.clcFolio.TabIndex = 9
        Me.clcFolio.Tag = "folio"
        Me.clcFolio.ToolTip = "folio"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(127, 85)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(132, 22)
        Me.lkpSucursal.TabIndex = 3
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = "sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(69, 89)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmPuntosVenta
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(346, 167)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lbl_ePuntoventa)
        Me.Controls.Add(Me.lblPuntoventa)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.clcFolio)
        Me.Name = "frmPuntosVenta"
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.txtSerie, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblPuntoventa, 0)
        Me.Controls.SetChildIndex(Me.lbl_ePuntoventa, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oPuntosVenta As VillarrealBusiness.clsPuntosVenta
    Private oSucursales As New VillarrealBusiness.clsSucursales
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmPuntosVenta_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oPuntosVenta.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oPuntosVenta.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oPuntosVenta.Eliminar(lblPuntoventa.Text)

        End Select
    End Sub

    Private Sub frmPuntosVenta_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oPuntosVenta.DespliegaDatos(OwnerForm.Value("puntoventa"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmPuntosVenta_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oPuntosVenta = New VillarrealBusiness.clsPuntosVenta

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmPuntosVenta_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oPuntosVenta.Validacion(Action, txtDescripcion.Text, txtSerie.Text, Me.lkpSucursal.EditValue)

        'DAM-11/JUN/08 YA NO SE VALIDA QUE EXISTA EL MISMO NUMERO DE SERIE
        'If Not Response.ErrorFound And (Action = Actions.Insert Or Action = Actions.Update) Then Response = oPuntosVenta.ValidaExisteSerie(CType(lblPuntoventa.Text, Long), txtSerie.Text)
    End Sub

    Private Sub frmPuntosVenta_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub





    Private Sub clcFolio_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcFolio.EditValueChanged
        If clcFolio.Text.Length = 0 Then
            clcFolio.EditValue = 0
        End If
    End Sub
    Private Sub txtDescripcion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oPuntosVenta.ValidaDescripcion(txtDescripcion.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub txtSerie_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSerie.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oPuntosVenta.ValidaSerie(txtSerie.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub








    'Private Sub clcFolio_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcFolio.Validating
    '	Dim oEvent As Dipros.Utils.Events
    '	If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '	oEvent = oPuntosVenta.ValidaFolio(clcFolio.value)
    '	If oEvent.ErrorFound Then
    '		oEvent.ShowError()
    '		e.Cancel = True
    '	End If
    'End Sub



#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region



End Class
