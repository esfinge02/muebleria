Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents mnuCatPuntosVenta As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProVentas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHerVariablesSistema As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem3 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuRepDiarioVentas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDiarioVentasDetallado As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatPuntosVenta = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProVentas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuHerVariablesSistema = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem3 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuRepDiarioVentas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDiarioVentasDetallado = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatPuntosVenta, Me.mnuProVentas, Me.mnuHerVariablesSistema, Me.BarStaticItem3, Me.mnuRepDiarioVentas, Me.mnuRepDiarioVentasDetallado})
        Me.BarManager.MaxItemId = 112
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.FloatLocation = New System.Drawing.Point(563, 44)
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerVariablesSistema, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatPuntosVenta)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProVentas)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDiarioVentas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDiarioVentasDetallado)})
        '
        'mnuCatPuntosVenta
        '
        Me.mnuCatPuntosVenta.Caption = "P&untos de Venta"
        Me.mnuCatPuntosVenta.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatPuntosVenta.Id = 105
        Me.mnuCatPuntosVenta.ImageIndex = 0
        Me.mnuCatPuntosVenta.Name = "mnuCatPuntosVenta"
        '
        'mnuProVentas
        '
        Me.mnuProVentas.Caption = "&Facturaci�n"
        Me.mnuProVentas.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProVentas.Id = 106
        Me.mnuProVentas.ImageIndex = 2
        Me.mnuProVentas.Name = "mnuProVentas"
        '
        'mnuHerVariablesSistema
        '
        Me.mnuHerVariablesSistema.Caption = "&Variables del Sistema"
        Me.mnuHerVariablesSistema.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuHerVariablesSistema.Id = 107
        Me.mnuHerVariablesSistema.Name = "mnuHerVariablesSistema"
        '
        'Bar1
        '
        Me.Bar1.BarName = "Bar2"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(41, 428)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem3)})
        Me.Bar1.Offset = 3
        Me.Bar1.Text = "Bar2"
        '
        'BarStaticItem3
        '
        Me.BarStaticItem3.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem3.Id = 109
        Me.BarStaticItem3.Name = "BarStaticItem3"
        Me.BarStaticItem3.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem3.Width = 32
        '
        'mnuRepDiarioVentas
        '
        Me.mnuRepDiarioVentas.Caption = "Diario de Ventas"
        Me.mnuRepDiarioVentas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDiarioVentas.Id = 110
        Me.mnuRepDiarioVentas.Name = "mnuRepDiarioVentas"
        '
        'mnuRepDiarioVentasDetallado
        '
        Me.mnuRepDiarioVentasDetallado.Caption = "Diario de Ventas Detallado"
        Me.mnuRepDiarioVentasDetallado.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDiarioVentasDetallado.Id = 111
        Me.mnuRepDiarioVentasDetallado.Name = "mnuRepDiarioVentasDetallado"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(824, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "Punto de Venta"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim oPuntosVenta As New VillarrealBusiness.clsPuntosVenta

        Dim response As Dipros.Utils.Events
        response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)



        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = response.Value
            Me.BarStaticItem3.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")

        End If

        response = oPuntosVenta.DespliegaDatos(Comunes.Common.PuntoVenta_Actual)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = response.Value
            Me.BarStaticItem3.Caption = Me.BarStaticItem3.Caption + "        " + "Punto Venta Actual: " + odataset.Tables(0).Rows(0).Item("descripcion")

        End If


    End Sub


    Private Sub frmMain_LoadOptions() Handles MyBase.LoadOptions
        With TINApp.Options.Security
            '---------------------------------------------------------------------------------------------
            .Options("mnuProVentas").AddAction("FECHA_VENTAS", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
        End With
    End Sub
#End Region

#Region "Catalogos"
    Public Sub mnuCatPuntosVenta_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatPuntosVenta.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oPuntosVenta As New VillarrealBusiness.clsPuntosVenta
        With oGrid
            .Title = "Puntos de Venta"
            .UpdateTitle = "un Punto de Venta"
            .DataSource = AddressOf oPuntosVenta.Listado
            .UpdateForm = New frmPuntosVenta
            .Format = AddressOf for_puntos_venta_grs
            .MdiParent = Me
            .Show()
        End With
        oPuntosVenta = Nothing
    End Sub

#End Region

#Region "Procesos"
    Public Sub mnuProVentas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProVentas.ItemClick
        Dim oGrid As New Comunes.brwVentas
        With oGrid
            .MenuOption = e.Item
            .Picture = GetIcon(ilsIcons, e.Item)
            .Title = "Facturaci�n"
            .UpdateTitle = "una Factura"
            .UpdateForm = New Comunes.frmVentas
            .Format = AddressOf Comunes.clsFormato.for_ventas_grs
            .MdiParent = Me
            .Show()
        End With

        'Dim oform As New Comunes.frmVentas
        'oform.Title = "Facturaci�n"
        'oform.MdiParent = Me
        'oform.Action = Actions.Insert
        'oform.CanDock = True
        'oform.Docked = True

        'oform.Show()
    End Sub


    'Public Sub mnuProCotizacion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCotizacion.ItemClick
    '    Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
    '    Dim oCotizaciones As New VillarrealBusiness.clsCotizaciones
    '    With oGrid
    '        .MenuOption = e.Item
    '        .Picture = GetIcon(ilsIcons, e.Item)
    '        .Title = "Cotizaci�n"
    '        .DataSource = AddressOf oCotizaciones.Listado
    '        .UpdateTitle = "una Cotizaci�n"
    '        .UpdateForm = New frmCotizaciones
    '        .Format = AddressOf for_cotizaciones_grs
    '        .MdiParent = Me
    '        .Show()
    '    End With
    'End Sub
#End Region

#Region "Reportes"

    Public Sub mnuRepDiarioVentas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDiarioVentas.ItemClick
        Dim oForm As New Comunes.frmDiarioVentas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Diario de Ventas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepDiarioVentasDetallado_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDiarioVentasDetallado.ItemClick
        Dim oForm As New Comunes.frmDiarioVentasDetallado
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Diario de Ventas Detallado"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

#End Region

#Region "Herramientas"

#End Region


End Class
