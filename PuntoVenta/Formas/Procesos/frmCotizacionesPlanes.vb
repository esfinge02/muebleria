Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCotizacionesPlanes
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Cotizacion As Long
    Dim KS As Keys
    Public sucursal_dependencia As Boolean
    Private oPlanesCredito As New VillarrealBusiness.clsPlanesCredito
    Private oPreciosVenta As New VillarrealBusiness.clsPrecios
    Private oSucursales As New VillarrealBusiness.clsSucursales

    'Private dEnganche As Double = 0

    'Private Property Enganche() As Double
    '    Get
    '        Return dEnganche
    '    End Get
    '    Set(ByVal Value As Double)
    '        dEnganche = Value
    '    End Set
    'End Property

    'Private ReadOnly Property Porcentaje_Enganche() As Double
    '    Get
    '        Return Enganche / 100
    '    End Get
    'End Property

    Private ReadOnly Property Plan() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpPlan_Credito)
        End Get
    End Property

#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblPlan_Credito As System.Windows.Forms.Label
    Friend WithEvents lkpPlan_Credito As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblEnganche As System.Windows.Forms.Label
    Friend WithEvents clcEnganche As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPlazo As System.Windows.Forms.Label
    Friend WithEvents clcPlazo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPrecio_Venta As System.Windows.Forms.Label
    Friend WithEvents cboPrecios As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblDescripcion_plan As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion_precio_venta As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcprecio_venta_modificado As DevExpress.XtraEditors.CalcEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCotizacionesPlanes))
        Me.lblPlan_Credito = New System.Windows.Forms.Label
        Me.lkpPlan_Credito = New Dipros.Editors.TINMultiLookup
        Me.lblEnganche = New System.Windows.Forms.Label
        Me.clcEnganche = New Dipros.Editors.TINCalcEdit
        Me.lblPlazo = New System.Windows.Forms.Label
        Me.clcPlazo = New Dipros.Editors.TINCalcEdit
        Me.lblPrecio_Venta = New System.Windows.Forms.Label
        Me.cboPrecios = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblDescripcion_plan = New System.Windows.Forms.Label
        Me.lblDescripcion_precio_venta = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcprecio_venta_modificado = New DevExpress.XtraEditors.CalcEdit
        CType(Me.clcEnganche.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPlazo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboPrecios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcprecio_venta_modificado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblPlan_Credito
        '
        Me.lblPlan_Credito.AutoSize = True
        Me.lblPlan_Credito.Location = New System.Drawing.Point(10, 40)
        Me.lblPlan_Credito.Name = "lblPlan_Credito"
        Me.lblPlan_Credito.Size = New System.Drawing.Size(76, 16)
        Me.lblPlan_Credito.TabIndex = 0
        Me.lblPlan_Credito.Tag = ""
        Me.lblPlan_Credito.Text = "&Plan Cr�dito:"
        Me.lblPlan_Credito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPlan_Credito
        '
        Me.lkpPlan_Credito.AllowAdd = False
        Me.lkpPlan_Credito.AutoReaload = False
        Me.lkpPlan_Credito.DataSource = Nothing
        Me.lkpPlan_Credito.DefaultSearchField = ""
        Me.lkpPlan_Credito.DisplayMember = "descripcion"
        Me.lkpPlan_Credito.EditValue = Nothing
        Me.lkpPlan_Credito.Filtered = False
        Me.lkpPlan_Credito.InitValue = Nothing
        Me.lkpPlan_Credito.Location = New System.Drawing.Point(90, 40)
        Me.lkpPlan_Credito.MultiSelect = False
        Me.lkpPlan_Credito.Name = "lkpPlan_Credito"
        Me.lkpPlan_Credito.NullText = ""
        Me.lkpPlan_Credito.PopupWidth = CType(400, Long)
        Me.lkpPlan_Credito.Required = False
        Me.lkpPlan_Credito.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlan_Credito.SearchMember = ""
        Me.lkpPlan_Credito.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlan_Credito.SelectAll = False
        Me.lkpPlan_Credito.Size = New System.Drawing.Size(232, 20)
        Me.lkpPlan_Credito.TabIndex = 1
        Me.lkpPlan_Credito.Tag = "Plan_Credito"
        Me.lkpPlan_Credito.ToolTip = Nothing
        Me.lkpPlan_Credito.ValueMember = "plan_Credito"
        '
        'lblEnganche
        '
        Me.lblEnganche.AutoSize = True
        Me.lblEnganche.Location = New System.Drawing.Point(23, 62)
        Me.lblEnganche.Name = "lblEnganche"
        Me.lblEnganche.Size = New System.Drawing.Size(63, 16)
        Me.lblEnganche.TabIndex = 2
        Me.lblEnganche.Tag = ""
        Me.lblEnganche.Text = "&Enganche:"
        Me.lblEnganche.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEnganche
        '
        Me.clcEnganche.EditValue = "0"
        Me.clcEnganche.Location = New System.Drawing.Point(90, 62)
        Me.clcEnganche.MaxValue = 0
        Me.clcEnganche.MinValue = 0
        Me.clcEnganche.Name = "clcEnganche"
        '
        'clcEnganche.Properties
        '
        Me.clcEnganche.Properties.DisplayFormat.FormatString = "n2"
        Me.clcEnganche.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche.Properties.EditFormat.FormatString = "n2"
        Me.clcEnganche.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche.Properties.Enabled = False
        Me.clcEnganche.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcEnganche.Properties.Precision = 2
        Me.clcEnganche.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEnganche.Size = New System.Drawing.Size(64, 19)
        Me.clcEnganche.TabIndex = 3
        Me.clcEnganche.Tag = "enganche"
        '
        'lblPlazo
        '
        Me.lblPlazo.AutoSize = True
        Me.lblPlazo.Location = New System.Drawing.Point(48, 84)
        Me.lblPlazo.Name = "lblPlazo"
        Me.lblPlazo.Size = New System.Drawing.Size(38, 16)
        Me.lblPlazo.TabIndex = 4
        Me.lblPlazo.Tag = ""
        Me.lblPlazo.Text = "&Plazo:"
        Me.lblPlazo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPlazo
        '
        Me.clcPlazo.EditValue = "0"
        Me.clcPlazo.Location = New System.Drawing.Point(90, 83)
        Me.clcPlazo.MaxValue = 0
        Me.clcPlazo.MinValue = 0
        Me.clcPlazo.Name = "clcPlazo"
        '
        'clcPlazo.Properties
        '
        Me.clcPlazo.Properties.DisplayFormat.FormatString = "n0"
        Me.clcPlazo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo.Properties.EditFormat.FormatString = "n0"
        Me.clcPlazo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPlazo.Properties.Enabled = False
        Me.clcPlazo.Properties.MaskData.EditMask = "########0.00"
        Me.clcPlazo.Properties.Precision = 18
        Me.clcPlazo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPlazo.Size = New System.Drawing.Size(64, 19)
        Me.clcPlazo.TabIndex = 5
        Me.clcPlazo.Tag = "plazo"
        '
        'lblPrecio_Venta
        '
        Me.lblPrecio_Venta.AutoSize = True
        Me.lblPrecio_Venta.Location = New System.Drawing.Point(8, 105)
        Me.lblPrecio_Venta.Name = "lblPrecio_Venta"
        Me.lblPrecio_Venta.Size = New System.Drawing.Size(78, 16)
        Me.lblPrecio_Venta.TabIndex = 6
        Me.lblPrecio_Venta.Tag = ""
        Me.lblPrecio_Venta.Text = "&Precio Venta:"
        Me.lblPrecio_Venta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPrecios
        '
        Me.cboPrecios.Location = New System.Drawing.Point(90, 104)
        Me.cboPrecios.Name = "cboPrecios"
        '
        'cboPrecios.Properties
        '
        Me.cboPrecios.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPrecios.Properties.Enabled = False
        Me.cboPrecios.Size = New System.Drawing.Size(150, 20)
        Me.cboPrecios.TabIndex = 7
        Me.cboPrecios.Tag = "precio_venta"
        '
        'lblDescripcion_plan
        '
        Me.lblDescripcion_plan.Location = New System.Drawing.Point(16, 136)
        Me.lblDescripcion_plan.Name = "lblDescripcion_plan"
        Me.lblDescripcion_plan.Size = New System.Drawing.Size(240, 23)
        Me.lblDescripcion_plan.TabIndex = 60
        Me.lblDescripcion_plan.Tag = "descripcion_plan"
        Me.lblDescripcion_plan.Text = "Label2"
        Me.lblDescripcion_plan.Visible = False
        '
        'lblDescripcion_precio_venta
        '
        Me.lblDescripcion_precio_venta.Location = New System.Drawing.Point(16, 160)
        Me.lblDescripcion_precio_venta.Name = "lblDescripcion_precio_venta"
        Me.lblDescripcion_precio_venta.Size = New System.Drawing.Size(240, 23)
        Me.lblDescripcion_precio_venta.TabIndex = 61
        Me.lblDescripcion_precio_venta.Tag = "descripcion_precio_venta"
        Me.lblDescripcion_precio_venta.Text = "Label3"
        Me.lblDescripcion_precio_venta.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(160, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(16, 16)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "%"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'clcprecio_venta_modificado
        '
        Me.clcprecio_venta_modificado.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcprecio_venta_modificado.Location = New System.Drawing.Point(240, 72)
        Me.clcprecio_venta_modificado.Name = "clcprecio_venta_modificado"
        '
        'clcprecio_venta_modificado.Properties
        '
        Me.clcprecio_venta_modificado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.clcprecio_venta_modificado.Properties.Enabled = False
        Me.clcprecio_venta_modificado.Size = New System.Drawing.Size(75, 20)
        Me.clcprecio_venta_modificado.TabIndex = 63
        Me.clcprecio_venta_modificado.Tag = "precio_venta_modificado"
        '
        'frmCotizacionesPlanes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(330, 136)
        Me.Controls.Add(Me.clcprecio_venta_modificado)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblDescripcion_precio_venta)
        Me.Controls.Add(Me.lblDescripcion_plan)
        Me.Controls.Add(Me.cboPrecios)
        Me.Controls.Add(Me.lblPlan_Credito)
        Me.Controls.Add(Me.lkpPlan_Credito)
        Me.Controls.Add(Me.lblEnganche)
        Me.Controls.Add(Me.clcEnganche)
        Me.Controls.Add(Me.lblPlazo)
        Me.Controls.Add(Me.clcPlazo)
        Me.Controls.Add(Me.lblPrecio_Venta)
        Me.Name = "frmCotizacionesPlanes"
        Me.Controls.SetChildIndex(Me.lblPrecio_Venta, 0)
        Me.Controls.SetChildIndex(Me.clcPlazo, 0)
        Me.Controls.SetChildIndex(Me.lblPlazo, 0)
        Me.Controls.SetChildIndex(Me.clcEnganche, 0)
        Me.Controls.SetChildIndex(Me.lblEnganche, 0)
        Me.Controls.SetChildIndex(Me.lkpPlan_Credito, 0)
        Me.Controls.SetChildIndex(Me.lblPlan_Credito, 0)
        Me.Controls.SetChildIndex(Me.cboPrecios, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion_plan, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion_precio_venta, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcprecio_venta_modificado, 0)
        CType(Me.clcEnganche.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPlazo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboPrecios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcprecio_venta_modificado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCotizacionesPlanes_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                    CType(OwnerForm, frmCotizaciones).CalculaeInsertaPrecios()
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
                    CType(OwnerForm, frmCotizaciones).EliminarPlan = True
                    CType(OwnerForm, frmCotizaciones).CalculaeInsertaPrecios()
            End Select
        End With
    End Sub

    Private Sub frmCotizacionesPlanes_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub frmCotizacionesPlanes_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        sucursal_dependencia = CType(CType(oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)

        LLenarPrecios()
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmCotizacionesPlanes_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Response = CType(OwnerForm, frmCotizaciones).oCotizacionesPlanes.Validacion(Action, Me.Plan, OwnerForm.MasterControl.DataSource)
    End Sub

   

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpPlan_LoadData(ByVal Initialize As Boolean) Handles lkpPlan_Credito.LoadData
        Dim Response As New Events
        Response = oPlanesCredito.Lookup(sucursal_dependencia, , 1)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlan_Credito.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpPlan_Format() Handles lkpPlan_Credito.Format
        Comunes.clsFormato.for_planes_credito_grl(Me.lkpPlan_Credito)
    End Sub
    Private Sub lkpPlan_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpPlan_Credito.EditValueChanged
        If Plan <> -1 Then
            Me.clcPlazo.EditValue = Me.lkpPlan_Credito.GetValue("plazo")
            Me.cboPrecios.EditValue = Me.lkpPlan_Credito.GetValue("precio_venta")
            Me.clcEnganche.EditValue = Me.lkpPlan_Credito.GetValue("enganche")
            Me.lblDescripcion_plan.Text = Me.lkpPlan_Credito.Text
            Me.clcPlazo.Focus()
            Me.clcEnganche.Focus()
        Else
            Me.lblDescripcion_plan.Text = ""
        End If
    End Sub
    Private Sub cboPrecios_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPrecios.SelectedIndexChanged
        Me.lblDescripcion_precio_venta.Text = Me.cboPrecios.Text
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub LLenarPrecios()
        Dim Response As New Events
        Response = oPreciosVenta.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            Dim item As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
            item.Description = "Precio Lista"
            item.Value = 0
            Me.cboPrecios.Properties.Items.Add(item)

            Dim i As Long
            For i = 1 To oDataSet.Tables(0).Rows.Count
                Dim item_for As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                item_for.Description = oDataSet.Tables(0).Rows(i - 1).Item("nombre")
                item_for.Value = oDataSet.Tables(0).Rows(i - 1).Item("precio")
                Me.cboPrecios.Properties.Items.Add(item_for)
            Next

            Me.cboPrecios.SelectedIndex = 0
            oDataSet = Nothing
        End If
    End Sub
#End Region


  

End Class
