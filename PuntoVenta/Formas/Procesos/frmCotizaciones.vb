Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmCotizaciones
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepItemLookUpGrupos As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents grvArticulos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDepartamento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepItemLookUpDepartamentos As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepItemLookUpArticulos As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepItemClcCantidad As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepItemClcPrecioImporte As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grArticulos As DevExpress.XtraGrid.GridControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grPlanes As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvPlanes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents clcCotizacion As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents tmaPlanes As Dipros.Windows.TINMaster
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grvPrecios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClaveArticuloPrecios As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcionArticuloPrecios As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClavePlanPrecios As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcionPlanPrecios As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grPrecios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcPrecioVentaPlanes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTotalPrecios As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents grcClavePrecioVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecio_venta_modificado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents repitemClcPrecioVenta As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcClavePlanCredito As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEnganche As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUltimoCostoArticuloConImpuesto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents grcIdentificable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepItemChkNoIdentificable As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkPrecio_Contado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtAtencion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtAutorizo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPuesto As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCotizaciones))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.grArticulos = New DevExpress.XtraGrid.GridControl
        Me.grvArticulos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcDepartamento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepItemLookUpDepartamentos = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.grcGrupo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepItemLookUpGrupos = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepItemLookUpArticulos = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepItemClcCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcIdentificable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepItemChkNoIdentificable = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.RepItemClcPrecioImporte = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.grPlanes = New DevExpress.XtraGrid.GridControl
        Me.grvPlanes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClavePlanCredito = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEnganche = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClavePrecioVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.repitemClcPrecioVenta = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcPrecio_venta_modificado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUltimoCostoArticuloConImpuesto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcCotizacion = New DevExpress.XtraEditors.CalcEdit
        Me.tmaPlanes = New Dipros.Windows.TINMaster
        Me.grPrecios = New DevExpress.XtraGrid.GridControl
        Me.grvPrecios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveArticuloPrecios = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcionArticuloPrecios = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClavePlanPrecios = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcionPlanPrecios = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioVentaPlanes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcTotalPrecios = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtTitulo = New DevExpress.XtraEditors.TextEdit
        Me.chkPrecio_Contado = New DevExpress.XtraEditors.CheckEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtAtencion = New DevExpress.XtraEditors.TextEdit
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtAutorizo = New DevExpress.XtraEditors.TextEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtPuesto = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemLookUpDepartamentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemLookUpGrupos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemLookUpArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemClcCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemChkNoIdentificable, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemClcPrecioImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grPlanes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPlanes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repitemClcPrecioVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCotizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPrecio_Contado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAtencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutorizo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(11711, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(28, 88)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 4
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = True
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "cliente"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(80, 88)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(80, 20)
        Me.lkpCliente.TabIndex = 5
        Me.lkpCliente.Tag = "cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(168, 88)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(472, 20)
        Me.txtNombre.TabIndex = 6
        Me.txtNombre.Tag = "nombre"
        '
        'grArticulos
        '
        Me.grArticulos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grArticulos.EmbeddedNavigator
        '
        Me.grArticulos.EmbeddedNavigator.Name = ""
        Me.grArticulos.Location = New System.Drawing.Point(8, 384)
        Me.grArticulos.MainView = Me.grvArticulos
        Me.grArticulos.Name = "grArticulos"
        Me.grArticulos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepItemLookUpGrupos, Me.RepItemLookUpDepartamentos, Me.RepItemLookUpArticulos, Me.RepItemClcCantidad, Me.RepItemClcPrecioImporte, Me.RepItemChkNoIdentificable})
        Me.grArticulos.Size = New System.Drawing.Size(848, 152)
        Me.grArticulos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Yellow, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulos.TabIndex = 23
        Me.grArticulos.TabStop = False
        Me.grArticulos.Text = "GridControl1"
        '
        'grvArticulos
        '
        Me.grvArticulos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcDepartamento, Me.grcGrupo, Me.grcArticulo, Me.grcCantidad, Me.grcIdentificable})
        Me.grvArticulos.GridControl = Me.grArticulos
        Me.grvArticulos.Name = "grvArticulos"
        Me.grvArticulos.OptionsView.ShowGroupPanel = False
        '
        'grcDepartamento
        '
        Me.grcDepartamento.Caption = "Departamento"
        Me.grcDepartamento.ColumnEdit = Me.RepItemLookUpDepartamentos
        Me.grcDepartamento.FieldName = "departamento"
        Me.grcDepartamento.Name = "grcDepartamento"
        Me.grcDepartamento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDepartamento.VisibleIndex = 0
        Me.grcDepartamento.Width = 186
        '
        'RepItemLookUpDepartamentos
        '
        Me.RepItemLookUpDepartamentos.AutoHeight = False
        Me.RepItemLookUpDepartamentos.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepItemLookUpDepartamentos.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("departamento", "Departamento", 20, DevExpress.Utils.FormatType.Numeric, "", True, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("nombre", "Nombre", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default)})
        Me.RepItemLookUpDepartamentos.DisplayMember = "nombre"
        Me.RepItemLookUpDepartamentos.Name = "RepItemLookUpDepartamentos"
        Me.RepItemLookUpDepartamentos.NullText = ""
        Me.RepItemLookUpDepartamentos.PopupWidth = 250
        Me.RepItemLookUpDepartamentos.ValueMember = "departamento"
        '
        'grcGrupo
        '
        Me.grcGrupo.Caption = "Grupo"
        Me.grcGrupo.ColumnEdit = Me.RepItemLookUpGrupos
        Me.grcGrupo.FieldName = "grupo"
        Me.grcGrupo.Name = "grcGrupo"
        Me.grcGrupo.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcGrupo.VisibleIndex = 1
        Me.grcGrupo.Width = 203
        '
        'RepItemLookUpGrupos
        '
        Me.RepItemLookUpGrupos.AutoHeight = False
        Me.RepItemLookUpGrupos.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepItemLookUpGrupos.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("grupo", "Grupo", 20, DevExpress.Utils.FormatType.Numeric, "", True, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripcion", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default)})
        Me.RepItemLookUpGrupos.DisplayMember = "descripcion"
        Me.RepItemLookUpGrupos.Name = "RepItemLookUpGrupos"
        Me.RepItemLookUpGrupos.NullText = ""
        Me.RepItemLookUpGrupos.PopupWidth = 250
        Me.RepItemLookUpGrupos.ValueMember = "grupo"
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Articulo"
        Me.grcArticulo.ColumnEdit = Me.RepItemLookUpArticulos
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 2
        Me.grcArticulo.Width = 242
        '
        'RepItemLookUpArticulos
        '
        Me.RepItemLookUpArticulos.AutoHeight = False
        Me.RepItemLookUpArticulos.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepItemLookUpArticulos.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("articulo", "Articulo", 20, DevExpress.Utils.FormatType.Numeric, "", True, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion_corta", "Descripcion", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("modelo", "Modelo", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default)})
        Me.RepItemLookUpArticulos.DisplayMember = "modelo"
        Me.RepItemLookUpArticulos.Name = "RepItemLookUpArticulos"
        Me.RepItemLookUpArticulos.NullText = ""
        Me.RepItemLookUpArticulos.PopupWidth = 250
        Me.RepItemLookUpArticulos.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        Me.RepItemLookUpArticulos.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.RepItemLookUpArticulos.ValueMember = "articulo"
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.ColumnEdit = Me.RepItemClcCantidad
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 3
        Me.grcCantidad.Width = 113
        '
        'RepItemClcCantidad
        '
        Me.RepItemClcCantidad.AutoHeight = False
        Me.RepItemClcCantidad.DisplayFormat.FormatString = "n0"
        Me.RepItemClcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepItemClcCantidad.EditFormat.FormatString = "n0"
        Me.RepItemClcCantidad.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepItemClcCantidad.Name = "RepItemClcCantidad"
        '
        'grcIdentificable
        '
        Me.grcIdentificable.Caption = "No Identificable"
        Me.grcIdentificable.ColumnEdit = Me.RepItemChkNoIdentificable
        Me.grcIdentificable.FieldName = "no_identificable"
        Me.grcIdentificable.Name = "grcIdentificable"
        Me.grcIdentificable.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcIdentificable.Width = 90
        '
        'RepItemChkNoIdentificable
        '
        Me.RepItemChkNoIdentificable.AutoHeight = False
        Me.RepItemChkNoIdentificable.Name = "RepItemChkNoIdentificable"
        '
        'RepItemClcPrecioImporte
        '
        Me.RepItemClcPrecioImporte.AutoHeight = False
        Me.RepItemClcPrecioImporte.DisplayFormat.FormatString = "c2"
        Me.RepItemClcPrecioImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepItemClcPrecioImporte.EditFormat.FormatString = "n2"
        Me.RepItemClcPrecioImporte.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepItemClcPrecioImporte.Name = "RepItemClcPrecioImporte"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 360)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(848, 23)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "ARTICULOS"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grPlanes
        '
        '
        'grPlanes.EmbeddedNavigator
        '
        Me.grPlanes.EmbeddedNavigator.Name = ""
        Me.grPlanes.Location = New System.Drawing.Point(9, 192)
        Me.grPlanes.MainView = Me.grvPlanes
        Me.grPlanes.Name = "grPlanes"
        Me.grPlanes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.repitemClcPrecioVenta})
        Me.grPlanes.Size = New System.Drawing.Size(431, 158)
        Me.grPlanes.TabIndex = 19
        Me.grPlanes.TabStop = False
        Me.grPlanes.Text = "GridControl1"
        '
        'grvPlanes
        '
        Me.grvPlanes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClavePlanCredito, Me.grcEnganche, Me.grcClavePrecioVenta, Me.GridColumn4, Me.GridColumn5, Me.grcPrecioVenta, Me.grcPrecio_venta_modificado, Me.grcUltimoCostoArticuloConImpuesto})
        Me.grvPlanes.GridControl = Me.grPlanes
        Me.grvPlanes.Name = "grvPlanes"
        Me.grvPlanes.OptionsBehavior.Editable = False
        Me.grvPlanes.OptionsView.ShowGroupPanel = False
        '
        'grcClavePlanCredito
        '
        Me.grcClavePlanCredito.Caption = "Clave Plan Cr�dito"
        Me.grcClavePlanCredito.FieldName = "plan_credito"
        Me.grcClavePlanCredito.Name = "grcClavePlanCredito"
        Me.grcClavePlanCredito.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcEnganche
        '
        Me.grcEnganche.Caption = "Enganche"
        Me.grcEnganche.FieldName = "enganche"
        Me.grcEnganche.Name = "grcEnganche"
        Me.grcEnganche.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEnganche.VisibleIndex = 1
        Me.grcEnganche.Width = 85
        '
        'grcClavePrecioVenta
        '
        Me.grcClavePrecioVenta.Caption = "Clave Precio venta"
        Me.grcClavePrecioVenta.FieldName = "precio_venta"
        Me.grcClavePrecioVenta.Name = "grcClavePrecioVenta"
        Me.grcClavePrecioVenta.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Plazo"
        Me.GridColumn4.FieldName = "plazo"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 67
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Plan Cr�dito"
        Me.GridColumn5.FieldName = "descripcion_plan"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.VisibleIndex = 0
        Me.GridColumn5.Width = 210
        '
        'grcPrecioVenta
        '
        Me.grcPrecioVenta.Caption = "Precio Venta"
        Me.grcPrecioVenta.ColumnEdit = Me.repitemClcPrecioVenta
        Me.grcPrecioVenta.FieldName = "descripcion_precio_venta"
        Me.grcPrecioVenta.Name = "grcPrecioVenta"
        Me.grcPrecioVenta.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioVenta.VisibleIndex = 3
        Me.grcPrecioVenta.Width = 87
        '
        'repitemClcPrecioVenta
        '
        Me.repitemClcPrecioVenta.AutoHeight = False
        Me.repitemClcPrecioVenta.DisplayFormat.FormatString = "c2"
        Me.repitemClcPrecioVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.repitemClcPrecioVenta.Name = "repitemClcPrecioVenta"
        '
        'grcPrecio_venta_modificado
        '
        Me.grcPrecio_venta_modificado.Caption = "Precio_venta_modificado"
        Me.grcPrecio_venta_modificado.ColumnEdit = Me.repitemClcPrecioVenta
        Me.grcPrecio_venta_modificado.FieldName = "precio_venta_modificado"
        Me.grcPrecio_venta_modificado.Name = "grcPrecio_venta_modificado"
        Me.grcPrecio_venta_modificado.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcUltimoCostoArticuloConImpuesto
        '
        Me.grcUltimoCostoArticuloConImpuesto.Caption = "Ult. Costo Art. c/impuesto"
        Me.grcUltimoCostoArticuloConImpuesto.FieldName = "ultmo_costo_con_impuesto"
        Me.grcUltimoCostoArticuloConImpuesto.Name = "grcUltimoCostoArticuloConImpuesto"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "Cotizacion:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCotizacion
        '
        Me.clcCotizacion.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCotizacion.Location = New System.Drawing.Point(80, 40)
        Me.clcCotizacion.Name = "clcCotizacion"
        '
        'clcCotizacion.Properties
        '
        Me.clcCotizacion.Properties.Enabled = False
        Me.clcCotizacion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcCotizacion.Size = New System.Drawing.Size(80, 20)
        Me.clcCotizacion.TabIndex = 1
        Me.clcCotizacion.Tag = "cotizacion"
        '
        'tmaPlanes
        '
        Me.tmaPlanes.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tmaPlanes.CanDelete = True
        Me.tmaPlanes.CanInsert = True
        Me.tmaPlanes.CanUpdate = False
        Me.tmaPlanes.Grid = Me.grPlanes
        Me.tmaPlanes.Location = New System.Drawing.Point(9, 168)
        Me.tmaPlanes.Name = "tmaPlanes"
        Me.tmaPlanes.Size = New System.Drawing.Size(431, 25)
        Me.tmaPlanes.TabIndex = 18
        Me.tmaPlanes.TabStop = False
        Me.tmaPlanes.Title = "Planes de Cr�dito"
        Me.tmaPlanes.UpdateTitle = "un Registro"
        '
        'grPrecios
        '
        '
        'grPrecios.EmbeddedNavigator
        '
        Me.grPrecios.EmbeddedNavigator.Name = ""
        Me.grPrecios.Location = New System.Drawing.Point(448, 192)
        Me.grPrecios.MainView = Me.grvPrecios
        Me.grPrecios.Name = "grPrecios"
        Me.grPrecios.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1})
        Me.grPrecios.Size = New System.Drawing.Size(408, 158)
        Me.grPrecios.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Info, System.Drawing.SystemColors.WindowText, System.Drawing.SystemColors.HighlightText, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPrecios.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.RoyalBlue, System.Drawing.SystemColors.HighlightText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPrecios.TabIndex = 21
        Me.grPrecios.TabStop = False
        Me.grPrecios.Text = "GridControl1"
        '
        'grvPrecios
        '
        Me.grvPrecios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveArticuloPrecios, Me.grcDescripcionArticuloPrecios, Me.grcClavePlanPrecios, Me.grcDescripcionPlanPrecios, Me.grcPrecioVentaPlanes, Me.grcTotalPrecios})
        Me.grvPrecios.GridControl = Me.grPrecios
        Me.grvPrecios.Name = "grvPrecios"
        Me.grvPrecios.OptionsCustomization.AllowFilter = False
        Me.grvPrecios.OptionsView.ShowGroupPanel = False
        '
        'grcClaveArticuloPrecios
        '
        Me.grcClaveArticuloPrecios.Caption = "Clave Articulo"
        Me.grcClaveArticuloPrecios.FieldName = "articulo"
        Me.grcClaveArticuloPrecios.Name = "grcClaveArticuloPrecios"
        Me.grcClaveArticuloPrecios.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDescripcionArticuloPrecios
        '
        Me.grcDescripcionArticuloPrecios.Caption = "Articulo"
        Me.grcDescripcionArticuloPrecios.FieldName = "descripcion_articulo"
        Me.grcDescripcionArticuloPrecios.Name = "grcDescripcionArticuloPrecios"
        Me.grcDescripcionArticuloPrecios.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcionArticuloPrecios.VisibleIndex = 0
        Me.grcDescripcionArticuloPrecios.Width = 109
        '
        'grcClavePlanPrecios
        '
        Me.grcClavePlanPrecios.Caption = "Clave Plan"
        Me.grcClavePlanPrecios.FieldName = "plan_credito"
        Me.grcClavePlanPrecios.Name = "grcClavePlanPrecios"
        Me.grcClavePlanPrecios.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDescripcionPlanPrecios
        '
        Me.grcDescripcionPlanPrecios.Caption = "Plan Cr�dito"
        Me.grcDescripcionPlanPrecios.FieldName = "descripcion_plan"
        Me.grcDescripcionPlanPrecios.Name = "grcDescripcionPlanPrecios"
        Me.grcDescripcionPlanPrecios.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcionPlanPrecios.VisibleIndex = 1
        Me.grcDescripcionPlanPrecios.Width = 116
        '
        'grcPrecioVentaPlanes
        '
        Me.grcPrecioVentaPlanes.Caption = "Precio"
        Me.grcPrecioVentaPlanes.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcPrecioVentaPlanes.FieldName = "importe_venta"
        Me.grcPrecioVentaPlanes.Name = "grcPrecioVentaPlanes"
        Me.grcPrecioVentaPlanes.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioVentaPlanes.VisibleIndex = 2
        Me.grcPrecioVentaPlanes.Width = 99
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatString = "c2"
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.EditFormat.FormatString = "n2"
        Me.RepositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'grcTotalPrecios
        '
        Me.grcTotalPrecios.Caption = "Total"
        Me.grcTotalPrecios.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcTotalPrecios.FieldName = "total"
        Me.grcTotalPrecios.Name = "grcTotalPrecios"
        Me.grcTotalPrecios.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTotalPrecios.VisibleIndex = 3
        Me.grcTotalPrecios.Width = 70
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(448, 168)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(408, 23)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "PRECIOS"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(160, 544)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(696, 72)
        Me.txtObservaciones.TabIndex = 13
        Me.txtObservaciones.Tag = "observaciones"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(64, 544)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 16)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Ob&servaciones:"
        '
        'lblSucursal
        '
        Me.lblSucursal.Location = New System.Drawing.Point(168, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(464, 20)
        Me.lblSucursal.TabIndex = 25
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(80, 112)
        Me.txtDomicilio.Name = "txtDomicilio"
        Me.txtDomicilio.Size = New System.Drawing.Size(776, 20)
        Me.txtDomicilio.TabIndex = 8
        Me.txtDomicilio.Tag = "direccion"
        '
        'lblDomicilio
        '
        Me.lblDomicilio.AutoSize = True
        Me.lblDomicilio.Location = New System.Drawing.Point(16, 112)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio.TabIndex = 7
        Me.lblDomicilio.Tag = ""
        Me.lblDomicilio.Text = "&Domicilio:"
        Me.lblDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(35, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Tag = ""
        Me.Label6.Text = "Titulo:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTitulo
        '
        Me.txtTitulo.EditValue = ""
        Me.txtTitulo.Location = New System.Drawing.Point(80, 64)
        Me.txtTitulo.Name = "txtTitulo"
        Me.txtTitulo.Size = New System.Drawing.Size(80, 20)
        Me.txtTitulo.TabIndex = 3
        Me.txtTitulo.Tag = "titulo"
        '
        'chkPrecio_Contado
        '
        Me.chkPrecio_Contado.Location = New System.Drawing.Point(736, 40)
        Me.chkPrecio_Contado.Name = "chkPrecio_Contado"
        '
        'chkPrecio_Contado.Properties
        '
        Me.chkPrecio_Contado.Properties.Caption = "Precio de Contado"
        Me.chkPrecio_Contado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkPrecio_Contado.Size = New System.Drawing.Size(120, 19)
        Me.chkPrecio_Contado.TabIndex = 11
        Me.chkPrecio_Contado.Tag = "precio_contado"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(18, 136)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 16)
        Me.Label7.TabIndex = 9
        Me.Label7.Tag = ""
        Me.Label7.Text = "&Atencion:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAtencion
        '
        Me.txtAtencion.EditValue = ""
        Me.txtAtencion.Location = New System.Drawing.Point(80, 136)
        Me.txtAtencion.Name = "txtAtencion"
        Me.txtAtencion.Size = New System.Drawing.Size(560, 20)
        Me.txtAtencion.TabIndex = 10
        Me.txtAtencion.Tag = "atencion"
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.Text = "Imprimir"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(107, 648)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Tag = ""
        Me.Label8.Text = "&Puesto:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAutorizo
        '
        Me.txtAutorizo.EditValue = ""
        Me.txtAutorizo.Location = New System.Drawing.Point(160, 624)
        Me.txtAutorizo.Name = "txtAutorizo"
        Me.txtAutorizo.Size = New System.Drawing.Size(472, 20)
        Me.txtAutorizo.TabIndex = 15
        Me.txtAutorizo.Tag = "autorizo"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(98, 624)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(55, 16)
        Me.Label9.TabIndex = 14
        Me.Label9.Tag = ""
        Me.Label9.Text = "&Autorizo:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPuesto
        '
        Me.txtPuesto.EditValue = ""
        Me.txtPuesto.Location = New System.Drawing.Point(160, 648)
        Me.txtPuesto.Name = "txtPuesto"
        Me.txtPuesto.Size = New System.Drawing.Size(248, 20)
        Me.txtPuesto.TabIndex = 17
        Me.txtPuesto.Tag = "puesto"
        '
        'frmCotizaciones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(866, 680)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtPuesto)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtAutorizo)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtAtencion)
        Me.Controls.Add(Me.chkPrecio_Contado)
        Me.Controls.Add(Me.txtTitulo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblDomicilio)
        Me.Controls.Add(Me.txtDomicilio)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.grPrecios)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tmaPlanes)
        Me.Controls.Add(Me.clcCotizacion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.grArticulos)
        Me.Controls.Add(Me.grPlanes)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Name = "frmCotizaciones"
        Me.Text = "frmCotizaciones"
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.grPlanes, 0)
        Me.Controls.SetChildIndex(Me.grArticulos, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.clcCotizacion, 0)
        Me.Controls.SetChildIndex(Me.tmaPlanes, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.grPrecios, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.txtDomicilio, 0)
        Me.Controls.SetChildIndex(Me.lblDomicilio, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.txtTitulo, 0)
        Me.Controls.SetChildIndex(Me.chkPrecio_Contado, 0)
        Me.Controls.SetChildIndex(Me.txtAtencion, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.txtAutorizo, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.txtPuesto, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemLookUpDepartamentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemLookUpGrupos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemLookUpArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemClcCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemChkNoIdentificable, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemClcPrecioImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grPlanes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPlanes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repitemClcPrecioVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCotizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPrecio_Contado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAtencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutorizo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Dipros Systems, Declaraciones"

    Private oClientes As VillarrealBusiness.clsClientes
    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
    Private oGruposArticulos As VillarrealBusiness.clsGruposArticulos
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oArticulosPrecios As VillarrealBusiness.clsArticulosPrecios

    Private oCotizaciones As VillarrealBusiness.clsCotizaciones
    Private oCotizacionesDetalle As VillarrealBusiness.clsCotizacionesDetalle
    Public oCotizacionesPlanes As VillarrealBusiness.clsCotizacionesPlanes
    Private oCotizacionesArticulosPlanes As VillarrealBusiness.clsCotizacionesArticulosPlanes

    Private oUsuarios As New VillarrealBusiness.clsUsuarios


    Public EliminarPlan As Boolean = False
    Private clone As DataView

    Private CantidadArticulosValidos As Long = 0
    Private sucursal_venta As Long
    Private serie_venta As String
    Private folio_venta As Long

    Private entro_displayfields As Boolean = False

    Private ReadOnly Property Cliente() As Long
        Get
            Return PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private Property Numero_Articulos() As Long
        Get
            Return CantidadArticulosValidos
        End Get
        Set(ByVal Value As Long)
            If Value = -1 Then
                CantidadArticulosValidos = 0
            Else
                CantidadArticulosValidos = CantidadArticulosValidos + Value
            End If

        End Set
    End Property
    Private ReadOnly Property CotizacionConVenta() As Boolean
        Get
            If sucursal_venta > 0 And serie_venta.Length > 0 And folio_venta > 0 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    Private ReadOnly Property ModificarCotizacion() As Boolean
        Get
            Return CType(oUsuarios.DespliegaDatosUsuario.Value, DataSet).Tables(0).Rows(0).Item("modificar_precios_cotizacion")
        End Get
    End Property
#End Region

#Region "Dipros Systems, Eventos de la Forma"

    Private Sub frmCotizaciones_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
        If Me.Action = Actions.Insert Then
            Me.ImprimeCotizacion(Me.clcCotizacion.Value)
        End If
    End Sub
    Private Sub frmCotizaciones_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmCotizaciones_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmCotizaciones_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oCotizaciones.Insertar(Me.clcCotizacion.Value, Comunes.Common.Sucursal_Actual, Date.Now, Cliente, Me.txtNombre.Text, Me.txtObservaciones.Text, Me.txtDomicilio.Text, Me.txtTitulo.EditValue, Me.chkPrecio_Contado.EditValue, Me.txtAtencion.EditValue, Me.txtAutorizo.EditValue, Me.txtPuesto.EditValue)

            Case Actions.Update
                Response = oCotizaciones.Actualizar(Me.clcCotizacion.Value, Comunes.Common.Sucursal_Actual, Date.Now, Cliente, Me.txtNombre.Text, Me.txtObservaciones.Text, Me.txtDomicilio.Text, Me.txtTitulo.EditValue, Me.chkPrecio_Contado.EditValue, Me.txtAtencion.EditValue, Me.txtAutorizo.EditValue, Me.txtPuesto.EditValue)

            Case Actions.Delete
                Response = oCotizaciones.Eliminar(Me.clcCotizacion.Value)
        End Select
    End Sub
    Private Sub frmCotizaciones_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        Dim i As Long
        Me.grvArticulos.CloseEditor()
        Me.grvArticulos.UpdateCurrentRow()
        Me.grvPrecios.CloseEditor()
        Me.grvPrecios.UpdateCurrentRow()

        If Action = Actions.Delete Or Action = Actions.Update Then
            Response = Me.oCotizacionesArticulosPlanes.Eliminar(Me.clcCotizacion.Value)
            If Not Response.ErrorFound Then Response = Me.oCotizacionesDetalle.Eliminar(Me.clcCotizacion.Value)
        End If

        ' Guardo los planes de credito
        ' ===========================================================
        With Me.tmaPlanes
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        If Not Response.ErrorFound Then Response = oCotizacionesPlanes.Insertar(Me.clcCotizacion.EditValue, Me.tmaPlanes.Item("plan_credito"), Me.tmaPlanes.Item("enganche"), Me.tmaPlanes.Item("plazo"), Me.tmaPlanes.Item("precio_venta"))

                    Case Actions.Delete
                        If Not Response.ErrorFound Then Response = oCotizacionesPlanes.Eliminar(Me.clcCotizacion.EditValue, .Item("plan_credito"))

                    Case Actions.None
                        If Not Response.ErrorFound Then Response = oCotizacionesPlanes.Actualizar(Me.clcCotizacion.EditValue, Me.tmaPlanes.Item("plan_credito"), Me.tmaPlanes.Item("enganche"), Me.tmaPlanes.Item("plazo"), Me.tmaPlanes.Item("precio_venta"))
                End Select

                .MoveNext()
            Loop
        End With
        ' ===========================================================
        'Guardo los articulos
        For i = 0 To Me.grvArticulos.RowCount - 1
            If Not Me.grvArticulos.GetRowCellValue(i, Me.grcArticulo) Is System.DBNull.Value Then
                Select Case Action
                    Case Actions.Insert, Actions.Update
                        If Not Response.ErrorFound Then Response = Me.oCotizacionesDetalle.Insertar(Me.clcCotizacion.Value, Me.grvArticulos.GetRowCellValue(i, Me.grcArticulo), Me.grvArticulos.GetRowCellValue(i, Me.grcCantidad))
                End Select
            End If
        Next
        ' ===========================================================
        'Guardo los precios
        For i = 0 To Me.grvPrecios.RowCount - 1
            If Action = Actions.Insert Or Action = Actions.Update Then
                If Not Response.ErrorFound And Not Me.grvPrecios.GetRowCellValue(i, Me.grcClaveArticuloPrecios) Is System.DBNull.Value Then
                    If Not (chkPrecio_Contado.Checked = True And (Me.grvPrecios.GetRowCellValue(i, Me.grcDescripcionPlanPrecios) = "Contado")) Then
                        Response = Me.oCotizacionesArticulosPlanes.Insertar(Me.clcCotizacion.Value, Me.grvPrecios.GetRowCellValue(i, Me.grcClaveArticuloPrecios), Me.grvPrecios.GetRowCellValue(i, Me.grcClavePlanPrecios), Me.grvPrecios.GetRowCellValue(i, Me.grcPrecioVentaPlanes), Me.grvPrecios.GetRowCellValue(i, Me.grcTotalPrecios))
                    End If
                End If
            End If
        Next

    End Sub
    Private Sub frmCotizaciones_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

        entro_displayfields = True

        sucursal_venta = OwnerForm.Value("sucursal_venta")
        serie_venta = OwnerForm.Value("serie_venta")
        folio_venta = OwnerForm.Value("folio_venta")

        ' no se puede actualizar o eliminar la cotizacion si ya esta utilizada en una venta
        Me.tbrTools.Buttons(0).Enabled = Not CotizacionConVenta


        Dim oDataSet As DataSet
        Response = oCotizaciones.DespliegaDatos(OwnerForm.Value("cotizacion"))
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            Me.lblSucursal.Text = "Sucursal de la Cotizacion: " + OwnerForm.Value("sucursal")
        End If

        Response = Me.oCotizacionesPlanes.Listado(OwnerForm.Value("cotizacion"))
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaPlanes.DataSource = oDataSet
        End If

        Response = Me.oCotizacionesDetalle.Listado(OwnerForm.Value("cotizacion"))
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.grArticulos.DataSource = oDataSet.Tables(0)
            Numero_Articulos = oDataSet.Tables(0).Rows.Count
        End If

        Response = Me.oCotizacionesArticulosPlanes.Listado(OwnerForm.Value("cotizacion"))
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.grPrecios.DataSource = oDataSet.Tables(0)
        End If

        If chkPrecio_Contado.Checked = True Then
            chkPrecio_Contado.Checked = False
            chkPrecio_Contado.Checked = True
        End If

        oDataSet = Nothing

        entro_displayfields = False
    End Sub
    Private Sub frmCotizaciones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub
    Private Sub frmCotizaciones_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oGruposArticulos = New VillarrealBusiness.clsGruposArticulos
        oArticulos = New VillarrealBusiness.clsArticulos

        oArticulosPrecios = New VillarrealBusiness.clsArticulosPrecios
        oCotizaciones = New VillarrealBusiness.clsCotizaciones
        oCotizacionesDetalle = New VillarrealBusiness.clsCotizacionesDetalle
        oCotizacionesPlanes = New VillarrealBusiness.clsCotizacionesPlanes
        oCotizacionesArticulosPlanes = New VillarrealBusiness.clsCotizacionesArticulosPlanes



        Dim oDataSet As DataSet
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.RepItemLookUpDepartamentos.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If

        Response = oGruposArticulos.Lookup(-1)
        If Not Response.ErrorFound Then

            oDataSet = Response.Value
            Me.RepItemLookUpGrupos.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If

        'Filtro los articulos que no sean de regalo
        Response = oArticulos.Lookup(, , , , , 0)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.RepItemLookUpArticulos.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing


        Response = oCotizacionesArticulosPlanes.Listado(0)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.grPrecios.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If

        InsertaNewRow()

        With Me.tmaPlanes
            .UpdateTitle = "un Plan"
            .UpdateForm = New frmCotizacionesPlanes

            .AddColumn("plan_credito", "System.Int32")
            .AddColumn("descripcion_plan")
            .AddColumn("enganche", "System.Double")
            .AddColumn("plazo")
            .AddColumn("precio_venta", "System.Int32")
            .AddColumn("descripcion_precio_venta")
            .AddColumn("precio_venta_modificado")
        End With



    End Sub
    Private Sub frmCotizaciones_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim table As DataTable = CType(Me.grvArticulos.DataSource, DataView).Table

        Response = oCotizaciones.Validacion(Action, Me.txtNombre.Text, Me.tmaPlanes.DataSource.Tables(0).DefaultView, Numero_Articulos, table)

        'Valida que los Articulos Seleccionados para Reparto Realmente Se puedan Repartir
        'Dim i As Integer
        'Dim oEvents As New Events
        'Dim banCotizacionValida As Boolean
        'oEvents = Nothing

    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button.Text = "Imprimir" Then
            ImprimeCotizacion(clcCotizacion.EditValue)
        End If
    End Sub
#End Region

#Region "Dipros Systems, Eventos de los Controles"

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        Dim Response As New Events
        Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

        If Me.lkpCliente.DataSource Is Nothing Then Exit Sub
        Me.txtNombre.Text = Me.lkpCliente.GetValue("nombre")
        Me.txtDomicilio.Text = Me.lkpCliente.GetValue("direccion")
    End Sub

    Private Sub grvPrecios_FocusedColumnChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs) Handles grvPrecios.FocusedColumnChanged
        If e.FocusedColumn Is Me.grcPrecioVentaPlanes Then
            If ModificarCotizacion = False Then
                Me.InhabilitaColumnas(Me.grcPrecioVentaPlanes)
                ShowMessage(MessageType.MsgInformation, "No tiene permiso para Modificar el precio")
            Else
                Me.HablilitaColumnas(Me.grcPrecioVentaPlanes)

            End If
        End If
    End Sub
    Private Sub grvPrecios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grvPrecios.KeyDown
        Try

            If e.KeyCode = e.KeyCode.Enter Or e.KeyCode = e.KeyCode.Left Or e.KeyCode = e.KeyCode.Right Or e.KeyCode = Keys.Down Then
                If Me.grvPrecios.FocusedColumn Is Me.grcPrecioVentaPlanes Then

                    Dim dprecio_anterior As Double = Me.grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcPrecioVentaPlanes)

                    ' Actualiza los Valores al Cambiar de fila
                    Me.grvPrecios.CloseEditor()
                    Me.grvPrecios.UpdateCurrentRow()

                    Dim llave_plan As String
                    Dim precio_venta As Double

                    Me.tmaPlanes.MoveFirst()
                    Do While Not tmaPlanes.EOF
                        If Me.tmaPlanes.Item("plan_credito") = Me.grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcClavePlanPrecios) Then
                            llave_plan = Me.tmaPlanes.Item("precio_venta")
                        End If
                        tmaPlanes.MoveNext()
                    Loop


                    'Obtengo la clave del plan y el nuevo precio 
                    'llave_plan = Me.grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcClavePlanPrecios)
                    precio_venta = Me.grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcPrecioVentaPlanes)
                    '**************************************
                    'VALIDA SI EL IMPORTE ES MENOR QUE EL ULTIMO COSTO DEL ARTICULO MAS IMPUESTO
                    Dim ultimo_costo_articulo As Double
                    Dim impuesto_articulo As Double
                    Dim response As Dipros.Utils.Events
                    Dim odataset As DataSet

                    response = oArticulosPrecios.DespliegaDatos(Me.grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcClaveArticuloPrecios), llave_plan) ' Me.tmaPlanes.Item("precio_venta"))
                    If Not response.ErrorFound Then
                        Dim dPrecioVenta As Double
                        odataset = response.Value

                        ultimo_costo_articulo = (CType(odataset.Tables(0).Rows(0).ItemArray.GetValue(4), Double))
                        impuesto_articulo = (CType(odataset.Tables(0).Rows(0).ItemArray.GetValue(5), Double))
                    End If

                    If precio_venta < (ultimo_costo_articulo * (1 + impuesto_articulo / 100)) Then
                        ShowMessage(MessageType.MsgInformation, "El precio de venta debe ser mayor que : $" + CType((ultimo_costo_articulo * (1 + impuesto_articulo / 100)), String))
                        Me.grvPrecios.SetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcPrecioVentaPlanes, dprecio_anterior)
                        ' Actualiza los Valores al Cambiar de fila
                        Me.grvPrecios.CloseEditor()
                        Me.grvPrecios.UpdateCurrentRow()

                        Exit Sub
                    End If
                    '*******************************************
                    'Actualizo el valor en el master
                    Dim orow As DataRow
                    Dim i As Integer = 0

                    For Each orow In CType(Me.tmaPlanes.DataSource, DataSet).Tables(0).Rows



                        If orow("plan_credito") = Me.grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcClavePlanPrecios) Then 'llave_plan Then
                            orow("precio_venta_modificado") = precio_venta

                        End If
                        i = i + 1
                        'If Not IsDBNull((Me.grvArticulos.GetRowCellValue(Me.grvArticulos.FocusedRowHandle, Me.grcArticulo))) Then
                        '    articulo_a_modificar = CType(Me.grvArticulos.GetRowCellValue(Me.grvArticulos.FocusedRowHandle, Me.grcArticulo), Integer)
                        'End If
                    Next

                    ' RESPALDO--- BORRAR DESPUES DEL DIA 5 DE ENERO 2008

                    '' For Each orow In CType(Me.tmaPlanes.DataSource, DataSet).Tables(0).Rows
                    'For Each orow In CType(Me.grArticulos.DataSource, DataTable).Rows

                    '    If i = grvPrecios.FocusedRowHandle Then  'llave_plan Then
                    '        'If orow("plan_credito") = Me.grvPrecios.GetRowCellValue(Me.grvPrecios.FocusedRowHandle, Me.grcClavePlanPrecios) Then 'llave_plan Then
                    '        orow("precio_venta_modificado") = precio_venta

                    '    End If
                    '    i = i + 1
                    '    'If Not IsDBNull((Me.grvArticulos.GetRowCellValue(Me.grvArticulos.FocusedRowHandle, Me.grcArticulo))) Then
                    '    '    articulo_a_modificar = CType(Me.grvArticulos.GetRowCellValue(Me.grvArticulos.FocusedRowHandle, Me.grcArticulo), Integer)
                    '    'End If
                    'Next

                    CalculaeInsertaPrecios()
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub chkPrecio_Contado_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPrecio_Contado.EditValueChanged
        'If chkPrecio_Contado.Checked = False Then EliminarPlan = True
        EliminarPlan = True
        CalculaeInsertaPrecios()
    End Sub

#Region "Grid Articulos"

    Private Sub grvArticulos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grvArticulos.KeyDown
        If e.KeyCode = e.KeyCode.Enter Then

            ' Actualiza los Valores al Cambiar de fila
            Me.grvArticulos.CloseEditor()
            Me.grvArticulos.UpdateCurrentRow()

            ' Valida si no tiene cuenta no deja agregar mas filas al grid
            If Me.grvArticulos.GetRowCellValue(Me.grvArticulos.FocusedRowHandle, Me.grcArticulo) Is System.DBNull.Value Or Me.grvArticulos.GetRowCellValue(Me.grvArticulos.FocusedRowHandle, Me.grcCantidad) = 0 Then Exit Sub

            If Me.grvArticulos.RowCount - 1 = Me.grvArticulos.FocusedRowHandle Then
                Me.grvArticulos.AddNewRow()
                Me.grvArticulos.FocusedRowHandle = 0
                Me.grvArticulos.MoveLast()
                Me.grvArticulos.SetRowCellValue(Me.grvArticulos.FocusedRowHandle, Me.grcCantidad, 0)
            End If
            ActualizaTotales()
            CalculaeInsertaPrecios()
        End If

        If e.KeyCode = e.KeyCode.Left Or e.KeyCode = e.KeyCode.Right Then
            ActualizaTotales()
        End If

    End Sub
    Private Sub grvArticulos_ShownEditor(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvArticulos.ShownEditor
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView
        view = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        If (view.FocusedColumn.FieldName = "grupo" Or view.FocusedColumn.FieldName = "articulo") AndAlso TypeOf view.ActiveEditor Is DevExpress.XtraEditors.LookUpEdit Then
            Dim edit As DevExpress.XtraEditors.LookUpEdit
            Dim table As DataTable
            Dim row As DataRow

            edit = CType(view.ActiveEditor, DevExpress.XtraEditors.LookUpEdit)

            table = CType(edit.Properties.DataSource, DataTable)
            clone = New DataView(table)
            row = view.GetDataRow(view.FocusedRowHandle)

            Select Case view.FocusedColumn.FieldName
                Case "grupo"
                    If row("departamento").ToString().Length = 0 Then
                        clone.RowFilter = "[departamento] = 0 "
                    Else
                        clone.RowFilter = "[departamento] = " + row("departamento").ToString()
                    End If

                Case "articulo"
                    If row("departamento").ToString().Length = 0 Or row("grupo").ToString().Length = 0 Then
                        clone.RowFilter = "[departamento] = 0 AND [grupo] = 0"
                    Else
                        clone.RowFilter = "[departamento] = " + row("departamento").ToString() + " AND [grupo] = " + row("grupo").ToString()
                    End If

            End Select


            edit.Properties.DataSource = clone
        End If
    End Sub
    Private Sub grvArticulos_HiddenEditor(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvArticulos.HiddenEditor
        If Not clone Is Nothing Then
            clone.Dispose()
            clone = Nothing
        End If
    End Sub

    Private Sub RepItemLookUpDepartamentos_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RepItemLookUpDepartamentos.EditValueChanged
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = Me.grvArticulos
        view.SetRowCellValue(view.FocusedRowHandle, view.Columns("grupo"), DBNull.Value)
        view.SetRowCellValue(view.FocusedRowHandle, view.Columns("articulo"), DBNull.Value)

        ' Se agrego este codigo para que obtiviera el valor del lookup 
        ' de no_identificable del catalogo de departamentos

        Dim vista As DataRowView
        Dim valor As Boolean
        vista = CType(sender, DevExpress.XtraEditors.LookUpEdit).Properties.GetDataSourceRowByKeyValue(CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue)
        valor = vista.Item("no_identificable")
        'If ValidaDepartamentosIdentificables(valor) = False Then
        view.SetRowCellValue(view.FocusedRowHandle, view.Columns("no_identificable"), valor)
        'End If

    End Sub
    Private Sub RepItemLookUpGrupos_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RepItemLookUpGrupos.EditValueChanged
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = Me.grvArticulos
        view.SetRowCellValue(view.FocusedRowHandle, view.Columns("articulo"), DBNull.Value)
    End Sub
    Private Sub RepItemClcCantidad_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RepItemClcCantidad.EditValueChanged
        Dim edit As DevExpress.XtraEditors.CalcEdit = CType(sender, DevExpress.XtraEditors.CalcEdit)
        If Not IsNumeric(edit.EditValue) Then
            edit.EditValue = 0
        End If
    End Sub

#End Region


#End Region

#Region "Dipros Systems, Funcionalidad"
    Private Sub InsertaNewRow()
        Dim response As Events
        Dim odataset As DataSet

        response = Me.oCotizacionesDetalle.Listado(0)
        odataset = response.Value
        Me.grArticulos.DataSource = odataset.Tables(0)

        Me.grvArticulos.AddNewRow()
        Me.grvArticulos.FocusedRowHandle = 0
        Me.grvArticulos.MoveLast()
        Me.grvArticulos.SetRowCellValue(Me.grvArticulos.FocusedRowHandle, Me.grcCantidad, 0)
    End Sub
    Private Sub ActualizaTotales()

        Me.grvArticulos.UpdateCurrentRow()
        Me.grvArticulos.CloseEditor()
        Me.grvArticulos.UpdateTotalSummary()

    End Sub
    Public Sub CalculaeInsertaPrecios()
        Dim i As Long
        Dim fila As Long = 0
        Dim response As Events
        Dim odataset As DataSet

        'se puso esta validacion para q no entrara cuando se esta haciendo el evento displayfields 
        If entro_displayfields = True Then Exit Sub

        response = oCotizacionesArticulosPlanes.Listado(0)
        If Not response.ErrorFound Then
            odataset = response.Value
            Me.grPrecios.DataSource = odataset.Tables(0)
            odataset = Nothing
        End If
        EliminarPlan = False
        'End If

        Me.tmaPlanes.DataSource.Tables(0).DefaultView.RowFilter = "control in (0,1,2)"

        If Me.grvArticulos.RowCount > 0 And Me.tmaPlanes.DataSource.Tables(0).DefaultView.Count > 0 Then

            For i = 0 To Me.grvPrecios.RowCount - 1
                Me.grvPrecios.DeleteRow(i)
            Next

            Numero_Articulos = -1

            Me.tmaPlanes.MoveFirst()
            Do While Not tmaPlanes.EOF
                Select Case tmaPlanes.CurrentAction
                    Case Actions.Insert, Actions.Update, Actions.None
                        For i = 0 To Me.grvArticulos.RowCount - 1
                            If Not Me.grvArticulos.GetRowCellValue(i, Me.grcArticulo) Is System.DBNull.Value Then
                                Dim articulo As Long
                                Dim descripcion_articulo As String
                                Dim cantidad As Long

                                'Incremento en uno la cantidad de articulos
                                Me.Numero_Articulos = 1


                                ' obtengo los valores de la vista de articulos
                                articulo = Me.grvArticulos.GetRowCellValue(i, Me.grcArticulo)
                                descripcion_articulo = Me.grvArticulos.GetRowCellDisplayText(i, Me.grcArticulo)
                                cantidad = Me.grvArticulos.GetRowCellValue(i, Me.grcCantidad)

                                'agrego un nueva fila insertando los valores del articulo y de plan
                                Me.grvPrecios.AddNewRow()

                                Me.grvPrecios.SetRowCellValue(fila, Me.grcClaveArticuloPrecios, articulo)
                                Me.grvPrecios.SetRowCellValue(fila, Me.grcDescripcionArticuloPrecios, descripcion_articulo)
                                Me.grvPrecios.SetRowCellValue(fila, Me.grcClavePlanPrecios, Me.tmaPlanes.Item("plan_credito"))
                                Me.grvPrecios.SetRowCellValue(fila, Me.grcDescripcionPlanPrecios, Me.tmaPlanes.Item("descripcion_plan"))

                                ' obtengo los valores de los precios correspondientes al articulo y clave del precio

                                response = oArticulosPrecios.DespliegaDatos(articulo, Me.tmaPlanes.Item("precio_venta"))

                                If Not response.ErrorFound Then
                                    Dim dPrecioVenta As Double
                                    odataset = response.Value
                                    If odataset.Tables(0).Rows.Count > 0 Or Me.tmaPlanes.Item("precio_venta_modificado") > 0 Then

                                        'DAM si el precio fue modificado por el usuario entonces calculo sobre ese precio
                                        ' sino sobre el precio del plan asignado
                                        If Me.tmaPlanes.Item("precio_venta_modificado") > 0 Then
                                            dPrecioVenta = Me.tmaPlanes.Item("precio_venta_modificado")
                                        Else
                                            dPrecioVenta = odataset.Tables(0).Rows(0).Item("precio_venta")
                                        End If

                                        Me.grvPrecios.SetRowCellValue(fila, Me.grcPrecioVentaPlanes, dPrecioVenta)
                                        Me.grvPrecios.SetRowCellValue(fila, Me.grcTotalPrecios, cantidad * dPrecioVenta)
                                    Else
                                        Me.grvPrecios.SetRowCellValue(fila, Me.grcPrecioVentaPlanes, 0)
                                        Me.grvPrecios.SetRowCellValue(fila, Me.grcTotalPrecios, 0)
                                    End If
                                End If
                                'incremento en uno el numero actual de la fila a insertar
                                fila = fila + 1


                            End If
                        Next

                End Select
                tmaPlanes.MoveNext()
            Loop
            If chkPrecio_Contado.Checked Then
                For i = 0 To Me.grvArticulos.RowCount - 1
                    If Not Me.grvArticulos.GetRowCellValue(i, Me.grcArticulo) Is System.DBNull.Value Then
                        Dim articulo As Long
                        Dim descripcion_articulo As String
                        Dim cantidad As Long

                        'Incremento en uno la cantidad de articulos
                        Me.Numero_Articulos = 1


                        ' obtengo los valores de la vista de articulos
                        articulo = Me.grvArticulos.GetRowCellValue(i, Me.grcArticulo)
                        descripcion_articulo = Me.grvArticulos.GetRowCellDisplayText(i, Me.grcArticulo)
                        cantidad = Me.grvArticulos.GetRowCellValue(i, Me.grcCantidad)

                        'agrego un nueva fila insertando los valores del articulo y de plan
                        Me.grvPrecios.AddNewRow()

                        Me.grvPrecios.SetRowCellValue(fila, Me.grcClaveArticuloPrecios, articulo)
                        Me.grvPrecios.SetRowCellValue(fila, Me.grcDescripcionArticuloPrecios, descripcion_articulo)
                        Me.grvPrecios.SetRowCellValue(fila, Me.grcClavePlanPrecios, -1)
                        Me.grvPrecios.SetRowCellValue(fila, Me.grcDescripcionPlanPrecios, "Contado")

                        ' obtengo los valores de los precios correspondientes al articulo y clave del precio

                        response = oArticulosPrecios.DespliegaDatos(articulo, 1)

                        If Not response.ErrorFound Then
                            Dim dPrecioVenta As Double
                            odataset = response.Value
                            If odataset.Tables(0).Rows.Count > 0 Or Me.tmaPlanes.Item("precio_venta_modificado") > 0 Then

                                'DAM si el precio fue modificado por el usuario entonces calculo sobre ese precio
                                ' sino sobre el precio del plan asignado
                                If Me.tmaPlanes.Item("precio_venta_modificado") > 0 Then
                                    dPrecioVenta = Me.tmaPlanes.Item("precio_venta_modificado")
                                Else
                                    dPrecioVenta = odataset.Tables(0).Rows(0).Item("precio_venta")
                                End If

                                Me.grvPrecios.SetRowCellValue(fila, Me.grcPrecioVentaPlanes, dPrecioVenta)
                                Me.grvPrecios.SetRowCellValue(fila, Me.grcTotalPrecios, cantidad * dPrecioVenta)
                            Else
                                Me.grvPrecios.SetRowCellValue(fila, Me.grcPrecioVentaPlanes, 0)
                                Me.grvPrecios.SetRowCellValue(fila, Me.grcTotalPrecios, 0)
                            End If
                        End If
                        'incremento en uno el numero actual de la fila a insertar
                        fila = fila + 1


                    End If
                Next


            End If


        End If

        odataset = Nothing
        response = Nothing
        Me.tmaPlanes.DataSource.Tables(0).DefaultView.RowFilter = ""
    End Sub
    Private Sub InhabilitaColumnas(ByVal columna As DevExpress.XtraGrid.Columns.GridColumn)
        With columna
            .Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        End With
    End Sub
    Public Sub HablilitaColumnas(ByVal columna As DevExpress.XtraGrid.Columns.GridColumn)
        With columna
            .Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm)), DevExpress.XtraGrid.Columns.ColumnOptions)
        End With
    End Sub
    'Private Function ValidaDepartamentosIdentificables(ByVal valor_nuevo_verficar As Boolean) As Boolean
    '    Dim i As Long
    '    For i = 0 To Me.grvArticulos.RowCount - 1
    '        If i > 0 Then
    '            'Validar que no sea un valor nulo para evitar la excepcion
    '            If Not Me.grvArticulos.GetRowCellValue(i - 1, Me.grcIdentificable) Is System.DBNull.Value Then
    '                If Me.grvArticulos.GetRowCellValue(1 - 1, Me.grcIdentificable) <> valor_nuevo_verficar Then
    '                    ValidaDepartamentosIdentificables = True
    '                    Exit For
    '                End If
    '            End If
    '        End If

    '    Next

    '    'If ValidaDepartamentosIdentificables = True Then
    '    '    ShowMessage(MessageType.MsgInformation, "No se puede Cotizar Articulos con Diferente clasifificacion de Identificable")

    '    'End If

    'End Function
  
    Private Sub ImprimeCotizacion(ByVal cotizacion As Long)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.Cotizaciones(cotizacion)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "La Cotizacion no puede mostrarse")
            Else
                Dim oDataSet As DataSet
                Dim oReport As New rptCotizaciones

                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Cotizaciones", oReport)
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

#End Region


 
End Class
