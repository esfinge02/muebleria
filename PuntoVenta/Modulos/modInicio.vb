Imports System.Globalization
Imports System.Threading

Module modInicio
    Public TINApp As New Dipros.Windows.Application

    'DESCRIPCION: Funci�n de inicio del sistema
    'DESARROLLO: DIPROS SYSTEMS
    'FECHA: 18/02/2006 00:00:00
    Public Sub Main()
        TINApp.Connection = New Dipros.Data.Data
        TINApp.Application = "Punto de Venta"
        TINApp.Prefix = "PVE"

        ' ================================================================================
        Dim oStyle As New DevExpress.Utils.ViewStyle
        oStyle.BackColor = System.Drawing.Color.Beige
        oStyle.Options = oStyle.Options And (DevExpress.Utils.StyleOptions.UseBackColor Or DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseForeColor)
        oStyle.ForeColor = System.Drawing.Color.Black

        TINApp.Options.StyleDisabled = oStyle
        ' ================================================================================

        If Not TINApp.Connection.Trusted Then End
        If Not TINApp.Login() Then End

        'Inicializa la capa de negocios
        VillarrealBusiness.BusinessEnvironment.Connection = TINApp.Connection

        Comunes.Common.Aplicacion = TINApp

        ''----------------------------------------------------------------------------------
        'Selecciono por default la cultura de M�xico como configuracion del programa
        Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")

        Dim oMicultura As New CultureInfo("es-MX")
        Dim instance As New DateTimeFormatInfo
        Dim value As String

        value = "hh:mm:ss:tt"
        instance.LongTimePattern = value
        oMicultura.DateTimeFormat.LongTimePattern = instance.LongTimePattern

        Thread.CurrentThread.CurrentCulture = oMicultura

        ''----------------------------------------------------------------------------------

        'Inicia la aplicaci�n
        Dim oMain As New frmMain

        If Comunes.clsUtilerias.AbrirArchivo(Comunes.Common.Sucursal_Actual) = False Then
            If TINApp.Connection.User = "SUPER" Then
                Dim oForm As New Comunes.frmSucursalActual
                Application.Run(oForm)
                If oForm.EntrarMain = True Then
                    If Comunes.clsUtilerias.AbrirArchivoPV(Comunes.Common.PuntoVenta_Actual) = False Then
                        Dim oFormPV As New Comunes.frmPuntoVentaActual
                        Application.Run(oFormPV)
                        If oFormPV.EntrarMain = True Then
                            Application.Run(oMain)
                        Else
                            MsgBox("No hay un Punto de Venta Definido", MsgBoxStyle.Information, "Mensaje del Sistema")
                        End If
                    Else
                        Application.Run(oMain)
                    End If
                Else
                    MsgBox("No hay una Sucursal Definida", MsgBoxStyle.Information, "Mensaje del Sistema")
                End If
            Else
                MsgBox("No hay una Sucursal Definida y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
            End If
        Else
            If Comunes.clsUtilerias.AbrirArchivoPV(Comunes.Common.PuntoVenta_Actual) = False Then
                If TINApp.Connection.User = "SUPER" Then
                    Dim oFormPV As New Comunes.frmPuntoVentaActual
                    Application.Run(oFormPV)
                    If oFormPV.EntrarMain = True Then
                        Application.Run(oMain)
                    Else
                        MsgBox("No hay un Punto de Venta Definido", MsgBoxStyle.Information, "Mensaje del Sistema")
                    End If
                Else
                    MsgBox("No hay un Punto de Venta Definido y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
                End If
            Else
                Application.Run(oMain)
            End If
        End If
    End Sub

End Module
