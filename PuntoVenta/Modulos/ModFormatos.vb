Imports Dipros.Windows.Forms

Module ModFormatos

#Region "PuntosVenta"

    Public Sub for_puntos_venta_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Punto de Venta", "puntoventa", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Descripción", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Serie", "serie", 2, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Folio", "folio", 3, DevExpress.Utils.FormatType.None, "", 100)
    End Sub

#End Region




End Module
