Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmArticulosKardex
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmArticulosKardex))
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(337, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Location = New System.Drawing.Point(8, 72)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(363, 56)
        Me.gpbFechas.TabIndex = 2
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(81, 17)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Ini.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde : "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(193, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Ha&sta : "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(241, 17)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Fin.TabIndex = 3
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(77, 40)
        Me.lkpBodega.MultiSelect = True
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = "(Todos)"
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = True
        Me.lkpBodega.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodega.TabIndex = 1
        Me.lkpBodega.ToolTip = "Bodega"
        Me.lkpBodega.ValueMember = "bodega"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "&Bodega:"
        '
        'frmArticulosKardex
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(378, 136)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.gpbFechas)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmArticulosKardex"
        Me.Text = "frmArticulosKardex"
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oReportes As New VillarrealBusiness.Reportes
    Private oBodegas As New VillarrealBusiness.clsBodegas

    Public Articulo As Long

    Private ReadOnly Property Bodega() As Events
        Get
            Return Comunes.clsUtilerias.ValidaMultiSeleccion(lkpBodega.ToXML, "Bodega")
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub frmArticulosKardex_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.ArticulosKardex(Me.lkpBodega.ToXML, Articulo, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Kardex de Art�culo no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptArticulosKardex
                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)
                oReport.oData = oDataSet
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TinApp.ShowReport(Me.MdiParent, "Kardex de Art�culo", oReport, , , , True)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub

    Private Sub frmArticulosKardex_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'oReportes = New VillarrealBusiness.Reportes
        'oBodegas = New VillarrealBusiness.clsBodegas

        Me.dteFecha_Fin.DateTime = CDate(TinApp.FechaServidor)
        Me.dteFecha_Ini.EditValue = CDate("01" + TinApp.FechaServidor.Substring(2, TinApp.FechaServidor.Length - 2))
    End Sub

    Private Sub frmArticulosKardex_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(dteFecha_Ini.Text, dteFecha_Fin.Text)
    End Sub

    Private Sub frmArticulosKardex_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        Me.OwnerForm.Enabled = True
    End Sub

#End Region

#Region "Eventos de Controles"

#Region "Lookup"
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region

#End Region

#Region "Funcionalidad"

#End Region

   
End Class
