Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class brwIdentificadoresPermisos
    Inherits Dipros.Windows.frmTINGridNet


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
        Me.Text = "brwIdentificadoresPermisos"
    End Sub

#End Region

#Region "Declaraciones"

    Public ActivarBrw As Boolean = True

    Private SistemaActual As String = TinApp.Prefix



    Private oIdentificadoresPermisos As New VillarrealBusiness.clsIdentificadoresPermisos
    'Private oPuntoVenta As New VillarrealBusiness.clsPuntosVenta


#End Region


    Private Sub brwIdentificadoresPermisos_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        Response = oIdentificadoresPermisos.Listado(SistemaActual)
    End Sub

    Private Sub brwIdentificadoresPermisos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.FooterPanel.Visible = False
        Me.FilterPanel.Visible = False

    End Sub
End Class
