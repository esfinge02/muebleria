Imports Dipros.Utils.Common

Public Class frmTiposLlamadas
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As System.Windows.Forms.Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblTipo_Llamada As System.Windows.Forms.Label
    Friend WithEvents clcTipo_Llamada As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTiposLlamadas))
        Me.lblTipo_Llamada = New System.Windows.Forms.Label
        Me.clcTipo_Llamada = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcTipo_Llamada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(167, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblTipo_Llamada
        '
        Me.lblTipo_Llamada.AutoSize = True
        Me.lblTipo_Llamada.Location = New System.Drawing.Point(56, 40)
        Me.lblTipo_Llamada.Name = "lblTipo_Llamada"
        Me.lblTipo_Llamada.Size = New System.Drawing.Size(32, 16)
        Me.lblTipo_Llamada.TabIndex = 0
        Me.lblTipo_Llamada.Tag = ""
        Me.lblTipo_Llamada.Text = "&Tipo:"
        Me.lblTipo_Llamada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTipo_Llamada
        '
        Me.clcTipo_Llamada.EditValue = "0"
        Me.clcTipo_Llamada.Location = New System.Drawing.Point(98, 40)
        Me.clcTipo_Llamada.MaxValue = 0
        Me.clcTipo_Llamada.MinValue = 0
        Me.clcTipo_Llamada.Name = "clcTipo_Llamada"
        '
        'clcTipo_Llamada.Properties
        '
        Me.clcTipo_Llamada.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcTipo_Llamada.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipo_Llamada.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcTipo_Llamada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipo_Llamada.Properties.Enabled = False
        Me.clcTipo_Llamada.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTipo_Llamada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTipo_Llamada.Size = New System.Drawing.Size(62, 19)
        Me.clcTipo_Llamada.TabIndex = 1
        Me.clcTipo_Llamada.Tag = "tipo_llamada"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(16, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(98, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 40
        Me.txtDescripcion.Size = New System.Drawing.Size(254, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'frmTiposLlamadas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 96)
        Me.Controls.Add(Me.lblTipo_Llamada)
        Me.Controls.Add(Me.clcTipo_Llamada)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmTiposLlamadas"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcTipo_Llamada, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Llamada, 0)
        CType(Me.clcTipo_Llamada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oTiposLlamadas As VillarrealBusiness.clsTiposLlamadas
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmTiposLlamadas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oTiposLlamadas.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oTiposLlamadas.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oTiposLlamadas.Eliminar(clcTipo_Llamada.value)

        End Select
    End Sub

    Private Sub frmTiposLlamadas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oTiposLlamadas.DespliegaDatos(OwnerForm.Value("tipo_llamada"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmTiposLlamadas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oTiposLlamadas = New VillarrealBusiness.clsTiposLlamadas

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmTiposLlamadas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oTiposLlamadas.Validacion(Action, txtDescripcion.Text)
    End Sub

    Private Sub frmTiposLlamadas_Localize() Handles MyBase.Localize
        Find("tipo_llamada", Me.clcTipo_Llamada.EditValue)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
