Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmVendedoresDetalles
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblEsquemas As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpEsquemasDetalles As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblNombreEsquemas As System.Windows.Forms.Label
    Friend WithEvents lblSucursalNombre As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVendedoresDetalles))
        Me.lkpEsquemasDetalles = New Dipros.Editors.TINMultiLookup
        Me.lblEsquemas = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblNombreEsquemas = New System.Windows.Forms.Label
        Me.lblSucursalNombre = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(547, 28)
        '
        'lkpEsquemasDetalles
        '
        Me.lkpEsquemasDetalles.AllowAdd = False
        Me.lkpEsquemasDetalles.AutoReaload = False
        Me.lkpEsquemasDetalles.DataSource = Nothing
        Me.lkpEsquemasDetalles.DefaultSearchField = ""
        Me.lkpEsquemasDetalles.DisplayMember = "nombre"
        Me.lkpEsquemasDetalles.EditValue = Nothing
        Me.lkpEsquemasDetalles.Filtered = False
        Me.lkpEsquemasDetalles.InitValue = Nothing
        Me.lkpEsquemasDetalles.Location = New System.Drawing.Point(80, 78)
        Me.lkpEsquemasDetalles.MultiSelect = False
        Me.lkpEsquemasDetalles.Name = "lkpEsquemasDetalles"
        Me.lkpEsquemasDetalles.NullText = ""
        Me.lkpEsquemasDetalles.PopupWidth = CType(400, Long)
        Me.lkpEsquemasDetalles.ReadOnlyControl = False
        Me.lkpEsquemasDetalles.Required = False
        Me.lkpEsquemasDetalles.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEsquemasDetalles.SearchMember = ""
        Me.lkpEsquemasDetalles.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEsquemasDetalles.SelectAll = False
        Me.lkpEsquemasDetalles.Size = New System.Drawing.Size(358, 20)
        Me.lkpEsquemasDetalles.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEsquemasDetalles.TabIndex = 3
        Me.lkpEsquemasDetalles.Tag = "esquema"
        Me.lkpEsquemasDetalles.ToolTip = "Esquema de comisiones para el vendedor"
        Me.lkpEsquemasDetalles.ValueMember = "esquema"
        '
        'lblEsquemas
        '
        Me.lblEsquemas.Location = New System.Drawing.Point(8, 78)
        Me.lblEsquemas.Name = "lblEsquemas"
        Me.lblEsquemas.Size = New System.Drawing.Size(64, 16)
        Me.lblEsquemas.TabIndex = 2
        Me.lblEsquemas.Text = "Esquema :"
        '
        'lblSucursal
        '
        Me.lblSucursal.Location = New System.Drawing.Point(8, 48)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(64, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Text = "Sucursal :"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 48)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(358, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblNombreEsquemas
        '
        Me.lblNombreEsquemas.Location = New System.Drawing.Point(160, 200)
        Me.lblNombreEsquemas.Name = "lblNombreEsquemas"
        Me.lblNombreEsquemas.Size = New System.Drawing.Size(232, 16)
        Me.lblNombreEsquemas.TabIndex = 5
        Me.lblNombreEsquemas.Tag = "nombre_esquema"
        '
        'lblSucursalNombre
        '
        Me.lblSucursalNombre.Location = New System.Drawing.Point(192, 152)
        Me.lblSucursalNombre.Name = "lblSucursalNombre"
        Me.lblSucursalNombre.TabIndex = 4
        Me.lblSucursalNombre.Tag = "nombre_sucursal"
        '
        'frmVendedoresDetalles
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(470, 116)
        Me.Controls.Add(Me.lblSucursalNombre)
        Me.Controls.Add(Me.lblNombreEsquemas)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblEsquemas)
        Me.Controls.Add(Me.lkpEsquemasDetalles)
        Me.Name = "frmVendedoresDetalles"
        Me.Text = "Esquemas del Vendedor"
        Me.Controls.SetChildIndex(Me.lkpEsquemasDetalles, 0)
        Me.Controls.SetChildIndex(Me.lblEsquemas, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblNombreEsquemas, 0)
        Me.Controls.SetChildIndex(Me.lblSucursalNombre, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"

    Private oVendedoresEsquemas As VillarrealBusiness.clsVendedoresEsquemas

    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oEsquemasComisiones As VillarrealBusiness.clsEsquemasComisiones



    Private ReadOnly Property Sucursal() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property Esquema() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpEsquemasDetalles)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmEsquemasDetalles_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select

        End With
    End Sub

    Private Sub frmEsquemasDetalles_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oVendedoresEsquemas = New VillarrealBusiness.clsVendedoresEsquemas
        oEsquemasComisiones = New VillarrealBusiness.clsEsquemasComisiones

    End Sub

    Private Sub frmEsquemasDetalles_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim TablaEsquema As DataTable = CType(Me.OwnerForm, frmVendedores).TablaEsquemas
        Response = oVendedoresEsquemas.Validacion(Action, Me.Sucursal, Me.Esquema, TablaEsquema)
    End Sub

    Private Sub frmVendedoresDetalles_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Responce As New Events
        Responce = oSucursales.Lookup
        If Not Responce.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Responce.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Responce = Nothing
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(lkpSucursal)
    End Sub

    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        If Me.Sucursal > 0 Then

            lblSucursalNombre.Text = lkpSucursal.GetValue("nombre")
        Else
            lblSucursalNombre.Text = ""
        End If
    End Sub

    Private Sub lkpEsquemasDetalles_LoadData(ByVal Initialize As Boolean) Handles lkpEsquemasDetalles.LoadData
        Dim Responce As New Events
        Responce = oEsquemasComisiones.Lookup
        If Not Responce.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Responce.Value
            Me.lkpEsquemasDetalles.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Responce = Nothing
    End Sub

    Private Sub lkpEsquemasDetalles_Format() Handles lkpEsquemasDetalles.Format
        Comunes.clsFormato.for_esquemas_comisiones_grl(lkpEsquemasDetalles)
    End Sub
    Private Sub lkpEsquemasDetalles_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpEsquemasDetalles.EditValueChanged
        If Me.Esquema > 0 Then
            lblNombreEsquemas.Text = lkpEsquemasDetalles.GetValue("nombre")
        Else
            lblNombreEsquemas.Text = ""
        End If
    End Sub


#End Region

#Region "DIPROS Systems, Funciones"

#End Region




 
End Class
