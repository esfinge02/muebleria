Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmSolicitudesCliente
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblCliente As System.Windows.Forms.Label

    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNotas As System.Windows.Forms.Label
    Friend WithEvents txtNotas As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblAval As System.Windows.Forms.Label
    Friend WithEvents txtAval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio_Aval As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia_Aval As System.Windows.Forms.Label
    Friend WithEvents txtColonia_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono_Aval As System.Windows.Forms.Label
    Friend WithEvents txtTelefono_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFecha_Alta As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Alta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grvOrdenesCompra As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcCobrador As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grCobradores As DevExpress.XtraGrid.GridControl
    Friend WithEvents tmaCobradores As Dipros.Windows.TINMaster
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents pnlGenerales As System.Windows.Forms.Panel
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nvrDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrCobradores As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents pnlCobradores As System.Windows.Forms.Panel
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtycalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtentrecalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpCiudad As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblPersona As System.Windows.Forms.Label
    Friend WithEvents cboPersona As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents lblRfc As System.Windows.Forms.Label
    Friend WithEvents txtRfc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCurp As System.Windows.Forms.Label
    Friend WithEvents txtCurp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lblCp As System.Windows.Forms.Label
    Friend WithEvents clcCp As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblTelefono1 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono2 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroExterior As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroInterior As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNombres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPaterno As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMaterno As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtCiudad_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtEstado_aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtParentesco_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcAños_aval As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents nvrDatosAval As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents pnlReferencias As System.Windows.Forms.Panel
    Friend WithEvents pnlAval As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtDepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtPuesto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPuesto As System.Windows.Forms.Label
    Friend WithEvents lblOcupacion As System.Windows.Forms.Label
    Friend WithEvents txtOcupacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblIngresos As System.Windows.Forms.Label
    Friend WithEvents clcIngresos As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents clcLimiteCredito As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkNoPagaComision As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcSaldo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTipo_Cobro As System.Windows.Forms.Label
    Friend WithEvents cboTipo_Cobro As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txtTelefonos_ocupacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAntiguedad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lkpClienteAval As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblAbogado As System.Windows.Forms.Label
    Friend WithEvents btnLlamadasCliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkLocalizado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtnombre_conyuge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lblmotivos_no_localizable As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioNoLocalizable As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtmotivos_no_localizable As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblPaterno As System.Windows.Forms.Label
    Friend WithEvents lblMaterno As System.Windows.Forms.Label
    Friend WithEvents txtEmpresa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Public WithEvents clcCliente As Dipros.Editors.TINCalcEdit
    Friend WithEvents nvrDatosReferenciasLaborales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrDatosReferenciasPersonales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents cboEstadoCivil As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txttrabaja_en_referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtparentesco_referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtestado_referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtciudad_referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtnombre_referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtdomicilio_referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtcolonia_referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txttelefono_referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txttelefonos_laboral_conyuge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents pnlReferenciasPersonales As System.Windows.Forms.Panel
    Friend WithEvents pnlConvenios As System.Windows.Forms.Panel
    Friend WithEvents tmaConvenios As Dipros.Windows.TINMaster
    Friend WithEvents grConvenios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvConvenios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcConvenio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreConvenio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents nvrDatosConvenios As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents lkpFormasPago As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As DevExpress.XtraEditors.TextEdit
    Public WithEvents clcUltimosDigitosCuenta As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkFacturacionEspecial As System.Windows.Forms.CheckBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSolicitudesCliente))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.clcCliente = New Dipros.Editors.TINCalcEdit
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblNotas = New System.Windows.Forms.Label
        Me.txtNotas = New DevExpress.XtraEditors.MemoEdit
        Me.lblAval = New System.Windows.Forms.Label
        Me.txtAval = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio_Aval = New System.Windows.Forms.Label
        Me.txtDomicilio_Aval = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia_Aval = New System.Windows.Forms.Label
        Me.txtColonia_Aval = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono_Aval = New System.Windows.Forms.Label
        Me.txtTelefono_Aval = New DevExpress.XtraEditors.TextEdit
        Me.lblFecha_Alta = New System.Windows.Forms.Label
        Me.dteFecha_Alta = New DevExpress.XtraEditors.DateEdit
        Me.grCobradores = New DevExpress.XtraGrid.GridControl
        Me.grvOrdenesCompra = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcCobrador = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaCobradores = New Dipros.Windows.TINMaster
        Me.pnlGenerales = New System.Windows.Forms.Panel
        Me.Label32 = New System.Windows.Forms.Label
        Me.cboEstadoCivil = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label26 = New System.Windows.Forms.Label
        Me.txtUsuarioNoLocalizable = New DevExpress.XtraEditors.TextEdit
        Me.lblmotivos_no_localizable = New System.Windows.Forms.Label
        Me.txtmotivos_no_localizable = New DevExpress.XtraEditors.MemoEdit
        Me.lblAbogado = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.chkNoPagaComision = New DevExpress.XtraEditors.CheckEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcSaldo = New Dipros.Editors.TINCalcEdit
        Me.lblTipo_Cobro = New System.Windows.Forms.Label
        Me.cboTipo_Cobro = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtycalle = New DevExpress.XtraEditors.TextEdit
        Me.txtentrecalle = New DevExpress.XtraEditors.TextEdit
        Me.lkpCiudad = New Dipros.Editors.TINMultiLookup
        Me.Label8 = New System.Windows.Forms.Label
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lblNombre = New System.Windows.Forms.Label
        Me.lblRfc = New System.Windows.Forms.Label
        Me.txtRfc = New DevExpress.XtraEditors.TextEdit
        Me.lblCurp = New System.Windows.Forms.Label
        Me.txtCurp = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lblCp = New System.Windows.Forms.Label
        Me.clcCp = New Dipros.Editors.TINCalcEdit
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.lblEstado = New System.Windows.Forms.Label
        Me.lblTelefono1 = New System.Windows.Forms.Label
        Me.txtTelefono1 = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono2 = New System.Windows.Forms.Label
        Me.txtTelefono2 = New DevExpress.XtraEditors.TextEdit
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtFax = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNumeroExterior = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtNumeroInterior = New DevExpress.XtraEditors.TextEdit
        Me.txtNombres = New DevExpress.XtraEditors.TextEdit
        Me.txtPaterno = New DevExpress.XtraEditors.TextEdit
        Me.lblPaterno = New System.Windows.Forms.Label
        Me.txtMaterno = New DevExpress.XtraEditors.TextEdit
        Me.lblMaterno = New System.Windows.Forms.Label
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.chkLocalizado = New DevExpress.XtraEditors.CheckEdit
        Me.txtEmpresa = New DevExpress.XtraEditors.TextEdit
        Me.lblPersona = New System.Windows.Forms.Label
        Me.cboPersona = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.btnLlamadasCliente = New DevExpress.XtraEditors.SimpleButton
        Me.pnlReferencias = New System.Windows.Forms.Panel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtnombre_conyuge = New DevExpress.XtraEditors.TextEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit
        Me.txttelefonos_laboral_conyuge = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.txtTelefonos_ocupacion = New DevExpress.XtraEditors.TextEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtAntiguedad = New DevExpress.XtraEditors.TextEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtDepto = New DevExpress.XtraEditors.TextEdit
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtPuesto = New DevExpress.XtraEditors.TextEdit
        Me.lblPuesto = New System.Windows.Forms.Label
        Me.lblOcupacion = New System.Windows.Forms.Label
        Me.txtOcupacion = New DevExpress.XtraEditors.TextEdit
        Me.lblIngresos = New System.Windows.Forms.Label
        Me.clcIngresos = New Dipros.Editors.TINCalcEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.clcLimiteCredito = New Dipros.Editors.TINCalcEdit
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.nvrDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosReferenciasLaborales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosAval = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrCobradores = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosReferenciasPersonales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosConvenios = New DevExpress.XtraNavBar.NavBarItem
        Me.pnlCobradores = New System.Windows.Forms.Panel
        Me.pnlAval = New System.Windows.Forms.Panel
        Me.Label19 = New System.Windows.Forms.Label
        Me.lkpClienteAval = New Dipros.Editors.TINMultiLookup
        Me.Label18 = New System.Windows.Forms.Label
        Me.clcAños_aval = New DevExpress.XtraEditors.CalcEdit
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtParentesco_Aval = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtEstado_aval = New DevExpress.XtraEditors.TextEdit
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtCiudad_Aval = New DevExpress.XtraEditors.TextEdit
        Me.pnlReferenciasPersonales = New System.Windows.Forms.Panel
        Me.lkpFormasPago = New Dipros.Editors.TINMultiLookup
        Me.Label34 = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.txtEmail = New DevExpress.XtraEditors.TextEdit
        Me.clcUltimosDigitosCuenta = New Dipros.Editors.TINCalcEdit
        Me.Label31 = New System.Windows.Forms.Label
        Me.txttrabaja_en_referencia = New DevExpress.XtraEditors.TextEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtparentesco_referencia = New DevExpress.XtraEditors.TextEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtestado_referencia = New DevExpress.XtraEditors.TextEdit
        Me.Label25 = New System.Windows.Forms.Label
        Me.txtciudad_referencia = New DevExpress.XtraEditors.TextEdit
        Me.Label27 = New System.Windows.Forms.Label
        Me.txtnombre_referencia = New DevExpress.XtraEditors.TextEdit
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtdomicilio_referencia = New DevExpress.XtraEditors.TextEdit
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtcolonia_referencia = New DevExpress.XtraEditors.TextEdit
        Me.Label30 = New System.Windows.Forms.Label
        Me.txttelefono_referencia = New DevExpress.XtraEditors.TextEdit
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.btnReimprimir = New System.Windows.Forms.ToolBarButton
        Me.pnlConvenios = New System.Windows.Forms.Panel
        Me.tmaConvenios = New Dipros.Windows.TINMaster
        Me.grConvenios = New DevExpress.XtraGrid.GridControl
        Me.grvConvenios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcConvenio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreConvenio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkFacturacionEspecial = New System.Windows.Forms.CheckBox
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Alta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grCobradores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGenerales.SuspendLayout()
        CType(Me.cboEstadoCivil.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsuarioNoLocalizable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtmotivos_no_localizable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNoPagaComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo_Cobro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtycalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtentrecalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRfc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroExterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroInterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPaterno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMaterno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLocalizado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmpresa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboPersona.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlReferencias.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtnombre_conyuge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txttelefonos_laboral_conyuge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefonos_ocupacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAntiguedad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOcupacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIngresos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcLimiteCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCobradores.SuspendLayout()
        Me.pnlAval.SuspendLayout()
        CType(Me.clcAños_aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtParentesco_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado_aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlReferenciasPersonales.SuspendLayout()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcUltimosDigitosCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txttrabaja_en_referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtparentesco_referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtestado_referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtciudad_referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtnombre_referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtdomicilio_referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtcolonia_referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txttelefono_referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlConvenios.SuspendLayout()
        CType(Me.grConvenios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvConvenios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnReimprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(35125, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(167, 41)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCliente
        '
        Me.clcCliente.EditValue = "0"
        Me.clcCliente.Location = New System.Drawing.Point(217, 39)
        Me.clcCliente.MaxValue = 0
        Me.clcCliente.MinValue = 0
        Me.clcCliente.Name = "clcCliente"
        '
        'clcCliente.Properties
        '
        Me.clcCliente.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCliente.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCliente.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCliente.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCliente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCliente.Size = New System.Drawing.Size(95, 19)
        Me.clcCliente.TabIndex = 1
        Me.clcCliente.Tag = "cliente"
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(344, 38)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.Enabled = False
        Me.txtNombre.Properties.MaxLength = 100
        Me.txtNombre.Size = New System.Drawing.Size(96, 20)
        Me.txtNombre.TabIndex = 60
        Me.txtNombre.TabStop = False
        Me.txtNombre.Tag = "nombre"
        Me.txtNombre.Visible = False
        '
        'lblNotas
        '
        Me.lblNotas.AutoSize = True
        Me.lblNotas.Location = New System.Drawing.Point(168, 376)
        Me.lblNotas.Name = "lblNotas"
        Me.lblNotas.Size = New System.Drawing.Size(41, 16)
        Me.lblNotas.TabIndex = 13
        Me.lblNotas.Tag = ""
        Me.lblNotas.Text = "Nota&s:"
        Me.lblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNotas
        '
        Me.txtNotas.EditValue = ""
        Me.txtNotas.Location = New System.Drawing.Point(216, 376)
        Me.txtNotas.Name = "txtNotas"
        '
        'txtNotas.Properties
        '
        Me.txtNotas.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtNotas.Size = New System.Drawing.Size(456, 43)
        Me.txtNotas.TabIndex = 59
        Me.txtNotas.Tag = "notas"
        '
        'lblAval
        '
        Me.lblAval.AutoSize = True
        Me.lblAval.Location = New System.Drawing.Point(32, 44)
        Me.lblAval.Name = "lblAval"
        Me.lblAval.Size = New System.Drawing.Size(53, 16)
        Me.lblAval.TabIndex = 0
        Me.lblAval.Tag = ""
        Me.lblAval.Text = "Nombre:"
        Me.lblAval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAval
        '
        Me.txtAval.EditValue = ""
        Me.txtAval.Location = New System.Drawing.Point(89, 41)
        Me.txtAval.Name = "txtAval"
        '
        'txtAval.Properties
        '
        Me.txtAval.Properties.MaxLength = 100
        Me.txtAval.Size = New System.Drawing.Size(600, 20)
        Me.txtAval.TabIndex = 1
        Me.txtAval.Tag = "aval"
        '
        'lblDomicilio_Aval
        '
        Me.lblDomicilio_Aval.AutoSize = True
        Me.lblDomicilio_Aval.Location = New System.Drawing.Point(26, 69)
        Me.lblDomicilio_Aval.Name = "lblDomicilio_Aval"
        Me.lblDomicilio_Aval.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio_Aval.TabIndex = 2
        Me.lblDomicilio_Aval.Tag = ""
        Me.lblDomicilio_Aval.Text = "Domici&lio:"
        Me.lblDomicilio_Aval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio_Aval
        '
        Me.txtDomicilio_Aval.EditValue = ""
        Me.txtDomicilio_Aval.Location = New System.Drawing.Point(89, 66)
        Me.txtDomicilio_Aval.Name = "txtDomicilio_Aval"
        '
        'txtDomicilio_Aval.Properties
        '
        Me.txtDomicilio_Aval.Properties.MaxLength = 50
        Me.txtDomicilio_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtDomicilio_Aval.TabIndex = 3
        Me.txtDomicilio_Aval.Tag = "domicilio_aval"
        '
        'lblColonia_Aval
        '
        Me.lblColonia_Aval.AutoSize = True
        Me.lblColonia_Aval.Location = New System.Drawing.Point(35, 94)
        Me.lblColonia_Aval.Name = "lblColonia_Aval"
        Me.lblColonia_Aval.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia_Aval.TabIndex = 4
        Me.lblColonia_Aval.Tag = ""
        Me.lblColonia_Aval.Text = "Colonia:"
        Me.lblColonia_Aval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColonia_Aval
        '
        Me.txtColonia_Aval.EditValue = ""
        Me.txtColonia_Aval.Location = New System.Drawing.Point(89, 91)
        Me.txtColonia_Aval.Name = "txtColonia_Aval"
        '
        'txtColonia_Aval.Properties
        '
        Me.txtColonia_Aval.Properties.MaxLength = 50
        Me.txtColonia_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtColonia_Aval.TabIndex = 5
        Me.txtColonia_Aval.Tag = "colonia_aval"
        '
        'lblTelefono_Aval
        '
        Me.lblTelefono_Aval.AutoSize = True
        Me.lblTelefono_Aval.Location = New System.Drawing.Point(525, 69)
        Me.lblTelefono_Aval.Name = "lblTelefono_Aval"
        Me.lblTelefono_Aval.Size = New System.Drawing.Size(56, 16)
        Me.lblTelefono_Aval.TabIndex = 12
        Me.lblTelefono_Aval.Tag = ""
        Me.lblTelefono_Aval.Text = "Teléfono:"
        Me.lblTelefono_Aval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono_Aval
        '
        Me.txtTelefono_Aval.EditValue = ""
        Me.txtTelefono_Aval.Location = New System.Drawing.Point(585, 66)
        Me.txtTelefono_Aval.Name = "txtTelefono_Aval"
        '
        'txtTelefono_Aval.Properties
        '
        Me.txtTelefono_Aval.Properties.MaxLength = 13
        Me.txtTelefono_Aval.Size = New System.Drawing.Size(104, 20)
        Me.txtTelefono_Aval.TabIndex = 13
        Me.txtTelefono_Aval.Tag = "telefono_aval"
        '
        'lblFecha_Alta
        '
        Me.lblFecha_Alta.AutoSize = True
        Me.lblFecha_Alta.Location = New System.Drawing.Point(640, 41)
        Me.lblFecha_Alta.Name = "lblFecha_Alta"
        Me.lblFecha_Alta.Size = New System.Drawing.Size(83, 16)
        Me.lblFecha_Alta.TabIndex = 2
        Me.lblFecha_Alta.Tag = ""
        Me.lblFecha_Alta.Text = "Fec&ha de alta:"
        Me.lblFecha_Alta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Alta
        '
        Me.dteFecha_Alta.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha_Alta.Location = New System.Drawing.Point(736, 38)
        Me.dteFecha_Alta.Name = "dteFecha_Alta"
        '
        'dteFecha_Alta.Properties
        '
        Me.dteFecha_Alta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Alta.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Alta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Alta.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Alta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Alta.Size = New System.Drawing.Size(96, 23)
        Me.dteFecha_Alta.TabIndex = 3
        Me.dteFecha_Alta.TabStop = False
        Me.dteFecha_Alta.Tag = "fecha_alta"
        '
        'grCobradores
        '
        '
        'grCobradores.EmbeddedNavigator
        '
        Me.grCobradores.EmbeddedNavigator.Name = ""
        Me.grCobradores.Location = New System.Drawing.Point(8, 32)
        Me.grCobradores.MainView = Me.grvOrdenesCompra
        Me.grCobradores.Name = "grCobradores"
        Me.grCobradores.Size = New System.Drawing.Size(688, 184)
        Me.grCobradores.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grCobradores.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.TabIndex = 62
        Me.grCobradores.TabStop = False
        Me.grCobradores.Text = "Cobradores"
        '
        'grvOrdenesCompra
        '
        Me.grvOrdenesCompra.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcCobrador, Me.grcNombre, Me.grcSucursal, Me.grcNombreSucursal})
        Me.grvOrdenesCompra.GridControl = Me.grCobradores
        Me.grvOrdenesCompra.Name = "grvOrdenesCompra"
        Me.grvOrdenesCompra.OptionsBehavior.Editable = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowFilter = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowGroup = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowSort = False
        Me.grvOrdenesCompra.OptionsView.ShowGroupPanel = False
        '
        'grcCobrador
        '
        Me.grcCobrador.Caption = "Clave"
        Me.grcCobrador.FieldName = "cobrador"
        Me.grcCobrador.Name = "grcCobrador"
        Me.grcCobrador.VisibleIndex = 0
        Me.grcCobrador.Width = 89
        '
        'grcNombre
        '
        Me.grcNombre.Caption = "Nombre"
        Me.grcNombre.FieldName = "nombre_cobrador"
        Me.grcNombre.Name = "grcNombre"
        Me.grcNombre.VisibleIndex = 1
        Me.grcNombre.Width = 340
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.VisibleIndex = 2
        Me.grcNombreSucursal.Width = 269
        '
        'tmaCobradores
        '
        Me.tmaCobradores.BackColor = System.Drawing.Color.White
        Me.tmaCobradores.CanDelete = True
        Me.tmaCobradores.CanInsert = True
        Me.tmaCobradores.CanUpdate = False
        Me.tmaCobradores.Grid = Me.grCobradores
        Me.tmaCobradores.Location = New System.Drawing.Point(8, 8)
        Me.tmaCobradores.Name = "tmaCobradores"
        Me.tmaCobradores.Size = New System.Drawing.Size(688, 23)
        Me.tmaCobradores.TabIndex = 61
        Me.tmaCobradores.TabStop = False
        Me.tmaCobradores.Title = "Cobradores"
        Me.tmaCobradores.UpdateTitle = "un Registro"
        '
        'pnlGenerales
        '
        Me.pnlGenerales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlGenerales.Controls.Add(Me.Label32)
        Me.pnlGenerales.Controls.Add(Me.cboEstadoCivil)
        Me.pnlGenerales.Controls.Add(Me.Label26)
        Me.pnlGenerales.Controls.Add(Me.txtUsuarioNoLocalizable)
        Me.pnlGenerales.Controls.Add(Me.lblmotivos_no_localizable)
        Me.pnlGenerales.Controls.Add(Me.txtmotivos_no_localizable)
        Me.pnlGenerales.Controls.Add(Me.lblAbogado)
        Me.pnlGenerales.Controls.Add(Me.Label5)
        Me.pnlGenerales.Controls.Add(Me.lkpCliente)
        Me.pnlGenerales.Controls.Add(Me.chkNoPagaComision)
        Me.pnlGenerales.Controls.Add(Me.lblSucursal)
        Me.pnlGenerales.Controls.Add(Me.lkpSucursal)
        Me.pnlGenerales.Controls.Add(Me.Label2)
        Me.pnlGenerales.Controls.Add(Me.clcSaldo)
        Me.pnlGenerales.Controls.Add(Me.lblTipo_Cobro)
        Me.pnlGenerales.Controls.Add(Me.cboTipo_Cobro)
        Me.pnlGenerales.Controls.Add(Me.lkpEstado)
        Me.pnlGenerales.Controls.Add(Me.Label11)
        Me.pnlGenerales.Controls.Add(Me.Label10)
        Me.pnlGenerales.Controls.Add(Me.txtycalle)
        Me.pnlGenerales.Controls.Add(Me.txtentrecalle)
        Me.pnlGenerales.Controls.Add(Me.lkpCiudad)
        Me.pnlGenerales.Controls.Add(Me.Label8)
        Me.pnlGenerales.Controls.Add(Me.lkpMunicipio)
        Me.pnlGenerales.Controls.Add(Me.lblNombre)
        Me.pnlGenerales.Controls.Add(Me.lblRfc)
        Me.pnlGenerales.Controls.Add(Me.txtRfc)
        Me.pnlGenerales.Controls.Add(Me.lblCurp)
        Me.pnlGenerales.Controls.Add(Me.txtCurp)
        Me.pnlGenerales.Controls.Add(Me.lblDomicilio)
        Me.pnlGenerales.Controls.Add(Me.txtDomicilio)
        Me.pnlGenerales.Controls.Add(Me.lblColonia)
        Me.pnlGenerales.Controls.Add(Me.lblCp)
        Me.pnlGenerales.Controls.Add(Me.clcCp)
        Me.pnlGenerales.Controls.Add(Me.lblCiudad)
        Me.pnlGenerales.Controls.Add(Me.lblEstado)
        Me.pnlGenerales.Controls.Add(Me.lblTelefono1)
        Me.pnlGenerales.Controls.Add(Me.txtTelefono1)
        Me.pnlGenerales.Controls.Add(Me.lblTelefono2)
        Me.pnlGenerales.Controls.Add(Me.txtTelefono2)
        Me.pnlGenerales.Controls.Add(Me.lblFax)
        Me.pnlGenerales.Controls.Add(Me.txtFax)
        Me.pnlGenerales.Controls.Add(Me.Label3)
        Me.pnlGenerales.Controls.Add(Me.txtNumeroExterior)
        Me.pnlGenerales.Controls.Add(Me.Label4)
        Me.pnlGenerales.Controls.Add(Me.txtNumeroInterior)
        Me.pnlGenerales.Controls.Add(Me.txtNombres)
        Me.pnlGenerales.Controls.Add(Me.txtPaterno)
        Me.pnlGenerales.Controls.Add(Me.lblPaterno)
        Me.pnlGenerales.Controls.Add(Me.txtMaterno)
        Me.pnlGenerales.Controls.Add(Me.lblMaterno)
        Me.pnlGenerales.Controls.Add(Me.lkpColonia)
        Me.pnlGenerales.Controls.Add(Me.chkLocalizado)
        Me.pnlGenerales.Controls.Add(Me.txtEmpresa)
        Me.pnlGenerales.Location = New System.Drawing.Point(128, 88)
        Me.pnlGenerales.Name = "pnlGenerales"
        Me.pnlGenerales.Size = New System.Drawing.Size(704, 280)
        Me.pnlGenerales.TabIndex = 5
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(248, 184)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(61, 16)
        Me.Label32.TabIndex = 142
        Me.Label32.Tag = ""
        Me.Label32.Text = "Edo. Civil:"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstadoCivil
        '
        Me.cboEstadoCivil.EditValue = "S"
        Me.cboEstadoCivil.Location = New System.Drawing.Point(312, 184)
        Me.cboEstadoCivil.Name = "cboEstadoCivil"
        '
        'cboEstadoCivil.Properties
        '
        Me.cboEstadoCivil.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstadoCivil.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Casado (a)", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Soltero (a)", "S", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Otro", "O", -1)})
        Me.cboEstadoCivil.Size = New System.Drawing.Size(144, 23)
        Me.cboEstadoCivil.TabIndex = 143
        Me.cboEstadoCivil.Tag = "estado_civil"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(552, 256)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(133, 16)
        Me.Label26.TabIndex = 152
        Me.Label26.Tag = ""
        Me.Label26.Text = "&Usuario No Localizable:"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label26.Visible = False
        '
        'txtUsuarioNoLocalizable
        '
        Me.txtUsuarioNoLocalizable.EditValue = ""
        Me.txtUsuarioNoLocalizable.Location = New System.Drawing.Point(656, 161)
        Me.txtUsuarioNoLocalizable.Name = "txtUsuarioNoLocalizable"
        '
        'txtUsuarioNoLocalizable.Properties
        '
        Me.txtUsuarioNoLocalizable.Properties.MaxLength = 25
        Me.txtUsuarioNoLocalizable.Size = New System.Drawing.Size(32, 20)
        Me.txtUsuarioNoLocalizable.TabIndex = 153
        Me.txtUsuarioNoLocalizable.Tag = "usuario_no_localizable"
        Me.txtUsuarioNoLocalizable.Visible = False
        '
        'lblmotivos_no_localizable
        '
        Me.lblmotivos_no_localizable.AutoSize = True
        Me.lblmotivos_no_localizable.Location = New System.Drawing.Point(408, 256)
        Me.lblmotivos_no_localizable.Name = "lblmotivos_no_localizable"
        Me.lblmotivos_no_localizable.Size = New System.Drawing.Size(134, 16)
        Me.lblmotivos_no_localizable.TabIndex = 150
        Me.lblmotivos_no_localizable.Tag = ""
        Me.lblmotivos_no_localizable.Text = "&Motivos No Localizable:"
        Me.lblmotivos_no_localizable.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblmotivos_no_localizable.Visible = False
        '
        'txtmotivos_no_localizable
        '
        Me.txtmotivos_no_localizable.EditValue = ""
        Me.txtmotivos_no_localizable.Location = New System.Drawing.Point(656, 129)
        Me.txtmotivos_no_localizable.Name = "txtmotivos_no_localizable"
        '
        'txtmotivos_no_localizable.Properties
        '
        Me.txtmotivos_no_localizable.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtmotivos_no_localizable.Size = New System.Drawing.Size(32, 24)
        Me.txtmotivos_no_localizable.TabIndex = 151
        Me.txtmotivos_no_localizable.Tag = "motivos_no_localizable"
        Me.txtmotivos_no_localizable.Visible = False
        '
        'lblAbogado
        '
        Me.lblAbogado.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbogado.ForeColor = System.Drawing.Color.Red
        Me.lblAbogado.Location = New System.Drawing.Point(88, 256)
        Me.lblAbogado.Name = "lblAbogado"
        Me.lblAbogado.Size = New System.Drawing.Size(304, 16)
        Me.lblAbogado.TabIndex = 149
        Me.lblAbogado.Tag = ""
        Me.lblAbogado.Text = "El Cliente tiene un Proceso Jurídico"
        Me.lblAbogado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAbogado.Visible = False
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(6, 231)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 24)
        Me.Label5.TabIndex = 149
        Me.Label5.Tag = ""
        Me.Label5.Text = "Cliente dependencia:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(88, 232)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(520, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(300, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 150
        Me.lkpCliente.Tag = "cliente_dependencias"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'chkNoPagaComision
        '
        Me.chkNoPagaComision.Location = New System.Drawing.Point(544, 208)
        Me.chkNoPagaComision.Name = "chkNoPagaComision"
        '
        'chkNoPagaComision.Properties
        '
        Me.chkNoPagaComision.Properties.Caption = "No Paga Comisión"
        Me.chkNoPagaComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkNoPagaComision.Size = New System.Drawing.Size(128, 20)
        Me.chkNoPagaComision.TabIndex = 148
        Me.chkNoPagaComision.Tag = "no_paga_comision"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(30, 211)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 146
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Enabled = False
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(88, 208)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(300, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 147
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(502, 187)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 16)
        Me.Label2.TabIndex = 144
        Me.Label2.Tag = ""
        Me.Label2.Text = "Saldo:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldo.Location = New System.Drawing.Point(545, 185)
        Me.clcSaldo.MaxValue = 0
        Me.clcSaldo.MinValue = 0
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatString = "C2"
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatString = "C2"
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSaldo.Properties.Precision = 2
        Me.clcSaldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldo.Size = New System.Drawing.Size(95, 19)
        Me.clcSaldo.TabIndex = 145
        Me.clcSaldo.Tag = "saldo"
        '
        'lblTipo_Cobro
        '
        Me.lblTipo_Cobro.AutoSize = True
        Me.lblTipo_Cobro.Location = New System.Drawing.Point(2, 187)
        Me.lblTipo_Cobro.Name = "lblTipo_Cobro"
        Me.lblTipo_Cobro.Size = New System.Drawing.Size(84, 16)
        Me.lblTipo_Cobro.TabIndex = 140
        Me.lblTipo_Cobro.Tag = ""
        Me.lblTipo_Cobro.Text = "Tipo de cobro:"
        Me.lblTipo_Cobro.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo_Cobro
        '
        Me.cboTipo_Cobro.EditValue = "O"
        Me.cboTipo_Cobro.Location = New System.Drawing.Point(88, 184)
        Me.cboTipo_Cobro.Name = "cboTipo_Cobro"
        '
        'cboTipo_Cobro.Properties
        '
        Me.cboTipo_Cobro.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo_Cobro.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ocurre", "O", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cobrar", "C", -1)})
        Me.cboTipo_Cobro.Size = New System.Drawing.Size(144, 23)
        Me.cboTipo_Cobro.TabIndex = 141
        Me.cboTipo_Cobro.Tag = "tipo_cobro"
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(89, 64)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.ReadOnlyControl = False
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(144, 20)
        Me.lkpEstado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEstado.TabIndex = 115
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(292, 115)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(16, 16)
        Me.Label11.TabIndex = 125
        Me.Label11.Tag = ""
        Me.Label11.Text = "y:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(47, 115)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(39, 16)
        Me.Label10.TabIndex = 123
        Me.Label10.Tag = ""
        Me.Label10.Text = "Entre:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtycalle
        '
        Me.txtycalle.EditValue = ""
        Me.txtycalle.Location = New System.Drawing.Point(313, 112)
        Me.txtycalle.Name = "txtycalle"
        '
        'txtycalle.Properties
        '
        Me.txtycalle.Properties.MaxLength = 30
        Me.txtycalle.Size = New System.Drawing.Size(192, 20)
        Me.txtycalle.TabIndex = 126
        Me.txtycalle.Tag = "ycalle"
        '
        'txtentrecalle
        '
        Me.txtentrecalle.EditValue = ""
        Me.txtentrecalle.Location = New System.Drawing.Point(89, 112)
        Me.txtentrecalle.Name = "txtentrecalle"
        '
        'txtentrecalle.Properties
        '
        Me.txtentrecalle.Properties.MaxLength = 30
        Me.txtentrecalle.Size = New System.Drawing.Size(192, 20)
        Me.txtentrecalle.TabIndex = 124
        Me.txtentrecalle.Tag = "entrecalle"
        '
        'lkpCiudad
        '
        Me.lkpCiudad.AllowAdd = False
        Me.lkpCiudad.AutoReaload = True
        Me.lkpCiudad.DataSource = Nothing
        Me.lkpCiudad.DefaultSearchField = ""
        Me.lkpCiudad.DisplayMember = "descripcion"
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad.Filtered = False
        Me.lkpCiudad.InitValue = Nothing
        Me.lkpCiudad.Location = New System.Drawing.Point(545, 64)
        Me.lkpCiudad.MultiSelect = False
        Me.lkpCiudad.Name = "lkpCiudad"
        Me.lkpCiudad.NullText = ""
        Me.lkpCiudad.PopupWidth = CType(300, Long)
        Me.lkpCiudad.ReadOnlyControl = False
        Me.lkpCiudad.Required = False
        Me.lkpCiudad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCiudad.SearchMember = ""
        Me.lkpCiudad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCiudad.SelectAll = False
        Me.lkpCiudad.Size = New System.Drawing.Size(144, 20)
        Me.lkpCiudad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCiudad.TabIndex = 118
        Me.lkpCiudad.Tag = "ciudad"
        Me.lkpCiudad.ToolTip = Nothing
        Me.lkpCiudad.ValueMember = "ciudad"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(248, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 16)
        Me.Label8.TabIndex = 115
        Me.Label8.Tag = ""
        Me.Label8.Text = "Municipio:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = True
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(313, 64)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(300, Long)
        Me.lkpMunicipio.ReadOnlyControl = False
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(144, 20)
        Me.lkpMunicipio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMunicipio.TabIndex = 116
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(14, 19)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(72, 16)
        Me.lblNombre.TabIndex = 102
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre (s):"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRfc
        '
        Me.lblRfc.AutoSize = True
        Me.lblRfc.Location = New System.Drawing.Point(60, 40)
        Me.lblRfc.Name = "lblRfc"
        Me.lblRfc.Size = New System.Drawing.Size(26, 16)
        Me.lblRfc.TabIndex = 110
        Me.lblRfc.Tag = ""
        Me.lblRfc.Text = "R&fc:"
        Me.lblRfc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRfc
        '
        Me.txtRfc.EditValue = ""
        Me.txtRfc.Location = New System.Drawing.Point(89, 40)
        Me.txtRfc.Name = "txtRfc"
        '
        'txtRfc.Properties
        '
        Me.txtRfc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRfc.Properties.MaxLength = 15
        Me.txtRfc.Size = New System.Drawing.Size(120, 20)
        Me.txtRfc.TabIndex = 111
        Me.txtRfc.Tag = "rfc"
        '
        'lblCurp
        '
        Me.lblCurp.AutoSize = True
        Me.lblCurp.Location = New System.Drawing.Point(273, 40)
        Me.lblCurp.Name = "lblCurp"
        Me.lblCurp.Size = New System.Drawing.Size(35, 16)
        Me.lblCurp.TabIndex = 112
        Me.lblCurp.Tag = ""
        Me.lblCurp.Text = "C&urp:"
        Me.lblCurp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCurp
        '
        Me.txtCurp.EditValue = ""
        Me.txtCurp.Location = New System.Drawing.Point(313, 40)
        Me.txtCurp.Name = "txtCurp"
        '
        'txtCurp.Properties
        '
        Me.txtCurp.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCurp.Properties.MaxLength = 20
        Me.txtCurp.Size = New System.Drawing.Size(120, 20)
        Me.txtCurp.TabIndex = 113
        Me.txtCurp.Tag = "curp"
        '
        'lblDomicilio
        '
        Me.lblDomicilio.AutoSize = True
        Me.lblDomicilio.Location = New System.Drawing.Point(272, 91)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(36, 16)
        Me.lblDomicilio.TabIndex = 121
        Me.lblDomicilio.Tag = ""
        Me.lblDomicilio.Text = "Calle:"
        Me.lblDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(313, 88)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.MaxLength = 50
        Me.txtDomicilio.Size = New System.Drawing.Size(312, 20)
        Me.txtDomicilio.TabIndex = 122
        Me.txtDomicilio.Tag = "domicilio"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(36, 91)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 119
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "C&olonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCp
        '
        Me.lblCp.AutoSize = True
        Me.lblCp.Location = New System.Drawing.Point(518, 138)
        Me.lblCp.Name = "lblCp"
        Me.lblCp.Size = New System.Drawing.Size(24, 16)
        Me.lblCp.TabIndex = 131
        Me.lblCp.Tag = ""
        Me.lblCp.Text = "Cp&:"
        Me.lblCp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCp
        '
        Me.clcCp.EditValue = "0"
        Me.clcCp.Location = New System.Drawing.Point(545, 136)
        Me.clcCp.MaxValue = 0
        Me.clcCp.MinValue = 0
        Me.clcCp.Name = "clcCp"
        '
        'clcCp.Properties
        '
        Me.clcCp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.Enabled = False
        Me.clcCp.Properties.MaskData.EditMask = "#"
        Me.clcCp.Properties.MaxLength = 5
        Me.clcCp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCp.Size = New System.Drawing.Size(80, 19)
        Me.clcCp.TabIndex = 132
        Me.clcCp.Tag = "cp"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(482, 67)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(60, 16)
        Me.lblCiudad.TabIndex = 117
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "Locali&dad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(40, 67)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 114
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "E&stado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTelefono1
        '
        Me.lblTelefono1.AutoSize = True
        Me.lblTelefono1.Location = New System.Drawing.Point(23, 163)
        Me.lblTelefono1.Name = "lblTelefono1"
        Me.lblTelefono1.Size = New System.Drawing.Size(55, 16)
        Me.lblTelefono1.TabIndex = 133
        Me.lblTelefono1.Tag = ""
        Me.lblTelefono1.Text = "Tel. Casa"
        Me.lblTelefono1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono1
        '
        Me.txtTelefono1.EditValue = ""
        Me.txtTelefono1.Location = New System.Drawing.Point(89, 160)
        Me.txtTelefono1.Name = "txtTelefono1"
        '
        'txtTelefono1.Properties
        '
        Me.txtTelefono1.Properties.MaxLength = 25
        Me.txtTelefono1.Size = New System.Drawing.Size(144, 20)
        Me.txtTelefono1.TabIndex = 134
        Me.txtTelefono1.Tag = "telefono1"
        '
        'lblTelefono2
        '
        Me.lblTelefono2.AutoSize = True
        Me.lblTelefono2.Location = New System.Drawing.Point(254, 163)
        Me.lblTelefono2.Name = "lblTelefono2"
        Me.lblTelefono2.Size = New System.Drawing.Size(54, 16)
        Me.lblTelefono2.TabIndex = 135
        Me.lblTelefono2.Tag = ""
        Me.lblTelefono2.Text = "Tel. Of. :"
        Me.lblTelefono2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono2
        '
        Me.txtTelefono2.EditValue = ""
        Me.txtTelefono2.Location = New System.Drawing.Point(313, 160)
        Me.txtTelefono2.Name = "txtTelefono2"
        '
        'txtTelefono2.Properties
        '
        Me.txtTelefono2.Properties.MaxLength = 25
        Me.txtTelefono2.Size = New System.Drawing.Size(144, 20)
        Me.txtTelefono2.TabIndex = 136
        Me.txtTelefono2.Tag = "telefono2"
        '
        'lblFax
        '
        Me.lblFax.AutoSize = True
        Me.lblFax.Location = New System.Drawing.Point(496, 163)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(48, 16)
        Me.lblFax.TabIndex = 137
        Me.lblFax.Tag = ""
        Me.lblFax.Text = "Celular:"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFax
        '
        Me.txtFax.EditValue = ""
        Me.txtFax.Location = New System.Drawing.Point(545, 160)
        Me.txtFax.Name = "txtFax"
        '
        'txtFax.Properties
        '
        Me.txtFax.Properties.MaxLength = 13
        Me.txtFax.Size = New System.Drawing.Size(78, 20)
        Me.txtFax.TabIndex = 138
        Me.txtFax.Tag = "fax"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(34, 138)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 16)
        Me.Label3.TabIndex = 127
        Me.Label3.Tag = ""
        Me.Label3.Text = "Ex&terior:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumeroExterior
        '
        Me.txtNumeroExterior.EditValue = ""
        Me.txtNumeroExterior.Location = New System.Drawing.Point(89, 135)
        Me.txtNumeroExterior.Name = "txtNumeroExterior"
        '
        'txtNumeroExterior.Properties
        '
        Me.txtNumeroExterior.Properties.MaxLength = 10
        Me.txtNumeroExterior.Size = New System.Drawing.Size(80, 20)
        Me.txtNumeroExterior.TabIndex = 128
        Me.txtNumeroExterior.Tag = "numero_exterior"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(257, 138)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 16)
        Me.Label4.TabIndex = 129
        Me.Label4.Tag = ""
        Me.Label4.Text = "&Interior:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumeroInterior
        '
        Me.txtNumeroInterior.EditValue = ""
        Me.txtNumeroInterior.Location = New System.Drawing.Point(313, 135)
        Me.txtNumeroInterior.Name = "txtNumeroInterior"
        '
        'txtNumeroInterior.Properties
        '
        Me.txtNumeroInterior.Properties.MaxLength = 10
        Me.txtNumeroInterior.Size = New System.Drawing.Size(78, 20)
        Me.txtNumeroInterior.TabIndex = 130
        Me.txtNumeroInterior.Tag = "numero_interior"
        '
        'txtNombres
        '
        Me.txtNombres.EditValue = ""
        Me.txtNombres.Location = New System.Drawing.Point(89, 16)
        Me.txtNombres.Name = "txtNombres"
        '
        'txtNombres.Properties
        '
        Me.txtNombres.Properties.MaxLength = 40
        Me.txtNombres.Size = New System.Drawing.Size(144, 20)
        Me.txtNombres.TabIndex = 103
        Me.txtNombres.Tag = "nombres"
        '
        'txtPaterno
        '
        Me.txtPaterno.EditValue = ""
        Me.txtPaterno.Location = New System.Drawing.Point(313, 16)
        Me.txtPaterno.Name = "txtPaterno"
        '
        'txtPaterno.Properties
        '
        Me.txtPaterno.Properties.MaxLength = 30
        Me.txtPaterno.Size = New System.Drawing.Size(144, 20)
        Me.txtPaterno.TabIndex = 105
        Me.txtPaterno.Tag = "paterno"
        '
        'lblPaterno
        '
        Me.lblPaterno.AutoSize = True
        Me.lblPaterno.Location = New System.Drawing.Point(241, 19)
        Me.lblPaterno.Name = "lblPaterno"
        Me.lblPaterno.Size = New System.Drawing.Size(67, 16)
        Me.lblPaterno.TabIndex = 104
        Me.lblPaterno.Tag = ""
        Me.lblPaterno.Text = "A. Pater&no:"
        Me.lblPaterno.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMaterno
        '
        Me.txtMaterno.EditValue = ""
        Me.txtMaterno.Location = New System.Drawing.Point(545, 16)
        Me.txtMaterno.Name = "txtMaterno"
        '
        'txtMaterno.Properties
        '
        Me.txtMaterno.Properties.MaxLength = 30
        Me.txtMaterno.Size = New System.Drawing.Size(144, 20)
        Me.txtMaterno.TabIndex = 107
        Me.txtMaterno.Tag = "materno"
        '
        'lblMaterno
        '
        Me.lblMaterno.AutoSize = True
        Me.lblMaterno.Location = New System.Drawing.Point(473, 19)
        Me.lblMaterno.Name = "lblMaterno"
        Me.lblMaterno.Size = New System.Drawing.Size(69, 16)
        Me.lblMaterno.TabIndex = 106
        Me.lblMaterno.Tag = ""
        Me.lblMaterno.Text = "A. Matern&o:"
        Me.lblMaterno.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = True
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(89, 88)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(300, Long)
        Me.lkpColonia.ReadOnlyControl = False
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(144, 20)
        Me.lkpColonia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpColonia.TabIndex = 120
        Me.lkpColonia.Tag = "colonia"
        Me.lkpColonia.ToolTip = Nothing
        Me.lkpColonia.ValueMember = "colonia"
        '
        'chkLocalizado
        '
        Me.chkLocalizado.EditValue = True
        Me.chkLocalizado.Location = New System.Drawing.Point(544, 232)
        Me.chkLocalizado.Name = "chkLocalizado"
        '
        'chkLocalizado.Properties
        '
        Me.chkLocalizado.Properties.Caption = "Localizado"
        Me.chkLocalizado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkLocalizado.Size = New System.Drawing.Size(128, 20)
        Me.chkLocalizado.TabIndex = 141
        Me.chkLocalizado.Tag = "localizado"
        '
        'txtEmpresa
        '
        Me.txtEmpresa.EditValue = ""
        Me.txtEmpresa.Location = New System.Drawing.Point(88, 16)
        Me.txtEmpresa.Name = "txtEmpresa"
        '
        'txtEmpresa.Properties
        '
        Me.txtEmpresa.Properties.MaxLength = 100
        Me.txtEmpresa.Size = New System.Drawing.Size(600, 20)
        Me.txtEmpresa.TabIndex = 154
        Me.txtEmpresa.Tag = ""
        Me.txtEmpresa.Visible = False
        '
        'lblPersona
        '
        Me.lblPersona.AutoSize = True
        Me.lblPersona.Location = New System.Drawing.Point(160, 64)
        Me.lblPersona.Name = "lblPersona"
        Me.lblPersona.Size = New System.Drawing.Size(53, 16)
        Me.lblPersona.TabIndex = 108
        Me.lblPersona.Tag = ""
        Me.lblPersona.Text = "P&ersona:"
        Me.lblPersona.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPersona
        '
        Me.cboPersona.EditValue = "F"
        Me.cboPersona.Location = New System.Drawing.Point(216, 64)
        Me.cboPersona.Name = "cboPersona"
        '
        'cboPersona.Properties
        '
        Me.cboPersona.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPersona.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Física", "F", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Moral", "M", -1)})
        Me.cboPersona.Size = New System.Drawing.Size(96, 23)
        Me.cboPersona.TabIndex = 109
        Me.cboPersona.Tag = "persona"
        '
        'btnLlamadasCliente
        '
        Me.btnLlamadasCliente.Location = New System.Drawing.Point(704, 384)
        Me.btnLlamadasCliente.Name = "btnLlamadasCliente"
        Me.btnLlamadasCliente.Size = New System.Drawing.Size(128, 24)
        Me.btnLlamadasCliente.TabIndex = 150
        Me.btnLlamadasCliente.Text = "Llamadas al Cliente"
        '
        'pnlReferencias
        '
        Me.pnlReferencias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReferencias.Controls.Add(Me.GroupBox1)
        Me.pnlReferencias.Controls.Add(Me.txtTelefonos_ocupacion)
        Me.pnlReferencias.Controls.Add(Me.Label14)
        Me.pnlReferencias.Controls.Add(Me.txtAntiguedad)
        Me.pnlReferencias.Controls.Add(Me.Label13)
        Me.pnlReferencias.Controls.Add(Me.txtDepto)
        Me.pnlReferencias.Controls.Add(Me.Label12)
        Me.pnlReferencias.Controls.Add(Me.txtPuesto)
        Me.pnlReferencias.Controls.Add(Me.lblPuesto)
        Me.pnlReferencias.Controls.Add(Me.lblOcupacion)
        Me.pnlReferencias.Controls.Add(Me.txtOcupacion)
        Me.pnlReferencias.Controls.Add(Me.lblIngresos)
        Me.pnlReferencias.Controls.Add(Me.clcIngresos)
        Me.pnlReferencias.Controls.Add(Me.Label9)
        Me.pnlReferencias.Controls.Add(Me.clcLimiteCredito)
        Me.pnlReferencias.Location = New System.Drawing.Point(128, 88)
        Me.pnlReferencias.Name = "pnlReferencias"
        Me.pnlReferencias.Size = New System.Drawing.Size(704, 280)
        Me.pnlReferencias.TabIndex = 6
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.txtnombre_conyuge)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.TextEdit4)
        Me.GroupBox1.Controls.Add(Me.TextEdit3)
        Me.GroupBox1.Controls.Add(Me.txttelefonos_laboral_conyuge)
        Me.GroupBox1.Controls.Add(Me.TextEdit2)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 139)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(688, 124)
        Me.GroupBox1.TabIndex = 67
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Conyuge"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(20, 24)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(53, 16)
        Me.Label24.TabIndex = 68
        Me.Label24.Tag = ""
        Me.Label24.Text = "Nombre:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtnombre_conyuge
        '
        Me.txtnombre_conyuge.EditValue = ""
        Me.txtnombre_conyuge.Location = New System.Drawing.Point(76, 24)
        Me.txtnombre_conyuge.Name = "txtnombre_conyuge"
        '
        'txtnombre_conyuge.Properties
        '
        Me.txtnombre_conyuge.Properties.MaxLength = 80
        Me.txtnombre_conyuge.Size = New System.Drawing.Size(306, 20)
        Me.txtnombre_conyuge.TabIndex = 69
        Me.txtnombre_conyuge.Tag = "nombre_conyuge"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(448, 32)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(62, 16)
        Me.Label20.TabIndex = 76
        Me.Label20.Text = "Telefonos:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(24, 96)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(42, 16)
        Me.Label21.TabIndex = 74
        Me.Label21.Text = "Depto:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(26, 72)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(46, 16)
        Me.Label22.TabIndex = 72
        Me.Label22.Text = "Puesto:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(4, 48)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(68, 16)
        Me.Label23.TabIndex = 70
        Me.Label23.Tag = ""
        Me.Label23.Text = "Trabaja en:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = ""
        Me.TextEdit4.Location = New System.Drawing.Point(76, 48)
        Me.TextEdit4.Name = "TextEdit4"
        '
        'TextEdit4.Properties
        '
        Me.TextEdit4.Properties.MaxLength = 50
        Me.TextEdit4.Size = New System.Drawing.Size(306, 20)
        Me.TextEdit4.TabIndex = 71
        Me.TextEdit4.Tag = "trabaja_en_conyuge"
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = ""
        Me.TextEdit3.Location = New System.Drawing.Point(76, 72)
        Me.TextEdit3.Name = "TextEdit3"
        '
        'TextEdit3.Properties
        '
        Me.TextEdit3.Properties.MaxLength = 80
        Me.TextEdit3.Size = New System.Drawing.Size(306, 20)
        Me.TextEdit3.TabIndex = 73
        Me.TextEdit3.Tag = "puesto_conyuge"
        '
        'txttelefonos_laboral_conyuge
        '
        Me.txttelefonos_laboral_conyuge.EditValue = ""
        Me.txttelefonos_laboral_conyuge.Location = New System.Drawing.Point(520, 32)
        Me.txttelefonos_laboral_conyuge.Name = "txttelefonos_laboral_conyuge"
        '
        'txttelefonos_laboral_conyuge.Properties
        '
        Me.txttelefonos_laboral_conyuge.Properties.MaxLength = 25
        Me.txttelefonos_laboral_conyuge.Size = New System.Drawing.Size(144, 20)
        Me.txttelefonos_laboral_conyuge.TabIndex = 77
        Me.txttelefonos_laboral_conyuge.Tag = "telefonos_laboral_conyuge"
        '
        'TextEdit2
        '
        Me.TextEdit2.EditValue = ""
        Me.TextEdit2.Location = New System.Drawing.Point(76, 96)
        Me.TextEdit2.Name = "TextEdit2"
        '
        'TextEdit2.Properties
        '
        Me.TextEdit2.Properties.MaxLength = 80
        Me.TextEdit2.Size = New System.Drawing.Size(306, 20)
        Me.TextEdit2.TabIndex = 75
        Me.TextEdit2.Tag = "departamento_conyuge"
        '
        'txtTelefonos_ocupacion
        '
        Me.txtTelefonos_ocupacion.EditValue = ""
        Me.txtTelefonos_ocupacion.Location = New System.Drawing.Point(89, 80)
        Me.txtTelefonos_ocupacion.Name = "txtTelefonos_ocupacion"
        Me.txtTelefonos_ocupacion.Size = New System.Drawing.Size(144, 20)
        Me.txtTelefonos_ocupacion.TabIndex = 64
        Me.txtTelefonos_ocupacion.Tag = "telefonos_laboral"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(22, 88)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(62, 16)
        Me.Label14.TabIndex = 63
        Me.Label14.Text = "Telefonos:"
        '
        'txtAntiguedad
        '
        Me.txtAntiguedad.EditValue = ""
        Me.txtAntiguedad.Location = New System.Drawing.Point(89, 104)
        Me.txtAntiguedad.Name = "txtAntiguedad"
        Me.txtAntiguedad.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtAntiguedad.Size = New System.Drawing.Size(80, 20)
        Me.txtAntiguedad.TabIndex = 66
        Me.txtAntiguedad.Tag = "antiguedad_laboral"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(13, 112)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(71, 16)
        Me.Label13.TabIndex = 65
        Me.Label13.Text = "Antigüedad:"
        '
        'txtDepto
        '
        Me.txtDepto.EditValue = ""
        Me.txtDepto.Location = New System.Drawing.Point(89, 56)
        Me.txtDepto.Name = "txtDepto"
        Me.txtDepto.Size = New System.Drawing.Size(306, 20)
        Me.txtDepto.TabIndex = 62
        Me.txtDepto.Tag = "departamento"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(42, 64)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(42, 16)
        Me.Label12.TabIndex = 61
        Me.Label12.Text = "Depto:"
        '
        'txtPuesto
        '
        Me.txtPuesto.EditValue = ""
        Me.txtPuesto.Location = New System.Drawing.Point(89, 32)
        Me.txtPuesto.Name = "txtPuesto"
        Me.txtPuesto.Size = New System.Drawing.Size(306, 20)
        Me.txtPuesto.TabIndex = 60
        Me.txtPuesto.Tag = "puesto"
        '
        'lblPuesto
        '
        Me.lblPuesto.AutoSize = True
        Me.lblPuesto.Location = New System.Drawing.Point(38, 32)
        Me.lblPuesto.Name = "lblPuesto"
        Me.lblPuesto.Size = New System.Drawing.Size(46, 16)
        Me.lblPuesto.TabIndex = 63
        Me.lblPuesto.Text = "Puesto:"
        '
        'lblOcupacion
        '
        Me.lblOcupacion.AutoSize = True
        Me.lblOcupacion.Location = New System.Drawing.Point(16, 8)
        Me.lblOcupacion.Name = "lblOcupacion"
        Me.lblOcupacion.Size = New System.Drawing.Size(68, 16)
        Me.lblOcupacion.TabIndex = 57
        Me.lblOcupacion.Tag = ""
        Me.lblOcupacion.Text = "Trabaja en:"
        Me.lblOcupacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtOcupacion
        '
        Me.txtOcupacion.EditValue = ""
        Me.txtOcupacion.Location = New System.Drawing.Point(89, 8)
        Me.txtOcupacion.Name = "txtOcupacion"
        '
        'txtOcupacion.Properties
        '
        Me.txtOcupacion.Properties.MaxLength = 50
        Me.txtOcupacion.Size = New System.Drawing.Size(306, 20)
        Me.txtOcupacion.TabIndex = 58
        Me.txtOcupacion.Tag = "ocupacion"
        '
        'lblIngresos
        '
        Me.lblIngresos.AutoSize = True
        Me.lblIngresos.Location = New System.Drawing.Point(477, 32)
        Me.lblIngresos.Name = "lblIngresos"
        Me.lblIngresos.Size = New System.Drawing.Size(106, 16)
        Me.lblIngresos.TabIndex = 67
        Me.lblIngresos.Tag = ""
        Me.lblIngresos.Text = "In&gresos Mensual:"
        Me.lblIngresos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIngresos
        '
        Me.clcIngresos.EditValue = "0"
        Me.clcIngresos.Location = New System.Drawing.Point(586, 32)
        Me.clcIngresos.MaxValue = 0
        Me.clcIngresos.MinValue = 0
        Me.clcIngresos.Name = "clcIngresos"
        '
        'clcIngresos.Properties
        '
        Me.clcIngresos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIngresos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIngresos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcIngresos.Properties.Precision = 2
        Me.clcIngresos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIngresos.Size = New System.Drawing.Size(80, 19)
        Me.clcIngresos.TabIndex = 68
        Me.clcIngresos.Tag = "ingresos"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(479, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 16)
        Me.Label9.TabIndex = 61
        Me.Label9.Tag = ""
        Me.Label9.Text = "Límite de Crédito:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcLimiteCredito
        '
        Me.clcLimiteCredito.EditValue = "0"
        Me.clcLimiteCredito.Location = New System.Drawing.Point(586, 8)
        Me.clcLimiteCredito.MaxValue = 0
        Me.clcLimiteCredito.MinValue = 0
        Me.clcLimiteCredito.Name = "clcLimiteCredito"
        '
        'clcLimiteCredito.Properties
        '
        Me.clcLimiteCredito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLimiteCredito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLimiteCredito.Properties.Enabled = False
        Me.clcLimiteCredito.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcLimiteCredito.Properties.Precision = 2
        Me.clcLimiteCredito.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcLimiteCredito.Size = New System.Drawing.Size(80, 19)
        Me.clcLimiteCredito.TabIndex = 62
        Me.clcLimiteCredito.Tag = "limite_credito"
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvrDatosReferenciasLaborales, Me.nvrDatosGenerales, Me.nvrCobradores, Me.nvrDatosAval, Me.nvrDatosReferenciasPersonales, Me.nvrDatosConvenios})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 28)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(120, 404)
        Me.NavBarControl1.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.NavBarControl1.TabIndex = 4
        Me.NavBarControl1.Text = "Entrada al Almacen"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosReferenciasLaborales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosAval), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrCobradores), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosReferenciasPersonales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosConvenios)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'nvrDatosGenerales
        '
        Me.nvrDatosGenerales.Caption = "Datos Personales"
        Me.nvrDatosGenerales.LargeImage = CType(resources.GetObject("nvrDatosGenerales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosGenerales.Name = "nvrDatosGenerales"
        '
        'nvrDatosReferenciasLaborales
        '
        Me.nvrDatosReferenciasLaborales.Caption = "Referencias Laborales"
        Me.nvrDatosReferenciasLaborales.LargeImage = CType(resources.GetObject("nvrDatosReferenciasLaborales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosReferenciasLaborales.Name = "nvrDatosReferenciasLaborales"
        '
        'nvrDatosAval
        '
        Me.nvrDatosAval.Caption = "Datos Aval"
        Me.nvrDatosAval.LargeImage = CType(resources.GetObject("nvrDatosAval.LargeImage"), System.Drawing.Image)
        Me.nvrDatosAval.Name = "nvrDatosAval"
        '
        'nvrCobradores
        '
        Me.nvrCobradores.Caption = "Cobradores"
        Me.nvrCobradores.LargeImage = CType(resources.GetObject("nvrCobradores.LargeImage"), System.Drawing.Image)
        Me.nvrCobradores.Name = "nvrCobradores"
        '
        'nvrDatosReferenciasPersonales
        '
        Me.nvrDatosReferenciasPersonales.Caption = "Referencias Personales"
        Me.nvrDatosReferenciasPersonales.Name = "nvrDatosReferenciasPersonales"
        '
        'nvrDatosConvenios
        '
        Me.nvrDatosConvenios.Caption = "Convenios"
        Me.nvrDatosConvenios.Name = "nvrDatosConvenios"
        '
        'pnlCobradores
        '
        Me.pnlCobradores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCobradores.Controls.Add(Me.tmaCobradores)
        Me.pnlCobradores.Controls.Add(Me.grCobradores)
        Me.pnlCobradores.Location = New System.Drawing.Point(128, 88)
        Me.pnlCobradores.Name = "pnlCobradores"
        Me.pnlCobradores.Size = New System.Drawing.Size(704, 280)
        Me.pnlCobradores.TabIndex = 8
        '
        'pnlAval
        '
        Me.pnlAval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlAval.Controls.Add(Me.Label19)
        Me.pnlAval.Controls.Add(Me.lkpClienteAval)
        Me.pnlAval.Controls.Add(Me.Label18)
        Me.pnlAval.Controls.Add(Me.clcAños_aval)
        Me.pnlAval.Controls.Add(Me.Label17)
        Me.pnlAval.Controls.Add(Me.txtParentesco_Aval)
        Me.pnlAval.Controls.Add(Me.Label16)
        Me.pnlAval.Controls.Add(Me.txtEstado_aval)
        Me.pnlAval.Controls.Add(Me.Label15)
        Me.pnlAval.Controls.Add(Me.txtCiudad_Aval)
        Me.pnlAval.Controls.Add(Me.lblAval)
        Me.pnlAval.Controls.Add(Me.txtAval)
        Me.pnlAval.Controls.Add(Me.lblDomicilio_Aval)
        Me.pnlAval.Controls.Add(Me.txtDomicilio_Aval)
        Me.pnlAval.Controls.Add(Me.lblColonia_Aval)
        Me.pnlAval.Controls.Add(Me.txtColonia_Aval)
        Me.pnlAval.Controls.Add(Me.lblTelefono_Aval)
        Me.pnlAval.Controls.Add(Me.txtTelefono_Aval)
        Me.pnlAval.Location = New System.Drawing.Point(128, 88)
        Me.pnlAval.Name = "pnlAval"
        Me.pnlAval.Size = New System.Drawing.Size(704, 280)
        Me.pnlAval.TabIndex = 7
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(37, 19)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(48, 16)
        Me.Label19.TabIndex = 150
        Me.Label19.Tag = ""
        Me.Label19.Text = "Cuenta:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpClienteAval
        '
        Me.lkpClienteAval.AllowAdd = False
        Me.lkpClienteAval.AutoReaload = False
        Me.lkpClienteAval.DataSource = Nothing
        Me.lkpClienteAval.DefaultSearchField = ""
        Me.lkpClienteAval.DisplayMember = "cliente"
        Me.lkpClienteAval.EditValue = Nothing
        Me.lkpClienteAval.Filtered = False
        Me.lkpClienteAval.InitValue = Nothing
        Me.lkpClienteAval.Location = New System.Drawing.Point(89, 16)
        Me.lkpClienteAval.MultiSelect = False
        Me.lkpClienteAval.Name = "lkpClienteAval"
        Me.lkpClienteAval.NullText = ""
        Me.lkpClienteAval.PopupWidth = CType(520, Long)
        Me.lkpClienteAval.ReadOnlyControl = False
        Me.lkpClienteAval.Required = False
        Me.lkpClienteAval.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteAval.SearchMember = ""
        Me.lkpClienteAval.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteAval.SelectAll = False
        Me.lkpClienteAval.Size = New System.Drawing.Size(152, 20)
        Me.lkpClienteAval.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteAval.TabIndex = 149
        Me.lkpClienteAval.Tag = "cliente_aval"
        Me.lkpClienteAval.ToolTip = Nothing
        Me.lkpClienteAval.ValueMember = "Cliente"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(299, 195)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(309, 16)
        Me.Label18.TabIndex = 14
        Me.Label18.Tag = ""
        Me.Label18.Text = "En caso de ser conocido, tiempo de conocerlo en años:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAños_aval
        '
        Me.clcAños_aval.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcAños_aval.Location = New System.Drawing.Point(614, 192)
        Me.clcAños_aval.Name = "clcAños_aval"
        Me.clcAños_aval.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcAños_aval.Size = New System.Drawing.Size(75, 20)
        Me.clcAños_aval.TabIndex = 15
        Me.clcAños_aval.Tag = "anios_aval"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(16, 169)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(69, 16)
        Me.Label17.TabIndex = 10
        Me.Label17.Tag = ""
        Me.Label17.Text = "Parentesco:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtParentesco_Aval
        '
        Me.txtParentesco_Aval.EditValue = ""
        Me.txtParentesco_Aval.Location = New System.Drawing.Point(89, 166)
        Me.txtParentesco_Aval.Name = "txtParentesco_Aval"
        '
        'txtParentesco_Aval.Properties
        '
        Me.txtParentesco_Aval.Properties.MaxLength = 50
        Me.txtParentesco_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtParentesco_Aval.TabIndex = 11
        Me.txtParentesco_Aval.Tag = "parentesco_aval"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(39, 144)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(46, 16)
        Me.Label16.TabIndex = 8
        Me.Label16.Tag = ""
        Me.Label16.Text = "Estado:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstado_aval
        '
        Me.txtEstado_aval.EditValue = ""
        Me.txtEstado_aval.Location = New System.Drawing.Point(89, 141)
        Me.txtEstado_aval.Name = "txtEstado_aval"
        '
        'txtEstado_aval.Properties
        '
        Me.txtEstado_aval.Properties.MaxLength = 50
        Me.txtEstado_aval.Size = New System.Drawing.Size(300, 20)
        Me.txtEstado_aval.TabIndex = 9
        Me.txtEstado_aval.Tag = "estado_aval"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(38, 119)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(47, 16)
        Me.Label15.TabIndex = 6
        Me.Label15.Tag = ""
        Me.Label15.Text = "Ciudad:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudad_Aval
        '
        Me.txtCiudad_Aval.EditValue = ""
        Me.txtCiudad_Aval.Location = New System.Drawing.Point(89, 116)
        Me.txtCiudad_Aval.Name = "txtCiudad_Aval"
        '
        'txtCiudad_Aval.Properties
        '
        Me.txtCiudad_Aval.Properties.MaxLength = 50
        Me.txtCiudad_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudad_Aval.TabIndex = 7
        Me.txtCiudad_Aval.Tag = "ciudad_aval"
        '
        'pnlReferenciasPersonales
        '
        Me.pnlReferenciasPersonales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReferenciasPersonales.Controls.Add(Me.lkpFormasPago)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label34)
        Me.pnlReferenciasPersonales.Controls.Add(Me.lblEmail)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label33)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txtEmail)
        Me.pnlReferenciasPersonales.Controls.Add(Me.clcUltimosDigitosCuenta)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label31)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txttrabaja_en_referencia)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label6)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txtparentesco_referencia)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label7)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txtestado_referencia)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label25)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txtciudad_referencia)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label27)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txtnombre_referencia)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label28)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txtdomicilio_referencia)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label29)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txtcolonia_referencia)
        Me.pnlReferenciasPersonales.Controls.Add(Me.Label30)
        Me.pnlReferenciasPersonales.Controls.Add(Me.txttelefono_referencia)
        Me.pnlReferenciasPersonales.Location = New System.Drawing.Point(128, 88)
        Me.pnlReferenciasPersonales.Name = "pnlReferenciasPersonales"
        Me.pnlReferenciasPersonales.Size = New System.Drawing.Size(704, 280)
        Me.pnlReferenciasPersonales.TabIndex = 61
        '
        'lkpFormasPago
        '
        Me.lkpFormasPago.AllowAdd = False
        Me.lkpFormasPago.AutoReaload = True
        Me.lkpFormasPago.DataSource = Nothing
        Me.lkpFormasPago.DefaultSearchField = ""
        Me.lkpFormasPago.DisplayMember = "descripcion"
        Me.lkpFormasPago.EditValue = Nothing
        Me.lkpFormasPago.Filtered = False
        Me.lkpFormasPago.InitValue = Nothing
        Me.lkpFormasPago.Location = New System.Drawing.Point(88, 216)
        Me.lkpFormasPago.MultiSelect = False
        Me.lkpFormasPago.Name = "lkpFormasPago"
        Me.lkpFormasPago.NullText = ""
        Me.lkpFormasPago.PopupWidth = CType(400, Long)
        Me.lkpFormasPago.ReadOnlyControl = False
        Me.lkpFormasPago.Required = False
        Me.lkpFormasPago.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFormasPago.SearchMember = ""
        Me.lkpFormasPago.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFormasPago.SelectAll = True
        Me.lkpFormasPago.Size = New System.Drawing.Size(297, 20)
        Me.lkpFormasPago.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFormasPago.TabIndex = 88
        Me.lkpFormasPago.Tag = "forma_pago"
        Me.lkpFormasPago.ToolTip = "Seleccione una Forma de Pago"
        Me.lkpFormasPago.ValueMember = "forma_pago"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(9, 216)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(75, 16)
        Me.Label34.TabIndex = 87
        Me.Label34.Text = "Forma Pago:"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(45, 192)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(39, 16)
        Me.lblEmail.TabIndex = 86
        Me.lblEmail.Tag = ""
        Me.lblEmail.Text = "Email:"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(8, 240)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(92, 16)
        Me.Label33.TabIndex = 85
        Me.Label33.Tag = ""
        Me.Label33.Text = "Ultimos Digitos:"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEmail
        '
        Me.txtEmail.EditValue = ""
        Me.txtEmail.Location = New System.Drawing.Point(88, 192)
        Me.txtEmail.Name = "txtEmail"
        '
        'txtEmail.Properties
        '
        Me.txtEmail.Properties.MaxLength = 100
        Me.txtEmail.Size = New System.Drawing.Size(601, 20)
        Me.txtEmail.TabIndex = 84
        Me.txtEmail.Tag = "email"
        '
        'clcUltimosDigitosCuenta
        '
        Me.clcUltimosDigitosCuenta.EditValue = "0"
        Me.clcUltimosDigitosCuenta.Location = New System.Drawing.Point(104, 240)
        Me.clcUltimosDigitosCuenta.MaxValue = 0
        Me.clcUltimosDigitosCuenta.MinValue = 0
        Me.clcUltimosDigitosCuenta.Name = "clcUltimosDigitosCuenta"
        '
        'clcUltimosDigitosCuenta.Properties
        '
        Me.clcUltimosDigitosCuenta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimosDigitosCuenta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimosDigitosCuenta.Properties.Enabled = False
        Me.clcUltimosDigitosCuenta.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcUltimosDigitosCuenta.Properties.MaxLength = 4
        Me.clcUltimosDigitosCuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcUltimosDigitosCuenta.Size = New System.Drawing.Size(72, 19)
        Me.clcUltimosDigitosCuenta.TabIndex = 83
        Me.clcUltimosDigitosCuenta.Tag = "ultimos_digitos_cuenta"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(16, 166)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(68, 16)
        Me.Label31.TabIndex = 59
        Me.Label31.Tag = ""
        Me.Label31.Text = "Trabaja en:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txttrabaja_en_referencia
        '
        Me.txttrabaja_en_referencia.EditValue = ""
        Me.txttrabaja_en_referencia.Location = New System.Drawing.Point(88, 166)
        Me.txttrabaja_en_referencia.Name = "txttrabaja_en_referencia"
        '
        'txttrabaja_en_referencia.Properties
        '
        Me.txttrabaja_en_referencia.Properties.MaxLength = 50
        Me.txttrabaja_en_referencia.Size = New System.Drawing.Size(300, 20)
        Me.txttrabaja_en_referencia.TabIndex = 60
        Me.txttrabaja_en_referencia.Tag = "trabaja_en_referencia"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 140)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 16)
        Me.Label6.TabIndex = 24
        Me.Label6.Tag = ""
        Me.Label6.Text = "Parentesco:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtparentesco_referencia
        '
        Me.txtparentesco_referencia.EditValue = ""
        Me.txtparentesco_referencia.Location = New System.Drawing.Point(88, 141)
        Me.txtparentesco_referencia.Name = "txtparentesco_referencia"
        '
        'txtparentesco_referencia.Properties
        '
        Me.txtparentesco_referencia.Properties.MaxLength = 50
        Me.txtparentesco_referencia.Size = New System.Drawing.Size(300, 20)
        Me.txtparentesco_referencia.TabIndex = 25
        Me.txtparentesco_referencia.Tag = "parentesco_referencia"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(40, 116)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 16)
        Me.Label7.TabIndex = 22
        Me.Label7.Tag = ""
        Me.Label7.Text = "Estado:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtestado_referencia
        '
        Me.txtestado_referencia.EditValue = ""
        Me.txtestado_referencia.Location = New System.Drawing.Point(88, 116)
        Me.txtestado_referencia.Name = "txtestado_referencia"
        '
        'txtestado_referencia.Properties
        '
        Me.txtestado_referencia.Properties.MaxLength = 50
        Me.txtestado_referencia.Size = New System.Drawing.Size(300, 20)
        Me.txtestado_referencia.TabIndex = 23
        Me.txtestado_referencia.Tag = "estado_referencia"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(40, 92)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(47, 16)
        Me.Label25.TabIndex = 20
        Me.Label25.Tag = ""
        Me.Label25.Text = "Ciudad:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtciudad_referencia
        '
        Me.txtciudad_referencia.EditValue = ""
        Me.txtciudad_referencia.Location = New System.Drawing.Point(88, 91)
        Me.txtciudad_referencia.Name = "txtciudad_referencia"
        '
        'txtciudad_referencia.Properties
        '
        Me.txtciudad_referencia.Properties.MaxLength = 50
        Me.txtciudad_referencia.Size = New System.Drawing.Size(300, 20)
        Me.txtciudad_referencia.TabIndex = 21
        Me.txtciudad_referencia.Tag = "ciudad_referencia"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(32, 20)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(53, 16)
        Me.Label27.TabIndex = 14
        Me.Label27.Tag = ""
        Me.Label27.Text = "Nombre:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtnombre_referencia
        '
        Me.txtnombre_referencia.EditValue = ""
        Me.txtnombre_referencia.Location = New System.Drawing.Point(88, 16)
        Me.txtnombre_referencia.Name = "txtnombre_referencia"
        '
        'txtnombre_referencia.Properties
        '
        Me.txtnombre_referencia.Properties.MaxLength = 100
        Me.txtnombre_referencia.Size = New System.Drawing.Size(600, 20)
        Me.txtnombre_referencia.TabIndex = 15
        Me.txtnombre_referencia.Tag = "nombre_referencia"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(24, 44)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(59, 16)
        Me.Label28.TabIndex = 16
        Me.Label28.Tag = ""
        Me.Label28.Text = "Domici&lio:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtdomicilio_referencia
        '
        Me.txtdomicilio_referencia.EditValue = ""
        Me.txtdomicilio_referencia.Location = New System.Drawing.Point(88, 41)
        Me.txtdomicilio_referencia.Name = "txtdomicilio_referencia"
        '
        'txtdomicilio_referencia.Properties
        '
        Me.txtdomicilio_referencia.Properties.MaxLength = 50
        Me.txtdomicilio_referencia.Size = New System.Drawing.Size(300, 20)
        Me.txtdomicilio_referencia.TabIndex = 17
        Me.txtdomicilio_referencia.Tag = "domicilio_referencia"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(32, 68)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(50, 16)
        Me.Label29.TabIndex = 18
        Me.Label29.Tag = ""
        Me.Label29.Text = "Colonia:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtcolonia_referencia
        '
        Me.txtcolonia_referencia.EditValue = ""
        Me.txtcolonia_referencia.Location = New System.Drawing.Point(88, 66)
        Me.txtcolonia_referencia.Name = "txtcolonia_referencia"
        '
        'txtcolonia_referencia.Properties
        '
        Me.txtcolonia_referencia.Properties.MaxLength = 50
        Me.txtcolonia_referencia.Size = New System.Drawing.Size(300, 20)
        Me.txtcolonia_referencia.TabIndex = 19
        Me.txtcolonia_referencia.Tag = "colonia_referencia"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(528, 44)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(56, 16)
        Me.Label30.TabIndex = 26
        Me.Label30.Tag = ""
        Me.Label30.Text = "Teléfono:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txttelefono_referencia
        '
        Me.txttelefono_referencia.EditValue = ""
        Me.txttelefono_referencia.Location = New System.Drawing.Point(584, 41)
        Me.txttelefono_referencia.Name = "txttelefono_referencia"
        '
        'txttelefono_referencia.Properties
        '
        Me.txttelefono_referencia.Properties.MaxLength = 13
        Me.txttelefono_referencia.Size = New System.Drawing.Size(104, 20)
        Me.txttelefono_referencia.TabIndex = 27
        Me.txttelefono_referencia.Tag = "telefono_referencia"
        '
        'CheckBox1
        '
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Location = New System.Drawing.Point(344, 64)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.TabIndex = 151
        Me.CheckBox1.Tag = "activo"
        Me.CheckBox1.Text = "Activo"
        '
        'btnReimprimir
        '
        Me.btnReimprimir.Text = "Reimprimir"
        Me.btnReimprimir.ToolTipText = "Reimprime la Solicitud del Cliente "
        '
        'pnlConvenios
        '
        Me.pnlConvenios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlConvenios.Controls.Add(Me.tmaConvenios)
        Me.pnlConvenios.Controls.Add(Me.grConvenios)
        Me.pnlConvenios.Location = New System.Drawing.Point(128, 88)
        Me.pnlConvenios.Name = "pnlConvenios"
        Me.pnlConvenios.Size = New System.Drawing.Size(704, 280)
        Me.pnlConvenios.TabIndex = 153
        '
        'tmaConvenios
        '
        Me.tmaConvenios.BackColor = System.Drawing.Color.White
        Me.tmaConvenios.CanDelete = True
        Me.tmaConvenios.CanInsert = True
        Me.tmaConvenios.CanUpdate = False
        Me.tmaConvenios.Grid = Me.grConvenios
        Me.tmaConvenios.Location = New System.Drawing.Point(8, 8)
        Me.tmaConvenios.Name = "tmaConvenios"
        Me.tmaConvenios.Size = New System.Drawing.Size(688, 23)
        Me.tmaConvenios.TabIndex = 61
        Me.tmaConvenios.TabStop = False
        Me.tmaConvenios.Title = "Convenios"
        Me.tmaConvenios.UpdateTitle = "un Registro"
        '
        'grConvenios
        '
        '
        'grConvenios.EmbeddedNavigator
        '
        Me.grConvenios.EmbeddedNavigator.Name = ""
        Me.grConvenios.Location = New System.Drawing.Point(8, 32)
        Me.grConvenios.MainView = Me.grvConvenios
        Me.grConvenios.Name = "grConvenios"
        Me.grConvenios.Size = New System.Drawing.Size(688, 184)
        Me.grConvenios.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grConvenios.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.TabIndex = 62
        Me.grConvenios.TabStop = False
        Me.grConvenios.Text = "Cobradores"
        '
        'grvConvenios
        '
        Me.grvConvenios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcConvenio, Me.grcNombreConvenio})
        Me.grvConvenios.GridControl = Me.grConvenios
        Me.grvConvenios.Name = "grvConvenios"
        Me.grvConvenios.OptionsBehavior.Editable = False
        Me.grvConvenios.OptionsCustomization.AllowFilter = False
        Me.grvConvenios.OptionsCustomization.AllowGroup = False
        Me.grvConvenios.OptionsCustomization.AllowSort = False
        Me.grvConvenios.OptionsView.ShowGroupPanel = False
        '
        'grcConvenio
        '
        Me.grcConvenio.Caption = "Clave"
        Me.grcConvenio.FieldName = "convenio"
        Me.grcConvenio.Name = "grcConvenio"
        Me.grcConvenio.VisibleIndex = 0
        Me.grcConvenio.Width = 89
        '
        'grcNombreConvenio
        '
        Me.grcNombreConvenio.Caption = "Nombre"
        Me.grcNombreConvenio.FieldName = "nombre_convenio"
        Me.grcNombreConvenio.Name = "grcNombreConvenio"
        Me.grcNombreConvenio.VisibleIndex = 1
        Me.grcNombreConvenio.Width = 340
        '
        'chkFacturacionEspecial
        '
        Me.chkFacturacionEspecial.Location = New System.Drawing.Point(456, 64)
        Me.chkFacturacionEspecial.Name = "chkFacturacionEspecial"
        Me.chkFacturacionEspecial.Size = New System.Drawing.Size(152, 24)
        Me.chkFacturacionEspecial.TabIndex = 154
        Me.chkFacturacionEspecial.Tag = "facturacion_especial"
        Me.chkFacturacionEspecial.Text = "Facturación Especial"
        '
        'frmSolicitudesCliente
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.CanDelete = False
        Me.ClientSize = New System.Drawing.Size(842, 432)
        Me.Controls.Add(Me.chkFacturacionEspecial)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.clcCliente)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblNotas)
        Me.Controls.Add(Me.txtNotas)
        Me.Controls.Add(Me.lblFecha_Alta)
        Me.Controls.Add(Me.dteFecha_Alta)
        Me.Controls.Add(Me.btnLlamadasCliente)
        Me.Controls.Add(Me.lblPersona)
        Me.Controls.Add(Me.cboPersona)
        Me.Controls.Add(Me.pnlGenerales)
        Me.Controls.Add(Me.pnlAval)
        Me.Controls.Add(Me.pnlReferencias)
        Me.Controls.Add(Me.pnlCobradores)
        Me.Controls.Add(Me.pnlReferenciasPersonales)
        Me.Controls.Add(Me.pnlConvenios)
        Me.DefaultDock = True
        Me.Docked = True
        Me.Name = "frmSolicitudesCliente"
        Me.Controls.SetChildIndex(Me.pnlConvenios, 0)
        Me.Controls.SetChildIndex(Me.pnlReferenciasPersonales, 0)
        Me.Controls.SetChildIndex(Me.pnlCobradores, 0)
        Me.Controls.SetChildIndex(Me.pnlReferencias, 0)
        Me.Controls.SetChildIndex(Me.pnlAval, 0)
        Me.Controls.SetChildIndex(Me.pnlGenerales, 0)
        Me.Controls.SetChildIndex(Me.cboPersona, 0)
        Me.Controls.SetChildIndex(Me.lblPersona, 0)
        Me.Controls.SetChildIndex(Me.btnLlamadasCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Alta, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Alta, 0)
        Me.Controls.SetChildIndex(Me.txtNotas, 0)
        Me.Controls.SetChildIndex(Me.lblNotas, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.clcCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        Me.Controls.SetChildIndex(Me.CheckBox1, 0)
        Me.Controls.SetChildIndex(Me.chkFacturacionEspecial, 0)
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Alta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grCobradores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGenerales.ResumeLayout(False)
        CType(Me.cboEstadoCivil.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsuarioNoLocalizable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtmotivos_no_localizable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNoPagaComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo_Cobro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtycalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtentrecalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRfc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroExterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroInterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPaterno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMaterno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLocalizado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmpresa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboPersona.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlReferencias.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtnombre_conyuge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txttelefonos_laboral_conyuge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefonos_ocupacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAntiguedad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOcupacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIngresos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcLimiteCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCobradores.ResumeLayout(False)
        Me.pnlAval.ResumeLayout(False)
        CType(Me.clcAños_aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtParentesco_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado_aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlReferenciasPersonales.ResumeLayout(False)
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcUltimosDigitosCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txttrabaja_en_referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtparentesco_referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtestado_referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtciudad_referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtnombre_referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtdomicilio_referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtcolonia_referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txttelefono_referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlConvenios.ResumeLayout(False)
        CType(Me.grConvenios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvConvenios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientes As VillarrealBusiness.clsClientes
    Private oCobradores As VillarrealBusiness.clsCobradores
    Private oClientesCobradores As VillarrealBusiness.clsClientesCobradores
    Private oClientesConvenios As VillarrealBusiness.clsClientesConvenios
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private ociudades As VillarrealBusiness.clsCiudades
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oVariables As New VillarrealBusiness.clsVariables
    Private oColonias As New VillarrealBusiness.clsColonias
    Private oEstados As VillarrealBusiness.clsEstados
    Private oReportes As New VillarrealBusiness.Reportes
    Private oFormasPago As New VillarrealBusiness.clsFormasPagos


    'Private oLlamadasClientes As VillarrealBusiness.clsLlamadasClientes

    'INICIO Código modificado por Alberto 18/06/07
    Private UsuarioActual As String
    'FIN    Código modificado por Alberto 18/06/07

    Private rfc_completo As Boolean = False
    Private tipo_captura As String

    Private Entro_Despliega As Boolean = False
    Private Entro_Despliega_localizado As Boolean = False
    Public Entro_motivo As Boolean = False

    Private Grabando As Boolean = False
    Private solita_ultimos_digitos As Boolean = False

    Private SALIENDO As Boolean = False
    Private ACCION As Int16 = 1  ' 1 : INSERT   2: UPDATE


    Public Property Tipo_Cobro() As Char
        Get
            Select Case Me.cboTipo_Cobro.SelectedIndex
                Case 0
                    Return "O"
                Case 1
                    Return "C"
            End Select

        End Get
        Set(ByVal Value As Char)
            Select Case Value
                Case "O"
                    Me.cboTipo_Cobro.SelectedIndex = 0
                Case "C"
                    Me.cboTipo_Cobro.SelectedIndex = 1
            End Select
        End Set
    End Property
    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property
    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property
    Private ReadOnly Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCiudad)
        End Get
    End Property
    Private ReadOnly Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
    End Property
    Private ReadOnly Property FactorLimiteCredito() As Long
        Get
            Return CLng(oVariables.TraeDatos("factor_limite_credito", VillarrealBusiness.clsVariables.tipo_dato.Entero))
        End Get
    End Property

    Private ReadOnly Property CobradorCasa() As Long
        Get
            Return CLng(oVariables.TraeDatos("cobrador_casa", VillarrealBusiness.clsVariables.tipo_dato.Entero))
        End Get
    End Property
    Private ReadOnly Property ClienteAval() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteAval)
        End Get
    End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property Cobradores() As Long
        Get
            Dim ODATAVIEW As DataView
            Dim numero As Long
            ODATAVIEW = tmaCobradores.DataSource.Tables(0).DefaultView
            ODATAVIEW.RowFilter = "CONTROL <> 3"
            numero = ODATAVIEW.Count

            Return numero
        End Get
    End Property
    Public WriteOnly Property MotivoNoLocalizable() As String
        Set(ByVal Value As String)
            If Value.Trim.Length > 0 Then
                Me.txtmotivos_no_localizable.Text = Value
            Else
                Me.txtmotivos_no_localizable.Text = ""
            End If

        End Set
    End Property

    Private ReadOnly Property FormaPago() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFormasPago)
        End Get
    End Property
    Private ReadOnly Property Sucursal_Dependencia() As Boolean
        Get
            Return Comunes.clsUtilerias.uti_SucursalDependencia(Comunes.Common.Sucursal_Actual)
        End Get
    End Property
    'Public WriteOnly Property ClaveModificable() As Boolean
    '    Set(ByVal Value As Boolean)
    '        Me.clcCliente.Enabled = Value
    '    End Set
    'End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmClientes_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
        Grabando = False
    End Sub
    Private Sub frmClientes_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
        Grabando = True
    End Sub
    Private Sub frmClientes_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
        Grabando = False

        If ACCION = 1 Then
            ShowMessage(MessageType.MsgInformation, "El numero de Cliente asignado es: " + Me.clcCliente.Value.ToString, "Alta de Clientes")
            ImprimiReporte()
        Else
            ShowMessage(MessageType.MsgInformation, "El Cliente: " + Me.clcCliente.Value.ToString + ", Fue Actualizado Correctamente", "Alta de Clientes")
        End If

    End Sub

    Private Sub frmClientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                'valida si cliente existe en plantilla
                Dim oEvents As New Events
                Dim existe As Long
                oEvents = oClientes.ExisteEnPlantilla(Me.clcCliente.Value)
                existe = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("existe"), Long)
                If existe > 0 Then
                    Me.lkpCliente.Enabled = False
                End If
                oEvents = Nothing



            Case Actions.Delete


        End Select
    End Sub
    Private Sub frmClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If cboPersona.Text = "Física" Then
            'If Action = Actions.Accept Then
            Me.txtNombre.Text = Me.txtNombres.Text + " " + Me.txtPaterno.Text + " " + Me.txtMaterno.Text
            'Else
            'Me.txtNombre.Text = Me.txtNombres.Text + Me.txtPaterno.Text + Me.txtMaterno.Text
            'End If
        Else
            Me.txtNombre.Text = Me.txtEmpresa.Text
            Me.txtNombres.Text = ""
            Me.txtPaterno.Text = ""
            Me.txtMaterno.Text = ""
        End If


        If Me.clcCliente.Value = 0 Then
            ACCION = 1
            Response = oClientes.Insertar(Me.DataSource, rfc_completo, "C")
        Else
            ACCION = 2
            Response = oClientes.Actualizar(Me.DataSource, rfc_completo)
        End If

    End Sub
    Private Sub frmClientes_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail


        If Me.cboTipo_Cobro.Value = "O" And Cobradores = 0 Then
            Response = oClientesCobradores.Insertar(Me.clcCliente.EditValue, Me.CobradorCasa, Me.Sucursal)
        End If

        If Response.ErrorFound Then Exit Sub

        With tmaCobradores
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oClientesCobradores.Insertar(.SelectedRow, Me.clcCliente.EditValue)
                        'Case Actions.Update
                        'Response = oClientesCobradores.Actualizar(.SelectedRow, Me.clcCliente.EditValue)
                    Case Actions.Delete
                        Response = oClientesCobradores.Eliminar(Me.clcCliente.EditValue, .Item("cobrador"))
                End Select
                .MoveNext()
            Loop
        End With

        If Response.ErrorFound Then Exit Sub

        With tmaConvenios
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oClientesConvenios.Insertar(.SelectedRow, Me.clcCliente.EditValue)
                        'Case Actions.Update
                        'Response = oClientesCobradores.Actualizar(.SelectedRow, Me.clcCliente.EditValue)
                    Case Actions.Delete
                        Response = oClientesConvenios.Eliminar(Me.clcCliente.EditValue, .Item("convenio"))
                End Select
                .MoveNext()
            Loop
        End With

    End Sub
    'Private Sub frmClientes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
    'Entro_Despliega = True
    'Entro_Despliega_localizado = True
    'Dim oDataSet As DataSet
    'Response = oClientes.DespliegaDatos(Me.clcCliente.Value)
    'If Not Response.ErrorFound Then
    '    oDataSet = Response.Value
    '    Me.DataSource = oDataSet
    '    Me.lkpCiudad.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad")), 0, oDataSet.Tables(0).Rows(0).Item("ciudad"))
    '    Me.lkpColonia.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia")), 0, oDataSet.Tables(0).Rows(0).Item("colonia"))

    '    'EL CLIENTE TIENE UN PROCESO JURIDICO
    '    If ((Action.Update Or Action.Delete) And oDataSet.Tables(0).Rows(0).Item("cliente_en_juridico") = True) Then
    '        Me.lblAbogado.Text = oDataSet.Tables(0).Rows(0).Item("frase")
    '        Me.lblAbogado.Visible = True
    '    End If

    'End If

    'If Not Response.ErrorFound Then Response = oClientesCobradores.Listado(Me.clcCliente.EditValue)
    'If Not Response.ErrorFound Then
    '    oDataSet = Response.Value
    '    Me.tmaCobradores.DataSource = oDataSet
    'End If

    'oDataSet = Nothing

    'tipo_captura = CStr(IIf(IsDBNull(OwnerForm.Value("tipo_captura")), "", OwnerForm.Value("tipo_captura"))).ToUpper
    'CalculaLimiteCredito()
    'Entro_Despliega_localizado = False
    ''txtEmpresa.Text = OwnerForm.Value("nombre")
    'End Sub
    Private Sub frmClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oCobradores = New VillarrealBusiness.clsCobradores
        oClientesCobradores = New VillarrealBusiness.clsClientesCobradores
        oClientesConvenios = New VillarrealBusiness.clsClientesConvenios
        oSucursales = New VillarrealBusiness.clsSucursales
        ociudades = New VillarrealBusiness.clsCiudades
        oMunicipios = New VillarrealBusiness.clsMunicipios
        oEstados = New VillarrealBusiness.clsEstados

        UsuarioActual = VillarrealBusiness.BusinessEnvironment.Connection.User()

        Me.dteFecha_Alta.EditValue = CDate(TinApp.FechaServidor)

        If Not Me.Sucursal_Dependencia Then
            Me.tmaConvenios.CanInsert = False
            Me.tmaConvenios.CanDelete = False
            Me.grConvenios.Enabled = False
            Me.pnlConvenios.Enabled = False
        End If

        With Me.tmaCobradores
            .UpdateTitle = "un Cobrador"

            .UpdateForm = New Comunes.frmClientesCobradores
            .AddColumn("cobrador", "System.Int32")
            .AddColumn("nombre_cobrador")
            .AddColumn("sucursal", "System.Int32")
            .AddColumn("nombre_sucursal")
        End With

        With Me.tmaConvenios
            .UpdateTitle = "un Convenio"
            .UpdateForm = New frmClientesConvenios
            .AddColumn("convenio", "System.Int32")
            .AddColumn("nombre_convenio")
            .AddColumn("no_empleado")
            .AddColumn("clave_pago")
        End With


        Me.lkpCliente.Enabled = False
        Me.tipo_captura = "C"
        Me.btnLlamadasCliente.Enabled = False
        If UsuarioActual.ToUpper.Equals("SUPER") Then
            Me.lkpSucursal.Enabled = True
        End If


        Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual

    End Sub
    Private Sub frmClientes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

        Response = oClientes.Validacion(Action, txtNombres.Text, Me.txtPaterno.Text, txtRfc.Text, Me.cboPersona.EditValue, rfc_completo, Me.txtDomicilio.Text, Me.clcCp.EditValue, Estado, Municipio, Ciudad, Colonia, Me.Cobradores, IIf(IsNumeric(Me.clcIngresos.EditValue), Me.clcIngresos.EditValue, 0), cboTipo_Cobro.Value, lkpSucursal.EditValue, UsuarioActual, txtEmpresa.Text, Me.cboEstadoCivil.Value, Me.txtnombre_conyuge.Text, Me.txttelefonos_laboral_conyuge.Text, solita_ultimos_digitos, Me.clcUltimosDigitosCuenta.EditValue)

        If Not Response.ErrorFound Then Exit Sub
        Response = oClientes.ValidaSucursal(Sucursal)

        If Response.ErrorFound Then Exit Sub
        Response = oClientes.ValidaAval(ClienteAval, Me.txtAval.Text)

    End Sub
    Private Sub frmClientes_Localize() Handles MyBase.Localize
        Find("cliente", Me.clcCliente.EditValue)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub btnLlamadasCliente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLlamadasCliente.Click
        Dim oForm As New Comunes.frmLlamadasClientes

        With oForm
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Action = Actions.Update
            '.Refresh()
            .cliente = Me.clcCliente.EditValue
            .entro_catalogo_clientes = True


            .Show()
            .cliente = Me.clcCliente.EditValue

            Me.Enabled = False
        End With



    End Sub

    Private Sub txtNombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oClientes.ValidaNombre(txtNombre.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

    Private Sub txtDomicilio_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDomicilio.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oClientes.ValidaDomicilio(txtDomicilio.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub clcCp_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcCp.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oClientes.ValidaCp(clcCp.Value)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

    'Private Sub txtRfc_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtRfc.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oClientes.ValidaRfc(Me.txtRfc.Text, Me.cboPersona.EditValue, rfc_completo)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing

    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupPlantilla()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData
        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        'If Me.lkpMunicipio.DataSource Is Nothing Or Me.lkpMunicipio.EditValue Is Nothing Then Exit Sub
        If Entro_Despliega = True Then Exit Sub
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad_LoadData(True)

    End Sub

    Private Sub lkpCiudad_Format() Handles lkpCiudad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpCiudad)
    End Sub
    Private Sub lkpCiudad_LoadData(ByVal Initialize As Boolean) Handles lkpCiudad.LoadData
        Dim Response As New Events
        Response = ociudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCiudad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCiudad_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpCiudad.EditValueChanged
        If Me.lkpCiudad.EditValue Is Nothing Then Exit Sub

        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia_LoadData(True)
    End Sub

    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub
    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpColonia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpColonia.EditValueChanged
        clcCp.EditValue = lkpColonia.GetValue("codigo_postal")
    End Sub

    Private Sub lkpFormasPago_Format() Handles lkpFormasPago.Format
        Comunes.clsFormato.for_formas_pagos_grl(Me.lkpFormasPago)
    End Sub
    Private Sub lkpFormasPago_LoadData(ByVal Initialize As Boolean) Handles lkpFormasPago.LoadData
        Dim Response As New Events
        Response = oFormasPago.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFormasPago.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpFormasPago_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFormasPago.EditValueChanged
        If Me.FormaPago > 0 Then
            solita_ultimos_digitos = Me.lkpFormasPago.GetValue("solicitar_ulitmos_digitos")

            If solita_ultimos_digitos = True Then
                Me.clcUltimosDigitosCuenta.Enabled = True
            Else
                Me.clcUltimosDigitosCuenta.EditValue = 0
                Me.clcUltimosDigitosCuenta.Enabled = False
            End If
        End If
    End Sub


    Private Sub clcIngresos_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.clcIngresos.IsLoading Then Exit Sub
        If Me.clcIngresos.EditValue Is Nothing Or Not IsNumeric(Me.clcIngresos.EditValue) Then Exit Sub

        'If tipo_captura = "C" Then
        CalculaLimiteCredito()
        'End If
    End Sub

    Private Sub nvrDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosGenerales.LinkPressed
        Me.pnlGenerales.BringToFront()
    End Sub
    Private Sub nvrDatosReferenciasLaborales_LinkPressed(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosReferenciasLaborales.LinkPressed
        Me.pnlReferencias.BringToFront()
    End Sub
    Private Sub nvrCobradores_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrCobradores.LinkPressed
        Me.pnlCobradores.BringToFront()
    End Sub
    Private Sub nvrDatosAval_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosAval.LinkPressed
        Me.pnlAval.BringToFront()
    End Sub
    Private Sub nvrDatosReferenciasPersonales_LinkPressed(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosReferenciasPersonales.LinkPressed
        Me.pnlReferenciasPersonales.BringToFront()
    End Sub
    Private Sub nvrDatosConvenios_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosConvenios.LinkPressed
        Me.pnlConvenios.BringToFront()
    End Sub


    Private Sub lkpClienteAval_Format() Handles lkpClienteAval.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteAval)
    End Sub
    Private Sub lkpClienteAval_LoadData(ByVal Initialize As Boolean) Handles lkpClienteAval.LoadData

        If Grabando Then Exit Sub

        Dim Response As New Events
        Response = oClientes.LookupRepartoClienteExcluido(, Me.clcCliente.Value)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteAval.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If

        Response = Nothing

    End Sub
    Private Sub lkpClienteAval_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpClienteAval.EditValueChanged
        If ClienteAval = -1 Then
            Me.txtAval.Enabled = True
            Me.txtDomicilio_Aval.Enabled = True
            Me.txtColonia_Aval.Enabled = True
            Me.txtCiudad_Aval.Enabled = True
            Me.txtEstado_aval.Enabled = True
            Me.txtTelefono_Aval.Enabled = True

            Me.txtAval.Text = ""
            Me.txtDomicilio_Aval.Text = ""
            Me.txtColonia_Aval.Text = ""
            Me.txtCiudad_Aval.Text = ""
            Me.txtEstado_aval.Text = ""
            Me.txtParentesco_Aval.Text = ""
            Me.txtTelefono_Aval.Text = ""

        Else
            Me.txtAval.Enabled = False
            Me.txtDomicilio_Aval.Enabled = False
            Me.txtColonia_Aval.Enabled = False
            Me.txtCiudad_Aval.Enabled = False
            Me.txtEstado_aval.Enabled = False
            Me.txtTelefono_Aval.Enabled = False

            Me.txtAval.Text = Me.lkpClienteAval.GetValue("nombre")
            Me.txtDomicilio_Aval.Text = Me.lkpClienteAval.GetValue("direccion_aval_resultado")
            Me.txtColonia_Aval.Text = Me.lkpClienteAval.GetValue("nombre_colonia")
            Me.txtCiudad_Aval.Text = Me.lkpClienteAval.GetValue("ciudad")
            Me.txtEstado_aval.Text = Me.lkpClienteAval.GetValue("nombre_estado")
            Me.txtTelefono_Aval.Text = Me.lkpClienteAval.GetValue("telefono1")
        End If
    End Sub

    Private Sub chkLocalizado_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkLocalizado.EditValueChanged
        If CType(sender, DevExpress.XtraEditors.CheckEdit).Checked = False And Entro_Despliega_localizado = False Then
            Motivo()
        End If

        If CType(sender, DevExpress.XtraEditors.CheckEdit).Checked = False Then
            Me.txtUsuarioNoLocalizable.Text = ""
            Me.txtmotivos_no_localizable.Text = ""
        End If
    End Sub
    Private Sub cboPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPersona.SelectedIndexChanged
        If cboPersona.Text = "Física" Then
            lblNombre.Text = "&Nombre (s):"
            txtNombres.Visible = True
            lblPaterno.Visible = True
            txtPaterno.Visible = True
            lblMaterno.Visible = True
            txtMaterno.Visible = True
            txtEmpresa.Visible = False
            txtEmpresa.Text = ""
        Else
            lblNombre.Text = "&Nombre :"
            txtNombres.Visible = False
            lblPaterno.Visible = False
            txtPaterno.Visible = False
            lblMaterno.Visible = False
            txtMaterno.Visible = False
            txtEmpresa.Visible = True
        End If

    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.btnReimprimir Then
            Me.ImprimiReporte()
        End If

    End Sub

    Private Sub clcCliente_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcCliente.Validating
        VerificaCliente()
    End Sub

    'Private Sub frmSolicitudesCliente_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    '    e.Cancel = True
    'End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CalculaLimiteCredito()
        Me.clcLimiteCredito.EditValue = Me.clcIngresos.EditValue * FactorLimiteCredito
    End Sub

    Private Sub Motivo()

        Dim frmModal As New Comunes.frmClientesMotivoNoLocalizable
        With frmModal
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Show()
        End With
        Me.Enabled = False
    End Sub

    Public Sub ValidaCheckLocalizado()
        If TinApp.Connection.User.ToUpper <> "SUPER" Then

            Dim response As Events
            Dim aceptado As Boolean = False

            response = oClientesCobradores.Clientes_cobradores_valida_dias_cobrador(Me.clcCliente.Value)
            If Not response.ErrorFound Then
                Dim odataset As DataSet
                odataset = response.Value
                If odataset.Tables(0).Rows.Count > 0 Then
                    aceptado = odataset.Tables(0).Rows(0).Item("aceptado")
                End If
            End If
            If (Cobradores = 0) Or (aceptado = False) Or Me.txtmotivos_no_localizable.Text.Trim.Length = 0 Then
                Me.chkLocalizado.Checked = Not chkLocalizado.Checked
            Else
                'Asigno el Ususario que esta desmarcando el check
                Me.txtUsuarioNoLocalizable.Text = TinApp.Connection.User.ToUpper
            End If
        Else
            If Me.txtmotivos_no_localizable.Text.Trim.Length > 0 Then
                'Asigno el Ususario que esta desmarcando el check
                Me.txtUsuarioNoLocalizable.Text = TinApp.Connection.User.ToUpper
            Else
                Me.chkLocalizado.Checked = Not chkLocalizado.Checked
            End If
        End If
    End Sub

    Private Sub VerificaCliente()

        Dim response As Events
        response = oClientes.Existe(Me.clcCliente.Value)
        If response.ErrorFound Then
            response.ShowMessage()
            Exit Sub
        Else
            'If CType(response.Value, Long) > 0 Then
            DespliegaDatos()
            'Else
            '    Me.clcCliente.Value = 0


            'End If
        End If
    End Sub

    Private Sub DespliegaDatos()
        Dim response As Events

        Entro_Despliega = True
        Entro_Despliega_localizado = True
        Dim oDataSet As DataSet
        response = oClientes.DespliegaDatos(Me.clcCliente.Value)
        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.DataSource = oDataSet
            Me.lkpCiudad.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad")), 0, oDataSet.Tables(0).Rows(0).Item("ciudad"))
            Me.lkpColonia.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia")), 0, oDataSet.Tables(0).Rows(0).Item("colonia"))

            Me.cboEstadoCivil.Value = oDataSet.Tables(0).Rows(0).Item("estado_civil")
            tipo_captura = CStr(IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("tipo_captura")), "", oDataSet.Tables(0).Rows(0).Item("tipo_captura"))).ToUpper

            'EL CLIENTE TIENE UN PROCESO JURIDICO
            If ((Action.Update Or Action.Delete) And oDataSet.Tables(0).Rows(0).Item("cliente_en_juridico") = True) Then
                Me.lblAbogado.Text = oDataSet.Tables(0).Rows(0).Item("frase")
                Me.lblAbogado.Visible = True
            End If

            If Me.cboPersona.Value = "M" Then
                Me.txtEmpresa.Text = oDataSet.Tables(0).Rows(0).Item("nombre")
            End If

            If oDataSet.Tables(0).Rows(0).Item("sucursal") = -1 Then
                Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual
            End If

        End If

        If Not response.ErrorFound Then response = oClientesCobradores.Listado(Me.clcCliente.EditValue)
        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.tmaCobradores.DataSource = oDataSet
        End If

        response = oClientesConvenios.Listado(Me.clcCliente.EditValue)
        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.tmaConvenios.DataSource = oDataSet
        End If

        oDataSet = Nothing



        CalculaLimiteCredito()
        Entro_Despliega_localizado = False
        'txtEmpresa.Text = OwnerForm.Value("nombre")
    End Sub

    Private Sub ImprimiReporte()
        If Me.clcCliente.Value > 0 Then
            Dim response As Events
            response = oReportes.SolicitudCliente(Me.clcCliente.Value)

            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptSolicitudesClientes

                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)


                    If TinApp.Connection.User.ToUpper = "SUPER" Then
                        TinApp.ShowReport(Me.MdiParent, "Solicitud de Cliente", oReport, , , True, True)
                    Else
                        TinApp.ShowReport(Me.MdiParent, "Solicitud de Cliente", oReport)
                    End If


                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
                End If
            End If

        End If

    End Sub
#End Region


   
  
End Class