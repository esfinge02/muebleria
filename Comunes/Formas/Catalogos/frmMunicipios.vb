Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Imports System.Data
Imports System.Windows.Forms

Public Class frmMunicipios
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblMunicipio As System.Windows.Forms.Label
    Friend WithEvents clcMunicipio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMunicipios))
        Me.lblMunicipio = New System.Windows.Forms.Label
        Me.clcMunicipio = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblEstado = New System.Windows.Forms.Label
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        CType(Me.clcMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblMunicipio
        '
        Me.lblMunicipio.AutoSize = True
        Me.lblMunicipio.Location = New System.Drawing.Point(22, 40)
        Me.lblMunicipio.Name = "lblMunicipio"
        Me.lblMunicipio.Size = New System.Drawing.Size(60, 16)
        Me.lblMunicipio.TabIndex = 0
        Me.lblMunicipio.Tag = ""
        Me.lblMunicipio.Text = "&Municipio:"
        Me.lblMunicipio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMunicipio
        '
        Me.clcMunicipio.EditValue = "0"
        Me.clcMunicipio.Location = New System.Drawing.Point(87, 40)
        Me.clcMunicipio.MaxValue = 0
        Me.clcMunicipio.MinValue = 0
        Me.clcMunicipio.Name = "clcMunicipio"
        '
        'clcMunicipio.Properties
        '
        Me.clcMunicipio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcMunicipio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMunicipio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcMunicipio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMunicipio.Properties.Enabled = False
        Me.clcMunicipio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcMunicipio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMunicipio.Size = New System.Drawing.Size(52, 21)
        Me.clcMunicipio.TabIndex = 1
        Me.clcMunicipio.Tag = "municipio"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(10, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(87, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 40
        Me.txtDescripcion.Size = New System.Drawing.Size(240, 22)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(36, 88)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 4
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "&Estado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(88, 88)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(240, 22)
        Me.lkpEstado.TabIndex = 5
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'frmMunicipios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(354, 120)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblMunicipio)
        Me.Controls.Add(Me.clcMunicipio)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmMunicipios"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lblMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        CType(Me.clcMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private ociudades As VillarrealBusiness.clsCiudades
    Private oEstados As VillarrealBusiness.clsEstados

    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property

    
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmMunicipios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oMunicipios.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oMunicipios.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oMunicipios.Eliminar(lkpEstado.EditValue, clcMunicipio.Value)

        End Select
    End Sub

    Private Sub frmMunicipios_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oMunicipios.DespliegaDatos(OwnerForm.Value("estado"), OwnerForm.Value("municipio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmMunicipios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oMunicipios = New VillarrealBusiness.clsMunicipios
        ociudades = New VillarrealBusiness.clsCiudades
        oEstados = New VillarrealBusiness.clsEstados
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpEstado.Enabled = False
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmMunicipios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMunicipios.Validacion(Action, Me.txtDescripcion.Text, Estado)
    End Sub

    Private Sub frmMunicipios_Localize() Handles MyBase.Localize
        Find("municipio", Me.clcMunicipio.Value)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        'modFormatos.for_estado_grl(Me.lkpEstado)
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData
        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
