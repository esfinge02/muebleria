Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmClientesCobradores
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lblNombreCobrador As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblNombreSucursal As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmClientesCobradores))
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lblNombreCobrador = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblNombreSucursal = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = False
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(80, 38)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = ""
        Me.lkpCobrador.PopupWidth = CType(400, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = True
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = True
        Me.lkpCobrador.Size = New System.Drawing.Size(248, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 1
        Me.lkpCobrador.Tag = "cobrador"
        Me.lkpCobrador.ToolTip = Nothing
        Me.lkpCobrador.ValueMember = "cobrador"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(16, 40)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 0
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNombreCobrador
        '
        Me.lblNombreCobrador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNombreCobrador.Location = New System.Drawing.Point(24, 112)
        Me.lblNombreCobrador.Name = "lblNombreCobrador"
        Me.lblNombreCobrador.Size = New System.Drawing.Size(80, 23)
        Me.lblNombreCobrador.TabIndex = 63
        Me.lblNombreCobrador.Tag = "nombre_cobrador"
        '
        'lblSucursal
        '
        Me.lblSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSucursal.Location = New System.Drawing.Point(120, 112)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(80, 23)
        Me.lblSucursal.TabIndex = 64
        Me.lblSucursal.Tag = ""
        '
        'lblNombreSucursal
        '
        Me.lblNombreSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNombreSucursal.Location = New System.Drawing.Point(216, 112)
        Me.lblNombreSucursal.Name = "lblNombreSucursal"
        Me.lblNombreSucursal.Size = New System.Drawing.Size(80, 23)
        Me.lblNombreSucursal.TabIndex = 65
        Me.lblNombreSucursal.Tag = "nombre_sucursal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "Sucursal:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 64)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(248, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 3
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'frmClientesCobradores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(346, 96)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblNombreSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblNombreCobrador)
        Me.Controls.Add(Me.lkpCobrador)
        Me.Controls.Add(Me.lblCobrador)
        Me.Name = "frmClientesCobradores"
        Me.Text = "frmClientesCobradores"
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblNombreCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblNombreSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCobradores As VillarrealBusiness.clsCobradores
    Private oClientesCobradores As VillarrealBusiness.clsClientesCobradores
    Private oSucursales As VillarrealBusiness.clsSucursales

    Private ReadOnly Property Cobrador() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCobrador)
        End Get
    End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmClientesCobradores_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select

        End With
    End Sub
    Private Sub frmClientesCobradores_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCobradores = New VillarrealBusiness.clsCobradores
        oClientesCobradores = New VillarrealBusiness.clsClientesCobradores
        oSucursales = New VillarrealBusiness.clsSucursales

    End Sub
    Private Sub frmClientesCobradores_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

        ' SE PUSO POR QUE EN SOLICITUDES DE CLIENTES NO APARECE EL BOTON PARA ELIMINAR
        If Me.Action = Actions.Delete Then
            Me.tbrTools.Buttons(0).Visible = True
        End If
    End Sub
    Private Sub frmClientesCobradores_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.lkpCobrador.Validate()
        Response = oClientesCobradores.Validacion(Action, Cobrador, CType(OwnerForm.MasterControl.DataSource, DataSet), Me.lkpSucursal.EditValue, Me.lblNombreSucursal.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing

    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        Me.lblNombreSucursal.Text = CStr(Me.lkpSucursal.GetValue("nombre"))
    End Sub

    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub
    Private Sub lkpCobrador_LoadData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData
        Dim Response As New Events
        Response = oCobradores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCobrador_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCobrador.EditValueChanged
        Me.lblNombreCobrador.Text = CStr(Me.lkpCobrador.GetValue("nombre"))
        'Me.lblSucursal.Text = CStr(Me.lkpCobrador.GetValue("sucursal"))
        'Me.lblNombreSucursal.Text = CStr(Me.lkpCobrador.GetValue("nombre_sucursal"))
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

   

  
End Class
