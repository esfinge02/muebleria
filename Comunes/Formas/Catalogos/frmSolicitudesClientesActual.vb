Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmSolicitudesClienteActual
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
    Dim ClienteActual As Long = 0
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNotas As System.Windows.Forms.Label
    Friend WithEvents txtNotas As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblFecha_Alta As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Alta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grvOrdenesCompra As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcCobrador As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grCobradores As DevExpress.XtraGrid.GridControl
    Friend WithEvents tmaCobradores As Dipros.Windows.TINMaster
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nvrDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrCobradores As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents pnlCobradores As System.Windows.Forms.Panel
    Friend WithEvents lblPersona As System.Windows.Forms.Label
    Friend WithEvents cboPersona As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents nvrDatosAval As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents pnlReferencias As System.Windows.Forms.Panel
    Friend WithEvents pnlAval As System.Windows.Forms.Panel
    Friend WithEvents btnLlamadasCliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtnombre_conyuge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Public WithEvents clcCliente As Dipros.Editors.TINCalcEdit
    Friend WithEvents nvrDatosReferenciasLaborales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrDatosReferenciasPersonales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents txttelefonos_laboral_conyuge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents pnlConvenios As System.Windows.Forms.Panel
    Friend WithEvents tmaConvenios As Dipros.Windows.TINMaster
    Friend WithEvents grConvenios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvConvenios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcConvenio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreConvenio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents nvrDatosConvenios As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents chkFacturacionEspecial As System.Windows.Forms.CheckBox
    Friend WithEvents pnlPersonales As System.Windows.Forms.Panel
    Friend WithEvents PnlConyugue As System.Windows.Forms.Panel
    Friend WithEvents nvrConyugue As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents pnlReferenciasPersonales As System.Windows.Forms.Panel


    Friend WithEvents UcClientesReferencias1 As Comunes.ucClientesReferencias
    Friend WithEvents UcClientesPersonales1 As Comunes.ucClientesPersonales
    Friend WithEvents UcClientesAval1 As Comunes.ucClientesAval
    Friend WithEvents UcClientesLaborales1 As Comunes.ucClientesLaborales

    Friend WithEvents lkpFormasPago As Dipros.Editors.TINMultiLookup
    Public WithEvents clcUltimosDigitosCuenta As Dipros.Editors.TINCalcEdit

    Friend WithEvents txtDireccionConyugue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtemail_conyuge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txttelefono_conyuge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtdireccion_trabajo_conyuge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents nvrDirecciones As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents tmaDirecciones As Dipros.Windows.TINMaster
    Friend WithEvents grDirecciones As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDirecciones As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents pnlDirecciones As System.Windows.Forms.Panel
  
   
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dteFechaActualizacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPersonaAutoriza As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label




    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSolicitudesClienteActual))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.clcCliente = New Dipros.Editors.TINCalcEdit
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblNotas = New System.Windows.Forms.Label
        Me.txtNotas = New DevExpress.XtraEditors.MemoEdit
        Me.lblFecha_Alta = New System.Windows.Forms.Label
        Me.dteFecha_Alta = New DevExpress.XtraEditors.DateEdit
        Me.grCobradores = New DevExpress.XtraGrid.GridControl
        Me.grvOrdenesCompra = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcCobrador = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaCobradores = New Dipros.Windows.TINMaster
        Me.lblPersona = New System.Windows.Forms.Label
        Me.cboPersona = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.btnLlamadasCliente = New DevExpress.XtraEditors.SimpleButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtdireccion_trabajo_conyuge = New DevExpress.XtraEditors.TextEdit
        Me.txttelefono_conyuge = New DevExpress.XtraEditors.TextEdit
        Me.txtemail_conyuge = New DevExpress.XtraEditors.TextEdit
        Me.txtDireccionConyugue = New DevExpress.XtraEditors.TextEdit
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtnombre_conyuge = New DevExpress.XtraEditors.TextEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit
        Me.txttelefonos_laboral_conyuge = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.nvrDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrConyugue = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDirecciones = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosReferenciasLaborales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosAval = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosReferenciasPersonales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrDatosConvenios = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrCobradores = New DevExpress.XtraNavBar.NavBarItem
        Me.pnlCobradores = New System.Windows.Forms.Panel
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.btnReimprimir = New System.Windows.Forms.ToolBarButton
        Me.pnlConvenios = New System.Windows.Forms.Panel
        Me.tmaConvenios = New Dipros.Windows.TINMaster
        Me.grConvenios = New DevExpress.XtraGrid.GridControl
        Me.grvConvenios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcConvenio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreConvenio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkFacturacionEspecial = New System.Windows.Forms.CheckBox
        Me.PnlConyugue = New System.Windows.Forms.Panel
        Me.pnlPersonales = New System.Windows.Forms.Panel
        Me.UcClientesPersonales1 = New Comunes.ucClientesPersonales
        Me.pnlAval = New System.Windows.Forms.Panel
        Me.UcClientesAval1 = New Comunes.ucClientesAval
        Me.pnlReferenciasPersonales = New System.Windows.Forms.Panel
        Me.UcClientesReferencias1 = New Comunes.ucClientesReferencias
        Me.pnlReferencias = New System.Windows.Forms.Panel
        Me.UcClientesLaborales1 = New Comunes.ucClientesLaborales
        Me.lkpFormasPago = New Dipros.Editors.TINMultiLookup
        Me.clcUltimosDigitosCuenta = New Dipros.Editors.TINCalcEdit
        Me.pnlDirecciones = New System.Windows.Forms.Panel
        Me.tmaDirecciones = New Dipros.Windows.TINMaster
        Me.grDirecciones = New DevExpress.XtraGrid.GridControl
        Me.grvDirecciones = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dteFechaActualizacion = New DevExpress.XtraEditors.DateEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtPersonaAutoriza = New DevExpress.XtraEditors.TextEdit
        Me.Label7 = New System.Windows.Forms.Label
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Alta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grCobradores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboPersona.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtdireccion_trabajo_conyuge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txttelefono_conyuge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtemail_conyuge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccionConyugue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtnombre_conyuge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txttelefonos_laboral_conyuge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCobradores.SuspendLayout()
        Me.pnlConvenios.SuspendLayout()
        CType(Me.grConvenios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvConvenios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlConyugue.SuspendLayout()
        Me.pnlPersonales.SuspendLayout()
        Me.pnlAval.SuspendLayout()
        Me.pnlReferenciasPersonales.SuspendLayout()
        Me.pnlReferencias.SuspendLayout()
        CType(Me.clcUltimosDigitosCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDirecciones.SuspendLayout()
        CType(Me.grDirecciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDirecciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dteFechaActualizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPersonaAutoriza.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnReimprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(63294, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(144, 41)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCliente
        '
        Me.clcCliente.EditValue = "0"
        Me.clcCliente.Location = New System.Drawing.Point(192, 39)
        Me.clcCliente.MaxValue = 0
        Me.clcCliente.MinValue = 0
        Me.clcCliente.Name = "clcCliente"
        '
        'clcCliente.Properties
        '
        Me.clcCliente.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCliente.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCliente.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCliente.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCliente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCliente.Size = New System.Drawing.Size(95, 19)
        Me.clcCliente.TabIndex = 1
        Me.clcCliente.Tag = "cliente"
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(128, 38)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.Enabled = False
        Me.txtNombre.Properties.MaxLength = 100
        Me.txtNombre.Size = New System.Drawing.Size(40, 20)
        Me.txtNombre.TabIndex = 60
        Me.txtNombre.TabStop = False
        Me.txtNombre.Tag = "nombre"
        Me.txtNombre.Visible = False
        '
        'lblNotas
        '
        Me.lblNotas.AutoSize = True
        Me.lblNotas.Location = New System.Drawing.Point(168, 437)
        Me.lblNotas.Name = "lblNotas"
        Me.lblNotas.Size = New System.Drawing.Size(41, 16)
        Me.lblNotas.TabIndex = 13
        Me.lblNotas.Tag = ""
        Me.lblNotas.Text = "Nota&s:"
        Me.lblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNotas
        '
        Me.txtNotas.EditValue = ""
        Me.txtNotas.Location = New System.Drawing.Point(216, 437)
        Me.txtNotas.Name = "txtNotas"
        '
        'txtNotas.Properties
        '
        Me.txtNotas.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtNotas.Size = New System.Drawing.Size(456, 43)
        Me.txtNotas.TabIndex = 59
        Me.txtNotas.Tag = "notas"
        '
        'lblFecha_Alta
        '
        Me.lblFecha_Alta.AutoSize = True
        Me.lblFecha_Alta.Location = New System.Drawing.Point(59, 10)
        Me.lblFecha_Alta.Name = "lblFecha_Alta"
        Me.lblFecha_Alta.Size = New System.Drawing.Size(30, 16)
        Me.lblFecha_Alta.TabIndex = 2
        Me.lblFecha_Alta.Tag = ""
        Me.lblFecha_Alta.Text = "Alta:"
        Me.lblFecha_Alta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Alta
        '
        Me.dteFecha_Alta.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha_Alta.Location = New System.Drawing.Point(96, 8)
        Me.dteFecha_Alta.Name = "dteFecha_Alta"
        '
        'dteFecha_Alta.Properties
        '
        Me.dteFecha_Alta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Alta.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Alta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Alta.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Alta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Alta.Size = New System.Drawing.Size(96, 23)
        Me.dteFecha_Alta.TabIndex = 3
        Me.dteFecha_Alta.TabStop = False
        Me.dteFecha_Alta.Tag = "fecha_alta"
        '
        'grCobradores
        '
        '
        'grCobradores.EmbeddedNavigator
        '
        Me.grCobradores.EmbeddedNavigator.Name = ""
        Me.grCobradores.Location = New System.Drawing.Point(8, 32)
        Me.grCobradores.MainView = Me.grvOrdenesCompra
        Me.grCobradores.Name = "grCobradores"
        Me.grCobradores.Size = New System.Drawing.Size(688, 216)
        Me.grCobradores.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grCobradores.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCobradores.TabIndex = 62
        Me.grCobradores.TabStop = False
        Me.grCobradores.Text = "Cobradores"
        '
        'grvOrdenesCompra
        '
        Me.grvOrdenesCompra.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcCobrador, Me.grcNombre, Me.grcSucursal, Me.grcNombreSucursal})
        Me.grvOrdenesCompra.GridControl = Me.grCobradores
        Me.grvOrdenesCompra.Name = "grvOrdenesCompra"
        Me.grvOrdenesCompra.OptionsBehavior.Editable = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowFilter = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowGroup = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowSort = False
        Me.grvOrdenesCompra.OptionsView.ShowGroupPanel = False
        '
        'grcCobrador
        '
        Me.grcCobrador.Caption = "Clave"
        Me.grcCobrador.FieldName = "cobrador"
        Me.grcCobrador.Name = "grcCobrador"
        Me.grcCobrador.VisibleIndex = 0
        Me.grcCobrador.Width = 89
        '
        'grcNombre
        '
        Me.grcNombre.Caption = "Nombre"
        Me.grcNombre.FieldName = "nombre_cobrador"
        Me.grcNombre.Name = "grcNombre"
        Me.grcNombre.VisibleIndex = 1
        Me.grcNombre.Width = 340
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.VisibleIndex = 2
        Me.grcNombreSucursal.Width = 269
        '
        'tmaCobradores
        '
        Me.tmaCobradores.BackColor = System.Drawing.Color.White
        Me.tmaCobradores.CanDelete = True
        Me.tmaCobradores.CanInsert = True
        Me.tmaCobradores.CanUpdate = False
        Me.tmaCobradores.Grid = Me.grCobradores
        Me.tmaCobradores.Location = New System.Drawing.Point(8, 8)
        Me.tmaCobradores.Name = "tmaCobradores"
        Me.tmaCobradores.Size = New System.Drawing.Size(688, 23)
        Me.tmaCobradores.TabIndex = 61
        Me.tmaCobradores.TabStop = False
        Me.tmaCobradores.Title = "Cobradores"
        Me.tmaCobradores.UpdateTitle = "un Registro"
        '
        'lblPersona
        '
        Me.lblPersona.AutoSize = True
        Me.lblPersona.Location = New System.Drawing.Point(136, 62)
        Me.lblPersona.Name = "lblPersona"
        Me.lblPersona.Size = New System.Drawing.Size(53, 16)
        Me.lblPersona.TabIndex = 2
        Me.lblPersona.Tag = ""
        Me.lblPersona.Text = "P&ersona:"
        Me.lblPersona.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPersona
        '
        Me.cboPersona.EditValue = "F"
        Me.cboPersona.Location = New System.Drawing.Point(192, 62)
        Me.cboPersona.Name = "cboPersona"
        '
        'cboPersona.Properties
        '
        Me.cboPersona.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPersona.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Física", "F", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Moral", "M", -1)})
        Me.cboPersona.Size = New System.Drawing.Size(96, 23)
        Me.cboPersona.TabIndex = 3
        Me.cboPersona.Tag = "persona"
        '
        'btnLlamadasCliente
        '
        Me.btnLlamadasCliente.Location = New System.Drawing.Point(704, 440)
        Me.btnLlamadasCliente.Name = "btnLlamadasCliente"
        Me.btnLlamadasCliente.Size = New System.Drawing.Size(128, 24)
        Me.btnLlamadasCliente.TabIndex = 150
        Me.btnLlamadasCliente.Text = "Llamadas al Cliente"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtdireccion_trabajo_conyuge)
        Me.GroupBox1.Controls.Add(Me.txttelefono_conyuge)
        Me.GroupBox1.Controls.Add(Me.txtemail_conyuge)
        Me.GroupBox1.Controls.Add(Me.txtDireccionConyugue)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.txtnombre_conyuge)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.TextEdit4)
        Me.GroupBox1.Controls.Add(Me.TextEdit3)
        Me.GroupBox1.Controls.Add(Me.txttelefonos_laboral_conyuge)
        Me.GroupBox1.Controls.Add(Me.TextEdit2)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(688, 312)
        Me.GroupBox1.TabIndex = 67
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Conyuge"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(10, 224)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 16)
        Me.Label5.TabIndex = 82
        Me.Label5.Tag = ""
        Me.Label5.Text = "Direccion:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 16)
        Me.Label4.TabIndex = 74
        Me.Label4.Text = "Telefono:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 16)
        Me.Label3.TabIndex = 72
        Me.Label3.Tag = ""
        Me.Label3.Text = "Correo:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 16)
        Me.Label2.TabIndex = 70
        Me.Label2.Tag = ""
        Me.Label2.Text = "Direccion:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtdireccion_trabajo_conyuge
        '
        Me.txtdireccion_trabajo_conyuge.EditValue = ""
        Me.txtdireccion_trabajo_conyuge.Location = New System.Drawing.Point(76, 224)
        Me.txtdireccion_trabajo_conyuge.Name = "txtdireccion_trabajo_conyuge"
        '
        'txtdireccion_trabajo_conyuge.Properties
        '
        Me.txtdireccion_trabajo_conyuge.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtdireccion_trabajo_conyuge.Properties.MaxLength = 50
        Me.txtdireccion_trabajo_conyuge.Size = New System.Drawing.Size(306, 20)
        Me.txtdireccion_trabajo_conyuge.TabIndex = 83
        Me.txtdireccion_trabajo_conyuge.Tag = "direccion_trabajo_conyuge"
        '
        'txttelefono_conyuge
        '
        Me.txttelefono_conyuge.EditValue = ""
        Me.txttelefono_conyuge.Location = New System.Drawing.Point(76, 96)
        Me.txttelefono_conyuge.Name = "txttelefono_conyuge"
        '
        'txttelefono_conyuge.Properties
        '
        Me.txttelefono_conyuge.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txttelefono_conyuge.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txttelefono_conyuge.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txttelefono_conyuge.Properties.MaxLength = 13
        Me.txttelefono_conyuge.Size = New System.Drawing.Size(144, 20)
        Me.txttelefono_conyuge.TabIndex = 75
        Me.txttelefono_conyuge.Tag = "telefono_conyuge"
        '
        'txtemail_conyuge
        '
        Me.txtemail_conyuge.EditValue = ""
        Me.txtemail_conyuge.Location = New System.Drawing.Point(76, 72)
        Me.txtemail_conyuge.Name = "txtemail_conyuge"
        '
        'txtemail_conyuge.Properties
        '
        Me.txtemail_conyuge.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtemail_conyuge.Properties.MaxLength = 100
        Me.txtemail_conyuge.Size = New System.Drawing.Size(408, 20)
        Me.txtemail_conyuge.TabIndex = 73
        Me.txtemail_conyuge.Tag = "email_conyuge"
        '
        'txtDireccionConyugue
        '
        Me.txtDireccionConyugue.EditValue = ""
        Me.txtDireccionConyugue.Location = New System.Drawing.Point(76, 48)
        Me.txtDireccionConyugue.Name = "txtDireccionConyugue"
        '
        'txtDireccionConyugue.Properties
        '
        Me.txtDireccionConyugue.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccionConyugue.Properties.MaxLength = 50
        Me.txtDireccionConyugue.Size = New System.Drawing.Size(306, 20)
        Me.txtDireccionConyugue.TabIndex = 71
        Me.txtDireccionConyugue.Tag = "direccion_conyuge"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(19, 24)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(53, 16)
        Me.Label24.TabIndex = 68
        Me.Label24.Tag = ""
        Me.Label24.Text = "Nombre:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtnombre_conyuge
        '
        Me.txtnombre_conyuge.EditValue = ""
        Me.txtnombre_conyuge.Location = New System.Drawing.Point(76, 24)
        Me.txtnombre_conyuge.Name = "txtnombre_conyuge"
        '
        'txtnombre_conyuge.Properties
        '
        Me.txtnombre_conyuge.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnombre_conyuge.Properties.MaxLength = 80
        Me.txtnombre_conyuge.Size = New System.Drawing.Size(306, 20)
        Me.txtnombre_conyuge.TabIndex = 69
        Me.txtnombre_conyuge.Tag = "nombre_conyuge"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(8, 248)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(62, 16)
        Me.Label20.TabIndex = 84
        Me.Label20.Text = "Telefonos:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(28, 200)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(42, 16)
        Me.Label21.TabIndex = 80
        Me.Label21.Text = "Depto:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(24, 176)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(46, 16)
        Me.Label22.TabIndex = 78
        Me.Label22.Text = "Puesto:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(2, 152)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(68, 16)
        Me.Label23.TabIndex = 76
        Me.Label23.Tag = ""
        Me.Label23.Text = "Trabaja en:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = ""
        Me.TextEdit4.Location = New System.Drawing.Point(76, 152)
        Me.TextEdit4.Name = "TextEdit4"
        '
        'TextEdit4.Properties
        '
        Me.TextEdit4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit4.Properties.MaxLength = 50
        Me.TextEdit4.Size = New System.Drawing.Size(306, 20)
        Me.TextEdit4.TabIndex = 77
        Me.TextEdit4.Tag = "trabaja_en_conyuge"
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = ""
        Me.TextEdit3.Location = New System.Drawing.Point(76, 176)
        Me.TextEdit3.Name = "TextEdit3"
        '
        'TextEdit3.Properties
        '
        Me.TextEdit3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit3.Properties.MaxLength = 80
        Me.TextEdit3.Size = New System.Drawing.Size(306, 20)
        Me.TextEdit3.TabIndex = 79
        Me.TextEdit3.Tag = "puesto_conyuge"
        '
        'txttelefonos_laboral_conyuge
        '
        Me.txttelefonos_laboral_conyuge.EditValue = ""
        Me.txttelefonos_laboral_conyuge.Location = New System.Drawing.Point(76, 248)
        Me.txttelefonos_laboral_conyuge.Name = "txttelefonos_laboral_conyuge"
        '
        'txttelefonos_laboral_conyuge.Properties
        '
        Me.txttelefonos_laboral_conyuge.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txttelefonos_laboral_conyuge.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txttelefonos_laboral_conyuge.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txttelefonos_laboral_conyuge.Properties.MaxLength = 25
        Me.txttelefonos_laboral_conyuge.Size = New System.Drawing.Size(144, 20)
        Me.txttelefonos_laboral_conyuge.TabIndex = 85
        Me.txttelefonos_laboral_conyuge.Tag = "telefonos_laboral_conyuge"
        '
        'TextEdit2
        '
        Me.TextEdit2.EditValue = ""
        Me.TextEdit2.Location = New System.Drawing.Point(76, 200)
        Me.TextEdit2.Name = "TextEdit2"
        '
        'TextEdit2.Properties
        '
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.MaxLength = 80
        Me.TextEdit2.Size = New System.Drawing.Size(306, 20)
        Me.TextEdit2.TabIndex = 81
        Me.TextEdit2.Tag = "departamento_conyuge"
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvrDatosReferenciasLaborales, Me.nvrDatosGenerales, Me.nvrCobradores, Me.nvrDatosAval, Me.nvrDatosReferenciasPersonales, Me.nvrDatosConvenios, Me.nvrConyugue, Me.nvrDirecciones})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 28)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(120, 456)
        Me.NavBarControl1.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.NavBarControl1.TabIndex = 4
        Me.NavBarControl1.Text = "Entrada al Almacen"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrConyugue), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDirecciones), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosReferenciasLaborales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosAval), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosReferenciasPersonales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosConvenios), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrCobradores)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'nvrDatosGenerales
        '
        Me.nvrDatosGenerales.Caption = "Datos Personales"
        Me.nvrDatosGenerales.LargeImage = CType(resources.GetObject("nvrDatosGenerales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosGenerales.Name = "nvrDatosGenerales"
        '
        'nvrConyugue
        '
        Me.nvrConyugue.Caption = "Conyugue"
        Me.nvrConyugue.LargeImage = CType(resources.GetObject("nvrConyugue.LargeImage"), System.Drawing.Image)
        Me.nvrConyugue.Name = "nvrConyugue"
        '
        'nvrDirecciones
        '
        Me.nvrDirecciones.Caption = "Direcciones"
        Me.nvrDirecciones.LargeImage = CType(resources.GetObject("nvrDirecciones.LargeImage"), System.Drawing.Image)
        Me.nvrDirecciones.Name = "nvrDirecciones"
        '
        'nvrDatosReferenciasLaborales
        '
        Me.nvrDatosReferenciasLaborales.Caption = "Referencias Laborales"
        Me.nvrDatosReferenciasLaborales.LargeImage = CType(resources.GetObject("nvrDatosReferenciasLaborales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosReferenciasLaborales.Name = "nvrDatosReferenciasLaborales"
        '
        'nvrDatosAval
        '
        Me.nvrDatosAval.Caption = "Datos Aval"
        Me.nvrDatosAval.LargeImage = CType(resources.GetObject("nvrDatosAval.LargeImage"), System.Drawing.Image)
        Me.nvrDatosAval.Name = "nvrDatosAval"
        '
        'nvrDatosReferenciasPersonales
        '
        Me.nvrDatosReferenciasPersonales.Caption = "Referencias Personales"
        Me.nvrDatosReferenciasPersonales.LargeImage = CType(resources.GetObject("nvrDatosReferenciasPersonales.LargeImage"), System.Drawing.Image)
        Me.nvrDatosReferenciasPersonales.Name = "nvrDatosReferenciasPersonales"
        '
        'nvrDatosConvenios
        '
        Me.nvrDatosConvenios.Caption = "Convenios"
        Me.nvrDatosConvenios.LargeImage = CType(resources.GetObject("nvrDatosConvenios.LargeImage"), System.Drawing.Image)
        Me.nvrDatosConvenios.Name = "nvrDatosConvenios"
        '
        'nvrCobradores
        '
        Me.nvrCobradores.Caption = "Cobradores"
        Me.nvrCobradores.LargeImage = CType(resources.GetObject("nvrCobradores.LargeImage"), System.Drawing.Image)
        Me.nvrCobradores.Name = "nvrCobradores"
        '
        'pnlCobradores
        '
        Me.pnlCobradores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCobradores.Controls.Add(Me.tmaCobradores)
        Me.pnlCobradores.Controls.Add(Me.grCobradores)
        Me.pnlCobradores.Location = New System.Drawing.Point(136, 96)
        Me.pnlCobradores.Name = "pnlCobradores"
        Me.pnlCobradores.Size = New System.Drawing.Size(704, 336)
        Me.pnlCobradores.TabIndex = 8
        '
        'CheckBox1
        '
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Location = New System.Drawing.Point(304, 40)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.TabIndex = 4
        Me.CheckBox1.Tag = "activo"
        Me.CheckBox1.Text = "Activo"
        '
        'btnReimprimir
        '
        Me.btnReimprimir.Text = "Reimprimir"
        Me.btnReimprimir.ToolTipText = "Reimprime la Solicitud del Cliente "
        '
        'pnlConvenios
        '
        Me.pnlConvenios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlConvenios.Controls.Add(Me.tmaConvenios)
        Me.pnlConvenios.Controls.Add(Me.grConvenios)
        Me.pnlConvenios.Location = New System.Drawing.Point(136, 96)
        Me.pnlConvenios.Name = "pnlConvenios"
        Me.pnlConvenios.Size = New System.Drawing.Size(704, 336)
        Me.pnlConvenios.TabIndex = 153
        '
        'tmaConvenios
        '
        Me.tmaConvenios.BackColor = System.Drawing.Color.White
        Me.tmaConvenios.CanDelete = True
        Me.tmaConvenios.CanInsert = True
        Me.tmaConvenios.CanUpdate = False
        Me.tmaConvenios.Grid = Me.grConvenios
        Me.tmaConvenios.Location = New System.Drawing.Point(8, 8)
        Me.tmaConvenios.Name = "tmaConvenios"
        Me.tmaConvenios.Size = New System.Drawing.Size(688, 23)
        Me.tmaConvenios.TabIndex = 61
        Me.tmaConvenios.TabStop = False
        Me.tmaConvenios.Title = "Convenios"
        Me.tmaConvenios.UpdateTitle = "un Registro"
        '
        'grConvenios
        '
        '
        'grConvenios.EmbeddedNavigator
        '
        Me.grConvenios.EmbeddedNavigator.Name = ""
        Me.grConvenios.Location = New System.Drawing.Point(8, 32)
        Me.grConvenios.MainView = Me.grvConvenios
        Me.grConvenios.Name = "grConvenios"
        Me.grConvenios.Size = New System.Drawing.Size(688, 216)
        Me.grConvenios.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grConvenios.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grConvenios.TabIndex = 62
        Me.grConvenios.TabStop = False
        Me.grConvenios.Text = "Cobradores"
        '
        'grvConvenios
        '
        Me.grvConvenios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcConvenio, Me.grcNombreConvenio})
        Me.grvConvenios.GridControl = Me.grConvenios
        Me.grvConvenios.Name = "grvConvenios"
        Me.grvConvenios.OptionsBehavior.Editable = False
        Me.grvConvenios.OptionsCustomization.AllowFilter = False
        Me.grvConvenios.OptionsCustomization.AllowGroup = False
        Me.grvConvenios.OptionsCustomization.AllowSort = False
        Me.grvConvenios.OptionsView.ShowGroupPanel = False
        '
        'grcConvenio
        '
        Me.grcConvenio.Caption = "Clave"
        Me.grcConvenio.FieldName = "convenio"
        Me.grcConvenio.Name = "grcConvenio"
        Me.grcConvenio.VisibleIndex = 0
        Me.grcConvenio.Width = 89
        '
        'grcNombreConvenio
        '
        Me.grcNombreConvenio.Caption = "Nombre"
        Me.grcNombreConvenio.FieldName = "nombre_convenio"
        Me.grcNombreConvenio.Name = "grcNombreConvenio"
        Me.grcNombreConvenio.VisibleIndex = 1
        Me.grcNombreConvenio.Width = 340
        '
        'chkFacturacionEspecial
        '
        Me.chkFacturacionEspecial.Location = New System.Drawing.Point(304, 64)
        Me.chkFacturacionEspecial.Name = "chkFacturacionEspecial"
        Me.chkFacturacionEspecial.Size = New System.Drawing.Size(96, 24)
        Me.chkFacturacionEspecial.TabIndex = 5
        Me.chkFacturacionEspecial.Tag = "facturacion_especial"
        Me.chkFacturacionEspecial.Text = "Fac. Especial"
        '
        'PnlConyugue
        '
        Me.PnlConyugue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlConyugue.Controls.Add(Me.GroupBox1)
        Me.PnlConyugue.Location = New System.Drawing.Point(136, 96)
        Me.PnlConyugue.Name = "PnlConyugue"
        Me.PnlConyugue.Size = New System.Drawing.Size(704, 336)
        Me.PnlConyugue.TabIndex = 155
        '
        'pnlPersonales
        '
        Me.pnlPersonales.BackColor = System.Drawing.SystemColors.Window
        Me.pnlPersonales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPersonales.Controls.Add(Me.UcClientesPersonales1)
        Me.pnlPersonales.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlPersonales.ForeColor = System.Drawing.Color.Black
        Me.pnlPersonales.Location = New System.Drawing.Point(136, 96)
        Me.pnlPersonales.Name = "pnlPersonales"
        Me.pnlPersonales.Size = New System.Drawing.Size(704, 336)
        Me.pnlPersonales.TabIndex = 5
        '
        'UcClientesPersonales1
        '
        Me.UcClientesPersonales1.Calle = ""
        Me.UcClientesPersonales1.Ciudad = CType(-1, Long)
        Me.UcClientesPersonales1.ClienteDepende = CType(-1, Long)
        Me.UcClientesPersonales1.CodigoPostal = CType(0, Long)
        Me.UcClientesPersonales1.Colonia = CType(-1, Long)
        Me.UcClientesPersonales1.Correo = ""
        Me.UcClientesPersonales1.CURPCliente = ""
        Me.UcClientesPersonales1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcClientesPersonales1.Empresa = ""
        Me.UcClientesPersonales1.EntreCalles = ""
        Me.UcClientesPersonales1.Estado = CType(-1, Long)
        Me.UcClientesPersonales1.EstadoCivil = "S"
        Me.UcClientesPersonales1.EtiquetaJuridico = "El Cliente tiene un Proceso Jurídico"
        Me.UcClientesPersonales1.Extension = ""
        Me.UcClientesPersonales1.Exterior = ""
        Me.UcClientesPersonales1.FormaMain = Nothing
        Me.UcClientesPersonales1.Interior = ""
        Me.UcClientesPersonales1.Localizado = True
        Me.UcClientesPersonales1.Location = New System.Drawing.Point(0, 0)
        Me.UcClientesPersonales1.Materno = ""
        Me.UcClientesPersonales1.MostratEtiquetaJuridico = False
        Me.UcClientesPersonales1.MotivoNoLocalizable = ""
        Me.UcClientesPersonales1.Municipio = CType(-1, Long)
        Me.UcClientesPersonales1.Name = "UcClientesPersonales1"
        Me.UcClientesPersonales1.Nombre = ""
        Me.UcClientesPersonales1.NoPagaComision = False
        Me.UcClientesPersonales1.PagoCasa = 0
        Me.UcClientesPersonales1.Paterno = ""
        Me.UcClientesPersonales1.RFC = ""
        Me.UcClientesPersonales1.Saldo = 0
        Me.UcClientesPersonales1.Size = New System.Drawing.Size(702, 334)
        Me.UcClientesPersonales1.Sucursal = CType(-1, Long)
        Me.UcClientesPersonales1.Sueldo = "0"
        Me.UcClientesPersonales1.TabIndex = 0
        Me.UcClientesPersonales1.TelefonoCasa = ""
        Me.UcClientesPersonales1.TelefonoCelular = ""
        Me.UcClientesPersonales1.TelefonoNextel = ""
        Me.UcClientesPersonales1.TelefonoOficina = ""
        Me.UcClientesPersonales1.TipoCasa = "P"
        Me.UcClientesPersonales1.TipoCobro = "O"
        Me.UcClientesPersonales1.UsuarioNoLocalizable = ""
        Me.UcClientesPersonales1.YCalles = ""
        '
        'pnlAval
        '
        Me.pnlAval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlAval.Controls.Add(Me.UcClientesAval1)
        Me.pnlAval.Location = New System.Drawing.Point(136, 96)
        Me.pnlAval.Name = "pnlAval"
        Me.pnlAval.Size = New System.Drawing.Size(704, 336)
        Me.pnlAval.TabIndex = 7
        '
        'UcClientesAval1
        '
        Me.UcClientesAval1.AniosConocerlo = CType(0, Long)
        Me.UcClientesAval1.CiudadAval = ""
        Me.UcClientesAval1.Cliente = CType(0, Long)
        Me.UcClientesAval1.ClienteAval = CType(-1, Long)
        Me.UcClientesAval1.CodigoPostal = CType(0, Long)
        Me.UcClientesAval1.ColoniaAval = ""
        Me.UcClientesAval1.CorreoAval = ""
        Me.UcClientesAval1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcClientesAval1.DomicilioAval = ""
        Me.UcClientesAval1.EstadoAval = ""
        Me.UcClientesAval1.EstadoCivil = "Soltero (a)"
        Me.UcClientesAval1.Extension = CType(0, Long)
        Me.UcClientesAval1.Location = New System.Drawing.Point(0, 0)
        Me.UcClientesAval1.Name = "UcClientesAval1"
        Me.UcClientesAval1.NombreAval = ""
        Me.UcClientesAval1.Observaciones = ""
        Me.UcClientesAval1.ParentescoAval = ""
        Me.UcClientesAval1.Size = New System.Drawing.Size(702, 334)
        Me.UcClientesAval1.TabIndex = 0
        Me.UcClientesAval1.TelefonoCasa = ""
        Me.UcClientesAval1.TelefonoCelular = ""
        Me.UcClientesAval1.TelefonoNextel = ""
        Me.UcClientesAval1.TelefonoOficina = ""
        Me.UcClientesAval1.TiempoConocerlo = "0"
        '
        'pnlReferenciasPersonales
        '
        Me.pnlReferenciasPersonales.BackColor = System.Drawing.SystemColors.Window
        Me.pnlReferenciasPersonales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReferenciasPersonales.Controls.Add(Me.UcClientesReferencias1)
        Me.pnlReferenciasPersonales.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlReferenciasPersonales.ForeColor = System.Drawing.Color.Black
        Me.pnlReferenciasPersonales.Location = New System.Drawing.Point(136, 96)
        Me.pnlReferenciasPersonales.Name = "pnlReferenciasPersonales"
        Me.pnlReferenciasPersonales.Size = New System.Drawing.Size(704, 336)
        Me.pnlReferenciasPersonales.TabIndex = 61
        '
        'UcClientesReferencias1
        '
        Me.UcClientesReferencias1.CelularRef1 = ""
        Me.UcClientesReferencias1.CelularRef2 = ""
        Me.UcClientesReferencias1.CelularRef3 = ""
        Me.UcClientesReferencias1.CiudadReferencia1 = ""
        Me.UcClientesReferencias1.CiudadReferencia2 = ""
        Me.UcClientesReferencias1.CiudadReferencia3 = ""
        Me.UcClientesReferencias1.CodigoPostalRef1 = CType(0, Long)
        Me.UcClientesReferencias1.CodigoPostalRef2 = CType(0, Long)
        Me.UcClientesReferencias1.CodigoPostalRef3 = CType(0, Long)
        Me.UcClientesReferencias1.ColoniaReferencia1 = ""
        Me.UcClientesReferencias1.ColoniaReferencia2 = ""
        Me.UcClientesReferencias1.ColoniaReferencia3 = ""
        Me.UcClientesReferencias1.CorreoReferencia1 = ""
        Me.UcClientesReferencias1.CorreoReferencia2 = ""
        Me.UcClientesReferencias1.CorreoReferencia3 = ""
        Me.UcClientesReferencias1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcClientesReferencias1.DomicilioReferencia1 = ""
        Me.UcClientesReferencias1.DomicilioReferencia2 = ""
        Me.UcClientesReferencias1.DomicilioReferencia3 = ""
        Me.UcClientesReferencias1.EstadoReferencia1 = ""
        Me.UcClientesReferencias1.EstadoReferencia2 = ""
        Me.UcClientesReferencias1.EstadoReferencia3 = ""
        Me.UcClientesReferencias1.ExtensionRef1 = CType(0, Long)
        Me.UcClientesReferencias1.ExtensionRef2 = CType(0, Long)
        Me.UcClientesReferencias1.ExtensionRef3 = CType(0, Long)
        Me.UcClientesReferencias1.Location = New System.Drawing.Point(0, 0)
        Me.UcClientesReferencias1.Name = "UcClientesReferencias1"
        Me.UcClientesReferencias1.NextelReferencia1 = ""
        Me.UcClientesReferencias1.NextelReferencia2 = ""
        Me.UcClientesReferencias1.NextelReferencia3 = ""
        Me.UcClientesReferencias1.NombreReferencia1 = ""
        Me.UcClientesReferencias1.NombreReferencia2 = ""
        Me.UcClientesReferencias1.NombreReferencia3 = ""
        Me.UcClientesReferencias1.OtrosReferencia1 = ""
        Me.UcClientesReferencias1.OtrosReferencia2 = ""
        Me.UcClientesReferencias1.OtrosReferencia3 = ""
        Me.UcClientesReferencias1.ParentescoReferencia1 = ""
        Me.UcClientesReferencias1.ParentescoReferencia2 = ""
        Me.UcClientesReferencias1.ParentescoReferencia3 = ""
        Me.UcClientesReferencias1.Size = New System.Drawing.Size(702, 334)
        Me.UcClientesReferencias1.TabIndex = 0
        Me.UcClientesReferencias1.TelefonoCasaRef1 = ""
        Me.UcClientesReferencias1.TelefonoCasaRef2 = ""
        Me.UcClientesReferencias1.TelefonoCasaRef3 = ""
        Me.UcClientesReferencias1.TelefonoOficinaRef1 = ""
        Me.UcClientesReferencias1.TelefonoOficinaRef2 = ""
        Me.UcClientesReferencias1.TelefonoOficinaRef3 = ""
        Me.UcClientesReferencias1.TrabajaReferencia1 = ""
        Me.UcClientesReferencias1.TrabajaReferencia2 = ""
        Me.UcClientesReferencias1.TrabajaReferencia3 = ""
        '
        'pnlReferencias
        '
        Me.pnlReferencias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReferencias.Controls.Add(Me.UcClientesLaborales1)
        Me.pnlReferencias.Location = New System.Drawing.Point(136, 96)
        Me.pnlReferencias.Name = "pnlReferencias"
        Me.pnlReferencias.Size = New System.Drawing.Size(704, 336)
        Me.pnlReferencias.TabIndex = 6
        '
        'UcClientesLaborales1
        '
        Me.UcClientesLaborales1.Antiguedad = ""
        Me.UcClientesLaborales1.Area = ""
        Me.UcClientesLaborales1.Departamento = ""
        Me.UcClientesLaborales1.Calle = ""
        Me.UcClientesLaborales1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcClientesLaborales1.Ingresos = 0
        Me.UcClientesLaborales1.LimiteCredito = 0
        Me.UcClientesLaborales1.Location = New System.Drawing.Point(0, 0)
        Me.UcClientesLaborales1.Name = "UcClientesLaborales1"
        Me.UcClientesLaborales1.Puesto = ""
        Me.UcClientesLaborales1.Size = New System.Drawing.Size(702, 334)
        Me.UcClientesLaborales1.TabIndex = 0
        Me.UcClientesLaborales1.Telefonos = ""
        Me.UcClientesLaborales1.TrabajaEn = ""
        '
        'lkpFormasPago
        '
        Me.lkpFormasPago.AllowAdd = False
        Me.lkpFormasPago.AutoReaload = True
        Me.lkpFormasPago.DataSource = Nothing
        Me.lkpFormasPago.DefaultSearchField = ""
        Me.lkpFormasPago.DisplayMember = "descripcion"
        Me.lkpFormasPago.EditValue = Nothing
        Me.lkpFormasPago.Enabled = False
        Me.lkpFormasPago.Filtered = False
        Me.lkpFormasPago.InitValue = Nothing
        Me.lkpFormasPago.Location = New System.Drawing.Point(424, 488)
        Me.lkpFormasPago.MultiSelect = False
        Me.lkpFormasPago.Name = "lkpFormasPago"
        Me.lkpFormasPago.NullText = ""
        Me.lkpFormasPago.PopupWidth = CType(400, Long)
        Me.lkpFormasPago.ReadOnlyControl = False
        Me.lkpFormasPago.Required = False
        Me.lkpFormasPago.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFormasPago.SearchMember = ""
        Me.lkpFormasPago.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFormasPago.SelectAll = True
        Me.lkpFormasPago.Size = New System.Drawing.Size(297, 20)
        Me.lkpFormasPago.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFormasPago.TabIndex = 157
        Me.lkpFormasPago.Tag = "forma_pago"
        Me.lkpFormasPago.ToolTip = "Seleccione una Forma de Pago"
        Me.lkpFormasPago.ValueMember = "forma_pago"
        Me.lkpFormasPago.Visible = False
        '
        'clcUltimosDigitosCuenta
        '
        Me.clcUltimosDigitosCuenta.EditValue = "0"
        Me.clcUltimosDigitosCuenta.Location = New System.Drawing.Point(424, 504)
        Me.clcUltimosDigitosCuenta.MaxValue = 0
        Me.clcUltimosDigitosCuenta.MinValue = 0
        Me.clcUltimosDigitosCuenta.Name = "clcUltimosDigitosCuenta"
        '
        'clcUltimosDigitosCuenta.Properties
        '
        Me.clcUltimosDigitosCuenta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimosDigitosCuenta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimosDigitosCuenta.Properties.Enabled = False
        Me.clcUltimosDigitosCuenta.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcUltimosDigitosCuenta.Properties.MaxLength = 4
        Me.clcUltimosDigitosCuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcUltimosDigitosCuenta.Size = New System.Drawing.Size(72, 19)
        Me.clcUltimosDigitosCuenta.TabIndex = 156
        Me.clcUltimosDigitosCuenta.Tag = "ultimos_digitos_cuenta"
        Me.clcUltimosDigitosCuenta.Visible = False
        '
        'pnlDirecciones
        '
        Me.pnlDirecciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlDirecciones.Controls.Add(Me.tmaDirecciones)
        Me.pnlDirecciones.Controls.Add(Me.grDirecciones)
        Me.pnlDirecciones.Location = New System.Drawing.Point(136, 96)
        Me.pnlDirecciones.Name = "pnlDirecciones"
        Me.pnlDirecciones.Size = New System.Drawing.Size(704, 336)
        Me.pnlDirecciones.TabIndex = 158
        '
        'tmaDirecciones
        '
        Me.tmaDirecciones.BackColor = System.Drawing.Color.White
        Me.tmaDirecciones.CanDelete = True
        Me.tmaDirecciones.CanInsert = True
        Me.tmaDirecciones.CanUpdate = True
        Me.tmaDirecciones.Grid = Me.grDirecciones
        Me.tmaDirecciones.Location = New System.Drawing.Point(7, 8)
        Me.tmaDirecciones.Name = "tmaDirecciones"
        Me.tmaDirecciones.Size = New System.Drawing.Size(688, 23)
        Me.tmaDirecciones.TabIndex = 63
        Me.tmaDirecciones.TabStop = False
        Me.tmaDirecciones.Title = "Direcciones"
        Me.tmaDirecciones.UpdateTitle = "un Registro"
        '
        'grDirecciones
        '
        '
        'grDirecciones.EmbeddedNavigator
        '
        Me.grDirecciones.EmbeddedNavigator.Name = ""
        Me.grDirecciones.Location = New System.Drawing.Point(8, 32)
        Me.grDirecciones.MainView = Me.grvDirecciones
        Me.grDirecciones.Name = "grDirecciones"
        Me.grDirecciones.Size = New System.Drawing.Size(688, 216)
        Me.grDirecciones.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grDirecciones.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDirecciones.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDirecciones.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDirecciones.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDirecciones.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDirecciones.TabIndex = 64
        Me.grDirecciones.TabStop = False
        Me.grDirecciones.Text = "Cobradores"
        '
        'grvDirecciones
        '
        Me.grvDirecciones.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.grvDirecciones.GridControl = Me.grDirecciones
        Me.grvDirecciones.Name = "grvDirecciones"
        Me.grvDirecciones.OptionsBehavior.Editable = False
        Me.grvDirecciones.OptionsCustomization.AllowFilter = False
        Me.grvDirecciones.OptionsCustomization.AllowGroup = False
        Me.grvDirecciones.OptionsCustomization.AllowSort = False
        Me.grvDirecciones.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Tipo"
        Me.GridColumn1.FieldName = "tipo_direccion"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 60
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Tipo Direccion"
        Me.GridColumn2.FieldName = "descripcion_direccion"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 144
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Dirección"
        Me.GridColumn3.FieldName = "direccion"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 470
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dteFechaActualizacion)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.dteFecha_Alta)
        Me.GroupBox2.Controls.Add(Me.lblFecha_Alta)
        Me.GroupBox2.Location = New System.Drawing.Point(641, 28)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 60)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Fechas:"
        '
        'dteFechaActualizacion
        '
        Me.dteFechaActualizacion.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFechaActualizacion.Location = New System.Drawing.Point(96, 32)
        Me.dteFechaActualizacion.Name = "dteFechaActualizacion"
        '
        'dteFechaActualizacion.Properties
        '
        Me.dteFechaActualizacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaActualizacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaActualizacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaActualizacion.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaActualizacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaActualizacion.Properties.Enabled = False
        Me.dteFechaActualizacion.Size = New System.Drawing.Size(96, 23)
        Me.dteFechaActualizacion.TabIndex = 5
        Me.dteFechaActualizacion.TabStop = False
        Me.dteFechaActualizacion.Tag = "fecha_actualizacion"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 34)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Tag = ""
        Me.Label6.Text = "Actualización:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPersonaAutoriza
        '
        Me.txtPersonaAutoriza.EditValue = ""
        Me.txtPersonaAutoriza.Location = New System.Drawing.Point(424, 64)
        Me.txtPersonaAutoriza.Name = "txtPersonaAutoriza"
        '
        'txtPersonaAutoriza.Properties
        '
        Me.txtPersonaAutoriza.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPersonaAutoriza.Properties.MaxLength = 25
        Me.txtPersonaAutoriza.Size = New System.Drawing.Size(184, 20)
        Me.txtPersonaAutoriza.TabIndex = 7
        Me.txtPersonaAutoriza.TabStop = False
        Me.txtPersonaAutoriza.Tag = "persona_autoriza"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(424, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(102, 16)
        Me.Label7.TabIndex = 6
        Me.Label7.Tag = ""
        Me.Label7.Text = "P&ersona Autoriza:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmSolicitudesClienteActual
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.CanDelete = False
        Me.ClientSize = New System.Drawing.Size(850, 484)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtPersonaAutoriza)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.pnlPersonales)
        Me.Controls.Add(Me.pnlAval)
        Me.Controls.Add(Me.pnlReferenciasPersonales)
        Me.Controls.Add(Me.pnlReferencias)
        Me.Controls.Add(Me.lkpFormasPago)
        Me.Controls.Add(Me.clcUltimosDigitosCuenta)
        Me.Controls.Add(Me.chkFacturacionEspecial)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.clcCliente)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblNotas)
        Me.Controls.Add(Me.txtNotas)
        Me.Controls.Add(Me.btnLlamadasCliente)
        Me.Controls.Add(Me.lblPersona)
        Me.Controls.Add(Me.cboPersona)
        Me.Controls.Add(Me.pnlConvenios)
        Me.Controls.Add(Me.pnlDirecciones)
        Me.Controls.Add(Me.pnlCobradores)
        Me.Controls.Add(Me.PnlConyugue)
        Me.DefaultDock = True
        Me.Docked = True
        Me.Name = "frmSolicitudesClienteActual"
        Me.Controls.SetChildIndex(Me.PnlConyugue, 0)
        Me.Controls.SetChildIndex(Me.pnlCobradores, 0)
        Me.Controls.SetChildIndex(Me.pnlDirecciones, 0)
        Me.Controls.SetChildIndex(Me.pnlConvenios, 0)
        Me.Controls.SetChildIndex(Me.cboPersona, 0)
        Me.Controls.SetChildIndex(Me.lblPersona, 0)
        Me.Controls.SetChildIndex(Me.btnLlamadasCliente, 0)
        Me.Controls.SetChildIndex(Me.txtNotas, 0)
        Me.Controls.SetChildIndex(Me.lblNotas, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.clcCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        Me.Controls.SetChildIndex(Me.CheckBox1, 0)
        Me.Controls.SetChildIndex(Me.chkFacturacionEspecial, 0)
        Me.Controls.SetChildIndex(Me.clcUltimosDigitosCuenta, 0)
        Me.Controls.SetChildIndex(Me.lkpFormasPago, 0)
        Me.Controls.SetChildIndex(Me.pnlReferencias, 0)
        Me.Controls.SetChildIndex(Me.pnlReferenciasPersonales, 0)
        Me.Controls.SetChildIndex(Me.pnlAval, 0)
        Me.Controls.SetChildIndex(Me.pnlPersonales, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.txtPersonaAutoriza, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Alta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grCobradores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboPersona.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtdireccion_trabajo_conyuge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txttelefono_conyuge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtemail_conyuge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccionConyugue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtnombre_conyuge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txttelefonos_laboral_conyuge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCobradores.ResumeLayout(False)
        Me.pnlConvenios.ResumeLayout(False)
        CType(Me.grConvenios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvConvenios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlConyugue.ResumeLayout(False)
        Me.pnlPersonales.ResumeLayout(False)
        Me.pnlAval.ResumeLayout(False)
        Me.pnlReferenciasPersonales.ResumeLayout(False)
        Me.pnlReferencias.ResumeLayout(False)
        CType(Me.clcUltimosDigitosCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDirecciones.ResumeLayout(False)
        CType(Me.grDirecciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDirecciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dteFechaActualizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPersonaAutoriza.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientes As VillarrealBusiness.clsClientes
    Private oCobradores As VillarrealBusiness.clsCobradores
    Private oClientesCobradores As VillarrealBusiness.clsClientesCobradores
    Private oClientesConvenios As VillarrealBusiness.clsClientesConvenios
    Private oClientesDirecciones As VillarrealBusiness.clsClientesDirecciones
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private ociudades As VillarrealBusiness.clsCiudades
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oVariables As New VillarrealBusiness.clsVariables
    Private oColonias As New VillarrealBusiness.clsColonias
    Private oEstados As VillarrealBusiness.clsEstados
    Private oReportes As New VillarrealBusiness.Reportes
    Private oFormasPago As New VillarrealBusiness.clsFormasPagos


    'Private oLlamadasClientes As VillarrealBusiness.clsLlamadasClientes

    'INICIO Código modificado por Alberto 18/06/07
    Private UsuarioActual As String
    'FIN    Código modificado por Alberto 18/06/07

    Private rfc_completo As Boolean = False
    Private tipo_captura As String

    Private Entro_Despliega As Boolean = False
    'Private Entro_Despliega_localizado As Boolean = False
    Public Entro_motivo As Boolean = False

    Private Grabando As Boolean = False
    Private solita_ultimos_digitos As Boolean = False


    Private SALIENDO As Boolean = False
    Private ACCION As Int16 = 1  ' 1 : INSERT   2: UPDATE


    Private ReadOnly Property CobradorCasa() As Long
        Get
            Return CLng(oVariables.TraeDatos("cobrador_casa", VillarrealBusiness.clsVariables.tipo_dato.Entero))
        End Get
    End Property

    Private ReadOnly Property Cobradores() As Long
        Get
            Dim ODATAVIEW As DataView
            Dim numero As Long
            ODATAVIEW = tmaCobradores.DataSource.Tables(0).DefaultView
            ODATAVIEW.RowFilter = "CONTROL <> 3"
            numero = ODATAVIEW.Count

            Return numero
        End Get
    End Property


    Private ReadOnly Property FormaPago() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFormasPago)
        End Get
    End Property
    Private ReadOnly Property Sucursal_Dependencia() As Boolean
        Get
            Return Comunes.clsUtilerias.uti_SucursalDependencia(Comunes.Common.Sucursal_Actual)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"


    Private Sub frmSolicitudesClienteActual_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
        Grabando = False
    End Sub
    Private Sub frmSolicitudesClienteActual_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
        Grabando = True
    End Sub
    Private Sub frmSolicitudesClienteActual_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
        Grabando = False

        If ACCION = 1 Then
            ShowMessage(MessageType.MsgInformation, "El numero de Cliente asignado es: " + Me.clcCliente.Value.ToString, "Alta de Clientes")
            ImprimiReporte()
        Else
            ShowMessage(MessageType.MsgInformation, "El Cliente: " + Me.clcCliente.Value.ToString + ", Fue Actualizado Correctamente", "Alta de Clientes")
        End If

    End Sub

    'Private Sub frmSolicitudesClienteActual_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
    'Select Case Action
    '    Case Actions.Insert
    '    Case Actions.Update
    '        'valida si cliente existe en plantilla
    '        Dim oEvents As New Events
    '        Dim existe As Long
    '        oEvents = oClientes.ExisteEnPlantilla(Me.clcCliente.Value)
    '        existe = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("existe"), Long)
    '        If existe > 0 Then
    '            Me.lkpCliente.Enabled = False
    '        End If
    '        oEvents = Nothing

    '    Case Actions.Delete
    'End Select
    'End Sub
    Private Sub frmSolicitudesClienteActual_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If cboPersona.Text = "Física" Then

            Me.txtNombre.Text = Me.UcClientesPersonales1.Nombre + " " + Me.UcClientesPersonales1.Paterno + " " + Me.UcClientesPersonales1.Materno
        Else
            Me.txtNombre.Text = Me.UcClientesPersonales1.Empresa
            Me.UcClientesPersonales1.Nombre = ""
            Me.UcClientesPersonales1.Paterno = ""
            Me.UcClientesPersonales1.Materno = ""


        End If


        'Try


        If Me.clcCliente.Value = 0 Then
            ACCION = 1

            Response = oClientes.Insertar(Me.clcCliente.EditValue, txtNombre.Text, Me.UcClientesPersonales1.Nombre, Me.UcClientesPersonales1.Paterno, Me.UcClientesPersonales1.Materno, Me.UcClientesPersonales1.RFC, Me.UcClientesPersonales1.CURPCliente, _
                        Me.UcClientesPersonales1.Calle, Me.UcClientesPersonales1.Exterior, Me.UcClientesPersonales1.Interior, Me.UcClientesPersonales1.EntreCalles, Me.UcClientesPersonales1.YCalles, Me.UcClientesPersonales1.Colonia, Me.UcClientesPersonales1.Estado, _
                        Me.UcClientesPersonales1.Ciudad, Me.UcClientesPersonales1.Municipio, Me.UcClientesPersonales1.CodigoPostal, Me.UcClientesPersonales1.TelefonoCasa, Me.UcClientesPersonales1.TelefonoOficina, Me.UcClientesPersonales1.TelefonoCelular, Me.UcClientesLaborales1.TrabajaEn, _
                         Me.UcClientesLaborales1.Ingresos, Me.UcClientesLaborales1.Puesto, Me.UcClientesLaborales1.Departamento, Me.UcClientesLaborales1.Telefonos, Me.UcClientesLaborales1.Antiguedad, Me.txtNotas.Text, Me.UcClientesAval1.ClienteAval, Me.UcClientesAval1.NombreAval, _
                         Me.UcClientesAval1.DomicilioAval, Me.UcClientesAval1.ColoniaAval, Me.UcClientesAval1.TelefonoCasa, Me.UcClientesAval1.CiudadAval, Me.UcClientesAval1.EstadoAval, Me.UcClientesAval1.AniosConocerlo, Me.UcClientesAval1.ParentescoAval, Me.UcClientesPersonales1.TipoCobro, Me.dteFecha_Alta.DateTime, _
                         Me.cboPersona.EditValue, Me.UcClientesPersonales1.Sucursal, Me.UcClientesPersonales1.NoPagaComision, rfc_completo, Me.UcClientesPersonales1.ClienteDepende, Me.UcClientesLaborales1.LimiteCredito, "C", Me.UcClientesPersonales1.Localizado, Me.txtnombre_conyuge.Text, _
                         Me.TextEdit4.Text, Me.TextEdit3.Text, Me.TextEdit2.Text, Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txttelefonos_laboral_conyuge.Text), Me.UcClientesPersonales1.MotivoNoLocalizable, Me.UcClientesPersonales1.UsuarioNoLocalizable, Me.CheckBox1.Checked, Me.UcClientesPersonales1.EstadoCivil, Me.UcClientesReferencias1.NombreReferencia1, _
                        Me.UcClientesReferencias1.DomicilioReferencia1, Me.UcClientesReferencias1.ColoniaReferencia1, Me.UcClientesReferencias1.TelefonoCasaRef1, Me.UcClientesReferencias1.CiudadReferencia1, Me.UcClientesReferencias1.EstadoReferencia1, Me.UcClientesReferencias1.TrabajaReferencia1, _
                         Me.UcClientesReferencias1.ParentescoReferencia1, Me.FormaPago, Me.clcUltimosDigitosCuenta.EditValue, Me.UcClientesPersonales1.Correo, Me.chkFacturacionEspecial.Checked, Me.UcClientesPersonales1.FechaNacimiento)
        Else
            ACCION = 2

            Response = oClientes.Actualizar(Me.clcCliente.EditValue, txtNombre.Text, Me.UcClientesPersonales1.Nombre, Me.UcClientesPersonales1.Paterno, Me.UcClientesPersonales1.Materno, Me.UcClientesPersonales1.RFC, Me.UcClientesPersonales1.CURPCliente, _
            Me.UcClientesPersonales1.Calle, Me.UcClientesPersonales1.Exterior, Me.UcClientesPersonales1.Interior, Me.UcClientesPersonales1.EntreCalles, Me.UcClientesPersonales1.YCalles, Me.UcClientesPersonales1.Colonia, Me.UcClientesPersonales1.Estado, _
            Me.UcClientesPersonales1.Ciudad, Me.UcClientesPersonales1.Municipio, Me.UcClientesPersonales1.CodigoPostal, Me.UcClientesPersonales1.TelefonoCasa, Me.UcClientesPersonales1.TelefonoOficina, Me.UcClientesPersonales1.TelefonoCelular, Me.UcClientesLaborales1.TrabajaEn, _
             Me.UcClientesLaborales1.Ingresos, Me.UcClientesLaborales1.Puesto, Me.UcClientesLaborales1.Departamento, Me.UcClientesLaborales1.Telefonos, Me.UcClientesLaborales1.Antiguedad, Me.txtNotas.Text, Me.UcClientesAval1.ClienteAval, Me.UcClientesAval1.NombreAval, _
             Me.UcClientesAval1.DomicilioAval, Me.UcClientesAval1.ColoniaAval, Me.UcClientesAval1.TelefonoCasa, Me.UcClientesAval1.CiudadAval, Me.UcClientesAval1.EstadoAval, Me.UcClientesAval1.AniosConocerlo, Me.UcClientesAval1.ParentescoAval, Me.UcClientesPersonales1.TipoCobro, Me.dteFecha_Alta.DateTime, _
             Me.cboPersona.EditValue, Me.UcClientesPersonales1.Sucursal, Me.UcClientesPersonales1.NoPagaComision, rfc_completo, Me.UcClientesPersonales1.ClienteDepende, Me.UcClientesLaborales1.LimiteCredito, Me.UcClientesPersonales1.Localizado, Me.txtnombre_conyuge.Text, _
             Me.TextEdit4.Text, Me.TextEdit3.Text, Me.TextEdit2.Text, Me.txttelefonos_laboral_conyuge.Text, Me.UcClientesPersonales1.MotivoNoLocalizable, Me.UcClientesPersonales1.UsuarioNoLocalizable, Me.CheckBox1.Checked, Me.UcClientesPersonales1.EstadoCivil, Me.UcClientesReferencias1.NombreReferencia1, _
            Me.UcClientesReferencias1.DomicilioReferencia1, Me.UcClientesReferencias1.ColoniaReferencia1, Me.UcClientesReferencias1.TelefonoCasaRef1, Me.UcClientesReferencias1.CiudadReferencia1, Me.UcClientesReferencias1.EstadoReferencia1, Me.UcClientesReferencias1.TrabajaReferencia1, _
             Me.UcClientesReferencias1.ParentescoReferencia1, Me.FormaPago, Me.clcUltimosDigitosCuenta.EditValue, Me.UcClientesPersonales1.Correo, Me.chkFacturacionEspecial.Checked, Me.UcClientesPersonales1.FechaNacimiento)
        End If

        If Not Response.ErrorFound Then

            Response = oClientes.InsertarDatosAdicionales(Me.clcCliente.EditValue, Me.UcClientesPersonales1.TelefonoNextel, Me.UcClientesPersonales1.Extension, Me.UcClientesPersonales1.TipoCasa, _
            Me.UcClientesPersonales1.PagoCasa, Me.UcClientesLaborales1.Area, Me.txtDireccionConyugue.Text, Me.txtemail_conyuge.Text, Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txttelefono_conyuge.Text), _
            Me.txtdireccion_trabajo_conyuge.Text, Me.UcClientesAval1.CodigoPostal, Me.UcClientesAval1.TelefonoCelular, Me.UcClientesAval1.TelefonoNextel, Me.UcClientesAval1.TelefonoOficina, _
            Me.UcClientesAval1.Extension, Me.UcClientesAval1.EstadoCivil, Me.UcClientesAval1.CorreoAval, Me.UcClientesAval1.Observaciones, Me.UcClientesReferencias1.CodigoPostalRef1, _
            Me.UcClientesReferencias1.CelularRef1, Me.UcClientesReferencias1.NextelReferencia1, Me.UcClientesReferencias1.TelefonoOficinaRef1, Me.UcClientesReferencias1.ExtensionRef1, Me.UcClientesReferencias1.OtrosReferencia1, _
               Me.UcClientesReferencias1.NombreReferencia2, Me.UcClientesReferencias1.DomicilioReferencia2, Me.UcClientesReferencias1.ColoniaReferencia2, Me.UcClientesReferencias1.TelefonoCasaRef2, _
             Me.UcClientesReferencias1.CiudadReferencia2, Me.UcClientesReferencias1.EstadoReferencia2, Me.UcClientesReferencias1.CodigoPostalRef2, Me.UcClientesReferencias1.TrabajaReferencia2, _
              Me.UcClientesReferencias1.ParentescoReferencia2, Me.UcClientesReferencias1.CelularRef2, Me.UcClientesReferencias1.NextelReferencia2, Me.UcClientesReferencias1.TelefonoOficinaRef2, _
              Me.UcClientesReferencias1.ExtensionRef2, Me.UcClientesReferencias1.OtrosReferencia2, _
               Me.UcClientesReferencias1.NombreReferencia3, Me.UcClientesReferencias1.DomicilioReferencia3, Me.UcClientesReferencias1.ColoniaReferencia3, Me.UcClientesReferencias1.TelefonoCasaRef3, _
             Me.UcClientesReferencias1.CiudadReferencia3, Me.UcClientesReferencias1.EstadoReferencia3, Me.UcClientesReferencias1.CodigoPostalRef3, Me.UcClientesReferencias1.TrabajaReferencia3, _
              Me.UcClientesReferencias1.ParentescoReferencia3, Me.UcClientesReferencias1.CelularRef3, Me.UcClientesReferencias1.NextelReferencia3, Me.UcClientesReferencias1.TelefonoOficinaRef3, _
              Me.UcClientesReferencias1.ExtensionRef3, Me.UcClientesReferencias1.OtrosReferencia3, Me.txtPersonaAutoriza.Text, Me.dteFechaActualizacion.DateTime, Me.UcClientesReferencias1.CorreoReferencia1, Me.UcClientesReferencias1.CorreoReferencia2, Me.UcClientesReferencias1.CorreoReferencia3, _
              Me.UcClientesLaborales1.Calle, Me.UcClientesLaborales1.Estado, Me.UcClientesLaborales1.Ciudad, Me.UcClientesLaborales1.Municipio, Me.UcClientesLaborales1.Colonia, Me.UcClientesLaborales1.CodigoPostal)
        End If

        'Catch ex As Exception
        '    ShowMessage(MessageType.MsgError, ex.Message, , ex)
        'End Try

    End Sub
    Private Sub frmSolicitudesClienteActual_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail


        With tmaCobradores
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oClientesCobradores.Insertar(.SelectedRow, Me.clcCliente.EditValue)

                    Case Actions.Delete
                        Response = oClientesCobradores.Eliminar(Me.clcCliente.EditValue, .Item("cobrador"))
                End Select
                .MoveNext()
            Loop
        End With

        If Response.ErrorFound Then Exit Sub

        If Me.UcClientesPersonales1.TipoCobro = "O" And Cobradores = 0 Then
            Response = oClientesCobradores.Insertar(Me.clcCliente.EditValue, Me.CobradorCasa, UcClientesPersonales1.Sucursal)
        End If

        If Response.ErrorFound Then Exit Sub

        With tmaConvenios
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oClientesConvenios.Insertar(.SelectedRow, Me.clcCliente.EditValue)

                    Case Actions.Delete
                        Response = oClientesConvenios.Eliminar(Me.clcCliente.EditValue, .Item("convenio"))
                End Select
                .MoveNext()
            Loop
        End With


        If Response.ErrorFound Then Exit Sub

        With tmaDirecciones
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oClientesDirecciones.Insertar(Me.clcCliente.EditValue, .Item("tipo_direccion"), .Item("domicilio"), .Item("numero_exterior"), .Item("numero_interior"), _
                                                            .Item("entrecalle"), .Item("ycalle"), .Item("cp"), .Item("colonia"), .Item("estado"), .Item("municipio"), .Item("ciudad"), _
                                                             Comunes.clsUtilerias.EliminaFormatoTelefonico(.Item("telefono_casa")), Comunes.clsUtilerias.EliminaFormatoTelefonico(.Item("telefono_celular")), _
                                                             .Item("observaciones"), .Item("latitud"), .Item("longitud"))
                    Case Actions.Update
                        Response = oClientesDirecciones.Actualizar(Me.clcCliente.EditValue, .Item("tipo_direccion"), .Item("domicilio"), .Item("numero_exterior"), .Item("numero_interior"), _
                                                                                    .Item("entrecalle"), .Item("ycalle"), .Item("cp"), .Item("colonia"), .Item("estado"), .Item("municipio"), .Item("ciudad"), _
                                                                                     Comunes.clsUtilerias.EliminaFormatoTelefonico(.Item("telefono_casa")), Comunes.clsUtilerias.EliminaFormatoTelefonico(.Item("telefono_celular")), _
                                                                                     .Item("observaciones"), .Item("latitud"), .Item("longitud"))
                    Case Actions.Delete
                        Response = oClientesDirecciones.Eliminar(Me.clcCliente.EditValue, .Item("tipo_direccion"))
                End Select
                .MoveNext()
            Loop
        End With
    End Sub
    'Private Sub frmClientes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
    'Entro_Despliega = True
    'Entro_Despliega_localizado = True
    'Dim oDataSet As DataSet
    'Response = oClientes.DespliegaDatos(Me.clcCliente.Value)
    'If Not Response.ErrorFound Then
    '    oDataSet = Response.Value
    '    Me.DataSource = oDataSet
    '    Me.lkpCiudad.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad")), 0, oDataSet.Tables(0).Rows(0).Item("ciudad"))
    '    Me.lkpColonia.EditValue = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia")), 0, oDataSet.Tables(0).Rows(0).Item("colonia"))

    '    'EL CLIENTE TIENE UN PROCESO JURIDICO
    '    If ((Action.Update Or Action.Delete) And oDataSet.Tables(0).Rows(0).Item("cliente_en_juridico") = True) Then
    '        Me.lblAbogado.Text = oDataSet.Tables(0).Rows(0).Item("frase")
    '        Me.lblAbogado.Visible = True
    '    End If

    'End If

    'If Not Response.ErrorFound Then Response = oClientesCobradores.Listado(Me.clcCliente.EditValue)
    'If Not Response.ErrorFound Then
    '    oDataSet = Response.Value
    '    Me.tmaCobradores.DataSource = oDataSet
    'End If

    'oDataSet = Nothing

    'tipo_captura = CStr(IIf(IsDBNull(OwnerForm.Value("tipo_captura")), "", OwnerForm.Value("tipo_captura"))).ToUpper
    'CalculaLimiteCredito()
    'Entro_Despliega_localizado = False
    ''txtEmpresa.Text = OwnerForm.Value("nombre")
    'End Sub

    Private Sub frmSolicitudesClienteActual_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oCobradores = New VillarrealBusiness.clsCobradores
        oClientesCobradores = New VillarrealBusiness.clsClientesCobradores
        oClientesConvenios = New VillarrealBusiness.clsClientesConvenios
        oClientesDirecciones = New VillarrealBusiness.clsClientesDirecciones
        oSucursales = New VillarrealBusiness.clsSucursales
        ' ociudades = New VillarrealBusiness.clsCiudades
        oMunicipios = New VillarrealBusiness.clsMunicipios
        oEstados = New VillarrealBusiness.clsEstados

        UsuarioActual = VillarrealBusiness.BusinessEnvironment.Connection.User()

        Me.dteFecha_Alta.EditValue = CDate(TinApp.FechaServidor)

        If Not Me.Sucursal_Dependencia Then
            Me.tmaConvenios.CanInsert = False
            Me.tmaConvenios.CanDelete = False
            Me.grConvenios.Enabled = False
            Me.pnlConvenios.Enabled = False
        End If

        With Me.tmaCobradores
            .UpdateTitle = "un Cobrador"

            .UpdateForm = New Comunes.frmClientesCobradores
            .AddColumn("cobrador", "System.Int32")
            .AddColumn("nombre_cobrador")
            .AddColumn("sucursal", "System.Int32")
            .AddColumn("nombre_sucursal")
        End With

        With Me.tmaConvenios
            .UpdateTitle = "un Convenio"
            .UpdateForm = New frmClientesConvenios
            .AddColumn("convenio", "System.Int32")
            .AddColumn("nombre_convenio")
            .AddColumn("no_empleado")
            .AddColumn("clave_pago")
        End With

        With Me.tmaDirecciones
            .UpdateTitle = "una Dirección"
            .UpdateForm = New frmClientesDirecciones
            .AddColumn("tipo_direccion", "System.Int32")
            .AddColumn("descripcion_direccion")
            .AddColumn("direccion")

            .AddColumn("domicilio")
            .AddColumn("numero_exterior")
            .AddColumn("numero_interior")
            .AddColumn("entrecalle")
            .AddColumn("ycalle")
            .AddColumn("cp", "System.Int32")
            .AddColumn("estado", "System.Int32")
            .AddColumn("municipio", "System.Int32")
            .AddColumn("ciudad", "System.Int32")
            .AddColumn("colonia", "System.Int32")
            .AddColumn("telefono_casa")
            .AddColumn("telefono_celular")
            .AddColumn("observaciones")
            .AddColumn("latitud")
            .AddColumn("longitud")

                    
        End With

        'Me.lkpCliente.Enabled = False
        Me.UcClientesPersonales1.HabilitaCliente = False

        Me.tipo_captura = "C"
        Me.btnLlamadasCliente.Enabled = False
        If UsuarioActual.ToUpper.Equals("SUPER") Then

            Me.UcClientesPersonales1.HabilitaSucursal = True
        End If

        Me.UcClientesPersonales1.Sucursal = Comunes.Common.Sucursal_Actual

        Me.UcClientesLaborales1.TipoCaptura = tipo_captura

        Me.UcClientesPersonales1.TeclaEscape = MyBase.KeyEsc
        Me.UcClientesPersonales1.Accion = Me.Action

    End Sub
    Private Sub frmSolicitudesClienteActual_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim oDataSet As DataSet = Me.tmaDirecciones.DataSource

        Try
            Response = oClientes.Validacion(Action, Me.UcClientesPersonales1.Nombre, Me.UcClientesPersonales1.Paterno, Me.UcClientesPersonales1.RFC, Me.cboPersona.EditValue, _
                              rfc_completo, UcClientesPersonales1.Calle, Me.UcClientesPersonales1.CodigoPostal, UcClientesPersonales1.Estado, UcClientesPersonales1.Municipio, _
                              UcClientesPersonales1.Ciudad, UcClientesPersonales1.Colonia, Me.Cobradores, Me.UcClientesLaborales1.Ingresos, Me.UcClientesPersonales1.TipoCobro, _
                              UcClientesPersonales1.Sucursal, UsuarioActual, Me.UcClientesPersonales1.Empresa, Me.UcClientesPersonales1.EstadoCivil, Me.txtnombre_conyuge.Text, Me.txttelefonos_laboral_conyuge.Text, solita_ultimos_digitos, Me.clcUltimosDigitosCuenta.EditValue)

            If Response.ErrorFound Then Exit Sub
            Response = oClientes.ValidaSucursal(Me.UcClientesPersonales1.Sucursal)

            If Response.ErrorFound Then Exit Sub
            Response = oClientes.ValidaAval(Me.UcClientesAval1.ClienteAval, Me.UcClientesAval1.NombreAval)

        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.Message, , ex)
        End Try

    End Sub


#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub btnLlamadasCliente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLlamadasCliente.Click
        Dim oForm As New Comunes.frmLlamadasClientes

        With oForm
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Action = Actions.Update
            '.Refresh()
            .cliente = Me.clcCliente.EditValue
            .entro_catalogo_clientes = True


            .Show()
            .cliente = Me.clcCliente.EditValue

            Me.Enabled = False
        End With



    End Sub

    Private Sub txtNombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oClientes.ValidaNombre(txtNombre.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub




    Private Sub lkpFormasPago_Format() Handles lkpFormasPago.Format
        Comunes.clsFormato.for_formas_pagos_grl(Me.lkpFormasPago)
    End Sub
    Private Sub lkpFormasPago_LoadData(ByVal Initialize As Boolean) Handles lkpFormasPago.LoadData
        Dim Response As New Events
        Response = oFormasPago.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFormasPago.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpFormasPago_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpFormasPago.EditValueChanged
        If Me.FormaPago > 0 Then
            solita_ultimos_digitos = Me.lkpFormasPago.GetValue("solicitar_ulitmos_digitos")

            If solita_ultimos_digitos = True Then
                Me.clcUltimosDigitosCuenta.Enabled = True
            Else
                Me.clcUltimosDigitosCuenta.EditValue = 0
                Me.clcUltimosDigitosCuenta.Enabled = False
            End If
        End If
    End Sub


    'Private Sub clcIngresos_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.clcIngresos.IsLoading Then Exit Sub
    '    If Me.clcIngresos.EditValue Is Nothing Or Not IsNumeric(Me.clcIngresos.EditValue) Then Exit Sub

    '    'If tipo_captura = "C" Then
    '    CalculaLimiteCredito()
    '    'End If
    'End Sub

    Private Sub nvrDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosGenerales.LinkPressed
        Me.pnlPersonales.BringToFront()
    End Sub
    Private Sub nvrDatosReferenciasLaborales_LinkPressed(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosReferenciasLaborales.LinkPressed
        Me.pnlReferencias.BringToFront()
    End Sub
    Private Sub nvrCobradores_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrCobradores.LinkPressed
        Me.pnlCobradores.BringToFront()
    End Sub
    Private Sub nvrDatosAval_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosAval.LinkPressed
        Me.pnlAval.BringToFront()
    End Sub
    Private Sub nvrDatosReferenciasPersonales_LinkPressed(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosReferenciasPersonales.LinkPressed
        Me.pnlReferenciasPersonales.BringToFront()
    End Sub
    Private Sub nvrDatosConvenios_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosConvenios.LinkPressed
        Me.pnlConvenios.BringToFront()
    End Sub
    Private Sub nvrConyugue_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrConyugue.LinkPressed
        PnlConyugue.BringToFront()
    End Sub
    Private Sub nvrDirecciones_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDirecciones.LinkPressed
        pnlDirecciones.BringToFront()
    End Sub

    Private Sub cboPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPersona.SelectedIndexChanged
        If cboPersona.Text = "Física" Then

            Me.UcClientesPersonales1.EtiquetaNombre = "&Nombre (s):"
            Me.UcClientesPersonales1.MostrarCampoNombre = True
            Me.UcClientesPersonales1.MostrarEtiquetaPaterno = True
            Me.UcClientesPersonales1.MostrarCampoPaterno = True
            Me.UcClientesPersonales1.MostrarEtiquetaMaterno = True
            Me.UcClientesPersonales1.MostrarCampoMaterno = True
            Me.UcClientesPersonales1.MostrarEmpresa = False
            Me.UcClientesPersonales1.Empresa = ""

        Else
            Me.UcClientesPersonales1.EtiquetaNombre = "&Nombre :"
            Me.UcClientesPersonales1.MostrarCampoNombre = False
            Me.UcClientesPersonales1.MostrarEtiquetaPaterno = False
            Me.UcClientesPersonales1.MostrarCampoPaterno = False
            Me.UcClientesPersonales1.MostrarEtiquetaMaterno = False
            Me.UcClientesPersonales1.MostrarCampoMaterno = False
            Me.UcClientesPersonales1.MostrarEmpresa = True


        End If

    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.btnReimprimir Then
            Me.ImprimiReporte()
        End If

    End Sub

    Private Sub clcCliente_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcCliente.Validating

        VerificaCliente()
    End Sub



#End Region

#Region "DIPROS Systems, Funcionalidad"

    Public Sub ValidaCheckLocalizado()
        If TinApp.Connection.User.ToUpper <> "SUPER" Then

            Dim response As Events
            Dim aceptado As Boolean = False

            response = oClientesCobradores.Clientes_cobradores_valida_dias_cobrador(Me.clcCliente.Value)
            If Not response.ErrorFound Then
                Dim odataset As DataSet
                odataset = response.Value
                If odataset.Tables(0).Rows.Count > 0 Then
                    aceptado = odataset.Tables(0).Rows(0).Item("aceptado")
                End If
            End If

            If (Cobradores = 0) Or (aceptado = False) Or Me.UcClientesPersonales1.MotivoNoLocalizable.Trim.Length = 0 Then
                UcClientesPersonales1.Localizado = Not UcClientesPersonales1.Localizado
            Else
                'Asigno el Ususario que esta desmarcando el check
                Me.UcClientesPersonales1.UsuarioNoLocalizable = TinApp.Connection.User.ToUpper
            End If
        Else
            If Me.UcClientesPersonales1.MotivoNoLocalizable.Trim.Length > 0 Then
                'Asigno el Ususario que esta desmarcando el check
                Me.UcClientesPersonales1.UsuarioNoLocalizable = TinApp.Connection.User.ToUpper
            Else
                UcClientesPersonales1.Localizado = Not UcClientesPersonales1.Localizado
            End If
        End If
    End Sub

    Private Sub VerificaCliente()

        Dim response As Events

      

        If Me.clcCliente.Value = 0 Then

            If Entro_Despliega = False Then
                DespliegaDatos(False)
            Else

                DespliegaDatos(True)
            End If

            Entro_Despliega = True
        Else
            response = oClientes.Existe(Me.clcCliente.Value)

            If response.ErrorFound Then
                response.ShowMessage()
                Exit Sub
            Else

                If CType(response.Value, Int32) > 0 Then

                    DespliegaDatos(True)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Cliente no Existe")
                    Me.clcCliente.Focus()
                    Me.clcCliente.EditValue = 0

                    Entro_Despliega = False
                    DespliegaDatos(False)
                End If


            End If
        End If





    End Sub

    Private Sub DespliegaDatos(ByVal verifica_cliente As Boolean)
        Dim response As Events

        Entro_Despliega = True
        'Entro_Despliega_localizado = True
        Dim oDataSet As DataSet



        If verifica_cliente = True And (Me.clcCliente.Value = Me.ClienteActual) Then Exit Sub



        response = oClientes.DespliegaDatos(Me.clcCliente.Value)
        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.DataSource = oDataSet

            Dim latitud As String = ""
            Dim longitud As String = ""



            ClienteActual = Me.clcCliente.Value
            '-------------------------------------------------------------------------------------------------------------------------------------------

            Me.UcClientesPersonales1.Nombre = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("nombres")), "", oDataSet.Tables(0).Rows(0).Item("nombres"))
            Me.UcClientesPersonales1.Paterno = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("paterno")), "", oDataSet.Tables(0).Rows(0).Item("paterno"))
            Me.UcClientesPersonales1.Materno = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("materno")), "", oDataSet.Tables(0).Rows(0).Item("materno"))


            Me.UcClientesPersonales1.RFC = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("rfc")), "", oDataSet.Tables(0).Rows(0).Item("rfc"))
            Me.UcClientesPersonales1.CURPCliente = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("curp")), "", oDataSet.Tables(0).Rows(0).Item("curp"))

            Me.UcClientesPersonales1.Estado = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("estado")), 0, oDataSet.Tables(0).Rows(0).Item("estado"))
            Me.UcClientesPersonales1.Municipio = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("municipio")), 0, oDataSet.Tables(0).Rows(0).Item("municipio"))

            Me.UcClientesPersonales1.Ciudad = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad")), 0, oDataSet.Tables(0).Rows(0).Item("ciudad"))
            Me.UcClientesPersonales1.Colonia = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia")), 0, oDataSet.Tables(0).Rows(0).Item("colonia"))
            Me.UcClientesPersonales1.Calle = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("domicilio")), "", oDataSet.Tables(0).Rows(0).Item("domicilio"))

            Me.UcClientesPersonales1.EntreCalles = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("entrecalle")), "", oDataSet.Tables(0).Rows(0).Item("entrecalle"))
            Me.UcClientesPersonales1.YCalles = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ycalle")), "", oDataSet.Tables(0).Rows(0).Item("ycalle"))
            Me.UcClientesPersonales1.Exterior = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("numero_exterior")), "", oDataSet.Tables(0).Rows(0).Item("numero_exterior"))
            Me.UcClientesPersonales1.Interior = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("numero_interior")), "", oDataSet.Tables(0).Rows(0).Item("numero_interior"))

            Me.UcClientesPersonales1.CodigoPostal = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cp")), 0, oDataSet.Tables(0).Rows(0).Item("cp"))
            Me.UcClientesPersonales1.TelefonoCasa = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono1")), "", oDataSet.Tables(0).Rows(0).Item("telefono1"))
            Me.UcClientesPersonales1.TelefonoOficina = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono2")), "", oDataSet.Tables(0).Rows(0).Item("telefono2"))
            Me.UcClientesPersonales1.TelefonoCelular = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("fax")), "", oDataSet.Tables(0).Rows(0).Item("fax"))
            Me.UcClientesPersonales1.TelefonoNextel = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_nextel")), "", oDataSet.Tables(0).Rows(0).Item("telefono_nextel"))
            Me.UcClientesPersonales1.Extension = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("extension")), "0", oDataSet.Tables(0).Rows(0).Item("extension"))

            Me.UcClientesPersonales1.UsuarioNoLocalizable = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("usuario_no_localizable")), "", oDataSet.Tables(0).Rows(0).Item("usuario_no_localizable"))
            Me.UcClientesPersonales1.MotivoNoLocalizable = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("motivos_no_localizable")), "", oDataSet.Tables(0).Rows(0).Item("motivos_no_localizable"))

            Me.UcClientesPersonales1.TipoCobro = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("tipo_cobro")), "", oDataSet.Tables(0).Rows(0).Item("tipo_cobro"))
            Me.UcClientesPersonales1.Saldo = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("saldo")), 0, oDataSet.Tables(0).Rows(0).Item("saldo"))
            Me.UcClientesPersonales1.NoPagaComision = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("no_paga_comision")), False, oDataSet.Tables(0).Rows(0).Item("no_paga_comision"))
            Me.UcClientesPersonales1.Localizado = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("localizado")), False, oDataSet.Tables(0).Rows(0).Item("localizado"))

            Me.UcClientesPersonales1.Sucursal = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("sucursal")), 0, oDataSet.Tables(0).Rows(0).Item("sucursal"))
            Me.UcClientesPersonales1.TipoCasa = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("tipo_casa")), "", oDataSet.Tables(0).Rows(0).Item("tipo_casa"))
            Me.UcClientesPersonales1.PagoCasa = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("pago_casa")), 0, oDataSet.Tables(0).Rows(0).Item("pago_casa"))

            Me.UcClientesPersonales1.Correo = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("email")), "", oDataSet.Tables(0).Rows(0).Item("email"))
            Me.UcClientesPersonales1.SucursalDependencia = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cliente_dependencias")), 0, oDataSet.Tables(0).Rows(0).Item("cliente_dependencias"))

            Me.UcClientesPersonales1.EstadoCivil = oDataSet.Tables(0).Rows(0).Item("estado_civil")
            '-------------------------------------------------------------------------------------------------------------------------------------------                     

            Me.UcClientesAval1.ClienteAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cliente_aval")), 0, oDataSet.Tables(0).Rows(0).Item("cliente_aval"))
            Me.UcClientesAval1.NombreAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("aval")), "", oDataSet.Tables(0).Rows(0).Item("aval"))
            Me.UcClientesAval1.DomicilioAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("domicilio_aval")), "", oDataSet.Tables(0).Rows(0).Item("domicilio_aval"))
            Me.UcClientesAval1.ColoniaAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia_aval")), "", oDataSet.Tables(0).Rows(0).Item("colonia_aval"))
            Me.UcClientesAval1.CodigoPostal = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cp_aval")), 0, oDataSet.Tables(0).Rows(0).Item("cp_aval"))
            Me.UcClientesAval1.ColoniaAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia_aval")), "", oDataSet.Tables(0).Rows(0).Item("colonia_aval"))
            Me.UcClientesAval1.TelefonoCasa = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_aval")), "", oDataSet.Tables(0).Rows(0).Item("telefono_aval"))
            Me.UcClientesAval1.CiudadAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad_aval")), "", oDataSet.Tables(0).Rows(0).Item("ciudad_aval"))
            Me.UcClientesAval1.EstadoAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("estado_aval")), "", oDataSet.Tables(0).Rows(0).Item("estado_aval"))


            Me.UcClientesAval1.TelefonoCelular = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_celular_aval")), "", oDataSet.Tables(0).Rows(0).Item("telefono_celular_aval"))
            Me.UcClientesAval1.TelefonoNextel = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_nextel_aval")), "", oDataSet.Tables(0).Rows(0).Item("telefono_nextel_aval"))
            Me.UcClientesAval1.TelefonoOficina = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_oficina_aval")), "", oDataSet.Tables(0).Rows(0).Item("telefono_oficina_aval"))
            Me.UcClientesAval1.Extension = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("extension_aval")), 0, oDataSet.Tables(0).Rows(0).Item("extension_aval"))
            Me.UcClientesAval1.EstadoCivil = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("estado_civil_aval")), "", oDataSet.Tables(0).Rows(0).Item("estado_civil_aval"))


            Me.UcClientesAval1.AniosConocerlo = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("anios_aval")), 0, oDataSet.Tables(0).Rows(0).Item("anios_aval"))
            Me.UcClientesAval1.ParentescoAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("parentesco_aval")), "", oDataSet.Tables(0).Rows(0).Item("parentesco_aval"))
            Me.UcClientesAval1.CorreoAval = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("email_aval")), "", oDataSet.Tables(0).Rows(0).Item("email_aval"))
            Me.UcClientesAval1.Observaciones = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("observaciones_aval")), "", oDataSet.Tables(0).Rows(0).Item("observaciones_aval"))


            '-------------------------------------------------------------------------------------------------------------------------------------------
            tipo_captura = CStr(IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("tipo_captura")), "", oDataSet.Tables(0).Rows(0).Item("tipo_captura"))).ToUpper
            Me.UcClientesLaborales1.TipoCaptura = tipo_captura
            Me.UcClientesLaborales1.TrabajaEn = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ocupacion")), "", oDataSet.Tables(0).Rows(0).Item("ocupacion"))
            Me.UcClientesLaborales1.Calle = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("direccion_laboral")), "", oDataSet.Tables(0).Rows(0).Item("direccion_laboral"))
            Me.UcClientesLaborales1.Estado = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("estado_laboral")), "", oDataSet.Tables(0).Rows(0).Item("estado_laboral"))
            Me.UcClientesLaborales1.Municipio = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("municipio_laboral")), "", oDataSet.Tables(0).Rows(0).Item("municipio_laboral"))
            Me.UcClientesLaborales1.Ciudad = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad_laboral")), "", oDataSet.Tables(0).Rows(0).Item("ciudad_laboral"))
            Me.UcClientesLaborales1.Colonia = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia_laboral")), "", oDataSet.Tables(0).Rows(0).Item("colonia_laboral"))
            Me.UcClientesLaborales1.CodigoPostal = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cp_laboral")), "", oDataSet.Tables(0).Rows(0).Item("cp_laboral"))
            Me.UcClientesLaborales1.Puesto = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("puesto")), "", oDataSet.Tables(0).Rows(0).Item("puesto"))
            Me.UcClientesLaborales1.Area = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("area_laboral")), "", oDataSet.Tables(0).Rows(0).Item("area_laboral"))
            Me.UcClientesLaborales1.Departamento = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("departamento")), "", oDataSet.Tables(0).Rows(0).Item("departamento"))
            Me.UcClientesLaborales1.Telefonos = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefonos_laboral")), "", oDataSet.Tables(0).Rows(0).Item("telefonos_laboral"))
            Me.UcClientesLaborales1.Ingresos = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ingresos")), 0, oDataSet.Tables(0).Rows(0).Item("ingresos"))
            Me.UcClientesLaborales1.Antiguedad = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("antiguedad_laboral")), "", oDataSet.Tables(0).Rows(0).Item("antiguedad_laboral"))

            '-------------------------------------------------------------------------------------------------------------------------------------------


            Me.UcClientesReferencias1.NombreReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("nombre_referencia")), "", oDataSet.Tables(0).Rows(0).Item("nombre_referencia"))
            Me.UcClientesReferencias1.ParentescoReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("parentesco_referencia")), "", oDataSet.Tables(0).Rows(0).Item("parentesco_referencia"))
            Me.UcClientesReferencias1.DomicilioReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("domicilio_referencia")), "", oDataSet.Tables(0).Rows(0).Item("domicilio_referencia"))
            Me.UcClientesReferencias1.CiudadReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad_referencia")), "", oDataSet.Tables(0).Rows(0).Item("ciudad_referencia"))
            Me.UcClientesReferencias1.EstadoReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("estado_referencia")), "", oDataSet.Tables(0).Rows(0).Item("estado_referencia"))
            Me.UcClientesReferencias1.ColoniaReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia_referencia")), "", oDataSet.Tables(0).Rows(0).Item("colonia_referencia"))
            Me.UcClientesReferencias1.CodigoPostalRef1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cp_referencia")), 0, oDataSet.Tables(0).Rows(0).Item("cp_referencia"))
            Me.UcClientesReferencias1.TelefonoCasaRef1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_referencia")), "", oDataSet.Tables(0).Rows(0).Item("telefono_referencia"))
            Me.UcClientesReferencias1.CelularRef1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_celular_referencia")), "", oDataSet.Tables(0).Rows(0).Item("telefono_celular_referencia"))
            Me.UcClientesReferencias1.NextelReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_nextel_referencia")), "", oDataSet.Tables(0).Rows(0).Item("telefono_nextel_referencia"))
            Me.UcClientesReferencias1.TelefonoOficinaRef1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_oficina_referencia")), "", oDataSet.Tables(0).Rows(0).Item("telefono_oficina_referencia"))
            Me.UcClientesReferencias1.ExtensionRef1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("extension_referencia")), "", oDataSet.Tables(0).Rows(0).Item("extension_referencia"))
            Me.UcClientesReferencias1.OtrosReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("otros_referencia")), "", oDataSet.Tables(0).Rows(0).Item("otros_referencia"))
            Me.UcClientesReferencias1.TrabajaReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("trabaja_en_referencia")), "", oDataSet.Tables(0).Rows(0).Item("trabaja_en_referencia"))
            Me.UcClientesReferencias1.CorreoReferencia1 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("email_referencia")), "", oDataSet.Tables(0).Rows(0).Item("email_referencia"))


            Me.UcClientesReferencias1.NombreReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("nombre_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("nombre_referencia2"))
            Me.UcClientesReferencias1.ParentescoReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("parentesco_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("parentesco_referencia2"))
            Me.UcClientesReferencias1.DomicilioReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("domicilio_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("domicilio_referencia2"))
            Me.UcClientesReferencias1.CiudadReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("ciudad_referencia2"))
            Me.UcClientesReferencias1.EstadoReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("estado_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("estado_referencia2"))
            Me.UcClientesReferencias1.ColoniaReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("colonia_referencia2"))
            Me.UcClientesReferencias1.CodigoPostalRef2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cp_referencia2")), 0, oDataSet.Tables(0).Rows(0).Item("cp_referencia2"))
            Me.UcClientesReferencias1.TelefonoCasaRef2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("telefono_referencia2"))
            Me.UcClientesReferencias1.CelularRef3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_celular_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("telefono_celular_referencia2"))
            Me.UcClientesReferencias1.NextelReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_nextel_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("telefono_nextel_referencia2"))
            Me.UcClientesReferencias1.TelefonoOficinaRef2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_oficina_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("telefono_oficina_referencia2"))
            Me.UcClientesReferencias1.ExtensionRef2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("extension_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("extension_referencia2"))
            Me.UcClientesReferencias1.OtrosReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("otros_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("otros_referencia2"))
            Me.UcClientesReferencias1.TrabajaReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("trabaja_en_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("trabaja_en_referencia2"))
            Me.UcClientesReferencias1.CorreoReferencia2 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("email_referencia2")), "", oDataSet.Tables(0).Rows(0).Item("email_referencia2"))


            Me.UcClientesReferencias1.NombreReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("nombre_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("nombre_referencia3"))
            Me.UcClientesReferencias1.ParentescoReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("parentesco_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("parentesco_referencia3"))
            Me.UcClientesReferencias1.DomicilioReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("domicilio_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("domicilio_referencia3"))
            Me.UcClientesReferencias1.CiudadReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("ciudad_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("ciudad_referencia3"))
            Me.UcClientesReferencias1.EstadoReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("estado_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("estado_referencia3"))
            Me.UcClientesReferencias1.ColoniaReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("colonia_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("colonia_referencia3"))
            Me.UcClientesReferencias1.CodigoPostalRef3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("cp_referencia3")), 0, oDataSet.Tables(0).Rows(0).Item("cp_referencia3"))
            Me.UcClientesReferencias1.TelefonoCasaRef3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("telefono_referencia3"))
            Me.UcClientesReferencias1.CelularRef3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_celular_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("telefono_celular_referencia3"))
            Me.UcClientesReferencias1.NextelReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_nextel_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("telefono_nextel_referencia3"))
            Me.UcClientesReferencias1.TelefonoOficinaRef3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("telefono_oficina_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("telefono_oficina_referencia3"))
            Me.UcClientesReferencias1.ExtensionRef3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("extension_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("extension_referencia3"))
            Me.UcClientesReferencias1.OtrosReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("otros_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("otros_referencia3"))
            Me.UcClientesReferencias1.TrabajaReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("trabaja_en_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("trabaja_en_referencia3"))
            Me.UcClientesReferencias1.CorreoReferencia3 = IIf(IsDBNull(oDataSet.Tables(0).Rows(0).Item("email_referencia3")), "", oDataSet.Tables(0).Rows(0).Item("email_referencia3"))


            '-------------------------------------------------------------------------------------------------------------------------------------------

            'EL CLIENTE TIENE UN PROCESO JURIDICO
            If ((Action.Update Or Action.Delete) And oDataSet.Tables(0).Rows(0).Item("cliente_en_juridico") = True) Then
                UcClientesPersonales1.EtiquetaJuridico = oDataSet.Tables(0).Rows(0).Item("frase")
                UcClientesPersonales1.MostratEtiquetaJuridico = True

            End If

            If Me.cboPersona.Value = "M" Then
                Me.UcClientesPersonales1.Empresa = oDataSet.Tables(0).Rows(0).Item("nombre")
            End If

            If oDataSet.Tables(0).Rows(0).Item("sucursal") = -1 Then
                Me.UcClientesPersonales1.Sucursal = Comunes.Common.Sucursal_Actual
            End If



            Me.UcClientesPersonales1.Accion = Actions.Update


        End If

        If Not response.ErrorFound Then response = oClientesCobradores.Listado(Me.clcCliente.EditValue)
        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.tmaCobradores.DataSource = oDataSet
        End If

        If Not response.ErrorFound Then response = oClientesConvenios.Listado(Me.clcCliente.EditValue)
        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.tmaConvenios.DataSource = oDataSet
        End If


        If Not response.ErrorFound Then response = oClientesDirecciones.Listado(Me.clcCliente.EditValue)
        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.tmaDirecciones.DataSource = oDataSet
        End If

        oDataSet = Nothing




        'Entro_Despliega_localizado = False

    End Sub

    Private Sub ImprimiReporte()
        If Me.clcCliente.Value > 0 Then
            Dim response As Events
            response = oReportes.SolicitudCliente(Me.clcCliente.Value)

            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptSolicitudesClientes

                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)


                    If TinApp.Connection.User.ToUpper = "SUPER" Then
                        TinApp.ShowReport(Me.MdiParent, "Solicitud de Cliente", oReport, , , True, True)
                    Else
                        TinApp.ShowReport(Me.MdiParent, "Solicitud de Cliente", oReport)
                    End If


                    oDataSet = Nothing
                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
                End If
            End If

        End If

    End Sub

#End Region


   
 
End Class