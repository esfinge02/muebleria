Imports System.IO
Imports System.Drawing
Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmArticulosFoto
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents TinImagen1 As Comunes.TINImagen
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmArticulosFoto))
        Me.TinImagen1 = New Comunes.TINImagen
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(409, 28)
        '
        'TinImagen1
        '
        Me.TinImagen1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TinImagen1.Foto = Nothing
        Me.TinImagen1.Location = New System.Drawing.Point(5, 32)
        Me.TinImagen1.Name = "TinImagen1"
        Me.TinImagen1.Size = New System.Drawing.Size(440, 376)
        Me.TinImagen1.TabIndex = 59
        '
        'frmArticulosFoto
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(450, 416)
        Me.Controls.Add(Me.TinImagen1)
        Me.Name = "frmArticulosFoto"
        Me.ShowInTaskbar = False
        Me.Text = "frmArticulosFoto"
        Me.Controls.SetChildIndex(Me.TinImagen1, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _Articulo As Long = 0
    Private _ModificarImagen As Boolean = True
    Private oArticulosFoto As VillarrealBusiness.clsArticulosFoto


    Public WriteOnly Property Articulo() As Long
        Set(ByVal Value As Long)
            _Articulo = Value
        End Set
    End Property
    Public WriteOnly Property ModificarImagen() As Boolean
        Set(ByVal Value As Boolean)
            _ModificarImagen = Value
        End Set
    End Property

    Private Sub frmArticulosFoto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oArticulosFoto = New VillarrealBusiness.clsArticulosFoto

        Me.tbrTools.Buttons(0).Enabled = _ModificarImagen
        Me.tbrTools.Buttons(0).Visible = _ModificarImagen
        Me.TinImagen1.ModificarImagen = _ModificarImagen

        CargarImagen(_Articulo)
    End Sub

    Private Sub CargarImagen(ByVal articulo As Long)
        Dim response As Events
        Dim odataSet As DataSet
        response = oArticulosFoto.DespliegaDatos(articulo)
        If Not response.ErrorFound Then
            odataSet = CType(response.Value, DataSet)
            If odataSet.Tables(0).Rows.Count > 0 Then

                If Not odataSet.Tables(0).Rows(0).Item("foto") Is System.DBNull.Value Then
                    Dim ms As MemoryStream
                    Dim Imagen As Image

                    ms = New MemoryStream(CType(odataSet.Tables(0).Rows(0).Item("foto"), Byte()))
                    Imagen = Image.FromStream(ms)

                    Me.TinImagen1.Foto = Imagen
                End If
            Else
                If _ModificarImagen = False Then
                    ShowMessage(MessageType.MsgInformation, "El Articulo no tiene Fotografia asignada", "Foto del Articulo", , False)
                    Me.Close()
                End If
            End If
        End If
    End Sub

    Private Sub frmArticulosFoto_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim ms As New MemoryStream
        Dim ImageIN As Image
        If Not Me.TinImagen1.Foto Is Nothing Then
            ImageIN = Me.TinImagen1.Foto
            ImageIN.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)

            Dim arrImage() As Byte = ms.GetBuffer
            Response = oArticulosFoto.Insertar(_Articulo, arrImage)
        Else
            Response = oArticulosFoto.Insertar(_Articulo, System.DBNull.Value)
        End If
    End Sub
End Class
