Public Class brwClientes
    Inherits Dipros.Windows.frmTINGridNet

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblPersona As System.Windows.Forms.Label
    Friend WithEvents cboEstado As DevExpress.XtraEditors.ImageComboBoxEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblPersona = New System.Windows.Forms.Label
        Me.cboEstado = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.FilterPanel.SuspendLayout()
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        '
        'FooterPanel
        '
        Me.FooterPanel.Location = New System.Drawing.Point(0, 606)
        Me.FooterPanel.Name = "FooterPanel"
        Me.FooterPanel.Size = New System.Drawing.Size(770, 8)
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.lblPersona)
        Me.FilterPanel.Controls.Add(Me.cboEstado)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(770, 37)
        Me.FilterPanel.Controls.SetChildIndex(Me.cboEstado, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblPersona, 0)
        '
        'lblPersona
        '
        Me.lblPersona.AutoSize = True
        Me.lblPersona.Location = New System.Drawing.Point(16, 8)
        Me.lblPersona.Name = "lblPersona"
        Me.lblPersona.Size = New System.Drawing.Size(43, 16)
        Me.lblPersona.TabIndex = 110
        Me.lblPersona.Tag = ""
        Me.lblPersona.Text = "&Estado:"
        Me.lblPersona.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstado
        '
        Me.cboEstado.EditValue = "-1"
        Me.cboEstado.Location = New System.Drawing.Point(72, 8)
        Me.cboEstado.Name = "cboEstado"
        '
        'cboEstado.Properties
        '
        Me.cboEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstado.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todos", "-1", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Activo", "1", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inactivo", "0", -1)})
        Me.cboEstado.Size = New System.Drawing.Size(96, 20)
        Me.cboEstado.TabIndex = 111
        Me.cboEstado.Tag = ""
        '
        'brwClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(770, 614)
        Me.Name = "brwClientes"
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientes As VillarrealBusiness.clsClientes
    Private bLoad As Boolean
#End Region

#Region "DIPROS SYSTEMS - Eventos de la Forma"

    Private Sub brwClientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oClientes = New VillarrealBusiness.clsClientes
        bLoad = True
    End Sub

    Private Sub brwClientes_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        'oClientes = Business.CreateInstance("clsContratos")
        Response = oClientes.Listado(cboEstado.Value)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub cboEstado_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEstado.EditValueChanged
        If bLoad = True Then
            Reload()
        End If
    End Sub
#End Region

End Class
