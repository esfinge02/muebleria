Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmArticulosExistencias
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents rptCantidades As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcOC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaPromesa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPSSP_OC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPSSP_Fecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPSSP_FechaPromesa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPSSP_Cantidad As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents lblNomBodega As System.Windows.Forms.Label
    Public WithEvents lblNomArticulo As System.Windows.Forms.Label
    Public WithEvents grExistencias As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcTraspaso_Traspaso As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTraspaso_Fecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTraspaso_Cantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTraspaso_BodegaS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio_Reparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie_Factura_Reparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio_Factura_Reparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad_Reparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha_Factura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad_Factura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha_Rreparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPedida As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents grvExistencias As DevExpress.XtraGrid.Views.Grid.GridView
    Public WithEvents grvPorSurtir As DevExpress.XtraGrid.Views.Grid.GridView
    Public WithEvents grvPorSurtirSobrePedido As DevExpress.XtraGrid.Views.Grid.GridView
    Public WithEvents grvTraspasos As DevExpress.XtraGrid.Views.Grid.GridView
    Public WithEvents grvPorEntregar As DevExpress.XtraGrid.Views.Grid.GridView
    Public WithEvents grvRepartiendose As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcCantidadVendida As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents grvGarantias As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents grvVistas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcFolioVista As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodegaSalida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerieVistaSalida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidadVistaSalida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUusuarioAutorizo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPF As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmArticulosExistencias))
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblNomBodega = New System.Windows.Forms.Label
        Me.lblNomArticulo = New System.Windows.Forms.Label
        Me.grExistencias = New DevExpress.XtraGrid.GridControl
        Me.rptCantidades = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grvPorEntregar = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPF = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha_Factura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcVendedor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad_Factura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvRepartiendose = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcFolio_Reparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie_Factura_Reparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio_Factura_Reparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha_Rreparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad_Reparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvGarantias = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvVistas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcFolioVista = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodegaSalida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerieVistaSalida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUusuarioAutorizo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidadVistaSalida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvPorSurtir = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcOC = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaPromesa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPedida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidadVendida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvPorSurtirSobrePedido = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPSSP_OC = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPSSP_Fecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPSSP_FechaPromesa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPSSP_Cantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvTraspasos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcTraspaso_Traspaso = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTraspaso_Fecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTraspaso_BodegaS = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTraspaso_Cantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grvExistencias = New DevExpress.XtraGrid.Views.Grid.GridView
        CType(Me.grExistencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptCantidades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPorEntregar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvRepartiendose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvGarantias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvVistas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPorSurtir, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPorSurtirSobrePedido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvTraspasos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvExistencias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1459, 28)
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 23)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "Bodega:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 23)
        Me.Label3.TabIndex = 60
        Me.Label3.Text = "Art�culo:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNomBodega
        '
        Me.lblNomBodega.Location = New System.Drawing.Point(88, 40)
        Me.lblNomBodega.Name = "lblNomBodega"
        Me.lblNomBodega.Size = New System.Drawing.Size(312, 23)
        Me.lblNomBodega.TabIndex = 61
        '
        'lblNomArticulo
        '
        Me.lblNomArticulo.Location = New System.Drawing.Point(88, 64)
        Me.lblNomArticulo.Name = "lblNomArticulo"
        Me.lblNomArticulo.Size = New System.Drawing.Size(448, 23)
        Me.lblNomArticulo.TabIndex = 62
        '
        'grExistencias
        '
        Me.grExistencias.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grExistencias.EmbeddedNavigator
        '
        Me.grExistencias.EmbeddedNavigator.Name = ""
        Me.grExistencias.LevelDefaults.Add("Level4", Me.grvPorEntregar)
        Me.grExistencias.LevelDefaults.Add("Level5", Me.grvRepartiendose)
        Me.grExistencias.LevelDefaults.Add("Level6", Me.grvGarantias)
        Me.grExistencias.LevelDefaults.Add("Level7", Me.grvVistas)
        Me.grExistencias.LevelDefaults.Add("Level1", Me.grvPorSurtir)
        Me.grExistencias.LevelDefaults.Add("Level2", Me.grvPorSurtirSobrePedido)
        Me.grExistencias.LevelDefaults.Add("Level3", Me.grvTraspasos)
        Me.grExistencias.Location = New System.Drawing.Point(8, 96)
        Me.grExistencias.MainView = Me.grvExistencias
        Me.grExistencias.Name = "grExistencias"
        Me.grExistencias.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptCantidades})
        Me.grExistencias.Size = New System.Drawing.Size(529, 192)
        Me.grExistencias.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Info, System.Drawing.Color.Black, System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grExistencias.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte)), System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grExistencias.TabIndex = 63
        '
        'rptCantidades
        '
        Me.rptCantidades.AutoHeight = False
        Me.rptCantidades.DisplayFormat.FormatString = "n2"
        Me.rptCantidades.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptCantidades.EditFormat.FormatString = "n2"
        Me.rptCantidades.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptCantidades.Name = "rptCantidades"
        '
        'grvPorEntregar
        '
        Me.grvPorEntregar.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPF, Me.grcFactura, Me.grcFecha_Factura, Me.grcCliente, Me.grcVendedor, Me.grcCantidad_Factura})
        Me.grvPorEntregar.GridControl = Me.grExistencias
        Me.grvPorEntregar.Name = "grvPorEntregar"
        Me.grvPorEntregar.OptionsView.ShowFooter = True
        Me.grvPorEntregar.OptionsView.ShowGroupPanel = False
        '
        'grcPF
        '
        Me.grcPF.FieldName = "sobrepedido"
        Me.grcPF.Name = "grcPF"
        Me.grcPF.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcPF.VisibleIndex = 0
        Me.grcPF.Width = 50
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcFactura.VisibleIndex = 1
        Me.grcFactura.Width = 91
        '
        'grcFecha_Factura
        '
        Me.grcFecha_Factura.Caption = "Fecha"
        Me.grcFecha_Factura.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha_Factura.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha_Factura.FieldName = "fecha"
        Me.grcFecha_Factura.Name = "grcFecha_Factura"
        Me.grcFecha_Factura.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcFecha_Factura.VisibleIndex = 2
        Me.grcFecha_Factura.Width = 91
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcCliente.VisibleIndex = 3
        Me.grcCliente.Width = 91
        '
        'grcVendedor
        '
        Me.grcVendedor.Caption = "Vendedor"
        Me.grcVendedor.FieldName = "vendedor"
        Me.grcVendedor.Name = "grcVendedor"
        Me.grcVendedor.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcVendedor.VisibleIndex = 4
        Me.grcVendedor.Width = 91
        '
        'grcCantidad_Factura
        '
        Me.grcCantidad_Factura.Caption = "Cantidad"
        Me.grcCantidad_Factura.DisplayFormat.FormatString = "n0"
        Me.grcCantidad_Factura.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCantidad_Factura.FieldName = "cantidad"
        Me.grcCantidad_Factura.Name = "grcCantidad_Factura"
        Me.grcCantidad_Factura.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcCantidad_Factura.VisibleIndex = 5
        Me.grcCantidad_Factura.Width = 101
        '
        'grvRepartiendose
        '
        Me.grvRepartiendose.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcFolio_Reparto, Me.grcSerie_Factura_Reparto, Me.grcFolio_Factura_Reparto, Me.grcFecha_Rreparto, Me.grcCantidad_Reparto})
        Me.grvRepartiendose.GridControl = Me.grExistencias
        Me.grvRepartiendose.Name = "grvRepartiendose"
        Me.grvRepartiendose.OptionsView.ShowFooter = True
        Me.grvRepartiendose.OptionsView.ShowGroupPanel = False
        '
        'grcFolio_Reparto
        '
        Me.grcFolio_Reparto.Caption = "Reparto"
        Me.grcFolio_Reparto.FieldName = "folio_reparto"
        Me.grcFolio_Reparto.Name = "grcFolio_Reparto"
        Me.grcFolio_Reparto.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcFolio_Reparto.VisibleIndex = 0
        '
        'grcSerie_Factura_Reparto
        '
        Me.grcSerie_Factura_Reparto.Caption = "Serie"
        Me.grcSerie_Factura_Reparto.FieldName = "serie"
        Me.grcSerie_Factura_Reparto.Name = "grcSerie_Factura_Reparto"
        Me.grcSerie_Factura_Reparto.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcSerie_Factura_Reparto.VisibleIndex = 1
        '
        'grcFolio_Factura_Reparto
        '
        Me.grcFolio_Factura_Reparto.Caption = "Folio"
        Me.grcFolio_Factura_Reparto.FieldName = "folio"
        Me.grcFolio_Factura_Reparto.Name = "grcFolio_Factura_Reparto"
        Me.grcFolio_Factura_Reparto.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcFolio_Factura_Reparto.VisibleIndex = 2
        '
        'grcFecha_Rreparto
        '
        Me.grcFecha_Rreparto.Caption = "Fecha"
        Me.grcFecha_Rreparto.FieldName = "fecha"
        Me.grcFecha_Rreparto.Name = "grcFecha_Rreparto"
        Me.grcFecha_Rreparto.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcFecha_Rreparto.VisibleIndex = 4
        '
        'grcCantidad_Reparto
        '
        Me.grcCantidad_Reparto.Caption = "Cantidad"
        Me.grcCantidad_Reparto.FieldName = "cantidad"
        Me.grcCantidad_Reparto.Name = "grcCantidad_Reparto"
        Me.grcCantidad_Reparto.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcCantidad_Reparto.VisibleIndex = 3
        '
        'grvGarantias
        '
        Me.grvGarantias.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7})
        Me.grvGarantias.GridControl = Me.grExistencias
        Me.grvGarantias.Name = "grvGarantias"
        Me.grvGarantias.OptionsView.ShowFooter = True
        Me.grvGarantias.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Tipo Servicio"
        Me.GridColumn1.FieldName = "tipo_servicio"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Folio"
        Me.GridColumn2.FieldName = "folio"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.VisibleIndex = 0
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "fecha"
        Me.GridColumn3.FieldName = "fecha"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn3.VisibleIndex = 1
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Serie"
        Me.GridColumn4.FieldName = "numero_serie"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.VisibleIndex = 2
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Factura"
        Me.GridColumn5.FieldName = "factura"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.VisibleIndex = 3
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Estatus"
        Me.GridColumn6.FieldName = "estatus"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.VisibleIndex = 4
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Devoluci�n"
        Me.GridColumn7.FieldName = "devolucion"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn7.VisibleIndex = 5
        '
        'grvVistas
        '
        Me.grvVistas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcFolioVista, Me.grcBodegaSalida, Me.grcSerieVistaSalida, Me.grcUusuarioAutorizo, Me.grcCantidadVistaSalida})
        Me.grvVistas.GridControl = Me.grExistencias
        Me.grvVistas.Name = "grvVistas"
        Me.grvVistas.OptionsView.ShowFooter = True
        Me.grvVistas.OptionsView.ShowGroupPanel = False
        '
        'grcFolioVista
        '
        Me.grcFolioVista.Caption = "Folio"
        Me.grcFolioVista.FieldName = "folio"
        Me.grcFolioVista.Name = "grcFolioVista"
        Me.grcFolioVista.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolioVista.VisibleIndex = 0
        '
        'grcBodegaSalida
        '
        Me.grcBodegaSalida.Caption = "Bodega"
        Me.grcBodegaSalida.FieldName = "bodega"
        Me.grcBodegaSalida.Name = "grcBodegaSalida"
        Me.grcBodegaSalida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodegaSalida.VisibleIndex = 1
        '
        'grcSerieVistaSalida
        '
        Me.grcSerieVistaSalida.Caption = "Numero Serie"
        Me.grcSerieVistaSalida.FieldName = "numero_serie"
        Me.grcSerieVistaSalida.Name = "grcSerieVistaSalida"
        Me.grcSerieVistaSalida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerieVistaSalida.VisibleIndex = 2
        '
        'grcUusuarioAutorizo
        '
        Me.grcUusuarioAutorizo.Caption = "Autoriz�"
        Me.grcUusuarioAutorizo.FieldName = "usuario_autorizo"
        Me.grcUusuarioAutorizo.Name = "grcUusuarioAutorizo"
        Me.grcUusuarioAutorizo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcUusuarioAutorizo.VisibleIndex = 3
        '
        'grcCantidadVistaSalida
        '
        Me.grcCantidadVistaSalida.Caption = "Cantidad"
        Me.grcCantidadVistaSalida.FieldName = "cantidad"
        Me.grcCantidadVistaSalida.Name = "grcCantidadVistaSalida"
        Me.grcCantidadVistaSalida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidadVistaSalida.VisibleIndex = 4
        '
        'grvPorSurtir
        '
        Me.grvPorSurtir.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcOC, Me.grcFecha, Me.grcFechaPromesa, Me.grcPedida, Me.grcCantidad, Me.grcCantidadVendida})
        Me.grvPorSurtir.GridControl = Me.grExistencias
        Me.grvPorSurtir.Name = "grvPorSurtir"
        Me.grvPorSurtir.OptionsView.ShowFooter = True
        Me.grvPorSurtir.OptionsView.ShowGroupPanel = False
        '
        'grcOC
        '
        Me.grcOC.Caption = "Orden"
        Me.grcOC.FieldName = "oc"
        Me.grcOC.Name = "grcOC"
        Me.grcOC.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcOC.VisibleIndex = 0
        Me.grcOC.Width = 60
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcFecha.VisibleIndex = 1
        Me.grcFecha.Width = 71
        '
        'grcFechaPromesa
        '
        Me.grcFechaPromesa.Caption = "Fecha Promesa"
        Me.grcFechaPromesa.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaPromesa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaPromesa.FieldName = "fecha_promesa"
        Me.grcFechaPromesa.Name = "grcFechaPromesa"
        Me.grcFechaPromesa.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcFechaPromesa.VisibleIndex = 2
        Me.grcFechaPromesa.Width = 95
        '
        'grcPedida
        '
        Me.grcPedida.Caption = "Pedida"
        Me.grcPedida.FieldName = "pedida"
        Me.grcPedida.Name = "grcPedida"
        Me.grcPedida.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcPedida.VisibleIndex = 3
        Me.grcPedida.Width = 60
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Por Surtir"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcCantidad.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcCantidad.VisibleIndex = 4
        Me.grcCantidad.Width = 65
        '
        'grcCantidadVendida
        '
        Me.grcCantidadVendida.Caption = "Vendida"
        Me.grcCantidadVendida.FieldName = "cantidad_vendida"
        Me.grcCantidadVendida.Name = "grcCantidadVendida"
        Me.grcCantidadVendida.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcCantidadVendida.VisibleIndex = 5
        Me.grcCantidadVendida.Width = 67
        '
        'grvPorSurtirSobrePedido
        '
        Me.grvPorSurtirSobrePedido.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPSSP_OC, Me.grcPSSP_Fecha, Me.grcPSSP_FechaPromesa, Me.grcPSSP_Cantidad})
        Me.grvPorSurtirSobrePedido.GridControl = Me.grExistencias
        Me.grvPorSurtirSobrePedido.Name = "grvPorSurtirSobrePedido"
        Me.grvPorSurtirSobrePedido.OptionsView.ShowFooter = True
        Me.grvPorSurtirSobrePedido.OptionsView.ShowGroupPanel = False
        '
        'grcPSSP_OC
        '
        Me.grcPSSP_OC.Caption = "Orden de Compra"
        Me.grcPSSP_OC.FieldName = "oc"
        Me.grcPSSP_OC.Name = "grcPSSP_OC"
        Me.grcPSSP_OC.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcPSSP_OC.VisibleIndex = 0
        '
        'grcPSSP_Fecha
        '
        Me.grcPSSP_Fecha.Caption = "Fecha"
        Me.grcPSSP_Fecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcPSSP_Fecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcPSSP_Fecha.FieldName = "fecha"
        Me.grcPSSP_Fecha.Name = "grcPSSP_Fecha"
        Me.grcPSSP_Fecha.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcPSSP_Fecha.VisibleIndex = 1
        '
        'grcPSSP_FechaPromesa
        '
        Me.grcPSSP_FechaPromesa.Caption = "Fecha Promesa"
        Me.grcPSSP_FechaPromesa.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcPSSP_FechaPromesa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcPSSP_FechaPromesa.FieldName = "fecha_promesa"
        Me.grcPSSP_FechaPromesa.Name = "grcPSSP_FechaPromesa"
        Me.grcPSSP_FechaPromesa.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcPSSP_FechaPromesa.VisibleIndex = 2
        '
        'grcPSSP_Cantidad
        '
        Me.grcPSSP_Cantidad.Caption = "Cantidad"
        Me.grcPSSP_Cantidad.FieldName = "cantidad"
        Me.grcPSSP_Cantidad.Name = "grcPSSP_Cantidad"
        Me.grcPSSP_Cantidad.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcPSSP_Cantidad.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcPSSP_Cantidad.VisibleIndex = 3
        '
        'grvTraspasos
        '
        Me.grvTraspasos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcTraspaso_Traspaso, Me.grcTraspaso_Fecha, Me.grcTraspaso_BodegaS, Me.grcTraspaso_Cantidad})
        Me.grvTraspasos.GridControl = Me.grExistencias
        Me.grvTraspasos.Name = "grvTraspasos"
        Me.grvTraspasos.OptionsView.ShowFooter = True
        Me.grvTraspasos.OptionsView.ShowGroupPanel = False
        '
        'grcTraspaso_Traspaso
        '
        Me.grcTraspaso_Traspaso.Caption = "Traspaso"
        Me.grcTraspaso_Traspaso.FieldName = "traspaso"
        Me.grcTraspaso_Traspaso.Name = "grcTraspaso_Traspaso"
        Me.grcTraspaso_Traspaso.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcTraspaso_Traspaso.VisibleIndex = 0
        Me.grcTraspaso_Traspaso.Width = 50
        '
        'grcTraspaso_Fecha
        '
        Me.grcTraspaso_Fecha.Caption = "Fecha"
        Me.grcTraspaso_Fecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcTraspaso_Fecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcTraspaso_Fecha.FieldName = "fecha"
        Me.grcTraspaso_Fecha.Name = "grcTraspaso_Fecha"
        Me.grcTraspaso_Fecha.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcTraspaso_Fecha.VisibleIndex = 1
        Me.grcTraspaso_Fecha.Width = 65
        '
        'grcTraspaso_BodegaS
        '
        Me.grcTraspaso_BodegaS.Caption = "Bodega Origen"
        Me.grcTraspaso_BodegaS.FieldName = "n_bodega"
        Me.grcTraspaso_BodegaS.Name = "grcTraspaso_BodegaS"
        Me.grcTraspaso_BodegaS.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcTraspaso_BodegaS.VisibleIndex = 2
        Me.grcTraspaso_BodegaS.Width = 135
        '
        'grcTraspaso_Cantidad
        '
        Me.grcTraspaso_Cantidad.Caption = "Cantidad"
        Me.grcTraspaso_Cantidad.FieldName = "cantidad"
        Me.grcTraspaso_Cantidad.Name = "grcTraspaso_Cantidad"
        Me.grcTraspaso_Cantidad.Options = DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Me.grcTraspaso_Cantidad.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcTraspaso_Cantidad.VisibleIndex = 3
        Me.grcTraspaso_Cantidad.Width = 50
        '
        'grvExistencias
        '
        Me.grvExistencias.GridControl = Me.grExistencias
        Me.grvExistencias.Name = "grvExistencias"
        Me.grvExistencias.OptionsCustomization.AllowFilter = False
        Me.grvExistencias.OptionsCustomization.AllowGroup = False
        Me.grvExistencias.OptionsView.ShowFooter = True
        Me.grvExistencias.OptionsView.ShowGroupPanel = False
        '
        'frmArticulosExistencias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(546, 292)
        Me.Controls.Add(Me.grExistencias)
        Me.Controls.Add(Me.lblNomArticulo)
        Me.Controls.Add(Me.lblNomBodega)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmArticulosExistencias"
        Me.Text = "frmArticulosExistencias"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lblNomBodega, 0)
        Me.Controls.SetChildIndex(Me.lblNomArticulo, 0)
        Me.Controls.SetChildIndex(Me.grExistencias, 0)
        CType(Me.grExistencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptCantidades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPorEntregar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvRepartiendose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvGarantias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvVistas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPorSurtir, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPorSurtirSobrePedido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvTraspasos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvExistencias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmArticulosExistencias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Left = 0
        Me.Top = 0

        Me.tbrTools.Buttons.Item(0).Visible = False
    End Sub

    Private Sub frmArticulosExistencias_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        Me.OwnerForm.Enabled = True
    End Sub
End Class
