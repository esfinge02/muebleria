Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmEtiquetas
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grEtiquetas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvEtiquetas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcDesde As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcHasta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPlan1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPlan2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPlan3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpPlan1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents clcDesdeHasta As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEtiquetas))
        Me.grEtiquetas = New DevExpress.XtraGrid.GridControl
        Me.grvEtiquetas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcDesde = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcHasta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPlan1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lkpPlan1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.grcPlan2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPlan3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.clcDesdeHasta = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        CType(Me.grEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkpPlan1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDesdeHasta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1733, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'grEtiquetas
        '
        '
        'grEtiquetas.EmbeddedNavigator
        '
        Me.grEtiquetas.EmbeddedNavigator.Name = ""
        Me.grEtiquetas.Location = New System.Drawing.Point(8, 40)
        Me.grEtiquetas.MainView = Me.grvEtiquetas
        Me.grEtiquetas.Name = "grEtiquetas"
        Me.grEtiquetas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.lkpPlan1, Me.clcDesdeHasta})
        Me.grEtiquetas.Size = New System.Drawing.Size(792, 144)
        Me.grEtiquetas.TabIndex = 59
        Me.grEtiquetas.Text = "Etiquetas"
        '
        'grvEtiquetas
        '
        Me.grvEtiquetas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcDesde, Me.grcHasta, Me.grcPlan1, Me.grcPlan2, Me.grcPlan3})
        Me.grvEtiquetas.GridControl = Me.grEtiquetas
        Me.grvEtiquetas.Name = "grvEtiquetas"
        Me.grvEtiquetas.OptionsCustomization.AllowFilter = False
        Me.grvEtiquetas.OptionsCustomization.AllowGroup = False
        Me.grvEtiquetas.OptionsCustomization.AllowSort = False
        Me.grvEtiquetas.OptionsView.ShowGroupPanel = False
        Me.grvEtiquetas.OptionsView.ShowIndicator = False
        '
        'grcDesde
        '
        Me.grcDesde.Caption = "Desde"
        Me.grcDesde.ColumnEdit = Me.clcDesdeHasta
        Me.grcDesde.DisplayFormat.FormatString = "$#,##0.00"
        Me.grcDesde.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDesde.FieldName = "desde"
        Me.grcDesde.Name = "grcDesde"
        Me.grcDesde.VisibleIndex = 0
        Me.grcDesde.Width = 76
        '
        'grcHasta
        '
        Me.grcHasta.Caption = "Hasta"
        Me.grcHasta.ColumnEdit = Me.clcDesdeHasta
        Me.grcHasta.DisplayFormat.FormatString = "$#,##0.00"
        Me.grcHasta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcHasta.FieldName = "hasta"
        Me.grcHasta.Name = "grcHasta"
        Me.grcHasta.VisibleIndex = 1
        Me.grcHasta.Width = 79
        '
        'grcPlan1
        '
        Me.grcPlan1.Caption = "Plan 1"
        Me.grcPlan1.ColumnEdit = Me.lkpPlan1
        Me.grcPlan1.FieldName = "plan1"
        Me.grcPlan1.Name = "grcPlan1"
        Me.grcPlan1.VisibleIndex = 2
        Me.grcPlan1.Width = 210
        '
        'lkpPlan1
        '
        Me.lkpPlan1.AutoHeight = False
        Me.lkpPlan1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.lkpPlan1.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("plan_credito", "", 5, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("plazo", "", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("interes", "", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("tipo_venta", "", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("tipo_plazo", "", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("precio_venta", "", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("plan_dependencia", "", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Default), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("enganche", "", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Default)})
        Me.lkpPlan1.DisplayMember = "descripcion"
        Me.lkpPlan1.Name = "lkpPlan1"
        Me.lkpPlan1.NullText = "(Ninguno)"
        Me.lkpPlan1.ValueMember = "plan_credito"
        '
        'grcPlan2
        '
        Me.grcPlan2.Caption = "Plan 2"
        Me.grcPlan2.ColumnEdit = Me.lkpPlan1
        Me.grcPlan2.FieldName = "plan2"
        Me.grcPlan2.Name = "grcPlan2"
        Me.grcPlan2.VisibleIndex = 3
        Me.grcPlan2.Width = 207
        '
        'grcPlan3
        '
        Me.grcPlan3.Caption = "Plan 3"
        Me.grcPlan3.ColumnEdit = Me.lkpPlan1
        Me.grcPlan3.FieldName = "plan3"
        Me.grcPlan3.Name = "grcPlan3"
        Me.grcPlan3.VisibleIndex = 4
        Me.grcPlan3.Width = 218
        '
        'clcDesdeHasta
        '
        Me.clcDesdeHasta.AutoHeight = False
        Me.clcDesdeHasta.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, False, False, DevExpress.Utils.HorzAlignment.Center, Nothing)})
        Me.clcDesdeHasta.Name = "clcDesdeHasta"
        '
        'frmEtiquetas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(810, 192)
        Me.Controls.Add(Me.grEtiquetas)
        Me.Name = "frmEtiquetas"
        Me.Text = "frmEtiquetas"
        Me.Controls.SetChildIndex(Me.grEtiquetas, 0)
        CType(Me.grEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkpPlan1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDesdeHasta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oEtiquetas As New VillarrealBusiness.clsEtiquetas
    Private oPlanesCredito As New VillarrealBusiness.clsPlanesCredito

    Private Const NumeroFilas As Long = 4

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmEtiquetas_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmEtiquetas_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmEtiquetas_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub

    Private Sub frmEtiquetas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oEtiquetas = New VillarrealBusiness.clsEtiquetas
        oPlanesCredito = New VillarrealBusiness.clsPlanesCredito

        Me.Location = New System.Drawing.Point(0, 0)
    End Sub
    Private Sub frmEtiquetas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim i As Integer = 0

        If Not Response.ErrorFound Then oEtiquetas.Eliminar()

        Dim orow As DataRow
        For Each orow In CType(Me.grEtiquetas.DataSource, DataTable).Rows
            If i < NumeroFilas Then
                If Not Response.ErrorFound Then Response = oEtiquetas.Insertar(orow("desde"), orow("hasta"), orow("plan1"), orow("plan2"), orow("plan3"))
                i = i + 1
            End If
        Next
    End Sub
    Private Sub frmEtiquetas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.grvEtiquetas.CloseEditor()
        Me.grvEtiquetas.UpdateCurrentRow()

        Response = oEtiquetas.Validacion(NumeroFilas, CType(Me.grEtiquetas.DataSource, DataTable))

    End Sub
    Private Sub frmEtiquetas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.CargarLookup(lkpPlan1)
        Me.CargarConfiguracionEtiquetas()

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarConfiguracionEtiquetas()
        Dim Response As Dipros.Utils.Events

        Response = oEtiquetas.Listado(NumeroFilas)

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(Response.Value, DataSet)

            Me.grEtiquetas.DataSource = oDataSet.Tables(0)

            oDataSet = Nothing
        End If

    End Sub

    Private Sub CargarLookup(ByRef Lookup As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit)
        Dim Response As Dipros.Utils.Events

        Response = oPlanesCredito.Lookup(, , 1)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(Response.Value, DataSet)

            Lookup.DataSource = oDataSet.Tables(0)

            oDataSet = Nothing
        End If


    End Sub
#End Region

End Class
