Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmDepartamentos
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " C�digo generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents clcDepartamento As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcporcentaje_incremento_convenios As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblporcentaje_incremento_convenios As System.Windows.Forms.Label
    Friend WithEvents chkIdentificado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDepartamentos))
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.clcDepartamento = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.clcporcentaje_incremento_convenios = New Dipros.Editors.TINCalcEdit
        Me.lblporcentaje_incremento_convenios = New System.Windows.Forms.Label
        Me.chkIdentificado = New DevExpress.XtraEditors.CheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.clcDepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcporcentaje_incremento_convenios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIdentificado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(403, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(64, 40)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 0
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDepartamento
        '
        Me.clcDepartamento.EditValue = "0"
        Me.clcDepartamento.Location = New System.Drawing.Point(160, 39)
        Me.clcDepartamento.MaxValue = 0
        Me.clcDepartamento.MinValue = 0
        Me.clcDepartamento.Name = "clcDepartamento"
        '
        'clcDepartamento.Properties
        '
        Me.clcDepartamento.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcDepartamento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDepartamento.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcDepartamento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDepartamento.Properties.Enabled = False
        Me.clcDepartamento.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcDepartamento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDepartamento.Size = New System.Drawing.Size(64, 19)
        Me.clcDepartamento.TabIndex = 1
        Me.clcDepartamento.Tag = "departamento"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(104, 64)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(160, 61)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 50
        Me.txtNombre.Size = New System.Drawing.Size(300, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'clcporcentaje_incremento_convenios
        '
        Me.clcporcentaje_incremento_convenios.EditValue = "0"
        Me.clcporcentaje_incremento_convenios.Location = New System.Drawing.Point(160, 88)
        Me.clcporcentaje_incremento_convenios.MaxValue = 0
        Me.clcporcentaje_incremento_convenios.MinValue = 0
        Me.clcporcentaje_incremento_convenios.Name = "clcporcentaje_incremento_convenios"
        '
        'clcporcentaje_incremento_convenios.Properties
        '
        Me.clcporcentaje_incremento_convenios.Properties.DisplayFormat.FormatString = "##0.##"
        Me.clcporcentaje_incremento_convenios.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcporcentaje_incremento_convenios.Properties.EditFormat.FormatString = "##0.##"
        Me.clcporcentaje_incremento_convenios.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcporcentaje_incremento_convenios.Properties.MaskData.EditMask = "##0.##"
        Me.clcporcentaje_incremento_convenios.Properties.MaxLength = 6
        Me.clcporcentaje_incremento_convenios.Properties.Precision = 2
        Me.clcporcentaje_incremento_convenios.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcporcentaje_incremento_convenios.Size = New System.Drawing.Size(64, 19)
        Me.clcporcentaje_incremento_convenios.TabIndex = 5
        Me.clcporcentaje_incremento_convenios.Tag = "porcentaje_incremento_convenios"
        '
        'lblporcentaje_incremento_convenios
        '
        Me.lblporcentaje_incremento_convenios.Location = New System.Drawing.Point(16, 88)
        Me.lblporcentaje_incremento_convenios.Name = "lblporcentaje_incremento_convenios"
        Me.lblporcentaje_incremento_convenios.Size = New System.Drawing.Size(136, 24)
        Me.lblporcentaje_incremento_convenios.TabIndex = 4
        Me.lblporcentaje_incremento_convenios.Tag = ""
        Me.lblporcentaje_incremento_convenios.Text = "Incremento Convenios:"
        Me.lblporcentaje_incremento_convenios.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkIdentificado
        '
        Me.chkIdentificado.Location = New System.Drawing.Point(160, 120)
        Me.chkIdentificado.Name = "chkIdentificado"
        '
        'chkIdentificado.Properties
        '
        Me.chkIdentificado.Properties.Caption = "No Identificable"
        Me.chkIdentificado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkIdentificado.Size = New System.Drawing.Size(128, 19)
        Me.chkIdentificado.TabIndex = 59
        Me.chkIdentificado.Tag = "no_identificable"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(232, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(16, 16)
        Me.Label2.TabIndex = 60
        Me.Label2.Text = "%"
        '
        'frmDepartamentos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(474, 152)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.chkIdentificado)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.clcDepartamento)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.clcporcentaje_incremento_convenios)
        Me.Controls.Add(Me.lblporcentaje_incremento_convenios)
        Me.Name = "frmDepartamentos"
        Me.Controls.SetChildIndex(Me.lblporcentaje_incremento_convenios, 0)
        Me.Controls.SetChildIndex(Me.clcporcentaje_incremento_convenios, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.clcDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.chkIdentificado, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        CType(Me.clcDepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcporcentaje_incremento_convenios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIdentificado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmDepartamentos_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmDepartamentos_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmDepartamentos_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub

    Private Sub frmDepartamentos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oDepartamentos.Insertar(Me.DataSource)
            Case Actions.Update
                Response = oDepartamentos.Actualizar(Me.DataSource)
            Case Actions.Delete
                Response = oDepartamentos.Eliminar(clcDepartamento.Value)
        End Select
    End Sub

    Private Sub frmDepartamentos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oDepartamentos.DespliegaDatos(OwnerForm.Value("departamento"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmDepartamentos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
    End Sub

    Private Sub frmDepartamentos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oDepartamentos.Validacion(Action, txtNombre.Text)
    End Sub

    Private Sub frmDepartamentos_Localize() Handles MyBase.Localize
        Find("departamento", CType(Me.clcDepartamento.EditValue, Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub txtNombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oDepartamentos.ValidaNombre(txtNombre.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
