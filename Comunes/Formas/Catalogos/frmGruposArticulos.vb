Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms


Public Class frmGruposArticulos
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents clcGrupo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grPrecios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvPrecios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombrePrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDepartamentoDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcDepartamentoDescuento As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents lblFactor_Ganancia As System.Windows.Forms.Label
    Friend WithEvents clcFactor_Ganancia As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcFactor_Ganancia_Expo As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grcporcentaje_anterior As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lkpVendedor As Dipros.Editors.TINMultiLookup

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGruposArticulos))
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.clcGrupo = New Dipros.Editors.TINCalcEdit
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.grPrecios = New DevExpress.XtraGrid.GridControl
        Me.grvPrecios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombrePrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDepartamentoDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.clcDepartamentoDescuento = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcporcentaje_anterior = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblFactor_Ganancia = New System.Windows.Forms.Label
        Me.clcFactor_Ganancia = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcFactor_Ganancia_Expo = New Dipros.Editors.TINCalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.lkpVendedor = New Dipros.Editors.TINMultiLookup
        CType(Me.clcGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDepartamentoDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFactor_Ganancia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFactor_Ganancia_Expo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(214, 28)
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(79, 40)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 0
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcGrupo
        '
        Me.clcGrupo.EditValue = "0"
        Me.clcGrupo.Location = New System.Drawing.Point(128, 39)
        Me.clcGrupo.MaxValue = 0
        Me.clcGrupo.MinValue = 0
        Me.clcGrupo.Name = "clcGrupo"
        '
        'clcGrupo.Properties
        '
        Me.clcGrupo.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcGrupo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGrupo.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcGrupo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcGrupo.Properties.Enabled = False
        Me.clcGrupo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcGrupo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcGrupo.Size = New System.Drawing.Size(56, 19)
        Me.clcGrupo.TabIndex = 1
        Me.clcGrupo.Tag = "grupo"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(34, 88)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 4
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(128, 86)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = True
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(304, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 5
        Me.lkpDepartamento.Tag = "Departamento"
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(50, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "De&scripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(128, 62)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(304, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'grPrecios
        '
        '
        'grPrecios.EmbeddedNavigator
        '
        Me.grPrecios.EmbeddedNavigator.Name = ""
        Me.grPrecios.Location = New System.Drawing.Point(8, 160)
        Me.grPrecios.MainView = Me.grvPrecios
        Me.grPrecios.Name = "grPrecios"
        Me.grPrecios.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.clcDepartamentoDescuento})
        Me.grPrecios.Size = New System.Drawing.Size(424, 184)
        Me.grPrecios.TabIndex = 13
        Me.grPrecios.Text = "GridControl1"
        '
        'grvPrecios
        '
        Me.grvPrecios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPrecio, Me.grcNombrePrecio, Me.grcDepartamentoDescuento, Me.grcporcentaje_anterior})
        Me.grvPrecios.GridControl = Me.grPrecios
        Me.grvPrecios.GroupPanelText = "Descuentos"
        Me.grvPrecios.Name = "grvPrecios"
        Me.grvPrecios.OptionsView.ShowGroupPanel = False
        Me.grvPrecios.OptionsView.ShowIndicator = False
        '
        'grcPrecio
        '
        Me.grcPrecio.Caption = "Clave Precio"
        Me.grcPrecio.FieldName = "precio"
        Me.grcPrecio.Name = "grcPrecio"
        Me.grcPrecio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombrePrecio
        '
        Me.grcNombrePrecio.Caption = "Precio"
        Me.grcNombrePrecio.FieldName = "nombre"
        Me.grcNombrePrecio.Name = "grcNombrePrecio"
        Me.grcNombrePrecio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombrePrecio.VisibleIndex = 0
        '
        'grcDepartamentoDescuento
        '
        Me.grcDepartamentoDescuento.Caption = "Descuento"
        Me.grcDepartamentoDescuento.ColumnEdit = Me.clcDepartamentoDescuento
        Me.grcDepartamentoDescuento.FieldName = "porcentaje_descuento"
        Me.grcDepartamentoDescuento.Name = "grcDepartamentoDescuento"
        Me.grcDepartamentoDescuento.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDepartamentoDescuento.VisibleIndex = 1
        '
        'clcDepartamentoDescuento
        '
        Me.clcDepartamentoDescuento.AutoHeight = False
        Me.clcDepartamentoDescuento.DisplayFormat.FormatString = "n2"
        Me.clcDepartamentoDescuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDepartamentoDescuento.EditFormat.FormatString = "n2"
        Me.clcDepartamentoDescuento.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDepartamentoDescuento.MaskData.EditMask = "##.##"
        Me.clcDepartamentoDescuento.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.clcDepartamentoDescuento.Name = "clcDepartamentoDescuento"
        '
        'grcporcentaje_anterior
        '
        Me.grcporcentaje_anterior.Caption = "porcentaje_anterior"
        Me.grcporcentaje_anterior.ColumnEdit = Me.clcDepartamentoDescuento
        Me.grcporcentaje_anterior.FieldName = "porcentaje_anterior"
        Me.grcporcentaje_anterior.Name = "grcporcentaje_anterior"
        Me.grcporcentaje_anterior.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'lblFactor_Ganancia
        '
        Me.lblFactor_Ganancia.AutoSize = True
        Me.lblFactor_Ganancia.Location = New System.Drawing.Point(8, 112)
        Me.lblFactor_Ganancia.Name = "lblFactor_Ganancia"
        Me.lblFactor_Ganancia.Size = New System.Drawing.Size(114, 16)
        Me.lblFactor_Ganancia.TabIndex = 6
        Me.lblFactor_Ganancia.Tag = ""
        Me.lblFactor_Ganancia.Text = "&Factor de ganancia:"
        Me.lblFactor_Ganancia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFactor_Ganancia
        '
        Me.clcFactor_Ganancia.EditValue = "0"
        Me.clcFactor_Ganancia.Location = New System.Drawing.Point(128, 110)
        Me.clcFactor_Ganancia.MaxValue = 0
        Me.clcFactor_Ganancia.MinValue = 0
        Me.clcFactor_Ganancia.Name = "clcFactor_Ganancia"
        '
        'clcFactor_Ganancia.Properties
        '
        Me.clcFactor_Ganancia.Properties.DisplayFormat.FormatString = "#,##0.##"
        Me.clcFactor_Ganancia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFactor_Ganancia.Properties.EditFormat.FormatString = "#,##0.##"
        Me.clcFactor_Ganancia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFactor_Ganancia.Properties.MaskData.EditMask = "#,##0.##"
        Me.clcFactor_Ganancia.Properties.Precision = 4
        Me.clcFactor_Ganancia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFactor_Ganancia.Size = New System.Drawing.Size(56, 19)
        Me.clcFactor_Ganancia.TabIndex = 7
        Me.clcFactor_Ganancia.Tag = "factor_ganancia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(208, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(145, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Tag = ""
        Me.Label2.Text = "Factor de ganancia &Expo:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFactor_Ganancia_Expo
        '
        Me.clcFactor_Ganancia_Expo.EditValue = "0"
        Me.clcFactor_Ganancia_Expo.Location = New System.Drawing.Point(360, 111)
        Me.clcFactor_Ganancia_Expo.MaxValue = 0
        Me.clcFactor_Ganancia_Expo.MinValue = 0
        Me.clcFactor_Ganancia_Expo.Name = "clcFactor_Ganancia_Expo"
        '
        'clcFactor_Ganancia_Expo.Properties
        '
        Me.clcFactor_Ganancia_Expo.Properties.DisplayFormat.FormatString = "#,##0.##"
        Me.clcFactor_Ganancia_Expo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFactor_Ganancia_Expo.Properties.EditFormat.FormatString = "#,##0.##"
        Me.clcFactor_Ganancia_Expo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFactor_Ganancia_Expo.Properties.MaskData.EditMask = "#,##0.##"
        Me.clcFactor_Ganancia_Expo.Properties.Precision = 4
        Me.clcFactor_Ganancia_Expo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFactor_Ganancia_Expo.Size = New System.Drawing.Size(56, 19)
        Me.clcFactor_Ganancia_Expo.TabIndex = 9
        Me.clcFactor_Ganancia_Expo.Tag = "factor_ganancia_expo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(416, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(16, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Tag = ""
        Me.Label4.Text = "%"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(59, 134)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(62, 16)
        Me.lblVendedor.TabIndex = 11
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "Ven&dedor:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpVendedor
        '
        Me.lkpVendedor.AllowAdd = False
        Me.lkpVendedor.AutoReaload = False
        Me.lkpVendedor.DataSource = Nothing
        Me.lkpVendedor.DefaultSearchField = ""
        Me.lkpVendedor.DisplayMember = "nombre"
        Me.lkpVendedor.EditValue = Nothing
        Me.lkpVendedor.Filtered = False
        Me.lkpVendedor.InitValue = Nothing
        Me.lkpVendedor.Location = New System.Drawing.Point(128, 133)
        Me.lkpVendedor.MultiSelect = False
        Me.lkpVendedor.Name = "lkpVendedor"
        Me.lkpVendedor.NullText = ""
        Me.lkpVendedor.PopupWidth = CType(420, Long)
        Me.lkpVendedor.ReadOnlyControl = False
        Me.lkpVendedor.Required = False
        Me.lkpVendedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpVendedor.SearchMember = ""
        Me.lkpVendedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpVendedor.SelectAll = False
        Me.lkpVendedor.Size = New System.Drawing.Size(304, 20)
        Me.lkpVendedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpVendedor.TabIndex = 12
        Me.lkpVendedor.Tag = "Vendedor"
        Me.lkpVendedor.ToolTip = Nothing
        Me.lkpVendedor.ValueMember = "Vendedor"
        '
        'frmGruposArticulos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(442, 352)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lkpVendedor)
        Me.Controls.Add(Me.grPrecios)
        Me.Controls.Add(Me.lblFactor_Ganancia)
        Me.Controls.Add(Me.clcFactor_Ganancia)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcFactor_Ganancia_Expo)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.clcGrupo)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.Label4)
        Me.Name = "frmGruposArticulos"
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.clcGrupo, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcFactor_Ganancia_Expo, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcFactor_Ganancia, 0)
        Me.Controls.SetChildIndex(Me.lblFactor_Ganancia, 0)
        Me.Controls.SetChildIndex(Me.grPrecios, 0)
        Me.Controls.SetChildIndex(Me.lkpVendedor, 0)
        Me.Controls.SetChildIndex(Me.lblVendedor, 0)
        CType(Me.clcGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDepartamentoDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFactor_Ganancia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFactor_Ganancia_Expo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oGruposArticulos As VillarrealBusiness.clsGruposArticulos
    Private oGruposArticulosDescuentos As VillarrealBusiness.clsGruposArticulosDescuentos
    Private odepartamentos As VillarrealBusiness.clsDepartamentos
    Private oVendedores As VillarrealBusiness.clsVendedores

    Private dFactorGanancia As Double = 0
    Private dFactorGananciaExpo As Double = 0

    Private ReadOnly Property Departamento()
        Get
            'Me.lkpDepartamento.Focus()
            Return clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Vendedor()
        Get         
            Return clsUtilerias.PreparaValorLookup(Me.lkpVendedor)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmGruposArticulos_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmGruposArticulos_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmGruposArticulos_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()

        Dim response As New Events
        Dim orow As DataRow

        If dFactorGanancia <> Me.clcFactor_Ganancia.EditValue Then
            If ShowMessage(MessageType.MsgQuestion, "�Deseas Cambiar los precios de los articulos de este grupo?", "Grupos de Articulos") = Answer.MsgYes Then
                response = Me.oGruposArticulos.ActualizaPrecioListaGrupo(Me.clcGrupo.EditValue, Me.clcFactor_Ganancia.EditValue)
                If Not response.ErrorFound Then
                    For Each orow In CType(Me.grPrecios.DataSource, DataTable).Rows
                        If Not response.ErrorFound Then response = oGruposArticulosDescuentos.calcula_precio_venta(Me.clcGrupo.Value, orow("precio"), orow("porcentaje_descuento"))
                    Next
                End If
            End If
        Else
            'Se verifica si al menos un descuento fue modificado
            If VerficaCambiosDescuentos() = True Then
                For Each orow In CType(Me.grPrecios.DataSource, DataTable).Rows
                    If Not response.ErrorFound And (orow("porcentaje_descuento") <> orow("porcentaje_anterior") Or orow("porcentaje_descuento") = 0) Then
                        response = oGruposArticulosDescuentos.calcula_precio_venta(Me.clcGrupo.Value, orow("precio"), orow("porcentaje_descuento"))
                    End If
                Next
            End If
        End If

        If response.ErrorFound Then
            response.ShowMessage()
        ElseIf dFactorGananciaExpo <> Me.clcFactor_Ganancia_Expo.EditValue Then
            If ShowMessage(MessageType.MsgQuestion, "�Desea cambiar el precio expo de los articulos de este grupo?", "Grupos de Articulos") = Answer.MsgYes Then
                response = Me.oGruposArticulos.ActualizaPrecioExpoGrupo(Me.clcGrupo.EditValue, Me.clcFactor_Ganancia_Expo.EditValue)
                If response.ErrorFound Then
                    response.ShowMessage()
                End If
            End If
        End If

    End Sub

    Private Sub frmGruposArticulos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim orow As DataRow
        Me.grvPrecios.UpdateCurrentRow()
        Me.grvPrecios.CloseEditor()
        Select Case Action
            Case Actions.Insert
                Response = oGruposArticulos.Insertar(Me.DataSource)
                For Each orow In CType(Me.grPrecios.DataSource, DataTable).Rows
                    If Not Response.ErrorFound Then Response = oGruposArticulosDescuentos.Insertar(Me.clcGrupo.Value, orow("precio"), orow("porcentaje_descuento"))
                Next
            Case Actions.Update
                Response = oGruposArticulos.Actualizar(Me.DataSource)
                For Each orow In CType(Me.grPrecios.DataSource, DataTable).Rows
                    If Not Response.ErrorFound Then Response = oGruposArticulosDescuentos.Actualizar(Me.clcGrupo.Value, orow("precio"), orow("porcentaje_descuento"))
                Next
            Case Actions.Delete

                For Each orow In CType(Me.grPrecios.DataSource, DataTable).Rows
                    If Not Response.ErrorFound Then Response = oGruposArticulosDescuentos.Eliminar(Me.clcGrupo.Value, orow("precio"))
                Next
                If Not Response.ErrorFound Then Response = oGruposArticulos.Eliminar(clcGrupo.Value)

        End Select
        orow = Nothing
    End Sub
    Private Sub frmGruposArticulos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oGruposArticulos.DespliegaDatos(OwnerForm.Value("grupo"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            dFactorGanancia = Me.clcFactor_Ganancia.EditValue
            dFactorGananciaExpo = Me.clcFactor_Ganancia_Expo.EditValue
        End If

    End Sub
    Private Sub frmGruposArticulos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oGruposArticulos = New VillarrealBusiness.clsGruposArticulos
        oGruposArticulosDescuentos = New VillarrealBusiness.clsGruposArticulosDescuentos
        odepartamentos = New VillarrealBusiness.clsDepartamentos
        oVendedores = New VillarrealBusiness.clsVendedores

        Select Case Action
            Case Actions.Insert
                Response = oGruposArticulosDescuentos.Listado
            Case Actions.Update
                Response = oGruposArticulosDescuentos.Listado(OwnerForm.Value("grupo"))
            Case Actions.Delete
                Response = oGruposArticulosDescuentos.Listado(OwnerForm.Value("grupo"))
        End Select

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.grPrecios.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub frmGruposArticulos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oGruposArticulos.Validacion(Action, Departamento, txtDescripcion.Text, clcFactor_Ganancia.Value, clcFactor_Ganancia_Expo.Value)
    End Sub

    Private Sub frmGruposArticulos_Localize() Handles MyBase.Localize
        Find("grupo", Me.clcGrupo.EditValue)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim response As Events
        response = odepartamentos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub clcFactor_Ganancia_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcFactor_Ganancia.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oGruposArticulos.ValidaFactor_Ganancia(clcFactor_Ganancia.Value)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

    Private Sub lkpVendedor_Format() Handles lkpVendedor.Format
        Comunes.clsFormato.for_vendedores_grl(Me.lkpVendedor)
    End Sub
    Private Sub lkpVendedor_LoadData(ByVal Initialize As Boolean) Handles lkpVendedor.LoadData
        Dim Response As New Events
        Response = oVendedores.Lookup(Comunes.Common.Sucursal_Actual)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpVendedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Function VerficaCambiosDescuentos() As Boolean
        Dim orow As DataRow
        Me.grvPrecios.UpdateCurrentRow()
        Me.grvPrecios.CloseEditor()

        For Each orow In CType(Me.grPrecios.DataSource, DataTable).Rows
            If (orow("porcentaje_descuento") <> orow("porcentaje_anterior")) Then
                VerficaCambiosDescuentos = True
            End If
        Next

        If VerficaCambiosDescuentos = True Then
            If ShowMessage(MessageType.MsgQuestion, "�Deseas Cambiar los precios de los articulos de este grupo?", "Grupos de Articulos") = Answer.MsgYes Then
                VerficaCambiosDescuentos = True
            Else
                VerficaCambiosDescuentos = False
            End If
        End If
    End Function


#End Region

End Class

