Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmArticulosBodegas
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Articulo As String
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblUbicacion As System.Windows.Forms.Label
    Friend WithEvents lblnombre_bodega As System.Windows.Forms.Label
    Friend WithEvents lkpUbicacion As Dipros.Editors.TINMultiLookup

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmArticulosBodegas))
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblUbicacion = New System.Windows.Forms.Label
        Me.lblnombre_bodega = New System.Windows.Forms.Label
        Me.lkpUbicacion = New Dipros.Editors.TINMultiLookup
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(213, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(22, 40)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 0
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(78, 40)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(248, 20)
        Me.lkpBodega.TabIndex = 1
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'lblUbicacion
        '
        Me.lblUbicacion.AutoSize = True
        Me.lblUbicacion.Location = New System.Drawing.Point(10, 63)
        Me.lblUbicacion.Name = "lblUbicacion"
        Me.lblUbicacion.Size = New System.Drawing.Size(62, 16)
        Me.lblUbicacion.TabIndex = 3
        Me.lblUbicacion.Tag = ""
        Me.lblUbicacion.Text = "&Ubicaci�n:"
        Me.lblUbicacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblnombre_bodega
        '
        Me.lblnombre_bodega.Location = New System.Drawing.Point(176, 88)
        Me.lblnombre_bodega.Name = "lblnombre_bodega"
        Me.lblnombre_bodega.Size = New System.Drawing.Size(128, 23)
        Me.lblnombre_bodega.TabIndex = 59
        Me.lblnombre_bodega.Tag = "nombre_bodega"
        Me.lblnombre_bodega.Visible = False
        '
        'lkpUbicacion
        '
        Me.lkpUbicacion.AutoReaload = True
        Me.lkpUbicacion.DataSource = Nothing
        Me.lkpUbicacion.DefaultSearchField = ""
        Me.lkpUbicacion.DisplayMember = "descripcion"
        Me.lkpUbicacion.EditValue = Nothing
        Me.lkpUbicacion.InitValue = Nothing
        Me.lkpUbicacion.Location = New System.Drawing.Point(78, 64)
        Me.lkpUbicacion.MultiSelect = False
        Me.lkpUbicacion.Name = "lkpUbicacion"
        Me.lkpUbicacion.NullText = ""
        Me.lkpUbicacion.PopupWidth = CType(400, Long)
        Me.lkpUbicacion.Required = False
        Me.lkpUbicacion.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpUbicacion.SearchMember = ""
        Me.lkpUbicacion.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpUbicacion.SelectAll = False
        Me.lkpUbicacion.Size = New System.Drawing.Size(248, 20)
        Me.lkpUbicacion.TabIndex = 60
        Me.lkpUbicacion.Tag = "ubicacion"
        Me.lkpUbicacion.ToolTip = Nothing
        Me.lkpUbicacion.ValueMember = "ubicacion"
        '
        'frmArticulosBodegas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(338, 96)
        Me.Controls.Add(Me.lkpUbicacion)
        Me.Controls.Add(Me.lblnombre_bodega)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lblUbicacion)
        Me.Name = "frmArticulosBodegas"
        Me.Controls.SetChildIndex(Me.lblUbicacion, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.lblnombre_bodega, 0)
        Me.Controls.SetChildIndex(Me.lkpUbicacion, 0)
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oArticulosBodegas As VillarrealBusiness.clsArticulosBodegas
    Private oUbicaciones As VillarrealBusiness.clsUbicaciones
    Private oBodegas As New VillarrealBusiness.clsBodegas

    Private ReadOnly Property Bodega() As String
        Get
            Return clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property
    Private ReadOnly Property Ubicacion() As String
        Get
            Return clsUtilerias.PreparaValorLookupStr(Me.lkpUbicacion)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmArticulosBodegas_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmArticulosBodegas_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub frmArticulosBodegas_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oArticulosBodegas = New VillarrealBusiness.clsArticulosBodegas
        oUbicaciones = New VillarrealBusiness.clsUbicaciones

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub


    Private Sub frmArticulosBodegas_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Response = oArticulosBodegas.Validacion(Action, Bodega, Me.Ubicacion)
    End Sub



#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events
        response = oBodegas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub

    Private Sub lkpBodega_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodega.EditValueChanged
        If Me.lkpBodega.DataSource Is Nothing Or Me.lkpBodega.EditValue Is Nothing Then Exit Sub

        Me.lblnombre_bodega.Text = Me.lkpBodega.Text
    End Sub


    Private Sub lkpUbicacion_LoadData(ByVal Initialize As Boolean) Handles lkpUbicacion.LoadData
        Dim response As Events
        response = oUbicaciones.Lookup(Me.Bodega)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpUbicacion.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpUbicacion_Format() Handles lkpUbicacion.Format
        clsFormato.for_ubicaciones_grl(Me.lkpUbicacion)
    End Sub
End Class
