Imports Dipros.Utils.Common
Imports Dipros.Utils
Imports System.Windows.Forms

Public Class frmIndentificadoresPermisos
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtIdentificador As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSistema As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmIndentificadoresPermisos))
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtIdentificador = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtSistema = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtIdentificador.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSistema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(11, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(74, 16)
        Me.lblDescripcion.TabIndex = 59
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "Identificador"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIdentificador
        '
        Me.txtIdentificador.EditValue = ""
        Me.txtIdentificador.Location = New System.Drawing.Point(88, 64)
        Me.txtIdentificador.Name = "txtIdentificador"
        '
        'txtIdentificador.Properties
        '
        Me.txtIdentificador.Properties.MaxLength = 20
        Me.txtIdentificador.Size = New System.Drawing.Size(144, 20)
        Me.txtIdentificador.TabIndex = 60
        Me.txtIdentificador.Tag = "identificador"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 16)
        Me.Label2.TabIndex = 61
        Me.Label2.Tag = ""
        Me.Label2.Text = "Sistema:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSistema
        '
        Me.txtSistema.EditValue = ""
        Me.txtSistema.Location = New System.Drawing.Point(88, 40)
        Me.txtSistema.Name = "txtSistema"
        '
        'txtSistema.Properties
        '
        Me.txtSistema.Properties.Enabled = False
        Me.txtSistema.Properties.MaxLength = 40
        Me.txtSistema.Size = New System.Drawing.Size(80, 20)
        Me.txtSistema.TabIndex = 62
        Me.txtSistema.Tag = "sistema"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 16)
        Me.Label3.TabIndex = 63
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Descripci�n:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(88, 88)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(344, 20)
        Me.txtDescripcion.TabIndex = 64
        Me.txtDescripcion.Tag = "descripcion"
        '
        'frmIndentificadoresPermisos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(449, 116)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtSistema)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtIdentificador)
        Me.Name = "frmIndentificadoresPermisos"
        Me.Text = "frmIndentificadoresPermisos"
        Me.Controls.SetChildIndex(Me.txtIdentificador, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.txtSistema, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        CType(Me.txtIdentificador.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSistema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oIdentificadores As VillarrealBusiness.clsIdentificadoresPermisos
#End Region

    Private Sub frmIndentificadoresPermisos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oIdentificadores.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oIdentificadores.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oIdentificadores.Eliminar(Me.txtSistema.Text, Me.txtIdentificador.Text)

        End Select
    End Sub

    Private Sub frmIndentificadoresPermisos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oIdentificadores = New VillarrealBusiness.clsIdentificadoresPermisos
        Me.txtSistema.Text = TinApp.Prefix

        Select Case Action
            Case Actions.Insert
                Me.txtIdentificador.Enabled = True
            Case Actions.Update
                Me.txtIdentificador.Enabled = False
            Case Actions.Delete
                Me.txtIdentificador.Enabled = False
        End Select
    End Sub

    Private Sub frmIndentificadoresPermisos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oIdentificadores.Validacion(Action, Me.txtSistema.Text, Me.txtIdentificador.Text, txtDescripcion.Text)
    End Sub

    Private Sub frmIndentificadoresPermisos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oIdentificadores.DespliegaDatos(OwnerForm.Value("sistema"), OwnerForm.Value("identificador"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If
    End Sub
    Private Sub frmIndentificadoresPermisos_Localize() Handles MyBase.Localize

    End Sub


#Region "DIPROS Systems, Eventos de Controles"
#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region



End Class
