Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmProveedoresDescuentos
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"

    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblDescuento As System.Windows.Forms.Label
    Friend WithEvents clcDescuento As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPorcentaje As System.Windows.Forms.Label
    Friend WithEvents clcPorcentaje As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkAntes_Iva As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkPronto_Pago As DevExpress.XtraEditors.CheckEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmProveedoresDescuentos))
        Me.lblDescuento = New System.Windows.Forms.Label
        Me.clcDescuento = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblPorcentaje = New System.Windows.Forms.Label
        Me.clcPorcentaje = New Dipros.Editors.TINCalcEdit
        Me.chkAntes_Iva = New DevExpress.XtraEditors.CheckEdit
        Me.chkPronto_Pago = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAntes_Iva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPronto_Pago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(527, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblDescuento
        '
        Me.lblDescuento.AutoSize = True
        Me.lblDescuento.Location = New System.Drawing.Point(17, 40)
        Me.lblDescuento.Name = "lblDescuento"
        Me.lblDescuento.Size = New System.Drawing.Size(67, 16)
        Me.lblDescuento.TabIndex = 0
        Me.lblDescuento.Tag = ""
        Me.lblDescuento.Text = "&Descuento:"
        Me.lblDescuento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDescuento
        '
        Me.clcDescuento.EditValue = "0"
        Me.clcDescuento.Location = New System.Drawing.Point(92, 40)
        Me.clcDescuento.MaxValue = 0
        Me.clcDescuento.MinValue = 0
        Me.clcDescuento.Name = "clcDescuento"
        '
        'clcDescuento.Properties
        '
        Me.clcDescuento.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcDescuento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDescuento.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcDescuento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDescuento.Properties.Enabled = False
        Me.clcDescuento.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcDescuento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDescuento.Size = New System.Drawing.Size(68, 19)
        Me.clcDescuento.TabIndex = 1
        Me.clcDescuento.Tag = "descuento"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(31, 63)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(92, 63)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 100
        Me.txtNombre.Size = New System.Drawing.Size(356, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.AutoSize = True
        Me.lblPorcentaje.Location = New System.Drawing.Point(16, 86)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(68, 16)
        Me.lblPorcentaje.TabIndex = 4
        Me.lblPorcentaje.Tag = ""
        Me.lblPorcentaje.Text = "P&orcentaje:"
        Me.lblPorcentaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPorcentaje
        '
        Me.clcPorcentaje.EditValue = "0"
        Me.clcPorcentaje.Location = New System.Drawing.Point(92, 88)
        Me.clcPorcentaje.MaxValue = 0
        Me.clcPorcentaje.MinValue = 0
        Me.clcPorcentaje.Name = "clcPorcentaje"
        '
        'clcPorcentaje.Properties
        '
        Me.clcPorcentaje.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcPorcentaje.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcPorcentaje.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.Properties.MaskData.EditMask = "########0.00"
        Me.clcPorcentaje.Properties.Precision = 18
        Me.clcPorcentaje.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPorcentaje.Size = New System.Drawing.Size(68, 19)
        Me.clcPorcentaje.TabIndex = 5
        Me.clcPorcentaje.Tag = "porcentaje"
        '
        'chkAntes_Iva
        '
        Me.chkAntes_Iva.Location = New System.Drawing.Point(92, 112)
        Me.chkAntes_Iva.Name = "chkAntes_Iva"
        '
        'chkAntes_Iva.Properties
        '
        Me.chkAntes_Iva.Properties.Caption = "Antes de &IVA"
        Me.chkAntes_Iva.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAntes_Iva.Size = New System.Drawing.Size(188, 19)
        Me.chkAntes_Iva.TabIndex = 6
        Me.chkAntes_Iva.Tag = "antes_iva"
        '
        'chkPronto_Pago
        '
        Me.chkPronto_Pago.Location = New System.Drawing.Point(92, 135)
        Me.chkPronto_Pago.Name = "chkPronto_Pago"
        '
        'chkPronto_Pago.Properties
        '
        Me.chkPronto_Pago.Properties.Caption = "Pron&to Pago"
        Me.chkPronto_Pago.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkPronto_Pago.Size = New System.Drawing.Size(196, 19)
        Me.chkPronto_Pago.TabIndex = 7
        Me.chkPronto_Pago.Tag = "pronto_pago"
        '
        'frmProveedoresDescuentos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(466, 160)
        Me.Controls.Add(Me.lblDescuento)
        Me.Controls.Add(Me.clcDescuento)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblPorcentaje)
        Me.Controls.Add(Me.clcPorcentaje)
        Me.Controls.Add(Me.chkAntes_Iva)
        Me.Controls.Add(Me.chkPronto_Pago)
        Me.Name = "frmProveedoresDescuentos"
        Me.Controls.SetChildIndex(Me.chkPronto_Pago, 0)
        Me.Controls.SetChildIndex(Me.chkAntes_Iva, 0)
        Me.Controls.SetChildIndex(Me.clcPorcentaje, 0)
        Me.Controls.SetChildIndex(Me.lblPorcentaje, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcDescuento, 0)
        Me.Controls.SetChildIndex(Me.lblDescuento, 0)
        CType(Me.clcDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAntes_Iva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPronto_Pago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oProveedoresDescuentos As New VillarrealBusiness.clsProveedoresDescuentos
    Private oEntradas As New VillarrealBusiness.clsEntradas




    Public ReadOnly Property Proveedor() As Long
        Get
            Return OwnerForm.Proveedor
        End Get
    End Property

    Public ReadOnly Property Vista_Descuentos() As DataView
        Get
            Return OwnerForm.Vista_Descuentos
        End Get
    End Property



#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmProveedoresDescuentos_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmProveedoresDescuentos_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Try
            Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Catch ex As Exception

        End Try

    End Sub

    Private Sub frmProveedoresDescuentos_Initialize(ByRef Response As Events) Handles MyBase.Initialize

        Select Case Action
            Case Actions.Insert
                Me.clcDescuento.Value = CType(OwnerForm.Vista_Descuentos, DataView).Count + 1
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmProveedoresDescuentos_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        If Action <> Actions.Delete Then
            Response = oProveedoresDescuentos.Validacion(Action, Me.txtNombre.Text, Me.clcPorcentaje.Value, Proveedor, Me.chkAntes_Iva.Checked, Vista_Descuentos, ExisteProntoPago())
        End If
    End Sub



#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Function ExisteProntoPago() As Boolean

        Dim i As Integer
        Dim existe_pronto_pago As Boolean = False
        Try
            Select Case Action
                Case Actions.Insert
                    If Me.chkPronto_Pago.Checked = True Then
                        For i = 0 To Me.OwnerForm.MasterControl.DataSource.Tables(0).Rows.count - 1
                            If CType(Me.OwnerForm.MasterControl.DataSource.Tables(0).Rows(i).Item("pronto_pago"), Boolean) Then

                                existe_pronto_pago = True

                            End If
                        Next
                    End If

                Case Actions.Update
                    If Me.chkPronto_Pago.Checked = True Then
                        For i = 0 To Me.OwnerForm.MasterControl.DataSource.Tables(0).Rows.count - 1
                            If CType(Me.OwnerForm.MasterControl.DataSource.Tables(0).Rows(i).Item("pronto_pago"), Boolean) Then
                                If CType(Me.OwnerForm.MasterControl.DataSource.Tables(0).Rows(i).Item("descuento"), Double) <> Me.clcDescuento.Value Then
                                    existe_pronto_pago = True
                                End If
                            End If
                        Next
                    End If
            End Select

        Catch ex As Exception
            ShowMessage(MessageType.MsgInformation, "Ocurr�o un error al validar el pronto pago")
            Return existe_pronto_pago
        End Try

        Return existe_pronto_pago

    End Function


#End Region


    Private Sub TinCalcEdit1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

End Class
