Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmBodegas
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents txtBodega As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblContrase�a As System.Windows.Forms.Label
    Friend WithEvents txtContrase�a As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkMovil As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtDireccion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBodegas))
        Me.lblBodega = New System.Windows.Forms.Label
        Me.txtBodega = New DevExpress.XtraEditors.TextEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblContrase�a = New System.Windows.Forms.Label
        Me.txtContrase�a = New DevExpress.XtraEditors.TextEdit
        Me.chkMovil = New DevExpress.XtraEditors.CheckEdit
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
        Me.txtDireccion = New DevExpress.XtraEditors.MemoEdit
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.txtBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContrase�a.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMovil.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(538, 28)
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(40, 40)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 0
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBodega
        '
        Me.txtBodega.EditValue = ""
        Me.txtBodega.Location = New System.Drawing.Point(92, 40)
        Me.txtBodega.Name = "txtBodega"
        '
        'txtBodega.Properties
        '
        Me.txtBodega.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBodega.Properties.MaxLength = 5
        Me.txtBodega.Size = New System.Drawing.Size(60, 20)
        Me.txtBodega.TabIndex = 1
        Me.txtBodega.Tag = "bodega"
        Me.txtBodega.ToolTip = "bodega"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(34, 63)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(92, 63)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = True
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(360, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 3
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = "sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(18, 86)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 4
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(92, 86)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 60
        Me.txtDescripcion.Size = New System.Drawing.Size(360, 20)
        Me.txtDescripcion.TabIndex = 5
        Me.txtDescripcion.Tag = "descripcion"
        Me.txtDescripcion.ToolTip = "descripci�n"
        '
        'lblContrase�a
        '
        Me.lblContrase�a.AutoSize = True
        Me.lblContrase�a.Location = New System.Drawing.Point(160, 200)
        Me.lblContrase�a.Name = "lblContrase�a"
        Me.lblContrase�a.Size = New System.Drawing.Size(72, 16)
        Me.lblContrase�a.TabIndex = 8
        Me.lblContrase�a.Tag = ""
        Me.lblContrase�a.Text = "&Contrase�a:"
        Me.lblContrase�a.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblContrase�a.Visible = False
        '
        'txtContrase�a
        '
        Me.txtContrase�a.EditValue = ""
        Me.txtContrase�a.Location = New System.Drawing.Point(240, 200)
        Me.txtContrase�a.Name = "txtContrase�a"
        '
        'txtContrase�a.Properties
        '
        Me.txtContrase�a.Properties.MaxLength = 10
        Me.txtContrase�a.Properties.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtContrase�a.Size = New System.Drawing.Size(60, 20)
        Me.txtContrase�a.TabIndex = 9
        Me.txtContrase�a.Tag = "contrase�a"
        Me.txtContrase�a.Visible = False
        '
        'chkMovil
        '
        Me.chkMovil.Location = New System.Drawing.Point(92, 184)
        Me.chkMovil.Name = "chkMovil"
        '
        'chkMovil.Properties
        '
        Me.chkMovil.Properties.Caption = "&M�vil"
        Me.chkMovil.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkMovil.Size = New System.Drawing.Size(76, 20)
        Me.chkMovil.TabIndex = 6
        Me.chkMovil.Tag = "movil"
        Me.chkMovil.ToolTip = "movil"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(312, 184)
        Me.CheckEdit1.Name = "CheckEdit1"
        '
        'CheckEdit1.Properties
        '
        Me.CheckEdit1.Properties.Caption = "B&odega Principal"
        Me.CheckEdit1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.CheckEdit1.Size = New System.Drawing.Size(140, 20)
        Me.CheckEdit1.TabIndex = 7
        Me.CheckEdit1.Tag = "bodega_principal"
        Me.CheckEdit1.ToolTip = "bodega principal"
        '
        'txtDireccion
        '
        Me.txtDireccion.EditValue = ""
        Me.txtDireccion.Location = New System.Drawing.Point(92, 112)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(360, 56)
        Me.txtDireccion.TabIndex = 7
        Me.txtDireccion.Tag = "direccion"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Tag = ""
        Me.Label2.Text = "Direcci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmBodegas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(462, 184)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.CheckEdit1)
        Me.Controls.Add(Me.chkMovil)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.txtBodega)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblContrase�a)
        Me.Controls.Add(Me.txtContrase�a)
        Me.Name = "frmBodegas"
        Me.Controls.SetChildIndex(Me.txtContrase�a, 0)
        Me.Controls.SetChildIndex(Me.lblContrase�a, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.txtBodega, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.chkMovil, 0)
        Me.Controls.SetChildIndex(Me.CheckEdit1, 0)
        Me.Controls.SetChildIndex(Me.txtDireccion, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        CType(Me.txtBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContrase�a.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMovil.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oSucursales As New VillarrealBusiness.clsSucursales

    Private bBodegaPrincipalActual As Boolean = False
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmBodegas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                If Me.CheckEdit1.Checked Then
                    If bodegaprincipalExiste() Then
                        If bBodegaPrincipalActual Then
                            Response = oBodegas.Insertar(Me.DataSource)
                        Else
                            If ShowMessage(MessageType.MsgQuestion, "Ya existe una bodega principal, �desea cambiarla?") = Answer.MsgYes Then
                                Response = oBodegas.Insertar(Me.DataSource)
                            End If
                        End If

                    Else
                        Response = oBodegas.Insertar(Me.DataSource)
                    End If
                Else
                    Response = oBodegas.Insertar(Me.DataSource)
                End If


            Case Actions.Update

                If Me.CheckEdit1.Checked Then
                    If bodegaprincipalExiste() Then
                        If bBodegaPrincipalActual Then
                            Response = oBodegas.Actualizar(Me.DataSource)
                        Else
                            If ShowMessage(MessageType.MsgQuestion, "Ya existe una bodega principal, �desea cambiarla?") = Answer.MsgYes Then
                                Response = oBodegas.Actualizar(Me.DataSource)
                            End If
                        End If

                    Else
                        Response = oBodegas.Actualizar(Me.DataSource)
                    End If
                Else
                    Response = oBodegas.Actualizar(Me.DataSource)
                End If

            Case Actions.Delete
                Response = oBodegas.Eliminar(txtBodega.Text)

        End Select
    End Sub

    Private Sub frmBodegas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oBodegas.DespliegaDatos(OwnerForm.Value("bodega"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            If oDataSet.Tables(0).Rows(0).Item("bodega_principal") Then
                bBodegaPrincipalActual = True
            End If
        End If

    End Sub

    Private Sub frmBodegas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oBodegas = New VillarrealBusiness.clsBodegas

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.txtBodega.Enabled = False
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmBodegas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oBodegas.Validacion(Action, Me.txtBodega.Text, lkpSucursal.EditValue, txtDescripcion.Text, txtContrase�a.Text, bodegaprincipalExiste())
    End Sub

    Private Sub frmBodegas_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub _Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtBodega.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oBodegas.ValidaBodega(Me.txtBodega.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    'Private Sub lkpSucursal_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles lkpSucursal.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oBodegas.ValidaSucursal(lkpSucursal.EditValue)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub
    'Private Sub txtDescripcion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oBodegas.ValidaDescripcion(txtDescripcion.Text)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub
    'Private Sub txtContrase�a_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtContrase�a.Validating
    '    Dim oEvent As Dipros.Utils.Events
    '    If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
    '    oEvent = oBodegas.ValidaContrase�a(txtContrase�a.Text)
    '    If oEvent.ErrorFound Then
    '        oEvent.ShowError()
    '        e.Cancel = True
    '    End If
    'End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Function bodegaprincipalExiste() As Boolean
        Dim existe As Boolean = False
        Dim response As Dipros.Utils.Events
        Dim oDataset As DataSet

        response = oBodegas.BodegaPrincipalExiste()
        If response.ErrorFound = False Then
            oDataset = response.Value
            existe = oDataset.Tables(0).Rows(0).Item("existe")
        End If

        Return existe
    End Function
#End Region





End Class
