Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmProveedoresCuentasBancarias
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
    Friend WithEvents lblBanco As System.Windows.Forms.Label
		Friend WithEvents lkpBanco As Dipros.Editors.TINMultiLookup
		Friend WithEvents lblSucursal As System.Windows.Forms.Label
		Friend WithEvents txtSucursal As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblPlaza As System.Windows.Forms.Label
		Friend WithEvents txtPlaza As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblCuenta As System.Windows.Forms.Label
		Friend WithEvents txtCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNombreBanco As System.Windows.Forms.Label

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmProveedoresCuentasBancarias))
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lkpBanco = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.txtSucursal = New DevExpress.XtraEditors.TextEdit
        Me.lblPlaza = New System.Windows.Forms.Label
        Me.txtPlaza = New DevExpress.XtraEditors.TextEdit
        Me.lblCuenta = New System.Windows.Forms.Label
        Me.txtCuenta = New DevExpress.XtraEditors.TextEdit
        Me.lblNombreBanco = New System.Windows.Forms.Label
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPlaza.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(189, 28)
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(34, 40)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 0
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBanco
        '
        Me.lkpBanco.AllowAdd = False
        Me.lkpBanco.AutoReaload = False
        Me.lkpBanco.DataSource = Nothing
        Me.lkpBanco.DefaultSearchField = ""
        Me.lkpBanco.DisplayMember = "nombre"
        Me.lkpBanco.EditValue = Nothing
        Me.lkpBanco.Filtered = False
        Me.lkpBanco.InitValue = Nothing
        Me.lkpBanco.Location = New System.Drawing.Point(80, 40)
        Me.lkpBanco.MultiSelect = False
        Me.lkpBanco.Name = "lkpBanco"
        Me.lkpBanco.NullText = ""
        Me.lkpBanco.PopupWidth = CType(400, Long)
        Me.lkpBanco.Required = False
        Me.lkpBanco.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBanco.SearchMember = ""
        Me.lkpBanco.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBanco.SelectAll = False
        Me.lkpBanco.Size = New System.Drawing.Size(136, 20)
        Me.lkpBanco.TabIndex = 1
        Me.lkpBanco.Tag = "Banco"
        Me.lkpBanco.ToolTip = Nothing
        Me.lkpBanco.ValueMember = "Banco"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(21, 64)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSucursal
        '
        Me.txtSucursal.EditValue = ""
        Me.txtSucursal.Location = New System.Drawing.Point(80, 64)
        Me.txtSucursal.Name = "txtSucursal"
        '
        'txtSucursal.Properties
        '
        Me.txtSucursal.Properties.MaxLength = 20
        Me.txtSucursal.Size = New System.Drawing.Size(136, 20)
        Me.txtSucursal.TabIndex = 3
        Me.txtSucursal.Tag = "sucursal"
        '
        'lblPlaza
        '
        Me.lblPlaza.AutoSize = True
        Me.lblPlaza.Location = New System.Drawing.Point(39, 88)
        Me.lblPlaza.Name = "lblPlaza"
        Me.lblPlaza.Size = New System.Drawing.Size(38, 16)
        Me.lblPlaza.TabIndex = 4
        Me.lblPlaza.Tag = ""
        Me.lblPlaza.Text = "P&laza:"
        Me.lblPlaza.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPlaza
        '
        Me.txtPlaza.EditValue = ""
        Me.txtPlaza.Location = New System.Drawing.Point(80, 88)
        Me.txtPlaza.Name = "txtPlaza"
        '
        'txtPlaza.Properties
        '
        Me.txtPlaza.Properties.MaxLength = 5
        Me.txtPlaza.Size = New System.Drawing.Size(72, 20)
        Me.txtPlaza.TabIndex = 5
        Me.txtPlaza.Tag = "plaza"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(29, 112)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(48, 16)
        Me.lblCuenta.TabIndex = 6
        Me.lblCuenta.Tag = ""
        Me.lblCuenta.Text = "C&uenta:"
        Me.lblCuenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCuenta
        '
        Me.txtCuenta.EditValue = ""
        Me.txtCuenta.Location = New System.Drawing.Point(80, 112)
        Me.txtCuenta.Name = "txtCuenta"
        '
        'txtCuenta.Properties
        '
        Me.txtCuenta.Properties.MaxLength = 20
        Me.txtCuenta.Size = New System.Drawing.Size(136, 20)
        Me.txtCuenta.TabIndex = 7
        Me.txtCuenta.Tag = "cuenta"
        '
        'lblNombreBanco
        '
        Me.lblNombreBanco.Location = New System.Drawing.Point(48, 152)
        Me.lblNombreBanco.Name = "lblNombreBanco"
        Me.lblNombreBanco.TabIndex = 59
        Me.lblNombreBanco.Tag = "nombre"
        Me.lblNombreBanco.Visible = False
        '
        'frmProveedoresCuentasBancarias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(242, 144)
        Me.Controls.Add(Me.lblNombreBanco)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.lkpBanco)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.txtSucursal)
        Me.Controls.Add(Me.lblPlaza)
        Me.Controls.Add(Me.txtPlaza)
        Me.Controls.Add(Me.lblCuenta)
        Me.Controls.Add(Me.txtCuenta)
        Me.Name = "frmProveedoresCuentasBancarias"
        Me.Controls.SetChildIndex(Me.txtCuenta, 0)
        Me.Controls.SetChildIndex(Me.lblCuenta, 0)
        Me.Controls.SetChildIndex(Me.txtPlaza, 0)
        Me.Controls.SetChildIndex(Me.lblPlaza, 0)
        Me.Controls.SetChildIndex(Me.txtSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpBanco, 0)
        Me.Controls.SetChildIndex(Me.lblBanco, 0)
        Me.Controls.SetChildIndex(Me.lblNombreBanco, 0)
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPlaza.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oProveedoresCuentasBancarias As VillarrealBusiness.clsProveedoresCuentasBancarias
    Private oBancos As VillarrealBusiness.clsBancos

    Private ReadOnly Property Banco() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpBanco)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmProveedoresCuentasBancarias_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub
    Private Sub frmProveedoresCuentasBancarias_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub
    Private Sub frmProveedoresCuentasBancarias_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oProveedoresCuentasBancarias = New VillarrealBusiness.clsProveedoresCuentasBancarias
        oBancos = New VillarrealBusiness.clsBancos

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpBanco.Enabled = False
                Me.txtCuenta.Enabled = False
            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmProveedoresCuentasBancarias_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        Response = CType(OwnerForm, frmProveedores).oProveedoresCuentasBancarias.Validacion(Action, Banco, Me.txtSucursal.Text, Me.txtPlaza.Text, Me.txtCuenta.Text)
    End Sub

    Private Sub frmProveedoresCuentasBancarias_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpBanco_Format() Handles lkpBanco.Format
        clsFormato.for_bancos_grl(Me.lkpBanco)
    End Sub
    Private Sub lkpBanco_LoadData(ByVal Initialize As Boolean) Handles lkpBanco.LoadData
        Dim response As Events
        response = oBancos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBanco.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpBanco_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBanco.EditValueChanged
        If Me.lkpBanco Is Nothing Then
            Me.lblNombreBanco.Text = ""
            Exit Sub
        End If

        Me.lblNombreBanco.Text = CStr(Me.lkpBanco.GetValue("nombre"))

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
