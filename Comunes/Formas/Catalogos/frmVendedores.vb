Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmVendedores
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As System.Windows.Forms.Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents clcVendedor As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSueldo As System.Windows.Forms.Label
    Friend WithEvents clcSueldo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcMeta As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtnomipaq As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lkpEsquemasComisiones As Dipros.Editors.TINMultiLookup
    Friend WithEvents grEsquemas As DevExpress.XtraGrid.GridControl
    Friend WithEvents tmaEsquemas As Dipros.Windows.TINMaster
    Friend WithEvents grvEsquemas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcNumSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroEsquema As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreEsquema As DevExpress.XtraGrid.Columns.GridColumn

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVendedores))
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.clcVendedor = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblSueldo = New System.Windows.Forms.Label
        Me.clcSueldo = New Dipros.Editors.TINCalcEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcMeta = New Dipros.Editors.TINCalcEdit
        Me.chkActivo = New DevExpress.XtraEditors.CheckEdit
        Me.txtnomipaq = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lkpEsquemasComisiones = New Dipros.Editors.TINMultiLookup
        Me.grEsquemas = New DevExpress.XtraGrid.GridControl
        Me.grvEsquemas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcNumSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroEsquema = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreEsquema = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaEsquemas = New Dipros.Windows.TINMaster
        CType(Me.clcVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSueldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMeta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtnomipaq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grEsquemas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvEsquemas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(859, 28)
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(7, 40)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(62, 16)
        Me.lblVendedor.TabIndex = 0
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "V&endedor:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcVendedor
        '
        Me.clcVendedor.EditValue = "0"
        Me.clcVendedor.Location = New System.Drawing.Point(74, 40)
        Me.clcVendedor.MaxValue = 0
        Me.clcVendedor.MinValue = 0
        Me.clcVendedor.Name = "clcVendedor"
        '
        'clcVendedor.Properties
        '
        Me.clcVendedor.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcVendedor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcVendedor.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcVendedor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcVendedor.Properties.Enabled = False
        Me.clcVendedor.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcVendedor.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcVendedor.Size = New System.Drawing.Size(46, 19)
        Me.clcVendedor.TabIndex = 1
        Me.clcVendedor.Tag = "vendedor"
        Me.clcVendedor.ToolTip = "vendedor"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(16, 64)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(74, 63)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 60
        Me.txtNombre.Size = New System.Drawing.Size(360, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        Me.txtNombre.ToolTip = "nombre"
        '
        'lblSueldo
        '
        Me.lblSueldo.AutoSize = True
        Me.lblSueldo.Location = New System.Drawing.Point(22, 112)
        Me.lblSueldo.Name = "lblSueldo"
        Me.lblSueldo.Size = New System.Drawing.Size(47, 16)
        Me.lblSueldo.TabIndex = 6
        Me.lblSueldo.Tag = ""
        Me.lblSueldo.Text = "&Sueldo:"
        Me.lblSueldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSueldo
        '
        Me.clcSueldo.EditValue = "0"
        Me.clcSueldo.Location = New System.Drawing.Point(74, 111)
        Me.clcSueldo.MaxValue = 0
        Me.clcSueldo.MinValue = 0
        Me.clcSueldo.Name = "clcSueldo"
        '
        'clcSueldo.Properties
        '
        Me.clcSueldo.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcSueldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSueldo.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcSueldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSueldo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSueldo.Properties.MaxLength = 8
        Me.clcSueldo.Properties.Precision = 2
        Me.clcSueldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSueldo.Size = New System.Drawing.Size(94, 19)
        Me.clcSueldo.TabIndex = 7
        Me.clcSueldo.Tag = "sueldo"
        Me.clcSueldo.ToolTip = "sueldo"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(13, 88)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(74, 87)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(358, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 5
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = "sucursal"
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(33, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Meta:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMeta
        '
        Me.clcMeta.EditValue = "0"
        Me.clcMeta.Location = New System.Drawing.Point(74, 134)
        Me.clcMeta.MaxValue = 0
        Me.clcMeta.MinValue = 0
        Me.clcMeta.Name = "clcMeta"
        '
        'clcMeta.Properties
        '
        Me.clcMeta.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcMeta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMeta.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcMeta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMeta.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMeta.Properties.MaxLength = 8
        Me.clcMeta.Properties.Precision = 2
        Me.clcMeta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMeta.Size = New System.Drawing.Size(94, 19)
        Me.clcMeta.TabIndex = 9
        Me.clcMeta.Tag = "meta"
        Me.clcMeta.ToolTip = "meta"
        '
        'chkActivo
        '
        Me.chkActivo.Location = New System.Drawing.Point(72, 160)
        Me.chkActivo.Name = "chkActivo"
        '
        'chkActivo.Properties
        '
        Me.chkActivo.Properties.Caption = "Activo"
        Me.chkActivo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkActivo.Size = New System.Drawing.Size(72, 19)
        Me.chkActivo.TabIndex = 14
        Me.chkActivo.Tag = "activo"
        Me.chkActivo.ToolTip = "activo"
        '
        'txtnomipaq
        '
        Me.txtnomipaq.EditValue = ""
        Me.txtnomipaq.Location = New System.Drawing.Point(296, 133)
        Me.txtnomipaq.Name = "txtnomipaq"
        '
        'txtnomipaq.Properties
        '
        Me.txtnomipaq.Properties.MaxLength = 5
        Me.txtnomipaq.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtnomipaq.Size = New System.Drawing.Size(64, 20)
        Me.txtnomipaq.TabIndex = 11
        Me.txtnomipaq.Tag = "codigo_nomipaq"
        Me.txtnomipaq.ToolTip = "nombre"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(192, 135)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Tag = ""
        Me.Label3.Text = "C�digo Nomipaq :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(192, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 16)
        Me.Label4.TabIndex = 12
        Me.Label4.Tag = ""
        Me.Label4.Text = "Esquema:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label4.Visible = False
        '
        'lkpEsquemasComisiones
        '
        Me.lkpEsquemasComisiones.AllowAdd = False
        Me.lkpEsquemasComisiones.AutoReaload = False
        Me.lkpEsquemasComisiones.DataSource = Nothing
        Me.lkpEsquemasComisiones.DefaultSearchField = ""
        Me.lkpEsquemasComisiones.DisplayMember = "nombre"
        Me.lkpEsquemasComisiones.EditValue = Nothing
        Me.lkpEsquemasComisiones.Filtered = False
        Me.lkpEsquemasComisiones.InitValue = Nothing
        Me.lkpEsquemasComisiones.Location = New System.Drawing.Point(256, 157)
        Me.lkpEsquemasComisiones.MultiSelect = False
        Me.lkpEsquemasComisiones.Name = "lkpEsquemasComisiones"
        Me.lkpEsquemasComisiones.NullText = ""
        Me.lkpEsquemasComisiones.PopupWidth = CType(400, Long)
        Me.lkpEsquemasComisiones.ReadOnlyControl = False
        Me.lkpEsquemasComisiones.Required = False
        Me.lkpEsquemasComisiones.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEsquemasComisiones.SearchMember = ""
        Me.lkpEsquemasComisiones.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEsquemasComisiones.SelectAll = False
        Me.lkpEsquemasComisiones.Size = New System.Drawing.Size(176, 20)
        Me.lkpEsquemasComisiones.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEsquemasComisiones.TabIndex = 13
        Me.lkpEsquemasComisiones.Tag = "esquema_comisiones"
        Me.lkpEsquemasComisiones.ToolTip = "Esquema de comisiones para el vendedor"
        Me.lkpEsquemasComisiones.ValueMember = "esquema"
        Me.lkpEsquemasComisiones.Visible = False
        '
        'grEsquemas
        '
        Me.grEsquemas.BackColor = System.Drawing.SystemColors.Window
        '
        'grEsquemas.EmbeddedNavigator
        '
        Me.grEsquemas.EmbeddedNavigator.Name = ""
        Me.grEsquemas.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grEsquemas.ForeColor = System.Drawing.Color.Black
        Me.grEsquemas.Location = New System.Drawing.Point(21, 216)
        Me.grEsquemas.MainView = Me.grvEsquemas
        Me.grEsquemas.Name = "grEsquemas"
        Me.grEsquemas.Size = New System.Drawing.Size(408, 184)
        Me.grEsquemas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEsquemas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEsquemas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEsquemas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grEsquemas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEsquemas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEsquemas.TabIndex = 64
        Me.grEsquemas.TabStop = False
        Me.grEsquemas.Text = "Esquemas"
        '
        'grvEsquemas
        '
        Me.grvEsquemas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcNumSucursal, Me.grcNombreSucursal, Me.grcNumeroEsquema, Me.grcNombreEsquema})
        Me.grvEsquemas.GridControl = Me.grEsquemas
        Me.grvEsquemas.GroupPanelText = "Esquemas del Vendedor"
        Me.grvEsquemas.Name = "grvEsquemas"
        Me.grvEsquemas.OptionsBehavior.Editable = False
        Me.grvEsquemas.OptionsView.ShowFilterPanel = False
        Me.grvEsquemas.OptionsView.ShowGroupedColumns = True
        Me.grvEsquemas.OptionsView.ShowGroupPanel = False
        '
        'grcNumSucursal
        '
        Me.grcNumSucursal.Caption = "No. Sucursal"
        Me.grcNumSucursal.FieldName = "sucursal"
        Me.grcNumSucursal.Name = "grcNumSucursal"
        Me.grcNumSucursal.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumSucursal.VisibleIndex = 0
        Me.grcNumSucursal.Width = 80
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Nombre Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursal.VisibleIndex = 1
        Me.grcNombreSucursal.Width = 120
        '
        'grcNumeroEsquema
        '
        Me.grcNumeroEsquema.Caption = "No. Esquema"
        Me.grcNumeroEsquema.FieldName = "esquema"
        Me.grcNumeroEsquema.MinWidth = 10
        Me.grcNumeroEsquema.Name = "grcNumeroEsquema"
        Me.grcNumeroEsquema.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumeroEsquema.VisibleIndex = 2
        Me.grcNumeroEsquema.Width = 80
        '
        'grcNombreEsquema
        '
        Me.grcNombreEsquema.Caption = "Nombre Esquema"
        Me.grcNombreEsquema.FieldName = "nombre_esquema"
        Me.grcNombreEsquema.Name = "grcNombreEsquema"
        Me.grcNombreEsquema.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreEsquema.VisibleIndex = 3
        Me.grcNombreEsquema.Width = 120
        '
        'tmaEsquemas
        '
        Me.tmaEsquemas.BackColor = System.Drawing.Color.White
        Me.tmaEsquemas.CanDelete = True
        Me.tmaEsquemas.CanInsert = True
        Me.tmaEsquemas.CanUpdate = False
        Me.tmaEsquemas.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tmaEsquemas.ForeColor = System.Drawing.Color.Black
        Me.tmaEsquemas.Grid = Me.grEsquemas
        Me.tmaEsquemas.Location = New System.Drawing.Point(24, 192)
        Me.tmaEsquemas.Name = "tmaEsquemas"
        Me.tmaEsquemas.Size = New System.Drawing.Size(400, 23)
        Me.tmaEsquemas.TabIndex = 63
        Me.tmaEsquemas.TabStop = False
        Me.tmaEsquemas.Title = "Esquemas"
        Me.tmaEsquemas.UpdateTitle = "un Registro"
        '
        'frmVendedores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(450, 412)
        Me.Controls.Add(Me.grEsquemas)
        Me.Controls.Add(Me.tmaEsquemas)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtnomipaq)
        Me.Controls.Add(Me.chkActivo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcMeta)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.clcVendedor)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblSueldo)
        Me.Controls.Add(Me.clcSueldo)
        Me.Controls.Add(Me.lkpEsquemasComisiones)
        Me.Name = "frmVendedores"
        Me.Text = "Vendedores"
        Me.Title = "Vendedores"
        Me.Controls.SetChildIndex(Me.lkpEsquemasComisiones, 0)
        Me.Controls.SetChildIndex(Me.clcSueldo, 0)
        Me.Controls.SetChildIndex(Me.lblSueldo, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcVendedor, 0)
        Me.Controls.SetChildIndex(Me.lblVendedor, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.clcMeta, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.chkActivo, 0)
        Me.Controls.SetChildIndex(Me.txtnomipaq, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.tmaEsquemas, 0)
        Me.Controls.SetChildIndex(Me.grEsquemas, 0)
        CType(Me.clcVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSueldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMeta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtnomipaq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grEsquemas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvEsquemas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oVendedores As VillarrealBusiness.clsVendedores
    Private oVendedoresEsquemas As VillarrealBusiness.clsVendedoresEsquemas
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oEsquemasComisiones As VillarrealBusiness.clsEsquemasComisiones


    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property


    Private ReadOnly Property Esquema() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEsquemasComisiones)
        End Get
    End Property

    Public ReadOnly Property TablaEsquemas() As DataTable
        Get
            Return CType(tmaEsquemas.DataSource, DataSet).Tables(0)
        End Get
        
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmVendedores_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub

    Private Sub frmVendedores_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub

    Private Sub frmVendedores_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub

    Private Sub frmVendedores_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oVendedores.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oVendedores.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oVendedores.Eliminar(clcVendedor.Value)

        End Select
    End Sub

    Private Sub frmVendedores_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oVendedores.DespliegaDatos(OwnerForm.Value("vendedor"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet


            Response = oVendedoresEsquemas.DespliegaDatos(Me.clcVendedor.EditValue)
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                Me.tmaEsquemas.DataSource = oDataSet
            End If
        End If

    End Sub

    Private Sub frmVendedores_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVendedores = New VillarrealBusiness.clsVendedores
        oVendedoresEsquemas = New VillarrealBusiness.clsVendedoresEsquemas
        oSucursales = New VillarrealBusiness.clsSucursales
        oEsquemasComisiones = New VillarrealBusiness.clsEsquemasComisiones

        ConfiguraMaster()
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmVendedores_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oVendedores.Validacion(Action, txtNombre.Text, clcSueldo.Value, Sucursal)
    End Sub

    Private Sub frmVendedores_Localize() Handles MyBase.Localize
        Find("vendedor", Me.clcVendedor.EditValue)

    End Sub

    Private Sub frmVendedores_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With tmaEsquemas
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oVendedoresEsquemas.Insertar(Me.clcVendedor.EditValue, .Item("esquema"), .Item("sucursal"))
                    
                    Case Actions.Delete
                        Response = oVendedoresEsquemas.Eliminar(Me.clcVendedor.EditValue, .Item("esquema"), .Item("sucursal"))
                End Select
                .MoveNext()
            Loop
        End With
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpEsquemasComisiones_Format() Handles lkpEsquemasComisiones.Format
        Comunes.clsFormato.for_esquemas_comisiones_grl(lkpEsquemasComisiones)
    End Sub

    Private Sub lkpEsquemasComisiones_LoadData(ByVal Initialize As Boolean) Handles lkpEsquemasComisiones.LoadData
        Dim Response As New Events
        Response = oEsquemasComisiones.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEsquemasComisiones.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub ConfiguraMaster()
        With tmaEsquemas
            .UpdateForm = New Comunes.frmVendedoresDetalles
            .UpdateTitle = "Un esquema"
            .AddColumn("sucursal", "System.Int32")
            .AddColumn("nombre_sucursal")
            .AddColumn("esquema", "System.Int32")
            .AddColumn("nombre_esquema")
        End With
    End Sub

#End Region




End Class
