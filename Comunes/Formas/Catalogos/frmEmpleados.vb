Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmEmpleados
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblEmpleado As System.Windows.Forms.Label
		Friend WithEvents clcEmpleado As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblNombre As System.Windows.Forms.Label
		Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
		Friend WithEvents chkBaja As DevExpress.XtraEditors.CheckEdit
    
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEmpleados))
        Me.lblEmpleado = New System.Windows.Forms.Label
        Me.clcEmpleado = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.chkBaja = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcEmpleado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkBaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(393, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblEmpleado
        '
        Me.lblEmpleado.AutoSize = True
        Me.lblEmpleado.Location = New System.Drawing.Point(10, 40)
        Me.lblEmpleado.Name = "lblEmpleado"
        Me.lblEmpleado.Size = New System.Drawing.Size(63, 16)
        Me.lblEmpleado.TabIndex = 0
        Me.lblEmpleado.Tag = ""
        Me.lblEmpleado.Text = "&Empleado:"
        Me.lblEmpleado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEmpleado
        '
        Me.clcEmpleado.EditValue = "0"
        Me.clcEmpleado.Location = New System.Drawing.Point(80, 40)
        Me.clcEmpleado.MaxValue = 0
        Me.clcEmpleado.MinValue = 0
        Me.clcEmpleado.Name = "clcEmpleado"
        '
        'clcEmpleado.Properties
        '
        Me.clcEmpleado.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcEmpleado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEmpleado.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcEmpleado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEmpleado.Properties.Enabled = False
        Me.clcEmpleado.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcEmpleado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEmpleado.Size = New System.Drawing.Size(56, 19)
        Me.clcEmpleado.TabIndex = 1
        Me.clcEmpleado.Tag = "empleado"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(20, 63)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(80, 63)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 60
        Me.txtNombre.Size = New System.Drawing.Size(360, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'chkBaja
        '
        Me.chkBaja.Location = New System.Drawing.Point(25, 86)
        Me.chkBaja.Name = "chkBaja"
        '
        'chkBaja.Properties
        '
        Me.chkBaja.Properties.Caption = "Baja"
        Me.chkBaja.Properties.Enabled = False
        Me.chkBaja.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkBaja.Size = New System.Drawing.Size(48, 19)
        Me.chkBaja.TabIndex = 4
        Me.chkBaja.Tag = "baja"
        Me.chkBaja.Visible = False
        '
        'frmEmpleados
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(444, 96)
        Me.Controls.Add(Me.lblEmpleado)
        Me.Controls.Add(Me.clcEmpleado)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.chkBaja)
        Me.Name = "frmEmpleados"
        Me.Controls.SetChildIndex(Me.chkBaja, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcEmpleado, 0)
        Me.Controls.SetChildIndex(Me.lblEmpleado, 0)
        CType(Me.clcEmpleado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkBaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oEmpleados As VillarrealBusiness.clsEmpleados
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmEmpleados_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub

    Private Sub frmEmpleados_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub

    Private Sub frmEmpleados_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub

    Private Sub frmEmpleados_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oEmpleados.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oEmpleados.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oEmpleados.Eliminar(clcEmpleado.Value)

        End Select
    End Sub

    Private Sub frmEmpleados_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oEmpleados.DespliegaDatos(OwnerForm.Value("empleado"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmEmpleados_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oEmpleados = New VillarrealBusiness.clsEmpleados

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmEmpleados_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oEmpleados.Validacion(Action, txtNombre.Text)
    End Sub

    Private Sub frmEmpleados_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub txtNombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oEmpleados.ValidaNombre(txtNombre.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
