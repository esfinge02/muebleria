Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmClientesDirecciones
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents txtLongitud As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLatitud As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpCiudad As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroExterior As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroInterior As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtycalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNotas As System.Windows.Forms.Label
    Friend WithEvents txtentrecalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lblCp As System.Windows.Forms.Label
    Friend WithEvents clcCp As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTelefono1 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono2 As System.Windows.Forms.Label
    Friend WithEvents txtNotas As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTipoDireccion As System.Windows.Forms.Label
    Friend WithEvents lblDireccionCompuesta As System.Windows.Forms.Label
    Friend WithEvents txtCelular As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpTipoDireccion As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmClientesDirecciones))
        Me.txtLongitud = New DevExpress.XtraEditors.TextEdit
        Me.txtLatitud = New DevExpress.XtraEditors.TextEdit
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.lkpCiudad = New Dipros.Editors.TINMultiLookup
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.lblEstado = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNumeroExterior = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtNumeroInterior = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtycalle = New DevExpress.XtraEditors.TextEdit
        Me.lblNotas = New System.Windows.Forms.Label
        Me.txtentrecalle = New DevExpress.XtraEditors.TextEdit
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lblCp = New System.Windows.Forms.Label
        Me.clcCp = New Dipros.Editors.TINCalcEdit
        Me.lblTelefono1 = New System.Windows.Forms.Label
        Me.txtTelefono1 = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono2 = New System.Windows.Forms.Label
        Me.txtCelular = New DevExpress.XtraEditors.TextEdit
        Me.txtNotas = New DevExpress.XtraEditors.MemoEdit
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.Label7 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblTipoDireccion = New System.Windows.Forms.Label
        Me.lblDireccionCompuesta = New System.Windows.Forms.Label
        Me.lkpTipoDireccion = New Dipros.Editors.TINMultiLookup
        Me.Label5 = New System.Windows.Forms.Label
        CType(Me.txtLongitud.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLatitud.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroExterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroInterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtycalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtentrecalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCelular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4460, 28)
        '
        'txtLongitud
        '
        Me.txtLongitud.EditValue = ""
        Me.txtLongitud.Location = New System.Drawing.Point(632, 192)
        Me.txtLongitud.Name = "txtLongitud"
        '
        'txtLongitud.Properties
        '
        Me.txtLongitud.Properties.Enabled = False
        Me.txtLongitud.Properties.MaxLength = 40
        Me.txtLongitud.Size = New System.Drawing.Size(64, 20)
        Me.txtLongitud.TabIndex = 26
        Me.txtLongitud.Tag = "longitud"
        Me.txtLongitud.Visible = False
        '
        'txtLatitud
        '
        Me.txtLatitud.EditValue = ""
        Me.txtLatitud.Location = New System.Drawing.Point(632, 168)
        Me.txtLatitud.Name = "txtLatitud"
        '
        'txtLatitud.Properties
        '
        Me.txtLatitud.Properties.Enabled = False
        Me.txtLatitud.Properties.MaxLength = 40
        Me.txtLatitud.Size = New System.Drawing.Size(64, 20)
        Me.txtLatitud.TabIndex = 25
        Me.txtLatitud.Tag = "latitud"
        Me.txtLatitud.Visible = False
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(80, 112)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.ReadOnlyControl = False
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(176, 20)
        Me.lkpEstado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEstado.TabIndex = 15
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'lkpCiudad
        '
        Me.lkpCiudad.AllowAdd = False
        Me.lkpCiudad.AutoReaload = True
        Me.lkpCiudad.DataSource = Nothing
        Me.lkpCiudad.DefaultSearchField = ""
        Me.lkpCiudad.DisplayMember = "descripcion"
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad.Filtered = False
        Me.lkpCiudad.InitValue = Nothing
        Me.lkpCiudad.Location = New System.Drawing.Point(584, 112)
        Me.lkpCiudad.MultiSelect = False
        Me.lkpCiudad.Name = "lkpCiudad"
        Me.lkpCiudad.NullText = ""
        Me.lkpCiudad.PopupWidth = CType(300, Long)
        Me.lkpCiudad.ReadOnlyControl = False
        Me.lkpCiudad.Required = False
        Me.lkpCiudad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCiudad.SearchMember = ""
        Me.lkpCiudad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCiudad.SelectAll = False
        Me.lkpCiudad.Size = New System.Drawing.Size(168, 20)
        Me.lkpCiudad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCiudad.TabIndex = 19
        Me.lkpCiudad.Tag = "ciudad"
        Me.lkpCiudad.ToolTip = Nothing
        Me.lkpCiudad.ValueMember = "ciudad"
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = True
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(336, 112)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(300, Long)
        Me.lkpMunicipio.ReadOnlyControl = False
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(168, 20)
        Me.lkpMunicipio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMunicipio.TabIndex = 17
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(520, 112)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(60, 16)
        Me.lblCiudad.TabIndex = 18
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "Locali&dad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(24, 112)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 14
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "E&stado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(480, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Tag = ""
        Me.Label3.Text = "# Ex&t.:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumeroExterior
        '
        Me.txtNumeroExterior.EditValue = "0"
        Me.txtNumeroExterior.Location = New System.Drawing.Point(528, 64)
        Me.txtNumeroExterior.Name = "txtNumeroExterior"
        '
        'txtNumeroExterior.Properties
        '
        Me.txtNumeroExterior.Properties.MaxLength = 10
        Me.txtNumeroExterior.Size = New System.Drawing.Size(72, 20)
        Me.txtNumeroExterior.TabIndex = 5
        Me.txtNumeroExterior.Tag = "numero_exterior"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(608, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Tag = ""
        Me.Label4.Text = "# Interior:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumeroInterior
        '
        Me.txtNumeroInterior.EditValue = ""
        Me.txtNumeroInterior.Location = New System.Drawing.Point(680, 64)
        Me.txtNumeroInterior.Name = "txtNumeroInterior"
        '
        'txtNumeroInterior.Properties
        '
        Me.txtNumeroInterior.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroInterior.Properties.MaxLength = 10
        Me.txtNumeroInterior.Size = New System.Drawing.Size(72, 20)
        Me.txtNumeroInterior.TabIndex = 7
        Me.txtNumeroInterior.Tag = "numero_interior"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "Callle :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(32, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(39, 16)
        Me.Label10.TabIndex = 8
        Me.Label10.Tag = ""
        Me.Label10.Text = "Entre:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(304, 88)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(11, 16)
        Me.Label11.TabIndex = 10
        Me.Label11.Tag = ""
        Me.Label11.Text = "y"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtycalle
        '
        Me.txtycalle.EditValue = ""
        Me.txtycalle.Location = New System.Drawing.Point(336, 88)
        Me.txtycalle.Name = "txtycalle"
        '
        'txtycalle.Properties
        '
        Me.txtycalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtycalle.Properties.MaxLength = 30
        Me.txtycalle.Size = New System.Drawing.Size(200, 20)
        Me.txtycalle.TabIndex = 11
        Me.txtycalle.Tag = "ycalle"
        '
        'lblNotas
        '
        Me.lblNotas.AutoSize = True
        Me.lblNotas.Location = New System.Drawing.Point(8, 216)
        Me.lblNotas.Name = "lblNotas"
        Me.lblNotas.Size = New System.Drawing.Size(93, 16)
        Me.lblNotas.TabIndex = 23
        Me.lblNotas.Tag = ""
        Me.lblNotas.Text = "Observaciones :"
        Me.lblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtentrecalle
        '
        Me.txtentrecalle.EditValue = ""
        Me.txtentrecalle.Location = New System.Drawing.Point(80, 88)
        Me.txtentrecalle.Name = "txtentrecalle"
        '
        'txtentrecalle.Properties
        '
        Me.txtentrecalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtentrecalle.Properties.MaxLength = 30
        Me.txtentrecalle.Size = New System.Drawing.Size(208, 20)
        Me.txtentrecalle.TabIndex = 9
        Me.txtentrecalle.Tag = "entrecalle"
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(80, 64)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDomicilio.Properties.MaxLength = 50
        Me.txtDomicilio.Size = New System.Drawing.Size(374, 20)
        Me.txtDomicilio.TabIndex = 3
        Me.txtDomicilio.Tag = "domicilio"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(24, 136)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 20
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "C&olonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCp
        '
        Me.lblCp.AutoSize = True
        Me.lblCp.Location = New System.Drawing.Point(632, 88)
        Me.lblCp.Name = "lblCp"
        Me.lblCp.Size = New System.Drawing.Size(39, 16)
        Me.lblCp.TabIndex = 12
        Me.lblCp.Tag = ""
        Me.lblCp.Text = "C. P. &:"
        Me.lblCp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCp
        '
        Me.clcCp.EditValue = "0"
        Me.clcCp.Location = New System.Drawing.Point(680, 88)
        Me.clcCp.MaxValue = 0
        Me.clcCp.MinValue = 0
        Me.clcCp.Name = "clcCp"
        '
        'clcCp.Properties
        '
        Me.clcCp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.MaskData.EditMask = "#"
        Me.clcCp.Properties.MaxLength = 5
        Me.clcCp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCp.Size = New System.Drawing.Size(72, 19)
        Me.clcCp.TabIndex = 13
        Me.clcCp.Tag = "cp"
        '
        'lblTelefono1
        '
        Me.lblTelefono1.AutoSize = True
        Me.lblTelefono1.Location = New System.Drawing.Point(32, 16)
        Me.lblTelefono1.Name = "lblTelefono1"
        Me.lblTelefono1.Size = New System.Drawing.Size(36, 16)
        Me.lblTelefono1.TabIndex = 0
        Me.lblTelefono1.Tag = ""
        Me.lblTelefono1.Text = "Casa:"
        Me.lblTelefono1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono1
        '
        Me.txtTelefono1.EditValue = ""
        Me.txtTelefono1.Location = New System.Drawing.Point(72, 16)
        Me.txtTelefono1.Name = "txtTelefono1"
        '
        'txtTelefono1.Properties
        '
        Me.txtTelefono1.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelefono1.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelefono1.Properties.MaxLength = 13
        Me.txtTelefono1.Size = New System.Drawing.Size(96, 20)
        Me.txtTelefono1.TabIndex = 1
        Me.txtTelefono1.Tag = "telefono_casa"
        '
        'lblTelefono2
        '
        Me.lblTelefono2.AutoSize = True
        Me.lblTelefono2.Location = New System.Drawing.Point(192, 16)
        Me.lblTelefono2.Name = "lblTelefono2"
        Me.lblTelefono2.Size = New System.Drawing.Size(101, 16)
        Me.lblTelefono2.TabIndex = 2
        Me.lblTelefono2.Tag = ""
        Me.lblTelefono2.Text = "Celular o Nextel :"
        Me.lblTelefono2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCelular
        '
        Me.txtCelular.EditValue = ""
        Me.txtCelular.Location = New System.Drawing.Point(296, 16)
        Me.txtCelular.Name = "txtCelular"
        '
        'txtCelular.Properties
        '
        Me.txtCelular.Properties.MaskData.EditMask = "(999)000-00-00"
        Me.txtCelular.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtCelular.Properties.MaxLength = 13
        Me.txtCelular.Size = New System.Drawing.Size(96, 20)
        Me.txtCelular.TabIndex = 3
        Me.txtCelular.Tag = "telefono_celular"
        '
        'txtNotas
        '
        Me.txtNotas.EditValue = ""
        Me.txtNotas.Location = New System.Drawing.Point(104, 216)
        Me.txtNotas.Name = "txtNotas"
        '
        'txtNotas.Properties
        '
        Me.txtNotas.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNotas.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtNotas.Size = New System.Drawing.Size(648, 43)
        Me.txtNotas.TabIndex = 24
        Me.txtNotas.Tag = "observaciones"
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = True
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(80, 136)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(360, Long)
        Me.lkpColonia.ReadOnlyControl = False
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(424, 20)
        Me.lkpColonia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpColonia.TabIndex = 21
        Me.lkpColonia.Tag = "colonia"
        Me.lkpColonia.ToolTip = Nothing
        Me.lkpColonia.ValueMember = "colonia"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(272, 112)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 16)
        Me.Label7.TabIndex = 16
        Me.Label7.Tag = ""
        Me.Label7.Text = "Municipio:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblTelefono1)
        Me.GroupBox1.Controls.Add(Me.txtTelefono1)
        Me.GroupBox1.Controls.Add(Me.lblTelefono2)
        Me.GroupBox1.Controls.Add(Me.txtCelular)
        Me.GroupBox1.Location = New System.Drawing.Point(80, 160)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(424, 48)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Telefonos:"
        '
        'lblTipoDireccion
        '
        Me.lblTipoDireccion.Location = New System.Drawing.Point(320, 32)
        Me.lblTipoDireccion.Name = "lblTipoDireccion"
        Me.lblTipoDireccion.TabIndex = 27
        Me.lblTipoDireccion.Tag = "descripcion_direccion"
        Me.lblTipoDireccion.Visible = False
        '
        'lblDireccionCompuesta
        '
        Me.lblDireccionCompuesta.Location = New System.Drawing.Point(440, 32)
        Me.lblDireccionCompuesta.Name = "lblDireccionCompuesta"
        Me.lblDireccionCompuesta.Size = New System.Drawing.Size(288, 23)
        Me.lblDireccionCompuesta.TabIndex = 28
        Me.lblDireccionCompuesta.Tag = "direccion"
        Me.lblDireccionCompuesta.Visible = False
        '
        'lkpTipoDireccion
        '
        Me.lkpTipoDireccion.AllowAdd = False
        Me.lkpTipoDireccion.AutoReaload = False
        Me.lkpTipoDireccion.DataSource = Nothing
        Me.lkpTipoDireccion.DefaultSearchField = ""
        Me.lkpTipoDireccion.DisplayMember = "descripcion"
        Me.lkpTipoDireccion.EditValue = Nothing
        Me.lkpTipoDireccion.Filtered = False
        Me.lkpTipoDireccion.InitValue = Nothing
        Me.lkpTipoDireccion.Location = New System.Drawing.Point(80, 40)
        Me.lkpTipoDireccion.MultiSelect = False
        Me.lkpTipoDireccion.Name = "lkpTipoDireccion"
        Me.lkpTipoDireccion.NullText = ""
        Me.lkpTipoDireccion.PopupWidth = CType(400, Long)
        Me.lkpTipoDireccion.ReadOnlyControl = False
        Me.lkpTipoDireccion.Required = False
        Me.lkpTipoDireccion.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTipoDireccion.SearchMember = ""
        Me.lkpTipoDireccion.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTipoDireccion.SelectAll = False
        Me.lkpTipoDireccion.Size = New System.Drawing.Size(208, 20)
        Me.lkpTipoDireccion.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpTipoDireccion.TabIndex = 1
        Me.lkpTipoDireccion.Tag = "tipo_direccion"
        Me.lkpTipoDireccion.ToolTip = Nothing
        Me.lkpTipoDireccion.ValueMember = "tipo_direccion"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(43, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Tag = ""
        Me.Label5.Text = "Tipo:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmClientesDirecciones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(762, 268)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lkpTipoDireccion)
        Me.Controls.Add(Me.lblDireccionCompuesta)
        Me.Controls.Add(Me.lblTipoDireccion)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtLongitud)
        Me.Controls.Add(Me.txtLatitud)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lkpCiudad)
        Me.Controls.Add(Me.lkpMunicipio)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtNumeroExterior)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtNumeroInterior)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtycalle)
        Me.Controls.Add(Me.lblNotas)
        Me.Controls.Add(Me.txtentrecalle)
        Me.Controls.Add(Me.txtDomicilio)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.lblCp)
        Me.Controls.Add(Me.clcCp)
        Me.Controls.Add(Me.txtNotas)
        Me.Controls.Add(Me.lkpColonia)
        Me.Controls.Add(Me.Label7)
        Me.Name = "frmClientesDirecciones"
        Me.Text = "frmClientesDirecciones"
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.lkpColonia, 0)
        Me.Controls.SetChildIndex(Me.txtNotas, 0)
        Me.Controls.SetChildIndex(Me.clcCp, 0)
        Me.Controls.SetChildIndex(Me.lblCp, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.txtDomicilio, 0)
        Me.Controls.SetChildIndex(Me.txtentrecalle, 0)
        Me.Controls.SetChildIndex(Me.lblNotas, 0)
        Me.Controls.SetChildIndex(Me.txtycalle, 0)
        Me.Controls.SetChildIndex(Me.Label11, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtNumeroInterior, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.txtNumeroExterior, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.lblCiudad, 0)
        Me.Controls.SetChildIndex(Me.lkpMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lkpCiudad, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        Me.Controls.SetChildIndex(Me.txtLatitud, 0)
        Me.Controls.SetChildIndex(Me.txtLongitud, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.lblTipoDireccion, 0)
        Me.Controls.SetChildIndex(Me.lblDireccionCompuesta, 0)
        Me.Controls.SetChildIndex(Me.lkpTipoDireccion, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        CType(Me.txtLongitud.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLatitud.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroExterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroInterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtycalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtentrecalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCelular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oClientesDirecciones As VillarrealBusiness.clsClientesDirecciones
    Private oColonias As New VillarrealBusiness.clsColonias
    Private oEstados As VillarrealBusiness.clsEstados
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oCiudades As VillarrealBusiness.clsCiudades
    Private oTiposDireccion As VillarrealBusiness.clsTiposDirecciones

    Private ReadOnly Property TipoDireccion() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpTipoDireccion)
        End Get
    End Property
    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property
    Private ReadOnly Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCiudad)
        End Get
    End Property
    Private ReadOnly Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
    End Property
    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub frmClientesDirecciones_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oEstados = New VillarrealBusiness.clsEstados
        oMunicipios = New VillarrealBusiness.clsMunicipios
        oCiudades = New VillarrealBusiness.clsCiudades
        oColonias = New VillarrealBusiness.clsColonias
        oClientesDirecciones = New VillarrealBusiness.clsClientesDirecciones
        oTiposDireccion = New VillarrealBusiness.clsTiposDirecciones
    End Sub

    Private Sub frmClientesDirecciones_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim odataset As DataSet = OwnerForm.MasterControl.SelectedRow

        Me.DataSource = OwnerForm.MasterControl.SelectedRow

        Me.lkpCiudad.EditValue = IIf(IsDBNull(odataset.Tables(0).Rows(0).Item("ciudad")), 0, odataset.Tables(0).Rows(0).Item("ciudad"))
        Me.lkpColonia.EditValue = IIf(IsDBNull(odataset.Tables(0).Rows(0).Item("colonia")), 0, odataset.Tables(0).Rows(0).Item("colonia"))

        Select Case Action
            Case Actions.Update
                Me.lkpTipoDireccion.Enabled = False

            Case Actions.Delete
                Me.tbrTools.Buttons(0).Visible = True
        End Select
    End Sub

    Private Sub frmClientesDirecciones_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Me.lblDireccionCompuesta.Text = Me.txtDomicilio.Text + " " + Me.txtNumeroExterior.Text + " Col. " + Me.lkpColonia.Text
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select

        End With
    End Sub

    Private Sub frmClientesDirecciones_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields


     

        Response = oClientesDirecciones.Validacion(1, Me.TipoDireccion, Me.txtDomicilio.Text, Me.txtNumeroExterior.Text, Me.txtNumeroInterior.Text, _
                           Me.txtentrecalle.Text, Me.txtycalle.Text, Me.clcCp.Value, Me.Colonia, Me.Estado, Me.Municipio, Me.Ciudad, _
                           Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelefono1.Text), Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtCelular.Text), _
                           Me.txtNotas.Text)

        If Response.ErrorFound Then Exit Sub

        If Me.Action = Actions.Insert Then
            Response = ValidaDirecciones(CType(OwnerForm.MasterControl.DataSource, DataSet), Me.TipoDireccion, lblTipoDireccion.Text)
        End If
     
    End Sub



    Public Function ValidaDirecciones(ByVal odata As DataSet, ByVal TipoDireccion As String, ByVal descripcion_direccion As String) As Events
        Dim oevent As New Events
        If odata.Tables(0).Select("tipo_direccion = " & TipoDireccion & " and control in(0,1,2)").Length >= 1 Then
            oevent.Ex = Nothing
            oevent.Message = "Ya tiene asignada una direcci�n para el Tipo:  " & descripcion_direccion
            Return oevent
        End If

        Return oevent
    End Function

#End Region

#Region "Eventos de los Controles"
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub
    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData
        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub


    Private Sub lkpMunicipio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        '        If Entro_Despliega = True Then Exit Sub
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad_LoadData(True)
    End Sub
    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData
        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCiudad_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCiudad.EditValueChanged
        If Me.lkpCiudad.EditValue Is Nothing Then Exit Sub

        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia_LoadData(True)
    End Sub
    Private Sub lkpCiudad_Format() Handles lkpCiudad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpCiudad)
    End Sub
    Private Sub lkpCiudad_LoadData(ByVal Initialize As Boolean) Handles lkpCiudad.LoadData
        Dim Response As New Events
        Response = oCiudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCiudad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpColonia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpColonia.EditValueChanged
        clcCp.EditValue = lkpColonia.GetValue("codigo_postal")
    End Sub
    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub


    Private Sub lkpTipoDireccion_LoadData(ByVal Initialize As Boolean) Handles lkpTipoDireccion.LoadData
        Dim Response As New Events
        Response = oTiposDireccion.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpTipoDireccion.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpTipoDireccion_Format() Handles lkpTipoDireccion.Format
        Comunes.clsFormato.for_tipos_direccion_grl(lkpTipoDireccion)
    End Sub
    Private Sub lkpTipoDireccion_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpTipoDireccion.EditValueChanged
        If Me.TipoDireccion > 0 Then
            Me.lblTipoDireccion.Text = Me.lkpTipoDireccion.Text

        End If
    End Sub


    Private Sub txtNumeroExterior_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumeroExterior.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
#End Region



  
  

  
End Class
