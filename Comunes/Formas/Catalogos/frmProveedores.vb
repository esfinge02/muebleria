Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmProveedores
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblNotas As System.Windows.Forms.Label
    Friend WithEvents txtNotas As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grCuentasBancarias As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvOrdenesCompra As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaCuentasBancarias As Dipros.Windows.TINMaster
    Friend WithEvents grcBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPlaza As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtEstado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents txtColonia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents clcProveedor As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents lblRfc As System.Windows.Forms.Label
    Friend WithEvents txtRfc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents txtCiudad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCp As System.Windows.Forms.Label
    Friend WithEvents clcCp As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTelefonos As System.Windows.Forms.Label
    Friend WithEvents txtTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblContactos As System.Windows.Forms.Label
    Friend WithEvents txtContactos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tmaDescuentosProveedores As Dipros.Windows.TINMaster
    Friend WithEvents grDescuentosProveedores As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDescuentosProveedor As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClaveDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPorcentaje As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAntesIVA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcProntoPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents pnDatosGenerales As System.Windows.Forms.Panel
    Friend WithEvents pnCuentasBancarias As System.Windows.Forms.Panel
    Friend WithEvents pnDescuentos As System.Windows.Forms.Panel
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NvDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NvCuentasBancarias As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NvDescuentos As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents clcSaldo As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblPersona As System.Windows.Forms.Label
    Friend WithEvents cboPersona As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblCurp As System.Windows.Forms.Label
    Friend WithEvents txtCurp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmProveedores))
        Me.lblNotas = New System.Windows.Forms.Label
        Me.txtNotas = New DevExpress.XtraEditors.MemoEdit
        Me.grCuentasBancarias = New DevExpress.XtraGrid.GridControl
        Me.grvOrdenesCompra = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcBanco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreBanco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPlaza = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaCuentasBancarias = New Dipros.Windows.TINMaster
        Me.pnDatosGenerales = New System.Windows.Forms.Panel
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.chkActivo = New DevExpress.XtraEditors.CheckEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboTipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblCurp = New System.Windows.Forms.Label
        Me.txtCurp = New DevExpress.XtraEditors.TextEdit
        Me.lblPersona = New System.Windows.Forms.Label
        Me.cboPersona = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcSaldo = New DevExpress.XtraEditors.CalcEdit
        Me.txtCuenta = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtEstado = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia = New System.Windows.Forms.Label
        Me.txtColonia = New DevExpress.XtraEditors.TextEdit
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.clcProveedor = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblCuenta = New System.Windows.Forms.Label
        Me.lblRfc = New System.Windows.Forms.Label
        Me.txtRfc = New DevExpress.XtraEditors.TextEdit
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.txtCiudad = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblCp = New System.Windows.Forms.Label
        Me.clcCp = New Dipros.Editors.TINCalcEdit
        Me.lblTelefonos = New System.Windows.Forms.Label
        Me.txtTelefonos = New DevExpress.XtraEditors.TextEdit
        Me.lblContactos = New System.Windows.Forms.Label
        Me.txtContactos = New DevExpress.XtraEditors.TextEdit
        Me.pnCuentasBancarias = New System.Windows.Forms.Panel
        Me.pnDescuentos = New System.Windows.Forms.Panel
        Me.tmaDescuentosProveedores = New Dipros.Windows.TINMaster
        Me.grDescuentosProveedores = New DevExpress.XtraGrid.GridControl
        Me.grvDescuentosProveedor = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPorcentaje = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAntesIVA = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcProntoPago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.NvDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.NvCuentasBancarias = New DevExpress.XtraNavBar.NavBarItem
        Me.NvDescuentos = New DevExpress.XtraNavBar.NavBarItem
        Me.Label5 = New System.Windows.Forms.Label
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grCuentasBancarias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnDatosGenerales.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboPersona.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRfc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnCuentasBancarias.SuspendLayout()
        Me.pnDescuentos.SuspendLayout()
        CType(Me.grDescuentosProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDescuentosProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(8054, 28)
        '
        'lblNotas
        '
        Me.lblNotas.AutoSize = True
        Me.lblNotas.Location = New System.Drawing.Point(232, 344)
        Me.lblNotas.Name = "lblNotas"
        Me.lblNotas.Size = New System.Drawing.Size(41, 16)
        Me.lblNotas.TabIndex = 22
        Me.lblNotas.Tag = ""
        Me.lblNotas.Text = "No&tas:"
        Me.lblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNotas
        '
        Me.txtNotas.EditValue = ""
        Me.txtNotas.Location = New System.Drawing.Point(280, 344)
        Me.txtNotas.Name = "txtNotas"
        Me.txtNotas.Size = New System.Drawing.Size(490, 38)
        Me.txtNotas.TabIndex = 23
        Me.txtNotas.Tag = "notas"
        '
        'grCuentasBancarias
        '
        '
        'grCuentasBancarias.EmbeddedNavigator
        '
        Me.grCuentasBancarias.EmbeddedNavigator.Name = ""
        Me.grCuentasBancarias.Location = New System.Drawing.Point(0, 24)
        Me.grCuentasBancarias.MainView = Me.grvOrdenesCompra
        Me.grCuentasBancarias.Name = "grCuentasBancarias"
        Me.grCuentasBancarias.Size = New System.Drawing.Size(640, 264)
        Me.grCuentasBancarias.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grCuentasBancarias.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCuentasBancarias.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCuentasBancarias.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCuentasBancarias.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCuentasBancarias.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCuentasBancarias.TabIndex = 26
        Me.grCuentasBancarias.TabStop = False
        Me.grCuentasBancarias.Text = "Cuentas Bancarias"
        '
        'grvOrdenesCompra
        '
        Me.grvOrdenesCompra.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcBanco, Me.grcNombreBanco, Me.grcSucursal, Me.grcPlaza, Me.grcCuenta})
        Me.grvOrdenesCompra.GridControl = Me.grCuentasBancarias
        Me.grvOrdenesCompra.Name = "grvOrdenesCompra"
        Me.grvOrdenesCompra.OptionsBehavior.Editable = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowFilter = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowGroup = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowSort = False
        Me.grvOrdenesCompra.OptionsView.ShowGroupPanel = False
        '
        'grcBanco
        '
        Me.grcBanco.Caption = "Banco"
        Me.grcBanco.FieldName = "banco"
        Me.grcBanco.Name = "grcBanco"
        Me.grcBanco.Width = 89
        '
        'grcNombreBanco
        '
        Me.grcNombreBanco.Caption = "Banco"
        Me.grcNombreBanco.FieldName = "nombre"
        Me.grcNombreBanco.Name = "grcNombreBanco"
        Me.grcNombreBanco.VisibleIndex = 0
        Me.grcNombreBanco.Width = 147
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.VisibleIndex = 1
        Me.grcSucursal.Width = 123
        '
        'grcPlaza
        '
        Me.grcPlaza.Caption = "Plaza"
        Me.grcPlaza.FieldName = "plaza"
        Me.grcPlaza.Name = "grcPlaza"
        Me.grcPlaza.VisibleIndex = 2
        Me.grcPlaza.Width = 142
        '
        'grcCuenta
        '
        Me.grcCuenta.Caption = "Cuenta"
        Me.grcCuenta.FieldName = "cuenta"
        Me.grcCuenta.Name = "grcCuenta"
        Me.grcCuenta.VisibleIndex = 3
        Me.grcCuenta.Width = 150
        '
        'tmaCuentasBancarias
        '
        Me.tmaCuentasBancarias.BackColor = System.Drawing.Color.White
        Me.tmaCuentasBancarias.CanDelete = True
        Me.tmaCuentasBancarias.CanInsert = True
        Me.tmaCuentasBancarias.CanUpdate = True
        Me.tmaCuentasBancarias.Dock = System.Windows.Forms.DockStyle.Top
        Me.tmaCuentasBancarias.Grid = Me.grCuentasBancarias
        Me.tmaCuentasBancarias.Location = New System.Drawing.Point(0, 0)
        Me.tmaCuentasBancarias.Name = "tmaCuentasBancarias"
        Me.tmaCuentasBancarias.Size = New System.Drawing.Size(640, 23)
        Me.tmaCuentasBancarias.TabIndex = 0
        Me.tmaCuentasBancarias.TabStop = False
        Me.tmaCuentasBancarias.Title = "Cuentas Bancarias"
        Me.tmaCuentasBancarias.UpdateTitle = "un Registro"
        '
        'pnDatosGenerales
        '
        Me.pnDatosGenerales.Controls.Add(Me.TextEdit1)
        Me.pnDatosGenerales.Controls.Add(Me.chkActivo)
        Me.pnDatosGenerales.Controls.Add(Me.Label4)
        Me.pnDatosGenerales.Controls.Add(Me.cboTipo)
        Me.pnDatosGenerales.Controls.Add(Me.lblCurp)
        Me.pnDatosGenerales.Controls.Add(Me.txtCurp)
        Me.pnDatosGenerales.Controls.Add(Me.lblPersona)
        Me.pnDatosGenerales.Controls.Add(Me.cboPersona)
        Me.pnDatosGenerales.Controls.Add(Me.Label3)
        Me.pnDatosGenerales.Controls.Add(Me.clcSaldo)
        Me.pnDatosGenerales.Controls.Add(Me.txtCuenta)
        Me.pnDatosGenerales.Controls.Add(Me.Label2)
        Me.pnDatosGenerales.Controls.Add(Me.txtEstado)
        Me.pnDatosGenerales.Controls.Add(Me.lblColonia)
        Me.pnDatosGenerales.Controls.Add(Me.txtColonia)
        Me.pnDatosGenerales.Controls.Add(Me.lblProveedor)
        Me.pnDatosGenerales.Controls.Add(Me.clcProveedor)
        Me.pnDatosGenerales.Controls.Add(Me.lblNombre)
        Me.pnDatosGenerales.Controls.Add(Me.txtNombre)
        Me.pnDatosGenerales.Controls.Add(Me.lblCuenta)
        Me.pnDatosGenerales.Controls.Add(Me.lblRfc)
        Me.pnDatosGenerales.Controls.Add(Me.txtRfc)
        Me.pnDatosGenerales.Controls.Add(Me.lblCiudad)
        Me.pnDatosGenerales.Controls.Add(Me.txtCiudad)
        Me.pnDatosGenerales.Controls.Add(Me.lblDomicilio)
        Me.pnDatosGenerales.Controls.Add(Me.txtDomicilio)
        Me.pnDatosGenerales.Controls.Add(Me.lblCp)
        Me.pnDatosGenerales.Controls.Add(Me.clcCp)
        Me.pnDatosGenerales.Controls.Add(Me.lblTelefonos)
        Me.pnDatosGenerales.Controls.Add(Me.txtTelefonos)
        Me.pnDatosGenerales.Controls.Add(Me.lblContactos)
        Me.pnDatosGenerales.Controls.Add(Me.txtContactos)
        Me.pnDatosGenerales.Controls.Add(Me.Label5)
        Me.pnDatosGenerales.Location = New System.Drawing.Point(152, 32)
        Me.pnDatosGenerales.Name = "pnDatosGenerales"
        Me.pnDatosGenerales.Size = New System.Drawing.Size(640, 296)
        Me.pnDatosGenerales.TabIndex = 0
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = ""
        Me.TextEdit1.Location = New System.Drawing.Point(512, 112)
        Me.TextEdit1.Name = "TextEdit1"
        '
        'TextEdit1.Properties
        '
        Me.TextEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit1.Properties.MaxLength = 10
        Me.TextEdit1.Size = New System.Drawing.Size(112, 20)
        Me.TextEdit1.TabIndex = 16
        Me.TextEdit1.Tag = "Alias"
        '
        'chkActivo
        '
        Me.chkActivo.EditValue = True
        Me.chkActivo.Location = New System.Drawing.Point(520, 240)
        Me.chkActivo.Name = "chkActivo"
        '
        'chkActivo.Properties
        '
        Me.chkActivo.Properties.Caption = "Activo"
        Me.chkActivo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.chkActivo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkActivo.Size = New System.Drawing.Size(104, 19)
        Me.chkActivo.TabIndex = 26
        Me.chkActivo.Tag = "activo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(475, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Tag = ""
        Me.Label4.Text = "Tipo:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = "N"
        Me.cboTipo.Location = New System.Drawing.Point(512, 64)
        Me.cboTipo.Name = "cboTipo"
        '
        'cboTipo.Properties
        '
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nacional", "N", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Importaci�n", "I", -1)})
        Me.cboTipo.Size = New System.Drawing.Size(112, 20)
        Me.cboTipo.TabIndex = 9
        Me.cboTipo.Tag = "tipo"
        '
        'lblCurp
        '
        Me.lblCurp.AutoSize = True
        Me.lblCurp.Location = New System.Drawing.Point(472, 90)
        Me.lblCurp.Name = "lblCurp"
        Me.lblCurp.Size = New System.Drawing.Size(35, 16)
        Me.lblCurp.TabIndex = 12
        Me.lblCurp.Tag = ""
        Me.lblCurp.Text = "C&urp:"
        Me.lblCurp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCurp
        '
        Me.txtCurp.EditValue = ""
        Me.txtCurp.Location = New System.Drawing.Point(512, 88)
        Me.txtCurp.Name = "txtCurp"
        '
        'txtCurp.Properties
        '
        Me.txtCurp.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCurp.Properties.MaxLength = 20
        Me.txtCurp.Size = New System.Drawing.Size(112, 20)
        Me.txtCurp.TabIndex = 13
        Me.txtCurp.Tag = "curp"
        '
        'lblPersona
        '
        Me.lblPersona.AutoSize = True
        Me.lblPersona.Location = New System.Drawing.Point(456, 16)
        Me.lblPersona.Name = "lblPersona"
        Me.lblPersona.Size = New System.Drawing.Size(53, 16)
        Me.lblPersona.TabIndex = 2
        Me.lblPersona.Tag = ""
        Me.lblPersona.Text = "P&ersona:"
        Me.lblPersona.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPersona
        '
        Me.cboPersona.EditValue = "F"
        Me.cboPersona.Location = New System.Drawing.Point(512, 16)
        Me.cboPersona.Name = "cboPersona"
        '
        'cboPersona.Properties
        '
        Me.cboPersona.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPersona.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("F�sica", "F", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Moral", "M", -1)})
        Me.cboPersona.Size = New System.Drawing.Size(112, 20)
        Me.cboPersona.TabIndex = 3
        Me.cboPersona.Tag = "persona"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(472, 265)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 16)
        Me.Label3.TabIndex = 29
        Me.Label3.Tag = ""
        Me.Label3.Text = "SA&LDO:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldo.Location = New System.Drawing.Point(520, 264)
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatString = "c2"
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.clcSaldo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSaldo.Size = New System.Drawing.Size(104, 20)
        Me.clcSaldo.TabIndex = 30
        Me.clcSaldo.Tag = "saldo"
        '
        'txtCuenta
        '
        Me.txtCuenta.EditValue = ""
        Me.txtCuenta.Location = New System.Drawing.Point(137, 65)
        Me.txtCuenta.Name = "txtCuenta"
        '
        'txtCuenta.Properties
        '
        Me.txtCuenta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuenta.Properties.MaxLength = 30
        Me.txtCuenta.Size = New System.Drawing.Size(194, 20)
        Me.txtCuenta.TabIndex = 7
        Me.txtCuenta.Tag = "cuenta"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(87, 190)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 16)
        Me.Label2.TabIndex = 20
        Me.Label2.Tag = ""
        Me.Label2.Text = "E&stado:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstado
        '
        Me.txtEstado.EditValue = ""
        Me.txtEstado.Location = New System.Drawing.Point(137, 190)
        Me.txtEstado.Name = "txtEstado"
        '
        'txtEstado.Properties
        '
        Me.txtEstado.Properties.MaxLength = 50
        Me.txtEstado.Size = New System.Drawing.Size(300, 20)
        Me.txtEstado.TabIndex = 21
        Me.txtEstado.Tag = "estado"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(84, 142)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 16
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "Colon&ia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColonia
        '
        Me.txtColonia.EditValue = ""
        Me.txtColonia.Location = New System.Drawing.Point(137, 140)
        Me.txtColonia.Name = "txtColonia"
        '
        'txtColonia.Properties
        '
        Me.txtColonia.Properties.MaxLength = 30
        Me.txtColonia.Size = New System.Drawing.Size(300, 20)
        Me.txtColonia.TabIndex = 17
        Me.txtColonia.Tag = "colonia"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(68, 17)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 0
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "Pr&oveedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcProveedor
        '
        Me.clcProveedor.EditValue = "0"
        Me.clcProveedor.Location = New System.Drawing.Point(137, 17)
        Me.clcProveedor.MaxValue = 0
        Me.clcProveedor.MinValue = 0
        Me.clcProveedor.Name = "clcProveedor"
        '
        'clcProveedor.Properties
        '
        Me.clcProveedor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcProveedor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcProveedor.Properties.Enabled = False
        Me.clcProveedor.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcProveedor.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcProveedor.Size = New System.Drawing.Size(58, 19)
        Me.clcProveedor.TabIndex = 1
        Me.clcProveedor.Tag = "proveedor"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(80, 41)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 4
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "N&ombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(137, 41)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 100
        Me.txtNombre.Size = New System.Drawing.Size(490, 20)
        Me.txtNombre.TabIndex = 5
        Me.txtNombre.Tag = "nombre"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(85, 65)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(48, 16)
        Me.lblCuenta.TabIndex = 6
        Me.lblCuenta.Tag = ""
        Me.lblCuenta.Text = "Cue&nta:"
        Me.lblCuenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRfc
        '
        Me.lblRfc.AutoSize = True
        Me.lblRfc.Location = New System.Drawing.Point(102, 90)
        Me.lblRfc.Name = "lblRfc"
        Me.lblRfc.Size = New System.Drawing.Size(31, 16)
        Me.lblRfc.TabIndex = 10
        Me.lblRfc.Tag = ""
        Me.lblRfc.Text = "R&FC:"
        Me.lblRfc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRfc
        '
        Me.txtRfc.EditValue = ""
        Me.txtRfc.Location = New System.Drawing.Point(137, 90)
        Me.txtRfc.Name = "txtRfc"
        '
        'txtRfc.Properties
        '
        Me.txtRfc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRfc.Properties.MaxLength = 20
        Me.txtRfc.Size = New System.Drawing.Size(194, 20)
        Me.txtRfc.TabIndex = 11
        Me.txtRfc.Tag = "rfc"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(86, 166)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(47, 16)
        Me.lblCiudad.TabIndex = 18
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "Ciu&dad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudad
        '
        Me.txtCiudad.EditValue = ""
        Me.txtCiudad.Location = New System.Drawing.Point(137, 165)
        Me.txtCiudad.Name = "txtCiudad"
        '
        'txtCiudad.Properties
        '
        Me.txtCiudad.Properties.MaxLength = 50
        Me.txtCiudad.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudad.TabIndex = 19
        Me.txtCiudad.Tag = "ciudad"
        '
        'lblDomicilio
        '
        Me.lblDomicilio.AutoSize = True
        Me.lblDomicilio.Location = New System.Drawing.Point(74, 115)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio.TabIndex = 14
        Me.lblDomicilio.Tag = ""
        Me.lblDomicilio.Text = "Do&micilio:"
        Me.lblDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(137, 115)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.MaxLength = 50
        Me.txtDomicilio.Size = New System.Drawing.Size(300, 20)
        Me.txtDomicilio.TabIndex = 15
        Me.txtDomicilio.Tag = "domicilio"
        '
        'lblCp
        '
        Me.lblCp.AutoSize = True
        Me.lblCp.Location = New System.Drawing.Point(49, 217)
        Me.lblCp.Name = "lblCp"
        Me.lblCp.Size = New System.Drawing.Size(84, 16)
        Me.lblCp.TabIndex = 22
        Me.lblCp.Tag = ""
        Me.lblCp.Text = "C�di&go Postal:"
        Me.lblCp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCp
        '
        Me.clcCp.EditValue = "0"
        Me.clcCp.Location = New System.Drawing.Point(137, 215)
        Me.clcCp.MaxValue = 0
        Me.clcCp.MinValue = 0
        Me.clcCp.Name = "clcCp"
        '
        'clcCp.Properties
        '
        Me.clcCp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCp.Properties.MaxLength = 5
        Me.clcCp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCp.Size = New System.Drawing.Size(82, 19)
        Me.clcCp.TabIndex = 23
        Me.clcCp.Tag = "cp"
        '
        'lblTelefonos
        '
        Me.lblTelefonos.AutoSize = True
        Me.lblTelefonos.Location = New System.Drawing.Point(73, 242)
        Me.lblTelefonos.Name = "lblTelefonos"
        Me.lblTelefonos.Size = New System.Drawing.Size(62, 16)
        Me.lblTelefonos.TabIndex = 24
        Me.lblTelefonos.Tag = ""
        Me.lblTelefonos.Text = "Tel�fono&s:"
        Me.lblTelefonos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefonos
        '
        Me.txtTelefonos.EditValue = ""
        Me.txtTelefonos.Location = New System.Drawing.Point(137, 239)
        Me.txtTelefonos.Name = "txtTelefonos"
        '
        'txtTelefonos.Properties
        '
        Me.txtTelefonos.Properties.MaxLength = 50
        Me.txtTelefonos.Size = New System.Drawing.Size(300, 20)
        Me.txtTelefonos.TabIndex = 25
        Me.txtTelefonos.Tag = "telefonos"
        '
        'lblContactos
        '
        Me.lblContactos.AutoSize = True
        Me.lblContactos.Location = New System.Drawing.Point(69, 265)
        Me.lblContactos.Name = "lblContactos"
        Me.lblContactos.Size = New System.Drawing.Size(64, 16)
        Me.lblContactos.TabIndex = 27
        Me.lblContactos.Tag = ""
        Me.lblContactos.Text = "Con&tactos:"
        Me.lblContactos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtContactos
        '
        Me.txtContactos.EditValue = ""
        Me.txtContactos.Location = New System.Drawing.Point(137, 264)
        Me.txtContactos.Name = "txtContactos"
        '
        'txtContactos.Properties
        '
        Me.txtContactos.Properties.MaxLength = 100
        Me.txtContactos.Size = New System.Drawing.Size(300, 20)
        Me.txtContactos.TabIndex = 28
        Me.txtContactos.Tag = "contactos"
        '
        'pnCuentasBancarias
        '
        Me.pnCuentasBancarias.Controls.Add(Me.grCuentasBancarias)
        Me.pnCuentasBancarias.Controls.Add(Me.tmaCuentasBancarias)
        Me.pnCuentasBancarias.Location = New System.Drawing.Point(152, 32)
        Me.pnCuentasBancarias.Name = "pnCuentasBancarias"
        Me.pnCuentasBancarias.Size = New System.Drawing.Size(640, 296)
        Me.pnCuentasBancarias.TabIndex = 60
        '
        'pnDescuentos
        '
        Me.pnDescuentos.Controls.Add(Me.tmaDescuentosProveedores)
        Me.pnDescuentos.Controls.Add(Me.grDescuentosProveedores)
        Me.pnDescuentos.Location = New System.Drawing.Point(152, 32)
        Me.pnDescuentos.Name = "pnDescuentos"
        Me.pnDescuentos.Size = New System.Drawing.Size(640, 296)
        Me.pnDescuentos.TabIndex = 61
        '
        'tmaDescuentosProveedores
        '
        Me.tmaDescuentosProveedores.BackColor = System.Drawing.Color.White
        Me.tmaDescuentosProveedores.CanDelete = True
        Me.tmaDescuentosProveedores.CanInsert = True
        Me.tmaDescuentosProveedores.CanUpdate = True
        Me.tmaDescuentosProveedores.Dock = System.Windows.Forms.DockStyle.Top
        Me.tmaDescuentosProveedores.Grid = Me.grDescuentosProveedores
        Me.tmaDescuentosProveedores.Location = New System.Drawing.Point(0, 0)
        Me.tmaDescuentosProveedores.Name = "tmaDescuentosProveedores"
        Me.tmaDescuentosProveedores.Size = New System.Drawing.Size(640, 23)
        Me.tmaDescuentosProveedores.TabIndex = 0
        Me.tmaDescuentosProveedores.TabStop = False
        Me.tmaDescuentosProveedores.Title = "Descuentos"
        Me.tmaDescuentosProveedores.UpdateTitle = "un Descuento"
        '
        'grDescuentosProveedores
        '
        '
        'grDescuentosProveedores.EmbeddedNavigator
        '
        Me.grDescuentosProveedores.EmbeddedNavigator.Name = ""
        Me.grDescuentosProveedores.Location = New System.Drawing.Point(0, 24)
        Me.grDescuentosProveedores.MainView = Me.grvDescuentosProveedor
        Me.grDescuentosProveedores.Name = "grDescuentosProveedores"
        Me.grDescuentosProveedores.Size = New System.Drawing.Size(640, 264)
        Me.grDescuentosProveedores.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grDescuentosProveedores.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentosProveedores.TabIndex = 28
        Me.grDescuentosProveedores.TabStop = False
        Me.grDescuentosProveedores.Text = "Cuentas Bancarias"
        '
        'grvDescuentosProveedor
        '
        Me.grvDescuentosProveedor.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveDescuento, Me.grcNombreDescuento, Me.grcPorcentaje, Me.grcAntesIVA, Me.grcProntoPago})
        Me.grvDescuentosProveedor.GridControl = Me.grDescuentosProveedores
        Me.grvDescuentosProveedor.Name = "grvDescuentosProveedor"
        Me.grvDescuentosProveedor.OptionsBehavior.Editable = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowFilter = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowGroup = False
        Me.grvDescuentosProveedor.OptionsCustomization.AllowSort = False
        Me.grvDescuentosProveedor.OptionsView.ShowGroupPanel = False
        '
        'grcClaveDescuento
        '
        Me.grcClaveDescuento.Caption = "Descuento"
        Me.grcClaveDescuento.FieldName = "descuento"
        Me.grcClaveDescuento.Name = "grcClaveDescuento"
        Me.grcClaveDescuento.Width = 89
        '
        'grcNombreDescuento
        '
        Me.grcNombreDescuento.Caption = "Descripci�n"
        Me.grcNombreDescuento.FieldName = "nombre"
        Me.grcNombreDescuento.Name = "grcNombreDescuento"
        Me.grcNombreDescuento.VisibleIndex = 0
        Me.grcNombreDescuento.Width = 280
        '
        'grcPorcentaje
        '
        Me.grcPorcentaje.Caption = "Porcentaje"
        Me.grcPorcentaje.DisplayFormat.FormatString = "n2"
        Me.grcPorcentaje.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPorcentaje.FieldName = "porcentaje"
        Me.grcPorcentaje.Name = "grcPorcentaje"
        Me.grcPorcentaje.VisibleIndex = 3
        Me.grcPorcentaje.Width = 120
        '
        'grcAntesIVA
        '
        Me.grcAntesIVA.Caption = "Antes de IVA"
        Me.grcAntesIVA.FieldName = "antes_iva"
        Me.grcAntesIVA.Name = "grcAntesIVA"
        Me.grcAntesIVA.VisibleIndex = 1
        Me.grcAntesIVA.Width = 107
        '
        'grcProntoPago
        '
        Me.grcProntoPago.Caption = "Pronto Pago"
        Me.grcProntoPago.FieldName = "pronto_pago"
        Me.grcProntoPago.Name = "grcProntoPago"
        Me.grcProntoPago.VisibleIndex = 2
        Me.grcProntoPago.Width = 119
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.NvDatosGenerales, Me.NvCuentasBancarias, Me.NvDescuentos})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 28)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(140, 364)
        Me.NavBarControl1.TabIndex = 62
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NvDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.NvCuentasBancarias), New DevExpress.XtraNavBar.NavBarItemLink(Me.NvDescuentos)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'NvDatosGenerales
        '
        Me.NvDatosGenerales.Caption = "Datos Generales"
        Me.NvDatosGenerales.LargeImage = CType(resources.GetObject("NvDatosGenerales.LargeImage"), System.Drawing.Image)
        Me.NvDatosGenerales.Name = "NvDatosGenerales"
        '
        'NvCuentasBancarias
        '
        Me.NvCuentasBancarias.Caption = "Cuentas Bancarias"
        Me.NvCuentasBancarias.LargeImage = CType(resources.GetObject("NvCuentasBancarias.LargeImage"), System.Drawing.Image)
        Me.NvCuentasBancarias.Name = "NvCuentasBancarias"
        '
        'NvDescuentos
        '
        Me.NvDescuentos.Caption = "Descuentos"
        Me.NvDescuentos.LargeImage = CType(resources.GetObject("NvDescuentos.LargeImage"), System.Drawing.Image)
        Me.NvDescuentos.Name = "NvDescuentos"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(472, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 16)
        Me.Label5.TabIndex = 12
        Me.Label5.Tag = ""
        Me.Label5.Text = "Alias:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmProveedores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(810, 392)
        Me.Controls.Add(Me.pnDatosGenerales)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.lblNotas)
        Me.Controls.Add(Me.txtNotas)
        Me.Controls.Add(Me.pnCuentasBancarias)
        Me.Controls.Add(Me.pnDescuentos)
        Me.Name = "frmProveedores"
        Me.Controls.SetChildIndex(Me.pnDescuentos, 0)
        Me.Controls.SetChildIndex(Me.pnCuentasBancarias, 0)
        Me.Controls.SetChildIndex(Me.txtNotas, 0)
        Me.Controls.SetChildIndex(Me.lblNotas, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        Me.Controls.SetChildIndex(Me.pnDatosGenerales, 0)
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grCuentasBancarias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnDatosGenerales.ResumeLayout(False)
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboPersona.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRfc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnCuentasBancarias.ResumeLayout(False)
        Me.pnDescuentos.ResumeLayout(False)
        CType(Me.grDescuentosProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDescuentosProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oProveedores As VillarrealBusiness.clsProveedores
    Friend oProveedoresCuentasBancarias As VillarrealBusiness.clsProveedoresCuentasBancarias
    Friend oProveedoresdescuentos As VillarrealBusiness.clsProveedoresDescuentos

    Public ReadOnly Property proveedor() As Long
        Get
            Return Me.clcProveedor.Value
        End Get
    End Property

    Public ReadOnly Property Vista_Descuentos() As DataView
        Get
            Return Me.grvDescuentosProveedor.DataSource
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmProveedores_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmProveedores_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmProveedores_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub


    Private Sub frmProveedores_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If cboPersona.EditValue = "M" Then
            txtCurp.Text = ""
        End If

        Select Case Action
            Case Actions.Insert
                Response = oProveedores.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oProveedores.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oProveedores.Eliminar(clcProveedor.Value)

        End Select
    End Sub
    Private Sub frmProveedores_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With Me.tmaCuentasBancarias

            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oProveedoresCuentasBancarias.Insertar(.SelectedRow, Me.clcProveedor.EditValue)
                    Case Actions.Update
                        Response = oProveedoresCuentasBancarias.Actualizar(.SelectedRow, Me.clcProveedor.EditValue)
                    Case Actions.Delete
                        Response = oProveedoresCuentasBancarias.Eliminar(Me.clcProveedor.EditValue, .Item("banco"), .Item("cuenta"))
                End Select
                .MoveNext()
            Loop
        End With


        With Me.tmaDescuentosProveedores
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = Me.oProveedoresdescuentos.Insertar(.SelectedRow, Me.clcProveedor.Value)
                    Case Actions.Update
                        Response = Me.oProveedoresdescuentos.Actualizar(.SelectedRow, Me.clcProveedor.Value)
                    Case Actions.Delete
                        Response = Me.oProveedoresdescuentos.Eliminar(Me.clcProveedor.Value, Me.tmaDescuentosProveedores.Item("descuento"))

                End Select
                .MoveNext()
            Loop
        End With

    End Sub

    Private Sub frmProveedores_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oProveedores.DespliegaDatos(OwnerForm.Value("proveedor"))
        Dim oDataSet As DataSet

        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

        If Not Response.ErrorFound Then Response = oProveedoresCuentasBancarias.Listado(Me.clcProveedor.EditValue)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaCuentasBancarias.DataSource = oDataSet
        End If


        If Not Response.ErrorFound Then Response = Me.oProveedoresdescuentos.DespliegaDatos(Me.clcProveedor.Value)
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.tmaDescuentosProveedores.DataSource = oDataSet
        End If

        oDataSet = Nothing

    End Sub
    Private Sub frmProveedores_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oProveedores = New VillarrealBusiness.clsProveedores
        oProveedoresCuentasBancarias = New VillarrealBusiness.clsProveedoresCuentasBancarias
        oProveedoresdescuentos = New VillarrealBusiness.clsProveedoresDescuentos

        Me.pnDatosGenerales.BringToFront()

        With Me.tmaCuentasBancarias
            .UpdateTitle = "una Cuenta Bancaria"
            .UpdateForm = New frmProveedoresCuentasBancarias

            .AddColumn("banco", "System.Int32")
            .AddColumn("nombre")
            .AddColumn("sucursal")
            .AddColumn("plaza")
            .AddColumn("cuenta")
        End With


        With Me.tmaDescuentosProveedores
            .UpdateTitle = " un Descuento"
            .UpdateForm = New frmProveedoresDescuentos
            .AddColumn("descuento", "System.Int32")
            .AddColumn("nombre", "System.String")
            .AddColumn("porcentaje", "System.Double")
            .AddColumn("antes_iva", "System.Boolean")
            .AddColumn("pronto_pago", "System.Boolean")
        End With



        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmProveedores_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oProveedores.Validacion(Action, txtNombre.Text, Me.txtCuenta.Text, txtRfc.Text, Me.txtDomicilio.Text, Me.txtColonia.Text, Me.txtCiudad.Text, Me.txtEstado.Text, Me.clcCp.EditValue, Me.txtTelefonos.Text, Me.txtContactos.Text, Me.txtCurp.Text, Me.cboPersona.EditValue)
    End Sub

    Private Sub frmProveedores_Localize() Handles MyBase.Localize
        Find("proveedor", Me.clcProveedor.EditValue)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub txtNombre_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oProveedores.ValidaNombre(txtNombre.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub clcCuenta_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oProveedores.ValidaCuenta(Me.txtCuenta.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub txtRfc_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oProveedores.ValidaRfc(txtRfc.Text, cboPersona.EditValue)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

    Private Sub NvDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NvDatosGenerales.LinkPressed
        Me.pnDatosGenerales.BringToFront()
    End Sub


    Private Sub NvCuentasBancarias_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NvCuentasBancarias.LinkPressed
        Me.pnCuentasBancarias.BringToFront()
    End Sub

    Private Sub NvDescuentos_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NvDescuentos.LinkPressed
        Me.pnDescuentos.BringToFront()
    End Sub



#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region




    

    Private Sub cboPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPersona.SelectedIndexChanged
        If cboPersona.Text = "F�sica" Then
            txtCurp.Visible = True
            lblCurp.Visible = True
        Else
            txtCurp.Visible = False
            lblCurp.Visible = False
        End If

    End Sub

   
End Class
