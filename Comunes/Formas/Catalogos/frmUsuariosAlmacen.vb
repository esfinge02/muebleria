Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmUsuariosAlmacen
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpUsuario As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkPermitirTodas As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpCajero As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpCajeroAsignado As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkModificarPreciosCotizacion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblPorcentajeCondonacionIntereses As System.Windows.Forms.Label
    Friend WithEvents clcPorcentajeCondonacionIntereses As Dipros.Editors.TINCalcEdit
    Friend WithEvents gbp1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblAbogado As System.Windows.Forms.Label
    Friend WithEvents lkpAbogado As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUsuariosAlmacen))
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpUsuario = New Dipros.Editors.TINMultiLookup
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.chkPermitirTodas = New DevExpress.XtraEditors.CheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpCajero = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpCajeroAsignado = New Dipros.Editors.TINMultiLookup
        Me.chkModificarPreciosCotizacion = New DevExpress.XtraEditors.CheckEdit
        Me.lblPorcentajeCondonacionIntereses = New System.Windows.Forms.Label
        Me.clcPorcentajeCondonacionIntereses = New Dipros.Editors.TINCalcEdit
        Me.gbp1 = New System.Windows.Forms.GroupBox
        Me.lblAbogado = New System.Windows.Forms.Label
        Me.lkpAbogado = New Dipros.Editors.TINMultiLookup
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        CType(Me.chkPermitirTodas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkModificarPreciosCotizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPorcentajeCondonacionIntereses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbp1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(253, 28)
        Me.tbrTools.TabIndex = 0
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(72, 16)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(51, 16)
        Me.lblProveedor.TabIndex = 0
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "&Usuario:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpUsuario
        '
        Me.lkpUsuario.AllowAdd = False
        Me.lkpUsuario.AutoReaload = False
        Me.lkpUsuario.DataSource = Nothing
        Me.lkpUsuario.DefaultSearchField = ""
        Me.lkpUsuario.DisplayMember = "nombre"
        Me.lkpUsuario.EditValue = Nothing
        Me.lkpUsuario.Enabled = False
        Me.lkpUsuario.Filtered = False
        Me.lkpUsuario.InitValue = Nothing
        Me.lkpUsuario.Location = New System.Drawing.Point(124, 16)
        Me.lkpUsuario.MultiSelect = False
        Me.lkpUsuario.Name = "lkpUsuario"
        Me.lkpUsuario.NullText = ""
        Me.lkpUsuario.PopupWidth = CType(400, Long)
        Me.lkpUsuario.Required = False
        Me.lkpUsuario.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpUsuario.SearchMember = ""
        Me.lkpUsuario.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpUsuario.SelectAll = False
        Me.lkpUsuario.Size = New System.Drawing.Size(204, 20)
        Me.lkpUsuario.TabIndex = 1
        Me.lkpUsuario.Tag = "usuario"
        Me.lkpUsuario.ToolTip = "usuario"
        Me.lkpUsuario.ValueMember = "usuario"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(73, 40)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 2
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(124, 40)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(204, 20)
        Me.lkpBodega.TabIndex = 3
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = "Bodega"
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'chkPermitirTodas
        '
        Me.chkPermitirTodas.Location = New System.Drawing.Point(80, 45)
        Me.chkPermitirTodas.Name = "chkPermitirTodas"
        '
        'chkPermitirTodas.Properties
        '
        Me.chkPermitirTodas.Properties.Caption = "Acceder a todas las bodegas"
        Me.chkPermitirTodas.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkPermitirTodas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkPermitirTodas.Size = New System.Drawing.Size(204, 22)
        Me.chkPermitirTodas.TabIndex = 1
        Me.chkPermitirTodas.Tag = "permitir_todas"
        Me.chkPermitirTodas.ToolTip = "Acceder a todas las bodegas"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(78, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Cajero:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCajero
        '
        Me.lkpCajero.AllowAdd = False
        Me.lkpCajero.AutoReaload = False
        Me.lkpCajero.DataSource = Nothing
        Me.lkpCajero.DefaultSearchField = ""
        Me.lkpCajero.DisplayMember = "nombre"
        Me.lkpCajero.EditValue = Nothing
        Me.lkpCajero.Filtered = False
        Me.lkpCajero.InitValue = Nothing
        Me.lkpCajero.Location = New System.Drawing.Point(124, 64)
        Me.lkpCajero.MultiSelect = False
        Me.lkpCajero.Name = "lkpCajero"
        Me.lkpCajero.NullText = ""
        Me.lkpCajero.PopupWidth = CType(400, Long)
        Me.lkpCajero.Required = False
        Me.lkpCajero.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCajero.SearchMember = ""
        Me.lkpCajero.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCajero.SelectAll = False
        Me.lkpCajero.Size = New System.Drawing.Size(204, 20)
        Me.lkpCajero.TabIndex = 5
        Me.lkpCajero.Tag = ""
        Me.lkpCajero.ToolTip = "cajero"
        Me.lkpCajero.ValueMember = "cajero"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(24, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Tag = ""
        Me.Label3.Text = "Cajero Asignado:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCajeroAsignado
        '
        Me.lkpCajeroAsignado.AllowAdd = False
        Me.lkpCajeroAsignado.AutoReaload = False
        Me.lkpCajeroAsignado.DataSource = Nothing
        Me.lkpCajeroAsignado.DefaultSearchField = ""
        Me.lkpCajeroAsignado.DisplayMember = "nombre"
        Me.lkpCajeroAsignado.EditValue = Nothing
        Me.lkpCajeroAsignado.Enabled = False
        Me.lkpCajeroAsignado.Filtered = False
        Me.lkpCajeroAsignado.InitValue = Nothing
        Me.lkpCajeroAsignado.Location = New System.Drawing.Point(124, 88)
        Me.lkpCajeroAsignado.MultiSelect = False
        Me.lkpCajeroAsignado.Name = "lkpCajeroAsignado"
        Me.lkpCajeroAsignado.NullText = ""
        Me.lkpCajeroAsignado.PopupWidth = CType(400, Long)
        Me.lkpCajeroAsignado.Required = False
        Me.lkpCajeroAsignado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCajeroAsignado.SearchMember = ""
        Me.lkpCajeroAsignado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCajeroAsignado.SelectAll = False
        Me.lkpCajeroAsignado.Size = New System.Drawing.Size(204, 20)
        Me.lkpCajeroAsignado.TabIndex = 7
        Me.lkpCajeroAsignado.Tag = ""
        Me.lkpCajeroAsignado.ToolTip = "cajero asignado"
        Me.lkpCajeroAsignado.ValueMember = "cajero"
        '
        'chkModificarPreciosCotizacion
        '
        Me.chkModificarPreciosCotizacion.Location = New System.Drawing.Point(80, 13)
        Me.chkModificarPreciosCotizacion.Name = "chkModificarPreciosCotizacion"
        '
        'chkModificarPreciosCotizacion.Properties
        '
        Me.chkModificarPreciosCotizacion.Properties.Caption = "Modificar Precios en Cotizaciones"
        Me.chkModificarPreciosCotizacion.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkModificarPreciosCotizacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkModificarPreciosCotizacion.Size = New System.Drawing.Size(204, 22)
        Me.chkModificarPreciosCotizacion.TabIndex = 0
        Me.chkModificarPreciosCotizacion.Tag = "modificar_precios_cotizacion"
        Me.chkModificarPreciosCotizacion.ToolTip = "Modificar Precios en Cotizaciones"
        '
        'lblPorcentajeCondonacionIntereses
        '
        Me.lblPorcentajeCondonacionIntereses.AutoSize = True
        Me.lblPorcentajeCondonacionIntereses.Location = New System.Drawing.Point(24, 136)
        Me.lblPorcentajeCondonacionIntereses.Name = "lblPorcentajeCondonacionIntereses"
        Me.lblPorcentajeCondonacionIntereses.Size = New System.Drawing.Size(232, 16)
        Me.lblPorcentajeCondonacionIntereses.TabIndex = 10
        Me.lblPorcentajeCondonacionIntereses.Tag = ""
        Me.lblPorcentajeCondonacionIntereses.Text = "&Porcentaje de Condonaci�n de Intereses:"
        Me.lblPorcentajeCondonacionIntereses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPorcentajeCondonacionIntereses
        '
        Me.clcPorcentajeCondonacionIntereses.EditValue = "0"
        Me.clcPorcentajeCondonacionIntereses.Location = New System.Drawing.Point(260, 136)
        Me.clcPorcentajeCondonacionIntereses.MaxValue = 0
        Me.clcPorcentajeCondonacionIntereses.MinValue = 0
        Me.clcPorcentajeCondonacionIntereses.Name = "clcPorcentajeCondonacionIntereses"
        '
        'clcPorcentajeCondonacionIntereses.Properties
        '
        Me.clcPorcentajeCondonacionIntereses.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcPorcentajeCondonacionIntereses.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentajeCondonacionIntereses.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcPorcentajeCondonacionIntereses.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentajeCondonacionIntereses.Properties.MaskData.EditMask = "########0.00"
        Me.clcPorcentajeCondonacionIntereses.Properties.Precision = 18
        Me.clcPorcentajeCondonacionIntereses.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPorcentajeCondonacionIntereses.Size = New System.Drawing.Size(68, 19)
        Me.clcPorcentajeCondonacionIntereses.TabIndex = 11
        Me.clcPorcentajeCondonacionIntereses.Tag = "porcentaje_condonacion_intereses"
        Me.clcPorcentajeCondonacionIntereses.ToolTip = "porcentaje de condonaci�n de intereses"
        '
        'gbp1
        '
        Me.gbp1.Controls.Add(Me.lblAbogado)
        Me.gbp1.Controls.Add(Me.lkpAbogado)
        Me.gbp1.Controls.Add(Me.lkpCajeroAsignado)
        Me.gbp1.Controls.Add(Me.lkpBodega)
        Me.gbp1.Controls.Add(Me.Label3)
        Me.gbp1.Controls.Add(Me.Label2)
        Me.gbp1.Controls.Add(Me.lblBodega)
        Me.gbp1.Controls.Add(Me.lkpUsuario)
        Me.gbp1.Controls.Add(Me.lkpCajero)
        Me.gbp1.Controls.Add(Me.lblProveedor)
        Me.gbp1.Controls.Add(Me.clcPorcentajeCondonacionIntereses)
        Me.gbp1.Controls.Add(Me.lblPorcentajeCondonacionIntereses)
        Me.gbp1.Location = New System.Drawing.Point(9, 32)
        Me.gbp1.Name = "gbp1"
        Me.gbp1.Size = New System.Drawing.Size(352, 168)
        Me.gbp1.TabIndex = 0
        Me.gbp1.TabStop = False
        '
        'lblAbogado
        '
        Me.lblAbogado.AutoSize = True
        Me.lblAbogado.Location = New System.Drawing.Point(66, 112)
        Me.lblAbogado.Name = "lblAbogado"
        Me.lblAbogado.Size = New System.Drawing.Size(57, 16)
        Me.lblAbogado.TabIndex = 8
        Me.lblAbogado.Tag = ""
        Me.lblAbogado.Text = "Abo&gado:"
        Me.lblAbogado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAbogado
        '
        Me.lkpAbogado.AllowAdd = False
        Me.lkpAbogado.AutoReaload = False
        Me.lkpAbogado.DataSource = Nothing
        Me.lkpAbogado.DefaultSearchField = ""
        Me.lkpAbogado.DisplayMember = "nombre"
        Me.lkpAbogado.EditValue = Nothing
        Me.lkpAbogado.Filtered = False
        Me.lkpAbogado.InitValue = Nothing
        Me.lkpAbogado.Location = New System.Drawing.Point(124, 112)
        Me.lkpAbogado.MultiSelect = False
        Me.lkpAbogado.Name = "lkpAbogado"
        Me.lkpAbogado.NullText = ""
        Me.lkpAbogado.PopupWidth = CType(400, Long)
        Me.lkpAbogado.Required = False
        Me.lkpAbogado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAbogado.SearchMember = ""
        Me.lkpAbogado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAbogado.SelectAll = False
        Me.lkpAbogado.Size = New System.Drawing.Size(204, 20)
        Me.lkpAbogado.TabIndex = 9
        Me.lkpAbogado.Tag = ""
        Me.lkpAbogado.ToolTip = "abogado"
        Me.lkpAbogado.ValueMember = "abogado"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkModificarPreciosCotizacion)
        Me.GroupBox1.Controls.Add(Me.chkPermitirTodas)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 208)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(352, 72)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Permisos"
        '
        'frmUsuariosAlmacen
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(370, 288)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbp1)
        Me.Name = "frmUsuariosAlmacen"
        Me.Text = "frmUsuariosAlmacen"
        Me.Controls.SetChildIndex(Me.gbp1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.chkPermitirTodas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkModificarPreciosCotizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPorcentajeCondonacionIntereses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbp1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oUsuarios As VillarrealBusiness.clsUsuarios
    Private oCajeros As VillarrealBusiness.clsCajeros
    Private oUsuariosCajeros As VillarrealBusiness.clsUsuariosCajeros
    Private oAbogados As VillarrealBusiness.clsAbogados


    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property

    Private ReadOnly Property Usuario() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpUsuario)
        End Get
    End Property
    Private ReadOnly Property cajero() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCajero)
        End Get
    End Property

    Private ReadOnly Property cajeroasignado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCajeroAsignado)
        End Get
    End Property
    Private ReadOnly Property Abogado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpabogado)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmUsuariosAlmacen_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oBodegas = New VillarrealBusiness.clsBodegas
        oCajeros = New VillarrealBusiness.clsCajeros
        oUsuarios = New VillarrealBusiness.clsUsuarios
        oUsuariosCajeros = New VillarrealBusiness.clsUsuariosCajeros
        oAbogados = New VillarrealBusiness.clsAbogados
    End Sub

    Private Sub frmUsuariosAlmacen_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim cajeroinsertar As Long
        Response = oUsuarios.ActualizarUsuariosAlmacen(Me.DataSource, Me.Abogado)
        If Me.lkpCajero.DisplayText = "" Then
            lkpCajero.EditValue = -1
        End If

        If Response.ErrorFound Then Exit Sub

        'If Me.lkpCajeroAsignado.DisplayText = "" Then
        '    lkpCajero.EditValue = -1
        'End If
        If cajero = -1 Then
            cajeroinsertar = cajeroasignado
        Else
            cajeroinsertar = cajero
        End If
        Response = oUsuariosCajeros.Actualizar(Usuario, cajeroinsertar)

        If Not Response.ErrorFound Then
            Response = oUsuarios.ActualizaModificarPreciosCotizacion(Usuario, Me.chkModificarPreciosCotizacion.Checked)
        End If
    End Sub

    Private Sub frmUsuariosAlmacen_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oUsuarios.SeleccionaUsuarioAlmacen(OwnerForm.Value("usuario"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            Me.DataSource = oDataSet
        End If
        Response = oUsuariosCajeros.DespliegaDatos(OwnerForm.Value("usuario"), -1)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            odataset = Response.Value
            If odataset.Tables(0).Rows.Count > 0 Then
                Me.lkpCajeroAsignado.EditValue = oDataSet.Tables(0).Rows(0).Item("cajero")
            End If
        End If
    End Sub

    Private Sub frmUsuariosAlmacen_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oUsuarios.Validacion(Me.Action, Usuario, Bodega, Me.clcPorcentajeCondonacionIntereses.Value)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events

        response = oBodegas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

    Private Sub lkpcajero_Format() Handles lkpCajero.Format
        Comunes.clsFormato.for_cajeros_grl(Me.lkpCajero)
    End Sub
    Private Sub lkpcajero_LoadData(ByVal Initialize As Boolean) Handles lkpCajero.LoadData
        Dim response As Events

        response = oCajeros.Lookup_usuarios()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCajero.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

    Private Sub lkpcajeroasignado_Format() Handles lkpCajeroAsignado.Format
        Comunes.clsFormato.for_cajeros_grl(Me.lkpCajeroAsignado)
    End Sub
    Private Sub lkpcajeroAsignado_LoadData(ByVal Initialize As Boolean) Handles lkpCajeroAsignado.LoadData
        Dim response As Events

        response = oCajeros.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCajeroAsignado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub
    Private Sub lkpUsuario_Format() Handles lkpUsuario.Format
        Comunes.clsFormato.for_usuarios_grl(Me.lkpUsuario)
    End Sub
    Private Sub lkpUsuario_LoadData(ByVal Initialize As Boolean) Handles lkpUsuario.LoadData
        Dim response As Events

        response = oUsuarios.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpUsuario.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

    Private Sub lkpAbogado_Format() Handles lkpAbogado.Format
        Comunes.clsFormato.for_abogados_grl(Me.lkpAbogado)
    End Sub
    Private Sub lkpAbogado_LoadData(ByVal Initialize As Boolean) Handles lkpAbogado.LoadData
        Dim response As Events

        response = oAbogados.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpAbogado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

#End Region



End Class
