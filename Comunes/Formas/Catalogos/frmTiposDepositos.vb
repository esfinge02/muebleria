Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmTiposDepositos
    Inherits Dipros.Windows.frmTINForm


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents clcTipoDeposito As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTiposDepositos))
        Me.clcTipoDeposito = New Dipros.Editors.TINCalcEdit
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.cboTipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblTipo = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.clcTipoDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(226, 28)
        '
        'clcTipoDeposito
        '
        Me.clcTipoDeposito.EditValue = "0"
        Me.clcTipoDeposito.Location = New System.Drawing.Point(96, 48)
        Me.clcTipoDeposito.MaxValue = 0
        Me.clcTipoDeposito.MinValue = 0
        Me.clcTipoDeposito.Name = "clcTipoDeposito"
        '
        'clcTipoDeposito.Properties
        '
        Me.clcTipoDeposito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipoDeposito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipoDeposito.Properties.Enabled = False
        Me.clcTipoDeposito.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTipoDeposito.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTipoDeposito.Size = New System.Drawing.Size(56, 19)
        Me.clcTipoDeposito.TabIndex = 1
        Me.clcTipoDeposito.TabStop = False
        Me.clcTipoDeposito.Tag = "Tipo_deposito"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(96, 72)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 30
        Me.txtDescripcion.Size = New System.Drawing.Size(256, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = "C"
        Me.cboTipo.Location = New System.Drawing.Point(96, 96)
        Me.cboTipo.Name = "cboTipo"
        '
        'cboTipo.Properties
        '
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cargo", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Abono", "A", -1)})
        Me.cboTipo.Size = New System.Drawing.Size(88, 20)
        Me.cboTipo.TabIndex = 5
        Me.cboTipo.Tag = "Tipo"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(9, 48)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(84, 16)
        Me.lblTipo.TabIndex = 0
        Me.lblTipo.Tag = ""
        Me.lblTipo.Text = "Tipo Deposito:"
        Me.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "Descripción:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(61, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Tag = ""
        Me.Label3.Text = "Tipo:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmTiposDepositos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 128)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.cboTipo)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.clcTipoDeposito)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Name = "frmTiposDepositos"
        Me.Text = "frmTiposDepositos"
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcTipoDeposito, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.cboTipo, 0)
        Me.Controls.SetChildIndex(Me.lblTipo, 0)
        CType(Me.clcTipoDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oTiposDepositos As VillarrealBusiness.clsTiposDepositos
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmTiposDepositos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Select Case Action
            Case Actions.Insert
                Response = oTiposDepositos.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oTiposDepositos.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oTiposDepositos.Eliminar(Me.clcTipoDeposito.Value)

        End Select

    End Sub

    Private Sub frmTiposDepositos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oTiposDepositos.DespliegaDatos(OwnerForm.Value("Tipo_deposito"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If
    End Sub

    Private Sub frmTiposDepositos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oTiposDepositos = New VillarrealBusiness.clsTiposDepositos

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmTiposDepositos_Localize() Handles MyBase.Localize
        Find("Tipo_deposito", Me.clcTipoDeposito.Value)
    End Sub

    Private Sub frmTiposDepositos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oTiposDepositos.ValidaDescripcion(Me.txtDescripcion.Text)
    End Sub

#End Region


    
   
   
  
End Class
