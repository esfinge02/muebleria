Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmClientesConvenios
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents txtno_empleado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtClave_pago As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblNombreConvenio As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmClientesConvenios))
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.txtno_empleado = New DevExpress.XtraEditors.TextEdit
        Me.txtClave_pago = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblNombreConvenio = New System.Windows.Forms.Label
        CType(Me.txtno_empleado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClave_pago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(458, 28)
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(98, 48)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = ""
        Me.lkpConvenio.PopupWidth = CType(400, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = True
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = True
        Me.lkpConvenio.Size = New System.Drawing.Size(368, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 1
        Me.lkpConvenio.Tag = "convenio"
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(34, 48)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 0
        Me.lblCobrador.Text = "Convenio:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtno_empleado
        '
        Me.txtno_empleado.EditValue = ""
        Me.txtno_empleado.Location = New System.Drawing.Point(98, 72)
        Me.txtno_empleado.Name = "txtno_empleado"
        '
        'txtno_empleado.Properties
        '
        Me.txtno_empleado.Properties.MaxLength = 10
        Me.txtno_empleado.Size = New System.Drawing.Size(94, 20)
        Me.txtno_empleado.TabIndex = 3
        Me.txtno_empleado.Tag = "no_empleado"
        '
        'txtClave_pago
        '
        Me.txtClave_pago.EditValue = ""
        Me.txtClave_pago.Location = New System.Drawing.Point(98, 96)
        Me.txtClave_pago.Name = "txtClave_pago"
        '
        'txtClave_pago.Properties
        '
        Me.txtClave_pago.Properties.MaxLength = 50
        Me.txtClave_pago.Size = New System.Drawing.Size(368, 20)
        Me.txtClave_pago.TabIndex = 5
        Me.txtClave_pago.Tag = "clave_pago"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "No. Empleado:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Clave Pago:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNombreConvenio
        '
        Me.lblNombreConvenio.AutoSize = True
        Me.lblNombreConvenio.Location = New System.Drawing.Point(224, 72)
        Me.lblNombreConvenio.Name = "lblNombreConvenio"
        Me.lblNombreConvenio.Size = New System.Drawing.Size(0, 16)
        Me.lblNombreConvenio.TabIndex = 59
        Me.lblNombreConvenio.Tag = "nombre_convenio"
        Me.lblNombreConvenio.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblNombreConvenio.Visible = False
        '
        'frmClientesConvenios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(474, 124)
        Me.Controls.Add(Me.lblNombreConvenio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtClave_pago)
        Me.Controls.Add(Me.txtno_empleado)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.lblCobrador)
        Me.Name = "frmClientesConvenios"
        Me.Text = "frmClientesConvenios"
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.txtno_empleado, 0)
        Me.Controls.SetChildIndex(Me.txtClave_pago, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lblNombreConvenio, 0)
        CType(Me.txtno_empleado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClave_pago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"


    Private oConvenios As VillarrealBusiness.clsConvenios
    Private oClientesConvenios As VillarrealBusiness.clsClientesConvenios
    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmClientesConvenios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientesConvenios = New VillarrealBusiness.clsClientesConvenios
        oConvenios = New VillarrealBusiness.clsConvenios

    End Sub

    Private Sub frmClientesConvenios_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

        Me.lkpConvenio.Enabled = False

        ' SE PUSO POR QUE EN SOLICITUDES DE CLIENTES NO APARECE EL BOTON PARA ELIMINAR
        If Me.Action = Actions.Delete Then
            Me.tbrTools.Buttons(0).Visible = True
        End If

    End Sub

    Private Sub frmClientesConvenios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select

        End With
    End Sub

    Private Sub frmClientesConvenios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.lkpConvenio.Validate()
        Response = oClientesConvenios.Validacion(Action, Convenio, CType(OwnerForm.MasterControl.DataSource, DataSet), Convenio.ToString())
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub

    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim Response As New Events
        Response = oConvenios.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpConvenio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConvenio.EditValueChanged
        Me.lblNombreConvenio.Text = CStr(Me.lkpConvenio.GetValue("nombre"))
    End Sub

#End Region

   
End Class
