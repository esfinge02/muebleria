Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms


Public Class frmArticulos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lblCodigo_Barras As System.Windows.Forms.Label
    Friend WithEvents txtCodigo_Barras As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblUnidad As System.Windows.Forms.Label
    Friend WithEvents lkpUnidad As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblUltimo_Costo As System.Windows.Forms.Label
    Friend WithEvents clcUltimo_Costo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblUltima_Compra As System.Windows.Forms.Label
    Friend WithEvents dteUltima_Compra As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblImpuesto As System.Windows.Forms.Label
    Friend WithEvents clcImpuesto As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImpuesto_Suntuario As System.Windows.Forms.Label
    Friend WithEvents clcImpuesto_Suntuario As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkImportacion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkDescontinuado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkManeja_Series As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkEntrega_Domicilio As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblPrecio_Lista As System.Windows.Forms.Label
    Friend WithEvents clcPrecio_Lista As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCajas As System.Windows.Forms.Label
    Friend WithEvents clcCajas As Dipros.Editors.TINCalcEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents tmaPrecios As Dipros.Windows.TINMaster
    Friend WithEvents grPrecios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvPrecios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcprecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombrePrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rptValor As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcPrecioVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtDescripcionCorta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grUbicaciones As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvUbicaciones As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents tmaUbicaciones As Dipros.Windows.TINMaster
    Friend WithEvents grcUbicacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClaveBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkNoResurtir As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcArticulo As Dipros.Editors.TINCalcEdit
    Friend WithEvents grcterminacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioexpo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkRequiereArmado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkRegalo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcMesesGarantia As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grcControl As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblTama�oEtiquetas As System.Windows.Forms.Label
    Friend WithEvents lkpTama�oEtiqueta As Dipros.Editors.TINMultiLookup
    Friend WithEvents txtCapacidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tlbKardex As System.Windows.Forms.ToolBarButton
    Friend WithEvents txtmodelo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblStockMinimo As System.Windows.Forms.Label
    Friend WithEvents clcStockMinimo As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcStockMaximo As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents chkRequiereInstalacion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkNoIdentificable As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grcDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkligero_uso As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents clcUltimoCostoSinFlete As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkPedidoFabrica As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcCostoPedidoFabrica As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nvrDatosGenerales As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvrPrecio As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grExistenciasSucursales As DevExpress.XtraGrid.GridControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents grExistencias As DevExpress.XtraGrid.GridControl
    Friend WithEvents btnVerDocumentos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grvClaveSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rptExistenciasSucursales As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFisicaCalculada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grvExistencias As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcBodegaExistencias As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombrebodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFisica As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rptCantidades As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcPorSurtir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTransito As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEntregar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcReparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcVistas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcGarantia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfisica_calculada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tlbCambioPrecios As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcEntregarPedidoFabrica As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEntregarPedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtmarca As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtclasificacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblMarca As System.Windows.Forms.Label
    Friend WithEvents lblClasificacion As System.Windows.Forms.Label
    Friend WithEvents tblMegaCred As System.Windows.Forms.ToolBarButton
    Friend WithEvents nvrFoto As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents grcRecalcular As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModificable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtDescripcionEspecial As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents nvrDatosEspeciales As DevExpress.XtraNavBar.NavBarItem

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmArticulos))
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lblCodigo_Barras = New System.Windows.Forms.Label
        Me.txtCodigo_Barras = New DevExpress.XtraEditors.TextEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblUnidad = New System.Windows.Forms.Label
        Me.lkpUnidad = New Dipros.Editors.TINMultiLookup
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.lblUltimo_Costo = New System.Windows.Forms.Label
        Me.clcUltimo_Costo = New Dipros.Editors.TINCalcEdit
        Me.lblUltima_Compra = New System.Windows.Forms.Label
        Me.dteUltima_Compra = New DevExpress.XtraEditors.DateEdit
        Me.lblImpuesto = New System.Windows.Forms.Label
        Me.clcImpuesto = New Dipros.Editors.TINCalcEdit
        Me.lblImpuesto_Suntuario = New System.Windows.Forms.Label
        Me.clcImpuesto_Suntuario = New Dipros.Editors.TINCalcEdit
        Me.chkImportacion = New DevExpress.XtraEditors.CheckEdit
        Me.chkDescontinuado = New DevExpress.XtraEditors.CheckEdit
        Me.chkManeja_Series = New DevExpress.XtraEditors.CheckEdit
        Me.chkEntrega_Domicilio = New DevExpress.XtraEditors.CheckEdit
        Me.lblPrecio_Lista = New System.Windows.Forms.Label
        Me.clcPrecio_Lista = New Dipros.Editors.TINCalcEdit
        Me.lblCajas = New System.Windows.Forms.Label
        Me.clcCajas = New Dipros.Editors.TINCalcEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label8 = New System.Windows.Forms.Label
        Me.clcCostoPedidoFabrica = New Dipros.Editors.TINCalcEdit
        Me.chkPedidoFabrica = New DevExpress.XtraEditors.CheckEdit
        Me.chkRegalo = New DevExpress.XtraEditors.CheckEdit
        Me.chkNoResurtir = New DevExpress.XtraEditors.CheckEdit
        Me.chkRequiereArmado = New DevExpress.XtraEditors.CheckEdit
        Me.chkRequiereInstalacion = New DevExpress.XtraEditors.CheckEdit
        Me.clcStockMinimo = New Dipros.Editors.TINCalcEdit
        Me.lblStockMinimo = New System.Windows.Forms.Label
        Me.clcStockMaximo = New Dipros.Editors.TINCalcEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.chkligero_uso = New DevExpress.XtraEditors.CheckEdit
        Me.txtDescripcion = New DevExpress.XtraEditors.MemoEdit
        Me.tmaPrecios = New Dipros.Windows.TINMaster
        Me.grPrecios = New DevExpress.XtraGrid.GridControl
        Me.grvPrecios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcprecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rptValor = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcNombrePrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcterminacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioexpo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcControl = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcRecalcular = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModificable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtDescripcionCorta = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.grUbicaciones = New DevExpress.XtraGrid.GridControl
        Me.grvUbicaciones = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUbicacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaUbicaciones = New Dipros.Windows.TINMaster
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.clcArticulo = New Dipros.Editors.TINCalcEdit
        Me.clcMesesGarantia = New Dipros.Editors.TINCalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpTama�oEtiqueta = New Dipros.Editors.TINMultiLookup
        Me.lblTama�oEtiquetas = New System.Windows.Forms.Label
        Me.txtCapacidad = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.tlbKardex = New System.Windows.Forms.ToolBarButton
        Me.txtmodelo = New DevExpress.XtraEditors.TextEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkNoIdentificable = New DevExpress.XtraEditors.CheckEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.clcUltimoCostoSinFlete = New Dipros.Editors.TINCalcEdit
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.nvrDatosGenerales = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrPrecio = New DevExpress.XtraNavBar.NavBarItem
        Me.nvrFoto = New DevExpress.XtraNavBar.NavBarItem
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.grExistenciasSucursales = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grvClaveSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rptExistenciasSucursales = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFisicaCalculada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEntregarPedidoFabrica = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.grExistencias = New DevExpress.XtraGrid.GridControl
        Me.grvExistencias = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcBodegaExistencias = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombrebodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFisica = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rptCantidades = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcPorSurtir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTransito = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEntregar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcReparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcVistas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGarantia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfisica_calculada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEntregarPedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnVerDocumentos = New DevExpress.XtraEditors.SimpleButton
        Me.tlbCambioPrecios = New System.Windows.Forms.ToolBarButton
        Me.txtmarca = New DevExpress.XtraEditors.TextEdit
        Me.txtclasificacion = New DevExpress.XtraEditors.TextEdit
        Me.lblMarca = New System.Windows.Forms.Label
        Me.lblClasificacion = New System.Windows.Forms.Label
        Me.tblMegaCred = New System.Windows.Forms.ToolBarButton
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.txtDescripcionEspecial = New DevExpress.XtraEditors.MemoEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.nvrDatosEspeciales = New DevExpress.XtraNavBar.NavBarItem
        CType(Me.txtCodigo_Barras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcUltimo_Costo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteUltima_Compra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImpuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImpuesto_Suntuario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkImportacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDescontinuado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManeja_Series.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEntrega_Domicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecio_Lista.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCajas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.clcCostoPedidoFabrica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPedidoFabrica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRegalo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNoResurtir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRequiereArmado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRequiereInstalacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcStockMinimo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcStockMaximo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkligero_uso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptValor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcionCorta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grUbicaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvUbicaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.clcArticulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMesesGarantia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCapacidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNoIdentificable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcUltimoCostoSinFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grExistenciasSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptExistenciasSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.grExistencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvExistencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptCantidades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtmarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtclasificacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.txtDescripcionEspecial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tlbKardex, Me.tlbCambioPrecios, Me.tblMegaCred})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(34042, 28)
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(392, 16)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 6
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Art�culo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCodigo_Barras
        '
        Me.lblCodigo_Barras.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCodigo_Barras.AutoSize = True
        Me.lblCodigo_Barras.Location = New System.Drawing.Point(528, 16)
        Me.lblCodigo_Barras.Name = "lblCodigo_Barras"
        Me.lblCodigo_Barras.Size = New System.Drawing.Size(103, 16)
        Me.lblCodigo_Barras.TabIndex = 8
        Me.lblCodigo_Barras.Tag = ""
        Me.lblCodigo_Barras.Text = "C�digo de &barras:"
        Me.lblCodigo_Barras.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCodigo_Barras
        '
        Me.txtCodigo_Barras.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCodigo_Barras.EditValue = ""
        Me.txtCodigo_Barras.Location = New System.Drawing.Point(640, 16)
        Me.txtCodigo_Barras.Name = "txtCodigo_Barras"
        '
        'txtCodigo_Barras.Properties
        '
        Me.txtCodigo_Barras.Properties.Enabled = False
        Me.txtCodigo_Barras.Properties.MaxLength = 20
        Me.txtCodigo_Barras.Size = New System.Drawing.Size(128, 20)
        Me.txtCodigo_Barras.TabIndex = 9
        Me.txtCodigo_Barras.Tag = "codigo_barras"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(40, 112)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 16
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(24, 16)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 0
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "Depar&tamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = True
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(120, 16)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(360, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = True
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(234, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 1
        Me.lkpDepartamento.Tag = "Departamento"
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "Departamento"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(69, 40)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 2
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = True
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(120, 40)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(290, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = True
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(234, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 3
        Me.lkpGrupo.Tag = "Grupo"
        Me.lkpGrupo.ToolTip = Nothing
        Me.lkpGrupo.ValueMember = "Grupo"
        '
        'lblUnidad
        '
        Me.lblUnidad.AutoSize = True
        Me.lblUnidad.Location = New System.Drawing.Point(65, 168)
        Me.lblUnidad.Name = "lblUnidad"
        Me.lblUnidad.Size = New System.Drawing.Size(47, 16)
        Me.lblUnidad.TabIndex = 19
        Me.lblUnidad.Tag = ""
        Me.lblUnidad.Text = "&Unidad:"
        Me.lblUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpUnidad
        '
        Me.lkpUnidad.AllowAdd = False
        Me.lkpUnidad.AutoReaload = True
        Me.lkpUnidad.DataSource = Nothing
        Me.lkpUnidad.DefaultSearchField = ""
        Me.lkpUnidad.DisplayMember = "descripcion"
        Me.lkpUnidad.EditValue = Nothing
        Me.lkpUnidad.Filtered = False
        Me.lkpUnidad.InitValue = Nothing
        Me.lkpUnidad.Location = New System.Drawing.Point(120, 168)
        Me.lkpUnidad.MultiSelect = False
        Me.lkpUnidad.Name = "lkpUnidad"
        Me.lkpUnidad.NullText = ""
        Me.lkpUnidad.PopupWidth = CType(290, Long)
        Me.lkpUnidad.ReadOnlyControl = False
        Me.lkpUnidad.Required = True
        Me.lkpUnidad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpUnidad.SearchMember = ""
        Me.lkpUnidad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpUnidad.SelectAll = False
        Me.lkpUnidad.Size = New System.Drawing.Size(234, 20)
        Me.lkpUnidad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpUnidad.TabIndex = 20
        Me.lkpUnidad.Tag = "Unidad"
        Me.lkpUnidad.ToolTip = Nothing
        Me.lkpUnidad.ValueMember = "Unidad"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(47, 192)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(65, 16)
        Me.lblProveedor.TabIndex = 21
        Me.lblProveedor.Tag = ""
        Me.lblProveedor.Text = "Prov&eedor:"
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = True
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(120, 192)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = ""
        Me.lkpProveedor.PopupWidth = CType(410, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = True
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = False
        Me.lkpProveedor.Size = New System.Drawing.Size(234, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 22
        Me.lkpProveedor.Tag = "Proveedor"
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "Proveedor"
        '
        'lblUltimo_Costo
        '
        Me.lblUltimo_Costo.AutoSize = True
        Me.lblUltimo_Costo.Location = New System.Drawing.Point(588, 169)
        Me.lblUltimo_Costo.Name = "lblUltimo_Costo"
        Me.lblUltimo_Costo.Size = New System.Drawing.Size(78, 16)
        Me.lblUltimo_Costo.TabIndex = 25
        Me.lblUltimo_Costo.Tag = ""
        Me.lblUltimo_Costo.Text = "�ltimo costo:"
        Me.lblUltimo_Costo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcUltimo_Costo
        '
        Me.clcUltimo_Costo.EditValue = "0"
        Me.clcUltimo_Costo.Location = New System.Drawing.Point(672, 168)
        Me.clcUltimo_Costo.MaxValue = 0
        Me.clcUltimo_Costo.MinValue = 0
        Me.clcUltimo_Costo.Name = "clcUltimo_Costo"
        '
        'clcUltimo_Costo.Properties
        '
        Me.clcUltimo_Costo.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcUltimo_Costo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimo_Costo.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcUltimo_Costo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimo_Costo.Properties.Enabled = False
        Me.clcUltimo_Costo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcUltimo_Costo.Properties.Precision = 2
        Me.clcUltimo_Costo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcUltimo_Costo.Size = New System.Drawing.Size(96, 19)
        Me.clcUltimo_Costo.TabIndex = 26
        Me.clcUltimo_Costo.Tag = "ultimo_costo"
        '
        'lblUltima_Compra
        '
        Me.lblUltima_Compra.AutoSize = True
        Me.lblUltima_Compra.Location = New System.Drawing.Point(384, 41)
        Me.lblUltima_Compra.Name = "lblUltima_Compra"
        Me.lblUltima_Compra.Size = New System.Drawing.Size(90, 16)
        Me.lblUltima_Compra.TabIndex = 23
        Me.lblUltima_Compra.Tag = ""
        Me.lblUltima_Compra.Text = "�&ltima compra:"
        Me.lblUltima_Compra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteUltima_Compra
        '
        Me.dteUltima_Compra.EditValue = "01/03/2006"
        Me.dteUltima_Compra.Location = New System.Drawing.Point(480, 40)
        Me.dteUltima_Compra.Name = "dteUltima_Compra"
        '
        'dteUltima_Compra.Properties
        '
        Me.dteUltima_Compra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteUltima_Compra.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteUltima_Compra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteUltima_Compra.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteUltima_Compra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteUltima_Compra.Properties.Enabled = False
        Me.dteUltima_Compra.Size = New System.Drawing.Size(95, 23)
        Me.dteUltima_Compra.TabIndex = 24
        Me.dteUltima_Compra.Tag = "ultima_compra"
        '
        'lblImpuesto
        '
        Me.lblImpuesto.AutoSize = True
        Me.lblImpuesto.Location = New System.Drawing.Point(70, 32)
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Size = New System.Drawing.Size(61, 16)
        Me.lblImpuesto.TabIndex = 2
        Me.lblImpuesto.Tag = ""
        Me.lblImpuesto.Text = "&Impuesto:"
        Me.lblImpuesto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImpuesto
        '
        Me.clcImpuesto.EditValue = "0"
        Me.clcImpuesto.Location = New System.Drawing.Point(136, 30)
        Me.clcImpuesto.MaxValue = 0
        Me.clcImpuesto.MinValue = 0
        Me.clcImpuesto.Name = "clcImpuesto"
        '
        'clcImpuesto.Properties
        '
        Me.clcImpuesto.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcImpuesto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImpuesto.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcImpuesto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImpuesto.Properties.MaskData.EditMask = "########0.00"
        Me.clcImpuesto.Properties.Precision = 18
        Me.clcImpuesto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImpuesto.Size = New System.Drawing.Size(80, 19)
        Me.clcImpuesto.TabIndex = 3
        Me.clcImpuesto.Tag = "impuesto"
        '
        'lblImpuesto_Suntuario
        '
        Me.lblImpuesto_Suntuario.AutoSize = True
        Me.lblImpuesto_Suntuario.Location = New System.Drawing.Point(12, 52)
        Me.lblImpuesto_Suntuario.Name = "lblImpuesto_Suntuario"
        Me.lblImpuesto_Suntuario.Size = New System.Drawing.Size(119, 16)
        Me.lblImpuesto_Suntuario.TabIndex = 4
        Me.lblImpuesto_Suntuario.Tag = ""
        Me.lblImpuesto_Suntuario.Text = "Impuesto &Suntuario:"
        Me.lblImpuesto_Suntuario.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImpuesto_Suntuario
        '
        Me.clcImpuesto_Suntuario.EditValue = "0"
        Me.clcImpuesto_Suntuario.Location = New System.Drawing.Point(136, 50)
        Me.clcImpuesto_Suntuario.MaxValue = 0
        Me.clcImpuesto_Suntuario.MinValue = 0
        Me.clcImpuesto_Suntuario.Name = "clcImpuesto_Suntuario"
        '
        'clcImpuesto_Suntuario.Properties
        '
        Me.clcImpuesto_Suntuario.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcImpuesto_Suntuario.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImpuesto_Suntuario.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcImpuesto_Suntuario.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImpuesto_Suntuario.Properties.MaskData.EditMask = "########0.00"
        Me.clcImpuesto_Suntuario.Properties.Precision = 18
        Me.clcImpuesto_Suntuario.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImpuesto_Suntuario.Size = New System.Drawing.Size(80, 19)
        Me.clcImpuesto_Suntuario.TabIndex = 5
        Me.clcImpuesto_Suntuario.Tag = "impuesto_suntuario"
        '
        'chkImportacion
        '
        Me.chkImportacion.Location = New System.Drawing.Point(10, 13)
        Me.chkImportacion.Name = "chkImportacion"
        '
        'chkImportacion.Properties
        '
        Me.chkImportacion.Properties.Caption = "Importaci�n"
        Me.chkImportacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkImportacion.Size = New System.Drawing.Size(96, 19)
        Me.chkImportacion.TabIndex = 0
        Me.chkImportacion.Tag = "importacion"
        '
        'chkDescontinuado
        '
        Me.chkDescontinuado.Location = New System.Drawing.Point(10, 35)
        Me.chkDescontinuado.Name = "chkDescontinuado"
        '
        'chkDescontinuado.Properties
        '
        Me.chkDescontinuado.Properties.Caption = "Descontinuado"
        Me.chkDescontinuado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkDescontinuado.Size = New System.Drawing.Size(104, 19)
        Me.chkDescontinuado.TabIndex = 1
        Me.chkDescontinuado.Tag = "descontinuado"
        '
        'chkManeja_Series
        '
        Me.chkManeja_Series.Location = New System.Drawing.Point(10, 56)
        Me.chkManeja_Series.Name = "chkManeja_Series"
        '
        'chkManeja_Series.Properties
        '
        Me.chkManeja_Series.Properties.Caption = "Maneja Series"
        Me.chkManeja_Series.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkManeja_Series.Size = New System.Drawing.Size(112, 19)
        Me.chkManeja_Series.TabIndex = 2
        Me.chkManeja_Series.Tag = "maneja_series"
        '
        'chkEntrega_Domicilio
        '
        Me.chkEntrega_Domicilio.Location = New System.Drawing.Point(136, 13)
        Me.chkEntrega_Domicilio.Name = "chkEntrega_Domicilio"
        '
        'chkEntrega_Domicilio.Properties
        '
        Me.chkEntrega_Domicilio.Properties.Caption = "Entrega a Domicilio"
        Me.chkEntrega_Domicilio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkEntrega_Domicilio.Size = New System.Drawing.Size(120, 19)
        Me.chkEntrega_Domicilio.TabIndex = 3
        Me.chkEntrega_Domicilio.Tag = "entrega_domicilio"
        '
        'lblPrecio_Lista
        '
        Me.lblPrecio_Lista.AutoSize = True
        Me.lblPrecio_Lista.Location = New System.Drawing.Point(45, 12)
        Me.lblPrecio_Lista.Name = "lblPrecio_Lista"
        Me.lblPrecio_Lista.Size = New System.Drawing.Size(86, 16)
        Me.lblPrecio_Lista.TabIndex = 0
        Me.lblPrecio_Lista.Tag = ""
        Me.lblPrecio_Lista.Text = "Precio de lista:"
        Me.lblPrecio_Lista.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecio_Lista
        '
        Me.clcPrecio_Lista.EditValue = "0"
        Me.clcPrecio_Lista.Location = New System.Drawing.Point(136, 10)
        Me.clcPrecio_Lista.MaxValue = 0
        Me.clcPrecio_Lista.MinValue = 0
        Me.clcPrecio_Lista.Name = "clcPrecio_Lista"
        '
        'clcPrecio_Lista.Properties
        '
        Me.clcPrecio_Lista.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecio_Lista.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio_Lista.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecio_Lista.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio_Lista.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecio_Lista.Properties.Precision = 2
        Me.clcPrecio_Lista.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecio_Lista.Size = New System.Drawing.Size(80, 19)
        Me.clcPrecio_Lista.TabIndex = 1
        Me.clcPrecio_Lista.Tag = "precio_lista"
        '
        'lblCajas
        '
        Me.lblCajas.AutoSize = True
        Me.lblCajas.Location = New System.Drawing.Point(92, 72)
        Me.lblCajas.Name = "lblCajas"
        Me.lblCajas.Size = New System.Drawing.Size(39, 16)
        Me.lblCajas.TabIndex = 6
        Me.lblCajas.Tag = ""
        Me.lblCajas.Text = "Ca&jas:"
        Me.lblCajas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCajas
        '
        Me.clcCajas.EditValue = "0"
        Me.clcCajas.Location = New System.Drawing.Point(136, 70)
        Me.clcCajas.MaxValue = 0
        Me.clcCajas.MinValue = 0
        Me.clcCajas.Name = "clcCajas"
        '
        'clcCajas.Properties
        '
        Me.clcCajas.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCajas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCajas.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCajas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCajas.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCajas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCajas.Size = New System.Drawing.Size(80, 19)
        Me.clcCajas.TabIndex = 7
        Me.clcCajas.Tag = "cajas"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.lblBodega)
        Me.GroupBox1.Controls.Add(Me.lkpBodega)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.clcCostoPedidoFabrica)
        Me.GroupBox1.Controls.Add(Me.chkPedidoFabrica)
        Me.GroupBox1.Controls.Add(Me.chkRegalo)
        Me.GroupBox1.Controls.Add(Me.chkImportacion)
        Me.GroupBox1.Controls.Add(Me.chkManeja_Series)
        Me.GroupBox1.Controls.Add(Me.chkEntrega_Domicilio)
        Me.GroupBox1.Controls.Add(Me.chkDescontinuado)
        Me.GroupBox1.Controls.Add(Me.chkNoResurtir)
        Me.GroupBox1.Controls.Add(Me.chkRequiereArmado)
        Me.GroupBox1.Controls.Add(Me.chkRequiereInstalacion)
        Me.GroupBox1.Controls.Add(Me.clcStockMinimo)
        Me.GroupBox1.Controls.Add(Me.lblStockMinimo)
        Me.GroupBox1.Controls.Add(Me.clcStockMaximo)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 216)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(760, 80)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(560, 56)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(30, 16)
        Me.lblBodega.TabIndex = 37
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "BPF:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(592, 56)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(160, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 38
        Me.lkpBodega.Tag = "bodega_pedido_fabrica"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(560, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(30, 16)
        Me.Label8.TabIndex = 36
        Me.Label8.Tag = ""
        Me.Label8.Text = "CPF:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCostoPedidoFabrica
        '
        Me.clcCostoPedidoFabrica.EditValue = "0"
        Me.clcCostoPedidoFabrica.Location = New System.Drawing.Point(592, 32)
        Me.clcCostoPedidoFabrica.MaxValue = 0
        Me.clcCostoPedidoFabrica.MinValue = 0
        Me.clcCostoPedidoFabrica.Name = "clcCostoPedidoFabrica"
        '
        'clcCostoPedidoFabrica.Properties
        '
        Me.clcCostoPedidoFabrica.Properties.DisplayFormat.FormatString = "c2"
        Me.clcCostoPedidoFabrica.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCostoPedidoFabrica.Properties.EditFormat.FormatString = "n2"
        Me.clcCostoPedidoFabrica.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCostoPedidoFabrica.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCostoPedidoFabrica.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCostoPedidoFabrica.Size = New System.Drawing.Size(64, 19)
        Me.clcCostoPedidoFabrica.TabIndex = 35
        Me.clcCostoPedidoFabrica.Tag = "costo_pedido_fabrica"
        '
        'chkPedidoFabrica
        '
        Me.chkPedidoFabrica.Location = New System.Drawing.Point(592, 13)
        Me.chkPedidoFabrica.Name = "chkPedidoFabrica"
        '
        'chkPedidoFabrica.Properties
        '
        Me.chkPedidoFabrica.Properties.Caption = "Ped. F�brica"
        Me.chkPedidoFabrica.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkPedidoFabrica.Size = New System.Drawing.Size(88, 19)
        Me.chkPedidoFabrica.TabIndex = 34
        Me.chkPedidoFabrica.Tag = "pedido_fabrica"
        '
        'chkRegalo
        '
        Me.chkRegalo.Location = New System.Drawing.Point(136, 56)
        Me.chkRegalo.Name = "chkRegalo"
        '
        'chkRegalo.Properties
        '
        Me.chkRegalo.Properties.Caption = "Art�culo de Regalo"
        Me.chkRegalo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkRegalo.Size = New System.Drawing.Size(121, 19)
        Me.chkRegalo.TabIndex = 5
        Me.chkRegalo.Tag = "articulo_regalo"
        '
        'chkNoResurtir
        '
        Me.chkNoResurtir.Location = New System.Drawing.Point(136, 35)
        Me.chkNoResurtir.Name = "chkNoResurtir"
        '
        'chkNoResurtir.Properties
        '
        Me.chkNoResurtir.Properties.Caption = "No Resurtir"
        Me.chkNoResurtir.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkNoResurtir.Size = New System.Drawing.Size(88, 19)
        Me.chkNoResurtir.TabIndex = 4
        Me.chkNoResurtir.Tag = "no_resurtir"
        '
        'chkRequiereArmado
        '
        Me.chkRequiereArmado.Location = New System.Drawing.Point(280, 13)
        Me.chkRequiereArmado.Name = "chkRequiereArmado"
        '
        'chkRequiereArmado.Properties
        '
        Me.chkRequiereArmado.Properties.Caption = "Requiere Armado"
        Me.chkRequiereArmado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkRequiereArmado.Size = New System.Drawing.Size(118, 19)
        Me.chkRequiereArmado.TabIndex = 6
        Me.chkRequiereArmado.Tag = "requiere_armado"
        '
        'chkRequiereInstalacion
        '
        Me.chkRequiereInstalacion.Location = New System.Drawing.Point(280, 35)
        Me.chkRequiereInstalacion.Name = "chkRequiereInstalacion"
        '
        'chkRequiereInstalacion.Properties
        '
        Me.chkRequiereInstalacion.Properties.Caption = "Requiere Instalaci�n"
        Me.chkRequiereInstalacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkRequiereInstalacion.Size = New System.Drawing.Size(131, 19)
        Me.chkRequiereInstalacion.TabIndex = 6
        Me.chkRequiereInstalacion.Tag = "requiere_instalacion"
        '
        'clcStockMinimo
        '
        Me.clcStockMinimo.EditValue = "0"
        Me.clcStockMinimo.Location = New System.Drawing.Point(488, 10)
        Me.clcStockMinimo.MaxValue = 0
        Me.clcStockMinimo.MinValue = 0
        Me.clcStockMinimo.Name = "clcStockMinimo"
        '
        'clcStockMinimo.Properties
        '
        Me.clcStockMinimo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcStockMinimo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcStockMinimo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcStockMinimo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcStockMinimo.Size = New System.Drawing.Size(64, 19)
        Me.clcStockMinimo.TabIndex = 30
        Me.clcStockMinimo.Tag = "stock_minimo"
        '
        'lblStockMinimo
        '
        Me.lblStockMinimo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStockMinimo.AutoSize = True
        Me.lblStockMinimo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStockMinimo.Location = New System.Drawing.Point(408, 14)
        Me.lblStockMinimo.Name = "lblStockMinimo"
        Me.lblStockMinimo.Size = New System.Drawing.Size(75, 16)
        Me.lblStockMinimo.TabIndex = 29
        Me.lblStockMinimo.Tag = ""
        Me.lblStockMinimo.Text = "Stock M�nimo:"
        Me.lblStockMinimo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcStockMaximo
        '
        Me.clcStockMaximo.EditValue = "0"
        Me.clcStockMaximo.Location = New System.Drawing.Point(488, 32)
        Me.clcStockMaximo.MaxValue = 0
        Me.clcStockMaximo.MinValue = 0
        Me.clcStockMaximo.Name = "clcStockMaximo"
        '
        'clcStockMaximo.Properties
        '
        Me.clcStockMaximo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcStockMaximo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcStockMaximo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcStockMaximo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcStockMaximo.Size = New System.Drawing.Size(64, 19)
        Me.clcStockMaximo.TabIndex = 32
        Me.clcStockMaximo.Tag = "stock_maximo"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(408, 34)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 16)
        Me.Label6.TabIndex = 31
        Me.Label6.Tag = ""
        Me.Label6.Text = "Stock M�ximo:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkligero_uso
        '
        Me.chkligero_uso.Location = New System.Drawing.Point(688, 192)
        Me.chkligero_uso.Name = "chkligero_uso"
        '
        'chkligero_uso.Properties
        '
        Me.chkligero_uso.Properties.Caption = "Ligero Uso"
        Me.chkligero_uso.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkligero_uso.Size = New System.Drawing.Size(80, 19)
        Me.chkligero_uso.TabIndex = 29
        Me.chkligero_uso.Tag = "ligero_uso"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(120, 112)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(392, 48)
        Me.txtDescripcion.TabIndex = 17
        Me.txtDescripcion.Tag = "descripcion"
        '
        'tmaPrecios
        '
        Me.tmaPrecios.BackColor = System.Drawing.Color.White
        Me.tmaPrecios.CanDelete = False
        Me.tmaPrecios.CanInsert = False
        Me.tmaPrecios.CanUpdate = True
        Me.tmaPrecios.Grid = Me.grPrecios
        Me.tmaPrecios.Location = New System.Drawing.Point(8, 8)
        Me.tmaPrecios.Name = "tmaPrecios"
        Me.tmaPrecios.Size = New System.Drawing.Size(376, 27)
        Me.tmaPrecios.TabIndex = 30
        Me.tmaPrecios.Title = "Precios"
        Me.tmaPrecios.UpdateTitle = "un precio"
        '
        'grPrecios
        '
        '
        'grPrecios.EmbeddedNavigator
        '
        Me.grPrecios.EmbeddedNavigator.Name = ""
        Me.grPrecios.Location = New System.Drawing.Point(8, 32)
        Me.grPrecios.MainView = Me.grvPrecios
        Me.grPrecios.Name = "grPrecios"
        Me.grPrecios.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptValor})
        Me.grPrecios.Size = New System.Drawing.Size(376, 248)
        Me.grPrecios.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPrecios.TabIndex = 31
        Me.grPrecios.TabStop = False
        Me.grPrecios.Text = "GridControl1"
        '
        'grvPrecios
        '
        Me.grvPrecios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcprecio, Me.grcNombrePrecio, Me.grcDescuento, Me.grcPrecioVenta, Me.grcterminacion, Me.grcPrecioexpo, Me.grcControl, Me.grcRecalcular, Me.grcModificable})
        Me.grvPrecios.GridControl = Me.grPrecios
        Me.grvPrecios.GroupPanelText = "Precios"
        Me.grvPrecios.Name = "grvPrecios"
        Me.grvPrecios.OptionsBehavior.Editable = False
        Me.grvPrecios.OptionsView.ShowGroupPanel = False
        Me.grvPrecios.OptionsView.ShowIndicator = False
        '
        'grcprecio
        '
        Me.grcprecio.Caption = "Clave Precio"
        Me.grcprecio.ColumnEdit = Me.rptValor
        Me.grcprecio.FieldName = "precio"
        Me.grcprecio.Name = "grcprecio"
        Me.grcprecio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'rptValor
        '
        Me.rptValor.AutoHeight = False
        Me.rptValor.Name = "rptValor"
        '
        'grcNombrePrecio
        '
        Me.grcNombrePrecio.Caption = "Precio"
        Me.grcNombrePrecio.FieldName = "nombre_precio"
        Me.grcNombrePrecio.Name = "grcNombrePrecio"
        Me.grcNombrePrecio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombrePrecio.VisibleIndex = 0
        Me.grcNombrePrecio.Width = 180
        '
        'grcDescuento
        '
        Me.grcDescuento.Caption = "% Descuento"
        Me.grcDescuento.ColumnEdit = Me.rptValor
        Me.grcDescuento.DisplayFormat.FormatString = "n2"
        Me.grcDescuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDescuento.FieldName = "porcentaje_descuento"
        Me.grcDescuento.Name = "grcDescuento"
        Me.grcDescuento.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescuento.VisibleIndex = 1
        Me.grcDescuento.Width = 91
        '
        'grcPrecioVenta
        '
        Me.grcPrecioVenta.Caption = "Precio Venta"
        Me.grcPrecioVenta.DisplayFormat.FormatString = "c2"
        Me.grcPrecioVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioVenta.FieldName = "precio_venta"
        Me.grcPrecioVenta.Name = "grcPrecioVenta"
        Me.grcPrecioVenta.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioVenta.VisibleIndex = 2
        Me.grcPrecioVenta.Width = 85
        '
        'grcterminacion
        '
        Me.grcterminacion.Caption = "terminacion"
        Me.grcterminacion.FieldName = "terminacion_precio"
        Me.grcterminacion.Name = "grcterminacion"
        '
        'grcPrecioexpo
        '
        Me.grcPrecioexpo.Caption = "precio expo"
        Me.grcPrecioexpo.FieldName = "precio_expo"
        Me.grcPrecioexpo.Name = "grcPrecioexpo"
        '
        'grcControl
        '
        Me.grcControl.Caption = "GridColumn1"
        Me.grcControl.FieldName = "Control"
        Me.grcControl.Name = "grcControl"
        '
        'grcRecalcular
        '
        Me.grcRecalcular.Caption = "Recalcular"
        Me.grcRecalcular.FieldName = "recalcular"
        Me.grcRecalcular.Name = "grcRecalcular"
        Me.grcRecalcular.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcModificable
        '
        Me.grcModificable.Caption = "Modificable"
        Me.grcModificable.FieldName = "modificable"
        Me.grcModificable.Name = "grcModificable"
        Me.grcModificable.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'txtDescripcionCorta
        '
        Me.txtDescripcionCorta.EditValue = ""
        Me.txtDescripcionCorta.Location = New System.Drawing.Point(120, 88)
        Me.txtDescripcionCorta.Name = "txtDescripcionCorta"
        '
        'txtDescripcionCorta.Properties
        '
        Me.txtDescripcionCorta.Properties.MaxLength = 50
        Me.txtDescripcionCorta.Size = New System.Drawing.Size(392, 20)
        Me.txtDescripcionCorta.TabIndex = 15
        Me.txtDescripcionCorta.Tag = "descripcion_corta"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 16)
        Me.Label2.TabIndex = 14
        Me.Label2.Tag = ""
        Me.Label2.Text = "Descripci�&n Corta:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grUbicaciones
        '
        Me.grUbicaciones.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grUbicaciones.EmbeddedNavigator
        '
        Me.grUbicaciones.EmbeddedNavigator.Name = ""
        Me.grUbicaciones.Location = New System.Drawing.Point(400, 32)
        Me.grUbicaciones.MainView = Me.grvUbicaciones
        Me.grUbicaciones.Name = "grUbicaciones"
        Me.grUbicaciones.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1})
        Me.grUbicaciones.Size = New System.Drawing.Size(376, 248)
        Me.grUbicaciones.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grUbicaciones.TabIndex = 33
        Me.grUbicaciones.TabStop = False
        '
        'grvUbicaciones
        '
        Me.grvUbicaciones.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveBodega, Me.grcBodega, Me.grcUbicacion})
        Me.grvUbicaciones.GridControl = Me.grUbicaciones
        Me.grvUbicaciones.GroupPanelText = "Precios"
        Me.grvUbicaciones.Name = "grvUbicaciones"
        Me.grvUbicaciones.OptionsBehavior.Editable = False
        Me.grvUbicaciones.OptionsView.ShowGroupPanel = False
        Me.grvUbicaciones.OptionsView.ShowIndicator = False
        '
        'grcClaveBodega
        '
        Me.grcClaveBodega.Caption = "Clave Bodega"
        Me.grcClaveBodega.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcClaveBodega.FieldName = "bodega"
        Me.grcClaveBodega.Name = "grcClaveBodega"
        Me.grcClaveBodega.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.FieldName = "nombre_bodega"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodega.VisibleIndex = 0
        Me.grcBodega.Width = 150
        '
        'grcUbicacion
        '
        Me.grcUbicacion.Caption = "Ubicaci�n"
        Me.grcUbicacion.FieldName = "ubicacion"
        Me.grcUbicacion.Name = "grcUbicacion"
        Me.grcUbicacion.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcUbicacion.VisibleIndex = 1
        Me.grcUbicacion.Width = 216
        '
        'tmaUbicaciones
        '
        Me.tmaUbicaciones.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tmaUbicaciones.BackColor = System.Drawing.Color.White
        Me.tmaUbicaciones.CanDelete = True
        Me.tmaUbicaciones.CanInsert = True
        Me.tmaUbicaciones.CanUpdate = False
        Me.tmaUbicaciones.Grid = Me.grUbicaciones
        Me.tmaUbicaciones.Location = New System.Drawing.Point(400, 8)
        Me.tmaUbicaciones.Name = "tmaUbicaciones"
        Me.tmaUbicaciones.Size = New System.Drawing.Size(376, 27)
        Me.tmaUbicaciones.TabIndex = 32
        Me.tmaUbicaciones.Title = "Ubicaciones"
        Me.tmaUbicaciones.UpdateTitle = "un Registro"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.Window
        Me.GroupBox2.Controls.Add(Me.lblPrecio_Lista)
        Me.GroupBox2.Controls.Add(Me.clcPrecio_Lista)
        Me.GroupBox2.Controls.Add(Me.lblImpuesto)
        Me.GroupBox2.Controls.Add(Me.clcImpuesto)
        Me.GroupBox2.Controls.Add(Me.lblImpuesto_Suntuario)
        Me.GroupBox2.Controls.Add(Me.clcImpuesto_Suntuario)
        Me.GroupBox2.Controls.Add(Me.lblCajas)
        Me.GroupBox2.Controls.Add(Me.clcCajas)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(536, 64)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(232, 96)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        '
        'clcArticulo
        '
        Me.clcArticulo.EditValue = "0"
        Me.clcArticulo.Location = New System.Drawing.Point(448, 16)
        Me.clcArticulo.MaxValue = 0
        Me.clcArticulo.MinValue = 0
        Me.clcArticulo.Name = "clcArticulo"
        '
        'clcArticulo.Properties
        '
        Me.clcArticulo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcArticulo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcArticulo.Properties.Enabled = False
        Me.clcArticulo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcArticulo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcArticulo.Size = New System.Drawing.Size(64, 19)
        Me.clcArticulo.TabIndex = 7
        Me.clcArticulo.Tag = "articulo"
        '
        'clcMesesGarantia
        '
        Me.clcMesesGarantia.EditValue = "0"
        Me.clcMesesGarantia.Location = New System.Drawing.Point(704, 40)
        Me.clcMesesGarantia.MaxValue = 0
        Me.clcMesesGarantia.MinValue = 0
        Me.clcMesesGarantia.Name = "clcMesesGarantia"
        '
        'clcMesesGarantia.Properties
        '
        Me.clcMesesGarantia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMesesGarantia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMesesGarantia.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcMesesGarantia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMesesGarantia.Size = New System.Drawing.Size(64, 19)
        Me.clcMesesGarantia.TabIndex = 11
        Me.clcMesesGarantia.Tag = "meses_garantia"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(592, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Tag = ""
        Me.Label3.Text = "Meses de Garant�a:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpTama�oEtiqueta
        '
        Me.lkpTama�oEtiqueta.AllowAdd = False
        Me.lkpTama�oEtiqueta.AutoReaload = False
        Me.lkpTama�oEtiqueta.DataSource = Nothing
        Me.lkpTama�oEtiqueta.DefaultSearchField = ""
        Me.lkpTama�oEtiqueta.DisplayMember = "descripcion"
        Me.lkpTama�oEtiqueta.EditValue = Nothing
        Me.lkpTama�oEtiqueta.Filtered = False
        Me.lkpTama�oEtiqueta.InitValue = Nothing
        Me.lkpTama�oEtiqueta.Location = New System.Drawing.Point(496, 192)
        Me.lkpTama�oEtiqueta.MultiSelect = False
        Me.lkpTama�oEtiqueta.Name = "lkpTama�oEtiqueta"
        Me.lkpTama�oEtiqueta.NullText = ""
        Me.lkpTama�oEtiqueta.PopupWidth = CType(300, Long)
        Me.lkpTama�oEtiqueta.ReadOnlyControl = False
        Me.lkpTama�oEtiqueta.Required = True
        Me.lkpTama�oEtiqueta.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTama�oEtiqueta.SearchMember = ""
        Me.lkpTama�oEtiqueta.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTama�oEtiqueta.SelectAll = True
        Me.lkpTama�oEtiqueta.Size = New System.Drawing.Size(184, 20)
        Me.lkpTama�oEtiqueta.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpTama�oEtiqueta.TabIndex = 28
        Me.lkpTama�oEtiqueta.Tag = "tamanio_etiqueta_precios"
        Me.lkpTama�oEtiqueta.ToolTip = Nothing
        Me.lkpTama�oEtiqueta.ValueMember = "tamanio"
        '
        'lblTama�oEtiquetas
        '
        Me.lblTama�oEtiquetas.AutoSize = True
        Me.lblTama�oEtiquetas.Location = New System.Drawing.Point(365, 194)
        Me.lblTama�oEtiquetas.Name = "lblTama�oEtiquetas"
        Me.lblTama�oEtiquetas.Size = New System.Drawing.Size(126, 16)
        Me.lblTama�oEtiquetas.TabIndex = 27
        Me.lblTama�oEtiquetas.Text = "Tama&�o de Etiquetas:"
        '
        'txtCapacidad
        '
        Me.txtCapacidad.EditValue = ""
        Me.txtCapacidad.Location = New System.Drawing.Point(296, 64)
        Me.txtCapacidad.Name = "txtCapacidad"
        '
        'txtCapacidad.Properties
        '
        Me.txtCapacidad.Properties.MaxLength = 15
        Me.txtCapacidad.Size = New System.Drawing.Size(112, 20)
        Me.txtCapacidad.TabIndex = 13
        Me.txtCapacidad.Tag = "capacidad"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(261, 66)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 16)
        Me.Label4.TabIndex = 12
        Me.Label4.Tag = ""
        Me.Label4.Text = "Cap.:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tlbKardex
        '
        Me.tlbKardex.ImageIndex = 6
        Me.tlbKardex.Text = "Imprimir Kardex"
        '
        'txtmodelo
        '
        Me.txtmodelo.EditValue = ""
        Me.txtmodelo.Location = New System.Drawing.Point(120, 64)
        Me.txtmodelo.Name = "txtmodelo"
        '
        'txtmodelo.Properties
        '
        Me.txtmodelo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtmodelo.Properties.MaxLength = 18
        Me.txtmodelo.Size = New System.Drawing.Size(130, 20)
        Me.txtmodelo.TabIndex = 5
        Me.txtmodelo.Tag = "modelo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(64, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Modelo:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkNoIdentificable
        '
        Me.chkNoIdentificable.Location = New System.Drawing.Point(416, 64)
        Me.chkNoIdentificable.Name = "chkNoIdentificable"
        '
        'chkNoIdentificable.Properties
        '
        Me.chkNoIdentificable.Properties.Caption = "No Identificable"
        Me.chkNoIdentificable.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkNoIdentificable.Properties.Enabled = False
        Me.chkNoIdentificable.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkNoIdentificable.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkNoIdentificable.Size = New System.Drawing.Size(120, 22)
        Me.chkNoIdentificable.TabIndex = 60
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(368, 169)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(125, 16)
        Me.Label7.TabIndex = 61
        Me.Label7.Tag = ""
        Me.Label7.Text = "�ltimo costo sin &flete:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcUltimoCostoSinFlete
        '
        Me.clcUltimoCostoSinFlete.EditValue = "0"
        Me.clcUltimoCostoSinFlete.Location = New System.Drawing.Point(496, 168)
        Me.clcUltimoCostoSinFlete.MaxValue = 0
        Me.clcUltimoCostoSinFlete.MinValue = 0
        Me.clcUltimoCostoSinFlete.Name = "clcUltimoCostoSinFlete"
        '
        'clcUltimoCostoSinFlete.Properties
        '
        Me.clcUltimoCostoSinFlete.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcUltimoCostoSinFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimoCostoSinFlete.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcUltimoCostoSinFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimoCostoSinFlete.Properties.Enabled = False
        Me.clcUltimoCostoSinFlete.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcUltimoCostoSinFlete.Properties.Precision = 2
        Me.clcUltimoCostoSinFlete.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcUltimoCostoSinFlete.Size = New System.Drawing.Size(77, 19)
        Me.clcUltimoCostoSinFlete.TabIndex = 62
        Me.clcUltimoCostoSinFlete.Tag = "ultimo_costo_sin_flete"
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvrPrecio, Me.nvrDatosGenerales, Me.nvrFoto, Me.nvrDatosEspeciales})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 32)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(88, 560)
        Me.NavBarControl1.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.NavBarControl1.TabIndex = 94
        Me.NavBarControl1.Text = "Articulos"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = ""
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosGenerales), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrPrecio), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrFoto), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvrDatosEspeciales)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'nvrDatosGenerales
        '
        Me.nvrDatosGenerales.Caption = "Datos Generales"
        Me.nvrDatosGenerales.LargeImageIndex = 0
        Me.nvrDatosGenerales.Name = "nvrDatosGenerales"
        '
        'nvrPrecio
        '
        Me.nvrPrecio.Caption = "Precios y Ubicaciones"
        Me.nvrPrecio.LargeImageIndex = 1
        Me.nvrPrecio.Name = "nvrPrecio"
        '
        'nvrFoto
        '
        Me.nvrFoto.Caption = "Imagen"
        Me.nvrFoto.Name = "nvrFoto"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lkpDepartamento)
        Me.Panel1.Controls.Add(Me.lkpUnidad)
        Me.Panel1.Controls.Add(Me.lblGrupo)
        Me.Panel1.Controls.Add(Me.txtDescripcionCorta)
        Me.Panel1.Controls.Add(Me.lkpProveedor)
        Me.Panel1.Controls.Add(Me.lblUltimo_Costo)
        Me.Panel1.Controls.Add(Me.clcUltimo_Costo)
        Me.Panel1.Controls.Add(Me.lblUltima_Compra)
        Me.Panel1.Controls.Add(Me.lkpGrupo)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.clcArticulo)
        Me.Panel1.Controls.Add(Me.clcMesesGarantia)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.lblUnidad)
        Me.Panel1.Controls.Add(Me.lblProveedor)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lkpTama�oEtiqueta)
        Me.Panel1.Controls.Add(Me.lblTama�oEtiquetas)
        Me.Panel1.Controls.Add(Me.txtCapacidad)
        Me.Panel1.Controls.Add(Me.txtmodelo)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.dteUltima_Compra)
        Me.Panel1.Controls.Add(Me.chkligero_uso)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.lblArticulo)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.chkNoIdentificable)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.clcUltimoCostoSinFlete)
        Me.Panel1.Controls.Add(Me.lblCodigo_Barras)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtCodigo_Barras)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Controls.Add(Me.lblDepartamento)
        Me.Panel1.Location = New System.Drawing.Point(88, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(784, 304)
        Me.Panel1.TabIndex = 95
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.tmaUbicaciones)
        Me.Panel2.Controls.Add(Me.tmaPrecios)
        Me.Panel2.Controls.Add(Me.grPrecios)
        Me.Panel2.Controls.Add(Me.grUbicaciones)
        Me.Panel2.Location = New System.Drawing.Point(88, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(784, 304)
        Me.Panel2.TabIndex = 96
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(96, 368)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(768, 216)
        Me.TabControl1.TabIndex = 101
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage2.Controls.Add(Me.grExistenciasSucursales)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(760, 190)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Sucursal"
        '
        'grExistenciasSucursales
        '
        Me.grExistenciasSucursales.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grExistenciasSucursales.EmbeddedNavigator
        '
        Me.grExistenciasSucursales.EmbeddedNavigator.Name = ""
        Me.grExistenciasSucursales.Location = New System.Drawing.Point(0, 0)
        Me.grExistenciasSucursales.MainView = Me.GridView1
        Me.grExistenciasSucursales.Name = "grExistenciasSucursales"
        Me.grExistenciasSucursales.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptExistenciasSucursales})
        Me.grExistenciasSucursales.Size = New System.Drawing.Size(760, 190)
        Me.grExistenciasSucursales.TabIndex = 0
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grvClaveSucursal, Me.grcNombreSucursal, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.grcFisicaCalculada, Me.grcEntregarPedidoFabrica})
        Me.GridView1.GridControl = Me.grExistenciasSucursales
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsMenu.EnableFooterMenu = False
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'grvClaveSucursal
        '
        Me.grvClaveSucursal.Caption = "Clave Sucursal"
        Me.grvClaveSucursal.FieldName = "sucursal"
        Me.grvClaveSucursal.Name = "grvClaveSucursal"
        Me.grvClaveSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursal.SummaryItem.DisplayFormat = "Totales"
        Me.grcNombreSucursal.SummaryItem.FieldName = "nombre_bodega"
        Me.grcNombreSucursal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom
        Me.grcNombreSucursal.VisibleIndex = 0
        Me.grcNombreSucursal.Width = 134
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Disponible"
        Me.GridColumn3.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn3.DisplayFormat.FormatString = "n0"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn3.FieldName = "fisica"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn3.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 58
        '
        'rptExistenciasSucursales
        '
        Me.rptExistenciasSucursales.AutoHeight = False
        Me.rptExistenciasSucursales.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rptExistenciasSucursales.DisplayFormat.FormatString = "n0"
        Me.rptExistenciasSucursales.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptExistenciasSucursales.EditFormat.FormatString = "n0"
        Me.rptExistenciasSucursales.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptExistenciasSucursales.Name = "rptExistenciasSucursales"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Por Surtir"
        Me.GridColumn4.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn4.DisplayFormat.FormatString = "n0"
        Me.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn4.FieldName = "por_surtir"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 63
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Tr�nsito"
        Me.GridColumn5.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn5.DisplayFormat.FormatString = "n0"
        Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn5.FieldName = "transito"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 67
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Entregar"
        Me.GridColumn6.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn6.DisplayFormat.FormatString = "n0"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn6.FieldName = "entregar"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn6.VisibleIndex = 5
        Me.GridColumn6.Width = 78
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Reparto"
        Me.GridColumn7.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn7.DisplayFormat.FormatString = "n0"
        Me.GridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn7.FieldName = "reparto"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn7.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn7.VisibleIndex = 7
        Me.GridColumn7.Width = 72
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Vistas"
        Me.GridColumn8.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn8.DisplayFormat.FormatString = "n0"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "vistas"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn8.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn8.VisibleIndex = 8
        Me.GridColumn8.Width = 63
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Garant�a"
        Me.GridColumn9.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn9.DisplayFormat.FormatString = "n0"
        Me.GridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn9.FieldName = "garantia"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn9.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn9.VisibleIndex = 9
        Me.GridColumn9.Width = 63
        '
        'grcFisicaCalculada
        '
        Me.grcFisicaCalculada.Caption = "F�sica"
        Me.grcFisicaCalculada.FieldName = "fisica_calculada"
        Me.grcFisicaCalculada.Name = "grcFisicaCalculada"
        Me.grcFisicaCalculada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFisicaCalculada.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcFisicaCalculada.VisibleIndex = 1
        Me.grcFisicaCalculada.Width = 52
        '
        'grcEntregarPedidoFabrica
        '
        Me.grcEntregarPedidoFabrica.Caption = "Pedido Fabrica"
        Me.grcEntregarPedidoFabrica.FieldName = "entregar_pedido_fabrica"
        Me.grcEntregarPedidoFabrica.Name = "grcEntregarPedidoFabrica"
        Me.grcEntregarPedidoFabrica.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEntregarPedidoFabrica.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcEntregarPedidoFabrica.VisibleIndex = 6
        Me.grcEntregarPedidoFabrica.Width = 96
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage1.Controls.Add(Me.grExistencias)
        Me.TabPage1.Controls.Add(Me.btnVerDocumentos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(760, 190)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Bodegas"
        Me.TabPage1.Visible = False
        '
        'grExistencias
        '
        Me.grExistencias.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grExistencias.EmbeddedNavigator
        '
        Me.grExistencias.EmbeddedNavigator.Name = ""
        Me.grExistencias.Location = New System.Drawing.Point(2, 2)
        Me.grExistencias.MainView = Me.grvExistencias
        Me.grExistencias.Name = "grExistencias"
        Me.grExistencias.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptCantidades})
        Me.grExistencias.Size = New System.Drawing.Size(752, 163)
        Me.grExistencias.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Info, System.Drawing.Color.Black, System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grExistencias.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte)), System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grExistencias.TabIndex = 34
        Me.grExistencias.TabStop = False
        Me.grExistencias.Text = "GridControl1"
        '
        'grvExistencias
        '
        Me.grvExistencias.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcBodegaExistencias, Me.grcNombrebodega, Me.grcFisica, Me.grcPorSurtir, Me.grcTransito, Me.grcEntregar, Me.grcReparto, Me.grcVistas, Me.grcGarantia, Me.grcfisica_calculada, Me.grcEntregarPedido})
        Me.grvExistencias.GridControl = Me.grExistencias
        Me.grvExistencias.Name = "grvExistencias"
        Me.grvExistencias.OptionsCustomization.AllowFilter = False
        Me.grvExistencias.OptionsCustomization.AllowGroup = False
        Me.grvExistencias.OptionsMenu.EnableFooterMenu = False
        Me.grvExistencias.OptionsView.ShowFooter = True
        Me.grvExistencias.OptionsView.ShowGroupPanel = False
        '
        'grcBodegaExistencias
        '
        Me.grcBodegaExistencias.Caption = "Clave Bodega"
        Me.grcBodegaExistencias.FieldName = "bodega"
        Me.grcBodegaExistencias.Name = "grcBodegaExistencias"
        Me.grcBodegaExistencias.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombrebodega
        '
        Me.grcNombrebodega.Caption = "Bodega"
        Me.grcNombrebodega.FieldName = "nombre_bodega"
        Me.grcNombrebodega.Name = "grcNombrebodega"
        Me.grcNombrebodega.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombrebodega.SummaryItem.DisplayFormat = "Totales"
        Me.grcNombrebodega.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom
        Me.grcNombrebodega.VisibleIndex = 0
        Me.grcNombrebodega.Width = 134
        '
        'grcFisica
        '
        Me.grcFisica.Caption = "Disponible"
        Me.grcFisica.ColumnEdit = Me.rptCantidades
        Me.grcFisica.DisplayFormat.FormatString = "n0"
        Me.grcFisica.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcFisica.FieldName = "fisica"
        Me.grcFisica.Name = "grcFisica"
        Me.grcFisica.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFisica.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcFisica.VisibleIndex = 2
        Me.grcFisica.Width = 71
        '
        'rptCantidades
        '
        Me.rptCantidades.AutoHeight = False
        Me.rptCantidades.DisplayFormat.FormatString = "n0"
        Me.rptCantidades.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptCantidades.EditFormat.FormatString = "n0"
        Me.rptCantidades.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptCantidades.Name = "rptCantidades"
        '
        'grcPorSurtir
        '
        Me.grcPorSurtir.Caption = "Por Surtir"
        Me.grcPorSurtir.ColumnEdit = Me.rptCantidades
        Me.grcPorSurtir.DisplayFormat.FormatString = "n0"
        Me.grcPorSurtir.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPorSurtir.FieldName = "por_surtir"
        Me.grcPorSurtir.Name = "grcPorSurtir"
        Me.grcPorSurtir.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPorSurtir.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcPorSurtir.VisibleIndex = 3
        Me.grcPorSurtir.Width = 74
        '
        'grcTransito
        '
        Me.grcTransito.Caption = "Tr�nsito"
        Me.grcTransito.ColumnEdit = Me.rptCantidades
        Me.grcTransito.DisplayFormat.FormatString = "n0"
        Me.grcTransito.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcTransito.FieldName = "transito"
        Me.grcTransito.Name = "grcTransito"
        Me.grcTransito.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTransito.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcTransito.VisibleIndex = 4
        Me.grcTransito.Width = 62
        '
        'grcEntregar
        '
        Me.grcEntregar.Caption = "Entregar"
        Me.grcEntregar.ColumnEdit = Me.rptCantidades
        Me.grcEntregar.DisplayFormat.FormatString = "n0"
        Me.grcEntregar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcEntregar.FieldName = "entregar"
        Me.grcEntregar.Name = "grcEntregar"
        Me.grcEntregar.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEntregar.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcEntregar.VisibleIndex = 5
        Me.grcEntregar.Width = 64
        '
        'grcReparto
        '
        Me.grcReparto.Caption = "Reparto"
        Me.grcReparto.ColumnEdit = Me.rptCantidades
        Me.grcReparto.DisplayFormat.FormatString = "n0"
        Me.grcReparto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcReparto.FieldName = "reparto"
        Me.grcReparto.Name = "grcReparto"
        Me.grcReparto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcReparto.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcReparto.VisibleIndex = 7
        Me.grcReparto.Width = 65
        '
        'grcVistas
        '
        Me.grcVistas.Caption = "Vistas"
        Me.grcVistas.ColumnEdit = Me.rptCantidades
        Me.grcVistas.DisplayFormat.FormatString = "n0"
        Me.grcVistas.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcVistas.FieldName = "vistas"
        Me.grcVistas.Name = "grcVistas"
        Me.grcVistas.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcVistas.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcVistas.VisibleIndex = 8
        Me.grcVistas.Width = 51
        '
        'grcGarantia
        '
        Me.grcGarantia.Caption = "Garant�a"
        Me.grcGarantia.ColumnEdit = Me.rptCantidades
        Me.grcGarantia.DisplayFormat.FormatString = "n0"
        Me.grcGarantia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcGarantia.FieldName = "garantia"
        Me.grcGarantia.Name = "grcGarantia"
        Me.grcGarantia.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcGarantia.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcGarantia.VisibleIndex = 9
        Me.grcGarantia.Width = 70
        '
        'grcfisica_calculada
        '
        Me.grcfisica_calculada.Caption = "F�sica"
        Me.grcfisica_calculada.FieldName = "fisica_calculada"
        Me.grcfisica_calculada.Name = "grcfisica_calculada"
        Me.grcfisica_calculada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcfisica_calculada.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcfisica_calculada.VisibleIndex = 1
        Me.grcfisica_calculada.Width = 61
        '
        'grcEntregarPedido
        '
        Me.grcEntregarPedido.Caption = "Pedido Fabrica"
        Me.grcEntregarPedido.FieldName = "entregar_pedido_fabrica"
        Me.grcEntregarPedido.Name = "grcEntregarPedido"
        Me.grcEntregarPedido.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEntregarPedido.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcEntregarPedido.VisibleIndex = 6
        Me.grcEntregarPedido.Width = 86
        '
        'btnVerDocumentos
        '
        Me.btnVerDocumentos.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.btnVerDocumentos.Enabled = False
        Me.btnVerDocumentos.Location = New System.Drawing.Point(14, 165)
        Me.btnVerDocumentos.Name = "btnVerDocumentos"
        Me.btnVerDocumentos.Size = New System.Drawing.Size(136, 24)
        Me.btnVerDocumentos.TabIndex = 35
        Me.btnVerDocumentos.Text = "Ver Documentos"
        '
        'tlbCambioPrecios
        '
        Me.tlbCambioPrecios.Text = "Cambio de Precios"
        Me.tlbCambioPrecios.ToolTipText = "Muestra los cambios de precios del articulo"
        '
        'txtmarca
        '
        Me.txtmarca.EditValue = ""
        Me.txtmarca.Location = New System.Drawing.Point(152, 340)
        Me.txtmarca.Name = "txtmarca"
        '
        'txtmarca.Properties
        '
        Me.txtmarca.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtmarca.Properties.MaxLength = 25
        Me.txtmarca.Size = New System.Drawing.Size(200, 20)
        Me.txtmarca.TabIndex = 98
        Me.txtmarca.Tag = "marca"
        '
        'txtclasificacion
        '
        Me.txtclasificacion.EditValue = ""
        Me.txtclasificacion.Location = New System.Drawing.Point(456, 340)
        Me.txtclasificacion.Name = "txtclasificacion"
        '
        'txtclasificacion.Properties
        '
        Me.txtclasificacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtclasificacion.Properties.MaxLength = 25
        Me.txtclasificacion.Size = New System.Drawing.Size(208, 20)
        Me.txtclasificacion.TabIndex = 100
        Me.txtclasificacion.Tag = "clasificacion"
        '
        'lblMarca
        '
        Me.lblMarca.AutoSize = True
        Me.lblMarca.Location = New System.Drawing.Point(104, 344)
        Me.lblMarca.Name = "lblMarca"
        Me.lblMarca.Size = New System.Drawing.Size(42, 16)
        Me.lblMarca.TabIndex = 97
        Me.lblMarca.Tag = ""
        Me.lblMarca.Text = "Marca:"
        Me.lblMarca.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblClasificacion
        '
        Me.lblClasificacion.AutoSize = True
        Me.lblClasificacion.Location = New System.Drawing.Point(368, 344)
        Me.lblClasificacion.Name = "lblClasificacion"
        Me.lblClasificacion.Size = New System.Drawing.Size(77, 16)
        Me.lblClasificacion.TabIndex = 99
        Me.lblClasificacion.Tag = ""
        Me.lblClasificacion.Text = "Clasificaci�n:"
        Me.lblClasificacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tblMegaCred
        '
        Me.tblMegaCred.Text = "Kardex Mega Cred"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtDescripcionEspecial)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Location = New System.Drawing.Point(88, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(784, 304)
        Me.Panel3.TabIndex = 102
        '
        'txtDescripcionEspecial
        '
        Me.txtDescripcionEspecial.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescripcionEspecial.EditValue = ""
        Me.txtDescripcionEspecial.Location = New System.Drawing.Point(136, 16)
        Me.txtDescripcionEspecial.Name = "txtDescripcionEspecial"
        '
        'txtDescripcionEspecial.Properties
        '
        Me.txtDescripcionEspecial.Properties.MaxLength = 8000
        Me.txtDescripcionEspecial.Size = New System.Drawing.Size(640, 72)
        Me.txtDescripcionEspecial.TabIndex = 19
        Me.txtDescripcionEspecial.Tag = "descripcion_especial"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(121, 16)
        Me.Label9.TabIndex = 18
        Me.Label9.Tag = ""
        Me.Label9.Text = "Descripci�n Especial:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'nvrDatosEspeciales
        '
        Me.nvrDatosEspeciales.Caption = "Datos Especiales"
        Me.nvrDatosEspeciales.Name = "nvrDatosEspeciales"
        '
        'frmArticulos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(874, 592)
        Me.Controls.Add(Me.lblClasificacion)
        Me.Controls.Add(Me.lblMarca)
        Me.Controls.Add(Me.txtclasificacion)
        Me.Controls.Add(Me.txtmarca)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel3)
        Me.MasterControl = Me.tmaUbicaciones
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmArticulos"
        Me.Controls.SetChildIndex(Me.Panel3, 0)
        Me.Controls.SetChildIndex(Me.Panel2, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.NavBarControl1, 0)
        Me.Controls.SetChildIndex(Me.TabControl1, 0)
        Me.Controls.SetChildIndex(Me.txtmarca, 0)
        Me.Controls.SetChildIndex(Me.txtclasificacion, 0)
        Me.Controls.SetChildIndex(Me.lblMarca, 0)
        Me.Controls.SetChildIndex(Me.lblClasificacion, 0)
        CType(Me.txtCodigo_Barras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcUltimo_Costo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteUltima_Compra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImpuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImpuesto_Suntuario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkImportacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDescontinuado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManeja_Series.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEntrega_Domicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecio_Lista.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCajas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.clcCostoPedidoFabrica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPedidoFabrica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRegalo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNoResurtir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRequiereArmado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRequiereInstalacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcStockMinimo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcStockMaximo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkligero_uso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptValor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcionCorta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grUbicaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvUbicaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.clcArticulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMesesGarantia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCapacidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNoIdentificable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcUltimoCostoSinFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grExistenciasSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptExistenciasSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        CType(Me.grExistencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvExistencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptCantidades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtmarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtclasificacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.txtDescripcionEspecial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    'Private TINApp As New Dipros.Windows.Application

    Public oArticulos As VillarrealBusiness.clsArticulos
    Private oArticulosFoto As New VillarrealBusiness.clsArticulosFoto
    Private oArticulosPrecios As New VillarrealBusiness.clsArticulosPrecios
    Private oArticulosBodegas As New VillarrealBusiness.clsArticulosBodegas
    Private oArticulosExistencias As New VillarrealBusiness.clsArticulosExistencias
    Private oHistoricoArticulosPrecios As New VillarrealBusiness.clsHisArticulosPrecios
    Private oTama�osEtiquetas As New VillarrealBusiness.clsTamaniosEtiquetas

    Private odepartamentos As New VillarrealBusiness.clsDepartamentos
    Private ogrupos As New VillarrealBusiness.clsGruposArticulos
    Private oUnidades As New VillarrealBusiness.clsUnidades
    Private oProvedores As New VillarrealBusiness.clsProveedores
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oReportes As New VillarrealBusiness.Reportes

    Private banUltimoCosto_0 As Boolean

    Private bodega_seleccionada As String
    Private n_bodega_seleccionada As String
    Private columna_seleccionada As String
    Private dUltimoprecioLista As Double = 0
    Dim KS As Keys
    Dim bGuardarExistencias As Boolean = False
    Private PedidoFabricaAnterior As Boolean = False
    Private CostoPedidoFabricaAnterior As Double = 0


    Private intDepartamentoActual As Long '@ACH-27/06/07: Variable que guarda el departamento del articulo que se est� modificando (Update)
    Private intGrupoActual As Long '@ACH-27/06/07: Variable que guarda el grupo del articulo que se est� modificando (Update)
    Private strModeloActual As String '@ACH-27/06/07: Variable que guarda el modelo del articulo que se est� modificando (Update)


    Private Property GuardarExistencias() As Boolean
        Get
            Return bGuardarExistencias
        End Get
        Set(ByVal Value As Boolean)
            bGuardarExistencias = Value
        End Set
    End Property
    Private ReadOnly Property Departamento() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Unidad() As String
        Get
            Return clsUtilerias.PreparaValorLookupStr(Me.lkpUnidad)
        End Get
    End Property
    Private ReadOnly Property Proveedor() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpProveedor)
        End Get
    End Property
    Private ReadOnly Property Tama�oEtiquetaPrecios() As String
        Get
            Return clsUtilerias.PreparaValorLookupStr(Me.lkpTama�oEtiqueta)
        End Get
    End Property
    Private ReadOnly Property BodegaPedidoFabrica() As String
        Get
            Return clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmArticulos_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub

    Private Sub frmArticulos_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub

    Private Sub frmArticulos_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub

    Private Sub frmArticulos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oArticulos.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oArticulos.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oArticulos.Eliminar(clcArticulo.Value)

        End Select
    End Sub
    Private Sub frmArticulos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim odataset As DataSet
        Dim bodega_pedido_fabrica As String

        Response = oArticulos.DespliegaDatos(OwnerForm.Value("articulo"))
        If Not Response.ErrorFound Then
            odataset = Response.Value
            Me.DataSource = odataset
            dUltimoprecioLista = odataset.Tables(0).Rows(0).Item("precio_lista")
            bodega_pedido_fabrica = odataset.Tables(0).Rows(0).Item("bodega_pedido_fabrica")
            PedidoFabricaAnterior = odataset.Tables(0).Rows(0).Item("pedido_fabrica")
            CostoPedidoFabricaAnterior = odataset.Tables(0).Rows(0).Item("costo_pedido_fabrica")
        End If

        DespliegaPrecios(OwnerForm.Value("articulo"), Response)

        ' Despliega Datos de Bodegas-Ubicaciones
        Response = oArticulosBodegas.Listado(OwnerForm.Value("articulo"))
        If Not Response.ErrorFound Then
            odataset = Response.Value
            Me.tmaUbicaciones.DataSource = odataset
        End If

        DespliegaExistencias(OwnerForm.Value("articulo"), Response)

        '@ACH-27/06/07: Variables creadas para almacenar el deparamento, grupo y modelo (respectivamente) del articulo que se est� modificando (Update)
        intDepartamentoActual = lkpDepartamento.EditValue
        intGrupoActual = lkpGrupo.EditValue
        strModeloActual = txtmodelo.EditValue
        '/@ACH-27/06/07

        RevisaPedidoFabrica(True, bodega_pedido_fabrica)
        RevisaCostos()


       
    End Sub
    Private Sub frmArticulos_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        If Action = Actions.Delete Then
            AccionesUbicaciones(Response)
            AccionesExistencias(Response)
            AccionesFoto(Response)
        End If

        Me.tmaPrecios.MoveFirst()
        Do While Not tmaPrecios.EOF
            Select Case tmaPrecios.CurrentAction
                'Case Actions.Insert
                'Response = oArticulosPrecios.Insertar(Me.tmaPrecios.SelectedRow, Me.txtArticulo.Text)
            Case Actions.Update
                    Response = oArticulosPrecios.Actualizar(Me.tmaPrecios.SelectedRow, Me.clcArticulo.Value)
                    GuardaHistorial(Response, Me.clcArticulo.Value, Me.tmaPrecios.Item("precio"), Me.tmaPrecios.Item("precio_venta"))

                Case Actions.Delete
                    Response = oArticulosPrecios.Eliminar(Me.clcArticulo.Value, Me.tmaPrecios.Item("precio"))
            End Select
            If Response.ErrorFound Then Exit Sub
            tmaPrecios.MoveNext()
        Loop

        If Action <> Actions.Delete Then
            AccionesUbicaciones(Response)
            AccionesExistencias(Response)
            If Response.ErrorFound = False And Action = Actions.Insert And Me.chkPedidoFabrica.Checked = True Then
                Response = oArticulos.InsertarArticulosPedidoFabrica(Me.clcArticulo.EditValue, BodegaPedidoFabrica, Me.clcCostoPedidoFabrica.EditValue)
            End If

            If Action = Actions.Update Then
                If Not Response.ErrorFound And Me.chkPedidoFabrica.Checked = True Then
                    If Me.PedidoFabricaAnterior <> chkPedidoFabrica.Checked Then
                        Response = oArticulos.InsertarArticulosPedidoFabrica(Me.clcArticulo.EditValue, BodegaPedidoFabrica, Me.clcCostoPedidoFabrica.EditValue)
                    Else
                        If Me.CostoPedidoFabricaAnterior <> Me.clcCostoPedidoFabrica.EditValue Then
                            Response = oArticulos.InsertarArticulosPedidoFabrica(Me.clcArticulo.EditValue, BodegaPedidoFabrica, Me.clcCostoPedidoFabrica.EditValue)
                        End If
                    End If
                End If
            End If
        End If

      
   

    End Sub

    Private Sub frmArticulos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oArticulos = New VillarrealBusiness.clsArticulos

        With tmaPrecios
            .UpdateTitle = "un Precio"
            .UpdateForm = New frmArticulosPrecios
            .AddColumn("precio", "System.Int32")
            .AddColumn("nombre_precio")
            .AddColumn("porcentaje_descuento", "System.Double")
            .AddColumn("precio_venta", "System.Double")
            .AddColumn("terminacion_precio", "System.Int32")
            .AddColumn("precio_expo", "System.Int32")
            .AddColumn("recalcular", "System.Boolean")
            .AddColumn("modificable", "System.Int32")
        End With

        With Me.tmaUbicaciones
            .UpdateTitle = "una Ubicaci�n"
            .UpdateForm = New frmArticulosBodegas
            .AddColumn("bodega")
            .AddColumn("nombre_bodega")
            .AddColumn("ubicacion")
        End With

        DespliegaPrecios(Me.clcArticulo.Value, Response)
        DespliegaExistencias(Me.clcArticulo.Value, Response)


        Select Case Action
            Case Actions.Insert
                Me.GuardarExistencias = True
                'Response = Me.oArticulos.folio
                'If Not Response.ErrorFound Then
                '    Me.clcArticulo.Value = Response.Value
                'End If
                Me.tbrTools.Buttons.Item(2).Visible = False
            Case Actions.Update
                Me.GuardarExistencias = False
                Me.clcArticulo.Enabled = False
                Me.btnVerDocumentos.Enabled = True
            Case Actions.Delete
                Me.GuardarExistencias = False
                'Me.tbrTools.Buttons.Item(2).Visible = False
        End Select

        bodega_seleccionada = ""
        columna_seleccionada = ""
        Me.dteUltima_Compra.EditValue = CDate(TinApp.FechaServidor)

        RevisaCostos()
    End Sub

    Private Sub frmArticulos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.clcPrecio_Lista.DoValidate()
        Response = oArticulos.Validacion(Action, Me.clcArticulo.Value, txtmodelo.Text, strModeloActual, Me.txtDescripcionCorta.Text, txtDescripcion.Text, Me.Departamento, intDepartamentoActual, Me.Grupo, intGrupoActual, Me.Unidad, Me.Proveedor, Tama�oEtiquetaPrecios, Me.chkRegalo.Checked, Me.chkPedidoFabrica.Checked, Me.clcCostoPedidoFabrica.Value, BodegaPedidoFabrica)
    End Sub

    Private Sub frmArticulos_Localize() Handles MyBase.Localize
        Find("articulo", Me.clcArticulo.Value)
    End Sub

    Private Sub frmArticulos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case Action
            Case Actions.Insert
                banUltimoCosto_0 = False
            Case Actions.Update
                If Me.clcUltimo_Costo.Value = 0 Then
                    banUltimoCosto_0 = True
                Else
                    banUltimoCosto_0 = False
                End If
            Case Actions.Delete
                banUltimoCosto_0 = False
        End Select
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#Region "Lookups"
#Region "Load Data"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim response As Events
        response = odepartamentos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim response As Events
        response = ogrupos.Lookup(clsUtilerias.PreparaValorLookup(Me.lkpDepartamento))
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpUnidad_LoadData(ByVal Initialize As Boolean) Handles lkpUnidad.LoadData
        Dim response As Events
        response = oUnidades.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpUnidad.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim response As Events
        response = oProvedores.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpTama�oEtiqueta_LoadData(ByVal Initialize As Boolean) Handles lkpTama�oEtiqueta.LoadData
        Dim Response As New Events
        Response = oTama�osEtiquetas.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpTama�oEtiqueta.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events
        response = oBodegas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

#End Region
#Region "Format"
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpUnidad_Format() Handles lkpUnidad.Format
        clsFormato.for_unidades_grl(Me.lkpUnidad)
    End Sub
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub
    Private Sub lkpTama�oEtiqueta_Format() Handles lkpTama�oEtiqueta.Format
        Comunes.clsFormato.for_tama�o_etiqueta_grl(Me.lkpTama�oEtiqueta)
    End Sub
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
#End Region
#Region "EditValue"
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
        Me.chkNoIdentificable.EditValue = Me.lkpDepartamento.GetValue("no_identificable")
        lkpGrupo_LoadData(True)
    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged
        If Me.Grupo > 0 Then
            Dim response As Events
            response = Me.ogrupos.ObtenerPreciosArticulosxGrupo(Grupo)
            If Not response.ErrorFound Then
                Dim odataset As DataSet
                odataset = response.Value
                Me.tmaPrecios.DataSource = odataset
                odataset = Nothing
                'RecalcularPrecios()
            End If
            response = Nothing

        End If
    End Sub
#End Region
#Region "Validating"

    Private Sub txtDescripcion_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oArticulos.ValidaDescripcion(txtDescripcion.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
#End Region

    
    
#End Region

    Private Sub clcPrecio_Lista_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcPrecio_Lista.Validated
        If Me.clcPrecio_Lista.IsLoading Then Exit Sub

        If Me.clcPrecio_Lista.Value < Me.clcUltimo_Costo.Value Then
            ShowMessage(MessageType.MsgInformation, "El Precio de Lista no puede ser menor al Ultimo Costo")
        End If

        If dUltimoprecioLista <> Me.clcPrecio_Lista.EditValue Then
            If ShowMessage(MessageType.MsgQuestion, "�Desea modificar los precios?", "Art�culos", , False) = Answer.MsgYes Then

                RecalcularPrecios()

            End If
            dUltimoprecioLista = Me.clcPrecio_Lista.EditValue
        End If
        

    End Sub
    Private Sub btnVerDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerDocumentos.Click
        Dim response As New Events
        Dim oData As DataSet

        If bodega_seleccionada <> "" And columna_seleccionada <> "" Then
            Dim frmModal As New frmArticulosExistencias
            With frmModal
                .lblNomBodega.Text = n_bodega_seleccionada
                .lblNomArticulo.Text = Me.txtDescripcionCorta.Text
                .OwnerForm = Me
                .MdiParent = Me.MdiParent
                Select Case columna_seleccionada
                    Case "grcPorSurtir"
                        .Text = "Por Surtir"
                        response = oArticulosExistencias.ArticulosExistencias_PorSurtir(bodega_seleccionada, Me.clcArticulo.Value)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvPorSurtir
                            .grExistencias.DataSource = oData.Tables(0)
                        End If

                    Case "grcTransito"
                        .Text = "Transito"
                        response = oArticulosExistencias.ArticulosExistencias_Traspasos(bodega_seleccionada, Me.clcArticulo.Value)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvTraspasos
                            .grExistencias.DataSource = oData.Tables(0)
                        End If


                    Case "grcEntregar"
                        .Text = "Entregar"
                        response = oArticulosExistencias.ArticulosExistencias_PorEntregar(bodega_seleccionada, Me.clcArticulo.Value, False)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvPorEntregar
                            .grExistencias.DataSource = oData.Tables(0)
                        End If

                    Case "grcEntregarPedido"
                        .Text = "Entregar P�dido F�brica"
                        response = oArticulosExistencias.ArticulosExistencias_PorEntregar(bodega_seleccionada, Me.clcArticulo.Value, True)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvPorEntregar
                            .grExistencias.DataSource = oData.Tables(0)
                        End If

                    Case "grcReparto"
                        .Text = "Reparto"
                        response = oArticulosExistencias.ArticulosExistencias_Repartiendose(bodega_seleccionada, Me.clcArticulo.Value)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvRepartiendose
                            .grExistencias.DataSource = oData.Tables(0)
                        End If

                    Case "grcGarantia"
                        .Text = "Garantia"
                        response = oArticulosExistencias.ArticulosExistencias_Garantias(bodega_seleccionada, Me.clcArticulo.Value)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvGarantias
                            .grExistencias.DataSource = oData.Tables(0)
                        End If

                    Case "grcVistas"
                        .Text = "Vistas"
                        response = oArticulosExistencias.ArticulosExistencias_Vistas(bodega_seleccionada, Me.clcArticulo.Value)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvVistas
                            .grExistencias.DataSource = oData.Tables(0)
                        End If
                        'Case "grcPrefacturas"
                        '    .Text = "Prefacturas"
                        '    response = oArticulosExistencias.ArticulosExistencias_Prefacturas(bodega_seleccionada, Me.clcArticulo.Value)
                        '    If Not response.ErrorFound Then
                        '        oData = CType(response.Value, DataSet)
                        '        .grExistencias.MainView = .grvPrefacturas
                        '        .grExistencias.DataSource = oData.Tables(0)
                        '    End If
                    Case Else
                        Exit Sub
                End Select
                .Show()
                Me.Enabled = False
            End With
        End If

    End Sub
    Private Sub grvExistencias_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grvExistencias.Click
        bodega_seleccionada = Me.grvExistencias.GetRowCellValue(Me.grvExistencias.FocusedRowHandle, Me.grcBodegaExistencias)
        n_bodega_seleccionada = Me.grvExistencias.GetRowCellValue(Me.grvExistencias.FocusedRowHandle, Me.grcNombrebodega)
        columna_seleccionada = Me.grvExistencias.FocusedColumn.Name
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button.Text = "Imprimir Kardex" Then
            Dim frmModal As New frmArticulosKardex
            With frmModal
                .Text = "Kardex de Art�culo"
                .Action = Me.Action.Report
                .Articulo = Me.clcArticulo.Value
                .OwnerForm = Me
                .MdiParent = Me.MdiParent
                .Show()
            End With
            Me.Enabled = False
        End If


        If e.Button Is Me.tlbCambioPrecios Then
            Dim odata As DataSet
            Dim response As Events

            Dim articulo As Long = IIf(Me.clcArticulo.Value = 0, -1, Me.clcArticulo.Value)
            response = Me.oArticulosPrecios.Listado_Cambio_Precios(articulo)
            If Not response.ErrorFound Then
                odata = response.Value
                Dim frmModal As New frmArticulosCambiosPrecios
                With frmModal
                    .Text = "Cambios de Precios del Art�culo"
                    .Action = Me.Action.Report
                    .lblNomArticulo.Text = articulo.ToString + "  " + Me.txtDescripcionCorta.Text
                    .OwnerForm = Me
                    .MdiParent = Me.MdiParent
                    .grExistencias.DataSource = odata.Tables(0)
                    .Show()
                End With
                Me.Enabled = False
            End If
        End If

        If e.Button Is Me.tblMegaCred Then
            ImprimeKardexMegaCred()
        End If
    End Sub
    Private Sub chkPedidoFabrica_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPedidoFabrica.CheckedChanged
        RevisaPedidoFabrica(False, "")
    End Sub
    Private Sub nvrDatosGenerales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosGenerales.LinkPressed
        Me.Panel1.BringToFront()
    End Sub
    Private Sub nvrPrecio_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrPrecio.LinkPressed
        Me.Panel2.BringToFront()
    End Sub
    Private Sub nvrDatosEspeciales_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrDatosEspeciales.LinkPressed
        Me.Panel3.BringToFront()
    End Sub

    Private Sub nvrFoto_LinkPressed(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvrFoto.LinkPressed
        CargarFotografiaArticulo(Me.clcArticulo.Value)
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub DespliegaExistencias(ByVal articulo As String, ByRef response As Events)
        Dim odataset As DataSet
        Dim EsInsert As Boolean = False

        If Action = Actions.Insert Then
            EsInsert = True
        End If

        ' Despliega Datos de Bodegas existencias
        response = oArticulosExistencias.Listado(articulo, EsInsert)
        If Not response.ErrorFound Then
            odataset = response.Value
            Me.grExistencias.DataSource = odataset.Tables(0)
            Me.grExistenciasSucursales.DataSource = odataset.Tables(1)
        End If
        odataset = Nothing
    End Sub
    Private Sub DespliegaPrecios(ByVal articulo As String, ByRef response As Events)
        Dim odataset As DataSet

        ' Despliega Datos de Precios
        response = oArticulosPrecios.Listado(articulo)
        If Not response.ErrorFound Then
            odataset = response.Value
            Me.tmaPrecios.DataSource = odataset
        End If
        odataset = Nothing
    End Sub
    Private Sub GuardaHistorial(ByRef response As Events, ByVal articulo As String, ByVal clave_precio As Long, ByVal precio_venta As Double)

        Dim folio As Long = 0

        'Guarda el historial de los precios de los articulos
        response = Me.oHistoricoArticulosPrecios.Insertar(folio, articulo, clave_precio, precio_venta)
    End Sub
    Private Sub AccionesUbicaciones(ByRef response As Events)
        Me.tmaUbicaciones.MoveFirst()
        Do While Not tmaUbicaciones.EOF
            Select Case tmaUbicaciones.CurrentAction
                Case Actions.Insert
                    response = oArticulosBodegas.Insertar(Me.tmaUbicaciones.SelectedRow, Me.clcArticulo.Value)
                Case Actions.Update
                    response = oArticulosBodegas.Actualizar(Me.tmaUbicaciones.SelectedRow, Me.clcArticulo.Value)
                Case Actions.Delete
                    response = oArticulosBodegas.Eliminar(Me.clcArticulo.Value, Me.tmaUbicaciones.Item("bodega"))
            End Select
            If response.ErrorFound Then Exit Sub
            tmaUbicaciones.MoveNext()
        Loop
    End Sub
    Private Sub AccionesExistencias(ByRef response As Events)


        If Action = Actions.Delete Then
            'response = Me.oArticulosExistencias.Eliminar(Me.clcArticulo.Value, Me.grvExistencias.GetRowCellValue(i, Me.grcBodegaExistencias))
            response = Me.oArticulosExistencias.Eliminar(Me.clcArticulo.Value)
            Exit Sub
        End If


        Dim i As Long = 0
        While i <= Me.grvExistencias.RowCount - 1
            Select Case Action
                Case Actions.Insert
                    response = Me.oArticulosExistencias.Insertar(Me.clcArticulo.Value, Me.grvExistencias.GetRowCellValue(i, Me.grcBodegaExistencias), Me.grvExistencias.GetRowCellValue(i, Me.grcFisica), Me.grvExistencias.GetRowCellValue(i, Me.grcPorSurtir), _
                                        Me.grvExistencias.GetRowCellValue(i, Me.grcTransito), Me.grvExistencias.GetRowCellValue(i, Me.grcBodega), Me.grvExistencias.GetRowCellValue(i, Me.grcReparto), Me.grvExistencias.GetRowCellValue(i, Me.grcVistas), Me.grvExistencias.GetRowCellValue(i, Me.grcGarantia))

            End Select
            i = i + 1
        End While
    End Sub
    Private Sub AccionesFoto(ByRef response As Events)
        If Action = Actions.Delete Then
            response = Me.oArticulosFoto.Eliminar(Me.clcArticulo.Value)
            Exit Sub
        End If
    End Sub

    Private Sub RecalcularPrecios()

        Dim i As Long = 0
        Dim bexpo As Boolean
        bexpo = False
        Me.grvPrecios.UpdateCurrentRow()
        With Me.grvPrecios
            While i <= .RowCount - 1

                Dim descuento As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcDescuento)
                Dim lexpo As Long = Me.grvPrecios.GetRowCellValue(i, Me.grcPrecioexpo)
                Dim lterminacion As Long = Me.grvPrecios.GetRowCellValue(i, Me.grcterminacion)
                Dim Recalcular As Boolean = Me.grvPrecios.GetRowCellValue(i, Me.grcRecalcular)

                If i + 1 = lexpo Then
                    bexpo = True

                Else
                    bexpo = False
                End If

                If bexpo = False And Recalcular = True Then
                    Me.grvPrecios.SetRowCellValue(i, Me.grcPrecioVenta, CalculaPrecio(descuento, lterminacion))
                    Me.grvPrecios.SetRowCellValue(i, Me.grcControl, 2)
                End If
                i = i + 1

            End While
        End With


    End Sub
    Private Sub RecalcularDescuentos()
        Dim i As Long = 0
        Dim bexpo As Boolean
        bexpo = False
        Me.grvPrecios.UpdateCurrentRow()
        With Me.grvPrecios
            While i <= .RowCount - 1
                Dim precio_venta As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcPrecioVenta)
                Dim descuento As Double = Me.grvPrecios.GetRowCellValue(i, Me.grcDescuento)

                Dim lexpo As Long = Me.grvPrecios.GetRowCellValue(i, Me.grcPrecioexpo)

                If i + 1 = lexpo Then
                    bexpo = True
                Else
                    bexpo = False
                End If
                If bexpo = False Then
                    Me.grvPrecios.SetRowCellValue(i, Me.grcDescuento, CalculaDescuento(precio_venta))
                    Me.grvPrecios.SetRowCellValue(i, Me.grcControl, 2)
                End If
                i = i + 1
            End While
        End With

    End Sub
    Private Function CalculaPrecio(ByVal utilidad As Long, ByVal terminacion As Long) As Double
        Dim precio As Double
        Dim precio_terminacion As Double
        Dim sprecio() As String
        Dim sprecio2 As String
        Dim ultimo_digito As String

        precio = System.Math.Round(clcPrecio_Lista.Value - (clcPrecio_Lista.Value * (utilidad / 100)))

        sprecio = precio.ToString.Split(".")
        sprecio2 = sprecio(0)


        ultimo_digito = sprecio2.Trim.Substring(sprecio2.Length - 1, 1)
        If CType(ultimo_digito, Long) > 5 And CType(ultimo_digito, Long) > terminacion Then
            precio = precio + 10
        End If

        sprecio = precio.ToString.Split(".")
        sprecio2 = sprecio(0)

        sprecio2 = sprecio2.Substring(0, sprecio2.Length - 1) + terminacion.ToString
        precio_terminacion = CType(sprecio2, Double)

        CalculaPrecio = precio_terminacion

        '---------------------SE BASO EN ESTE FRAGMENTO DE LA CLASE DE CLSARTICULOS DE NEGOCIOS-------------------------------
        ' --------------------BORRAR EN CUANTO QUEDE LIBERADA ESTA MODIFICACION

        'sprecio = precio.ToString.Split(".")
        'sprecio2 = sprecio(0)

        'ultimo_digito = sprecio2.Trim.Substring(sprecio2.Length - 1, 1)
        'If CType(ultimo_digito, Long) > 5 And CType(ultimo_digito, Long) > terminacion Then
        '    precio = precio + 10
        'End If

        'sprecio = precio.ToString.Split(".")
        'sprecio2 = sprecio(0)

        'sprecio2 = sprecio2.Substring(0, sprecio2.Length - 1) + terminacion.ToString
        'precio_terminacion = CType(sprecio2, Double)
        'oEvent.Value = precio_terminacion

    End Function
    Private Function CalculaDescuento(ByVal precio_venta As Double) As Double
        CalculaDescuento = ((clcPrecio_Lista.Value - precio_venta) / clcPrecio_Lista.Value) * 100
    End Function
    Private Sub RevisaPedidoFabrica(ByVal EntroDespliega As Boolean, ByVal bodega_pedido_fabrica As String)
        If Me.chkPedidoFabrica.Checked = True Then


            If Me.clcUltimo_Costo.Value = 0 Then
                Dim factor As Double = 0
                Dim response As Events = ogrupos.DespliegaDatos(Grupo)
                If Not response.ErrorFound Then
                    If CType(response.Value, DataSet).Tables(0).Rows.Count > 0 Then
                        factor = CType(response.Value, DataSet).Tables(0).Rows(0).Item("factor_ganancia")
                    End If
                End If

                If factor > 0 Then
                    Me.clcCostoPedidoFabrica.Value = Me.clcPrecio_Lista.Value / factor
                Else
                    Me.clcCostoPedidoFabrica.Value = 0
                End If

            Else
                Me.clcCostoPedidoFabrica.Value = Me.clcUltimo_Costo.Value
            End If


            Me.clcCostoPedidoFabrica.Enabled = True
            Me.lkpBodega.Enabled = True
            If EntroDespliega Then
                Me.lkpBodega.EditValue = bodega_pedido_fabrica
            Else
                Me.lkpBodega.EditValue = ObtenerUltimaBodegaPedidoFabrica()

            End If




        Else
            Me.clcCostoPedidoFabrica.Value = 0
            Me.clcCostoPedidoFabrica.Enabled = False
            Me.lkpBodega.Enabled = False
            Me.lkpBodega.EditValue = Nothing
        End If


       
    End Sub
    Private Sub RevisaCostos()
        If Me.clcUltimo_Costo.Value = 0 Then
            Me.clcUltimo_Costo.Enabled = True
        Else
            Me.clcUltimo_Costo.Enabled = False
        End If

        If Me.clcUltimoCostoSinFlete.Value = 0 Then
            Me.clcUltimoCostoSinFlete.Enabled = True
        Else
            Me.clcUltimoCostoSinFlete.Enabled = False
        End If
    End Sub
    Private Function ObtenerUltimaBodegaPedidoFabrica() As String
        Dim odataset As DataSet
        Dim response As Dipros.Utils.Events

        response = Me.oArticulos.ObtenerUltimaBodegaPedidoFabrica(Me.clcArticulo.Value)

        If Not response.ErrorFound Then
            odataset = response.Value

            If odataset.Tables.Count > 0 Then
                If odataset.Tables(0).Rows.Count > 0 Then

                    Return odataset.Tables(0).Rows(0).Item("bodega")
                End If
            End If
            Return ""

        End If
    End Function
    Private Sub ImprimeKardexMegaCred()
        Dim response As Events

        response = oReportes.KardexMegaCred(Me.txtCodigo_Barras.Text)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Kardex de Art�culo no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptKardexMegaCred

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)


                TinApp.ShowReport(Me.MdiParent, "Kardex Mega Cred", oReport, , , , True)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub
    Private Sub CargarFotografiaArticulo(ByVal Articulo As Long)
        Dim oform As New frmArticulosFoto
        oform.ModificarImagen = True
        oform.Articulo = Articulo
        oform.Title = "Foto del Articulo"
        oform.ShowDialog(Me.MdiParent)
    End Sub
#End Region


    Private Sub grvPrecios_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvPrecios.FocusedRowChanged
        Dim Esmodificable As Int32 = Me.grvPrecios.GetRowCellValue(e.FocusedRowHandle, Me.grcModificable)

        If Esmodificable = 0 Then
            Me.tmaPrecios.CanUpdate = False
        Else
            Me.tmaPrecios.CanUpdate = True
        End If
    End Sub

    End Class

