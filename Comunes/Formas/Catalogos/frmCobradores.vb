Imports Dipros.Utils.Common
Imports Dipros.Utils
Imports System.Windows.Forms

Public Class frmCobradores
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents clcCobrador As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblComision As System.Windows.Forms.Label
    Friend WithEvents clcComision As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkdescontar_sueldo_comision As DevExpress.XtraEditors.CheckEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCobradores))
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.clcCobrador = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblComision = New System.Windows.Forms.Label
        Me.clcComision = New Dipros.Editors.TINCalcEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.chkdescontar_sueldo_comision = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcCobrador.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkdescontar_sueldo_comision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(368, 28)
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(7, 40)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 0
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCobrador
        '
        Me.clcCobrador.EditValue = "0"
        Me.clcCobrador.Location = New System.Drawing.Point(74, 40)
        Me.clcCobrador.MaxValue = 0
        Me.clcCobrador.MinValue = 0
        Me.clcCobrador.Name = "clcCobrador"
        '
        'clcCobrador.Properties
        '
        Me.clcCobrador.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCobrador.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCobrador.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCobrador.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCobrador.Properties.Enabled = False
        Me.clcCobrador.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCobrador.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCobrador.Size = New System.Drawing.Size(46, 19)
        Me.clcCobrador.TabIndex = 1
        Me.clcCobrador.Tag = "cobrador"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(14, 63)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(74, 63)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 60
        Me.txtNombre.Size = New System.Drawing.Size(360, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'lblComision
        '
        Me.lblComision.AutoSize = True
        Me.lblComision.Location = New System.Drawing.Point(8, 112)
        Me.lblComision.Name = "lblComision"
        Me.lblComision.Size = New System.Drawing.Size(59, 16)
        Me.lblComision.TabIndex = 6
        Me.lblComision.Tag = ""
        Me.lblComision.Text = "Co&mision:"
        Me.lblComision.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcComision
        '
        Me.clcComision.EditValue = "0"
        Me.clcComision.Location = New System.Drawing.Point(74, 112)
        Me.clcComision.MaxValue = 0
        Me.clcComision.MinValue = 0
        Me.clcComision.Name = "clcComision"
        '
        'clcComision.Properties
        '
        Me.clcComision.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcComision.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComision.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcComision.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComision.Properties.MaskData.EditMask = "########0.00"
        Me.clcComision.Properties.MaxLength = 5
        Me.clcComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcComision.Size = New System.Drawing.Size(70, 19)
        Me.clcComision.TabIndex = 7
        Me.clcComision.Tag = "comision"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(11, 88)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(74, 88)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(358, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 5
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'chkdescontar_sueldo_comision
        '
        Me.chkdescontar_sueldo_comision.Location = New System.Drawing.Point(160, 112)
        Me.chkdescontar_sueldo_comision.Name = "chkdescontar_sueldo_comision"
        '
        'chkdescontar_sueldo_comision.Properties
        '
        Me.chkdescontar_sueldo_comision.Properties.Caption = "Descontar Sueldo  de Comisi�n"
        Me.chkdescontar_sueldo_comision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkdescontar_sueldo_comision.Size = New System.Drawing.Size(192, 19)
        Me.chkdescontar_sueldo_comision.TabIndex = 8
        Me.chkdescontar_sueldo_comision.Tag = "descontar_sueldo"
        '
        'frmCobradores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(444, 144)
        Me.Controls.Add(Me.chkdescontar_sueldo_comision)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.clcCobrador)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblComision)
        Me.Controls.Add(Me.clcComision)
        Me.Name = "frmCobradores"
        Me.Controls.SetChildIndex(Me.clcComision, 0)
        Me.Controls.SetChildIndex(Me.lblComision, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.chkdescontar_sueldo_comision, 0)
        CType(Me.clcCobrador.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkdescontar_sueldo_comision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCobradores As VillarrealBusiness.clsCobradores
    Private oSucursales As VillarrealBusiness.clsSucursales

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCobradores_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oCobradores.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oCobradores.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oCobradores.Eliminar(clcCobrador.Value)

        End Select
    End Sub
    Private Sub frmCobradores_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oCobradores.DespliegaDatos(OwnerForm.Value("cobrador"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub
    Private Sub frmCobradores_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCobradores = New VillarrealBusiness.clsCobradores
        oSucursales = New VillarrealBusiness.clsSucursales

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select

        Me.lkpSucursal.EditValue = CLng(Comunes.Common.Sucursal_Actual)
    End Sub
    Private Sub frmCobradores_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCobradores.Validacion(Action, txtNombre.Text, Sucursal)
    End Sub
    Private Sub frmCobradores_Localize() Handles MyBase.Localize
        Find("cobrador", Me.clcCobrador.EditValue)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub clcComision_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcComision.EditValueChanged
        If Me.clcComision.IsLoading Then Exit Sub

        If Me.clcComision.Value < 0 Or Me.clcComision.Value > 100 Then
            Me.clcComision.EditValue = 0
        End If
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
