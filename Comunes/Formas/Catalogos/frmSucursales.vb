Imports Dipros.Utils.Common
Imports Dipros.Utils
Imports System.Windows.Forms

Public Class frmSucursales
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblSucursal As System.Windows.Forms.Label
		Friend WithEvents clcSucursal As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblNombre As System.Windows.Forms.Label
		Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblDireccion As System.Windows.Forms.Label
		Friend WithEvents txtDireccion As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblColonia As System.Windows.Forms.Label
		Friend WithEvents txtColonia As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblCp As System.Windows.Forms.Label
		Friend WithEvents clcCp As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblCiudad As System.Windows.Forms.Label
		Friend WithEvents txtCiudad As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblEstado As System.Windows.Forms.Label
		Friend WithEvents txtEstado As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblTelefono As System.Windows.Forms.Label
		Friend WithEvents txtTelefono As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblGerente As System.Windows.Forms.Label
		Friend WithEvents txtGerente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lkpConceptoSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpConceptoEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents clcFolio_interes As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtSerie_interes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lkpPrecioMinimoVenta As Dipros.Editors.TINMultiLookup
    Friend WithEvents cboPrecios As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkSucursalDependencia As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents clcAnioEnvio As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcQuincenaEnvio As Dipros.Editors.TINCalcEdit
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSerieTimbrado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcTimbrado As Dipros.Editors.TINCalcEdit
    Friend WithEvents gbxFacElec As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblPuertoImpresion As System.Windows.Forms.Label
    Friend WithEvents txtPuertoImpresion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents chkFacturaElectronica As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtSerieRecibosElectronica As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcFolioRecibosElectronica As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtSerieNotaCreditoElectronica As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcFolioNotaCreditoElectronica As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtSerieNotaCargoElectronica As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcFolioNotaCargoElectronica As Dipros.Editors.TINCalcEdit

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSucursales))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.clcSucursal = New Dipros.Editors.TINCalcEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblDireccion = New System.Windows.Forms.Label
        Me.txtDireccion = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia = New System.Windows.Forms.Label
        Me.txtColonia = New DevExpress.XtraEditors.TextEdit
        Me.lblCp = New System.Windows.Forms.Label
        Me.clcCp = New Dipros.Editors.TINCalcEdit
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.txtCiudad = New DevExpress.XtraEditors.TextEdit
        Me.lblEstado = New System.Windows.Forms.Label
        Me.txtEstado = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono = New System.Windows.Forms.Label
        Me.txtTelefono = New DevExpress.XtraEditors.TextEdit
        Me.lblGerente = New System.Windows.Forms.Label
        Me.txtGerente = New DevExpress.XtraEditors.TextEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpConceptoEntrada = New Dipros.Editors.TINMultiLookup
        Me.lkpConceptoSalida = New Dipros.Editors.TINMultiLookup
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtSerie_interes = New DevExpress.XtraEditors.TextEdit
        Me.clcFolio_interes = New Dipros.Editors.TINCalcEdit
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.cboPrecios = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.lkpPrecioMinimoVenta = New Dipros.Editors.TINMultiLookup
        Me.chkSucursalDependencia = New DevExpress.XtraEditors.CheckEdit
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.clcAnioEnvio = New Dipros.Editors.TINCalcEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.clcQuincenaEnvio = New Dipros.Editors.TINCalcEdit
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtSerieTimbrado = New DevExpress.XtraEditors.TextEdit
        Me.clcTimbrado = New Dipros.Editors.TINCalcEdit
        Me.chkFacturaElectronica = New DevExpress.XtraEditors.CheckEdit
        Me.gbxFacElec = New System.Windows.Forms.GroupBox
        Me.lblPuertoImpresion = New System.Windows.Forms.Label
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtSerieRecibosElectronica = New DevExpress.XtraEditors.TextEdit
        Me.clcFolioRecibosElectronica = New Dipros.Editors.TINCalcEdit
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtSerieNotaCreditoElectronica = New DevExpress.XtraEditors.TextEdit
        Me.clcFolioNotaCreditoElectronica = New Dipros.Editors.TINCalcEdit
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtSerieNotaCargoElectronica = New DevExpress.XtraEditors.TextEdit
        Me.clcFolioNotaCargoElectronica = New Dipros.Editors.TINCalcEdit
        Me.txtPuertoImpresion = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGerente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtSerie_interes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio_interes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.cboPrecios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSucursalDependencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.clcAnioEnvio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcQuincenaEnvio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtSerieTimbrado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTimbrado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFacturaElectronica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFacElec.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.txtSerieRecibosElectronica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioRecibosElectronica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.txtSerieNotaCreditoElectronica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioNotaCreditoElectronica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.txtSerieNotaCargoElectronica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioNotaCargoElectronica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPuertoImpresion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(481, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSucursal
        '
        Me.clcSucursal.EditValue = "0"
        Me.clcSucursal.Location = New System.Drawing.Point(80, 39)
        Me.clcSucursal.MaxValue = 0
        Me.clcSucursal.MinValue = 0
        Me.clcSucursal.Name = "clcSucursal"
        '
        'clcSucursal.Properties
        '
        Me.clcSucursal.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcSucursal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcSucursal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.Enabled = False
        Me.clcSucursal.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSucursal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSucursal.Size = New System.Drawing.Size(48, 19)
        Me.clcSucursal.TabIndex = 1
        Me.clcSucursal.Tag = "sucursal"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(19, 63)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "Nom&bre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(80, 61)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 50
        Me.txtNombre.Size = New System.Drawing.Size(300, 20)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(12, 86)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(60, 16)
        Me.lblDireccion.TabIndex = 4
        Me.lblDireccion.Tag = ""
        Me.lblDireccion.Text = "&Dirección:"
        Me.lblDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDireccion
        '
        Me.txtDireccion.EditValue = ""
        Me.txtDireccion.Location = New System.Drawing.Point(80, 84)
        Me.txtDireccion.Name = "txtDireccion"
        '
        'txtDireccion.Properties
        '
        Me.txtDireccion.Properties.MaxLength = 30
        Me.txtDireccion.Size = New System.Drawing.Size(300, 20)
        Me.txtDireccion.TabIndex = 5
        Me.txtDireccion.Tag = "direccion"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(22, 109)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 6
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "C&olonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColonia
        '
        Me.txtColonia.EditValue = ""
        Me.txtColonia.Location = New System.Drawing.Point(80, 107)
        Me.txtColonia.Name = "txtColonia"
        '
        'txtColonia.Properties
        '
        Me.txtColonia.Properties.MaxLength = 50
        Me.txtColonia.Size = New System.Drawing.Size(300, 20)
        Me.txtColonia.TabIndex = 7
        Me.txtColonia.Tag = "colonia"
        '
        'lblCp
        '
        Me.lblCp.AutoSize = True
        Me.lblCp.Location = New System.Drawing.Point(48, 180)
        Me.lblCp.Name = "lblCp"
        Me.lblCp.Size = New System.Drawing.Size(24, 16)
        Me.lblCp.TabIndex = 12
        Me.lblCp.Tag = ""
        Me.lblCp.Text = "Cp&:"
        Me.lblCp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCp
        '
        Me.clcCp.EditValue = "0"
        Me.clcCp.Location = New System.Drawing.Point(80, 179)
        Me.clcCp.MaxValue = 0
        Me.clcCp.MinValue = 0
        Me.clcCp.Name = "clcCp"
        '
        'clcCp.Properties
        '
        Me.clcCp.Properties.DisplayFormat.FormatString = "0"
        Me.clcCp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.EditFormat.FormatString = "0"
        Me.clcCp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.MaskData.EditMask = "0"
        Me.clcCp.Properties.MaxLength = 8
        Me.clcCp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCp.Size = New System.Drawing.Size(48, 19)
        Me.clcCp.TabIndex = 13
        Me.clcCp.Tag = "cp"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(25, 132)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(47, 16)
        Me.lblCiudad.TabIndex = 8
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "Ci&udad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudad
        '
        Me.txtCiudad.EditValue = ""
        Me.txtCiudad.Location = New System.Drawing.Point(80, 130)
        Me.txtCiudad.Name = "txtCiudad"
        '
        'txtCiudad.Properties
        '
        Me.txtCiudad.Properties.MaxLength = 50
        Me.txtCiudad.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudad.TabIndex = 9
        Me.txtCiudad.Tag = "ciudad"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(26, 156)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 10
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "Es&tado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstado
        '
        Me.txtEstado.EditValue = ""
        Me.txtEstado.Location = New System.Drawing.Point(80, 154)
        Me.txtEstado.Name = "txtEstado"
        '
        'txtEstado.Properties
        '
        Me.txtEstado.Properties.MaxLength = 50
        Me.txtEstado.Size = New System.Drawing.Size(300, 20)
        Me.txtEstado.TabIndex = 11
        Me.txtEstado.Tag = "estado"
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(232, 180)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(56, 16)
        Me.lblTelefono.TabIndex = 14
        Me.lblTelefono.Tag = ""
        Me.lblTelefono.Text = "Telé&fono:"
        Me.lblTelefono.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono
        '
        Me.txtTelefono.EditValue = ""
        Me.txtTelefono.Location = New System.Drawing.Point(292, 178)
        Me.txtTelefono.Name = "txtTelefono"
        '
        'txtTelefono.Properties
        '
        Me.txtTelefono.Properties.MaxLength = 13
        Me.txtTelefono.Size = New System.Drawing.Size(88, 20)
        Me.txtTelefono.TabIndex = 15
        Me.txtTelefono.Tag = "telefono"
        '
        'lblGerente
        '
        Me.lblGerente.AutoSize = True
        Me.lblGerente.Location = New System.Drawing.Point(19, 204)
        Me.lblGerente.Name = "lblGerente"
        Me.lblGerente.Size = New System.Drawing.Size(53, 16)
        Me.lblGerente.TabIndex = 16
        Me.lblGerente.Tag = ""
        Me.lblGerente.Text = "&Gerente:"
        Me.lblGerente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtGerente
        '
        Me.txtGerente.EditValue = ""
        Me.txtGerente.Location = New System.Drawing.Point(80, 202)
        Me.txtGerente.Name = "txtGerente"
        '
        'txtGerente.Properties
        '
        Me.txtGerente.Properties.MaxLength = 80
        Me.txtGerente.Size = New System.Drawing.Size(300, 20)
        Me.txtGerente.TabIndex = 17
        Me.txtGerente.Tag = "gerente"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lkpConceptoEntrada)
        Me.GroupBox1.Controls.Add(Me.lkpConceptoSalida)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 360)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(376, 80)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Conceptos de Traspasos"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Entrada:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Tag = ""
        Me.Label2.Text = "Sa&lida:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoEntrada
        '
        Me.lkpConceptoEntrada.AllowAdd = False
        Me.lkpConceptoEntrada.AutoReaload = False
        Me.lkpConceptoEntrada.DataSource = Nothing
        Me.lkpConceptoEntrada.DefaultSearchField = ""
        Me.lkpConceptoEntrada.DisplayMember = "descripcion"
        Me.lkpConceptoEntrada.EditValue = Nothing
        Me.lkpConceptoEntrada.Filtered = False
        Me.lkpConceptoEntrada.InitValue = Nothing
        Me.lkpConceptoEntrada.Location = New System.Drawing.Point(72, 48)
        Me.lkpConceptoEntrada.MultiSelect = False
        Me.lkpConceptoEntrada.Name = "lkpConceptoEntrada"
        Me.lkpConceptoEntrada.NullText = ""
        Me.lkpConceptoEntrada.PopupWidth = CType(400, Long)
        Me.lkpConceptoEntrada.ReadOnlyControl = False
        Me.lkpConceptoEntrada.Required = False
        Me.lkpConceptoEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoEntrada.SearchMember = ""
        Me.lkpConceptoEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoEntrada.SelectAll = False
        Me.lkpConceptoEntrada.Size = New System.Drawing.Size(288, 20)
        Me.lkpConceptoEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoEntrada.TabIndex = 3
        Me.lkpConceptoEntrada.Tag = "concepto_entrada_traspaso"
        Me.lkpConceptoEntrada.ToolTip = Nothing
        Me.lkpConceptoEntrada.ValueMember = "concepto"
        '
        'lkpConceptoSalida
        '
        Me.lkpConceptoSalida.AllowAdd = False
        Me.lkpConceptoSalida.AutoReaload = False
        Me.lkpConceptoSalida.DataSource = Nothing
        Me.lkpConceptoSalida.DefaultSearchField = ""
        Me.lkpConceptoSalida.DisplayMember = "descripcion"
        Me.lkpConceptoSalida.EditValue = Nothing
        Me.lkpConceptoSalida.Filtered = False
        Me.lkpConceptoSalida.InitValue = Nothing
        Me.lkpConceptoSalida.Location = New System.Drawing.Point(72, 22)
        Me.lkpConceptoSalida.MultiSelect = False
        Me.lkpConceptoSalida.Name = "lkpConceptoSalida"
        Me.lkpConceptoSalida.NullText = ""
        Me.lkpConceptoSalida.PopupWidth = CType(400, Long)
        Me.lkpConceptoSalida.ReadOnlyControl = False
        Me.lkpConceptoSalida.Required = False
        Me.lkpConceptoSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoSalida.SearchMember = ""
        Me.lkpConceptoSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoSalida.SelectAll = False
        Me.lkpConceptoSalida.Size = New System.Drawing.Size(288, 20)
        Me.lkpConceptoSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoSalida.TabIndex = 1
        Me.lkpConceptoSalida.Tag = "concepto_salida_traspaso"
        Me.lkpConceptoSalida.ToolTip = Nothing
        Me.lkpConceptoSalida.ValueMember = "concepto"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.txtSerie_interes)
        Me.GroupBox3.Controls.Add(Me.clcFolio_interes)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 304)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(376, 48)
        Me.GroupBox3.TabIndex = 19
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Intereses"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(176, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Tag = ""
        Me.Label4.Text = "Folio:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(45, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(37, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Tag = ""
        Me.Label5.Text = "Ser&ie:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerie_interes
        '
        Me.txtSerie_interes.EditValue = ""
        Me.txtSerie_interes.Location = New System.Drawing.Point(85, 18)
        Me.txtSerie_interes.Name = "txtSerie_interes"
        '
        'txtSerie_interes.Properties
        '
        Me.txtSerie_interes.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie_interes.Properties.MaxLength = 3
        Me.txtSerie_interes.Size = New System.Drawing.Size(54, 20)
        Me.txtSerie_interes.TabIndex = 1
        Me.txtSerie_interes.Tag = "serie_intereses"
        '
        'clcFolio_interes
        '
        Me.clcFolio_interes.EditValue = "0"
        Me.clcFolio_interes.Location = New System.Drawing.Point(216, 16)
        Me.clcFolio_interes.MaxValue = 0
        Me.clcFolio_interes.MinValue = 0
        Me.clcFolio_interes.Name = "clcFolio_interes"
        '
        'clcFolio_interes.Properties
        '
        Me.clcFolio_interes.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_interes.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_interes.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_interes.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_interes.Size = New System.Drawing.Size(54, 19)
        Me.clcFolio_interes.TabIndex = 3
        Me.clcFolio_interes.Tag = "folio_intereses"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.cboPrecios)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.lkpPrecioMinimoVenta)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 224)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(376, 72)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(52, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Tag = ""
        Me.Label7.Text = "Precio de Venta:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPrecios
        '
        Me.cboPrecios.Location = New System.Drawing.Point(152, 16)
        Me.cboPrecios.Name = "cboPrecios"
        '
        'cboPrecios.Properties
        '
        Me.cboPrecios.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPrecios.Size = New System.Drawing.Size(128, 23)
        Me.cboPrecios.TabIndex = 1
        Me.cboPrecios.Tag = "precio_venta"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(139, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Tag = ""
        Me.Label6.Text = "Precio &Mínimo de Venta:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPrecioMinimoVenta
        '
        Me.lkpPrecioMinimoVenta.AllowAdd = False
        Me.lkpPrecioMinimoVenta.AutoReaload = False
        Me.lkpPrecioMinimoVenta.DataSource = Nothing
        Me.lkpPrecioMinimoVenta.DefaultSearchField = ""
        Me.lkpPrecioMinimoVenta.DisplayMember = "nombre"
        Me.lkpPrecioMinimoVenta.EditValue = Nothing
        Me.lkpPrecioMinimoVenta.Filtered = False
        Me.lkpPrecioMinimoVenta.InitValue = Nothing
        Me.lkpPrecioMinimoVenta.Location = New System.Drawing.Point(152, 40)
        Me.lkpPrecioMinimoVenta.MultiSelect = False
        Me.lkpPrecioMinimoVenta.Name = "lkpPrecioMinimoVenta"
        Me.lkpPrecioMinimoVenta.NullText = ""
        Me.lkpPrecioMinimoVenta.PopupWidth = CType(400, Long)
        Me.lkpPrecioMinimoVenta.ReadOnlyControl = False
        Me.lkpPrecioMinimoVenta.Required = False
        Me.lkpPrecioMinimoVenta.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPrecioMinimoVenta.SearchMember = ""
        Me.lkpPrecioMinimoVenta.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPrecioMinimoVenta.SelectAll = False
        Me.lkpPrecioMinimoVenta.Size = New System.Drawing.Size(208, 20)
        Me.lkpPrecioMinimoVenta.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPrecioMinimoVenta.TabIndex = 3
        Me.lkpPrecioMinimoVenta.Tag = "precio_minimo_venta"
        Me.lkpPrecioMinimoVenta.ToolTip = Nothing
        Me.lkpPrecioMinimoVenta.ValueMember = "precio"
        '
        'chkSucursalDependencia
        '
        Me.chkSucursalDependencia.Location = New System.Drawing.Point(392, 40)
        Me.chkSucursalDependencia.Name = "chkSucursalDependencia"
        '
        'chkSucursalDependencia.Properties
        '
        Me.chkSucursalDependencia.Properties.Caption = "Sucursal de Co&nvenio"
        Me.chkSucursalDependencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkSucursalDependencia.Size = New System.Drawing.Size(144, 19)
        Me.chkSucursalDependencia.TabIndex = 22
        Me.chkSucursalDependencia.Tag = "sucursal_dependencia"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label9)
        Me.GroupBox5.Controls.Add(Me.clcAnioEnvio)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.clcQuincenaEnvio)
        Me.GroupBox5.Location = New System.Drawing.Point(9, 448)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(376, 48)
        Me.GroupBox5.TabIndex = 21
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Envios"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(200, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(30, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Tag = ""
        Me.Label9.Text = "A&ño:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAnioEnvio
        '
        Me.clcAnioEnvio.EditValue = "0"
        Me.clcAnioEnvio.Location = New System.Drawing.Point(240, 19)
        Me.clcAnioEnvio.MaxValue = 0
        Me.clcAnioEnvio.MinValue = 0
        Me.clcAnioEnvio.Name = "clcAnioEnvio"
        '
        'clcAnioEnvio.Properties
        '
        Me.clcAnioEnvio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnioEnvio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnioEnvio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcAnioEnvio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAnioEnvio.Size = New System.Drawing.Size(54, 19)
        Me.clcAnioEnvio.TabIndex = 7
        Me.clcAnioEnvio.Tag = "anio_envio"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(40, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Tag = ""
        Me.Label8.Text = "&Quincena:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcQuincenaEnvio
        '
        Me.clcQuincenaEnvio.EditValue = "0"
        Me.clcQuincenaEnvio.Location = New System.Drawing.Point(104, 19)
        Me.clcQuincenaEnvio.MaxValue = 0
        Me.clcQuincenaEnvio.MinValue = 0
        Me.clcQuincenaEnvio.Name = "clcQuincenaEnvio"
        '
        'clcQuincenaEnvio.Properties
        '
        Me.clcQuincenaEnvio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincenaEnvio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincenaEnvio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcQuincenaEnvio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcQuincenaEnvio.Size = New System.Drawing.Size(54, 19)
        Me.clcQuincenaEnvio.TabIndex = 5
        Me.clcQuincenaEnvio.Tag = "quincena_envio"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtSerieTimbrado)
        Me.GroupBox2.Controls.Add(Me.clcTimbrado)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 24)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(184, 72)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Factura Electronica"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(45, 42)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 16)
        Me.Label10.TabIndex = 2
        Me.Label10.Tag = ""
        Me.Label10.Text = "Folio:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(45, 21)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(37, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Tag = ""
        Me.Label11.Text = "Ser&ie:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerieTimbrado
        '
        Me.txtSerieTimbrado.EditValue = ""
        Me.txtSerieTimbrado.Location = New System.Drawing.Point(85, 18)
        Me.txtSerieTimbrado.Name = "txtSerieTimbrado"
        '
        'txtSerieTimbrado.Properties
        '
        Me.txtSerieTimbrado.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerieTimbrado.Properties.MaxLength = 3
        Me.txtSerieTimbrado.Size = New System.Drawing.Size(54, 20)
        Me.txtSerieTimbrado.TabIndex = 1
        Me.txtSerieTimbrado.Tag = "serie_factura_electronica"
        '
        'clcTimbrado
        '
        Me.clcTimbrado.EditValue = "0"
        Me.clcTimbrado.Location = New System.Drawing.Point(85, 42)
        Me.clcTimbrado.MaxValue = 0
        Me.clcTimbrado.MinValue = 0
        Me.clcTimbrado.Name = "clcTimbrado"
        '
        'clcTimbrado.Properties
        '
        Me.clcTimbrado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTimbrado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTimbrado.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTimbrado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTimbrado.Size = New System.Drawing.Size(54, 19)
        Me.clcTimbrado.TabIndex = 3
        Me.clcTimbrado.Tag = "folio_factura_electronica"
        '
        'chkFacturaElectronica
        '
        Me.chkFacturaElectronica.Location = New System.Drawing.Point(392, 59)
        Me.chkFacturaElectronica.Name = "chkFacturaElectronica"
        '
        'chkFacturaElectronica.Properties
        '
        Me.chkFacturaElectronica.Properties.Caption = "Factura Electronica"
        Me.chkFacturaElectronica.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkFacturaElectronica.Size = New System.Drawing.Size(152, 19)
        Me.chkFacturaElectronica.TabIndex = 23
        Me.chkFacturaElectronica.Tag = "factura_electronica"
        '
        'gbxFacElec
        '
        Me.gbxFacElec.Controls.Add(Me.lblPuertoImpresion)
        Me.gbxFacElec.Controls.Add(Me.GroupBox8)
        Me.gbxFacElec.Controls.Add(Me.GroupBox7)
        Me.gbxFacElec.Controls.Add(Me.GroupBox6)
        Me.gbxFacElec.Controls.Add(Me.GroupBox2)
        Me.gbxFacElec.Controls.Add(Me.txtPuertoImpresion)
        Me.gbxFacElec.Location = New System.Drawing.Point(392, 82)
        Me.gbxFacElec.Name = "gbxFacElec"
        Me.gbxFacElec.Size = New System.Drawing.Size(200, 414)
        Me.gbxFacElec.TabIndex = 24
        Me.gbxFacElec.TabStop = False
        Me.gbxFacElec.Text = "Datos para Factura Electronica"
        '
        'lblPuertoImpresion
        '
        Me.lblPuertoImpresion.AutoSize = True
        Me.lblPuertoImpresion.Location = New System.Drawing.Point(12, 343)
        Me.lblPuertoImpresion.Name = "lblPuertoImpresion"
        Me.lblPuertoImpresion.Size = New System.Drawing.Size(104, 16)
        Me.lblPuertoImpresion.TabIndex = 17
        Me.lblPuertoImpresion.Tag = ""
        Me.lblPuertoImpresion.Text = "Puerto Impresión:"
        Me.lblPuertoImpresion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Label16)
        Me.GroupBox8.Controls.Add(Me.Label17)
        Me.GroupBox8.Controls.Add(Me.txtSerieRecibosElectronica)
        Me.GroupBox8.Controls.Add(Me.clcFolioRecibosElectronica)
        Me.GroupBox8.Location = New System.Drawing.Point(8, 264)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(184, 72)
        Me.GroupBox8.TabIndex = 3
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Recibos"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(45, 42)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(35, 16)
        Me.Label16.TabIndex = 2
        Me.Label16.Tag = ""
        Me.Label16.Text = "Folio:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(45, 21)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(37, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Tag = ""
        Me.Label17.Text = "Ser&ie:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerieRecibosElectronica
        '
        Me.txtSerieRecibosElectronica.EditValue = ""
        Me.txtSerieRecibosElectronica.Location = New System.Drawing.Point(85, 18)
        Me.txtSerieRecibosElectronica.Name = "txtSerieRecibosElectronica"
        '
        'txtSerieRecibosElectronica.Properties
        '
        Me.txtSerieRecibosElectronica.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerieRecibosElectronica.Properties.MaxLength = 3
        Me.txtSerieRecibosElectronica.Size = New System.Drawing.Size(54, 20)
        Me.txtSerieRecibosElectronica.TabIndex = 1
        Me.txtSerieRecibosElectronica.Tag = "serie_recibos_electronica"
        '
        'clcFolioRecibosElectronica
        '
        Me.clcFolioRecibosElectronica.EditValue = "0"
        Me.clcFolioRecibosElectronica.Location = New System.Drawing.Point(85, 42)
        Me.clcFolioRecibosElectronica.MaxValue = 0
        Me.clcFolioRecibosElectronica.MinValue = 0
        Me.clcFolioRecibosElectronica.Name = "clcFolioRecibosElectronica"
        '
        'clcFolioRecibosElectronica.Properties
        '
        Me.clcFolioRecibosElectronica.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioRecibosElectronica.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioRecibosElectronica.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolioRecibosElectronica.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolioRecibosElectronica.Size = New System.Drawing.Size(54, 19)
        Me.clcFolioRecibosElectronica.TabIndex = 3
        Me.clcFolioRecibosElectronica.Tag = "folio_recibos_electronica"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.Label14)
        Me.GroupBox7.Controls.Add(Me.Label15)
        Me.GroupBox7.Controls.Add(Me.txtSerieNotaCreditoElectronica)
        Me.GroupBox7.Controls.Add(Me.clcFolioNotaCreditoElectronica)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 184)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(184, 72)
        Me.GroupBox7.TabIndex = 2
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Nota de Credito"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(45, 42)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 16)
        Me.Label14.TabIndex = 2
        Me.Label14.Tag = ""
        Me.Label14.Text = "Folio:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(45, 21)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(37, 16)
        Me.Label15.TabIndex = 0
        Me.Label15.Tag = ""
        Me.Label15.Text = "Ser&ie:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerieNotaCreditoElectronica
        '
        Me.txtSerieNotaCreditoElectronica.EditValue = ""
        Me.txtSerieNotaCreditoElectronica.Location = New System.Drawing.Point(85, 18)
        Me.txtSerieNotaCreditoElectronica.Name = "txtSerieNotaCreditoElectronica"
        '
        'txtSerieNotaCreditoElectronica.Properties
        '
        Me.txtSerieNotaCreditoElectronica.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerieNotaCreditoElectronica.Properties.MaxLength = 3
        Me.txtSerieNotaCreditoElectronica.Size = New System.Drawing.Size(54, 20)
        Me.txtSerieNotaCreditoElectronica.TabIndex = 1
        Me.txtSerieNotaCreditoElectronica.Tag = "serie_nota_credito_electronica"
        '
        'clcFolioNotaCreditoElectronica
        '
        Me.clcFolioNotaCreditoElectronica.EditValue = "0"
        Me.clcFolioNotaCreditoElectronica.Location = New System.Drawing.Point(85, 42)
        Me.clcFolioNotaCreditoElectronica.MaxValue = 0
        Me.clcFolioNotaCreditoElectronica.MinValue = 0
        Me.clcFolioNotaCreditoElectronica.Name = "clcFolioNotaCreditoElectronica"
        '
        'clcFolioNotaCreditoElectronica.Properties
        '
        Me.clcFolioNotaCreditoElectronica.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioNotaCreditoElectronica.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioNotaCreditoElectronica.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolioNotaCreditoElectronica.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolioNotaCreditoElectronica.Size = New System.Drawing.Size(54, 19)
        Me.clcFolioNotaCreditoElectronica.TabIndex = 3
        Me.clcFolioNotaCreditoElectronica.Tag = "folio_nota_credito_electronica"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label12)
        Me.GroupBox6.Controls.Add(Me.Label13)
        Me.GroupBox6.Controls.Add(Me.txtSerieNotaCargoElectronica)
        Me.GroupBox6.Controls.Add(Me.clcFolioNotaCargoElectronica)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 104)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(184, 72)
        Me.GroupBox6.TabIndex = 1
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Nota de Cargo"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(45, 42)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 16)
        Me.Label12.TabIndex = 2
        Me.Label12.Tag = ""
        Me.Label12.Text = "Folio:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(45, 21)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(37, 16)
        Me.Label13.TabIndex = 0
        Me.Label13.Tag = ""
        Me.Label13.Text = "Ser&ie:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerieNotaCargoElectronica
        '
        Me.txtSerieNotaCargoElectronica.EditValue = ""
        Me.txtSerieNotaCargoElectronica.Location = New System.Drawing.Point(85, 18)
        Me.txtSerieNotaCargoElectronica.Name = "txtSerieNotaCargoElectronica"
        '
        'txtSerieNotaCargoElectronica.Properties
        '
        Me.txtSerieNotaCargoElectronica.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerieNotaCargoElectronica.Properties.MaxLength = 3
        Me.txtSerieNotaCargoElectronica.Size = New System.Drawing.Size(54, 20)
        Me.txtSerieNotaCargoElectronica.TabIndex = 1
        Me.txtSerieNotaCargoElectronica.Tag = "serie_nota_cargo_electronica"
        '
        'clcFolioNotaCargoElectronica
        '
        Me.clcFolioNotaCargoElectronica.EditValue = "0"
        Me.clcFolioNotaCargoElectronica.Location = New System.Drawing.Point(85, 42)
        Me.clcFolioNotaCargoElectronica.MaxValue = 0
        Me.clcFolioNotaCargoElectronica.MinValue = 0
        Me.clcFolioNotaCargoElectronica.Name = "clcFolioNotaCargoElectronica"
        '
        'clcFolioNotaCargoElectronica.Properties
        '
        Me.clcFolioNotaCargoElectronica.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioNotaCargoElectronica.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioNotaCargoElectronica.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolioNotaCargoElectronica.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolioNotaCargoElectronica.Size = New System.Drawing.Size(54, 19)
        Me.clcFolioNotaCargoElectronica.TabIndex = 3
        Me.clcFolioNotaCargoElectronica.Tag = "folio_nota_cargo_electronica"
        '
        'txtPuertoImpresion
        '
        Me.txtPuertoImpresion.EditValue = ""
        Me.txtPuertoImpresion.Location = New System.Drawing.Point(8, 360)
        Me.txtPuertoImpresion.Name = "txtPuertoImpresion"
        '
        'txtPuertoImpresion.Properties
        '
        Me.txtPuertoImpresion.Properties.MaxLength = 100
        Me.txtPuertoImpresion.Size = New System.Drawing.Size(184, 48)
        Me.txtPuertoImpresion.TabIndex = 18
        Me.txtPuertoImpresion.Tag = "puerto_impresion"
        '
        'frmSucursales
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(602, 500)
        Me.Controls.Add(Me.gbxFacElec)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.chkSucursalDependencia)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.clcSucursal)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.txtColonia)
        Me.Controls.Add(Me.lblCp)
        Me.Controls.Add(Me.clcCp)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.txtCiudad)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.txtEstado)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.txtTelefono)
        Me.Controls.Add(Me.lblGerente)
        Me.Controls.Add(Me.txtGerente)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.chkFacturaElectronica)
        Me.Name = "frmSucursales"
        Me.Controls.SetChildIndex(Me.chkFacturaElectronica, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.txtGerente, 0)
        Me.Controls.SetChildIndex(Me.lblGerente, 0)
        Me.Controls.SetChildIndex(Me.txtTelefono, 0)
        Me.Controls.SetChildIndex(Me.lblTelefono, 0)
        Me.Controls.SetChildIndex(Me.txtEstado, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.txtCiudad, 0)
        Me.Controls.SetChildIndex(Me.lblCiudad, 0)
        Me.Controls.SetChildIndex(Me.clcCp, 0)
        Me.Controls.SetChildIndex(Me.lblCp, 0)
        Me.Controls.SetChildIndex(Me.txtColonia, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.txtDireccion, 0)
        Me.Controls.SetChildIndex(Me.lblDireccion, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.clcSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox4, 0)
        Me.Controls.SetChildIndex(Me.chkSucursalDependencia, 0)
        Me.Controls.SetChildIndex(Me.GroupBox5, 0)
        Me.Controls.SetChildIndex(Me.gbxFacElec, 0)
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGerente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.txtSerie_interes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio_interes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.cboPrecios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSucursalDependencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.clcAnioEnvio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcQuincenaEnvio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtSerieTimbrado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTimbrado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFacturaElectronica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFacElec.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.txtSerieRecibosElectronica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioRecibosElectronica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        CType(Me.txtSerieNotaCreditoElectronica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioNotaCreditoElectronica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.txtSerieNotaCargoElectronica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioNotaCargoElectronica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPuertoImpresion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario
    Private oPreciosVenta As VillarrealBusiness.clsPrecios

    Private ReadOnly Property PrecioMinimo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpPrecioMinimoVenta)
        End Get
    End Property

    Private ReadOnly Property ConceptoSalida() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoSalida)
        End Get
    End Property

    Private ReadOnly Property ConceptoEntrada() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoEntrada)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
	Private Sub frmSucursales_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
		Select Case Action
		Case Actions.Insert
						Response = oSucursales.Insertar(Me.DataSource)

		Case Actions.Update
						Response = oSucursales.Actualizar(Me.DataSource)

		Case Actions.Delete
						Response = oSucursales.Eliminar(clcSucursal.value)

		End Select
	End Sub
    Private Sub frmSucursales_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oSucursales.DespliegaDatos(OwnerForm.Value("sucursal"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            If Me.chkSucursalDependencia.Checked = False Then
                Me.clcQuincenaEnvio.Enabled = False
                Me.clcAnioEnvio.Enabled = False
            End If
        End If


    End Sub
    Private Sub frmSucursales_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario
        oPreciosVenta = New VillarrealBusiness.clsPrecios
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select

        LLenarPrecios()
    End Sub
    Private Sub frmSucursales_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oSucursales.Validacion(Action, txtNombre.Text, txtDireccion.Text, txtColonia.Text, clcCp.Value, txtCiudad.Text, txtEstado.Text, txtGerente.Text, Me.txtSerie_interes.Text, Me.PrecioMinimo, Me.ConceptoSalida, Me.ConceptoEntrada, txtSerieTimbrado.Text)
    End Sub
    Private Sub frmSucursales_Localize() Handles MyBase.Localize
        Find("sucursal", Me.clcSucursal.Value)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
	Private Sub txtNombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oSucursales.ValidaNombre(txtNombre.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub
	Private Sub txtDireccion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDireccion.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oSucursales.ValidaDireccion(txtDireccion.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub
	Private Sub txtColonia_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtColonia.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oSucursales.ValidaColonia(txtColonia.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub
	Private Sub clcCp_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcCp.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oSucursales.ValidaCp(clcCp.value)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub
	Private Sub txtCiudad_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCiudad.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oSucursales.ValidaCiudad(txtCiudad.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub
	Private Sub txtEstado_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtEstado.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oSucursales.ValidaEstado(txtEstado.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub
	Private Sub txtGerente_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtGerente.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oSucursales.ValidaGerente(txtGerente.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub
    Private Sub lkpConceptoEntrada_Format() Handles lkpConceptoEntrada.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConceptoEntrada)
    End Sub
    Private Sub lkpConceptoEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoEntrada.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpConceptoSalida_Format() Handles lkpConceptoSalida.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConceptoSalida)
    End Sub
    Private Sub lkpConceptoSalida_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoSalida.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub lkpPrecioMinimoVenta_Format() Handles lkpPrecioMinimoVenta.Format
        Comunes.clsFormato.for_precios_grl(Me.lkpPrecioMinimoVenta)
    End Sub
    Private Sub lkpPrecioMinimoVenta_LoadData(ByVal Initialize As Boolean) Handles lkpPrecioMinimoVenta.LoadData
        Dim Response As New Events
        Response = oPreciosVenta.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPrecioMinimoVenta.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub chkSucursalDependencia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursalDependencia.CheckedChanged
        If Me.chkSucursalDependencia.IsLoading = True Then Exit Sub

        If Me.chkSucursalDependencia.Checked = False Then
            Me.clcQuincenaEnvio.Value = 0
            Me.clcAnioEnvio.Value = 0
        End If

        Me.clcQuincenaEnvio.Enabled = chkSucursalDependencia.Checked
        Me.clcAnioEnvio.Enabled = chkSucursalDependencia.Checked

    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub LLenarPrecios()
        Dim Response As New Events
        Response = oPreciosVenta.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            Dim item As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
            item.Description = "Precio Lista"
            item.Value = 0
            Me.cboPrecios.Properties.Items.Add(item)

            Dim i As Long
            For i = 1 To oDataSet.Tables(0).Rows.Count
                Dim item_for As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                item_for.Description = oDataSet.Tables(0).Rows(i - 1).Item("nombre")
                item_for.Value = oDataSet.Tables(0).Rows(i - 1).Item("precio")
                Me.cboPrecios.Properties.Items.Add(item_for)
            Next

            Me.cboPrecios.SelectedIndex = 0
            oDataSet = Nothing
        End If
    End Sub
#End Region


End Class
