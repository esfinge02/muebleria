Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmArticulosPrecios
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Articulo As String
    Dim KS As Keys
    Private bCargando As Boolean = True
    'Private dpreciolista As Double = 0
    'Private utilidad As Boolean = False
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblPrecio As System.Windows.Forms.Label
    Friend WithEvents lkpPrecio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblUtilidad As System.Windows.Forms.Label
    Friend WithEvents clcUtilidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPrecio_Venta As System.Windows.Forms.Label
    Friend WithEvents clcPrecio_Venta As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblLkpPrecio As System.Windows.Forms.Label
    Friend WithEvents clcterminacion As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcprecio_expo As Dipros.Editors.TINCalcEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmArticulosPrecios))
        Me.lblPrecio = New System.Windows.Forms.Label
        Me.lkpPrecio = New Dipros.Editors.TINMultiLookup
        Me.lblUtilidad = New System.Windows.Forms.Label
        Me.clcUtilidad = New Dipros.Editors.TINCalcEdit
        Me.lblPrecio_Venta = New System.Windows.Forms.Label
        Me.clcPrecio_Venta = New Dipros.Editors.TINCalcEdit
        Me.lblLkpPrecio = New System.Windows.Forms.Label
        Me.clcterminacion = New Dipros.Editors.TINCalcEdit
        Me.clcprecio_expo = New Dipros.Editors.TINCalcEdit
        CType(Me.clcUtilidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecio_Venta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcterminacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcprecio_expo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblPrecio
        '
        Me.lblPrecio.AutoSize = True
        Me.lblPrecio.Location = New System.Drawing.Point(52, 40)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(42, 16)
        Me.lblPrecio.TabIndex = 0
        Me.lblPrecio.Tag = ""
        Me.lblPrecio.Text = "&Precio:"
        Me.lblPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPrecio
        '
        Me.lkpPrecio.AllowAdd = False
        Me.lkpPrecio.AutoReaload = False
        Me.lkpPrecio.DataSource = Nothing
        Me.lkpPrecio.DefaultSearchField = ""
        Me.lkpPrecio.DisplayMember = "nombre"
        Me.lkpPrecio.EditValue = Nothing
        Me.lkpPrecio.Enabled = False
        Me.lkpPrecio.Filtered = False
        Me.lkpPrecio.InitValue = Nothing
        Me.lkpPrecio.Location = New System.Drawing.Point(99, 37)
        Me.lkpPrecio.MultiSelect = False
        Me.lkpPrecio.Name = "lkpPrecio"
        Me.lkpPrecio.NullText = ""
        Me.lkpPrecio.PopupWidth = CType(400, Long)
        Me.lkpPrecio.ReadOnlyControl = False
        Me.lkpPrecio.Required = False
        Me.lkpPrecio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPrecio.SearchMember = ""
        Me.lkpPrecio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPrecio.SelectAll = False
        Me.lkpPrecio.Size = New System.Drawing.Size(264, 20)
        Me.lkpPrecio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPrecio.TabIndex = 1
        Me.lkpPrecio.Tag = "Precio"
        Me.lkpPrecio.ToolTip = Nothing
        Me.lkpPrecio.ValueMember = "Precio"
        '
        'lblUtilidad
        '
        Me.lblUtilidad.AutoSize = True
        Me.lblUtilidad.Location = New System.Drawing.Point(27, 63)
        Me.lblUtilidad.Name = "lblUtilidad"
        Me.lblUtilidad.Size = New System.Drawing.Size(67, 16)
        Me.lblUtilidad.TabIndex = 3
        Me.lblUtilidad.Tag = ""
        Me.lblUtilidad.Text = "&Descuento:"
        Me.lblUtilidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcUtilidad
        '
        Me.clcUtilidad.EditValue = "0"
        Me.clcUtilidad.Location = New System.Drawing.Point(99, 63)
        Me.clcUtilidad.MaxValue = 0
        Me.clcUtilidad.MinValue = 0
        Me.clcUtilidad.Name = "clcUtilidad"
        '
        'clcUtilidad.Properties
        '
        Me.clcUtilidad.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcUtilidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUtilidad.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcUtilidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUtilidad.Properties.MaskData.EditMask = "########0.00"
        Me.clcUtilidad.Properties.Precision = 18
        Me.clcUtilidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcUtilidad.Size = New System.Drawing.Size(70, 19)
        Me.clcUtilidad.TabIndex = 4
        Me.clcUtilidad.Tag = "porcentaje_descuento"
        '
        'lblPrecio_Venta
        '
        Me.lblPrecio_Venta.AutoSize = True
        Me.lblPrecio_Venta.Location = New System.Drawing.Point(16, 86)
        Me.lblPrecio_Venta.Name = "lblPrecio_Venta"
        Me.lblPrecio_Venta.Size = New System.Drawing.Size(78, 16)
        Me.lblPrecio_Venta.TabIndex = 5
        Me.lblPrecio_Venta.Tag = ""
        Me.lblPrecio_Venta.Text = "Precio &Venta:"
        Me.lblPrecio_Venta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecio_Venta
        '
        Me.clcPrecio_Venta.EditValue = "0"
        Me.clcPrecio_Venta.Location = New System.Drawing.Point(99, 86)
        Me.clcPrecio_Venta.MaxValue = 0
        Me.clcPrecio_Venta.MinValue = 0
        Me.clcPrecio_Venta.Name = "clcPrecio_Venta"
        '
        'clcPrecio_Venta.Properties
        '
        Me.clcPrecio_Venta.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecio_Venta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio_Venta.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecio_Venta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio_Venta.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecio_Venta.Properties.Precision = 2
        Me.clcPrecio_Venta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecio_Venta.Size = New System.Drawing.Size(70, 19)
        Me.clcPrecio_Venta.TabIndex = 6
        Me.clcPrecio_Venta.Tag = "precio_venta"
        '
        'lblLkpPrecio
        '
        Me.lblLkpPrecio.Location = New System.Drawing.Point(187, 72)
        Me.lblLkpPrecio.Name = "lblLkpPrecio"
        Me.lblLkpPrecio.Size = New System.Drawing.Size(176, 23)
        Me.lblLkpPrecio.TabIndex = 59
        Me.lblLkpPrecio.Tag = "nombre_precio"
        Me.lblLkpPrecio.Visible = False
        '
        'clcterminacion
        '
        Me.clcterminacion.EditValue = "0"
        Me.clcterminacion.Location = New System.Drawing.Point(288, 136)
        Me.clcterminacion.MaxValue = 0
        Me.clcterminacion.MinValue = 0
        Me.clcterminacion.Name = "clcterminacion"
        '
        'clcterminacion.Properties
        '
        Me.clcterminacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcterminacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcterminacion.Properties.MaskData.EditMask = "########0.00"
        Me.clcterminacion.Properties.Precision = 18
        Me.clcterminacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcterminacion.Size = New System.Drawing.Size(70, 19)
        Me.clcterminacion.TabIndex = 60
        Me.clcterminacion.Tag = "terminacion_precio"
        '
        'clcprecio_expo
        '
        Me.clcprecio_expo.EditValue = "0"
        Me.clcprecio_expo.Location = New System.Drawing.Point(200, 136)
        Me.clcprecio_expo.MaxValue = 0
        Me.clcprecio_expo.MinValue = 0
        Me.clcprecio_expo.Name = "clcprecio_expo"
        '
        'clcprecio_expo.Properties
        '
        Me.clcprecio_expo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcprecio_expo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcprecio_expo.Properties.MaskData.EditMask = "########0.00"
        Me.clcprecio_expo.Properties.Precision = 18
        Me.clcprecio_expo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcprecio_expo.Size = New System.Drawing.Size(70, 19)
        Me.clcprecio_expo.TabIndex = 61
        Me.clcprecio_expo.Tag = "precio_expo"
        '
        'frmArticulosPrecios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(378, 112)
        Me.Controls.Add(Me.clcprecio_expo)
        Me.Controls.Add(Me.clcterminacion)
        Me.Controls.Add(Me.lblLkpPrecio)
        Me.Controls.Add(Me.lblPrecio)
        Me.Controls.Add(Me.lkpPrecio)
        Me.Controls.Add(Me.lblUtilidad)
        Me.Controls.Add(Me.clcUtilidad)
        Me.Controls.Add(Me.lblPrecio_Venta)
        Me.Controls.Add(Me.clcPrecio_Venta)
        Me.Name = "frmArticulosPrecios"
        Me.Controls.SetChildIndex(Me.clcPrecio_Venta, 0)
        Me.Controls.SetChildIndex(Me.lblPrecio_Venta, 0)
        Me.Controls.SetChildIndex(Me.clcUtilidad, 0)
        Me.Controls.SetChildIndex(Me.lblUtilidad, 0)
        Me.Controls.SetChildIndex(Me.lkpPrecio, 0)
        Me.Controls.SetChildIndex(Me.lblPrecio, 0)
        Me.Controls.SetChildIndex(Me.lblLkpPrecio, 0)
        Me.Controls.SetChildIndex(Me.clcterminacion, 0)
        Me.Controls.SetChildIndex(Me.clcprecio_expo, 0)
        CType(Me.clcUtilidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecio_Venta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcterminacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcprecio_expo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oArticulosPrecios As VillarrealBusiness.clsArticulosPrecios
    Private oPrecios As New VillarrealBusiness.clsPrecios
    Private bprecio As Boolean = False
    Private bdescuento As Boolean = False

    'Public WriteOnly Property PrecioLista() As Double
    '    Set(ByVal Value As Double)
    '        dpreciolista = Value
    '    End Set
    'End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmArticulosPrecios_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmArticulosPrecios_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        If Me.lkpPrecio.EditValue = Me.clcprecio_expo.EditValue Then
            Me.clcUtilidad.Enabled = False
        End If
        If CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value() = 0 Then
            Me.clcPrecio_Venta.Enabled = False
            Me.clcUtilidad.Enabled = False
            Exit Sub
        End If
    End Sub
    Private Sub frmArticulosPrecios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oArticulosPrecios = New VillarrealBusiness.clsArticulosPrecios

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmArticulosPrecios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        If clcUtilidad.Value < 0 Then
            ShowMessage(MessageType.MsgInformation, "La utilidad es menor a 0")
        End If

        Response = oArticulosPrecios.Validacion(Action, lkpPrecio.EditValue, clcUtilidad.Value, CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value, Me.clcPrecio_Venta.Value)
    End Sub

    'Private Sub frmArticulosPrecios_Localize() Handles MyBase.Localize
    '    Find("Unknow", CType("Replace by a control", Object))

    'End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpPrecio_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles lkpPrecio.Validating
       
    End Sub

    Private Sub clcPrecio_Venta_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcPrecio_Venta.Validating
        'Dim oEvent As Dipros.Utils.Events
        ''If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        'oEvent = oArticulosPrecios.ValidaPrecio(Me.clcPrecio_Venta.EditValue, CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value)
        'If oEvent.ErrorFound Then
        '    oEvent.ShowError()
        '    e.Cancel = True
        'Else
        '    CalculaUtilidadDetalle()
        'End If   

    End Sub
    Private Sub clcUtilidad_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcUtilidad.Validating
        'Dim oEvent As Dipros.Utils.Events
        'If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        'oEvent = oArticulosPrecios.ValidaUtilidad(clcUtilidad.value)
        'If oEvent.ErrorFound Then
        '    oEvent.ShowError()
        '    e.Cancel = True
        'End If
        '  CalculaImporteDetalle()

    End Sub

    Private Sub clc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles clcPrecio_Venta.KeyPress, clcUtilidad.KeyPress
        bCargando = False
    End Sub
    Private Sub clcUtilidad_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcUtilidad.EditValueChanged
        If Me.clcUtilidad.IsLoading Or bCargando Then Exit Sub
        If IsNumeric(Me.clcUtilidad.EditValue) And bprecio = False Then
            'CalculaImporteDetalle()
            bdescuento = True
            CalculaPrecio()
            bdescuento = False
        End If
    End Sub
    Private Sub clcPrecio_Venta_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcPrecio_Venta.EditValueChanged
        If Me.clcPrecio_Venta.IsLoading Or bCargando Then Exit Sub
        If IsNumeric(Me.clcPrecio_Venta.EditValue) And bdescuento = False Then
            'CalculaUtilidadDetalle()
            bprecio = True
            CalculaDescuento()
            bprecio = False
        End If
    End Sub

    Private Sub lkpPrecio_LoadData(ByVal Initialize As Boolean) Handles lkpPrecio.LoadData
        Dim response As Events
        response = oPrecios.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpPrecio.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpPrecio_Format() Handles lkpPrecio.Format
        clsFormato.for_precios_grl(Me.lkpPrecio)
    End Sub
    Private Sub lkpPrecio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpPrecio.EditValueChanged
        If Me.lkpPrecio.DataSource Is Nothing Or Me.lkpPrecio.EditValue Is Nothing Then Exit Sub
        Me.lblLkpPrecio.Text = Me.lkpPrecio.Text
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CalculaImporteDetalle()
        'Dim oEvent As New Dipros.Utils.Events
        'oEvent = CType(OwnerForm, frmArticulos).oArticulos.CalculaImporte(clcUtilidad.Value, CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value)
        'clcPrecio_Venta.EditValue = oEvent.Value
    End Sub
    Private Sub CalculaUtilidadDetalle()
        'Dim oEvent As New Dipros.Utils.Events
        '  oEvent = CType(OwnerForm, frmArticulos).oArticulos.CalculaUtilidad(clcPrecio_Venta.Value, CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value)
        'clcUtilidad.EditValue = oEvent.Value
    End Sub

    Private Sub CalculaPrecio()
        Dim precio As Double
        Dim precio_terminacion As Double
        Dim sprecio() As String
        Dim sprecio2 As String
        Dim ultimo_digito As String
     
        precio = CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value - (CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value * (Me.clcUtilidad.EditValue / 100))
        precio = System.Math.Round(precio)

        sprecio = precio.ToString.Split(".")
        sprecio2 = sprecio(0)

        If IsNumeric(clcterminacion.EditValue) Then
            ultimo_digito = sprecio2.Trim.Substring(sprecio2.Length - 1, 1)
            If CType(ultimo_digito, Long) > 5 And CType(ultimo_digito, Long) > clcterminacion.Value Then
                precio = precio + 10
            End If
        End If

        sprecio = precio.ToString.Split(".")
        sprecio2 = sprecio(0)

        sprecio2 = sprecio2.Substring(0, sprecio2.Length - 1) + clcterminacion.Value.ToString
        precio_terminacion = CType(sprecio2, Double)

        Me.clcPrecio_Venta.Value = precio_terminacion


        '---------------------SE BASO EN ESTE FRAGMENTO DE LA CLASE DE CLSARTICULOS DE NEGOCIOS-------------------------------
        ' --------------------BORRAR EN CUANTO QUEDE LIBERADA ESTA MODIFICACION

        'sprecio = precio.ToString.Split(".")
        'sprecio2 = sprecio(0)

        'ultimo_digito = sprecio2.Trim.Substring(sprecio2.Length - 1, 1)
        'If CType(ultimo_digito, Long) > 5 And CType(ultimo_digito, Long) > terminacion Then
        '    precio = precio + 10
        'End If

        'sprecio = precio.ToString.Split(".")
        'sprecio2 = sprecio(0)

        'sprecio2 = sprecio2.Substring(0, sprecio2.Length - 1) + terminacion.ToString
        'precio_terminacion = CType(sprecio2, Double)
        'oEvent.Value = precio_terminacion
    End Sub

    Private Sub CalculaDescuento()
        If Me.lkpPrecio.EditValue <> Me.clcprecio_expo.EditValue Then
            Me.clcUtilidad.Value = ((CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value - clcPrecio_Venta.Value) / CType(OwnerForm, frmArticulos).clcPrecio_Lista.Value) * 100

        End If
    End Sub
#End Region






    
   
End Class
