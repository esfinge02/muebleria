Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Imports System.Windows.Forms


Public Class frmVentasDetalle
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"

    Dim KS As Keys
    Private banCargando As Boolean = True
    Private banSobrePedido As Boolean = False
    Private articulo_temporal As Long = 0
    Private dsHelper As DataSetHelper.DataSetHelper


#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lbl_ePartida As System.Windows.Forms.Label
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPreciounitario As System.Windows.Forms.Label
    Friend WithEvents clcPreciounitario As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents clcTotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkSobrepedido As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents chkSurtido As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkReparto As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents grBodegas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvBodegas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaBodegas As Dipros.Windows.TINMaster
    Friend WithEvents grcSeleccionada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSeleccionada As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFisica As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPorSurtir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPorSurtirSobrePedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpPrecios As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcNBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtNBodega As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcPrecioMinimo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFolioHistorial As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionCorta As System.Windows.Forms.Label
    Friend WithEvents txtNArticulo As System.Windows.Forms.Label
    Friend WithEvents clcCosto As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents chkRegalo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents clcPrecioContado As Dipros.Editors.TINCalcEdit
    Friend WithEvents grcPrincipal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtmodelo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcFolioAutorizacion As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grcFecha_Promesa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidadVendida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnFotografia As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents clcPrecioPactado As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtDescripcionEspecial As DevExpress.XtraEditors.MemoEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVentasDetalle))
        Me.lbl_ePartida = New System.Windows.Forms.Label
        Me.lblPartida = New System.Windows.Forms.Label
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.lblPreciounitario = New System.Windows.Forms.Label
        Me.clcPreciounitario = New Dipros.Editors.TINCalcEdit
        Me.lblTotal = New System.Windows.Forms.Label
        Me.clcTotal = New Dipros.Editors.TINCalcEdit
        Me.chkSobrepedido = New DevExpress.XtraEditors.CheckEdit
        Me.lblCosto = New System.Windows.Forms.Label
        Me.chkSurtido = New DevExpress.XtraEditors.CheckEdit
        Me.chkReparto = New DevExpress.XtraEditors.CheckEdit
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.grBodegas = New DevExpress.XtraGrid.GridControl
        Me.grvBodegas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSeleccionada = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcNBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFisica = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPorSurtir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPorSurtirSobrePedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrincipal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha_Promesa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidadVendida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaBodegas = New Dipros.Windows.TINMaster
        Me.lkpPrecios = New Dipros.Editors.TINMultiLookup
        Me.txtNBodega = New DevExpress.XtraEditors.TextEdit
        Me.clcPrecioMinimo = New Dipros.Editors.TINCalcEdit
        Me.lblFolioHistorial = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblDescripcionCorta = New System.Windows.Forms.Label
        Me.txtNArticulo = New System.Windows.Forms.Label
        Me.clcCosto = New DevExpress.XtraEditors.CalcEdit
        Me.chkRegalo = New DevExpress.XtraEditors.CheckEdit
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.clcPrecioContado = New Dipros.Editors.TINCalcEdit
        Me.txtmodelo = New DevExpress.XtraEditors.TextEdit
        Me.clcFolioAutorizacion = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnFotografia = New DevExpress.XtraEditors.SimpleButton
        Me.clcPrecioPactado = New Dipros.Editors.TINCalcEdit
        Me.txtDescripcionEspecial = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPreciounitario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSobrepedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSurtido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grBodegas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvBodegas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSeleccionada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioMinimo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRegalo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioContado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioAutorizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioPactado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcionEspecial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3097, 28)
        '
        'lbl_ePartida
        '
        Me.lbl_ePartida.AutoSize = True
        Me.lbl_ePartida.Location = New System.Drawing.Point(56, 32)
        Me.lbl_ePartida.Name = "lbl_ePartida"
        Me.lbl_ePartida.Size = New System.Drawing.Size(48, 16)
        Me.lbl_ePartida.TabIndex = 0
        Me.lbl_ePartida.Tag = ""
        Me.lbl_ePartida.Text = "Part&ida:"
        Me.lbl_ePartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(112, 32)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(11, 16)
        Me.lblPartida.TabIndex = 1
        Me.lblPartida.Tag = "partida"
        Me.lblPartida.Text = "0"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(53, 104)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 6
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Art�c&ulo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = True
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(112, 104)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(400, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = "modelo"
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.SearchMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(192, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 7
        Me.lkpArticulo.Tag = "Articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(46, 152)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(58, 16)
        Me.lblCantidad.TabIndex = 10
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "Ca&ntidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = "0"
        Me.clcCantidad.Location = New System.Drawing.Point(112, 152)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatString = "n0"
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatString = "n0"
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcCantidad.Size = New System.Drawing.Size(48, 19)
        Me.clcCantidad.TabIndex = 11
        Me.clcCantidad.Tag = "cantidad"
        '
        'lblPreciounitario
        '
        Me.lblPreciounitario.AutoSize = True
        Me.lblPreciounitario.Location = New System.Drawing.Point(16, 176)
        Me.lblPreciounitario.Name = "lblPreciounitario"
        Me.lblPreciounitario.Size = New System.Drawing.Size(88, 16)
        Me.lblPreciounitario.TabIndex = 12
        Me.lblPreciounitario.Tag = ""
        Me.lblPreciounitario.Text = "Preci&o unitario:"
        Me.lblPreciounitario.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPreciounitario
        '
        Me.clcPreciounitario.EditValue = "0"
        Me.clcPreciounitario.Location = New System.Drawing.Point(112, 176)
        Me.clcPreciounitario.MaxValue = 0
        Me.clcPreciounitario.MinValue = 0
        Me.clcPreciounitario.Name = "clcPreciounitario"
        '
        'clcPreciounitario.Properties
        '
        Me.clcPreciounitario.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcPreciounitario.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPreciounitario.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcPreciounitario.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPreciounitario.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPreciounitario.Properties.Precision = 2
        Me.clcPreciounitario.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPreciounitario.Size = New System.Drawing.Size(80, 19)
        Me.clcPreciounitario.TabIndex = 13
        Me.clcPreciounitario.Tag = "preciounitario"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(68, 200)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(36, 16)
        Me.lblTotal.TabIndex = 14
        Me.lblTotal.Tag = ""
        Me.lblTotal.Text = "&Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotal
        '
        Me.clcTotal.EditValue = "0"
        Me.clcTotal.Location = New System.Drawing.Point(112, 200)
        Me.clcTotal.MaxValue = 0
        Me.clcTotal.MinValue = 0
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.Enabled = False
        Me.clcTotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotal.Properties.Precision = 2
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Size = New System.Drawing.Size(80, 19)
        Me.clcTotal.TabIndex = 15
        Me.clcTotal.Tag = "total"
        '
        'chkSobrepedido
        '
        Me.chkSobrepedido.EditValue = "False"
        Me.chkSobrepedido.Location = New System.Drawing.Point(112, 224)
        Me.chkSobrepedido.Name = "chkSobrepedido"
        '
        'chkSobrepedido.Properties
        '
        Me.chkSobrepedido.Properties.Caption = "P&edido a Fabrica"
        Me.chkSobrepedido.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkSobrepedido.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkSobrepedido.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkSobrepedido.Size = New System.Drawing.Size(112, 19)
        Me.chkSobrepedido.TabIndex = 16
        Me.chkSobrepedido.Tag = "sobrepedido"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Enabled = False
        Me.lblCosto.Location = New System.Drawing.Point(352, 152)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(40, 16)
        Me.lblCosto.TabIndex = 23
        Me.lblCosto.Tag = ""
        Me.lblCosto.Text = "&Costo:"
        Me.lblCosto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCosto.Visible = False
        '
        'chkSurtido
        '
        Me.chkSurtido.EditValue = "False"
        Me.chkSurtido.Location = New System.Drawing.Point(208, 152)
        Me.chkSurtido.Name = "chkSurtido"
        '
        'chkSurtido.Properties
        '
        Me.chkSurtido.Properties.Caption = "Surtido"
        Me.chkSurtido.Properties.Enabled = False
        Me.chkSurtido.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkSurtido.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkSurtido.Size = New System.Drawing.Size(75, 19)
        Me.chkSurtido.TabIndex = 25
        Me.chkSurtido.Tag = "surtido"
        '
        'chkReparto
        '
        Me.chkReparto.EditValue = "False"
        Me.chkReparto.Location = New System.Drawing.Point(232, 224)
        Me.chkReparto.Name = "chkReparto"
        '
        'chkReparto.Properties
        '
        Me.chkReparto.Properties.Caption = "Repar&to"
        Me.chkReparto.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkReparto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkReparto.Size = New System.Drawing.Size(64, 19)
        Me.chkReparto.TabIndex = 18
        Me.chkReparto.Tag = "reparto"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Enabled = False
        Me.lblBodega.Location = New System.Drawing.Point(342, 200)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 26
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(400, 200)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(75, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 27
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'grBodegas
        '
        '
        'grBodegas.EmbeddedNavigator
        '
        Me.grBodegas.EmbeddedNavigator.Name = ""
        Me.grBodegas.Location = New System.Drawing.Point(8, 272)
        Me.grBodegas.MainView = Me.grvBodegas
        Me.grBodegas.Name = "grBodegas"
        Me.grBodegas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.grchkSeleccionada})
        Me.grBodegas.Size = New System.Drawing.Size(464, 152)
        Me.grBodegas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grBodegas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grBodegas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grBodegas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grBodegas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grBodegas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grBodegas.TabIndex = 29
        Me.grBodegas.Text = "Bodegas"
        '
        'grvBodegas
        '
        Me.grvBodegas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionada, Me.grcNBodega, Me.grcBodega, Me.grcFisica, Me.grcPorSurtir, Me.grcPorSurtirSobrePedido, Me.grcPrincipal, Me.grcFecha_Promesa, Me.grcCantidadVendida})
        Me.grvBodegas.GridControl = Me.grBodegas
        Me.grvBodegas.Name = "grvBodegas"
        Me.grvBodegas.OptionsCustomization.AllowFilter = False
        Me.grvBodegas.OptionsCustomization.AllowGroup = False
        Me.grvBodegas.OptionsCustomization.AllowSort = False
        Me.grvBodegas.OptionsView.ShowGroupPanel = False
        '
        'grcSeleccionada
        '
        Me.grcSeleccionada.ColumnEdit = Me.grchkSeleccionada
        Me.grcSeleccionada.FieldName = "seleccionada"
        Me.grcSeleccionada.Name = "grcSeleccionada"
        Me.grcSeleccionada.VisibleIndex = 0
        Me.grcSeleccionada.Width = 63
        '
        'grchkSeleccionada
        '
        Me.grchkSeleccionada.AutoHeight = False
        Me.grchkSeleccionada.Name = "grchkSeleccionada"
        Me.grchkSeleccionada.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcNBodega
        '
        Me.grcNBodega.Caption = "Bodega"
        Me.grcNBodega.FieldName = "n_bodega"
        Me.grcNBodega.Name = "grcNBodega"
        Me.grcNBodega.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNBodega.VisibleIndex = 1
        Me.grcNBodega.Width = 184
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.FieldName = "bodega"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodega.Width = 130
        '
        'grcFisica
        '
        Me.grcFisica.Caption = "Disponible"
        Me.grcFisica.FieldName = "fisica"
        Me.grcFisica.Name = "grcFisica"
        Me.grcFisica.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFisica.VisibleIndex = 2
        Me.grcFisica.Width = 101
        '
        'grcPorSurtir
        '
        Me.grcPorSurtir.Caption = "Por Surtir"
        Me.grcPorSurtir.FieldName = "por_surtir"
        Me.grcPorSurtir.Name = "grcPorSurtir"
        Me.grcPorSurtir.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPorSurtir.VisibleIndex = 3
        Me.grcPorSurtir.Width = 99
        '
        'grcPorSurtirSobrePedido
        '
        Me.grcPorSurtirSobrePedido.Caption = "Por Surtir Sobre Pedido"
        Me.grcPorSurtirSobrePedido.FieldName = "por_surtir_sobre_pedido"
        Me.grcPorSurtirSobrePedido.Name = "grcPorSurtirSobrePedido"
        Me.grcPorSurtirSobrePedido.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPorSurtirSobrePedido.Width = 135
        '
        'grcPrincipal
        '
        Me.grcPrincipal.Caption = "Principal"
        Me.grcPrincipal.ColumnEdit = Me.grchkSeleccionada
        Me.grcPrincipal.FieldName = "bodega_principal"
        Me.grcPrincipal.Name = "grcPrincipal"
        Me.grcPrincipal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFecha_Promesa
        '
        Me.grcFecha_Promesa.Caption = "Fecha_Promesa"
        Me.grcFecha_Promesa.FieldName = "fecha_promesa"
        Me.grcFecha_Promesa.Name = "grcFecha_Promesa"
        Me.grcFecha_Promesa.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha_Promesa.VisibleIndex = 4
        '
        'grcCantidadVendida
        '
        Me.grcCantidadVendida.Caption = "Vendidas"
        Me.grcCantidadVendida.FieldName = "cantidad_vendida"
        Me.grcCantidadVendida.Name = "grcCantidadVendida"
        Me.grcCantidadVendida.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidadVendida.VisibleIndex = 5
        '
        'tmaBodegas
        '
        Me.tmaBodegas.BackColor = System.Drawing.Color.White
        Me.tmaBodegas.CanDelete = False
        Me.tmaBodegas.CanInsert = False
        Me.tmaBodegas.CanUpdate = True
        Me.tmaBodegas.Grid = Me.grBodegas
        Me.tmaBodegas.Location = New System.Drawing.Point(8, 280)
        Me.tmaBodegas.Name = "tmaBodegas"
        Me.tmaBodegas.Size = New System.Drawing.Size(464, 23)
        Me.tmaBodegas.TabIndex = 87
        Me.tmaBodegas.Title = "Bodegas"
        Me.tmaBodegas.UpdateTitle = "una Bodega"
        '
        'lkpPrecios
        '
        Me.lkpPrecios.AllowAdd = False
        Me.lkpPrecios.AutoReaload = False
        Me.lkpPrecios.DataSource = Nothing
        Me.lkpPrecios.DefaultSearchField = ""
        Me.lkpPrecios.DisplayMember = "precio_venta"
        Me.lkpPrecios.EditValue = Nothing
        Me.lkpPrecios.Filtered = False
        Me.lkpPrecios.InitValue = Nothing
        Me.lkpPrecios.Location = New System.Drawing.Point(377, 32)
        Me.lkpPrecios.MultiSelect = False
        Me.lkpPrecios.Name = "lkpPrecios"
        Me.lkpPrecios.NullText = ""
        Me.lkpPrecios.PopupWidth = CType(230, Long)
        Me.lkpPrecios.ReadOnlyControl = False
        Me.lkpPrecios.Required = False
        Me.lkpPrecios.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPrecios.SearchMember = ""
        Me.lkpPrecios.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPrecios.SelectAll = False
        Me.lkpPrecios.Size = New System.Drawing.Size(98, 20)
        Me.lkpPrecios.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPrecios.TabIndex = 20
        Me.lkpPrecios.Tag = ""
        Me.lkpPrecios.ToolTip = Nothing
        Me.lkpPrecios.ValueMember = "precio_venta"
        Me.lkpPrecios.Visible = False
        '
        'txtNBodega
        '
        Me.txtNBodega.EditValue = "n_bodega"
        Me.txtNBodega.Location = New System.Drawing.Point(400, 224)
        Me.txtNBodega.Name = "txtNBodega"
        Me.txtNBodega.Size = New System.Drawing.Size(75, 20)
        Me.txtNBodega.TabIndex = 28
        Me.txtNBodega.Tag = "n_bodega"
        '
        'clcPrecioMinimo
        '
        Me.clcPrecioMinimo.EditValue = "0"
        Me.clcPrecioMinimo.Location = New System.Drawing.Point(400, 128)
        Me.clcPrecioMinimo.MaxValue = 0
        Me.clcPrecioMinimo.MinValue = 0
        Me.clcPrecioMinimo.Name = "clcPrecioMinimo"
        '
        'clcPrecioMinimo.Properties
        '
        Me.clcPrecioMinimo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioMinimo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioMinimo.Properties.Enabled = False
        Me.clcPrecioMinimo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecioMinimo.Properties.Precision = 2
        Me.clcPrecioMinimo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecioMinimo.Size = New System.Drawing.Size(75, 19)
        Me.clcPrecioMinimo.TabIndex = 22
        Me.clcPrecioMinimo.Tag = "precio_minimo"
        '
        'lblFolioHistorial
        '
        Me.lblFolioHistorial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFolioHistorial.Enabled = False
        Me.lblFolioHistorial.Location = New System.Drawing.Point(288, 32)
        Me.lblFolioHistorial.Name = "lblFolioHistorial"
        Me.lblFolioHistorial.Size = New System.Drawing.Size(80, 16)
        Me.lblFolioHistorial.TabIndex = 19
        Me.lblFolioHistorial.Tag = "folio_historico_costo"
        Me.lblFolioHistorial.Text = "0"
        Me.lblFolioHistorial.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 128)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Descripci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescripcionCorta
        '
        Me.lblDescripcionCorta.Enabled = False
        Me.lblDescripcionCorta.Location = New System.Drawing.Point(208, 192)
        Me.lblDescripcionCorta.Name = "lblDescripcionCorta"
        Me.lblDescripcionCorta.Size = New System.Drawing.Size(128, 20)
        Me.lblDescripcionCorta.TabIndex = 21
        Me.lblDescripcionCorta.Tag = ""
        Me.lblDescripcionCorta.Text = "descripcion_corta"
        Me.lblDescripcionCorta.Visible = False
        '
        'txtNArticulo
        '
        Me.txtNArticulo.Location = New System.Drawing.Point(112, 128)
        Me.txtNArticulo.Name = "txtNArticulo"
        Me.txtNArticulo.Size = New System.Drawing.Size(280, 16)
        Me.txtNArticulo.TabIndex = 9
        Me.txtNArticulo.Tag = "n_articulo"
        Me.txtNArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'clcCosto
        '
        Me.clcCosto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCosto.Location = New System.Drawing.Point(400, 152)
        Me.clcCosto.Name = "clcCosto"
        '
        'clcCosto.Properties
        '
        Me.clcCosto.Properties.Enabled = False
        Me.clcCosto.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcCosto.Size = New System.Drawing.Size(75, 20)
        Me.clcCosto.TabIndex = 24
        Me.clcCosto.Tag = "costo"
        '
        'chkRegalo
        '
        Me.chkRegalo.Location = New System.Drawing.Point(320, 224)
        Me.chkRegalo.Name = "chkRegalo"
        '
        'chkRegalo.Properties
        '
        Me.chkRegalo.Properties.Caption = "Regalo"
        Me.chkRegalo.Properties.Enabled = False
        Me.chkRegalo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkRegalo.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkRegalo.Size = New System.Drawing.Size(75, 19)
        Me.chkRegalo.TabIndex = 17
        Me.chkRegalo.Tag = "articulo_regalo"
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = True
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(112, 80)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(274, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 5
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(61, 80)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 4
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = True
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(112, 56)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(274, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 3
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(16, 56)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 2
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecioContado
        '
        Me.clcPrecioContado.EditValue = "0"
        Me.clcPrecioContado.Location = New System.Drawing.Point(272, 152)
        Me.clcPrecioContado.MaxValue = 0
        Me.clcPrecioContado.MinValue = 0
        Me.clcPrecioContado.Name = "clcPrecioContado"
        '
        'clcPrecioContado.Properties
        '
        Me.clcPrecioContado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioContado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioContado.Properties.Enabled = False
        Me.clcPrecioContado.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecioContado.Properties.Precision = 2
        Me.clcPrecioContado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecioContado.Size = New System.Drawing.Size(75, 19)
        Me.clcPrecioContado.TabIndex = 88
        Me.clcPrecioContado.Tag = "precio_contado"
        Me.clcPrecioContado.Visible = False
        '
        'txtmodelo
        '
        Me.txtmodelo.EditValue = ""
        Me.txtmodelo.Location = New System.Drawing.Point(400, 72)
        Me.txtmodelo.Name = "txtmodelo"
        '
        'txtmodelo.Properties
        '
        Me.txtmodelo.Properties.Enabled = False
        Me.txtmodelo.Size = New System.Drawing.Size(75, 20)
        Me.txtmodelo.TabIndex = 89
        Me.txtmodelo.TabStop = False
        Me.txtmodelo.Tag = "modelo"
        Me.txtmodelo.Visible = False
        '
        'clcFolioAutorizacion
        '
        Me.clcFolioAutorizacion.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolioAutorizacion.Location = New System.Drawing.Point(400, 175)
        Me.clcFolioAutorizacion.Name = "clcFolioAutorizacion"
        Me.clcFolioAutorizacion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolioAutorizacion.Size = New System.Drawing.Size(75, 20)
        Me.clcFolioAutorizacion.TabIndex = 90
        Me.clcFolioAutorizacion.Tag = "folio_autorizacion"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(288, 176)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(106, 16)
        Me.Label3.TabIndex = 91
        Me.Label3.Tag = ""
        Me.Label3.Text = "Folio Autori&zacion:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 250)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(448, 19)
        Me.Label4.TabIndex = 92
        Me.Label4.Text = "Se toman en Cuenta las cantidades de los articulos ya ingresados"
        '
        'btnFotografia
        '
        Me.btnFotografia.Enabled = False
        Me.btnFotografia.Location = New System.Drawing.Point(400, 96)
        Me.btnFotografia.Name = "btnFotografia"
        Me.btnFotografia.TabIndex = 93
        Me.btnFotografia.Text = "Fotografia"
        '
        'clcPrecioPactado
        '
        Me.clcPrecioPactado.EditValue = "0"
        Me.clcPrecioPactado.Location = New System.Drawing.Point(208, 176)
        Me.clcPrecioPactado.MaxValue = 0
        Me.clcPrecioPactado.MinValue = 0
        Me.clcPrecioPactado.Name = "clcPrecioPactado"
        '
        'clcPrecioPactado.Properties
        '
        Me.clcPrecioPactado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioPactado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioPactado.Properties.Enabled = False
        Me.clcPrecioPactado.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecioPactado.Properties.Precision = 2
        Me.clcPrecioPactado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecioPactado.Size = New System.Drawing.Size(75, 19)
        Me.clcPrecioPactado.TabIndex = 94
        Me.clcPrecioPactado.Tag = "precio_pactado"
        Me.clcPrecioPactado.Visible = False
        '
        'txtDescripcionEspecial
        '
        Me.txtDescripcionEspecial.EditValue = ""
        Me.txtDescripcionEspecial.Location = New System.Drawing.Point(168, 32)
        Me.txtDescripcionEspecial.Name = "txtDescripcionEspecial"
        '
        'txtDescripcionEspecial.Properties
        '
        Me.txtDescripcionEspecial.Properties.Enabled = False
        Me.txtDescripcionEspecial.Size = New System.Drawing.Size(112, 16)
        Me.txtDescripcionEspecial.TabIndex = 95
        Me.txtDescripcionEspecial.Tag = "descripcion_especial"
        Me.txtDescripcionEspecial.Visible = False
        '
        'frmVentasDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(482, 432)
        Me.Controls.Add(Me.txtDescripcionEspecial)
        Me.Controls.Add(Me.clcPrecioPactado)
        Me.Controls.Add(Me.btnFotografia)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.clcFolioAutorizacion)
        Me.Controls.Add(Me.txtmodelo)
        Me.Controls.Add(Me.clcPrecioContado)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.chkRegalo)
        Me.Controls.Add(Me.clcCosto)
        Me.Controls.Add(Me.txtNArticulo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grBodegas)
        Me.Controls.Add(Me.lblDescripcionCorta)
        Me.Controls.Add(Me.lblFolioHistorial)
        Me.Controls.Add(Me.clcPrecioMinimo)
        Me.Controls.Add(Me.txtNBodega)
        Me.Controls.Add(Me.tmaBodegas)
        Me.Controls.Add(Me.lbl_ePartida)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.clcCantidad)
        Me.Controls.Add(Me.lblPreciounitario)
        Me.Controls.Add(Me.clcPreciounitario)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.clcTotal)
        Me.Controls.Add(Me.chkSobrepedido)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.chkSurtido)
        Me.Controls.Add(Me.chkReparto)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lkpPrecios)
        Me.DefaultDock = True
        Me.Name = "frmVentasDetalle"
        Me.Text = "frmVentasDetalle"
        Me.Controls.SetChildIndex(Me.lkpPrecios, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.chkReparto, 0)
        Me.Controls.SetChildIndex(Me.chkSurtido, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.chkSobrepedido, 0)
        Me.Controls.SetChildIndex(Me.clcTotal, 0)
        Me.Controls.SetChildIndex(Me.lblTotal, 0)
        Me.Controls.SetChildIndex(Me.clcPreciounitario, 0)
        Me.Controls.SetChildIndex(Me.lblPreciounitario, 0)
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lbl_ePartida, 0)
        Me.Controls.SetChildIndex(Me.tmaBodegas, 0)
        Me.Controls.SetChildIndex(Me.txtNBodega, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioMinimo, 0)
        Me.Controls.SetChildIndex(Me.lblFolioHistorial, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcionCorta, 0)
        Me.Controls.SetChildIndex(Me.grBodegas, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtNArticulo, 0)
        Me.Controls.SetChildIndex(Me.clcCosto, 0)
        Me.Controls.SetChildIndex(Me.chkRegalo, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioContado, 0)
        Me.Controls.SetChildIndex(Me.txtmodelo, 0)
        Me.Controls.SetChildIndex(Me.clcFolioAutorizacion, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.btnFotografia, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioPactado, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcionEspecial, 0)
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPreciounitario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSobrepedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSurtido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grBodegas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvBodegas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSeleccionada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioMinimo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRegalo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioContado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioAutorizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioPactado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcionEspecial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oVentasDetalle As New VillarrealBusiness.clsVentasDetalle
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oArticulosExistencias As New VillarrealBusiness.clsArticulosExistencias
    Private oArticulosPrecios As New VillarrealBusiness.clsArticulosPrecios
    Private oHistorialCostos As New VillarrealBusiness.clsHisCostos
    Private oDescuentosEspecialesProgramados As New VillarrealBusiness.clsDescuentosEspecialesProgramados
    Private oAutorizaciones As New VillarrealBusiness.clsAutorizacionesVentasPedidoFabrica

    Private ENTRO_ARTICULO_DIRECTO As Boolean = False

    Private lCategoriasIdentificablesArticulo As Long = -1
    Private bNo_Resurtir As Boolean = False


    Private catalago_pedido_fabrica As Boolean = False
    Private catalago_costo_pedido_fabrica As Double = 0
    Private catalogo_bodega_pedido_fabrica As String = ""
    Private Precio_contado_articulo As Double = 0







    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
    Private ReadOnly Property Bodega() As String
        Get
            Return PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property

    ReadOnly Property Cliente() As Long
        Get
            Return CType(OwnerForm, frmVentas).Cliente
        End Get
    End Property
    ReadOnly Property Vendedor() As Long
        Get
            Return CType(OwnerForm, frmVentas).Vendedor
        End Get
    End Property

    Private Property ArticuloRegalo() As Boolean
        Get
            Return Me.chkRegalo.Checked
        End Get
        Set(ByVal Value As Boolean)
            Me.chkRegalo.Checked = Value
        End Set
    End Property
    Private Property No_Resurtir() As Boolean
        Get
            Return bNo_Resurtir
        End Get
        Set(ByVal Value As Boolean)
            bNo_Resurtir = Value
        End Set
    End Property

    Private ReadOnly Property FolioDescuento() As Long
        Get
            Return CType(Me.OwnerForm, frmVentas).FolioDescuento
        End Get
    End Property


    Private ReadOnly Property Tiene_Precio_Expo() As Boolean
        Get
            Return CType(Me.OwnerForm, frmVentas).Tiene_Precio_Expo
        End Get
    End Property

 
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmVentasDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept

        With OwnerForm.MasterControl

            Select Case Action

                Case Actions.Insert

                    'CargarCostosArticulos() 
                    .AddRow(Me.DataSource)
                    OwnerForm.dteFechaPedido.Enabled = False
                    CType(OwnerForm, frmVentas).CategoriasIdentificables = Me.lCategoriasIdentificablesArticulo

                Case Actions.Update

                    'CargarCostosArticulos() 
                    .UpdateRow(Me.DataSource)

                Case Actions.Delete

                    .DeleteRow()
                    CType(OwnerForm, frmVentas).CategoriasIdentificables = -1

            End Select

        End With

        OwnerForm.DatosImportacion(Action)
        OwnerForm.RecalculaTotales()

    End Sub
    Private Sub frmVentasDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields

        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        'DEPARTAMENTO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("departamento")) Then

            Me.lkpDepartamento.EditValue = -1

        Else

            Me.lkpDepartamento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("departamento")), Long)

        End If
        'GRUPO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("grupo")) Then

            Me.lkpGrupo.EditValue = -1

        Else

            Me.lkpGrupo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("grupo")), Long)

        End If
        'ARTICULO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("articulo")) Then

            Me.lkpArticulo.EditValue = -1

        Else

            Me.lkpArticulo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("articulo")), Long)
            If Me.lkpArticulo.EditValue > 0 Then

                If Departamento <= 0 Or Grupo <= 0 Then

                    Actualiza_Lookups()

                End If

            End If

        End If

    End Sub
    Private Sub frmVentasDetalle_Initialize(ByRef Response As Events) Handles MyBase.Initialize

        Me.lkpBodega.Enabled = False
        Me.lkpBodega.Visible = False

        Select Case Action
            Case Actions.Insert

                Me.clcCantidad.Value = CInt(1)

            Case Actions.Update

                CargarBodegas() 'CSEQUERA 08/01/2007 Colocada para que cargue el valor de la bandera sobrepedido
                Me.lkpDepartamento.Enabled = False
                Me.lkpGrupo.Enabled = False
                Me.lkpArticulo.Enabled = False
                SeleccionarBodegaGrid()

            Case Actions.Delete

                CargarBodegas() 'CSEQUERA 08/01/2007 Colocada para que cargue el valor de la bandera sobrepedido
                SeleccionarBodegaGrid()

        End Select

    End Sub
    Private Sub frmVentasDetalle_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        If Action = Actions.Insert Or Action = Actions.Update Then
            Response = oVentasDetalle.Validacion(Action, Articulo, Me.clcCantidad.EditValue, Bodega, Me.clcPreciounitario.EditValue, ArticuloRegalo, Me.Departamento, Me.Grupo, lCategoriasIdentificablesArticulo, CType(Me.OwnerForm, frmVentas).CategoriasIdentificables, Me.chkSobrepedido.Checked, FolioDescuento)

            If Not Response.ErrorFound Then
                Dim i As Integer = 0
                For i = 0 To Me.grvBodegas.DataRowCount - 1
                    If lkpBodega.EditValue = Me.grvBodegas.GetRowCellValue(i, Me.grcBodega) And Me.grvBodegas.GetRowCellValue(i, Me.grcSeleccionada) = True Then
                        Dim fisica As Integer = Me.grvBodegas.GetRowCellValue(i, Me.grcFisica)
                        If Not Me.ValidarCantidad(fisica) Then
                            Response.Message = "La cantidad disponible es menor que la solicitada"
                        End If
                    End If
                Next
            End If


            If Not Response.ErrorFound And (Action = Actions.Insert Or Action = Actions.Update) And Me.chkSobrepedido.Checked = True And ArticuloRegalo = False Then

                Dim odataset As DataSet = Me.oDescuentosEspecialesProgramados.DespliegaDatosVentas(Articulo, -1, CType(OwnerForm, frmVentas).dteFecha.DateTime)
                If odataset.Tables(0).Rows.Count = 0 And Me.Tiene_Precio_Expo = False Then
                    Response = oVentasDetalle.Validacion_SobrePedido(Me.clcFolioAutorizacion.Value, Articulo, Bodega, Cliente, Vendedor, Me.clcCantidad.Value)
                End If


            End If

            If Not Response.ErrorFound And (Action = Actions.Insert Or Action = Actions.Update) And Me.chkSobrepedido.Checked = False Then
                Response = oArticulosExistencias.ValidaExistenciaArticuloBodega(Articulo, Bodega, Cantidad_por_Articulo(Action, Articulo, Bodega, clcCantidad.EditValue))
            End If


            If Not Response.ErrorFound Then

                If Action = Actions.Insert And Not NumeroArticulosAgrupados() Then
                    Response.Message = "No se permite agregar mas 17 Partidas por Factura"
                End If

            End If
        End If


    End Sub

    Private Sub frmVentasDetalle_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.lblCosto.Visible = False
        Me.chkSurtido.Visible = False
        Me.lblBodega.Visible = False
        'Me.lkpBodega.Visible = False
        Me.txtNBodega.Visible = False
        Me.clcPrecioMinimo.Visible = False
        Me.clcPreciounitario.Enabled = False
        Me.clcCantidad.Value = CInt(1)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#Region "Lookup"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData

        Dim Response As New Events

        Response = oDepartamentos.Lookup()

        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")
        Me.lkpGrupo.EditValue = Nothing

    End Sub

    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpGrupo.Text = Me.lkpGrupo.GetValue("descripcion")
        Me.lkpArticulo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData

        Dim Response As New Events

        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData

        Dim Response As New Events
        Response = oArticulos.Lookup(Departamento, Grupo)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged

        If Articulo <> -1 Then


            Me.clcCantidad.Value = CInt(1)

            If articulo_temporal <> Articulo Then articulo_temporal = Articulo
            Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            Me.lkpArticulo.EditValue = articulo_temporal
            Me.txtNArticulo.Text = Me.lkpArticulo.GetValue("descripcion_corta")
            Me.txtmodelo.Text = Me.lkpArticulo.GetValue("modelo")
            Me.txtDescripcionEspecial.Text = Me.lkpArticulo.GetValue("descripcion_especial")

            'Guarda si es articulo de regalo
            ArticuloRegalo = Me.lkpArticulo.GetValue("articulo_regalo")
            No_Resurtir = CType(Me.lkpArticulo.GetValue("no_resurtir"), Boolean)

            Me.chkReparto.Checked = CType(Me.lkpArticulo.GetValue("entrega_domicilio"), Boolean)

            Me.catalago_pedido_fabrica = CType(Me.lkpArticulo.GetValue("pedido_fabrica"), Boolean)
            Me.catalago_costo_pedido_fabrica = CType(Me.lkpArticulo.GetValue("costo_pedido_fabrica"), Double)
            Me.catalogo_bodega_pedido_fabrica = CType(Me.lkpArticulo.GetValue("bodega_pedido_fabrica"), String)

            VerficaSobrePedidoFabrica()

            'Poner Precios
            Me.clcPreciounitario.EditValue = oArticulos.ArticuloPrecioVenta(Articulo, uti_SucursalPrecioVenta(Comunes.Common.Sucursal_Actual), OwnerForm.sucursal_dependencia, OwnerForm.dteFechaPedido.datetime, 0).Value
            Me.clcPrecioMinimo.EditValue = oArticulos.ArticuloPrecioVenta(Articulo, uti_SucursalPrecioMinimoVenta(Comunes.Common.Sucursal_Actual), OwnerForm.sucursal_dependencia, OwnerForm.dteFechaPedido.datetime, 0).Value

            If CType(Me.OwnerForm, frmVentas).Tiene_Precio_Expo = True Then
                Dim precio_expo As Long = -1

                precio_expo = CType(Me.OwnerForm, frmVentas).Precio_Expo
                Precio_contado_articulo = oArticulos.ArticuloPrecioVenta(Articulo, precio_expo, OwnerForm.sucursal_dependencia, OwnerForm.dteFechaPedido.datetime, 0).Value
                If Precio_contado_articulo = 0 Then
                    Precio_contado_articulo = oArticulos.ArticuloPrecioVenta(Articulo, Comunes.clsUtilerias.uti_VariablesPrecioContado(), OwnerForm.sucursal_dependencia, OwnerForm.dteFechaPedido.datetime, 0).Value
                End If

            Else
                Precio_contado_articulo = oArticulos.ArticuloPrecioVenta(Articulo, Comunes.clsUtilerias.uti_VariablesPrecioContado(), OwnerForm.sucursal_dependencia, OwnerForm.dteFechaPedido.datetime, 0).Value
            End If

            'si venta es de sucursal de dependencia
            'obtener del departamento del art el porcentaje_incremento_convenios
            'para incrementarselo al precio del articulo
            If OwnerForm.sucursal_dependencia Then

                Dim porcentaje_incremento_convenios As Double
                porcentaje_incremento_convenios = CType(Me.lkpArticulo.GetValue("porcentaje_incremento_convenios"), Double)
                Me.clcPreciounitario.EditValue = Me.clcPreciounitario.EditValue + (Me.clcPreciounitario.EditValue * (porcentaje_incremento_convenios / 100))

            End If


            CargarBodegas()
            If Action = Actions.Insert Then lkpBodega.EditValue = Comunes.clsUtilerias.uti_BodegaDeUsuario(TinApp.Connection.User)
            SeleccionarBodegaGrid()

            Select Case Me.lkpArticulo.GetValue("no_identificable")
                Case True
                    lCategoriasIdentificablesArticulo = 0
                Case False
                    lCategoriasIdentificablesArticulo = 1

            End Select

            Me.clcCantidad.Focus()
            Me.grBodegas.Focus()
            Me.btnFotografia.Enabled = True

        Else
            Me.btnFotografia.Enabled = False

            Me.txtNArticulo.Text = ""
            grBodegas.DataSource = Nothing
            Me.lkpBodega.EditValue = Nothing
            Me.clcPreciounitario.EditValue = 0
            Me.clcPrecioMinimo.EditValue = 0
            Me.clcCantidad.Value = CInt(0)

            Me.chkSobrepedido.Enabled = False
            Me.chkSobrepedido.Checked = False
            Me.txtmodelo.Text = ""

        End If

    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData

        Dim response As Events
        response = oBodegas.Lookup()
        If Not response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If

        response = Nothing

    End Sub
    Private Sub lkpBodega_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodega.EditValueChanged

        If (Action = Actions.Update Or Action = Actions.Delete) And banCargando Then

            SeleccionarBodegaGrid()
            banCargando = False

        End If

        If Bodega <> "" Then

            Me.txtNBodega.Text = Me.lkpBodega.GetValue("descripcion")

        Else

            Me.txtNBodega.Text = ""

        End If

    End Sub

#End Region

    Private Sub clcPreciounitario_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcPreciounitario.EditValueChanged

        If Me.clcPreciounitario.Text.Length = 0 Then

            Me.clcPreciounitario.EditValue = 0

        End If

        RecalculaTotal()

    End Sub
    Private Sub clcCantidad_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcCantidad.EditValueChanged
        If Me.clcCantidad.Text.Length = 0 Then

            Me.clcCantidad.Value = CInt(0)

        End If

        RecalculaTotal()
    End Sub

    Private Sub grvBodegas_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvBodegas.CellValueChanging

        If e.Value = True Then

            Dim j As Integer = Me.grvBodegas.FocusedRowHandle
            Dim i As Integer = 0
            Dim fisica As Integer = Me.grvBodegas.GetRowCellValue(j, Me.grcFisica)

            If Me.ValidarCantidad(fisica) Then
                For i = 0 To Me.grvBodegas.DataRowCount - 1

                    If i <> j Then
                        Me.grvBodegas.SetRowCellValue(i, Me.grcSeleccionada, False)
                    Else
                        Me.grvBodegas.SetRowCellValue(i, Me.grcSeleccionada, True)
                        SeleccionarBodegaLkp()
                    End If

                Next

            Else

                Me.grvBodegas.SetRowCellValue(j, Me.grcSeleccionada, False)
                ShowMessage(MessageType.MsgInformation, "La cantidad disponible es menor que la solicitada")
            End If


        End If

        grvBodegas.CloseEditor()
        grvBodegas.UpdateCurrentRow()

    End Sub
    Private Sub chkSobrepedido_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSobrepedido.CheckedChanged

        If chkSobrepedido.Checked = True Then

            If Me.Tiene_Precio_Expo = True Then
                SeleccionaBodega()
            Else
                'valida que el art se pueda marcar sobrepedido
                If banSobrePedido = False And Me.clcFolioAutorizacion.Value = 0 And _
                    catalago_pedido_fabrica = False Then
                    ShowMessage(MessageType.MsgInformation, "No se puede marcar el Art�culo como Pedido a Fabrica", "Ventas", Nothing, False)
                    Me.chkSobrepedido.Checked = False
                    Me.grBodegas.Enabled = True


                Else
                    SeleccionaBodega()
                End If
            End If
        Else
            Me.grBodegas.Enabled = True
        End If

    End Sub
    Private Sub chkRegalo_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRegalo.EditValueChanged

        If Me.chkRegalo.Checked = True Then
            Me.chkSobrepedido.Checked = False
            Me.chkSobrepedido.Enabled = False
        Else
            Me.chkSobrepedido.Enabled = True
        End If

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub SeleccionaBodega()
        Dim I As Long
        Dim EX As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs

        Dim oevents As Events
        Dim odataset As DataSet
        Dim bodega_obtenida As String = ""


        If Me.clcFolioAutorizacion.Value > 0 Then
            'se obtiene la bodega de la autorizacion de venta pedido a fabrica
            oevents = oAutorizaciones.DespliegaDatos(Me.clcFolioAutorizacion.Value)
            odataset = oevents.Value
            bodega_obtenida = odataset.Tables(0).Rows(0).Item("bodega_pedido_fabrica")
        End If


        If bodega_obtenida.Trim.Length = 0 Then
            'se obtiene la bodega en base al catalogo de articulo
            bodega_obtenida = Me.catalogo_bodega_pedido_fabrica
        End If


        If bodega_obtenida.Trim.Length = 0 Then
            'Se revisa cual es la ultima bodega donde arribo este articulo
            bodega_obtenida = ObtenerUltimaBodegaPedidoFabrica()

            If bodega_obtenida.Trim.Length = 0 Then
                ShowMessage(MessageType.MsgInformation, "Se Requiere una Autorizacion de Pedido a Fabrica", "Ventas")
                Exit Sub
            End If
        End If

        'Se asgina la bodega al lkp para que sea tomado si el grid no tiene filas
        Me.lkpBodega.EditValue = bodega_obtenida

        For I = 0 To Me.grvBodegas.DataRowCount - 1

            If Me.grvBodegas.GetRowCellValue(I, Me.grcNBodega) = bodega_obtenida Then
                grvBodegas.SetRowCellValue(I, Me.grcSeleccionada, True)
                Me.grvBodegas.UpdateCurrentRow()
                SeleccionarBodegaLkp()
                Me.grBodegas.Enabled = False

            Else
                grvBodegas.SetRowCellValue(I, Me.grcSeleccionada, False)
            End If

            'If Me.grvBodegas.GetRowCellValue(I, Me.grcPrincipal) = True Then
            '    grvBodegas.SetRowCellValue(I, Me.grcSeleccionada, True)
            '    Me.grvBodegas.UpdateCurrentRow()
            '    SeleccionarBodegaLkp()
            '    Me.grBodegas.Enabled = False

            'Else
            '    grvBodegas.SetRowCellValue(I, Me.grcSeleccionada, False)
            'End If
        Next
    End Sub

    Private Sub SeleccionarBodegaGrid()

        Dim i As Integer = 0
        For i = 0 To Me.grvBodegas.DataRowCount - 1

            If lkpBodega.EditValue = Me.grvBodegas.GetRowCellValue(i, Me.grcBodega) Then
                Dim fisica As Integer = Me.grvBodegas.GetRowCellValue(i, Me.grcFisica)

                If Me.ValidarCantidad(fisica) Then
                    Me.grvBodegas.SetRowCellValue(i, Me.grcSeleccionada, True)
                    Me.grvBodegas.UpdateCurrentRow()
                End If

                'Exit For
            Else
                Me.grvBodegas.SetRowCellValue(i, Me.grcSeleccionada, False)
                Me.grvBodegas.UpdateCurrentRow()
            End If

        Next

    End Sub
    Private Sub SeleccionarBodegaLkp()

        lkpBodega.EditValue = Nothing
        Dim i As Integer = 0
        For i = 0 To Me.grvBodegas.DataRowCount - 1

            If Me.grvBodegas.GetRowCellValue(i, Me.grcSeleccionada) = True Then

                Me.lkpBodega.EditValue = Me.grvBodegas.GetRowCellValue(i, Me.grcBodega)
                Exit For

            End If

        Next

    End Sub

    Private Sub CargarBodegas()

        Dim odataset As DataSet
        Dim response As New Events
        response = oVentasDetalle.ListadoExistenciasArticulosBodegas(Articulo)
        If Not response.ErrorFound Then

            odataset = response.Value
            Me.grBodegas.DataSource = odataset.Tables(0)
            banSobrePedido = CType(odataset.Tables(1).Rows(0).Item("marcar_sobrepedido"), Boolean)

        End If

        odataset = Nothing

    End Sub
    Private Sub RecalculaTotal()

        Me.clcTotal.EditValue = Me.clcCantidad.EditValue * Me.clcPreciounitario.EditValue
        Me.clcPrecioContado.EditValue = Precio_contado_articulo 'Me.clcCantidad.EditValue * Precio_contado_articulo ' Me.clcPrecioContado.EditValue
        Me.clcPrecioPactado.EditValue = Precio_contado_articulo

    End Sub
    'Public Sub CargarCostosArticulos() 

    'Dim Response As Events
    'Dim oDataSet As DataSet

    'If AfectaCostos Then
    '    ' Traer costo del historial de costos
    '    Response = oHistorialCostos.UltimoCostoArticulo(Me.lkpArticulo.GetValue("articulo"))
    '    If Not Response.ErrorFound Then

    '        oDataSet = Response.Value
    '        If oDataSet.Tables(0).Rows.Count > 0 Then

    '            Me.clcCosto.EditValue = oDataSet.Tables(0).Rows(0).Item("costo")
    '            'lblImporte.Text = CType(lblCosto.Text, Decimal) * clcCantidad.Value
    '            lblFolioHistorial.Text = oDataSet.Tables(0).Rows(0).Item("folio")

    '        End If
    '    End If
    'Else

    '    'Traer ultimo costo de Articulos
    '    Response = oArticulos.DespliegaDatos(Me.lkpArticulo.GetValue("articulo"))
    '    If Not Response.ErrorFound Then

    '        oDataSet = Response.Value
    '        If oDataSet.Tables(0).Rows.Count > 0 Then

    '            Me.clcCosto.EditValue = oDataSet.Tables(0).Rows(0).Item("ultimo_costo")
    '            lblFolioHistorial.Text = -1

    '        End If
    '    End If
    'End If

    'oDataSet = Nothing

    'End Sub
    Private Sub Actualiza_Lookups()

        Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        Me.lkpDepartamento_LoadData(True)
        If Me.lkpDepartamento.EditValue > 0 Then

            Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")

        End If

        Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")

    End Sub

    Private Sub VerficaSobrePedidoFabrica()
        If No_Resurtir = True Then

            Me.chkSobrepedido.Enabled = False
            Me.chkSobrepedido.Checked = False

        Else
            Me.chkSobrepedido.Enabled = True
            Me.chkSobrepedido.Checked = False

        End If
    End Sub

    Private Function Cantidad_por_Articulo(ByVal Action As Actions, ByVal articulo As Long, ByVal bodega As String, ByVal cantidad As Double) As Double
        If Action = Actions.Insert Then
            Dim odetalle As DataView = CType(Me.OwnerForm, frmVentas).grvVentas.DataSource
            If odetalle.Table.Rows.Count > 0 Then
                Dim dv As New DataView
                dv.Table = odetalle.Table
                dv.RowFilter = "articulo = " & articulo.ToString & " and bodega = " & bodega & " and control in (0,1,2)"
                If dv.Count > 0 Then
                    Dim i As Long = 0
                    For i = 0 To dv.Count - 1
                        cantidad = cantidad + dv.Table.Rows(i)("CANTIDAD")
                    Next

                End If
            End If
        End If

        Return cantidad
    End Function

    Private Function ObtenerUltimaBodegaPedidoFabrica() As String
        Dim odataset As DataSet
        Dim response As Dipros.Utils.Events

        response = Me.oArticulos.ObtenerUltimaBodegaPedidoFabrica(Me.Articulo)

        If Not response.ErrorFound Then
            odataset = response.Value

            If odataset.Tables.Count > 0 Then
                If odataset.Tables(0).Rows.Count > 0 Then

                    Return odataset.Tables(0).Rows(0).Item("bodega")
                End If
            End If
            Return ""

        End If
    End Function

    Private Function ValidarCantidad(ByVal cantidad_bodega As Integer) As Boolean
        ValidarCantidad = True

        If cantidad_bodega < CInt(Me.clcCantidad.Value) Then
            ValidarCantidad = False
        End If

        Return ValidarCantidad
    End Function


#End Region

    Private Function NumeroArticulosAgrupados() As Boolean
        NumeroArticulosAgrupados = True



        Dim odataset As DataSet
        Dim odetalle As DataView = CType(Me.OwnerForm, frmVentas).grvVentas.DataSource
        Dim i As Long
        Dim y As Long


        Dim dt As New DataTable("Orders")
        dt.Columns.Add("Articulo", GetType(Long))
        dt.Columns.Add("Bodega", GetType(String))
        dt.Columns.Add("sobrepedido", GetType(Boolean))


        Dim orow As DataRow

        'clase.SelectGroupByInto("Resultado", odetalle.Table, "Articulo, Bodega, Sum(Cantidad) Cantidad", "", "Articulo, Bodega")

        For i = 0 To odetalle.Table.Rows.Count - 1
            orow = dt.NewRow()

            If i = 0 Then
                orow("Articulo") = odetalle.Table.Rows(i).Item("articulo")
                orow("Bodega") = odetalle.Table.Rows(i).Item("bodega")
                orow("sobrepedido") = odetalle.Table.Rows(i).Item("sobrepedido")
                dt.Rows.Add(orow)
            Else

                For y = 0 To dt.Rows.Count - 1
                    If (odetalle.Table.Rows(i).Item("articulo") <> dt.Rows(y).Item("Articulo")) Or (odetalle.Table.Rows(i).Item("Bodega") <> dt.Rows(y).Item("Bodega")) Or (odetalle.Table.Rows(i).Item("sobrepedido") <> dt.Rows(y).Item("sobrepedido")) Then
                        orow("Articulo") = odetalle.Table.Rows(i).Item("articulo")
                        orow("Bodega") = odetalle.Table.Rows(i).Item("bodega")
                        orow("sobrepedido") = odetalle.Table.Rows(i).Item("sobrepedido")
                        dt.Rows.Add(orow)
                        Exit For
                    End If
                Next
            End If



        Next

        If dt.Rows.Count >= 25 Then
            NumeroArticulosAgrupados = False
        End If



    End Function


    Private Sub btnFotografia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFotografia.Click
        CargarFotografiaArticulo(Me.Articulo)
    End Sub

    Private Sub CargarFotografiaArticulo(ByVal Articulo As Long)
        Dim oform As New frmArticulosFoto
        oform.ModificarImagen = False
        oform.Articulo = Articulo
        oform.Title = "Foto del Articulo"
        oform.ShowDialog(Me.MdiParent)
    End Sub
End Class

