Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class brwDepositosBancarios
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tmaDepositosBancarios As Dipros.Windows.TINMaster
    Friend WithEvents grDepositosBancarios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDepositosBancarios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConsecutivo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcChequera As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDepositante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAplicado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAplico As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcInserto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btnAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grcSucursalNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBancoNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaDeposito As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoDeposito As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(brwDepositosBancarios))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.tmaDepositosBancarios = New Dipros.Windows.TINMaster
        Me.grDepositosBancarios = New DevExpress.XtraGrid.GridControl
        Me.grvDepositosBancarios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursalNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConsecutivo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBanco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBancoNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcChequera = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDepositante = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAplicado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcAplico = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcInserto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.btnAplicar = New DevExpress.XtraEditors.SimpleButton
        Me.grcFechaDeposito = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoDeposito = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grDepositosBancarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDepositosBancarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5946, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(40, 42)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(8, 66)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(87, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Fecha Ingreso:"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(104, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(200, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2007, 7, 2, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(104, 64)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha.Size = New System.Drawing.Size(104, 20)
        Me.dteFecha.TabIndex = 3
        '
        'tmaDepositosBancarios
        '
        Me.tmaDepositosBancarios.BackColor = System.Drawing.Color.White
        Me.tmaDepositosBancarios.CanDelete = True
        Me.tmaDepositosBancarios.CanInsert = True
        Me.tmaDepositosBancarios.CanUpdate = True
        Me.tmaDepositosBancarios.Grid = Me.grDepositosBancarios
        Me.tmaDepositosBancarios.Location = New System.Drawing.Point(16, 96)
        Me.tmaDepositosBancarios.Name = "tmaDepositosBancarios"
        Me.tmaDepositosBancarios.Size = New System.Drawing.Size(792, 25)
        Me.tmaDepositosBancarios.TabIndex = 4
        Me.tmaDepositosBancarios.Title = "Dep�sitos Bancarios"
        Me.tmaDepositosBancarios.UpdateTitle = "un Registro"
        '
        'grDepositosBancarios
        '
        '
        'grDepositosBancarios.EmbeddedNavigator
        '
        Me.grDepositosBancarios.EmbeddedNavigator.Name = ""
        Me.grDepositosBancarios.Location = New System.Drawing.Point(8, 128)
        Me.grDepositosBancarios.MainView = Me.grvDepositosBancarios
        Me.grDepositosBancarios.Name = "grDepositosBancarios"
        Me.grDepositosBancarios.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1, Me.RepositoryItemTextEdit1})
        Me.grDepositosBancarios.Size = New System.Drawing.Size(920, 272)
        Me.grDepositosBancarios.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDepositosBancarios.TabIndex = 5
        Me.grDepositosBancarios.Text = "GridControl1"
        '
        'grvDepositosBancarios
        '
        Me.grvDepositosBancarios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSucursal, Me.grcSucursalNombre, Me.grcFecha, Me.grcConsecutivo, Me.grcBanco, Me.grcBancoNombre, Me.grcChequera, Me.grcFolio, Me.grcDepositante, Me.grcImporte, Me.grcAplicado, Me.grcAplico, Me.grcInserto, Me.grcFechaDeposito, Me.grcTipoDeposito})
        Me.grvDepositosBancarios.GridControl = Me.grDepositosBancarios
        Me.grvDepositosBancarios.Name = "grvDepositosBancarios"
        Me.grvDepositosBancarios.OptionsView.ColumnAutoWidth = False
        Me.grvDepositosBancarios.OptionsView.ShowFilterPanel = False
        Me.grvDepositosBancarios.OptionsView.ShowGroupPanel = False
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSucursalNombre
        '
        Me.grcSucursalNombre.Caption = "Sucursal"
        Me.grcSucursalNombre.FieldName = "sucursal_nombre"
        Me.grcSucursalNombre.Name = "grcSucursalNombre"
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha Ingreso"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 1
        Me.grcFecha.Width = 73
        '
        'grcConsecutivo
        '
        Me.grcConsecutivo.Caption = "Consecutivo"
        Me.grcConsecutivo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcConsecutivo.FieldName = "consecutivo"
        Me.grcConsecutivo.Name = "grcConsecutivo"
        Me.grcConsecutivo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConsecutivo.SortIndex = 0
        Me.grcConsecutivo.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcConsecutivo.VisibleIndex = 3
        Me.grcConsecutivo.Width = 94
        '
        'grcBanco
        '
        Me.grcBanco.Caption = "Banco"
        Me.grcBanco.FieldName = "banco"
        Me.grcBanco.Name = "grcBanco"
        Me.grcBanco.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBanco.Width = 113
        '
        'grcBancoNombre
        '
        Me.grcBancoNombre.Caption = "Banco"
        Me.grcBancoNombre.FieldName = "banco_nombre"
        Me.grcBancoNombre.Name = "grcBancoNombre"
        Me.grcBancoNombre.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBancoNombre.VisibleIndex = 4
        Me.grcBancoNombre.Width = 162
        '
        'grcChequera
        '
        Me.grcChequera.Caption = "Chequera"
        Me.grcChequera.FieldName = "chequera"
        Me.grcChequera.Name = "grcChequera"
        Me.grcChequera.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcChequera.VisibleIndex = 5
        Me.grcChequera.Width = 113
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio"
        Me.grcFolio.FieldName = "folio_movimiento_chequera"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 7
        Me.grcFolio.Width = 40
        '
        'grcDepositante
        '
        Me.grcDepositante.Caption = "Depositante"
        Me.grcDepositante.FieldName = "depositante"
        Me.grcDepositante.Name = "grcDepositante"
        Me.grcDepositante.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDepositante.VisibleIndex = 8
        Me.grcDepositante.Width = 125
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "C2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 9
        Me.grcImporte.Width = 85
        '
        'grcAplicado
        '
        Me.grcAplicado.Caption = "Aplicar"
        Me.grcAplicado.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcAplicado.FieldName = "aplicado_movimiento_chequera"
        Me.grcAplicado.Name = "grcAplicado"
        Me.grcAplicado.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcAplicado.VisibleIndex = 0
        Me.grcAplicado.Width = 58
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grcAplico
        '
        Me.grcAplico.Caption = "Aplic�"
        Me.grcAplico.FieldName = "usuario_aplico"
        Me.grcAplico.Name = "grcAplico"
        Me.grcAplico.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcInserto
        '
        Me.grcInserto.Caption = "Insert�"
        Me.grcInserto.FieldName = "usuario_inserto"
        Me.grcInserto.Name = "grcInserto"
        Me.grcInserto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'btnAplicar
        '
        Me.btnAplicar.Enabled = False
        Me.btnAplicar.Location = New System.Drawing.Point(856, 408)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.TabIndex = 65
        Me.btnAplicar.Text = "Aplicar"
        '
        'grcFechaDeposito
        '
        Me.grcFechaDeposito.Caption = "Fecha Deposito"
        Me.grcFechaDeposito.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaDeposito.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaDeposito.FieldName = "fecha_deposito"
        Me.grcFechaDeposito.Name = "grcFechaDeposito"
        Me.grcFechaDeposito.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaDeposito.VisibleIndex = 2
        '
        'grcTipoDeposito
        '
        Me.grcTipoDeposito.Caption = "Deposito"
        Me.grcTipoDeposito.FieldName = "Tipo_deposito"
        Me.grcTipoDeposito.Name = "grcTipoDeposito"
        Me.grcTipoDeposito.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipoDeposito.VisibleIndex = 6
        '
        'brwDepositosBancarios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(938, 432)
        Me.Controls.Add(Me.grDepositosBancarios)
        Me.Controls.Add(Me.btnAplicar)
        Me.Controls.Add(Me.tmaDepositosBancarios)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblSucursal)
        Me.Name = "brwDepositosBancarios"
        Me.Text = "brwDepositosBancarios"
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.tmaDepositosBancarios, 0)
        Me.Controls.SetChildIndex(Me.btnAplicar, 0)
        Me.Controls.SetChildIndex(Me.grDepositosBancarios, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grDepositosBancarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDepositosBancarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDepositosBancarios As VillarrealBusiness.clsDepositosBancarios
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private intAplicados As Long
    Private oMovimientosChequera As VillarrealBusiness.clsMovimientosChequera
    Private intConceptoMovimientoChequera As Long
    Private oVariables As VillarrealBusiness.clsVariables

    Public ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
#End Region


#Region "DIPROS Systems, Eventos de la forma"
    Private Sub brwDepositosBancarios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oDepositosBancarios = New VillarrealBusiness.clsDepositosBancarios
        oSucursales = New VillarrealBusiness.clsSucursales
        oMovimientosChequera = New VillarrealBusiness.clsMovimientosChequera
        oVariables = New VillarrealBusiness.clsVariables
        intConceptoMovimientoChequera = 0
        Me.tbrTools.Buttons(0).Visible = False

        lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual
        dteFecha.EditValue = Today()
        If Not lkpSucursal.EditValue = Nothing Then
            lkpSucursal.Enabled = False
            intAplicados = -1
            btnAplicar.Visible = False
            grvDepositosBancarios.Columns("aplicado_movimiento_chequera").Options = grvDepositosBancarios.Columns("aplicado_movimiento_chequera").Options And DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable
        Else
            lkpSucursal.Enabled = True
            intAplicados = 0
            'tmaDepositosBancarios.CanInsert = True
            'tmaDepositosBancarios.CanUpdate = False
            'tmaDepositosBancarios.CanDelete = False
            btnAplicar.Visible = True
            '@ACH-03/07/07: Obtener el concepto movimiento
            Dim oResponse As Events
            oResponse = oVariables.DespliegaDatos()
            If Not oResponse.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = oResponse.Value()
                intConceptoMovimientoChequera = oDataSet.Tables(0).Rows(0).Item("concepto_movimiento_chequera")
                If Not intConceptoMovimientoChequera > 0 Then
                    ShowMessage(MessageType.MsgError, "No se ha definido el concepto del Movimiento de Chequera para los Dep�sitos Bancarios. Por favor, defina este valor para poder aplicar los dep�sitos.")
                End If
            Else
                oResponse.ShowError()
            End If
            '/@ACH-03/07/07
        End If
        With tmaDepositosBancarios
            .AddColumn("sucursal")
            .AddColumn("sucursal_nombre")
            .AddColumn("fecha", "System.DateTime")
            .AddColumn("consecutivo", "System.Int16")
            .AddColumn("banco")
            .AddColumn("banco_nombre")
            .AddColumn("chequera")
            .AddColumn("folio_movimiento_chequera")
            .AddColumn("depositante")
            .AddColumn("importe", "System.Double")
            .AddColumn("aplicado_movimiento_chequera", "System.Boolean")
            .AddColumn("usuario_inserto")
            .AddColumn("usuario_aplico")
            .AddColumn("fecha_deposito", "System.DateTime")
            .AddColumn("Tipo_deposito")
            .UpdateForm = New frmDepositosBancarios
        End With
        CargaDatos_grDepositosBancarios()
    End Sub
    Private Sub brwDepositosBancarios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oDepositosBancarios.ValidaSucursal(Sucursal)
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de los controles"
#Region "Sucursal"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim oResponse As New Events

        oResponse = oSucursales.Listado()
        If oResponse.ErrorFound Then
            oResponse.ShowError()
        Else
            Dim oDataSet As DataSet
            oDataSet = oResponse.Value
            lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        CargaDatos_grDepositosBancarios()
    End Sub
#End Region

#Region "Fecha"
    Private Sub dteFecha_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha.EditValueChanged
        If (Not tmaDepositosBancarios Is Nothing) And (Not oDepositosBancarios Is Nothing) Then
            CargaDatos_grDepositosBancarios()
        End If
    End Sub
#End Region

#Region "Bot�n Aplicar"
    Private Sub btnAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAplicar.Click
        Dim oRow As DataRow

        For Each oRow In CType(Me.grvDepositosBancarios.DataSource, DataView).Table.Rows
            If oRow.Item("aplicado_movimiento_chequera") = True Then
                Dim oEvent As Events
                oEvent = oDepositosBancarios.Actualizar_Aplicado(oRow.Item("sucursal"), oRow.Item("fecha"), oRow.Item("consecutivo"), 0)
                If oEvent.ErrorFound Then
                    oEvent.ShowError()
                Else
                    Dim intFolio As Long
                    Dim dblSaldoActual As Long
                    oEvent = oMovimientosChequera.Insertar(oRow.Item("banco"), oRow.Item("chequera"), intFolio, oRow.Item("fecha"), "", oRow.Item("importe"), "A", False, "", 0, True, oRow.Item("usuario_inserto"), TinApp.Connection.User, "Dep�sito de Sucursal: " + oRow.Item("sucursal_nombre"), dblSaldoActual, False, 0, intConceptoMovimientoChequera, 0)
                    If oEvent.ErrorFound Then
                        oEvent.ShowError()
                    Else
                        oEvent = oDepositosBancarios.Actualizar_Aplicado(oRow.Item("sucursal"), oRow.Item("fecha"), oRow.Item("consecutivo"), intFolio)
                    End If
                End If
            End If
        Next
        Me.CargaDatos_grDepositosBancarios()
    End Sub
#End Region
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Public Sub CargaDatos_grDepositosBancarios()
        Dim oResponse As Events

        oResponse = oDepositosBancarios.Listado(Me.dteFecha.EditValue, intAplicados, Me.lkpSucursal.EditValue)
        If Not oResponse.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = oResponse.Value
            Me.tmaDepositosBancarios.DataSource = oDataSet
            oDataSet = Nothing
            'If intAplicados = -1 Then
            '    Me.grvDepositosBancarios.Columns("aplicado_movimiento_chequera").VisibleIndex = -1
            'End If
            If intAplicados = 0 Then
                If grvDepositosBancarios.RowCount > 0 And intConceptoMovimientoChequera > 0 Then
                    btnAplicar.Enabled = True
                Else
                    btnAplicar.Enabled = False
                End If

            End If
        End If
    End Sub
#End Region

    
End Class
