Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Imports System.Data
Imports System.Windows.Forms
Imports SendKey = System.Windows.Forms.SendKeys

Public Class frmVentas
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblMonto_Liquida_Vencimiento As System.Windows.Forms.Label
    Friend WithEvents tblReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents Panel_Generales As System.Windows.Forms.Panel
    Friend WithEvents panel_Creditos As System.Windows.Forms.Panel
    Friend WithEvents Panel_Repartos As System.Windows.Forms.Panel
    Friend WithEvents Panel_Cliente As System.Windows.Forms.Panel
    Friend WithEvents lkpFolioVistaSalida As Dipros.Editors.TINMultiLookup
    Public WithEvents Label10 As System.Windows.Forms.Label
    Public WithEvents lblSerieSucursal As System.Windows.Forms.Label
    Public WithEvents lblSerie As System.Windows.Forms.Label
    Public WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents lblMenosSoloDespliegue As System.Windows.Forms.Label
    Friend WithEvents clcMenosSoloDespliegue As Dipros.Editors.TINCalcEdit
    Public WithEvents Label18 As System.Windows.Forms.Label
    Public WithEvents lkpCotizacion As Dipros.Editors.TINMultiLookup
    Public WithEvents lblFolioPedido As System.Windows.Forms.Label
    Friend WithEvents clcFolioPedido As Dipros.Editors.TINCalcEdit
    Public WithEvents dteFechaPedido As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFechaPedido As System.Windows.Forms.Label
    Friend WithEvents btnNuevoClientePlantilla As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtObservacionesVenta As DevExpress.XtraEditors.MemoEdit
    Public WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblTotalContado As System.Windows.Forms.Label
    Public WithEvents clcTotalArticulos As Dipros.Editors.TINCalcEdit
    Public WithEvents clcTotalMinimo As Dipros.Editors.TINCalcEdit
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grVentas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvVentas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSobrePedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSobrePedido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcReparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkReparto As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSurtido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSurtido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcPrecioMinimo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcRegalo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcContado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioAutorizacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPartidaVistas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaVentas As Dipros.Windows.TINMaster
    Friend WithEvents lblSubtotal As System.Windows.Forms.Label
    Friend WithEvents clcSubtotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImpuesto As System.Windows.Forms.Label
    Friend WithEvents clcImpuesto As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Public WithEvents clcTotal As Dipros.Editors.TINCalcEdit
    Public WithEvents lblTipoventa As System.Windows.Forms.Label
    Friend WithEvents cboTipoventa As DevExpress.XtraEditors.ImageComboBoxEdit
    Public WithEvents chkIvadesglosado As DevExpress.XtraEditors.CheckEdit
    Public WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lkpVendedor As Dipros.Editors.TINMultiLookup
    Public WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Public WithEvents lbl_eFecha As System.Windows.Forms.Label
    Public WithEvents lbl_eFolio As System.Windows.Forms.Label
    Friend WithEvents TinMaster1 As Dipros.Windows.TINMaster
    Friend WithEvents lblTotalDocumentos As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblDocumentosUltimoDocumento As System.Windows.Forms.Label
    Friend WithEvents lblImporteUltimoDocumento As System.Windows.Forms.Label
    Friend WithEvents lblLeyendaDocumentoFinal As System.Windows.Forms.Label
    Friend WithEvents lblDocumentoFinal As System.Windows.Forms.Label
    Friend WithEvents lblLeyendaDocumentos As System.Windows.Forms.Label
    Friend WithEvents lblDocumentosIguales As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents clcImportePlan As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcEnganche2 As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkEnganche_en_Menos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents clcMenos_Enganch As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkLiquida_Vencimiento As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkIvadesglosado1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents clcTotalCredito As Dipros.Editors.TINCalcEdit
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents clcMonto_Liquida_Vencimiento As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkEnganche_En_Documento As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblFecha_Enganche_Documento As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Enganche_Documento As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblImporte_Enganche_Documento As System.Windows.Forms.Label
    Friend WithEvents clcImporte_Enganche_Documento As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblPlan As System.Windows.Forms.Label
    Friend WithEvents lkpPlan As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha_Primer_Documento As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Primer_Documento As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblNumero_Documentos As System.Windows.Forms.Label
    Friend WithEvents clcNumero_Documentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporte_Documentos As System.Windows.Forms.Label
    Friend WithEvents clcImporte_Documentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporte_Ultimo_Documento As System.Windows.Forms.Label
    Friend WithEvents clcImporte_Ultimo_Documento As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblMenos As System.Windows.Forms.Label
    Friend WithEvents clcMenos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblInteres As System.Windows.Forms.Label
    Friend WithEvents clcEnganche1 As Dipros.Editors.TINCalcEdit
    Public WithEvents txtObservacionesReparto As DevExpress.XtraEditors.MemoEdit
    Public WithEvents Label2 As System.Windows.Forms.Label
    Public WithEvents txtDomicilioReparto As DevExpress.XtraEditors.TextEdit
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents txtColoniaReparto As DevExpress.XtraEditors.TextEdit
    Public WithEvents Label4 As System.Windows.Forms.Label
    Public WithEvents txtCiudadReparto As DevExpress.XtraEditors.TextEdit
    Public WithEvents Label6 As System.Windows.Forms.Label
    Public WithEvents txtEstadoReparto As DevExpress.XtraEditors.TextEdit
    Public WithEvents Label7 As System.Windows.Forms.Label
    Public WithEvents txtTelefonoReparto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtMunicipio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNumInterior As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNumExterior As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCurp As System.Windows.Forms.Label
    Friend WithEvents txtCurp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblRfc As System.Windows.Forms.Label
    Friend WithEvents txtRfc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents txtColonia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents txtCiudad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCp As System.Windows.Forms.Label
    Friend WithEvents clcCp As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtEstado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono1 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono2 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Panel_Aduana As System.Windows.Forms.Panel
    Friend WithEvents Panel_Cancelacion As System.Windows.Forms.Panel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cboTipoCancelacion As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txtUsuarioCancelacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents dteFechaCancelacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtObservacionesCancelacion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblPedimento As System.Windows.Forms.Label
    Public WithEvents txtPedimento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblAduana As System.Windows.Forms.Label
    Public WithEvents txtAduana As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFecha_Importacion As System.Windows.Forms.Label
    Public WithEvents dteFecha_Importacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnGenerales As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCredito As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReparto As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAduana As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancelacion As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboHorario As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents dteFechaReparto As DevExpress.XtraEditors.DateEdit
    Public WithEvents lkpDescuentoCliente As Dipros.Editors.TINMultiLookup
    Public WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents clcSubtotalContado As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents gpbFactura As System.Windows.Forms.GroupBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Public WithEvents chkReFacturado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpFolio_fac_cancelada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursal_fac_Cancelada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSerie_fac_cancelada As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkaplica_enganche As DevExpress.XtraEditors.CheckEdit
    Public WithEvents lkpPaquetes As Dipros.Editors.TINMultiLookup
    Public WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents grcPrecioPactado As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents chkRfcCompleto As DevExpress.XtraEditors.CheckEdit
    Public WithEvents chkFacturacionEspecial As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grcDescripcionEspecial As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Panel_Anticipos As System.Windows.Forms.Panel
    Friend WithEvents btnAnticipos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grAnticipos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvAnticipos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents RepositoryItemCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents RepositoryItemCheckEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents tmaAnticipos As Dipros.Windows.TINMaster
    Friend WithEvents grcAnticipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConceptoAnticipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteAnticipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents repClcImporte As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.MemoEdit
    Public WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents clcTotalAnticipos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblAnticipos As System.Windows.Forms.Label
    Friend WithEvents lblEmailCliente As System.Windows.Forms.Label
    Friend WithEvents txtEmailCliente As DevExpress.XtraEditors.TextEdit
    Public WithEvents chkApartados As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVentas))
        Me.tblReimprimir = New System.Windows.Forms.ToolBarButton
        Me.Panel_Generales = New System.Windows.Forms.Panel
        Me.chkApartados = New DevExpress.XtraEditors.CheckEdit
        Me.lblAnticipos = New System.Windows.Forms.Label
        Me.clcTotalAnticipos = New Dipros.Editors.TINCalcEdit
        Me.chkFacturacionEspecial = New DevExpress.XtraEditors.CheckEdit
        Me.Label24 = New System.Windows.Forms.Label
        Me.lkpPaquetes = New Dipros.Editors.TINMultiLookup
        Me.chkReFacturado = New DevExpress.XtraEditors.CheckEdit
        Me.gpbFactura = New System.Windows.Forms.GroupBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpFolio_fac_cancelada = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursal_fac_Cancelada = New Dipros.Editors.TINMultiLookup
        Me.lkpSerie_fac_cancelada = New Dipros.Editors.TINMultiLookup
        Me.clcSubtotalContado = New Dipros.Editors.TINCalcEdit
        Me.Label22 = New System.Windows.Forms.Label
        Me.lkpDescuentoCliente = New Dipros.Editors.TINMultiLookup
        Me.lkpFolioVistaSalida = New Dipros.Editors.TINMultiLookup
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblSerieSucursal = New System.Windows.Forms.Label
        Me.lblSerie = New System.Windows.Forms.Label
        Me.lblFolio = New System.Windows.Forms.Label
        Me.lblMenosSoloDespliegue = New System.Windows.Forms.Label
        Me.clcMenosSoloDespliegue = New Dipros.Editors.TINCalcEdit
        Me.Label18 = New System.Windows.Forms.Label
        Me.lkpCotizacion = New Dipros.Editors.TINMultiLookup
        Me.lblFolioPedido = New System.Windows.Forms.Label
        Me.clcFolioPedido = New Dipros.Editors.TINCalcEdit
        Me.dteFechaPedido = New DevExpress.XtraEditors.DateEdit
        Me.lblFechaPedido = New System.Windows.Forms.Label
        Me.btnNuevoClientePlantilla = New DevExpress.XtraEditors.SimpleButton
        Me.txtObservacionesVenta = New DevExpress.XtraEditors.MemoEdit
        Me.Label12 = New System.Windows.Forms.Label
        Me.lblTotalContado = New System.Windows.Forms.Label
        Me.clcTotalArticulos = New Dipros.Editors.TINCalcEdit
        Me.clcTotalMinimo = New Dipros.Editors.TINCalcEdit
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.grVentas = New DevExpress.XtraGrid.GridControl
        Me.grvVentas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSobrePedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSobrePedido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcReparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkReparto = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSurtido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSurtido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPrecioMinimo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcRegalo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcContado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioAutorizacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPartidaVistas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioPactado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcionEspecial = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaVentas = New Dipros.Windows.TINMaster
        Me.lblSubtotal = New System.Windows.Forms.Label
        Me.lblImpuesto = New System.Windows.Forms.Label
        Me.clcImpuesto = New Dipros.Editors.TINCalcEdit
        Me.lblTotal = New System.Windows.Forms.Label
        Me.clcTotal = New Dipros.Editors.TINCalcEdit
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboTipoventa = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkIvadesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.lkpVendedor = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lbl_eFecha = New System.Windows.Forms.Label
        Me.lbl_eFolio = New System.Windows.Forms.Label
        Me.TinMaster1 = New Dipros.Windows.TINMaster
        Me.clcSubtotal = New Dipros.Editors.TINCalcEdit
        Me.panel_Creditos = New System.Windows.Forms.Panel
        Me.chkaplica_enganche = New DevExpress.XtraEditors.CheckEdit
        Me.lblTotalDocumentos = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.lblDocumentosUltimoDocumento = New System.Windows.Forms.Label
        Me.lblImporteUltimoDocumento = New System.Windows.Forms.Label
        Me.lblLeyendaDocumentoFinal = New System.Windows.Forms.Label
        Me.lblDocumentoFinal = New System.Windows.Forms.Label
        Me.lblLeyendaDocumentos = New System.Windows.Forms.Label
        Me.lblDocumentosIguales = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.clcImportePlan = New Dipros.Editors.TINCalcEdit
        Me.clcEnganche2 = New Dipros.Editors.TINCalcEdit
        Me.chkEnganche_en_Menos = New DevExpress.XtraEditors.CheckEdit
        Me.Label11 = New System.Windows.Forms.Label
        Me.clcMenos_Enganch = New Dipros.Editors.TINCalcEdit
        Me.chkLiquida_Vencimiento = New DevExpress.XtraEditors.CheckEdit
        Me.chkIvadesglosado1 = New DevExpress.XtraEditors.CheckEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.clcTotalCredito = New Dipros.Editors.TINCalcEdit
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.clcMonto_Liquida_Vencimiento = New Dipros.Editors.TINCalcEdit
        Me.chkEnganche_En_Documento = New DevExpress.XtraEditors.CheckEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblFecha_Enganche_Documento = New System.Windows.Forms.Label
        Me.dteFecha_Enganche_Documento = New DevExpress.XtraEditors.DateEdit
        Me.lblImporte_Enganche_Documento = New System.Windows.Forms.Label
        Me.clcImporte_Enganche_Documento = New Dipros.Editors.TINCalcEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblPlan = New System.Windows.Forms.Label
        Me.lkpPlan = New Dipros.Editors.TINMultiLookup
        Me.lblFecha_Primer_Documento = New System.Windows.Forms.Label
        Me.dteFecha_Primer_Documento = New DevExpress.XtraEditors.DateEdit
        Me.lblNumero_Documentos = New System.Windows.Forms.Label
        Me.clcNumero_Documentos = New Dipros.Editors.TINCalcEdit
        Me.lblImporte_Documentos = New System.Windows.Forms.Label
        Me.clcImporte_Documentos = New Dipros.Editors.TINCalcEdit
        Me.lblImporte_Ultimo_Documento = New System.Windows.Forms.Label
        Me.clcImporte_Ultimo_Documento = New Dipros.Editors.TINCalcEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblMenos = New System.Windows.Forms.Label
        Me.clcMenos = New Dipros.Editors.TINCalcEdit
        Me.lblInteres = New System.Windows.Forms.Label
        Me.clcEnganche1 = New Dipros.Editors.TINCalcEdit
        Me.Panel_Repartos = New System.Windows.Forms.Panel
        Me.Label21 = New System.Windows.Forms.Label
        Me.cboHorario = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFechaReparto = New DevExpress.XtraEditors.DateEdit
        Me.txtObservacionesReparto = New DevExpress.XtraEditors.MemoEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDomicilioReparto = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtColoniaReparto = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtCiudadReparto = New DevExpress.XtraEditors.TextEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtEstadoReparto = New DevExpress.XtraEditors.TextEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtTelefonoReparto = New DevExpress.XtraEditors.TextEdit
        Me.Panel_Cliente = New System.Windows.Forms.Panel
        Me.lblEmailCliente = New System.Windows.Forms.Label
        Me.txtEmailCliente = New DevExpress.XtraEditors.TextEdit
        Me.chkRfcCompleto = New DevExpress.XtraEditors.CheckEdit
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtMunicipio = New DevExpress.XtraEditors.TextEdit
        Me.txtNumInterior = New DevExpress.XtraEditors.TextEdit
        Me.txtNumExterior = New DevExpress.XtraEditors.TextEdit
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtFax = New DevExpress.XtraEditors.TextEdit
        Me.lblCurp = New System.Windows.Forms.Label
        Me.txtCurp = New DevExpress.XtraEditors.TextEdit
        Me.lblRfc = New System.Windows.Forms.Label
        Me.txtRfc = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio = New System.Windows.Forms.Label
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia = New System.Windows.Forms.Label
        Me.txtColonia = New DevExpress.XtraEditors.TextEdit
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.txtCiudad = New DevExpress.XtraEditors.TextEdit
        Me.lblCp = New System.Windows.Forms.Label
        Me.clcCp = New Dipros.Editors.TINCalcEdit
        Me.lblEstado = New System.Windows.Forms.Label
        Me.txtEstado = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono1 = New System.Windows.Forms.Label
        Me.txtTelefono1 = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono2 = New System.Windows.Forms.Label
        Me.txtTelefono2 = New DevExpress.XtraEditors.TextEdit
        Me.Panel_Aduana = New System.Windows.Forms.Panel
        Me.lblPedimento = New System.Windows.Forms.Label
        Me.txtPedimento = New DevExpress.XtraEditors.TextEdit
        Me.lblAduana = New System.Windows.Forms.Label
        Me.txtAduana = New DevExpress.XtraEditors.TextEdit
        Me.lblFecha_Importacion = New System.Windows.Forms.Label
        Me.dteFecha_Importacion = New DevExpress.XtraEditors.DateEdit
        Me.Panel_Cancelacion = New System.Windows.Forms.Panel
        Me.Label16 = New System.Windows.Forms.Label
        Me.cboTipoCancelacion = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.txtUsuarioCancelacion = New DevExpress.XtraEditors.TextEdit
        Me.Label15 = New System.Windows.Forms.Label
        Me.dteFechaCancelacion = New DevExpress.XtraEditors.DateEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtObservacionesCancelacion = New DevExpress.XtraEditors.MemoEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.btnGenerales = New DevExpress.XtraEditors.SimpleButton
        Me.btnCredito = New DevExpress.XtraEditors.SimpleButton
        Me.btnReparto = New DevExpress.XtraEditors.SimpleButton
        Me.btnCliente = New DevExpress.XtraEditors.SimpleButton
        Me.btnAduana = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancelacion = New DevExpress.XtraEditors.SimpleButton
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnAnticipos = New DevExpress.XtraEditors.SimpleButton
        Me.Panel_Anticipos = New System.Windows.Forms.Panel
        Me.txtConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.grAnticipos = New DevExpress.XtraGrid.GridControl
        Me.grvAnticipos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcAnticipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConceptoAnticipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteAnticipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.repClcImporte = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.RepositoryItemCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.RepositoryItemCheckEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.tmaAnticipos = New Dipros.Windows.TINMaster
        Me.Panel_Generales.SuspendLayout()
        CType(Me.chkApartados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotalAnticipos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFacturacionEspecial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkReFacturado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFactura.SuspendLayout()
        CType(Me.clcSubtotalContado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMenosSoloDespliegue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioPedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaPedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservacionesVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotalArticulos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotalMinimo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImpuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIvadesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSubtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panel_Creditos.SuspendLayout()
        CType(Me.chkaplica_enganche.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImportePlan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcEnganche2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEnganche_en_Menos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMenos_Enganch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLiquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIvadesglosado1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotalCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.clcMonto_Liquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEnganche_En_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dteFecha_Enganche_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte_Enganche_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Primer_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcNumero_Documentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte_Documentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte_Ultimo_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMenos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcEnganche1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel_Repartos.SuspendLayout()
        CType(Me.cboHorario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservacionesReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilioReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColoniaReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudadReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstadoReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefonoReparto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel_Cliente.SuspendLayout()
        CType(Me.txtEmailCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRfcCompleto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumInterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumExterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRfc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel_Aduana.SuspendLayout()
        CType(Me.txtPedimento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAduana.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Importacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel_Cancelacion.SuspendLayout()
        CType(Me.cboTipoCancelacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsuarioCancelacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaCancelacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservacionesCancelacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.Panel_Anticipos.SuspendLayout()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grAnticipos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvAnticipos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repClcImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tblReimprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(41131, 28)
        '
        'tblReimprimir
        '
        Me.tblReimprimir.Text = "Reimprimir"
        '
        'Panel_Generales
        '
        Me.Panel_Generales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_Generales.Controls.Add(Me.chkApartados)
        Me.Panel_Generales.Controls.Add(Me.lblAnticipos)
        Me.Panel_Generales.Controls.Add(Me.clcTotalAnticipos)
        Me.Panel_Generales.Controls.Add(Me.chkFacturacionEspecial)
        Me.Panel_Generales.Controls.Add(Me.Label24)
        Me.Panel_Generales.Controls.Add(Me.lkpPaquetes)
        Me.Panel_Generales.Controls.Add(Me.chkReFacturado)
        Me.Panel_Generales.Controls.Add(Me.gpbFactura)
        Me.Panel_Generales.Controls.Add(Me.clcSubtotalContado)
        Me.Panel_Generales.Controls.Add(Me.Label22)
        Me.Panel_Generales.Controls.Add(Me.lkpDescuentoCliente)
        Me.Panel_Generales.Controls.Add(Me.lkpFolioVistaSalida)
        Me.Panel_Generales.Controls.Add(Me.Label10)
        Me.Panel_Generales.Controls.Add(Me.lblSerieSucursal)
        Me.Panel_Generales.Controls.Add(Me.lblSerie)
        Me.Panel_Generales.Controls.Add(Me.lblFolio)
        Me.Panel_Generales.Controls.Add(Me.lblMenosSoloDespliegue)
        Me.Panel_Generales.Controls.Add(Me.clcMenosSoloDespliegue)
        Me.Panel_Generales.Controls.Add(Me.Label18)
        Me.Panel_Generales.Controls.Add(Me.lkpCotizacion)
        Me.Panel_Generales.Controls.Add(Me.lblFolioPedido)
        Me.Panel_Generales.Controls.Add(Me.clcFolioPedido)
        Me.Panel_Generales.Controls.Add(Me.dteFechaPedido)
        Me.Panel_Generales.Controls.Add(Me.lblFechaPedido)
        Me.Panel_Generales.Controls.Add(Me.btnNuevoClientePlantilla)
        Me.Panel_Generales.Controls.Add(Me.txtObservacionesVenta)
        Me.Panel_Generales.Controls.Add(Me.Label12)
        Me.Panel_Generales.Controls.Add(Me.lblTotalContado)
        Me.Panel_Generales.Controls.Add(Me.clcTotalArticulos)
        Me.Panel_Generales.Controls.Add(Me.clcTotalMinimo)
        Me.Panel_Generales.Controls.Add(Me.dteFecha)
        Me.Panel_Generales.Controls.Add(Me.grVentas)
        Me.Panel_Generales.Controls.Add(Me.tmaVentas)
        Me.Panel_Generales.Controls.Add(Me.lblSubtotal)
        Me.Panel_Generales.Controls.Add(Me.lblImpuesto)
        Me.Panel_Generales.Controls.Add(Me.clcImpuesto)
        Me.Panel_Generales.Controls.Add(Me.lblTotal)
        Me.Panel_Generales.Controls.Add(Me.clcTotal)
        Me.Panel_Generales.Controls.Add(Me.lblTipoventa)
        Me.Panel_Generales.Controls.Add(Me.cboTipoventa)
        Me.Panel_Generales.Controls.Add(Me.chkIvadesglosado)
        Me.Panel_Generales.Controls.Add(Me.lblVendedor)
        Me.Panel_Generales.Controls.Add(Me.lkpVendedor)
        Me.Panel_Generales.Controls.Add(Me.lblCliente)
        Me.Panel_Generales.Controls.Add(Me.lkpCliente)
        Me.Panel_Generales.Controls.Add(Me.lbl_eFecha)
        Me.Panel_Generales.Controls.Add(Me.lbl_eFolio)
        Me.Panel_Generales.Controls.Add(Me.TinMaster1)
        Me.Panel_Generales.Controls.Add(Me.clcSubtotal)
        Me.Panel_Generales.Location = New System.Drawing.Point(8, 64)
        Me.Panel_Generales.Name = "Panel_Generales"
        Me.Panel_Generales.Size = New System.Drawing.Size(731, 360)
        Me.Panel_Generales.TabIndex = 1
        '
        'chkApartados
        '
        Me.chkApartados.EditValue = "False"
        Me.chkApartados.Location = New System.Drawing.Point(512, 104)
        Me.chkApartados.Name = "chkApartados"
        '
        'chkApartados.Properties
        '
        Me.chkApartados.Properties.Caption = "Apartados"
        Me.chkApartados.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkApartados.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkApartados.Size = New System.Drawing.Size(80, 19)
        Me.chkApartados.TabIndex = 84
        Me.chkApartados.Tag = "apartados"
        '
        'lblAnticipos
        '
        Me.lblAnticipos.AutoSize = True
        Me.lblAnticipos.Location = New System.Drawing.Point(408, 296)
        Me.lblAnticipos.Name = "lblAnticipos"
        Me.lblAnticipos.Size = New System.Drawing.Size(59, 16)
        Me.lblAnticipos.TabIndex = 83
        Me.lblAnticipos.Tag = ""
        Me.lblAnticipos.Text = "Anticipos:"
        Me.lblAnticipos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotalAnticipos
        '
        Me.clcTotalAnticipos.EditValue = "0"
        Me.clcTotalAnticipos.Location = New System.Drawing.Point(472, 296)
        Me.clcTotalAnticipos.MaxValue = 0
        Me.clcTotalAnticipos.MinValue = 0
        Me.clcTotalAnticipos.Name = "clcTotalAnticipos"
        '
        'clcTotalAnticipos.Properties
        '
        Me.clcTotalAnticipos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalAnticipos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalAnticipos.Properties.Enabled = False
        Me.clcTotalAnticipos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotalAnticipos.Properties.Precision = 2
        Me.clcTotalAnticipos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotalAnticipos.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcTotalAnticipos.Size = New System.Drawing.Size(80, 19)
        Me.clcTotalAnticipos.TabIndex = 82
        Me.clcTotalAnticipos.TabStop = False
        Me.clcTotalAnticipos.Tag = ""
        '
        'chkFacturacionEspecial
        '
        Me.chkFacturacionEspecial.EditValue = "False"
        Me.chkFacturacionEspecial.Location = New System.Drawing.Point(368, 104)
        Me.chkFacturacionEspecial.Name = "chkFacturacionEspecial"
        '
        'chkFacturacionEspecial.Properties
        '
        Me.chkFacturacionEspecial.Properties.Caption = "Facturaci�n Especial"
        Me.chkFacturacionEspecial.Properties.Enabled = False
        Me.chkFacturacionEspecial.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkFacturacionEspecial.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkFacturacionEspecial.Size = New System.Drawing.Size(136, 19)
        Me.chkFacturacionEspecial.TabIndex = 81
        Me.chkFacturacionEspecial.Tag = "facturacion_especial"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(567, 82)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(53, 16)
        Me.Label24.TabIndex = 80
        Me.Label24.Tag = ""
        Me.Label24.Text = "Paquete:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPaquetes
        '
        Me.lkpPaquetes.AllowAdd = False
        Me.lkpPaquetes.AutoReaload = True
        Me.lkpPaquetes.DataSource = Nothing
        Me.lkpPaquetes.DefaultSearchField = ""
        Me.lkpPaquetes.DisplayMember = "paquete"
        Me.lkpPaquetes.EditValue = Nothing
        Me.lkpPaquetes.Filtered = False
        Me.lkpPaquetes.InitValue = Nothing
        Me.lkpPaquetes.Location = New System.Drawing.Point(624, 80)
        Me.lkpPaquetes.MultiSelect = False
        Me.lkpPaquetes.Name = "lkpPaquetes"
        Me.lkpPaquetes.NullText = ""
        Me.lkpPaquetes.PopupWidth = CType(420, Long)
        Me.lkpPaquetes.ReadOnlyControl = False
        Me.lkpPaquetes.Required = False
        Me.lkpPaquetes.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPaquetes.SearchMember = ""
        Me.lkpPaquetes.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPaquetes.SelectAll = False
        Me.lkpPaquetes.Size = New System.Drawing.Size(95, 20)
        Me.lkpPaquetes.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPaquetes.TabIndex = 79
        Me.lkpPaquetes.Tag = ""
        Me.lkpPaquetes.ToolTip = Nothing
        Me.lkpPaquetes.ValueMember = "paquete"
        '
        'chkReFacturado
        '
        Me.chkReFacturado.EditValue = "False"
        Me.chkReFacturado.Location = New System.Drawing.Point(16, 296)
        Me.chkReFacturado.Name = "chkReFacturado"
        '
        'chkReFacturado.Properties
        '
        Me.chkReFacturado.Properties.Caption = "ReFacturado"
        Me.chkReFacturado.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkReFacturado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkReFacturado.Size = New System.Drawing.Size(88, 19)
        Me.chkReFacturado.TabIndex = 78
        Me.chkReFacturado.Tag = ""
        '
        'gpbFactura
        '
        Me.gpbFactura.Controls.Add(Me.Label23)
        Me.gpbFactura.Controls.Add(Me.lblSucursal)
        Me.gpbFactura.Controls.Add(Me.lblOrden)
        Me.gpbFactura.Controls.Add(Me.lkpFolio_fac_cancelada)
        Me.gpbFactura.Controls.Add(Me.lkpSucursal_fac_Cancelada)
        Me.gpbFactura.Controls.Add(Me.lkpSerie_fac_cancelada)
        Me.gpbFactura.Enabled = False
        Me.gpbFactura.Location = New System.Drawing.Point(8, 299)
        Me.gpbFactura.Name = "gpbFactura"
        Me.gpbFactura.Size = New System.Drawing.Size(392, 53)
        Me.gpbFactura.TabIndex = 77
        Me.gpbFactura.TabStop = False
        Me.gpbFactura.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(392, 24)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(36, 17)
        Me.Label23.TabIndex = 4
        Me.Label23.Tag = ""
        Me.Label23.Text = "&Folio:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 24)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(256, 24)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 2
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFolio_fac_cancelada
        '
        Me.lkpFolio_fac_cancelada.AllowAdd = False
        Me.lkpFolio_fac_cancelada.AutoReaload = False
        Me.lkpFolio_fac_cancelada.DataSource = Nothing
        Me.lkpFolio_fac_cancelada.DefaultSearchField = ""
        Me.lkpFolio_fac_cancelada.DisplayMember = "folio"
        Me.lkpFolio_fac_cancelada.EditValue = Nothing
        Me.lkpFolio_fac_cancelada.Filtered = False
        Me.lkpFolio_fac_cancelada.InitValue = Nothing
        Me.lkpFolio_fac_cancelada.Location = New System.Drawing.Point(432, 24)
        Me.lkpFolio_fac_cancelada.MultiSelect = False
        Me.lkpFolio_fac_cancelada.Name = "lkpFolio_fac_cancelada"
        Me.lkpFolio_fac_cancelada.NullText = ""
        Me.lkpFolio_fac_cancelada.PopupWidth = CType(350, Long)
        Me.lkpFolio_fac_cancelada.ReadOnlyControl = False
        Me.lkpFolio_fac_cancelada.Required = False
        Me.lkpFolio_fac_cancelada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolio_fac_cancelada.SearchMember = ""
        Me.lkpFolio_fac_cancelada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolio_fac_cancelada.SelectAll = False
        Me.lkpFolio_fac_cancelada.Size = New System.Drawing.Size(112, 20)
        Me.lkpFolio_fac_cancelada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolio_fac_cancelada.TabIndex = 5
        Me.lkpFolio_fac_cancelada.Tag = "folio_factura_cancelada"
        Me.lkpFolio_fac_cancelada.ToolTip = Nothing
        Me.lkpFolio_fac_cancelada.ValueMember = "folio"
        '
        'lkpSucursal_fac_Cancelada
        '
        Me.lkpSucursal_fac_Cancelada.AllowAdd = False
        Me.lkpSucursal_fac_Cancelada.AutoReaload = False
        Me.lkpSucursal_fac_Cancelada.DataSource = Nothing
        Me.lkpSucursal_fac_Cancelada.DefaultSearchField = ""
        Me.lkpSucursal_fac_Cancelada.DisplayMember = "nombre"
        Me.lkpSucursal_fac_Cancelada.EditValue = Nothing
        Me.lkpSucursal_fac_Cancelada.Filtered = False
        Me.lkpSucursal_fac_Cancelada.InitValue = Nothing
        Me.lkpSucursal_fac_Cancelada.Location = New System.Drawing.Point(80, 24)
        Me.lkpSucursal_fac_Cancelada.MultiSelect = False
        Me.lkpSucursal_fac_Cancelada.Name = "lkpSucursal_fac_Cancelada"
        Me.lkpSucursal_fac_Cancelada.NullText = ""
        Me.lkpSucursal_fac_Cancelada.PopupWidth = CType(400, Long)
        Me.lkpSucursal_fac_Cancelada.ReadOnlyControl = False
        Me.lkpSucursal_fac_Cancelada.Required = False
        Me.lkpSucursal_fac_Cancelada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal_fac_Cancelada.SearchMember = ""
        Me.lkpSucursal_fac_Cancelada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal_fac_Cancelada.SelectAll = False
        Me.lkpSucursal_fac_Cancelada.Size = New System.Drawing.Size(168, 20)
        Me.lkpSucursal_fac_Cancelada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal_fac_Cancelada.TabIndex = 1
        Me.lkpSucursal_fac_Cancelada.Tag = "sucursal_factura_cancelada"
        Me.lkpSucursal_fac_Cancelada.ToolTip = Nothing
        Me.lkpSucursal_fac_Cancelada.ValueMember = "Sucursal"
        '
        'lkpSerie_fac_cancelada
        '
        Me.lkpSerie_fac_cancelada.AllowAdd = False
        Me.lkpSerie_fac_cancelada.AutoReaload = False
        Me.lkpSerie_fac_cancelada.DataSource = Nothing
        Me.lkpSerie_fac_cancelada.DefaultSearchField = ""
        Me.lkpSerie_fac_cancelada.DisplayMember = "serie"
        Me.lkpSerie_fac_cancelada.EditValue = Nothing
        Me.lkpSerie_fac_cancelada.Filtered = False
        Me.lkpSerie_fac_cancelada.InitValue = Nothing
        Me.lkpSerie_fac_cancelada.Location = New System.Drawing.Point(296, 24)
        Me.lkpSerie_fac_cancelada.MultiSelect = False
        Me.lkpSerie_fac_cancelada.Name = "lkpSerie_fac_cancelada"
        Me.lkpSerie_fac_cancelada.NullText = ""
        Me.lkpSerie_fac_cancelada.PopupWidth = CType(350, Long)
        Me.lkpSerie_fac_cancelada.ReadOnlyControl = False
        Me.lkpSerie_fac_cancelada.Required = False
        Me.lkpSerie_fac_cancelada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSerie_fac_cancelada.SearchMember = ""
        Me.lkpSerie_fac_cancelada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSerie_fac_cancelada.SelectAll = False
        Me.lkpSerie_fac_cancelada.Size = New System.Drawing.Size(88, 20)
        Me.lkpSerie_fac_cancelada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSerie_fac_cancelada.TabIndex = 3
        Me.lkpSerie_fac_cancelada.Tag = "serie_factura_cancelada"
        Me.lkpSerie_fac_cancelada.ToolTip = Nothing
        Me.lkpSerie_fac_cancelada.ValueMember = "serie"
        '
        'clcSubtotalContado
        '
        Me.clcSubtotalContado.EditValue = "0"
        Me.clcSubtotalContado.Location = New System.Drawing.Point(640, 296)
        Me.clcSubtotalContado.MaxValue = 0
        Me.clcSubtotalContado.MinValue = 0
        Me.clcSubtotalContado.Name = "clcSubtotalContado"
        '
        'clcSubtotalContado.Properties
        '
        Me.clcSubtotalContado.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSubtotalContado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotalContado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotalContado.Properties.Enabled = False
        Me.clcSubtotalContado.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSubtotalContado.Properties.Precision = 2
        Me.clcSubtotalContado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSubtotalContado.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcSubtotalContado.Size = New System.Drawing.Size(80, 19)
        Me.clcSubtotalContado.TabIndex = 76
        Me.clcSubtotalContado.TabStop = False
        Me.clcSubtotalContado.Tag = ""
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(553, 56)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(67, 16)
        Me.Label22.TabIndex = 75
        Me.Label22.Tag = ""
        Me.Label22.Text = "Promocion:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDescuentoCliente
        '
        Me.lkpDescuentoCliente.AllowAdd = False
        Me.lkpDescuentoCliente.AutoReaload = True
        Me.lkpDescuentoCliente.DataSource = Nothing
        Me.lkpDescuentoCliente.DefaultSearchField = ""
        Me.lkpDescuentoCliente.DisplayMember = "folio_descuento"
        Me.lkpDescuentoCliente.EditValue = Nothing
        Me.lkpDescuentoCliente.Filtered = False
        Me.lkpDescuentoCliente.InitValue = Nothing
        Me.lkpDescuentoCliente.Location = New System.Drawing.Point(624, 56)
        Me.lkpDescuentoCliente.MultiSelect = False
        Me.lkpDescuentoCliente.Name = "lkpDescuentoCliente"
        Me.lkpDescuentoCliente.NullText = ""
        Me.lkpDescuentoCliente.PopupWidth = CType(420, Long)
        Me.lkpDescuentoCliente.ReadOnlyControl = False
        Me.lkpDescuentoCliente.Required = False
        Me.lkpDescuentoCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDescuentoCliente.SearchMember = ""
        Me.lkpDescuentoCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDescuentoCliente.SelectAll = False
        Me.lkpDescuentoCliente.Size = New System.Drawing.Size(95, 20)
        Me.lkpDescuentoCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDescuentoCliente.TabIndex = 74
        Me.lkpDescuentoCliente.Tag = ""
        Me.lkpDescuentoCliente.ToolTip = Nothing
        Me.lkpDescuentoCliente.ValueMember = "folio_descuento"
        '
        'lkpFolioVistaSalida
        '
        Me.lkpFolioVistaSalida.AllowAdd = False
        Me.lkpFolioVistaSalida.AutoReaload = False
        Me.lkpFolioVistaSalida.DataSource = Nothing
        Me.lkpFolioVistaSalida.DefaultSearchField = ""
        Me.lkpFolioVistaSalida.DisplayMember = "folio_vista_salida"
        Me.lkpFolioVistaSalida.EditValue = Nothing
        Me.lkpFolioVistaSalida.Filtered = False
        Me.lkpFolioVistaSalida.InitValue = Nothing
        Me.lkpFolioVistaSalida.Location = New System.Drawing.Point(624, 32)
        Me.lkpFolioVistaSalida.MultiSelect = False
        Me.lkpFolioVistaSalida.Name = "lkpFolioVistaSalida"
        Me.lkpFolioVistaSalida.NullText = ""
        Me.lkpFolioVistaSalida.PopupWidth = CType(400, Long)
        Me.lkpFolioVistaSalida.ReadOnlyControl = False
        Me.lkpFolioVistaSalida.Required = False
        Me.lkpFolioVistaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolioVistaSalida.SearchMember = ""
        Me.lkpFolioVistaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolioVistaSalida.SelectAll = False
        Me.lkpFolioVistaSalida.Size = New System.Drawing.Size(96, 20)
        Me.lkpFolioVistaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolioVistaSalida.TabIndex = 57
        Me.lkpFolioVistaSalida.Tag = "folio_vista_salida"
        Me.lkpFolioVistaSalida.ToolTip = Nothing
        Me.lkpFolioVistaSalida.ValueMember = "folio_vista_salida"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(584, 32)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(36, 16)
        Me.Label10.TabIndex = 56
        Me.Label10.Tag = ""
        Me.Label10.Text = "Vista:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSerieSucursal
        '
        Me.lblSerieSucursal.AutoSize = True
        Me.lblSerieSucursal.Location = New System.Drawing.Point(264, 8)
        Me.lblSerieSucursal.Name = "lblSerieSucursal"
        Me.lblSerieSucursal.Size = New System.Drawing.Size(31, 16)
        Me.lblSerieSucursal.TabIndex = 73
        Me.lblSerieSucursal.Tag = ""
        Me.lblSerieSucursal.Text = "serie"
        Me.lblSerieSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(216, 8)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie.TabIndex = 72
        Me.lblSerie.Tag = ""
        Me.lblSerie.Text = "Serie:"
        Me.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(112, 8)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(11, 16)
        Me.lblFolio.TabIndex = 71
        Me.lblFolio.Tag = "folio"
        Me.lblFolio.Text = "0"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMenosSoloDespliegue
        '
        Me.lblMenosSoloDespliegue.Location = New System.Drawing.Point(560, 320)
        Me.lblMenosSoloDespliegue.Name = "lblMenosSoloDespliegue"
        Me.lblMenosSoloDespliegue.Size = New System.Drawing.Size(56, 16)
        Me.lblMenosSoloDespliegue.TabIndex = 68
        Me.lblMenosSoloDespliegue.Tag = ""
        Me.lblMenosSoloDespliegue.Text = "Menos:"
        Me.lblMenosSoloDespliegue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMenosSoloDespliegue
        '
        Me.clcMenosSoloDespliegue.EditValue = "0"
        Me.clcMenosSoloDespliegue.Location = New System.Drawing.Point(640, 315)
        Me.clcMenosSoloDespliegue.MaxValue = 0
        Me.clcMenosSoloDespliegue.MinValue = 0
        Me.clcMenosSoloDespliegue.Name = "clcMenosSoloDespliegue"
        '
        'clcMenosSoloDespliegue.Properties
        '
        Me.clcMenosSoloDespliegue.Properties.DisplayFormat.FormatString = "c2"
        Me.clcMenosSoloDespliegue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenosSoloDespliegue.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenosSoloDespliegue.Properties.Enabled = False
        Me.clcMenosSoloDespliegue.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMenosSoloDespliegue.Properties.Precision = 2
        Me.clcMenosSoloDespliegue.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMenosSoloDespliegue.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcMenosSoloDespliegue.Size = New System.Drawing.Size(80, 19)
        Me.clcMenosSoloDespliegue.TabIndex = 69
        Me.clcMenosSoloDespliegue.TabStop = False
        Me.clcMenosSoloDespliegue.Tag = ""
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(552, 8)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(65, 16)
        Me.Label18.TabIndex = 54
        Me.Label18.Tag = ""
        Me.Label18.Text = "Coti&zaci�n:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCotizacion
        '
        Me.lkpCotizacion.AllowAdd = False
        Me.lkpCotizacion.AutoReaload = False
        Me.lkpCotizacion.DataSource = Nothing
        Me.lkpCotizacion.DefaultSearchField = ""
        Me.lkpCotizacion.DisplayMember = "cotizacion"
        Me.lkpCotizacion.EditValue = Nothing
        Me.lkpCotizacion.Filtered = False
        Me.lkpCotizacion.InitValue = Nothing
        Me.lkpCotizacion.Location = New System.Drawing.Point(624, 8)
        Me.lkpCotizacion.MultiSelect = False
        Me.lkpCotizacion.Name = "lkpCotizacion"
        Me.lkpCotizacion.NullText = ""
        Me.lkpCotizacion.PopupWidth = CType(420, Long)
        Me.lkpCotizacion.ReadOnlyControl = False
        Me.lkpCotizacion.Required = False
        Me.lkpCotizacion.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCotizacion.SearchMember = ""
        Me.lkpCotizacion.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCotizacion.SelectAll = False
        Me.lkpCotizacion.Size = New System.Drawing.Size(95, 20)
        Me.lkpCotizacion.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCotizacion.TabIndex = 55
        Me.lkpCotizacion.Tag = "cotizacion"
        Me.lkpCotizacion.ToolTip = Nothing
        Me.lkpCotizacion.ValueMember = "cotizacion"
        '
        'lblFolioPedido
        '
        Me.lblFolioPedido.AutoSize = True
        Me.lblFolioPedido.Location = New System.Drawing.Point(528, 112)
        Me.lblFolioPedido.Name = "lblFolioPedido"
        Me.lblFolioPedido.Size = New System.Drawing.Size(93, 16)
        Me.lblFolioPedido.TabIndex = 50
        Me.lblFolioPedido.Tag = ""
        Me.lblFolioPedido.Text = "&Folio de pedido:"
        Me.lblFolioPedido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolioPedido
        '
        Me.clcFolioPedido.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolioPedido.Location = New System.Drawing.Point(624, 112)
        Me.clcFolioPedido.MaxValue = 0
        Me.clcFolioPedido.MinValue = 0
        Me.clcFolioPedido.Name = "clcFolioPedido"
        '
        'clcFolioPedido.Properties
        '
        Me.clcFolioPedido.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioPedido.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioPedido.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolioPedido.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolioPedido.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcFolioPedido.Size = New System.Drawing.Size(64, 19)
        Me.clcFolioPedido.TabIndex = 51
        Me.clcFolioPedido.Tag = "folio_pedido"
        '
        'dteFechaPedido
        '
        Me.dteFechaPedido.EditValue = "10/04/2006"
        Me.dteFechaPedido.Location = New System.Drawing.Point(624, 136)
        Me.dteFechaPedido.Name = "dteFechaPedido"
        '
        'dteFechaPedido.Properties
        '
        Me.dteFechaPedido.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaPedido.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaPedido.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaPedido.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFechaPedido.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaPedido.TabIndex = 53
        Me.dteFechaPedido.Tag = "fecha_pedido"
        '
        'lblFechaPedido
        '
        Me.lblFechaPedido.AutoSize = True
        Me.lblFechaPedido.Location = New System.Drawing.Point(520, 136)
        Me.lblFechaPedido.Name = "lblFechaPedido"
        Me.lblFechaPedido.Size = New System.Drawing.Size(99, 16)
        Me.lblFechaPedido.TabIndex = 52
        Me.lblFechaPedido.Tag = ""
        Me.lblFechaPedido.Text = "Fec&ha de pedido:"
        Me.lblFechaPedido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnNuevoClientePlantilla
        '
        Me.btnNuevoClientePlantilla.Location = New System.Drawing.Point(472, 80)
        Me.btnNuevoClientePlantilla.Name = "btnNuevoClientePlantilla"
        Me.btnNuevoClientePlantilla.Size = New System.Drawing.Size(56, 24)
        Me.btnNuevoClientePlantilla.TabIndex = 44
        Me.btnNuevoClientePlantilla.Text = "Agregar"
        '
        'txtObservacionesVenta
        '
        Me.txtObservacionesVenta.EditValue = ""
        Me.txtObservacionesVenta.Location = New System.Drawing.Point(104, 128)
        Me.txtObservacionesVenta.Name = "txtObservacionesVenta"
        Me.txtObservacionesVenta.Size = New System.Drawing.Size(357, 32)
        Me.txtObservacionesVenta.TabIndex = 49
        Me.txtObservacionesVenta.Tag = "observaciones_venta"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 128)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(89, 16)
        Me.Label12.TabIndex = 48
        Me.Label12.Tag = ""
        Me.Label12.Text = "&Observaciones:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalContado
        '
        Me.lblTotalContado.AutoSize = True
        Me.lblTotalContado.Location = New System.Drawing.Point(584, 336)
        Me.lblTotalContado.Name = "lblTotalContado"
        Me.lblTotalContado.Size = New System.Drawing.Size(36, 16)
        Me.lblTotalContado.TabIndex = 70
        Me.lblTotalContado.Tag = ""
        Me.lblTotalContado.Text = "Total:"
        Me.lblTotalContado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotalArticulos
        '
        Me.clcTotalArticulos.EditValue = "0"
        Me.clcTotalArticulos.Location = New System.Drawing.Point(640, 334)
        Me.clcTotalArticulos.MaxValue = 0
        Me.clcTotalArticulos.MinValue = 0
        Me.clcTotalArticulos.Name = "clcTotalArticulos"
        '
        'clcTotalArticulos.Properties
        '
        Me.clcTotalArticulos.Properties.DisplayFormat.FormatString = "c2"
        Me.clcTotalArticulos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalArticulos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalArticulos.Properties.Enabled = False
        Me.clcTotalArticulos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotalArticulos.Properties.Precision = 2
        Me.clcTotalArticulos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotalArticulos.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcTotalArticulos.Size = New System.Drawing.Size(80, 19)
        Me.clcTotalArticulos.TabIndex = 37
        Me.clcTotalArticulos.TabStop = False
        Me.clcTotalArticulos.Tag = ""
        '
        'clcTotalMinimo
        '
        Me.clcTotalMinimo.EditValue = "0"
        Me.clcTotalMinimo.Location = New System.Drawing.Point(40, 376)
        Me.clcTotalMinimo.MaxValue = 0
        Me.clcTotalMinimo.MinValue = 0
        Me.clcTotalMinimo.Name = "clcTotalMinimo"
        '
        'clcTotalMinimo.Properties
        '
        Me.clcTotalMinimo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalMinimo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalMinimo.Properties.Enabled = False
        Me.clcTotalMinimo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotalMinimo.Properties.Precision = 2
        Me.clcTotalMinimo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotalMinimo.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcTotalMinimo.Size = New System.Drawing.Size(80, 19)
        Me.clcTotalMinimo.TabIndex = 60
        Me.clcTotalMinimo.TabStop = False
        Me.clcTotalMinimo.Tag = "totalminimo"
        Me.clcTotalMinimo.Visible = False
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "10/04/2006"
        Me.dteFecha.Location = New System.Drawing.Point(104, 32)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 39
        Me.dteFecha.Tag = "fecha"
        '
        'grVentas
        '
        '
        'grVentas.EmbeddedNavigator
        '
        Me.grVentas.EmbeddedNavigator.Name = ""
        Me.grVentas.Location = New System.Drawing.Point(8, 184)
        Me.grVentas.MainView = Me.grvVentas
        Me.grVentas.Name = "grVentas"
        Me.grVentas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.grchkSobrePedido, Me.grchkReparto, Me.grchkSurtido})
        Me.grVentas.Size = New System.Drawing.Size(712, 112)
        Me.grVentas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grVentas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.TabIndex = 61
        Me.grVentas.TabStop = False
        Me.grVentas.Text = "Ventas"
        '
        'grvVentas
        '
        Me.grvVentas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPartida, Me.grcNArticulo, Me.grcArticulo, Me.grcNBodega, Me.grcBodega, Me.grcSobrePedido, Me.grcReparto, Me.grcCantidad, Me.grcPrecioUnitario, Me.grcTotal, Me.grcCosto, Me.grcSurtido, Me.grcPrecioMinimo, Me.grcRegalo, Me.grcContado, Me.grcModelo, Me.grcFolioAutorizacion, Me.grcPartidaVistas, Me.grcNumeroSerie, Me.grcPrecioPactado, Me.grcDescripcionEspecial})
        Me.grvVentas.GridControl = Me.grVentas
        Me.grvVentas.Name = "grvVentas"
        Me.grvVentas.OptionsCustomization.AllowFilter = False
        Me.grvVentas.OptionsCustomization.AllowGroup = False
        Me.grvVentas.OptionsCustomization.AllowSort = False
        Me.grvVentas.OptionsView.ShowGroupPanel = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "Partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.Width = 52
        '
        'grcNArticulo
        '
        Me.grcNArticulo.Caption = "Art�culo"
        Me.grcNArticulo.FieldName = "n_articulo"
        Me.grcNArticulo.Name = "grcNArticulo"
        Me.grcNArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNArticulo.VisibleIndex = 1
        Me.grcNArticulo.Width = 167
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNBodega
        '
        Me.grcNBodega.Caption = "Bodega"
        Me.grcNBodega.FieldName = "n_bodega"
        Me.grcNBodega.Name = "grcNBodega"
        Me.grcNBodega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNBodega.VisibleIndex = 2
        Me.grcNBodega.Width = 80
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.FieldName = "bodega"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSobrePedido
        '
        Me.grcSobrePedido.Caption = "Ped. a F�brica"
        Me.grcSobrePedido.ColumnEdit = Me.grchkSobrePedido
        Me.grcSobrePedido.FieldName = "sobrepedido"
        Me.grcSobrePedido.Name = "grcSobrePedido"
        Me.grcSobrePedido.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSobrePedido.VisibleIndex = 4
        Me.grcSobrePedido.Width = 83
        '
        'grchkSobrePedido
        '
        Me.grchkSobrePedido.AutoHeight = False
        Me.grchkSobrePedido.Name = "grchkSobrePedido"
        Me.grchkSobrePedido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcReparto
        '
        Me.grcReparto.Caption = "Reparto"
        Me.grcReparto.ColumnEdit = Me.grchkReparto
        Me.grcReparto.FieldName = "reparto"
        Me.grcReparto.Name = "grcReparto"
        Me.grcReparto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcReparto.Width = 68
        '
        'grchkReparto
        '
        Me.grchkReparto.AutoHeight = False
        Me.grchkReparto.Name = "grchkReparto"
        Me.grchkReparto.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 5
        Me.grcCantidad.Width = 55
        '
        'grcPrecioUnitario
        '
        Me.grcPrecioUnitario.Caption = "Precio"
        Me.grcPrecioUnitario.DisplayFormat.FormatString = "c2"
        Me.grcPrecioUnitario.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioUnitario.FieldName = "preciounitario"
        Me.grcPrecioUnitario.Name = "grcPrecioUnitario"
        Me.grcPrecioUnitario.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioUnitario.VisibleIndex = 6
        Me.grcPrecioUnitario.Width = 63
        '
        'grcTotal
        '
        Me.grcTotal.Caption = "Total"
        Me.grcTotal.DisplayFormat.FormatString = "c2"
        Me.grcTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcTotal.FieldName = "total"
        Me.grcTotal.Name = "grcTotal"
        Me.grcTotal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTotal.VisibleIndex = 8
        Me.grcTotal.Width = 98
        '
        'grcCosto
        '
        Me.grcCosto.Caption = "Costo"
        Me.grcCosto.FieldName = "costo"
        Me.grcCosto.Name = "grcCosto"
        Me.grcCosto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSurtido
        '
        Me.grcSurtido.Caption = "Surtido"
        Me.grcSurtido.ColumnEdit = Me.grchkSurtido
        Me.grcSurtido.FieldName = "surtido"
        Me.grcSurtido.Name = "grcSurtido"
        Me.grcSurtido.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grchkSurtido
        '
        Me.grchkSurtido.AutoHeight = False
        Me.grchkSurtido.Name = "grchkSurtido"
        Me.grchkSurtido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcPrecioMinimo
        '
        Me.grcPrecioMinimo.Caption = "Precio Minimo"
        Me.grcPrecioMinimo.FieldName = "precio_minimo"
        Me.grcPrecioMinimo.Name = "grcPrecioMinimo"
        Me.grcPrecioMinimo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcRegalo
        '
        Me.grcRegalo.Caption = "Regalo"
        Me.grcRegalo.ColumnEdit = Me.grchkSurtido
        Me.grcRegalo.FieldName = "articulo_regalo"
        Me.grcRegalo.Name = "grcRegalo"
        Me.grcRegalo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcRegalo.VisibleIndex = 3
        Me.grcRegalo.Width = 50
        '
        'grcContado
        '
        Me.grcContado.Caption = "P. Contado"
        Me.grcContado.DisplayFormat.FormatString = "c2"
        Me.grcContado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcContado.FieldName = "precio_contado"
        Me.grcContado.Name = "grcContado"
        Me.grcContado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcContado.VisibleIndex = 7
        Me.grcContado.Width = 72
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 0
        Me.grcModelo.Width = 78
        '
        'grcFolioAutorizacion
        '
        Me.grcFolioAutorizacion.Caption = "Folio Aut."
        Me.grcFolioAutorizacion.FieldName = "folio_autorizacion"
        Me.grcFolioAutorizacion.Name = "grcFolioAutorizacion"
        Me.grcFolioAutorizacion.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcPartidaVistas
        '
        Me.grcPartidaVistas.Caption = "Partida Vistas"
        Me.grcPartidaVistas.FieldName = "partida_vistas"
        Me.grcPartidaVistas.Name = "grcPartidaVistas"
        Me.grcPartidaVistas.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNumeroSerie
        '
        Me.grcNumeroSerie.Caption = "N�mero Serie"
        Me.grcNumeroSerie.FieldName = "numero_serie"
        Me.grcNumeroSerie.Name = "grcNumeroSerie"
        Me.grcNumeroSerie.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcPrecioPactado
        '
        Me.grcPrecioPactado.Caption = "PrecioPactado"
        Me.grcPrecioPactado.FieldName = "precio_pactado"
        Me.grcPrecioPactado.Name = "grcPrecioPactado"
        Me.grcPrecioPactado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDescripcionEspecial
        '
        Me.grcDescripcionEspecial.Caption = "Desc. Especial"
        Me.grcDescripcionEspecial.FieldName = "descripcion_especial"
        Me.grcDescripcionEspecial.Name = "grcDescripcionEspecial"
        Me.grcDescripcionEspecial.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'tmaVentas
        '
        Me.tmaVentas.BackColor = System.Drawing.Color.White
        Me.tmaVentas.CanDelete = True
        Me.tmaVentas.CanInsert = True
        Me.tmaVentas.CanUpdate = True
        Me.tmaVentas.Grid = Me.grVentas
        Me.tmaVentas.Location = New System.Drawing.Point(8, 160)
        Me.tmaVentas.Name = "tmaVentas"
        Me.tmaVentas.Size = New System.Drawing.Size(712, 23)
        Me.tmaVentas.TabIndex = 59
        Me.tmaVentas.TabStop = False
        Me.tmaVentas.Title = "Art�culos"
        Me.tmaVentas.UpdateTitle = "un Art�culo"
        '
        'lblSubtotal
        '
        Me.lblSubtotal.AutoSize = True
        Me.lblSubtotal.Location = New System.Drawing.Point(568, 296)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(55, 16)
        Me.lblSubtotal.TabIndex = 66
        Me.lblSubtotal.Tag = ""
        Me.lblSubtotal.Text = "Subtotal:"
        Me.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImpuesto
        '
        Me.lblImpuesto.AutoSize = True
        Me.lblImpuesto.Location = New System.Drawing.Point(152, 376)
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Size = New System.Drawing.Size(61, 16)
        Me.lblImpuesto.TabIndex = 62
        Me.lblImpuesto.Tag = ""
        Me.lblImpuesto.Text = "&Impuesto:"
        Me.lblImpuesto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblImpuesto.Visible = False
        '
        'clcImpuesto
        '
        Me.clcImpuesto.EditValue = "0"
        Me.clcImpuesto.Location = New System.Drawing.Point(216, 376)
        Me.clcImpuesto.MaxValue = 0
        Me.clcImpuesto.MinValue = 0
        Me.clcImpuesto.Name = "clcImpuesto"
        '
        'clcImpuesto.Properties
        '
        Me.clcImpuesto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImpuesto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImpuesto.Properties.Enabled = False
        Me.clcImpuesto.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImpuesto.Properties.Precision = 2
        Me.clcImpuesto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImpuesto.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcImpuesto.Size = New System.Drawing.Size(80, 19)
        Me.clcImpuesto.TabIndex = 63
        Me.clcImpuesto.TabStop = False
        Me.clcImpuesto.Tag = "impuesto"
        Me.clcImpuesto.Visible = False
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(296, 376)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(36, 16)
        Me.lblTotal.TabIndex = 64
        Me.lblTotal.Tag = ""
        Me.lblTotal.Text = "&Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTotal.Visible = False
        '
        'clcTotal
        '
        Me.clcTotal.EditValue = "0"
        Me.clcTotal.Location = New System.Drawing.Point(336, 376)
        Me.clcTotal.MaxValue = 0
        Me.clcTotal.MinValue = 0
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.Enabled = False
        Me.clcTotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotal.Properties.Precision = 2
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcTotal.Size = New System.Drawing.Size(80, 19)
        Me.clcTotal.TabIndex = 65
        Me.clcTotal.TabStop = False
        Me.clcTotal.Tag = "total"
        Me.clcTotal.Visible = False
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(16, 104)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(84, 16)
        Me.lblTipoventa.TabIndex = 45
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "&Tipo de venta:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoventa
        '
        Me.cboTipoventa.EditValue = "C"
        Me.cboTipoventa.Location = New System.Drawing.Point(104, 104)
        Me.cboTipoventa.Name = "cboTipoventa"
        '
        'cboTipoventa.Properties
        '
        Me.cboTipoventa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoventa.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Contado", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cr�dito", "C", -1)})
        Me.cboTipoventa.Size = New System.Drawing.Size(72, 23)
        Me.cboTipoventa.TabIndex = 46
        Me.cboTipoventa.Tag = "tipoventa"
        '
        'chkIvadesglosado
        '
        Me.chkIvadesglosado.EditValue = "False"
        Me.chkIvadesglosado.Location = New System.Drawing.Point(192, 104)
        Me.chkIvadesglosado.Name = "chkIvadesglosado"
        '
        'chkIvadesglosado.Properties
        '
        Me.chkIvadesglosado.Properties.Caption = "Desea Deducir la Factura"
        Me.chkIvadesglosado.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkIvadesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkIvadesglosado.Size = New System.Drawing.Size(168, 19)
        Me.chkIvadesglosado.TabIndex = 47
        Me.chkIvadesglosado.Tag = "ivadesglosado"
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(40, 56)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(62, 16)
        Me.lblVendedor.TabIndex = 40
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "Ven&dedor:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpVendedor
        '
        Me.lkpVendedor.AllowAdd = False
        Me.lkpVendedor.AutoReaload = False
        Me.lkpVendedor.DataSource = Nothing
        Me.lkpVendedor.DefaultSearchField = ""
        Me.lkpVendedor.DisplayMember = "nombre"
        Me.lkpVendedor.EditValue = Nothing
        Me.lkpVendedor.Filtered = False
        Me.lkpVendedor.InitValue = Nothing
        Me.lkpVendedor.Location = New System.Drawing.Point(104, 56)
        Me.lkpVendedor.MultiSelect = False
        Me.lkpVendedor.Name = "lkpVendedor"
        Me.lkpVendedor.NullText = ""
        Me.lkpVendedor.PopupWidth = CType(420, Long)
        Me.lkpVendedor.ReadOnlyControl = False
        Me.lkpVendedor.Required = False
        Me.lkpVendedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpVendedor.SearchMember = ""
        Me.lkpVendedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpVendedor.SelectAll = False
        Me.lkpVendedor.Size = New System.Drawing.Size(360, 20)
        Me.lkpVendedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpVendedor.TabIndex = 41
        Me.lkpVendedor.Tag = "Vendedor"
        Me.lkpVendedor.ToolTip = Nothing
        Me.lkpVendedor.ValueMember = "Vendedor"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(56, 80)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 42
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(104, 80)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(520, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(360, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 43
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lbl_eFecha
        '
        Me.lbl_eFecha.AutoSize = True
        Me.lbl_eFecha.Location = New System.Drawing.Point(64, 32)
        Me.lbl_eFecha.Name = "lbl_eFecha"
        Me.lbl_eFecha.Size = New System.Drawing.Size(41, 16)
        Me.lbl_eFecha.TabIndex = 38
        Me.lbl_eFecha.Tag = ""
        Me.lbl_eFecha.Text = "F&echa:"
        Me.lbl_eFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_eFolio
        '
        Me.lbl_eFolio.AutoSize = True
        Me.lbl_eFolio.Location = New System.Drawing.Point(64, 8)
        Me.lbl_eFolio.Name = "lbl_eFolio"
        Me.lbl_eFolio.Size = New System.Drawing.Size(35, 16)
        Me.lbl_eFolio.TabIndex = 36
        Me.lbl_eFolio.Tag = ""
        Me.lbl_eFolio.Text = "Folio:"
        Me.lbl_eFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TinMaster1
        '
        Me.TinMaster1.BackColor = System.Drawing.Color.White
        Me.TinMaster1.CanDelete = True
        Me.TinMaster1.CanInsert = True
        Me.TinMaster1.CanUpdate = True
        Me.TinMaster1.Grid = Nothing
        Me.TinMaster1.Location = New System.Drawing.Point(8, 160)
        Me.TinMaster1.Name = "TinMaster1"
        Me.TinMaster1.Size = New System.Drawing.Size(648, 23)
        Me.TinMaster1.TabIndex = 58
        Me.TinMaster1.TabStop = False
        Me.TinMaster1.Title = "Art�culos"
        Me.TinMaster1.UpdateTitle = "un Art�culo"
        '
        'clcSubtotal
        '
        Me.clcSubtotal.EditValue = "0"
        Me.clcSubtotal.Location = New System.Drawing.Point(448, 8)
        Me.clcSubtotal.MaxValue = 0
        Me.clcSubtotal.MinValue = 0
        Me.clcSubtotal.Name = "clcSubtotal"
        '
        'clcSubtotal.Properties
        '
        Me.clcSubtotal.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSubtotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSubtotal.Properties.Enabled = False
        Me.clcSubtotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSubtotal.Properties.Precision = 2
        Me.clcSubtotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSubtotal.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcSubtotal.Size = New System.Drawing.Size(80, 19)
        Me.clcSubtotal.TabIndex = 67
        Me.clcSubtotal.TabStop = False
        Me.clcSubtotal.Tag = "subtotal"
        Me.clcSubtotal.Visible = False
        '
        'panel_Creditos
        '
        Me.panel_Creditos.Controls.Add(Me.chkaplica_enganche)
        Me.panel_Creditos.Controls.Add(Me.lblTotalDocumentos)
        Me.panel_Creditos.Controls.Add(Me.Label19)
        Me.panel_Creditos.Controls.Add(Me.lblDocumentosUltimoDocumento)
        Me.panel_Creditos.Controls.Add(Me.lblImporteUltimoDocumento)
        Me.panel_Creditos.Controls.Add(Me.lblLeyendaDocumentoFinal)
        Me.panel_Creditos.Controls.Add(Me.lblDocumentoFinal)
        Me.panel_Creditos.Controls.Add(Me.lblLeyendaDocumentos)
        Me.panel_Creditos.Controls.Add(Me.lblDocumentosIguales)
        Me.panel_Creditos.Controls.Add(Me.Label20)
        Me.panel_Creditos.Controls.Add(Me.clcImportePlan)
        Me.panel_Creditos.Controls.Add(Me.clcEnganche2)
        Me.panel_Creditos.Controls.Add(Me.chkEnganche_en_Menos)
        Me.panel_Creditos.Controls.Add(Me.Label11)
        Me.panel_Creditos.Controls.Add(Me.clcMenos_Enganch)
        Me.panel_Creditos.Controls.Add(Me.chkLiquida_Vencimiento)
        Me.panel_Creditos.Controls.Add(Me.chkIvadesglosado1)
        Me.panel_Creditos.Controls.Add(Me.Label8)
        Me.panel_Creditos.Controls.Add(Me.clcTotalCredito)
        Me.panel_Creditos.Controls.Add(Me.GroupBox2)
        Me.panel_Creditos.Controls.Add(Me.chkEnganche_En_Documento)
        Me.panel_Creditos.Controls.Add(Me.GroupBox1)
        Me.panel_Creditos.Controls.Add(Me.Label9)
        Me.panel_Creditos.Controls.Add(Me.lblPlan)
        Me.panel_Creditos.Controls.Add(Me.lkpPlan)
        Me.panel_Creditos.Controls.Add(Me.lblFecha_Primer_Documento)
        Me.panel_Creditos.Controls.Add(Me.dteFecha_Primer_Documento)
        Me.panel_Creditos.Controls.Add(Me.lblNumero_Documentos)
        Me.panel_Creditos.Controls.Add(Me.clcNumero_Documentos)
        Me.panel_Creditos.Controls.Add(Me.lblImporte_Documentos)
        Me.panel_Creditos.Controls.Add(Me.clcImporte_Documentos)
        Me.panel_Creditos.Controls.Add(Me.lblImporte_Ultimo_Documento)
        Me.panel_Creditos.Controls.Add(Me.clcImporte_Ultimo_Documento)
        Me.panel_Creditos.Controls.Add(Me.lblImporte)
        Me.panel_Creditos.Controls.Add(Me.clcImporte)
        Me.panel_Creditos.Controls.Add(Me.lblMenos)
        Me.panel_Creditos.Controls.Add(Me.clcMenos)
        Me.panel_Creditos.Controls.Add(Me.lblInteres)
        Me.panel_Creditos.Controls.Add(Me.clcEnganche1)
        Me.panel_Creditos.Location = New System.Drawing.Point(8, 64)
        Me.panel_Creditos.Name = "panel_Creditos"
        Me.panel_Creditos.Size = New System.Drawing.Size(731, 360)
        Me.panel_Creditos.TabIndex = 2
        '
        'chkaplica_enganche
        '
        Me.chkaplica_enganche.EditValue = "False"
        Me.chkaplica_enganche.Location = New System.Drawing.Point(488, 176)
        Me.chkaplica_enganche.Name = "chkaplica_enganche"
        '
        'chkaplica_enganche.Properties
        '
        Me.chkaplica_enganche.Properties.Caption = "Aplica para Enganche"
        Me.chkaplica_enganche.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkaplica_enganche.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkaplica_enganche.Size = New System.Drawing.Size(168, 19)
        Me.chkaplica_enganche.TabIndex = 113
        Me.chkaplica_enganche.Tag = "aplica_enganche"
        '
        'lblTotalDocumentos
        '
        Me.lblTotalDocumentos.Location = New System.Drawing.Point(224, 184)
        Me.lblTotalDocumentos.Name = "lblTotalDocumentos"
        Me.lblTotalDocumentos.Size = New System.Drawing.Size(88, 23)
        Me.lblTotalDocumentos.TabIndex = 112
        Me.lblTotalDocumentos.Text = "0.00"
        Me.lblTotalDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label19
        '
        Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label19.Location = New System.Drawing.Point(224, 232)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(100, 1)
        Me.Label19.TabIndex = 111
        '
        'lblDocumentosUltimoDocumento
        '
        Me.lblDocumentosUltimoDocumento.Location = New System.Drawing.Point(224, 232)
        Me.lblDocumentosUltimoDocumento.Name = "lblDocumentosUltimoDocumento"
        Me.lblDocumentosUltimoDocumento.Size = New System.Drawing.Size(88, 23)
        Me.lblDocumentosUltimoDocumento.TabIndex = 110
        Me.lblDocumentosUltimoDocumento.Text = "0.00"
        Me.lblDocumentosUltimoDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImporteUltimoDocumento
        '
        Me.lblImporteUltimoDocumento.Location = New System.Drawing.Point(224, 208)
        Me.lblImporteUltimoDocumento.Name = "lblImporteUltimoDocumento"
        Me.lblImporteUltimoDocumento.Size = New System.Drawing.Size(88, 23)
        Me.lblImporteUltimoDocumento.TabIndex = 109
        Me.lblImporteUltimoDocumento.Text = "0.00"
        Me.lblImporteUltimoDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLeyendaDocumentoFinal
        '
        Me.lblLeyendaDocumentoFinal.Location = New System.Drawing.Point(56, 208)
        Me.lblLeyendaDocumentoFinal.Name = "lblLeyendaDocumentoFinal"
        Me.lblLeyendaDocumentoFinal.Size = New System.Drawing.Size(160, 23)
        Me.lblLeyendaDocumentoFinal.TabIndex = 108
        Me.lblLeyendaDocumentoFinal.Text = "Documento de  ="
        Me.lblLeyendaDocumentoFinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDocumentoFinal
        '
        Me.lblDocumentoFinal.BackColor = System.Drawing.SystemColors.Window
        Me.lblDocumentoFinal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentoFinal.ForeColor = System.Drawing.Color.Black
        Me.lblDocumentoFinal.Location = New System.Drawing.Point(24, 208)
        Me.lblDocumentoFinal.Name = "lblDocumentoFinal"
        Me.lblDocumentoFinal.Size = New System.Drawing.Size(32, 23)
        Me.lblDocumentoFinal.TabIndex = 107
        Me.lblDocumentoFinal.Text = "0"
        Me.lblDocumentoFinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLeyendaDocumentos
        '
        Me.lblLeyendaDocumentos.Location = New System.Drawing.Point(56, 184)
        Me.lblLeyendaDocumentos.Name = "lblLeyendaDocumentos"
        Me.lblLeyendaDocumentos.Size = New System.Drawing.Size(160, 23)
        Me.lblLeyendaDocumentos.TabIndex = 106
        Me.lblLeyendaDocumentos.Text = "Documentos de = "
        Me.lblLeyendaDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDocumentosIguales
        '
        Me.lblDocumentosIguales.BackColor = System.Drawing.SystemColors.Window
        Me.lblDocumentosIguales.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentosIguales.ForeColor = System.Drawing.Color.Black
        Me.lblDocumentosIguales.Location = New System.Drawing.Point(24, 184)
        Me.lblDocumentosIguales.Name = "lblDocumentosIguales"
        Me.lblDocumentosIguales.Size = New System.Drawing.Size(32, 23)
        Me.lblDocumentosIguales.TabIndex = 105
        Me.lblDocumentosIguales.Text = "0"
        Me.lblDocumentosIguales.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(136, 56)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(80, 16)
        Me.Label20.TabIndex = 103
        Me.Label20.Tag = ""
        Me.Label20.Text = "&Importe Plan:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImportePlan
        '
        Me.clcImportePlan.EditValue = "0"
        Me.clcImportePlan.Location = New System.Drawing.Point(224, 56)
        Me.clcImportePlan.MaxValue = 0
        Me.clcImportePlan.MinValue = 0
        Me.clcImportePlan.Name = "clcImportePlan"
        '
        'clcImportePlan.Properties
        '
        Me.clcImportePlan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImportePlan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImportePlan.Properties.Enabled = False
        Me.clcImportePlan.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcImportePlan.Properties.Precision = 2
        Me.clcImportePlan.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImportePlan.Size = New System.Drawing.Size(96, 19)
        Me.clcImportePlan.TabIndex = 104
        Me.clcImportePlan.Tag = ""
        '
        'clcEnganche2
        '
        Me.clcEnganche2.EditValue = "0"
        Me.clcEnganche2.Location = New System.Drawing.Point(568, 80)
        Me.clcEnganche2.MaxValue = 0
        Me.clcEnganche2.MinValue = 0
        Me.clcEnganche2.Name = "clcEnganche2"
        '
        'clcEnganche2.Properties
        '
        Me.clcEnganche2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche2.Properties.Enabled = False
        Me.clcEnganche2.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcEnganche2.Properties.Precision = 2
        Me.clcEnganche2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEnganche2.Size = New System.Drawing.Size(80, 19)
        Me.clcEnganche2.TabIndex = 84
        Me.clcEnganche2.Tag = "interes"
        '
        'chkEnganche_en_Menos
        '
        Me.chkEnganche_en_Menos.EditValue = "False"
        Me.chkEnganche_en_Menos.Location = New System.Drawing.Point(32, 352)
        Me.chkEnganche_en_Menos.Name = "chkEnganche_en_Menos"
        '
        'chkEnganche_en_Menos.Properties
        '
        Me.chkEnganche_en_Menos.Properties.Caption = "Enganche en Menos"
        Me.chkEnganche_en_Menos.Properties.Enabled = False
        Me.chkEnganche_en_Menos.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkEnganche_en_Menos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkEnganche_en_Menos.Size = New System.Drawing.Size(128, 19)
        Me.chkEnganche_en_Menos.TabIndex = 102
        Me.chkEnganche_en_Menos.Tag = "enganche_en_menos"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(496, 56)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(63, 16)
        Me.Label11.TabIndex = 81
        Me.Label11.Tag = ""
        Me.Label11.Text = "&Enganche:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMenos_Enganch
        '
        Me.clcMenos_Enganch.EditValue = "0"
        Me.clcMenos_Enganch.Location = New System.Drawing.Point(32, 328)
        Me.clcMenos_Enganch.MaxValue = 0
        Me.clcMenos_Enganch.MinValue = 0
        Me.clcMenos_Enganch.Name = "clcMenos_Enganch"
        '
        'clcMenos_Enganch.Properties
        '
        Me.clcMenos_Enganch.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenos_Enganch.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenos_Enganch.Properties.Enabled = False
        Me.clcMenos_Enganch.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMenos_Enganch.Properties.Precision = 2
        Me.clcMenos_Enganch.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMenos_Enganch.Size = New System.Drawing.Size(80, 19)
        Me.clcMenos_Enganch.TabIndex = 101
        Me.clcMenos_Enganch.Tag = ""
        Me.clcMenos_Enganch.Visible = False
        '
        'chkLiquida_Vencimiento
        '
        Me.chkLiquida_Vencimiento.EditValue = "False"
        Me.chkLiquida_Vencimiento.Location = New System.Drawing.Point(40, 264)
        Me.chkLiquida_Vencimiento.Name = "chkLiquida_Vencimiento"
        '
        'chkLiquida_Vencimiento.Properties
        '
        Me.chkLiquida_Vencimiento.Properties.Caption = "&Liquida al vencimiento a tiempo"
        Me.chkLiquida_Vencimiento.Properties.Enabled = False
        Me.chkLiquida_Vencimiento.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkLiquida_Vencimiento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkLiquida_Vencimiento.Size = New System.Drawing.Size(184, 19)
        Me.chkLiquida_Vencimiento.TabIndex = 97
        Me.chkLiquida_Vencimiento.Tag = "liquida_vencimiento"
        '
        'chkIvadesglosado1
        '
        Me.chkIvadesglosado1.EditValue = "False"
        Me.chkIvadesglosado1.Location = New System.Drawing.Point(488, 152)
        Me.chkIvadesglosado1.Name = "chkIvadesglosado1"
        '
        'chkIvadesglosado1.Properties
        '
        Me.chkIvadesglosado1.Properties.Caption = "&Factura con IVA desglosado"
        Me.chkIvadesglosado1.Properties.Enabled = False
        Me.chkIvadesglosado1.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkIvadesglosado1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkIvadesglosado1.Size = New System.Drawing.Size(168, 19)
        Me.chkIvadesglosado1.TabIndex = 96
        Me.chkIvadesglosado1.Tag = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(520, 112)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(36, 16)
        Me.Label8.TabIndex = 86
        Me.Label8.Tag = ""
        Me.Label8.Text = "&Total:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotalCredito
        '
        Me.clcTotalCredito.EditValue = "0"
        Me.clcTotalCredito.Location = New System.Drawing.Point(568, 112)
        Me.clcTotalCredito.MaxValue = 0
        Me.clcTotalCredito.MinValue = 0
        Me.clcTotalCredito.Name = "clcTotalCredito"
        '
        'clcTotalCredito.Properties
        '
        Me.clcTotalCredito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalCredito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalCredito.Properties.Enabled = False
        Me.clcTotalCredito.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotalCredito.Properties.Precision = 2
        Me.clcTotalCredito.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotalCredito.Size = New System.Drawing.Size(80, 19)
        Me.clcTotalCredito.TabIndex = 87
        Me.clcTotalCredito.Tag = ""
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.clcMonto_Liquida_Vencimiento)
        Me.GroupBox2.Location = New System.Drawing.Point(32, 264)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(352, 56)
        Me.GroupBox2.TabIndex = 98
        Me.GroupBox2.TabStop = False
        '
        'clcMonto_Liquida_Vencimiento
        '
        Me.clcMonto_Liquida_Vencimiento.EditValue = "0"
        Me.clcMonto_Liquida_Vencimiento.Location = New System.Drawing.Point(80, 24)
        Me.clcMonto_Liquida_Vencimiento.MaxValue = 0
        Me.clcMonto_Liquida_Vencimiento.MinValue = 0
        Me.clcMonto_Liquida_Vencimiento.Name = "clcMonto_Liquida_Vencimiento"
        '
        'clcMonto_Liquida_Vencimiento.Properties
        '
        Me.clcMonto_Liquida_Vencimiento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto_Liquida_Vencimiento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto_Liquida_Vencimiento.Properties.Enabled = False
        Me.clcMonto_Liquida_Vencimiento.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMonto_Liquida_Vencimiento.Properties.Precision = 2
        Me.clcMonto_Liquida_Vencimiento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMonto_Liquida_Vencimiento.Size = New System.Drawing.Size(80, 19)
        Me.clcMonto_Liquida_Vencimiento.TabIndex = 1
        Me.clcMonto_Liquida_Vencimiento.Tag = "monto_liquida_vencimiento"
        '
        'chkEnganche_En_Documento
        '
        Me.chkEnganche_En_Documento.EditValue = "False"
        Me.chkEnganche_En_Documento.Location = New System.Drawing.Point(448, 240)
        Me.chkEnganche_En_Documento.Name = "chkEnganche_En_Documento"
        '
        'chkEnganche_En_Documento.Properties
        '
        Me.chkEnganche_En_Documento.Properties.Caption = "&Enganche en documento"
        Me.chkEnganche_En_Documento.Properties.Enabled = False
        Me.chkEnganche_En_Documento.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkEnganche_En_Documento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkEnganche_En_Documento.Size = New System.Drawing.Size(152, 19)
        Me.chkEnganche_En_Documento.TabIndex = 99
        Me.chkEnganche_En_Documento.Tag = "enganche_en_documento"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblFecha_Enganche_Documento)
        Me.GroupBox1.Controls.Add(Me.dteFecha_Enganche_Documento)
        Me.GroupBox1.Controls.Add(Me.lblImporte_Enganche_Documento)
        Me.GroupBox1.Controls.Add(Me.clcImporte_Enganche_Documento)
        Me.GroupBox1.Location = New System.Drawing.Point(440, 240)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(264, 80)
        Me.GroupBox1.TabIndex = 100
        Me.GroupBox1.TabStop = False
        '
        'lblFecha_Enganche_Documento
        '
        Me.lblFecha_Enganche_Documento.AutoSize = True
        Me.lblFecha_Enganche_Documento.Location = New System.Drawing.Point(32, 24)
        Me.lblFecha_Enganche_Documento.Name = "lblFecha_Enganche_Documento"
        Me.lblFecha_Enganche_Documento.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha_Enganche_Documento.TabIndex = 0
        Me.lblFecha_Enganche_Documento.Tag = ""
        Me.lblFecha_Enganche_Documento.Text = "&Fecha:"
        Me.lblFecha_Enganche_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Enganche_Documento
        '
        Me.dteFecha_Enganche_Documento.EditValue = "10/04/2006"
        Me.dteFecha_Enganche_Documento.Location = New System.Drawing.Point(80, 24)
        Me.dteFecha_Enganche_Documento.Name = "dteFecha_Enganche_Documento"
        '
        'dteFecha_Enganche_Documento.Properties
        '
        Me.dteFecha_Enganche_Documento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Enganche_Documento.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Enganche_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Enganche_Documento.Properties.Enabled = False
        Me.dteFecha_Enganche_Documento.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFecha_Enganche_Documento.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha_Enganche_Documento.TabIndex = 1
        Me.dteFecha_Enganche_Documento.Tag = "fecha_enganche_documento"
        '
        'lblImporte_Enganche_Documento
        '
        Me.lblImporte_Enganche_Documento.AutoSize = True
        Me.lblImporte_Enganche_Documento.Location = New System.Drawing.Point(20, 48)
        Me.lblImporte_Enganche_Documento.Name = "lblImporte_Enganche_Documento"
        Me.lblImporte_Enganche_Documento.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte_Enganche_Documento.TabIndex = 2
        Me.lblImporte_Enganche_Documento.Tag = ""
        Me.lblImporte_Enganche_Documento.Text = "&Importe:"
        Me.lblImporte_Enganche_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte_Enganche_Documento
        '
        Me.clcImporte_Enganche_Documento.EditValue = "0"
        Me.clcImporte_Enganche_Documento.Location = New System.Drawing.Point(80, 48)
        Me.clcImporte_Enganche_Documento.MaxValue = 0
        Me.clcImporte_Enganche_Documento.MinValue = 0
        Me.clcImporte_Enganche_Documento.Name = "clcImporte_Enganche_Documento"
        '
        'clcImporte_Enganche_Documento.Properties
        '
        Me.clcImporte_Enganche_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Enganche_Documento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Enganche_Documento.Properties.Enabled = False
        Me.clcImporte_Enganche_Documento.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte_Enganche_Documento.Properties.Precision = 2
        Me.clcImporte_Enganche_Documento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte_Enganche_Documento.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcImporte_Enganche_Documento.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte_Enganche_Documento.TabIndex = 3
        Me.clcImporte_Enganche_Documento.Tag = "importe_enganche_documento"
        '
        'Label9
        '
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Location = New System.Drawing.Point(480, 104)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(180, 1)
        Me.Label9.TabIndex = 85
        '
        'lblPlan
        '
        Me.lblPlan.AutoSize = True
        Me.lblPlan.Location = New System.Drawing.Point(120, 8)
        Me.lblPlan.Name = "lblPlan"
        Me.lblPlan.Size = New System.Drawing.Size(91, 16)
        Me.lblPlan.TabIndex = 75
        Me.lblPlan.Tag = ""
        Me.lblPlan.Text = "&Plan de cr�dito:"
        Me.lblPlan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPlan
        '
        Me.lkpPlan.AllowAdd = False
        Me.lkpPlan.AutoReaload = False
        Me.lkpPlan.DataSource = Nothing
        Me.lkpPlan.DefaultSearchField = ""
        Me.lkpPlan.DisplayMember = "descripcion"
        Me.lkpPlan.EditValue = Nothing
        Me.lkpPlan.Enabled = False
        Me.lkpPlan.Filtered = False
        Me.lkpPlan.InitValue = Nothing
        Me.lkpPlan.Location = New System.Drawing.Point(224, 8)
        Me.lkpPlan.MultiSelect = False
        Me.lkpPlan.Name = "lkpPlan"
        Me.lkpPlan.NullText = ""
        Me.lkpPlan.PopupWidth = CType(390, Long)
        Me.lkpPlan.ReadOnlyControl = False
        Me.lkpPlan.Required = False
        Me.lkpPlan.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlan.SearchMember = ""
        Me.lkpPlan.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlan.SelectAll = False
        Me.lkpPlan.Size = New System.Drawing.Size(264, 20)
        Me.lkpPlan.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPlan.TabIndex = 76
        Me.lkpPlan.Tag = ""
        Me.lkpPlan.ToolTip = Nothing
        Me.lkpPlan.ValueMember = "plan_credito"
        '
        'lblFecha_Primer_Documento
        '
        Me.lblFecha_Primer_Documento.AutoSize = True
        Me.lblFecha_Primer_Documento.Location = New System.Drawing.Point(48, 32)
        Me.lblFecha_Primer_Documento.Name = "lblFecha_Primer_Documento"
        Me.lblFecha_Primer_Documento.Size = New System.Drawing.Size(167, 16)
        Me.lblFecha_Primer_Documento.TabIndex = 88
        Me.lblFecha_Primer_Documento.Tag = ""
        Me.lblFecha_Primer_Documento.Text = "&Fecha del primer documento:"
        Me.lblFecha_Primer_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Primer_Documento
        '
        Me.dteFecha_Primer_Documento.EditValue = "10/04/2006"
        Me.dteFecha_Primer_Documento.Location = New System.Drawing.Point(224, 32)
        Me.dteFecha_Primer_Documento.Name = "dteFecha_Primer_Documento"
        '
        'dteFecha_Primer_Documento.Properties
        '
        Me.dteFecha_Primer_Documento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Primer_Documento.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Primer_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Primer_Documento.Properties.Enabled = False
        Me.dteFecha_Primer_Documento.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha_Primer_Documento.TabIndex = 89
        Me.dteFecha_Primer_Documento.Tag = "fecha_primer_documento"
        '
        'lblNumero_Documentos
        '
        Me.lblNumero_Documentos.AutoSize = True
        Me.lblNumero_Documentos.Location = New System.Drawing.Point(72, 80)
        Me.lblNumero_Documentos.Name = "lblNumero_Documentos"
        Me.lblNumero_Documentos.Size = New System.Drawing.Size(141, 16)
        Me.lblNumero_Documentos.TabIndex = 90
        Me.lblNumero_Documentos.Tag = ""
        Me.lblNumero_Documentos.Text = "&N�mero de documentos:"
        Me.lblNumero_Documentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcNumero_Documentos
        '
        Me.clcNumero_Documentos.EditValue = "0"
        Me.clcNumero_Documentos.Location = New System.Drawing.Point(224, 80)
        Me.clcNumero_Documentos.MaxValue = 0
        Me.clcNumero_Documentos.MinValue = 0
        Me.clcNumero_Documentos.Name = "clcNumero_Documentos"
        '
        'clcNumero_Documentos.Properties
        '
        Me.clcNumero_Documentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcNumero_Documentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcNumero_Documentos.Properties.Enabled = False
        Me.clcNumero_Documentos.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcNumero_Documentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcNumero_Documentos.Size = New System.Drawing.Size(32, 19)
        Me.clcNumero_Documentos.TabIndex = 91
        Me.clcNumero_Documentos.Tag = "numero_documentos"
        '
        'lblImporte_Documentos
        '
        Me.lblImporte_Documentos.AutoSize = True
        Me.lblImporte_Documentos.Location = New System.Drawing.Point(72, 104)
        Me.lblImporte_Documentos.Name = "lblImporte_Documentos"
        Me.lblImporte_Documentos.Size = New System.Drawing.Size(142, 16)
        Me.lblImporte_Documentos.TabIndex = 92
        Me.lblImporte_Documentos.Tag = ""
        Me.lblImporte_Documentos.Text = "Importe de documentos:"
        Me.lblImporte_Documentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte_Documentos
        '
        Me.clcImporte_Documentos.EditValue = "0"
        Me.clcImporte_Documentos.Location = New System.Drawing.Point(224, 104)
        Me.clcImporte_Documentos.MaxValue = 0
        Me.clcImporte_Documentos.MinValue = 0
        Me.clcImporte_Documentos.Name = "clcImporte_Documentos"
        '
        'clcImporte_Documentos.Properties
        '
        Me.clcImporte_Documentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Documentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Documentos.Properties.Enabled = False
        Me.clcImporte_Documentos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte_Documentos.Properties.Precision = 2
        Me.clcImporte_Documentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte_Documentos.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte_Documentos.TabIndex = 93
        Me.clcImporte_Documentos.Tag = "importe_documentos"
        '
        'lblImporte_Ultimo_Documento
        '
        Me.lblImporte_Ultimo_Documento.AutoSize = True
        Me.lblImporte_Ultimo_Documento.Location = New System.Drawing.Point(40, 128)
        Me.lblImporte_Ultimo_Documento.Name = "lblImporte_Ultimo_Documento"
        Me.lblImporte_Ultimo_Documento.Size = New System.Drawing.Size(177, 16)
        Me.lblImporte_Ultimo_Documento.TabIndex = 94
        Me.lblImporte_Ultimo_Documento.Tag = ""
        Me.lblImporte_Ultimo_Documento.Text = "Importe del �ltimo documento:"
        Me.lblImporte_Ultimo_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte_Ultimo_Documento
        '
        Me.clcImporte_Ultimo_Documento.EditValue = "0"
        Me.clcImporte_Ultimo_Documento.Location = New System.Drawing.Point(224, 128)
        Me.clcImporte_Ultimo_Documento.MaxValue = 0
        Me.clcImporte_Ultimo_Documento.MinValue = 0
        Me.clcImporte_Ultimo_Documento.Name = "clcImporte_Ultimo_Documento"
        '
        'clcImporte_Ultimo_Documento.Properties
        '
        Me.clcImporte_Ultimo_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Ultimo_Documento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Ultimo_Documento.Properties.Enabled = False
        Me.clcImporte_Ultimo_Documento.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte_Ultimo_Documento.Properties.Precision = 2
        Me.clcImporte_Ultimo_Documento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte_Ultimo_Documento.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcImporte_Ultimo_Documento.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte_Ultimo_Documento.TabIndex = 95
        Me.clcImporte_Ultimo_Documento.Tag = "importe_ultimo_documento"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(504, 8)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 77
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(568, 8)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte.TabIndex = 78
        Me.clcImporte.Tag = "importe"
        '
        'lblMenos
        '
        Me.lblMenos.AutoSize = True
        Me.lblMenos.Location = New System.Drawing.Point(512, 32)
        Me.lblMenos.Name = "lblMenos"
        Me.lblMenos.Size = New System.Drawing.Size(44, 16)
        Me.lblMenos.TabIndex = 79
        Me.lblMenos.Tag = ""
        Me.lblMenos.Text = "&Menos:"
        Me.lblMenos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMenos
        '
        Me.clcMenos.EditValue = "0"
        Me.clcMenos.Location = New System.Drawing.Point(568, 32)
        Me.clcMenos.MaxValue = 0
        Me.clcMenos.MinValue = 0
        Me.clcMenos.Name = "clcMenos"
        '
        'clcMenos.Properties
        '
        Me.clcMenos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenos.Properties.Enabled = False
        Me.clcMenos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMenos.Properties.Precision = 2
        Me.clcMenos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMenos.Size = New System.Drawing.Size(80, 19)
        Me.clcMenos.TabIndex = 80
        Me.clcMenos.Tag = "menos"
        '
        'lblInteres
        '
        Me.lblInteres.AutoSize = True
        Me.lblInteres.Location = New System.Drawing.Point(504, 80)
        Me.lblInteres.Name = "lblInteres"
        Me.lblInteres.Size = New System.Drawing.Size(48, 16)
        Me.lblInteres.TabIndex = 83
        Me.lblInteres.Tag = ""
        Me.lblInteres.Text = "&Int�res:"
        Me.lblInteres.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEnganche1
        '
        Me.clcEnganche1.EditValue = "0"
        Me.clcEnganche1.Location = New System.Drawing.Point(568, 56)
        Me.clcEnganche1.MaxValue = 0
        Me.clcEnganche1.MinValue = 0
        Me.clcEnganche1.Name = "clcEnganche1"
        '
        'clcEnganche1.Properties
        '
        Me.clcEnganche1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche1.Properties.Enabled = False
        Me.clcEnganche1.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcEnganche1.Properties.Precision = 2
        Me.clcEnganche1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEnganche1.Size = New System.Drawing.Size(80, 19)
        Me.clcEnganche1.TabIndex = 82
        Me.clcEnganche1.Tag = "enganche"
        '
        'Panel_Repartos
        '
        Me.Panel_Repartos.Controls.Add(Me.Label21)
        Me.Panel_Repartos.Controls.Add(Me.cboHorario)
        Me.Panel_Repartos.Controls.Add(Me.lblFecha)
        Me.Panel_Repartos.Controls.Add(Me.dteFechaReparto)
        Me.Panel_Repartos.Controls.Add(Me.txtObservacionesReparto)
        Me.Panel_Repartos.Controls.Add(Me.Label5)
        Me.Panel_Repartos.Controls.Add(Me.Label2)
        Me.Panel_Repartos.Controls.Add(Me.txtDomicilioReparto)
        Me.Panel_Repartos.Controls.Add(Me.Label3)
        Me.Panel_Repartos.Controls.Add(Me.txtColoniaReparto)
        Me.Panel_Repartos.Controls.Add(Me.Label4)
        Me.Panel_Repartos.Controls.Add(Me.txtCiudadReparto)
        Me.Panel_Repartos.Controls.Add(Me.Label6)
        Me.Panel_Repartos.Controls.Add(Me.txtEstadoReparto)
        Me.Panel_Repartos.Controls.Add(Me.Label7)
        Me.Panel_Repartos.Controls.Add(Me.txtTelefonoReparto)
        Me.Panel_Repartos.Location = New System.Drawing.Point(8, 64)
        Me.Panel_Repartos.Name = "Panel_Repartos"
        Me.Panel_Repartos.Size = New System.Drawing.Size(731, 360)
        Me.Panel_Repartos.TabIndex = 3
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(95, 136)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(50, 16)
        Me.Label21.TabIndex = 22
        Me.Label21.Tag = ""
        Me.Label21.Text = "H&orario:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboHorario
        '
        Me.cboHorario.EditValue = 1
        Me.cboHorario.Location = New System.Drawing.Point(152, 136)
        Me.cboHorario.Name = "cboHorario"
        '
        'cboHorario.Properties
        '
        Me.cboHorario.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboHorario.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ma�ana", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tarde", 2, -1)})
        Me.cboHorario.Size = New System.Drawing.Size(96, 23)
        Me.cboHorario.TabIndex = 23
        Me.cboHorario.Tag = "horario_reparto"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(104, 160)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 24
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaReparto
        '
        Me.dteFechaReparto.EditValue = New Date(2006, 4, 3, 0, 0, 0, 0)
        Me.dteFechaReparto.Location = New System.Drawing.Point(152, 160)
        Me.dteFechaReparto.Name = "dteFechaReparto"
        '
        'dteFechaReparto.Properties
        '
        Me.dteFechaReparto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaReparto.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaReparto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaReparto.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaReparto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaReparto.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaReparto.TabIndex = 25
        Me.dteFechaReparto.Tag = "fecha_reparto"
        '
        'txtObservacionesReparto
        '
        Me.txtObservacionesReparto.EditValue = ""
        Me.txtObservacionesReparto.Location = New System.Drawing.Point(152, 184)
        Me.txtObservacionesReparto.Name = "txtObservacionesReparto"
        Me.txtObservacionesReparto.Size = New System.Drawing.Size(488, 112)
        Me.txtObservacionesReparto.TabIndex = 27
        Me.txtObservacionesReparto.Tag = "observaciones_reparto"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(56, 184)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 16)
        Me.Label5.TabIndex = 26
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Observaciones:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(86, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Domicilio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilioReparto
        '
        Me.txtDomicilioReparto.EditValue = ""
        Me.txtDomicilioReparto.Location = New System.Drawing.Point(152, 16)
        Me.txtDomicilioReparto.Name = "txtDomicilioReparto"
        '
        'txtDomicilioReparto.Properties
        '
        Me.txtDomicilioReparto.Properties.MaxLength = 80
        Me.txtDomicilioReparto.Size = New System.Drawing.Size(488, 20)
        Me.txtDomicilioReparto.TabIndex = 13
        Me.txtDomicilioReparto.Tag = "domicilio_reparto"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(95, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 16)
        Me.Label3.TabIndex = 14
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Colonia:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColoniaReparto
        '
        Me.txtColoniaReparto.EditValue = ""
        Me.txtColoniaReparto.Location = New System.Drawing.Point(152, 40)
        Me.txtColoniaReparto.Name = "txtColoniaReparto"
        '
        'txtColoniaReparto.Properties
        '
        Me.txtColoniaReparto.Properties.MaxLength = 50
        Me.txtColoniaReparto.Size = New System.Drawing.Size(300, 20)
        Me.txtColoniaReparto.TabIndex = 15
        Me.txtColoniaReparto.Tag = "colonia_reparto"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(98, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 16
        Me.Label4.Tag = ""
        Me.Label4.Text = "C&iudad:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudadReparto
        '
        Me.txtCiudadReparto.EditValue = ""
        Me.txtCiudadReparto.Location = New System.Drawing.Point(152, 64)
        Me.txtCiudadReparto.Name = "txtCiudadReparto"
        '
        'txtCiudadReparto.Properties
        '
        Me.txtCiudadReparto.Properties.MaxLength = 50
        Me.txtCiudadReparto.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudadReparto.TabIndex = 17
        Me.txtCiudadReparto.Tag = "ciudad_reparto"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(99, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 16)
        Me.Label6.TabIndex = 18
        Me.Label6.Tag = ""
        Me.Label6.Text = "&Estado:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstadoReparto
        '
        Me.txtEstadoReparto.EditValue = ""
        Me.txtEstadoReparto.Location = New System.Drawing.Point(152, 88)
        Me.txtEstadoReparto.Name = "txtEstadoReparto"
        '
        'txtEstadoReparto.Properties
        '
        Me.txtEstadoReparto.Properties.MaxLength = 50
        Me.txtEstadoReparto.Size = New System.Drawing.Size(300, 20)
        Me.txtEstadoReparto.TabIndex = 19
        Me.txtEstadoReparto.Tag = "estado_reparto"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(89, 112)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 16)
        Me.Label7.TabIndex = 20
        Me.Label7.Tag = ""
        Me.Label7.Text = "&Tel�fono:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefonoReparto
        '
        Me.txtTelefonoReparto.EditValue = ""
        Me.txtTelefonoReparto.Location = New System.Drawing.Point(152, 112)
        Me.txtTelefonoReparto.Name = "txtTelefonoReparto"
        '
        'txtTelefonoReparto.Properties
        '
        Me.txtTelefonoReparto.Properties.MaxLength = 13
        Me.txtTelefonoReparto.Size = New System.Drawing.Size(88, 20)
        Me.txtTelefonoReparto.TabIndex = 21
        Me.txtTelefonoReparto.Tag = "telefono_reparto"
        '
        'Panel_Cliente
        '
        Me.Panel_Cliente.Controls.Add(Me.lblEmailCliente)
        Me.Panel_Cliente.Controls.Add(Me.txtEmailCliente)
        Me.Panel_Cliente.Controls.Add(Me.chkRfcCompleto)
        Me.Panel_Cliente.Controls.Add(Me.Label17)
        Me.Panel_Cliente.Controls.Add(Me.txtMunicipio)
        Me.Panel_Cliente.Controls.Add(Me.txtNumInterior)
        Me.Panel_Cliente.Controls.Add(Me.txtNumExterior)
        Me.Panel_Cliente.Controls.Add(Me.lblFax)
        Me.Panel_Cliente.Controls.Add(Me.txtFax)
        Me.Panel_Cliente.Controls.Add(Me.lblCurp)
        Me.Panel_Cliente.Controls.Add(Me.txtCurp)
        Me.Panel_Cliente.Controls.Add(Me.lblRfc)
        Me.Panel_Cliente.Controls.Add(Me.txtRfc)
        Me.Panel_Cliente.Controls.Add(Me.lblDomicilio)
        Me.Panel_Cliente.Controls.Add(Me.txtDomicilio)
        Me.Panel_Cliente.Controls.Add(Me.lblColonia)
        Me.Panel_Cliente.Controls.Add(Me.txtColonia)
        Me.Panel_Cliente.Controls.Add(Me.lblCiudad)
        Me.Panel_Cliente.Controls.Add(Me.txtCiudad)
        Me.Panel_Cliente.Controls.Add(Me.lblCp)
        Me.Panel_Cliente.Controls.Add(Me.clcCp)
        Me.Panel_Cliente.Controls.Add(Me.lblEstado)
        Me.Panel_Cliente.Controls.Add(Me.txtEstado)
        Me.Panel_Cliente.Controls.Add(Me.lblTelefono1)
        Me.Panel_Cliente.Controls.Add(Me.txtTelefono1)
        Me.Panel_Cliente.Controls.Add(Me.lblTelefono2)
        Me.Panel_Cliente.Controls.Add(Me.txtTelefono2)
        Me.Panel_Cliente.Location = New System.Drawing.Point(8, 64)
        Me.Panel_Cliente.Name = "Panel_Cliente"
        Me.Panel_Cliente.Size = New System.Drawing.Size(731, 360)
        Me.Panel_Cliente.TabIndex = 4
        '
        'lblEmailCliente
        '
        Me.lblEmailCliente.AutoSize = True
        Me.lblEmailCliente.Location = New System.Drawing.Point(96, 280)
        Me.lblEmailCliente.Name = "lblEmailCliente"
        Me.lblEmailCliente.Size = New System.Drawing.Size(46, 16)
        Me.lblEmailCliente.TabIndex = 51
        Me.lblEmailCliente.Tag = ""
        Me.lblEmailCliente.Text = "Correo:"
        Me.lblEmailCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEmailCliente
        '
        Me.txtEmailCliente.EditValue = ""
        Me.txtEmailCliente.Location = New System.Drawing.Point(152, 280)
        Me.txtEmailCliente.Name = "txtEmailCliente"
        '
        'txtEmailCliente.Properties
        '
        Me.txtEmailCliente.Properties.Enabled = False
        Me.txtEmailCliente.Properties.MaxLength = 20
        Me.txtEmailCliente.Size = New System.Drawing.Size(304, 20)
        Me.txtEmailCliente.TabIndex = 52
        Me.txtEmailCliente.Tag = "correo"
        '
        'chkRfcCompleto
        '
        Me.chkRfcCompleto.EditValue = "False"
        Me.chkRfcCompleto.Location = New System.Drawing.Point(288, 256)
        Me.chkRfcCompleto.Name = "chkRfcCompleto"
        '
        'chkRfcCompleto.Properties
        '
        Me.chkRfcCompleto.Properties.Caption = "RFC Completo"
        Me.chkRfcCompleto.Properties.Enabled = False
        Me.chkRfcCompleto.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkRfcCompleto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkRfcCompleto.Size = New System.Drawing.Size(104, 19)
        Me.chkRfcCompleto.TabIndex = 50
        Me.chkRfcCompleto.Tag = ""
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(82, 88)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(60, 16)
        Me.Label17.TabIndex = 49
        Me.Label17.Tag = ""
        Me.Label17.Text = "Municipio:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMunicipio
        '
        Me.txtMunicipio.EditValue = ""
        Me.txtMunicipio.Location = New System.Drawing.Point(152, 88)
        Me.txtMunicipio.Name = "txtMunicipio"
        '
        'txtMunicipio.Properties
        '
        Me.txtMunicipio.Properties.Enabled = False
        Me.txtMunicipio.Properties.MaxLength = 50
        Me.txtMunicipio.Size = New System.Drawing.Size(300, 20)
        Me.txtMunicipio.TabIndex = 48
        Me.txtMunicipio.Tag = "municipio"
        '
        'txtNumInterior
        '
        Me.txtNumInterior.EditValue = ""
        Me.txtNumInterior.Location = New System.Drawing.Point(112, 328)
        Me.txtNumInterior.Name = "txtNumInterior"
        '
        'txtNumInterior.Properties
        '
        Me.txtNumInterior.Properties.Enabled = False
        Me.txtNumInterior.Properties.MaxLength = 10
        Me.txtNumInterior.Size = New System.Drawing.Size(40, 20)
        Me.txtNumInterior.TabIndex = 47
        Me.txtNumInterior.Tag = ""
        Me.txtNumInterior.Visible = False
        '
        'txtNumExterior
        '
        Me.txtNumExterior.EditValue = ""
        Me.txtNumExterior.Location = New System.Drawing.Point(64, 328)
        Me.txtNumExterior.Name = "txtNumExterior"
        '
        'txtNumExterior.Properties
        '
        Me.txtNumExterior.Properties.Enabled = False
        Me.txtNumExterior.Properties.MaxLength = 10
        Me.txtNumExterior.Size = New System.Drawing.Size(40, 20)
        Me.txtNumExterior.TabIndex = 46
        Me.txtNumExterior.Tag = ""
        Me.txtNumExterior.Visible = False
        '
        'lblFax
        '
        Me.lblFax.AutoSize = True
        Me.lblFax.Location = New System.Drawing.Point(114, 208)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(28, 16)
        Me.lblFax.TabIndex = 40
        Me.lblFax.Tag = ""
        Me.lblFax.Text = "&Fax:"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFax
        '
        Me.txtFax.EditValue = ""
        Me.txtFax.Location = New System.Drawing.Point(152, 208)
        Me.txtFax.Name = "txtFax"
        '
        'txtFax.Properties
        '
        Me.txtFax.Properties.Enabled = False
        Me.txtFax.Properties.MaxLength = 13
        Me.txtFax.Size = New System.Drawing.Size(88, 20)
        Me.txtFax.TabIndex = 41
        Me.txtFax.Tag = "fax"
        '
        'lblCurp
        '
        Me.lblCurp.AutoSize = True
        Me.lblCurp.Location = New System.Drawing.Point(107, 232)
        Me.lblCurp.Name = "lblCurp"
        Me.lblCurp.Size = New System.Drawing.Size(35, 16)
        Me.lblCurp.TabIndex = 42
        Me.lblCurp.Tag = ""
        Me.lblCurp.Text = "C&urp:"
        Me.lblCurp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCurp
        '
        Me.txtCurp.EditValue = ""
        Me.txtCurp.Location = New System.Drawing.Point(152, 232)
        Me.txtCurp.Name = "txtCurp"
        '
        'txtCurp.Properties
        '
        Me.txtCurp.Properties.Enabled = False
        Me.txtCurp.Properties.MaxLength = 20
        Me.txtCurp.Size = New System.Drawing.Size(120, 20)
        Me.txtCurp.TabIndex = 43
        Me.txtCurp.Tag = "curp"
        '
        'lblRfc
        '
        Me.lblRfc.AutoSize = True
        Me.lblRfc.Location = New System.Drawing.Point(111, 256)
        Me.lblRfc.Name = "lblRfc"
        Me.lblRfc.Size = New System.Drawing.Size(31, 16)
        Me.lblRfc.TabIndex = 44
        Me.lblRfc.Tag = ""
        Me.lblRfc.Text = "&RFC:"
        Me.lblRfc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRfc
        '
        Me.txtRfc.EditValue = ""
        Me.txtRfc.Location = New System.Drawing.Point(152, 256)
        Me.txtRfc.Name = "txtRfc"
        '
        'txtRfc.Properties
        '
        Me.txtRfc.Properties.Enabled = False
        Me.txtRfc.Properties.MaxLength = 20
        Me.txtRfc.Size = New System.Drawing.Size(120, 20)
        Me.txtRfc.TabIndex = 45
        Me.txtRfc.Tag = "rfc"
        '
        'lblDomicilio
        '
        Me.lblDomicilio.AutoSize = True
        Me.lblDomicilio.Location = New System.Drawing.Point(83, 16)
        Me.lblDomicilio.Name = "lblDomicilio"
        Me.lblDomicilio.Size = New System.Drawing.Size(59, 16)
        Me.lblDomicilio.TabIndex = 26
        Me.lblDomicilio.Tag = ""
        Me.lblDomicilio.Text = "&Domicilio:"
        Me.lblDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(152, 16)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.Enabled = False
        Me.txtDomicilio.Properties.MaxLength = 80
        Me.txtDomicilio.Size = New System.Drawing.Size(488, 20)
        Me.txtDomicilio.TabIndex = 27
        Me.txtDomicilio.Tag = "domicilio"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(92, 40)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 28
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "&Colonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColonia
        '
        Me.txtColonia.EditValue = ""
        Me.txtColonia.Location = New System.Drawing.Point(152, 40)
        Me.txtColonia.Name = "txtColonia"
        '
        'txtColonia.Properties
        '
        Me.txtColonia.Properties.Enabled = False
        Me.txtColonia.Properties.MaxLength = 50
        Me.txtColonia.Size = New System.Drawing.Size(300, 20)
        Me.txtColonia.TabIndex = 29
        Me.txtColonia.Tag = "colonia"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(95, 64)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(47, 16)
        Me.lblCiudad.TabIndex = 30
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "C&iudad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudad
        '
        Me.txtCiudad.EditValue = ""
        Me.txtCiudad.Location = New System.Drawing.Point(152, 64)
        Me.txtCiudad.Name = "txtCiudad"
        '
        'txtCiudad.Properties
        '
        Me.txtCiudad.Properties.Enabled = False
        Me.txtCiudad.Properties.MaxLength = 50
        Me.txtCiudad.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudad.TabIndex = 31
        Me.txtCiudad.Tag = "ciudad"
        '
        'lblCp
        '
        Me.lblCp.AutoSize = True
        Me.lblCp.Location = New System.Drawing.Point(118, 112)
        Me.lblCp.Name = "lblCp"
        Me.lblCp.Size = New System.Drawing.Size(24, 16)
        Me.lblCp.TabIndex = 32
        Me.lblCp.Tag = ""
        Me.lblCp.Text = "C&p:"
        Me.lblCp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCp
        '
        Me.clcCp.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCp.Location = New System.Drawing.Point(152, 112)
        Me.clcCp.MaxValue = 0
        Me.clcCp.MinValue = 0
        Me.clcCp.Name = "clcCp"
        '
        'clcCp.Properties
        '
        Me.clcCp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.Enabled = False
        Me.clcCp.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCp.Properties.MaxLength = 5
        Me.clcCp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCp.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcCp.Size = New System.Drawing.Size(40, 19)
        Me.clcCp.TabIndex = 33
        Me.clcCp.Tag = "cp"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(96, 136)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 34
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "&Estado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstado
        '
        Me.txtEstado.EditValue = ""
        Me.txtEstado.Location = New System.Drawing.Point(152, 136)
        Me.txtEstado.Name = "txtEstado"
        '
        'txtEstado.Properties
        '
        Me.txtEstado.Properties.Enabled = False
        Me.txtEstado.Properties.MaxLength = 50
        Me.txtEstado.Size = New System.Drawing.Size(300, 20)
        Me.txtEstado.TabIndex = 35
        Me.txtEstado.Tag = "estado"
        '
        'lblTelefono1
        '
        Me.lblTelefono1.AutoSize = True
        Me.lblTelefono1.Location = New System.Drawing.Point(75, 160)
        Me.lblTelefono1.Name = "lblTelefono1"
        Me.lblTelefono1.Size = New System.Drawing.Size(67, 16)
        Me.lblTelefono1.TabIndex = 36
        Me.lblTelefono1.Tag = ""
        Me.lblTelefono1.Text = "&Tel�fono 1:"
        Me.lblTelefono1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono1
        '
        Me.txtTelefono1.EditValue = ""
        Me.txtTelefono1.Location = New System.Drawing.Point(152, 160)
        Me.txtTelefono1.Name = "txtTelefono1"
        '
        'txtTelefono1.Properties
        '
        Me.txtTelefono1.Properties.Enabled = False
        Me.txtTelefono1.Properties.MaxLength = 13
        Me.txtTelefono1.Size = New System.Drawing.Size(88, 20)
        Me.txtTelefono1.TabIndex = 37
        Me.txtTelefono1.Tag = "telefono1"
        '
        'lblTelefono2
        '
        Me.lblTelefono2.AutoSize = True
        Me.lblTelefono2.Location = New System.Drawing.Point(75, 184)
        Me.lblTelefono2.Name = "lblTelefono2"
        Me.lblTelefono2.Size = New System.Drawing.Size(67, 16)
        Me.lblTelefono2.TabIndex = 38
        Me.lblTelefono2.Tag = ""
        Me.lblTelefono2.Text = "T&el�fono 2:"
        Me.lblTelefono2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono2
        '
        Me.txtTelefono2.EditValue = ""
        Me.txtTelefono2.Location = New System.Drawing.Point(152, 184)
        Me.txtTelefono2.Name = "txtTelefono2"
        '
        'txtTelefono2.Properties
        '
        Me.txtTelefono2.Properties.Enabled = False
        Me.txtTelefono2.Properties.MaxLength = 13
        Me.txtTelefono2.Size = New System.Drawing.Size(88, 20)
        Me.txtTelefono2.TabIndex = 39
        Me.txtTelefono2.Tag = "telefono2"
        '
        'Panel_Aduana
        '
        Me.Panel_Aduana.Controls.Add(Me.lblPedimento)
        Me.Panel_Aduana.Controls.Add(Me.txtPedimento)
        Me.Panel_Aduana.Controls.Add(Me.lblAduana)
        Me.Panel_Aduana.Controls.Add(Me.txtAduana)
        Me.Panel_Aduana.Controls.Add(Me.lblFecha_Importacion)
        Me.Panel_Aduana.Controls.Add(Me.dteFecha_Importacion)
        Me.Panel_Aduana.Location = New System.Drawing.Point(8, 64)
        Me.Panel_Aduana.Name = "Panel_Aduana"
        Me.Panel_Aduana.Size = New System.Drawing.Size(731, 360)
        Me.Panel_Aduana.TabIndex = 5
        '
        'lblPedimento
        '
        Me.lblPedimento.AutoSize = True
        Me.lblPedimento.Location = New System.Drawing.Point(80, 40)
        Me.lblPedimento.Name = "lblPedimento"
        Me.lblPedimento.Size = New System.Drawing.Size(67, 16)
        Me.lblPedimento.TabIndex = 8
        Me.lblPedimento.Tag = ""
        Me.lblPedimento.Text = "&Pedimento:"
        Me.lblPedimento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPedimento
        '
        Me.txtPedimento.EditValue = ""
        Me.txtPedimento.Location = New System.Drawing.Point(152, 40)
        Me.txtPedimento.Name = "txtPedimento"
        '
        'txtPedimento.Properties
        '
        Me.txtPedimento.Properties.Enabled = False
        Me.txtPedimento.Properties.MaxLength = 30
        Me.txtPedimento.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.txtPedimento.Size = New System.Drawing.Size(180, 20)
        Me.txtPedimento.TabIndex = 9
        Me.txtPedimento.Tag = "pedimento"
        '
        'lblAduana
        '
        Me.lblAduana.AutoSize = True
        Me.lblAduana.Location = New System.Drawing.Point(96, 16)
        Me.lblAduana.Name = "lblAduana"
        Me.lblAduana.Size = New System.Drawing.Size(51, 16)
        Me.lblAduana.TabIndex = 6
        Me.lblAduana.Tag = ""
        Me.lblAduana.Text = "&Aduana:"
        Me.lblAduana.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAduana
        '
        Me.txtAduana.EditValue = ""
        Me.txtAduana.Location = New System.Drawing.Point(152, 16)
        Me.txtAduana.Name = "txtAduana"
        '
        'txtAduana.Properties
        '
        Me.txtAduana.Properties.Enabled = False
        Me.txtAduana.Properties.MaxLength = 50
        Me.txtAduana.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.txtAduana.Size = New System.Drawing.Size(300, 20)
        Me.txtAduana.TabIndex = 7
        Me.txtAduana.Tag = "aduana"
        '
        'lblFecha_Importacion
        '
        Me.lblFecha_Importacion.AutoSize = True
        Me.lblFecha_Importacion.Location = New System.Drawing.Point(20, 64)
        Me.lblFecha_Importacion.Name = "lblFecha_Importacion"
        Me.lblFecha_Importacion.Size = New System.Drawing.Size(127, 16)
        Me.lblFecha_Importacion.TabIndex = 10
        Me.lblFecha_Importacion.Tag = ""
        Me.lblFecha_Importacion.Text = "&Fecha de importaci�n:"
        Me.lblFecha_Importacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Importacion
        '
        Me.dteFecha_Importacion.EditValue = "10/04/2006"
        Me.dteFecha_Importacion.Location = New System.Drawing.Point(152, 64)
        Me.dteFecha_Importacion.Name = "dteFecha_Importacion"
        '
        'dteFecha_Importacion.Properties
        '
        Me.dteFecha_Importacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Importacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Importacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Importacion.Properties.Enabled = False
        Me.dteFecha_Importacion.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFecha_Importacion.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha_Importacion.TabIndex = 11
        Me.dteFecha_Importacion.Tag = "fecha_importacion"
        '
        'Panel_Cancelacion
        '
        Me.Panel_Cancelacion.Controls.Add(Me.Label16)
        Me.Panel_Cancelacion.Controls.Add(Me.cboTipoCancelacion)
        Me.Panel_Cancelacion.Controls.Add(Me.txtUsuarioCancelacion)
        Me.Panel_Cancelacion.Controls.Add(Me.Label15)
        Me.Panel_Cancelacion.Controls.Add(Me.dteFechaCancelacion)
        Me.Panel_Cancelacion.Controls.Add(Me.Label14)
        Me.Panel_Cancelacion.Controls.Add(Me.txtObservacionesCancelacion)
        Me.Panel_Cancelacion.Controls.Add(Me.Label13)
        Me.Panel_Cancelacion.Location = New System.Drawing.Point(8, 64)
        Me.Panel_Cancelacion.Name = "Panel_Cancelacion"
        Me.Panel_Cancelacion.Size = New System.Drawing.Size(731, 360)
        Me.Panel_Cancelacion.TabIndex = 6
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(32, 64)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(117, 16)
        Me.Label16.TabIndex = 12
        Me.Label16.Tag = ""
        Me.Label16.Text = "&Tipo de cancelaci�n:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoCancelacion
        '
        Me.cboTipoCancelacion.Location = New System.Drawing.Point(152, 64)
        Me.cboTipoCancelacion.Name = "cboTipoCancelacion"
        '
        'cboTipoCancelacion.Properties
        '
        Me.cboTipoCancelacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoCancelacion.Properties.Enabled = False
        Me.cboTipoCancelacion.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Eliminada", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelada", "C", -1)})
        Me.cboTipoCancelacion.Size = New System.Drawing.Size(80, 23)
        Me.cboTipoCancelacion.TabIndex = 13
        Me.cboTipoCancelacion.Tag = "tipo_cancelacion"
        '
        'txtUsuarioCancelacion
        '
        Me.txtUsuarioCancelacion.EditValue = ""
        Me.txtUsuarioCancelacion.Location = New System.Drawing.Point(152, 40)
        Me.txtUsuarioCancelacion.Name = "txtUsuarioCancelacion"
        '
        'txtUsuarioCancelacion.Properties
        '
        Me.txtUsuarioCancelacion.Properties.Enabled = False
        Me.txtUsuarioCancelacion.Properties.MaxLength = 15
        Me.txtUsuarioCancelacion.Size = New System.Drawing.Size(176, 20)
        Me.txtUsuarioCancelacion.TabIndex = 11
        Me.txtUsuarioCancelacion.Tag = "usuario_cancelacion"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(24, 40)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(120, 16)
        Me.Label15.TabIndex = 10
        Me.Label15.Tag = ""
        Me.Label15.Text = "&Usuario que cancel�:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaCancelacion
        '
        Me.dteFechaCancelacion.EditValue = "10/04/2006"
        Me.dteFechaCancelacion.Location = New System.Drawing.Point(152, 16)
        Me.dteFechaCancelacion.Name = "dteFechaCancelacion"
        '
        'dteFechaCancelacion.Properties
        '
        Me.dteFechaCancelacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCancelacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCancelacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaCancelacion.Properties.Enabled = False
        Me.dteFechaCancelacion.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFechaCancelacion.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaCancelacion.TabIndex = 9
        Me.dteFechaCancelacion.Tag = "fecha_cancelacion"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(16, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(126, 16)
        Me.Label14.TabIndex = 8
        Me.Label14.Tag = ""
        Me.Label14.Text = "F&echa de cancelaci�n:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservacionesCancelacion
        '
        Me.txtObservacionesCancelacion.EditValue = ""
        Me.txtObservacionesCancelacion.Location = New System.Drawing.Point(152, 88)
        Me.txtObservacionesCancelacion.Name = "txtObservacionesCancelacion"
        '
        'txtObservacionesCancelacion.Properties
        '
        Me.txtObservacionesCancelacion.Properties.Enabled = False
        Me.txtObservacionesCancelacion.Size = New System.Drawing.Size(392, 88)
        Me.txtObservacionesCancelacion.TabIndex = 15
        Me.txtObservacionesCancelacion.Tag = "observaciones_cancelacion"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(56, 88)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(89, 16)
        Me.Label13.TabIndex = 14
        Me.Label13.Tag = ""
        Me.Label13.Text = "&Observaciones:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnGenerales
        '
        Me.btnGenerales.Location = New System.Drawing.Point(8, 9)
        Me.btnGenerales.Name = "btnGenerales"
        Me.btnGenerales.Size = New System.Drawing.Size(89, 23)
        Me.btnGenerales.TabIndex = 0
        Me.btnGenerales.Text = " Generales"
        '
        'btnCredito
        '
        Me.btnCredito.Location = New System.Drawing.Point(97, 9)
        Me.btnCredito.Name = "btnCredito"
        Me.btnCredito.Size = New System.Drawing.Size(89, 23)
        Me.btnCredito.TabIndex = 1
        Me.btnCredito.Text = "Datos Cr�dito"
        '
        'btnReparto
        '
        Me.btnReparto.Location = New System.Drawing.Point(186, 9)
        Me.btnReparto.Name = "btnReparto"
        Me.btnReparto.Size = New System.Drawing.Size(89, 23)
        Me.btnReparto.TabIndex = 2
        Me.btnReparto.Text = "Reparto"
        '
        'btnCliente
        '
        Me.btnCliente.Location = New System.Drawing.Point(275, 9)
        Me.btnCliente.Name = "btnCliente"
        Me.btnCliente.Size = New System.Drawing.Size(89, 23)
        Me.btnCliente.TabIndex = 3
        Me.btnCliente.Text = "Cliente"
        '
        'btnAduana
        '
        Me.btnAduana.Location = New System.Drawing.Point(364, 9)
        Me.btnAduana.Name = "btnAduana"
        Me.btnAduana.Size = New System.Drawing.Size(89, 23)
        Me.btnAduana.TabIndex = 4
        Me.btnAduana.Text = "Aduana"
        '
        'btnCancelacion
        '
        Me.btnCancelacion.Location = New System.Drawing.Point(453, 9)
        Me.btnCancelacion.Name = "btnCancelacion"
        Me.btnCancelacion.Size = New System.Drawing.Size(89, 23)
        Me.btnCancelacion.TabIndex = 5
        Me.btnCancelacion.Text = "Cancelacion"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnAnticipos)
        Me.GroupBox3.Controls.Add(Me.btnCredito)
        Me.GroupBox3.Controls.Add(Me.btnGenerales)
        Me.GroupBox3.Controls.Add(Me.btnAduana)
        Me.GroupBox3.Controls.Add(Me.btnCliente)
        Me.GroupBox3.Controls.Add(Me.btnCancelacion)
        Me.GroupBox3.Controls.Add(Me.btnReparto)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 32)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(731, 32)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        '
        'btnAnticipos
        '
        Me.btnAnticipos.Location = New System.Drawing.Point(542, 9)
        Me.btnAnticipos.Name = "btnAnticipos"
        Me.btnAnticipos.Size = New System.Drawing.Size(89, 23)
        Me.btnAnticipos.TabIndex = 6
        Me.btnAnticipos.Text = "Anticipos"
        '
        'Panel_Anticipos
        '
        Me.Panel_Anticipos.Controls.Add(Me.txtConcepto)
        Me.Panel_Anticipos.Controls.Add(Me.lblConcepto)
        Me.Panel_Anticipos.Controls.Add(Me.grAnticipos)
        Me.Panel_Anticipos.Controls.Add(Me.tmaAnticipos)
        Me.Panel_Anticipos.Location = New System.Drawing.Point(8, 64)
        Me.Panel_Anticipos.Name = "Panel_Anticipos"
        Me.Panel_Anticipos.Size = New System.Drawing.Size(731, 360)
        Me.Panel_Anticipos.TabIndex = 59
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(88, 16)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(632, 56)
        Me.txtConcepto.TabIndex = 65
        Me.txtConcepto.Tag = ""
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(16, 16)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 64
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "Concepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grAnticipos
        '
        '
        'grAnticipos.EmbeddedNavigator
        '
        Me.grAnticipos.EmbeddedNavigator.Name = ""
        Me.grAnticipos.Location = New System.Drawing.Point(8, 107)
        Me.grAnticipos.MainView = Me.grvAnticipos
        Me.grAnticipos.Name = "grAnticipos"
        Me.grAnticipos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1, Me.RepositoryItemCheckEdit2, Me.RepositoryItemCheckEdit3, Me.repClcImporte})
        Me.grAnticipos.Size = New System.Drawing.Size(712, 248)
        Me.grAnticipos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grAnticipos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grAnticipos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grAnticipos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grAnticipos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grAnticipos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grAnticipos.TabIndex = 63
        Me.grAnticipos.TabStop = False
        Me.grAnticipos.Text = "Ventas"
        '
        'grvAnticipos
        '
        Me.grvAnticipos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcAnticipo, Me.grcConceptoAnticipo, Me.grcImporteAnticipo})
        Me.grvAnticipos.GridControl = Me.grAnticipos
        Me.grvAnticipos.Name = "grvAnticipos"
        Me.grvAnticipos.OptionsCustomization.AllowFilter = False
        Me.grvAnticipos.OptionsCustomization.AllowGroup = False
        Me.grvAnticipos.OptionsCustomization.AllowSort = False
        Me.grvAnticipos.OptionsView.ShowFooter = True
        Me.grvAnticipos.OptionsView.ShowGroupPanel = False
        '
        'grcAnticipo
        '
        Me.grcAnticipo.Caption = "Anticipo"
        Me.grcAnticipo.FieldName = "folio"
        Me.grcAnticipo.Name = "grcAnticipo"
        Me.grcAnticipo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcAnticipo.VisibleIndex = 0
        Me.grcAnticipo.Width = 72
        '
        'grcConceptoAnticipo
        '
        Me.grcConceptoAnticipo.Caption = "Concepto"
        Me.grcConceptoAnticipo.FieldName = "concepto"
        Me.grcConceptoAnticipo.Name = "grcConceptoAnticipo"
        Me.grcConceptoAnticipo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConceptoAnticipo.VisibleIndex = 1
        Me.grcConceptoAnticipo.Width = 440
        '
        'grcImporteAnticipo
        '
        Me.grcImporteAnticipo.Caption = "Importe"
        Me.grcImporteAnticipo.ColumnEdit = Me.repClcImporte
        Me.grcImporteAnticipo.FieldName = "importe"
        Me.grcImporteAnticipo.Name = "grcImporteAnticipo"
        Me.grcImporteAnticipo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteAnticipo.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporteAnticipo.VisibleIndex = 2
        Me.grcImporteAnticipo.Width = 186
        '
        'repClcImporte
        '
        Me.repClcImporte.AutoHeight = False
        Me.repClcImporte.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.repClcImporte.DisplayFormat.FormatString = "c2"
        Me.repClcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.repClcImporte.EditFormat.FormatString = "2"
        Me.repClcImporte.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.repClcImporte.Name = "repClcImporte"
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        Me.RepositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'RepositoryItemCheckEdit2
        '
        Me.RepositoryItemCheckEdit2.AutoHeight = False
        Me.RepositoryItemCheckEdit2.Name = "RepositoryItemCheckEdit2"
        Me.RepositoryItemCheckEdit2.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'RepositoryItemCheckEdit3
        '
        Me.RepositoryItemCheckEdit3.AutoHeight = False
        Me.RepositoryItemCheckEdit3.Name = "RepositoryItemCheckEdit3"
        Me.RepositoryItemCheckEdit3.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'tmaAnticipos
        '
        Me.tmaAnticipos.BackColor = System.Drawing.Color.White
        Me.tmaAnticipos.CanDelete = True
        Me.tmaAnticipos.CanInsert = True
        Me.tmaAnticipos.CanUpdate = False
        Me.tmaAnticipos.Grid = Me.grAnticipos
        Me.tmaAnticipos.Location = New System.Drawing.Point(8, 84)
        Me.tmaAnticipos.Name = "tmaAnticipos"
        Me.tmaAnticipos.Size = New System.Drawing.Size(712, 23)
        Me.tmaAnticipos.TabIndex = 62
        Me.tmaAnticipos.TabStop = False
        Me.tmaAnticipos.Title = "Anticipos"
        Me.tmaAnticipos.UpdateTitle = "un anticipo"
        '
        'frmVentas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.CanDock = False
        Me.ClientSize = New System.Drawing.Size(746, 424)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Panel_Generales)
        Me.Controls.Add(Me.panel_Creditos)
        Me.Controls.Add(Me.Panel_Repartos)
        Me.Controls.Add(Me.Panel_Aduana)
        Me.Controls.Add(Me.Panel_Cliente)
        Me.Controls.Add(Me.Panel_Cancelacion)
        Me.Controls.Add(Me.Panel_Anticipos)
        Me.Name = "frmVentas"
        Me.Text = "frmVentas"
        Me.Controls.SetChildIndex(Me.Panel_Anticipos, 0)
        Me.Controls.SetChildIndex(Me.Panel_Cancelacion, 0)
        Me.Controls.SetChildIndex(Me.Panel_Cliente, 0)
        Me.Controls.SetChildIndex(Me.Panel_Aduana, 0)
        Me.Controls.SetChildIndex(Me.Panel_Repartos, 0)
        Me.Controls.SetChildIndex(Me.panel_Creditos, 0)
        Me.Controls.SetChildIndex(Me.Panel_Generales, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Panel_Generales.ResumeLayout(False)
        CType(Me.chkApartados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotalAnticipos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFacturacionEspecial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkReFacturado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFactura.ResumeLayout(False)
        CType(Me.clcSubtotalContado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMenosSoloDespliegue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioPedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaPedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservacionesVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotalArticulos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotalMinimo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImpuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIvadesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSubtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panel_Creditos.ResumeLayout(False)
        CType(Me.chkaplica_enganche.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImportePlan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcEnganche2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEnganche_en_Menos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMenos_Enganch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLiquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIvadesglosado1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotalCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.clcMonto_Liquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEnganche_En_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dteFecha_Enganche_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte_Enganche_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Primer_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcNumero_Documentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte_Documentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte_Ultimo_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMenos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcEnganche1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel_Repartos.ResumeLayout(False)
        CType(Me.cboHorario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservacionesReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilioReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColoniaReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudadReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstadoReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefonoReparto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel_Cliente.ResumeLayout(False)
        CType(Me.txtEmailCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRfcCompleto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumInterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumExterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRfc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel_Aduana.ResumeLayout(False)
        CType(Me.txtPedimento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAduana.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Importacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel_Cancelacion.ResumeLayout(False)
        CType(Me.cboTipoCancelacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsuarioCancelacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaCancelacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservacionesCancelacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel_Anticipos.ResumeLayout(False)
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grAnticipos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvAnticipos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repClcImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones de Variables"

    Dim KS As Keys

    Public banCerrar As Boolean
    Public banCerrarVenta As Boolean = False
    Public bVentaGuardada As Boolean = False

    Public AfectaCostos As Boolean = False
    Public lSucursal As Long = -1
    Public sSerie As String = ""
    Public lFolio As String = -1
    Public sucursal_dependencia As Boolean = False


    Private folio_movimiento As Long = 0
    Private folio_movimiento_detalle As Long = 0
    Public Guardado As Boolean = False


    Private arrBodegas As New ArrayList
    Private arrFoliosMovimientos As New ArrayList
    Private banRepartoValido As Boolean = False

    Private banConceptoCancelacionVenta As Boolean = False
    Private banConceptoVenta As Boolean = False
    Private banConceptoFactura As Boolean = False
    Private banConceptoFacturaCredito As Boolean = False
    Private banConceptoVistaEntrada As Boolean = False
    Private banConceptoNCR As Boolean = False
    Private banConceptoVistaSalida As Boolean = False

    Public ConceptoVenta As String
    Private ConceptoFactura As String
    Private ConceptoFacturaCredito As String
    Private ConceptoNCR As String

    Public ConceptoVistaEntrada As String
    Public ConceptoCancelacionVenta As String
    Public ConceptoVistaSalida As String


    Private sucursal_venta As Long
    Private serie_venta As String
    Private dsDocumentosCobrar As DataSet
    Private banCancelada As Boolean = False
    Private tipo_precio_venta As Long
    Private folio_pedido As Long = -1
    'Private Cadena_Error_Cancelacion As String = ""
    'Private Cadena_Error_Entrada_Vista As String = ""
    Private FechaCotizacion As DateTime
    Private persona As String
    Private folio_plantilla As Integer

    'DAM 27-02-07 Variable para saber cual fue el ultimo articulo insertado 
    ' 0 Identificable
    ' 1 No Identificable
    ' -1 Vacio
    Private lCategoriasIdentificables As Long = -1

    'Dam 21/09/06 Variable para el limite de credito del cliente
    Public dLimiteCredito As Double = 0

    'DAM 16/ABR/07 Variable para obtener el factor de enajenacion de la factura
    Private dFactor_Enajenacion As Double = 0

    ''DAM - Sirve Para Saber Por que opcion de exe Entro
    'Private bEntroPuntoVenta As Boolean

    'DAM - Sucursal_vista_salida
    Public Sucursal_vista_salida As Long = 0


    '' Verifica si tiene precio expo
    Public Tiene_Precio_Expo As Boolean = False
    Public Precio_Expo As Long

    Private TotalCantidades As Long

    Private factura_electronica As Boolean = False

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oVentas As VillarrealBusiness.clsVentas
    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle
    Private oVendedores As VillarrealBusiness.clsVendedores
    Private oClientes As VillarrealBusiness.clsClientes
    Private oClientesCobradores As VillarrealBusiness.clsClientesCobradores
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oVariables As VillarrealBusiness.clsVariables
    Private oMovimientosInventario As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventarioDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oArticulosExistencias As VillarrealBusiness.clsArticulosExistencias
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oPlanesCredito As VillarrealBusiness.clsPlanesCredito
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario
    Private oHistoricoCostos As VillarrealBusiness.clsHisCostos
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oCotizaciones As VillarrealBusiness.clsCotizaciones
    Private oAutorizaciones As VillarrealBusiness.clsAutorizacionesVentasPedidoFabrica
    Private oVistasSalidas As VillarrealBusiness.clsVistasSalidas
    Private oVistasSalidasDetalle As VillarrealBusiness.clsVistasSalidasDetalle
    Private oVistasEntradas As VillarrealBusiness.clsVistasEntradas
    Private oVistasEntradasDetalle As VillarrealBusiness.clsVistasEntradasDetalle
    Private oMovimientosInventarioDetalleSeries As VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oDescuentosEspecialesProgramados As New VillarrealBusiness.clsDescuentosEspecialesProgramados
    Private oDescuentosEspecialesClientes As New VillarrealBusiness.clsDescuentosEspecialesClientes
    Private oPaquetes As New VillarrealBusiness.clsPaquetes
    Private oAnticiposDetalle As New VillarrealBusiness.clsAnticiposDetalle



    Public banImprimirFac As Boolean = False
    Private lCobrador As Long = 0
    Private DiferenciaPrecios As Long = 0
    Private PrecioRegalo As Long = 0
    Private Menos As Long = 0

    Private lDiasVigenciaCotizacion As Long = 0
    Private lArticuloAnticipos As Long = 0
    Private ImpuestoVariables As Double = 0

    Public ReadOnly Property Cliente() As Long
        Get
            Return PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Public ReadOnly Property Vendedor() As Long
        Get
            Return PreparaValorLookup(Me.lkpVendedor)
        End Get
    End Property
    Private Property Cobrador() As Long
        Get
            Return lCobrador
        End Get
        Set(ByVal Value As Long)
            lCobrador = Value
        End Set
    End Property
    Private Property PrecioArticulosRegalo() As Long
        Get
            Return PrecioRegalo
        End Get
        Set(ByVal Value As Long)
            PrecioRegalo = Value
        End Set
    End Property
    Private Property DiferenciaPreciosVentayContado() As Long
        Get
            Return DiferenciaPrecios
        End Get
        Set(ByVal Value As Long)
            DiferenciaPrecios = Value
        End Set
    End Property
    Private Property TotalMenos() As Long
        Get
            Return Menos
        End Get
        Set(ByVal Value As Long)
            Menos = Value
        End Set
    End Property
    Private ReadOnly Property Cotizacion() As Long
        Get
            Return PreparaValorLookup(Me.lkpCotizacion)
        End Get
    End Property
    Private ReadOnly Property DiasVigenciaCotizacion() As Long
        Get
            Return lDiasVigenciaCotizacion
            'Return oVariables.TraeDatos("dias_vigencia_cotizacion", VillarrealBusiness.clsVariables.tipo_dato.Entero)
        End Get
    End Property
    Private ReadOnly Property ArticuloAnticipos() As Long
        Get
            Return lArticuloAnticipos
            'Return oVariables.TraeDatos("articulo_anticipos", VillarrealBusiness.clsVariables.tipo_dato.Entero)
        End Get
    End Property
    Public Property CategoriasIdentificables() As Long
        Get
            Return lCategoriasIdentificables
        End Get
        Set(ByVal Value As Long)
            If Value = -1 Then
                If Me.tmaVentas.View.RowCount = 0 Then
                    lCategoriasIdentificables = -1
                End If
            Else
                lCategoriasIdentificables = Value
            End If


        End Set
    End Property
    ReadOnly Property FolioVistaSalida() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolioVistaSalida)
        End Get
    End Property
    Public ReadOnly Property FolioDescuento() As Long
        Get
            Return PreparaValorLookup(Me.lkpDescuentoCliente)
        End Get
    End Property
    Public ReadOnly Property Paquete() As Long
        Get
            Return PreparaValorLookup(Me.lkpPaquetes)
        End Get
    End Property



    Private ReadOnly Property sucursal_factura_cancelada() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal_fac_Cancelada)
        End Get
    End Property
    Private ReadOnly Property FolioVenta_cancelada() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolio_fac_cancelada)
        End Get
    End Property
    Private ReadOnly Property serie_factura_cancelada() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie_fac_cancelada)
        End Get
    End Property

    'Descuento Especial Cliente
    Private Monto_descuento_especial_cliente As Double = 0
    Private Plan_credito_descuento_especial_cliente As Long
    Private Porcentaje_Descuento_especial_cliente As Double = 0

    'Paquetes
    Private Plan_credito_Paquetes As Long = 0


#End Region

#Region "DIPROS Systems, Transacciones"

    Private Sub frmVentas_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub

    Private Sub frmVentas_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub

    Private Sub frmVentas_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()

        If Action = Actions.Insert And Me.cboTipoventa.Value = "D" Then
            'ImprimeFactura()
            banImprimirFac = True
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmVentas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If Me.cboTipoventa.Value = "D" Then 'Contado
            'Si es CONTADO - GUARDAR VENTA
            '                GENERO LA CXC PARA EL MISMO DIA
            '                GENERO ENCABEZADOS DE MOVIMIENTOS

            Me.lblFolio.Text = TraeFolioPuntoVenta(factura_electronica)

            RecalculaTotales()
            Select Case Action
                Case Actions.Insert

                    ' ==============================================================================
                    ' CODIGO PARA LA VENTA CON SALIDA A VISTA 
                    ' ==============================================================================

                    Dim folio_movimiento_detalle_consecutivo As Long
                    'SI ES VENTA DE VISTA:GENERA UN MOVIMIENTO DE ENTRADA POR VISTA PARA CADA ART�CULO
                    If Me.FolioVistaSalida > 0 Then
                        If Me.tmaVentas.Count > 0 Then
                            Dim oEvent As Events
                            Dim FolioVistaEntrada As Integer = 0
                            '/*csequera enero 2008
                            oEvent = oVistasEntradas.Insertar(FolioVistaEntrada, Me.FolioVistaSalida, Comunes.Common.Sucursal_Actual, Me.dteFecha.EditValue, Cliente, "VENTAS", TinApp.Connection.User, "Entrada Generada Por Punto Venta")
                            '*/csequera enero 2008

                            'oEvent = oVistasEntradas.Insertar(FolioVistaEntrada, Me.FolioVistaSalida, Comunes.Common.Sucursal_Actual, Me.dteFecha.EditValue, Cliente, "VENTAS", TinApp.Connection.User, "Entrada Generada Por Punto Venta")
                            If oEvent.ErrorFound Then
                                Response = oEvent
                            Else
                                With Me.tmaVentas
                                    .MoveFirst()
                                    Do While Not .EOF
                                        If .Item("partida_vistas") > 0 Then

                                            Select Case .CurrentAction
                                                Case Actions.Insert


                                                    '/*csequera enero 2008
                                                    Response = oMovimientosInventarioDetalle.ValidacantidadArticulos(.Item("partida_vistas"), .Item("articulo"), 1, .Item("bodega"), ConceptoVistaEntrada, folio_movimiento, "I", "Entrada Vistas")
                                                    '*/csequera enero 2008

                                                    If Not Response.ErrorFound Then
                                                        '    'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
                                                        GeneraMovimientoInventario(Action, Response, Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistaEntrada, folio_movimiento, Me.dteFecha.DateTime, Sucursal_vista_salida, ConceptoVistaSalida, Me.FolioVistaSalida, "Movimiento generado desde Entrada a Vistas por Punto de Venta")

                                                        '/*csequera enero 2008
                                                        If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistaEntrada, folio_movimiento, dteFecha.EditValue, .Item("partida_vistas"), .Item("articulo"), 1, .Item("costo"), 1 * .Item("costo"), -1, folio_movimiento_detalle_consecutivo)


                                                        If .Item("numero_serie") <> Nothing And Comunes.clsUtilerias.UsarSeries Then
                                                            If CType(.Item("numero_serie"), String).Trim.Length > 0 Then



                                                                ''AQUI SE CAMBIA LA SERIE DE BODEGA EN EL HIS_SERIES
                                                                'If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistaEntrada, FolioVistaEntrada, .Item("partida_vistas"), .Item("numero_serie"), .Item("articulo"))

                                                                '' SE MANDA LA SERIE A LA BODEGA DE ENTRADA
                                                                'If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.CambiarBodegaHisSeries(.Item("articulo"), .Item("numero_serie"), .Item("bodega"))


                                                                'AQUI SE CAMBIA LA SERIE DE BODEGA EN EL HIS_SERIES
                                                                '/*csequera enero 2008
                                                                If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistaEntrada, FolioVistaEntrada, .Item("partida_vistas"), .Item("numero_serie"), .Item("articulo"))
                                                                '*/csequera enero 2008
                                                                ' SE MANDA LA SERIE A LA BODEGA DE ENTRADA
                                                                '/*csequera enero 2008
                                                                If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.CambiarBodegaHisSeries(.Item("articulo"), .Item("numero_serie"), .Item("bodega"))
                                                                '*/csequera enero 2008

                                                            End If
                                                        End If
                                                        '/*csequera enero 2008
                                                        If Not Response.ErrorFound Then
                                                            Response = Me.oVistasEntradasDetalle.Insertar(FolioVistaEntrada, .Item("partida_vistas"), .Item("articulo"), .Item("numero_serie"), .Item("bodega"), Me.ConceptoVistaEntrada, folio_movimiento)
                                                            '*/csequera enero 2008
                                                        End If

                                                        '  If Not Response.ErrorFound Then Response = Me.oVistasEntradasDetalle.Insertar(FolioVistaEntrada, .Item("partida_vistas"), .Item("articulo"), .Item("numero_serie"), .Item("bodega"), Me.ConceptoVistaEntrada, folio_movimiento)
                                                    End If

                                            End Select
                                        End If
                                        .MoveNext()

                                    Loop
                                End With
                            End If
                        End If
                    End If


                    If Me.chkIvadesglosado.Checked = False Then txtRfc.Text = "XAXX010101000"


                    ' AQUI EMPIEZA EL CODIGO NORMAL DE LA VENTA 
                    '=================================================

                    'GUARDA LA VENTA
                    Response = oVentas.Insertar(Me.DataSource, Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, Me.dteFecha.DateTime, Comunes.Common.PuntoVenta_Actual, Me.clcTotalArticulos.EditValue, Menos, , , , , , , , , , , , , , , , , , , Me.FolioDescuento, , Me.Paquete, , Me.clcTotalAnticipos.EditValue, Me.chkApartados.Checked)

                    'GUARDA LA CXC
                    If Not Response.ErrorFound Then Response = oMovimientosCobrar.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFactura, Me.lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long), Cliente, 0, Me.dteFecha.DateTime.Date, -1, -1, Me.Cobrador, 1, Me.clcTotalArticulos.EditValue, 0, Me.clcTotalArticulos.EditValue - Me.clcImpuesto.EditValue, Me.clcImpuesto.EditValue, Me.clcTotalArticulos.EditValue, Me.clcTotalArticulos.EditValue, CDate("01/01/1900").Date, "", "", False, -1)

                    'DAM 17/05/2006 -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO
                    Dim observaciones As String
                    observaciones = "Nota de Venta " + lblSerieSucursal.Text + "-" + Me.lblFolio.Text

                    If Not Response.ErrorFound Then
                        Response = Me.GuardaAnticipos(Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long))
                    End If

                    'GUARDA DOCUMENTOS DE CXC
                    If Not Response.ErrorFound Then Response = GuardaDocumentos(Response, observaciones)

                    'GUARDA ENCABEZADOS DE LOS MOVIMIENTOS
                    If Not Response.ErrorFound Then
                        GeneraListaBodegas()
                        Dim i As Integer = 0
                        For i = 0 To arrBodegas.Count - 1
                            'ShowMessage(MessageType.MsgInformation, arrBodegas(i))
                            GeneraMovimientoInventario(Response, folio_movimiento, ConceptoVenta, arrBodegas(i))
                            arrFoliosMovimientos.Add(folio_movimiento)
                            folio_movimiento = 0
                        Next
                    End If


                    Response = oDescuentosEspecialesClientes.ActualizarVentas(Me.FolioDescuento, True)
                    If Response.ErrorFound Then Exit Sub




            End Select
            Response = ActualizaCotizacion(Me.lkpCotizacion.EditValue, Comunes.Common.Sucursal_Actual, lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long))
            banCerrar = True
            CategoriasIdentificables = -2 ' Significa que limpiara la bandera para las categorias ya que la venta fue realizada


        Else    'Credito
            'Si es CREDITO - LLAMAR PANTALLA:DATOS CREDITO
            ''''''''''''''''''GUARDAR VENTA
            ''''''''''''''''''GUARDAR CXC
            '�Me.tmaVentas.DataSource.Tables(0).DefaultView.RowFilter = "control in (0,1,2)"

            Dim frmModal As New frmVentasDatosCredito
            With frmModal
                .Tiene_Precio_Expo = Me.Tiene_Precio_Expo
                .Precio_Expo = Me.Precio_Expo
                .Text = "Datos de Cr�dito"
                .Action = Me.Action
                .odataVenta = Me.DataSource
                .odataArticulos = Me.tmaVentas.DataSource.Tables(0).DefaultView
                .persona = persona 'Me.lkpCliente.GetValue("persona")
                .folio_plantilla = folio_plantilla 'Me.lkpCliente.GetValue("folio_plantilla")
                .sucursal_dependencia = sucursal_dependencia
                .cotizacion = Cotizacion
                .tipo_precio_venta = tipo_precio_venta
                .Cobrador = Me.Cobrador
                .Paquete = Me.Paquete
                .Plan_credito_Paquetes = Plan_credito_Paquetes

                .Plan_credito_descuento_especial_cliente = Plan_credito_descuento_especial_cliente
                .Porcentaje_Descuento_especial_cliente = Porcentaje_Descuento_especial_cliente
                .FolioDescuento = Me.FolioDescuento
                .OwnerForm = Me
                .MdiParent = Me.MdiParent
                .Icon = Me.Icon
                .LimiteCreditoCliente = Me.dLimiteCredito
                .SerieFactura = Me.lblSerieSucursal.Text
                .FacturaElectronica = Me.factura_electronica
                .chkIvadesglosado1.Enabled = Me.chkIvadesglosado.Checked
                .chkFacturacionEspecial.Enabled = Me.chkFacturacionEspecial.Enabled
                .chkFacturacionEspecial.Checked = Me.chkFacturacionEspecial.Checked


                .oDescuentosEspecialesClientes = Me.oDescuentosEspecialesClientes
                .ArticuloAnticipo = Me.ArticuloAnticipos
                .TotalAnticipos = Me.clcTotalAnticipos.EditValue

                .Show()
            End With
            Me.Enabled = False
            banCerrar = False
        End If


    End Sub
    Private Sub frmVentas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim i As Long
        Dim TableVentaEncabezado As DataTable

        If lSucursal <> -1 Then
            Response = oVentas.DespliegaDatos(lSucursal, sSerie, lFolio)
            Me.tbrTools.Buttons.Item(2).Visible = False
            Me.tbrTools.Buttons.Item(2).Enabled = False
        Else
            Response = oVentas.DespliegaDatos(OwnerForm.Value("sucursal"), OwnerForm.Value("serie"), OwnerForm.Value("folio"))
        End If


        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            '=========================================
            'Dam 15 jul 07
            TableVentaEncabezado = oDataSet.Tables(0)
            ' ========================================

            sucursal_venta = oDataSet.Tables(0).Rows(0).Item("sucursal")
            serie_venta = oDataSet.Tables(0).Rows(0).Item("serie")
            banCancelada = CType(oDataSet.Tables(0).Rows(0).Item("cancelada"), Boolean)
            sucursal_dependencia = CType(CType(oSucursales.DespliegaDatos(sucursal_venta).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)
            folio_pedido = CType(oDataSet.Tables(0).Rows(0).Item("folio_pedido"), Long)

            'DAM 16/ABR/07
            If CType(oDataSet.Tables(0).Rows(0).Item("enajenacion"), Boolean) = True Then
                dFactor_Enajenacion = CType(oDataSet.Tables(0).Rows(0).Item("factor_enajenacion"), Double)
            End If


            Me.DataSource = oDataSet

            If oDataSet.Tables(0).Rows(0).Item("tipoventa") = "C" Then

                LlenaDatosCredito(oDataSet.Tables(0).Rows(0).Item("plan"), oDataSet.Tables(0).Rows(0).Item("ivadesglosado"))
            Else

                Me.btnCredito.Enabled = False

            End If


            Dim sucursal_factura As Long
            Dim serie_factura As String
            Dim folio_factura As Long

            If lSucursal <> -1 Then
                sucursal_factura = lSucursal
                serie_factura = sSerie
                folio_factura = lFolio

            Else
                sucursal_factura = OwnerForm.Value("sucursal")
                serie_factura = OwnerForm.Value("serie")
                folio_factura = OwnerForm.Value("folio")
            End If

            Response = oVentasDetalle.Listado(sucursal_factura, serie_factura, folio_factura)

            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                Me.tmaVentas.DataSource = oDataSet
                Me.lblSerieSucursal.Text = CType(oDataSet.Tables(0).Rows(0).Item("serie"), String)
                Me.lblFolio.Text = CType(oDataSet.Tables(0).Rows(0).Item("folio"), String)


                '' 28/Enero/13  Reviso cual es el articulo anticipo y si lo hay obtengo la descipcion del concepto capturado
                '''''  ===============================================
                Dim FoundRows() As DataRow
                Dim orow As DataRow
                FoundRows = oDataSet.Tables(0).Select("EsArticuloAnticipo = 1")

                For Each orow In FoundRows
                    Me.txtConcepto.Text = orow("descripcion_anterior")
                Next
                '''''  ===============================================

                CalculaTotalArticulos()
                RecalculaTotales()

                If Not Response.ErrorFound Then
                    '' 28/Enero/13  Despliego los Anticipos de la Venta
                    Response = Me.oAnticiposDetalle.DespliegaDatosVentas(sucursal_factura, serie_factura, folio_factura)
                    If Not Response.ErrorFound Then
                        oDataSet = Response.Value
                        Me.tmaAnticipos.DataSource = oDataSet
                    End If
                End If
            End If



        End If

        Me.tmaAnticipos.Enabled = False
        Me.txtConcepto.Enabled = False


        'DAM 17-DIC-07
        'Se agrego esta Funcion para los Totales  de Abajo para las Ventas de Contado
        VerificaTipoVentaTotales(TableVentaEncabezado)

        'Me.TabControl1.SelectedTab = Me.TabPage1
        Me.Panel_Generales.BringToFront()
    End Sub
    Private Sub frmVentas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVentas = New VillarrealBusiness.clsVentas
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        oVendedores = New VillarrealBusiness.clsVendedores
        oClientes = New VillarrealBusiness.clsClientes
        oClientesCobradores = New VillarrealBusiness.clsClientesCobradores
        oArticulos = New VillarrealBusiness.clsArticulos
        oVariables = New VillarrealBusiness.clsVariables
        oMovimientosInventario = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientosInventarioDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oArticulosExistencias = New VillarrealBusiness.clsArticulosExistencias
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oPlanesCredito = New VillarrealBusiness.clsPlanesCredito
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario
        oHistoricoCostos = New VillarrealBusiness.clsHisCostos
        oSucursales = New VillarrealBusiness.clsSucursales
        oCotizaciones = New VillarrealBusiness.clsCotizaciones
        oAutorizaciones = New VillarrealBusiness.clsAutorizacionesVentasPedidoFabrica
        oVistasSalidas = New VillarrealBusiness.clsVistasSalidas
        oVistasSalidasDetalle = New VillarrealBusiness.clsVistasSalidasDetalle
        oVistasEntradas = New VillarrealBusiness.clsVistasEntradas
        oVistasEntradasDetalle = New VillarrealBusiness.clsVistasEntradasDetalle
        oMovimientosInventarioDetalleSeries = New VillarrealBusiness.clsMovimientosInventariosDetalleSeries



        Dim oDataSet As DataSet

        Dim folio_siguiente_electronica As String


        Response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        oDataSet = CType(Response.Value, DataSet)
        tipo_precio_venta = CType(oDataSet.Tables(0).Rows(0).Item("precio_venta"), Long)
        factura_electronica = CType(oDataSet.Tables(0).Rows(0).Item("factura_electronica"), Boolean)

        folio_siguiente_electronica = CType(oDataSet.Tables(0).Rows(0).Item("folio_factura_electronica"), String)

        response = Nothing

        TraeDatosVariables()


        'TraeConceptoVenta()
        'TraeConceptoFactura()        'Contado
        'TraeConceptoFacturaCredito() 'Credito
        'TraeConceptoVistaEntrada() 'Vista Entrada
        'TraeConceptoVistaSalida() 'Vista Salida
        'TraePrecioExpo() 'PRECIO EXPO

        dteFechaReparto.DateTime = CDate(TinApp.FechaServidor)

        If Not banConceptoVenta Or Not banConceptoFactura Or Not banConceptoFacturaCredito Or Not Me.banConceptoVistaEntrada Or Not banConceptoVistaSalida Then
            Me.tbrTools.Buttons.Item(0).Visible = False
            Me.tbrTools.Buttons.Item(2).Visible = False

            Me.tbrTools.Buttons.Item(0).Enabled = False
            Me.tbrTools.Buttons.Item(2).Enabled = False


            Me.tmaVentas.Enabled = False
            Me.grVentas.Enabled = False
            DeshabilitarControles()
        End If

        If banConceptoVenta Then
            AfectaCostos = CType(CType(oConceptosInventario.DespliegaDatos(ConceptoVenta).Value, DataSet).Tables(0).Rows(0).Item("afecta_saldos"), Boolean)
        End If

        Select Case Action
            Case Actions.Insert
                Me.lblFolio.Text = TraeFolioPuntoVenta(factura_electronica)
                Me.lblSerieSucursal.Text = TraeSeriePuntoVenta(factura_electronica)
            Case Actions.Update
                Me.tbrTools.Buttons.Item(0).Visible = False
                Me.tbrTools.Buttons.Item(0).Enabled = False

                Me.tmaVentas.CanInsert = False
                Me.tmaVentas.CanUpdate = False
                Me.tmaVentas.CanDelete = False

                Me.tmaVentas.Enabled = False
                Me.grVentas.Enabled = True
                DeshabilitarControles()


        End Select

        ConfiguraMaster()


    End Sub
    Private Sub frmVentas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim oEvent As Events
        Dim bandera As Boolean
        Dim articulos_descripciones As String = ""

        Try

            Response = oVentas.Validacion(Action, Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, CType(lblFolio.Text, Long), Vendedor, Cliente, Me.txtRfc.Text, Me.grvVentas.RowCount, sucursal_dependencia, Me.clcFolioPedido.Value, Me.chkReFacturado.Checked, Me.sucursal_factura_cancelada, Me.serie_factura_cancelada, Me.FolioVenta_cancelada)

        Catch ex As Exception
            oEvent.Ex = ex
            Exit Sub

        End Try

        If Not Response.ErrorFound And Me.chkIvadesglosado.Checked = True Then Response = oClientes.ValidaRfc(Me.txtRfc.Text, persona)

        If Not Response.ErrorFound Then

            bandera = ValidarImporteCero()
            If bandera = True Then
                Response.Ex = Nothing
                Response.Message = "El Precio Total debe ser Mayor a Cero"

                Exit Sub
            End If

            ' ===================================================================================

            'Valida que los Articulos Seleccionados para Reparto Realmente Se puedan Repartir
            Dim i As Integer
            Dim oEvents As New Events
            Dim contArticulos_NoReparto As Long = 0


            For i = 0 To grvVentas.RowCount - 1
                If Me.grvVentas.GetRowCellValue(i, Me.grcReparto) = True Then
                    oEvents = oArticulos.DespliegaDatos(Me.grvVentas.GetRowCellValue(i, Me.grcArticulo))
                    If CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("entrega_domicilio") = False Then
                        banRepartoValido = False
                        If articulos_descripciones.Length = 0 Then
                            articulos_descripciones = CType(Me.grvVentas.GetRowCellValue(i, Me.grcNArticulo), String)
                        Else
                            articulos_descripciones = articulos_descripciones + "," + CType(Me.grvVentas.GetRowCellValue(i, Me.grcNArticulo), String)
                        End If
                    Else
                        banRepartoValido = True
                    End If
                Else

                    contArticulos_NoReparto = contArticulos_NoReparto + 1
                End If

            Next
            oEvents = Nothing


            'Si ningun articulo se quiere repartir, pasa esta validacion
            If contArticulos_NoReparto = grvVentas.RowCount Then
                banRepartoValido = True
            End If



            If banRepartoValido = False Then
                'Llenar response con Error
                Response.Ex = Nothing
                Response.Message = "Lo(s) art�culo(s) :" + articulos_descripciones + " no se puede(n) repartir"
                Response.Layer = Response.ErrorLayer.InterfaceLayer
            End If

            ' ===================================================================================


            Dim articulo_detalle As Long
            Dim folio_autorizacion_detalle As Long
            Dim SobrePedido_detalle As Boolean
            Dim bodega_detalle As String
            Dim cantidad As Long

            Dim odataset As DataSet

            With Me.tmaVentas
                .MoveFirst()
                Do While Not .EOF
                    articulo_detalle = Me.tmaVentas.Item("articulo")
                    folio_autorizacion_detalle = Me.tmaVentas.Item("folio_autorizacion")
                    SobrePedido_detalle = Me.tmaVentas.Item("sobrepedido")
                    bodega_detalle = Me.tmaVentas.Item("bodega")
                    cantidad = Me.tmaVentas.Item("cantidad")

                    odataset = Me.oDescuentosEspecialesProgramados.DespliegaDatosVentas(articulo_detalle, -1, dteFecha.DateTime)
                    If odataset.Tables(0).Rows.Count = 0 Then
                        If SobrePedido_detalle And Me.Tiene_Precio_Expo = False Then

                            Response = oVentasDetalle.Validacion_SobrePedido(folio_autorizacion_detalle, articulo_detalle, bodega_detalle, Cliente, Vendedor, cantidad)
                            Me.tmaVentas.Item("costo") = Response.Value

                            If Response.ErrorFound Then Exit Sub
                        End If
                    End If
                    .MoveNext()
                Loop
            End With


            If Response.ErrorFound Then Exit Sub

            If Me.FolioDescuento > 0 Then
                Select Case Me.cboTipoventa.Value
                    Case "C"
                        If Me.Plan_credito_descuento_especial_cliente < 0 And Me.Porcentaje_Descuento_especial_cliente <= 0 Then
                            Response.Message = "No tiene un Plan de Credito o un porcentaje de descuento para esa Promoci�n"
                        End If
                    Case "D"
                        If Me.Monto_descuento_especial_cliente = 0 Then
                            Response.Message = "No tiene un Monto de Descuento para esa promoci�n"
                        End If
                End Select
            End If


            If Response.ErrorFound Then Exit Sub

            If Me.tmaAnticipos.View.RowCount > 0 Then
                Dim ObtenerImporteAnticipos As Double

                ObtenerImporteAnticipos = Me.ObtenerTotalImporteAnticipos()

                If Me.cboTipoventa.EditValue = "C" Then
                    If ObtenerImporteAnticipos > Me.clcTotalArticulos.EditValue Then
                        Response.Message = "El importe de los Anticipos es Mayor al Total de la Factura"
                    End If
                Else
                    If ObtenerImporteAnticipos > Me.clcTotal.EditValue Then
                        Response.Message = "El importe de los Anticipos es Mayor al Total de la Factura"
                    End If

                End If



            End If


            ' ===================================================================================

            'Esta es una validacion por si no se factura una articulo de regalo de manera individual
            'DAM 13/10/2006

            'If banRepartoValido = False Then
            '    Dim cantidad_articulos As Long
            '    Dim cantidad_articulos_regalo As Long

            '    cantidad_articulos = grvVentas.RowCount
            '    CType(Me.grVentas.DataSource, DataView).RowFilter = "articulo_regalo = 1"

            '    cantidad_articulos_regalo = grvVentas.RowCount
            '    CType(Me.grVentas.DataSource, DataView).RowFilter = ""


            '    If cantidad_articulos = cantidad_articulos_regalo Then
            '        Response.Ex = Nothing
            '        Response.Message = "Al menos un articulo de venta es requerido"
            '        Response.Layer = Response.ErrorLayer.InterfaceLayer
            '    End If

            'End If



        End If
    End Sub
    Private Sub frmVentas_Localize() Handles MyBase.Localize
        Find("folio", CType(Me.lblFolio.Text, Long))

    End Sub
    Private Sub frmVentas_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        If Me.cboTipoventa.Value = "D" Then 'Contado

            Dim folio_mvto As Long
            Dim i As Integer
            Dim Costo_Detalle As Double = 0
            Dim PrecioDescuentoEspecial As Double = 0
            Dim Partidas As Long = 0
            TotalCantidades = 0


            With Me.tmaVentas
                .MoveFirst()
                PrecioDescuentoEspecial = 0

                Do While Not .EOF

                    For i = 0 To arrBodegas.Count - 1
                        If arrBodegas(i) = .Item("bodega") Then
                            folio_mvto = arrFoliosMovimientos(i)
                            Exit For

                        End If
                    Next

                    Select Case .CurrentAction
                        Case Actions.Insert

                            Response = New Events
                            If Me.tmaVentas.Item("sobrepedido") = 0 Then Response = oMovimientosInventarioDetalle.ValidacantidadArticulos(.SelectedRow, .Item("bodega"), ConceptoVenta, folio_mvto, "I", "Ventas")
                            If Not Response.ErrorFound Then

                                Dim cantidades As Long
                                cantidades = Me.tmaVentas.Item("cantidad")

                                Partidas = Partidas + 1


                                ' Se metio este ciclo para obtener los costos de los articulos en las salidas, 
                                'ya que ahora se podran capturar cantidades mayores a 1
                                '--------------------------------------------------------------------------------
                                Do While cantidades > 0
                                    TotalCantidades = TotalCantidades + 1


                                    'si el articulo no es sobrepedido
                                    If Me.tmaVentas.Item("sobrepedido") = 0 Then
                                        Dim oDataSet As DataSet
                                        'si el concepto de la venta afecta costos
                                        If AfectaCostos Then

                                            '+++++++++++++++++++++++++++++++++++

                                            ' Traer costo del historico de costos
                                            Response = oHistoricoCostos.UltimoCostoArticulo(CType(Me.tmaVentas.Item("articulo"), Long), CType(Me.tmaVentas.Item("bodega"), String))
                                            If Not Response.ErrorFound Then
                                                oDataSet = Response.Value
                                                If oDataSet.Tables(0).Rows.Count > 0 Then
                                                    Me.tmaVentas.Item("costo") = oDataSet.Tables(0).Rows(0).Item("costo")
                                                    If Not oDataSet.Tables(0).Rows(0).Item("folio") Is System.DBNull.Value Then
                                                        Me.tmaVentas.Item("folio_historico_costo") = oDataSet.Tables(0).Rows(0).Item("folio")
                                                    Else
                                                        Me.tmaVentas.Item("folio_historico_costo") = -1
                                                    End If

                                                    'disminuyo el saldo del articulo en el historico de costos
                                                    Response = oHistoricoCostos.ActualizaSaldo(Me.tmaVentas.Item("folio_historico_costo"), -1)
                                                End If
                                            End If
                                        Else

                                            Me.tmaVentas.Item("costo") = 0
                                            Me.tmaVentas.Item("folio_historico_costo") = -1

                                        End If
                                        oDataSet = Nothing
                                        Costo_Detalle = Me.tmaVentas.Item("costo")
                                    Else

                                        'si el articulo es sobrepedido
                                        Costo_Detalle = Me.tmaVentas.Item("costo")
                                        Me.tmaVentas.Item("folio_historico_costo") = -1


                                        If Me.tmaVentas.Item("folio_autorizacion") > 0 Then
                                            Response = oAutorizaciones.Actualizar_autorizaciones_pedido_fabrica_punto_venta(Me.tmaVentas.Item("folio_autorizacion"), Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long))
                                        End If
                                    End If

                                    '==================================================================
                                    '' SI LA PARTIDA VIENE DE UNA SALIDA POR VISTA , SE MARCA COMO FACTURADA = TRUE Y 
                                    '' SE GUARDAN LOS DATOS DE LA FACTURA
                                    'If Not Response.ErrorFound And IsNumeric(.Item("partida_vistas")) > 0 Then Response = Me.oVistasSalidasDetalle.ActualizarFacturada(.Item("partida_vistas"), .Item("partida"), .Item("articulo"), True, Comunes.Common.Sucursal_Actual, uti_SeriePV(Comunes.Common.PuntoVenta_Actual), CType(Me.lblFolio.Text, Long))


                                    'If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), ConceptoVenta, folio_mvto, dteFecha.EditValue, .Item("partida"), .Item("articulo"), .Item("cantidad"), Costo_Detalle, .Item("precio_contado"), .Item("folio_historico_costo"), folio_movimiento_detalle)

                                    ''DAM - SE VERIFICA POR CADA PARTIDA SI VIENE DE UNA VISTA Y MANEJA SERIES 
                                    ''PARA GENERAR EL MOVIMIENTO AL INVENTARIO DE SALIDA PARA EL NUMERO DE SERIE
                                    ''YA QUE ESTO SE DEBERIA DE HACER  NORMALMENTE POR REPARTO PERO COMO LA MCIA. 
                                    '' YA ESTA ENTREGADA SOLO SE GENERA EL MOVIMIENTO
                                    'If Not Response.ErrorFound And IsNumeric(.Item("partida_vistas")) Then

                                    '    If CType(.Item("numero_serie"), String).Trim.Length > 0 Then
                                    '        Response = oMovimientosInventarioDetalleSeries.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), ConceptoVenta, folio_mvto, .Item("partida"), .Item("numero_serie"), .Item("articulo"))
                                    '    End If

                                    'End If

                                    'If Not Response.ErrorFound Then Response = oVentasDetalle.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, uti_SeriePV(Comunes.Common.PuntoVenta_Actual), CType(Me.lblFolio.Text, Long), folio_movimiento_detalle, tipo_precio_venta)
                                    '==================================================================



                                    ' SI LA PARTIDA VIENE DE UNA SALIDA POR VISTA , SE MARCA COMO FACTURADA = TRUE Y 
                                    ' SE GUARDAN LOS DATOS DE LA FACTURA
                                    '/*csequera enero 2008
                                    If Not Response.ErrorFound Then Response = Me.oVistasSalidasDetalle.ActualizarFacturada(Me.lkpFolioVistaSalida.EditValue, TotalCantidades, .Item("articulo"), True, Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long))
                                    '*/csequera enero 2008
                                    If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), ConceptoVenta, folio_mvto, dteFecha.EditValue, TotalCantidades, .Item("articulo"), 1, Costo_Detalle, Costo_Detalle * 1, .Item("folio_historico_costo"), folio_movimiento_detalle)


                                    'DAM - SE VERIFICA POR CADA PARTIDA SI VIENE DE UNA VISTA Y MANEJA SERIES 
                                    'PARA GENERAR EL MOVIMIENTO AL INVENTARIO DE SALIDA PARA EL NUMERO DE SERIE
                                    'YA QUE ESTO SE DEBERIA DE HACER  NORMALMENTE POR REPARTO PERO COMO LA MCIA. 
                                    ' YA ESTA ENTREGADA SOLO SE GENERA EL MOVIMIENTO
                                    If Not Response.ErrorFound And Not System.DBNull.Value Is .Item("numero_serie") And Not System.DBNull.Value Is .Item("partida_vistas") Then
                                        If .Item("partida_vistas") > 0 And CType(.Item("numero_serie"), String).Trim.Length > 0 Then
                                            Response = oMovimientosInventarioDetalleSeries.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), ConceptoVenta, folio_mvto, TotalCantidades, .Item("numero_serie"), .Item("articulo"))
                                        End If

                                    End If

                                    If Me.FolioDescuento > 0 And Me.Monto_descuento_especial_cliente > 0 Then
                                        Dim porcentaje_descuento As Double = (Me.clcSubtotalContado.Value - Me.Monto_descuento_especial_cliente) / Me.clcSubtotalContado.Value
                                        PrecioDescuentoEspecial = System.Math.Round(.Item("preciounitario") * porcentaje_descuento)

                                        If Me.cboTipoventa.Value = "D" Then
                                            .Item("preciounitario") = PrecioDescuentoEspecial
                                        End If
                                    Else
                                        If Me.cboTipoventa.Value = "D" Then
                                            .Item("preciounitario") = .Item("precio_contado")
                                        End If
                                        PrecioDescuentoEspecial = .Item("preciounitario")

                                    End If


                                    ' Para revisar si se guardara la descripcion especial del articulo
                                    Dim descripcion_especial As String = ""

                                    If Me.chkFacturacionEspecial.Checked Then
                                        descripcion_especial = .Item("descripcion_especial")
                                    End If


                                    If Not Response.ErrorFound Then
                                        If Me.FolioDescuento > 0 Then
                                            Response = oVentasDetalle.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long), TotalCantidades, folio_movimiento_detalle, tipo_precio_venta, -1, PrecioDescuentoEspecial, PrecioDescuentoEspecial, descripcion_especial)
                                        Else
                                            Response = oVentasDetalle.Insertar(.SelectedRow, Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long), TotalCantidades, folio_movimiento_detalle, tipo_precio_venta, -1, PrecioDescuentoEspecial, .Item("precio_contado"), descripcion_especial)
                                        End If

                                    End If

                                    If Not Response.ErrorFound Then
                                        Dim columna_actualizar_entregar As String
                                        Dim columna_actualizar_fisicas As String = "FI"

                                        If Me.tmaVentas.Item("sobrepedido") = 0 Then
                                            columna_actualizar_entregar = "EN"
                                            Response = oArticulosExistencias.Actualizar(.Item("articulo"), .Item("bodega"), columna_actualizar_fisicas, -1, 0)
                                        Else
                                            columna_actualizar_entregar = "EP"
                                        End If


                                        Response = oArticulosExistencias.Actualizar(.Item("articulo"), .Item("bodega"), columna_actualizar_entregar, 1, 0)
                                    End If


                                    cantidades = cantidades - 1
                                Loop

                                folio_movimiento_detalle = 0
                                folio_movimiento_detalle = 0
                            Else
                                Exit Do
                            End If

                    End Select
                    .MoveNext()
                Loop
            End With

            If Not Response.ErrorFound And Me.tmaAnticipos.Count > 0 Then

                Dim ArticuloAnticipo As Long = ArticuloAnticipos
                Dim PrecioContadoAnticipo As Double = 0
                Dim TotalAnticipos As Double = Me.clcTotalAnticipos.EditValue
                Dim oArticulosPrecios As New VillarrealBusiness.clsArticulosPrecios

                'PrecioContadoAnticipo = oArticulos.ArticuloPrecioVenta(ArticuloAnticipo, Comunes.clsUtilerias.uti_VariablesPrecioContado(), sucursal_dependencia, dteFechaPedido.DateTime, 0).Value
                TotalAnticipos = TotalAnticipos * -1

                Me.InsertarArticuloAnticipo(Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long), Partidas + 1, ArticuloAnticipo, TotalAnticipos, TotalAnticipos, TotalAnticipos, 0, 1, Me.FolioDescuento, TotalAnticipos)
            End If

        End If
    End Sub
    Private Sub frmVentas_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        'respaldo los datos de la venta para posteriormente imprimir su factura
        Dim folio As Long
        Dim ivadesglosado As Boolean
        Dim tipo_venta As String
        Dim banImprimirFacAux As Boolean
        folio = CType(Me.lblFolio.Text, Long)
        ivadesglosado = Me.chkIvadesglosado.Checked
        tipo_venta = Me.cboTipoventa.Value
        banImprimirFacAux = banImprimirFac

        'si no se quiere cerrar la ventana de ventas se cancela la accion
        'esto aplica cuando aun no se concreta o da salir a una venta
        If Not banCerrar Then
            e.Cancel = True

            If Not (OwnerForm Is Nothing) Then
                OwnerForm.Enabled = False
            End If

        Else    'si da salir o se concreto una venta
            'si la accion de la venta es insert y se concreto una venta
            If Action = Actions.Insert And banImprimirFacAux Then
                'si se quiere cerrar el punto de venta
                '''''''''''''''''''''''''''''''''''''''''If banCerrarPVenta Then
                '''''''''''''''''''''''''''''''''''''''''OwnerForm.Enabled = True
                '''''''''''''''''''''''''''''''''''''''''Else    'sino se quiere cerrar el punto de venta

                If Not (OwnerForm Is Nothing) Then
                    OwnerForm.Enabled = False
                End If

                'Abro una ventana limpia para una nueva venta
                'OwnerForm.AbrePVenta()
                'Imprimo la factura de la venta anterior
                If sucursal_dependencia = True Then
                    'si fue venta en sucursal de dependencia
                    bVentaGuardada = False
                    ImprimeFacturaMagisterio(folio, ivadesglosado)
                Else
                    'si fue venta en sucursal normal
                    bVentaGuardada = False
                    ImprimeFactura(folio, True, tipo_venta)
                End If
                LimpiarVentana()
                e.Cancel = True

                ''''''''''''''''''''''''''''''''''''''''End If
            Else    'si la accion fue salir sin concretarse una venta
                If Not (OwnerForm Is Nothing) Then
                    OwnerForm.Enabled = True
                End If
            End If
        End If
    End Sub
    Private Sub frmVentas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        '@ACH-28/06/07: Validaci�n agregada para evitar traer el punto de venta en formas que no lo definan (ya que ocasiona un error)
        If Not Action = Actions.Update And bVentaGuardada = False Then
            Me.lblFolio.Text = TraeFolioPuntoVenta(factura_electronica)
            Me.lblFolio.Refresh()
        End If
        '/@ACH-28/06/07

        If Enabled And banCerrarVenta Then
            Me.Close()
        End If
    End Sub
    Private Sub frmVentas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        banImprimirFac = False

        Me.clcTotalMinimo.Visible = False
        Me.lblSubtotal.Visible = True
        Me.clcSubtotal.Visible = False

        If VerificaPermisoExtendido(Me.OwnerForm.MenuOption.Name, "FECHA_VENTAS") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If


        If Action = Actions.Insert Then
            Me.Text = "Insertando una Venta"
            Me.dteFecha.DateTime = CType(OwnerForm, brwVentas).dteFecha.DateTime
            Me.dteFecha_Importacion.EditValue = System.DBNull.Value
            Me.clcTotalArticulos.EditValue = 0

            'Me.TabControl1.TabPages.Remove(Me.TabPage5)
            'Me.TabControl1.TabPages.Remove(Me.TabPage6)
            'Me.TabControl1.SelectedTab = Me.TabPage1

            Me.btnCredito.Enabled = False
            Me.btnCancelacion.Enabled = False
            Me.Panel_Generales.BringToFront()


            Me.tbrTools.Buttons.Item(2).Visible = False
            Me.tbrTools.Buttons.Item(2).Enabled = False

            sucursal_dependencia = CType(CType(oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)

            If sucursal_dependencia = False Then
                btnNuevoClientePlantilla.Visible = False
            Else
                Me.cboTipoventa.Value = "C"
                Me.cboTipoventa.Enabled = False
                btnNuevoClientePlantilla.Visible = False
            End If

            If sucursal_dependencia = False Then
                Me.lblFechaPedido.Visible = False
                Me.dteFechaPedido.Visible = False
                Me.dteFechaPedido.DateTime = CDate("01/01/1900")

                Me.lblFolioPedido.Visible = False
                Me.clcFolioPedido.Visible = False
                Me.clcFolioPedido.EditValue = 0
            Else
                Me.dteFechaPedido.DateTime = CDate(TinApp.FechaServidor).Date
                Me.clcFolioPedido.EditValue = 0
            End If
        End If

        If Action = Actions.Update Then
            Me.Text = "Consultando una Venta"
            Me.lkpCotizacion.Enabled = False
            Me.lkpFolioVistaSalida.Enabled = False
            Me.lkpDescuentoCliente.Enabled = False

            'si no esta en ejecutivo, no se puede cancelar
            If Comunes.Common.banEjecutivo = False Then

                Me.btnCancelacion.Enabled = False
                Me.btnCancelacion.Visible = False
                Me.Panel_Generales.BringToFront()



            Else
                If Not banCancelada Then
                    Me.btnCancelacion.Visible = False
                    Me.btnCancelacion.Enabled = False
                    Me.Panel_Generales.BringToFront()

                    'Dim response As New Events
                    'response = oVentas.ValidaCancelar(CType(Me.lblFolio.Text, Long), sucursal_venta, serie_venta)
                    'If Not response.ErrorFound Then
                    '    If CType(response.Value, DataSet).Tables(0).Rows(0).Item("en_reparto") > 0 Then
                    '        tlbCancelarVenta.Enabled = False
                    '    End If
                    'Else
                    '    tlbCancelarVenta.Enabled = False
                    'End If
                    'response = Nothing

                End If
            End If

            Me.btnNuevoClientePlantilla.Visible = False
            Me.dteFechaPedido.Enabled = False
            Me.clcFolioPedido.Enabled = False

            If folio_pedido <= 0 Then
                Me.lblFechaPedido.Visible = False
                Me.dteFechaPedido.Visible = False
                Me.dteFechaPedido.DateTime = CDate("01/01/1900")

                Me.lblFolioPedido.Visible = False
                Me.clcFolioPedido.Visible = False
                Me.clcFolioPedido.EditValue = 0
            End If
        End If

        If Action = Actions.Delete Then
            Me.btnNuevoClientePlantilla.Visible = False
            Me.dteFechaPedido.Enabled = False
            Me.clcFolioPedido.Enabled = False

            If folio_pedido <= 0 Then
                Me.lblFechaPedido.Visible = False
                Me.dteFechaPedido.Visible = False
                Me.dteFechaPedido.DateTime = CDate("01/01/1900")

                Me.lblFolioPedido.Visible = False
                Me.clcFolioPedido.Visible = False
                Me.clcFolioPedido.EditValue = 0
            End If
        End If
    End Sub


    Private Sub frmVentas_ReInitialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.ReInitialize
        LimpiarVentana()
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#Region "LookUp"
    Private Sub lkpPlan_LoadData(ByVal Initialize As Boolean) Handles lkpPlan.LoadData

        If Action = Actions.Insert Then
            Dim Response As New Events
            Response = oPlanesCredito.Lookup(sucursal_dependencia, Cotizacion)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.lkpPlan.DataSource = oDataSet.Tables(0)
                oDataSet = Nothing
            End If
            Response = Nothing

        Else

            If folio_pedido > 0 Then

                Dim Response As New Events
                Response = oPlanesCredito.Lookup(True, , 1)
                If Not Response.ErrorFound Then
                    Dim oDataSet As DataSet
                    oDataSet = Response.Value
                    Me.lkpPlan.DataSource = oDataSet.Tables(0)
                    oDataSet = Nothing
                End If
                Response = Nothing

            Else

                Dim Response As New Events
                Response = oPlanesCredito.Lookup(False)
                If Not Response.ErrorFound Then

                    Dim oDataSet As DataSet
                    oDataSet = Response.Value
                    Me.lkpPlan.DataSource = oDataSet.Tables(0)
                    oDataSet = Nothing

                End If
                Response = Nothing

            End If

        End If

    End Sub
    Private Sub lkpPlan_Format() Handles lkpPlan.Format
        Comunes.clsFormato.for_planes_credito_grl(Me.lkpPlan)
    End Sub
    Private Sub lkpVendedor_Format() Handles lkpVendedor.Format
        Comunes.clsFormato.for_vendedores_grl(Me.lkpVendedor)
    End Sub
    Private Sub lkpVendedor_LoadData(ByVal Initialize As Boolean) Handles lkpVendedor.LoadData
        Dim Response As New Events
        Response = oVendedores.Lookup(Comunes.Common.Sucursal_Actual)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpVendedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData

        If Action = Actions.Insert Then

            Dim Response As New Events
            If Me.sucursal_dependencia Then
                Response = oClientes.LookupClientesConvenios()
            Else
                Response = oClientes.LookupCliente()
            End If

            If Not Response.ErrorFound Then

                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.lkpCliente.DataSource = oDataSet.Tables(0)
                oDataSet = Nothing

            End If
            Response = Nothing

        Else

            If folio_pedido > 0 Then

                Dim Response As New Events
                Response = oClientes.LookupLlenado(True, 0)
                If Not Response.ErrorFound Then

                    Dim oDataSet As DataSet
                    oDataSet = Response.Value
                    Me.lkpCliente.DataSource = oDataSet.Tables(0)
                    oDataSet = Nothing

                End If
                Response = Nothing

            Else

                Dim Response As New Events
                Response = oClientes.LookupCliente()
                If Not Response.ErrorFound Then

                    Dim oDataSet As DataSet
                    oDataSet = Response.Value
                    Me.lkpCliente.DataSource = oDataSet.Tables(0)
                    oDataSet = Nothing

                End If
                Response = Nothing

            End If

        End If

    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged

        If Cliente <> -1 Then 'And Action = Actions.Insert

            LlenarDatosCliente()
            LlenarDatosReparto()    'If Action = Actions.Insert Then 

        Else

            LimpiarDatosCliente()
            LimpiarDatosReparto()

        End If
    End Sub

    Private Sub lkpCotizacion_LoadData(ByVal Initialize As Boolean) Handles lkpCotizacion.LoadData

        Dim Response As New Events
        Response = oCotizaciones.Lookup()
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCotizacion.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpCotizacion_Format() Handles lkpCotizacion.Format
        Comunes.clsFormato.for_cotizaciones_grl(Me.lkpCotizacion)
    End Sub
    Private Sub lkpCotizacion_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpCotizacion.EditValueChanged

        If Me.Cotizacion > 0 Then

            Me.lkpCliente.EditValue = Me.lkpCotizacion.GetValue("cliente")
            FechaCotizacion = Me.lkpCotizacion.GetValue("fecha")
            Me.lkpFolioVistaSalida.EditValue = -1
            Me.lkpFolioVistaSalida.Text = ""
            'DAM 07-09-2006
            'Provoco que se cambie el foco del control para que entre a su evento lkpCotizacion_Validating
            SendKey.Send("{tab}")


        End If

    End Sub

    Private Sub lkpCotizacion_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles lkpCotizacion.Validating

        If Me.Cotizacion > 0 Then

            If ValidaVigenciaCotizacion(DiasVigenciaCotizacion) Then

                CargarArticulosCotizacion(Cotizacion)

            Else

                ShowMessage(MessageType.MsgInformation, "La vigencia de la cotizaci�n ya expiro", "Vigencia de Cotizaci�n")
                Me.lkpCotizacion.EditValue = Nothing
                CargarArticulosCotizacion(-1)

            End If

        End If

    End Sub
    Private Sub lkpFolio_Vista_Salida_LoadData(ByVal Initialize As Boolean) Handles lkpFolioVistaSalida.LoadData
        Dim Response As New Events
        Response = oVistasSalidas.Lookup(True)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFolioVistaSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpFolio_Vista_Salida_Format() Handles lkpFolioVistaSalida.Format
        Comunes.clsFormato.for_vistas_salidas_grl(Me.lkpFolioVistaSalida)
    End Sub
    Private Sub lkpFolioVistaSalida_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpFolioVistaSalida.EditValueChanged
        If Me.FolioVistaSalida <= 0 Then Exit Sub
        Me.lkpCliente.EditValue = Me.lkpFolioVistaSalida.GetValue("cliente")
        Me.lkpCotizacion.EditValue = -1
        Sucursal_vista_salida = Me.lkpFolioVistaSalida.GetValue("sucursal")
        CargarArticulosVistasSinEntregar(FolioVistaSalida)
    End Sub

    Private Sub lkpDescuentoCliente_LoadData(ByVal Initialize As Boolean) Handles lkpDescuentoCliente.LoadData
        Dim Response As New Events
        Response = Me.oDescuentosEspecialesClientes.Lookup(Cliente, False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDescuentoCliente.DataSource = oDataSet.Tables(0)
        End If

    End Sub
    Private Sub lkpDescuentoCliente_Format() Handles lkpDescuentoCliente.Format
        Comunes.clsFormato.for_descuentos_especiales_clientes_grl(Me.lkpDescuentoCliente)
    End Sub
    Private Sub lkpDescuentoCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDescuentoCliente.EditValueChanged
        If Me.FolioDescuento > 0 Then

            Monto_descuento_especial_cliente = Me.lkpDescuentoCliente.GetValue("monto")
            Plan_credito_descuento_especial_cliente = Me.lkpDescuentoCliente.GetValue("plan_credito")
            Porcentaje_Descuento_especial_cliente = Me.lkpDescuentoCliente.GetValue("porcentaje_descuento")
            CargarArticulosDescuentoCliente(Me.FolioDescuento)

        Else
            Monto_descuento_especial_cliente = 0
            Plan_credito_descuento_especial_cliente = -1
            Porcentaje_Descuento_especial_cliente = 0

            Me.tmaVentas.CanInsert = True
            Me.tmaVentas.DataSource = Nothing

        End If
    End Sub

    Private Sub lkpPaquetes_LoadData(ByVal Initialize As Boolean) Handles lkpPaquetes.LoadData
        Dim Response As New Events
        Response = Me.oPaquetes.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPaquetes.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpPaquetes_Format() Handles lkpPaquetes.Format
        Comunes.clsFormato.for_paquetes_grl(Me.lkpPaquetes)
    End Sub
    Private Sub lkpPaquetes_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpPaquetes.EditValueChanged
        If Me.Paquete > 0 Then


            Plan_credito_Paquetes = Me.lkpPaquetes.GetValue("plan_credito")

            CargarArticulosPaquetes(Me.Paquete)

        Else
            'Monto_descuento_especial_cliente = 0
            'Plan_credito_descuento_especial_cliente = -1
            'Porcentaje_Descuento_especial_cliente = 0

            Me.tmaVentas.CanInsert = True
            Me.tmaVentas.DataSource = Nothing

        End If
    End Sub

#End Region


    Public Function ValidoRecuperacionesRepartos() As Boolean
        Dim response As Events
        Dim odataset As DataSet
        Dim respuesta As Boolean = True

        response = oVentas.ValidaRecuperacionesRepartos(sucursal_venta, serie_venta, CType(Me.lblFolio.Text, Long))
        If Not response.ErrorFound Then
            odataset = response.Value
            If odataset.Tables(0).Rows.Count > 0 Then respuesta = odataset.Tables(0).Rows(0)(0)
        End If
        odataset = Nothing
        ValidoRecuperacionesRepartos = respuesta
    End Function
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick


        If e.Button Is Me.tblReimprimir Then
            ReimprimirFactura(sucursal_venta, serie_venta, CType(Me.lblFolio.Text, Long), True, Me.cboTipoventa.Value)
        End If

        If e.Button.Text = "Salir" Then

            banCerrar = True
            Me.Close()

        End If

        'If e.Button Is Me.tlbCancelarVenta Then

        '    'DAM 10/12/2007
        '    'VALIDA QUE NO SE PUEDA CANCELAR LA FACTURA SI TIENE ARTICULOS EN REPARTO O SIN RECUPERAR
        '    If Not ValidoRecuperacionesRepartos() Then
        '        ShowMessage(MessageType.MsgError, "Hay Mercancia en Reparto o Aun no se Termina su Recuperaci�n", "Cancelaci�n de un Venta")
        '        Exit Sub
        '    End If

        '    'si la fecha de la venta es la misma a la fecha de la cancelacion
        '    If Me.dteFecha.DateTime.Date = CDate(TinApp.FechaServidor).Date Then

        '        'pregunta : eliminar o cancelar
        '        Dim frmModal As New frmVentasEliminarCancelar
        '        With frmModal
        '            .OwnerForm = Me
        '            .MdiParent = Me.MdiParent
        '            .Show()
        '        End With
        '        Me.Enabled = False

        '    Else

        '        CancelarVenta("")

        '    End If

        'End If

    End Sub
    Private Sub btnNuevoClientePlantilla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoClientePlantilla.Click

        Dim frmModal As New frmVentasNuevoClienteDependencia
        With frmModal
            .Text = "Insertando un Cliente"
            .Action = Actions.Insert
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Show()
        End With
        Me.Enabled = False

    End Sub
    Private Sub clcFolioPedido_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcFolioPedido.EditValueChanged

        If Me.clcFolioPedido.Text.Length = 0 Then

            Me.clcFolioPedido.EditValue = 0

        Else

            If IsNumeric(Me.clcFolioPedido.Text) Then

                If Me.clcFolioPedido.Value < 0 Then

                    Me.clcFolioPedido.EditValue = 0

                End If

            Else

                Me.clcFolioPedido.EditValue = 0

            End If

        End If

    End Sub
    Private Sub tmaVentas_ValidateFields(ByRef Cancel As Boolean) Handles tmaVentas.ValidateFields
        If tmaVentas.Action = Actions.Insert And Not NumeroArticulosAgrupados() Then
            Cancel = True
            ShowMessage(MessageType.MsgInformation, "No se puede Insertar mas de 17 articulos por Factura")
        End If
    End Sub
    Private Sub btn_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReparto.GotFocus, btnGenerales.GotFocus, btnCredito.GotFocus, btnCliente.GotFocus, btnAduana.GotFocus, btnCancelacion.GotFocus
        'CType(sender, DevExpress.XtraEditors.SimpleButton).ForeColor = System.Drawing.Color.Blue
        CType(sender, DevExpress.XtraEditors.SimpleButton).BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003


        Select Case CType(sender, DevExpress.XtraEditors.SimpleButton).Name
            Case "btnGenerales"
                'Me.btnCredito.ForeColor = System.Drawing.Color.Black
                'Me.btnReparto.ForeColor = System.Drawing.Color.Black
                'Me.btnCliente.ForeColor = System.Drawing.Color.Black
                'Me.btnAduana.ForeColor = System.Drawing.Color.Black
                'Me.btnCancelacion.ForeColor = System.Drawing.Color.Black

                Me.btnCredito.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnReparto.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCliente.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnAduana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCancelacion.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default

            Case "btnCredito"
                Me.btnGenerales.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnReparto.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCliente.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnAduana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCancelacion.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
            Case "btnReparto"
                Me.btnGenerales.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCredito.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCliente.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnAduana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCancelacion.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default

            Case "btnCliente"
                Me.btnGenerales.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCredito.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnReparto.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnAduana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCancelacion.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
            Case "btnAduana"
                Me.btnGenerales.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCredito.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnReparto.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCliente.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCancelacion.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
            Case "btnCancelacion"
                Me.btnGenerales.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCredito.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnReparto.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnCliente.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
                Me.btnAduana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default

        End Select


    End Sub


    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerales.Click
        Me.Panel_Generales.BringToFront()
    End Sub
    Private Sub SimpleButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCredito.Click
        Me.panel_Creditos.BringToFront()
    End Sub
    Private Sub SimpleButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReparto.Click
        Me.Panel_Repartos.BringToFront()
    End Sub
    Private Sub SimpleButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCliente.Click
        Me.Panel_Cliente.BringToFront()
    End Sub
    Private Sub SimpleButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAduana.Click
        Me.Panel_Aduana.BringToFront()
    End Sub
    Private Sub SimpleButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelacion.Click
        Me.Panel_Cancelacion.BringToFront()
    End Sub
    Private Sub btnAnticipos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnticipos.Click
        Me.Panel_Anticipos.BringToFront()
    End Sub

    Private Sub chkReFacturado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkReFacturado.CheckedChanged

        Me.lkpSucursal_fac_Cancelada.EditValue = Nothing
        Me.lkpSerie_fac_cancelada.EditValue = Nothing
        Me.lkpFolio_fac_cancelada.EditValue = Nothing


        If Me.chkReFacturado.Checked = True Then
            Me.gpbFactura.Enabled = True
        Else
            Me.gpbFactura.Enabled = False
        End If
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal_fac_Cancelada.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal_fac_Cancelada)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal_fac_Cancelada.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal_fac_Cancelada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSucursal_fac_Cancelada.EditValueChanged
        lkpSerie_LoadData(True)
    End Sub

    Private Sub lkpSerie_LoadData(ByVal Initialize As Boolean) Handles lkpSerie_fac_cancelada.LoadData
        Dim response As Events
        response = oVentas.LookupSerieVentas(sucursal_factura_cancelada, True)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSerie_fac_cancelada.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpSerie_Format() Handles lkpSerie_fac_cancelada.Format
        Comunes.clsFormato.for_series_ventas_grl(Me.lkpSerie_fac_cancelada)
    End Sub
    Private Sub lkpSerie_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSerie_fac_cancelada.EditValueChanged
        'Select Case Action
        '    Case Actions.Insert
        lkpFolio_LoadData(True)
        'End Select
    End Sub

    Private Sub lkpFolio_LoadData(ByVal Initialize As Boolean) Handles lkpFolio_fac_cancelada.LoadData
        Dim response As Events
        response = oVentas.LookupFolioVentas(sucursal_factura_cancelada, serie_factura_cancelada, True)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolio_fac_cancelada.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpfolio_Format() Handles lkpFolio_fac_cancelada.Format
        Comunes.clsFormato.for_folio_ventas_grl(Me.lkpFolio_fac_cancelada)
    End Sub

    Private Sub chkFacturacionEspecial_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFacturacionEspecial.CheckedChanged
        If Me.chkFacturacionEspecial.Checked Then
            Me.grcDescripcionEspecial.VisibleIndex = 1
        Else
            Me.grcDescripcionEspecial.VisibleIndex = -1
        End If
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub ConfiguraMaster()

        With Me.tmaVentas
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmVentasDetalle
            .AddColumn("departamento")
            .AddColumn("grupo")
            .AddColumn("articulo", "System.Int32")
            .AddColumn("n_articulo")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("preciounitario", "System.Double")
            .AddColumn("total", "System.Double")
            .AddColumn("sobrepedido", "System.Boolean")
            .AddColumn("costo", "System.Double")    'oculto
            .AddColumn("surtido", "System.Boolean") 'oculto
            .AddColumn("reparto", "System.Boolean")
            .AddColumn("bodega")
            .AddColumn("n_bodega")
            .AddColumn("precio_minimo", "System.Double")    'oculto
            .AddColumn("folio_historico_costo", "System.Int32")
            .AddColumn("articulo_regalo", "System.Boolean")
            .AddColumn("precio_contado", "System.Double")
            .AddColumn("modelo")
            .AddColumn("folio_autorizacion", "System.Int32")
            .AddColumn("partida_vistas", "System.Int32")
            .AddColumn("numero_serie")
            .AddColumn("precio_pactado", "System.Double")
            .AddColumn("descripcion_especial")

        End With

        With tmaAnticipos
            .UpdateTitle = "un Anticipo"
            .UpdateForm = New frmVentasAnticipos
            .AddColumn("folio", "System.Int32")
            .AddColumn("concepto")
            .AddColumn("importe", "System.Double")
        End With


    End Sub
    Private Sub LlenarDatosCliente()

        Dim response As New Events
        Dim oData As DataSet
        response = oClientes.DespliegaDatos(Cliente)
        If Not response.ErrorFound Then

            oData = response.Value
            With oData.Tables(0).Rows(0)
                Me.clcCp.EditValue = .Item("cp")
                Me.txtCiudad.Text = .Item("nombre_ciudad")
                Me.txtColonia.Text = .Item("nombre_colonia")
                Me.txtMunicipio.Text = .Item("nombre_municipio")
                Me.txtCurp.Text = .Item("curp")
                Me.txtNumExterior.Text = .Item("numero_exterior")
                Me.txtNumInterior.Text = .Item("numero_interior")
                Me.txtDomicilio.Text = .Item("domicilio_numeros")

                ' .Item("domicilio") + "  " + Me.txtNumExterior.Text  '.Item("domicilio_numeros") '.Item("domicilio") + " Ext." + Me.txtNumExterior.Text + " Int." + Me.txtNumInterior.Text
                Me.txtEstado.Text = .Item("nombre_estado")
                Me.txtFax.Text = .Item("fax")

                Me.txtTelefono1.Text = .Item("telefono1")
                Me.txtTelefono2.Text = .Item("telefono2")
                Me.dLimiteCredito = .Item("limite_credito")
                Me.chkRfcCompleto.Checked = .Item("rfc_completo")
                Me.txtEmailCliente.Text = .Item("email")

                If CType(.Item("facturacion_especial"), Boolean) = True Then
                    Me.chkFacturacionEspecial.Enabled = True
                Else
                    Me.chkFacturacionEspecial.Enabled = False
                    Me.chkFacturacionEspecial.Checked = False
                End If

                If Me.chkRfcCompleto.Checked Then
                    Me.chkIvadesglosado.Checked = False
                    Me.chkIvadesglosado.Enabled = True
                    Me.txtRfc.Text = .Item("rfc")
                Else
                    Me.txtRfc.Text = "XAXX010101000" ' Se pone un RFC Generico
                    Me.chkIvadesglosado.Checked = False
                    Me.chkIvadesglosado.Enabled = False
                End If

                persona = .Item("persona")
                folio_plantilla = .Item("folio_plantilla")


            End With



            response = oClientesCobradores.DespliegaDatosCobradoresVentas(Me.Cliente, Comunes.Common.Sucursal_Actual)
            If Not response.ErrorFound Then

                oData = response.Value
                If oData.Tables(0).Rows.Count > 0 Then Me.Cobrador = oData.Tables(0).Rows(0).Item("cobrador")

            End If

        Else

            LimpiarDatosCliente()
            LimpiarDatosReparto()

        End If

    End Sub
    Private Sub LimpiarDatosCliente()

        Me.clcCp.EditValue = 0
        Me.txtDomicilio.Text = ""
        Me.txtColonia.Text = ""
        Me.txtCiudad.Text = ""
        Me.txtMunicipio.Text = ""
        Me.txtCurp.Text = ""
        Me.txtEstado.Text = ""
        Me.txtFax.Text = ""
        Me.txtRfc.Text = ""
        Me.txtTelefono1.Text = ""
        Me.txtTelefono2.Text = ""
        Me.txtNumInterior.Text = ""
        Me.txtNumExterior.Text = ""
        Me.dLimiteCredito = 0

    End Sub
    Private Sub LlenarDatosReparto()

        Me.txtCiudadReparto.Text = txtCiudad.Text
        Me.txtColoniaReparto.Text = txtColonia.Text
        Me.txtDomicilioReparto.Text = txtDomicilio.Text
        Me.txtEstadoReparto.Text = txtEstado.Text
        Me.txtTelefonoReparto.Text = txtTelefono1.Text


    End Sub
    Private Sub LimpiarDatosReparto()

        Me.txtCiudadReparto.Text = ""
        Me.txtColoniaReparto.Text = ""
        Me.txtDomicilioReparto.Text = ""
        Me.txtEstadoReparto.Text = ""
        Me.txtTelefonoReparto.Text = ""
        Me.txtObservacionesReparto.Text = ""
        Me.cboHorario.SelectedIndex = 0
        Me.dteFechaReparto.EditValue = CDate(TinApp.FechaServidor)

    End Sub

    'Private Sub TraeConceptoVenta()

    '    ConceptoVenta = CType(oVariables.TraeDatos("concepto_venta", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoVenta <> "" Then

    '        banConceptoVenta = True

    '    Else

    '        ShowMessage(MessageType.MsgInformation, "El Concepto de Venta no esta definido", "Variables del Sistema", Nothing, False)

    '    End If

    'End Sub
    'Private Sub TraeConceptoNCR()

    '    ConceptoNCR = CType(oVariables.TraeDatos("concepto_cxc_nota_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoNCR <> "" Then

    '        banConceptoNCR = True

    '    Else

    '        Cadena_Error_Cancelacion = "El Concepto de Cancelaci�n de la Venta no esta definido"

    '    End If

    'End Sub
    'Private Sub TraeConceptoCancelacionVenta()

    '    ConceptoCancelacionVenta = CType(oVariables.TraeDatos("concepto_cancelacion_venta", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoCancelacionVenta <> "" Then

    '        banConceptoCancelacionVenta = True

    '    Else

    '        Cadena_Error_Cancelacion = "El Concepto de Cancelaci�n de la Venta no esta definido"

    '    End If

    'End Sub

    'Private Sub TraeConceptoVistaEntrada()

    '    ConceptoVistaEntrada = CType(oVariables.TraeDatos("concepto_vistas_entrada", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoVistaEntrada <> "" Then

    '        banConceptoVistaEntrada = True

    '    Else

    '        Cadena_Error_Entrada_Vista = "El Concepto de Entrada por Vista no esta definido"

    '    End If

    'End Sub
    'Private Sub TraeConceptoVistaSalida()
    '    ConceptoVistaSalida = CType(oVariables.TraeDatos("concepto_vistas_salida", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoVistaSalida <> "" Then

    '        banConceptoVistaSalida = True

    '    Else

    '        Cadena_Error_Entrada_Vista = "El Concepto de Salida por Vista no esta definido"

    '    End If
    'End Sub

    'Private Sub TraePrecioExpo()
    '    Dim valor As Object = CType(oVariables.TraeDatos("precio_expo", VillarrealBusiness.clsVariables.tipo_dato.Entero), String)

    '    If valor = 0 Then
    '        Me.Tiene_Precio_Expo = False
    '    Else
    '        Precio_Expo = valor
    '        Me.Tiene_Precio_Expo = True

    '    End If

    '    'Me.Tiene_Precio_Expo = False
    'End Sub

    Private Sub GeneraListaBodegas()

        arrBodegas.Clear()
        Dim i As Integer

        For i = 0 To Me.grvVentas.RowCount - 1

            If arrBodegas.Contains(Me.grvVentas.GetRowCellValue(i, Me.grcBodega)) = False Then

                arrBodegas.Add(Me.grvVentas.GetRowCellValue(i, Me.grcBodega))

            End If

        Next

        arrBodegas.Sort()

    End Sub
    'Private Sub TraeConceptoFactura()

    '    ConceptoFactura = CType(oVariables.TraeDatos("concepto_cxc_factura_contado", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoFactura <> "" Then

    '        banConceptoFactura = True

    '    Else

    '        ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Contado no esta definido", "Variables del Sistema", Nothing, False)

    '    End If

    'End Sub
    'Private Sub TraeConceptoFacturaCredito()

    '    ConceptoFacturaCredito = CType(oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoFacturaCredito <> "" Then

    '        banConceptoFacturaCredito = True

    '    Else

    '        ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Cr�dito no esta definido", "Variables del Sistema", Nothing, False)

    '    End If

    'End Sub
    Private Sub LlenaDatosCredito(ByVal plan As Long, ByVal ivadesglosado As Boolean)
        Dim documentos_iguales As Integer
        Dim importe_ultimo As Integer
        Dim importe_documentos As Integer

        'Me.TabControl1.SelectedTab = Me.TabPage5

        Me.panel_Creditos.BringToFront()
        Me.lkpPlan.EditValue = plan
        Me.chkIvadesglosado1.Checked = ivadesglosado
        Me.clcTotalCredito.EditValue = Me.clcImporte.EditValue - Me.clcMenos.EditValue - Me.clcEnganche1.EditValue + Me.clcEnganche2.EditValue 'Me.clcTotal.EditValue 
        Me.chkEnganche_en_Menos.Visible = False

        Me.clcImportePlan.EditValue = (Me.clcNumero_Documentos.EditValue * Me.clcImporte_Documentos.EditValue) + Me.clcMenos.EditValue + Me.clcEnganche1.EditValue


        documentos_iguales = clcNumero_Documentos.EditValue - 1
        importe_ultimo = clcImporte_Ultimo_Documento.EditValue
        importe_documentos = clcImporte_Documentos.EditValue

        Me.lblDocumentosIguales.Text = documentos_iguales.ToString
        lblLeyendaDocumentos.Text = "Documentos de $ " + System.Math.Round(importe_documentos).ToString + " = "
        Me.lblTotalDocumentos.Text = "$ " + System.Math.Round(documentos_iguales * importe_documentos).ToString
        Me.lblDocumentoFinal.Text = "1"
        Me.lblLeyendaDocumentoFinal.Text = "Documento de $ " + importe_ultimo.ToString + " = "
        lblImporteUltimoDocumento.Text = "$ " + (importe_ultimo).ToString
        Me.lblDocumentosUltimoDocumento.Text = "$ " + (System.Math.Round(documentos_iguales * importe_documentos) + importe_ultimo).ToString

        'Me.TabControl1.SelectedTab = Me.TabPage1
        Me.Panel_Generales.BringToFront()

    End Sub
    Private Sub DeshabilitarControles()

        'Datos Generales
        Me.lkpVendedor.Enabled = False
        Me.lkpCliente.Enabled = False
        Me.chkIvadesglosado.Enabled = False
        Me.cboTipoventa.Enabled = False
        Me.txtObservacionesVenta.Enabled = False

        'Cliente
        Me.txtDomicilio.Enabled = False
        Me.txtEstado.Enabled = False
        Me.txtCiudad.Enabled = False
        Me.txtTelefono1.Enabled = False
        Me.txtTelefono2.Enabled = False
        Me.txtRfc.Enabled = False
        Me.txtCurp.Enabled = False
        Me.txtColonia.Enabled = False
        Me.clcCp.Enabled = False
        Me.txtFax.Enabled = False

        'Reparto
        Me.txtObservacionesReparto.Enabled = False
        Me.txtDomicilioReparto.Enabled = False
        Me.txtEstadoReparto.Enabled = False
        Me.txtColoniaReparto.Enabled = False
        Me.txtCiudadReparto.Enabled = False
        Me.txtTelefonoReparto.Enabled = False
        Me.cboHorario.Enabled = False
        Me.dteFechaReparto.Enabled = False


    End Sub
    Private Sub LimpiarVentana()
        ''''''''''''''''''Inicializar Variables''''''''''''''''''
        folio_movimiento = 0
        folio_movimiento_detalle = 0
        folio_pedido = -1
        arrBodegas = Nothing
        arrBodegas = New ArrayList
        arrFoliosMovimientos = Nothing
        arrFoliosMovimientos = New ArrayList
        sucursal_venta = 0      'para despliegue
        serie_venta = ""        'para despliegue
        lSucursal = -1
        sSerie = ""
        lFolio = -1
        dsDocumentosCobrar = Nothing

        ConceptoCancelacionVenta = ""
        ConceptoNCR = ""


        banConceptoCancelacionVenta = False
        Guardado = False
        banRepartoValido = False
        banCerrar = False
        banCerrarVenta = False

        banConceptoNCR = False

        banCancelada = False

        'inicializar controles
        Me.DataSource = Nothing
        Me.Reload(True)
        Me.Refresh()
        'Me.TabControl1.SelectedTab = Me.TabPage1
        Me.Panel_Generales.BringToFront()


        'Me.dteFecha.DateTime = CType(TinApp.FechaServidor, DateTime)       'en el load
        Me.lkpVendedor.DataSource = Nothing
        Me.lkpVendedor.EditValue = Nothing
        Me.lkpVendedor_LoadData(True)
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente_LoadData(True)
        Me.cboTipoventa.SelectedIndex = 1
        Me.chkIvadesglosado.Checked = False
        Me.txtObservacionesVenta.Text = ""
        Me.clcFolioPedido.EditValue = 0
        Me.dteFechaPedido.DateTime = CType(TinApp.FechaServidor, DateTime)

        Me.tmaVentas.DataSource.Clear() '= Nothing
        Me.tmaVentas.Refresh()
        Me.grVentas.Refresh()
        Me.grvVentas.UpdateCurrentRow()

        Me.clcTotalMinimo.EditValue = 0
        Me.clcSubtotal.EditValue = 0
        Me.clcImpuesto.EditValue = 0
        Me.clcTotal.EditValue = 0
        Me.clcTotalAnticipos.EditValue = 0

        Me.tmaAnticipos.DataSource.Clear()
        Me.tmaAnticipos.Refresh()
        Me.grAnticipos.Refresh()
        Me.grvAnticipos.UpdateCurrentRow()

        Me.txtConcepto.Text = ""

        'Me.TabControl1.SelectedTab = Me.TabPage2
        LimpiarDatosCliente()
        'Me.TabControl1.SelectedTab = Me.TabPage3
        LimpiarDatosReparto()

        'Me.TabControl1.SelectedTab = Me.TabPage4
        Me.txtAduana.Text = ""
        Me.txtPedimento.Text = ""
        Me.dteFecha_Importacion.EditValue = System.DBNull.Value

        'Me.TabControl1.SelectedTab = Me.TabPage1

        Me.lkpCotizacion.EditValue = -1
        Me.lkpFolioVistaSalida.EditValue = -1
        Me.lkpDescuentoCliente.EditValue = -1



        Me.clcSubtotal.Value = 0
        Me.clcMenosSoloDespliegue.Value = 0
        Me.clcTotalArticulos.EditValue = 0
        Me.chkReFacturado.Checked = False

        'poner action en insert
        Me.Action = Actions.Insert

        'llamar inicialize
        '        Me.frmVentas_Initialize(Nothing)

        'llamar load
        Me.Refresh()
        Me.frmVentas_Load(Nothing, Nothing)

        'llamar activated
        Me.frmVentas_Activated(Nothing, Nothing)

    End Sub
    Private Sub ImprimeFactura(ByVal folio As Long, ByVal ivadesglosado As Boolean, ByVal tipo_venta As String)
        Dim response As New Events
        Dim concepto_fac As String



        If tipo_venta = "C" Then 'Credito
            concepto_fac = ConceptoFacturaCredito
        Else    'Contado
            concepto_fac = ConceptoFactura
        End If
        response = oVentas.ImpresionFactura(Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, folio, concepto_fac, ivadesglosado)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La Impresi�n de la Factura no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                If Not Me.factura_electronica Then
                    Dim oReport As New rptFactura
                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    If TinApp.Connection.User.ToUpper = "SUPER" Then
                        TinApp.ShowReport(Me.MdiParent, "Factura", oReport, False, Nothing, True, True)
                    Else
                        TinApp.PrintReport(oReport, False, True)
                    End If
                    oReport = Nothing

                Else
                    Dim oReport As New rptFacturaFormato
                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    oreport.txtLeyendaReparto.Visible = True
                    oreport.txtLeyendaReparto.Text = "CLIENTE"
                    oreport.txtDescripcionArticulo.MultiLine = CType(oDataSet.Tables(0).Rows(0).Item("EsMultiline"), Boolean)

                    If TinApp.Connection.User.ToUpper = "SUPER" Then
                        TinApp.ShowReport(Me.MdiParent, "Factura Electronica", oReport, False, Nothing, True, True)
                    Else
                        TinApp.PrintReport(oReport, False, True)
                    End If
                    oReport = Nothing
                End If

                oDataSet = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "La Factura no tiene Informaci�n")
            End If

        End If

    End Sub
    Private Sub ReimprimirFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal ivadesglosado As Boolean, ByVal tipo_venta As String)
        Dim response As New Events
        Dim concepto_fac As String


        If tipo_venta = "C" Then 'Credito
            concepto_fac = ConceptoFacturaCredito
        Else    'Contado
            concepto_fac = ConceptoFactura
        End If

        response = oVentas.ImpresionFactura(sucursal, serie, folio, concepto_fac, ivadesglosado)

        If response.ErrorFound Then

            ShowMessage(MessageType.MsgInformation, "La Impresi�n de la Factura no se puede Mostrar")

        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                If Not Me.factura_electronica Then
                    Dim oReport As New rptFactura
                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    If TinApp.Connection.User.ToUpper = "SUPER" Then
                        TinApp.ShowReport(Me.MdiParent, "Factura", oReport, False, Nothing, True, True)
                    Else
                        TinApp.PrintReport(oReport, False, True)
                    End If
                    oReport = Nothing

                Else
                    Dim oReport As New rptFacturaFormato
                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    oreport.txtLeyendaReparto.Visible = True


                    oreport.txtLeyendaReparto.Text = CType(oDataSet.Tables(0).Rows(0).Item("LeyendaReimpresion"), String)
                    oreport.txtDescripcionArticulo.MultiLine = CType(oDataSet.Tables(0).Rows(0).Item("EsMultiline"), Boolean)


                    'If TinApp.Connection.User.ToUpper = "SUPER" Then
                    TinApp.ShowReport(Me.MdiParent, "Factura Electronica", oReport, False, Nothing, True, True)
                    'Else
                    'TinApp.PrintReport(oReport, False, True)
                    'End If
                    'oReport = Nothing
                End If

            Else
                ShowMessage(MessageType.MsgInformation, "La Factura no tiene Informaci�n")
            End If

        End If
    End Sub
    Private Sub ImprimeFacturaMagisterio(ByVal folio As Long, ByVal ivadesglosado As Boolean)

        Dim response As New Events
        Dim concepto_fac As String

        concepto_fac = ConceptoFacturaCredito  'las ventas de magisterio siempre son a credito
        response = oVentas.ImpresionFacturaMagisterio(Comunes.Common.Sucursal_Actual, Me.lblSerieSucursal.Text, folio, concepto_fac, ivadesglosado)

        If response.ErrorFound Then

            ShowMessage(MessageType.MsgInformation, "La Impresi�n de la Factura no se puede Mostrar")

        Else

            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptFacturaMagisterio
                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)
                TinApp.ShowReport(Me.MdiParent, "Factura", oReport, False, Nothing, False, True)
                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "La Factura no tiene Informaci�n")

            End If

        End If
    End Sub

    Private Sub CargarArticulosVistasSinEntregar(ByVal folio_vista_salida As Long)
        Dim response As Events

        response = oVistasEntradasDetalle.ArticulosSinEntregarFacturarPtoVenta(folio_vista_salida)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.tmaVentas.DataSource = oDataSet
            oDataSet = Nothing

            RecalculaTotales()
        End If

    End Sub
    Private Sub CargarArticulosCotizacion(ByVal lCotizacion As Long)

        Dim response As New Events
        response = Me.oVentas.TraerArticulosVentas(lCotizacion)
        If Not response.ErrorFound Then

            Dim odataset As DataSet
            odataset = response.Value
            Me.tmaVentas.DataSource = odataset
            RecalculaTotales()

            If odataset.Tables(0).Rows.Count > 0 Then Me.CategoriasIdentificables = odataset.Tables(0).Rows(0).Item("identificables")

        End If

    End Sub
    Private Sub CargarArticulosDescuentoCliente(ByVal folio_descuento As Long)

        Dim response As New Events
        response = Me.oVentas.TraerArticulosDescuentosClienteVentas(folio_descuento)
        If Not response.ErrorFound Then

            Dim odataset As DataSet
            odataset = response.Value
            Me.tmaVentas.DataSource = odataset
            RecalculaTotales()

            If odataset.Tables(0).Rows.Count > 0 Then
                Me.CategoriasIdentificables = odataset.Tables(0).Rows(0).Item("identificables")
                Me.tmaVentas.CanInsert = False

            End If
        Else
            response.ShowMessage()
        End If

    End Sub
    Private Sub CargarArticulosPaquetes(ByVal paquete As Long)
        Dim response As New Events
        response = Me.oVentas.TraerArticulosPaquetesVentas(paquete)
        If Not response.ErrorFound Then

            Dim odataset As DataSet
            odataset = response.Value
            Me.tmaVentas.DataSource = odataset
            RecalculaTotales()

            If odataset.Tables(0).Rows.Count > 0 Then
                Me.CategoriasIdentificables = odataset.Tables(0).Rows(0).Item("identificables")
                Me.tmaVentas.CanInsert = False
            End If
        Else
            response.ShowMessage()
        End If
    End Sub

    Public Sub DatosImportacion(ByVal action As Actions)

        Dim response As New Events
        Dim odata As DataSet

        If (action = Actions.Insert) And (Me.txtAduana.Text = "" Or Me.txtPedimento.Text = "") Then
            'checar si el nuevo articulo es de importacion

            response = oArticulos.DespliegaDatos(CType(Me.grvVentas.GetRowCellValue(Me.grvVentas.RowCount - 1, Me.grcArticulo), Long))
            odata = response.Value
            If odata.Tables(0).Rows.Count > 0 Then

                If (Not odata.Tables(0).Rows(0).Item("pedimento") Is System.DBNull.Value) And (Not odata.Tables(0).Rows(0).Item("aduana") Is System.DBNull.Value) Then

                    If odata.Tables(0).Rows(0).Item("pedimento") <> "" And odata.Tables(0).Rows(0).Item("aduana") <> "" Then

                        Me.txtAduana.Text = odata.Tables(0).Rows(0).Item("aduana")
                        Me.txtPedimento.Text = odata.Tables(0).Rows(0).Item("pedimento")
                        Me.dteFecha_Importacion.DateTime = odata.Tables(0).Rows(0).Item("fecha_importacion")

                    End If

                End If

            End If

            odata = Nothing

        End If

        If (action = Actions.Delete) And (Me.txtAduana.Text <> "" Or Me.txtPedimento.Text <> "") Then

            Me.txtAduana.Text = ""
            Me.txtPedimento.Text = ""
            Me.dteFecha_Importacion.EditValue = System.DBNull.Value

            'busca el primer articulo de importacion
            Dim i As Integer = 0
            For i = 0 To Me.grvVentas.RowCount - 1

                response = oArticulos.DespliegaDatos(CType(Me.grvVentas.GetRowCellValue(i, Me.grcArticulo), Long))
                odata = response.Value
                If odata.Tables(0).Rows.Count > 0 Then

                    If (Not odata.Tables(0).Rows(0).Item("pedimento") Is System.DBNull.Value) And (Not odata.Tables(0).Rows(0).Item("aduana") Is System.DBNull.Value) Then

                        If odata.Tables(0).Rows(0).Item("pedimento") <> "" And odata.Tables(0).Rows(0).Item("aduana") <> "" Then

                            Me.txtAduana.Text = odata.Tables(0).Rows(0).Item("aduana")
                            Me.txtPedimento.Text = odata.Tables(0).Rows(0).Item("pedimento")
                            Me.dteFecha_Importacion.DateTime = odata.Tables(0).Rows(0).Item("fecha_importacion")
                            Exit For

                        End If

                    End If

                End If

            Next

            odata = Nothing

        End If

    End Sub
    Public Sub SeleccionaClienteAgregado(ByVal cliente As Long)

        Me.lkpCliente.Reload()
        lkpCliente.EditValue = cliente
        SendKey.Send("{tab}")

    End Sub
    Private Function GeneraMovimientoInventario(ByRef response As Events, ByRef folio As Long, ByVal concepto As String, ByVal bodega As String)

        response = oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, bodega, concepto, folio, _
                    dteFecha.EditValue, Comunes.Common.Sucursal_Actual, lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long), "Movimiento generado desde el proceso de Ventas")

    End Function
    Private Function GeneraMovimientoInventario(ByVal accion As Actions, ByRef response As Events, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha_movimiento As Date, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String) '/*se sobrecargo la funcion para que esta funcione para guardar movimientos al inv de entradas de vistas */
        If Guardado = False Then
            Select Case accion
                Case Actions.Insert, Actions.Delete
                    response = oMovimientosInventario.Insertar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                    Guardado = True
                Case Actions.Update
                    response = oMovimientosInventario.Actualizar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                    Guardado = True
            End Select

        End If
    End Function
    Private Function GeneraMovimientoInventarioCancelacion(ByRef response As Events, ByRef folio As Long, ByVal concepto As String, ByVal bodega As String)

        response = oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, bodega, concepto, folio, _
                    CType(TinApp.FechaServidor, DateTime), sucursal_venta, serie_venta, CType(Me.lblFolio.Text, Long), "Movimiento generado desde el proceso de Ventas")

    End Function
    Private Function GuardaDocumentos(ByVal response As Events, ByVal observaciones As String) As Events

        response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFactura, lblSerieSucursal.Text, CType(Me.lblFolio.Text, Long), Cliente, 1, 0, Me.dteFecha.DateTime.Date, Me.clcTotalArticulos.EditValue, -1, -1, -1, -1, -1, -1, 0, Me.dteFecha.DateTime.Date, Me.clcTotalArticulos.EditValue, "", observaciones, 0)

        Return response

    End Function
    Private Function NumeroDocumentos(ByVal importe As Double) As Long

        Dim i As Long
        Dim importe_documento As Double
        For i = 0 To dsDocumentosCobrar.Tables(0).Rows.Count - 1

            If importe > dsDocumentosCobrar.Tables(0).Rows(i).Item("saldo") Then

                importe_documento = dsDocumentosCobrar.Tables(0).Rows(i).Item("saldo")

            Else

                importe_documento = importe

            End If

            importe = importe - importe_documento
            If importe = 0 Then Exit For

        Next

        Return i + 1

    End Function
    Public Function ActualizaCotizacion(ByVal Cotizacion, ByVal sucursal_venta, ByVal serie_venta, ByVal folio_movimiento) As Events

        ActualizaCotizacion = Me.oVentas.ActualizaCotizacion(Cotizacion, sucursal_venta, serie_venta, folio_movimiento)

    End Function
    Private Function ValidaVigenciaCotizacion(ByVal lDiasVigenciaCotizacion As Long) As Boolean

        ValidaVigenciaCotizacion = False

        If Me.dteFecha.DateTime < FechaCotizacion.AddDays(lDiasVigenciaCotizacion) Then

            ValidaVigenciaCotizacion = True

        End If

    End Function
    Private Function ValidarImporteCero() As Boolean

        Dim i As Long
        Dim totalNoValido As Boolean
        Dim oEvents As Events

        For i = 0 To grvVentas.RowCount - 1

            If Me.grvVentas.GetRowCellValue(i, Me.grcRegalo) = False And Me.grvVentas.GetRowCellValue(i, Me.grcTotal) <= 0 Then

                totalNoValido = True
                'Exit For
            End If

        Next

        ValidarImporteCero = totalNoValido

    End Function
    Private Sub CalculaTotalArticulos()

        Me.clcTotal.EditValue = 0
        Me.clcTotalArticulos.EditValue = 0
        Me.clcSubtotal.EditValue = 0
        Me.clcSubtotalContado.Value = 0



        Me.clcTotalArticulos.EditValue = 0
        Dim i As Integer = 0
        For i = 0 To Me.grvVentas.RowCount - 1

            If Me.grvVentas.GetRowCellValue(i, Me.grcRegalo) = False Then
                'DAM SE AGREGO LA MULTIPLICACION DE CANTIDADES EN LA FORMULA
                Me.clcTotalArticulos.EditValue = Me.clcTotalArticulos.EditValue + (CType(Me.grvVentas.GetRowCellValue(i, Me.grcContado), Double) * CType(Me.grvVentas.GetRowCellValue(i, Me.grcCantidad), Long))
                Me.clcTotal.EditValue = Me.clcTotal.EditValue + CType(Me.grvVentas.GetRowCellValue(i, Me.grcTotal), Double)
                Me.clcSubtotalContado.EditValue = Me.clcSubtotalContado.EditValue + CType(Me.grvVentas.GetRowCellValue(i, Me.grcTotal), Double)
            End If

        Next

        If Me.clcTotalAnticipos.EditValue > 0 Then
            Me.clcTotalArticulos.EditValue = Me.clcTotalArticulos.EditValue - Me.clcTotalAnticipos.EditValue
        End If



    End Sub
    Public Sub RecalculaTotales()

        Me.clcSubtotal.Value = 0
        Me.clcImpuesto.Value = 0
        Me.clcTotal.Value = 0
        Me.clcTotalMinimo.Value = 0
        Me.clcTotalArticulos.Value = 0
        Me.clcSubtotalContado.Value = 0

        '////////////////////////////////IMPUESTO
        Dim impuesto As Double
        Dim impuesto_porcentaje As Double

        impuesto = ImpuestoVariables
        'impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)

        If impuesto <> -1 Then
            impuesto_porcentaje = 1 + (impuesto / 100)
        Else
            ShowMessage(MessageType.MsgInformation, "El Porcentaje de Impuesto no esta definido", "Variables del Sistema", Nothing, False)
        End If

        ''////////////////////////////////TOTAL



        Dim i As Integer = 0

        CalculaTotalArticulos()

        '////////////////////////////////TOTAL MINIMO
        For i = 0 To Me.grvVentas.RowCount - 1

            Me.clcTotalMinimo.Value += CType(Me.grvVentas.GetRowCellValue(i, Me.grcPrecioMinimo), Double)
        Next

        '////////////////////////////////SUBTOTAL

        Dim ImporteRegalo As Long = 0
        ImporteRegalo = CalculaImporteTotalArticulosRegalo()

        If Me.FolioDescuento > 0 And Me.Monto_descuento_especial_cliente > 0 Then
            TotalMenos = ImporteRegalo + Monto_descuento_especial_cliente
            Me.clcTotalArticulos.Value = Me.clcTotal.Value - Monto_descuento_especial_cliente
        Else
            TotalMenos = ImporteRegalo + DiferenciaPrecios
        End If

        Me.clcMenosSoloDespliegue.EditValue = Me.TotalMenos

        Me.clcTotal.EditValue = Me.clcTotal.EditValue + CalculaImporteTotalArticulosRegalo()
        Me.clcSubtotal.EditValue = System.Math.Round(Me.clcTotalArticulos.EditValue / impuesto_porcentaje, 2)
        Me.clcImpuesto.Value = Me.clcTotalArticulos.EditValue - Me.clcSubtotal.EditValue

    End Sub
    'Public Function RecalculaTotalesContado() 'csequera 05/01/2007 resto al total el importe de los articulos de regalo

    'CalculaTotalArticulos()
    'RecalculaTotales()

    'End Function
    Public Function CalculaImporteTotalArticulosRegalo() As Long

        DiferenciaPrecios = 0
        Dim Suma As Long
        Dim i As Long
        Suma = 0
        i = 0
        While i <= Me.grvVentas.RowCount() - 1

            If Me.grvVentas.GetRowCellValue(i, Me.grcRegalo) = True Then

                Suma = Suma + Me.grvVentas.GetRowCellValue(i, Me.grcTotal)

            Else
                ' DAM SE AGREGO LA MULTIPLICACION POR LA CANTIDAD EN LA FORMULA
                DiferenciaPrecios = DiferenciaPrecios + (Me.grvVentas.GetRowCellValue(i, Me.grcTotal) - (Me.grvVentas.GetRowCellValue(i, Me.grcContado) * Me.grvVentas.GetRowCellValue(i, Me.grcCantidad)))

            End If

            i = i + 1

        End While

        Return Suma

    End Function
    Public Function TraeFolioPuntoVenta(ByVal Factura_Electronica As Boolean) As String

        If Factura_Electronica Then
            Dim Response As Dipros.Utils.Events
            Dim odataset As DataSet
            Response = oSucursales.DespliegaDatos(Common.Sucursal_Actual)
            If Response.ErrorFound = False Then
                odataset = Response.Value
                TraeFolioPuntoVenta = CType(CType(odataset.Tables(0).Rows(0).Item("folio_factura_electronica"), Integer) + 1, String)
            End If
        Else
            TraeFolioPuntoVenta = uti_FolioPV(Comunes.Common.PuntoVenta_Actual)
        End If


    End Function
    Private Function TraeSeriePuntoVenta(ByVal Factura_Electronica As Boolean) As String
        If Action = Actions.Insert Then
            If Factura_Electronica Then
                Dim Response As Dipros.Utils.Events
                Dim odataset As DataSet
                Response = oSucursales.DespliegaDatos(Common.Sucursal_Actual)
                If Response.ErrorFound = False Then
                    odataset = Response.Value
                    TraeSeriePuntoVenta = CType(odataset.Tables(0).Rows(0).Item("serie_factura_electronica"), String)
                End If
            Else
                TraeSeriePuntoVenta = uti_SeriePV(Comunes.Common.PuntoVenta_Actual)

            End If
        End If
    End Function

    Private Sub VerificaTipoVentaTotales(ByVal TableVentaEncabezado As DataTable)
        If Action = Actions.Update Then
            Dim menos As Double
            Dim enganche As Double

            Dim es_contado As Boolean = False

            menos = TableVentaEncabezado.Rows(0).Item("menos")
            enganche = TableVentaEncabezado.Rows(0).Item("enganche")

            Me.grcPrecioUnitario.VisibleIndex = -1
            Me.grcContado.VisibleIndex = -1

            ''DAM 15 JULIO 07
            ' se agrego esta rutina para mostrar datos de venta cuando es a credito
            If Me.cboTipoventa.EditValue = "C" Then   'CREDITO

                es_contado = False
                Me.clcSubtotal.EditValue = 0
                Me.clcSubtotal.EditValue = TableVentaEncabezado.Rows(0).Item("importe")
            Else
                ''DAM 17 Dic 07
                ' se agrego esta rutina para mostrar datos de venta cuando es a contado
                Me.clcSubtotal.EditValue = 0
                Me.clcSubtotal.EditValue = TableVentaEncabezado.Rows(0).Item("importe")
                Me.clcMenosSoloDespliegue.Value = menos

                es_contado = True
            End If

            Me.lblSubtotal.Visible = es_contado
            Me.clcSubtotal.Visible = es_contado
            Me.lblMenosSoloDespliegue.Visible = es_contado
            Me.clcMenosSoloDespliegue.Visible = es_contado
            Me.lblTotalContado.Visible = es_contado
            Me.clcTotalArticulos.Visible = es_contado

            If menos > 0 Then
                Me.lblMenosSoloDespliegue.Text = "Menos:"
            Else
                lblMenosSoloDespliegue.Text = "Enganche:"
            End If
        End If

    End Sub

#End Region

#Region "Anticipos"

    Public Function GuardaAnticipos(ByVal sucursal_venta As Long, ByVal serie_venta As String, ByVal folio_venta As Long) As Events
        Dim response As New Events

        Me.tmaAnticipos.MoveFirst()
        While Not Me.tmaAnticipos.EOF

            Select Case Me.tmaAnticipos.Action
                Case Actions.Insert
                    response = Me.oAnticiposDetalle.Insertar(Me.tmaAnticipos.Item("folio"), sucursal_venta, serie_venta, folio_venta, Me.dteFecha.DateTime, Me.tmaAnticipos.Item("importe"))
                Case Actions.Delete
                    response = Me.oAnticiposDetalle.Eliminar(Me.tmaAnticipos.Item("folio"), sucursal_venta, serie_venta, folio_venta)

            End Select
            Me.tmaAnticipos.MoveNext()

        End While

        Return response
    End Function

    Public Function ObtenerTotalImporteAnticipos() As Double
        Dim ImporteTotal As Double = 0.0
        Dim contador As Long = 0


        While contador <= Me.tmaAnticipos.View.RowCount - 1

            ImporteTotal = ImporteTotal + Me.tmaAnticipos.View.GetRowCellValue(contador, Me.grcImporteAnticipo)
            contador = contador + 1
        End While

        Me.clcTotalAnticipos.EditValue = ImporteTotal

        Return ImporteTotal
    End Function

    Public Function InsertarArticuloAnticipo(ByVal sucursal_venta As Long, ByVal serie_venta As String, ByVal folio_venta As Long, ByVal partida_siguiente As Long, ByVal Articulo As Long, ByVal PrecioUnitario As Double, ByVal PrecioContado As Double, ByVal PrecioPactado As Double, ByVal Costo As Double, ByVal TipoPrecioVenta As Long, ByVal FolioDescuentoEspecialProgramado As Long, ByVal PrecioEspecialProgramado As Double) As Events
        Dim response As New Events
        response = Me.oVentasDetalle.InsertarAnticipo(sucursal_venta, serie_venta, folio_venta, partida_siguiente, Articulo, PrecioUnitario, PrecioContado, False, Costo, False, False, "01", -1, TipoPrecioVenta, -1, FolioDescuentoEspecialProgramado, PrecioEspecialProgramado, PrecioPactado, Me.txtConcepto.Text, True)
        Return response
    End Function
#End Region

    Private Function NumeroArticulosAgrupados() As Boolean
        NumeroArticulosAgrupados = True

        If Me.tmaVentas.Count = 0 Then Exit Function

        Dim odataset As DataSet
        Dim odetalle As DataView = grvVentas.DataSource
        Dim i As Long
        Dim y As Long


        Dim dt As New DataTable("Orders")
        dt.Columns.Add("Articulo", GetType(Long))
        dt.Columns.Add("Bodega", GetType(String))
        dt.Columns.Add("sobrepedido", GetType(Boolean))

        Dim orow As DataRow

        For i = 0 To odetalle.Count - 1
            orow = dt.NewRow()

            If i = 0 Then
                orow("Articulo") = odetalle.Table.Rows(i).Item("articulo")
                orow("Bodega") = odetalle.Table.Rows(i).Item("bodega")
                orow("sobrepedido") = odetalle.Table.Rows(i).Item("sobrepedido")
                dt.Rows.Add(orow)
            Else

                For y = 0 To dt.Rows.Count - 1
                    If (odetalle.Table.Rows(i).Item("articulo") <> dt.Rows(y).Item("Articulo")) Or (odetalle.Table.Rows(i).Item("Bodega") <> dt.Rows(y).Item("Bodega")) Or (odetalle.Table.Rows(i).Item("sobrepedido") <> dt.Rows(y).Item("sobrepedido")) Then
                        orow("Articulo") = odetalle.Table.Rows(i).Item("articulo")
                        orow("Bodega") = odetalle.Table.Rows(i).Item("bodega")
                        orow("sobrepedido") = odetalle.Table.Rows(i).Item("sobrepedido")
                        dt.Rows.Add(orow)
                        Exit For
                    End If
                Next
            End If

        Next

        If dt.Rows.Count >= 17 Then
            NumeroArticulosAgrupados = False
        End If



    End Function

    Private Sub TraeDatosVariables()

        Dim response As New Events

        response = oVariables.DespliegaDatos()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet

            oDataSet = response.Value
            ConceptoVenta = CType(oDataSet.Tables(0).Rows(0).Item("concepto_venta"), String)
            lDiasVigenciaCotizacion = CType(oDataSet.Tables(0).Rows(0).Item("dias_vigencia_cotizacion"), Long)
            lArticuloAnticipos = CType(oDataSet.Tables(0).Rows(0).Item("articulo_anticipos"), Long)
            ConceptoNCR = CType(oDataSet.Tables(0).Rows(0).Item("concepto_venta"), String)
            ConceptoCancelacionVenta = CType(oDataSet.Tables(0).Rows(0).Item("concepto_cancelacion_venta"), String)
            ConceptoVistaEntrada = CType(oDataSet.Tables(0).Rows(0).Item("concepto_vistas_entrada"), String)
            ConceptoVistaSalida = CType(oDataSet.Tables(0).Rows(0).Item("concepto_vistas_salida"), String)
            Dim valor As Object = CType(oDataSet.Tables(0).Rows(0).Item("precio_expo"), String)
            ConceptoFactura = CType(oDataSet.Tables(0).Rows(0).Item("concepto_cxc_factura_contado"), String)
            ConceptoFacturaCredito = CType(oDataSet.Tables(0).Rows(0).Item("concepto_cxc_factura_credito"), String)
            ImpuestoVariables = CType(oDataSet.Tables(0).Rows(0).Item("impuesto"), Double)


            If ConceptoVenta <> "" Then
                banConceptoVenta = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Venta no esta definido", "Variables del Sistema", Nothing, False)
            End If

            If ConceptoNCR <> "" Then
                banConceptoNCR = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Nota de Credito no esta definido", "Variables del Sistema", Nothing, False)
                'Cadena_Error_Cancelacion = "El Concepto de Nota de Credito no esta definido"
            End If


            If ConceptoCancelacionVenta <> "" Then
                banConceptoCancelacionVenta = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Cancelaci�n de la Venta no esta definido", "Variables del Sistema", Nothing, False)
                ' Cadena_Error_Cancelacion = "El Concepto de Cancelaci�n de la Venta no esta definido"
            End If

            If ConceptoVistaEntrada <> "" Then
                banConceptoVistaEntrada = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Entrada por Vista  no esta definido", "Variables del Sistema", Nothing, False)
                'Cadena_Error_Entrada_Vista = "El Concepto de Entrada por Vista no esta definido"
            End If


            If ConceptoVistaSalida <> "" Then
                banConceptoVistaSalida = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Salida por Vista  no esta definido", "Variables del Sistema", Nothing, False)
                'Cadena_Error_Entrada_Vista = "El Concepto de Salida por Vista no esta definido"
            End If

            If valor = 0 Then
                Me.Tiene_Precio_Expo = False
            Else
                Precio_Expo = valor
                Me.Tiene_Precio_Expo = True

            End If

            If ConceptoFactura <> "" Then
                banConceptoFactura = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Contado no esta definido", "Variables del Sistema", Nothing, False)
            End If

            If ConceptoFacturaCredito <> "" Then
                banConceptoFacturaCredito = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Cr�dito no esta definido", "Variables del Sistema", Nothing, False)
            End If


        End If

    End Sub



End Class


