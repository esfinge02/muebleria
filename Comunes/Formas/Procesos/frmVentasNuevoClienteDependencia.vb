Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmVentasNuevoClienteDependencia
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpDependencia As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpTrabajador As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clcCliente As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpCiudad As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents clcplaza As DevExpress.XtraEditors.CalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVentasNuevoClienteDependencia))
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.lkpDependencia = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpTrabajador = New Dipros.Editors.TINMultiLookup
        Me.Label4 = New System.Windows.Forms.Label
        Me.clcCliente = New DevExpress.XtraEditors.CalcEdit
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.lkpCiudad = New Dipros.Editors.TINMultiLookup
        Me.Label8 = New System.Windows.Forms.Label
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.lblEstado = New System.Windows.Forms.Label
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.clcplaza = New DevExpress.XtraEditors.CalcEdit
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcplaza.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1161, 28)
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(40, 40)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenio.TabIndex = 0
        Me.lblConvenio.Tag = ""
        Me.lblConvenio.Text = "&Convenio:"
        Me.lblConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDependencia
        '
        Me.lkpDependencia.AllowAdd = False
        Me.lkpDependencia.AutoReaload = False
        Me.lkpDependencia.DataSource = Nothing
        Me.lkpDependencia.DefaultSearchField = ""
        Me.lkpDependencia.DisplayMember = "descripcion"
        Me.lkpDependencia.EditValue = Nothing
        Me.lkpDependencia.Filtered = False
        Me.lkpDependencia.InitValue = Nothing
        Me.lkpDependencia.Location = New System.Drawing.Point(104, 40)
        Me.lkpDependencia.MultiSelect = False
        Me.lkpDependencia.Name = "lkpDependencia"
        Me.lkpDependencia.NullText = ""
        Me.lkpDependencia.PopupWidth = CType(310, Long)
        Me.lkpDependencia.Required = False
        Me.lkpDependencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDependencia.SearchMember = ""
        Me.lkpDependencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDependencia.SelectAll = False
        Me.lkpDependencia.Size = New System.Drawing.Size(184, 20)
        Me.lkpDependencia.TabIndex = 1
        Me.lkpDependencia.Tag = ""
        Me.lkpDependencia.ToolTip = Nothing
        Me.lkpDependencia.ValueMember = "dependencia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Trabajador:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpTrabajador
        '
        Me.lkpTrabajador.AllowAdd = False
        Me.lkpTrabajador.AutoReaload = False
        Me.lkpTrabajador.DataSource = Nothing
        Me.lkpTrabajador.DefaultSearchField = ""
        Me.lkpTrabajador.DisplayMember = "nombre"
        Me.lkpTrabajador.EditValue = Nothing
        Me.lkpTrabajador.Filtered = False
        Me.lkpTrabajador.InitValue = Nothing
        Me.lkpTrabajador.Location = New System.Drawing.Point(104, 64)
        Me.lkpTrabajador.MultiSelect = False
        Me.lkpTrabajador.Name = "lkpTrabajador"
        Me.lkpTrabajador.NullText = ""
        Me.lkpTrabajador.PopupWidth = CType(510, Long)
        Me.lkpTrabajador.Required = True
        Me.lkpTrabajador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTrabajador.SearchMember = ""
        Me.lkpTrabajador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTrabajador.SelectAll = False
        Me.lkpTrabajador.Size = New System.Drawing.Size(368, 20)
        Me.lkpTrabajador.TabIndex = 3
        Me.lkpTrabajador.Tag = ""
        Me.lkpTrabajador.ToolTip = Nothing
        Me.lkpTrabajador.ValueMember = "plaza"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(28, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 23)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "&Plaza:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'clcCliente
        '
        Me.clcCliente.Location = New System.Drawing.Point(397, 40)
        Me.clcCliente.Name = "clcCliente"
        '
        'clcCliente.Properties
        '
        Me.clcCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.clcCliente.Size = New System.Drawing.Size(75, 20)
        Me.clcCliente.TabIndex = 12
        Me.clcCliente.Tag = "cliente"
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(104, 88)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(144, 20)
        Me.lkpEstado.TabIndex = 5
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'lkpCiudad
        '
        Me.lkpCiudad.AllowAdd = False
        Me.lkpCiudad.AutoReaload = True
        Me.lkpCiudad.DataSource = Nothing
        Me.lkpCiudad.DefaultSearchField = ""
        Me.lkpCiudad.DisplayMember = "descripcion"
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad.Filtered = False
        Me.lkpCiudad.InitValue = Nothing
        Me.lkpCiudad.Location = New System.Drawing.Point(104, 112)
        Me.lkpCiudad.MultiSelect = False
        Me.lkpCiudad.Name = "lkpCiudad"
        Me.lkpCiudad.NullText = ""
        Me.lkpCiudad.PopupWidth = CType(300, Long)
        Me.lkpCiudad.Required = False
        Me.lkpCiudad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCiudad.SearchMember = ""
        Me.lkpCiudad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCiudad.SelectAll = False
        Me.lkpCiudad.Size = New System.Drawing.Size(144, 20)
        Me.lkpCiudad.TabIndex = 7
        Me.lkpCiudad.Tag = "ciudad"
        Me.lkpCiudad.ToolTip = Nothing
        Me.lkpCiudad.ValueMember = "ciudad"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(264, 88)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 16)
        Me.Label8.TabIndex = 8
        Me.Label8.Tag = ""
        Me.Label8.Text = "Municipio:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = True
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(328, 88)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(300, Long)
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(144, 20)
        Me.lkpMunicipio.TabIndex = 9
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(272, 112)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 10
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "C&olonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(40, 112)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(60, 16)
        Me.lblCiudad.TabIndex = 6
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "Locali&dad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(54, 88)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 4
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "E&stado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = True
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(328, 112)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(300, Long)
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(144, 20)
        Me.lkpColonia.TabIndex = 11
        Me.lkpColonia.Tag = "colonia"
        Me.lkpColonia.ToolTip = Nothing
        Me.lkpColonia.ValueMember = "colonia"
        '
        'clcplaza
        '
        Me.clcplaza.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcplaza.Location = New System.Drawing.Point(104, 136)
        Me.clcplaza.Name = "clcplaza"
        Me.clcplaza.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcplaza.Size = New System.Drawing.Size(96, 20)
        Me.clcplaza.TabIndex = 59
        '
        'frmVentasNuevoClienteDependencia
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(490, 168)
        Me.Controls.Add(Me.clcplaza)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lkpCiudad)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lkpMunicipio)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lkpColonia)
        Me.Controls.Add(Me.clcCliente)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpTrabajador)
        Me.Controls.Add(Me.lblConvenio)
        Me.Controls.Add(Me.lkpDependencia)
        Me.Name = "frmVentasNuevoClienteDependencia"
        Me.Text = "frmVentasNuevoClienteDependencia"
        Me.Controls.SetChildIndex(Me.lkpDependencia, 0)
        Me.Controls.SetChildIndex(Me.lblConvenio, 0)
        Me.Controls.SetChildIndex(Me.lkpTrabajador, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.clcCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpColonia, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.lblCiudad, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.lkpMunicipio, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.lkpCiudad, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        Me.Controls.SetChildIndex(Me.clcplaza, 0)
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcplaza.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oClientes As New VillarrealBusiness.clsClientes
    Private oPlantilla As New VillarrealBusiness.clsPlantilla
    Private oDependencias As New VillarrealBusiness.clsDependencias
    Private oVentas As New VillarrealBusiness.clsVentas


    Private oEstados As New VillarrealBusiness.clsEstados
    Private oMunicipios As New VillarrealBusiness.clsMunicipios
    Private ociudades As New VillarrealBusiness.clsCiudades
    Private oColonias As New VillarrealBusiness.clsColonias

    Private ReadOnly Property Dependencia() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpDependencia)
        End Get
    End Property
    Private ReadOnly Property Trabajador() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpTrabajador)
        End Get
    End Property

    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property
    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property
    Private ReadOnly Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCiudad)
        End Get
    End Property
    Private ReadOnly Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"

    Private Sub frmVentasNuevoClienteDependencia_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmVentasNuevoClienteDependencia_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub
    Private Sub frmVentasNuevoClienteDependencia_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub

    Private Sub frmVentasNuevoClienteDependencia_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        'si no tiene nombres y apellidos descomponer nombre
        Dim nombre As String
        Dim nombres As String
        Dim apellidos As String
        Dim clave_identificacion As String
        nombre = Me.lkpTrabajador.GetValue("nombre")
        nombres = Me.lkpTrabajador.GetValue("nombres")
        apellidos = Me.lkpTrabajador.GetValue("apellidos")
        clave_identificacion = Me.lkpTrabajador.GetValue("clave_identificacion")
        If nombres.Trim(" ").Length = 0 Then
            nombres = nombre.Substring(0, nombre.IndexOf(" "))
            If ((nombre.Length) - (nombre.IndexOf(" ") + 1)) > 0 Then apellidos = nombre.Substring(nombre.IndexOf(" ") + 1, ((nombre.Length) - (nombre.IndexOf(" ") + 1)))
        End If


        Response = oClientes.InsertarDePlantilla(Me.DataSource, CType(Me.lkpTrabajador.GetValue("nombre"), String), CType(Me.lkpTrabajador.GetValue("rfc"), String), nombres, apellidos, "", CType(TinApp.FechaServidor, DateTime), Estado, Municipio, Ciudad, Colonia, Comunes.Common.Sucursal_Actual)
        If Not Response.ErrorFound Then Response = oPlantilla.ActualizaCliente(Me.clcCliente.Value, CType(Me.lkpTrabajador.GetValue("dependencia"), Long), clave_identificacion)
        If Not Response.ErrorFound Then OwnerForm.SeleccionaClienteAgregado(Me.clcCliente.Value)
    End Sub

    Private Sub frmVentasNuevoClienteDependencia_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0

        Me.Text = "Insertando un Cliente"

        Me.clcCliente.Visible = False
    End Sub

    Private Sub frmVentasNuevoClienteDependencia_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oVentas.ValidacionNuevoClienteDependencia(Actions.Insert, Trabajador)
    End Sub
#End Region

#Region "Eventos de Controles"

#Region "Lookup"
    Private Sub lkpTrabajador_LoadData(ByVal Initialize As Boolean) Handles lkpTrabajador.LoadData
        Dim Response As New Events
        Response = oPlantilla.LookupTrabajadores(Dependencia)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpTrabajador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpTrabajador_Format() Handles lkpTrabajador.Format
        Comunes.clsFormato.for_plantilla_trabajadores_grl(Me.lkpTrabajador)
    End Sub
    Private Sub lkpTrabajador_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpTrabajador.EditValueChanged
        'If Trabajador = -1 Then
        ' Me.lblPlaza.Text = ""
        'Me.lblMunicipio.Text = ""
        'Else
        Me.clcplaza.Value = Me.lkpTrabajador.GetValue("plaza")

        'End If
    End Sub

    Private Sub lkpDependencia_Format() Handles lkpDependencia.Format
        Comunes.clsFormato.for_dependencias_grl(Me.lkpDependencia)
    End Sub
    Private Sub lkpDependencia_LoadData(ByVal Initialize As Boolean) Handles lkpDependencia.LoadData
        Dim Response As New Events
        Response = oDependencias.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDependencia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDependencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDependencia.EditValueChanged
        Me.lkpTrabajador.EditValue = Nothing
        Me.lkpTrabajador_LoadData(True)
    End Sub

    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData
        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged

        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad_LoadData(True)

    End Sub

    Private Sub lkpCiudad_Format() Handles lkpCiudad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpCiudad)
    End Sub
    Private Sub lkpCiudad_LoadData(ByVal Initialize As Boolean) Handles lkpCiudad.LoadData
        Dim Response As New Events
        Response = ociudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCiudad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCiudad_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpCiudad.EditValueChanged
        If Me.lkpCiudad.EditValue Is Nothing Then Exit Sub

        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia_LoadData(True)
    End Sub

    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub
    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#End Region

#End Region

#Region "Funcionalidad"

#End Region

End Class
