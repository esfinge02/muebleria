Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmOrdenesRecuperacion
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Que As String
    Dim Quien As String
    Dim Cuando As Date
    Dim Donde As String
    'Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblElabora As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblBodega_Recibe As System.Windows.Forms.Label
    Friend WithEvents lblPersona_Recibe As System.Windows.Forms.Label
    Friend WithEvents lblFecha_Recibe As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Recibe As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblPersona_Inspecciona As System.Windows.Forms.Label
    Friend WithEvents lblFecha_Inspecciona As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Inspecciona As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblObservaciones_Inspeccion As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones_Inspeccion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grOrdenesRecuperacion As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvOrdenesRecuperacion As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gpbFactura As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lkpFolio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSerie As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblPersona_Recoge As System.Windows.Forms.Label
    Friend WithEvents lkpPersonaRecibe As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpPersonaInspecciona As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpPersonaRecoge As Dipros.Editors.TINMultiLookup
    Friend WithEvents gpbRecoge As System.Windows.Forms.GroupBox
    Friend WithEvents lkpBodegaRecibe As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblPersonaElabora As System.Windows.Forms.Label
    Friend WithEvents lblSucursalElabora As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkFacturaCancelada As System.Windows.Forms.CheckBox
    Friend WithEvents tbrReimprimir As System.Windows.Forms.ToolBarButton

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOrdenesRecuperacion))
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.lblElabora = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblBodega_Recibe = New System.Windows.Forms.Label
        Me.lblPersona_Recibe = New System.Windows.Forms.Label
        Me.lblFecha_Recibe = New System.Windows.Forms.Label
        Me.dteFecha_Recibe = New DevExpress.XtraEditors.DateEdit
        Me.lblPersona_Recoge = New System.Windows.Forms.Label
        Me.lblPersona_Inspecciona = New System.Windows.Forms.Label
        Me.lblFecha_Inspecciona = New System.Windows.Forms.Label
        Me.dteFecha_Inspecciona = New DevExpress.XtraEditors.DateEdit
        Me.lblObservaciones_Inspeccion = New System.Windows.Forms.Label
        Me.txtObservaciones_Inspeccion = New DevExpress.XtraEditors.MemoEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.grOrdenesRecuperacion = New DevExpress.XtraGrid.GridControl
        Me.grvOrdenesRecuperacion = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lkpPersonaRecibe = New Dipros.Editors.TINMultiLookup
        Me.lkpBodegaRecibe = New Dipros.Editors.TINMultiLookup
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lkpPersonaInspecciona = New Dipros.Editors.TINMultiLookup
        Me.lkpPersonaRecoge = New Dipros.Editors.TINMultiLookup
        Me.gpbFactura = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpFolio = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpSerie = New Dipros.Editors.TINMultiLookup
        Me.gpbRecoge = New System.Windows.Forms.GroupBox
        Me.lblPersonaElabora = New System.Windows.Forms.Label
        Me.lblSucursalElabora = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.chkFacturaCancelada = New System.Windows.Forms.CheckBox
        Me.tbrReimprimir = New System.Windows.Forms.ToolBarButton
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Recibe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Inspecciona.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones_Inspeccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grOrdenesRecuperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvOrdenesRecuperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.gpbFactura.SuspendLayout()
        Me.gpbRecoge.SuspendLayout()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbrReimprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5374, 28)
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(72, 40)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 0
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(120, 40)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(72, 19)
        Me.clcFolio.TabIndex = 1
        Me.clcFolio.Tag = "folio"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(496, 40)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblElabora
        '
        Me.lblElabora.AutoSize = True
        Me.lblElabora.Location = New System.Drawing.Point(57, 64)
        Me.lblElabora.Name = "lblElabora"
        Me.lblElabora.Size = New System.Drawing.Size(50, 16)
        Me.lblElabora.TabIndex = 2
        Me.lblElabora.Tag = ""
        Me.lblElabora.Text = "&Elabora:"
        Me.lblElabora.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(493, 64)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 8
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "&Status:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "P"
        Me.cboStatus.Location = New System.Drawing.Point(552, 62)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Enabled = False
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelado", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("En Proceso", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Recuperado", "R", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(96, 20)
        Me.cboStatus.TabIndex = 9
        Me.cboStatus.Tag = "status"
        '
        'lblBodega_Recibe
        '
        Me.lblBodega_Recibe.AutoSize = True
        Me.lblBodega_Recibe.Location = New System.Drawing.Point(11, 16)
        Me.lblBodega_Recibe.Name = "lblBodega_Recibe"
        Me.lblBodega_Recibe.Size = New System.Drawing.Size(87, 16)
        Me.lblBodega_Recibe.TabIndex = 10
        Me.lblBodega_Recibe.Tag = ""
        Me.lblBodega_Recibe.Text = "&Bodega recibe:"
        Me.lblBodega_Recibe.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPersona_Recibe
        '
        Me.lblPersona_Recibe.AutoSize = True
        Me.lblPersona_Recibe.Location = New System.Drawing.Point(360, 16)
        Me.lblPersona_Recibe.Name = "lblPersona_Recibe"
        Me.lblPersona_Recibe.Size = New System.Drawing.Size(90, 16)
        Me.lblPersona_Recibe.TabIndex = 12
        Me.lblPersona_Recibe.Tag = ""
        Me.lblPersona_Recibe.Text = "&Persona recibe:"
        Me.lblPersona_Recibe.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha_Recibe
        '
        Me.lblFecha_Recibe.AutoSize = True
        Me.lblFecha_Recibe.Location = New System.Drawing.Point(24, 40)
        Me.lblFecha_Recibe.Name = "lblFecha_Recibe"
        Me.lblFecha_Recibe.Size = New System.Drawing.Size(78, 16)
        Me.lblFecha_Recibe.TabIndex = 14
        Me.lblFecha_Recibe.Tag = ""
        Me.lblFecha_Recibe.Text = "&Fecha recibe:"
        Me.lblFecha_Recibe.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Recibe
        '
        Me.dteFecha_Recibe.EditValue = "11/10/2007"
        Me.dteFecha_Recibe.Location = New System.Drawing.Point(104, 40)
        Me.dteFecha_Recibe.Name = "dteFecha_Recibe"
        '
        'dteFecha_Recibe.Properties
        '
        Me.dteFecha_Recibe.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Recibe.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Recibe.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Recibe.Properties.Enabled = False
        Me.dteFecha_Recibe.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Recibe.TabIndex = 15
        Me.dteFecha_Recibe.Tag = "fecha_recibe"
        '
        'lblPersona_Recoge
        '
        Me.lblPersona_Recoge.AutoSize = True
        Me.lblPersona_Recoge.Location = New System.Drawing.Point(8, 16)
        Me.lblPersona_Recoge.Name = "lblPersona_Recoge"
        Me.lblPersona_Recoge.Size = New System.Drawing.Size(94, 16)
        Me.lblPersona_Recoge.TabIndex = 0
        Me.lblPersona_Recoge.Tag = ""
        Me.lblPersona_Recoge.Text = "&Persona recoge:"
        Me.lblPersona_Recoge.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPersona_Inspecciona
        '
        Me.lblPersona_Inspecciona.AutoSize = True
        Me.lblPersona_Inspecciona.Location = New System.Drawing.Point(38, 16)
        Me.lblPersona_Inspecciona.Name = "lblPersona_Inspecciona"
        Me.lblPersona_Inspecciona.Size = New System.Drawing.Size(120, 16)
        Me.lblPersona_Inspecciona.TabIndex = 24
        Me.lblPersona_Inspecciona.Tag = ""
        Me.lblPersona_Inspecciona.Text = "&Persona inspecciona:"
        Me.lblPersona_Inspecciona.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha_Inspecciona
        '
        Me.lblFecha_Inspecciona.AutoSize = True
        Me.lblFecha_Inspecciona.Location = New System.Drawing.Point(424, 16)
        Me.lblFecha_Inspecciona.Name = "lblFecha_Inspecciona"
        Me.lblFecha_Inspecciona.Size = New System.Drawing.Size(109, 16)
        Me.lblFecha_Inspecciona.TabIndex = 26
        Me.lblFecha_Inspecciona.Tag = ""
        Me.lblFecha_Inspecciona.Text = "&Fecha inspecciona:"
        Me.lblFecha_Inspecciona.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Inspecciona
        '
        Me.dteFecha_Inspecciona.EditValue = "11/10/2007"
        Me.dteFecha_Inspecciona.Location = New System.Drawing.Point(544, 16)
        Me.dteFecha_Inspecciona.Name = "dteFecha_Inspecciona"
        '
        'dteFecha_Inspecciona.Properties
        '
        Me.dteFecha_Inspecciona.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Inspecciona.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Inspecciona.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Inspecciona.Properties.Enabled = False
        Me.dteFecha_Inspecciona.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Inspecciona.TabIndex = 27
        Me.dteFecha_Inspecciona.Tag = "fecha_inspecciona"
        '
        'lblObservaciones_Inspeccion
        '
        Me.lblObservaciones_Inspeccion.AutoSize = True
        Me.lblObservaciones_Inspeccion.Location = New System.Drawing.Point(8, 40)
        Me.lblObservaciones_Inspeccion.Name = "lblObservaciones_Inspeccion"
        Me.lblObservaciones_Inspeccion.Size = New System.Drawing.Size(150, 16)
        Me.lblObservaciones_Inspeccion.TabIndex = 28
        Me.lblObservaciones_Inspeccion.Tag = ""
        Me.lblObservaciones_Inspeccion.Text = "&Observaciones inspecci�n:"
        Me.lblObservaciones_Inspeccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones_Inspeccion
        '
        Me.txtObservaciones_Inspeccion.EditValue = ""
        Me.txtObservaciones_Inspeccion.Location = New System.Drawing.Point(168, 40)
        Me.txtObservaciones_Inspeccion.Name = "txtObservaciones_Inspeccion"
        '
        'txtObservaciones_Inspeccion.Properties
        '
        Me.txtObservaciones_Inspeccion.Properties.Enabled = False
        Me.txtObservaciones_Inspeccion.Size = New System.Drawing.Size(464, 38)
        Me.txtObservaciones_Inspeccion.TabIndex = 29
        Me.txtObservaciones_Inspeccion.Tag = "observaciones_inspeccion"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(10, 42)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 3
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(112, 40)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(520, 32)
        Me.txtObservaciones.TabIndex = 4
        Me.txtObservaciones.Tag = "observaciones"
        '
        'grOrdenesRecuperacion
        '
        '
        'grOrdenesRecuperacion.EmbeddedNavigator
        '
        Me.grOrdenesRecuperacion.EmbeddedNavigator.Name = ""
        Me.grOrdenesRecuperacion.Location = New System.Drawing.Point(8, 440)
        Me.grOrdenesRecuperacion.MainView = Me.grvOrdenesRecuperacion
        Me.grOrdenesRecuperacion.Name = "grOrdenesRecuperacion"
        Me.grOrdenesRecuperacion.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.grOrdenesRecuperacion.Size = New System.Drawing.Size(640, 152)
        Me.grOrdenesRecuperacion.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grOrdenesRecuperacion.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesRecuperacion.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesRecuperacion.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesRecuperacion.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesRecuperacion.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grOrdenesRecuperacion.TabIndex = 14
        Me.grOrdenesRecuperacion.TabStop = False
        Me.grOrdenesRecuperacion.Text = "OrdenesRecuperacion"
        '
        'grvOrdenesRecuperacion
        '
        Me.grvOrdenesRecuperacion.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcFolio, Me.grcArticulo, Me.grcDescripcion, Me.grcPartida, Me.grcCantidad, Me.grcSerie, Me.grcCosto, Me.grcModelo})
        Me.grvOrdenesRecuperacion.GridControl = Me.grOrdenesRecuperacion
        Me.grvOrdenesRecuperacion.Name = "grvOrdenesRecuperacion"
        Me.grvOrdenesRecuperacion.OptionsCustomization.AllowFilter = False
        Me.grvOrdenesRecuperacion.OptionsCustomization.AllowGroup = False
        Me.grvOrdenesRecuperacion.OptionsCustomization.AllowSort = False
        Me.grvOrdenesRecuperacion.OptionsView.ShowGroupPanel = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.Caption = "Seleccionar"
        Me.grcSeleccionar.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 74
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.Width = 60
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 2
        Me.grcArticulo.Width = 66
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion_articulo"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 4
        Me.grcDescripcion.Width = 192
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.VisibleIndex = 1
        Me.grcPartida.Width = 57
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 5
        Me.grcCantidad.Width = 82
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.Width = 49
        '
        'grcCosto
        '
        Me.grcCosto.Caption = "Costo"
        Me.grcCosto.DisplayFormat.FormatString = "c2"
        Me.grcCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCosto.FieldName = "costo"
        Me.grcCosto.Name = "grcCosto"
        Me.grcCosto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCosto.VisibleIndex = 6
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 3
        Me.grcModelo.Width = 80
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(51, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "S&ucursal:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblPersona_Recibe)
        Me.GroupBox1.Controls.Add(Me.lblBodega_Recibe)
        Me.GroupBox1.Controls.Add(Me.lblFecha_Recibe)
        Me.GroupBox1.Controls.Add(Me.dteFecha_Recibe)
        Me.GroupBox1.Controls.Add(Me.lkpPersonaRecibe)
        Me.GroupBox1.Controls.Add(Me.lkpBodegaRecibe)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 264)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(640, 72)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Recibe:"
        '
        'lkpPersonaRecibe
        '
        Me.lkpPersonaRecibe.AllowAdd = False
        Me.lkpPersonaRecibe.AutoReaload = True
        Me.lkpPersonaRecibe.DataSource = Nothing
        Me.lkpPersonaRecibe.DefaultSearchField = ""
        Me.lkpPersonaRecibe.DisplayMember = "nombre"
        Me.lkpPersonaRecibe.EditValue = Nothing
        Me.lkpPersonaRecibe.Enabled = False
        Me.lkpPersonaRecibe.Filtered = False
        Me.lkpPersonaRecibe.InitValue = Nothing
        Me.lkpPersonaRecibe.Location = New System.Drawing.Point(456, 16)
        Me.lkpPersonaRecibe.MultiSelect = False
        Me.lkpPersonaRecibe.Name = "lkpPersonaRecibe"
        Me.lkpPersonaRecibe.NullText = ""
        Me.lkpPersonaRecibe.PopupWidth = CType(400, Long)
        Me.lkpPersonaRecibe.ReadOnlyControl = False
        Me.lkpPersonaRecibe.Required = False
        Me.lkpPersonaRecibe.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPersonaRecibe.SearchMember = ""
        Me.lkpPersonaRecibe.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPersonaRecibe.SelectAll = True
        Me.lkpPersonaRecibe.Size = New System.Drawing.Size(176, 20)
        Me.lkpPersonaRecibe.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPersonaRecibe.TabIndex = 64
        Me.lkpPersonaRecibe.Tag = "persona_recibe"
        Me.lkpPersonaRecibe.ToolTip = "Seleccione un cliente"
        Me.lkpPersonaRecibe.ValueMember = "usuario"
        '
        'lkpBodegaRecibe
        '
        Me.lkpBodegaRecibe.AllowAdd = False
        Me.lkpBodegaRecibe.AutoReaload = False
        Me.lkpBodegaRecibe.DataSource = Nothing
        Me.lkpBodegaRecibe.DefaultSearchField = ""
        Me.lkpBodegaRecibe.DisplayMember = "descripcion"
        Me.lkpBodegaRecibe.EditValue = Nothing
        Me.lkpBodegaRecibe.Enabled = False
        Me.lkpBodegaRecibe.Filtered = False
        Me.lkpBodegaRecibe.InitValue = Nothing
        Me.lkpBodegaRecibe.Location = New System.Drawing.Point(104, 16)
        Me.lkpBodegaRecibe.MultiSelect = False
        Me.lkpBodegaRecibe.Name = "lkpBodegaRecibe"
        Me.lkpBodegaRecibe.NullText = ""
        Me.lkpBodegaRecibe.PopupWidth = CType(400, Long)
        Me.lkpBodegaRecibe.ReadOnlyControl = False
        Me.lkpBodegaRecibe.Required = False
        Me.lkpBodegaRecibe.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaRecibe.SearchMember = ""
        Me.lkpBodegaRecibe.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaRecibe.SelectAll = False
        Me.lkpBodegaRecibe.Size = New System.Drawing.Size(224, 20)
        Me.lkpBodegaRecibe.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaRecibe.TabIndex = 59
        Me.lkpBodegaRecibe.Tag = "bodega_recibe"
        Me.lkpBodegaRecibe.ToolTip = Nothing
        Me.lkpBodegaRecibe.ValueMember = "Bodega"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lkpPersonaInspecciona)
        Me.GroupBox2.Controls.Add(Me.lblPersona_Inspecciona)
        Me.GroupBox2.Controls.Add(Me.dteFecha_Inspecciona)
        Me.GroupBox2.Controls.Add(Me.lblFecha_Inspecciona)
        Me.GroupBox2.Controls.Add(Me.lblObservaciones_Inspeccion)
        Me.GroupBox2.Controls.Add(Me.txtObservaciones_Inspeccion)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 344)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(640, 88)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Inspecciona:"
        '
        'lkpPersonaInspecciona
        '
        Me.lkpPersonaInspecciona.AllowAdd = False
        Me.lkpPersonaInspecciona.AutoReaload = True
        Me.lkpPersonaInspecciona.DataSource = Nothing
        Me.lkpPersonaInspecciona.DefaultSearchField = ""
        Me.lkpPersonaInspecciona.DisplayMember = "nombre"
        Me.lkpPersonaInspecciona.EditValue = Nothing
        Me.lkpPersonaInspecciona.Enabled = False
        Me.lkpPersonaInspecciona.Filtered = False
        Me.lkpPersonaInspecciona.InitValue = Nothing
        Me.lkpPersonaInspecciona.Location = New System.Drawing.Point(168, 16)
        Me.lkpPersonaInspecciona.MultiSelect = False
        Me.lkpPersonaInspecciona.Name = "lkpPersonaInspecciona"
        Me.lkpPersonaInspecciona.NullText = ""
        Me.lkpPersonaInspecciona.PopupWidth = CType(400, Long)
        Me.lkpPersonaInspecciona.ReadOnlyControl = False
        Me.lkpPersonaInspecciona.Required = False
        Me.lkpPersonaInspecciona.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPersonaInspecciona.SearchMember = ""
        Me.lkpPersonaInspecciona.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPersonaInspecciona.SelectAll = True
        Me.lkpPersonaInspecciona.Size = New System.Drawing.Size(168, 20)
        Me.lkpPersonaInspecciona.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPersonaInspecciona.TabIndex = 25
        Me.lkpPersonaInspecciona.Tag = "persona_inspecciona"
        Me.lkpPersonaInspecciona.ToolTip = "Seleccione un cliente"
        Me.lkpPersonaInspecciona.ValueMember = "usuario"
        '
        'lkpPersonaRecoge
        '
        Me.lkpPersonaRecoge.AllowAdd = False
        Me.lkpPersonaRecoge.AutoReaload = True
        Me.lkpPersonaRecoge.DataSource = Nothing
        Me.lkpPersonaRecoge.DefaultSearchField = ""
        Me.lkpPersonaRecoge.DisplayMember = "nombre"
        Me.lkpPersonaRecoge.EditValue = Nothing
        Me.lkpPersonaRecoge.Filtered = False
        Me.lkpPersonaRecoge.InitValue = Nothing
        Me.lkpPersonaRecoge.Location = New System.Drawing.Point(112, 14)
        Me.lkpPersonaRecoge.MultiSelect = False
        Me.lkpPersonaRecoge.Name = "lkpPersonaRecoge"
        Me.lkpPersonaRecoge.NullText = ""
        Me.lkpPersonaRecoge.PopupWidth = CType(400, Long)
        Me.lkpPersonaRecoge.ReadOnlyControl = False
        Me.lkpPersonaRecoge.Required = False
        Me.lkpPersonaRecoge.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPersonaRecoge.SearchMember = ""
        Me.lkpPersonaRecoge.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPersonaRecoge.SelectAll = True
        Me.lkpPersonaRecoge.Size = New System.Drawing.Size(224, 20)
        Me.lkpPersonaRecoge.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPersonaRecoge.TabIndex = 1
        Me.lkpPersonaRecoge.Tag = "persona_recoje"
        Me.lkpPersonaRecoge.ToolTip = "Seleccione un cliente"
        Me.lkpPersonaRecoge.ValueMember = "usuario"
        '
        'gpbFactura
        '
        Me.gpbFactura.Controls.Add(Me.Label5)
        Me.gpbFactura.Controls.Add(Me.lblSucursal)
        Me.gpbFactura.Controls.Add(Me.lblOrden)
        Me.gpbFactura.Controls.Add(Me.lkpFolio)
        Me.gpbFactura.Controls.Add(Me.lkpSucursal)
        Me.gpbFactura.Controls.Add(Me.lkpSerie)
        Me.gpbFactura.Location = New System.Drawing.Point(8, 120)
        Me.gpbFactura.Name = "gpbFactura"
        Me.gpbFactura.Size = New System.Drawing.Size(640, 48)
        Me.gpbFactura.TabIndex = 10
        Me.gpbFactura.TabStop = False
        Me.gpbFactura.Text = "Factura:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(472, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 17)
        Me.Label5.TabIndex = 69
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Folio:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(40, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 65
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(296, 16)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 67
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFolio
        '
        Me.lkpFolio.AllowAdd = False
        Me.lkpFolio.AutoReaload = False
        Me.lkpFolio.DataSource = Nothing
        Me.lkpFolio.DefaultSearchField = ""
        Me.lkpFolio.DisplayMember = "folio"
        Me.lkpFolio.EditValue = Nothing
        Me.lkpFolio.Filtered = False
        Me.lkpFolio.InitValue = Nothing
        Me.lkpFolio.Location = New System.Drawing.Point(520, 14)
        Me.lkpFolio.MultiSelect = False
        Me.lkpFolio.Name = "lkpFolio"
        Me.lkpFolio.NullText = ""
        Me.lkpFolio.PopupWidth = CType(350, Long)
        Me.lkpFolio.ReadOnlyControl = False
        Me.lkpFolio.Required = False
        Me.lkpFolio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolio.SearchMember = ""
        Me.lkpFolio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolio.SelectAll = False
        Me.lkpFolio.Size = New System.Drawing.Size(112, 20)
        Me.lkpFolio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolio.TabIndex = 70
        Me.lkpFolio.Tag = "folio_factura"
        Me.lkpFolio.ToolTip = Nothing
        Me.lkpFolio.ValueMember = "folio"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(104, 14)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(168, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 66
        Me.lkpSucursal.Tag = "sucursal_factura"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lkpSerie
        '
        Me.lkpSerie.AllowAdd = False
        Me.lkpSerie.AutoReaload = False
        Me.lkpSerie.DataSource = Nothing
        Me.lkpSerie.DefaultSearchField = ""
        Me.lkpSerie.DisplayMember = "serie"
        Me.lkpSerie.EditValue = Nothing
        Me.lkpSerie.Filtered = False
        Me.lkpSerie.InitValue = Nothing
        Me.lkpSerie.Location = New System.Drawing.Point(336, 14)
        Me.lkpSerie.MultiSelect = False
        Me.lkpSerie.Name = "lkpSerie"
        Me.lkpSerie.NullText = ""
        Me.lkpSerie.PopupWidth = CType(350, Long)
        Me.lkpSerie.ReadOnlyControl = False
        Me.lkpSerie.Required = False
        Me.lkpSerie.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSerie.SearchMember = ""
        Me.lkpSerie.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSerie.SelectAll = False
        Me.lkpSerie.Size = New System.Drawing.Size(112, 20)
        Me.lkpSerie.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSerie.TabIndex = 68
        Me.lkpSerie.Tag = "serie_factura"
        Me.lkpSerie.ToolTip = Nothing
        Me.lkpSerie.ValueMember = "serie"
        '
        'gpbRecoge
        '
        Me.gpbRecoge.Controls.Add(Me.lkpPersonaRecoge)
        Me.gpbRecoge.Controls.Add(Me.lblObservaciones)
        Me.gpbRecoge.Controls.Add(Me.lblPersona_Recoge)
        Me.gpbRecoge.Controls.Add(Me.txtObservaciones)
        Me.gpbRecoge.Location = New System.Drawing.Point(8, 176)
        Me.gpbRecoge.Name = "gpbRecoge"
        Me.gpbRecoge.Size = New System.Drawing.Size(640, 80)
        Me.gpbRecoge.TabIndex = 11
        Me.gpbRecoge.TabStop = False
        Me.gpbRecoge.Text = "Recoge:"
        '
        'lblPersonaElabora
        '
        Me.lblPersonaElabora.Enabled = False
        Me.lblPersonaElabora.Location = New System.Drawing.Point(120, 64)
        Me.lblPersonaElabora.Name = "lblPersonaElabora"
        Me.lblPersonaElabora.Size = New System.Drawing.Size(368, 20)
        Me.lblPersonaElabora.TabIndex = 3
        Me.lblPersonaElabora.Tag = "elabora"
        '
        'lblSucursalElabora
        '
        Me.lblSucursalElabora.Enabled = False
        Me.lblSucursalElabora.Location = New System.Drawing.Point(120, 88)
        Me.lblSucursalElabora.Name = "lblSucursalElabora"
        Me.lblSucursalElabora.Size = New System.Drawing.Size(368, 20)
        Me.lblSucursalElabora.TabIndex = 5
        Me.lblSucursalElabora.Tag = "sucursal"
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "11/10/2007"
        Me.dteFecha.Location = New System.Drawing.Point(560, 39)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha.TabIndex = 59
        Me.dteFecha.Tag = "fecha"
        '
        'chkFacturaCancelada
        '
        Me.chkFacturaCancelada.Enabled = False
        Me.chkFacturaCancelada.Location = New System.Drawing.Point(520, 88)
        Me.chkFacturaCancelada.Name = "chkFacturaCancelada"
        Me.chkFacturaCancelada.Size = New System.Drawing.Size(128, 24)
        Me.chkFacturaCancelada.TabIndex = 60
        Me.chkFacturaCancelada.Tag = "cancelada"
        Me.chkFacturaCancelada.Text = "Factura Cancelada"
        '
        'tbrReimprimir
        '
        Me.tbrReimprimir.Enabled = False
        Me.tbrReimprimir.Text = "Reimprimir"
        Me.tbrReimprimir.Visible = False
        '
        'frmOrdenesRecuperacion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(658, 600)
        Me.Controls.Add(Me.chkFacturaCancelada)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblElabora)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.grOrdenesRecuperacion)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblSucursalElabora)
        Me.Controls.Add(Me.lblPersonaElabora)
        Me.Controls.Add(Me.gpbRecoge)
        Me.Controls.Add(Me.gpbFactura)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.cboStatus)
        Me.Name = "frmOrdenesRecuperacion"
        Me.Text = "Ordenes de Recuperaci�n"
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.gpbFactura, 0)
        Me.Controls.SetChildIndex(Me.gpbRecoge, 0)
        Me.Controls.SetChildIndex(Me.lblPersonaElabora, 0)
        Me.Controls.SetChildIndex(Me.lblSucursalElabora, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.grOrdenesRecuperacion, 0)
        Me.Controls.SetChildIndex(Me.lblStatus, 0)
        Me.Controls.SetChildIndex(Me.lblElabora, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.chkFacturaCancelada, 0)
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Recibe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Inspecciona.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones_Inspeccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grOrdenesRecuperacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvOrdenesRecuperacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.gpbFactura.ResumeLayout(False)
        Me.gpbRecoge.ResumeLayout(False)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oOrdenesRecuperacion As VillarrealBusiness.clsOrdenesRecuperacion
    Private oOrdenesRecuperacionDetalle As VillarrealBusiness.clsOrdenesRecuperacionDetalle
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle
    Private oUsuarios As VillarrealBusiness.clsUsuarios
    Private oBodegas As New VillarrealBusiness.clsBodegas

    Private ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property folio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolio)
        End Get
    End Property
    Private ReadOnly Property serie() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie)
        End Get
    End Property


    Private Property PersonaRecoge() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpPersonaRecoge)
        End Get
        Set(ByVal Value As String)
            Me.lkpPersonaRecoge.EditValue = Value
        End Set
    End Property
    Private Property PersonaInspecciona() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpPersonaInspecciona)
        End Get
        Set(ByVal Value As String)
            Me.lkpPersonaInspecciona.EditValue = Value
        End Set
    End Property
    Private Property PersonaRecibe() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpPersonaRecibe)
        End Get
        Set(ByVal Value As String)
            Me.lkpPersonaRecibe.EditValue = Value
        End Set
    End Property

    Private ReadOnly Property folio_recuperacion() As Long
        Get
            Return clcFolio.Value
        End Get
    End Property



#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    

    Private Sub frmOrdenesRecuperacion_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        'Try
        TinApp.Connection.Begin()
        Select Case Action
            Case Actions.Insert


                Response = oOrdenesRecuperacion.Insertar(Me.DataSource, Me.TraeSucursalActual, CType(Comunes.TinApp.Connection.UserName(), String))
                If Response.ErrorFound = False Then
                    Me.GuardaDetalle(Response)
                    If Response.ErrorFound Then
                        TinApp.Connection.Rollback()
                        Response.ShowMessage()
                        Exit Sub
                    End If
                Else
                    TinApp.Connection.Rollback()
                    Response.ShowMessage()
                    Exit Sub
                End If

            Case Actions.Update
                Response = oOrdenesRecuperacion.Actualizar(Me.DataSource, PersonaRecibe, PersonaRecoge, PersonaInspecciona)
                If Response.ErrorFound = False Then

                    Me.GuardaDetalle(Response)
                    If Response.ErrorFound Then
                        TinApp.Connection.Rollback()
                        Response.ShowMessage()
                        Exit Sub
                    End If
                Else
                    TinApp.Connection.Rollback()
                    Response.ShowMessage()
                    Exit Sub
                End If


            Case Actions.Delete
                'Me.GuardaDetalle(Response)
                'If Response.ErrorFound = False Then
                Response = oOrdenesRecuperacion.Eliminar(clcFolio.Value)
                If Response.ErrorFound Then
                    TinApp.Connection.Rollback()
                    Response.ShowMessage()
                    Exit Sub
                End If
                'Else
                '    TinApp.Connection.Rollback()
                '    Response.ShowMessage()
                '    Exit Sub
                'End If
        End Select


        'TERMINA LA TRANSACCION
        TinApp.Connection.Commit()
        Me.ImprimirVale()
        'Catch ex As Exception
        '    ShowMessage(MessageType.MsgInformation, "Ha ocurrido un error al guardar los datos")
        'End Try







    End Sub

    Private Sub frmOrdenesRecuperacion_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Dim oDataSet As DataSet
        Response = oOrdenesRecuperacion.DespliegaDatos(OwnerForm.Value("folio"))
        If Not Response.ErrorFound Then

            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If


        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpFolio.EditValue = CType(Me.lkpFolio.DisplayText, Long)
                If IsDBNull(oDataSet.Tables(0).Rows(0).Item("bodega_recibe")) = False Then
                    If CType(oDataSet.Tables(0).Rows(0).Item("bodega_recibe"), String).Trim.Length > 0 Then
                        Me.lkpBodegaRecibe_Load(True, 1)
                        Me.lkpBodegaRecibe.EditValue = oDataSet.Tables(0).Rows(0).Item("bodega_recibe")

                    End If
                End If

                VerificaBodegaXUsuario(oDataSet.Tables(0).Rows(0).Item("persona_recibe"))
                'Me.grOrdenesRecuperacion.Enabled = False
            Case Actions.Delete
                Me.lkpFolio.EditValue = CType(Me.lkpFolio.DisplayText, Long)
                'Me.grOrdenesRecuperacion.Enabled = False
        End Select
    End Sub

    Private Sub frmOrdenesRecuperacion_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oOrdenesRecuperacion = New VillarrealBusiness.clsOrdenesRecuperacion
        oOrdenesRecuperacionDetalle = New VillarrealBusiness.clsOrdenesRecuperacionDetalle
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        oUsuarios = New VillarrealBusiness.clsUsuarios
        oBodegas = New VillarrealBusiness.clsBodegas

        'With Me.tmaOrdenesRecuperacion
        '    .UpdateTitle = "un Art�culo"
        '    .UpdateForm = New frmOrdenesRecuperacionDetalle
        '    .AddColumn("seleccionar", "System.Boolean")
        '    .AddColumn("articulo", "System.Int32")
        '    .AddColumn("partida", "System.Int32")
        '    .AddColumn("cantidad", "System.Int32")
        '    .AddColumn("serie")
        '    .AddColumn("costo", "System.Int32")
        'End With
        Me.dteFecha.EditValue = Comunes.TinApp.FechaServidor
        Me.dteFecha_Inspecciona.EditValue = Comunes.TinApp.FechaServidor
        Me.dteFecha_Recibe.EditValue = Comunes.TinApp.FechaServidor

        Select Case Action
            Case Actions.Insert
                Me.lkpPersonaRecibe_LoadData(True)
                Me.lkpBodegaRecibe_LoadData(True)
                Me.lkpPersonaRecoge_LoadData(True)
                Me.lkpPersonaInspecciona_LoadData(True)
                TraeDatosElaboracion()
                '**********************************
                Me.lkpBodegaRecibe.Enabled = False
                Me.lkpBodegaRecibe.EditValue = -1
                Me.lkpPersonaRecibe.Enabled = False
                Me.lkpPersonaRecibe.EditValue = ""
                Me.lkpSucursal.Enabled = True
                Me.lkpSerie.Enabled = True
                Me.lkpFolio.Enabled = True
                Me.lkpPersonaInspecciona.Enabled = False
                Me.lkpPersonaInspecciona.EditValue = ""
                Me.dteFecha_Inspecciona.Enabled = False
                Me.txtObservaciones_Inspeccion.Enabled = False
                '**********************************
            Case Actions.Update
                '**********************************
                Me.lkpBodegaRecibe.Enabled = True
                Me.lkpPersonaRecibe.Enabled = False
                Me.lkpSucursal.Enabled = False
                Me.lkpSerie.Enabled = False
                Me.lkpFolio.Enabled = False
                Me.lkpPersonaInspecciona.Enabled = True
                Me.dteFecha_Inspecciona.Enabled = True
                Me.txtObservaciones_Inspeccion.Enabled = True
                '**********************************
            Case Actions.Delete
                '**********************************
                Me.lkpBodegaRecibe.Enabled = False
                Me.lkpPersonaRecibe.Enabled = False
                Me.lkpSucursal.Enabled = False
                Me.lkpSerie.Enabled = False
                Me.lkpFolio.Enabled = False
                Me.lkpPersonaInspecciona.Enabled = False
                Me.dteFecha_Inspecciona.Enabled = False
                Me.txtObservaciones_Inspeccion.Enabled = False
                '**********************************
        End Select

    End Sub

    Private Sub frmOrdenesRecuperacion_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Select Case Action
            Case Actions.Insert
                Response = oOrdenesRecuperacion.Validacion(Action, Me.lkpSucursal.EditValue, Me.lkpSerie.EditValue, Me.lkpFolio.EditValue)
                If Not Response.ErrorFound Then
                    Response = ValidaGrid()
                End If
            Case Actions.Update
                Response = ValidaGrid()
            Case Actions.Delete
        End Select

    End Sub

    Private Sub frmOrdenesRecuperacion_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

    Private Sub frmOrdenesRecuperacion_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        'With Me.tmaOrdenesRecuperacion
        With Me.grvOrdenesRecuperacion
            .MoveFirst()
            Dim i As Integer
            For i = 0 To Me.grvOrdenesRecuperacion.RowCount - 1
                Select Case Me.Action
                    Case Actions.Insert 'And .Item("cantidad") > 0
                        If (CType(Me.grvOrdenesRecuperacion.GetRowCellValue(i, Me.grcSeleccionar), Boolean) = True) Then
                            Response = oOrdenesRecuperacionDetalle.Insertar(CType(Me.grvOrdenesRecuperacion.GetDataRow(i), DataRow), Me.clcFolio.EditValue)
                        End If

                    Case Actions.Update
                        If (CType(Me.grvOrdenesRecuperacion.GetRowCellValue(i, Me.grcSeleccionar), Boolean) = True) Then
                            Response = oOrdenesRecuperacionDetalle.Actualizar(CType(Me.grvOrdenesRecuperacion.GetDataRow(i), DataRow), Me.clcFolio.EditValue)
                        End If
                    Case Actions.Delete
                        '    Response = oOrdenesRecuperacionDetalle.Eliminar(Me.clcOrden.Value, .Item("Partida"), .Item("cantidad"), .Item("articulo"), lkpBodega.EditValue, Me.chkSobrePedido.Checked)
                End Select

                ''Me.grvMovimientos.SetRowCellValue(i, Me.grcProgramar, False)
            Next
            'Do While Not .EOF

            '    Select Case .CurrentAction
            '        Case Actions.Insert And .Item("cantidad") > 0
            '            Response = oOrdenesRecuperacion.Insertar(.SelectedRow, clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)

            '            If Not Response.ErrorFound And .Item("sobre_pedido") = True Then

            '                Response = Me.oOrdenesRecuperacion.ActualizarVentaDetalleSobrePedidoOC(clcOrden.EditValue, .Item("folios_ventas"))
            '            End If

            '        Case Actions.Update
            '            Response = oOrdenesRecuperacionDetalle.Actualizar(.SelectedRow, clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)
            '        Case Actions.Delete
            '            Response = oOrdenesRecuperacionDetalle.Eliminar(Me.clcOrden.Value, .Item("Partida"), .Item("cantidad"), .Item("articulo"), lkpBodega.EditValue, Me.chkSobrePedido.Checked)
            '    End Select
            '    .MoveNext()
            'Loop
        End With

        'If Me.Action = Actions.Insert And Not Response.ErrorFound Then
        '    ImprimeOrden(Me.clcOrden.Value)
        'End If
    End Sub


#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub chkFacturaCancelada_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFacturaCancelada.CheckedChanged
        If Me.chkFacturaCancelada.Checked Then
            Me.tbrTools.Buttons(0).Enabled = False
        Else
            Me.tbrTools.Buttons(0).Enabled = True
        End If
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tbrReimprimir Then
            Me.ImprimirVale()
        End If

    End Sub

#Region "DIPROS Systems, LookUps"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        'Select Case Action
        '    Case Actions.Insert
        lkpSerie_LoadData(True)
        'End Select
    End Sub

    'Private Sub lkpSucursalElabora_Format()
    '    Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursalElabora)
    'End Sub
    'Private Sub lkpSucursalElabora_LoadData(ByVal Initialize As Boolean)
    '    Dim Response As New Events
    '    Response = oVentas.LookupSucursalVentas
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpSucursalElabora.DataSource = oDataSet.Tables(0)
    '        oDataSet = Nothing
    '    End If
    '    Response = Nothing
    'End Sub
    'Private Sub lkpSucursalElabora_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs)
    ''lkpSerie_LoadData(True)
    'End Sub

    Private Sub lkpSerie_LoadData(ByVal Initialize As Boolean) Handles lkpSerie.LoadData
        Dim response As Events
        response = oVentas.LookupSerieVentas(sucursal, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSerie.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpSerie_Format() Handles lkpSerie.Format
        Comunes.clsFormato.for_series_ventas_grl(Me.lkpSerie)
    End Sub
    Private Sub lkpSerie_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSerie.EditValueChanged
        'Select Case Action
        '    Case Actions.Insert
        lkpFolio_LoadData(True)
        'End Select
    End Sub

    Private Sub lkpFolio_LoadData(ByVal Initialize As Boolean) Handles lkpFolio.LoadData
        Dim response As Events
        response = oVentas.LookupFolioVentas(sucursal, serie, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolio.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpfolio_Format() Handles lkpFolio.Format
        Comunes.clsFormato.for_folio_ventas_grl(Me.lkpFolio)
    End Sub
    Private Sub lkpFolio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolio.EditValueChanged
        'Select Case Action
        '    Case Actions.Insert
        traerDatos()
        'End Select
    End Sub

    'Private Sub lkpPersonaElabora_Format()
    '    Comunes.clsFormato.for_usuarios_grl(Me.lkpPersonaElabora)
    'End Sub
    'Private Sub lkpPersonaElabora_LoadData(ByVal Initialize As Boolean)
    '    Dim Response As New Events
    '    Response = oUsuarios.Lookup()
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpPersonaElabora.DataSource = oDataSet.Tables(0)
    '        oDataSet = Nothing
    '    End If
    '    Response = Nothing
    'End Sub
    'Private Sub lkpPersonaElabora_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs)
    '    'lkpSerie_LoadData(True)
    'End Sub

    Private Sub lkpPersonaInspecciona_Format() Handles lkpPersonaInspecciona.Format
        Comunes.clsFormato.for_usuarios_grl(Me.lkpPersonaInspecciona)
    End Sub
    Private Sub lkpPersonaInspecciona_LoadData(ByVal Initialize As Boolean) Handles lkpPersonaInspecciona.LoadData
        Dim Response As New Events
        Response = oUsuarios.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPersonaInspecciona.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpPersonaInspecciona_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpPersonaInspecciona.EditValueChanged
        'lkpSerie_LoadData(True)
        Me.PersonaInspecciona = Me.lkpPersonaInspecciona.EditValue
    End Sub

    Private Sub lkpPersonaRecibe_Format() Handles lkpPersonaRecibe.Format
        Comunes.clsFormato.for_usuarios_grl(Me.lkpPersonaRecibe)
    End Sub
    Private Sub lkpPersonaRecibe_LoadData(ByVal Initialize As Boolean) Handles lkpPersonaRecibe.LoadData
        Dim Response As New Events
        Response = oUsuarios.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPersonaRecibe.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpPersonaRecibe_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpPersonaRecibe.EditValueChanged
        'lkpSerie_LoadData(True)
        'Me.PersonaRecibe = Me.lkpPersonaRecibe.EditValue

    End Sub

    Private Sub lkpPersonaRecoge_Format() Handles lkpPersonaRecoge.Format
        Comunes.clsFormato.for_usuarios_grl(Me.lkpPersonaRecoge)
    End Sub
    Private Sub lkpPersonaRecoge_LoadData(ByVal Initialize As Boolean) Handles lkpPersonaRecoge.LoadData
        Dim Response As New Events
        Response = oUsuarios.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPersonaRecoge.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpPersonaRecoge_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpPersonaRecoge.EditValueChanged
        'lkpSerie_LoadData(True)
        Me.PersonaRecoge = Me.lkpPersonaRecoge.EditValue
    End Sub

    Private Sub lkpBodegaRecibe_Format() Handles lkpBodegaRecibe.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodegaRecibe)
    End Sub

    Private Sub lkpBodegaRecibe_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaRecibe.LoadData
        Dim Response As New Events

        Response = oBodegas.Lookup

        Response = oBodegas.LookupBodegasUsuarios(TinApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaRecibe.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaRecibe_Load(ByVal Initialize As Boolean, Optional ByVal bodega_recibe_ya_capturada As Long = -1)
        Dim Response As New Events
        If bodega_recibe_ya_capturada = -1 Then
            Response = oBodegas.LookupBodegasUsuarios(TinApp.Connection.User)
        Else
            Response = oBodegas.Lookup
        End If
        'Response = oBodegas.LookupBodegasUsuarios(TinApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaRecibe.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaRecibe_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaRecibe.EditValueChanged
        'CargarFacturas()
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Dim RESPONSE As New Dipros.Utils.Events
                Dim oDataSet As DataSet
                RESPONSE = oOrdenesRecuperacion.DespliegaDatos(Me.clcFolio.EditValue)
                If Not RESPONSE.ErrorFound Then

                    oDataSet = RESPONSE.Value

                    If (oDataSet.Tables(0).Rows(0).Item("persona_recibe")) <> Nothing Then
                        If CType(oDataSet.Tables(0).Rows(0).Item("persona_recibe"), String).Trim.Length > 0 Then
                        Else
                            Me.lkpPersonaRecibe.EditValue = Comunes.TinApp.Connection.User
                        End If
                    Else
                        If (oDataSet.Tables(0).Rows(0).Item("persona_recibe")) = "" Then
                            Me.lkpPersonaRecibe.EditValue = Comunes.TinApp.Connection.User
                        End If
                    End If
                End If
            Case Actions.Delete
        End Select
    End Sub


#End Region

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub traerDatos()
        Dim response As Events
        Select Case Action
            Case Actions.Insert
                response = oVentasDetalle.DespliegaDatosParaOrdenesRecuperacion(sucursal, serie, folio, -1)
            Case Actions.Update
                response = Me.oOrdenesRecuperacionDetalle.DespliegaDatos(folio_recuperacion)
            Case Actions.Delete
                response = Me.oOrdenesRecuperacionDetalle.DespliegaDatos(folio_recuperacion)
        End Select

        Me.grOrdenesRecuperacion.DataSource = Nothing

        If Not response.ErrorFound Then


            If response.Value.Tables(0).Rows.Count > 0 Then
                Me.grOrdenesRecuperacion.DataSource = response.Value.Tables(0)
                If response.Value.Tables(0).Rows(0).Item("cancelada") = 1 Then
                    Me.tbrTools.Buttons(0).Enabled = False
                End If
            End If
        Else

            response.ShowError()
        End If
    End Sub


    Private Sub TraeDatosElaboracion()
        Dim response As Dipros.Utils.Events
        'response = Me.oOrdenesRecuperacion.DespliegaDatos
        'If Not response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = response.Value
        Me.lblPersonaElabora.Text = CType(Comunes.TinApp.Connection.UserName(), String)

        response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value
            Me.lblSucursalElabora.Text = CType(odataset.Tables(0).Rows(0).Item("nombre"), String)
        End If
    End Sub
    Private Function TraeSucursalActual() As Long
        Dim response As Dipros.Utils.Events
        Dim sucursal As Long
        Dim odataset As DataSet
        Try
            response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
            If Not response.ErrorFound Then
                odataset = response.Value
                Return CType(odataset.Tables(0).Rows(0).Item("sucursal"), Long)
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgInformation, "Error en la petici�n de la sucursal actual", , , False)
        End Try

    End Function
    Private Function GuardaDetalle(ByRef Response As Dipros.Utils.Events) As Dipros.Utils.Events
        'Dim response As New Dipros.Utils.Events
        'With Me.tmaOrdenesRecuperacion
        With Me.grvOrdenesRecuperacion
            .MoveFirst()
            Dim i As Integer
            For i = 0 To Me.grvOrdenesRecuperacion.RowCount - 1
                Select Case Me.Action
                    Case Actions.Insert 'And .Item("cantidad") > 0
                        If (CType(Me.grvOrdenesRecuperacion.GetRowCellValue(i, Me.grcSeleccionar), Boolean) = True) Then
                            Response = oOrdenesRecuperacionDetalle.Insertar(CType(Me.grvOrdenesRecuperacion.GetDataRow(i), DataRow), Me.clcFolio.EditValue)
                        End If

                    Case Actions.Update
                        If (CType(Me.grvOrdenesRecuperacion.GetRowCellValue(i, Me.grcSeleccionar), Boolean) = True) Then
                            Response = oOrdenesRecuperacionDetalle.Actualizar(CType(Me.grvOrdenesRecuperacion.GetDataRow(i), DataRow), Me.clcFolio.EditValue)
                        End If
                    Case Actions.Delete
                        '    Response = oOrdenesRecuperacionDetalle.Eliminar(Me.clcOrden.Value, .Item("Partida"), .Item("cantidad"), .Item("articulo"), lkpBodega.EditValue, Me.chkSobrePedido.Checked)
                End Select

                ''Me.grvMovimientos.SetRowCellValue(i, Me.grcProgramar, False)
            Next
            'Do While Not .EOF

            '    Select Case .CurrentAction
            '        Case Actions.Insert And .Item("cantidad") > 0
            '            Response = oOrdenesRecuperacion.Insertar(.SelectedRow, clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)

            '            If Not Response.ErrorFound And .Item("sobre_pedido") = True Then

            '                Response = Me.oOrdenesRecuperacion.ActualizarVentaDetalleSobrePedidoOC(clcOrden.EditValue, .Item("folios_ventas"))
            '            End If

            '        Case Actions.Update
            '            Response = oOrdenesRecuperacionDetalle.Actualizar(.SelectedRow, clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)
            '        Case Actions.Delete
            '            Response = oOrdenesRecuperacionDetalle.Eliminar(Me.clcOrden.Value, .Item("Partida"), .Item("cantidad"), .Item("articulo"), lkpBodega.EditValue, Me.chkSobrePedido.Checked)
            '    End Select
            '    .MoveNext()
            'Loop
        End With

        'If Me.Action = Actions.Insert And Not Response.ErrorFound Then
        '    ImprimeOrden(Me.clcOrden.Value)
        'End If
    End Function
    Private Sub VerificaBodegaXUsuario(ByVal usuario_recibe As String)
        If CType(TinApp.Connection.User, String).ToUpper <> usuario_recibe.ToUpper And Me.lkpBodegaRecibe.EditValue <> Nothing Then
            If CType(Me.lkpBodegaRecibe.EditValue, String).Trim.Length > 0 Then
                Me.lkpBodegaRecibe.Enabled = False
            End If

        Else
            Me.lkpBodegaRecibe.Enabled = True
        End If
    End Sub


    Private Function ValidaGrid() As Events
        Dim oevent As New Events

        Me.grvOrdenesRecuperacion.CloseEditor()
        Me.grvOrdenesRecuperacion.UpdateCurrentRow()


        If Me.grvOrdenesRecuperacion.DataSource Is Nothing Then
            oevent.Ex = Nothing
            oevent.Message = "Se Debe Seleccionar Al Menos Una Factura"

        Else
            If CType(Me.grvOrdenesRecuperacion.DataSource, DataView).Table.Select("seleccionar = 1").Length <= 0 Then
                oevent.Ex = Nothing
                oevent.Message = "Se Debe Seleccionar Al Menos Una Factura"
            End If


        End If

        Return oevent
    End Function

    Private Sub ImprimirVale()
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.ValeOrdenesRecuperacion(folio_recuperacion)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Vale de la Orden de Recuperaci�n no puede Mostrarse")
            Else
                If Response.Value.Tables(0).Rows.Count > 0 Then
                    Dim oDataSet As DataSet
                    Dim oReport As New rptOrdenRecuperacionVale

                    'If Me.chkChofer.Checked = False Then
                    '    oReport.lineChofer.Visible = False
                    'End If


                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)


                    TinApp.ShowReport(Me.MdiParent, "Vale de Orden de Recuperaci�n", oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

    End Sub

#End Region





End Class


