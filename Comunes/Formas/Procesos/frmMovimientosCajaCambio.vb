Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Imports System.IO
Imports DataDynamics
Public Class frmMovimientosCajaCambio
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblImporteRecibido As System.Windows.Forms.Label
    Friend WithEvents lblImportePagar2 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblImportePagar As System.Windows.Forms.Label
    Friend WithEvents clcImporteRecibido As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblCambio2 As System.Windows.Forms.Label
    Friend WithEvents lblCambio As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUltimosDigitos As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosCajaCambio))
        Me.lblImportePagar2 = New System.Windows.Forms.Label
        Me.lblImporteRecibido = New System.Windows.Forms.Label
        Me.lblCambio2 = New System.Windows.Forms.Label
        Me.btnAceptar = New DevExpress.XtraEditors.SimpleButton
        Me.lblImportePagar = New System.Windows.Forms.Label
        Me.clcImporteRecibido = New DevExpress.XtraEditors.CalcEdit
        Me.lblCambio = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtUltimosDigitos = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcImporteRecibido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUltimosDigitos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        '
        'lblImportePagar2
        '
        Me.lblImportePagar2.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImportePagar2.Location = New System.Drawing.Point(14, 40)
        Me.lblImportePagar2.Name = "lblImportePagar2"
        Me.lblImportePagar2.Size = New System.Drawing.Size(208, 32)
        Me.lblImportePagar2.TabIndex = 0
        Me.lblImportePagar2.Text = "Importe a Pagar:"
        Me.lblImportePagar2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImporteRecibido
        '
        Me.lblImporteRecibido.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImporteRecibido.Location = New System.Drawing.Point(14, 80)
        Me.lblImporteRecibido.Name = "lblImporteRecibido"
        Me.lblImporteRecibido.Size = New System.Drawing.Size(208, 32)
        Me.lblImporteRecibido.TabIndex = 2
        Me.lblImporteRecibido.Text = "Importe Recibido:"
        Me.lblImporteRecibido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCambio2
        '
        Me.lblCambio2.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCambio2.ForeColor = System.Drawing.Color.Red
        Me.lblCambio2.Location = New System.Drawing.Point(14, 160)
        Me.lblCambio2.Name = "lblCambio2"
        Me.lblCambio2.Size = New System.Drawing.Size(208, 32)
        Me.lblCambio2.TabIndex = 6
        Me.lblCambio2.Text = "Cambio:"
        Me.lblCambio2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(172, 200)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(104, 32)
        Me.btnAceptar.TabIndex = 8
        Me.btnAceptar.Text = "&Aceptar"
        '
        'lblImportePagar
        '
        Me.lblImportePagar.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImportePagar.Location = New System.Drawing.Point(234, 40)
        Me.lblImportePagar.Name = "lblImportePagar"
        Me.lblImportePagar.Size = New System.Drawing.Size(200, 32)
        Me.lblImportePagar.TabIndex = 1
        Me.lblImportePagar.Text = "0"
        Me.lblImportePagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporteRecibido
        '
        Me.clcImporteRecibido.Location = New System.Drawing.Point(234, 79)
        Me.clcImporteRecibido.Name = "clcImporteRecibido"
        '
        'clcImporteRecibido.Properties
        '
        Me.clcImporteRecibido.Properties.DisplayFormat.FormatString = "N2"
        Me.clcImporteRecibido.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteRecibido.Properties.EditFormat.FormatString = "N2"
        Me.clcImporteRecibido.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteRecibido.Properties.MaskData.EditMask = "###,###,##0.00"
        Me.clcImporteRecibido.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporteRecibido.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImporteRecibido.Size = New System.Drawing.Size(200, 33)
        Me.clcImporteRecibido.TabIndex = 3
        '
        'lblCambio
        '
        Me.lblCambio.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCambio.ForeColor = System.Drawing.Color.Red
        Me.lblCambio.Location = New System.Drawing.Point(234, 160)
        Me.lblCambio.Name = "lblCambio"
        Me.lblCambio.Size = New System.Drawing.Size(200, 32)
        Me.lblCambio.TabIndex = 7
        Me.lblCambio.Text = "0"
        Me.lblCambio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 32)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Ultimos D�gitos:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtUltimosDigitos
        '
        Me.txtUltimosDigitos.EditValue = ""
        Me.txtUltimosDigitos.Location = New System.Drawing.Point(234, 120)
        Me.txtUltimosDigitos.Name = "txtUltimosDigitos"
        '
        'txtUltimosDigitos.Properties
        '
        Me.txtUltimosDigitos.Properties.MaxLength = 4
        Me.txtUltimosDigitos.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtUltimosDigitos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 16.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtUltimosDigitos.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtUltimosDigitos.Size = New System.Drawing.Size(200, 33)
        Me.txtUltimosDigitos.TabIndex = 59
        Me.txtUltimosDigitos.Tag = "observaciones"
        '
        'frmMovimientosCajaCambio
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(449, 236)
        Me.Controls.Add(Me.txtUltimosDigitos)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCambio)
        Me.Controls.Add(Me.clcImporteRecibido)
        Me.Controls.Add(Me.lblImportePagar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.lblCambio2)
        Me.Controls.Add(Me.lblImporteRecibido)
        Me.Controls.Add(Me.lblImportePagar2)
        Me.Name = "frmMovimientosCajaCambio"
        Me.Text = "Movimientos de Caja: Cambio"
        Me.Controls.SetChildIndex(Me.lblImportePagar2, 0)
        Me.Controls.SetChildIndex(Me.lblImporteRecibido, 0)
        Me.Controls.SetChildIndex(Me.lblCambio2, 0)
        Me.Controls.SetChildIndex(Me.btnAceptar, 0)
        Me.Controls.SetChildIndex(Me.lblImportePagar, 0)
        Me.Controls.SetChildIndex(Me.clcImporteRecibido, 0)
        Me.Controls.SetChildIndex(Me.lblCambio, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtUltimosDigitos, 0)
        CType(Me.clcImporteRecibido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUltimosDigitos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region



#Region "DIPROS Systems, Declaraciones"

    Private oMovimientosCobrar As New VillarrealBusiness.clsMovimientosCobrar
    Private factura_electronica As Boolean = False
    Private solicita_ultimos_digitos As Boolean = False

    Public Property ImporteAPagar() As Double
        Get
            Return Me.lblImportePagar.Text.Replace("$", "")
        End Get
        Set(ByVal Value As Double)
            Me.lblImportePagar.Text = Value
        End Set
    End Property

    Public WriteOnly Property ImportePagado() As Double
        Set(ByVal Value As Double)
            Me.clcImporteRecibido.Value = Value
            Me.clcImporteRecibido.SelectAll()


            If Me.clcImporteRecibido.Value > 0 Then
                Me.clcImporteRecibido.Enabled = False
            Else
                Me.clcImporteRecibido.Enabled = True
                Me.clcImporteRecibido.Focus()
            End If
        End Set
    End Property

    Public WriteOnly Property ManejaFacturaElectronica() As Boolean
        Set(ByVal Value As Boolean)
            factura_electronica = Value
        End Set
    End Property

    Public WriteOnly Property SolicitaUltimosDigitos() As Boolean
        Set(ByVal Value As Boolean)
            solicita_ultimos_digitos = Value
        End Set
    End Property

    Public ReadOnly Property UltimosDigitos() As String
        Get
            Return Me.txtUltimosDigitos.Text
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmMovimientosCajaCambio_Inicialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar

        Me.txtUltimosDigitos.Enabled = solicita_ultimos_digitos
    End Sub

    Private Sub frmMovimientosCajaCambio_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        If Me.clcImporteRecibido.Text.Trim.Length = 0 Then
            Me.clcImporteRecibido.Text = "0"
            Me.clcImporteRecibido.EditValue = "0"
        End If

       
        Response = Me.oMovimientosCobrar.Validacion(Me.ImporteAPagar, Replace(Me.clcImporteRecibido.Text, "$", ""), Replace(Me.lblCambio.Text, "$", ""))
        If Response.ErrorFound Then
            Me.clcImporteRecibido.SelectAll()
        End If

        Response = Me.oMovimientosCobrar.ValidacionUltimosDigitos(Me.solicita_ultimos_digitos, Me.UltimosDigitos)
        If Response.ErrorFound Then
            Me.txtUltimosDigitos.SelectAll()
        End If



    End Sub
    Private Sub frmMovimientosCajaCambio_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Guardar()
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub frmMovimientosCajaCambio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If e.KeyChar = ChrW(27) Then
            Me.Close()
        End If
    End Sub

    Private Sub clcImporteRecibido_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcImporteRecibido.TextChanged
        If IsNumeric(Me.clcImporteRecibido.EditValue) And IsNumeric(Me.lblImportePagar.Text) Then

            If CType(Me.lblImportePagar.Text, Double) > 0 And CType(Me.clcImporteRecibido.EditValue, Double) > 0 And CType(Me.lblImportePagar.Text, Double) > CType(Me.clcImporteRecibido.EditValue, Double) Then
                'ShowMessage(MessageType.MsgInformation, "El importe recibido debe ser igual al importe a pagar")
            End If

            If CType(Me.lblImportePagar.Text, Double) > 0 And CType(Me.clcImporteRecibido.EditValue, Double) > 0 And CType(Me.lblImportePagar.Text, Double) <= CType(Me.clcImporteRecibido.EditValue, Double) Then
                Me.lblCambio.Text = CType(CType(Me.clcImporteRecibido.EditValue, Double) - CType(Me.lblImportePagar.Text, Double), String)
                Me.lblCambio.Text = CStr(Format(Me.lblCambio.Text, "currency"))

            End If

        Else
            Me.lblCambio.Text = "0"
        End If

    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim response As Dipros.Utils.Events

        response = Me.oMovimientosCobrar.Validacion(Me.ImporteAPagar, Replace(Me.clcImporteRecibido.Text, "$", ""), Replace(Me.lblCambio.Text, "$", ""))
        If response.ErrorFound Then

            ShowMessage(MessageType.MsgInformation, response.Message)
            Me.clcImporteRecibido.Focus()
            Me.clcImporteRecibido.SelectAll()
        Else

            response = Me.oMovimientosCobrar.ValidacionUltimosDigitos(Me.solicita_ultimos_digitos, Me.UltimosDigitos)
            If response.ErrorFound Then
                Me.txtUltimosDigitos.Focus()
                Me.txtUltimosDigitos.SelectAll()
                ShowMessage(MessageType.MsgInformation, response.Message)
            Else
                Guardar()
            End If
        End If

        CType(Me.OwnerForm, frmMovimientosCaja).Enabled = True

    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub Guardar()
        Dim response As Dipros.Utils.Events
        Try
            With CType(Me.OwnerForm, frmMovimientosCaja)
                .UltimosDigitos = Me.UltimosDigitos
                response = .GuardarCambios()
            End With
            Me.Close()
        Catch ex As Exception
            ShowMessage(MessageType.MsgInformation, ex.Message)
        End Try
    End Sub
#End Region


End Class
