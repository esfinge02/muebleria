Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmLlamadasClientesDetalle
    Inherits Dipros.Windows.frmTINForm
#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFechaHora As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Hora As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblTipo_Llamada As System.Windows.Forms.Label
    Friend WithEvents lkpTipo_Llamada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents lblPersona_Contesta As System.Windows.Forms.Label
    Friend WithEvents txtPersona_Contesta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblParentesco_Persona_Contesta As System.Windows.Forms.Label
    Friend WithEvents lblMensaje_Dejado As System.Windows.Forms.Label
    Friend WithEvents lblMensaje_Recibido As System.Windows.Forms.Label
    Friend WithEvents txtMensaje_Recibido As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblDescTipoTelefono As System.Windows.Forms.Label
    Friend WithEvents lblDescTipoLLamada As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioCapturo As System.Windows.Forms.Label
    Friend WithEvents lkpParentesco As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDescParentesco As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtFecha_promesa_pago As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lkpTelefono As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDescMensaje As System.Windows.Forms.Label
    Friend WithEvents lkpMensaje As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLlamadasClientesDetalle))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblFechaHora = New System.Windows.Forms.Label
        Me.dteFecha_Hora = New DevExpress.XtraEditors.DateEdit
        Me.lblTipo_Llamada = New System.Windows.Forms.Label
        Me.lkpTipo_Llamada = New Dipros.Editors.TINMultiLookup
        Me.lblTelefono = New System.Windows.Forms.Label
        Me.lblPersona_Contesta = New System.Windows.Forms.Label
        Me.txtPersona_Contesta = New DevExpress.XtraEditors.TextEdit
        Me.lblParentesco_Persona_Contesta = New System.Windows.Forms.Label
        Me.lblMensaje_Dejado = New System.Windows.Forms.Label
        Me.lblMensaje_Recibido = New System.Windows.Forms.Label
        Me.txtMensaje_Recibido = New DevExpress.XtraEditors.MemoEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblDescTipoTelefono = New System.Windows.Forms.Label
        Me.lblDescTipoLLamada = New System.Windows.Forms.Label
        Me.lblUsuarioCapturo = New System.Windows.Forms.Label
        Me.lkpParentesco = New Dipros.Editors.TINMultiLookup
        Me.lblDescParentesco = New System.Windows.Forms.Label
        Me.dtFecha_promesa_pago = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpTelefono = New Dipros.Editors.TINMultiLookup
        Me.lblDescMensaje = New System.Windows.Forms.Label
        Me.lkpMensaje = New Dipros.Editors.TINMultiLookup
        CType(Me.dteFecha_Hora.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPersona_Contesta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMensaje_Recibido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtFecha_promesa_pago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5643, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(136, 43)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(192, 40)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(264, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 1
        Me.lkpCliente.Tag = "cliente"
        Me.lkpCliente.ToolTip = "cliente"
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblFechaHora
        '
        Me.lblFechaHora.AutoSize = True
        Me.lblFechaHora.Location = New System.Drawing.Point(113, 67)
        Me.lblFechaHora.Name = "lblFechaHora"
        Me.lblFechaHora.Size = New System.Drawing.Size(71, 16)
        Me.lblFechaHora.TabIndex = 2
        Me.lblFechaHora.Tag = ""
        Me.lblFechaHora.Text = "&Fecha/hora:"
        Me.lblFechaHora.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Hora
        '
        Me.dteFecha_Hora.EditValue = "19/01/2007"
        Me.dteFecha_Hora.Location = New System.Drawing.Point(192, 64)
        Me.dteFecha_Hora.Name = "dteFecha_Hora"
        '
        'dteFecha_Hora.Properties
        '
        Me.dteFecha_Hora.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Hora.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Hora.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Hora.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Hora.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Hora.Properties.Enabled = False
        Me.dteFecha_Hora.Size = New System.Drawing.Size(264, 23)
        Me.dteFecha_Hora.TabIndex = 3
        Me.dteFecha_Hora.Tag = "fecha_hora"
        Me.dteFecha_Hora.ToolTip = "fecha"
        '
        'lblTipo_Llamada
        '
        Me.lblTipo_Llamada.AutoSize = True
        Me.lblTipo_Llamada.Location = New System.Drawing.Point(103, 96)
        Me.lblTipo_Llamada.Name = "lblTipo_Llamada"
        Me.lblTipo_Llamada.Size = New System.Drawing.Size(80, 16)
        Me.lblTipo_Llamada.TabIndex = 4
        Me.lblTipo_Llamada.Tag = ""
        Me.lblTipo_Llamada.Text = "&Tipo llamada:"
        Me.lblTipo_Llamada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpTipo_Llamada
        '
        Me.lkpTipo_Llamada.AllowAdd = False
        Me.lkpTipo_Llamada.AutoReaload = False
        Me.lkpTipo_Llamada.DataSource = Nothing
        Me.lkpTipo_Llamada.DefaultSearchField = ""
        Me.lkpTipo_Llamada.DisplayMember = "descripcion"
        Me.lkpTipo_Llamada.EditValue = Nothing
        Me.lkpTipo_Llamada.Filtered = False
        Me.lkpTipo_Llamada.InitValue = Nothing
        Me.lkpTipo_Llamada.Location = New System.Drawing.Point(192, 96)
        Me.lkpTipo_Llamada.MultiSelect = False
        Me.lkpTipo_Llamada.Name = "lkpTipo_Llamada"
        Me.lkpTipo_Llamada.NullText = ""
        Me.lkpTipo_Llamada.PopupWidth = CType(400, Long)
        Me.lkpTipo_Llamada.ReadOnlyControl = False
        Me.lkpTipo_Llamada.Required = False
        Me.lkpTipo_Llamada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTipo_Llamada.SearchMember = ""
        Me.lkpTipo_Llamada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTipo_Llamada.SelectAll = False
        Me.lkpTipo_Llamada.Size = New System.Drawing.Size(264, 20)
        Me.lkpTipo_Llamada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpTipo_Llamada.TabIndex = 5
        Me.lkpTipo_Llamada.Tag = "tipo_llamada"
        Me.lkpTipo_Llamada.ToolTip = "tipo de llamada"
        Me.lkpTipo_Llamada.ValueMember = "Tipo_Llamada"
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(128, 136)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(56, 16)
        Me.lblTelefono.TabIndex = 6
        Me.lblTelefono.Tag = ""
        Me.lblTelefono.Text = "Tel�&fono:"
        Me.lblTelefono.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPersona_Contesta
        '
        Me.lblPersona_Contesta.AutoSize = True
        Me.lblPersona_Contesta.Location = New System.Drawing.Point(80, 168)
        Me.lblPersona_Contesta.Name = "lblPersona_Contesta"
        Me.lblPersona_Contesta.Size = New System.Drawing.Size(104, 16)
        Me.lblPersona_Contesta.TabIndex = 10
        Me.lblPersona_Contesta.Tag = ""
        Me.lblPersona_Contesta.Text = "&Persona contesta:"
        Me.lblPersona_Contesta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPersona_Contesta
        '
        Me.txtPersona_Contesta.EditValue = ""
        Me.txtPersona_Contesta.Location = New System.Drawing.Point(192, 160)
        Me.txtPersona_Contesta.Name = "txtPersona_Contesta"
        '
        'txtPersona_Contesta.Properties
        '
        Me.txtPersona_Contesta.Properties.MaxLength = 50
        Me.txtPersona_Contesta.Size = New System.Drawing.Size(264, 20)
        Me.txtPersona_Contesta.TabIndex = 11
        Me.txtPersona_Contesta.Tag = "persona_contesta"
        Me.txtPersona_Contesta.ToolTip = "persona que contesta"
        '
        'lblParentesco_Persona_Contesta
        '
        Me.lblParentesco_Persona_Contesta.AutoSize = True
        Me.lblParentesco_Persona_Contesta.Location = New System.Drawing.Point(16, 192)
        Me.lblParentesco_Persona_Contesta.Name = "lblParentesco_Persona_Contesta"
        Me.lblParentesco_Persona_Contesta.Size = New System.Drawing.Size(168, 16)
        Me.lblParentesco_Persona_Contesta.TabIndex = 12
        Me.lblParentesco_Persona_Contesta.Tag = ""
        Me.lblParentesco_Persona_Contesta.Text = "Parentesco persona &contesta:"
        Me.lblParentesco_Persona_Contesta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMensaje_Dejado
        '
        Me.lblMensaje_Dejado.AutoSize = True
        Me.lblMensaje_Dejado.Location = New System.Drawing.Point(80, 240)
        Me.lblMensaje_Dejado.Name = "lblMensaje_Dejado"
        Me.lblMensaje_Dejado.Size = New System.Drawing.Size(96, 16)
        Me.lblMensaje_Dejado.TabIndex = 14
        Me.lblMensaje_Dejado.Tag = ""
        Me.lblMensaje_Dejado.Text = "&Mensaje dejado:"
        Me.lblMensaje_Dejado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMensaje_Recibido
        '
        Me.lblMensaje_Recibido.AutoSize = True
        Me.lblMensaje_Recibido.Location = New System.Drawing.Point(80, 328)
        Me.lblMensaje_Recibido.Name = "lblMensaje_Recibido"
        Me.lblMensaje_Recibido.Size = New System.Drawing.Size(102, 16)
        Me.lblMensaje_Recibido.TabIndex = 16
        Me.lblMensaje_Recibido.Tag = ""
        Me.lblMensaje_Recibido.Text = "Mensaje &recibido:"
        Me.lblMensaje_Recibido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMensaje_Recibido
        '
        Me.txtMensaje_Recibido.EditValue = ""
        Me.txtMensaje_Recibido.Location = New System.Drawing.Point(192, 328)
        Me.txtMensaje_Recibido.Name = "txtMensaje_Recibido"
        Me.txtMensaje_Recibido.Size = New System.Drawing.Size(401, 56)
        Me.txtMensaje_Recibido.TabIndex = 17
        Me.txtMensaje_Recibido.Tag = "mensaje_recibido"
        Me.txtMensaje_Recibido.ToolTip = "mensaje recibido"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(96, 384)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 20
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(192, 384)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(401, 56)
        Me.txtObservaciones.TabIndex = 21
        Me.txtObservaciones.Tag = "observaciones"
        Me.txtObservaciones.ToolTip = "observaciones"
        '
        'lblDescTipoTelefono
        '
        Me.lblDescTipoTelefono.Location = New System.Drawing.Point(464, 128)
        Me.lblDescTipoTelefono.Name = "lblDescTipoTelefono"
        Me.lblDescTipoTelefono.Size = New System.Drawing.Size(120, 24)
        Me.lblDescTipoTelefono.TabIndex = 59
        Me.lblDescTipoTelefono.Tag = "tipo_telefono"
        Me.lblDescTipoTelefono.Text = "descripcion_tipo_telefono"
        Me.lblDescTipoTelefono.Visible = False
        '
        'lblDescTipoLLamada
        '
        Me.lblDescTipoLLamada.Location = New System.Drawing.Point(472, 88)
        Me.lblDescTipoLLamada.Name = "lblDescTipoLLamada"
        Me.lblDescTipoLLamada.Size = New System.Drawing.Size(120, 24)
        Me.lblDescTipoLLamada.TabIndex = 60
        Me.lblDescTipoLLamada.Tag = "descripcion_tipo_llamada"
        Me.lblDescTipoLLamada.Text = "descripcion_tipo_llamada"
        Me.lblDescTipoLLamada.Visible = False
        '
        'lblUsuarioCapturo
        '
        Me.lblUsuarioCapturo.Enabled = False
        Me.lblUsuarioCapturo.Location = New System.Drawing.Point(472, 48)
        Me.lblUsuarioCapturo.Name = "lblUsuarioCapturo"
        Me.lblUsuarioCapturo.TabIndex = 61
        Me.lblUsuarioCapturo.Tag = "usuario_capturo"
        Me.lblUsuarioCapturo.Text = "usuario_capturo"
        Me.lblUsuarioCapturo.Visible = False
        '
        'lkpParentesco
        '
        Me.lkpParentesco.AllowAdd = False
        Me.lkpParentesco.AutoReaload = False
        Me.lkpParentesco.DataSource = Nothing
        Me.lkpParentesco.DefaultSearchField = ""
        Me.lkpParentesco.DisplayMember = "descripcion"
        Me.lkpParentesco.EditValue = Nothing
        Me.lkpParentesco.Filtered = False
        Me.lkpParentesco.InitValue = Nothing
        Me.lkpParentesco.Location = New System.Drawing.Point(192, 184)
        Me.lkpParentesco.MultiSelect = False
        Me.lkpParentesco.Name = "lkpParentesco"
        Me.lkpParentesco.NullText = ""
        Me.lkpParentesco.PopupWidth = CType(400, Long)
        Me.lkpParentesco.ReadOnlyControl = False
        Me.lkpParentesco.Required = False
        Me.lkpParentesco.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpParentesco.SearchMember = ""
        Me.lkpParentesco.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpParentesco.SelectAll = False
        Me.lkpParentesco.Size = New System.Drawing.Size(264, 20)
        Me.lkpParentesco.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpParentesco.TabIndex = 65
        Me.lkpParentesco.Tag = "parentesco"
        Me.lkpParentesco.ToolTip = "Parentesco"
        Me.lkpParentesco.ValueMember = "parentesco"
        '
        'lblDescParentesco
        '
        Me.lblDescParentesco.Location = New System.Drawing.Point(472, 184)
        Me.lblDescParentesco.Name = "lblDescParentesco"
        Me.lblDescParentesco.Size = New System.Drawing.Size(120, 24)
        Me.lblDescParentesco.TabIndex = 66
        Me.lblDescParentesco.Tag = "des_parentesco"
        Me.lblDescParentesco.Text = "des_parentesco"
        Me.lblDescParentesco.Visible = False
        '
        'dtFecha_promesa_pago
        '
        Me.dtFecha_promesa_pago.EditValue = ""
        Me.dtFecha_promesa_pago.Location = New System.Drawing.Point(192, 208)
        Me.dtFecha_promesa_pago.Name = "dtFecha_promesa_pago"
        '
        'dtFecha_promesa_pago.Properties
        '
        Me.dtFecha_promesa_pago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtFecha_promesa_pago.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dtFecha_promesa_pago.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dtFecha_promesa_pago.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dtFecha_promesa_pago.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dtFecha_promesa_pago.Size = New System.Drawing.Size(264, 23)
        Me.dtFecha_promesa_pago.TabIndex = 67
        Me.dtFecha_promesa_pago.Tag = "fecha_promesa_pago"
        Me.dtFecha_promesa_pago.ToolTip = "fecha_promesa_pago"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 216)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(158, 16)
        Me.Label2.TabIndex = 68
        Me.Label2.Tag = ""
        Me.Label2.Text = "Fecha de Promesa de Pago:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpTelefono
        '
        Me.lkpTelefono.AllowAdd = False
        Me.lkpTelefono.AutoReaload = False
        Me.lkpTelefono.DataSource = Nothing
        Me.lkpTelefono.DefaultSearchField = ""
        Me.lkpTelefono.DisplayMember = "tel"
        Me.lkpTelefono.EditValue = Nothing
        Me.lkpTelefono.Filtered = False
        Me.lkpTelefono.InitValue = Nothing
        Me.lkpTelefono.Location = New System.Drawing.Point(192, 128)
        Me.lkpTelefono.MultiSelect = False
        Me.lkpTelefono.Name = "lkpTelefono"
        Me.lkpTelefono.NullText = ""
        Me.lkpTelefono.PopupWidth = CType(400, Long)
        Me.lkpTelefono.ReadOnlyControl = False
        Me.lkpTelefono.Required = False
        Me.lkpTelefono.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTelefono.SearchMember = ""
        Me.lkpTelefono.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTelefono.SelectAll = False
        Me.lkpTelefono.Size = New System.Drawing.Size(264, 20)
        Me.lkpTelefono.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpTelefono.TabIndex = 69
        Me.lkpTelefono.Tag = "telefono"
        Me.lkpTelefono.ToolTip = "Telefono"
        Me.lkpTelefono.ValueMember = "tel"
        '
        'lblDescMensaje
        '
        Me.lblDescMensaje.Location = New System.Drawing.Point(192, 264)
        Me.lblDescMensaje.Name = "lblDescMensaje"
        Me.lblDescMensaje.Size = New System.Drawing.Size(400, 56)
        Me.lblDescMensaje.TabIndex = 71
        Me.lblDescMensaje.Tag = "mensaje_dejado"
        '
        'lkpMensaje
        '
        Me.lkpMensaje.AllowAdd = False
        Me.lkpMensaje.AutoReaload = False
        Me.lkpMensaje.DataSource = Nothing
        Me.lkpMensaje.DefaultSearchField = ""
        Me.lkpMensaje.DisplayMember = "descripcion"
        Me.lkpMensaje.EditValue = Nothing
        Me.lkpMensaje.Filtered = False
        Me.lkpMensaje.InitValue = Nothing
        Me.lkpMensaje.Location = New System.Drawing.Point(192, 232)
        Me.lkpMensaje.MultiSelect = False
        Me.lkpMensaje.Name = "lkpMensaje"
        Me.lkpMensaje.NullText = ""
        Me.lkpMensaje.PopupWidth = CType(400, Long)
        Me.lkpMensaje.ReadOnlyControl = False
        Me.lkpMensaje.Required = False
        Me.lkpMensaje.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMensaje.SearchMember = ""
        Me.lkpMensaje.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMensaje.SelectAll = False
        Me.lkpMensaje.Size = New System.Drawing.Size(264, 20)
        Me.lkpMensaje.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMensaje.TabIndex = 72
        Me.lkpMensaje.Tag = "id_mensaje"
        Me.lkpMensaje.ToolTip = "Mensaje Dejado"
        Me.lkpMensaje.ValueMember = "mensaje_dejado"
        '
        'frmLlamadasClientesDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(602, 483)
        Me.Controls.Add(Me.lkpMensaje)
        Me.Controls.Add(Me.lblDescMensaje)
        Me.Controls.Add(Me.lkpTelefono)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtFecha_promesa_pago)
        Me.Controls.Add(Me.lblDescParentesco)
        Me.Controls.Add(Me.lkpParentesco)
        Me.Controls.Add(Me.lblUsuarioCapturo)
        Me.Controls.Add(Me.lblDescTipoLLamada)
        Me.Controls.Add(Me.lblDescTipoTelefono)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblFechaHora)
        Me.Controls.Add(Me.dteFecha_Hora)
        Me.Controls.Add(Me.lblTipo_Llamada)
        Me.Controls.Add(Me.lkpTipo_Llamada)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.lblPersona_Contesta)
        Me.Controls.Add(Me.txtPersona_Contesta)
        Me.Controls.Add(Me.lblParentesco_Persona_Contesta)
        Me.Controls.Add(Me.lblMensaje_Dejado)
        Me.Controls.Add(Me.lblMensaje_Recibido)
        Me.Controls.Add(Me.txtMensaje_Recibido)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Name = "frmLlamadasClientesDetalle"
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtMensaje_Recibido, 0)
        Me.Controls.SetChildIndex(Me.lblMensaje_Recibido, 0)
        Me.Controls.SetChildIndex(Me.lblMensaje_Dejado, 0)
        Me.Controls.SetChildIndex(Me.lblParentesco_Persona_Contesta, 0)
        Me.Controls.SetChildIndex(Me.txtPersona_Contesta, 0)
        Me.Controls.SetChildIndex(Me.lblPersona_Contesta, 0)
        Me.Controls.SetChildIndex(Me.lblTelefono, 0)
        Me.Controls.SetChildIndex(Me.lkpTipo_Llamada, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Llamada, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Hora, 0)
        Me.Controls.SetChildIndex(Me.lblFechaHora, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblDescTipoTelefono, 0)
        Me.Controls.SetChildIndex(Me.lblDescTipoLLamada, 0)
        Me.Controls.SetChildIndex(Me.lblUsuarioCapturo, 0)
        Me.Controls.SetChildIndex(Me.lkpParentesco, 0)
        Me.Controls.SetChildIndex(Me.lblDescParentesco, 0)
        Me.Controls.SetChildIndex(Me.dtFecha_promesa_pago, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpTelefono, 0)
        Me.Controls.SetChildIndex(Me.lblDescMensaje, 0)
        Me.Controls.SetChildIndex(Me.lkpMensaje, 0)
        CType(Me.dteFecha_Hora.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPersona_Contesta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMensaje_Recibido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtFecha_promesa_pago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oClientes As New VillarrealBusiness.clsClientes
    Private oTipoLlamada As New VillarrealBusiness.clsTiposLlamadas
    Private oLlamadasClientesDetalle As New VillarrealBusiness.clsLlamadasClientesDetalle
    'Private oTiposParentesco As New VillarrealBusiness.clsTipoParentesco
    Private oMensaje As New VillarrealBusiness.clsMensajeDejado
    'Private oLlamadasClientes As New VillarrealBusiness.clsLlamadasClientes
    Public sucursal_dependencia As Boolean = False
    Dim banderaCerrarForm As Boolean = False
    Public ReadOnly Property cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property

    Public ReadOnly Property tipo_llamada() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpTipo_Llamada)
        End Get
    End Property

    Public ReadOnly Property tipo_parentesco() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpParentesco)
        End Get
    End Property

    Public ReadOnly Property numero_telefonico() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpTelefono)
        End Get
    End Property

    Public ReadOnly Property id_mensaje() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMensaje)
        End Get
    End Property



#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmLlamadasClientesDetalle_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    Me.lblUsuarioCapturo.Text = TinApp.Connection.User
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With

    End Sub

    Private Sub frmLlamadasClientesDetalle_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oTipoLlamada = New VillarrealBusiness.clsTiposLlamadas
        oLlamadasClientesDetalle = New VillarrealBusiness.clsLlamadasClientesDetalle


        Me.lkpCliente.EditValue = OwnerForm.cliente
        Me.dteFecha_Hora.EditValue = TinApp.FechaServidor
        'Me.dtFecha_promesa_pago.EditValue = TinApp.FechaServidor

        ValidaCliente()

        Select Case Action
            Case Actions.Insert
                Me.dteFecha_Hora.EditValue = TINApp.FechaServidor
            Case Actions.Update
                'lblUsuarioCapturo.Text = 
            Case Actions.Delete
        End Select
      
    End Sub
    
    Private Sub frmLlamadasClientesDetalle_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.lblUsuarioCapturo.Text = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0).Item("usuario_capturo")
    End Sub

    Private Sub frmLlamadasClientesDetalle_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oLlamadasClientesDetalle.Validacion(Action, cliente, Me.dteFecha_Hora.Text, tipo_llamada, Me.txtPersona_Contesta.Text, Me.txtMensaje_Recibido.Text, tipo_parentesco, numero_telefonico, id_mensaje)
    End Sub





#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(sucursal_dependencia, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub

    Private Sub lkpTipo_Llamada_Format() Handles lkpTipo_Llamada.Format
        Comunes.clsFormato.for_tipos_llamadas_grl(Me.lkpTipo_Llamada)
    End Sub

    Private Sub lkpTelefono_Format() Handles lkpTelefono.Format
        Comunes.clsFormato.for_telefono_grl(Me.lkpTelefono)
    End Sub

    Private Sub lkpParentesco_Format() Handles lkpParentesco.Format
        Comunes.clsFormato.for_parentescos_grl(Me.lkpParentesco)
    End Sub

    Private Sub lkpMensaje_Format() Handles lkpMensaje.Format
        Comunes.clsFormato.for_mensaje_grl(Me.lkpMensaje)
    End Sub

    Private Sub lkpTipo_Llamada_LoadData(ByVal Initialize As Boolean) Handles lkpTipo_Llamada.LoadData
        Dim Response As New Events
        Response = oTipoLlamada.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpTipo_Llamada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpTelefono_LoadData(ByVal Initialize As Boolean) Handles lkpTelefono.LoadData
        Dim Response As New Events
        Response = oLlamadasClientesDetalle.ConsultarNumeros(cliente)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpTelefono.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpParentesco_LoadData(ByVal Initialize As Boolean) Handles lkpParentesco.LoadData
        Dim Response As New Events
        ' Response = oTiposParentesco.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpParentesco.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpMensaje_LoadData(ByVal Initialize As Boolean) Handles lkpMensaje.LoadData
        Dim Response As New Events

        'ShowMessage(MessageType.MsgInformation, "tipo llamada " & tipo_llamada)

        Response = oMensaje.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMensaje.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpTipo_Llamada_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpTipo_Llamada.EditValueChanged
        If tipo_llamada > 0 Then
            Me.lblDescTipoLLamada.Text = Me.lkpTipo_Llamada.DisplayText
            lkpMensaje_LoadData(True)
        End If
    End Sub

    Private Sub lkpTelefono_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpTelefono.EditValueChanged
        If numero_telefonico > 0 Then
            Me.lblDescTipoTelefono.Text = Me.lkpTelefono.GetValue("tipo")
        End If
    End Sub

    Private Sub lkpParentesco_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpParentesco.EditValueChanged
        If tipo_parentesco > 0 Then
            Me.lblDescParentesco.Text = Me.lkpParentesco.DisplayText
        End If
    End Sub

    Private Sub lkpMensaje_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpMensaje.EditValueChanged
        If id_mensaje > 0 Then
            Me.lblDescMensaje.Text = Me.lkpMensaje.DisplayText
        End If
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub ValidaCliente()
        If Me.lkpCliente.EditValue = -1 Or IsNumeric(Me.lkpCliente.EditValue) = False Or Me.lkpCliente.EditValue = Nothing Then
            ShowMessage(MessageType.MsgInformation, "El Cliente es Requerido")
            banderaCerrarForm = True
        End If
    End Sub

#End Region


End Class
