Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmVentasAnticipos
    Inherits Dipros.Windows.frmTINForm

    Private oAnticipos As New VillarrealBusiness.clsAnticipos

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Public WithEvents lblAnticipo As System.Windows.Forms.Label
    Public WithEvents lblImporte As System.Windows.Forms.Label
    Public WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents clcImporte As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lkpAnticipo As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVentasAnticipos))
        Me.lblAnticipo = New System.Windows.Forms.Label
        Me.lkpAnticipo = New Dipros.Editors.TINMultiLookup
        Me.lblImporte = New System.Windows.Forms.Label
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.txtConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.clcImporte = New DevExpress.XtraEditors.CalcEdit
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        '
        'lblAnticipo
        '
        Me.lblAnticipo.AutoSize = True
        Me.lblAnticipo.Location = New System.Drawing.Point(15, 40)
        Me.lblAnticipo.Name = "lblAnticipo"
        Me.lblAnticipo.Size = New System.Drawing.Size(53, 16)
        Me.lblAnticipo.TabIndex = 59
        Me.lblAnticipo.Tag = ""
        Me.lblAnticipo.Text = "Anticipo:"
        Me.lblAnticipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAnticipo
        '
        Me.lkpAnticipo.AllowAdd = False
        Me.lkpAnticipo.AutoReaload = False
        Me.lkpAnticipo.DataSource = Nothing
        Me.lkpAnticipo.DefaultSearchField = ""
        Me.lkpAnticipo.DisplayMember = "folio"
        Me.lkpAnticipo.EditValue = Nothing
        Me.lkpAnticipo.Filtered = False
        Me.lkpAnticipo.InitValue = Nothing
        Me.lkpAnticipo.Location = New System.Drawing.Point(76, 40)
        Me.lkpAnticipo.MultiSelect = False
        Me.lkpAnticipo.Name = "lkpAnticipo"
        Me.lkpAnticipo.NullText = ""
        Me.lkpAnticipo.PopupWidth = CType(420, Long)
        Me.lkpAnticipo.ReadOnlyControl = False
        Me.lkpAnticipo.Required = False
        Me.lkpAnticipo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAnticipo.SearchMember = ""
        Me.lkpAnticipo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAnticipo.SelectAll = False
        Me.lkpAnticipo.Size = New System.Drawing.Size(108, 20)
        Me.lkpAnticipo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpAnticipo.TabIndex = 60
        Me.lkpAnticipo.Tag = "folio"
        Me.lkpAnticipo.ToolTip = Nothing
        Me.lkpAnticipo.ValueMember = "folio"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(15, 135)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 61
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(8, 64)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 62
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "Concepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(76, 65)
        Me.txtConcepto.Name = "txtConcepto"
        '
        'txtConcepto.Properties
        '
        Me.txtConcepto.Properties.Enabled = False
        Me.txtConcepto.Size = New System.Drawing.Size(360, 64)
        Me.txtConcepto.TabIndex = 63
        Me.txtConcepto.Tag = "concepto"
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImporte.Location = New System.Drawing.Point(76, 134)
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "c2"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImporte.Size = New System.Drawing.Size(104, 20)
        Me.clcImporte.TabIndex = 64
        Me.clcImporte.Tag = "importe"
        '
        'frmVentasAnticipos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(449, 164)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.txtConcepto)
        Me.Controls.Add(Me.lblConcepto)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.lblAnticipo)
        Me.Controls.Add(Me.lkpAnticipo)
        Me.Name = "frmVentasAnticipos"
        Me.Text = "frmVentasAnticipos"
        Me.Controls.SetChildIndex(Me.lkpAnticipo, 0)
        Me.Controls.SetChildIndex(Me.lblAnticipo, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto, 0)
        Me.Controls.SetChildIndex(Me.txtConcepto, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private ReadOnly Property Cliente() As Long
        Get
            Return CType(Me.OwnerForm, frmVentas).Cliente
        End Get
    End Property

    Private ReadOnly Property Anticipo() As Long
        Get
            Return PreparaValorLookup(Me.lkpAnticipo)
        End Get
    End Property

    Private Sub frmVentasAnticipos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl

            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select

        End With
        CType(OwnerForm, frmVentas).ObtenerTotalImporteAnticipos()

    End Sub

    Private Sub frmVentasAnticipos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

    End Sub

    Private Sub frmVentasAnticipos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub lkpAnticipo_LoadData(ByVal Initialize As Boolean) Handles lkpAnticipo.LoadData
        Dim Response As New Events
        Response = oAnticipos.Lookup(Me.cliente)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpAnticipo.DataSource = oDataSet.Tables(0)

        End If
    End Sub

    Private Sub lkpAnticipo_Format() Handles lkpAnticipo.Format
        Comunes.clsFormato.for_anticipos_grl(Me.lkpAnticipo)
    End Sub

    Private Sub lkpAnticipo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpAnticipo.EditValueChanged
        If Anticipo <> -1 Then
            Me.txtConcepto.Text = Me.lkpAnticipo.GetValue("concepto")
            Me.clcImporte.EditValue = Me.lkpAnticipo.GetValue("importe")
        End If
    End Sub
End Class
