Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmPaquetesDetalle
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbldescripcion As System.Windows.Forms.Label
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcPrecioContado As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcPrecioCredito As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblModelo As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPaquetesDetalle))
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lbldescripcion = New System.Windows.Forms.Label
        Me.lblPartida = New System.Windows.Forms.Label
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.lblCosto = New System.Windows.Forms.Label
        Me.clcPrecioContado = New Dipros.Editors.TINCalcEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcPrecioCredito = New Dipros.Editors.TINCalcEdit
        Me.lblModelo = New System.Windows.Forms.Label
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioContado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(188, 28)
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(104, 88)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(178, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 64
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = Nothing
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(56, 88)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 63
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(104, 64)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(178, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 62
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(11, 64)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 61
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 67
        Me.Label2.Tag = ""
        Me.Label2.Text = "Descripci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbldescripcion
        '
        Me.lbldescripcion.Location = New System.Drawing.Point(104, 136)
        Me.lbldescripcion.Name = "lbldescripcion"
        Me.lbldescripcion.Size = New System.Drawing.Size(286, 20)
        Me.lbldescripcion.TabIndex = 68
        Me.lbldescripcion.Tag = "n_articulo"
        Me.lbldescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(51, 40)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 59
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "&Partida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(104, 40)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(46, 19)
        Me.clcPartida.TabIndex = 60
        Me.clcPartida.Tag = "Partida"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(48, 112)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 65
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "&Articulo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(104, 112)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(470, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(178, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 66
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "articulo"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(41, 160)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(58, 16)
        Me.lblCantidad.TabIndex = 69
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "&Cantidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCantidad.Location = New System.Drawing.Point(104, 160)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "########0"
        Me.clcCantidad.Properties.MaxLength = 5
        Me.clcCantidad.Properties.Precision = 18
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Size = New System.Drawing.Size(46, 19)
        Me.clcCantidad.TabIndex = 70
        Me.clcCantidad.Tag = "cantidad"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(7, 186)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(92, 16)
        Me.lblCosto.TabIndex = 71
        Me.lblCosto.Tag = ""
        Me.lblCosto.Text = "Precio Contado:"
        Me.lblCosto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecioContado
        '
        Me.clcPrecioContado.EditValue = "0"
        Me.clcPrecioContado.Location = New System.Drawing.Point(104, 184)
        Me.clcPrecioContado.MaxValue = 0
        Me.clcPrecioContado.MinValue = 0
        Me.clcPrecioContado.Name = "clcPrecioContado"
        '
        'clcPrecioContado.Properties
        '
        Me.clcPrecioContado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioContado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioContado.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecioContado.Properties.Precision = 2
        Me.clcPrecioContado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecioContado.Size = New System.Drawing.Size(78, 19)
        Me.clcPrecioContado.TabIndex = 72
        Me.clcPrecioContado.Tag = "precio_contado"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(13, 208)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(86, 16)
        Me.lblImporte.TabIndex = 73
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "Precio Cr�dito:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecioCredito
        '
        Me.clcPrecioCredito.EditValue = "0"
        Me.clcPrecioCredito.Location = New System.Drawing.Point(104, 208)
        Me.clcPrecioCredito.MaxValue = 0
        Me.clcPrecioCredito.MinValue = 0
        Me.clcPrecioCredito.Name = "clcPrecioCredito"
        '
        'clcPrecioCredito.Properties
        '
        Me.clcPrecioCredito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioCredito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioCredito.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecioCredito.Properties.Precision = 2
        Me.clcPrecioCredito.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecioCredito.Size = New System.Drawing.Size(78, 19)
        Me.clcPrecioCredito.TabIndex = 74
        Me.clcPrecioCredito.Tag = "precio_credito"
        '
        'lblModelo
        '
        Me.lblModelo.Location = New System.Drawing.Point(232, 192)
        Me.lblModelo.Name = "lblModelo"
        Me.lblModelo.Size = New System.Drawing.Size(144, 20)
        Me.lblModelo.TabIndex = 75
        Me.lblModelo.Tag = "modelo"
        Me.lblModelo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblModelo.Visible = False
        '
        'frmPaquetesDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 240)
        Me.Controls.Add(Me.lblModelo)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.clcPrecioContado)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcPrecioCredito)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lbldescripcion)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.clcCantidad)
        Me.Name = "frmPaquetesDetalle"
        Me.Text = "frmPaquetesDetalle"
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lbldescripcion, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioCredito, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioContado, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.lblModelo, 0)
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioContado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oPaquetesDetalle As New VillarrealBusiness.clsPaquetesDetalle

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property

    Private Modelo As String = ""
    Private articulo_temporal As Long = 0


#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmPaquetesDetalle_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With

        OwnerForm.CalculaPrecioDescuento()

    End Sub
    Private Sub frmPaquetesDetalle_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.lkpDepartamento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("departamento")), Long)
        Me.lkpGrupo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("grupo")), Long)
        Me.lkpArticulo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("articulo")), Long)

    End Sub
    Private Sub frmPaquetesDetalle_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Select Case Action
            Case Actions.Insert
                Me.clcPartida.Enabled = False

            Case Actions.Update
                Me.clcPartida.Enabled = False
                Me.lkpDepartamento.Enabled = False
                Me.lkpGrupo.Enabled = False
                Me.lkpArticulo.Enabled = False

            Case Actions.Delete
                Me.clcPartida.Enabled = False
                Me.lkpArticulo.Enabled = False
                Me.clcPrecioContado.Enabled = False
                Me.clcPrecioCredito.Enabled = False
                Me.clcCantidad.Enabled = False
        End Select
    End Sub
    Private Sub frmPaquetesDetalle_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oPaquetesDetalle.Validacion(Articulo, Me.clcCantidad.Value, Me.clcPrecioContado.Value, Me.clcPrecioCredito.Value)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo_FilterData(-1)
        Me.lkpGrupo_InitData(-1)
        Me.lbldescripcion.Text = ""

    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpArticulo.EditValue = Nothing
        'Me.lkpArticulo_FilterData(-1)
        'Me.lkpArticulo_InitData(-1)
        Me.lkpArticulo_LoadData(True)
        Me.lbldescripcion.Text = ""

    End Sub
    Private Sub lkpGrupo_FilterData(ByVal Value As Object) Handles lkpGrupo.FilterData
        Dim Response As New Events
        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpGrupo_InitData(ByVal Value As Object) Handles lkpGrupo.InitData
        Dim Response As New Events

        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then
                Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            End If

        End If
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events
        Response = oArticulos.Lookup(Departamento, Grupo, 0)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
        End If
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    'Private Sub lkpArticulo_FilterData(ByVal Value As Object) Handles lkpArticulo.FilterData
    'Dim Response As New Events
    ' Response = oArticulos.Lookup(Departamento, Grupo, 0, Value)
    ' If Not Response.ErrorFound Then
    ' Dim oDataSet As DataSet
    ' oDataSet = Response.Value
    ' Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    'End If
    'End Sub

    'Private Sub lkpArticulo_InitData(ByVal Value As Object) Handles lkpArticulo.InitData
    'Dim Response As New Events

    'Response = oArticulos.Lookup(Departamento, Grupo, 0, , Value)
    'If Not Response.ErrorFound Then
    'Dim oDataSet As DataSet
    ' oDataSet = Response.Value
    'If oDataSet.Tables(0).Rows.Count > 0 Then
    '   Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    ' End If

    'End If
    'End Sub

    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        If Me.lkpArticulo.DataSource Is Nothing Or Me.lkpArticulo.EditValue Is Nothing Then
            Me.clcCantidad.Value = 0
            Me.clcCantidad.Enabled = False
            Exit Sub
        Else
            Me.clcCantidad.Enabled = True
        End If

        If articulo_temporal <> Articulo Then articulo_temporal = Articulo
        Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
        Me.lkpArticulo.EditValue = articulo_temporal
        Me.lbldescripcion.Text = lkpArticulo.GetValue("descripcion_corta")

        Modelo = lkpArticulo.GetValue("modelo")

        Me.lblModelo.Text = Modelo

    End Sub

#End Region


End Class
