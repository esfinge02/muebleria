Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmMovimientosCajaArticulosFactura
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grProductos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvProductos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Ar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dtefecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents clcFolio As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents txtSucursal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosCajaArticulosFactura))
        Me.grProductos = New DevExpress.XtraGrid.GridControl
        Me.grvProductos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.Ar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.dtefecha = New DevExpress.XtraEditors.DateEdit
        Me.clcFolio = New DevExpress.XtraEditors.CalcEdit
        Me.txtSucursal = New DevExpress.XtraEditors.TextEdit
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        CType(Me.grProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtefecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2414, 28)
        '
        'grProductos
        '
        '
        'grProductos.EmbeddedNavigator
        '
        Me.grProductos.EmbeddedNavigator.Name = ""
        Me.grProductos.Location = New System.Drawing.Point(8, 128)
        Me.grProductos.MainView = Me.grvProductos
        Me.grProductos.Name = "grProductos"
        Me.grProductos.Size = New System.Drawing.Size(803, 248)
        Me.grProductos.TabIndex = 60
        Me.grProductos.Text = "GridControl1"
        '
        'grvProductos
        '
        Me.grvProductos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Ar, Me.grcArticulo, Me.grcPrecio, Me.grcCantidad, Me.grcBodega, Me.grcModelo})
        Me.grvProductos.GridControl = Me.grProductos
        Me.grvProductos.Name = "grvProductos"
        Me.grvProductos.OptionsView.ColumnAutoWidth = False
        Me.grvProductos.OptionsView.ShowGroupPanel = False
        '
        'Ar
        '
        Me.Ar.Caption = "Art�culo"
        Me.Ar.FieldName = "articulo"
        Me.Ar.Name = "Ar"
        Me.Ar.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Ar.VisibleIndex = 0
        Me.Ar.Width = 76
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Descripci�n"
        Me.grcArticulo.FieldName = "descripcion_corta"
        Me.grcArticulo.MinWidth = 50
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 2
        Me.grcArticulo.Width = 200
        '
        'grcPrecio
        '
        Me.grcPrecio.Caption = "Precio"
        Me.grcPrecio.DisplayFormat.FormatString = "c2"
        Me.grcPrecio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecio.FieldName = "precio"
        Me.grcPrecio.Name = "grcPrecio"
        Me.grcPrecio.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecio.VisibleIndex = 3
        Me.grcPrecio.Width = 107
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 4
        Me.grcCantidad.Width = 62
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.FieldName = "nombre_bodega"
        Me.grcBodega.MinWidth = 50
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBodega.VisibleIndex = 5
        Me.grcBodega.Width = 210
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 1
        Me.grcModelo.Width = 131
        '
        'dtefecha
        '
        Me.dtefecha.EditValue = New Date(2008, 6, 13, 0, 0, 0, 0)
        Me.dtefecha.Location = New System.Drawing.Point(488, 24)
        Me.dtefecha.Name = "dtefecha"
        '
        'dtefecha.Properties
        '
        Me.dtefecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtefecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dtefecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dtefecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dtefecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dtefecha.Properties.Enabled = False
        Me.dtefecha.Size = New System.Drawing.Size(88, 20)
        Me.dtefecha.TabIndex = 61
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio.Location = New System.Drawing.Point(488, 48)
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolio.Size = New System.Drawing.Size(75, 20)
        Me.clcFolio.TabIndex = 62
        '
        'txtSucursal
        '
        Me.txtSucursal.EditValue = ""
        Me.txtSucursal.Location = New System.Drawing.Point(128, 24)
        Me.txtSucursal.Name = "txtSucursal"
        '
        'txtSucursal.Properties
        '
        Me.txtSucursal.Properties.Enabled = False
        Me.txtSucursal.Size = New System.Drawing.Size(224, 20)
        Me.txtSucursal.TabIndex = 63
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(128, 48)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.Enabled = False
        Me.txtSerie.Size = New System.Drawing.Size(75, 20)
        Me.txtSerie.TabIndex = 64
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(64, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 65
        Me.Label2.Text = "Sucursal:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(80, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 16)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "Serie:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(448, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 16)
        Me.Label4.TabIndex = 67
        Me.Label4.Text = "Folio:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(440, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 16)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "Fecha:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.dtefecha)
        Me.GroupBox1.Controls.Add(Me.txtSerie)
        Me.GroupBox1.Controls.Add(Me.txtSucursal)
        Me.GroupBox1.Controls.Add(Me.clcFolio)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(81, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(656, 88)
        Me.GroupBox1.TabIndex = 69
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Factura:"
        '
        'frmMovimientosCajaArticulosFactura
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(818, 384)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grProductos)
        Me.Name = "frmMovimientosCajaArticulosFactura"
        Me.Text = "frmMovimientosCajaArticulosFactura"
        Me.Controls.SetChildIndex(Me.grProductos, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.grProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtefecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Public FormaPadre As Dipros.Windows.frmTINForm

    Private oVentasDetalle As New VillarrealBusiness.clsVentasDetalle

    Private lSucursal_factura As Long
    'Private lserie_factura As String
    'Private lfolio_factura As Long
    'Private dfecha_factura As DateTime


    Public Property Sucursal_factura() As Long
        Get
            Return lSucursal_factura
        End Get
        Set(ByVal Value As Long)
            lSucursal_factura = Value
        End Set
    End Property

    Public Property NombreSucursal() As String
        Get
            Return Me.txtSucursal.Text
        End Get
        Set(ByVal Value As String)
            Me.txtSucursal.Text = Value
        End Set
    End Property

    Public Property serie_factura() As String
        Get
            Return Me.txtSerie.Text
        End Get
        Set(ByVal Value As String)
            Me.txtSerie.Text = Value
        End Set
    End Property

    Public Property folio_factura() As Long
        Get
            Return Me.clcFolio.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcFolio.Value = Value
        End Set
    End Property

    Public Property fecha_factura() As DateTime
        Get
            Return Me.dtefecha.DateTime
        End Get
        Set(ByVal Value As DateTime)
            Me.dtefecha.DateTime = Value
        End Set
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmVerProductos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        tbrTools.Buttons(0).Visible = False
        tbrTools.Buttons(0).Enabled = False

        FormaPadre.Enabled = False
        Response = oVentasDetalle.CajaArticulosFactura(Sucursal_factura, serie_factura, folio_factura)
        If Response.ErrorFound Then
            Response.ShowError()
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            grProductos.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub frmVerProductos_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Disposed
        FormaPadre.Enabled = True
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los Controles"
#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region

End Class
