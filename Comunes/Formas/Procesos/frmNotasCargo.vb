Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Imports System.Drawing

Public Class frmNotasCargo
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblconcepto As System.Windows.Forms.Label
    Friend WithEvents lkpconcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents tlbrAceptar As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup

    Public WithEvents lblTipoventa As System.Windows.Forms.Label
    Friend WithEvents cboTipoventa As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblConvenioCambiado As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    Friend WithEvents UcSolicitaFormaPago2 As Comunes.ucSolicitaFormaPago
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmNotasCargo))
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblconcepto = New System.Windows.Forms.Label
        Me.lkpconcepto = New Dipros.Editors.TINMultiLookup
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.tlbrAceptar = New System.Windows.Forms.ToolBarButton
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboTipoventa = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblConvenioCambiado = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        Me.UcSolicitaFormaPago2 = New Comunes.ucSolicitaFormaPago
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tlbrAceptar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1208, 28)
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(8, 160)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 10
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(104, 157)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservaciones.Size = New System.Drawing.Size(360, 38)
        Me.txtObservaciones.TabIndex = 11
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(40, 136)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 8
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(104, 135)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(96, 19)
        Me.clcImporte.TabIndex = 9
        Me.clcImporte.Tag = "importe"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(56, 112)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(104, 109)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(96, 23)
        Me.dteFecha.TabIndex = 7
        Me.dteFecha.Tag = ""
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(48, 64)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 2
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cl&iente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(104, 63)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = True
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(360, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 3
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lblconcepto
        '
        Me.lblconcepto.AutoSize = True
        Me.lblconcepto.Location = New System.Drawing.Point(24, 88)
        Me.lblconcepto.Name = "lblconcepto"
        Me.lblconcepto.Size = New System.Drawing.Size(73, 16)
        Me.lblconcepto.TabIndex = 4
        Me.lblconcepto.Tag = ""
        Me.lblconcepto.Text = "&Movimiento:"
        Me.lblconcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpconcepto
        '
        Me.lkpconcepto.AllowAdd = False
        Me.lkpconcepto.AutoReaload = True
        Me.lkpconcepto.DataSource = Nothing
        Me.lkpconcepto.DefaultSearchField = ""
        Me.lkpconcepto.DisplayMember = "descripcion"
        Me.lkpconcepto.EditValue = Nothing
        Me.lkpconcepto.Filtered = False
        Me.lkpconcepto.InitValue = Nothing
        Me.lkpconcepto.Location = New System.Drawing.Point(104, 86)
        Me.lkpconcepto.MultiSelect = False
        Me.lkpconcepto.Name = "lkpconcepto"
        Me.lkpconcepto.NullText = ""
        Me.lkpconcepto.PopupWidth = CType(400, Long)
        Me.lkpconcepto.ReadOnlyControl = False
        Me.lkpconcepto.Required = False
        Me.lkpconcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpconcepto.SearchMember = ""
        Me.lkpconcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpconcepto.SelectAll = False
        Me.lkpconcepto.Size = New System.Drawing.Size(360, 20)
        Me.lkpconcepto.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpconcepto.TabIndex = 5
        Me.lkpconcepto.Tag = ""
        Me.lkpconcepto.ToolTip = Nothing
        Me.lkpconcepto.ValueMember = "concepto"
        '
        'chkDesglosado
        '
        Me.chkDesglosado.EditValue = True
        Me.chkDesglosado.Location = New System.Drawing.Point(104, 224)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "I.V.A. D&esglosado"
        Me.chkDesglosado.Properties.Enabled = False
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(128, 19)
        Me.chkDesglosado.TabIndex = 14
        Me.chkDesglosado.Tag = ""
        '
        'tlbrAceptar
        '
        Me.tlbrAceptar.ImageIndex = 0
        Me.tlbrAceptar.Text = "Aceptar"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(39, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(104, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(360, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(16, 248)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(95, 16)
        Me.lblTipoventa.TabIndex = 15
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "Opci�n de Pago:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoventa
        '
        Me.cboTipoventa.EditValue = "D"
        Me.cboTipoventa.Location = New System.Drawing.Point(120, 248)
        Me.cboTipoventa.Name = "cboTipoventa"
        '
        'cboTipoventa.Properties
        '
        Me.cboTipoventa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoventa.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pago en una sola Exhibici�n", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pago en 1 Parcialidad", "C", -1)})
        Me.cboTipoventa.Size = New System.Drawing.Size(168, 23)
        Me.cboTipoventa.TabIndex = 16
        Me.cboTipoventa.Tag = ""
        '
        'lblConvenioCambiado
        '
        Me.lblConvenioCambiado.AutoSize = True
        Me.lblConvenioCambiado.Location = New System.Drawing.Point(37, 200)
        Me.lblConvenioCambiado.Name = "lblConvenioCambiado"
        Me.lblConvenioCambiado.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenioCambiado.TabIndex = 12
        Me.lblConvenioCambiado.Tag = ""
        Me.lblConvenioCambiado.Text = "Convenio:"
        Me.lblConvenioCambiado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Enabled = False
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(104, 198)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = ""
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(360, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 13
        Me.lkpConvenio.Tag = ""
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'UcSolicitaFormaPago2
        '
        Me.UcSolicitaFormaPago2.Location = New System.Drawing.Point(8, 280)
        Me.UcSolicitaFormaPago2.Name = "UcSolicitaFormaPago2"
        Me.UcSolicitaFormaPago2.Size = New System.Drawing.Size(456, 80)
        Me.UcSolicitaFormaPago2.TabIndex = 59
        '
        'frmNotasCargo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(474, 364)
        Me.Controls.Add(Me.lblConvenioCambiado)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.lblTipoventa)
        Me.Controls.Add(Me.cboTipoventa)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.lblconcepto)
        Me.Controls.Add(Me.lkpconcepto)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.UcSolicitaFormaPago2)
        Me.Name = "frmNotasCargo"
        Me.Text = "frmNotasCargo"
        Me.Controls.SetChildIndex(Me.UcSolicitaFormaPago2, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lkpconcepto, 0)
        Me.Controls.SetChildIndex(Me.lblconcepto, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.cboTipoventa, 0)
        Me.Controls.SetChildIndex(Me.lblTipoventa, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioCambiado, 0)
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas
    Private oConceptos_cxc As VillarrealBusiness.clsConceptosCxc
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oMovimientosCobrarFormasPago as VillarrealBusiness.clsMovimientosCobrarFormasPago
    Private oVariables As VillarrealBusiness.clsVariables
    Private oReportes As VillarrealBusiness.Reportes
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oConvenios As VillarrealBusiness.clsConvenios
    Private oVales As VillarrealBusiness.clsVales
    Private oValesDetalle As VillarrealBusiness.clsValesDetalle



    Private Folio_Movimiento As Long = -1
    Private ImportePagar As Double
    Private banvalidar As Boolean = False
    Private fila_seleccionada As Integer
    Private factura_electronica As Boolean = False
    Private SerieNCargo As String = ""
    Private SerieAbono As String = ""
    Private ConceptoAbono As String
    Private banConceptoAbono As Boolean = False
    Private SucursalDependencia As Boolean = False



    'Public ConceptoNotacargo As String
    'Private banConceptoNotacargo As Boolean = False
    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property

    Private ReadOnly Property Concepto() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpconcepto)
        End Get
    End Property
    Private ReadOnly Property ConceptoFacturaCredito() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
 
    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Tipo_Cambio() As Double
        Get
            Return Comunes.clsUtilerias.uti_TipoCambio(Comunes.Common.Cajero, TinApp.FechaServidor)
        End Get
    End Property

    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"


    Private Sub frmNotasCargo_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
       

    End Sub
    Private Sub frmNotasCargo_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle

        oConceptos_cxc = New VillarrealBusiness.clsConceptosCxc
        oVariables = New VillarrealBusiness.clsVariables
        oReportes = New VillarrealBusiness.Reportes
        oSucursales = New VillarrealBusiness.clsSucursales
        oConvenios = New VillarrealBusiness.clsConvenios
        oVales = New VillarrealBusiness.clsVales
        oValesDetalle = New VillarrealBusiness.clsValesDetalle


        Me.Location = New Point(0, 0)
        Me.Size = New Size(480, 272)

        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
        Me.tbrTools.Buttons(0).Visible = False

        TraeConceptoAbono()


      
        If Comunes.clsUtilerias.uti_Usuariocajero(TinApp.Connection.User, Comunes.Common.Cajero) = False Then
            Me.cboTipoventa.Value = "C"
            Me.cboTipoventa.Enabled = False
            ShowMessage(MessageType.MsgInformation, "Este usuario No tiene un Cajero asignado", Me.Title)
        End If

        
    End Sub
    Private Sub frmNotasCargo_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = Me.oMovimientosCobrar.ValidacionNotaCargo(Action, Sucursal, Me.Cliente, Me.Concepto, Me.clcImporte.EditValue, Me.txtObservaciones.Text, Me.factura_electronica, Me.cboTipoventa.Value, Me.UcSolicitaFormaPago2.FormaPago, Me.UcSolicitaFormaPago2.SolicitaUltimosDigitos, Me.UcSolicitaFormaPago2.UltimosDigitos, SucursalDependencia, Me.Convenio, UcSolicitaFormaPago2.SolicitaFolioVale, UcSolicitaFormaPago2.FolioVale)
        If Not Response.ErrorFound Then

            If Me.UcSolicitaFormaPago2.SolicitaFolioVale Then
                Response = ValidaVale()
            End If


            If Not Response.ErrorFound Then
                banvalidar = True
            End If

        End If
    End Sub

    Private Sub frmNotasCargo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.MenuOption.Name, "FECHA_N_C") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If
    End Sub


#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        If Me.Sucursal > 0 Then
            Me.factura_electronica = CType(Me.lkpSucursal.GetValue("factura_electronica"), Boolean)
            SerieNCargo = CType(Me.lkpSucursal.GetValue("serie_nota_cargo_electronica"), String)
            SerieAbono = CType(lkpSucursal.GetValue("serie_recibos_electronica"), String)
            SucursalDependencia = CType(Me.lkpSucursal.GetValue("sucursal_dependencia"), Boolean)

            
            Me.lkpConvenio.Enabled = SucursalDependencia


            If Me.factura_electronica Then
                Me.chkDesglosado.Checked = True
                Me.chkDesglosado.Enabled = False
                Me.Size = New Size(480, 392)
            Else
                Me.chkDesglosado.Checked = False
                Me.chkDesglosado.Enabled = True
                Me.Size = New Size(480, 272)
            End If
        End If
    End Sub

    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpconcepto.LoadData
        Dim Response As New Events
        ' es true para que solo muestre los conceptos que se pueden hacer en este modulo
        Response = oConceptos_cxc.Lookup("C", True)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpconcepto.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpConcepto_Format() Handles lkpconcepto.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpconcepto)
    End Sub


    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub

    Private Sub clcImporte_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcImporte.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing

        If IsNumeric(Me.clcImporte.EditValue) = True Then ImportePagar = Me.clcImporte.EditValue
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tlbrAceptar And banvalidar = True Then
            Dim folio As Long = 0
            Dim response As New Events
            Dim impuesto As Double = 0
            Dim subtotal As Double = 0
            Dim iva As Double = 0

            TinApp.Connection.Begin()

            If Not factura_electronica Then
                SerieNCargo = Comunes.clsUtilerias.uti_SerieCajaNotaCargo(Comunes.Common.Caja_Actual)
            End If


            impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)
            If impuesto <> -1 Then

                subtotal = Me.clcImporte.Value / (1 + (impuesto / 100))
                iva = Me.clcImporte.Value - subtotal
            Else
                TinApp.Connection.Rollback()
                ShowMessage(MessageType.MsgInformation, "El Porcentaje de Impuesto no esta definido", "Variables del Sistema", Nothing, False)
                Exit Sub
            End If

            response = oMovimientosCobrar.InsertarCajas(Sucursal, Concepto, SerieNCargo, Cliente, 0, Me.dteFecha.DateTime.Date, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, 1, Me.clcImporte.EditValue, 0, subtotal, iva, Me.clcImporte.Value, Me.clcImporte.Value, System.DBNull.Value, Me.txtObservaciones.Text, "", Sucursal, folio, , Me.Convenio)
            If response.ErrorFound Then
                TinApp.Connection.Rollback()
                response.ShowError()
                Exit Sub
            End If

            'DAM 17/05/2006 -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO
            Dim observaciones As String
            observaciones = "Nota de Cargo " + Me.txtObservaciones.Text


            response = oMovimientosCobrarDetalle.Insertar(Sucursal, Concepto, SerieNCargo, folio, Cliente, 1, 0, Me.dteFecha.DateTime.Date, Me.clcImporte.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, 0, Me.dteFecha.DateTime.Date, Me.clcImporte.EditValue, "", observaciones, 0)
            If response.ErrorFound Then
                TinApp.Connection.Rollback()
                response.ShowError()
                Exit Sub
            End If



            'Se verifica si usa factura electronica 
            If Me.factura_electronica Then
                If Me.cboTipoventa.Value = "D" Then
                    Dim FolioAbono As Long = 0
                    Dim CobradorAbono As Long

                    CobradorAbono = ObtenerCobradorAbonoElectronico(Cliente, Sucursal)

                    'Se inserta un movimiento por cobrar de Tipo Abono electronico para pagar la nota de cargo electronica
                    response = Me.oMovimientosCobrar.InsertarCajas(Sucursal, ConceptoAbono, SerieAbono, Cliente, 1, Me.dteFecha.DateTime.Date, Comunes.Common.Caja_Actual, Comunes.Common.Cajero, CobradorAbono, 1, 0, Me.clcImporte.Value, subtotal, iva, Me.clcImporte.Value, 0, System.DBNull.Value, "Abono generado por nota de cargo electronica", "", Sucursal, FolioAbono)
                    If Not response.ErrorFound Then
                        response = oMovimientosCobrarDetalle.Insertar(Sucursal, ConceptoAbono, SerieAbono, FolioAbono, Cliente, 1, 1, Me.dteFecha.DateTime.Date, Me.clcImporte.Value, Sucursal, Concepto, SerieNCargo, folio, Cliente, 1, 0, Me.dteFecha.DateTime.Date, 0, "N", "Abono Generado por una Nota de Cargo Electronica", 0)
                    End If

                    If Not response.ErrorFound Then
                        Dim dolares As Double = 0
                        Dim TipoCambio As Double = Me.Tipo_Cambio

                        If Me.UcSolicitaFormaPago2.ManejaDolares Then
                            dolares = Me.clcImporte.EditValue / TipoCambio
                        End If

                        oMovimientosCobrarFormasPago = New VillarrealBusiness.clsMovimientosCobrarFormasPago

                        response = oMovimientosCobrarFormasPago.Insertar(Sucursal, ConceptoAbono, SerieAbono, FolioAbono, Cliente, Me.UcSolicitaFormaPago2.FormaPago, Common.Sucursal_Actual, Me.dteFecha.DateTime, Common.Caja_Actual, Common.Cajero, Me.clcImporte.EditValue, TipoCambio, dolares, Me.UcSolicitaFormaPago2.UltimosDigitos)

                        If Not response.ErrorFound Then
                            If Me.UcSolicitaFormaPago2.SolicitaFolioVale Then
                                response = oValesDetalle.Insertar(Me.UcSolicitaFormaPago2.FolioVale, Sucursal, ConceptoAbono, SerieAbono, FolioAbono, Me.dteFecha.DateTime, Me.clcImporte.Value)
                            End If
                        End If


                    End If
                End If


                If Not response.ErrorFound Then
                    'Se llena la nota de cargo Electronica
                    response = Me.oMovimientosCobrar.LllenarNotaCargoCFDI(Sucursal, Concepto, SerieNCargo, folio, Cliente, Me.chkDesglosado.Checked, Common.Sucursal_Actual)
                End If

                'Fin de movimientos para factura electronica
            End If




            TinApp.Connection.Commit()

            response = oClientes.Recalcula_saldo_documentos(Cliente)
            If (Not response.ErrorFound) And (Not Me.factura_electronica) Then
                Me.ImprimirNotaCargo(Concepto, Me.SerieNCargo, 0, folio)
            End If

            Limpiar()
            banvalidar = False

            If Not response.ErrorFound Then
                Me.Close()

            End If
        End If
    End Sub

    Private Sub cboTipoventa_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoventa.EditValueChanged
        If Me.cboTipoventa.Value = "C" Then
            Me.UcSolicitaFormaPago2.EnabledAll = False
        Else
            Me.UcSolicitaFormaPago2.EnabledAll = True
        End If
    End Sub

    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Public Sub Limpiar()
        Me.clcImporte.EditValue = 0
        'Me.chkDesglosado.EditValue = False
        '      Me.lkpCliente.EditValue = 
        '     Me.lkpconcepto.EditValue = 
        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
        Me.txtObservaciones.Text = ""
    End Sub

    Private Sub ImprimirNotaCargo(ByVal conceptointereses As String, ByVal serieinteres As String, ByVal saldo_intereses As Double, ByVal foliointeres As Long)
        Dim response As New Events

        response = oReportes.ImprimeNotaDeCargo(Sucursal, conceptointereses, serieinteres, foliointeres, Cliente, Me.chkDesglosado.EditValue)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La Nota de Cargo no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New Comunes.rptNotaCargoDescuentosAnticipados   'Clientes.rptNotaCargo

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cargo ", oReport)
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

    Private Sub TraeConceptoAbono()
        ConceptoAbono = CType(oVariables.TraeDatos("concepto_cxc_abono", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoAbono <> "" Then
            banConceptoAbono = True
        Else
            ShowMessage(MessageType.MsgInformation, "El Concepto de CXC de Abono no esta definido", "Variables del Sistema", Nothing, False)
        End If
    End Sub

    Private Function ObtenerCobradorAbonoElectronico(ByVal cliente_abono As Long, ByVal sucursal_abono As Long) As Long

        Dim cajeros As New VillarrealBusiness.clsClientesCobradores
        Dim Response As Events

        Response = cajeros.DespliegaDatosCobradoresVentas(cliente_abono, sucursal_abono)
        If Not Response.ErrorFound Then
            Dim odataset As DataSet

            odataset = CType(Response.Value, DataSet)

            If odataset.Tables(0).Rows.Count = 0 Then
                Return 15
            Else
                Return CType(odataset.Tables(0).Rows(0).Item("cobrador"), Long)

            End If

        Else
            Return 15
        End If

    End Function

    Private Function ValidaVale() As Events
        Dim response As New Events

        response = oVales.ValidaVale(Me.UcSolicitaFormaPago2.FolioVale, Me.Cliente, Me.clcImporte.EditValue)
        If Not response.ErrorFound Then

            Dim resultado As Long = response.Value()

            Select Case resultado
                'Case 0 'Es correcto el vale

                Case 1 ' El folio del Vale no Existe
                    response.Message = "El folio del Vale no Existe"
                Case 2 ' El Saldo del Vale no es Suficiente
                    response.Message = "El Saldo del Vale no es Suficiente"
                Case 3 ' La vigencia del Saldo ya Expiro.                    
                    response.Message = "La vigencia del Saldo ya Expiro"
                Case 4 ' El vale no esta asignado al Cliente
                    response.Message = "El vale no esta asignado al Cliente"

            End Select
        End If

        Return response
    End Function

#End Region

   
End Class
