Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Drawing

Public Class frmTimbrarFacturas
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Public WithEvents lblTipoventa As System.Windows.Forms.Label
    Friend WithEvents cboTipoventa As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents UcSolicitaFormaPago2 As Comunes.ucSolicitaFormaPago
    Friend WithEvents lkpSerieFactura As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpFolioFactura As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTimbrarFacturas))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboTipoventa = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpSerieFactura = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpFolioFactura = New Dipros.Editors.TINMultiLookup
        Me.UcSolicitaFormaPago2 = New Comunes.ucSolicitaFormaPago
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(637, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(35, 48)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(96, 48)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(328, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(8, 120)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(84, 16)
        Me.lblTipoventa.TabIndex = 6
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "&Tipo de venta:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoventa
        '
        Me.cboTipoventa.EditValue = "C"
        Me.cboTipoventa.Location = New System.Drawing.Point(96, 120)
        Me.cboTipoventa.Name = "cboTipoventa"
        '
        'cboTipoventa.Properties
        '
        Me.cboTipoventa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoventa.Properties.Enabled = False
        Me.cboTipoventa.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Contado", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cr�dito", "C", -1)})
        Me.cboTipoventa.Size = New System.Drawing.Size(72, 23)
        Me.cboTipoventa.TabIndex = 7
        Me.cboTipoventa.Tag = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "Serie Factura:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSerieFactura
        '
        Me.lkpSerieFactura.AllowAdd = False
        Me.lkpSerieFactura.AutoReaload = True
        Me.lkpSerieFactura.DataSource = Nothing
        Me.lkpSerieFactura.DefaultSearchField = ""
        Me.lkpSerieFactura.DisplayMember = "serie"
        Me.lkpSerieFactura.EditValue = Nothing
        Me.lkpSerieFactura.Filtered = False
        Me.lkpSerieFactura.InitValue = Nothing
        Me.lkpSerieFactura.Location = New System.Drawing.Point(96, 72)
        Me.lkpSerieFactura.MultiSelect = False
        Me.lkpSerieFactura.Name = "lkpSerieFactura"
        Me.lkpSerieFactura.NullText = ""
        Me.lkpSerieFactura.PopupWidth = CType(400, Long)
        Me.lkpSerieFactura.ReadOnlyControl = False
        Me.lkpSerieFactura.Required = False
        Me.lkpSerieFactura.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSerieFactura.SearchMember = ""
        Me.lkpSerieFactura.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSerieFactura.SelectAll = False
        Me.lkpSerieFactura.Size = New System.Drawing.Size(112, 20)
        Me.lkpSerieFactura.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSerieFactura.TabIndex = 3
        Me.lkpSerieFactura.Tag = ""
        Me.lkpSerieFactura.ToolTip = Nothing
        Me.lkpSerieFactura.ValueMember = "serie"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(56, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Tag = ""
        Me.Label3.Text = "Folio:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFolioFactura
        '
        Me.lkpFolioFactura.AllowAdd = False
        Me.lkpFolioFactura.AutoReaload = True
        Me.lkpFolioFactura.DataSource = Nothing
        Me.lkpFolioFactura.DefaultSearchField = ""
        Me.lkpFolioFactura.DisplayMember = "folio"
        Me.lkpFolioFactura.EditValue = Nothing
        Me.lkpFolioFactura.Filtered = False
        Me.lkpFolioFactura.InitValue = Nothing
        Me.lkpFolioFactura.Location = New System.Drawing.Point(96, 96)
        Me.lkpFolioFactura.MultiSelect = False
        Me.lkpFolioFactura.Name = "lkpFolioFactura"
        Me.lkpFolioFactura.NullText = ""
        Me.lkpFolioFactura.PopupWidth = CType(400, Long)
        Me.lkpFolioFactura.ReadOnlyControl = False
        Me.lkpFolioFactura.Required = False
        Me.lkpFolioFactura.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolioFactura.SearchMember = ""
        Me.lkpFolioFactura.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolioFactura.SelectAll = False
        Me.lkpFolioFactura.Size = New System.Drawing.Size(112, 20)
        Me.lkpFolioFactura.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolioFactura.TabIndex = 5
        Me.lkpFolioFactura.Tag = ""
        Me.lkpFolioFactura.ToolTip = Nothing
        Me.lkpFolioFactura.ValueMember = "folio"
        '
        'UcSolicitaFormaPago2
        '
        Me.UcSolicitaFormaPago2.Location = New System.Drawing.Point(9, 152)
        Me.UcSolicitaFormaPago2.Name = "UcSolicitaFormaPago2"
        Me.UcSolicitaFormaPago2.Size = New System.Drawing.Size(424, 80)
        Me.UcSolicitaFormaPago2.TabIndex = 8
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(280, 74)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 59
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(328, 72)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(96, 23)
        Me.dteFecha.TabIndex = 60
        Me.dteFecha.Tag = ""
        '
        'frmTimbrarFacturas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(442, 244)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpFolioFactura)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpSerieFactura)
        Me.Controls.Add(Me.lblTipoventa)
        Me.Controls.Add(Me.cboTipoventa)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.UcSolicitaFormaPago2)
        Me.Name = "frmTimbrarFacturas"
        Me.Text = "Timbrado de Facturas"
        Me.Controls.SetChildIndex(Me.UcSolicitaFormaPago2, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.cboTipoventa, 0)
        Me.Controls.SetChildIndex(Me.lblTipoventa, 0)
        Me.Controls.SetChildIndex(Me.lkpSerieFactura, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpFolioFactura, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Private oVariables As VillarrealBusiness.clsVariables
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oMovimientosCobrarFormasPago As VillarrealBusiness.clsMovimientosCobrarFormasPago


    Private ConceptoFactura As String = ""
    Private IvaDesglosado As Boolean = True
    Private ConceptoAbono As String
    Private banConceptoAbono As Boolean = False
    Private SerieAbono As String = ""
    Private FolioAbono As Long = 0
    Private impuesto As Double = 0
    Private subtotal As Double = 0
    Private Total As Double = 0
    Private ClienteFactura As Long = 0
    Private CobradorFactura As Long = 0
    Private EsEnajenacion As Boolean = False
    Private dIva As Double = 0.0
    Private FactorEnajenacion As Double = 0.0
    Private dImporte_proporcional_costo As Double = 0.0
    Private Menos As Double = 0.0





    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property Serie() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerieFactura)
        End Get
    End Property
    Private ReadOnly Property Folio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolioFactura)
        End Get
    End Property
    Private ReadOnly Property Tipo_Cambio() As Double
        Get
            Return Comunes.clsUtilerias.uti_TipoCambio(Comunes.Common.Cajero, TinApp.FechaServidor)
        End Get
    End Property

    Private Sub TraeConceptoAbono()
        ConceptoAbono = CType(oVariables.TraeDatos("concepto_cxc_abono", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoAbono <> "" Then
            banConceptoAbono = True
        Else
            ShowMessage(MessageType.MsgInformation, "El Concepto de CXC de Abono no esta definido", "Variables del Sistema", Nothing, False)
        End If
    End Sub


    Private Sub frmTimbrarFacturas_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmTimbrarFacturas_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()

        Me.ImprimeFactura(Me.Folio, Me.IvaDesglosado)
        If Me.Menos > 0 Then
            ImprimirTicket(Me.Folio)
        End If
    End Sub
    Private Sub frmTimbrarFacturas_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub

    Private Sub frmTimbrarFacturas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oVariables = New VillarrealBusiness.clsVariables
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oMovimientosCobrarFormasPago = New VillarrealBusiness.clsMovimientosCobrarFormasPago

        Me.Location = New Point(0, 0)
        Me.dteFecha.EditValue = CDate(TinApp.FechaServidor)

        TraeConceptoAbono()

        If Common.Cajero < 1 Then
            Me.tbrTools.Buttons(0).Enabled = False
            Me.lkpSucursal.Enabled = False
            Me.lkpSerieFactura.Enabled = False
            Me.lkpFolioFactura.Enabled = False
            Me.UcSolicitaFormaPago2.Enabled = False
            ShowMessage(MessageType.MsgInformation, "El usuario NO tiene asignado un cajero para Timbrar", "Timbrar Facturas")

        End If

        Me.lkpSucursal.EditValue = Common.Sucursal_Actual
        Me.lkpSucursal.Enabled = False

        Me.UcSolicitaFormaPago2.EnabledAll = True
    End Sub
    Private Sub frmTimbrarFacturas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        'Response = oVentas.TimbrarFactura(Sucursal, Serie, Folio, ConceptoFactura, IvaDesglosado, Common.Sucursal_Actual)

        If Me.cboTipoventa.Value = "D" Then

            Me.ObtenerSerieFolioAbonosElectronicos(SerieAbono)
            Response = oMovimientosCobrar.InsertarCajas(Sucursal, Me.ConceptoAbono, Me.SerieAbono, Me.ClienteFactura, 0, Me.dteFecha.EditValue, Comunes.Common.Caja_Actual, Comunes.Common.Cajero, Me.CobradorFactura, 1, 0, Me.Total, Me.subtotal, Me.impuesto, Me.Total, 0, System.DBNull.Value, "", "", Sucursal, Me.FolioAbono)

            If Not Response.ErrorFound Then
                Dim sObservaciones As String
                sObservaciones = "Abono a la Referencia " + CType(Me.Serie, String) + "-" + CType(Me.Folio, String) + " al documento " + CType(1, String)
                Response = oMovimientosCobrarDetalle.Insertar(Sucursal, ConceptoAbono, SerieAbono, Me.FolioAbono, Me.ClienteFactura, 1, 0, Me.dteFecha.DateTime.Date, Me.Total, Me.Sucursal, ConceptoFactura, Me.Serie, Me.Folio, Me.ClienteFactura, 1, 0, Me.dteFecha.DateTime.Date, 0, "C", sObservaciones, dImporte_proporcional_costo * -1)

                If Not Response.ErrorFound Then
                    Dim dolares As Double = 0
                    Dim TipoCambio As Double = Me.Tipo_Cambio

                    If Me.UcSolicitaFormaPago2.ManejaDolares Then
                        dolares = Me.Total / TipoCambio
                    End If
                    Response = Me.oMovimientosCobrarFormasPago.Insertar(Sucursal, ConceptoAbono, SerieAbono, Me.FolioAbono, Me.ClienteFactura, Me.UcSolicitaFormaPago2.FormaPago, Sucursal, Me.dteFecha.DateTime, Common.Caja_Actual, Common.Cajero, Me.Total, TipoCambio, dolares, Me.UcSolicitaFormaPago2.UltimosDigitos)
                End If
            End If
        End If

        If Not Response.ErrorFound Then
            Response = oVentas.TimbrarFactura(Sucursal, Serie, Folio, ConceptoFactura, IvaDesglosado, Common.Sucursal_Actual)
        End If
       


    End Sub


    Private Sub frmTimbrarFacturas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oVentas.ValidaTimbradoFacturas(Me.Sucursal, Me.Serie, Me.Folio, Me.cboTipoventa.Value, Me.UcSolicitaFormaPago2.FormaPago, Me.UcSolicitaFormaPago2.SolicitaUltimosDigitos, Me.UcSolicitaFormaPago2.UltimosDigitos)
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpSerieFactura_Format() Handles lkpSerieFactura.Format
        Comunes.clsFormato.for_series_ventas_grl(Me.lkpSerieFactura)
    End Sub
    Private Sub lkpSerieFactura_LoadData(ByVal Initialize As Boolean) Handles lkpSerieFactura.LoadData
        Dim Response As New Events
        Response = oVentas.ObtenerFacturasTimbrar(True, Me.Sucursal)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSerieFactura.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSerieFactura_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSerieFactura.EditValueChanged
        Me.lkpFolioFactura.DataSource = Nothing
    End Sub


    Private Sub lkpFolioFactura_LoadData(ByVal Initialize As Boolean) Handles lkpFolioFactura.LoadData
        Dim Response As New Events
        Response = oVentas.ObtenerFacturasTimbrar(False, Me.Sucursal, Me.Serie)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFolioFactura.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpFolioFactura_Format() Handles lkpFolioFactura.Format
        Comunes.clsFormato.for_folio_ventas_grl(Me.lkpFolioFactura)
    End Sub
    Private Sub lkpFolioFactura_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolioFactura.EditValueChanged
        If Me.Folio > 0 Then
            Me.cboTipoventa.EditValue = Me.lkpFolioFactura.GetValue("tipoventa")
            'Me.IvaDesglosado = Me.lkpFolioFactura.GetValue("ivadesglosado")
            Me.ConceptoFactura = Me.lkpFolioFactura.GetValue("concepto")
            Me.ClienteFactura = Me.lkpFolioFactura.GetValue("cliente")
            Me.impuesto = Me.lkpFolioFactura.GetValue("impuesto")
            Me.subtotal = Me.lkpFolioFactura.GetValue("subtotal")
            Me.Total = Me.lkpFolioFactura.GetValue("total")
            Me.CobradorFactura = Me.lkpFolioFactura.GetValue("cobrador")
            Me.EsEnajenacion = Me.lkpFolioFactura.GetValue("enajenacion")
            Me.dIva = Me.lkpFolioFactura.GetValue("dIva")
            Me.FactorEnajenacion = Me.lkpFolioFactura.GetValue("FactorEnajenacion")
            Me.dImporte_proporcional_costo = (Total / dIva) * FactorEnajenacion
            Me.cboTipoventa.EditValue = Me.lkpFolioFactura.GetValue("tipoventa")
            Me.Menos = Me.lkpFolioFactura.GetValue("menos")

            If Me.cboTipoventa.EditValue = "C" Then
                'Credito
                Me.UcSolicitaFormaPago2.EnabledAll = False

            Else
                'Contado
                Me.UcSolicitaFormaPago2.EnabledAll = True
            End If

        End If
    End Sub


    Private Sub ObtenerSerieFolioAbonosElectronicos(ByRef serie As String)
        Dim Response As Dipros.Utils.Events
        Dim odataset As DataSet
        serie = ""


        Response = oSucursales.DespliegaDatos(Sucursal)
        If Response.ErrorFound = False Then
            odataset = Response.Value
            serie = CType(odataset.Tables(0).Rows(0).Item("serie_recibos_electronica"), String)

        End If
    End Sub

    Private Sub ImprimeFactura(ByVal folio As Long, ByVal ivadesglosado As Boolean)
        Dim response As New Events

        response = oVentas.ImpresionFactura(Sucursal, Serie, folio, ConceptoFactura, ivadesglosado)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La Impresi�n de la Factura no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptFacturaFormato
                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)
                oReport.txtLeyendaReparto.Text = "REPARTO"
                oReport.txtLeyendaReparto.Visible = True


                If TinApp.Connection.User.ToUpper = "SUPER" Then
                    TinApp.ShowReport(Me.MdiParent, "Factura Electronica", oReport, False, Nothing, True, True)
                Else
                    TinApp.PrintReport(oReport, False, True)
                End If
                oReport = Nothing


                oDataSet = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "La Factura no tiene Informaci�n")
            End If

        End If

    End Sub

    Private Sub ImprimirTicket(ByVal folio As Long)
        Dim response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim RutaImpresoraTicket As String
        RutaImpresoraTicket = clsUtilerias.uti_RutaImpresionTicket(Common.Caja_Actual)

        If RutaImpresoraTicket.Trim.Length = 0 Then
            ShowMessage(MessageType.MsgError, "No se puede imprimir el ticket por que la caja no tiene una ruta de impresi�n", "Impresi�n de Ticket")
        Else

            response = oReportes.ImprimirabonoMenosTicket(Me.Sucursal, Serie, folio, Me.ClienteFactura, "", 0, 0)

            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
            Else
                If response.Value.Tables(0).Rows.Count > 0 Then

                    Dim oDataSet As DataSet
                    Dim oReport As New rptTicketMenos


                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    oReport.RutaImpresora = RutaImpresoraTicket

                    'oReport.Document.Printer.PrinterSettings.Copies = 2

                    If TinApp.Connection.User.ToUpper = "SUPER" Then
                        TinApp.ShowReport(Me.MdiParent, "Impresi�n del Menos ", oReport, , , True, True)
                    Else

                        TinApp.PrintReport(oReport)
                    End If

                    oDataSet = Nothing
                    oReport = Nothing
                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        End If
    End Sub

End Class
