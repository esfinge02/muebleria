Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Drawing
Imports System.Windows.Forms

Public Class frmLlamadasClientesEjecutivo
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
    Public sucursal_dependencia As Boolean = False
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents grLlamadasClientes As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvLlamadasClientes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaLlamadasClientes As Dipros.Windows.TINMaster
    Friend WithEvents grcFechaLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTelefono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcContesto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcObservaciones As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClienteLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoTelefono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcParentescoPersonaContesta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMensajeDejado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMensajeRecibido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUsuarioCapturo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblNombreCliente As System.Windows.Forms.Label
    Friend WithEvents grcPromesa_pago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcParentesco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDes_Parentesco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcIdMensaje As DevExpress.XtraGrid.Columns.GridColumn

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLlamadasClientesEjecutivo))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.grLlamadasClientes = New DevExpress.XtraGrid.GridControl
        Me.grvLlamadasClientes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClienteLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTelefono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoTelefono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcContesto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcParentescoPersonaContesta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcParentesco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDes_Parentesco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPromesa_pago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMensajeDejado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMensajeRecibido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcObservaciones = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUsuarioCapturo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaLlamadasClientes = New Dipros.Windows.TINMaster
        Me.lblNombreCliente = New System.Windows.Forms.Label
        Me.grcIdMensaje = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.grLlamadasClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvLlamadasClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(9559, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente.Location = New System.Drawing.Point(16, 33)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(53, 18)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grLlamadasClientes
        '
        '
        'grLlamadasClientes.EmbeddedNavigator
        '
        Me.grLlamadasClientes.EmbeddedNavigator.Name = ""
        Me.grLlamadasClientes.Location = New System.Drawing.Point(9, 84)
        Me.grLlamadasClientes.MainView = Me.grvLlamadasClientes
        Me.grLlamadasClientes.Name = "grLlamadasClientes"
        Me.grLlamadasClientes.Size = New System.Drawing.Size(727, 188)
        Me.grLlamadasClientes.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grLlamadasClientes.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.TabIndex = 61
        Me.grLlamadasClientes.Text = "LlamadasClientes"
        '
        'grvLlamadasClientes
        '
        Me.grvLlamadasClientes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClienteLlamada, Me.grcFechaLlamada, Me.grcTipoLlamada, Me.grcTelefono, Me.grcTipoTelefono, Me.grcContesto, Me.grcParentescoPersonaContesta, Me.grcParentesco, Me.grcDes_Parentesco, Me.grcPromesa_pago, Me.grcMensajeDejado, Me.grcIdMensaje, Me.grcMensajeRecibido, Me.grcObservaciones, Me.grcUsuarioCapturo})
        Me.grvLlamadasClientes.GridControl = Me.grLlamadasClientes
        Me.grvLlamadasClientes.Name = "grvLlamadasClientes"
        Me.grvLlamadasClientes.OptionsBehavior.Editable = False
        Me.grvLlamadasClientes.OptionsCustomization.AllowFilter = False
        Me.grvLlamadasClientes.OptionsCustomization.AllowGroup = False
        Me.grvLlamadasClientes.OptionsView.ShowGroupPanel = False
        '
        'grcClienteLlamada
        '
        Me.grcClienteLlamada.Caption = "Cliente"
        Me.grcClienteLlamada.FieldName = "cliente"
        Me.grcClienteLlamada.Name = "grcClienteLlamada"
        Me.grcClienteLlamada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFechaLlamada
        '
        Me.grcFechaLlamada.Caption = "Fecha"
        Me.grcFechaLlamada.DisplayFormat.FormatString = "d"
        Me.grcFechaLlamada.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaLlamada.FieldName = "fecha_hora"
        Me.grcFechaLlamada.Name = "grcFechaLlamada"
        Me.grcFechaLlamada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaLlamada.VisibleIndex = 0
        Me.grcFechaLlamada.Width = 74
        '
        'grcTipoLlamada
        '
        Me.grcTipoLlamada.Caption = "Tipo Llamada"
        Me.grcTipoLlamada.FieldName = "descripcion_tipo_llamada"
        Me.grcTipoLlamada.Name = "grcTipoLlamada"
        Me.grcTipoLlamada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipoLlamada.VisibleIndex = 1
        Me.grcTipoLlamada.Width = 84
        '
        'grcTelefono
        '
        Me.grcTelefono.Caption = "Tel�fono"
        Me.grcTelefono.FieldName = "telefono"
        Me.grcTelefono.Name = "grcTelefono"
        Me.grcTelefono.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTelefono.VisibleIndex = 3
        Me.grcTelefono.Width = 79
        '
        'grcTipoTelefono
        '
        Me.grcTipoTelefono.Caption = "Tipo de Tel�fono"
        Me.grcTipoTelefono.FieldName = "descripcion_tipo_telefono"
        Me.grcTipoTelefono.Name = "grcTipoTelefono"
        Me.grcTipoTelefono.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipoTelefono.VisibleIndex = 2
        Me.grcTipoTelefono.Width = 79
        '
        'grcContesto
        '
        Me.grcContesto.Caption = "Contest�"
        Me.grcContesto.FieldName = "persona_contesta"
        Me.grcContesto.Name = "grcContesto"
        Me.grcContesto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcContesto.VisibleIndex = 4
        Me.grcContesto.Width = 79
        '
        'grcParentescoPersonaContesta
        '
        Me.grcParentescoPersonaContesta.Caption = "Parentesco Persona Contesta"
        Me.grcParentescoPersonaContesta.FieldName = "parentesco_persona_contesta"
        Me.grcParentescoPersonaContesta.Name = "grcParentescoPersonaContesta"
        Me.grcParentescoPersonaContesta.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcParentescoPersonaContesta.Width = 79
        '
        'grcParentesco
        '
        Me.grcParentesco.Caption = "id_parentesco"
        Me.grcParentesco.FieldName = "parentesco"
        Me.grcParentesco.Name = "grcParentesco"
        Me.grcParentesco.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDes_Parentesco
        '
        Me.grcDes_Parentesco.Caption = "Parentesco"
        Me.grcDes_Parentesco.FieldName = "des_parentesco"
        Me.grcDes_Parentesco.Name = "grcDes_Parentesco"
        Me.grcDes_Parentesco.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDes_Parentesco.VisibleIndex = 7
        '
        'grcPromesa_pago
        '
        Me.grcPromesa_pago.Caption = "Fecha de Promesa de Pago"
        Me.grcPromesa_pago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcPromesa_pago.FieldName = "fecha_promesa_pago"
        Me.grcPromesa_pago.Name = "grcPromesa_pago"
        Me.grcPromesa_pago.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPromesa_pago.VisibleIndex = 9
        '
        'grcMensajeDejado
        '
        Me.grcMensajeDejado.Caption = "Mensaje Dejado"
        Me.grcMensajeDejado.FieldName = "mensaje_dejado"
        Me.grcMensajeDejado.Name = "grcMensajeDejado"
        Me.grcMensajeDejado.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMensajeDejado.Width = 79
        '
        'grcMensajeRecibido
        '
        Me.grcMensajeRecibido.Caption = "Mensaje Recibido"
        Me.grcMensajeRecibido.FieldName = "mensaje_recibido"
        Me.grcMensajeRecibido.Name = "grcMensajeRecibido"
        Me.grcMensajeRecibido.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMensajeRecibido.VisibleIndex = 5
        Me.grcMensajeRecibido.Width = 79
        '
        'grcObservaciones
        '
        Me.grcObservaciones.Caption = "Observaciones"
        Me.grcObservaciones.FieldName = "observaciones"
        Me.grcObservaciones.Name = "grcObservaciones"
        Me.grcObservaciones.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcObservaciones.VisibleIndex = 6
        Me.grcObservaciones.Width = 103
        '
        'grcUsuarioCapturo
        '
        Me.grcUsuarioCapturo.Caption = "Captur�"
        Me.grcUsuarioCapturo.FieldName = "usuario_capturo"
        Me.grcUsuarioCapturo.Name = "grcUsuarioCapturo"
        Me.grcUsuarioCapturo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcUsuarioCapturo.VisibleIndex = 8
        '
        'tmaLlamadasClientes
        '
        Me.tmaLlamadasClientes.BackColor = System.Drawing.Color.White
        Me.tmaLlamadasClientes.CanDelete = True
        Me.tmaLlamadasClientes.CanInsert = True
        Me.tmaLlamadasClientes.CanUpdate = True
        Me.tmaLlamadasClientes.Grid = Me.grLlamadasClientes
        Me.tmaLlamadasClientes.Location = New System.Drawing.Point(9, 59)
        Me.tmaLlamadasClientes.Name = "tmaLlamadasClientes"
        Me.tmaLlamadasClientes.Size = New System.Drawing.Size(719, 23)
        Me.tmaLlamadasClientes.TabIndex = 60
        Me.tmaLlamadasClientes.Title = "Llamadas a Clientes"
        Me.tmaLlamadasClientes.UpdateTitle = "un Registro"
        '
        'lblNombreCliente
        '
        Me.lblNombreCliente.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreCliente.Location = New System.Drawing.Point(72, 33)
        Me.lblNombreCliente.Name = "lblNombreCliente"
        Me.lblNombreCliente.Size = New System.Drawing.Size(437, 16)
        Me.lblNombreCliente.TabIndex = 62
        '
        'grcIdMensaje
        '
        Me.grcIdMensaje.Caption = "Mensaje Dejado"
        Me.grcIdMensaje.FieldName = "id_mensaje"
        Me.grcIdMensaje.Name = "grcIdMensaje"
        Me.grcIdMensaje.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'frmLlamadasClientesEjecutivo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(746, 280)
        Me.Controls.Add(Me.lblNombreCliente)
        Me.Controls.Add(Me.grLlamadasClientes)
        Me.Controls.Add(Me.tmaLlamadasClientes)
        Me.Controls.Add(Me.lblCliente)
        Me.MasterControl = Me.tmaLlamadasClientes
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmLlamadasClientesEjecutivo"
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.tmaLlamadasClientes, 0)
        Me.Controls.SetChildIndex(Me.grLlamadasClientes, 0)
        Me.Controls.SetChildIndex(Me.lblNombreCliente, 0)
        CType(Me.grLlamadasClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvLlamadasClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oLlamadasClientes As VillarrealBusiness.clsLlamadasClientes
    Private oLlamadasClientesDetalle As VillarrealBusiness.clsLlamadasClientesDetalle
    Private oClientes As VillarrealBusiness.clsClientes
    'Private ociudades As VillarrealBusiness.clsCiudades
    'Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oVariables As VillarrealBusiness.clsVariables
    'Private oColonias As VillarrealBusiness.clsColonias
    'Private oEstados As VillarrealBusiness.clsEstados
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar

    Private oTabla As DataTable
    Private bentro_catalogo_clientes As Boolean = False
    Private nombre_cliente As String = ""
    Private lcliente As Long = 0
    Dim BanderaNoCargarDetalle As Boolean = False
    'Private oComunes As New VillarrealBusiness.clsUtilerias 

    Public Property cliente() As Long
        Get
            Return lcliente
        End Get
        Set(ByVal Value As Long)
            lcliente = Value
        End Set
    End Property
    Public Property NombreCliente() As String
        Get
            Return nombre_cliente
        End Get
        Set(ByVal Value As String)
            nombre_cliente = Value
        End Set
    End Property


    
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmLlamadasClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub

    Private Sub frmLlamadasClientes_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With Me.tmaLlamadasClientes


            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        ValidaCliente()
                        If BanderaNoCargarDetalle = True Then Exit Sub
                        Response = oLlamadasClientes.Insertar(.SelectedRow) ', clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)
                    Case Actions.Update
                        Response = oLlamadasClientes.Actualizar(.SelectedRow)
                    Case Actions.Delete
                        Response = oLlamadasClientes.Eliminar(.SelectedRow.Tables(0).Rows(0).Item("cliente"), .SelectedRow.Tables(0).Rows(0).Item("fecha_hora"))
                End Select
                .MoveNext()
            Loop
        End With

    End Sub

    Private Sub frmLlamadasClientes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

        Response = oLlamadasClientes.DespliegaDatos(cliente)

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.tmaLlamadasClientes.DataSource = oDataSet
        End If
        



    End Sub

    Private Sub frmLlamadasClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oLlamadasClientesDetalle = New VillarrealBusiness.clsLlamadasClientesDetalle
        oLlamadasClientes = New VillarrealBusiness.clsLlamadasClientes
        oClientes = New VillarrealBusiness.clsClientes
        'ociudades = New VillarrealBusiness.clsCiudades
        'oMunicipios = New VillarrealBusiness.clsMunicipios
        oVariables = New VillarrealBusiness.clsVariables
        'oColonias = New VillarrealBusiness.clsColonias
        'oEstados = New VillarrealBusiness.clsEstados
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar

        Me.lblNombreCliente.Text = Me.NombreCliente

        With Me.tmaLlamadasClientes

            .UpdateTitle = "una llamada"
            .UpdateForm = New frmLlamadasClientesDetalle
            .AddColumn("cliente", "System.Int32")
            .AddColumn("fecha_hora", "System.DateTime")
            .AddColumn("tipo_llamada", "System.Int32")
            .AddColumn("descripcion_tipo_llamada", "System.String")
            .AddColumn("telefono", "System.String")
            .AddColumn("tipo_telefono", "System.String")
            .AddColumn("descripcion_tipo_telefono", "System.String")
            .AddColumn("persona_contesta", "System.String")
            .AddColumn("parentesco_persona_contesta", "System.String")
            .AddColumn("mensaje_recibido", "System.String")
            .AddColumn("observaciones", "System.String")
            .AddColumn("usuario_capturo", "System.String")
            .AddColumn("fecha_promesa_pago", "System.DateTime")
            .AddColumn("parentesco", "System.Int32")
            .AddColumn("des_parentesco", "System.String")
            .AddColumn("mensaje_dejado", "System.String")
            .AddColumn("id_mensaje", "System.String")

        End With
    End Sub

    Private Sub frmLlamadasClientes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oLlamadasClientes.Validacion(Action, cliente)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"


    Private Sub LlenarGridLlamadas()
        Dim response As New Dipros.Utils.Events
        Dim odataset As New DataSet

        response = oLlamadasClientes.DespliegaDatos(cliente)
        If Not response.ErrorFound Then
            odataset = response.Value

            Me.tmaLlamadasClientes.DataSource = odataset


        End If

        odataset = Nothing

    End Sub
    Private Sub ValidaCliente()
        If cliente = Nothing Or cliente = -1 Or IsNumeric(cliente) = False Then
            BanderaNoCargarDetalle = True
        End If
    End Sub


#End Region



End Class
