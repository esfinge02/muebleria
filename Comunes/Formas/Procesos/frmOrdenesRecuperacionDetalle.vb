
Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmOrdenesRecuperacionDetalle
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents clcArticulo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents clcCosto As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOrdenesRecuperacionDetalle))
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.clcArticulo = New Dipros.Editors.TINCalcEdit
        Me.lblPartida = New System.Windows.Forms.Label
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.lblSerie = New System.Windows.Forms.Label
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.lblCosto = New System.Windows.Forms.Label
        Me.clcCosto = New Dipros.Editors.TINCalcEdit
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcArticulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(40, 50)
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(33, 40)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 0
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(74, 40)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(62, 19)
        Me.clcFolio.TabIndex = 1
        Me.clcFolio.Tag = "folio"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(17, 63)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 2
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "&Articulo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcArticulo
        '
        Me.clcArticulo.EditValue = "0"
        Me.clcArticulo.Location = New System.Drawing.Point(74, 63)
        Me.clcArticulo.MaxValue = 0
        Me.clcArticulo.MinValue = 0
        Me.clcArticulo.Name = "clcArticulo"
        '
        'clcArticulo.Properties
        '
        Me.clcArticulo.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcArticulo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcArticulo.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcArticulo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcArticulo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcArticulo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcArticulo.Size = New System.Drawing.Size(182, 19)
        Me.clcArticulo.TabIndex = 3
        Me.clcArticulo.Tag = "articulo"
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(20, 86)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 4
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "&Partida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(10, 109)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(58, 16)
        Me.lblCantidad.TabIndex = 6
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "&Cantidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = "0"
        Me.clcCantidad.Location = New System.Drawing.Point(74, 109)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Size = New System.Drawing.Size(48, 19)
        Me.clcCantidad.TabIndex = 7
        Me.clcCantidad.Tag = "cantidad"
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(31, 132)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie.TabIndex = 8
        Me.lblSerie.Tag = ""
        Me.lblSerie.Text = "&Serie:"
        Me.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(74, 132)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.MaxLength = 30
        Me.txtSerie.Size = New System.Drawing.Size(180, 20)
        Me.txtSerie.TabIndex = 9
        Me.txtSerie.Tag = "serie"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(28, 155)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(40, 16)
        Me.lblCosto.TabIndex = 10
        Me.lblCosto.Tag = ""
        Me.lblCosto.Text = "&Costo:"
        Me.lblCosto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCosto
        '
        Me.clcCosto.EditValue = "0"
        Me.clcCosto.Location = New System.Drawing.Point(74, 155)
        Me.clcCosto.MaxValue = 0
        Me.clcCosto.MinValue = 0
        Me.clcCosto.Name = "clcCosto"
        '
        'clcCosto.Properties
        '
        Me.clcCosto.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcCosto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCosto.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCosto.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcCosto.Properties.Precision = 2
        Me.clcCosto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCosto.Size = New System.Drawing.Size(48, 19)
        Me.clcCosto.TabIndex = 11
        Me.clcCosto.Tag = "costo"
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(74, 85)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(62, 19)
        Me.clcPartida.TabIndex = 59
        Me.clcPartida.Tag = "partida"
        '
        'frmOrdenesRecuperacionDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(266, 184)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.clcArticulo)
        Me.Controls.Add(Me.clcCantidad)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.clcCosto)
        Me.Name = "frmOrdenesRecuperacionDetalle"
        Me.Controls.SetChildIndex(Me.clcCosto, 0)
        Me.Controls.SetChildIndex(Me.txtSerie, 0)
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.clcArticulo, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcArticulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmOrdenesRecuperacionDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmOrdenesRecuperacionDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
       
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub frmOrdenesRecuperacionDetalle_Initialize(ByRef Response As Events) Handles MyBase.Initialize

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmOrdenesRecuperacionDetalle_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields

    End Sub

    Private Sub frmOrdenesRecuperacionDetalle_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class



