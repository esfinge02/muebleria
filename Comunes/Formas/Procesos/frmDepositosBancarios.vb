Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports VillarrealBusiness.BusinessEnvironment

Public Class frmDepositosBancarios
	Inherits Dipros.Windows.frmTINForm

#Region " Código generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents clcSucursal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblConsecutivo As System.Windows.Forms.Label
    Friend WithEvents clcConsecutivo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblBanco As System.Windows.Forms.Label

    Friend WithEvents lblChequera As System.Windows.Forms.Label
    Friend WithEvents lblFolio_Movimiento_Chequera As System.Windows.Forms.Label
    Friend WithEvents lblDepositante As System.Windows.Forms.Label
    Friend WithEvents txtDepositante As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblUsuario_Inserto As System.Windows.Forms.Label
    Friend WithEvents lblUsuario_Aplico As System.Windows.Forms.Label
    Friend WithEvents chkAplicadoMovimientoChequera As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpBanco As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpChequera As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcFolioMovimientoChequera As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtUsuarioInserto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUsuarioAplico As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBancoNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblBancoNombre As System.Windows.Forms.Label
    Friend WithEvents txtSucursalNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSucursalNombre As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpTiposDepositos As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DteFechaDeposito As DevExpress.XtraEditors.DateEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDepositosBancarios))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.clcSucursal = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblConsecutivo = New System.Windows.Forms.Label
        Me.clcConsecutivo = New Dipros.Editors.TINCalcEdit
        Me.lblBanco = New System.Windows.Forms.Label
        Me.lblChequera = New System.Windows.Forms.Label
        Me.lblFolio_Movimiento_Chequera = New System.Windows.Forms.Label
        Me.clcFolioMovimientoChequera = New Dipros.Editors.TINCalcEdit
        Me.lblDepositante = New System.Windows.Forms.Label
        Me.txtDepositante = New DevExpress.XtraEditors.TextEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.chkAplicadoMovimientoChequera = New DevExpress.XtraEditors.CheckEdit
        Me.lblUsuario_Inserto = New System.Windows.Forms.Label
        Me.txtUsuarioInserto = New DevExpress.XtraEditors.TextEdit
        Me.lblUsuario_Aplico = New System.Windows.Forms.Label
        Me.txtUsuarioAplico = New DevExpress.XtraEditors.TextEdit
        Me.lkpBanco = New Dipros.Editors.TINMultiLookup
        Me.lkpChequera = New Dipros.Editors.TINMultiLookup
        Me.txtBancoNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblBancoNombre = New System.Windows.Forms.Label
        Me.txtSucursalNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblSucursalNombre = New System.Windows.Forms.Label
        Me.DteFechaDeposito = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpTiposDepositos = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcConsecutivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioMovimientoChequera.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepositante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAplicadoMovimientoChequera.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsuarioInserto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsuarioAplico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBancoNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSucursalNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DteFechaDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(360, 41)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSucursal
        '
        Me.clcSucursal.EditValue = "0"
        Me.clcSucursal.Location = New System.Drawing.Point(424, 40)
        Me.clcSucursal.MaxValue = 0
        Me.clcSucursal.MinValue = 0
        Me.clcSucursal.Name = "clcSucursal"
        '
        'clcSucursal.Properties
        '
        Me.clcSucursal.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcSucursal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcSucursal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.Enabled = False
        Me.clcSucursal.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSucursal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSucursal.Size = New System.Drawing.Size(24, 19)
        Me.clcSucursal.TabIndex = 1
        Me.clcSucursal.TabStop = False
        Me.clcSucursal.Tag = "sucursal"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(12, 64)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(87, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha Ingreso:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "30/06/2007"
        Me.dteFecha.Location = New System.Drawing.Point(104, 64)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.TabStop = False
        Me.dteFecha.Tag = "fecha"
        '
        'lblConsecutivo
        '
        Me.lblConsecutivo.AutoSize = True
        Me.lblConsecutivo.Location = New System.Drawing.Point(23, 40)
        Me.lblConsecutivo.Name = "lblConsecutivo"
        Me.lblConsecutivo.Size = New System.Drawing.Size(76, 16)
        Me.lblConsecutivo.TabIndex = 0
        Me.lblConsecutivo.Tag = ""
        Me.lblConsecutivo.Text = "C&onsecutivo:"
        Me.lblConsecutivo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcConsecutivo
        '
        Me.clcConsecutivo.EditValue = "0"
        Me.clcConsecutivo.Location = New System.Drawing.Point(104, 40)
        Me.clcConsecutivo.MaxValue = 0
        Me.clcConsecutivo.MinValue = 0
        Me.clcConsecutivo.Name = "clcConsecutivo"
        '
        'clcConsecutivo.Properties
        '
        Me.clcConsecutivo.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcConsecutivo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcConsecutivo.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcConsecutivo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcConsecutivo.Properties.Enabled = False
        Me.clcConsecutivo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcConsecutivo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcConsecutivo.Size = New System.Drawing.Size(48, 19)
        Me.clcConsecutivo.TabIndex = 1
        Me.clcConsecutivo.TabStop = False
        Me.clcConsecutivo.Tag = "consecutivo"
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(56, 112)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 6
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblChequera
        '
        Me.lblChequera.AutoSize = True
        Me.lblChequera.Location = New System.Drawing.Point(37, 136)
        Me.lblChequera.Name = "lblChequera"
        Me.lblChequera.Size = New System.Drawing.Size(62, 16)
        Me.lblChequera.TabIndex = 8
        Me.lblChequera.Tag = ""
        Me.lblChequera.Text = "Ch&equera:"
        Me.lblChequera.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio_Movimiento_Chequera
        '
        Me.lblFolio_Movimiento_Chequera.AutoSize = True
        Me.lblFolio_Movimiento_Chequera.Location = New System.Drawing.Point(376, 64)
        Me.lblFolio_Movimiento_Chequera.Name = "lblFolio_Movimiento_Chequera"
        Me.lblFolio_Movimiento_Chequera.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio_Movimiento_Chequera.TabIndex = 10
        Me.lblFolio_Movimiento_Chequera.Tag = ""
        Me.lblFolio_Movimiento_Chequera.Text = "&Folio:"
        Me.lblFolio_Movimiento_Chequera.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolioMovimientoChequera
        '
        Me.clcFolioMovimientoChequera.EditValue = "0"
        Me.clcFolioMovimientoChequera.Location = New System.Drawing.Point(424, 64)
        Me.clcFolioMovimientoChequera.MaxValue = 0
        Me.clcFolioMovimientoChequera.MinValue = 0
        Me.clcFolioMovimientoChequera.Name = "clcFolioMovimientoChequera"
        '
        'clcFolioMovimientoChequera.Properties
        '
        Me.clcFolioMovimientoChequera.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolioMovimientoChequera.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioMovimientoChequera.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolioMovimientoChequera.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioMovimientoChequera.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolioMovimientoChequera.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolioMovimientoChequera.Size = New System.Drawing.Size(24, 19)
        Me.clcFolioMovimientoChequera.TabIndex = 11
        Me.clcFolioMovimientoChequera.TabStop = False
        Me.clcFolioMovimientoChequera.Tag = "folio_movimiento_chequera"
        '
        'lblDepositante
        '
        Me.lblDepositante.AutoSize = True
        Me.lblDepositante.Location = New System.Drawing.Point(24, 184)
        Me.lblDepositante.Name = "lblDepositante"
        Me.lblDepositante.Size = New System.Drawing.Size(75, 16)
        Me.lblDepositante.TabIndex = 12
        Me.lblDepositante.Tag = ""
        Me.lblDepositante.Text = "&Depositante:"
        Me.lblDepositante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDepositante
        '
        Me.txtDepositante.EditValue = ""
        Me.txtDepositante.Location = New System.Drawing.Point(104, 184)
        Me.txtDepositante.Name = "txtDepositante"
        '
        'txtDepositante.Properties
        '
        Me.txtDepositante.Properties.MaxLength = 30
        Me.txtDepositante.Size = New System.Drawing.Size(216, 20)
        Me.txtDepositante.TabIndex = 13
        Me.txtDepositante.Tag = "depositante"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(46, 208)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 14
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(104, 208)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "########0.00"
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(96, 19)
        Me.clcImporte.TabIndex = 15
        Me.clcImporte.Tag = "importe"
        '
        'chkAplicadoMovimientoChequera
        '
        Me.chkAplicadoMovimientoChequera.EditValue = "False"
        Me.chkAplicadoMovimientoChequera.Location = New System.Drawing.Point(464, 40)
        Me.chkAplicadoMovimientoChequera.Name = "chkAplicadoMovimientoChequera"
        '
        'chkAplicadoMovimientoChequera.Properties
        '
        Me.chkAplicadoMovimientoChequera.Properties.AutoHeight = False
        Me.chkAplicadoMovimientoChequera.Properties.Caption = "Aplicado"
        Me.chkAplicadoMovimientoChequera.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAplicadoMovimientoChequera.Size = New System.Drawing.Size(88, 24)
        Me.chkAplicadoMovimientoChequera.TabIndex = 16
        Me.chkAplicadoMovimientoChequera.TabStop = False
        Me.chkAplicadoMovimientoChequera.Tag = "aplicado_movimiento_chequera"
        '
        'lblUsuario_Inserto
        '
        Me.lblUsuario_Inserto.AutoSize = True
        Me.lblUsuario_Inserto.Location = New System.Drawing.Point(368, 88)
        Me.lblUsuario_Inserto.Name = "lblUsuario_Inserto"
        Me.lblUsuario_Inserto.Size = New System.Drawing.Size(49, 16)
        Me.lblUsuario_Inserto.TabIndex = 17
        Me.lblUsuario_Inserto.Tag = ""
        Me.lblUsuario_Inserto.Text = "&Insertó:"
        Me.lblUsuario_Inserto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtUsuarioInserto
        '
        Me.txtUsuarioInserto.EditValue = ""
        Me.txtUsuarioInserto.Location = New System.Drawing.Point(424, 88)
        Me.txtUsuarioInserto.Name = "txtUsuarioInserto"
        '
        'txtUsuarioInserto.Properties
        '
        Me.txtUsuarioInserto.Properties.MaxLength = 15
        Me.txtUsuarioInserto.Size = New System.Drawing.Size(90, 20)
        Me.txtUsuarioInserto.TabIndex = 18
        Me.txtUsuarioInserto.TabStop = False
        Me.txtUsuarioInserto.Tag = "usuario_inserto"
        '
        'lblUsuario_Aplico
        '
        Me.lblUsuario_Aplico.AutoSize = True
        Me.lblUsuario_Aplico.Location = New System.Drawing.Point(376, 112)
        Me.lblUsuario_Aplico.Name = "lblUsuario_Aplico"
        Me.lblUsuario_Aplico.Size = New System.Drawing.Size(42, 16)
        Me.lblUsuario_Aplico.TabIndex = 19
        Me.lblUsuario_Aplico.Tag = ""
        Me.lblUsuario_Aplico.Text = "&Aplicó:"
        Me.lblUsuario_Aplico.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtUsuarioAplico
        '
        Me.txtUsuarioAplico.EditValue = ""
        Me.txtUsuarioAplico.Location = New System.Drawing.Point(424, 112)
        Me.txtUsuarioAplico.Name = "txtUsuarioAplico"
        '
        'txtUsuarioAplico.Properties
        '
        Me.txtUsuarioAplico.Properties.MaxLength = 15
        Me.txtUsuarioAplico.Size = New System.Drawing.Size(90, 20)
        Me.txtUsuarioAplico.TabIndex = 20
        Me.txtUsuarioAplico.TabStop = False
        Me.txtUsuarioAplico.Tag = "usuario_aplico"
        '
        'lkpBanco
        '
        Me.lkpBanco.AllowAdd = False
        Me.lkpBanco.AutoReaload = False
        Me.lkpBanco.DataSource = Nothing
        Me.lkpBanco.DefaultSearchField = ""
        Me.lkpBanco.DisplayMember = "nombre"
        Me.lkpBanco.EditValue = Nothing
        Me.lkpBanco.Filtered = False
        Me.lkpBanco.InitValue = Nothing
        Me.lkpBanco.Location = New System.Drawing.Point(104, 112)
        Me.lkpBanco.MultiSelect = False
        Me.lkpBanco.Name = "lkpBanco"
        Me.lkpBanco.NullText = ""
        Me.lkpBanco.PopupWidth = CType(400, Long)
        Me.lkpBanco.ReadOnlyControl = False
        Me.lkpBanco.Required = False
        Me.lkpBanco.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBanco.SearchMember = ""
        Me.lkpBanco.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBanco.SelectAll = False
        Me.lkpBanco.Size = New System.Drawing.Size(216, 20)
        Me.lkpBanco.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBanco.TabIndex = 7
        Me.lkpBanco.Tag = "banco"
        Me.lkpBanco.ToolTip = Nothing
        Me.lkpBanco.ValueMember = "banco"
        '
        'lkpChequera
        '
        Me.lkpChequera.AllowAdd = False
        Me.lkpChequera.AutoReaload = False
        Me.lkpChequera.DataSource = Nothing
        Me.lkpChequera.DefaultSearchField = ""
        Me.lkpChequera.DisplayMember = "cuenta_bancaria"
        Me.lkpChequera.EditValue = Nothing
        Me.lkpChequera.Filtered = False
        Me.lkpChequera.InitValue = Nothing
        Me.lkpChequera.Location = New System.Drawing.Point(104, 136)
        Me.lkpChequera.MultiSelect = False
        Me.lkpChequera.Name = "lkpChequera"
        Me.lkpChequera.NullText = ""
        Me.lkpChequera.PopupWidth = CType(400, Long)
        Me.lkpChequera.ReadOnlyControl = False
        Me.lkpChequera.Required = False
        Me.lkpChequera.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpChequera.SearchMember = ""
        Me.lkpChequera.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpChequera.SelectAll = False
        Me.lkpChequera.Size = New System.Drawing.Size(216, 20)
        Me.lkpChequera.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpChequera.TabIndex = 9
        Me.lkpChequera.Tag = "chequera"
        Me.lkpChequera.ToolTip = Nothing
        Me.lkpChequera.ValueMember = "cuenta_bancaria"
        '
        'txtBancoNombre
        '
        Me.txtBancoNombre.EditValue = ""
        Me.txtBancoNombre.Location = New System.Drawing.Point(488, 136)
        Me.txtBancoNombre.Name = "txtBancoNombre"
        '
        'txtBancoNombre.Properties
        '
        Me.txtBancoNombre.Properties.MaxLength = 15
        Me.txtBancoNombre.Size = New System.Drawing.Size(90, 20)
        Me.txtBancoNombre.TabIndex = 61
        Me.txtBancoNombre.TabStop = False
        Me.txtBancoNombre.Tag = "banco_nombre"
        '
        'lblBancoNombre
        '
        Me.lblBancoNombre.AutoSize = True
        Me.lblBancoNombre.Location = New System.Drawing.Point(376, 138)
        Me.lblBancoNombre.Name = "lblBancoNombre"
        Me.lblBancoNombre.Size = New System.Drawing.Size(86, 16)
        Me.lblBancoNombre.TabIndex = 62
        Me.lblBancoNombre.Tag = ""
        Me.lblBancoNombre.Text = "BancoNombre:"
        Me.lblBancoNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSucursalNombre
        '
        Me.txtSucursalNombre.EditValue = ""
        Me.txtSucursalNombre.Location = New System.Drawing.Point(488, 160)
        Me.txtSucursalNombre.Name = "txtSucursalNombre"
        '
        'txtSucursalNombre.Properties
        '
        Me.txtSucursalNombre.Properties.MaxLength = 15
        Me.txtSucursalNombre.Size = New System.Drawing.Size(90, 20)
        Me.txtSucursalNombre.TabIndex = 63
        Me.txtSucursalNombre.TabStop = False
        Me.txtSucursalNombre.Tag = "sucursal_nombre"
        '
        'lblSucursalNombre
        '
        Me.lblSucursalNombre.AutoSize = True
        Me.lblSucursalNombre.Location = New System.Drawing.Point(376, 160)
        Me.lblSucursalNombre.Name = "lblSucursalNombre"
        Me.lblSucursalNombre.TabIndex = 64
        Me.lblSucursalNombre.Tag = ""
        Me.lblSucursalNombre.Text = "SucursalNombre:"
        Me.lblSucursalNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DteFechaDeposito
        '
        Me.DteFechaDeposito.EditValue = "30/06/2007"
        Me.DteFechaDeposito.Location = New System.Drawing.Point(104, 88)
        Me.DteFechaDeposito.Name = "DteFechaDeposito"
        '
        'DteFechaDeposito.Properties
        '
        Me.DteFechaDeposito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DteFechaDeposito.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.DteFechaDeposito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.DteFechaDeposito.Size = New System.Drawing.Size(95, 20)
        Me.DteFechaDeposito.TabIndex = 5
        Me.DteFechaDeposito.Tag = "fecha_deposito"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Fecha Deposito:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpTiposDepositos
        '
        Me.lkpTiposDepositos.AllowAdd = False
        Me.lkpTiposDepositos.AutoReaload = False
        Me.lkpTiposDepositos.DataSource = Nothing
        Me.lkpTiposDepositos.DefaultSearchField = ""
        Me.lkpTiposDepositos.DisplayMember = "descripcion"
        Me.lkpTiposDepositos.EditValue = Nothing
        Me.lkpTiposDepositos.Filtered = False
        Me.lkpTiposDepositos.InitValue = Nothing
        Me.lkpTiposDepositos.Location = New System.Drawing.Point(104, 160)
        Me.lkpTiposDepositos.MultiSelect = False
        Me.lkpTiposDepositos.Name = "lkpTiposDepositos"
        Me.lkpTiposDepositos.NullText = ""
        Me.lkpTiposDepositos.PopupWidth = CType(400, Long)
        Me.lkpTiposDepositos.ReadOnlyControl = False
        Me.lkpTiposDepositos.Required = False
        Me.lkpTiposDepositos.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTiposDepositos.SearchMember = ""
        Me.lkpTiposDepositos.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTiposDepositos.SelectAll = False
        Me.lkpTiposDepositos.Size = New System.Drawing.Size(216, 20)
        Me.lkpTiposDepositos.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpTiposDepositos.TabIndex = 11
        Me.lkpTiposDepositos.Tag = "Tipo_deposito"
        Me.lkpTiposDepositos.ToolTip = Nothing
        Me.lkpTiposDepositos.ValueMember = "Tipo_deposito"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 160)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Tag = ""
        Me.Label3.Text = "Tipo Depositos:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmDepositosBancarios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(338, 240)
        Me.Controls.Add(Me.lkpTiposDepositos)
        Me.Controls.Add(Me.lblSucursalNombre)
        Me.Controls.Add(Me.txtSucursalNombre)
        Me.Controls.Add(Me.lblBancoNombre)
        Me.Controls.Add(Me.txtBancoNombre)
        Me.Controls.Add(Me.lkpChequera)
        Me.Controls.Add(Me.lkpBanco)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.clcSucursal)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblConsecutivo)
        Me.Controls.Add(Me.clcConsecutivo)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.lblChequera)
        Me.Controls.Add(Me.lblFolio_Movimiento_Chequera)
        Me.Controls.Add(Me.clcFolioMovimientoChequera)
        Me.Controls.Add(Me.lblDepositante)
        Me.Controls.Add(Me.txtDepositante)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.chkAplicadoMovimientoChequera)
        Me.Controls.Add(Me.lblUsuario_Inserto)
        Me.Controls.Add(Me.txtUsuarioInserto)
        Me.Controls.Add(Me.lblUsuario_Aplico)
        Me.Controls.Add(Me.txtUsuarioAplico)
        Me.Controls.Add(Me.DteFechaDeposito)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Name = "frmDepositosBancarios"
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.DteFechaDeposito, 0)
        Me.Controls.SetChildIndex(Me.txtUsuarioAplico, 0)
        Me.Controls.SetChildIndex(Me.lblUsuario_Aplico, 0)
        Me.Controls.SetChildIndex(Me.txtUsuarioInserto, 0)
        Me.Controls.SetChildIndex(Me.lblUsuario_Inserto, 0)
        Me.Controls.SetChildIndex(Me.chkAplicadoMovimientoChequera, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.txtDepositante, 0)
        Me.Controls.SetChildIndex(Me.lblDepositante, 0)
        Me.Controls.SetChildIndex(Me.clcFolioMovimientoChequera, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Movimiento_Chequera, 0)
        Me.Controls.SetChildIndex(Me.lblChequera, 0)
        Me.Controls.SetChildIndex(Me.lblBanco, 0)
        Me.Controls.SetChildIndex(Me.clcConsecutivo, 0)
        Me.Controls.SetChildIndex(Me.lblConsecutivo, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpBanco, 0)
        Me.Controls.SetChildIndex(Me.lkpChequera, 0)
        Me.Controls.SetChildIndex(Me.txtBancoNombre, 0)
        Me.Controls.SetChildIndex(Me.lblBancoNombre, 0)
        Me.Controls.SetChildIndex(Me.txtSucursalNombre, 0)
        Me.Controls.SetChildIndex(Me.lblSucursalNombre, 0)
        Me.Controls.SetChildIndex(Me.lkpTiposDepositos, 0)
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcConsecutivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioMovimientoChequera.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepositante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAplicadoMovimientoChequera.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsuarioInserto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsuarioAplico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBancoNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSucursalNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DteFechaDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDepositosBancarios As VillarrealBusiness.clsDepositosBancarios
    Private oBancos As VillarrealBusiness.clsBancos
    Private oChequeras As VillarrealBusiness.clsChequeras
    Private oTiposDepositos As VillarrealBusiness.clsTiposDepositos

    Private ReadOnly Property Banco() As Long
        Get
            If lkpBanco.EditValue = Nothing Then
                Return -1
            Else
                Return lkpBanco.EditValue
            End If
        End Get
    End Property

    Public Property Chequera() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpChequera)
        End Get
        Set(ByVal Value As String)
            Me.lkpChequera.EditValue = Value
        End Set
    End Property

    Private ReadOnly Property Sucursal() As Long
        Get
            Return CType(OwnerForm, brwDepositosBancarios).Sucursal
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmDepositosBancarios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert

                clcSucursal.EditValue = Sucursal
                clcConsecutivo.EditValue = 0
                clcFolioMovimientoChequera.EditValue = 0
                chkAplicadoMovimientoChequera.CheckState = Windows.Forms.CheckState.Unchecked
                txtUsuarioAplico.EditValue = ""
                '/@ACH-30/06/07
                Response = oDepositosBancarios.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oDepositosBancarios.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oDepositosBancarios.Eliminar(clcSucursal.Value, dteFecha.DateTime, clcConsecutivo.Value)

        End Select
        CType(OwnerForm, brwDepositosBancarios).CargaDatos_grDepositosBancarios()
    End Sub

    Private Sub frmDepositosBancarios_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        If (chkAplicadoMovimientoChequera.CheckState = Windows.Forms.CheckState.Checked) Or (Me.txtUsuarioInserto.Text <> TinApp.Connection.User) Then
            Me.tbrTools.Buttons(0).Visible = False
            Me.dteFecha.Enabled = False
            Me.clcConsecutivo.Enabled = False
            Me.lkpBanco.Enabled = False
            Me.lkpChequera.Enabled = False
            Me.txtDepositante.Enabled = False
            Me.clcImporte.Enabled = False
        End If
        Me.lkpBanco.Focus()
    End Sub

    Private Sub frmDepositosBancarios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oDepositosBancarios = New VillarrealBusiness.clsDepositosBancarios
        oBancos = New VillarrealBusiness.clsBancos
        oChequeras = New VillarrealBusiness.clsChequeras
        Me.oTiposDepositos = New VillarrealBusiness.clsTiposDepositos


        Select Case Action
            Case Actions.Insert
                dteFecha.EditValue = CDate(TinApp.FechaServidor).ToShortDateString
                DteFechaDeposito.EditValue = CDate(TinApp.FechaServidor).ToShortDateString
            Case Actions.Update
            Case Actions.Delete
        End Select

        Me.lkpBanco.Focus()
    End Sub

    Private Sub frmDepositosBancarios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim intControl As Long

        txtDepositante.EditValue = Trim(txtDepositante.EditValue)

        Response = oDepositosBancarios.Validacion(Action, Connection.User, txtUsuarioInserto.EditValue, lkpBanco.EditValue, Chequera, txtDepositante.Text, clcImporte.Value, intControl)
        Select Case intControl
            Case 1
                lkpBanco.Focus()
            Case 2
                lkpChequera.Focus()
            Case 3
                txtDepositante.Focus()
            Case 4
                clcImporte.Focus()
        End Select
    End Sub

    Private Sub frmDepositosBancarios_Localize() Handles MyBase.Localize
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
#Region "Banco"
    Private Sub lkpBanco_Format() Handles lkpBanco.Format
        Comunes.clsFormato.for_bancos_grl(Me.lkpBanco)
    End Sub
    Private Sub lkpBanco_LoadData(ByVal Initialize As Boolean) Handles lkpBanco.LoadData
        Dim response As Events
        response = oBancos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBanco.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpBanco_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBanco.EditValueChanged
        lkpChequera_LoadData(True)
    End Sub
#End Region

#Region "Chequera"
    Private Sub lkpChequera_Format() Handles lkpChequera.Format
        Comunes.clsFormato.for_chequeras_grl(Me.lkpChequera)
    End Sub
    Private Sub lkpChequera_LoadData(ByVal Initialize As Boolean) Handles lkpChequera.LoadData
        Dim response As Events
        response = oChequeras.Lookup(Me.Banco)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpChequera.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpCuentaBancaria_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpChequera.EditValueChanged
        If lkpBanco.EditValue = Nothing Then
            lkpBanco_LoadData(True)
            lkpBanco.EditValue = lkpChequera.GetValue("banco")
        End If
    End Sub
#End Region

#Region "Tipos Depositos"

    Private Sub lkpTiposDepositos_LoadData(ByVal Initialize As Boolean) Handles lkpTiposDepositos.LoadData
        Dim response As Events
        response = oTiposDepositos.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpTiposDepositos.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpTiposDepositos_Format() Handles lkpTiposDepositos.Format
        Comunes.clsFormato.for_tipos_depositos_grl(Me.lkpTiposDepositos)
    End Sub

#End Region

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

    
 
End Class
