Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmPaquetes
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grDescuentos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDescuentos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSobrePedido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grchkReparto As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grchkSurtido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents lkpPlan As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblPlan As System.Windows.Forms.Label
    Friend WithEvents clcPorcentaje As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grcPrecioCredito As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents clcTotalContado As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcTotalCredito As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcPaquete As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents tmaDetalle As Dipros.Windows.TINMaster
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPaquetes))
        Me.clcPaquete = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.grDescuentos = New DevExpress.XtraGrid.GridControl
        Me.grvDescuentos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioCredito = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSobrePedido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grchkReparto = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grchkSurtido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.tmaDetalle = New Dipros.Windows.TINMaster
        Me.lkpPlan = New Dipros.Editors.TINMultiLookup
        Me.lblPlan = New System.Windows.Forms.Label
        Me.clcPorcentaje = New DevExpress.XtraEditors.CalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.MemoEdit
        Me.clcTotalContado = New DevExpress.XtraEditors.CalcEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.clcTotalCredito = New DevExpress.XtraEditors.CalcEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.Label8 = New System.Windows.Forms.Label
        CType(Me.clcPaquete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grDescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotalContado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotalCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2032, 28)
        '
        'clcPaquete
        '
        Me.clcPaquete.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPaquete.Location = New System.Drawing.Point(104, 37)
        Me.clcPaquete.Name = "clcPaquete"
        '
        'clcPaquete.Properties
        '
        Me.clcPaquete.Properties.Enabled = False
        Me.clcPaquete.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPaquete.Size = New System.Drawing.Size(72, 20)
        Me.clcPaquete.TabIndex = 76
        Me.clcPaquete.Tag = "paquete"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 16)
        Me.Label3.TabIndex = 75
        Me.Label3.Tag = ""
        Me.Label3.Text = "Paque&te:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grDescuentos
        '
        '
        'grDescuentos.EmbeddedNavigator
        '
        Me.grDescuentos.EmbeddedNavigator.Name = ""
        Me.grDescuentos.Location = New System.Drawing.Point(5, 248)
        Me.grDescuentos.MainView = Me.grvDescuentos
        Me.grDescuentos.Name = "grDescuentos"
        Me.grDescuentos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.grchkSobrePedido, Me.grchkReparto, Me.grchkSurtido})
        Me.grDescuentos.Size = New System.Drawing.Size(632, 160)
        Me.grDescuentos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grDescuentos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.TabIndex = 88
        Me.grDescuentos.TabStop = False
        Me.grDescuentos.Text = "Ventas"
        '
        'grvDescuentos
        '
        Me.grvDescuentos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPartida, Me.grcNArticulo, Me.grcArticulo, Me.grcCantidad, Me.grcModelo, Me.grcPrecioDescuento, Me.grcPrecioCredito})
        Me.grvDescuentos.GridControl = Me.grDescuentos
        Me.grvDescuentos.Name = "grvDescuentos"
        Me.grvDescuentos.OptionsCustomization.AllowFilter = False
        Me.grvDescuentos.OptionsCustomization.AllowGroup = False
        Me.grvDescuentos.OptionsCustomization.AllowSort = False
        Me.grvDescuentos.OptionsView.ShowFooter = True
        Me.grvDescuentos.OptionsView.ShowGroupPanel = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "Partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.Width = 52
        '
        'grcNArticulo
        '
        Me.grcNArticulo.Caption = "Art�culo"
        Me.grcNArticulo.FieldName = "n_articulo"
        Me.grcNArticulo.Name = "grcNArticulo"
        Me.grcNArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNArticulo.VisibleIndex = 2
        Me.grcNArticulo.Width = 229
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 0
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 3
        Me.grcCantidad.Width = 57
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 1
        Me.grcModelo.Width = 107
        '
        'grcPrecioDescuento
        '
        Me.grcPrecioDescuento.Caption = "Precio Contado"
        Me.grcPrecioDescuento.DisplayFormat.FormatString = "c2"
        Me.grcPrecioDescuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioDescuento.FieldName = "precio_contado"
        Me.grcPrecioDescuento.Name = "grcPrecioDescuento"
        Me.grcPrecioDescuento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioDescuento.SummaryItem.DisplayFormat = "{0:C2}"
        Me.grcPrecioDescuento.SummaryItem.FieldName = "precio_descuento"
        Me.grcPrecioDescuento.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcPrecioDescuento.VisibleIndex = 4
        Me.grcPrecioDescuento.Width = 115
        '
        'grcPrecioCredito
        '
        Me.grcPrecioCredito.Caption = "Precio Credito"
        Me.grcPrecioCredito.DisplayFormat.FormatString = "c2"
        Me.grcPrecioCredito.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioCredito.FieldName = "precio_credito"
        Me.grcPrecioCredito.Name = "grcPrecioCredito"
        Me.grcPrecioCredito.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioCredito.VisibleIndex = 5
        '
        'grchkSobrePedido
        '
        Me.grchkSobrePedido.AutoHeight = False
        Me.grchkSobrePedido.Name = "grchkSobrePedido"
        Me.grchkSobrePedido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grchkReparto
        '
        Me.grchkReparto.AutoHeight = False
        Me.grchkReparto.Name = "grchkReparto"
        Me.grchkReparto.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grchkSurtido
        '
        Me.grchkSurtido.AutoHeight = False
        Me.grchkSurtido.Name = "grchkSurtido"
        Me.grchkSurtido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'tmaDetalle
        '
        Me.tmaDetalle.BackColor = System.Drawing.Color.White
        Me.tmaDetalle.CanDelete = True
        Me.tmaDetalle.CanInsert = True
        Me.tmaDetalle.CanUpdate = True
        Me.tmaDetalle.Grid = Me.grDescuentos
        Me.tmaDetalle.Location = New System.Drawing.Point(6, 224)
        Me.tmaDetalle.Name = "tmaDetalle"
        Me.tmaDetalle.Size = New System.Drawing.Size(611, 23)
        Me.tmaDetalle.TabIndex = 87
        Me.tmaDetalle.TabStop = False
        Me.tmaDetalle.Title = "Art�culos"
        Me.tmaDetalle.UpdateTitle = "un Art�culo"
        '
        'lkpPlan
        '
        Me.lkpPlan.AllowAdd = False
        Me.lkpPlan.AutoReaload = False
        Me.lkpPlan.DataSource = Nothing
        Me.lkpPlan.DefaultSearchField = ""
        Me.lkpPlan.DisplayMember = "descripcion"
        Me.lkpPlan.EditValue = Nothing
        Me.lkpPlan.Filtered = False
        Me.lkpPlan.InitValue = Nothing
        Me.lkpPlan.Location = New System.Drawing.Point(104, 115)
        Me.lkpPlan.MultiSelect = False
        Me.lkpPlan.Name = "lkpPlan"
        Me.lkpPlan.NullText = ""
        Me.lkpPlan.PopupWidth = CType(450, Long)
        Me.lkpPlan.ReadOnlyControl = False
        Me.lkpPlan.Required = False
        Me.lkpPlan.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlan.SearchMember = ""
        Me.lkpPlan.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlan.SelectAll = False
        Me.lkpPlan.Size = New System.Drawing.Size(272, 20)
        Me.lkpPlan.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPlan.TabIndex = 90
        Me.lkpPlan.Tag = "plan_credito"
        Me.lkpPlan.ToolTip = Nothing
        Me.lkpPlan.ValueMember = "plan_credito"
        '
        'lblPlan
        '
        Me.lblPlan.AutoSize = True
        Me.lblPlan.Location = New System.Drawing.Point(7, 121)
        Me.lblPlan.Name = "lblPlan"
        Me.lblPlan.Size = New System.Drawing.Size(91, 16)
        Me.lblPlan.TabIndex = 89
        Me.lblPlan.Tag = ""
        Me.lblPlan.Text = "&Plan de cr�dito:"
        Me.lblPlan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPorcentaje
        '
        Me.clcPorcentaje.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPorcentaje.Location = New System.Drawing.Point(104, 140)
        Me.clcPorcentaje.Name = "clcPorcentaje"
        '
        'clcPorcentaje.Properties
        '
        Me.clcPorcentaje.Properties.DisplayFormat.FormatString = "n2"
        Me.clcPorcentaje.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.Properties.EditFormat.FormatString = "n2"
        Me.clcPorcentaje.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPorcentaje.Size = New System.Drawing.Size(75, 20)
        Me.clcPorcentaje.TabIndex = 91
        Me.clcPorcentaje.Tag = "enganche"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 16)
        Me.Label2.TabIndex = 92
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Enganche:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(26, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 16)
        Me.Label4.TabIndex = 93
        Me.Label4.Tag = ""
        Me.Label4.Text = "&Descripci�n:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(104, 62)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(528, 48)
        Me.txtDescripcion.TabIndex = 94
        Me.txtDescripcion.Tag = "descripcion"
        '
        'clcTotalContado
        '
        Me.clcTotalContado.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcTotalContado.Location = New System.Drawing.Point(228, 424)
        Me.clcTotalContado.Name = "clcTotalContado"
        '
        'clcTotalContado.Properties
        '
        Me.clcTotalContado.Properties.DisplayFormat.FormatString = "c2"
        Me.clcTotalContado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalContado.Properties.EditFormat.FormatString = "c2"
        Me.clcTotalContado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalContado.Properties.Enabled = False
        Me.clcTotalContado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcTotalContado.Size = New System.Drawing.Size(75, 20)
        Me.clcTotalContado.TabIndex = 95
        Me.clcTotalContado.Tag = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(140, 424)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 16)
        Me.Label5.TabIndex = 96
        Me.Label5.Tag = ""
        Me.Label5.Text = "Total Contado:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotalCredito
        '
        Me.clcTotalCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcTotalCredito.Location = New System.Drawing.Point(428, 424)
        Me.clcTotalCredito.Name = "clcTotalCredito"
        '
        'clcTotalCredito.Properties
        '
        Me.clcTotalCredito.Properties.DisplayFormat.FormatString = "c2"
        Me.clcTotalCredito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalCredito.Properties.EditFormat.FormatString = "c2"
        Me.clcTotalCredito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotalCredito.Properties.Enabled = False
        Me.clcTotalCredito.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcTotalCredito.Size = New System.Drawing.Size(75, 20)
        Me.clcTotalCredito.TabIndex = 97
        Me.clcTotalCredito.Tag = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(348, 424)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 98
        Me.Label6.Tag = ""
        Me.Label6.Text = "Total Cr�dito:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(104, 165)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Ini.TabIndex = 4
        Me.dteFecha_Ini.Tag = "fecha_inicial"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(51, 171)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 16)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "&Desde: "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(104, 190)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha_Fin.TabIndex = 6
        Me.dteFecha_Fin.Tag = "fecha_final"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(54, 192)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 16)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Has&ta: "
        '
        'frmPaquetes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(642, 448)
        Me.Controls.Add(Me.clcTotalCredito)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.clcTotalContado)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.clcPorcentaje)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpPlan)
        Me.Controls.Add(Me.lblPlan)
        Me.Controls.Add(Me.grDescuentos)
        Me.Controls.Add(Me.tmaDetalle)
        Me.Controls.Add(Me.clcPaquete)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dteFecha_Fin)
        Me.Controls.Add(Me.dteFecha_Ini)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Name = "frmPaquetes"
        Me.Text = "frmPaquetes"
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Ini, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Fin, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.clcPaquete, 0)
        Me.Controls.SetChildIndex(Me.tmaDetalle, 0)
        Me.Controls.SetChildIndex(Me.grDescuentos, 0)
        Me.Controls.SetChildIndex(Me.lblPlan, 0)
        Me.Controls.SetChildIndex(Me.lkpPlan, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcPorcentaje, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.clcTotalContado, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.clcTotalCredito, 0)
        CType(Me.clcPaquete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grDescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotalContado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotalCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oPlanesCredito As New VillarrealBusiness.clsPlanesCredito
    Private osucursales As New VillarrealBusiness.clsSucursales
    Private oPaquetes As New VillarrealBusiness.clsPaquetes
    Private oPaquetesDetalle As New VillarrealBusiness.clsPaquetesDetalle


    Private sucursal_dependencia As Boolean

    Private ReadOnly Property PlanCredito() As Long
        Get
            Return PreparaValorLookup(Me.lkpPlan)
        End Get
    End Property

    Private Sub frmPaquetes_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmPaquetes_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()

    End Sub
    Private Sub frmPaquetes_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

    End Sub

    Private Sub frmPaquetes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = Me.oPaquetes.Insertar(Me.DataSource)
            Case Actions.Update
                Response = Me.oPaquetes.Actualizar(Me.DataSource)
            Case Actions.Delete
                Response = Me.oPaquetes.Eliminar(Me.DataSource)
        End Select
    End Sub
    Private Sub frmPaquetes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        ConfiguraMaster()
        sucursal_dependencia = CType(CType(osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)

    End Sub
    Private Sub frmPaquetes_Localize() Handles MyBase.Localize
        Find("paquete", Me.clcPaquete.Value)
    End Sub
    Private Sub frmPaquetes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oPaquetes.DespliegaDatos(OwnerForm.Value("paquete"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            Response = oPaquetesDetalle.DespliegaDatos(OwnerForm.Value("paquete"), -1)
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                tmaDetalle.DataSource = oDataSet
            End If

        End If
        CalculaPrecioDescuento()
    End Sub
    Private Sub frmPaquetes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = Me.oPaquetes.Validacion(Action, PlanCredito)
    End Sub
    Private Sub frmPaquetes_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With tmaDetalle
            .MoveFirst()
            Do While Not tmaDetalle.EOF
                If Me.tmaDetalle.Item("cantidad") > 0 Then

                    Select Case tmaDetalle.CurrentAction
                        Case Actions.Insert
                            Response = Me.oPaquetesDetalle.Insertar(Me.clcPaquete.Value, .SelectedRow)
                        Case Actions.Update
                            Response = Me.oPaquetesDetalle.Actualizar(Me.clcPaquete.Value, .SelectedRow)
                        Case Actions.Delete
                            Response = Me.oPaquetesDetalle.Eliminar(Me.clcPaquete.Value, .SelectedRow.Tables(0).Rows(0).Item("partida"))
                    End Select

                End If
                tmaDetalle.MoveNext()
            Loop
        End With
    End Sub

    Private Sub lkpPlan_LoadData(ByVal Initialize As Boolean) Handles lkpPlan.LoadData

        Dim Response As New Events
        Response = oPlanesCredito.Lookup(sucursal_dependencia, , 1)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlan.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If

        Response = Nothing

    End Sub
    Private Sub lkpPlan_Format() Handles lkpPlan.Format
        Comunes.clsFormato.for_planes_credito_grl(Me.lkpPlan)
    End Sub


    Private Sub ConfiguraMaster()
        With Me.tmaDetalle
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmPaquetesDetalle
            .AddColumn("departamento")
            .AddColumn("grupo")
            .AddColumn("articulo", "System.Int32")
            .AddColumn("n_articulo")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("modelo")
            .AddColumn("precio_contado", "System.Double")
            .AddColumn("precio_credito", "System.Double")
        End With
    End Sub
    Public Sub CalculaPrecioDescuento()
        Dim precio_contado_total As Double = 0
        Dim precio_credito_total As Double = 0

        With Me.tmaDetalle
            .MoveFirst()
            Do While Not .EOF

                precio_contado_total = precio_contado_total + (.Item("precio_contado") * .Item("cantidad"))
                precio_credito_total = precio_credito_total + (.Item("precio_credito") * .Item("cantidad"))

               
                .MoveNext()
            Loop
        End With

        Me.clcTotalContado.Value = precio_contado_total
        Me.clcTotalCredito.Value = precio_credito_total
    End Sub

End Class
