Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Drawing
Imports System.Windows.Forms

Public Class frmMovimientosArticulos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lblUnidad As System.Windows.Forms.Label
    Friend WithEvents txtUnidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcPrecioLista As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clcPrecioContado As Dipros.Editors.TINCalcEdit
    Friend WithEvents gpEstadisticas As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents clcCantidadEntradas As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcCantidadSalidas As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents lblUltimaFecha As System.Windows.Forms.Label
    Friend WithEvents clcFechaSalida As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcFechaEntrada As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblUltimoCostoPrecio As System.Windows.Forms.Label
    Friend WithEvents clcUltimoPrecio As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcUltimoCosto As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblValor As System.Windows.Forms.Label
    Friend WithEvents clcValorSalida As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcValorEntrada As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcionCorta As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblMovimientos As System.Windows.Forms.Label
    Friend WithEvents grMovimientos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvMovimientos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcReferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents grExistencias As DevExpress.XtraGrid.GridControl
    Friend WithEvents btnVerDocumentos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grExistenciasSucursales As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvExistencias As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcBodegaExistencias As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombrebodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFisica As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rptCantidades As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcPorSurtir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTransito As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEntregar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcReparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcVistas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcGarantia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grvClaveSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rptExistenciasSucursales As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents cmdVerMovimiento As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEntregarPedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcProveedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnFotografia As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosArticulos))
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lblDescripcionCorta = New System.Windows.Forms.Label
        Me.lblUnidad = New System.Windows.Forms.Label
        Me.clcPrecioLista = New Dipros.Editors.TINCalcEdit
        Me.txtUnidad = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.clcPrecioContado = New Dipros.Editors.TINCalcEdit
        Me.gpEstadisticas = New System.Windows.Forms.GroupBox
        Me.clcCantidadEntradas = New Dipros.Editors.TINCalcEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.clcCantidadSalidas = New Dipros.Editors.TINCalcEdit
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.lblUltimaFecha = New System.Windows.Forms.Label
        Me.clcFechaSalida = New Dipros.Editors.TINCalcEdit
        Me.clcFechaEntrada = New Dipros.Editors.TINCalcEdit
        Me.lblUltimoCostoPrecio = New System.Windows.Forms.Label
        Me.clcUltimoPrecio = New Dipros.Editors.TINCalcEdit
        Me.clcUltimoCosto = New Dipros.Editors.TINCalcEdit
        Me.lblValor = New System.Windows.Forms.Label
        Me.clcValorSalida = New Dipros.Editors.TINCalcEdit
        Me.clcValorEntrada = New Dipros.Editors.TINCalcEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.lblMovimientos = New System.Windows.Forms.Label
        Me.grMovimientos = New DevExpress.XtraGrid.GridControl
        Me.grvMovimientos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcProveedor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcReferencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.grExistenciasSucursales = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grvClaveSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rptExistenciasSucursales = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.grExistencias = New DevExpress.XtraGrid.GridControl
        Me.grvExistencias = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcBodegaExistencias = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombrebodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFisica = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rptCantidades = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcPorSurtir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTransito = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEntregar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcReparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcVistas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGarantia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEntregarPedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnVerDocumentos = New DevExpress.XtraEditors.SimpleButton
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.cmdVerMovimiento = New DevExpress.XtraEditors.SimpleButton
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.btnFotografia = New DevExpress.XtraEditors.SimpleButton
        CType(Me.clcPrecioLista.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioContado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpEstadisticas.SuspendLayout()
        CType(Me.clcCantidadEntradas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidadSalidas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFechaSalida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFechaEntrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcUltimoPrecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcUltimoCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcValorSalida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcValorEntrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grExistenciasSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptExistenciasSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.grExistencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvExistencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptCantidades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(6697, 28)
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = True
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(128, 80)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(470, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(200, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 5
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "articulo"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(72, 80)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 4
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Art�cul&o:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescripcionCorta
        '
        Me.lblDescripcionCorta.Location = New System.Drawing.Point(336, 80)
        Me.lblDescripcionCorta.Name = "lblDescripcionCorta"
        Me.lblDescripcionCorta.Size = New System.Drawing.Size(288, 20)
        Me.lblDescripcionCorta.TabIndex = 6
        Me.lblDescripcionCorta.Tag = "descripcion_corta"
        '
        'lblUnidad
        '
        Me.lblUnidad.AutoSize = True
        Me.lblUnidad.Location = New System.Drawing.Point(15, 127)
        Me.lblUnidad.Name = "lblUnidad"
        Me.lblUnidad.Size = New System.Drawing.Size(108, 16)
        Me.lblUnidad.TabIndex = 11
        Me.lblUnidad.Tag = ""
        Me.lblUnidad.Text = "&Unidad de Medida:"
        Me.lblUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecioLista
        '
        Me.clcPrecioLista.EditValue = "0"
        Me.clcPrecioLista.Location = New System.Drawing.Point(344, 127)
        Me.clcPrecioLista.MaxValue = 0
        Me.clcPrecioLista.MinValue = 0
        Me.clcPrecioLista.Name = "clcPrecioLista"
        '
        'clcPrecioLista.Properties
        '
        Me.clcPrecioLista.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecioLista.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioLista.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecioLista.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioLista.Properties.Enabled = False
        Me.clcPrecioLista.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecioLista.Properties.Precision = 2
        Me.clcPrecioLista.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecioLista.Size = New System.Drawing.Size(72, 19)
        Me.clcPrecioLista.TabIndex = 14
        Me.clcPrecioLista.Tag = ""
        '
        'txtUnidad
        '
        Me.txtUnidad.EditValue = ""
        Me.txtUnidad.Location = New System.Drawing.Point(128, 127)
        Me.txtUnidad.Name = "txtUnidad"
        '
        'txtUnidad.Properties
        '
        Me.txtUnidad.Properties.Enabled = False
        Me.txtUnidad.Properties.MaxLength = 50
        Me.txtUnidad.Size = New System.Drawing.Size(96, 20)
        Me.txtUnidad.TabIndex = 12
        Me.txtUnidad.Tag = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(248, 127)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 16)
        Me.Label2.TabIndex = 13
        Me.Label2.Tag = ""
        Me.Label2.Text = "Precio de &Lista:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(440, 127)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(109, 16)
        Me.Label4.TabIndex = 15
        Me.Label4.Tag = ""
        Me.Label4.Text = "Prec&io de Contado:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecioContado
        '
        Me.clcPrecioContado.EditValue = "0"
        Me.clcPrecioContado.Location = New System.Drawing.Point(552, 127)
        Me.clcPrecioContado.MaxValue = 0
        Me.clcPrecioContado.MinValue = 0
        Me.clcPrecioContado.Name = "clcPrecioContado"
        '
        'clcPrecioContado.Properties
        '
        Me.clcPrecioContado.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecioContado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioContado.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcPrecioContado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioContado.Properties.Enabled = False
        Me.clcPrecioContado.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPrecioContado.Properties.Precision = 2
        Me.clcPrecioContado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPrecioContado.Size = New System.Drawing.Size(72, 19)
        Me.clcPrecioContado.TabIndex = 16
        Me.clcPrecioContado.Tag = ""
        '
        'gpEstadisticas
        '
        Me.gpEstadisticas.Controls.Add(Me.clcCantidadEntradas)
        Me.gpEstadisticas.Controls.Add(Me.Label5)
        Me.gpEstadisticas.Controls.Add(Me.Label6)
        Me.gpEstadisticas.Controls.Add(Me.clcCantidadSalidas)
        Me.gpEstadisticas.Controls.Add(Me.lblCantidad)
        Me.gpEstadisticas.Controls.Add(Me.lblUltimaFecha)
        Me.gpEstadisticas.Controls.Add(Me.clcFechaSalida)
        Me.gpEstadisticas.Controls.Add(Me.clcFechaEntrada)
        Me.gpEstadisticas.Controls.Add(Me.lblUltimoCostoPrecio)
        Me.gpEstadisticas.Controls.Add(Me.clcUltimoPrecio)
        Me.gpEstadisticas.Controls.Add(Me.clcUltimoCosto)
        Me.gpEstadisticas.Controls.Add(Me.lblValor)
        Me.gpEstadisticas.Controls.Add(Me.clcValorSalida)
        Me.gpEstadisticas.Controls.Add(Me.clcValorEntrada)
        Me.gpEstadisticas.Location = New System.Drawing.Point(16, 152)
        Me.gpEstadisticas.Name = "gpEstadisticas"
        Me.gpEstadisticas.Size = New System.Drawing.Size(608, 80)
        Me.gpEstadisticas.TabIndex = 17
        Me.gpEstadisticas.TabStop = False
        Me.gpEstadisticas.Text = "Estad�sticas"
        '
        'clcCantidadEntradas
        '
        Me.clcCantidadEntradas.EditValue = "0"
        Me.clcCantidadEntradas.Location = New System.Drawing.Point(72, 32)
        Me.clcCantidadEntradas.MaxValue = 0
        Me.clcCantidadEntradas.MinValue = 0
        Me.clcCantidadEntradas.Name = "clcCantidadEntradas"
        '
        'clcCantidadEntradas.Properties
        '
        Me.clcCantidadEntradas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadEntradas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadEntradas.Properties.Enabled = False
        Me.clcCantidadEntradas.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidadEntradas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidadEntradas.Size = New System.Drawing.Size(112, 19)
        Me.clcCantidadEntradas.TabIndex = 14
        Me.clcCantidadEntradas.Tag = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 16)
        Me.Label5.TabIndex = 13
        Me.Label5.Tag = ""
        Me.Label5.Text = "Entradas:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(18, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 16)
        Me.Label6.TabIndex = 18
        Me.Label6.Tag = ""
        Me.Label6.Text = "Salidas:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidadSalidas
        '
        Me.clcCantidadSalidas.EditValue = "0"
        Me.clcCantidadSalidas.Location = New System.Drawing.Point(72, 56)
        Me.clcCantidadSalidas.MaxValue = 0
        Me.clcCantidadSalidas.MinValue = 0
        Me.clcCantidadSalidas.Name = "clcCantidadSalidas"
        '
        'clcCantidadSalidas.Properties
        '
        Me.clcCantidadSalidas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadSalidas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidadSalidas.Properties.Enabled = False
        Me.clcCantidadSalidas.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidadSalidas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidadSalidas.Size = New System.Drawing.Size(112, 19)
        Me.clcCantidadSalidas.TabIndex = 19
        Me.clcCantidadSalidas.Tag = ""
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(102, 16)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(53, 16)
        Me.lblCantidad.TabIndex = 100
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "Cantidad"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUltimaFecha
        '
        Me.lblUltimaFecha.AutoSize = True
        Me.lblUltimaFecha.Location = New System.Drawing.Point(208, 16)
        Me.lblUltimaFecha.Name = "lblUltimaFecha"
        Me.lblUltimaFecha.Size = New System.Drawing.Size(76, 16)
        Me.lblUltimaFecha.TabIndex = 5
        Me.lblUltimaFecha.Tag = ""
        Me.lblUltimaFecha.Text = "�ltima Fecha"
        Me.lblUltimaFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFechaSalida
        '
        Me.clcFechaSalida.EditValue = "0"
        Me.clcFechaSalida.Location = New System.Drawing.Point(192, 56)
        Me.clcFechaSalida.MaxValue = 0
        Me.clcFechaSalida.MinValue = 0
        Me.clcFechaSalida.Name = "clcFechaSalida"
        '
        'clcFechaSalida.Properties
        '
        Me.clcFechaSalida.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.clcFechaSalida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.clcFechaSalida.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.clcFechaSalida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.clcFechaSalida.Properties.Enabled = False
        Me.clcFechaSalida.Properties.MaskData.EditMask = "dd/MMM/yyyy"
        Me.clcFechaSalida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFechaSalida.Size = New System.Drawing.Size(112, 19)
        Me.clcFechaSalida.TabIndex = 20
        Me.clcFechaSalida.Tag = ""
        '
        'clcFechaEntrada
        '
        Me.clcFechaEntrada.EditValue = "0"
        Me.clcFechaEntrada.Location = New System.Drawing.Point(192, 32)
        Me.clcFechaEntrada.MaxValue = 0
        Me.clcFechaEntrada.MinValue = 0
        Me.clcFechaEntrada.Name = "clcFechaEntrada"
        '
        'clcFechaEntrada.Properties
        '
        Me.clcFechaEntrada.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.clcFechaEntrada.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.clcFechaEntrada.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.clcFechaEntrada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.clcFechaEntrada.Properties.Enabled = False
        Me.clcFechaEntrada.Properties.MaskData.EditMask = "dd/MMM/yyyy"
        Me.clcFechaEntrada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFechaEntrada.Size = New System.Drawing.Size(112, 19)
        Me.clcFechaEntrada.TabIndex = 15
        Me.clcFechaEntrada.Tag = ""
        '
        'lblUltimoCostoPrecio
        '
        Me.lblUltimoCostoPrecio.AutoSize = True
        Me.lblUltimoCostoPrecio.Location = New System.Drawing.Point(312, 16)
        Me.lblUltimoCostoPrecio.Name = "lblUltimoCostoPrecio"
        Me.lblUltimoCostoPrecio.Size = New System.Drawing.Size(113, 16)
        Me.lblUltimoCostoPrecio.TabIndex = 5
        Me.lblUltimoCostoPrecio.Tag = ""
        Me.lblUltimoCostoPrecio.Text = "�ltimo Costo/Precio"
        Me.lblUltimoCostoPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcUltimoPrecio
        '
        Me.clcUltimoPrecio.EditValue = "0"
        Me.clcUltimoPrecio.Location = New System.Drawing.Point(312, 56)
        Me.clcUltimoPrecio.MaxValue = 0
        Me.clcUltimoPrecio.MinValue = 0
        Me.clcUltimoPrecio.Name = "clcUltimoPrecio"
        '
        'clcUltimoPrecio.Properties
        '
        Me.clcUltimoPrecio.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcUltimoPrecio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimoPrecio.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcUltimoPrecio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimoPrecio.Properties.Enabled = False
        Me.clcUltimoPrecio.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcUltimoPrecio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcUltimoPrecio.Size = New System.Drawing.Size(112, 19)
        Me.clcUltimoPrecio.TabIndex = 21
        Me.clcUltimoPrecio.Tag = ""
        '
        'clcUltimoCosto
        '
        Me.clcUltimoCosto.EditValue = "0"
        Me.clcUltimoCosto.Location = New System.Drawing.Point(312, 32)
        Me.clcUltimoCosto.MaxValue = 0
        Me.clcUltimoCosto.MinValue = 0
        Me.clcUltimoCosto.Name = "clcUltimoCosto"
        '
        'clcUltimoCosto.Properties
        '
        Me.clcUltimoCosto.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcUltimoCosto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimoCosto.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcUltimoCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcUltimoCosto.Properties.Enabled = False
        Me.clcUltimoCosto.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcUltimoCosto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcUltimoCosto.Size = New System.Drawing.Size(112, 19)
        Me.clcUltimoCosto.TabIndex = 16
        Me.clcUltimoCosto.Tag = ""
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Location = New System.Drawing.Point(488, 16)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(33, 16)
        Me.lblValor.TabIndex = 5
        Me.lblValor.Tag = ""
        Me.lblValor.Text = "Valor"
        Me.lblValor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcValorSalida
        '
        Me.clcValorSalida.EditValue = "0"
        Me.clcValorSalida.Location = New System.Drawing.Point(432, 56)
        Me.clcValorSalida.MaxValue = 0
        Me.clcValorSalida.MinValue = 0
        Me.clcValorSalida.Name = "clcValorSalida"
        '
        'clcValorSalida.Properties
        '
        Me.clcValorSalida.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcValorSalida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcValorSalida.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcValorSalida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcValorSalida.Properties.Enabled = False
        Me.clcValorSalida.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcValorSalida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcValorSalida.Size = New System.Drawing.Size(144, 19)
        Me.clcValorSalida.TabIndex = 22
        Me.clcValorSalida.Tag = ""
        '
        'clcValorEntrada
        '
        Me.clcValorEntrada.EditValue = "0"
        Me.clcValorEntrada.Location = New System.Drawing.Point(432, 32)
        Me.clcValorEntrada.MaxValue = 0
        Me.clcValorEntrada.MinValue = 0
        Me.clcValorEntrada.Name = "clcValorEntrada"
        '
        'clcValorEntrada.Properties
        '
        Me.clcValorEntrada.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcValorEntrada.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcValorEntrada.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcValorEntrada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcValorEntrada.Properties.Enabled = False
        Me.clcValorEntrada.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcValorEntrada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcValorEntrada.Size = New System.Drawing.Size(144, 19)
        Me.clcValorEntrada.TabIndex = 17
        Me.clcValorEntrada.Tag = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(16, 240)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 17)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "EXISTENCIAS"
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(175, 440)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha_Ini.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Ini.TabIndex = 22
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(128, 442)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 16)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "De&sde: "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(288, 442)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(41, 16)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "Has&ta:"
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(329, 440)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha_Fin.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Fin.TabIndex = 24
        '
        'lblMovimientos
        '
        Me.lblMovimientos.AutoSize = True
        Me.lblMovimientos.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMovimientos.Location = New System.Drawing.Point(16, 448)
        Me.lblMovimientos.Name = "lblMovimientos"
        Me.lblMovimientos.Size = New System.Drawing.Size(99, 17)
        Me.lblMovimientos.TabIndex = 20
        Me.lblMovimientos.Text = "MOVIMIENTOS"
        '
        'grMovimientos
        '
        '
        'grMovimientos.EmbeddedNavigator
        '
        Me.grMovimientos.EmbeddedNavigator.Name = ""
        Me.grMovimientos.Location = New System.Drawing.Point(16, 464)
        Me.grMovimientos.MainView = Me.grvMovimientos
        Me.grMovimientos.Name = "grMovimientos"
        Me.grMovimientos.Size = New System.Drawing.Size(608, 128)
        Me.grMovimientos.TabIndex = 25
        Me.grMovimientos.Text = "Movimientos"
        '
        'grvMovimientos
        '
        Me.grvMovimientos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcConcepto, Me.grcProveedor, Me.grcFecha, Me.grcReferencia, Me.grcCantidad})
        Me.grvMovimientos.GridControl = Me.grMovimientos
        Me.grvMovimientos.Name = "grvMovimientos"
        Me.grvMovimientos.OptionsBehavior.Editable = False
        Me.grvMovimientos.OptionsCustomization.AllowFilter = False
        Me.grvMovimientos.OptionsCustomization.AllowGroup = False
        Me.grvMovimientos.OptionsCustomization.AllowSort = False
        Me.grvMovimientos.OptionsView.ShowGroupPanel = False
        Me.grvMovimientos.OptionsView.ShowIndicator = False
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "descripcion_concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.VisibleIndex = 0
        Me.grcConcepto.Width = 255
        '
        'grcProveedor
        '
        Me.grcProveedor.Caption = "Proveedor"
        Me.grcProveedor.FieldName = "proveedor"
        Me.grcProveedor.Name = "grcProveedor"
        Me.grcProveedor.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcProveedor.VisibleIndex = 1
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 2
        Me.grcFecha.Width = 110
        '
        'grcReferencia
        '
        Me.grcReferencia.Caption = "Referencia"
        Me.grcReferencia.FieldName = "referencia"
        Me.grcReferencia.Name = "grcReferencia"
        Me.grcReferencia.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcReferencia.VisibleIndex = 3
        Me.grcReferencia.Width = 175
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.DisplayFormat.FormatString = "###,###,##0"
        Me.grcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 4
        Me.grcCantidad.Width = 66
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(16, 256)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(608, 175)
        Me.TabControl1.TabIndex = 19
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage2.Controls.Add(Me.grExistenciasSucursales)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(600, 149)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Sucursal"
        Me.TabPage2.Visible = False
        '
        'grExistenciasSucursales
        '
        Me.grExistenciasSucursales.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grExistenciasSucursales.EmbeddedNavigator
        '
        Me.grExistenciasSucursales.EmbeddedNavigator.Name = ""
        Me.grExistenciasSucursales.Location = New System.Drawing.Point(0, 0)
        Me.grExistenciasSucursales.MainView = Me.GridView1
        Me.grExistenciasSucursales.Name = "grExistenciasSucursales"
        Me.grExistenciasSucursales.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptExistenciasSucursales})
        Me.grExistenciasSucursales.Size = New System.Drawing.Size(600, 149)
        Me.grExistenciasSucursales.TabIndex = 0
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grvClaveSucursal, Me.grcNombreSucursal, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn1})
        Me.GridView1.GridControl = Me.grExistenciasSucursales
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsMenu.EnableFooterMenu = False
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'grvClaveSucursal
        '
        Me.grvClaveSucursal.Caption = "Clave Sucursal"
        Me.grvClaveSucursal.FieldName = "sucursal"
        Me.grvClaveSucursal.Name = "grvClaveSucursal"
        Me.grvClaveSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursal.SummaryItem.DisplayFormat = "Totales"
        Me.grcNombreSucursal.SummaryItem.FieldName = "nombre_bodega"
        Me.grcNombreSucursal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom
        Me.grcNombreSucursal.VisibleIndex = 0
        Me.grcNombreSucursal.Width = 182
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "F�sica"
        Me.GridColumn3.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn3.DisplayFormat.FormatString = "n0"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn3.FieldName = "fisica"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn3.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 62
        '
        'rptExistenciasSucursales
        '
        Me.rptExistenciasSucursales.AutoHeight = False
        Me.rptExistenciasSucursales.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rptExistenciasSucursales.DisplayFormat.FormatString = "n0"
        Me.rptExistenciasSucursales.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptExistenciasSucursales.EditFormat.FormatString = "n0"
        Me.rptExistenciasSucursales.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptExistenciasSucursales.Name = "rptExistenciasSucursales"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Por Surtir"
        Me.GridColumn4.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn4.DisplayFormat.FormatString = "n0"
        Me.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn4.FieldName = "por_surtir"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 77
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Tr�nsito"
        Me.GridColumn5.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn5.DisplayFormat.FormatString = "n0"
        Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn5.FieldName = "transito"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn5.VisibleIndex = 3
        Me.GridColumn5.Width = 81
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Entregar"
        Me.GridColumn6.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn6.DisplayFormat.FormatString = "n0"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn6.FieldName = "entregar"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn6.VisibleIndex = 4
        Me.GridColumn6.Width = 94
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Reparto"
        Me.GridColumn7.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn7.DisplayFormat.FormatString = "n0"
        Me.GridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn7.FieldName = "reparto"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn7.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn7.VisibleIndex = 6
        Me.GridColumn7.Width = 87
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Vistas"
        Me.GridColumn8.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn8.DisplayFormat.FormatString = "n0"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "vistas"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn8.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn8.VisibleIndex = 7
        Me.GridColumn8.Width = 77
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Garant�a"
        Me.GridColumn9.ColumnEdit = Me.rptExistenciasSucursales
        Me.GridColumn9.DisplayFormat.FormatString = "n0"
        Me.GridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn9.FieldName = "garantia"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn9.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn9.VisibleIndex = 8
        Me.GridColumn9.Width = 86
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Pedido Fabrica"
        Me.GridColumn1.FieldName = "entregar_pedido_fabrica"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn1.VisibleIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage1.Controls.Add(Me.grExistencias)
        Me.TabPage1.Controls.Add(Me.btnVerDocumentos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(600, 149)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Bodegas"
        '
        'grExistencias
        '
        Me.grExistencias.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grExistencias.EmbeddedNavigator
        '
        Me.grExistencias.EmbeddedNavigator.Name = ""
        Me.grExistencias.Location = New System.Drawing.Point(0, 2)
        Me.grExistencias.MainView = Me.grvExistencias
        Me.grExistencias.Name = "grExistencias"
        Me.grExistencias.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptCantidades})
        Me.grExistencias.Size = New System.Drawing.Size(600, 117)
        Me.grExistencias.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Info, System.Drawing.Color.Black, System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grExistencias.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte)), System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grExistencias.TabIndex = 34
        Me.grExistencias.TabStop = False
        Me.grExistencias.Text = "GridControl1"
        '
        'grvExistencias
        '
        Me.grvExistencias.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcBodegaExistencias, Me.grcNombrebodega, Me.grcFisica, Me.grcPorSurtir, Me.grcTransito, Me.grcEntregar, Me.grcReparto, Me.grcVistas, Me.grcGarantia, Me.grcEntregarPedido})
        Me.grvExistencias.GridControl = Me.grExistencias
        Me.grvExistencias.Name = "grvExistencias"
        Me.grvExistencias.OptionsCustomization.AllowFilter = False
        Me.grvExistencias.OptionsCustomization.AllowGroup = False
        Me.grvExistencias.OptionsMenu.EnableFooterMenu = False
        Me.grvExistencias.OptionsView.ShowFooter = True
        Me.grvExistencias.OptionsView.ShowGroupPanel = False
        '
        'grcBodegaExistencias
        '
        Me.grcBodegaExistencias.Caption = "Clave Bodega"
        Me.grcBodegaExistencias.FieldName = "bodega"
        Me.grcBodegaExistencias.Name = "grcBodegaExistencias"
        Me.grcBodegaExistencias.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombrebodega
        '
        Me.grcNombrebodega.Caption = "Bodega"
        Me.grcNombrebodega.FieldName = "nombre_bodega"
        Me.grcNombrebodega.Name = "grcNombrebodega"
        Me.grcNombrebodega.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombrebodega.SummaryItem.DisplayFormat = "Totales"
        Me.grcNombrebodega.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom
        Me.grcNombrebodega.VisibleIndex = 0
        Me.grcNombrebodega.Width = 182
        '
        'grcFisica
        '
        Me.grcFisica.Caption = "F�sica"
        Me.grcFisica.ColumnEdit = Me.rptCantidades
        Me.grcFisica.DisplayFormat.FormatString = "n0"
        Me.grcFisica.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcFisica.FieldName = "fisica"
        Me.grcFisica.Name = "grcFisica"
        Me.grcFisica.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFisica.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcFisica.VisibleIndex = 1
        Me.grcFisica.Width = 62
        '
        'rptCantidades
        '
        Me.rptCantidades.AutoHeight = False
        Me.rptCantidades.DisplayFormat.FormatString = "n0"
        Me.rptCantidades.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptCantidades.EditFormat.FormatString = "n0"
        Me.rptCantidades.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rptCantidades.Name = "rptCantidades"
        '
        'grcPorSurtir
        '
        Me.grcPorSurtir.Caption = "Por Surtir"
        Me.grcPorSurtir.ColumnEdit = Me.rptCantidades
        Me.grcPorSurtir.DisplayFormat.FormatString = "n0"
        Me.grcPorSurtir.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPorSurtir.FieldName = "por_surtir"
        Me.grcPorSurtir.Name = "grcPorSurtir"
        Me.grcPorSurtir.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPorSurtir.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcPorSurtir.VisibleIndex = 2
        Me.grcPorSurtir.Width = 77
        '
        'grcTransito
        '
        Me.grcTransito.Caption = "Tr�nsito"
        Me.grcTransito.ColumnEdit = Me.rptCantidades
        Me.grcTransito.DisplayFormat.FormatString = "n0"
        Me.grcTransito.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcTransito.FieldName = "transito"
        Me.grcTransito.Name = "grcTransito"
        Me.grcTransito.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTransito.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcTransito.VisibleIndex = 3
        Me.grcTransito.Width = 81
        '
        'grcEntregar
        '
        Me.grcEntregar.Caption = "Entregar"
        Me.grcEntregar.ColumnEdit = Me.rptCantidades
        Me.grcEntregar.DisplayFormat.FormatString = "n0"
        Me.grcEntregar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcEntregar.FieldName = "entregar"
        Me.grcEntregar.Name = "grcEntregar"
        Me.grcEntregar.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEntregar.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcEntregar.VisibleIndex = 4
        Me.grcEntregar.Width = 94
        '
        'grcReparto
        '
        Me.grcReparto.Caption = "Reparto"
        Me.grcReparto.ColumnEdit = Me.rptCantidades
        Me.grcReparto.DisplayFormat.FormatString = "n0"
        Me.grcReparto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcReparto.FieldName = "reparto"
        Me.grcReparto.Name = "grcReparto"
        Me.grcReparto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcReparto.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcReparto.VisibleIndex = 6
        Me.grcReparto.Width = 87
        '
        'grcVistas
        '
        Me.grcVistas.Caption = "Vistas"
        Me.grcVistas.ColumnEdit = Me.rptCantidades
        Me.grcVistas.DisplayFormat.FormatString = "n0"
        Me.grcVistas.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcVistas.FieldName = "vistas"
        Me.grcVistas.Name = "grcVistas"
        Me.grcVistas.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcVistas.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcVistas.VisibleIndex = 7
        Me.grcVistas.Width = 77
        '
        'grcGarantia
        '
        Me.grcGarantia.Caption = "Garant�a"
        Me.grcGarantia.ColumnEdit = Me.rptCantidades
        Me.grcGarantia.DisplayFormat.FormatString = "n0"
        Me.grcGarantia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcGarantia.FieldName = "garantia"
        Me.grcGarantia.Name = "grcGarantia"
        Me.grcGarantia.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcGarantia.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcGarantia.VisibleIndex = 8
        Me.grcGarantia.Width = 86
        '
        'grcEntregarPedido
        '
        Me.grcEntregarPedido.Caption = "Pedido Fabrica"
        Me.grcEntregarPedido.FieldName = "entregar_pedido_fabrica"
        Me.grcEntregarPedido.Name = "grcEntregarPedido"
        Me.grcEntregarPedido.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcEntregarPedido.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcEntregarPedido.VisibleIndex = 5
        '
        'btnVerDocumentos
        '
        Me.btnVerDocumentos.Enabled = False
        Me.btnVerDocumentos.Location = New System.Drawing.Point(14, 121)
        Me.btnVerDocumentos.Name = "btnVerDocumentos"
        Me.btnVerDocumentos.Size = New System.Drawing.Size(136, 23)
        Me.btnVerDocumentos.TabIndex = 35
        Me.btnVerDocumentos.Text = "Ver Documentos"
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(128, 56)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(400, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = True
        Me.lkpGrupo.Size = New System.Drawing.Size(280, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 3
        Me.lkpGrupo.ToolTip = "Grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(80, 56)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 16)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "&Grupo:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(32, 32)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "&Departamento:"
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(128, 32)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = True
        Me.lkpDepartamento.Size = New System.Drawing.Size(280, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 1
        Me.lkpDepartamento.ToolTip = "Departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'cmdVerMovimiento
        '
        Me.cmdVerMovimiento.Enabled = False
        Me.cmdVerMovimiento.Location = New System.Drawing.Point(472, 440)
        Me.cmdVerMovimiento.Name = "cmdVerMovimiento"
        Me.cmdVerMovimiento.Size = New System.Drawing.Size(136, 23)
        Me.cmdVerMovimiento.TabIndex = 59
        Me.cmdVerMovimiento.Text = "Ver &Movimientos"
        '
        'lblProveedor
        '
        Me.lblProveedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblProveedor.Location = New System.Drawing.Point(128, 103)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(496, 20)
        Me.lblProveedor.TabIndex = 60
        Me.lblProveedor.Tag = "nombre_proveedor"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(58, 104)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(65, 16)
        Me.Label12.TabIndex = 61
        Me.Label12.Tag = ""
        Me.Label12.Text = "Proveedor:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(496, 130)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(109, 16)
        Me.Label13.TabIndex = 15
        Me.Label13.Tag = ""
        Me.Label13.Text = "Prec&io de Contado:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnFotografia
        '
        Me.btnFotografia.Enabled = False
        Me.btnFotografia.Location = New System.Drawing.Point(552, 48)
        Me.btnFotografia.Name = "btnFotografia"
        Me.btnFotografia.TabIndex = 94
        Me.btnFotografia.Text = "Fotografia"
        '
        'frmMovimientosArticulos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(634, 608)
        Me.Controls.Add(Me.btnFotografia)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.cmdVerMovimiento)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.grMovimientos)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.gpEstadisticas)
        Me.Controls.Add(Me.txtUnidad)
        Me.Controls.Add(Me.clcPrecioLista)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lblDescripcionCorta)
        Me.Controls.Add(Me.lblUnidad)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.clcPrecioContado)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblMovimientos)
        Me.Controls.Add(Me.dteFecha_Ini)
        Me.Controls.Add(Me.dteFecha_Fin)
        Me.Controls.Add(Me.Label13)
        Me.Name = "frmMovimientosArticulos"
        Me.Text = "frmMovimientosArticulos"
        Me.Controls.SetChildIndex(Me.Label13, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Fin, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Ini, 0)
        Me.Controls.SetChildIndex(Me.lblMovimientos, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioContado, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblUnidad, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcionCorta, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioLista, 0)
        Me.Controls.SetChildIndex(Me.txtUnidad, 0)
        Me.Controls.SetChildIndex(Me.gpEstadisticas, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.TabControl1, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.grMovimientos, 0)
        Me.Controls.SetChildIndex(Me.Label11, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.cmdVerMovimiento, 0)
        Me.Controls.SetChildIndex(Me.lblProveedor, 0)
        Me.Controls.SetChildIndex(Me.Label12, 0)
        Me.Controls.SetChildIndex(Me.btnFotografia, 0)
        CType(Me.clcPrecioLista.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioContado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpEstadisticas.ResumeLayout(False)
        CType(Me.clcCantidadEntradas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidadSalidas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFechaSalida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFechaEntrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcUltimoPrecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcUltimoCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcValorSalida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcValorEntrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grExistenciasSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptExistenciasSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        CType(Me.grExistencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvExistencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptCantidades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private EntroDespliegaArticulo As Boolean = False

#Region "DIPROS Systems, Declaraciones"
    Dim key As Keys

    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oArticulosExistencias As VillarrealBusiness.clsArticulosExistencias
    Private oMovimientosInventariosDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
    Private oGruposArticulos As VillarrealBusiness.clsGruposArticulos

    Private bodega_seleccionada As String
    Private n_bodega_seleccionada As String
    Private columna_seleccionada As String

    Private bodega_movimiento_seleccionada As String
    Private concepto_movimiento_seleccionada As String
    Private folio_movimiento_seleccionada As Long
    Private fecha_movimiento_seleccionada As Date
    Private fila_movimiento_seleccionada As Integer

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property

    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property

    Private ReadOnly Property Articulo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmMovimientosArticulos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oArticulos = New VillarrealBusiness.clsArticulos
        oArticulosExistencias = New VillarrealBusiness.clsArticulosExistencias
        oMovimientosInventariosDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oGruposArticulos = New VillarrealBusiness.clsGruposArticulos

        Me.Location = New Point(0, 0)
        Me.tbrTools.Buttons.Item(0).Visible = False
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged
        lkpGrupo.EditValue = Nothing
        lkpGrupo_LoadData(True)

    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        Response = oGruposArticulos.Lookup(Me.Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.SelectAll = True
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpgrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged
        If EntroDespliegaArticulo = True Then Exit Sub
        lkpArticulo.EditValue = Nothing
        lkpArticulo_LoadData(True)

    End Sub

    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events
        Response = oArticulos.Lookup(Me.Departamento, Me.Grupo)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        Dim oEvents As New Events


        If Me.lkpArticulo.DataSource Is Nothing Or Me.lkpArticulo.EditValue Is Nothing Or Not IsNumeric(Me.lkpArticulo.EditValue) Then
            DespliegaDatosGeneralesArticulo(True)
        Else
            DespliegaDatosGeneralesArticulo(False)
        End If


        Me.DespliegaEstadisticasDeArticulo(oEvents)
        If oEvents.ErrorFound Then
            oEvents.ShowError()
            Exit Sub
        End If

        Me.DespliegaExistencias(Articulo, oEvents)
        If oEvents.ErrorFound Then
            Me.btnVerDocumentos.Enabled = False
            oEvents.ShowError()
            Exit Sub
        End If

        Me.DespliegaMovimientosDeArticulo(oEvents)
        If oEvents.ErrorFound Then
            Me.cmdVerMovimiento.Enabled = False
            oEvents.ShowError()
            Exit Sub
        End If

        Me.btnVerDocumentos.Enabled = True
        Me.cmdVerMovimiento.Enabled = True
    End Sub

    Private Sub dteFecha_Ini_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha_Ini.EditValueChanged
        If Me.dteFecha_Ini.IsLoading Then Exit Sub
        If Not IsDate(Me.dteFecha_Ini.Text) Then Exit Sub

        Dim oevents As New Events
        Me.DespliegaMovimientosDeArticulo(oevents)
        If oevents.ErrorFound Then
            oevents.ShowError()
            Exit Sub
        End If

    End Sub
    Private Sub dteFecha_Fin_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha_Fin.EditValueChanged
        If Me.dteFecha_Fin.IsLoading Then Exit Sub
        If Not IsDate(Me.dteFecha_Fin.Text) Then Exit Sub

        Dim oevents As New Events
        Me.DespliegaMovimientosDeArticulo(oevents)
        If oevents.ErrorFound Then
            oevents.ShowError()
            Exit Sub
        End If

    End Sub
    Private Sub btnVerDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerDocumentos.Click
        Dim response As New Events
        Dim oData As DataSet

        If bodega_seleccionada <> "" And columna_seleccionada <> "" Then
            Dim frmModal As New Comunes.frmArticulosExistencias
            With frmModal
                .lblNomBodega.Text = n_bodega_seleccionada
                .lblNomArticulo.Text = Me.lblDescripcionCorta.Text
                .OwnerForm = Me
                .MdiParent = Me.MdiParent
                Select Case columna_seleccionada
                    Case "grcPorSurtir"
                        .Text = "Por Surtir"
                        response = oArticulosExistencias.ArticulosExistencias_PorSurtir(bodega_seleccionada, Articulo)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvPorSurtir
                            .grExistencias.DataSource = oData.Tables(0)
                        End If
                    Case "grcTransito"
                        .Text = "Transito"
                        response = oArticulosExistencias.ArticulosExistencias_Traspasos(bodega_seleccionada, Articulo)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvTraspasos
                            .grExistencias.DataSource = oData.Tables(0)
                        End If


                    Case "grcEntregar"
                        .Text = "Entregar"
                        response = oArticulosExistencias.ArticulosExistencias_PorEntregar(bodega_seleccionada, Articulo, False)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvPorEntregar
                            .grExistencias.DataSource = oData.Tables(0)
                        End If

                    Case "grcEntregarPedido"
                        .Text = "Entregar P�dido F�brica"
                        response = oArticulosExistencias.ArticulosExistencias_PorEntregar(bodega_seleccionada, Articulo, True)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvPorEntregar
                            .grExistencias.DataSource = oData.Tables(0)
                        End If

                    Case "grcReparto"
                        .Text = "Reparto"
                        response = oArticulosExistencias.ArticulosExistencias_Repartiendose(bodega_seleccionada, Articulo)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvRepartiendose
                            .grExistencias.DataSource = oData.Tables(0)
                        End If
                    Case "grcGarantia"
                        .Text = "Garantia"
                        response = oArticulosExistencias.ArticulosExistencias_Garantias(bodega_seleccionada, Articulo)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvGarantias
                            .grExistencias.DataSource = oData.Tables(0)
                        End If

                    Case "grcVistas"
                        .Text = "Vistas"
                        response = oArticulosExistencias.ArticulosExistencias_Vistas(bodega_seleccionada, Articulo)
                        If Not response.ErrorFound Then
                            oData = CType(response.Value, DataSet)
                            .grExistencias.MainView = .grvVistas
                            .grExistencias.DataSource = oData.Tables(0)
                        End If
                    Case Else
                        Exit Sub
                End Select
                .Show()
                Me.Enabled = False
            End With
        End If

    End Sub
    Private Sub grvExistencias_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grvExistencias.Click
        bodega_seleccionada = Me.grvExistencias.GetRowCellValue(Me.grvExistencias.FocusedRowHandle, Me.grcBodegaExistencias)
        n_bodega_seleccionada = Me.grvExistencias.GetRowCellValue(Me.grvExistencias.FocusedRowHandle, Me.grcNombrebodega)
        columna_seleccionada = Me.grvExistencias.FocusedColumn.Name
    End Sub
    Private Sub grMovimientos_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grMovimientos.DoubleClick
        Me.cmdVerMovimiento_Click(sender, e)
    End Sub
    Private Sub grvMovimientos_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvMovimientos.FocusedRowChanged
        If CType(Me.grMovimientos.DataSource, DataTable).Rows.Count < 1 Then Exit Sub

        bodega_movimiento_seleccionada = CStr(CType(Me.grMovimientos.DataSource, DataTable).Rows(Me.grvMovimientos.FocusedRowHandle).Item("bodega"))
        concepto_movimiento_seleccionada = CStr(CType(Me.grMovimientos.DataSource, DataTable).Rows(Me.grvMovimientos.FocusedRowHandle).Item("concepto"))
        folio_movimiento_seleccionada = CLng(CType(Me.grMovimientos.DataSource, DataTable).Rows(Me.grvMovimientos.FocusedRowHandle).Item("folio"))
        fecha_movimiento_seleccionada = CDate(CType(Me.grMovimientos.DataSource, DataTable).Rows(Me.grvMovimientos.FocusedRowHandle).Item("fecha"))
        fila_movimiento_seleccionada = e.FocusedRowHandle
    End Sub
    Private Sub cmdVerMovimiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If CType(Me.grMovimientos.DataSource, DataTable).Rows.Count < 1 Then Exit Sub
        Dim response As New Events
        Dim oData As DataSet

        If bodega_movimiento_seleccionada.Length > 0 And concepto_movimiento_seleccionada.Length > 0 And folio_movimiento_seleccionada > 0 Then
            Try

                Dim frmModal As New frmMovimientosArticulosDetalle
                With frmModal
                    .OwnerForm = Me
                    .MdiParent = Me.MdiParent
                    .Text = "Movimiento al Inventario"

                    response = oMovimientosInventariosDetalle.Listado(folio_movimiento_seleccionada, bodega_movimiento_seleccionada, concepto_movimiento_seleccionada)
                    If Not response.ErrorFound Then
                        .Show()
                        Me.Enabled = False

                        oData = CType(response.Value, DataSet)
                        .lkpBodega.EditValue = bodega_movimiento_seleccionada
                        .lkpConcepto.EditValue = concepto_movimiento_seleccionada
                        .clcFolio.EditValue = folio_movimiento_seleccionada
                        .dteFecha.DateTime = fecha_movimiento_seleccionada

                        .txtObservaciones.Text = CStr(CType(Me.grMovimientos.DataSource, DataTable).Rows(fila_movimiento_seleccionada).Item("observaciones"))
                        .grMovimientosInventarios.DataSource = oData.Tables(0)
                    Else
                        response.ShowError()
                    End If
                End With
            Catch ex As Exception
            End Try

        End If

    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub DespliegaDatosGeneralesArticulo(ByVal articulo_is_nothing As Boolean)
        If articulo_is_nothing = False Then
            EntroDespliegaArticulo = True
            Me.lblDescripcionCorta.Text = CStr(lkpArticulo.GetValue("descripcion_corta"))
            Me.txtUnidad.Text = CStr(lkpArticulo.GetValue("nombre_unidad"))
            Me.clcPrecioLista.EditValue = CDbl(lkpArticulo.GetValue("precio_lista"))
            Me.clcUltimoCosto.EditValue = CDbl(lkpArticulo.GetValue("ultimo_costo"))
            Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            Me.lblProveedor.Text = CStr(lkpArticulo.GetValue("nombre_proveedor"))
            Me.btnFotografia.Enabled = True
            EntroDespliegaArticulo = False
        Else
            Me.lblDescripcionCorta.Text = ""
            Me.lblProveedor.Text = ""
            Me.txtUnidad.Text = ""
            Me.clcPrecioLista.EditValue = 0
            Me.clcUltimoCosto.EditValue = 0
            Me.btnFotografia.Enabled = False
        End If
    
    End Sub
    Private Sub DespliegaExistencias(ByVal articulo As String, ByRef response As Events)
        Dim odataset As DataSet

        ' Despliega Datos de Bodegas existencias
        response = oArticulosExistencias.Listado(articulo, False)
        If Not response.ErrorFound Then
            odataset = response.Value
            Me.grExistencias.DataSource = odataset.Tables(0)
            Me.grExistenciasSucursales.DataSource = odataset.Tables(1)
        End If
        odataset = Nothing
    End Sub
    Private Sub DespliegaEstadisticasDeArticulo(ByRef Response As Events)
        Try
            Response = oArticulos.EstadisticasDeArticulo(Articulo)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.DataSource = oDataSet.Tables(0)
                With oDataSet.Tables(0)
                    Me.clcCantidadEntradas.EditValue = .Rows(0).Item("cantidad_entradas")
                    Me.clcCantidadSalidas.EditValue = .Rows(0).Item("cantidad_salidas")
                    Me.clcFechaEntrada.EditValue = .Rows(0).Item("ultima_fecha_entrada")
                    Me.clcFechaSalida.EditValue = .Rows(0).Item("ultima_fecha_salida")
                    Me.clcUltimoPrecio.EditValue = .Rows(0).Item("ultimo_precio")
                    Me.clcValorEntrada.EditValue = .Rows(0).Item("valor_entradas")
                    Me.clcValorSalida.EditValue = .Rows(0).Item("valor_salidas")
                    Me.clcPrecioContado.EditValue = .Rows(0).Item("precio_contado")

                End With

                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub DespliegaMovimientosDeArticulo(ByRef Response As Events)
        Try
            Response = oArticulos.MovimientosDeArticuloPorFechas(Articulo, Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grMovimientos.DataSource = oDataSet.Tables(0)

                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try
    End Sub

#End Region

 
    Private Sub btnFotografia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFotografia.Click
        CargarFotografiaArticulo(Me.Articulo)
    End Sub

    Private Sub CargarFotografiaArticulo(ByVal Articulo As Long)
        Dim oform As New frmArticulosFoto
        oform.ModificarImagen = True
        oform.Articulo = Articulo
        oform.Title = "Foto del Articulo"
        oform.ShowDialog(Me.MdiParent)
    End Sub

End Class
