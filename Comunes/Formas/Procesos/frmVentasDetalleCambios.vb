Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Imports System.Windows.Forms
Public Class frmVentasDetalleCambios
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblDescripcionCorta As System.Windows.Forms.Label
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNArticulo As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpGrupoDestino As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpDepartamentoDestino As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpArticuloDestino As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblNArticuloDestino As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lkpBodegaDestino As Dipros.Editors.TINMultiLookup
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVentasDetalleCambios))
        Me.lblDescripcionCorta = New System.Windows.Forms.Label
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.lkpGrupoDestino = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpDepartamentoDestino = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lkpArticuloDestino = New Dipros.Editors.TINMultiLookup
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtNArticulo = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblNArticuloDestino = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label7 = New System.Windows.Forms.Label
        Me.lkpBodegaDestino = New Dipros.Editors.TINMultiLookup
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(895, 28)
        '
        'lblDescripcionCorta
        '
        Me.lblDescripcionCorta.Enabled = False
        Me.lblDescripcionCorta.Location = New System.Drawing.Point(256, 72)
        Me.lblDescripcionCorta.Name = "lblDescripcionCorta"
        Me.lblDescripcionCorta.Size = New System.Drawing.Size(128, 20)
        Me.lblDescripcionCorta.TabIndex = 6
        Me.lblDescripcionCorta.Tag = ""
        Me.lblDescripcionCorta.Text = "descripcion_corta"
        Me.lblDescripcionCorta.Visible = False
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = True
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(128, 48)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(280, 20)
        Me.lkpGrupo.TabIndex = 3
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(80, 48)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 2
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = True
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(128, 24)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(280, 20)
        Me.lkpDepartamento.TabIndex = 1
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(32, 24)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 0
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(72, 72)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 4
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Art�c&ulo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = True
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(128, 72)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(400, Long)
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(108, 20)
        Me.lkpArticulo.TabIndex = 5
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'lkpGrupoDestino
        '
        Me.lkpGrupoDestino.AllowAdd = False
        Me.lkpGrupoDestino.AutoReaload = True
        Me.lkpGrupoDestino.DataSource = Nothing
        Me.lkpGrupoDestino.DefaultSearchField = ""
        Me.lkpGrupoDestino.DisplayMember = "descripcion"
        Me.lkpGrupoDestino.EditValue = Nothing
        Me.lkpGrupoDestino.Filtered = False
        Me.lkpGrupoDestino.InitValue = Nothing
        Me.lkpGrupoDestino.Location = New System.Drawing.Point(112, 47)
        Me.lkpGrupoDestino.MultiSelect = False
        Me.lkpGrupoDestino.Name = "lkpGrupoDestino"
        Me.lkpGrupoDestino.NullText = ""
        Me.lkpGrupoDestino.PopupWidth = CType(470, Long)
        Me.lkpGrupoDestino.Required = False
        Me.lkpGrupoDestino.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupoDestino.SearchMember = ""
        Me.lkpGrupoDestino.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupoDestino.SelectAll = False
        Me.lkpGrupoDestino.Size = New System.Drawing.Size(280, 20)
        Me.lkpGrupoDestino.TabIndex = 3
        Me.lkpGrupoDestino.Tag = "grupodestino"
        Me.lkpGrupoDestino.ToolTip = "grupo"
        Me.lkpGrupoDestino.ValueMember = "grupo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(64, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Grupo:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamentoDestino
        '
        Me.lkpDepartamentoDestino.AllowAdd = False
        Me.lkpDepartamentoDestino.AutoReaload = True
        Me.lkpDepartamentoDestino.DataSource = Nothing
        Me.lkpDepartamentoDestino.DefaultSearchField = ""
        Me.lkpDepartamentoDestino.DisplayMember = "nombre"
        Me.lkpDepartamentoDestino.EditValue = Nothing
        Me.lkpDepartamentoDestino.Filtered = False
        Me.lkpDepartamentoDestino.InitValue = Nothing
        Me.lkpDepartamentoDestino.Location = New System.Drawing.Point(112, 24)
        Me.lkpDepartamentoDestino.MultiSelect = False
        Me.lkpDepartamentoDestino.Name = "lkpDepartamentoDestino"
        Me.lkpDepartamentoDestino.NullText = ""
        Me.lkpDepartamentoDestino.PopupWidth = CType(470, Long)
        Me.lkpDepartamentoDestino.Required = False
        Me.lkpDepartamentoDestino.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamentoDestino.SearchMember = ""
        Me.lkpDepartamentoDestino.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamentoDestino.SelectAll = False
        Me.lkpDepartamentoDestino.Size = New System.Drawing.Size(280, 20)
        Me.lkpDepartamentoDestino.TabIndex = 1
        Me.lkpDepartamentoDestino.Tag = "departamentodestino"
        Me.lkpDepartamentoDestino.ToolTip = "departamento"
        Me.lkpDepartamentoDestino.ValueMember = "departamento"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Departamento:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(56, 70)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Tag = ""
        Me.Label4.Text = "Art�c&ulo:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticuloDestino
        '
        Me.lkpArticuloDestino.AllowAdd = False
        Me.lkpArticuloDestino.AutoReaload = True
        Me.lkpArticuloDestino.DataSource = Nothing
        Me.lkpArticuloDestino.DefaultSearchField = "modelo"
        Me.lkpArticuloDestino.DisplayMember = "modelo"
        Me.lkpArticuloDestino.EditValue = Nothing
        Me.lkpArticuloDestino.Filtered = False
        Me.lkpArticuloDestino.InitValue = Nothing
        Me.lkpArticuloDestino.Location = New System.Drawing.Point(112, 70)
        Me.lkpArticuloDestino.MultiSelect = False
        Me.lkpArticuloDestino.Name = "lkpArticuloDestino"
        Me.lkpArticuloDestino.NullText = ""
        Me.lkpArticuloDestino.PopupWidth = CType(400, Long)
        Me.lkpArticuloDestino.Required = False
        Me.lkpArticuloDestino.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticuloDestino.SearchMember = ""
        Me.lkpArticuloDestino.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticuloDestino.SelectAll = False
        Me.lkpArticuloDestino.Size = New System.Drawing.Size(108, 20)
        Me.lkpArticuloDestino.TabIndex = 5
        Me.lkpArticuloDestino.Tag = "articulo_destino"
        Me.lkpArticuloDestino.ToolTip = Nothing
        Me.lkpArticuloDestino.ValueMember = "Articulo"
        '
        'Label5
        '
        Me.Label5.Enabled = False
        Me.Label5.Location = New System.Drawing.Point(240, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 20)
        Me.Label5.TabIndex = 6
        Me.Label5.Tag = ""
        Me.Label5.Text = "descripcion_corta"
        Me.Label5.Visible = False
        '
        'txtNArticulo
        '
        Me.txtNArticulo.Location = New System.Drawing.Point(128, 96)
        Me.txtNArticulo.Name = "txtNArticulo"
        Me.txtNArticulo.Size = New System.Drawing.Size(280, 16)
        Me.txtNArticulo.TabIndex = 8
        Me.txtNArticulo.Tag = "n_articulo"
        Me.txtNArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(48, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 16)
        Me.Label6.TabIndex = 7
        Me.Label6.Tag = ""
        Me.Label6.Text = "&Descripci�n:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNArticuloDestino
        '
        Me.lblNArticuloDestino.Location = New System.Drawing.Point(112, 93)
        Me.lblNArticuloDestino.Name = "lblNArticuloDestino"
        Me.lblNArticuloDestino.Size = New System.Drawing.Size(280, 16)
        Me.lblNArticuloDestino.TabIndex = 8
        Me.lblNArticuloDestino.Tag = ""
        Me.lblNArticuloDestino.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(32, 93)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 16)
        Me.Label8.TabIndex = 7
        Me.Label8.Tag = ""
        Me.Label8.Text = "&Descripci�n:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Enabled = False
        Me.lblBodega.Location = New System.Drawing.Point(72, 112)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 9
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Enabled = False
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(128, 112)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodega.TabIndex = 10
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Enabled = False
        Me.Label7.Location = New System.Drawing.Point(56, 115)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 16)
        Me.Label7.TabIndex = 10
        Me.Label7.Tag = ""
        Me.Label7.Text = "&Bodega:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodegaDestino
        '
        Me.lkpBodegaDestino.AllowAdd = False
        Me.lkpBodegaDestino.AutoReaload = False
        Me.lkpBodegaDestino.DataSource = Nothing
        Me.lkpBodegaDestino.DefaultSearchField = ""
        Me.lkpBodegaDestino.DisplayMember = "descripcion"
        Me.lkpBodegaDestino.EditValue = Nothing
        Me.lkpBodegaDestino.Filtered = False
        Me.lkpBodegaDestino.InitValue = Nothing
        Me.lkpBodegaDestino.Location = New System.Drawing.Point(112, 112)
        Me.lkpBodegaDestino.MultiSelect = False
        Me.lkpBodegaDestino.Name = "lkpBodegaDestino"
        Me.lkpBodegaDestino.NullText = ""
        Me.lkpBodegaDestino.PopupWidth = CType(400, Long)
        Me.lkpBodegaDestino.Required = False
        Me.lkpBodegaDestino.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaDestino.SearchMember = ""
        Me.lkpBodegaDestino.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaDestino.SelectAll = False
        Me.lkpBodegaDestino.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodegaDestino.TabIndex = 0
        Me.lkpBodegaDestino.Tag = "bodega_destino"
        Me.lkpBodegaDestino.ToolTip = Nothing
        Me.lkpBodegaDestino.ValueMember = "Bodega"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lkpGrupo)
        Me.GroupBox1.Controls.Add(Me.lblGrupo)
        Me.GroupBox1.Controls.Add(Me.lblArticulo)
        Me.GroupBox1.Controls.Add(Me.txtNArticulo)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.lkpDepartamento)
        Me.GroupBox1.Controls.Add(Me.lblBodega)
        Me.GroupBox1.Controls.Add(Me.lblDepartamento)
        Me.GroupBox1.Controls.Add(Me.lkpArticulo)
        Me.GroupBox1.Controls.Add(Me.lkpBodega)
        Me.GroupBox1.Controls.Add(Me.lblDescripcionCorta)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(432, 152)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Origen"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblNArticuloDestino)
        Me.GroupBox2.Controls.Add(Me.lkpDepartamentoDestino)
        Me.GroupBox2.Controls.Add(Me.lkpArticuloDestino)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.lkpBodegaDestino)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.lkpGrupoDestino)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 207)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(432, 152)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Destino"
        '
        'GroupBox3
        '
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox3.Location = New System.Drawing.Point(8, 192)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(432, 8)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        '
        'frmVentasDetalleCambios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(450, 368)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmVentasDetalleCambios"
        Me.Text = "frmVentasDetalleCambios"
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Dipros Systems, Declaraciones"

    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oventascambios As New VillarrealBusiness.clsVentasCambios

    Private articulo_temporal As Long = 0

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property DepartamentoDestino() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamentoDestino)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property GrupoDestino() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupoDestino)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
    Private ReadOnly Property ArticuloDestino() As Long
        Get
            Return PreparaValorLookup(Me.lkpArticuloDestino)
        End Get
    End Property
    Private ReadOnly Property Bodega() As String
        Get
            Return PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property
    Private ReadOnly Property BodegaDestino() As String
        Get
            Return PreparaValorLookupStr(Me.lkpBodegaDestino)
        End Get
    End Property
#End Region

#Region "Dipros Systems, Eventos de la Forma"

    Private Sub frmVentasDetalleCambios_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        'DEPARTAMENTO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("departamento")) Then
            Me.lkpDepartamento.EditValue = -1
        Else
            Me.lkpDepartamento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("departamento")), Long)
        End If

        'GRUPO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("grupo")) Then
            Me.lkpGrupo.EditValue = -1
        Else
            Me.lkpGrupo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("grupo")), Long)
        End If

        'ARTICULO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("articulo")) Then
            Me.lkpArticulo.EditValue = -1
        Else
            Me.lkpArticulo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("articulo")), Long)
            If Me.lkpArticulo.EditValue > 0 Then
                If Departamento <= 0 Or Grupo <= 0 Then
                    Actualiza_Lookups()
                End If
            End If
        End If

        'Me.lkpDepartamentoDestino.EditValue = Me.lkpDepartamento.EditValue
        'Me.lkpGrupoDestino.EditValue = Me.lkpGrupo.EditValue
        'Me.lkpBodegaDestino.EditValue = Me.lkpBodega.EditValue

    End Sub
    Private Sub frmVentasDetalleCambios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.lkpDepartamento.Enabled = False
        Me.lkpGrupo.Enabled = False
        Me.lkpArticulo.Enabled = False
    End Sub
    Private Sub frmVentasDetalleCambios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oventascambios.Validacion(Me.ArticuloDestino, BodegaDestino)
    End Sub
    Private Sub frmVentasDetalleCambios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl

            .UpdateRow(Me.DataSource)
        End With
    End Sub
#End Region

#Region "Dipros Systems, Eventos de los Controles"

#Region "Origen"

    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")
        Me.lkpGrupo.EditValue = Nothing

    End Sub

    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpGrupo.Text = Me.lkpGrupo.GetValue("descripcion")
        Me.lkpArticulo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData

        Dim Response As New Events
        Response = oArticulos.Lookup(Me.lkpDepartamento.EditValue, Me.lkpGrupo.EditValue)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        If Articulo <> -1 Then
            If articulo_temporal <> Articulo Then articulo_temporal = Articulo
            Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            Me.lkpArticulo.EditValue = Articulo
            Me.txtNArticulo.Text = Me.lkpArticulo.GetValue("descripcion_corta")
        End If
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events
        response = oBodegas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

#End Region

#Region "Destino"


    Private Sub lkpDepartamentoDestino_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamentoDestino.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamentoDestino.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpDepartamentoDestino_Format() Handles lkpDepartamentoDestino.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamentoDestino)
    End Sub
    Private Sub lkpDepartamentoDestino_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamentoDestino.EditValueChanged
        Me.lkpDepartamentoDestino.Text = Me.lkpDepartamentoDestino.GetValue("nombre")
        Me.lkpGrupoDestino.EditValue = Nothing
    End Sub

    Private Sub lkpGrupoDestino_LoadData(ByVal Initialize As Boolean) Handles lkpGrupoDestino.LoadData
        Dim Response As New Events
        Response = oGrupos.Lookup(DepartamentoDestino)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupoDestino.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpGrupoDestino_Format() Handles lkpGrupoDestino.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupoDestino)
    End Sub
    Private Sub lkpGrupoDestino_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupoDestino.EditValueChanged
        Me.lkpGrupoDestino.Text = Me.lkpGrupoDestino.GetValue("descripcion")
        Me.lkpArticuloDestino.EditValue = Nothing

    End Sub

    Private Sub lkpArticuloDestino_LoadData(ByVal Initialize As Boolean) Handles lkpArticuloDestino.LoadData
        Dim Response As New Events
        'Response = oArticulos.Lookup(Departamento, Grupo)
        Response = oArticulos.LookupArticulosLigeroUso(DepartamentoDestino, GrupoDestino, True)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticuloDestino.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpArticuloDestino_Format() Handles lkpArticuloDestino.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticuloDestino)
    End Sub
    Private Sub lkpArticuloDestino_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticuloDestino.EditValueChanged
        If ArticuloDestino <> -1 Then
            Me.lblNArticuloDestino.Text = Me.lkpArticuloDestino.GetValue("descripcion_corta")
        End If
    End Sub

    Private Sub lkpBodegaDestino_Format() Handles lkpBodegaDestino.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodegaDestino)
    End Sub
    Private Sub lkpBodegaDestino_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaDestino.LoadData
        Dim response As Events
        response = oBodegas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodegaDestino.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub

#End Region

#End Region

#Region "Dipros Systems, Funcionalidad"
    Private Sub Actualiza_Lookups()

        Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        Me.lkpDepartamento_LoadData(True)
        If Me.lkpDepartamento.EditValue > 0 Then

            Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")

        End If

        Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")

    End Sub

#End Region


  


End Class
