Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmVentasEjecutivo
    Inherits Dipros.Windows.frmTINForm


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grVentas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvVentas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSobrePedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSobrePedido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcReparto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkReparto As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSurtido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSurtido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcPrecioMinimo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcRegalo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcContado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioAutorizacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPartidaVistas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaVentas As Dipros.Windows.TINMaster
    Public WithEvents lblSerieSucursal As System.Windows.Forms.Label
    Public WithEvents lblSerie As System.Windows.Forms.Label
    Public WithEvents lblFolio As System.Windows.Forms.Label
    Public WithEvents lbl_eFolio As System.Windows.Forms.Label
    Public WithEvents Label12 As System.Windows.Forms.Label
    Public WithEvents lblTipoventa As System.Windows.Forms.Label
    Public WithEvents lblVendedor As System.Windows.Forms.Label
    Public WithEvents lblCliente As System.Windows.Forms.Label
    Public WithEvents lbl_eFecha As System.Windows.Forms.Label
    Friend WithEvents lblMenosSoloDespliegue As System.Windows.Forms.Label
    Friend WithEvents lblTotalContado As System.Windows.Forms.Label
    Friend WithEvents lblSubtotal As System.Windows.Forms.Label
    Public WithEvents lblFechaValor As System.Windows.Forms.Label
    Public WithEvents lblVendedorValue As System.Windows.Forms.Label
    Public WithEvents lblClienteValor As System.Windows.Forms.Label
    Public WithEvents lblTipoVentaValor As System.Windows.Forms.Label
    Friend WithEvents lblTotalDocumentos As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblDocumentosUltimoDocumento As System.Windows.Forms.Label
    Friend WithEvents lblImporteUltimoDocumento As System.Windows.Forms.Label
    Friend WithEvents lblLeyendaDocumentoFinal As System.Windows.Forms.Label
    Friend WithEvents lblDocumentoFinal As System.Windows.Forms.Label
    Friend WithEvents lblLeyendaDocumentos As System.Windows.Forms.Label
    Friend WithEvents lblDocumentosIguales As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblInteres As System.Windows.Forms.Label
    Friend WithEvents chkLiquida_Vencimiento As DevExpress.XtraEditors.CheckEdit
    Public WithEvents Label2 As System.Windows.Forms.Label
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents Label4 As System.Windows.Forms.Label
    Public WithEvents Label5 As System.Windows.Forms.Label
    Public WithEvents Label6 As System.Windows.Forms.Label
    Public WithEvents Label8 As System.Windows.Forms.Label
    Public WithEvents Label10 As System.Windows.Forms.Label
    Public WithEvents Label11 As System.Windows.Forms.Label
    Public WithEvents lblPlanCredito As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVentasEjecutivo))
        Me.grVentas = New DevExpress.XtraGrid.GridControl
        Me.grvVentas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSobrePedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSobrePedido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcReparto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkReparto = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSurtido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSurtido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPrecioMinimo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcRegalo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcContado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioAutorizacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPartidaVistas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaVentas = New Dipros.Windows.TINMaster
        Me.lblSerieSucursal = New System.Windows.Forms.Label
        Me.lblSerie = New System.Windows.Forms.Label
        Me.lblFolio = New System.Windows.Forms.Label
        Me.lbl_eFolio = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lbl_eFecha = New System.Windows.Forms.Label
        Me.lblMenosSoloDespliegue = New System.Windows.Forms.Label
        Me.lblTotalContado = New System.Windows.Forms.Label
        Me.lblSubtotal = New System.Windows.Forms.Label
        Me.lblFechaValor = New System.Windows.Forms.Label
        Me.lblVendedorValue = New System.Windows.Forms.Label
        Me.lblClienteValor = New System.Windows.Forms.Label
        Me.lblTipoVentaValor = New System.Windows.Forms.Label
        Me.lblTotalDocumentos = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.lblDocumentosUltimoDocumento = New System.Windows.Forms.Label
        Me.lblImporteUltimoDocumento = New System.Windows.Forms.Label
        Me.lblLeyendaDocumentoFinal = New System.Windows.Forms.Label
        Me.lblDocumentoFinal = New System.Windows.Forms.Label
        Me.lblLeyendaDocumentos = New System.Windows.Forms.Label
        Me.lblDocumentosIguales = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblInteres = New System.Windows.Forms.Label
        Me.chkLiquida_Vencimiento = New DevExpress.XtraEditors.CheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblPlanCredito = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        CType(Me.grVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLiquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1628, 28)
        '
        'grVentas
        '
        '
        'grVentas.EmbeddedNavigator
        '
        Me.grVentas.EmbeddedNavigator.Name = ""
        Me.grVentas.Location = New System.Drawing.Point(8, 192)
        Me.grVentas.MainView = Me.grvVentas
        Me.grVentas.Name = "grVentas"
        Me.grVentas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.grchkSobrePedido, Me.grchkReparto, Me.grchkSurtido})
        Me.grVentas.Size = New System.Drawing.Size(728, 120)
        Me.grVentas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grVentas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grVentas.TabIndex = 63
        Me.grVentas.TabStop = False
        Me.grVentas.Text = "Ventas"
        '
        'grvVentas
        '
        Me.grvVentas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPartida, Me.grcNArticulo, Me.grcArticulo, Me.grcNBodega, Me.grcBodega, Me.grcSobrePedido, Me.grcReparto, Me.grcCantidad, Me.grcPrecioUnitario, Me.grcTotal, Me.grcCosto, Me.grcSurtido, Me.grcPrecioMinimo, Me.grcRegalo, Me.grcContado, Me.grcModelo, Me.grcFolioAutorizacion, Me.grcPartidaVistas, Me.grcNumeroSerie})
        Me.grvVentas.GridControl = Me.grVentas
        Me.grvVentas.Name = "grvVentas"
        Me.grvVentas.OptionsCustomization.AllowFilter = False
        Me.grvVentas.OptionsCustomization.AllowGroup = False
        Me.grvVentas.OptionsCustomization.AllowSort = False
        Me.grvVentas.OptionsView.ShowGroupPanel = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "Partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.Width = 52
        '
        'grcNArticulo
        '
        Me.grcNArticulo.Caption = "Art�culo"
        Me.grcNArticulo.FieldName = "n_articulo"
        Me.grcNArticulo.Name = "grcNArticulo"
        Me.grcNArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNArticulo.VisibleIndex = 1
        Me.grcNArticulo.Width = 167
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNBodega
        '
        Me.grcNBodega.Caption = "Bodega"
        Me.grcNBodega.FieldName = "n_bodega"
        Me.grcNBodega.Name = "grcNBodega"
        Me.grcNBodega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNBodega.VisibleIndex = 2
        Me.grcNBodega.Width = 80
        '
        'grcBodega
        '
        Me.grcBodega.Caption = "Bodega"
        Me.grcBodega.FieldName = "bodega"
        Me.grcBodega.Name = "grcBodega"
        Me.grcBodega.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSobrePedido
        '
        Me.grcSobrePedido.Caption = "Ped. a F�brica"
        Me.grcSobrePedido.ColumnEdit = Me.grchkSobrePedido
        Me.grcSobrePedido.FieldName = "sobrepedido"
        Me.grcSobrePedido.Name = "grcSobrePedido"
        Me.grcSobrePedido.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSobrePedido.VisibleIndex = 4
        Me.grcSobrePedido.Width = 83
        '
        'grchkSobrePedido
        '
        Me.grchkSobrePedido.AutoHeight = False
        Me.grchkSobrePedido.Name = "grchkSobrePedido"
        Me.grchkSobrePedido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcReparto
        '
        Me.grcReparto.Caption = "Reparto"
        Me.grcReparto.ColumnEdit = Me.grchkReparto
        Me.grcReparto.FieldName = "reparto"
        Me.grcReparto.Name = "grcReparto"
        Me.grcReparto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcReparto.Width = 68
        '
        'grchkReparto
        '
        Me.grchkReparto.AutoHeight = False
        Me.grchkReparto.Name = "grchkReparto"
        Me.grchkReparto.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 5
        Me.grcCantidad.Width = 55
        '
        'grcPrecioUnitario
        '
        Me.grcPrecioUnitario.Caption = "Precio"
        Me.grcPrecioUnitario.DisplayFormat.FormatString = "c2"
        Me.grcPrecioUnitario.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioUnitario.FieldName = "preciounitario"
        Me.grcPrecioUnitario.Name = "grcPrecioUnitario"
        Me.grcPrecioUnitario.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioUnitario.VisibleIndex = 6
        Me.grcPrecioUnitario.Width = 63
        '
        'grcTotal
        '
        Me.grcTotal.Caption = "Total"
        Me.grcTotal.DisplayFormat.FormatString = "c2"
        Me.grcTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcTotal.FieldName = "total"
        Me.grcTotal.Name = "grcTotal"
        Me.grcTotal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTotal.VisibleIndex = 8
        Me.grcTotal.Width = 98
        '
        'grcCosto
        '
        Me.grcCosto.Caption = "Costo"
        Me.grcCosto.FieldName = "costo"
        Me.grcCosto.Name = "grcCosto"
        Me.grcCosto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSurtido
        '
        Me.grcSurtido.Caption = "Surtido"
        Me.grcSurtido.ColumnEdit = Me.grchkSurtido
        Me.grcSurtido.FieldName = "surtido"
        Me.grcSurtido.Name = "grcSurtido"
        Me.grcSurtido.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grchkSurtido
        '
        Me.grchkSurtido.AutoHeight = False
        Me.grchkSurtido.Name = "grchkSurtido"
        Me.grchkSurtido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grcPrecioMinimo
        '
        Me.grcPrecioMinimo.Caption = "Precio Minimo"
        Me.grcPrecioMinimo.FieldName = "precio_minimo"
        Me.grcPrecioMinimo.Name = "grcPrecioMinimo"
        Me.grcPrecioMinimo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcRegalo
        '
        Me.grcRegalo.Caption = "Regalo"
        Me.grcRegalo.ColumnEdit = Me.grchkSurtido
        Me.grcRegalo.FieldName = "articulo_regalo"
        Me.grcRegalo.Name = "grcRegalo"
        Me.grcRegalo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcRegalo.VisibleIndex = 3
        Me.grcRegalo.Width = 50
        '
        'grcContado
        '
        Me.grcContado.Caption = "P. Contado"
        Me.grcContado.DisplayFormat.FormatString = "c2"
        Me.grcContado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcContado.FieldName = "precio_contado"
        Me.grcContado.Name = "grcContado"
        Me.grcContado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcContado.VisibleIndex = 7
        Me.grcContado.Width = 72
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 0
        Me.grcModelo.Width = 78
        '
        'grcFolioAutorizacion
        '
        Me.grcFolioAutorizacion.Caption = "Folio Aut."
        Me.grcFolioAutorizacion.FieldName = "folio_autorizacion"
        Me.grcFolioAutorizacion.Name = "grcFolioAutorizacion"
        Me.grcFolioAutorizacion.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcPartidaVistas
        '
        Me.grcPartidaVistas.Caption = "Partida Vistas"
        Me.grcPartidaVistas.FieldName = "partida_vistas"
        Me.grcPartidaVistas.Name = "grcPartidaVistas"
        Me.grcPartidaVistas.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNumeroSerie
        '
        Me.grcNumeroSerie.Caption = "N�mero Serie"
        Me.grcNumeroSerie.FieldName = "numero_serie"
        Me.grcNumeroSerie.Name = "grcNumeroSerie"
        Me.grcNumeroSerie.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'tmaVentas
        '
        Me.tmaVentas.BackColor = System.Drawing.Color.White
        Me.tmaVentas.CanDelete = True
        Me.tmaVentas.CanInsert = True
        Me.tmaVentas.CanUpdate = True
        Me.tmaVentas.Enabled = False
        Me.tmaVentas.Grid = Me.grVentas
        Me.tmaVentas.Location = New System.Drawing.Point(8, 200)
        Me.tmaVentas.Name = "tmaVentas"
        Me.tmaVentas.Size = New System.Drawing.Size(712, 23)
        Me.tmaVentas.TabIndex = 62
        Me.tmaVentas.TabStop = False
        Me.tmaVentas.Title = "Art�culos"
        Me.tmaVentas.UpdateTitle = "un Art�culo"
        Me.tmaVentas.Visible = False
        '
        'lblSerieSucursal
        '
        Me.lblSerieSucursal.AutoSize = True
        Me.lblSerieSucursal.Location = New System.Drawing.Point(256, 32)
        Me.lblSerieSucursal.Name = "lblSerieSucursal"
        Me.lblSerieSucursal.Size = New System.Drawing.Size(31, 16)
        Me.lblSerieSucursal.TabIndex = 77
        Me.lblSerieSucursal.Tag = "serie"
        Me.lblSerieSucursal.Text = "serie"
        Me.lblSerieSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(208, 32)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie.TabIndex = 76
        Me.lblSerie.Tag = ""
        Me.lblSerie.Text = "Serie:"
        Me.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(104, 32)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(11, 16)
        Me.lblFolio.TabIndex = 75
        Me.lblFolio.Tag = "folio"
        Me.lblFolio.Text = "0"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_eFolio
        '
        Me.lbl_eFolio.AutoSize = True
        Me.lbl_eFolio.Location = New System.Drawing.Point(56, 32)
        Me.lbl_eFolio.Name = "lbl_eFolio"
        Me.lbl_eFolio.Size = New System.Drawing.Size(35, 16)
        Me.lbl_eFolio.TabIndex = 74
        Me.lbl_eFolio.Tag = ""
        Me.lbl_eFolio.Text = "Folio:"
        Me.lbl_eFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 152)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(89, 16)
        Me.Label12.TabIndex = 83
        Me.Label12.Tag = ""
        Me.Label12.Text = "&Observaciones:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(16, 128)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(84, 16)
        Me.lblTipoventa.TabIndex = 81
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "&Tipo de venta:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(40, 80)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(62, 16)
        Me.lblVendedor.TabIndex = 79
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "Ven&dedor:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(56, 104)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 80
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_eFecha
        '
        Me.lbl_eFecha.AutoSize = True
        Me.lbl_eFecha.Location = New System.Drawing.Point(64, 56)
        Me.lbl_eFecha.Name = "lbl_eFecha"
        Me.lbl_eFecha.Size = New System.Drawing.Size(41, 16)
        Me.lbl_eFecha.TabIndex = 78
        Me.lbl_eFecha.Tag = ""
        Me.lbl_eFecha.Text = "F&echa:"
        Me.lbl_eFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMenosSoloDespliegue
        '
        Me.lblMenosSoloDespliegue.Location = New System.Drawing.Point(552, 344)
        Me.lblMenosSoloDespliegue.Name = "lblMenosSoloDespliegue"
        Me.lblMenosSoloDespliegue.Size = New System.Drawing.Size(88, 16)
        Me.lblMenosSoloDespliegue.TabIndex = 87
        Me.lblMenosSoloDespliegue.Tag = "etiqueta_menos_enganche"
        Me.lblMenosSoloDespliegue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalContado
        '
        Me.lblTotalContado.AutoSize = True
        Me.lblTotalContado.Location = New System.Drawing.Point(600, 392)
        Me.lblTotalContado.Name = "lblTotalContado"
        Me.lblTotalContado.Size = New System.Drawing.Size(36, 16)
        Me.lblTotalContado.TabIndex = 89
        Me.lblTotalContado.Tag = ""
        Me.lblTotalContado.Text = "Total:"
        Me.lblTotalContado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSubtotal
        '
        Me.lblSubtotal.AutoSize = True
        Me.lblSubtotal.Location = New System.Drawing.Point(584, 320)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(55, 16)
        Me.lblSubtotal.TabIndex = 86
        Me.lblSubtotal.Tag = ""
        Me.lblSubtotal.Text = "Subtotal:"
        Me.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFechaValor
        '
        Me.lblFechaValor.AutoSize = True
        Me.lblFechaValor.Location = New System.Drawing.Point(112, 56)
        Me.lblFechaValor.Name = "lblFechaValor"
        Me.lblFechaValor.Size = New System.Drawing.Size(41, 16)
        Me.lblFechaValor.TabIndex = 91
        Me.lblFechaValor.Tag = "fecha"
        Me.lblFechaValor.Text = "Fecha:"
        Me.lblFechaValor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblVendedorValue
        '
        Me.lblVendedorValue.AutoSize = True
        Me.lblVendedorValue.Location = New System.Drawing.Point(112, 80)
        Me.lblVendedorValue.Name = "lblVendedorValue"
        Me.lblVendedorValue.Size = New System.Drawing.Size(62, 16)
        Me.lblVendedorValue.TabIndex = 92
        Me.lblVendedorValue.Tag = "vendedor"
        Me.lblVendedorValue.Text = "Vendedor:"
        Me.lblVendedorValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblClienteValor
        '
        Me.lblClienteValor.AutoSize = True
        Me.lblClienteValor.Location = New System.Drawing.Point(112, 104)
        Me.lblClienteValor.Name = "lblClienteValor"
        Me.lblClienteValor.Size = New System.Drawing.Size(47, 16)
        Me.lblClienteValor.TabIndex = 93
        Me.lblClienteValor.Tag = "cliente"
        Me.lblClienteValor.Text = "Cliente:"
        Me.lblClienteValor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTipoVentaValor
        '
        Me.lblTipoVentaValor.AutoSize = True
        Me.lblTipoVentaValor.Location = New System.Drawing.Point(112, 128)
        Me.lblTipoVentaValor.Name = "lblTipoVentaValor"
        Me.lblTipoVentaValor.Size = New System.Drawing.Size(63, 16)
        Me.lblTipoVentaValor.TabIndex = 94
        Me.lblTipoVentaValor.Tag = "tipo_venta"
        Me.lblTipoVentaValor.Text = "tipo_venta"
        Me.lblTipoVentaValor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalDocumentos
        '
        Me.lblTotalDocumentos.Location = New System.Drawing.Point(216, 8)
        Me.lblTotalDocumentos.Name = "lblTotalDocumentos"
        Me.lblTotalDocumentos.Size = New System.Drawing.Size(88, 23)
        Me.lblTotalDocumentos.TabIndex = 120
        Me.lblTotalDocumentos.Tag = "importe_documentos"
        Me.lblTotalDocumentos.Text = "0.00"
        Me.lblTotalDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label19
        '
        Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label19.Location = New System.Drawing.Point(216, 56)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(100, 1)
        Me.Label19.TabIndex = 119
        '
        'lblDocumentosUltimoDocumento
        '
        Me.lblDocumentosUltimoDocumento.Location = New System.Drawing.Point(216, 56)
        Me.lblDocumentosUltimoDocumento.Name = "lblDocumentosUltimoDocumento"
        Me.lblDocumentosUltimoDocumento.Size = New System.Drawing.Size(88, 23)
        Me.lblDocumentosUltimoDocumento.TabIndex = 118
        Me.lblDocumentosUltimoDocumento.Tag = "documentosultimodocumento"
        Me.lblDocumentosUltimoDocumento.Text = "0.00"
        Me.lblDocumentosUltimoDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImporteUltimoDocumento
        '
        Me.lblImporteUltimoDocumento.Location = New System.Drawing.Point(216, 32)
        Me.lblImporteUltimoDocumento.Name = "lblImporteUltimoDocumento"
        Me.lblImporteUltimoDocumento.Size = New System.Drawing.Size(88, 23)
        Me.lblImporteUltimoDocumento.TabIndex = 117
        Me.lblImporteUltimoDocumento.Tag = "importe_ultimo_documento"
        Me.lblImporteUltimoDocumento.Text = "0.00"
        Me.lblImporteUltimoDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLeyendaDocumentoFinal
        '
        Me.lblLeyendaDocumentoFinal.Location = New System.Drawing.Point(48, 32)
        Me.lblLeyendaDocumentoFinal.Name = "lblLeyendaDocumentoFinal"
        Me.lblLeyendaDocumentoFinal.Size = New System.Drawing.Size(160, 23)
        Me.lblLeyendaDocumentoFinal.TabIndex = 116
        Me.lblLeyendaDocumentoFinal.Text = "Documento de  ="
        Me.lblLeyendaDocumentoFinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDocumentoFinal
        '
        Me.lblDocumentoFinal.BackColor = System.Drawing.SystemColors.Window
        Me.lblDocumentoFinal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentoFinal.ForeColor = System.Drawing.Color.Black
        Me.lblDocumentoFinal.Location = New System.Drawing.Point(16, 32)
        Me.lblDocumentoFinal.Name = "lblDocumentoFinal"
        Me.lblDocumentoFinal.Size = New System.Drawing.Size(32, 23)
        Me.lblDocumentoFinal.TabIndex = 115
        Me.lblDocumentoFinal.Tag = "ultimo_documento"
        Me.lblDocumentoFinal.Text = "0"
        Me.lblDocumentoFinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLeyendaDocumentos
        '
        Me.lblLeyendaDocumentos.Location = New System.Drawing.Point(48, 8)
        Me.lblLeyendaDocumentos.Name = "lblLeyendaDocumentos"
        Me.lblLeyendaDocumentos.Size = New System.Drawing.Size(160, 23)
        Me.lblLeyendaDocumentos.TabIndex = 114
        Me.lblLeyendaDocumentos.Text = "Documentos de = "
        Me.lblLeyendaDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDocumentosIguales
        '
        Me.lblDocumentosIguales.BackColor = System.Drawing.SystemColors.Window
        Me.lblDocumentosIguales.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentosIguales.ForeColor = System.Drawing.Color.Black
        Me.lblDocumentosIguales.Location = New System.Drawing.Point(16, 8)
        Me.lblDocumentosIguales.Name = "lblDocumentosIguales"
        Me.lblDocumentosIguales.Size = New System.Drawing.Size(32, 23)
        Me.lblDocumentosIguales.TabIndex = 113
        Me.lblDocumentosIguales.Tag = "numero_documentos"
        Me.lblDocumentosIguales.Text = "0"
        Me.lblDocumentosIguales.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Location = New System.Drawing.Point(560, 384)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(180, 1)
        Me.Label9.TabIndex = 123
        '
        'lblInteres
        '
        Me.lblInteres.AutoSize = True
        Me.lblInteres.Location = New System.Drawing.Point(584, 361)
        Me.lblInteres.Name = "lblInteres"
        Me.lblInteres.Size = New System.Drawing.Size(48, 16)
        Me.lblInteres.TabIndex = 121
        Me.lblInteres.Tag = ""
        Me.lblInteres.Text = "&Int�res:"
        Me.lblInteres.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkLiquida_Vencimiento
        '
        Me.chkLiquida_Vencimiento.EditValue = "False"
        Me.chkLiquida_Vencimiento.Location = New System.Drawing.Point(328, 14)
        Me.chkLiquida_Vencimiento.Name = "chkLiquida_Vencimiento"
        '
        'chkLiquida_Vencimiento.Properties
        '
        Me.chkLiquida_Vencimiento.Properties.Caption = ""
        Me.chkLiquida_Vencimiento.Properties.Enabled = False
        Me.chkLiquida_Vencimiento.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkLiquida_Vencimiento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkLiquida_Vencimiento.Size = New System.Drawing.Size(24, 17)
        Me.chkLiquida_Vencimiento.TabIndex = 98
        Me.chkLiquida_Vencimiento.Tag = "liquida_vencimiento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(392, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 16)
        Me.Label2.TabIndex = 124
        Me.Label2.Tag = "monto_liquida_vencimiento"
        Me.Label2.Text = "monto"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(648, 320)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 16)
        Me.Label3.TabIndex = 125
        Me.Label3.Tag = "importe"
        Me.Label3.Text = "importe"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(648, 343)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 16)
        Me.Label4.TabIndex = 126
        Me.Label4.Tag = "menos_enganche"
        Me.Label4.Text = "menos_eng"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(648, 361)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 16)
        Me.Label5.TabIndex = 127
        Me.Label5.Tag = "interes"
        Me.Label5.Text = "interes"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(648, 392)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 16)
        Me.Label6.TabIndex = 128
        Me.Label6.Tag = "total"
        Me.Label6.Text = "total"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPlanCredito
        '
        Me.lblPlanCredito.AutoSize = True
        Me.lblPlanCredito.Location = New System.Drawing.Point(232, 128)
        Me.lblPlanCredito.Name = "lblPlanCredito"
        Me.lblPlanCredito.Size = New System.Drawing.Size(76, 16)
        Me.lblPlanCredito.TabIndex = 129
        Me.lblPlanCredito.Tag = ""
        Me.lblPlanCredito.Text = "Plan Cr�dito:"
        Me.lblPlanCredito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(312, 128)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 16)
        Me.Label8.TabIndex = 130
        Me.Label8.Tag = "plan_credito"
        Me.Label8.Text = "plan_credito"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(344, 15)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(185, 16)
        Me.Label10.TabIndex = 131
        Me.Label10.Tag = ""
        Me.Label10.Text = "&Liquida al vencimiento a tiempo:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(112, 152)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(120, 16)
        Me.Label11.TabIndex = 132
        Me.Label11.Tag = "observaciones_venta"
        Me.Label11.Text = "observaciones_venta"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblTotalDocumentos)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.lblDocumentosUltimoDocumento)
        Me.Panel1.Controls.Add(Me.lblImporteUltimoDocumento)
        Me.Panel1.Controls.Add(Me.lblLeyendaDocumentoFinal)
        Me.Panel1.Controls.Add(Me.lblDocumentoFinal)
        Me.Panel1.Controls.Add(Me.lblLeyendaDocumentos)
        Me.Panel1.Controls.Add(Me.lblDocumentosIguales)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.chkLiquida_Vencimiento)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(8, 320)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(536, 100)
        Me.Panel1.TabIndex = 133
        '
        'frmVentasEjecutivo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(746, 424)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblPlanCredito)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblInteres)
        Me.Controls.Add(Me.lblTipoVentaValor)
        Me.Controls.Add(Me.lblClienteValor)
        Me.Controls.Add(Me.lblVendedorValue)
        Me.Controls.Add(Me.lblFechaValor)
        Me.Controls.Add(Me.lblMenosSoloDespliegue)
        Me.Controls.Add(Me.lblTotalContado)
        Me.Controls.Add(Me.lblSubtotal)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.lblTipoventa)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lbl_eFecha)
        Me.Controls.Add(Me.lblSerieSucursal)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.lbl_eFolio)
        Me.Controls.Add(Me.grVentas)
        Me.Controls.Add(Me.tmaVentas)
        Me.Name = "frmVentasEjecutivo"
        Me.Text = "frmVentasEjecutivo"
        Me.Controls.SetChildIndex(Me.tmaVentas, 0)
        Me.Controls.SetChildIndex(Me.grVentas, 0)
        Me.Controls.SetChildIndex(Me.lbl_eFolio, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.lblSerieSucursal, 0)
        Me.Controls.SetChildIndex(Me.lbl_eFecha, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblVendedor, 0)
        Me.Controls.SetChildIndex(Me.lblTipoventa, 0)
        Me.Controls.SetChildIndex(Me.Label12, 0)
        Me.Controls.SetChildIndex(Me.lblSubtotal, 0)
        Me.Controls.SetChildIndex(Me.lblTotalContado, 0)
        Me.Controls.SetChildIndex(Me.lblMenosSoloDespliegue, 0)
        Me.Controls.SetChildIndex(Me.lblFechaValor, 0)
        Me.Controls.SetChildIndex(Me.lblVendedorValue, 0)
        Me.Controls.SetChildIndex(Me.lblClienteValor, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVentaValor, 0)
        Me.Controls.SetChildIndex(Me.lblInteres, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.lblPlanCredito, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.Label11, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        CType(Me.grVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLiquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"

    Private oVentas As VillarrealBusiness.clsVentas
    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle

    Public lSucursal As Long = -1
    Public sSerie As String = ""
    Public lFolio As String = -1

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmVentasEjecutivo_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub

    Private Sub frmVentasEjecutivo_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVentas = New VillarrealBusiness.clsVentas
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        ConfiguraMaster()

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub ConfiguraMaster()

        With Me.tmaVentas
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmVentasDetalle
            .AddColumn("departamento")
            .AddColumn("grupo")
            .AddColumn("articulo", "System.Int32")
            .AddColumn("n_articulo")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("preciounitario", "System.Double")
            .AddColumn("total", "System.Double")
            .AddColumn("sobrepedido", "System.Boolean")
            .AddColumn("costo", "System.Double")    'oculto
            .AddColumn("surtido", "System.Boolean") 'oculto
            .AddColumn("reparto", "System.Boolean")
            .AddColumn("bodega")
            .AddColumn("n_bodega")
            .AddColumn("precio_minimo", "System.Double")    'oculto
            .AddColumn("folio_historico_costo", "System.Int32")
            .AddColumn("articulo_regalo", "System.Boolean")
            .AddColumn("precio_contado", "System.Double")
            .AddColumn("modelo")
            .AddColumn("folio_autorizacion", "System.Int32")
            .AddColumn("partida_vistas", "System.Int32")
            .AddColumn("numero_serie")

        End With


    End Sub
#End Region

    Private Sub frmVentasEjecutivo_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oVentas.DespliegaDatosEjecutivo(lSucursal, sSerie, lFolio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            If CType(oDataSet.Tables(0).Rows(0).Item("tipoventa"), String) = "D" Then
                Me.lblPlanCredito.Visible = False
                Me.Panel1.Visible = False
            End If


            Response = oVentasDetalle.Listado(lSucursal, sSerie, lFolio)
            If Not Response.ErrorFound Then
                If Not Response.ErrorFound Then
                    oDataSet = Response.Value
                    Me.tmaVentas.DataSource = oDataSet
                    Me.lblSerieSucursal.Text = CType(oDataSet.Tables(0).Rows(0).Item("serie"), String)
                    Me.lblFolio.Text = CType(oDataSet.Tables(0).Rows(0).Item("folio"), String)

                End If
            End If

        End If
    End Sub

 
End Class
