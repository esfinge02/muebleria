Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports VillarrealBusiness.BusinessEnvironment

Public Class frmCambioChequesDetalle
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"

#End Region

#Region " Código generado por el Diseñador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents clcSucursal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCaja As System.Windows.Forms.Label
    Friend WithEvents clcCaja As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents txtBanco As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNumero_Cheque As System.Windows.Forms.Label
    Friend WithEvents txtNumero_Cheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblMonto As System.Windows.Forms.Label
    Friend WithEvents clcMonto As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtQuien As DevExpress.XtraEditors.TextEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambioChequesDetalle))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.clcSucursal = New Dipros.Editors.TINCalcEdit
        Me.lblCaja = New System.Windows.Forms.Label
        Me.clcCaja = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblPartida = New System.Windows.Forms.Label
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        Me.lblBanco = New System.Windows.Forms.Label
        Me.txtBanco = New DevExpress.XtraEditors.TextEdit
        Me.lblNumero_Cheque = New System.Windows.Forms.Label
        Me.txtNumero_Cheque = New DevExpress.XtraEditors.TextEdit
        Me.lblMonto = New System.Windows.Forms.Label
        Me.clcMonto = New Dipros.Editors.TINCalcEdit
        Me.txtQuien = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumero_Cheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMonto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQuien.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(230, 28)
        Me.tbrTools.TabIndex = 0
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(432, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 8
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSucursal
        '
        Me.clcSucursal.EditValue = "0"
        Me.clcSucursal.Location = New System.Drawing.Point(496, 40)
        Me.clcSucursal.MaxValue = 0
        Me.clcSucursal.MinValue = 0
        Me.clcSucursal.Name = "clcSucursal"
        '
        'clcSucursal.Properties
        '
        Me.clcSucursal.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcSucursal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcSucursal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSucursal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSucursal.Size = New System.Drawing.Size(40, 19)
        Me.clcSucursal.TabIndex = 9
        Me.clcSucursal.TabStop = False
        Me.clcSucursal.Tag = "sucursal"
        '
        'lblCaja
        '
        Me.lblCaja.AutoSize = True
        Me.lblCaja.Location = New System.Drawing.Point(454, 64)
        Me.lblCaja.Name = "lblCaja"
        Me.lblCaja.Size = New System.Drawing.Size(34, 16)
        Me.lblCaja.TabIndex = 10
        Me.lblCaja.Tag = ""
        Me.lblCaja.Text = "&Caja:"
        Me.lblCaja.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCaja
        '
        Me.clcCaja.EditValue = "0"
        Me.clcCaja.Location = New System.Drawing.Point(496, 64)
        Me.clcCaja.MaxValue = 0
        Me.clcCaja.MinValue = 0
        Me.clcCaja.Name = "clcCaja"
        '
        'clcCaja.Properties
        '
        Me.clcCaja.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCaja.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCaja.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCaja.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCaja.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCaja.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCaja.Size = New System.Drawing.Size(40, 19)
        Me.clcCaja.TabIndex = 11
        Me.clcCaja.TabStop = False
        Me.clcCaja.Tag = "caja"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(72, 40)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "22/06/2007"
        Me.dteFecha.Location = New System.Drawing.Point(120, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 1
        Me.dteFecha.Tag = "fecha"
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(440, 112)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 13
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "&Partida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(496, 112)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(24, 19)
        Me.clcPartida.TabIndex = 14
        Me.clcPartida.TabStop = False
        Me.clcPartida.Tag = "partida"
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(70, 64)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(43, 16)
        Me.lblBanco.TabIndex = 2
        Me.lblBanco.Tag = ""
        Me.lblBanco.Text = "&Banco:"
        Me.lblBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBanco
        '
        Me.txtBanco.EditValue = ""
        Me.txtBanco.Location = New System.Drawing.Point(120, 64)
        Me.txtBanco.Name = "txtBanco"
        '
        'txtBanco.Properties
        '
        Me.txtBanco.Properties.MaxLength = 25
        Me.txtBanco.Size = New System.Drawing.Size(150, 20)
        Me.txtBanco.TabIndex = 3
        Me.txtBanco.Tag = "banco"
        '
        'lblNumero_Cheque
        '
        Me.lblNumero_Cheque.AutoSize = True
        Me.lblNumero_Cheque.Location = New System.Drawing.Point(17, 88)
        Me.lblNumero_Cheque.Name = "lblNumero_Cheque"
        Me.lblNumero_Cheque.Size = New System.Drawing.Size(96, 16)
        Me.lblNumero_Cheque.TabIndex = 4
        Me.lblNumero_Cheque.Tag = ""
        Me.lblNumero_Cheque.Text = "&Número cheque:"
        Me.lblNumero_Cheque.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumero_Cheque
        '
        Me.txtNumero_Cheque.EditValue = ""
        Me.txtNumero_Cheque.Location = New System.Drawing.Point(120, 88)
        Me.txtNumero_Cheque.Name = "txtNumero_Cheque"
        '
        'txtNumero_Cheque.Properties
        '
        Me.txtNumero_Cheque.Properties.MaxLength = 40
        Me.txtNumero_Cheque.Size = New System.Drawing.Size(240, 20)
        Me.txtNumero_Cheque.TabIndex = 5
        Me.txtNumero_Cheque.Tag = "numero_cheque"
        '
        'lblMonto
        '
        Me.lblMonto.AutoSize = True
        Me.lblMonto.Location = New System.Drawing.Point(70, 112)
        Me.lblMonto.Name = "lblMonto"
        Me.lblMonto.Size = New System.Drawing.Size(43, 16)
        Me.lblMonto.TabIndex = 6
        Me.lblMonto.Tag = ""
        Me.lblMonto.Text = "&Monto:"
        Me.lblMonto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMonto
        '
        Me.clcMonto.EditValue = "0"
        Me.clcMonto.Location = New System.Drawing.Point(120, 112)
        Me.clcMonto.MaxValue = 0
        Me.clcMonto.MinValue = 0
        Me.clcMonto.Name = "clcMonto"
        '
        'clcMonto.Properties
        '
        Me.clcMonto.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcMonto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcMonto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto.Properties.MaskData.EditMask = "########0.00"
        Me.clcMonto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMonto.Size = New System.Drawing.Size(112, 19)
        Me.clcMonto.TabIndex = 7
        Me.clcMonto.Tag = "monto"
        '
        'txtQuien
        '
        Me.txtQuien.EditValue = "QUIEN"
        Me.txtQuien.Location = New System.Drawing.Point(496, 88)
        Me.txtQuien.Name = "txtQuien"
        '
        'txtQuien.Properties
        '
        Me.txtQuien.Properties.MaxLength = 25
        Me.txtQuien.Size = New System.Drawing.Size(48, 20)
        Me.txtQuien.TabIndex = 12
        Me.txtQuien.TabStop = False
        Me.txtQuien.Tag = "QUIEN"
        '
        'frmCambioChequesDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(626, 147)
        Me.Controls.Add(Me.txtQuien)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblCaja)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.lblNumero_Cheque)
        Me.Controls.Add(Me.lblMonto)
        Me.Controls.Add(Me.clcSucursal)
        Me.Controls.Add(Me.clcCaja)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.txtBanco)
        Me.Controls.Add(Me.txtNumero_Cheque)
        Me.Controls.Add(Me.clcMonto)
        Me.Name = "frmCambioChequesDetalle"
        Me.Controls.SetChildIndex(Me.clcMonto, 0)
        Me.Controls.SetChildIndex(Me.txtNumero_Cheque, 0)
        Me.Controls.SetChildIndex(Me.txtBanco, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.clcCaja, 0)
        Me.Controls.SetChildIndex(Me.clcSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblMonto, 0)
        Me.Controls.SetChildIndex(Me.lblNumero_Cheque, 0)
        Me.Controls.SetChildIndex(Me.lblBanco, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblCaja, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.txtQuien, 0)
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumero_Cheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMonto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQuien.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCambioCheques As New VillarrealBusiness.clsCambioCheques
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCambioCheques_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oCambioCheques.Insertar(Comunes.Common.Sucursal_Actual, Comunes.Common.Caja_Actual, Me.DataSource)
                If Not Response.ErrorFound Then
                    OwnerForm.MasterControl.AddRow(Me.DataSource)
                End If

            Case Actions.Update
                Response = oCambioCheques.Actualizar(Me.DataSource)
                If Not Response.ErrorFound Then
                    OwnerForm.MasterControl.UpdateRow(Me.DataSource)
                End If
            Case Actions.Delete
                Response = oCambioCheques.Eliminar(clcSucursal.Value, clcCaja.Value, dteFecha.DateTime, clcPartida.Value)
                If Not Response.ErrorFound Then
                    OwnerForm.MasterControl.DeleteRow()
                End If
        End Select

    End Sub

    Private Sub frmCambioCheques_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub frmCambioCheques_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Select Case Action
            Case Actions.Insert
                dteFecha.EditValue = Today()
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmCambioCheques_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim intNumeroControl As Long

        Response = oCambioCheques.Validacion(Action, txtBanco.EditValue, txtNumero_Cheque.EditValue, clcMonto.EditValue, txtQuien.EditValue, intNumeroControl)
        Select Case intNumeroControl
            Case 1
                txtBanco.Focus()
            Case 2
                txtNumero_Cheque.Focus()
            Case 3
                clcMonto.Focus()
        End Select
    End Sub

    Private Sub frmCambioCheques_Localize() Handles MyBase.Localize
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
