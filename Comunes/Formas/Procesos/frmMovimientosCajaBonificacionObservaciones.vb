Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmMovimientosCajaBonificacionObservaciones
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMotivoBonificacion As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosCajaBonificacionObservaciones))
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton
        Me.btnAceptar = New DevExpress.XtraEditors.SimpleButton
        Me.txtMotivoBonificacion = New DevExpress.XtraEditors.MemoEdit
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.txtMotivoBonificacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(499, 28)
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(237, 143)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(104, 32)
        Me.btnSalir.TabIndex = 3
        Me.btnSalir.Text = "&Salir"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(117, 143)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(104, 32)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "&Aceptar"
        '
        'txtMotivoBonificacion
        '
        Me.txtMotivoBonificacion.EditValue = ""
        Me.txtMotivoBonificacion.Location = New System.Drawing.Point(9, 64)
        Me.txtMotivoBonificacion.Name = "txtMotivoBonificacion"
        '
        'txtMotivoBonificacion.Properties
        '
        Me.txtMotivoBonificacion.Properties.MaxLength = 300
        Me.txtMotivoBonificacion.Size = New System.Drawing.Size(440, 72)
        Me.txtMotivoBonificacion.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label3.Location = New System.Drawing.Point(9, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(296, 32)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Motivo de la Bonificaci�n:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmMovimientosCajaBonificacionObservaciones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(458, 180)
        Me.Controls.Add(Me.txtMotivoBonificacion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Name = "frmMovimientosCajaBonificacionObservaciones"
        Me.Text = "frmMovimientosCajaBonificacionObservaciones"
        Me.Controls.SetChildIndex(Me.btnAceptar, 0)
        Me.Controls.SetChildIndex(Me.btnSalir, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.txtMotivoBonificacion, 0)
        CType(Me.txtMotivoBonificacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Enum VentanaPadre As Long
        Caja = 1
        NotasCargoAnticipados = 2
    End Enum

    'Public SerieRecibo As String = ""
    'Public FolioRecibo As Long = 0
    Private bObservacionesValida As Boolean = False
    Private sObservaciones As String = ""
    Private lVentanaPadreAnterior As Long

    Public WriteOnly Property VentanaPadreAnterior() As VentanaPadre
        Set(ByVal Value As VentanaPadre)
            lVentanaPadreAnterior = Value
        End Set
    End Property

    Public ReadOnly Property ObservacionesValida() As Boolean
        Get
            Return bObservacionesValida
        End Get
    End Property

    Public ReadOnly Property Observaciones() As String
        Get
            Return sObservaciones
        End Get
    End Property

    Private Sub frmMovimientosCajaBonificacionObservaciones_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.tbrTools.Visible = False
        Me.tbrTools.Enabled = False
        'Me.lblSerieRecibo.Text = SerieRecibo
        'Me.lblFolioRecibo.Text = FolioRecibo
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Me.txtMotivoBonificacion.Text.Trim.Length = 0 Then
            ShowMessage(MessageType.MsgError, "El Motivo de la Bonificaci�n es Requerido", "Caja")
        Else
            bObservacionesValida = True

            Select Case CType(lVentanaPadreAnterior, VentanaPadre)
                Case VentanaPadre.Caja
                    CType(OwnerForm, frmMovimientosCaja).BonificacionObservaciones = Me.txtMotivoBonificacion.Text
                    CType(OwnerForm, frmMovimientosCaja).MostrarFormaPagarCajaCambio(0, 0)
                Case VentanaPadre.NotasCargoAnticipados
                    CType(OwnerForm, frmGeneracionNotasCargoDescuentosAnticipados).BonificacionObservaciones = Me.txtMotivoBonificacion.Text
                    CType(OwnerForm, frmGeneracionNotasCargoDescuentosAnticipados).GuardarCambios()
            End Select
          
            CerrarForma()
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        CerrarForma()

    End Sub

    Private Sub CerrarForma()
        Me.Close()
        Select Case CType(lVentanaPadreAnterior, VentanaPadre)
            Case VentanaPadre.Caja
                CType(OwnerForm, frmMovimientosCaja).Enabled = True
            Case VentanaPadre.NotasCargoAnticipados
                CType(OwnerForm, frmGeneracionNotasCargoDescuentosAnticipados).Enabled = True
        End Select
    End Sub
End Class
