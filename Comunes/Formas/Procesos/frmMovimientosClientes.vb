Imports Dipros.Utils.Common
Imports Dipros.Utils


Public Class frmMovimientosClientes
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcNConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechamovimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteMovimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grcConceptoFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grFacturasDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFacturasDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grcSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcSaldo As Dipros.Editors.TINCalcEdit
    Friend WithEvents grcTipo_documento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblMovimientos As System.Windows.Forms.Label
    Friend WithEvents grvConcentrado As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClaveSucursalTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreSucursalTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcVencidoTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPorVencerTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaUltimoAbonoTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rptDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents grSaldos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcUltimoPAgo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSaldo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkSucursal As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnDocumentos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRefrescar As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblMensajeMercanciaSinEntregar As System.Windows.Forms.Label
    Friend WithEvents lblMensajeGarantias As System.Windows.Forms.Label
    Friend WithEvents lblMensajePedidoFabrica As System.Windows.Forms.Label
    Friend WithEvents grcSerieFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcClaveCliente As Dipros.Editors.TINCalcEdit
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txttelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtentrecalles As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNotas As System.Windows.Forms.Label
    Friend WithEvents txtNotas As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblentrecalles As System.Windows.Forms.Label
    Friend WithEvents lbltelefonos As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblNotasV As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents lbldomicilio As System.Windows.Forms.Label
    Friend WithEvents lblNumeroCliente As System.Windows.Forms.Label
    Friend WithEvents lblNombreCobrador As System.Windows.Forms.Label
    Friend WithEvents btnDetalleVenta As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSaldosVencidos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSaldosxVencer As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnConsolidado As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLlamadasCliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grcCobrador As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConvenio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblTelefonoCelular As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosClientes))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConceptoFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipo_documento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConvenio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grFacturasDetalle = New DevExpress.XtraGrid.GridControl
        Me.grvFacturasDetalle = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcNConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechamovimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteMovimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerieFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcSaldo = New Dipros.Editors.TINCalcEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblMovimientos = New System.Windows.Forms.Label
        Me.grSaldos = New DevExpress.XtraGrid.GridControl
        Me.grvConcentrado = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveSucursalTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursalTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcVencidoTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPorVencerTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaUltimoAbonoTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rptDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.grcUltimoPAgo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCobrador = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label5 = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.chkSaldo = New DevExpress.XtraEditors.CheckEdit
        Me.chkSucursal = New DevExpress.XtraEditors.CheckEdit
        Me.btnDocumentos = New DevExpress.XtraEditors.SimpleButton
        Me.btnRefrescar = New System.Windows.Forms.ToolBarButton
        Me.lblMensajeMercanciaSinEntregar = New System.Windows.Forms.Label
        Me.lblMensajeGarantias = New System.Windows.Forms.Label
        Me.lblMensajePedidoFabrica = New System.Windows.Forms.Label
        Me.clcClaveCliente = New Dipros.Editors.TINCalcEdit
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.txttelefonos = New DevExpress.XtraEditors.TextEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtentrecalles = New DevExpress.XtraEditors.TextEdit
        Me.lblNotas = New System.Windows.Forms.Label
        Me.txtNotas = New DevExpress.XtraEditors.MemoEdit
        Me.lbldomicilio = New System.Windows.Forms.Label
        Me.lblentrecalles = New System.Windows.Forms.Label
        Me.lbltelefonos = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblNotasV = New System.Windows.Forms.Label
        Me.lblSaldo = New System.Windows.Forms.Label
        Me.lblNumeroCliente = New System.Windows.Forms.Label
        Me.lblNombreCobrador = New System.Windows.Forms.Label
        Me.btnDetalleVenta = New DevExpress.XtraEditors.SimpleButton
        Me.btnSaldosVencidos = New DevExpress.XtraEditors.SimpleButton
        Me.btnSaldosxVencer = New DevExpress.XtraEditors.SimpleButton
        Me.btnConsolidado = New DevExpress.XtraEditors.SimpleButton
        Me.btnLlamadasCliente = New DevExpress.XtraEditors.SimpleButton
        Me.lblTelefonoCelular = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grFacturasDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturasDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grSaldos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvConcentrado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcClaveCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txttelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtentrecalles.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnRefrescar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(24489, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(24, 40)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(294, 128)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 5
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(16, 344)
        Me.grFacturas.MainView = Me.grvFacturas
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grFacturas.Size = New System.Drawing.Size(408, 224)
        Me.grFacturas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.InactiveCaption, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFacturas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFacturas.Styles.AddReplace("Row", New DevExpress.Utils.ViewStyleEx("Row", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.Utils.StyleOptions.StyleEnabled, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFacturas.TabIndex = 66
        Me.grFacturas.Text = "Facturas"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcNombreSucursal, Me.grcFactura, Me.grcFecha, Me.grcImporte, Me.grcSucursal, Me.grcFolio, Me.grcSerie, Me.grcConceptoFactura, Me.grcCliente, Me.grcSaldo, Me.grcTipo_documento, Me.grcConvenio})
        Me.grvFacturas.GridControl = Me.grFacturas
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsCustomization.AllowFilter = False
        Me.grvFacturas.OptionsCustomization.AllowGroup = False
        Me.grvFacturas.OptionsCustomization.AllowSort = False
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        Me.grvFacturas.OptionsView.ShowIndicator = False
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "sucursal_nombre"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursal.VisibleIndex = 0
        Me.grcNombreSucursal.Width = 62
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 1
        Me.grcFactura.Width = 83
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 67
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "total"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 76
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "folio"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        '
        'grcConceptoFactura
        '
        Me.grcConceptoFactura.Caption = "ConceptoFactura"
        Me.grcConceptoFactura.FieldName = "concepto"
        Me.grcConceptoFactura.Name = "grcConceptoFactura"
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        '
        'grcSaldo
        '
        Me.grcSaldo.Caption = "Saldo"
        Me.grcSaldo.DisplayFormat.FormatString = "c2"
        Me.grcSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldo.FieldName = "saldo"
        Me.grcSaldo.Name = "grcSaldo"
        Me.grcSaldo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldo.VisibleIndex = 5
        Me.grcSaldo.Width = 65
        '
        'grcTipo_documento
        '
        Me.grcTipo_documento.Caption = "Tipo Documento"
        Me.grcTipo_documento.FieldName = "tipo_documento"
        Me.grcTipo_documento.Name = "grcTipo_documento"
        Me.grcTipo_documento.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcConvenio
        '
        Me.grcConvenio.Caption = "Convenio"
        Me.grcConvenio.FieldName = "convenio"
        Me.grcConvenio.Name = "grcConvenio"
        Me.grcConvenio.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConvenio.VisibleIndex = 2
        Me.grcConvenio.Width = 53
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grFacturasDetalle
        '
        '
        'grFacturasDetalle.EmbeddedNavigator
        '
        Me.grFacturasDetalle.EmbeddedNavigator.Name = ""
        Me.grFacturasDetalle.Location = New System.Drawing.Point(432, 344)
        Me.grFacturasDetalle.MainView = Me.grvFacturasDetalle
        Me.grFacturasDetalle.Name = "grFacturasDetalle"
        Me.grFacturasDetalle.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.grFacturasDetalle.Size = New System.Drawing.Size(400, 224)
        Me.grFacturasDetalle.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.InactiveCaption, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFacturasDetalle.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFacturasDetalle.Styles.AddReplace("Row", New DevExpress.Utils.ViewStyleEx("Row", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.Utils.StyleOptions.StyleEnabled, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFacturasDetalle.TabIndex = 67
        Me.grFacturasDetalle.Text = "Facturas"
        '
        'grvFacturasDetalle
        '
        Me.grvFacturasDetalle.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcNConcepto, Me.grcFechamovimiento, Me.grcImporteMovimiento, Me.grcConcepto, Me.grcDocumento, Me.grcSerieFolio})
        Me.grvFacturasDetalle.GridControl = Me.grFacturasDetalle
        Me.grvFacturasDetalle.Name = "grvFacturasDetalle"
        Me.grvFacturasDetalle.OptionsCustomization.AllowFilter = False
        Me.grvFacturasDetalle.OptionsCustomization.AllowGroup = False
        Me.grvFacturasDetalle.OptionsCustomization.AllowSort = False
        Me.grvFacturasDetalle.OptionsView.ShowGroupPanel = False
        Me.grvFacturasDetalle.OptionsView.ShowIndicator = False
        '
        'grcNConcepto
        '
        Me.grcNConcepto.Caption = "Concepto"
        Me.grcNConcepto.FieldName = "concepto_nombre"
        Me.grcNConcepto.Name = "grcNConcepto"
        Me.grcNConcepto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNConcepto.VisibleIndex = 0
        Me.grcNConcepto.Width = 121
        '
        'grcFechamovimiento
        '
        Me.grcFechamovimiento.Caption = "Fecha"
        Me.grcFechamovimiento.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechamovimiento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechamovimiento.FieldName = "fecha"
        Me.grcFechamovimiento.Name = "grcFechamovimiento"
        Me.grcFechamovimiento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechamovimiento.VisibleIndex = 3
        Me.grcFechamovimiento.Width = 98
        '
        'grcImporteMovimiento
        '
        Me.grcImporteMovimiento.Caption = "Importe"
        Me.grcImporteMovimiento.DisplayFormat.FormatString = "c2"
        Me.grcImporteMovimiento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteMovimiento.FieldName = "importe"
        Me.grcImporteMovimiento.Name = "grcImporteMovimiento"
        Me.grcImporteMovimiento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteMovimiento.VisibleIndex = 4
        Me.grcImporteMovimiento.Width = 83
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Docto."
        Me.grcDocumento.FieldName = "parcialidad"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.VisibleIndex = 1
        Me.grcDocumento.Width = 46
        '
        'grcSerieFolio
        '
        Me.grcSerieFolio.Caption = "Recibo"
        Me.grcSerieFolio.FieldName = "serie_folio"
        Me.grcSerieFolio.Name = "grcSerieFolio"
        Me.grcSerieFolio.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerieFolio.VisibleIndex = 2
        Me.grcSerieFolio.Width = 50
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Tag = ""
        Me.Label2.Text = "Domicilio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(80, 62)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.Enabled = False
        Me.txtDomicilio.Properties.MaxLength = 50
        Me.txtDomicilio.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.txtDomicilio.Size = New System.Drawing.Size(568, 20)
        Me.txtDomicilio.TabIndex = 4
        Me.txtDomicilio.Tag = ""
        Me.txtDomicilio.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(376, 106)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 16)
        Me.Label4.TabIndex = 7
        Me.Label4.Tag = ""
        Me.Label4.Text = "Sucursal:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(680, 576)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 16)
        Me.Label3.TabIndex = 72
        Me.Label3.Tag = ""
        Me.Label3.Text = "Saldo:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = "0"
        Me.clcSaldo.Location = New System.Drawing.Point(728, 576)
        Me.clcSaldo.MaxValue = 0
        Me.clcSaldo.MinValue = 0
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatString = "c2"
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSaldo.Properties.Precision = 2
        Me.clcSaldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldo.Size = New System.Drawing.Size(104, 19)
        Me.clcSaldo.TabIndex = 73
        Me.clcSaldo.Tag = ""
        Me.clcSaldo.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(16, 325)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(67, 17)
        Me.Label7.TabIndex = 74
        Me.Label7.Text = "C&OMPRAS"
        '
        'lblMovimientos
        '
        Me.lblMovimientos.AutoSize = True
        Me.lblMovimientos.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMovimientos.Location = New System.Drawing.Point(424, 325)
        Me.lblMovimientos.Name = "lblMovimientos"
        Me.lblMovimientos.Size = New System.Drawing.Size(99, 17)
        Me.lblMovimientos.TabIndex = 75
        Me.lblMovimientos.Text = "&MOVIMIENTOS"
        '
        'grSaldos
        '
        '
        'grSaldos.EmbeddedNavigator
        '
        Me.grSaldos.EmbeddedNavigator.Name = ""
        Me.grSaldos.Location = New System.Drawing.Point(16, 164)
        Me.grSaldos.MainView = Me.grvConcentrado
        Me.grSaldos.Name = "grSaldos"
        Me.grSaldos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rptDate})
        Me.grSaldos.Size = New System.Drawing.Size(632, 160)
        Me.grSaldos.Styles.AddReplace("FooterPanel", New DevExpress.Utils.ViewStyleEx("FooterPanel", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSaldos.Styles.AddReplace("GroupFooter", New DevExpress.Utils.ViewStyleEx("GroupFooter", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.SystemColors.ControlText, System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSaldos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.InactiveCaption, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSaldos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSaldos.Styles.AddReplace("Row", New DevExpress.Utils.ViewStyleEx("Row", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.Utils.StyleOptions.StyleEnabled, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grSaldos.TabIndex = 76
        Me.grSaldos.Text = "GridControl1"
        '
        'grvConcentrado
        '
        Me.grvConcentrado.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveSucursalTotal, Me.grcNombreSucursalTotal, Me.grcSaldoTotal, Me.grcVencidoTotal, Me.grcPorVencerTotal, Me.grcFechaUltimoAbonoTotal, Me.grcUltimoPAgo, Me.grcCobrador})
        Me.grvConcentrado.GridControl = Me.grSaldos
        Me.grvConcentrado.Name = "grvConcentrado"
        Me.grvConcentrado.OptionsBehavior.Editable = False
        Me.grvConcentrado.OptionsCustomization.AllowFilter = False
        Me.grvConcentrado.OptionsCustomization.AllowGroup = False
        Me.grvConcentrado.OptionsMenu.EnableFooterMenu = False
        Me.grvConcentrado.OptionsView.ShowFilterPanel = False
        Me.grvConcentrado.OptionsView.ShowFooter = True
        Me.grvConcentrado.OptionsView.ShowGroupPanel = False
        Me.grvConcentrado.OptionsView.ShowIndicator = False
        '
        'grcClaveSucursalTotal
        '
        Me.grcClaveSucursalTotal.Caption = "Clave Sucursal"
        Me.grcClaveSucursalTotal.FieldName = "sucursal"
        Me.grcClaveSucursalTotal.Name = "grcClaveSucursalTotal"
        Me.grcClaveSucursalTotal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombreSucursalTotal
        '
        Me.grcNombreSucursalTotal.Caption = "Sucursal"
        Me.grcNombreSucursalTotal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursalTotal.Name = "grcNombreSucursalTotal"
        Me.grcNombreSucursalTotal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursalTotal.SummaryItem.DisplayFormat = "Totales"
        Me.grcNombreSucursalTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom
        Me.grcNombreSucursalTotal.VisibleIndex = 0
        Me.grcNombreSucursalTotal.Width = 123
        '
        'grcSaldoTotal
        '
        Me.grcSaldoTotal.Caption = "Saldo"
        Me.grcSaldoTotal.DisplayFormat.FormatString = "c2"
        Me.grcSaldoTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoTotal.FieldName = "saldo"
        Me.grcSaldoTotal.Name = "grcSaldoTotal"
        Me.grcSaldoTotal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoTotal.SummaryItem.DisplayFormat = "{0:c2}"
        Me.grcSaldoTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcSaldoTotal.VisibleIndex = 3
        Me.grcSaldoTotal.Width = 133
        '
        'grcVencidoTotal
        '
        Me.grcVencidoTotal.Caption = "Vencido"
        Me.grcVencidoTotal.DisplayFormat.FormatString = "c2"
        Me.grcVencidoTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcVencidoTotal.FieldName = "vencido"
        Me.grcVencidoTotal.Name = "grcVencidoTotal"
        Me.grcVencidoTotal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcVencidoTotal.SummaryItem.DisplayFormat = "{0:c2}"
        Me.grcVencidoTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcVencidoTotal.VisibleIndex = 1
        Me.grcVencidoTotal.Width = 123
        '
        'grcPorVencerTotal
        '
        Me.grcPorVencerTotal.Caption = "Por Vencer"
        Me.grcPorVencerTotal.DisplayFormat.FormatString = "c2"
        Me.grcPorVencerTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPorVencerTotal.FieldName = "vencer"
        Me.grcPorVencerTotal.Name = "grcPorVencerTotal"
        Me.grcPorVencerTotal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPorVencerTotal.SummaryItem.DisplayFormat = "{0:c2}"
        Me.grcPorVencerTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcPorVencerTotal.VisibleIndex = 2
        Me.grcPorVencerTotal.Width = 110
        '
        'grcFechaUltimoAbonoTotal
        '
        Me.grcFechaUltimoAbonoTotal.Caption = "Fecha �timo Abono"
        Me.grcFechaUltimoAbonoTotal.ColumnEdit = Me.rptDate
        Me.grcFechaUltimoAbonoTotal.FieldName = "fecha_ultimo_pago"
        Me.grcFechaUltimoAbonoTotal.Name = "grcFechaUltimoAbonoTotal"
        Me.grcFechaUltimoAbonoTotal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaUltimoAbonoTotal.VisibleIndex = 4
        Me.grcFechaUltimoAbonoTotal.Width = 141
        '
        'rptDate
        '
        Me.rptDate.AutoHeight = False
        Me.rptDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rptDate.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.rptDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.rptDate.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.rptDate.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.rptDate.Name = "rptDate"
        '
        'grcUltimoPAgo
        '
        Me.grcUltimoPAgo.Caption = "Ultimo Pago"
        Me.grcUltimoPAgo.FieldName = "ultimo_pago"
        Me.grcUltimoPAgo.Name = "grcUltimoPAgo"
        Me.grcUltimoPAgo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCobrador
        '
        Me.grcCobrador.Caption = "Cobrador"
        Me.grcCobrador.FieldName = "cobrador"
        Me.grcCobrador.Name = "grcCobrador"
        Me.grcCobrador.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(16, 148)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 17)
        Me.Label5.TabIndex = 77
        Me.Label5.Text = "&TOTALES:"
        '
        'Timer1
        '
        Me.Timer1.Interval = 800000
        '
        'chkSaldo
        '
        Me.chkSaldo.EditValue = True
        Me.chkSaldo.Location = New System.Drawing.Point(696, 40)
        Me.chkSaldo.Name = "chkSaldo"
        '
        'chkSaldo.Properties
        '
        Me.chkSaldo.Properties.Caption = "Saldo mayor a cero"
        Me.chkSaldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkSaldo.Size = New System.Drawing.Size(128, 19)
        Me.chkSaldo.TabIndex = 9
        Me.chkSaldo.Tag = "saldo"
        '
        'chkSucursal
        '
        Me.chkSucursal.EditValue = True
        Me.chkSucursal.Location = New System.Drawing.Point(696, 64)
        Me.chkSucursal.Name = "chkSucursal"
        '
        'chkSucursal.Properties
        '
        Me.chkSucursal.Properties.Caption = "Todas las Sucursales"
        Me.chkSucursal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkSucursal.Size = New System.Drawing.Size(136, 19)
        Me.chkSucursal.TabIndex = 10
        Me.chkSucursal.Tag = "Sucursal"
        '
        'btnDocumentos
        '
        Me.btnDocumentos.Location = New System.Drawing.Point(16, 572)
        Me.btnDocumentos.Name = "btnDocumentos"
        Me.btnDocumentos.Size = New System.Drawing.Size(88, 23)
        Me.btnDocumentos.TabIndex = 80
        Me.btnDocumentos.Text = "Documentos"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.Text = "Refrescar"
        Me.btnRefrescar.ToolTipText = "Obtener Info Actualizada"
        '
        'lblMensajeMercanciaSinEntregar
        '
        Me.lblMensajeMercanciaSinEntregar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMensajeMercanciaSinEntregar.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeMercanciaSinEntregar.ForeColor = System.Drawing.Color.Blue
        Me.lblMensajeMercanciaSinEntregar.Location = New System.Drawing.Point(664, 224)
        Me.lblMensajeMercanciaSinEntregar.Name = "lblMensajeMercanciaSinEntregar"
        Me.lblMensajeMercanciaSinEntregar.Size = New System.Drawing.Size(160, 32)
        Me.lblMensajeMercanciaSinEntregar.TabIndex = 99
        Me.lblMensajeMercanciaSinEntregar.Text = "LA FACTURA TIENE MCIA. SIN ENTREGAR"
        Me.lblMensajeMercanciaSinEntregar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMensajeMercanciaSinEntregar.Visible = False
        '
        'lblMensajeGarantias
        '
        Me.lblMensajeGarantias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMensajeGarantias.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeGarantias.ForeColor = System.Drawing.Color.Blue
        Me.lblMensajeGarantias.Location = New System.Drawing.Point(664, 184)
        Me.lblMensajeGarantias.Name = "lblMensajeGarantias"
        Me.lblMensajeGarantias.Size = New System.Drawing.Size(160, 32)
        Me.lblMensajeGarantias.TabIndex = 98
        Me.lblMensajeGarantias.Text = "LA FACTURA TIENE ORD. DE SERV.  PENDIENTES"
        Me.lblMensajeGarantias.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMensajeGarantias.Visible = False
        '
        'lblMensajePedidoFabrica
        '
        Me.lblMensajePedidoFabrica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMensajePedidoFabrica.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajePedidoFabrica.ForeColor = System.Drawing.Color.Blue
        Me.lblMensajePedidoFabrica.Location = New System.Drawing.Point(664, 264)
        Me.lblMensajePedidoFabrica.Name = "lblMensajePedidoFabrica"
        Me.lblMensajePedidoFabrica.Size = New System.Drawing.Size(160, 32)
        Me.lblMensajePedidoFabrica.TabIndex = 97
        Me.lblMensajePedidoFabrica.Text = "LA FACTURA TIENE  PEDIDO A FABRICA"
        Me.lblMensajePedidoFabrica.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMensajePedidoFabrica.Visible = False
        '
        'clcClaveCliente
        '
        Me.clcClaveCliente.EditValue = "0"
        Me.clcClaveCliente.Location = New System.Drawing.Point(80, 40)
        Me.clcClaveCliente.MaxValue = 0
        Me.clcClaveCliente.MinValue = 0
        Me.clcClaveCliente.Name = "clcClaveCliente"
        '
        'clcClaveCliente.Properties
        '
        Me.clcClaveCliente.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcClaveCliente.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcClaveCliente.Properties.Enabled = False
        Me.clcClaveCliente.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcClaveCliente.Properties.Precision = 2
        Me.clcClaveCliente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcClaveCliente.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcClaveCliente.Size = New System.Drawing.Size(40, 19)
        Me.clcClaveCliente.TabIndex = 1
        Me.clcClaveCliente.Tag = ""
        Me.clcClaveCliente.Visible = False
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(136, 40)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(800, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(512, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 2
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = "Seleccione un cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'txttelefonos
        '
        Me.txttelefonos.EditValue = ""
        Me.txttelefonos.Location = New System.Drawing.Point(80, 105)
        Me.txttelefonos.Name = "txttelefonos"
        '
        'txttelefonos.Properties
        '
        Me.txttelefonos.Properties.Enabled = False
        Me.txttelefonos.Properties.MaxLength = 50
        Me.txttelefonos.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.txttelefonos.Size = New System.Drawing.Size(208, 20)
        Me.txttelefonos.TabIndex = 100
        Me.txttelefonos.Tag = ""
        Me.txttelefonos.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 16)
        Me.Label6.TabIndex = 101
        Me.Label6.Tag = ""
        Me.Label6.Text = "Tel. Casa:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(32, 86)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 16)
        Me.Label8.TabIndex = 102
        Me.Label8.Tag = ""
        Me.Label8.Text = "Entre:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtentrecalles
        '
        Me.txtentrecalles.EditValue = ""
        Me.txtentrecalles.Location = New System.Drawing.Point(80, 84)
        Me.txtentrecalles.Name = "txtentrecalles"
        '
        'txtentrecalles.Properties
        '
        Me.txtentrecalles.Properties.Enabled = False
        Me.txtentrecalles.Properties.MaxLength = 50
        Me.txtentrecalles.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.txtentrecalles.Size = New System.Drawing.Size(568, 20)
        Me.txtentrecalles.TabIndex = 103
        Me.txtentrecalles.Tag = ""
        Me.txtentrecalles.Visible = False
        '
        'lblNotas
        '
        Me.lblNotas.AutoSize = True
        Me.lblNotas.Location = New System.Drawing.Point(656, 88)
        Me.lblNotas.Name = "lblNotas"
        Me.lblNotas.Size = New System.Drawing.Size(41, 16)
        Me.lblNotas.TabIndex = 104
        Me.lblNotas.Tag = ""
        Me.lblNotas.Text = "Nota&s:"
        Me.lblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNotas
        '
        Me.txtNotas.EditValue = ""
        Me.txtNotas.Location = New System.Drawing.Point(664, 107)
        Me.txtNotas.Name = "txtNotas"
        '
        'txtNotas.Properties
        '
        Me.txtNotas.Properties.Enabled = False
        Me.txtNotas.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtNotas.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.txtNotas.Size = New System.Drawing.Size(160, 69)
        Me.txtNotas.TabIndex = 105
        Me.txtNotas.Tag = "notas"
        Me.txtNotas.Visible = False
        '
        'lbldomicilio
        '
        Me.lbldomicilio.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldomicilio.Location = New System.Drawing.Point(80, 62)
        Me.lbldomicilio.Name = "lbldomicilio"
        Me.lbldomicilio.Size = New System.Drawing.Size(568, 20)
        Me.lbldomicilio.TabIndex = 106
        Me.lbldomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblentrecalles
        '
        Me.lblentrecalles.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblentrecalles.Location = New System.Drawing.Point(80, 84)
        Me.lblentrecalles.Name = "lblentrecalles"
        Me.lblentrecalles.Size = New System.Drawing.Size(568, 20)
        Me.lblentrecalles.TabIndex = 107
        Me.lblentrecalles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbltelefonos
        '
        Me.lbltelefonos.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltelefonos.Location = New System.Drawing.Point(80, 104)
        Me.lbltelefonos.Name = "lbltelefonos"
        Me.lbltelefonos.Size = New System.Drawing.Size(280, 20)
        Me.lbltelefonos.TabIndex = 108
        Me.lbltelefonos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSucursal
        '
        Me.lblSucursal.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSucursal.Location = New System.Drawing.Point(440, 104)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(208, 20)
        Me.lblSucursal.TabIndex = 109
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNotasV
        '
        Me.lblNotasV.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotasV.Location = New System.Drawing.Point(664, 107)
        Me.lblNotasV.Name = "lblNotasV"
        Me.lblNotasV.Size = New System.Drawing.Size(160, 69)
        Me.lblNotasV.TabIndex = 110
        Me.lblNotasV.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSaldo
        '
        Me.lblSaldo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldo.Location = New System.Drawing.Point(728, 575)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(104, 20)
        Me.lblSaldo.TabIndex = 111
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNumeroCliente
        '
        Me.lblNumeroCliente.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumeroCliente.Location = New System.Drawing.Point(72, 40)
        Me.lblNumeroCliente.Name = "lblNumeroCliente"
        Me.lblNumeroCliente.Size = New System.Drawing.Size(56, 20)
        Me.lblNumeroCliente.TabIndex = 112
        Me.lblNumeroCliente.Text = "0"
        Me.lblNumeroCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNombreCobrador
        '
        Me.lblNombreCobrador.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreCobrador.Location = New System.Drawing.Point(360, 127)
        Me.lblNombreCobrador.Name = "lblNombreCobrador"
        Me.lblNombreCobrador.Size = New System.Drawing.Size(288, 20)
        Me.lblNombreCobrador.TabIndex = 113
        Me.lblNombreCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDetalleVenta
        '
        Me.btnDetalleVenta.Location = New System.Drawing.Point(112, 572)
        Me.btnDetalleVenta.Name = "btnDetalleVenta"
        Me.btnDetalleVenta.Size = New System.Drawing.Size(104, 23)
        Me.btnDetalleVenta.TabIndex = 114
        Me.btnDetalleVenta.Text = "Detalles de Venta"
        '
        'btnSaldosVencidos
        '
        Me.btnSaldosVencidos.Location = New System.Drawing.Point(224, 572)
        Me.btnSaldosVencidos.Name = "btnSaldosVencidos"
        Me.btnSaldosVencidos.Size = New System.Drawing.Size(104, 23)
        Me.btnSaldosVencidos.TabIndex = 115
        Me.btnSaldosVencidos.Text = "Saldos Vencidos"
        '
        'btnSaldosxVencer
        '
        Me.btnSaldosxVencer.Location = New System.Drawing.Point(336, 572)
        Me.btnSaldosxVencer.Name = "btnSaldosxVencer"
        Me.btnSaldosxVencer.Size = New System.Drawing.Size(112, 23)
        Me.btnSaldosxVencer.TabIndex = 116
        Me.btnSaldosxVencer.Text = "Saldos por Vencer"
        '
        'btnConsolidado
        '
        Me.btnConsolidado.Location = New System.Drawing.Point(456, 572)
        Me.btnConsolidado.Name = "btnConsolidado"
        Me.btnConsolidado.Size = New System.Drawing.Size(80, 23)
        Me.btnConsolidado.TabIndex = 117
        Me.btnConsolidado.Text = "Consolidado"
        '
        'btnLlamadasCliente
        '
        Me.btnLlamadasCliente.Location = New System.Drawing.Point(544, 572)
        Me.btnLlamadasCliente.Name = "btnLlamadasCliente"
        Me.btnLlamadasCliente.Size = New System.Drawing.Size(88, 23)
        Me.btnLlamadasCliente.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText)
        Me.btnLlamadasCliente.TabIndex = 118
        Me.btnLlamadasCliente.Text = "Llamadas"
        '
        'lblTelefonoCelular
        '
        Me.lblTelefonoCelular.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelefonoCelular.Location = New System.Drawing.Point(80, 123)
        Me.lblTelefonoCelular.Name = "lblTelefonoCelular"
        Me.lblTelefonoCelular.Size = New System.Drawing.Size(216, 20)
        Me.lblTelefonoCelular.TabIndex = 119
        Me.lblTelefonoCelular.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(22, 124)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(50, 16)
        Me.Label10.TabIndex = 120
        Me.Label10.Tag = ""
        Me.Label10.Text = "Tel. Cel:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmMovimientosClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(842, 600)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblTelefonoCelular)
        Me.Controls.Add(Me.btnLlamadasCliente)
        Me.Controls.Add(Me.btnConsolidado)
        Me.Controls.Add(Me.btnSaldosxVencer)
        Me.Controls.Add(Me.btnSaldosVencidos)
        Me.Controls.Add(Me.btnDetalleVenta)
        Me.Controls.Add(Me.lblNombreCobrador)
        Me.Controls.Add(Me.lblNumeroCliente)
        Me.Controls.Add(Me.lblSaldo)
        Me.Controls.Add(Me.lblNotasV)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lbltelefonos)
        Me.Controls.Add(Me.lblentrecalles)
        Me.Controls.Add(Me.lbldomicilio)
        Me.Controls.Add(Me.lblNotas)
        Me.Controls.Add(Me.txtNotas)
        Me.Controls.Add(Me.txtentrecalles)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.grSaldos)
        Me.Controls.Add(Me.txttelefonos)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.clcClaveCliente)
        Me.Controls.Add(Me.lblMensajeMercanciaSinEntregar)
        Me.Controls.Add(Me.lblMensajeGarantias)
        Me.Controls.Add(Me.lblMensajePedidoFabrica)
        Me.Controls.Add(Me.btnDocumentos)
        Me.Controls.Add(Me.chkSucursal)
        Me.Controls.Add(Me.chkSaldo)
        Me.Controls.Add(Me.clcSaldo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.grFacturasDetalle)
        Me.Controls.Add(Me.txtDomicilio)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblMovimientos)
        Me.Controls.Add(Me.Label5)
        Me.Name = "frmMovimientosClientes"
        Me.Text = "frmMovimientosClientes"
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.lblMovimientos, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.txtDomicilio, 0)
        Me.Controls.SetChildIndex(Me.grFacturasDetalle, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.clcSaldo, 0)
        Me.Controls.SetChildIndex(Me.chkSaldo, 0)
        Me.Controls.SetChildIndex(Me.chkSucursal, 0)
        Me.Controls.SetChildIndex(Me.btnDocumentos, 0)
        Me.Controls.SetChildIndex(Me.lblMensajePedidoFabrica, 0)
        Me.Controls.SetChildIndex(Me.lblMensajeGarantias, 0)
        Me.Controls.SetChildIndex(Me.lblMensajeMercanciaSinEntregar, 0)
        Me.Controls.SetChildIndex(Me.clcClaveCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.txttelefonos, 0)
        Me.Controls.SetChildIndex(Me.grSaldos, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.txtentrecalles, 0)
        Me.Controls.SetChildIndex(Me.txtNotas, 0)
        Me.Controls.SetChildIndex(Me.lblNotas, 0)
        Me.Controls.SetChildIndex(Me.lbldomicilio, 0)
        Me.Controls.SetChildIndex(Me.lblentrecalles, 0)
        Me.Controls.SetChildIndex(Me.lbltelefonos, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblNotasV, 0)
        Me.Controls.SetChildIndex(Me.lblSaldo, 0)
        Me.Controls.SetChildIndex(Me.lblNumeroCliente, 0)
        Me.Controls.SetChildIndex(Me.lblNombreCobrador, 0)
        Me.Controls.SetChildIndex(Me.btnDetalleVenta, 0)
        Me.Controls.SetChildIndex(Me.btnSaldosVencidos, 0)
        Me.Controls.SetChildIndex(Me.btnSaldosxVencer, 0)
        Me.Controls.SetChildIndex(Me.btnConsolidado, 0)
        Me.Controls.SetChildIndex(Me.btnLlamadasCliente, 0)
        Me.Controls.SetChildIndex(Me.lblTelefonoCelular, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grFacturasDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturasDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grSaldos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvConcentrado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcClaveCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txttelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtentrecalles.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"

    Private numero_sucursal As Integer
    Private bandera As Boolean
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVendedores As VillarrealBusiness.clsVendedores
    Private oCobradores As VillarrealBusiness.clsCobradores
    Private oClientescobradores As VillarrealBusiness.clsClientesCobradores
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oReportes As VillarrealBusiness.Reportes


    Private sucursal_actual As Long
    Private bload As Boolean
    Public ConsultaMaestra As Boolean = False
    Private EntroLkp As Boolean = False

    Private ReadOnly Property cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    'Private ReadOnly Property cobrador() As Long
    '    Get
    '        Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCobrador)
    '    End Get
    'End Property

    Private ReadOnly Property Sucursal_dependencia() As Boolean
        Get
            Return CType(CType(oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmMovimientosClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oClientes = New VillarrealBusiness.clsClientes
        oVendedores = New VillarrealBusiness.clsVendedores
        oCobradores = New VillarrealBusiness.clsCobradores
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oClientescobradores = New VillarrealBusiness.clsClientesCobradores
        oReportes = New VillarrealBusiness.Reportes

        Me.tbrTools.Buttons(0).Visible = False
        sucursal_actual = Comunes.Common.Sucursal_Actual
        bload = False
    End Sub
    Private Sub frmMovimientosClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub
    Private Sub frmMovimientosClientes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        '   Response = oVentas.ValidacionCambiarVendedor(Action, sucursal, serie, folio, vendedor)
    End Sub
    Private Sub frmMovimientosClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"


    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        'Response = oClientes.LookupCliente()
        Response = oClientes.LookupClienteEjecutivo(Common.Sucursal_Actual)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        'Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
        Comunes.clsFormato.for_clientes_caja_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        EntroLkp = True
        TraerDatosCliente()
        TraerFacturas()
        EntroLkp = False
    End Sub

    'Private Sub lkpCobrador_LoadData(ByVal Initialize As Boolean)
    '    Dim Response As New Events
    '    Response = oCobradores.Lookup()
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpCobrador.DataSource = oDataSet.Tables(0)
    '        oDataSet = Nothing
    '    End If
    '    Response = Nothing
    'End Sub
    'Private Sub lkpCobrador_Format()
    '    Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    'End Sub


    Private Sub grvFacturas_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvFacturas.FocusedRowChanged
        If Me.EntroLkp = False Then
            CalcularSaldosyLlenarDetalle(e.FocusedRowHandle)
            RevisaFactura(e.FocusedRowHandle)
        End If
    End Sub
    Private Sub grvFacturas_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvFacturas.DoubleClick
        If Me.ConsultaMaestra = True Then
            TraerDetalleVenta()
        End If
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'TraerFacturas() ' se comento para que ya no refrescara la informacion
    End Sub
    Private Sub chkSaldo_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSaldo.EditValueChanged
        If bload = True Then
            TraerFacturas()
        End If

    End Sub
    Private Sub chkSucursal_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSucursal.EditValueChanged
        If bload = True Then
            TraerFacturas()
        End If
    End Sub
    Private Sub grvConcentrado_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvConcentrado.FocusedRowChanged

        If Me.grvConcentrado.FocusedRowHandle >= 0 Then
            Dim j As Integer = Me.grvConcentrado.FocusedRowHandle
            Me.lblNombreCobrador.Text = Me.grvConcentrado.GetRowCellValue(j, Me.grcCobrador)

            If chkSucursal.EditValue = False And bandera = True Then
                TraerFacturas()
            End If

        End If
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.btnRefrescar Then
            TraerDatosCliente()
            TraerFacturas()
        End If
    End Sub


    Private Sub btnDetalleVenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetalleVenta.Click
        TraerDetalleVenta()
    End Sub
    Private Sub btnSaldosVencidos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaldosVencidos.Click
        ReporteSaldos(0)
    End Sub
    Private Sub btnSaldosxVencer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaldosxVencer.Click
        ReporteSaldos(1)

    End Sub
    Private Sub btnConsolidado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsolidado.Click
        ' tipo_movimiento :   -- 0:Vencidos, 1: Por Vencer
        Dim response As Events

        response = oReportes.Consolidado(cliente, 1)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte Consolidado no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oReport As New Comunes.rptConsolidado
                'oReport.lblSucursal.Visible = False
                'oReport.lblnombre_sucursal.Visible = False
                oReport.DataSource = oDataSet.Tables(0)
                TinApp.ShowReport(Me.MdiParent, "Consolidado", oReport, , , , True)
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub
    Private Sub btnLlamadasCliente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLlamadasCliente.Click
        If Me.cliente < 1 Then Exit Sub

        Dim oForm As New Comunes.frmLlamadasClientesEjecutivo
        With oForm
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Action = Actions.Update
            '.Refresh()
            .cliente = cliente
            .NombreCliente = Me.lkpCliente.DisplayText
            .Title = "Llamadas de Clientes"

            .Show()
            .cliente = cliente

            Me.Enabled = False
        End With
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub TraerFacturas()
        Dim response As New Events
        Dim oDataset As DataSet
        numero_sucursal = grvConcentrado.FocusedRowHandle

        response = Me.oMovimientosCobrar.MovimientosVentaClienteFiltro(cliente, chkSaldo.EditValue, chkSucursal.EditValue, grvConcentrado.GetRowCellValue(grvConcentrado.FocusedRowHandle, Me.grcClaveSucursalTotal))

        If Not response.ErrorFound Then
            oDataset = response.Value
            Me.grFacturas.DataSource = oDataset.Tables(0)
            oDataset = Nothing

        End If
        response = Nothing
        TraerSaldos()
        bload = True

        SeleccionarSucursalActual()

        Me.CalcularSaldosyLlenarDetalle(0)


    End Sub
    Private Sub TraerDetalle(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long)
        Dim response As New Events
        Dim oDataset As DataSet
        response = Me.oMovimientosCobrar.MovimientosVentaClienteDetalle(sucursal, concepto, serie, folio, cliente, Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            oDataset = response.Value
            Me.grFacturasDetalle.DataSource = oDataset.Tables(0)
            oDataset = Nothing
        End If
        response = Nothing
    End Sub
    Private Sub TraerSaldos()

        Dim response As New Events
        Dim oDataset As DataSet
        bandera = False

        response = Me.oMovimientosCobrar.SaldosMovimientosCliente(cliente, Me.chkSaldo.Checked)

        If Not response.ErrorFound Then
            oDataset = response.Value
            Me.grSaldos.DataSource = oDataset.Tables(0)
            oDataset = Nothing
        End If
        response = Nothing

        'If bandera = False Then
        '    'If numero_sucursal <> grvConcentrado.GetRowCellValue(grvConcentrado.FocusedRowHandle, Me.grcClaveSucursalTotal) Then
        '    If numero_sucursal <> grvConcentrado.FocusedRowHandle Then
        '        grvConcentrado.FocusedRowHandle() = numero_sucursal
        '    End If
        'End If
        'bandera = True

    End Sub

    Private Sub CalcularSaldosyLlenarDetalle(ByVal rowhandle As Long)
        Me.grFacturasDetalle.DataSource = Nothing
        TraerDetalle(Me.grvFacturas.GetRowCellValue(rowhandle, Me.grcSucursal), grvFacturas.GetRowCellValue(rowhandle, Me.grcConceptoFactura), grvFacturas.GetRowCellValue(rowhandle, Me.grcSerie), grvFacturas.GetRowCellValue(rowhandle, Me.grcFolio), grvFacturas.GetRowCellValue(rowhandle, Me.grcCliente))
        Me.clcSaldo.EditValue = Me.grvFacturas.GetRowCellValue(rowhandle, Me.grcSaldo)

        Me.lblSaldo.Text = Format(Me.clcSaldo.Value, "c2")
    End Sub
    Private Sub SeleccionarSucursalActual()
        If Me.chkSucursal.Checked = True Then Exit Sub

        Dim i As Long
        For i = 0 To Me.grvConcentrado.RowCount - 1
            If Me.sucursal_actual = Me.grvConcentrado.GetRowCellValue(i, Me.grcClaveSucursalTotal) Then
                Me.grvConcentrado.FocusedRowHandle = i
                Exit For
            End If
        Next

        i = Nothing
    End Sub
    Private Sub btnDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocumentos.Click
        TraerDocumentosVenta()
    End Sub
    Private Sub TraerDocumentosVenta()

        If grvFacturas.RowCount > 0 Then

            Dim orow As DataRowView = Me.grvFacturas.GetRow(Me.grvFacturas.FocusedRowHandle)
            Dim oVerProductos As New frmMovimientosClientesDocumentos

            With oVerProductos
                .Title = "Documentos de la Venta"
                .FormaPadre = Me
                .MdiParent = Me.MdiParent
                .Sucursal_factura = orow("sucursal")
                .concepto_factura = orow("concepto")
                .serie_factura = orow("serie")
                .folio_factura = orow("folio")
                .cliente_factura = Me.cliente
                .Show()
            End With

            oVerProductos = Nothing
        End If
    End Sub

    Private Sub RevisaFactura(ByVal fila As Long)

        If CType(Me.grFacturas.DataSource, DataTable).Rows.Count > 0 Then
            Dim response As New Events

            Dim orow As DataRowView = Me.grvFacturas.GetRow(fila)
            Dim articulos_no_entregados As Long = orow("articulos_no_entregados")
            Dim sobrepedidos As Long = orow("sobrepedidos")
            Dim servicios_no_terminados As Long = orow("servicios_no_terminados")


            If sobrepedidos > 0 Then
                Me.lblMensajePedidoFabrica.Visible = True
            Else
                Me.lblMensajePedidoFabrica.Visible = False
            End If

            If servicios_no_terminados > 0 Then
                Me.lblMensajeGarantias.Visible = True
            Else
                Me.lblMensajeGarantias.Visible = False
            End If

            If articulos_no_entregados > 0 Then
                Me.lblMensajeMercanciaSinEntregar.Visible = True
            Else
                Me.lblMensajeMercanciaSinEntregar.Visible = False
            End If

            'END IF
        End If
    End Sub

    Private Sub TraerDatosCliente()
        Dim oDataset As DataSet
        Dim Response As New Events
        Dim oRow As DataRow

        Response = oClientes.LookupLlenado(Sucursal_dependencia, cliente)
        If Not Response.ErrorFound Then

            oDataset = Response.Value
            oRow = oDataset.Tables(0).Rows(0)
            oDataset = Nothing
        End If
        Response = Nothing

        Me.txtDomicilio.Text = oRow("direccion")
        Me.clcClaveCliente.Value = 0
        Me.clcClaveCliente.Value = cliente
        Me.txtentrecalles.Text = oRow("entre_calles")
        Me.txttelefonos.Text = oRow("telefonos")
        Me.txtNotas.Text = oRow("notas")


        Me.lbldomicilio.Text = oRow("direccion")
        Me.lblSucursal.Text = IIf(IsDBNull(oRow("sucursal_nombre")), "", oRow("sucursal_nombre"))
        Me.lblentrecalles.Text = oRow("entre_calles")
        Me.lbltelefonos.Text = oRow("telefonos")
        Me.lblNotasV.Text = oRow("notas")
        Me.lblNumeroCliente.Text = cliente.ToString
        Me.lblTelefonoCelular.Text = oRow("fax")





        'Response = Me.oClientescobradores.Traercobrador(IIf(IsDBNull(oRow("sucursal")), -1, oRow("sucursal")), cliente)

        'If Not Response.ErrorFound Then
        '    oDataset = Response.Value
        '    If oDataset.Tables(0).Rows.Count > 0 Then
        '        Me.lblNombreCobrador.Text = oDataset.Tables(0).Rows(0).Item("nombre")
        '    Else
        '        Me.lblNombreCobrador.Text = ""
        '    End If
        'End If

    End Sub

    Private Sub TraerDetalleVenta()
        If grvFacturas.RowCount > 0 Then
            If Me.grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcTipo_documento) = "V" Then
                Dim oform As Object

                'Dim oForm As New Comunes.frmVentas
                If ConsultaMaestra = True Then
                    oForm = New Comunes.frmVentas
                Else
                    oForm = New Comunes.frmVentasEjecutivo
                End If

                With oForm
                    .Title = "Consulta de Ventas"
                    .OwnerForm = Me
                    .MdiParent = Me.MdiParent
                    .Action = Actions.Update
                    .lSucursal = Me.grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcSucursal)
                    .sSerie = grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcSerie)
                    .lFolio = grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcFolio)
                    .Show()
                    Me.Enabled = False
                End With
            Else
                Dim oForm As New frmConsultarNotaCargo
                With oForm
                    .Title = "Consulta de Cargos"
                    .OwnerForm = Me
                    .MdiParent = Me.MdiParent
                    .Action = Actions.Update
                    .lSucursal = Me.grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcSucursal)
                    .sSerie = grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcSerie)
                    .lFolio = grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcFolio)
                    .sConcepto = grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcConceptoFactura)
                    .lCliente = grvFacturas.GetRowCellValue(grvFacturas.FocusedRowHandle, Me.grcCliente)

                    .Show()
                    Me.Enabled = False
                End With
            End If
        End If
    End Sub

    Private Sub ReporteSaldos(ByVal tipo_movimiento As Long)

        ' tipo_movimiento :   -- 0:Vencidos, 1: Por Vencer
        Dim response As Events

        response = oReportes.DocumentosVencerEjcutivo(cliente, tipo_movimiento)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Documentos por Vencer no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oReport As New Comunes.rptDocumentosVencer
                'oReport.lblSucursal.Visible = False
                'oReport.lblnombre_sucursal.Visible = False
                oReport.DataSource = oDataSet.Tables(0)
                TinApp.ShowReport(Me.MdiParent, "Documentos por Vencer", oReport, , , , True)
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub

#End Region




  
End Class
