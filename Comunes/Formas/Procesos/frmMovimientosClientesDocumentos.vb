Imports Dipros.Utils.Events

Public Class frmMovimientosClientesDocumentos
    Inherits Dipros.Windows.frmTINForm

    Public Sucursal_factura As Long
    Public concepto_factura As String
    Public serie_factura As String
    Public folio_factura As Long
    Public cliente_factura As Long
    Public FormaPadre As Dipros.Windows.frmTINForm
    Private oMovimientosDetalle As New VillarrealBusiness.clsMovimientosCobrarDetalle


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grDocumentos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDocumentos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaVencimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcIntereses As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosClientesDocumentos))
        Me.grDocumentos = New DevExpress.XtraGrid.GridControl
        Me.grvDocumentos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaVencimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcIntereses = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.grDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(570, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'grDocumentos
        '
        '
        'grDocumentos.EmbeddedNavigator
        '
        Me.grDocumentos.EmbeddedNavigator.Name = ""
        Me.grDocumentos.Location = New System.Drawing.Point(8, 40)
        Me.grDocumentos.MainView = Me.grvDocumentos
        Me.grDocumentos.Name = "grDocumentos"
        Me.grDocumentos.Size = New System.Drawing.Size(512, 328)
        Me.grDocumentos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDocumentos.TabIndex = 59
        Me.grDocumentos.Text = "GridControl1"
        '
        'grvDocumentos
        '
        Me.grvDocumentos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcDocumento, Me.grcFechaVencimiento, Me.grcIntereses, Me.grcImporte, Me.grcSaldo})
        Me.grvDocumentos.GridControl = Me.grDocumentos
        Me.grvDocumentos.Name = "grvDocumentos"
        Me.grvDocumentos.OptionsView.ShowFooter = True
        Me.grvDocumentos.OptionsView.ShowGroupPanel = False
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Docto."
        Me.grcDocumento.FieldName = "documento"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.VisibleIndex = 0
        Me.grcDocumento.Width = 48
        '
        'grcFechaVencimiento
        '
        Me.grcFechaVencimiento.Caption = "Fecha Vto."
        Me.grcFechaVencimiento.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaVencimiento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaVencimiento.FieldName = "fecha_vencimiento"
        Me.grcFechaVencimiento.Name = "grcFechaVencimiento"
        Me.grcFechaVencimiento.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaVencimiento.VisibleIndex = 1
        Me.grcFechaVencimiento.Width = 109
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.HeaderStyleName = "Style1"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 3
        Me.grcImporte.Width = 85
        '
        'grcSaldo
        '
        Me.grcSaldo.Caption = "Saldo"
        Me.grcSaldo.DisplayFormat.FormatString = "c2"
        Me.grcSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldo.FieldName = "saldo"
        Me.grcSaldo.HeaderStyleName = "Style1"
        Me.grcSaldo.Name = "grcSaldo"
        Me.grcSaldo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldo.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcSaldo.VisibleIndex = 4
        Me.grcSaldo.Width = 86
        '
        'grcIntereses
        '
        Me.grcIntereses.Caption = "Intereses"
        Me.grcIntereses.DisplayFormat.FormatString = "c2"
        Me.grcIntereses.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcIntereses.FieldName = "intereses"
        Me.grcIntereses.HeaderStyleName = "Style1"
        Me.grcIntereses.Name = "grcIntereses"
        Me.grcIntereses.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcIntereses.VisibleIndex = 2
        Me.grcIntereses.Width = 90
        '
        'frmMovimientosClientesDocumentos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(530, 376)
        Me.Controls.Add(Me.grDocumentos)
        Me.Name = "frmMovimientosClientesDocumentos"
        Me.Text = "Documentos"
        Me.Controls.SetChildIndex(Me.grDocumentos, 0)
        CType(Me.grDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmMovimientosClientesDocumentos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        tbrTools.Buttons(0).Visible = False
        tbrTools.Buttons(0).Enabled = False

        FormaPadre.Enabled = False
        Response = oMovimientosDetalle.MovimientosClientesFacturaDetalle(Sucursal_factura, concepto_factura, serie_factura, folio_factura, cliente_factura)
        If Response.ErrorFound Then
            Response.ShowError()
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.grDocumentos.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub frmMovimientosClientesDocumentos_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Disposed
        FormaPadre.Enabled = True
    End Sub
End Class
