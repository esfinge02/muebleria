Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmEnvioInformacionConveniosDetalle
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblNom As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblRFC As System.Windows.Forms.Label
    Friend WithEvents txtRFC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblImpQ As System.Windows.Forms.Label
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcImporteAbonado As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcImporteMes As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporteMes As System.Windows.Forms.Label
    Friend WithEvents lblTipoMov As System.Windows.Forms.Label
    Friend WithEvents clcMovQuincenaFinal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblQuincenaFinal As System.Windows.Forms.Label
    Friend WithEvents lblMovQuincenaInicial As System.Windows.Forms.Label
    Friend WithEvents clcMovQuincenaInicial As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcTipo As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtFiller As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cboTipoConvenio As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txtNumDocumento As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEnvioInformacionConveniosDetalle))
        Me.lblNom = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblRFC = New System.Windows.Forms.Label
        Me.txtRFC = New DevExpress.XtraEditors.TextEdit
        Me.lblImpQ = New System.Windows.Forms.Label
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit
        Me.clcImporteAbonado = New Dipros.Editors.TINCalcEdit
        Me.clcImporteMes = New Dipros.Editors.TINCalcEdit
        Me.lblImporteMes = New System.Windows.Forms.Label
        Me.lblTipoMov = New System.Windows.Forms.Label
        Me.clcMovQuincenaFinal = New Dipros.Editors.TINCalcEdit
        Me.lblQuincenaFinal = New System.Windows.Forms.Label
        Me.clcMovQuincenaInicial = New Dipros.Editors.TINCalcEdit
        Me.lblMovQuincenaInicial = New System.Windows.Forms.Label
        Me.clcTipo = New Dipros.Editors.TINCalcEdit
        Me.txtFiller = New DevExpress.XtraEditors.TextEdit
        Me.cboTipoConvenio = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.txtNumDocumento = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRFC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporteAbonado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporteMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMovQuincenaFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMovQuincenaInicial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFiller.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoConvenio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(763, 28)
        '
        'lblNom
        '
        Me.lblNom.AutoSize = True
        Me.lblNom.Location = New System.Drawing.Point(72, 208)
        Me.lblNom.Name = "lblNom"
        Me.lblNom.Size = New System.Drawing.Size(57, 16)
        Me.lblNom.TabIndex = 14
        Me.lblNom.Text = "Nombre :"
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(144, 213)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 28
        Me.txtNombre.Size = New System.Drawing.Size(328, 20)
        Me.txtNombre.TabIndex = 15
        Me.txtNombre.Tag = "nombre"
        '
        'lblRFC
        '
        Me.lblRFC.AutoSize = True
        Me.lblRFC.Location = New System.Drawing.Point(96, 184)
        Me.lblRFC.Name = "lblRFC"
        Me.lblRFC.Size = New System.Drawing.Size(31, 16)
        Me.lblRFC.TabIndex = 12
        Me.lblRFC.Text = "RFC:"
        '
        'txtRFC
        '
        Me.txtRFC.EditValue = ""
        Me.txtRFC.Location = New System.Drawing.Point(144, 188)
        Me.txtRFC.Name = "txtRFC"
        '
        'txtRFC.Properties
        '
        Me.txtRFC.Properties.MaxLength = 13
        Me.txtRFC.Size = New System.Drawing.Size(168, 20)
        Me.txtRFC.TabIndex = 13
        Me.txtRFC.Tag = "rfc"
        '
        'lblImpQ
        '
        Me.lblImpQ.AutoSize = True
        Me.lblImpQ.Location = New System.Drawing.Point(16, 160)
        Me.lblImpQ.Name = "lblImpQ"
        Me.lblImpQ.Size = New System.Drawing.Size(113, 16)
        Me.lblImpQ.TabIndex = 10
        Me.lblImpQ.Text = "Importe Quincena :"
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(72, 88)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 4
        Me.lblConcepto.Text = "Concepto:"
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(144, 88)
        Me.txtConcepto.Name = "txtConcepto"
        '
        'txtConcepto.Properties
        '
        Me.txtConcepto.Properties.MaxLength = 2
        Me.txtConcepto.Size = New System.Drawing.Size(40, 20)
        Me.txtConcepto.TabIndex = 5
        Me.txtConcepto.Tag = "concepto_movto"
        '
        'clcImporteAbonado
        '
        Me.clcImporteAbonado.EditValue = "0"
        Me.clcImporteAbonado.Location = New System.Drawing.Point(144, 164)
        Me.clcImporteAbonado.MaxValue = 0
        Me.clcImporteAbonado.MinValue = 0
        Me.clcImporteAbonado.Name = "clcImporteAbonado"
        '
        'clcImporteAbonado.Properties
        '
        Me.clcImporteAbonado.Properties.DisplayFormat.FormatString = "n2"
        Me.clcImporteAbonado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteAbonado.Properties.EditFormat.FormatString = "n2"
        Me.clcImporteAbonado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteAbonado.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcImporteAbonado.Properties.MaxLength = 7
        Me.clcImporteAbonado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporteAbonado.Size = New System.Drawing.Size(78, 19)
        Me.clcImporteAbonado.TabIndex = 11
        Me.clcImporteAbonado.Tag = "importe_quincenal"
        Me.clcImporteAbonado.ToolTip = "Importe Quincenal"
        '
        'clcImporteMes
        '
        Me.clcImporteMes.EditValue = "0"
        Me.clcImporteMes.Location = New System.Drawing.Point(144, 140)
        Me.clcImporteMes.MaxValue = 0
        Me.clcImporteMes.MinValue = 0
        Me.clcImporteMes.Name = "clcImporteMes"
        '
        'clcImporteMes.Properties
        '
        Me.clcImporteMes.Properties.DisplayFormat.FormatString = "n2"
        Me.clcImporteMes.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteMes.Properties.EditFormat.FormatString = "n2"
        Me.clcImporteMes.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteMes.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcImporteMes.Properties.MaxLength = 7
        Me.clcImporteMes.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporteMes.Size = New System.Drawing.Size(78, 19)
        Me.clcImporteMes.TabIndex = 9
        Me.clcImporteMes.Tag = "importe_mensual"
        Me.clcImporteMes.ToolTip = "Importe Mensual"
        '
        'lblImporteMes
        '
        Me.lblImporteMes.AutoSize = True
        Me.lblImporteMes.Location = New System.Drawing.Point(48, 136)
        Me.lblImporteMes.Name = "lblImporteMes"
        Me.lblImporteMes.Size = New System.Drawing.Size(83, 16)
        Me.lblImporteMes.TabIndex = 8
        Me.lblImporteMes.Text = "Importe Mes :"
        '
        'lblTipoMov
        '
        Me.lblTipoMov.AutoSize = True
        Me.lblTipoMov.Location = New System.Drawing.Point(32, 112)
        Me.lblTipoMov.Name = "lblTipoMov"
        Me.lblTipoMov.Size = New System.Drawing.Size(104, 16)
        Me.lblTipoMov.TabIndex = 6
        Me.lblTipoMov.Text = "Tipo Movimiento :"
        '
        'clcMovQuincenaFinal
        '
        Me.clcMovQuincenaFinal.EditValue = "0"
        Me.clcMovQuincenaFinal.Location = New System.Drawing.Point(144, 64)
        Me.clcMovQuincenaFinal.MaxValue = 0
        Me.clcMovQuincenaFinal.MinValue = 0
        Me.clcMovQuincenaFinal.Name = "clcMovQuincenaFinal"
        '
        'clcMovQuincenaFinal.Properties
        '
        Me.clcMovQuincenaFinal.Properties.DisplayFormat.FormatString = "###0"
        Me.clcMovQuincenaFinal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMovQuincenaFinal.Properties.EditFormat.FormatString = "###0"
        Me.clcMovQuincenaFinal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMovQuincenaFinal.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcMovQuincenaFinal.Properties.MaxLength = 6
        Me.clcMovQuincenaFinal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMovQuincenaFinal.Size = New System.Drawing.Size(80, 19)
        Me.clcMovQuincenaFinal.TabIndex = 3
        Me.clcMovQuincenaFinal.Tag = "quincena_final"
        Me.clcMovQuincenaFinal.ToolTip = "Quincena Final"
        '
        'lblQuincenaFinal
        '
        Me.lblQuincenaFinal.AutoSize = True
        Me.lblQuincenaFinal.Location = New System.Drawing.Point(8, 64)
        Me.lblQuincenaFinal.Name = "lblQuincenaFinal"
        Me.lblQuincenaFinal.Size = New System.Drawing.Size(124, 16)
        Me.lblQuincenaFinal.TabIndex = 2
        Me.lblQuincenaFinal.Text = "Mov. Quincena Final :"
        '
        'clcMovQuincenaInicial
        '
        Me.clcMovQuincenaInicial.EditValue = "0"
        Me.clcMovQuincenaInicial.Location = New System.Drawing.Point(144, 40)
        Me.clcMovQuincenaInicial.MaxValue = 0
        Me.clcMovQuincenaInicial.MinValue = 0
        Me.clcMovQuincenaInicial.Name = "clcMovQuincenaInicial"
        '
        'clcMovQuincenaInicial.Properties
        '
        Me.clcMovQuincenaInicial.Properties.DisplayFormat.FormatString = "###0"
        Me.clcMovQuincenaInicial.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMovQuincenaInicial.Properties.EditFormat.FormatString = "###0"
        Me.clcMovQuincenaInicial.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMovQuincenaInicial.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcMovQuincenaInicial.Properties.MaxLength = 6
        Me.clcMovQuincenaInicial.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMovQuincenaInicial.Size = New System.Drawing.Size(78, 19)
        Me.clcMovQuincenaInicial.TabIndex = 1
        Me.clcMovQuincenaInicial.Tag = "quincena_inicial"
        Me.clcMovQuincenaInicial.ToolTip = "Quincena Inicial"
        '
        'lblMovQuincenaInicial
        '
        Me.lblMovQuincenaInicial.AutoSize = True
        Me.lblMovQuincenaInicial.Location = New System.Drawing.Point(8, 40)
        Me.lblMovQuincenaInicial.Name = "lblMovQuincenaInicial"
        Me.lblMovQuincenaInicial.Size = New System.Drawing.Size(131, 16)
        Me.lblMovQuincenaInicial.TabIndex = 0
        Me.lblMovQuincenaInicial.Text = "Mov. Quincena Inicial :"
        '
        'clcTipo
        '
        Me.clcTipo.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.clcTipo.Location = New System.Drawing.Point(416, 48)
        Me.clcTipo.MaxValue = 0
        Me.clcTipo.MinValue = 0
        Me.clcTipo.Name = "clcTipo"
        '
        'clcTipo.Properties
        '
        Me.clcTipo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipo.Properties.Enabled = False
        Me.clcTipo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTipo.Properties.MaxLength = 1
        Me.clcTipo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTipo.Size = New System.Drawing.Size(24, 19)
        Me.clcTipo.TabIndex = 67
        Me.clcTipo.Tag = "tipo"
        Me.clcTipo.ToolTip = "tipo"
        '
        'txtFiller
        '
        Me.txtFiller.EditValue = ""
        Me.txtFiller.Location = New System.Drawing.Point(416, 72)
        Me.txtFiller.Name = "txtFiller"
        '
        'txtFiller.Properties
        '
        Me.txtFiller.Properties.Enabled = False
        Me.txtFiller.Properties.MaxLength = 7
        Me.txtFiller.Size = New System.Drawing.Size(64, 20)
        Me.txtFiller.TabIndex = 68
        Me.txtFiller.Tag = "filler"
        '
        'cboTipoConvenio
        '
        Me.cboTipoConvenio.EditValue = "A"
        Me.cboTipoConvenio.Location = New System.Drawing.Point(144, 112)
        Me.cboTipoConvenio.Name = "cboTipoConvenio"
        '
        'cboTipoConvenio.Properties
        '
        Me.cboTipoConvenio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoConvenio.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Alta", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Baja", "B", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Modificado", "M", -1)})
        Me.cboTipoConvenio.Size = New System.Drawing.Size(72, 23)
        Me.cboTipoConvenio.TabIndex = 7
        Me.cboTipoConvenio.Tag = "tipo_movto"
        '
        'txtNumDocumento
        '
        Me.txtNumDocumento.EditValue = "00000000"
        Me.txtNumDocumento.Location = New System.Drawing.Point(416, 96)
        Me.txtNumDocumento.Name = "txtNumDocumento"
        '
        'txtNumDocumento.Properties
        '
        Me.txtNumDocumento.Properties.Enabled = False
        Me.txtNumDocumento.Properties.MaxLength = 7
        Me.txtNumDocumento.Size = New System.Drawing.Size(64, 20)
        Me.txtNumDocumento.TabIndex = 70
        Me.txtNumDocumento.Tag = "num_docto"
        '
        'frmEnvioInformacionConveniosDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(490, 244)
        Me.Controls.Add(Me.txtNumDocumento)
        Me.Controls.Add(Me.cboTipoConvenio)
        Me.Controls.Add(Me.txtFiller)
        Me.Controls.Add(Me.clcTipo)
        Me.Controls.Add(Me.clcMovQuincenaInicial)
        Me.Controls.Add(Me.lblMovQuincenaInicial)
        Me.Controls.Add(Me.clcMovQuincenaFinal)
        Me.Controls.Add(Me.lblQuincenaFinal)
        Me.Controls.Add(Me.lblTipoMov)
        Me.Controls.Add(Me.clcImporteMes)
        Me.Controls.Add(Me.lblImporteMes)
        Me.Controls.Add(Me.clcImporteAbonado)
        Me.Controls.Add(Me.txtConcepto)
        Me.Controls.Add(Me.lblConcepto)
        Me.Controls.Add(Me.txtRFC)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblImpQ)
        Me.Controls.Add(Me.lblRFC)
        Me.Controls.Add(Me.lblNom)
        Me.Name = "frmEnvioInformacionConveniosDetalle"
        Me.Text = "Detalles Del Convenio"
        Me.Controls.SetChildIndex(Me.lblNom, 0)
        Me.Controls.SetChildIndex(Me.lblRFC, 0)
        Me.Controls.SetChildIndex(Me.lblImpQ, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.txtRFC, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto, 0)
        Me.Controls.SetChildIndex(Me.txtConcepto, 0)
        Me.Controls.SetChildIndex(Me.clcImporteAbonado, 0)
        Me.Controls.SetChildIndex(Me.lblImporteMes, 0)
        Me.Controls.SetChildIndex(Me.clcImporteMes, 0)
        Me.Controls.SetChildIndex(Me.lblTipoMov, 0)
        Me.Controls.SetChildIndex(Me.lblQuincenaFinal, 0)
        Me.Controls.SetChildIndex(Me.clcMovQuincenaFinal, 0)
        Me.Controls.SetChildIndex(Me.lblMovQuincenaInicial, 0)
        Me.Controls.SetChildIndex(Me.clcMovQuincenaInicial, 0)
        Me.Controls.SetChildIndex(Me.clcTipo, 0)
        Me.Controls.SetChildIndex(Me.txtFiller, 0)
        Me.Controls.SetChildIndex(Me.cboTipoConvenio, 0)
        Me.Controls.SetChildIndex(Me.txtNumDocumento, 0)
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRFC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporteAbonado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporteMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMovQuincenaFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMovQuincenaInicial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFiller.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoConvenio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmDetalleConvenio_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
    End Sub

    Private Sub frmDetalleConvenio_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmEnvioInformacionConveniosDetalle_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = ValidaLongitudConcepto(Me.txtConcepto.Text)
        If Response.ErrorFound Then Exit Sub

        Response = ValidaLongitudQuincenas(True, Me.clcMovQuincenaInicial.Value.ToString())
        If Response.ErrorFound Then Exit Sub
        Response = ValidaLongitudQuincenas(False, Me.clcMovQuincenaFinal.Value.ToString())
        If Response.ErrorFound Then Exit Sub

        Response = ValidaLongitudRFC(Me.txtRFC.Text)
        If Response.ErrorFound Then Exit Sub
        Response = ValidaLongitudNombre(Me.txtNombre.Text)

    End Sub


    Private Function ValidaLongitudRFC(ByRef rfc As String) As Events
        Dim response As New Events

        If rfc.Trim.Length < 9 Then
            response.Message = "El RFC es Requerido o su longitud no es valida"
            'Else
            '    If rfc.Trim.Length < 13 Then
            '        rfc = rfc.PadRight(13, " ")
            '    End If
        End If

        Return response
    End Function


    Private Function ValidaLongitudConcepto(ByRef concepto As String) As Events
        Dim response As New Events

        If concepto.Trim.Length = 0 Then
            response.Message = "El concepto es Requerido"
           
        End If

        Return response
    End Function
    Private Function ValidaLongitudNombre(ByRef nombre As String) As Events
        Dim response As New Events

        If nombre.Trim.Length = 0 Then
            response.Message = "El concepto es Requerido"
           
        End If

        Return response
    End Function

    Private Function ValidaLongitudQuincenas(ByVal inicial As Boolean, ByRef quincena As String)
        Dim response As New Events
        If quincena.Trim.Length < 6 Then
            If inicial Then
                response.Message = "La quincena inicial debe tener 6 d�gitos"
            Else
                response.Message = "La quincena final debe tener 6 d�gitos"
            End If
        End If

        Return response
    End Function



End Class
