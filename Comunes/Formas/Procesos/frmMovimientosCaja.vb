Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Imports System.IO
Imports System.Decimal
Imports DataDynamics


Public Class frmMovimientosCaja
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblTipoventa As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents grMovimientos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvMovimientos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcChecar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkChecar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit

    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblIntereses As System.Windows.Forms.Label
    Friend WithEvents clcIntereses As Dipros.Editors.TINCalcEdit
    Friend WithEvents grFormaspago As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFormaspago As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaFormaspago As Dipros.Windows.TINMaster
    Friend WithEvents grcClaveFormapago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFormapago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcManejaDolares As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDolares As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkManejadolares As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcTipocambio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcInteres As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents clcMonto As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcBonificar As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtconcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tlbrGuardar As System.Windows.Forms.ToolBarButton
    Friend WithEvents rclcImporte As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcDocumentos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cboTipoCobro As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkAbonoProximo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcImporteDocumentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents grcPredeterminada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkPredeterminada As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents lblNoRecibirPagos As System.Windows.Forms.Label
    Friend WithEvents grcenajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfactor_enajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnVerArticulosFactura As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnLlamadasCliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grcUltimoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNotaCargo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPermitePagarUltimoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnCambioCheques As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents clcClaveCliente As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblMensajePedidoFabrica As System.Windows.Forms.Label
    Friend WithEvents lblMensajeGarantias As System.Windows.Forms.Label
    Friend WithEvents lblMensajeMercanciaSinEntregar As System.Windows.Forms.Label
    Friend WithEvents tlbrRefrescar As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblMensajeNotasCargoPendientesPagar As System.Windows.Forms.Label
    Friend WithEvents lbldomicilio As System.Windows.Forms.Label
    Friend WithEvents lblNumeroCliente As System.Windows.Forms.Label
    Friend WithEvents grcCobrador As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcsolicita_ultimos_digitos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTieneIvaDesglosado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcManejaPromocion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDiasVencidos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMismosDoctosVentaPlan As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosCaja))
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboTipoCobro = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.grMovimientos = New DevExpress.XtraGrid.GridControl
        Me.grvMovimientos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcChecar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkChecar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcInteres = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumentos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcenajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfactor_enajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUltimoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNotaCargo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPermitePagarUltimoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCobrador = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTieneIvaDesglosado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcManejaPromocion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDiasVencidos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblIntereses = New System.Windows.Forms.Label
        Me.clcIntereses = New Dipros.Editors.TINCalcEdit
        Me.grFormaspago = New DevExpress.XtraGrid.GridControl
        Me.grvFormaspago = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveFormapago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFormapago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcManejaDolares = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkManejadolares = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcTipocambio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDolares = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rclcImporte = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPredeterminada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkPredeterminada = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcsolicita_ultimos_digitos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaFormaspago = New Dipros.Windows.TINMaster
        Me.Label7 = New System.Windows.Forms.Label
        Me.clcMonto = New Dipros.Editors.TINCalcEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.clcBonificar = New Dipros.Editors.TINCalcEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtconcepto = New DevExpress.XtraEditors.TextEdit
        Me.tlbrGuardar = New System.Windows.Forms.ToolBarButton
        Me.chkAbonoProximo = New DevExpress.XtraEditors.CheckEdit
        Me.clcImporteDocumentos = New Dipros.Editors.TINCalcEdit
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblNoRecibirPagos = New System.Windows.Forms.Label
        Me.btnVerArticulosFactura = New DevExpress.XtraEditors.SimpleButton
        Me.btnLlamadasCliente = New DevExpress.XtraEditors.SimpleButton
        Me.btnCambioCheques = New DevExpress.XtraEditors.SimpleButton
        Me.clcClaveCliente = New Dipros.Editors.TINCalcEdit
        Me.lblMensajePedidoFabrica = New System.Windows.Forms.Label
        Me.lblMensajeGarantias = New System.Windows.Forms.Label
        Me.lblMensajeMercanciaSinEntregar = New System.Windows.Forms.Label
        Me.tlbrRefrescar = New System.Windows.Forms.ToolBarButton
        Me.lblMensajeNotasCargoPendientesPagar = New System.Windows.Forms.Label
        Me.lbldomicilio = New System.Windows.Forms.Label
        Me.lblNumeroCliente = New System.Windows.Forms.Label
        Me.grcMismosDoctosVentaPlan = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.cboTipoCobro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkChecar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grFormaspago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFormaspago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManejadolares, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rclcImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPredeterminada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMonto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcBonificar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtconcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbonoProximo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporteDocumentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcClaveCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tlbrGuardar, Me.tlbrRefrescar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(30708, 28)
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(6, 174)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(86, 16)
        Me.lblTipoventa.TabIndex = 25
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "T&ipo de Cobro:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoCobro
        '
        Me.cboTipoCobro.EditValue = "A"
        Me.cboTipoCobro.Location = New System.Drawing.Point(97, 171)
        Me.cboTipoCobro.Name = "cboTipoCobro"
        '
        'cboTipoCobro.Properties
        '
        Me.cboTipoCobro.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoCobro.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Abono", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enganche", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pago a Nota Cargo", "N", -1)})
        Me.cboTipoCobro.Size = New System.Drawing.Size(127, 23)
        Me.cboTipoCobro.TabIndex = 26
        Me.cboTipoCobro.Tag = ""
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(45, 35)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(152, 32)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(800, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(248, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 2
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = "Seleccione un cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'grMovimientos
        '
        Me.grMovimientos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        'grMovimientos.EmbeddedNavigator
        '
        Me.grMovimientos.EmbeddedNavigator.Name = ""
        Me.grMovimientos.Location = New System.Drawing.Point(8, 195)
        Me.grMovimientos.MainView = Me.grvMovimientos
        Me.grMovimientos.Name = "grMovimientos"
        Me.grMovimientos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkChecar})
        Me.grMovimientos.Size = New System.Drawing.Size(752, 112)
        Me.grMovimientos.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientos.TabIndex = 31
        Me.grMovimientos.TabStop = False
        Me.grMovimientos.Text = "GridControl1"
        '
        'grvMovimientos
        '
        Me.grvMovimientos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcChecar, Me.grcFecha, Me.grcDocumento, Me.grcSaldoDocumento, Me.grcSaldoVenta, Me.grcSucursal, Me.grcConcepto, Me.grcSerie, Me.grcFolio, Me.grcCliente, Me.grcInteres, Me.grcDocumentos, Me.grcenajenacion, Me.grcfactor_enajenacion, Me.grcNombreSucursal, Me.grcUltimoDocumento, Me.grcNotaCargo, Me.grcPermitePagarUltimoDocumento, Me.grcCobrador, Me.grcTieneIvaDesglosado, Me.grcManejaPromocion, Me.grcDiasVencidos, Me.grcMismosDoctosVentaPlan})
        Me.grvMovimientos.GridControl = Me.grMovimientos
        Me.grvMovimientos.GroupPanelText = "Precios"
        Me.grvMovimientos.Name = "grvMovimientos"
        Me.grvMovimientos.OptionsView.ShowGroupPanel = False
        Me.grvMovimientos.OptionsView.ShowIndicator = False
        '
        'grcChecar
        '
        Me.grcChecar.Caption = "Incluir"
        Me.grcChecar.ColumnEdit = Me.chkChecar
        Me.grcChecar.FieldName = "checar"
        Me.grcChecar.Name = "grcChecar"
        Me.grcChecar.Options = CType((DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcChecar.VisibleIndex = 0
        Me.grcChecar.Width = 76
        '
        'chkChecar
        '
        Me.chkChecar.AutoHeight = False
        Me.chkChecar.Name = "chkChecar"
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 4
        Me.grcFecha.Width = 82
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Documento"
        Me.grcDocumento.DisplayFormat.FormatString = "n"
        Me.grcDocumento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDocumento.FieldName = "documento"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.Width = 74
        '
        'grcSaldoDocumento
        '
        Me.grcSaldoDocumento.Caption = "Saldo Documento"
        Me.grcSaldoDocumento.DisplayFormat.FormatString = "c2"
        Me.grcSaldoDocumento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoDocumento.FieldName = "saldo_documento"
        Me.grcSaldoDocumento.Name = "grcSaldoDocumento"
        Me.grcSaldoDocumento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoDocumento.VisibleIndex = 8
        Me.grcSaldoDocumento.Width = 104
        '
        'grcSaldoVenta
        '
        Me.grcSaldoVenta.Caption = "Saldo Venta"
        Me.grcSaldoVenta.DisplayFormat.FormatString = "c2"
        Me.grcSaldoVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoVenta.FieldName = "saldo_venta"
        Me.grcSaldoVenta.Name = "grcSaldoVenta"
        Me.grcSaldoVenta.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoVenta.VisibleIndex = 6
        Me.grcSaldoVenta.Width = 78
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto Factura"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.Width = 107
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie Factura"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.VisibleIndex = 2
        Me.grcSerie.Width = 83
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio Factura"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 3
        Me.grcFolio.Width = 81
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcInteres
        '
        Me.grcInteres.Caption = "Interes"
        Me.grcInteres.DisplayFormat.FormatString = "c2"
        Me.grcInteres.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcInteres.FieldName = "interes"
        Me.grcInteres.Name = "grcInteres"
        Me.grcInteres.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcInteres.VisibleIndex = 7
        Me.grcInteres.Width = 65
        '
        'grcDocumentos
        '
        Me.grcDocumentos.Caption = "Documento"
        Me.grcDocumentos.FieldName = "documentos"
        Me.grcDocumentos.Name = "grcDocumentos"
        Me.grcDocumentos.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumentos.VisibleIndex = 5
        '
        'grcenajenacion
        '
        Me.grcenajenacion.Caption = "enajenacion"
        Me.grcenajenacion.FieldName = "enajenacion"
        Me.grcenajenacion.Name = "grcenajenacion"
        Me.grcenajenacion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcfactor_enajenacion
        '
        Me.grcfactor_enajenacion.Caption = "factor_enajenacion"
        Me.grcfactor_enajenacion.FieldName = "factor_enajenacion"
        Me.grcfactor_enajenacion.Name = "grcfactor_enajenacion"
        Me.grcfactor_enajenacion.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursal.VisibleIndex = 1
        '
        'grcUltimoDocumento
        '
        Me.grcUltimoDocumento.Caption = "Ultimo Documento"
        Me.grcUltimoDocumento.ColumnEdit = Me.chkChecar
        Me.grcUltimoDocumento.FieldName = "ultima_letra"
        Me.grcUltimoDocumento.Name = "grcUltimoDocumento"
        Me.grcUltimoDocumento.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNotaCargo
        '
        Me.grcNotaCargo.Caption = "Nota Cargo"
        Me.grcNotaCargo.FieldName = "nota_cargo_descuento_anticipado"
        Me.grcNotaCargo.Name = "grcNotaCargo"
        Me.grcNotaCargo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcPermitePagarUltimoDocumento
        '
        Me.grcPermitePagarUltimoDocumento.Caption = "Permite Pagar Ultimo Documento"
        Me.grcPermitePagarUltimoDocumento.ColumnEdit = Me.chkChecar
        Me.grcPermitePagarUltimoDocumento.FieldName = "permite_pagar_ultimo_documento"
        Me.grcPermitePagarUltimoDocumento.Name = "grcPermitePagarUltimoDocumento"
        Me.grcPermitePagarUltimoDocumento.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCobrador
        '
        Me.grcCobrador.Caption = "Cobrador"
        Me.grcCobrador.FieldName = "cobrador"
        Me.grcCobrador.Name = "grcCobrador"
        Me.grcCobrador.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcTieneIvaDesglosado
        '
        Me.grcTieneIvaDesglosado.Caption = "TieneIvaDesglosado"
        Me.grcTieneIvaDesglosado.ColumnEdit = Me.chkChecar
        Me.grcTieneIvaDesglosado.FieldName = "ivadesglosado"
        Me.grcTieneIvaDesglosado.Name = "grcTieneIvaDesglosado"
        Me.grcTieneIvaDesglosado.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcManejaPromocion
        '
        Me.grcManejaPromocion.Caption = "ManejaPromocion"
        Me.grcManejaPromocion.ColumnEdit = Me.chkChecar
        Me.grcManejaPromocion.FieldName = "maneja_promocion"
        Me.grcManejaPromocion.Name = "grcManejaPromocion"
        Me.grcManejaPromocion.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDiasVencidos
        '
        Me.grcDiasVencidos.Caption = "Dias Vencido"
        Me.grcDiasVencidos.FieldName = "dias_vencidos"
        Me.grcDiasVencidos.Name = "grcDiasVencidos"
        Me.grcDiasVencidos.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(33, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 16)
        Me.Label2.TabIndex = 5
        Me.Label2.Tag = ""
        Me.Label2.Text = "Domicilio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(32, 126)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 19
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = False
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(97, 123)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = ""
        Me.lkpCobrador.PopupWidth = CType(400, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = False
        Me.lkpCobrador.Size = New System.Drawing.Size(300, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 20
        Me.lkpCobrador.Tag = ""
        Me.lkpCobrador.ToolTip = Nothing
        Me.lkpCobrador.ValueMember = "cobrador"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(627, 35)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 3
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(672, 32)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy "
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha.TabIndex = 4
        Me.dteFecha.Tag = ""
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(598, 315)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 32
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(656, 315)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcImporte.Size = New System.Drawing.Size(104, 19)
        Me.clcImporte.TabIndex = 33
        Me.clcImporte.Tag = ""
        '
        'lblIntereses
        '
        Me.lblIntereses.AutoSize = True
        Me.lblIntereses.Location = New System.Drawing.Point(590, 363)
        Me.lblIntereses.Name = "lblIntereses"
        Me.lblIntereses.Size = New System.Drawing.Size(61, 16)
        Me.lblIntereses.TabIndex = 36
        Me.lblIntereses.Tag = ""
        Me.lblIntereses.Text = "In&tereses:"
        Me.lblIntereses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIntereses
        '
        Me.clcIntereses.EditValue = "0"
        Me.clcIntereses.Location = New System.Drawing.Point(656, 363)
        Me.clcIntereses.MaxValue = 0
        Me.clcIntereses.MinValue = 0
        Me.clcIntereses.Name = "clcIntereses"
        '
        'clcIntereses.Properties
        '
        Me.clcIntereses.Properties.DisplayFormat.FormatString = "c2"
        Me.clcIntereses.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.Enabled = False
        Me.clcIntereses.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcIntereses.Properties.Precision = 2
        Me.clcIntereses.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIntereses.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcIntereses.Size = New System.Drawing.Size(104, 19)
        Me.clcIntereses.TabIndex = 37
        Me.clcIntereses.Tag = ""
        '
        'grFormaspago
        '
        Me.grFormaspago.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'grFormaspago.EmbeddedNavigator
        '
        Me.grFormaspago.EmbeddedNavigator.Name = ""
        Me.grFormaspago.Location = New System.Drawing.Point(8, 313)
        Me.grFormaspago.MainView = Me.grvFormaspago
        Me.grFormaspago.Name = "grFormaspago"
        Me.grFormaspago.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkManejadolares, Me.rclcImporte, Me.chkPredeterminada})
        Me.grFormaspago.Size = New System.Drawing.Size(512, 125)
        Me.grFormaspago.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFormaspago.TabIndex = 42
        Me.grFormaspago.TabStop = False
        '
        'grvFormaspago
        '
        Me.grvFormaspago.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveFormapago, Me.grcFormapago, Me.grcManejaDolares, Me.grcTipocambio, Me.grcDolares, Me.grcImporte, Me.grcPredeterminada, Me.grcsolicita_ultimos_digitos})
        Me.grvFormaspago.GridControl = Me.grFormaspago
        Me.grvFormaspago.GroupPanelText = "Precios"
        Me.grvFormaspago.Name = "grvFormaspago"
        Me.grvFormaspago.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvFormaspago.OptionsView.ShowFooter = True
        Me.grvFormaspago.OptionsView.ShowGroupPanel = False
        Me.grvFormaspago.OptionsView.ShowIndicator = False
        '
        'grcClaveFormapago
        '
        Me.grcClaveFormapago.Caption = "Clave Bodega"
        Me.grcClaveFormapago.FieldName = "forma_pago"
        Me.grcClaveFormapago.Name = "grcClaveFormapago"
        Me.grcClaveFormapago.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFormapago
        '
        Me.grcFormapago.Caption = "Forma pago"
        Me.grcFormapago.FieldName = "descripcion"
        Me.grcFormapago.Name = "grcFormapago"
        Me.grcFormapago.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFormapago.VisibleIndex = 0
        Me.grcFormapago.Width = 105
        '
        'grcManejaDolares
        '
        Me.grcManejaDolares.Caption = "Maneja d�lares"
        Me.grcManejaDolares.ColumnEdit = Me.chkManejadolares
        Me.grcManejaDolares.FieldName = "maneja_dolares"
        Me.grcManejaDolares.Name = "grcManejaDolares"
        Me.grcManejaDolares.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcManejaDolares.Width = 90
        '
        'chkManejadolares
        '
        Me.chkManejadolares.AutoHeight = False
        Me.chkManejadolares.Name = "chkManejadolares"
        '
        'grcTipocambio
        '
        Me.grcTipocambio.Caption = "Tipo de Cambio"
        Me.grcTipocambio.DisplayFormat.FormatString = "c2"
        Me.grcTipocambio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcTipocambio.FieldName = "tipo_cambio"
        Me.grcTipocambio.Name = "grcTipocambio"
        Me.grcTipocambio.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipocambio.VisibleIndex = 1
        Me.grcTipocambio.Width = 89
        '
        'grcDolares
        '
        Me.grcDolares.Caption = "D�lares"
        Me.grcDolares.ColumnEdit = Me.rclcImporte
        Me.grcDolares.DisplayFormat.FormatString = "c2"
        Me.grcDolares.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDolares.FieldName = "dolares"
        Me.grcDolares.Name = "grcDolares"
        Me.grcDolares.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDolares.VisibleIndex = 2
        Me.grcDolares.Width = 66
        '
        'rclcImporte
        '
        Me.rclcImporte.AutoHeight = False
        Me.rclcImporte.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, False, False, DevExpress.Utils.HorzAlignment.Center, Nothing)})
        Me.rclcImporte.DisplayFormat.FormatString = "c2"
        Me.rclcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rclcImporte.EditFormat.FormatString = "n2"
        Me.rclcImporte.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rclcImporte.Name = "rclcImporte"
        Me.rclcImporte.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never
        Me.rclcImporte.ShowPopupShadow = False
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.ColumnEdit = Me.rclcImporte
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.SummaryItem.DisplayFormat = "{0:c2}"
        Me.grcImporte.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporte.SummaryItem.Tag = "importe"
        Me.grcImporte.VisibleIndex = 3
        Me.grcImporte.Width = 67
        '
        'grcPredeterminada
        '
        Me.grcPredeterminada.Caption = "Predeterminada"
        Me.grcPredeterminada.ColumnEdit = Me.chkPredeterminada
        Me.grcPredeterminada.FieldName = "predeterminada"
        Me.grcPredeterminada.Name = "grcPredeterminada"
        Me.grcPredeterminada.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPredeterminada.Width = 91
        '
        'chkPredeterminada
        '
        Me.chkPredeterminada.AutoHeight = False
        Me.chkPredeterminada.Name = "chkPredeterminada"
        '
        'grcsolicita_ultimos_digitos
        '
        Me.grcsolicita_ultimos_digitos.Caption = "solicita_ultimos_digitos"
        Me.grcsolicita_ultimos_digitos.ColumnEdit = Me.chkPredeterminada
        Me.grcsolicita_ultimos_digitos.FieldName = "solicitar_ulitmos_digitos"
        Me.grcsolicita_ultimos_digitos.Name = "grcsolicita_ultimos_digitos"
        Me.grcsolicita_ultimos_digitos.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcsolicita_ultimos_digitos.VisibleIndex = 4
        '
        'tmaFormaspago
        '
        Me.tmaFormaspago.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tmaFormaspago.BackColor = System.Drawing.Color.White
        Me.tmaFormaspago.CanDelete = True
        Me.tmaFormaspago.CanInsert = True
        Me.tmaFormaspago.CanUpdate = True
        Me.tmaFormaspago.Grid = Me.grFormaspago
        Me.tmaFormaspago.Location = New System.Drawing.Point(8, 392)
        Me.tmaFormaspago.Name = "tmaFormaspago"
        Me.tmaFormaspago.Size = New System.Drawing.Size(424, 27)
        Me.tmaFormaspago.TabIndex = 43
        Me.tmaFormaspago.Title = "Formas de Pago"
        Me.tmaFormaspago.UpdateTitle = "un Registro"
        Me.tmaFormaspago.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(562, 416)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(89, 16)
        Me.Label7.TabIndex = 40
        Me.Label7.Tag = ""
        Me.Label7.Text = "Monto a Pa&gar:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMonto
        '
        Me.clcMonto.EditValue = "0"
        Me.clcMonto.Location = New System.Drawing.Point(656, 418)
        Me.clcMonto.MaxValue = 0
        Me.clcMonto.MinValue = 0
        Me.clcMonto.Name = "clcMonto"
        '
        'clcMonto.Properties
        '
        Me.clcMonto.Properties.DisplayFormat.FormatString = "c2"
        Me.clcMonto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMonto.Properties.Precision = 2
        Me.clcMonto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMonto.Size = New System.Drawing.Size(104, 19)
        Me.clcMonto.TabIndex = 41
        Me.clcMonto.Tag = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(529, 387)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(122, 16)
        Me.Label8.TabIndex = 38
        Me.Label8.Tag = ""
        Me.Label8.Text = "&Intereses a bonificar:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcBonificar
        '
        Me.clcBonificar.EditValue = "0"
        Me.clcBonificar.Location = New System.Drawing.Point(656, 387)
        Me.clcBonificar.MaxValue = 0
        Me.clcBonificar.MinValue = 0
        Me.clcBonificar.Name = "clcBonificar"
        '
        'clcBonificar.Properties
        '
        Me.clcBonificar.Properties.DisplayFormat.FormatString = "c2"
        Me.clcBonificar.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBonificar.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBonificar.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcBonificar.Properties.Precision = 2
        Me.clcBonificar.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBonificar.Size = New System.Drawing.Size(104, 19)
        Me.clcBonificar.TabIndex = 39
        Me.clcBonificar.Tag = ""
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(32, 150)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 16)
        Me.Label9.TabIndex = 22
        Me.Label9.Tag = ""
        Me.Label9.Text = "Co&ncepto:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtconcepto
        '
        Me.txtconcepto.EditValue = ""
        Me.txtconcepto.Location = New System.Drawing.Point(97, 147)
        Me.txtconcepto.Name = "txtconcepto"
        '
        'txtconcepto.Properties
        '
        Me.txtconcepto.Properties.Enabled = False
        Me.txtconcepto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtconcepto.Size = New System.Drawing.Size(300, 20)
        Me.txtconcepto.TabIndex = 23
        Me.txtconcepto.Tag = ""
        '
        'tlbrGuardar
        '
        Me.tlbrGuardar.ImageIndex = 0
        Me.tlbrGuardar.Text = "Aceptar"
        '
        'chkAbonoProximo
        '
        Me.chkAbonoProximo.Location = New System.Drawing.Point(240, 173)
        Me.chkAbonoProximo.Name = "chkAbonoProximo"
        '
        'chkAbonoProximo.Properties
        '
        Me.chkAbonoProximo.Properties.AllowFocused = False
        Me.chkAbonoProximo.Properties.Caption = "Pr�ximo A&bono"
        Me.chkAbonoProximo.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkAbonoProximo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkAbonoProximo.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.InactiveCaption)
        Me.chkAbonoProximo.Size = New System.Drawing.Size(116, 20)
        Me.chkAbonoProximo.TabIndex = 27
        Me.chkAbonoProximo.Tag = ""
        '
        'clcImporteDocumentos
        '
        Me.clcImporteDocumentos.EditValue = "0"
        Me.clcImporteDocumentos.Location = New System.Drawing.Point(656, 339)
        Me.clcImporteDocumentos.MaxValue = 0
        Me.clcImporteDocumentos.MinValue = 0
        Me.clcImporteDocumentos.Name = "clcImporteDocumentos"
        '
        'clcImporteDocumentos.Properties
        '
        Me.clcImporteDocumentos.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporteDocumentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteDocumentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteDocumentos.Properties.Enabled = False
        Me.clcImporteDocumentos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporteDocumentos.Properties.Precision = 2
        Me.clcImporteDocumentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporteDocumentos.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcImporteDocumentos.Size = New System.Drawing.Size(104, 19)
        Me.clcImporteDocumentos.TabIndex = 35
        Me.clcImporteDocumentos.Tag = ""
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(536, 339)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(115, 16)
        Me.Label10.TabIndex = 34
        Me.Label10.Tag = ""
        Me.Label10.Text = "&Monto Documentos:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNoRecibirPagos
        '
        Me.lblNoRecibirPagos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNoRecibirPagos.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoRecibirPagos.ForeColor = System.Drawing.Color.Red
        Me.lblNoRecibirPagos.Location = New System.Drawing.Point(416, 56)
        Me.lblNoRecibirPagos.Name = "lblNoRecibirPagos"
        Me.lblNoRecibirPagos.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNoRecibirPagos.Size = New System.Drawing.Size(344, 32)
        Me.lblNoRecibirPagos.TabIndex = 7
        Me.lblNoRecibirPagos.Text = "NO  RECIBIR PAGOS, EL CLIENTE TIENE UN PROCESO JUR�DICO."
        Me.lblNoRecibirPagos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblNoRecibirPagos.Visible = False
        '
        'btnVerArticulosFactura
        '
        Me.btnVerArticulosFactura.Location = New System.Drawing.Point(416, 168)
        Me.btnVerArticulosFactura.Name = "btnVerArticulosFactura"
        Me.btnVerArticulosFactura.Size = New System.Drawing.Size(104, 23)
        Me.btnVerArticulosFactura.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText)
        Me.btnVerArticulosFactura.TabIndex = 28
        Me.btnVerArticulosFactura.Text = "Ver Articulos"
        '
        'btnLlamadasCliente
        '
        Me.btnLlamadasCliente.Location = New System.Drawing.Point(525, 168)
        Me.btnLlamadasCliente.Name = "btnLlamadasCliente"
        Me.btnLlamadasCliente.Size = New System.Drawing.Size(104, 23)
        Me.btnLlamadasCliente.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText)
        Me.btnLlamadasCliente.TabIndex = 29
        Me.btnLlamadasCliente.Text = "Llamadas"
        '
        'btnCambioCheques
        '
        Me.btnCambioCheques.Location = New System.Drawing.Point(632, 168)
        Me.btnCambioCheques.Name = "btnCambioCheques"
        Me.btnCambioCheques.Size = New System.Drawing.Size(128, 23)
        Me.btnCambioCheques.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText)
        Me.btnCambioCheques.TabIndex = 30
        Me.btnCambioCheques.Text = "Cambio de Cheques"
        '
        'clcClaveCliente
        '
        Me.clcClaveCliente.EditValue = "0"
        Me.clcClaveCliente.Location = New System.Drawing.Point(96, 32)
        Me.clcClaveCliente.MaxValue = 0
        Me.clcClaveCliente.MinValue = 0
        Me.clcClaveCliente.Name = "clcClaveCliente"
        '
        'clcClaveCliente.Properties
        '
        Me.clcClaveCliente.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcClaveCliente.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcClaveCliente.Properties.Enabled = False
        Me.clcClaveCliente.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcClaveCliente.Properties.Precision = 2
        Me.clcClaveCliente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcClaveCliente.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcClaveCliente.Size = New System.Drawing.Size(40, 19)
        Me.clcClaveCliente.TabIndex = 1
        Me.clcClaveCliente.Tag = ""
        '
        'lblMensajePedidoFabrica
        '
        Me.lblMensajePedidoFabrica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMensajePedidoFabrica.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajePedidoFabrica.ForeColor = System.Drawing.Color.Blue
        Me.lblMensajePedidoFabrica.Location = New System.Drawing.Point(416, 145)
        Me.lblMensajePedidoFabrica.Name = "lblMensajePedidoFabrica"
        Me.lblMensajePedidoFabrica.Size = New System.Drawing.Size(344, 19)
        Me.lblMensajePedidoFabrica.TabIndex = 24
        Me.lblMensajePedidoFabrica.Text = "LA FACTURA TIENE  PEDIDO A FABRICA"
        Me.lblMensajePedidoFabrica.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMensajePedidoFabrica.Visible = False
        '
        'lblMensajeGarantias
        '
        Me.lblMensajeGarantias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMensajeGarantias.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeGarantias.ForeColor = System.Drawing.Color.Blue
        Me.lblMensajeGarantias.Location = New System.Drawing.Point(416, 107)
        Me.lblMensajeGarantias.Name = "lblMensajeGarantias"
        Me.lblMensajeGarantias.Size = New System.Drawing.Size(344, 19)
        Me.lblMensajeGarantias.TabIndex = 18
        Me.lblMensajeGarantias.Text = "LA FACTURA TIENE ORD. DE SERV.  PENDIENTES"
        Me.lblMensajeGarantias.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMensajeGarantias.Visible = False
        '
        'lblMensajeMercanciaSinEntregar
        '
        Me.lblMensajeMercanciaSinEntregar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMensajeMercanciaSinEntregar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeMercanciaSinEntregar.ForeColor = System.Drawing.Color.Blue
        Me.lblMensajeMercanciaSinEntregar.Location = New System.Drawing.Point(416, 126)
        Me.lblMensajeMercanciaSinEntregar.Name = "lblMensajeMercanciaSinEntregar"
        Me.lblMensajeMercanciaSinEntregar.Size = New System.Drawing.Size(344, 19)
        Me.lblMensajeMercanciaSinEntregar.TabIndex = 21
        Me.lblMensajeMercanciaSinEntregar.Text = "LA FACTURA TIENE MERCANCIA SIN ENTREGAR"
        Me.lblMensajeMercanciaSinEntregar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMensajeMercanciaSinEntregar.Visible = False
        '
        'tlbrRefrescar
        '
        Me.tlbrRefrescar.Enabled = False
        Me.tlbrRefrescar.Text = "Refrescar"
        Me.tlbrRefrescar.ToolTipText = "Refrescar la Informaci�n  del Cliente"
        '
        'lblMensajeNotasCargoPendientesPagar
        '
        Me.lblMensajeNotasCargoPendientesPagar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMensajeNotasCargoPendientesPagar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeNotasCargoPendientesPagar.ForeColor = System.Drawing.Color.Red
        Me.lblMensajeNotasCargoPendientesPagar.Location = New System.Drawing.Point(416, 88)
        Me.lblMensajeNotasCargoPendientesPagar.Name = "lblMensajeNotasCargoPendientesPagar"
        Me.lblMensajeNotasCargoPendientesPagar.Size = New System.Drawing.Size(344, 19)
        Me.lblMensajeNotasCargoPendientesPagar.TabIndex = 59
        Me.lblMensajeNotasCargoPendientesPagar.Text = "TIENE NOTAS DE CARGO PENDIENTES DE PAGAR"
        Me.lblMensajeNotasCargoPendientesPagar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMensajeNotasCargoPendientesPagar.Visible = False
        '
        'lbldomicilio
        '
        Me.lbldomicilio.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldomicilio.Location = New System.Drawing.Point(97, 56)
        Me.lbldomicilio.Name = "lbldomicilio"
        Me.lbldomicilio.Size = New System.Drawing.Size(300, 64)
        Me.lbldomicilio.TabIndex = 107
        Me.lbldomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNumeroCliente
        '
        Me.lblNumeroCliente.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumeroCliente.Location = New System.Drawing.Point(96, 32)
        Me.lblNumeroCliente.Name = "lblNumeroCliente"
        Me.lblNumeroCliente.Size = New System.Drawing.Size(56, 19)
        Me.lblNumeroCliente.TabIndex = 113
        Me.lblNumeroCliente.Text = "0"
        Me.lblNumeroCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grcMismosDoctosVentaPlan
        '
        Me.grcMismosDoctosVentaPlan.Caption = "Mismos Documentos"
        Me.grcMismosDoctosVentaPlan.ColumnEdit = Me.chkChecar
        Me.grcMismosDoctosVentaPlan.FieldName = "mismos_documentos_venta_plan"
        Me.grcMismosDoctosVentaPlan.Name = "grcMismosDoctosVentaPlan"
        Me.grcMismosDoctosVentaPlan.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMismosDoctosVentaPlan.VisibleIndex = 9
        '
        'frmMovimientosCaja
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(770, 440)
        Me.Controls.Add(Me.lblNumeroCliente)
        Me.Controls.Add(Me.lbldomicilio)
        Me.Controls.Add(Me.lblMensajeNotasCargoPendientesPagar)
        Me.Controls.Add(Me.lblMensajeMercanciaSinEntregar)
        Me.Controls.Add(Me.lblMensajeGarantias)
        Me.Controls.Add(Me.lblMensajePedidoFabrica)
        Me.Controls.Add(Me.clcClaveCliente)
        Me.Controls.Add(Me.btnCambioCheques)
        Me.Controls.Add(Me.btnVerArticulosFactura)
        Me.Controls.Add(Me.lblNoRecibirPagos)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.clcImporteDocumentos)
        Me.Controls.Add(Me.chkAbonoProximo)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtconcepto)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.clcMonto)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.grFormaspago)
        Me.Controls.Add(Me.clcBonificar)
        Me.Controls.Add(Me.tmaFormaspago)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lblIntereses)
        Me.Controls.Add(Me.clcIntereses)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.lkpCobrador)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grMovimientos)
        Me.Controls.Add(Me.lblTipoventa)
        Me.Controls.Add(Me.cboTipoCobro)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.btnLlamadasCliente)
        Me.Name = "frmMovimientosCaja"
        Me.Text = "frmMovimientosCaja"
        Me.Controls.SetChildIndex(Me.btnLlamadasCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.cboTipoCobro, 0)
        Me.Controls.SetChildIndex(Me.lblTipoventa, 0)
        Me.Controls.SetChildIndex(Me.grMovimientos, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcIntereses, 0)
        Me.Controls.SetChildIndex(Me.lblIntereses, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.tmaFormaspago, 0)
        Me.Controls.SetChildIndex(Me.clcBonificar, 0)
        Me.Controls.SetChildIndex(Me.grFormaspago, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.clcMonto, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.txtconcepto, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        Me.Controls.SetChildIndex(Me.chkAbonoProximo, 0)
        Me.Controls.SetChildIndex(Me.clcImporteDocumentos, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.lblNoRecibirPagos, 0)
        Me.Controls.SetChildIndex(Me.btnVerArticulosFactura, 0)
        Me.Controls.SetChildIndex(Me.btnCambioCheques, 0)
        Me.Controls.SetChildIndex(Me.clcClaveCliente, 0)
        Me.Controls.SetChildIndex(Me.lblMensajePedidoFabrica, 0)
        Me.Controls.SetChildIndex(Me.lblMensajeGarantias, 0)
        Me.Controls.SetChildIndex(Me.lblMensajeMercanciaSinEntregar, 0)
        Me.Controls.SetChildIndex(Me.lblMensajeNotasCargoPendientesPagar, 0)
        Me.Controls.SetChildIndex(Me.lbldomicilio, 0)
        Me.Controls.SetChildIndex(Me.lblNumeroCliente, 0)
        CType(Me.cboTipoCobro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkChecar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grFormaspago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFormaspago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManejadolares, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rclcImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPredeterminada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMonto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcBonificar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtconcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbonoProximo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporteDocumentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcClaveCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Public ConceptoAbono As String
    Public ConceptoNotacredito As String
    Public ConceptoNotacargo As String
    Public Conceptointeres As String
    Public CobradorCasa As Long
    Public FormaPagoVales As Long
    Private ConceptoPromocion As String
    Private ImpuestoVariables As Double = 0.0

    Private banConceptoAbono As Boolean = False
    Private banValidar As Boolean = False
    Public banCobradorCasa As Boolean = False

    Private banConceptoNotacredito As Boolean = False
    Private banConceptoNotacargo As Boolean = False
    Private banConceptoInteres As Boolean = False
    Private banSalir As Boolean = False
    Private banConceptoNCGastosAdministrativos As Boolean = False
    'Private banPermiteCambiarTipoPago As Boolean = False
    Private banFormaPagoVales As Boolean = False
    Private banImpuestoVariables As Boolean = False
    Private banConceptoPromocion As Boolean = False

    Private subtotal As Double = 0
    Private iva As Double = 0
    Private folio As Long = 0
    Private concepto As String = ""
    Private conceptointereses As String = ""
    Private serie As String = ""
    Private dImportencr As Double = 0

    Private bReciboImpreso As Boolean
    Private banderanotacargo As Boolean = False
    Private serieinteres As String = ""
    Private foliointeres As Long = 0
    Private saldo_intereses As Double = 0

    Private Modificado_monto As Boolean = False

    Private oMovimientosCobrar As New VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As New VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oMovimientosCobrar_formaspago As New VillarrealBusiness.clsMovimientosCobrarFormasPago
    Private oVentas As New VillarrealBusiness.clsVentas
    Private oClientes As VillarrealBusiness.clsClientes
    Private oCobradores As VillarrealBusiness.clsCobradores
    Private oVariables As VillarrealBusiness.clsVariables
    Private oReportes As VillarrealBusiness.Reportes
    Private oFormasPagos As New VillarrealBusiness.clsFormasPagos
    Private oAutorizacionesBonificacionesInteres As New VillarrealBusiness.clsAutorizacionesBonificacionesInteres
    Private oClientesCobradores As New VillarrealBusiness.clsClientesCobradores
    Private oValesDetalle As New VillarrealBusiness.clsValesDetalle
    Private oSucursales As New VillarrealBusiness.clsSucursales



    Private PorcentajeCondonacion As Double = 0
    Private oTabla As DataTable
    Private sucursal_activa As Long
    Private Folio_Bonificacion As Long = 0

    Public EntroCaja As Boolean = True

    Private FolioVale As Long = 0

    Private _BonificacionObservaciones As String = ""
    Private factura_electronica As Boolean = False
    Private Ultimos_Digitos As String = ""
    Private tipoabono As Char = ""

    Private TieneIvaDesglosadoFacturaNotaCargo As Boolean = False
    Private EsUltimoDocumento As Boolean = False
    Private ManejaPromocion As Boolean = False
    Private DiasVencidos As Integer = 0
    Private bImprimeReciboMenosManejaPromocion As Boolean = False
    Private FilaManejaPromocion As Integer = 0
    Private TieneIvaDesglosadoFactura As Boolean = False
    Private FormaPagoEfectivo As Long = 0
    Private banFormaPagoEfectivo As Boolean = False
    Private MismosDoctosVentaPlan As Boolean = False
    Private SaldoDocto As Double = 0.0
    Private ConceptoSeleccionadoDocumento As String = ""




    Private ReadOnly Property cliente() As Long
        Get
            Return PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property

    Private ReadOnly Property cobrador() As Long
        Get
            Return PreparaValorLookup(Me.lkpCobrador)
        End Get
    End Property

    Private ReadOnly Property Tipo_Cambio() As Double
        Get
            Return Comunes.clsUtilerias.uti_TipoCambio(Comunes.Common.Cajero, TinApp.FechaServidor)
        End Get
    End Property

    Private ReadOnly Property UsuarioPorcentajeCondonacionIntereses() As Double
        Get
            Return Comunes.clsUtilerias.uti_UsuarioPorcentajeCondonacionIntereses(TinApp.Connection.User)
        End Get
    End Property
    Private ReadOnly Property AbogadoLogueado() As Long
        Get
            Return Comunes.clsUtilerias.uti_AbogadoLogueado(TinApp.Connection.User)
        End Get
    End Property

    Private ReadOnly Property Sucursal_Dependencia() As Boolean
        Get
            Return Comunes.clsUtilerias.uti_SucursalDependencia(Comunes.Common.Sucursal_Actual)
        End Get
    End Property

    Private ReadOnly Property SerieNotaCargo() As String
        Get
            Return Comunes.clsUtilerias.uti_SerieCajaNotaCargo(Comunes.Common.Caja_Actual)

        End Get
    End Property

    Public WriteOnly Property BonificacionObservaciones() As String
        Set(ByVal Value As String)
            _BonificacionObservaciones = Value
        End Set
    End Property

    Public Property UltimosDigitos() As String
        Get
            If Ultimos_Digitos.Trim.Length = 0 Then
                Return "0"
            Else
                Return Ultimos_Digitos
            End If

        End Get
        Set(ByVal Value As String)
            Ultimos_Digitos = Value
        End Set
    End Property

   


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmMovimientosCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CADENA As String = ""

        If EntroCaja Then
            CADENA = "FECHA_CAJA_CAJA"
        Else
            CADENA = "FECHA_COB_CAJA"
        End If

        If VerificaPermisoExtendido(Me.MenuOption.Name, CADENA) Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If
    End Sub

    Private Sub frmMovimientosCaja_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

        oClientes = New VillarrealBusiness.clsClientes
        oCobradores = New VillarrealBusiness.clsCobradores
        oVariables = New VillarrealBusiness.clsVariables
        oReportes = New VillarrealBusiness.Reportes
        oVentas = New VillarrealBusiness.clsVentas
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar

        PorcentajeCondonacion = UsuarioPorcentajeCondonacionIntereses
        Me.Location = New System.Drawing.Point(0, 0)
        Me.tbrTools.Buttons(0).Visible = False

        ' Sucursales
        '-----------------------------
        Dim oDataSet As DataSet
        Response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        oDataSet = CType(Response.Value, DataSet)
        factura_electronica = CType(oDataSet.Tables(0).Rows(0).Item("factura_electronica"), Boolean)
        '-----------------------------



        Me.chkAbonoProximo.Checked = False
        Me.chkAbonoProximo.Enabled = False
        Me.cboTipoCobro.SelectedIndex = 1 ' ENGANCHE

        Me.clcImporteDocumentos.EditValue = 0

        Me.dteFecha.EditValue = TinApp.FechaServidor

        'Me.TraeConceptoAbono()
        'Me.TraeConceptoNotaCredito()
        'Me.TraeConceptoNotaCargo()
        'Me.TraeConceptointeres()
        'Me.TraeCobradorCasa()
        'Me.TraeFormaPagoVales()
        'Me.TraeFormaPagoEfectivo()

        Me.TraeDatosVariables()

        If Not banConceptoAbono Or Not banConceptoNotacredito Or Not Me.banConceptoNotacargo Or Not Me.banConceptoInteres Or Not Me.banCobradorCasa Or Not banFormaPagoVales Or Not banFormaPagoEfectivo Or Not banImpuestoVariables Then 'Or Not banConceptoPromocion Then
            Me.tbrTools.Buttons.Item(2).Visible = False
            Me.grMovimientos.Enabled = False
            Me.grFormaspago.Enabled = False
        End If

        With tmaFormaspago
            .UpdateTitle = "una Forma de Pago"
            .UpdateForm = New frmMovimientosCajaFormasPago
            .AddColumn("forma_pago", "System.Int32")
            .AddColumn("descripcion")
            .AddColumn("maneja_dolares", "System.Boolean")
            .AddColumn("importe", "System.Double")
            .AddColumn("dolares", "System.Double")
            .AddColumn("tipo_cambio", "System.Double")
            .AddColumn("predeterminada", "System.Boolean")
            .AddColumn("solicitar_ulitmos_digitos", "System.Boolean")

        End With
        Response = oFormasPagos.MovimientosCaja
        If Not Response.ErrorFound Then
            Me.tmaFormaspago.DataSource = Response.Value
            AgregarTipoCambio()
        End If

        'Dim OPERMISOS As New Dipros.Utils.ExtendedSecurity(Me.MenuOption.Name)
        'If OPERMISOS.Extended("CONDONAR") = False Then
        '    Me.clcBonificar.Enabled = False
        'End If


    End Sub
    Private Sub frmMovimientosCaja_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

    End Sub
    Private Sub frmMovimientosCaja_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

        Dim oEvent As New Events
        Me.grvFormaspago.UpdateCurrentRow()
        Me.grvFormaspago.CloseEditor()
        Me.grvFormaspago.UpdateTotalSummary()

        Folio_Bonificacion = ObtieneFolioAutorizacionBonificacion()

        Response = Me.oMovimientosCobrar.Validacion(Action, Me.cobrador, Me.clcMonto.EditValue, Me.clcIntereses.EditValue, IIf(CType(Me.grcImporte.SummaryItem.SummaryValue, String) = "", 0, CType(Me.grcImporte.SummaryItem.SummaryValue, Double)), Me.clcBonificar.EditValue, Me.clcImporte.EditValue, Me.lkpCliente.GetValue("no_recibir_abonos"), Me.UsuarioPorcentajeCondonacionIntereses, Folio_Bonificacion)
        If Response.ErrorFound Then
            banValidar = True
        End If
        If Me.cboTipoCobro.EditValue = "P" And Me.validaImporteProntoPago And Not Response.ErrorFound Then
            oEvent.Ex = Nothing
            oEvent.Message = "El monto a pagar es mayor que el importe de los documentos"
            oEvent.Layer = oEvent.ErrorLayer.BussinessLayer
            Response = oEvent
        End If

        If Me.cboTipoCobro.EditValue = "A" Then
            If clcMonto.EditValue > (clcImporteDocumentos.EditValue + clcIntereses.EditValue) Then
                oEvent.Ex = Nothing
                oEvent.Message = "El monto a pagar es mayor que los documentos seleccionados"
                oEvent.Layer = oEvent.ErrorLayer.BussinessLayer
                Response = oEvent
                banValidar = True
            End If
        End If
    End Sub
    Private Sub frmMovimientosCaja_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Me.lkpCliente.Focus()
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"


    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCaja()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format

        Comunes.clsFormato.for_clientes_caja_grl(Me.lkpCliente)

    End Sub
    Private Sub lkpCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.Validated
        If Me.cliente = -1 Then
            LimpiarDatosCliente()
        End If
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged

        Me.cboTipoCobro.SelectedIndex = ObtenerPrioridad(tipoabono) 'Enganche

        Me.lblMensajeGarantias.Visible = False
        Me.lblMensajeMercanciaSinEntregar.Visible = False
        Me.lblMensajePedidoFabrica.Visible = False
        Me.lblNoRecibirPagos.Visible = False

        If cliente <> -1 Then
            RefrescarInfoCliente()
            Me.tbrTools.Buttons.Item(3).Enabled = True
        Else

            LimpiarDatosCliente()
            Me.tbrTools.Buttons.Item(3).Enabled = False
        End If

        Folio_Bonificacion = 0
    End Sub

    Private Sub lkpCobrador_LoadData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData

        Dim Response As New Events
        Response = oCobradores.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub

    Private Sub cboTipoCobro_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoCobro.SelectedIndexChanged
        If Me.cboTipoCobro.SelectedIndex = 0 Then 'ABONO
            Me.chkAbonoProximo.Enabled = True
            Me.chkAbonoProximo.Checked = True
        Else
            Me.chkAbonoProximo.Enabled = False
            Me.chkAbonoProximo.Checked = False
        End If
        Llenargrid(True)
    End Sub
    Private Sub chkAbonoProximo_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAbonoProximo.EditValueChanged
        If Me.cboTipoCobro.SelectedIndex = 0 Then 'ABONO
            Llenargrid(False)
        End If

    End Sub
    Private Sub clcMonto_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcMonto.EditValueChanged
        If Me.clcImporte.IsLoading = True Or Not IsNumeric(Me.clcImporte.EditValue) Or Not IsNumeric(Me.clcMonto.EditValue) Then Exit Sub
        Dim fila_predeterminada As Long = 0
        fila_predeterminada = Me.FilaPredeterminadaFormaPago()
        calculartotales()
        AsignarMonto_FormaPago(fila_predeterminada, Me.clcMonto.EditValue)
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is tlbrRefrescar Then
            RefrescarInfoCliente()
        End If


        If e.Button Is tlbrGuardar And banValidar = False Then
            Me.grvMovimientos.UpdateCurrentRow()
            Me.grvMovimientos.CloseEditor()
            Dim ImporteVales As Double = ValorVales()


            If ImporteVales > 0 Then
                ' ============================================================
                MostrarFormaValidacionVales(ImporteVales)
                ' ============================================================
            Else
                If clcBonificar.EditValue > 0 Then
                    MostrarFormaBonificacionObservaciones()

                Else
                    MostrarFormaPagarCajaCambio(0, 0)
                End If

            End If

            ' ============================================================


        End If
        banValidar = False


    End Sub
    Private Sub grvFormaspago_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvFormaspago.CellValueChanged
        Dim j As Integer = Me.grvFormaspago.FocusedRowHandle

        If (CType(Me.grvFormaspago.GetRowCellDisplayText(j, Me.grcImporte), String) <> "$0.00") And (CType(Me.grvFormaspago.GetRowCellDisplayText(j, Me.grcImporte), String).Trim <> "") Then
            If CType(Me.grvFormaspago.GetRowCellValue(j, Me.grcImporte), Double) > 0 Then
                If ValidaIngresaVariasFormasPago(j) = True Then
                    ShowMessage(MessageType.MsgInformation, "S�lo se permite una forma de pago")
                    Me.grvFormaspago.SetRowCellValue(j, Me.grcImporte, 0)
                End If
            End If
        End If


        If e.Column.ColumnHandle = 6 And Me.grvFormaspago.GetRowCellValue(Me.grvFormaspago.FocusedRowHandle, Me.grcManejaDolares) = False And banSalir = False Then
            banSalir = True
            Me.grvFormaspago.SetRowCellValue(Me.grvFormaspago.FocusedRowHandle, Me.grcDolares, 0)
            grvFormaspago.UpdateCurrentRow()
            banSalir = False
        ElseIf (e.Column.ColumnHandle = 5 Or e.Column.ColumnHandle = 6) And Me.grvFormaspago.GetRowCellValue(Me.grvFormaspago.FocusedRowHandle, Me.grcManejaDolares) = True And banSalir = False Then
            banSalir = True
            Me.grvFormaspago.SetRowCellValue(Me.grvFormaspago.FocusedRowHandle, Me.grcImporte, Me.grvFormaspago.GetRowCellValue(Me.grvFormaspago.FocusedRowHandle, Me.grcDolares) * Me.grvFormaspago.GetRowCellValue(Me.grvFormaspago.FocusedRowHandle, Me.grcTipocambio))
            grvFormaspago.UpdateCurrentRow()
            banSalir = False
        End If

        ' grvMovimientos.CloseEditor()
    End Sub
    Private Sub grvFormaspago_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grvFormaspago.KeyDown
        Try

            If e.KeyCode = e.KeyCode.Enter Or e.KeyCode = e.KeyCode.Left Or e.KeyCode = e.KeyCode.Right Then
                Me.grvFormaspago.CloseEditor()
                Me.grvFormaspago.UpdateCurrentRow()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub grvFormaspago_FocusedColumnChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs) Handles grvFormaspago.FocusedColumnChanged
        Me.grvFormaspago.CloseEditor()
        Me.grvFormaspago.UpdateCurrentRow()
    End Sub

    Private Sub grvMovimientos_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvMovimientos.CellValueChanging
        '        Me.clcMonto.EditValue = 0
        Me.clcMonto.Enabled = True


        Dim i As Integer = 0
        Dim j As Integer = Me.grvMovimientos.FocusedRowHandle

        Dim fila_predeterminada As Long = 0
        Dim contador As Long = 0
        Dim Factura_Diferente As Boolean = False
        Dim COBRADOR_ANTERIOR As Long

        Me.grvMovimientos.UpdateCurrentRow()
        fila_predeterminada = Me.FilaPredeterminadaFormaPago()

        'Me.clcIntereses.Value = 0


        ConceptoSeleccionadoDocumento = ""


        COBRADOR_ANTERIOR = Me.lkpCobrador.EditValue

        Me.lkpCobrador.EditValue = Me.grvMovimientos.GetRowCellValue(j, Me.grcCobrador)
        TieneIvaDesglosadoFacturaNotaCargo = Me.grvMovimientos.GetRowCellValue(j, Me.grcTieneIvaDesglosado)
        ConceptoSeleccionadoDocumento = Me.grvMovimientos.GetRowCellValue(j, Me.grcConcepto)

        If Me.cboTipoCobro.EditValue <> "A" Then
            For i = 0 To Me.grvMovimientos.RowCount - 1
                If i <> j Then
                    Me.grvMovimientos.SetRowCellValue(i, Me.grcChecar, False)

                Else
                    Me.grvMovimientos.SetRowCellValue(i, Me.grcChecar, True)
                End If
            Next

            sucursal_activa = Me.grvMovimientos.GetRowCellValue(j, Me.grcSucursal)
            Me.clcIntereses.Value = Me.clcIntereses.Value + Me.grvMovimientos.GetRowCellValue(j, Me.grcInteres)
            Me.clcImporteDocumentos.EditValue = Me.grvMovimientos.GetRowCellValue(j, Me.grcSaldoDocumento)
            Me.clcMonto.EditValue = Me.clcImporteDocumentos.EditValue + Me.clcIntereses.Value - Me.clcBonificar.Value
            'ConceptoSeleccionado = Me.grvMovimientos.GetRowCellValue(j, Me.grcConcepto)

            grvMovimientos.UpdateCurrentRow()
            AsignarMonto_FormaPago(fila_predeterminada, Me.clcImporteDocumentos.EditValue + Me.clcIntereses.Value - Me.clcBonificar.Value)

        Else
            If Me.cboTipoCobro.EditValue = "A" And e.Column Is Me.grcChecar Then

                'Validacion para que no se puedan seleccionar documentos de diferentes facturas
                If Me.grvMovimientos.GetRowCellValue(j, Me.grcChecar) = False Then
                    If ValidaAbonoDiferentesFacturas(j) = True Then

                        ShowMessage(MessageType.MsgInformation, "Debe seleccionar documentos de una misma factura")
                        Me.grvMovimientos.SetRowCellValue(j, Me.grcChecar, False)
                        Factura_Diferente = True
                        Me.lkpCobrador.EditValue = COBRADOR_ANTERIOR
                        Exit Sub
                    End If
                End If


                'If Me.ValidaAbonoMismaFacturaLetraAnteriorConSaldo(e.RowHandle) = False Then
                '    ShowMessage(MessageType.MsgInformation, "Hay documentos anteriores pendientes de la misma factura")
                '    Me.grvMovimientos.SetRowCellValue(j, Me.grcChecar, False)
                '    Exit Sub
                'End If

                EsUltimoDocumento = Me.grvMovimientos.GetRowCellValue(e.RowHandle, Me.grcUltimoDocumento)
               

                ' Validacion para Checar si ya se genero la Nota de Cargo
                If EsUltimoDocumento = True And Me.grvMovimientos.GetRowCellValue(e.RowHandle, Me.grcPermitePagarUltimoDocumento) = False Then
                    ShowMessage(MessageType.MsgInformation, "No se ha generado la Nota de Cargo para este Documento")
                    Me.grvMovimientos.SetRowCellValue(j, Me.grcChecar, False)
                    Me.lkpCobrador.EditValue = COBRADOR_ANTERIOR
                    Exit Sub

                End If

                ManejaPromocion = Me.grvMovimientos.GetRowCellValue(e.RowHandle, grcManejaPromocion)
                DiasVencidos = Me.grvMovimientos.GetRowCellValue(e.RowHandle, Me.grcDiasVencidos)
                TieneIvaDesglosadoFactura = Me.grvMovimientos.GetRowCellValue(e.RowHandle, Me.grcTieneIvaDesglosado)
                SaldoDocto = Me.grvMovimientos.GetRowCellValue(e.RowHandle, Me.grcSaldoDocumento)
                MismosDoctosVentaPlan = Me.grvMovimientos.GetRowCellValue(e.RowHandle, Me.grcMismosDoctosVentaPlan)

                'Validar si ya se tiene marcado un ultimo documento
                If Not ValidaSoloUltimoDocumento(e.RowHandle, EsUltimoDocumento, ManejaPromocion, DiasVencidos, TieneIvaDesglosadoFactura, MismosDoctosVentaPlan) Then
                    ShowMessage(MessageType.MsgInformation, "No se permite pagar el ultimo documento junto con otro documento")
                    Me.grvMovimientos.SetRowCellValue(e.RowHandle, Me.grcChecar, False)
                    EsUltimoDocumento = False
                    Exit Sub
                End If


                sucursal_activa = 0
                Me.clcMonto.EditValue = 0
                Me.clcIntereses.Value = 0
                Me.clcImporteDocumentos.EditValue = 0



                For i = 0 To Me.grvMovimientos.RowCount - 1
                    If e.RowHandle = i And e.Value = True And Factura_Diferente = False Then
                        sucursal_activa = Me.grvMovimientos.GetRowCellValue(i, Me.grcSucursal)

                        Me.clcImporteDocumentos.EditValue = Me.clcImporteDocumentos.EditValue + Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                        'DAM 25/Oct/2007 ahora los intereses se obtienes dependiendo la documento seleccionado
                        Me.clcIntereses.Value = Me.clcIntereses.Value + Me.grvMovimientos.GetRowCellValue(i, Me.grcInteres)
                        'Me.clcNotaCargo.Value = Me.clcNotaCargo.Value + Me.grvMovimientos.GetRowCellValue(i, Me.grcNotaCargo)
                        contador += 1
                    Else
                        If e.RowHandle <> i And grvMovimientos.GetRowCellValue(i, Me.grcChecar) = True Then
                            sucursal_activa = Me.grvMovimientos.GetRowCellValue(i, Me.grcSucursal)
                            Me.clcImporteDocumentos.EditValue = Me.clcImporteDocumentos.EditValue + Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                            Me.clcIntereses.Value = Me.clcIntereses.Value + Me.grvMovimientos.GetRowCellValue(i, Me.grcInteres)

                            contador += 1
                        End If
                    End If


                Next


                AsignarMonto_FormaPago(fila_predeterminada, Me.clcImporteDocumentos.EditValue + Me.clcIntereses.Value - Me.clcBonificar.Value)


                CalculaMonto()
            End If
        End If

        Modificado_monto = False


    End Sub
    Private Sub grvMovimientos_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvMovimientos.CellValueChanged
        Me.grvMovimientos.CloseEditor()
    End Sub
    Private Sub grvMovimientos_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvMovimientos.FocusedRowChanged
        If e.FocusedRowHandle < 0 Then Exit Sub
        RevisaFactura(e.FocusedRowHandle)
    End Sub

    Private Sub btnVerArticulosFactura_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerArticulosFactura.Click
        If oTabla.Rows.Count > 0 Then

            Dim orow As DataRowView = Me.grvMovimientos.GetRow(Me.grvMovimientos.FocusedRowHandle)
            Dim oVerProductos As New frmMovimientosCajaArticulosFactura

            With oVerProductos
                .Title = "Productos de la Factura"
                .FormaPadre = Me
                .MdiParent = Me.MdiParent
                .Sucursal_factura = orow("sucursal")
                .serie_factura = orow("serie")
                .folio_factura = orow("folio")
                .fecha_factura = orow("fecha")
                .NombreSucursal = orow("nombre_sucursal")
                .Show()
            End With

            oVerProductos = Nothing
        End If
    End Sub
    Private Sub btnLlamadasCliente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLlamadasCliente.Click
        If Me.cliente < 1 Then Exit Sub

        Dim oForm As New Comunes.frmLlamadasClientes
        With oForm
            .OwnerForm = Me
            .MdiParent = Me.MdiParent
            .Action = Actions.Update
            '.Refresh()
            .cliente = cliente
            .Title = "Llamadas de Clientes"
            .entro_catalogo_clientes = True

            .Show()
            .cliente = cliente

            Me.Enabled = False
        End With



    End Sub
    Private Sub btnCambioCheques_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambioCheques.Click
        Dim ofrmCambiosCheques As New frmCambiosCheques
        With ofrmCambiosCheques
            '.MenuOption = e.Item
            .MdiParent = Me.MdiParent
            .Text = "Cambio de Cheques"
            .Show()
        End With
    End Sub
    Private Sub clcBonificar_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcBonificar.EditValueChanged
        If Me.clcBonificar.IsLoading = True Or Not IsNumeric(Me.clcBonificar.EditValue) Then Exit Sub
        If Modificado_monto = False Then
            CalculaMonto()
            'Else
            '    Me.clcMonto.Value = Me.clcMonto.Value - clcBonificar.Value
        End If
    End Sub

    Private Sub clcMonto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles clcMonto.KeyPress
        Modificado_monto = True
    End Sub

    Private Sub clcMonto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcMonto.Click
        CType(sender, DevExpress.XtraEditors.CalcEdit).SelectAll()
    End Sub
    Private Sub clcBonificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcBonificar.Click
        CType(sender, DevExpress.XtraEditors.CalcEdit).SelectAll()
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub LlenarDatosCliente()
        Dim response As New Events
        Dim tipo_cobro As String
        Dim oData As DataSet
        Dim notas As String

        response = oClientes.DespliegaDatos(cliente, Comunes.Common.Sucursal_Actual)
        If Not response.ErrorFound Then
            oData = response.Value
            With oData.Tables(0).Rows(0)
                Me.clcClaveCliente.Value = cliente
                Me.lblNumeroCliente.Text = cliente.ToString
                Me.lbldomicilio.Text = .Item("domicilio_numeros") + " " + .Item("nombre_colonia") + vbCrLf + " " + .Item("nombre_ciudad") + vbCrLf + " " + .Item("nombre_estado") + vbCrLf + "Tel: " + .Item("telefono1")
                tipo_cobro = .Item("tipo_cobro")
                notas = .Item("notas")

                If tipo_cobro = "O" Then
                    Me.lkpCobrador.EditValue = Me.CobradorCasa
                Else
                    Me.lkpCobrador.EditValue = .Item("cobrador")

                End If


                If notas.Trim.Length > 0 Then
                    Dim oform As New frmMovimientosCajaNotas
                    With oform
                        .OwnerForm = Me
                        .MdiParent = Me.MdiParent
                        .Title = "Notas del Cliente"
                        .lblMuestraNotas.Text = notas
                        .Show()
                        Me.Enabled = False
                    End With

                End If
            End With

        Else
            LimpiarDatosCliente()
        End If
    End Sub
    Private Sub LimpiarDatosCliente()
        Me.clcClaveCliente.Value = 0
        Me.lblNumeroCliente.Text = ""
        'Me.txtCiudad.Text = ""
        'Me.txtColonia.Text = ""

        'Me.txtDomicilio.Text = ""
        'Me.txtEstado.Text = ""

        'Me.txtTelefono1.Text = ""
        'Me.txtTelefono2.Text = ""

        Me.lbldomicilio.Text = ""
        Me.lkpCobrador.EditValue = Nothing

        Me.clcImporte.Value = 0
        Me.clcImporteDocumentos.Value = 0
        Me.clcIntereses.Value = 0
        Me.clcBonificar.Value = 0


        Me.grMovimientos.DataSource = Nothing
        oTabla.Rows.Clear()



    End Sub
    Private Sub Llenargrid(ByVal busqueda As Boolean)

        Dim response As New Events
        Dim odataset As New DataSet
        Dim filas As Long = 0

        'banPermiteCambiarTipoPago = True
        Me.cboTipoCobro.Enabled = True

        'Limpio cada ves que llene el grid si es que el docto estaba manejando promocion
        bImprimeReciboMenosManejaPromocion = False
        FilaManejaPromocion = 0


        Me.txtconcepto.Text = ""
        Me.txtconcepto.Enabled = False
        response = Me.oMovimientosCobrar.DespliegaDatosVentas(Comunes.Common.Sucursal_Actual, cliente, Me.cboTipoCobro.EditValue, Me.dteFecha.EditValue, Me.chkAbonoProximo.EditValue)
        If Not response.ErrorFound Then
            odataset = response.Value
            oTabla = odataset.Tables(0)
            Me.grMovimientos.DataSource = oTabla

            'CANTIDAD DE REGISTROS DEL CLIENTE
            filas = odataset.Tables(0).Rows.Count

            ''''''''' nuevo
            If odataset.Tables(1).Rows.Count > 0 Then
                If odataset.Tables(1).Rows(0).Item(0) > 0 Then
                    Me.lblMensajeNotasCargoPendientesPagar.Visible = True
                Else
                    Me.lblMensajeNotasCargoPendientesPagar.Visible = False
                End If

            End If
            '''''''''

            Me.clcMonto.EditValue = 0
            Me.clcMonto.Enabled = True


            If Me.cboTipoCobro.EditValue <> "A" And filas > 0 Then

                Me.clcMonto.EditValue = oTabla.Rows(0).Item("saldo_documento")
                'Me.clcMonto.Enabled = False -- DAM 16-JUN-08 SE PUEDE PAGAR EL ENGANCHE EN PARTES
                If Me.cboTipoCobro.EditValue = "P" Then
                    Me.txtconcepto.Enabled = True
                    Me.txtconcepto.Text = "Descuento por Pronto Pago"
                End If

                If Me.cboTipoCobro.EditValue = "N" Then
                    Me.txtconcepto.Enabled = True
                    Me.txtconcepto.Text = "NOTA DE CARGO"
                End If


                ' 05-NOV-09 Se comento esta validacion a peticion de creditos 
                If Me.cboTipoCobro.EditValue = "E" And ValidaExisteEnganchesVencidos(cliente) = True Then
                    'banPermiteCambiarTipoPago = False
                    Me.cboTipoCobro.Enabled = False
                End If


            End If
        End If
        response = oFormasPagos.MovimientosCaja
        If Not response.ErrorFound Then
            Me.tmaFormaspago.DataSource = response.Value
            AgregarTipoCambio()
        End If

        CalcularTotalDocumentos()
        Me.grvMovimientos.FocusedRowHandle = -1
        odataset = Nothing

        If filas > 0 Then
            Me.RevisaFactura(0)  ' para vuelva a cargar la info de la factura en mencion
        End If





    End Sub

    Public Sub CalcularTotalDocumentos()
        Dim i As Integer = 0
        Dim intereses As Double = 0
        Dim importe As Double = 0
        If Me.oTabla Is Nothing Or IsNumeric(Me.oTabla.Rows.Count) = False Or IsDBNull(Me.oTabla.Rows.Count) Then
            MsgBox("valor nothing  en el contador de filas de la tabla")
            Exit Sub
        End If
        For i = 0 To Me.oTabla.Rows.Count - 1

            importe = importe + oTabla.Rows(i).Item("saldo_documento") 'me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
        Next

        Me.clcImporte.EditValue = importe
        Me.clcBonificar.EditValue = 0


    End Sub

    'Private Sub TraeConceptoGastosAdministrativos()

    '    ConceptoGastosAdministrativos = CType(oVariables.TraeDatos("concepto_gastos_administrativos", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoGastosAdministrativos <> "" Then

    '        banConceptoNCGastosAdministrativos = True

    '    Else
    '        ShowMessage(MessageType.MsgInformation, "El Concepto de Gastos Administrativos no esta definido", "Variables del Sistema", Nothing, False)
    '    End If

    'End Sub

    'Private Sub TraeConceptoAbono()
    '    ConceptoAbono = CType(oVariables.TraeDatos("concepto_cxc_abono", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoAbono <> "" Then
    '        banConceptoAbono = True
    '    Else
    '        ShowMessage(MessageType.MsgInformation, "El Concepto de CXC de Abono no esta definido", "Variables del Sistema", Nothing, False)
    '    End If
    'End Sub
    'Private Sub TraeConceptoNotaCredito()
    '    Me.ConceptoNotacredito = CType(oVariables.TraeDatos("concepto_cxc_nota_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoNotacredito <> "" Then
    '        banConceptoNotacredito = True
    '    Else
    '        ShowMessage(MessageType.MsgInformation, "El Concepto de  CXC de Nota Credito no esta definido", "Variables del Sistema", Nothing, False)
    '    End If
    'End Sub
    'Private Sub TraeConceptoNotaCargo()
    '    Me.ConceptoNotacargo = CType(oVariables.TraeDatos("concepto_cxc_nota_cargo", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoNotacargo <> "" Then
    '        banConceptoNotacargo = True
    '    Else
    '        ShowMessage(MessageType.MsgInformation, "El Concepto de  CXC de Nota de Cargo no esta definido", "Variables del Sistema", Nothing, False)
    '    End If
    'End Sub
    'Private Sub TraeCobradorCasa()
    '    Me.CobradorCasa = CType(oVariables.TraeDatos("cobrador_casa", VillarrealBusiness.clsVariables.tipo_dato.Entero), String)
    '    If CobradorCasa <> 0 Then
    '        banCobradorCasa = True
    '    Else
    '        ShowMessage(MessageType.MsgInformation, "El Cobrador de Variales del Sistema no esta definido", "Variables del Sistema", Nothing, False)
    '    End If
    'End Sub
    'Private Sub TraeConceptointeres()
    '    Me.Conceptointeres = CType(oVariables.TraeDatos("concepto_intereses", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If Conceptointeres <> "" Then
    '        banConceptoInteres = True
    '    Else
    '        ShowMessage(MessageType.MsgInformation, "El Concepto de  Intereses no esta definido", "Variables del Sistema", Nothing, False)
    '    End If
    'End Sub
   

    'Private Sub TraeFormaPagoVales()
    '    Me.FormaPagoVales = CType(oVariables.TraeDatos("pago_vales_caja", VillarrealBusiness.clsVariables.tipo_dato.Entero), Long)
    '    If FormaPagoVales > 0 Then
    '        banFormaPagoVales = True
    '    Else
    '        ShowMessage(MessageType.MsgInformation, "La forma de Pago para Vales no esta definido", "Variables del Sistema", Nothing, False)
    '    End If
    'End Sub

    'Private Sub TraeFormaPagoEfectivo()
    '    Me.FormaPagoEfectivo = CType(oVariables.TraeDatos("pago_efectivo_caja", VillarrealBusiness.clsVariables.tipo_dato.Entero), Long)
    '    If FormaPagoEfectivo > 0 Then
    '        banFormaPagoEfectivo = True
    '    Else
    '        ShowMessage(MessageType.MsgInformation, "La forma de Pago para Efectivo no esta definido", "Variables del Sistema", Nothing, False)
    '    End If
    'End Sub

    Private Sub calculartotales()

        subtotal = Me.clcMonto.Value / (1 + (ImpuestoVariables / 100))
        Me.iva = Me.clcMonto.Value - subtotal

    End Sub
    Private Function calculadocumentos(ByVal saldo As Double) As Long

        Dim documentos As Long = 0
        Dim importe_documento As Double = 0
        Dim dfoliofactura As Long = 0
        Dim lsucursalfactura As Long = 0
        Dim sconceptofactura As String = ""
        Dim sseriefactura As String = ""
        Dim i As Long = 0
        For i = 0 To Me.oTabla.Rows.Count - 1
            If Me.oTabla.Rows(i).Item("checar") Then ' Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) Then
                documentos = documentos + 1
                If saldo >= Me.oTabla.Rows(i).Item("saldo_documento") Then ' Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento) Then
                    importe_documento = Me.oTabla.Rows(i).Item("saldo_documento") ' Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                Else
                    importe_documento = saldo
                End If
                saldo = saldo - importe_documento 'Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                dfoliofactura = Me.oTabla.Rows(i).Item("folio") 'Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio)
                lsucursalfactura = Me.oTabla.Rows(i).Item("sucursal") ' Me.grvMovimientos.GetRowCellValue(i, Me.grcSucursal)
                sconceptofactura = Me.oTabla.Rows(i).Item("concepto") 'Me.grvMovimientos.GetRowCellValue(i, Me.grcConcepto)
                sseriefactura = Me.oTabla.Rows(i).Item("serie") 'Me.grvMovimientos.GetRowCellValue(i, Me.grcSerie)

                If saldo = 0 Then Exit For
            End If

        Next
        If saldo > 0 And Me.cboTipoCobro.EditValue = "P" Then

            For i = 0 To Me.oTabla.Rows.Count - 1
                If Not Me.oTabla.Rows(i).Item("checar") And dfoliofactura = Me.oTabla.Rows(i).Item("folio") Then ' Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) And dfoliofactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio) Then
                    documentos = documentos + 1 'Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento) And lsucursalfactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcSucursal) And _'Me.grvMovimientos.GetRowCellValue(i, Me.grcConcepto)
                    If saldo >= Me.oTabla.Rows(i).Item("saldo_documento") And lsucursalfactura = Me.oTabla.Rows(i).Item("sucursal") And _
                    sconceptofactura = Me.oTabla.Rows(i).Item("concepto") And _
                    sseriefactura = Me.oTabla.Rows(i).Item("serie") Then 'Me.grvMovimientos.GetRowCellValue(i, Me.grcSerie) Then

                        importe_documento = Me.oTabla.Rows(i).Item("saldo_documento") ' Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                    Else
                        importe_documento = saldo
                    End If
                    saldo = saldo - importe_documento 'Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                    If saldo = 0 Then Exit For

                End If

            Next
        Else 'Me.grvMovimientos.RowCount - 1
            If saldo > 0 Then
                For i = 0 To Me.oTabla.Rows.Count - 1
                    If Not Me.oTabla.Rows(i).Item("checar") Then
                        documentos = documentos + 1
                        If saldo >= Me.oTabla.Rows(i).Item("saldo_documento") Then

                            importe_documento = Me.oTabla.Rows(i).Item("saldo_documento")
                        Else
                            importe_documento = saldo
                        End If
                        saldo = saldo - importe_documento
                        If saldo = 0 Then Exit For

                    End If
                Next
            End If
        End If

        calculadocumentos = documentos

    End Function
    Private Function validaImporteProntoPago() As Boolean

        Dim documentos As Long = 0
        Dim importe_documento As Double = 0
        Dim dfoliofactura As Long = 0
        Dim lsucursalfactura As Long = 0
        Dim sconceptofactura As String = ""
        Dim sseriefactura As String = ""
        Dim saldo As Double
        Dim numero_documento As Long
        Dim i As Long = 0
        For i = 0 To Me.grvMovimientos.RowCount - 1
            If Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) Then
                documentos = documentos + 1
                importe_documento = importe_documento + Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                dfoliofactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio)
                lsucursalfactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcSucursal)
                sconceptofactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcConcepto)
                sseriefactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcSerie)

            End If

        Next
        If Me.cboTipoCobro.EditValue = "P" Then

            For i = 0 To Me.grvMovimientos.RowCount - 1
                If Not Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) And Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio) = dfoliofactura And dfoliofactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio) And _
                    lsucursalfactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcSucursal) And _
                    sconceptofactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcConcepto) And _
                    sseriefactura = Me.grvMovimientos.GetRowCellValue(i, Me.grcSerie) Then

                    importe_documento = importe_documento + Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)

                End If
            Next



        End If

        If importe_documento < (Me.clcMonto.EditValue - Me.clcIntereses.EditValue - Me.clcBonificar.EditValue) Then
            validaImporteProntoPago = True
        Else
            validaImporteProntoPago = False
        End If


    End Function
    Public Function calculaimporteFormapagar() As Double
        Dim i As Long
        Dim importe As Double = 0
        For i = 0 To Me.grvFormaspago.RowCount - 1
            importe = importe + Me.grvFormaspago.GetRowCellValue(i, Me.grcImporte)

        Next
        calculaimporteFormapagar = Me.clcMonto.EditValue - importe
    End Function

    Private Sub Imprimirengancheycontado()
        Dim response As New Events
        response = oReportes.ImprimirEngancheContadoNotaCargo(sucursal_activa, concepto, serie, folio, cliente, 1)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptRecibosCaja

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                If TinApp.Connection.User.ToUpper = "SUPER" Then
                    TinApp.ShowReport(Me.MdiParent, "Impresi�n del Recibo ", oReport, False, , True, True)
                Else
                    'TinApp.ShowReport(Me.MdiParent, "Impresi�n del Recibo ", oReport)
                    TinApp.PrintReport(oReport)
                End If

                If Me.lkpCliente.GetValue("email_abogado") <> "" Then
                    'CREA UN PDF Y LO MANDA POR EMAIL AL ABOGADO
                    GeneraReporteEnPDFyEnviaEmail(oReport, "Impresi�n del Recibo ", oDataSet, 2)
                End If

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub
    Private Sub ImprimirAbono(ByVal saldo As Double)
        Dim response As New Events

        Dim serie_referencia As String
        Dim folio_referencia As Integer
        Dim documento_referencia As Integer
        Dim sucursal_referencia As Integer

        Dim serie_referencia_anterior As String
        Dim folio_referencia_anterior As Integer
        Dim sucursal_referencia_anterior As Integer


        documento_referencia = 0

        Dim i As Integer
        For i = 0 To Me.grvMovimientos.RowCount - 1

            If Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) = True Then

                documento_referencia = Me.grvMovimientos.GetRowCellValue(i, Me.grcDocumento)
                serie_referencia = Me.grvMovimientos.GetRowCellValue(i, Me.grcSerie)
                folio_referencia = Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio)
                sucursal_referencia = Me.grvMovimientos.GetRowCellValue(i, grcSucursal)

                If i > 0 Then
                    If (sucursal_referencia = sucursal_referencia_anterior) And (serie_referencia = serie_referencia_anterior) And (folio_referencia = folio_referencia_anterior) Then
                        Exit For
                    End If

                End If


                response = oReportes.Imprimirabono(sucursal_activa, concepto, serie, folio, cliente, serie_referencia, folio_referencia, documento_referencia)

                If response.ErrorFound Then
                    ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
                Else
                    If response.Value.Tables(0).Rows.Count > 0 Then

                        Dim oDataSet As DataSet
                        Dim oReport As New rptReciboAbonos

                        oDataSet = response.Value
                        oReport.DataSource = oDataSet.Tables(0)

                        bReciboImpreso = True

                        If TinApp.Connection.User.ToUpper = "SUPER" Then
                            TinApp.ShowReport(Me.MdiParent, "Impresi�n del Recibo ", oReport, , , True, True)
                        Else
                            'TinApp.ShowReport(Me.MdiParent, "", oReport)
                            TinApp.PrintReport(oReport)
                        End If


                        If banderanotacargo = True Then
                            Me.ImprimirNotaCargo(conceptointereses, serieinteres, saldo_intereses, foliointeres)
                        End If


                        If Me.lkpCliente.GetValue("email_abogado") <> "" Then
                            'CREA UN PDF Y LO MANDA POR EMAIL AL ABOGADO
                            GeneraReporteEnPDFyEnviaEmail(oReport, "Impresi�n del Recibo ", oDataSet, 1)
                        End If

                        oDataSet = Nothing
                        oReport = Nothing
                    Else

                        ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                    End If
                End If

                serie_referencia_anterior = serie_referencia
                folio_referencia_anterior = folio_referencia
                sucursal_referencia_anterior = sucursal_referencia

            End If
        Next
    End Sub
    Private Sub ImprimirPagoNotaCargoDescAnt()
        Dim response As New Events
        response = oReportes.ImprimirEngancheContadoNotaCargo(sucursal_activa, concepto, serie, folio, cliente, 1)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptRecibosCaja

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                If TinApp.Connection.User.ToUpper = "SUPER" Then
                    TinApp.ShowReport(Me.MdiParent, "Impresi�n del Recibo ", oReport, False, , True, True)
                Else
                    TinApp.PrintReport(oReport)
                End If

                If Me.lkpCliente.GetValue("email_abogado") <> "" Then
                    'CREA UN PDF Y LO MANDA POR EMAIL AL ABOGADO
                    GeneraReporteEnPDFyEnviaEmail(oReport, "Impresi�n del Recibo ", oDataSet, 2)
                End If

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub
    Private Sub ImprimirNotacredito()
        Dim response As New Events

        response = oReportes.ImprimeNotaDeCredito(sucursal_activa, concepto, serie, folio, cliente, Me.clcMonto.EditValue)



        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "Las Notas de Cr�dito no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptNotaCredito

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                If TinApp.Connection.User.ToUpper = "SUPER" Then
                    TinApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cr�dito ", oReport, , , True, True)
                Else
                    TinApp.PrintReport(oReport)
                End If

                If Me.lkpCliente.GetValue("email_abogado") <> "" Then
                    'CREA UN PDF Y LO MANDA POR EMAIL AL ABOGADO
                    GeneraReporteEnPDFyEnviaEmail(oReport, "Impresi�n de la Nota de Cr�dito ", oDataSet, 3)
                End If
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub
    Private Sub ImprimirNotaCargo(ByVal conceptointereses As String, ByVal serieinteres As String, ByVal saldo_intereses As Double, ByVal foliointeres As Long)
        Dim response As New Events

        response = oReportes.ImprimeNotaDeCargo(sucursal_activa, conceptointereses, serieinteres, foliointeres, cliente, True)


        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La Nota de Cargo no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptNotaCargo

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)


                TinApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cargo ", oReport)


                If Me.lkpCliente.GetValue("email_abogado") <> "" Then
                    'CREA UN PDF Y LO MANDA POR EMAIL AL ABOGADO
                    GeneraReporteEnPDFyEnviaEmail(oReport, "Impresi�n de la Nota de Cargo ", oDataSet, 4)
                End If
                oDataSet = Nothing
                oReport = Nothing

            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")


            End If
        End If


    End Sub
    Private Sub ImprimirVale(ByVal folio As Long)
        Dim response As New Events

        response = oReportes.ImprimeVale(folio)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Vale no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                oDataSet = response.Value

                '21 - Dic - 2011 Esta Validacion es Propia de este modulo de caja.....
                ' Si el saldo es Mayor a 0 (cero) se imprime el vale...

                If oDataSet.Tables(0).Rows(0).Item("saldo") > 0 Then
                    Dim oReport As New Comunes.rptValeFormato
                    oReport.DataSource = oDataSet.Tables(0)
                    TinApp.ShowReport(Me.MdiParent, "Impresi�n del Vale ", oReport)
                End If

            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

    Private Sub guardarpago()
        Dim response As Events = New Events
        Me.tmaFormaspago.MoveFirst()

        Do While Not tmaFormaspago.EOF
            If Me.tmaFormaspago.Item("importe") > 0 Then

                response = oMovimientosCobrar_formaspago.Insertar(sucursal_activa, concepto, serie, folio, cliente, Me.tmaFormaspago.Item("forma_pago"), Comunes.Common.Sucursal_Actual, Me.dteFecha.EditValue, Comunes.Common.Caja_Actual, Comunes.Common.Cajero, Me.tmaFormaspago.Item("importe"), Me.tmaFormaspago.Item("tipo_cambio"), Me.tmaFormaspago.Item("dolares"), CLng(Me.UltimosDigitos))
                If response.ErrorFound Then
                    TinApp.Connection.Rollback()
                    response.ShowError()
                    Exit Sub
                Else
                    If Me.tmaFormaspago.Item("forma_pago") = Me.FormaPagoVales Then
                        response = oValesDetalle.Insertar(Me.FolioVale, sucursal_activa, concepto, serie, folio, Me.dteFecha.DateTime, Me.tmaFormaspago.Item("importe"))
                        If response.ErrorFound Then
                            TinApp.Connection.Rollback()
                            response.ShowError()
                            Exit Sub
                        End If
                    End If
                End If
            End If

            tmaFormaspago.MoveNext()
        Loop
        Me.tmaFormaspago.DataSource.Clear()
    End Sub
    Private Sub AgregarTipoCambio()
        Dim i As Long

        For i = 0 To Me.grvFormaspago.RowCount - 1
            If Me.grvFormaspago.GetRowCellValue(i, Me.grcManejaDolares) Then
                Me.grvFormaspago.SetRowCellValue(i, Me.grcTipocambio, Tipo_Cambio)
            End If
        Next

    End Sub

    Private Sub GeneraReporteEnPDFyEnviaEmail(ByVal oReport As Object, ByVal titulo As String, ByVal oDataSet As DataSet, ByVal numero_reporte As Long)
        Select Case numero_reporte
            Case 1
                oReport = New rptReciboAbonos
            Case 2
                oReport = New rptRecibosCaja
            Case 3
                oReport = New rptNotaCredito
            Case 4
                oReport = New rptNotaCargo  'Comunes.rptNotaCargo
        End Select

        Dim sArchivo As String
        Dim Data As DataSet



        Dim Path As String = System.Windows.Forms.Application.StartupPath  '"C:\Desarrollo\Mueblerias\Caja\Bin"
        Path = Path & "\Exportar\"
        sArchivo = Path & CType(cliente, String) & "_" & CType(Me.lkpCliente.GetValue("abogado"), String) & "_" & (TinApp.FechaServidor.Replace("/", "_").Replace(":", "_")) & ".PDF"  '  (TINApp.FechaServidor.Substring(0, 7)) & ".PDF"

        Dim pdfExp As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        With oReport
            .DataSource = oDataSet.Tables(0)
            oReport.Run()
            'Verifica si existe el directorio o lo crea
            If Directory.Exists(Path) Then
                pdfExp.Export(oReport.Document, sArchivo)
            Else
                Directory.CreateDirectory(Path)
                pdfExp.Export(oReport.Document, sArchivo)
            End If

            'Manda un EMail al Abogado
            If Me.lkpCliente.GetValue("email_abogado") <> "" Then
                Dipros.Utils.Mail.SendMail(Me.lkpCliente.GetValue("email_abogado"), "Movimientos de Caja", "Mensaje generado autom�ticamente desde el m�dulo de caja, cliente : #" + CType(cliente, String) + " - " + Me.lkpCliente.GetValue("nombre"), sArchivo, , True, True)
            End If
        End With
    End Sub

    'Private Function VerificaeInsertaNotadeCargo(ByVal Tabla As DataTable)

    '    Dim i As Integer = 0
    '    Dim fecha As DateTime = Me.dteFecha.EditValue
    '    Dim sucursal As Integer
    '    Dim concepto As String
    '    Dim serie As String
    '    Dim folio As Integer
    '    Dim cliente As Integer
    '    Dim documento As Integer
    '    Dim importe_ncr As Double
    '    Dim Response As Dipros.Utils.Events
    '    Dim iva As Double
    '    Dim subtotal As Double
    '    Dim impuesto As Double
    '    impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)
    '    If impuesto <> -1 Then

    '        For i = 0 To Tabla.Rows.Count - 1
    '            'Reviso la fecha de la ultima letra de CADA FACTURA

    '            If CType(Tabla.Rows(i).Item("ultima_letra"), Boolean) = True Then

    '                If fecha > CType(Tabla.Rows(i).Item("fecha"), DateTime).AddDays(CType(Tabla.Rows(i).Item("dias_gracia"), Long)) Then

    '                    If Me.oTabla.Rows(i).Item("impresa_nota_credito") Then

    '                        sucursal = CType(Tabla.Rows(i).Item("sucursal"), Integer)
    '                        concepto = CType(Tabla.Rows(i).Item("concepto"), String)
    '                        serie = CType(Tabla.Rows(i).Item("serie"), String)
    '                        folio = CType(Tabla.Rows(i).Item("folio"), Integer)
    '                        cliente = CType(Tabla.Rows(i).Item("cliente"), Integer)
    '                        documento = CType(Tabla.Rows(i).Item("documento"), Integer)

    '                        'VERIFICAR SI NO EXISTE LA NOTA DE CARGO
    '                        'HAY QUE REVISAR LOS CONCEPTOS DE LA LINEA SIGUIENTE
    '                        If CType(oMovimientosCobrar.Existe(sucursal, concepto, serie, folio, cliente).Value, Boolean) = True And concepto = Me.ConceptoNotacargo Then
    '                        Else
    '                            'Generar Nota de Cargo
    '                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '                            Select Case True
    '                                Case fecha > CType(Tabla.Rows(i).Item("fecha"), DateTime).AddDays(CType(Tabla.Rows(i).Item("dias_gracia"), Long)) And fecha <= CType(Tabla.Rows(i).Item("fecha"), DateTime).AddDays(CType(Tabla.Rows(i).Item("dias_gracia"), Long) + 60)   'Actions.Insert

    '                                    importe_ncr = CType(Tabla.Rows(i).Item("importe_ncr"), Double) * 0.1
    '                                    subtotal = CType(importe_ncr / (1 + (impuesto / 100)), Double)
    '                                    iva = importe_ncr - subtotal

    '                                    Response = oMovimientosCobrar.InsertarCajas(Comunes.Common.Sucursal_Actual, Me.ConceptoNotacargo, Me.SerieNotaCargo, cliente, 0, Me.dteFecha.DateTime.Date, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, 1, importe_ncr, 0, subtotal, iva, importe_ncr, importe_ncr, System.DBNull.Value, "Nota de Cargo Generada desde Movimientos de Caja ", "", folio)

    '                                Case fecha > CType(Tabla.Rows(i).Item("fecha"), DateTime).AddDays(CType(Tabla.Rows(i).Item("dias_gracia"), Long) + 60) And fecha <= CType(Tabla.Rows(i).Item("fecha"), DateTime).AddDays(CType(Tabla.Rows(i).Item("dias_gracia"), Long) + 90)   'Actions.Insert

    '                                    importe_ncr = CType(Tabla.Rows(i).Item("importe_ncr"), Double) * 0.2
    '                                    subtotal = CType(importe_ncr / (1 + (impuesto / 100)), Double)
    '                                    iva = importe_ncr - subtotal

    '                                    Response = oMovimientosCobrar.InsertarCajas(Comunes.Common.Sucursal_Actual, Me.ConceptoNotacargo, Me.SerieNotaCargo, cliente, 0, Me.dteFecha.DateTime.Date, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, 1, importe_ncr, 0, subtotal, iva, importe_ncr, importe_ncr, System.DBNull.Value, "Nota de Cargo Generada desde Movimientos de Caja ", "", folio)

    '                                Case fecha > CType(Tabla.Rows(i).Item("fecha"), DateTime).AddDays(CType(Tabla.Rows(i).Item("dias_gracia"), Long) + 90)  'Actions.Insert

    '                                    importe_ncr = CType(Tabla.Rows(i).Item("importe_ncr"), Double)
    '                                    subtotal = CType(importe_ncr / (1 + (impuesto / 100)), Double)
    '                                    iva = importe_ncr - subtotal

    '                                    Response = oMovimientosCobrar.InsertarCajas(Comunes.Common.Sucursal_Actual, Me.ConceptoNotacargo, Me.SerieNotaCargo, cliente, 0, Me.dteFecha.DateTime.Date, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, 1, importe_ncr, 0, subtotal, iva, importe_ncr, importe_ncr, System.DBNull.Value, "Nota de Cargo Generada desde Movimientos de Caja ", "", folio)

    '                            End Select

    '                            If Response.ErrorFound Then
    '                                Response.ShowError()
    '                                Exit Function
    '                            End If


    '                            Response = oVentas.ActualizaNotaCargoGenerada(sucursal, serie, folio, importe_ncr)
    '                            If Response.ErrorFound Then
    '                                Response.ShowError()
    '                                Exit Function
    '                            End If
    '                            ShowMessage(MessageType.MsgInformation, "Se ha ha generado una nota de cargo a la factura : " + CType(sucursal, String) + "-" + CType(concepto, String) + "-" + CType(serie, String) + "-" + CType(folio, String) + "-" + CType(cliente, String) + " por : $" + CType(importe_ncr, String))

    '                        End If

    '                    End If
    '                End If
    '            End If
    '        Next

    '    End If

    'End Function
    Private Sub HabilitarDeshabilitarBonificar(ByVal cliente_en_juridico As Long, ByVal abogado As Long)

        If Me.lkpCliente.EditValue > 0 And cliente_en_juridico = 1 And abogado = Me.AbogadoLogueado Then
            Me.clcBonificar.Enabled = True
        Else
            If PorcentajeCondonacion = 0 Then
                Me.clcBonificar.Value = 0
                Me.clcBonificar.Enabled = False
            End If
        End If

    End Sub
    Private Function FilaPredeterminadaFormaPago() As Long
        Dim k As Integer = 0
        Dim fila_predeterminada As Long = 0

        'SE REVISA CUAL ES LA FORMA DE PAGO PREDETERMINADA
        For k = 0 To Me.grvFormaspago.RowCount - 1
            If (Me.grvFormaspago.GetRowCellValue(k, Me.grcPredeterminada) = True) Then
                fila_predeterminada = k
                Exit For
            End If
        Next

        Return fila_predeterminada
    End Function

    Private Sub AsignarMonto_FormaPago(ByVal fila_predeterminada As Long, ByVal monto As Double)

        Me.grvFormaspago.SetRowCellValue(fila_predeterminada, Me.grcImporte, monto)
        Me.grvFormaspago.UpdateTotalSummary()

    End Sub
    Private Function ValidaAbonoDiferentesFacturas(ByVal fila_actual As Integer) As Boolean
        Dim i As Long = 0
        Dim bExisteOtraFacturaSeleccionada As Boolean = False

        For i = 0 To Me.grvMovimientos.RowCount - 1
            If Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) Then
                ' Revisa si el renglon actual es de la misma factura de otro rengl�n seleccionado
                If (Me.grvMovimientos.GetRowCellValue(i, Me.grcSucursal) = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcSucursal) And Me.grvMovimientos.GetRowCellValue(i, Me.grcSerie) = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcSerie) And Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio) = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcFolio)) Then
                    'si son de la misma factura 
                    bExisteOtraFacturaSeleccionada = False
                Else
                    'si no son de la misma factura 
                    bExisteOtraFacturaSeleccionada = True
                    ValidaAbonoDiferentesFacturas = True
                End If
            End If
        Next

        ValidaAbonoDiferentesFacturas = bExisteOtraFacturaSeleccionada

    End Function

    Private Function ValidaAbonoMismaFacturaLetraAnteriorConSaldo(ByVal fila_actual As Integer) As Boolean
        Dim i As Long = 0
        'Dim bValidaAbonoMismaFacturaLetraAnteriorConSaldo As Boolean = False
        Dim sucursal_seleccionada As Long = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcSucursal)
        Dim serie_seleccionada As String = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcSerie)
        Dim folio_seleccionada As Long = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcFolio)
        Dim documento_seleccionada As Long = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcDocumento)

        Dim filtro As String

        filtro = "sucursal =" & sucursal_seleccionada.ToString & " and serie = '" & serie_seleccionada & "'  and folio = " & folio_seleccionada.ToString & " and documento < " & documento_seleccionada.ToString
        Dim rows As DataRow() = oTabla.Select(filtro)

        If rows.Length > 0 Then
            ValidaAbonoMismaFacturaLetraAnteriorConSaldo = False
        Else
            ValidaAbonoMismaFacturaLetraAnteriorConSaldo = True
        End If



    End Function
    Private Function ValidaIngresaVariasFormasPago(ByVal fila_actual As Integer) As Boolean
        Dim i As Long = 0
        Dim iExisteOtraFormaDePagoConValor As Boolean = False

        For i = 0 To Me.grvFormaspago.RowCount - 1
            ' Revisa si existe otra forma de pago mayor que cero
            If Me.grvFormaspago.GetRowCellValue(i, Me.grcImporte) > 0 And i <> fila_actual Then

                iExisteOtraFormaDePagoConValor = True
                ValidaIngresaVariasFormasPago = True
                Return True
            Else
                'si no son de la misma factura 
                iExisteOtraFormaDePagoConValor = False

            End If
        Next

        ValidaIngresaVariasFormasPago = iExisteOtraFormaDePagoConValor

    End Function

    Private Sub CalculaMonto()
        Me.clcMonto.Value = Me.clcImporteDocumentos.EditValue + Me.clcIntereses.Value - Me.clcBonificar.Value
    End Sub
    Private Function ValorDolares() As Double
        Dim valor As Double = 0
        Me.tmaFormaspago.MoveFirst()
        Do While Not tmaFormaspago.EOF
            If Me.tmaFormaspago.Item("importe") > 0 And Me.tmaFormaspago.Item("maneja_dolares") = True Then
                valor = Me.tmaFormaspago.Item("importe")
                Exit Do
            End If

            tmaFormaspago.MoveNext()
        Loop
        Return valor
    End Function
    Private Function ValorVales() As Double
        Dim valor As Double = 0
        Me.tmaFormaspago.MoveFirst()
        Do While Not tmaFormaspago.EOF
            If Me.tmaFormaspago.Item("importe") > 0 And Me.tmaFormaspago.Item("forma_pago") = Me.FormaPagoVales Then
                valor = Me.tmaFormaspago.Item("importe")
                Exit Do
            End If

            tmaFormaspago.MoveNext()
        Loop
        Return valor
    End Function

    Private Function ValorEfectivo() As Double
        Dim valor As Double = 0
        Me.tmaFormaspago.MoveFirst()
        Do While Not tmaFormaspago.EOF
            If Me.tmaFormaspago.Item("importe") > 0 And Me.tmaFormaspago.Item("forma_pago") = Me.FormaPagoEfectivo Then
                valor = Me.tmaFormaspago.Item("importe")
                Exit Do
            End If

            tmaFormaspago.MoveNext()
        Loop
        Return valor
    End Function

    Private Function VerificarUtilizaUltimosDigitos() As Boolean
        Dim valor As Boolean = False
        Me.tmaFormaspago.MoveFirst()
        Do While Not tmaFormaspago.EOF
            If Me.tmaFormaspago.Item("importe") > 0 Then
                valor = Me.tmaFormaspago.Item("solicitar_ulitmos_digitos")
                Exit Do
            End If

            tmaFormaspago.MoveNext()
        Loop
        Return valor
    End Function


    Private Sub RevisaFactura(ByVal fila As Long)

        If oTabla.Rows.Count > 0 Then
            Dim response As New Events

            Dim orow As DataRowView = Me.grvMovimientos.GetRow(fila)
            Dim articulos_no_entregados As Long = orow("articulos_no_entregados")
            Dim sobrepedidos As Long = orow("sobrepedidos")
            Dim servicios_no_terminados As Long = orow("servicios_no_terminados")



            If sobrepedidos > 0 Then
                Me.lblMensajePedidoFabrica.Visible = True
            Else
                Me.lblMensajePedidoFabrica.Visible = False
            End If

            If servicios_no_terminados > 0 Then
                Me.lblMensajeGarantias.Visible = True
            Else
                Me.lblMensajeGarantias.Visible = False
            End If

            If articulos_no_entregados > 0 Then
                Me.lblMensajeMercanciaSinEntregar.Visible = True
            Else
                Me.lblMensajeMercanciaSinEntregar.Visible = False
            End If

            'END IF
        End If
    End Sub
    Private Sub RefrescarInfoCliente()
        Dim Response As New Events
        Dim datarow As DataRow

        Response = oClientes.LookupLlenado(Sucursal_Dependencia, lkpCliente.GetValue("cliente"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            datarow = oDataSet.Tables(0).Rows(0)

            If datarow("no_recibir_abonos") = True Then
                Me.lblNoRecibirPagos.Visible = True
            Else
                Me.lblNoRecibirPagos.Visible = False

            End If
            oDataSet = Nothing
        End If

        Response = Nothing
        LlenarDatosCliente()
        Llenargrid(True)


        HabilitarDeshabilitarBonificar(datarow("cliente_en_juridico"), datarow("abogado"))

    End Sub

    Private Function ObtieneFolioAutorizacionBonificacion() As Long
        Dim response As Events
        Dim folio_autorizacion As Long = 0

        response = oAutorizacionesBonificacionesInteres.DespliegaDatosCaja(cliente, Comunes.Common.Caja_Actual, Me.clcBonificar.Value)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = response.Value
            If odataset.Tables(0).Rows.Count > 0 Then
                folio_autorizacion = odataset.Tables(0).Rows(0).Item("folio")
            Else
                folio_autorizacion = 0
            End If
        Else
            folio_autorizacion = 0
        End If

        Return folio_autorizacion
    End Function
    Public Function GuardarCambios() As Events

        If banValidar = False Then

            Dim timbrar_recibos As Boolean
            Dim imprimir_recibo_no_fiscal As Boolean

            timbrar_recibos = CType(oVariables.TraeDatos("timbrar_recibos", VillarrealBusiness.clsVariables.tipo_dato.Bit), Boolean)
            imprimir_recibo_no_fiscal = CType(oVariables.TraeDatos("imprimir_recibo_no_fiscal", VillarrealBusiness.clsVariables.tipo_dato.Bit), Boolean)


            Me.grvMovimientos.UpdateCurrentRow()
            Me.grvMovimientos.CloseEditor()

            Dim i As Integer


            Dim Response As New Events
            concepto = ""

            Dim documentos As Long = 0
            Dim saldo As Double = 0
            Dim importe_documento As Double = 0

            Dim dfoliofactura_guardar As Long = 0
            Dim lsucursalfactura_guardar As Long = 0
            Dim sconceptofactura_guardar As String = ""
            Dim sseriefactura_guardar As String = ""
            Dim lcliente_guardar As Long = 0

            Dim intereses As Double = 0
            Dim total_interes As Double = 0
            Dim subtotal_interes As Double = 0
            Dim iva_interes As Double = 0
            Dim banderaabono As Boolean = False
            Dim numero_documento As Long = 0


            Dim sObservaciones As String = ""
            Dim impuesto As Double = Me.ImpuestoVariables

            Dim dImporte_proporcional_costo As Double = 0
            Dim dIva As Double = 0

            Dim TipoAbono As String = cboTipoCobro.EditValue

            Dim folio_recibos_electronica As Long
            Dim TieneNotaCargoIntereses As Boolean = False
            Dim ImprimirComprobanteFiscalSimplicado As Boolean = False

            Dim FolioPromocion As String = ""
            Dim Documentos_Pagados As String = ""

            serieinteres = ""
            saldo_intereses = 0
            foliointeres = 0
            banderanotacargo = False
            bImprimeReciboMenosManejaPromocion = False


            conceptointereses = ""
            folio = 0
            intereses = Me.clcIntereses.EditValue - Me.clcBonificar.EditValue  '/csequera 30 nov 2007
            saldo = Me.clcMonto.EditValue  '/csequera 30 nov 2007

            If Me.cboTipoCobro.EditValue = "P" Then 'Pago a Nota de Cargo
                concepto = ConceptoNotacredito

                If factura_electronica Then
                    ObtenerSerieAbonosElectronicos(serie, TipoDocumentoElectronico.NotaCredito)
                Else
                    serie = uti_SerieCajasNotaCredito(Comunes.Common.Caja_Actual)
                End If

            Else

                Dim MontoEfectivo As Double = Me.ValorEfectivo()

                ' Revisar si es el ultimo documento y maneja promocion y no tiene dias vencidos (incluye dias de gracia)  para crear la nota de credito
                ' DAM 10-Dic-2013
                If EsUltimoDocumento And ManejaPromocion And Me.DiasVencidos <= 0 And TieneIvaDesglosadoFactura = False And (MontoEfectivo = Me.clcMonto.Value) And (SaldoDocto = Me.clcMonto.Value) And Me.MismosDoctosVentaPlan Then
                    concepto = Me.ConceptoPromocion
                    bImprimeReciboMenosManejaPromocion = True
                    TipoAbono = "P"
                    If factura_electronica Then
                        ObtenerSerieAbonosElectronicos(serie, TipoDocumentoElectronico.NotaCredito)
                    Else
                        serie = uti_SerieCajasNotaCredito(Comunes.Common.Caja_Actual)
                    End If
                Else
                    concepto = ConceptoAbono
                    bImprimeReciboMenosManejaPromocion = False
                    If factura_electronica Then
                        ObtenerSerieAbonosElectronicos(serie, TipoDocumentoElectronico.Abonos)
                    Else
                        serie = uti_SerieCajasRecibo(Comunes.Common.Caja_Actual)
                    End If
                End If

            End If



            If saldo < intereses Then
                saldo_intereses = saldo
                saldo = 0
            Else
                saldo_intereses = intereses
                saldo = saldo - intereses
            End If



            If impuesto <> -1 Then

                subtotal_interes = saldo_intereses / (1 + (impuesto / 100))
                iva_interes = saldo_intereses - subtotal_interes

                dIva = 1 + (impuesto / 100)
            Else

                ShowMessage(MessageType.MsgInformation, "El Porcentaje de Impuesto no esta definido", "Variables del Sistema", Nothing, False)
                Exit Function
            End If


            If cboTipoCobro.EditValue = "A" And intereses > 0 Then

                ' si usa factura electronica
                If Me.factura_electronica Then
                    ' DAM 17/05/2006
                    conceptointereses = Conceptointeres

                    If TieneIvaDesglosadoFacturaNotaCargo Then
                        Me.ObtenerSerieAbonosElectronicos(serieinteres, ModCommon.TipoDocumentoElectronico.NotaCargo)
                    Else
                        ' DAM 22-Oct-12 Antes se usaba la serie del campo serie_intereses de Cat_Sucursales
                        serieinteres = uti_SerieSucursalInteres(Comunes.Common.Sucursal_Actual)
                    End If

                    sObservaciones = "Intereses Moratorios generados en Caja"

                Else
                    ' si NO usa factura electronica
                    If ShowMessage(MessageType.MsgQuestion, "�Desea imprimir nota de cargo?", "Movimientos de caja", , False) = Answer.MsgYes Then
                        ' DAM 17/05/2006
                        conceptointereses = ConceptoNotacargo
                        serieinteres = uti_SerieCajaNotaCargo(Comunes.Common.Caja_Actual)
                        banderanotacargo = True
                        sObservaciones = "@Nota de Cargo generada en Caja"

                    Else
                        ' DAM 17/05/2006
                        conceptointereses = Conceptointeres
                        serieinteres = uti_SerieSucursalInteres(Comunes.Common.Sucursal_Actual)
                        sObservaciones = "Intereses Moratorios generados en Caja"
                    End If
                End If

            End If

            ' =================================
            TinApp.Connection.Begin()
            ' =================================

            If bImprimeReciboMenosManejaPromocion = True Then
                FolioPromocion = ObtenerFolioPromocion()
            End If


            'calcular intereses
            If cboTipoCobro.EditValue = "A" And intereses > 0 Then
                Dim observaciones_intereses As String

                If banderanotacargo = True Then
                    observaciones_intereses = "@Intereses Moratorios"
                Else
                    observaciones_intereses = "Intereses Moratorios"
                End If

                Response = oMovimientosCobrar.InsertarCajas(sucursal_activa, _
                           conceptointereses, _
                           serieinteres, _
                           cliente, 0, _
                           Me.dteFecha.DateTime.Date, _
                            Comunes.Common.Caja_Actual, _
                           Comunes.Common.Cajero, cobrador, 1, _
                           saldo_intereses, 0, _
                           subtotal_interes, _
                           iva_interes, _
                           saldo_intereses, _
                           saldo_intereses, _
                           System.DBNull.Value, _
                           observaciones_intereses, "", _
                           Comunes.Common.Sucursal_Actual, foliointeres, TieneIvaDesglosadoFacturaNotaCargo)
                If Response.ErrorFound Then
                    TinApp.Connection.Rollback()
                    Response.ShowError()
                    Exit Function
                End If


                documentos = Me.calculadocumentos(saldo) + 1
                Response = oMovimientosCobrar.InsertarCajas(sucursal_activa, _
                                            ConceptoAbono, serie, _
                                            cliente, 0, _
                                            Me.dteFecha.DateTime.Date, _
                                            Comunes.Common.Caja_Actual, _
                                            Comunes.Common.Cajero, _
                                            cobrador, documentos, 0, _
                                            Me.clcMonto.EditValue, subtotal, _
                                            iva, Me.clcMonto.EditValue, 0, _
                                            System.DBNull.Value, "", _
                                            "", Comunes.Common.Sucursal_Actual, folio)
                If Response.ErrorFound Then
                    TinApp.Connection.Rollback()
                    Response.ShowError()
                    Exit Function
                End If


                Dim saldo_letras_con_intereses As Double = 0
                Dim importe_documento_aux As Double = 0

                Dim valor_intereses_dam As Double

                saldo_letras_con_intereses = saldo_intereses

                For i = 0 To Me.oTabla.Rows.Count - 1

                    valor_intereses_dam = Me.oTabla.Rows(i).Item("interes")


                    importe_documento_aux = 0  ' DAM 08-JUL - DESCOMENTAR PARA CORREGIR UN ERROR DE DOCUMENTOS CON INTERESES PERO QUE NO SON DOCUMENTOS CONSECUTIVOS EN EL GRID

                    If Me.oTabla.Rows(i).Item("interes") > 0 And saldo_letras_con_intereses > 0 And Me.oTabla.Rows(i).Item("checar") = True Then
                        ' CARGO O INTERESES

                        numero_documento = numero_documento + 1


                        If saldo_letras_con_intereses >= Me.oTabla.Rows(i).Item("interes") Then
                            importe_documento_aux = Me.oTabla.Rows(i).Item("interes")
                        Else
                            importe_documento_aux = saldo_letras_con_intereses
                        End If

                        If Me.oTabla.Rows(i).Item("enajenacion") = True Then
                            'DAM 16/ABR/07 SE AGREGO ESTE CALCULO PARA EL IMPORTE PROPORCIONAL DE LOS DOCUMENTOS DE LA NOTA DE CREDITO
                            dImporte_proporcional_costo = (importe_documento_aux / dIva) * Me.oTabla.Rows(i).Item("factor_enajenacion")
                        Else
                            dImporte_proporcional_costo = 0

                        End If

                        Response = oMovimientosCobrarDetalle.Insertar(sucursal_activa, _
                                            conceptointereses, _
                                            serieinteres, foliointeres, _
                                            cliente, numero_documento, 0, _
                                            Me.dteFecha.DateTime.Date, _
                                            importe_documento_aux, _
                                            Me.oTabla.Rows(i).Item("sucursal"), _
                                            Me.oTabla.Rows(i).Item("concepto"), _
                                            Me.oTabla.Rows(i).Item("serie"), _
                                            Me.oTabla.Rows(i).Item("folio"), _
                                            Me.oTabla.Rows(i).Item("cliente"), _
                                            Me.oTabla.Rows(i).Item("documento"), 0, _
                                            Me.dteFecha.DateTime.Date, _
                                            importe_documento_aux, "", sObservaciones, dImporte_proporcional_costo)
                        If Response.ErrorFound Then
                            TinApp.Connection.Rollback()
                            Response.ShowError()
                            Exit Function
                        End If

                        TieneNotaCargoIntereses = True
                        'TieneIvaDesglosadoFacturaNotaCargo = CType(Me.oTabla.Rows(i).Item("ivadesglosado"), Boolean)

                        ' ==========================================================
                        ' Si la factura de los intereses tiene iva desglosado entonces se manda a timbrar la nota de cargo

                        ImprimirComprobanteFiscalSimplicado = Not TieneIvaDesglosadoFacturaNotaCargo



                        ' ==========================================================

                        ' DAM 17/05/2006
                        ' PAGO A INTERESES INDEPENDIENTEMENTE Q SEA UNA NOTA DE CARGO O NOTA DE INTERESES
                        ' OJO - dImporte_proporcional_costo * -1 aqui el importe se pone negativo ya que es un movimiento de tipo abono
                        sObservaciones = "Pago a la Referencia " + serieinteres + "-" + foliointeres.ToString
                        Response = oMovimientosCobrarDetalle.Insertar(sucursal_activa, _
                                            ConceptoAbono, _
                                            serie, _
                                            folio, _
                                            cliente, numero_documento, 0, _
                                            Me.dteFecha.DateTime.Date, _
                                            importe_documento_aux, _
                                            sucursal_activa, _
                                            conceptointereses, _
                                            serieinteres, _
                                            foliointeres, _
                                            cliente, numero_documento, 0, _
                                            Me.dteFecha.DateTime.Date, 0, _
                                            "I", sObservaciones, dImporte_proporcional_costo * -1)
                        If Response.ErrorFound Then
                            TinApp.Connection.Rollback()
                            Response.ShowError()
                            Exit Function
                        End If

                    End If

                    saldo_letras_con_intereses = saldo_letras_con_intereses - importe_documento_aux
                    If saldo_letras_con_intereses = 0 Then Exit For
                Next

                banderaabono = True

                For i = 0 To Me.oTabla.Rows.Count - 1
                    If Me.oTabla.Rows(i).Item("checar") = True Then

                        valor_intereses_dam = Me.oTabla.Rows(i).Item("interes")

                        If saldo_intereses >= Me.oTabla.Rows(i).Item("interes") Then
                            importe_documento = Me.oTabla.Rows(i).Item("interes")
                            Response = oMovimientosCobrarDetalle.ActualizarInteres(sucursal_activa, Me.oTabla.Rows(i).Item("concepto"), Me.oTabla.Rows(i).Item("serie"), Me.oTabla.Rows(i).Item("folio"), Me.oTabla.Rows(i).Item("cliente"), Me.oTabla.Rows(i).Item("documento"), 0, Me.dteFecha.EditValue)
                            If Response.ErrorFound Then
                                TinApp.Connection.Rollback()
                                Response.ShowError()
                                Exit Function
                            End If
                        Else
                            importe_documento = saldo_intereses
                            Response = oMovimientosCobrarDetalle.ActualizarInteres(sucursal_activa, Me.oTabla.Rows(i).Item("concepto"), Me.oTabla.Rows(i).Item("serie"), Me.oTabla.Rows(i).Item("folio"), Me.oTabla.Rows(i).Item("cliente"), Me.oTabla.Rows(i).Item("documento"), Me.oTabla.Rows(i).Item("interes") - importe_documento, Me.dteFecha.EditValue)
                            If Response.ErrorFound Then
                                TinApp.Connection.Rollback()
                                Response.ShowError()
                                Exit Function
                            End If
                        End If

                        'GUARDA DOCUMENTOS DE CXC

                        saldo_intereses = saldo_intereses - importe_documento
                        If saldo_intereses = 0 Then Exit For
                    End If
                Next

            End If

            '///////////////////////////////////////////////////////////////////////////


            If saldo > 0 Then

                importe_documento = 0



                If Not banderaabono Then
                    documentos = Me.calculadocumentos(saldo)
                End If

                'GUARDA LA CXC
                If Me.cboTipoCobro.EditValue = "P" Then
                    Response = oMovimientosCobrar.InsertarCajas(sucursal_activa, concepto, serie, cliente, 0, Me.dteFecha.DateTime.Date, Comunes.Common.Caja_Actual, Comunes.Common.Cajero, cobrador, documentos, 0, Me.clcMonto.EditValue, Me.subtotal, Me.iva, Me.clcMonto.EditValue, 0, System.DBNull.Value, Me.txtconcepto.Text, "", Comunes.Common.Sucursal_Actual, folio)
                    If Response.ErrorFound Then
                        TinApp.Connection.Rollback()
                        Response.ShowError()
                        Exit Function
                    End If
                ElseIf Not banderaabono Then
                    Response = oMovimientosCobrar.InsertarCajas(sucursal_activa, concepto, serie, cliente, 0, Me.dteFecha.EditValue, Comunes.Common.Caja_Actual, Comunes.Common.Cajero, cobrador, documentos, 0, Me.clcMonto.EditValue, Me.subtotal, Me.iva, Me.clcMonto.EditValue, 0, System.DBNull.Value, FolioPromocion, "", Comunes.Common.Sucursal_Actual, folio)
                    If Response.ErrorFound Then
                        TinApp.Connection.Rollback()
                        Response.ShowError()
                        Exit Function
                    End If
                End If


                ' se movio a este lugar la bonificacion para poder guardar el folio del recibo

                'Guardar el Registro de la Bonificacion
                If clcBonificar.EditValue > 0 Then
                    Response = oMovimientosCobrar.InsertarBonificacion(Comunes.Common.Sucursal_Actual, Comunes.Common.Caja_Actual, dteFecha.EditValue, cliente, "I", clcBonificar.EditValue, Me._BonificacionObservaciones, serie, folio)
                    If Response.ErrorFound Then
                        TinApp.Connection.Rollback()
                        Response.ShowError()
                        Exit Function
                    End If
                End If



                'Inserta los abonos o prontos pagos a los movimientos (documentos) seleccionados
                For i = 0 To Me.oTabla.Rows.Count - 1
                    If Me.oTabla.Rows(i).Item("checar") Then

                        If saldo >= Me.oTabla.Rows(i).Item("saldo_documento") Then
                            importe_documento = Me.oTabla.Rows(i).Item("saldo_documento")
                        Else
                            importe_documento = saldo
                        End If

                        If Me.oTabla.Rows(i).Item("enajenacion") = True Then
                            'DAM 16/ABR/07 SE AGREGO ESTE CALCULO PARA EL IMPORTE PROPORCIONAL DE LOS DOCUMENTOS DE LA NOTA DE CREDITO
                            dImporte_proporcional_costo = (importe_documento / dIva) * Me.oTabla.Rows(i).Item("factor_enajenacion")
                        Else
                            dImporte_proporcional_costo = 0
                        End If

                        lsucursalfactura_guardar = Me.oTabla.Rows(i).Item("sucursal")
                        sconceptofactura_guardar = Me.oTabla.Rows(i).Item("concepto") 'Me.grvMovimientos.GetRowCellValue(i, Me.grcConcepto)
                        sseriefactura_guardar = Me.oTabla.Rows(i).Item("serie")
                        dfoliofactura_guardar = Me.oTabla.Rows(i).Item("folio") 'Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio)
                        lcliente_guardar = Me.oTabla.Rows(i).Item("cliente")

                        If Documentos_Pagados.Trim.Length = 0 Then
                            Documentos_Pagados = Me.oTabla.Rows(i).Item("documentos")
                        Else
                            Documentos_Pagados = Documentos_Pagados + "," + Me.oTabla.Rows(i).Item("documentos")
                        End If




                        'GUARDA DOCUMENTOS DE CXC
                        If Me.cboTipoCobro.EditValue = "P" Then
                            '   If Not Response.ErrorFound Then 
                            numero_documento = numero_documento + 1
                            ' DAM 17/05/2006
                            sObservaciones = "Pronto Pago a la Referencia " + CType(Me.oTabla.Rows(i).Item("serie"), String) + "-" + CType(Me.oTabla.Rows(i).Item("folio"), String) + " al documento " + CType(Me.oTabla.Rows(i).Item("documento"), String)
                            Response = oMovimientosCobrarDetalle.Insertar(sucursal_activa, concepto, serie, folio, cliente, numero_documento, 0, Me.dteFecha.DateTime.Date, importe_documento, Me.oTabla.Rows(i).Item("sucursal"), Me.oTabla.Rows(i).Item("concepto"), Me.oTabla.Rows(i).Item("serie"), Me.oTabla.Rows(i).Item("folio"), Me.oTabla.Rows(i).Item("cliente"), Me.oTabla.Rows(i).Item("documento"), 0, Me.dteFecha.DateTime.Date, 0, "P", sObservaciones, dImporte_proporcional_costo * -1)
                            If Response.ErrorFound Then
                                TinApp.Connection.Rollback()
                                Response.ShowError()
                                Exit Function
                            End If

                        Else
                            '   If Not Response.ErrorFound Then
                            numero_documento = numero_documento + 1
                            sObservaciones = "Abono a la Referencia " + CType(Me.oTabla.Rows(i).Item("serie"), String) + "-" + CType(Me.oTabla.Rows(i).Item("folio"), String) + " al documento " + CType(Me.oTabla.Rows(i).Item("documento"), String)
                            Response = oMovimientosCobrarDetalle.Insertar(sucursal_activa, concepto, serie, folio, cliente, numero_documento, 0, Me.dteFecha.DateTime.Date, importe_documento, Me.oTabla.Rows(i).Item("sucursal"), Me.oTabla.Rows(i).Item("concepto"), Me.oTabla.Rows(i).Item("serie"), Me.oTabla.Rows(i).Item("folio"), Me.oTabla.Rows(i).Item("cliente"), Me.oTabla.Rows(i).Item("documento"), 0, Me.dteFecha.DateTime.Date, 0, TipoAbono, sObservaciones, dImporte_proporcional_costo * -1)
                            If Response.ErrorFound Then
                                TinApp.Connection.Rollback()
                                Response.ShowError()
                                Exit Function
                            End If
                        End If
                        saldo = saldo - importe_documento
                        If saldo = 0 Then Exit For

                    End If
                Next
                ''guarda facturas si el saldo todavia es mayor 
                If saldo > 0 And Me.cboTipoCobro.EditValue <> "P" Then

                    For i = 0 To Me.oTabla.Rows.Count - 1

                        'SE VALIDO MAS PARA QUE SOLO ABONO EN EL CASO DE SOBRAR DINERO A LAS LETRAS DE LA MISMA FACTURA
                        If Not Me.oTabla.Rows(i).Item("checar") And lsucursalfactura_guardar = Me.oTabla.Rows(i).Item("sucursal") And sconceptofactura_guardar = Me.oTabla.Rows(i).Item("concepto") And sseriefactura_guardar = Me.oTabla.Rows(i).Item("serie") And dfoliofactura_guardar = Me.oTabla.Rows(i).Item("folio") Then
                            If saldo >= Me.oTabla.Rows(i).Item("saldo_documento") Then
                                importe_documento = Me.oTabla.Rows(i).Item("saldo_documento")
                            Else
                                importe_documento = saldo
                            End If

                            If Me.oTabla.Rows(i).Item("enajenacion") = True Then
                                'DAM 16/ABR/07 SE AGREGO ESTE CALCULO PARA EL IMPORTE PROPORCIONAL DE LOS DOCUMENTOS DE LA NOTA DE CREDITO
                                dImporte_proporcional_costo = (importe_documento / dIva) * Me.oTabla.Rows(i).Item("factor_enajenacion")
                            Else
                                dImporte_proporcional_costo = 0

                            End If

                            If Documentos_Pagados.Trim.Length = 0 Then
                                Documentos_Pagados = Me.oTabla.Rows(i).Item("documentos")
                            Else
                                Documentos_Pagados = Documentos_Pagados + "," + Me.oTabla.Rows(i).Item("documentos")
                            End If



                            'GUARDA DOCUMENTOS DE CXC
                            numero_documento = numero_documento + 1
                            sObservaciones = "Abono a la Referencia " + CType(Me.oTabla.Rows(i).Item("serie"), String) + "-" + CType(Me.oTabla.Rows(i).Item("folio"), String) + " al documento " + CType(Me.oTabla.Rows(i).Item("documento"), String)
                            If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.Insertar(sucursal_activa, concepto, serie, folio, cliente, numero_documento, 0, Me.dteFecha.DateTime.Date, importe_documento, Me.oTabla.Rows(i).Item("sucursal"), Me.oTabla.Rows(i).Item("concepto"), Me.oTabla.Rows(i).Item("serie"), Me.oTabla.Rows(i).Item("folio"), Me.oTabla.Rows(i).Item("cliente"), Me.oTabla.Rows(i).Item("documento"), 0, Me.dteFecha.DateTime.Date, 0, TipoAbono, sObservaciones, dImporte_proporcional_costo * -1)
                            If Response.ErrorFound Then
                                TinApp.Connection.Rollback()
                                Response.ShowError()
                                Exit Function
                            End If
                            saldo = saldo - importe_documento 'Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                            If saldo = 0 Then Exit For
                        End If
                    Next
                End If



                'SI SOBRA SALDO SE HACE LA RUTINA PARA LAS FACTURAS DIFERENTES A LA SELCCIONADA
                If saldo > 0 And Me.cboTipoCobro.EditValue <> "P" Then

                    For i = 0 To Me.oTabla.Rows.Count - 1

                        'SE VALIDO MAS PARA QUE SOLO ABONO EN EL CASO DE SOBRAR DINERO A LAS LETRAS DIFERENTE FACTURA
                        If Not Me.oTabla.Rows(i).Item("checar") And (lsucursalfactura_guardar <> Me.oTabla.Rows(i).Item("sucursal") Or sconceptofactura_guardar <> Me.oTabla.Rows(i).Item("concepto") Or sseriefactura_guardar <> Me.oTabla.Rows(i).Item("serie") Or dfoliofactura_guardar <> Me.oTabla.Rows(i).Item("folio")) Then
                            If saldo >= Me.oTabla.Rows(i).Item("saldo_documento") Then
                                importe_documento = Me.oTabla.Rows(i).Item("saldo_documento")
                            Else
                                importe_documento = saldo
                            End If

                            If Me.oTabla.Rows(i).Item("enajenacion") = True Then
                                'DAM 16/ABR/07 SE AGREGO ESTE CALCULO PARA EL IMPORTE PROPORCIONAL DE LOS DOCUMENTOS DE LA NOTA DE CREDITO
                                dImporte_proporcional_costo = (importe_documento / dIva) * Me.oTabla.Rows(i).Item("factor_enajenacion")
                            Else
                                dImporte_proporcional_costo = 0

                            End If

                            If Documentos_Pagados.Trim.Length = 0 Then
                                Documentos_Pagados = Me.oTabla.Rows(i).Item("documentos")
                            Else
                                Documentos_Pagados = Documentos_Pagados + "," + Me.oTabla.Rows(i).Item("documentos")
                            End If



                            'GUARDA DOCUMENTOS DE CXC
                            numero_documento = numero_documento + 1
                            sObservaciones = "Abono a la Referencia " + CType(Me.oTabla.Rows(i).Item("serie"), String) + "-" + CType(Me.oTabla.Rows(i).Item("folio"), String) + " al documento " + CType(Me.oTabla.Rows(i).Item("documento"), String)
                            If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.Insertar(Me.oTabla.Rows(i).Item("sucursal"), concepto, serie, folio, cliente, numero_documento, 0, Me.dteFecha.DateTime.Date, importe_documento, Me.oTabla.Rows(i).Item("sucursal"), Me.oTabla.Rows(i).Item("concepto"), Me.oTabla.Rows(i).Item("serie"), Me.oTabla.Rows(i).Item("folio"), Me.oTabla.Rows(i).Item("cliente"), Me.oTabla.Rows(i).Item("documento"), 0, Me.dteFecha.DateTime.Date, 0, TipoAbono, sObservaciones, dImporte_proporcional_costo * -1)
                            If Response.ErrorFound Then
                                TinApp.Connection.Rollback()
                                Response.ShowError()
                                Exit Function
                            End If
                            saldo = saldo - importe_documento 'Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                            If saldo = 0 Then Exit For

                        End If
                    Next
                End If


            End If


            'If Not Response.ErrorFound Then
            '    Response = oMovimientosCobrar.ActualizarObservaciones(sucursal_activa, concepto, serie, folio, cliente, Documentos_Pagados)
            '    If Response.ErrorFound Then
            '        TinApp.Connection.Rollback()
            '        Response.ShowError()
            '        Exit Function
            '    End If
            'End If



            If Not Response.ErrorFound Then
                Response = GuardarFolioAutorizacion()
                If Response.ErrorFound Then
                    TinApp.Connection.Rollback()
                    Response.ShowError()
                    Exit Function
                End If
            End If

            ' Manda a recalcular el saldo del cliente despues de haber generado todo el proceso de pagos
            If Not Response.ErrorFound Then
                Response = oClientes.RecalcularSaldosClientes(cliente)
                If Response.ErrorFound Then
                    TinApp.Connection.Rollback()
                    Response.ShowError()
                    Exit Function
                End If
            End If



            'Manda a Actualizar el folio de la promocion en el caso de que se haya aplicado
            'DAM 11/12/2013
            If bImprimeReciboMenosManejaPromocion Then
                If Not Response.ErrorFound Then
                    Response = oSucursales.ActualizarFolioPromocion(Common.Sucursal_Actual, FolioPromocion)
                    If Response.ErrorFound Then
                        TinApp.Connection.Rollback()
                        Response.ShowError()
                        Exit Function
                    End If
                End If
            End If

            guardarpago()


            If Me.factura_electronica Then

                If TieneNotaCargoIntereses Then
                    If TieneIvaDesglosadoFacturaNotaCargo Then
                        Response = Me.oMovimientosCobrar.LllenarNotaCargoCFDI(sucursal_activa, conceptointereses, serieinteres, foliointeres, cliente, True, Common.Sucursal_Actual)

                        If Response.ErrorFound Then
                            ImprimirComprobanteFiscalSimplicado = False
                            TinApp.Connection.Rollback()
                            Response.ShowError()
                            Exit Function

                        End If
                    Else
                        ImprimirComprobanteFiscalSimplicado = True

                    End If
                End If

                If bImprimeReciboMenosManejaPromocion Then
                    ' Si usa maneja promocion el ultimo documento se manda a timbrar como notaa de credito
                    'Se llena la nota de credito Electronica

                    Response = Me.oMovimientosCobrar.LlenaNotaCreditoCFDI(sucursal_activa, concepto, serie, folio, cliente, False, Common.Sucursal_Actual, False)
                    If Response.ErrorFound Then
                        TinApp.Connection.Rollback()
                        Response.ShowError()
                        Exit Function
                    End If
                End If

                ' Si esta configurado para timbrar abonos/recibos se manda a llenar la tabla para que sean timbrados
                If timbrar_recibos Then
                    ' Si no usa promocion el recibo se manda a timbrar como abono
                    If Me.bImprimeReciboMenosManejaPromocion = False Then
                        'Si utiliza Factura Electronica manda llenar las tablas Detecno
                        Response = Me.oMovimientosCobrar.LlenaAbonoCFDI(sucursal_activa, concepto, serie, folio, cliente, TipoAbono, Common.Sucursal_Actual)
                        If Response.ErrorFound Then
                            TinApp.Connection.Rollback()
                            Response.ShowError()
                            Exit Function
                        End If
                    End If


                End If
            End If

            ' =================================
            TinApp.Connection.Commit()
            ' =================================

            bReciboImpreso = False

            Me.clcImporteDocumentos.Value = 0
            Me.clcIntereses.Value = 0
            Me.clcBonificar.Value = 0
            Me.clcMonto.Value = 0



            If Not Me.factura_electronica Then

                If Me.cboTipoCobro.EditValue = "A" Then
                    Me.ImprimirAbono(saldo)
                End If
                If banderanotacargo = True And Not bReciboImpreso Then
                    Me.ImprimirNotaCargo(conceptointereses, serieinteres, saldo_intereses, foliointeres)
                End If

                If Me.cboTipoCobro.EditValue = "E" Or Me.cboTipoCobro.EditValue = "C" Then
                    Me.Imprimirengancheycontado()
                ElseIf Me.cboTipoCobro.EditValue = "P" Then
                    Me.ImprimirNotacredito()
                End If

                If Me.cboTipoCobro.EditValue = "N" Then
                    Me.ImprimirPagoNotaCargoDescAnt()
                End If

                If Me.FolioVale > 0 Then
                    ImprimirVale(FolioVale)
                End If
            Else

                If imprimir_recibo_no_fiscal Then
                    If bImprimeReciboMenosManejaPromocion Then
                        ImprimirTicket(True)
                    Else
                        ImprimirTicket(False)
                    End If

                End If


                'If Not timbrar_recibos Then
                '    ImprimirTicket(False)
                'End If

                'If imprimir_recibo_no_fiscal And bImprimeReciboMenosManejaPromocion Then
                '    ImprimirTicket(True)
                'End If

                'Usa Factura Electronica
                If ImprimirComprobanteFiscalSimplicado = True And timbrar_recibos = True And imprimir_recibo_no_fiscal = False Then
                    Imprimir_ComprobanteFiscalSimplicado(conceptointereses, serieinteres, saldo_intereses, foliointeres)
                End If
            End If

            Me.lkpCliente.EditValue = Nothing


            Llenargrid(False)

        End If

        banValidar = False


    End Function
    Private Function GuardarFolioAutorizacion() As Events
        Dim response As New Events
        If Me.Folio_Bonificacion > 0 Then
            response = Me.oAutorizacionesBonificacionesInteres.ActualizarCaja(Me.Folio_Bonificacion, True)
            If response.ErrorFound Then
                response.Message = "Error al Actualizar el Folio de Autorizaci�n de la Bonificaci�n de Interes"
            End If
        End If

        Return response
    End Function
    Private Function ObtenerPrioridad(ByRef tipoabono As Char) As Long
        Dim response As New Events
        Dim valor As Integer

        response = Me.oMovimientosCobrar.MovimientosCliente(cliente)
        If Not response.ErrorFound Then
            valor = response.Value

            Select Case valor 'resultado del SP

                Case 2  'enganches
                    tipoabono = ""
                    Return 1
                Case 3 ' notas cargo
                    tipoabono = ""
                    Return 2
                Case 4 ' abonos
                    tipoabono = "A"
                    Return 0
            End Select


            'Select Case valor 'resultado del SP
            '    Case 1 'contado 
            '        Return 2
            '    Case 2 ' enganches
            '        Return 1
            '    Case 3 ' notas cargo
            '        Return 3
            '    Case 4 ' abonos
            '        Return 0
            'End Select
        End If

    End Function
    Private Function ValidaExisteEnganchesVencidos(ByVal cliente As Long) As Boolean

        Dim response As Dipros.Utils.Events
        response = Me.oMovimientosCobrar.ExisteEnganchesVencidos(cliente)
        If Not response.ErrorFound Then
            If CType(response.Value, Int16) = 0 Then
                ValidaExisteEnganchesVencidos = False
            Else
                ValidaExisteEnganchesVencidos = True
            End If

        End If
    End Function



    Private Function ImporteCadena(ByVal valor As Double) As String
        Dim texto As String
        Dim decimales As String()

        texto = CType(valor, String)

        If InStr(texto, ".") > 0 Then
            'If (Truncate(CType(texto, Decimal)) - valor) > 0 Then

            decimales = texto.Split(".")
            If CType(decimales.GetValue(1), String).Length = 4 Then
                texto = CType(decimales.GetValue(1), String).Remove(2, 2)
                Return "$" + decimales.GetValue(0) + "." + texto
            Else
                Return "$" + texto
            End If

        Else
            Return "$" + texto + ".00"
        End If
    End Function
    Public Sub MostrarFormaPagarCajaCambio(ByVal folio_Vale As Long, ByVal importe_vale As Decimal)

        FolioVale = folio_Vale

        Dim oform As New frmMovimientosCajaCambio
        Dim valor As Decimal = Me.clcMonto.Value

        oform.MdiParent = Me.MdiParent
        oform.OwnerForm = Me
        oform.ManejaFacturaElectronica = Me.factura_electronica
        oform.SolicitaUltimosDigitos = Me.VerificarUtilizaUltimosDigitos()
        oform.lblImportePagar.Text = ImporteCadena(Me.clcMonto.Value)
       

        oform.Show()

        If FolioVale > 0 Then
            oform.ImportePagado = importe_vale
        Else
            oform.ImportePagado = ValorDolares()
        End If

        Me.Enabled = False

        Exit Sub
    End Sub
    Public Sub MostrarFormaValidacionVales(ByVal ImporteVales As Double)
        Dim oformVerificaFolioVale As New frmMovimientosCajaPagoVales
        oformVerificaFolioVale.MdiParent = Me.MdiParent
        oformVerificaFolioVale.OwnerForm = Me
        oformVerificaFolioVale.Cliente = Me.cliente
        oformVerificaFolioVale.Concepto = Me.ConceptoSeleccionadoDocumento
        oformVerificaFolioVale.Title = "Validando Vales"

        oformVerificaFolioVale.ImporteVale = ImporteCadena(ImporteVales)
        Me.Enabled = False
        oformVerificaFolioVale.Show()
    End Sub
    Private Sub MostrarFormaBonificacionObservaciones()
        Dim oformVerificaBonificacionObservaciones As New frmMovimientosCajaBonificacionObservaciones
        oformVerificaBonificacionObservaciones.MdiParent = Me.MdiParent
        oformVerificaBonificacionObservaciones.OwnerForm = Me
        oformVerificaBonificacionObservaciones.VentanaPadreAnterior = frmMovimientosCajaBonificacionObservaciones.VentanaPadre.Caja
        oformVerificaBonificacionObservaciones.Title = "Observaciones de la Bonificaci�n"

        Me.Enabled = False
        oformVerificaBonificacionObservaciones.Show()
    End Sub

    Private Sub Imprimir_ComprobanteFiscalSimplicado(ByVal conceptointereses As String, ByVal serieinteres As String, ByVal saldo_intereses As Double, ByVal foliointeres As Long)
        Dim response As New Events

        response = oReportes.ImprimeNotaDeCargo(sucursal_activa, conceptointereses, serieinteres, foliointeres, cliente, False)


        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El comprobante fiscal simplificado no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptComprobanteFiscalSimplicado

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)


                'TinApp.ShowReport(Me.MdiParent, "Impresi�n del comprobante fiscal simplificado ", oReport)
                TinApp.PrintReport(oReport, False, False)

                oDataSet = Nothing
                oReport = Nothing

            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")


            End If
        End If

    End Sub

    Private Sub ObtenerSerieAbonosElectronicos(ByRef serie As String, ByVal Tipo As TipoDocumentoElectronico)
        Dim Response As Dipros.Utils.Events
        Dim odataset As DataSet
        serie = ""


        Response = oSucursales.DespliegaDatos(Common.Sucursal_Actual)
        If Response.ErrorFound = False Then
            odataset = Response.Value


            Select Case Tipo
                Case TipoDocumentoElectronico.Abonos
                    serie = CType(odataset.Tables(0).Rows(0).Item("serie_recibos_electronica"), String)
                Case TipoDocumentoElectronico.NotaCredito
                    serie = CType(odataset.Tables(0).Rows(0).Item("serie_nota_credito_electronica"), String)
                Case TipoDocumentoElectronico.NotaCargo
                    serie = CType(odataset.Tables(0).Rows(0).Item("serie_nota_cargo_electronica"), String)
            End Select


        End If
    End Sub

    Private Sub ImprimirTicket(ByVal ImprimeTickeMenosPromocion As Boolean)
        Dim response As New Events

        Dim serie_referencia As String
        Dim folio_referencia As Integer
        Dim documento_referencia As Integer
        Dim sucursal_referencia As Integer

        Dim serie_referencia_anterior As String
        Dim folio_referencia_anterior As Integer
        Dim sucursal_referencia_anterior As Integer

        Dim RutaImpresoraTicket As String
        RutaImpresoraTicket = clsUtilerias.uti_RutaImpresionTicket(Common.Caja_Actual)

        If RutaImpresoraTicket.Trim.Length = 0 Then
            ShowMessage(MessageType.MsgError, "No se puede imprimir el ticket por que la caja no tiene una ruta de impresi�n", "Impresi�n de Ticket")
        Else
            documento_referencia = 0

            Dim i As Integer
            For i = 0 To Me.grvMovimientos.RowCount - 1

                If Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) = True Then

                    documento_referencia = Me.grvMovimientos.GetRowCellValue(i, Me.grcDocumento)
                    serie_referencia = Me.grvMovimientos.GetRowCellValue(i, Me.grcSerie)
                    folio_referencia = Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio)
                    sucursal_referencia = Me.grvMovimientos.GetRowCellValue(i, grcSucursal)

                    If i > 0 Then
                        If (sucursal_referencia = sucursal_referencia_anterior) And (serie_referencia = serie_referencia_anterior) And (folio_referencia = folio_referencia_anterior) Then
                            Exit For
                        End If

                    End If



                    If ImprimeTickeMenosPromocion = False Then
                        response = oReportes.ImprimirabonoTicket(sucursal_activa, concepto, serie, folio, cliente, serie_referencia, folio_referencia, documento_referencia)
                    Else
                        response = oReportes.ImprimirabonoTicketPromocion(sucursal_activa, concepto, serie, folio, cliente)
                    End If


                    If response.ErrorFound Then
                        ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
                    Else
                        If response.Value.Tables(0).Rows.Count > 0 Then

                            Dim oDataSet As DataSet

                            If ImprimeTickeMenosPromocion Then
                                Dim oReport As New rptTicketMenos

                                oDataSet = response.Value
                                oReport.DataSource = oDataSet.Tables(0)
                                oReport.RutaImpresora = RutaImpresoraTicket

                                oreport.lblSaldo.Visible = True
                                oreport.txtSaldo.Visible = True
                                oreport.lblMetodoPago.Visible = True
                                oreport.txtMetodoPago.Visible = True



                                bReciboImpreso = True

                                If TinApp.Connection.User.ToUpper = "SUPER" Then
                                    TinApp.ShowReport(Me.MdiParent, "Impresi�n del Ticket ", oReport, , , True, True)
                                Else

                                    TinApp.PrintReport(oReport)
                                End If

                                oReport = Nothing
                            Else

                                Dim oReport As New rptTicket

                                oDataSet = response.Value
                                oReport.DataSource = oDataSet.Tables(0)
                                oReport.RutaImpresora = RutaImpresoraTicket

                                bReciboImpreso = True

                                If TinApp.Connection.User.ToUpper = "SUPER" Then
                                    TinApp.ShowReport(Me.MdiParent, "Impresi�n del Ticket ", oReport, , , True, True)
                                Else

                                    TinApp.PrintReport(oReport)
                                End If

                                If Me.lkpCliente.GetValue("email_abogado") <> "" Then
                                    'CREA UN PDF Y LO MANDA POR EMAIL AL ABOGADO
                                    GeneraReporteEnPDFyEnviaEmail(oReport, "Impresi�n del Ticket ", oDataSet, 1)
                                End If

                                oReport = Nothing
                            End If

                            If banderanotacargo = True Then
                                Me.ImprimirNotaCargo(conceptointereses, serieinteres, saldo_intereses, foliointeres)
                            End If

                            oDataSet = Nothing

                        Else

                            ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                        End If
                    End If

                    serie_referencia_anterior = serie_referencia
                    folio_referencia_anterior = folio_referencia
                    sucursal_referencia_anterior = sucursal_referencia

                End If
            Next

        End If
    End Sub


    Private Function ObtenerFolioPromocion() As String
        Dim Response As Dipros.Utils.Events
        Dim odataset As DataSet

        Dim FolioPromocion As Integer = 0

        Response = oSucursales.DespliegaDatos(Common.Sucursal_Actual)
        If Response.ErrorFound = False Then
            odataset = Response.Value
            FolioPromocion = CType(odataset.Tables(0).Rows(0).Item("folio_promocion"), Integer)
            FolioPromocion = FolioPromocion + 1
        End If

        Return FolioPromocion.ToString()
    End Function


    Private Function ValidaSoloUltimoDocumento(ByVal Fila As Long, ByVal bEsUltimoDocumento As Boolean, ByVal bManejaPromocion As Boolean, ByVal dDiasVencidos As Double, ByVal bTieneIvaDesglosadoFactura As Boolean, ByVal bMismosDoctosVentaPlan As Boolean) As Boolean
        Dim valido As Boolean = True
        If bImprimeReciboMenosManejaPromocion = False And FilaManejaPromocion = 0 Then
            If bEsUltimoDocumento And bManejaPromocion And dDiasVencidos <= 0 And bTieneIvaDesglosadoFactura = False And bMismosDoctosVentaPlan Then
                Dim i As Long
                For i = 0 To Me.grvMovimientos.RowCount - 1
                    If Fila <> i Then
                        If Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) Then
                            valido = False
                            Exit Function

                        End If

                    End If
                Next i

                FilaManejaPromocion = Fila
                Me.bImprimeReciboMenosManejaPromocion = True
                valido = True
            End If
        Else
            If Fila = FilaManejaPromocion Then
                bImprimeReciboMenosManejaPromocion = False
                FilaManejaPromocion = 0
                valido = True
            Else
                valido = False
            End If

        End If

        Return valido
    End Function




    Private Sub TraeDatosVariables()

        Dim response As New Events

        response = oVariables.DespliegaDatos()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet

            oDataSet = response.Value
            ConceptoAbono = CType(oDataSet.Tables(0).Rows(0).Item("concepto_cxc_abono"), String)
            ConceptoNotacredito = CType(oDataSet.Tables(0).Rows(0).Item("concepto_cxc_nota_credito"), String)
            ConceptoNotacargo = CType(oDataSet.Tables(0).Rows(0).Item("concepto_cxc_nota_cargo"), String)

            Me.CobradorCasa = CType(oDataSet.Tables(0).Rows(0).Item("cobrador_casa"), Long)
            Me.Conceptointeres = CType(oDataSet.Tables(0).Rows(0).Item("concepto_intereses"), String)
            Me.ConceptoPromocion = CType(oDataSet.Tables(0).Rows(0).Item("concepto_descuentos"), String)

            ImpuestoVariables = CType(oDataSet.Tables(0).Rows(0).Item("impuesto"), Double)
            Me.FormaPagoVales = CType(oDataSet.Tables(0).Rows(0).Item("pago_vales_caja"), Long)
            Me.FormaPagoEfectivo = CType(oDataSet.Tables(0).Rows(0).Item("pago_efectivo_caja"), Long)




            If ConceptoAbono <> "" Then
                banConceptoAbono = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de CXC de Abono no esta definido", "Variables del Sistema", Nothing, False)
            End If

            If ConceptoNotacredito <> "" Then
                banConceptoNotacredito = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de  CXC de Nota Credito no esta definido", "Variables del Sistema", Nothing, False)
            End If


            If ConceptoNotacargo <> "" Then
                banConceptoNotacargo = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de  CXC de Nota de Cargo no esta definido", "Variables del Sistema", Nothing, False)
            End If

            If CobradorCasa <> 0 Then
                banCobradorCasa = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Cobrador de Variales del Sistema no esta definido", "Variables del Sistema", Nothing, False)
            End If

            If Conceptointeres <> "" Then
                banConceptoInteres = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de  Intereses no esta definido", "Variables del Sistema", Nothing, False)
            End If

            If Me.ImpuestoVariables > 0 Then
                banImpuestoVariables = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Porcentaje de Impuesto no esta definido", "Variables del Sistema", Nothing, False)
            End If


            If FormaPagoVales > 0 Then
                banFormaPagoVales = True
            Else
                ShowMessage(MessageType.MsgInformation, "La forma de Pago para Vales no esta definido", "Variables del Sistema", Nothing, False)
            End If

            If FormaPagoEfectivo > 0 Then
                banFormaPagoEfectivo = True
            Else
                ShowMessage(MessageType.MsgInformation, "La forma de Pago para Efectivo no esta definido", "Variables del Sistema", Nothing, False)
            End If

            'If ConceptoPromocion <> "" Then
            '    banConceptoPromocion = True
            'Else
            '    ShowMessage(MessageType.MsgInformation, "El Concepto de Promoci�n no esta definido", "Variables del Sistema", Nothing, False)
            'End If


        End If

    End Sub
#End Region


End Class


