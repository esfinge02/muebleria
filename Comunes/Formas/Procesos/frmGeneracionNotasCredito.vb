Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmGeneracionNotasCredito
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcFolioFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEnajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfactor_enajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnLlenar As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnVaciar As System.Windows.Forms.ToolBarButton
    Friend WithEvents tlbNotasCredito As System.Windows.Forms.ToolBar
    Friend WithEvents tblImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcClaveCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcIvaDesglosado As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGeneracionNotasCredito))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEnajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfactor_enajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClaveCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tlbNotasCredito = New System.Windows.Forms.ToolBar
        Me.btnLlenar = New System.Windows.Forms.ToolBarButton
        Me.btnVaciar = New System.Windows.Forms.ToolBarButton
        Me.tblImprimir = New System.Windows.Forms.ToolBarButton
        Me.grcIvaDesglosado = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tblImprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1051, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(8, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 59
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(64, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(264, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 60
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(16, 64)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 61
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 6, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(64, 64)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 62
        Me.dteFecha.Tag = "fecha"
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(8, 120)
        Me.grFacturas.MainView = Me.grvFacturas
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grFacturas.Size = New System.Drawing.Size(480, 312)
        Me.grFacturas.TabIndex = 63
        Me.grFacturas.Text = "Articulos"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcPartida, Me.grcFactura, Me.grcCliente, Me.grcImporte, Me.grcFolioFactura, Me.grcEnajenacion, Me.grcfactor_enajenacion, Me.grcClaveCliente, Me.grcIvaDesglosado})
        Me.grvFacturas.GridControl = Me.grFacturas
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsCustomization.AllowFilter = False
        Me.grvFacturas.OptionsCustomization.AllowGroup = False
        Me.grvFacturas.OptionsCustomization.AllowSort = False
        Me.grvFacturas.OptionsMenu.EnableFooterMenu = False
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        Me.grvFacturas.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 50
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "clave_compuesta"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 1
        Me.grcFactura.Width = 76
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "nombre_cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.VisibleIndex = 3
        Me.grcCliente.Width = 267
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "$###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe_bonificar"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 85
        '
        'grcFolioFactura
        '
        Me.grcFolioFactura.Caption = "Folio Factura"
        Me.grcFolioFactura.FieldName = "folio_factura"
        Me.grcFolioFactura.Name = "grcFolioFactura"
        Me.grcFolioFactura.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcEnajenacion
        '
        Me.grcEnajenacion.Caption = "Enajenacion"
        Me.grcEnajenacion.FieldName = "enajenacion"
        Me.grcEnajenacion.Name = "grcEnajenacion"
        Me.grcEnajenacion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcfactor_enajenacion
        '
        Me.grcfactor_enajenacion.Caption = "Factor Enajenacion"
        Me.grcfactor_enajenacion.FieldName = "factor_enajenacion"
        Me.grcfactor_enajenacion.Name = "grcfactor_enajenacion"
        Me.grcfactor_enajenacion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcClaveCliente
        '
        Me.grcClaveCliente.Caption = "Cliente"
        Me.grcClaveCliente.FieldName = "cliente"
        Me.grcClaveCliente.Name = "grcClaveCliente"
        Me.grcClaveCliente.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcClaveCliente.VisibleIndex = 2
        '
        'tlbNotasCredito
        '
        Me.tlbNotasCredito.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tlbNotasCredito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tlbNotasCredito.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnLlenar, Me.btnVaciar})
        Me.tlbNotasCredito.ButtonSize = New System.Drawing.Size(77, 22)
        Me.tlbNotasCredito.Dock = System.Windows.Forms.DockStyle.None
        Me.tlbNotasCredito.DropDownArrows = True
        Me.tlbNotasCredito.ImageList = Me.ilsToolbar
        Me.tlbNotasCredito.Location = New System.Drawing.Point(8, 94)
        Me.tlbNotasCredito.Name = "tlbNotasCredito"
        Me.tlbNotasCredito.ShowToolTips = True
        Me.tlbNotasCredito.Size = New System.Drawing.Size(480, 29)
        Me.tlbNotasCredito.TabIndex = 64
        Me.tlbNotasCredito.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right
        '
        'btnLlenar
        '
        Me.btnLlenar.ImageIndex = 4
        Me.btnLlenar.Text = "Todos"
        '
        'btnVaciar
        '
        Me.btnVaciar.ImageIndex = 1
        Me.btnVaciar.Text = "Ninguno"
        '
        'tblImprimir
        '
        Me.tblImprimir.ImageIndex = 6
        Me.tblImprimir.Text = "Imprimir"
        Me.tblImprimir.ToolTipText = "Impresion del Reporte de Notas de Cr�dito"
        '
        'grcIvaDesglosado
        '
        Me.grcIvaDesglosado.Caption = "IvaDesglosado"
        Me.grcIvaDesglosado.ColumnEdit = Me.chkSeleccionar
        Me.grcIvaDesglosado.FieldName = "ivadesglosado"
        Me.grcIvaDesglosado.Name = "grcIvaDesglosado"
        Me.grcIvaDesglosado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcIvaDesglosado.VisibleIndex = 5
        '
        'frmGeneracionNotasCredito
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(498, 440)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.tlbNotasCredito)
        Me.Name = "frmGeneracionNotasCredito"
        Me.Text = "frmGeneracionNotasCredito"
        Me.Controls.SetChildIndex(Me.tlbNotasCredito, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Public oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar

    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oVariables As VillarrealBusiness.clsVariables
    Private intCajero As Long

    Private sConceptoDescuentosAnticipados As String = ""
    Private sConceptoFacturaCredito As String = ""

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property Concepto_Descuentos_Anticipados() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_descuentos_anticipados", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property Concepto_Factura_Credito() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property Cajero() As Long
        Get
            If Comunes.clsUtilerias.uti_Usuariocajero(CStr(TinApp.Connection.User), intCajero) = True Then
                Return intCajero
            Else
                Return -1
            End If
        End Get
    End Property

    Private folio_movimiento As Long = 0
    Private factura_electronica As Boolean = False
    Private Serie As String = ""


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmGeneracionNotasCredito_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmGeneracionNotasCredito_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmGeneracionNotasCredito_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        If (Not Me.factura_electronica) Then
            Dim orow As DataRow
            For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows
                If orow("seleccionar") = True Then
                    Me.ImprimeNotaDeCredito(Sucursal, sConceptoDescuentosAnticipados, Serie, orow("folio_movimiento"), orow("cliente"), True)
                    'Me.ImprimeNotaDeCredito(Sucursal, sConceptoDescuentosAnticipados, Serie, orow("folio_movimiento"), orow("cliente"), orow("ivadesglosado"))
                End If
            Next
        End If
    End Sub

    Private Sub frmGeneracionNotasCredito_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oVariables = New VillarrealBusiness.clsVariables

        Me.dteFecha.EditValue = CDate(TinApp.FechaServidor)

      
        If Concepto_Descuentos_Anticipados.Length = 0 Then
            Me.tbrTools.Buttons.Item(0).Visible = False
            Me.tbrTools.Buttons.Item(0).Enabled = False
            ShowMessage(MessageType.MsgInformation, "El Concepto de  CXC de Descuentos Anticipados no esta definido", "Variables del Sistema", Nothing, False)
        End If
    End Sub
    Private Sub frmGeneracionNotasCredito_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Dim impuesto As Double
        Dim subtotal As Double
        Dim iva As Double
        Dim dImporte_proporcional_costo As Double
        Dim dIVa As Double


        If Not factura_electronica Then         
            Serie = Comunes.clsUtilerias.uti_SerieCajasNotaCredito(Comunes.Common.Caja_Actual)
        End If



        sConceptoDescuentosAnticipados = Concepto_Descuentos_Anticipados
        sConceptoFacturaCredito = Concepto_Factura_Credito
        impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)

        Dim orow As DataRow
        For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows
            If Not Response.ErrorFound And orow("seleccionar") = True Then

                If impuesto <> -1 Then

                    subtotal = orow("importe_bonificar") / (1 + (impuesto / 100))
                    iva = orow("importe_bonificar") - subtotal

                    dIVa = 1 + (impuesto / 100)
                Else
                    Response.Message = "El Porcentaje de Impuesto no esta definido"
                    Exit Sub
                End If

                If Not Response.ErrorFound Then
                    Response = oMovimientosCobrar.InsertarCajas(Sucursal, sConceptoDescuentosAnticipados, Serie, orow("cliente"), 0, _
                                                      Me.dteFecha.DateTime, Comunes.Common.Caja_Actual, Cajero, System.DBNull.Value, 1, 0, orow("importe_bonificar"), subtotal, iva, _
                                                     orow("importe_bonificar"), 0, System.DBNull.Value, " Descuento Anticipado", "", Sucursal, orow("folio_movimiento"))

                    'DAM 17/05/2006 -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO
                    Dim observaciones As String
                    observaciones = "Nota de Cr�dito generada para la Venta " + CType(orow("serie"), String) + "-" + CType(orow("folio_factura"), String)

                    If orow("enajenacion") = True Then
                        'DAM 16/ABR/07 SE AGREGO ESTE CALCULO PARA EL IMPORTE PROPORCIONAL DE LOS DOCUMENTOS DE LA NOTA DE CREDITO
                        dImporte_proporcional_costo = (orow("importe_bonificar") / dIVa) * orow("factor_enajenacion")
                        dImporte_proporcional_costo = dImporte_proporcional_costo * -1
                    Else
                        dImporte_proporcional_costo = 0

                    End If

                    If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.Insertar(Sucursal, sConceptoDescuentosAnticipados, Serie, orow("folio_movimiento"), orow("cliente"), _
                                                         1, 0, Me.dteFecha.DateTime, orow("importe_bonificar"), Sucursal, sConceptoFacturaCredito, orow("serie"), orow("folio_factura"), orow("cliente"), orow("numero_documentos"), 0, System.DBNull.Value, 0, "D", observaciones, dImporte_proporcional_costo)

                    observaciones = Nothing

                    ' Checa si existe el documento al cual se le va aplicar la nota de credito, y si existe se actualiza el bit de nota de credito impresa
                    If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.ExisteDocumento(Sucursal, sConceptoFacturaCredito, orow("serie"), orow("folio_factura"), orow("cliente"), orow("numero_documentos"))
                    Dim ExisteDocumento As Boolean
                    ExisteDocumento = CType(Response.Value, Boolean)
                    If Not Response.ErrorFound And ExisteDocumento = True Then Response = oVentas.ActualizaImpresaNotaCredito(Sucursal, orow("serie"), orow("folio_factura"), orow("importe_bonificar"))

                    'Se verifica si usa factura electronica y no hubo un error anterior
                    If Me.factura_electronica And Not Response.ErrorFound Then
                        'Se llena la nota de credito Electronica
                        Response = Me.oMovimientosCobrar.LlenaNotaCreditoCFDI(Sucursal, sConceptoDescuentosAnticipados, Serie, orow("folio_movimiento"), orow("cliente"), True, Common.Sucursal_Actual, False)
                        'Response = Me.oMovimientosCobrar.LlenaNotaCreditoCFDI(Sucursal, sConceptoDescuentosAnticipados, Serie, orow("folio_movimiento"), orow("cliente"), orow("ivadesglosado"), Common.Sucursal_Actual, False)
                    End If

                End If
            End If
        Next

        

    End Sub
    Private Sub frmGeneracionNotasCredito_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = ValidaGrid()
        If Not Response.ErrorFound Then Response = ValidaCajero()
    End Sub
    Private Sub frmGeneracionNotasCredito_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        CargarFacturas()
        If Me.Sucursal > 0 Then
            ' Datos de Sucursal
            '-----------------------------
        
            factura_electronica = CType(Me.lkpSucursal.GetValue("factura_electronica"), Boolean)
            Serie = CType(Me.lkpSucursal.GetValue("serie_nota_credito_electronica"), String)
            '-----------------------------

        End If
    End Sub
    Private Sub dteFecha_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha.EditValueChanged

        CargarFacturas()
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button.Text = "Imprimir" Then
            ImprimeListadoNotasCredito()
        End If

    End Sub
    Private Sub tlbNotasCredito_ButtonClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tlbNotasCredito.ButtonClick
        If e.Button Is btnLlenar Then
            Dim oRow As DataRow
            For Each oRow In CType(grvFacturas.DataSource, DataView).Table.Rows
                oRow.Item("seleccionar") = True
            Next
        ElseIf e.Button Is btnVaciar Then
            Dim oRow As DataRow
            For Each oRow In CType(grvFacturas.DataSource, DataView).Table.Rows
                oRow.Item("seleccionar") = False
            Next
        End If

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarFacturas()
        Dim Response As Dipros.Utils.Events
        Try
            Response = oVentas.VentasCreditoPorFecha(Sucursal, Me.dteFecha.EditValue)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grFacturas.DataSource = oDataSet.Tables(0)

                If Me.grFacturas.DataSource Is Nothing Then
                    tlbNotasCredito.Enabled = False
                Else
                    If CType(Me.grvFacturas.DataSource, DataView).Table.Rows.Count > 0 Then
                        tlbNotasCredito.Enabled = True
                    Else
                        tlbNotasCredito.Enabled = False
                    End If

                End If

                oDataSet = Nothing
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Function ValidaGrid() As Events
        Dim oevent As New Events

        Me.grvFacturas.CloseEditor()
        Me.grvFacturas.UpdateCurrentRow()

        If CType(Me.grFacturas.DataSource, DataTable).Select("seleccionar = 1").Length <= 0 Then
            oevent.Ex = Nothing
            oevent.Message = "Se Debe Seleccionar Al Menos Una Factura"
        End If
        Return oevent
    End Function
    Private Function ValidaCajero() As Events
        Dim oevent As New Events
        If Cajero <= 0 Then
            oevent.Ex = Nothing
            oevent.Message = "El Usuario No Tiene un Cajero Asignado"
        End If
        Return oevent
    End Function
    Private Sub ImprimeListadoNotasCredito()
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes
        Try
            Response = oReportes.ListadoNotasDeCredito(Sucursal, dteFecha.EditValue)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "La Impresi�n de las Notas de Cr�dito no pueden Mostrarse")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New Comunes.rptNotasCredito
                    oReport.DataSource = oDataSet.Tables(0)

                    TINApp.ShowReport(Me.MdiParent, "Impresi�n de Listado de Notas de Cr�dito", oReport)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try

    End Sub
    Private Sub ImprimeNotaDeCredito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal iva_desglosado As Boolean)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            Response = oReportes.ImprimeNotaDeCredito(sucursal, concepto, serie, folio, cliente, iva_desglosado)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Las Notas de Cr�dito no pueden Mostrarse")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New Comunes.rptNotaDeCredito
                    oReport.DataSource = oDataSet.Tables(0)

                    TINApp.PrintReport(oReport)
                    'TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cr�dito " & CType(folio, String), oReport)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

    
#End Region

    Dim _drcFolioFactura
End Class