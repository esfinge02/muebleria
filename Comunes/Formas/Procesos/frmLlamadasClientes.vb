Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Drawing
Imports System.Windows.Forms

Public Class frmLlamadasClientes
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
    Public sucursal_dependencia As Boolean = False
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblMunicipio As System.Windows.Forms.Label
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblLocalidad As System.Windows.Forms.Label
    Friend WithEvents lkpCiudad As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblTelefono_Casa As System.Windows.Forms.Label
    Friend WithEvents txtTelefono_Casa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCodigo_Postal As System.Windows.Forms.Label
    Friend WithEvents clcCodigo_Postal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSaldoActual As System.Windows.Forms.Label
    Friend WithEvents lblSaldoVencido As System.Windows.Forms.Label
    Friend WithEvents lblIntereses As System.Windows.Forms.Label
    Friend WithEvents lblSaldoTotal As System.Windows.Forms.Label
    Friend WithEvents lblImporteAbonado As System.Windows.Forms.Label
    Friend WithEvents lblUltimoAbono As System.Windows.Forms.Label
    Friend WithEvents clcSaldoActual As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcSaldoVencido As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcIntereses As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcSaldoTotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcImporteAbonado As Dipros.Editors.TINCalcEdit
    Friend WithEvents dteUltimoAbono As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFecha_Hora As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Hora As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grMovimientos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvMovimientos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcChecar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkChecar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcInteres As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumentos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grLlamadasClientes As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvLlamadasClientes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaLlamadasClientes As Dipros.Windows.TINMaster
    Friend WithEvents grcFechaLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTelefono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcContesto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcObservaciones As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClienteLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoLlamada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoTelefono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcParentescoPersonaContesta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMensajeDejado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMensajeRecibido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUsuarioCapturo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaVencimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfechapago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcParentesco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDes_parentesco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPromesa_Pago As DevExpress.XtraGrid.Columns.GridColumn

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLlamadasClientes))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblDireccion = New System.Windows.Forms.Label
        Me.txtDireccion = New DevExpress.XtraEditors.TextEdit
        Me.lblEstado = New System.Windows.Forms.Label
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.lblMunicipio = New System.Windows.Forms.Label
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lblLocalidad = New System.Windows.Forms.Label
        Me.lkpCiudad = New Dipros.Editors.TINMultiLookup
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.lblTelefono_Casa = New System.Windows.Forms.Label
        Me.txtTelefono_Casa = New DevExpress.XtraEditors.TextEdit
        Me.lblCodigo_Postal = New System.Windows.Forms.Label
        Me.clcCodigo_Postal = New Dipros.Editors.TINCalcEdit
        Me.lblSaldoActual = New System.Windows.Forms.Label
        Me.clcSaldoActual = New Dipros.Editors.TINCalcEdit
        Me.lblSaldoVencido = New System.Windows.Forms.Label
        Me.clcSaldoVencido = New Dipros.Editors.TINCalcEdit
        Me.lblIntereses = New System.Windows.Forms.Label
        Me.clcIntereses = New Dipros.Editors.TINCalcEdit
        Me.lblSaldoTotal = New System.Windows.Forms.Label
        Me.clcSaldoTotal = New Dipros.Editors.TINCalcEdit
        Me.lblImporteAbonado = New System.Windows.Forms.Label
        Me.clcImporteAbonado = New Dipros.Editors.TINCalcEdit
        Me.lblUltimoAbono = New System.Windows.Forms.Label
        Me.dteUltimoAbono = New DevExpress.XtraEditors.DateEdit
        Me.lblFecha_Hora = New System.Windows.Forms.Label
        Me.dteFecha_Hora = New DevExpress.XtraEditors.DateEdit
        Me.grMovimientos = New DevExpress.XtraGrid.GridControl
        Me.grvMovimientos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcChecar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkChecar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcFechaVencimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcInteres = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumentos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grLlamadasClientes = New DevExpress.XtraGrid.GridControl
        Me.grvLlamadasClientes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClienteLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoLlamada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTelefono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoTelefono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcContesto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcParentescoPersonaContesta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcParentesco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDes_parentesco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMensajeDejado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMensajeRecibido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcObservaciones = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUsuarioCapturo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfechapago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaLlamadasClientes = New Dipros.Windows.TINMaster
        Me.grcPromesa_Pago = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono_Casa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCodigo_Postal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldoActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldoVencido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldoTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporteAbonado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteUltimoAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Hora.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkChecar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grLlamadasClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvLlamadasClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(8371, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(56, 43)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(106, 40)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(304, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 1
        Me.lkpCliente.Tag = "cliente"
        Me.lkpCliente.ToolTip = "cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(43, 67)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(60, 16)
        Me.lblDireccion.TabIndex = 2
        Me.lblDireccion.Tag = ""
        Me.lblDireccion.Text = "&Direcci�n:"
        Me.lblDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDireccion
        '
        Me.txtDireccion.EditValue = ""
        Me.txtDireccion.Location = New System.Drawing.Point(106, 64)
        Me.txtDireccion.Name = "txtDireccion"
        '
        'txtDireccion.Properties
        '
        Me.txtDireccion.Properties.Enabled = False
        Me.txtDireccion.Properties.MaxLength = 100
        Me.txtDireccion.Size = New System.Drawing.Size(304, 20)
        Me.txtDireccion.TabIndex = 3
        Me.txtDireccion.Tag = "direccion"
        Me.txtDireccion.ToolTip = "direcci�n"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(57, 163)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 10
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "&Estado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Enabled = False
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(106, 160)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.ReadOnlyControl = False
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(254, 20)
        Me.lkpEstado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEstado.TabIndex = 11
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = "estado"
        Me.lkpEstado.ValueMember = "Estado"
        '
        'lblMunicipio
        '
        Me.lblMunicipio.AutoSize = True
        Me.lblMunicipio.Location = New System.Drawing.Point(43, 139)
        Me.lblMunicipio.Name = "lblMunicipio"
        Me.lblMunicipio.Size = New System.Drawing.Size(60, 16)
        Me.lblMunicipio.TabIndex = 8
        Me.lblMunicipio.Tag = ""
        Me.lblMunicipio.Text = "M&unicipio:"
        Me.lblMunicipio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = False
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Enabled = False
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(106, 136)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(400, Long)
        Me.lkpMunicipio.ReadOnlyControl = False
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(254, 20)
        Me.lkpMunicipio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMunicipio.TabIndex = 9
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = "municipio"
        Me.lkpMunicipio.ValueMember = "Municipio"
        '
        'lblLocalidad
        '
        Me.lblLocalidad.AutoSize = True
        Me.lblLocalidad.Location = New System.Drawing.Point(43, 115)
        Me.lblLocalidad.Name = "lblLocalidad"
        Me.lblLocalidad.Size = New System.Drawing.Size(60, 16)
        Me.lblLocalidad.TabIndex = 6
        Me.lblLocalidad.Tag = ""
        Me.lblLocalidad.Text = "Local&idad:"
        Me.lblLocalidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCiudad
        '
        Me.lkpCiudad.AllowAdd = False
        Me.lkpCiudad.AutoReaload = False
        Me.lkpCiudad.DataSource = Nothing
        Me.lkpCiudad.DefaultSearchField = ""
        Me.lkpCiudad.DisplayMember = "descripcion"
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad.Enabled = False
        Me.lkpCiudad.Filtered = False
        Me.lkpCiudad.InitValue = Nothing
        Me.lkpCiudad.Location = New System.Drawing.Point(106, 112)
        Me.lkpCiudad.MultiSelect = False
        Me.lkpCiudad.Name = "lkpCiudad"
        Me.lkpCiudad.NullText = ""
        Me.lkpCiudad.PopupWidth = CType(400, Long)
        Me.lkpCiudad.ReadOnlyControl = False
        Me.lkpCiudad.Required = False
        Me.lkpCiudad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCiudad.SearchMember = ""
        Me.lkpCiudad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCiudad.SelectAll = False
        Me.lkpCiudad.Size = New System.Drawing.Size(254, 20)
        Me.lkpCiudad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCiudad.TabIndex = 7
        Me.lkpCiudad.Tag = "ciudad"
        Me.lkpCiudad.ToolTip = "ciudad"
        Me.lkpCiudad.ValueMember = "ciudad"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(53, 91)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 4
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "C&olonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = False
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Enabled = False
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(106, 88)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(400, Long)
        Me.lkpColonia.ReadOnlyControl = False
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(254, 20)
        Me.lkpColonia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpColonia.TabIndex = 5
        Me.lkpColonia.Tag = "colonia"
        Me.lkpColonia.ToolTip = "colonia"
        Me.lkpColonia.ValueMember = "Colonia"
        '
        'lblTelefono_Casa
        '
        Me.lblTelefono_Casa.AutoSize = True
        Me.lblTelefono_Casa.Location = New System.Drawing.Point(41, 187)
        Me.lblTelefono_Casa.Name = "lblTelefono_Casa"
        Me.lblTelefono_Casa.Size = New System.Drawing.Size(62, 16)
        Me.lblTelefono_Casa.TabIndex = 12
        Me.lblTelefono_Casa.Tag = ""
        Me.lblTelefono_Casa.Text = "&Tel�fonos:"
        Me.lblTelefono_Casa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono_Casa
        '
        Me.txtTelefono_Casa.EditValue = ""
        Me.txtTelefono_Casa.Location = New System.Drawing.Point(106, 184)
        Me.txtTelefono_Casa.Name = "txtTelefono_Casa"
        '
        'txtTelefono_Casa.Properties
        '
        Me.txtTelefono_Casa.Properties.Enabled = False
        Me.txtTelefono_Casa.Properties.MaxLength = 25
        Me.txtTelefono_Casa.Size = New System.Drawing.Size(150, 20)
        Me.txtTelefono_Casa.TabIndex = 13
        Me.txtTelefono_Casa.Tag = "tel1"
        Me.txtTelefono_Casa.ToolTip = "tel�fono de casa"
        '
        'lblCodigo_Postal
        '
        Me.lblCodigo_Postal.AutoSize = True
        Me.lblCodigo_Postal.Location = New System.Drawing.Point(19, 210)
        Me.lblCodigo_Postal.Name = "lblCodigo_Postal"
        Me.lblCodigo_Postal.Size = New System.Drawing.Size(84, 16)
        Me.lblCodigo_Postal.TabIndex = 14
        Me.lblCodigo_Postal.Tag = ""
        Me.lblCodigo_Postal.Text = "C�di&go postal:"
        Me.lblCodigo_Postal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCodigo_Postal
        '
        Me.clcCodigo_Postal.EditValue = "0"
        Me.clcCodigo_Postal.Location = New System.Drawing.Point(106, 208)
        Me.clcCodigo_Postal.MaxValue = 0
        Me.clcCodigo_Postal.MinValue = 0
        Me.clcCodigo_Postal.Name = "clcCodigo_Postal"
        '
        'clcCodigo_Postal.Properties
        '
        Me.clcCodigo_Postal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigo_Postal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigo_Postal.Properties.Enabled = False
        Me.clcCodigo_Postal.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCodigo_Postal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCodigo_Postal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcCodigo_Postal.Size = New System.Drawing.Size(78, 19)
        Me.clcCodigo_Postal.TabIndex = 15
        Me.clcCodigo_Postal.Tag = "cp"
        Me.clcCodigo_Postal.ToolTip = "c�digo postal"
        '
        'lblSaldoActual
        '
        Me.lblSaldoActual.AutoSize = True
        Me.lblSaldoActual.Location = New System.Drawing.Point(466, 43)
        Me.lblSaldoActual.Name = "lblSaldoActual"
        Me.lblSaldoActual.Size = New System.Drawing.Size(78, 16)
        Me.lblSaldoActual.TabIndex = 16
        Me.lblSaldoActual.Tag = ""
        Me.lblSaldoActual.Text = "&Saldo Actual:"
        Me.lblSaldoActual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldoActual
        '
        Me.clcSaldoActual.EditValue = "0"
        Me.clcSaldoActual.Location = New System.Drawing.Point(546, 41)
        Me.clcSaldoActual.MaxValue = 0
        Me.clcSaldoActual.MinValue = 0
        Me.clcSaldoActual.Name = "clcSaldoActual"
        '
        'clcSaldoActual.Properties
        '
        Me.clcSaldoActual.Properties.DisplayFormat.FormatString = "$###,###.##"
        Me.clcSaldoActual.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoActual.Properties.EditFormat.FormatString = "$###,###.##"
        Me.clcSaldoActual.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoActual.Properties.Enabled = False
        Me.clcSaldoActual.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSaldoActual.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldoActual.Size = New System.Drawing.Size(78, 19)
        Me.clcSaldoActual.TabIndex = 17
        Me.clcSaldoActual.Tag = "saldo"
        Me.clcSaldoActual.ToolTip = "saldo actual"
        '
        'lblSaldoVencido
        '
        Me.lblSaldoVencido.AutoSize = True
        Me.lblSaldoVencido.Location = New System.Drawing.Point(457, 67)
        Me.lblSaldoVencido.Name = "lblSaldoVencido"
        Me.lblSaldoVencido.Size = New System.Drawing.Size(87, 16)
        Me.lblSaldoVencido.TabIndex = 18
        Me.lblSaldoVencido.Tag = ""
        Me.lblSaldoVencido.Text = "Saldo Vencido:"
        Me.lblSaldoVencido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldoVencido
        '
        Me.clcSaldoVencido.EditValue = "0"
        Me.clcSaldoVencido.Location = New System.Drawing.Point(546, 65)
        Me.clcSaldoVencido.MaxValue = 0
        Me.clcSaldoVencido.MinValue = 0
        Me.clcSaldoVencido.Name = "clcSaldoVencido"
        '
        'clcSaldoVencido.Properties
        '
        Me.clcSaldoVencido.Properties.DisplayFormat.FormatString = "$###,###.##"
        Me.clcSaldoVencido.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoVencido.Properties.EditFormat.FormatString = "$###,###.##"
        Me.clcSaldoVencido.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoVencido.Properties.Enabled = False
        Me.clcSaldoVencido.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSaldoVencido.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldoVencido.Size = New System.Drawing.Size(78, 19)
        Me.clcSaldoVencido.TabIndex = 19
        Me.clcSaldoVencido.Tag = "saldo_vencido"
        Me.clcSaldoVencido.ToolTip = "saldo vencido"
        '
        'lblIntereses
        '
        Me.lblIntereses.AutoSize = True
        Me.lblIntereses.Location = New System.Drawing.Point(482, 91)
        Me.lblIntereses.Name = "lblIntereses"
        Me.lblIntereses.Size = New System.Drawing.Size(61, 16)
        Me.lblIntereses.TabIndex = 20
        Me.lblIntereses.Tag = ""
        Me.lblIntereses.Text = "I&ntereses:"
        Me.lblIntereses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIntereses
        '
        Me.clcIntereses.EditValue = "0"
        Me.clcIntereses.Location = New System.Drawing.Point(546, 89)
        Me.clcIntereses.MaxValue = 0
        Me.clcIntereses.MinValue = 0
        Me.clcIntereses.Name = "clcIntereses"
        '
        'clcIntereses.Properties
        '
        Me.clcIntereses.Properties.DisplayFormat.FormatString = "$###,###.##"
        Me.clcIntereses.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.EditFormat.FormatString = "$##,##0.##"
        Me.clcIntereses.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.Enabled = False
        Me.clcIntereses.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcIntereses.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIntereses.Size = New System.Drawing.Size(78, 19)
        Me.clcIntereses.TabIndex = 21
        Me.clcIntereses.Tag = "intereses"
        Me.clcIntereses.ToolTip = "intereses"
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.AutoSize = True
        Me.lblSaldoTotal.Location = New System.Drawing.Point(472, 115)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(71, 16)
        Me.lblSaldoTotal.TabIndex = 22
        Me.lblSaldoTotal.Tag = ""
        Me.lblSaldoTotal.Text = "Saldo Total:"
        Me.lblSaldoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldoTotal
        '
        Me.clcSaldoTotal.EditValue = "0"
        Me.clcSaldoTotal.Location = New System.Drawing.Point(546, 113)
        Me.clcSaldoTotal.MaxValue = 0
        Me.clcSaldoTotal.MinValue = 0
        Me.clcSaldoTotal.Name = "clcSaldoTotal"
        '
        'clcSaldoTotal.Properties
        '
        Me.clcSaldoTotal.Properties.DisplayFormat.FormatString = "$###,###.##"
        Me.clcSaldoTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoTotal.Properties.EditFormat.FormatString = "$###,##0.##"
        Me.clcSaldoTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldoTotal.Properties.Enabled = False
        Me.clcSaldoTotal.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSaldoTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldoTotal.Size = New System.Drawing.Size(78, 19)
        Me.clcSaldoTotal.TabIndex = 23
        Me.clcSaldoTotal.Tag = "saldo_total"
        Me.clcSaldoTotal.ToolTip = "saldo total"
        '
        'lblImporteAbonado
        '
        Me.lblImporteAbonado.AutoSize = True
        Me.lblImporteAbonado.Location = New System.Drawing.Point(438, 163)
        Me.lblImporteAbonado.Name = "lblImporteAbonado"
        Me.lblImporteAbonado.Size = New System.Drawing.Size(105, 16)
        Me.lblImporteAbonado.TabIndex = 26
        Me.lblImporteAbonado.Tag = ""
        Me.lblImporteAbonado.Text = "Importe Abonado:"
        Me.lblImporteAbonado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporteAbonado
        '
        Me.clcImporteAbonado.EditValue = "0"
        Me.clcImporteAbonado.Location = New System.Drawing.Point(546, 161)
        Me.clcImporteAbonado.MaxValue = 0
        Me.clcImporteAbonado.MinValue = 0
        Me.clcImporteAbonado.Name = "clcImporteAbonado"
        '
        'clcImporteAbonado.Properties
        '
        Me.clcImporteAbonado.Properties.DisplayFormat.FormatString = "$###,###.##"
        Me.clcImporteAbonado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteAbonado.Properties.EditFormat.FormatString = "$###,##0.##"
        Me.clcImporteAbonado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteAbonado.Properties.Enabled = False
        Me.clcImporteAbonado.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcImporteAbonado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporteAbonado.Size = New System.Drawing.Size(78, 19)
        Me.clcImporteAbonado.TabIndex = 27
        Me.clcImporteAbonado.Tag = "importe_abonado"
        Me.clcImporteAbonado.ToolTip = "importe abonado"
        '
        'lblUltimoAbono
        '
        Me.lblUltimoAbono.AutoSize = True
        Me.lblUltimoAbono.Location = New System.Drawing.Point(460, 139)
        Me.lblUltimoAbono.Name = "lblUltimoAbono"
        Me.lblUltimoAbono.Size = New System.Drawing.Size(83, 16)
        Me.lblUltimoAbono.TabIndex = 24
        Me.lblUltimoAbono.Tag = ""
        Me.lblUltimoAbono.Text = "�lti&mo Abono:"
        Me.lblUltimoAbono.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteUltimoAbono
        '
        Me.dteUltimoAbono.EditValue = ""
        Me.dteUltimoAbono.Location = New System.Drawing.Point(546, 136)
        Me.dteUltimoAbono.Name = "dteUltimoAbono"
        '
        'dteUltimoAbono.Properties
        '
        Me.dteUltimoAbono.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteUltimoAbono.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteUltimoAbono.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteUltimoAbono.Properties.Enabled = False
        Me.dteUltimoAbono.Size = New System.Drawing.Size(95, 23)
        Me.dteUltimoAbono.TabIndex = 25
        Me.dteUltimoAbono.Tag = "ultimo_abono"
        Me.dteUltimoAbono.ToolTip = "�ltimo abono"
        '
        'lblFecha_Hora
        '
        Me.lblFecha_Hora.AutoSize = True
        Me.lblFecha_Hora.Location = New System.Drawing.Point(473, 187)
        Me.lblFecha_Hora.Name = "lblFecha_Hora"
        Me.lblFecha_Hora.Size = New System.Drawing.Size(71, 16)
        Me.lblFecha_Hora.TabIndex = 28
        Me.lblFecha_Hora.Tag = ""
        Me.lblFecha_Hora.Text = "&Fecha/hora:"
        Me.lblFecha_Hora.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Hora
        '
        Me.dteFecha_Hora.EditValue = ""
        Me.dteFecha_Hora.Location = New System.Drawing.Point(546, 184)
        Me.dteFecha_Hora.Name = "dteFecha_Hora"
        '
        'dteFecha_Hora.Properties
        '
        Me.dteFecha_Hora.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Hora.Properties.DisplayFormat.FormatString = "dd/MMM/yyy/ hh:mm:ss"
        Me.dteFecha_Hora.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Hora.Properties.Enabled = False
        Me.dteFecha_Hora.Size = New System.Drawing.Size(150, 23)
        Me.dteFecha_Hora.TabIndex = 29
        Me.dteFecha_Hora.Tag = "fecha_hora"
        Me.dteFecha_Hora.ToolTip = "fecha"
        '
        'grMovimientos
        '
        Me.grMovimientos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        'grMovimientos.EmbeddedNavigator
        '
        Me.grMovimientos.EmbeddedNavigator.Name = ""
        Me.grMovimientos.Location = New System.Drawing.Point(9, 233)
        Me.grMovimientos.MainView = Me.grvMovimientos
        Me.grMovimientos.Name = "grMovimientos"
        Me.grMovimientos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkChecar})
        Me.grMovimientos.Size = New System.Drawing.Size(727, 151)
        Me.grMovimientos.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientos.TabIndex = 59
        Me.grMovimientos.TabStop = False
        Me.grMovimientos.Text = "GridControl1"
        '
        'grvMovimientos
        '
        Me.grvMovimientos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcChecar, Me.grcFechaVencimiento, Me.grcDocumento, Me.grcSaldoDocumento, Me.grcSaldoVenta, Me.grcSucursal, Me.grcConcepto, Me.grcSerie, Me.grcFolio, Me.grcCliente, Me.grcInteres, Me.grcDocumentos})
        Me.grvMovimientos.GridControl = Me.grMovimientos
        Me.grvMovimientos.GroupPanelText = "Precios"
        Me.grvMovimientos.Name = "grvMovimientos"
        Me.grvMovimientos.OptionsView.ShowGroupPanel = False
        Me.grvMovimientos.OptionsView.ShowIndicator = False
        '
        'grcChecar
        '
        Me.grcChecar.Caption = "Incluir"
        Me.grcChecar.ColumnEdit = Me.chkChecar
        Me.grcChecar.FieldName = "checar"
        Me.grcChecar.Name = "grcChecar"
        Me.grcChecar.Options = CType((DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcChecar.Width = 76
        '
        'chkChecar
        '
        Me.chkChecar.AutoHeight = False
        Me.chkChecar.Name = "chkChecar"
        '
        'grcFechaVencimiento
        '
        Me.grcFechaVencimiento.Caption = "Fecha"
        Me.grcFechaVencimiento.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaVencimiento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaVencimiento.FieldName = "fecha_vencimiento"
        Me.grcFechaVencimiento.Name = "grcFechaVencimiento"
        Me.grcFechaVencimiento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaVencimiento.VisibleIndex = 2
        Me.grcFechaVencimiento.Width = 116
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Documento"
        Me.grcDocumento.DisplayFormat.FormatString = "n"
        Me.grcDocumento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDocumento.FieldName = "documento"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.Width = 65
        '
        'grcSaldoDocumento
        '
        Me.grcSaldoDocumento.Caption = "Saldo Documento"
        Me.grcSaldoDocumento.DisplayFormat.FormatString = "c2"
        Me.grcSaldoDocumento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoDocumento.FieldName = "saldo_documento"
        Me.grcSaldoDocumento.Name = "grcSaldoDocumento"
        Me.grcSaldoDocumento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoDocumento.VisibleIndex = 4
        Me.grcSaldoDocumento.Width = 147
        '
        'grcSaldoVenta
        '
        Me.grcSaldoVenta.Caption = "Saldo Venta"
        Me.grcSaldoVenta.DisplayFormat.FormatString = "c2"
        Me.grcSaldoVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoVenta.FieldName = "saldo_venta"
        Me.grcSaldoVenta.Name = "grcSaldoVenta"
        Me.grcSaldoVenta.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoVenta.VisibleIndex = 5
        Me.grcSaldoVenta.Width = 111
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto Factura"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.Width = 107
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie Factura"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.VisibleIndex = 0
        Me.grcSerie.Width = 80
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio Factura"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 1
        Me.grcFolio.Width = 80
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcInteres
        '
        Me.grcInteres.Caption = "Inter�s"
        Me.grcInteres.DisplayFormat.FormatString = "c2"
        Me.grcInteres.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcInteres.FieldName = "interes"
        Me.grcInteres.Name = "grcInteres"
        Me.grcInteres.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcInteres.VisibleIndex = 6
        Me.grcInteres.Width = 108
        '
        'grcDocumentos
        '
        Me.grcDocumentos.Caption = "Documento"
        Me.grcDocumentos.FieldName = "documentos"
        Me.grcDocumentos.Name = "grcDocumentos"
        Me.grcDocumentos.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumentos.VisibleIndex = 3
        Me.grcDocumentos.Width = 106
        '
        'grLlamadasClientes
        '
        '
        'grLlamadasClientes.EmbeddedNavigator
        '
        Me.grLlamadasClientes.EmbeddedNavigator.Name = ""
        Me.grLlamadasClientes.Location = New System.Drawing.Point(9, 424)
        Me.grLlamadasClientes.MainView = Me.grvLlamadasClientes
        Me.grLlamadasClientes.Name = "grLlamadasClientes"
        Me.grLlamadasClientes.Size = New System.Drawing.Size(727, 160)
        Me.grLlamadasClientes.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grLlamadasClientes.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grLlamadasClientes.TabIndex = 61
        Me.grLlamadasClientes.Text = "LlamadasClientes"
        '
        'grvLlamadasClientes
        '
        Me.grvLlamadasClientes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClienteLlamada, Me.grcFechaLlamada, Me.grcTipoLlamada, Me.grcTelefono, Me.grcTipoTelefono, Me.grcContesto, Me.grcParentescoPersonaContesta, Me.grcParentesco, Me.grcDes_parentesco, Me.grcPromesa_Pago, Me.grcMensajeDejado, Me.grcMensajeRecibido, Me.grcObservaciones, Me.grcUsuarioCapturo, Me.grcfechapago})
        Me.grvLlamadasClientes.GridControl = Me.grLlamadasClientes
        Me.grvLlamadasClientes.Name = "grvLlamadasClientes"
        Me.grvLlamadasClientes.OptionsBehavior.Editable = False
        Me.grvLlamadasClientes.OptionsCustomization.AllowFilter = False
        Me.grvLlamadasClientes.OptionsCustomization.AllowGroup = False
        Me.grvLlamadasClientes.OptionsView.ShowGroupPanel = False
        '
        'grcClienteLlamada
        '
        Me.grcClienteLlamada.Caption = "Cliente"
        Me.grcClienteLlamada.FieldName = "cliente"
        Me.grcClienteLlamada.Name = "grcClienteLlamada"
        Me.grcClienteLlamada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFechaLlamada
        '
        Me.grcFechaLlamada.Caption = "Fecha"
        Me.grcFechaLlamada.DisplayFormat.FormatString = "d"
        Me.grcFechaLlamada.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaLlamada.FieldName = "fecha_hora"
        Me.grcFechaLlamada.Name = "grcFechaLlamada"
        Me.grcFechaLlamada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaLlamada.VisibleIndex = 0
        Me.grcFechaLlamada.Width = 74
        '
        'grcTipoLlamada
        '
        Me.grcTipoLlamada.Caption = "Tipo Llamada"
        Me.grcTipoLlamada.FieldName = "descripcion_tipo_llamada"
        Me.grcTipoLlamada.Name = "grcTipoLlamada"
        Me.grcTipoLlamada.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipoLlamada.VisibleIndex = 1
        Me.grcTipoLlamada.Width = 84
        '
        'grcTelefono
        '
        Me.grcTelefono.Caption = "Tel�fono"
        Me.grcTelefono.FieldName = "telefono"
        Me.grcTelefono.Name = "grcTelefono"
        Me.grcTelefono.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTelefono.VisibleIndex = 3
        Me.grcTelefono.Width = 79
        '
        'grcTipoTelefono
        '
        Me.grcTipoTelefono.Caption = "Tipo de Tel�fono"
        Me.grcTipoTelefono.FieldName = "descripcion_tipo_telefono"
        Me.grcTipoTelefono.Name = "grcTipoTelefono"
        Me.grcTipoTelefono.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipoTelefono.VisibleIndex = 2
        Me.grcTipoTelefono.Width = 79
        '
        'grcContesto
        '
        Me.grcContesto.Caption = "Contest�"
        Me.grcContesto.FieldName = "persona_contesta"
        Me.grcContesto.Name = "grcContesto"
        Me.grcContesto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcContesto.VisibleIndex = 4
        Me.grcContesto.Width = 79
        '
        'grcParentescoPersonaContesta
        '
        Me.grcParentescoPersonaContesta.Caption = "Parentesco Persona Contesta"
        Me.grcParentescoPersonaContesta.FieldName = "parentesco_persona_contesta"
        Me.grcParentescoPersonaContesta.Name = "grcParentescoPersonaContesta"
        Me.grcParentescoPersonaContesta.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcParentescoPersonaContesta.Width = 79
        '
        'grcParentesco
        '
        Me.grcParentesco.Caption = "id_parentesco"
        Me.grcParentesco.FieldName = "parentesco"
        Me.grcParentesco.Name = "grcParentesco"
        Me.grcParentesco.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcDes_parentesco
        '
        Me.grcDes_parentesco.Caption = "Parentesco"
        Me.grcDes_parentesco.FieldName = "des_parentesco"
        Me.grcDes_parentesco.Name = "grcDes_parentesco"
        Me.grcDes_parentesco.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDes_parentesco.VisibleIndex = 6
        '
        'grcMensajeDejado
        '
        Me.grcMensajeDejado.Caption = "Mensaje Dejado"
        Me.grcMensajeDejado.FieldName = "mensaje_dejado"
        Me.grcMensajeDejado.Name = "grcMensajeDejado"
        Me.grcMensajeDejado.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMensajeDejado.Width = 79
        '
        'grcMensajeRecibido
        '
        Me.grcMensajeRecibido.Caption = "Mensaje Recibido"
        Me.grcMensajeRecibido.FieldName = "mensaje_recibido"
        Me.grcMensajeRecibido.Name = "grcMensajeRecibido"
        Me.grcMensajeRecibido.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMensajeRecibido.VisibleIndex = 5
        Me.grcMensajeRecibido.Width = 79
        '
        'grcObservaciones
        '
        Me.grcObservaciones.Caption = "Observaciones"
        Me.grcObservaciones.FieldName = "observaciones"
        Me.grcObservaciones.Name = "grcObservaciones"
        Me.grcObservaciones.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcObservaciones.VisibleIndex = 7
        Me.grcObservaciones.Width = 103
        '
        'grcUsuarioCapturo
        '
        Me.grcUsuarioCapturo.Caption = "Captur�"
        Me.grcUsuarioCapturo.FieldName = "usuario_capturo"
        Me.grcUsuarioCapturo.Name = "grcUsuarioCapturo"
        Me.grcUsuarioCapturo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcUsuarioCapturo.VisibleIndex = 8
        '
        'grcfechapago
        '
        Me.grcfechapago.Caption = "Fecha de Promesa de Pago"
        Me.grcfechapago.FieldName = "promesa_pago"
        Me.grcfechapago.Name = "grcfechapago"
        Me.grcfechapago.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcfechapago.VisibleIndex = 10
        '
        'tmaLlamadasClientes
        '
        Me.tmaLlamadasClientes.BackColor = System.Drawing.Color.White
        Me.tmaLlamadasClientes.CanDelete = True
        Me.tmaLlamadasClientes.CanInsert = True
        Me.tmaLlamadasClientes.CanUpdate = True
        Me.tmaLlamadasClientes.Enabled = False
        Me.tmaLlamadasClientes.Grid = Me.grLlamadasClientes
        Me.tmaLlamadasClientes.Location = New System.Drawing.Point(9, 400)
        Me.tmaLlamadasClientes.Name = "tmaLlamadasClientes"
        Me.tmaLlamadasClientes.Size = New System.Drawing.Size(719, 23)
        Me.tmaLlamadasClientes.TabIndex = 60
        Me.tmaLlamadasClientes.Title = "Llamadas a Clientes"
        Me.tmaLlamadasClientes.UpdateTitle = "un Registro"
        '
        'grcPromesa_Pago
        '
        Me.grcPromesa_Pago.Caption = "Fecha de Promesa de Pago"
        Me.grcPromesa_Pago.FieldName = "fecha_promesa_pago"
        Me.grcPromesa_Pago.Name = "grcPromesa_Pago"
        Me.grcPromesa_Pago.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPromesa_Pago.VisibleIndex = 9
        '
        'frmLlamadasClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(746, 592)
        Me.Controls.Add(Me.grMovimientos)
        Me.Controls.Add(Me.grLlamadasClientes)
        Me.Controls.Add(Me.tmaLlamadasClientes)
        Me.Controls.Add(Me.lblFecha_Hora)
        Me.Controls.Add(Me.dteFecha_Hora)
        Me.Controls.Add(Me.lblUltimoAbono)
        Me.Controls.Add(Me.dteUltimoAbono)
        Me.Controls.Add(Me.lblImporteAbonado)
        Me.Controls.Add(Me.clcImporteAbonado)
        Me.Controls.Add(Me.lblSaldoTotal)
        Me.Controls.Add(Me.clcSaldoTotal)
        Me.Controls.Add(Me.lblIntereses)
        Me.Controls.Add(Me.clcIntereses)
        Me.Controls.Add(Me.lblSaldoVencido)
        Me.Controls.Add(Me.clcSaldoVencido)
        Me.Controls.Add(Me.lblSaldoActual)
        Me.Controls.Add(Me.clcSaldoActual)
        Me.Controls.Add(Me.lblCodigo_Postal)
        Me.Controls.Add(Me.clcCodigo_Postal)
        Me.Controls.Add(Me.lblTelefono_Casa)
        Me.Controls.Add(Me.txtTelefono_Casa)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lblMunicipio)
        Me.Controls.Add(Me.lkpMunicipio)
        Me.Controls.Add(Me.lblLocalidad)
        Me.Controls.Add(Me.lkpCiudad)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.lkpColonia)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.MasterControl = Me.tmaLlamadasClientes
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmLlamadasClientes"
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpColonia, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.lkpCiudad, 0)
        Me.Controls.SetChildIndex(Me.lblLocalidad, 0)
        Me.Controls.SetChildIndex(Me.lkpMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lblMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.txtDireccion, 0)
        Me.Controls.SetChildIndex(Me.lblDireccion, 0)
        Me.Controls.SetChildIndex(Me.txtTelefono_Casa, 0)
        Me.Controls.SetChildIndex(Me.lblTelefono_Casa, 0)
        Me.Controls.SetChildIndex(Me.clcCodigo_Postal, 0)
        Me.Controls.SetChildIndex(Me.lblCodigo_Postal, 0)
        Me.Controls.SetChildIndex(Me.clcSaldoActual, 0)
        Me.Controls.SetChildIndex(Me.lblSaldoActual, 0)
        Me.Controls.SetChildIndex(Me.clcSaldoVencido, 0)
        Me.Controls.SetChildIndex(Me.lblSaldoVencido, 0)
        Me.Controls.SetChildIndex(Me.clcIntereses, 0)
        Me.Controls.SetChildIndex(Me.lblIntereses, 0)
        Me.Controls.SetChildIndex(Me.clcSaldoTotal, 0)
        Me.Controls.SetChildIndex(Me.lblSaldoTotal, 0)
        Me.Controls.SetChildIndex(Me.clcImporteAbonado, 0)
        Me.Controls.SetChildIndex(Me.lblImporteAbonado, 0)
        Me.Controls.SetChildIndex(Me.dteUltimoAbono, 0)
        Me.Controls.SetChildIndex(Me.lblUltimoAbono, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Hora, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Hora, 0)
        Me.Controls.SetChildIndex(Me.tmaLlamadasClientes, 0)
        Me.Controls.SetChildIndex(Me.grLlamadasClientes, 0)
        Me.Controls.SetChildIndex(Me.grMovimientos, 0)
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono_Casa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCodigo_Postal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldoActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldoVencido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldoTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporteAbonado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteUltimoAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Hora.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkChecar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grLlamadasClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvLlamadasClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oLlamadasClientes As VillarrealBusiness.clsLlamadasClientes
    Private oLlamadasClientesDetalle As VillarrealBusiness.clsLlamadasClientesDetalle
    Private oClientes As VillarrealBusiness.clsClientes
    Private ociudades As VillarrealBusiness.clsCiudades
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oVariables As VillarrealBusiness.clsVariables
    Private oColonias As VillarrealBusiness.clsColonias
    Private oEstados As VillarrealBusiness.clsEstados
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar

    Private oTabla As DataTable
    Private bentro_catalogo_clientes As Boolean = False
    Dim BanderaNoCargarDetalle As Boolean = False
    'Private oComunes As New VillarrealBusiness.clsUtilerias 

    Public Property cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
        Set(ByVal Value As Long)
            Me.lkpCliente.EditValue = Value
        End Set
    End Property
    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property
    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property
    Private ReadOnly Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCiudad)
        End Get
    End Property
    Private ReadOnly Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
    End Property
    Public WriteOnly Property entro_catalogo_clientes() As Boolean
        Set(ByVal Value As Boolean)
            bentro_catalogo_clientes = Value
            Me.lkpCliente.Enabled = Not Value
            If Value = True Then
                oClientes = New VillarrealBusiness.clsClientes
                Me.lkpCliente_LoadData(True)
            End If
        End Set
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmLlamadasClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub

    Private Sub frmLlamadasClientes_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With Me.tmaLlamadasClientes

            'If (Me.Action = Actions.Update) Then
            '    .MoveFirst()
            '    Do While Not .EOF
            '        'Response = oLamadasClientesDetalle.Actualizar  '.CambiarBodega(intBodega_Anterior, Me.lkpBodega.EditValue, .Item("articulo"), .Item("cantidad_anterior"), Me.clcOrden.Text, Me.chkSobrePedido.Checked)
            '        .MoveNext()
            '    Loop
            'End If

            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert 'And .Item("cantidad") > 0
                        ValidaCliente()
                        If BanderaNoCargarDetalle = True Then Exit Sub
                        Response = oLlamadasClientes.Insertar(.SelectedRow) ', clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)
                    Case Actions.Update
                        Response = oLlamadasClientes.Actualizar(.SelectedRow)
                    Case Actions.Delete
                        Response = oLlamadasClientes.Eliminar(.SelectedRow.Tables(0).Rows(0).Item("cliente"), .SelectedRow.Tables(0).Rows(0).Item("fecha_hora"))
                End Select
                .MoveNext()
            Loop
        End With

    End Sub

    Private Sub frmLlamadasClientes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        If bentro_catalogo_clientes = False Then
            Response = oLlamadasClientes.DespliegaDatos(OwnerForm.Value("cliente"))

            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.DataSource = oDataSet
            End If
        Else
            If cliente >= 1 Then
                Response = oLlamadasClientes.DespliegaDatos(cliente)

                If Not Response.ErrorFound Then
                    Dim oDataSet As DataSet
                    oDataSet = Response.Value
                    Me.DataSource = oDataSet
                End If
            End If

        End If


        

    End Sub

    Private Sub frmLlamadasClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oLlamadasClientesDetalle = New VillarrealBusiness.clsLlamadasClientesDetalle
        oLlamadasClientes = New VillarrealBusiness.clsLlamadasClientes
        oClientes = New VillarrealBusiness.clsClientes
        ociudades = New VillarrealBusiness.clsCiudades
        oMunicipios = New VillarrealBusiness.clsMunicipios
        oVariables = New VillarrealBusiness.clsVariables
        oColonias = New VillarrealBusiness.clsColonias
        oEstados = New VillarrealBusiness.clsEstados
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar


        With Me.tmaLlamadasClientes

            .UpdateTitle = "una llamada"
            .UpdateForm = New frmLlamadasClientesDetalle
            .AddColumn("cliente", "System.Int32")
            .AddColumn("fecha_hora", "System.DateTime")
            .AddColumn("tipo_llamada", "System.Int32")
            .AddColumn("descripcion_tipo_llamada")
            .AddColumn("telefono", "System.String")
            .AddColumn("tipo_telefono", "System.String")
            .AddColumn("descripcion_tipo_telefono", "System.String")
            .AddColumn("persona_contesta", "System.String")
            .AddColumn("parentesco_persona_contesta", "System.String")
            .AddColumn("parentesco", "System.Int32")
            .AddColumn("des_parentesco", "System.String")
            .AddColumn("fecha_promesa_pago", "System.DateTime")
            .AddColumn("mensaje_dejado", "System.String")
            .AddColumn("mensaje_recibido", "System.String")
            .AddColumn("observaciones", "System.String")
            .AddColumn("usuario_capturo", "System.String")

        End With
    End Sub

    Private Sub frmLlamadasClientes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oLlamadasClientes.Validacion(Action, cliente)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        
        '  Me.cboTipoCobro.SelectedIndex = 2  'Contado
        If cliente <> -1 Then   'And Action = Actions.Insert
            Dim Response As New Events
            Dim oDataTable As DataTable
            Response = oClientes.LookupLlenado(sucursal_dependencia, cliente)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                oDataTable = oDataSet.Tables(0)
                oDataSet = Nothing
            End If
            LlenarDatosCliente(oDataTable.Rows(0))
            Llenargrid()
            LlenarGridLlamadas()
            Me.tmaLlamadasClientes.Enabled = True

            Response = Nothing
            oDataTable = Nothing
        Else
            LimpiarDatosCliente()
            Me.tmaLlamadasClientes.Enabled = False
        End If
    End Sub

    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        'Me.lkpMunicipio.EditValue = Nothing
        'Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData
        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        ''If Me.lkpMunicipio.DataSource Is Nothing Or Me.lkpMunicipio.EditValue Is Nothing Then Exit Sub
        ''If Entro_Despliega = True Then Exit Sub
        'Me.lkpCiudad.EditValue = Nothing
        'Me.lkpCiudad_LoadData(True)

    End Sub

    Private Sub lkpCiudad_Format() Handles lkpCiudad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpCiudad)
    End Sub
    Private Sub lkpCiudad_LoadData(ByVal Initialize As Boolean) Handles lkpCiudad.LoadData
        Dim Response As New Events
        Response = ociudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCiudad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCiudad_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpCiudad.EditValueChanged
        'If Me.lkpCiudad.EditValue Is Nothing Then Exit Sub

        'Me.lkpColonia.EditValue = Nothing
        'Me.lkpColonia_LoadData(True)
    End Sub

    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub
    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub LlenarDatosCliente(ByVal odatarow As DataRow)
        Dim response As New Events
        Dim tipo_cobro As String
        Dim oData As DataSet
        Dim dFecha As DateTime


        dFecha = TinApp.FechaServidor

        Me.lkpEstado.EditValue = odatarow("estado")
        Me.lkpMunicipio.EditValue = odatarow("municipio")
        Me.lkpCiudad.EditValue = odatarow("ciudad")
        Me.lkpColonia.EditValue = odatarow("colonia")
        Me.txtTelefono_Casa.EditValue = odatarow("telefono1")
        Me.clcCodigo_Postal.EditValue = odatarow("cp")
        Me.txtDireccion.EditValue = odatarow("direccion")


        'Me.lkpEstado.EditValue = Me.lkpCliente.GetValue("estado")
        'Me.lkpMunicipio.EditValue = Me.lkpCliente.GetValue("municipio")
        'Me.lkpCiudad.EditValue = Me.lkpCliente.GetValue("ciudad")
        'Me.lkpColonia.EditValue = Me.lkpCliente.GetValue("colonia")
        'Me.txtTelefono_Casa.EditValue = Me.lkpCliente.GetValue("telefono1")
        'Me.clcCodigo_Postal.EditValue = Me.lkpCliente.GetValue("cp")
        'Me.txtDireccion.EditValue = Me.lkpCliente.GetValue("direccion")
        Me.dteFecha_Hora.EditValue = TinApp.FechaServidor

        response = Comunes.clsUtilerias.obtenerSaldosVencidosInteresesCliente(cliente, dFecha)
        If Not response.ErrorFound Then
            oData = response.Value
            Try
                With oData.Tables(0).Rows(0)
                    Me.clcSaldoActual.EditValue = .Item("saldo_actual")
                    Me.clcSaldoVencido.EditValue = .Item("saldo_vencido")
                    Me.clcIntereses.EditValue = .Item("intereses")
                    Me.clcSaldoTotal.EditValue = .Item("saldo_total")
                    Me.dteUltimoAbono.EditValue = .Item("fecha_ultimo_abono")
                    Me.clcImporteAbonado.EditValue = .Item("importe_ultimo_abono")
                    'Me.dteFecha_Hora.EditValue = .Item("fecha")

                End With
                'Dim oEvent As New Dipros.Utils.Events
            Catch ex As Exception
                'oEvent.Ex = ex
                'oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
                'oEvent.Layer = Events.ErrorLayer.DataLayer
            End Try


        Else
            LimpiarDatosCliente()
        End If
    End Sub
    Private Sub LimpiarDatosCliente()

        Me.lkpColonia.EditValue = -1
        Me.lkpCiudad.EditValue = -1
        Me.lkpMunicipio.EditValue = -1
        Me.lkpEstado.EditValue = -1

    End Sub

    Private Sub Llenargrid()
        Dim response As New Dipros.Utils.Events
        Dim odataset As New DataSet
        Dim Fecha As DateTime
        Fecha = TinApp.FechaServidor

        response = Comunes.clsUtilerias.obtenerSaldosVencidosInteresesCliente(cliente, Fecha)
        If Not response.ErrorFound Then
            odataset = response.Value
            oTabla = odataset.Tables(1)
            Me.grMovimientos.DataSource = oTabla


        End If

        odataset = Nothing

    End Sub
    Private Sub LlenarGridLlamadas()
        Dim response As New Dipros.Utils.Events
        Dim odataset As New DataSet

        response = oLlamadasClientes.DespliegaDatos(cliente)
        If Not response.ErrorFound Then
            odataset = response.Value

            Me.tmaLlamadasClientes.DataSource = odataset


        End If

        odataset = Nothing

    End Sub
    Private Sub ValidaCliente()
        If cliente = Nothing Or cliente = -1 Or IsNumeric(cliente) = False Then
            BanderaNoCargarDetalle = True
        End If
    End Sub


#End Region



End Class
