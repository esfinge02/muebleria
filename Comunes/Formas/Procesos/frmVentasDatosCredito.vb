Imports Comunes.clsUtilerias
Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmVentasDatosCredito
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblMenos As System.Windows.Forms.Label
    Friend WithEvents clcMenos As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcEnganche1 As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPlan As System.Windows.Forms.Label
    Friend WithEvents lkpPlan As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha_Primer_Documento As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Primer_Documento As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblNumero_Documentos As System.Windows.Forms.Label
    Friend WithEvents clcNumero_Documentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporte_Documentos As System.Windows.Forms.Label
    Friend WithEvents clcImporte_Documentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporte_Ultimo_Documento As System.Windows.Forms.Label
    Friend WithEvents clcImporte_Ultimo_Documento As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkEnganche_En_Documento As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblImporte_Enganche_Documento As System.Windows.Forms.Label
    Friend WithEvents clcImporte_Enganche_Documento As Dipros.Editors.TINCalcEdit
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblFecha_Enganche_Documento As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Enganche_Documento As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents clcTotal As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkIvadesglosado1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblMonto_Liquida_Vencimiento As System.Windows.Forms.Label
    Friend WithEvents clcMonto_Liquida_Vencimiento As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkLiquida_Vencimiento As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblInteres As System.Windows.Forms.Label
    Friend WithEvents lblEnganche1 As System.Windows.Forms.Label
    Friend WithEvents clcMenos_Enganch As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkEnganche_en_Menos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcIntereses As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label

    Friend WithEvents lblImporteUltimoDocumento As System.Windows.Forms.Label
    Friend WithEvents lblDocumentosIguales As System.Windows.Forms.Label
    Friend WithEvents lblLeyendaDocumentos As System.Windows.Forms.Label
    Friend WithEvents lblDocumentoFinal As System.Windows.Forms.Label
    Friend WithEvents lblLeyendaDocumentoFinal As System.Windows.Forms.Label
    Friend WithEvents lblDocumentosUltimoDocumento As System.Windows.Forms.Label
    Friend WithEvents lblTotalDocumentos As System.Windows.Forms.Label
    Friend WithEvents clcPagoInicial As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clcImportePlan As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents clcPagoInicialDisplay As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcImporteConDescuento As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkaplica_enganche As DevExpress.XtraEditors.CheckEdit
    Public WithEvents lkpConveniosClientes As Dipros.Editors.TINMultiLookup
    Public WithEvents lblConvenio As System.Windows.Forms.Label
    Public WithEvents chkFacturacionEspecial As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcAnticipos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblAnticipos As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVentasDatosCredito))
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblMenos = New System.Windows.Forms.Label
        Me.clcMenos = New Dipros.Editors.TINCalcEdit
        Me.lblInteres = New System.Windows.Forms.Label
        Me.clcEnganche1 = New Dipros.Editors.TINCalcEdit
        Me.clcIntereses = New Dipros.Editors.TINCalcEdit
        Me.lblPlan = New System.Windows.Forms.Label
        Me.lkpPlan = New Dipros.Editors.TINMultiLookup
        Me.lblFecha_Primer_Documento = New System.Windows.Forms.Label
        Me.dteFecha_Primer_Documento = New DevExpress.XtraEditors.DateEdit
        Me.lblNumero_Documentos = New System.Windows.Forms.Label
        Me.clcNumero_Documentos = New Dipros.Editors.TINCalcEdit
        Me.lblImporte_Documentos = New System.Windows.Forms.Label
        Me.clcImporte_Documentos = New Dipros.Editors.TINCalcEdit
        Me.lblImporte_Ultimo_Documento = New System.Windows.Forms.Label
        Me.clcImporte_Ultimo_Documento = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkEnganche_En_Documento = New DevExpress.XtraEditors.CheckEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblFecha_Enganche_Documento = New System.Windows.Forms.Label
        Me.dteFecha_Enganche_Documento = New DevExpress.XtraEditors.DateEdit
        Me.lblImporte_Enganche_Documento = New System.Windows.Forms.Label
        Me.clcImporte_Enganche_Documento = New Dipros.Editors.TINCalcEdit
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.clcMonto_Liquida_Vencimiento = New Dipros.Editors.TINCalcEdit
        Me.lblMonto_Liquida_Vencimiento = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.clcTotal = New Dipros.Editors.TINCalcEdit
        Me.chkIvadesglosado1 = New DevExpress.XtraEditors.CheckEdit
        Me.chkLiquida_Vencimiento = New DevExpress.XtraEditors.CheckEdit
        Me.clcMenos_Enganch = New Dipros.Editors.TINCalcEdit
        Me.lblEnganche1 = New System.Windows.Forms.Label
        Me.chkEnganche_en_Menos = New DevExpress.XtraEditors.CheckEdit
        Me.lblLeyendaDocumentos = New System.Windows.Forms.Label
        Me.lblLeyendaDocumentoFinal = New System.Windows.Forms.Label
        Me.lblDocumentoFinal = New System.Windows.Forms.Label
        Me.lblDocumentosIguales = New System.Windows.Forms.Label
        Me.lblImporteUltimoDocumento = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblDocumentosUltimoDocumento = New System.Windows.Forms.Label
        Me.lblTotalDocumentos = New System.Windows.Forms.Label
        Me.clcPagoInicial = New Dipros.Editors.TINCalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.clcImportePlan = New Dipros.Editors.TINCalcEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.clcPagoInicialDisplay = New Dipros.Editors.TINCalcEdit
        Me.clcImporteConDescuento = New Dipros.Editors.TINCalcEdit
        Me.chkaplica_enganche = New DevExpress.XtraEditors.CheckEdit
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.lkpConveniosClientes = New Dipros.Editors.TINMultiLookup
        Me.chkFacturacionEspecial = New DevExpress.XtraEditors.CheckEdit
        Me.clcAnticipos = New Dipros.Editors.TINCalcEdit
        Me.lblAnticipos = New System.Windows.Forms.Label
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMenos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcEnganche1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Primer_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcNumero_Documentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte_Documentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte_Ultimo_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEnganche_En_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dteFecha_Enganche_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte_Enganche_Documento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.clcMonto_Liquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIvadesglosado1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLiquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMenos_Enganch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEnganche_en_Menos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPagoInicial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImportePlan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPagoInicialDisplay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporteConDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkaplica_enganche.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFacturacionEspecial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcAnticipos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(19005, 28)
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(479, 64)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 23
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(536, 64)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte.TabIndex = 24
        Me.clcImporte.Tag = "importe"
        '
        'lblMenos
        '
        Me.lblMenos.AutoSize = True
        Me.lblMenos.Location = New System.Drawing.Point(353, 112)
        Me.lblMenos.Name = "lblMenos"
        Me.lblMenos.Size = New System.Drawing.Size(44, 16)
        Me.lblMenos.TabIndex = 102
        Me.lblMenos.Tag = ""
        Me.lblMenos.Text = "&Menos:"
        Me.lblMenos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMenos.Visible = False
        '
        'clcMenos
        '
        Me.clcMenos.EditValue = "0"
        Me.clcMenos.Location = New System.Drawing.Point(401, 112)
        Me.clcMenos.MaxValue = 0
        Me.clcMenos.MinValue = 0
        Me.clcMenos.Name = "clcMenos"
        '
        'clcMenos.Properties
        '
        Me.clcMenos.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcMenos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenos.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcMenos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenos.Properties.Enabled = False
        Me.clcMenos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMenos.Properties.Precision = 2
        Me.clcMenos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMenos.Size = New System.Drawing.Size(32, 19)
        Me.clcMenos.TabIndex = 103
        Me.clcMenos.TabStop = False
        Me.clcMenos.Tag = "menos"
        Me.clcMenos.Visible = False
        '
        'lblInteres
        '
        Me.lblInteres.AutoSize = True
        Me.lblInteres.Location = New System.Drawing.Point(484, 114)
        Me.lblInteres.Name = "lblInteres"
        Me.lblInteres.Size = New System.Drawing.Size(48, 16)
        Me.lblInteres.TabIndex = 27
        Me.lblInteres.Tag = ""
        Me.lblInteres.Text = "&Int�res:"
        Me.lblInteres.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEnganche1
        '
        Me.clcEnganche1.EditValue = "0"
        Me.clcEnganche1.Location = New System.Drawing.Point(401, 136)
        Me.clcEnganche1.MaxValue = 0
        Me.clcEnganche1.MinValue = 0
        Me.clcEnganche1.Name = "clcEnganche1"
        '
        'clcEnganche1.Properties
        '
        Me.clcEnganche1.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcEnganche1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche1.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcEnganche1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEnganche1.Properties.Enabled = False
        Me.clcEnganche1.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcEnganche1.Properties.Precision = 2
        Me.clcEnganche1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEnganche1.Size = New System.Drawing.Size(32, 19)
        Me.clcEnganche1.TabIndex = 105
        Me.clcEnganche1.TabStop = False
        Me.clcEnganche1.Tag = "enganche"
        Me.clcEnganche1.Visible = False
        '
        'clcIntereses
        '
        Me.clcIntereses.EditValue = "0"
        Me.clcIntereses.Location = New System.Drawing.Point(536, 109)
        Me.clcIntereses.MaxValue = 0
        Me.clcIntereses.MinValue = 0
        Me.clcIntereses.Name = "clcIntereses"
        '
        'clcIntereses.Properties
        '
        Me.clcIntereses.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcIntereses.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcIntereses.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcIntereses.Properties.Precision = 2
        Me.clcIntereses.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIntereses.Size = New System.Drawing.Size(80, 19)
        Me.clcIntereses.TabIndex = 28
        Me.clcIntereses.Tag = "interes"
        '
        'lblPlan
        '
        Me.lblPlan.AutoSize = True
        Me.lblPlan.Location = New System.Drawing.Point(96, 42)
        Me.lblPlan.Name = "lblPlan"
        Me.lblPlan.Size = New System.Drawing.Size(91, 16)
        Me.lblPlan.TabIndex = 0
        Me.lblPlan.Tag = ""
        Me.lblPlan.Text = "&Plan de cr�dito:"
        Me.lblPlan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPlan
        '
        Me.lkpPlan.AllowAdd = False
        Me.lkpPlan.AutoReaload = False
        Me.lkpPlan.DataSource = Nothing
        Me.lkpPlan.DefaultSearchField = ""
        Me.lkpPlan.DisplayMember = "descripcion"
        Me.lkpPlan.EditValue = Nothing
        Me.lkpPlan.Filtered = False
        Me.lkpPlan.InitValue = Nothing
        Me.lkpPlan.Location = New System.Drawing.Point(192, 39)
        Me.lkpPlan.MultiSelect = False
        Me.lkpPlan.Name = "lkpPlan"
        Me.lkpPlan.NullText = ""
        Me.lkpPlan.PopupWidth = CType(450, Long)
        Me.lkpPlan.ReadOnlyControl = False
        Me.lkpPlan.Required = False
        Me.lkpPlan.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlan.SearchMember = ""
        Me.lkpPlan.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlan.SelectAll = False
        Me.lkpPlan.Size = New System.Drawing.Size(344, 20)
        Me.lkpPlan.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPlan.TabIndex = 1
        Me.lkpPlan.Tag = "plan_credito"
        Me.lkpPlan.ToolTip = Nothing
        Me.lkpPlan.ValueMember = "plan_credito"
        '
        'lblFecha_Primer_Documento
        '
        Me.lblFecha_Primer_Documento.AutoSize = True
        Me.lblFecha_Primer_Documento.Location = New System.Drawing.Point(20, 66)
        Me.lblFecha_Primer_Documento.Name = "lblFecha_Primer_Documento"
        Me.lblFecha_Primer_Documento.Size = New System.Drawing.Size(167, 16)
        Me.lblFecha_Primer_Documento.TabIndex = 2
        Me.lblFecha_Primer_Documento.Tag = ""
        Me.lblFecha_Primer_Documento.Text = "&Fecha del primer documento:"
        Me.lblFecha_Primer_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Primer_Documento
        '
        Me.dteFecha_Primer_Documento.EditValue = "10/04/2006"
        Me.dteFecha_Primer_Documento.Location = New System.Drawing.Point(192, 63)
        Me.dteFecha_Primer_Documento.Name = "dteFecha_Primer_Documento"
        '
        'dteFecha_Primer_Documento.Properties
        '
        Me.dteFecha_Primer_Documento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Primer_Documento.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Primer_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Primer_Documento.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha_Primer_Documento.TabIndex = 3
        Me.dteFecha_Primer_Documento.Tag = "fecha_primer_documento"
        '
        'lblNumero_Documentos
        '
        Me.lblNumero_Documentos.AutoSize = True
        Me.lblNumero_Documentos.Location = New System.Drawing.Point(48, 136)
        Me.lblNumero_Documentos.Name = "lblNumero_Documentos"
        Me.lblNumero_Documentos.Size = New System.Drawing.Size(141, 16)
        Me.lblNumero_Documentos.TabIndex = 8
        Me.lblNumero_Documentos.Tag = ""
        Me.lblNumero_Documentos.Text = "&N�mero de documentos:"
        Me.lblNumero_Documentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcNumero_Documentos
        '
        Me.clcNumero_Documentos.EditValue = "0"
        Me.clcNumero_Documentos.Location = New System.Drawing.Point(192, 136)
        Me.clcNumero_Documentos.MaxValue = 0
        Me.clcNumero_Documentos.MinValue = 0
        Me.clcNumero_Documentos.Name = "clcNumero_Documentos"
        '
        'clcNumero_Documentos.Properties
        '
        Me.clcNumero_Documentos.Properties.DisplayFormat.FormatString = "####0"
        Me.clcNumero_Documentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcNumero_Documentos.Properties.EditFormat.FormatString = "####0"
        Me.clcNumero_Documentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcNumero_Documentos.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcNumero_Documentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcNumero_Documentos.Size = New System.Drawing.Size(32, 19)
        Me.clcNumero_Documentos.TabIndex = 9
        Me.clcNumero_Documentos.Tag = "numero_documentos"
        '
        'lblImporte_Documentos
        '
        Me.lblImporte_Documentos.AutoSize = True
        Me.lblImporte_Documentos.Location = New System.Drawing.Point(48, 160)
        Me.lblImporte_Documentos.Name = "lblImporte_Documentos"
        Me.lblImporte_Documentos.Size = New System.Drawing.Size(142, 16)
        Me.lblImporte_Documentos.TabIndex = 10
        Me.lblImporte_Documentos.Tag = ""
        Me.lblImporte_Documentos.Text = "&Importe de documentos:"
        Me.lblImporte_Documentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte_Documentos
        '
        Me.clcImporte_Documentos.EditValue = "0"
        Me.clcImporte_Documentos.Location = New System.Drawing.Point(192, 160)
        Me.clcImporte_Documentos.MaxValue = 0
        Me.clcImporte_Documentos.MinValue = 0
        Me.clcImporte_Documentos.Name = "clcImporte_Documentos"
        '
        'clcImporte_Documentos.Properties
        '
        Me.clcImporte_Documentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Documentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Documentos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte_Documentos.Properties.Precision = 2
        Me.clcImporte_Documentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte_Documentos.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte_Documentos.TabIndex = 11
        Me.clcImporte_Documentos.Tag = "importe_documentos"
        '
        'lblImporte_Ultimo_Documento
        '
        Me.lblImporte_Ultimo_Documento.AutoSize = True
        Me.lblImporte_Ultimo_Documento.Location = New System.Drawing.Point(8, 184)
        Me.lblImporte_Ultimo_Documento.Name = "lblImporte_Ultimo_Documento"
        Me.lblImporte_Ultimo_Documento.Size = New System.Drawing.Size(177, 16)
        Me.lblImporte_Ultimo_Documento.TabIndex = 12
        Me.lblImporte_Ultimo_Documento.Tag = ""
        Me.lblImporte_Ultimo_Documento.Text = "&Importe del �ltimo documento:"
        Me.lblImporte_Ultimo_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte_Ultimo_Documento
        '
        Me.clcImporte_Ultimo_Documento.EditValue = "0"
        Me.clcImporte_Ultimo_Documento.Location = New System.Drawing.Point(192, 184)
        Me.clcImporte_Ultimo_Documento.MaxValue = 0
        Me.clcImporte_Ultimo_Documento.MinValue = 0
        Me.clcImporte_Ultimo_Documento.Name = "clcImporte_Ultimo_Documento"
        '
        'clcImporte_Ultimo_Documento.Properties
        '
        Me.clcImporte_Ultimo_Documento.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte_Ultimo_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Ultimo_Documento.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte_Ultimo_Documento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Ultimo_Documento.Properties.Enabled = False
        Me.clcImporte_Ultimo_Documento.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte_Ultimo_Documento.Properties.Precision = 2
        Me.clcImporte_Ultimo_Documento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte_Ultimo_Documento.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcImporte_Ultimo_Documento.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte_Ultimo_Documento.TabIndex = 13
        Me.clcImporte_Ultimo_Documento.Tag = "importe_ultimo_documento"
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Location = New System.Drawing.Point(452, 152)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(180, 1)
        Me.Label2.TabIndex = 20
        '
        'chkEnganche_En_Documento
        '
        Me.chkEnganche_En_Documento.EditValue = "False"
        Me.chkEnganche_En_Documento.Location = New System.Drawing.Point(376, 264)
        Me.chkEnganche_En_Documento.Name = "chkEnganche_En_Documento"
        '
        'chkEnganche_En_Documento.Properties
        '
        Me.chkEnganche_En_Documento.Properties.Caption = "&Enganche en documento"
        Me.chkEnganche_En_Documento.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkEnganche_En_Documento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkEnganche_En_Documento.Size = New System.Drawing.Size(152, 19)
        Me.chkEnganche_En_Documento.TabIndex = 32
        Me.chkEnganche_En_Documento.Tag = "enganche_en_documento"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblFecha_Enganche_Documento)
        Me.GroupBox1.Controls.Add(Me.dteFecha_Enganche_Documento)
        Me.GroupBox1.Controls.Add(Me.lblImporte_Enganche_Documento)
        Me.GroupBox1.Controls.Add(Me.clcImporte_Enganche_Documento)
        Me.GroupBox1.Location = New System.Drawing.Point(368, 272)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(264, 80)
        Me.GroupBox1.TabIndex = 33
        Me.GroupBox1.TabStop = False
        '
        'lblFecha_Enganche_Documento
        '
        Me.lblFecha_Enganche_Documento.AutoSize = True
        Me.lblFecha_Enganche_Documento.Location = New System.Drawing.Point(67, 24)
        Me.lblFecha_Enganche_Documento.Name = "lblFecha_Enganche_Documento"
        Me.lblFecha_Enganche_Documento.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha_Enganche_Documento.TabIndex = 0
        Me.lblFecha_Enganche_Documento.Tag = ""
        Me.lblFecha_Enganche_Documento.Text = "&Fecha:"
        Me.lblFecha_Enganche_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Enganche_Documento
        '
        Me.dteFecha_Enganche_Documento.EditValue = "10/04/2006"
        Me.dteFecha_Enganche_Documento.Location = New System.Drawing.Point(115, 24)
        Me.dteFecha_Enganche_Documento.Name = "dteFecha_Enganche_Documento"
        '
        'dteFecha_Enganche_Documento.Properties
        '
        Me.dteFecha_Enganche_Documento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Enganche_Documento.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Enganche_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Enganche_Documento.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFecha_Enganche_Documento.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha_Enganche_Documento.TabIndex = 1
        Me.dteFecha_Enganche_Documento.Tag = "fecha_enganche_documento"
        '
        'lblImporte_Enganche_Documento
        '
        Me.lblImporte_Enganche_Documento.AutoSize = True
        Me.lblImporte_Enganche_Documento.Location = New System.Drawing.Point(55, 48)
        Me.lblImporte_Enganche_Documento.Name = "lblImporte_Enganche_Documento"
        Me.lblImporte_Enganche_Documento.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte_Enganche_Documento.TabIndex = 2
        Me.lblImporte_Enganche_Documento.Tag = ""
        Me.lblImporte_Enganche_Documento.Text = "&Importe:"
        Me.lblImporte_Enganche_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte_Enganche_Documento
        '
        Me.clcImporte_Enganche_Documento.EditValue = "0"
        Me.clcImporte_Enganche_Documento.Location = New System.Drawing.Point(115, 48)
        Me.clcImporte_Enganche_Documento.MaxValue = 0
        Me.clcImporte_Enganche_Documento.MinValue = 0
        Me.clcImporte_Enganche_Documento.Name = "clcImporte_Enganche_Documento"
        '
        'clcImporte_Enganche_Documento.Properties
        '
        Me.clcImporte_Enganche_Documento.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte_Enganche_Documento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Enganche_Documento.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte_Enganche_Documento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte_Enganche_Documento.Properties.Enabled = False
        Me.clcImporte_Enganche_Documento.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte_Enganche_Documento.Properties.Precision = 2
        Me.clcImporte_Enganche_Documento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte_Enganche_Documento.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcImporte_Enganche_Documento.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte_Enganche_Documento.TabIndex = 3
        Me.clcImporte_Enganche_Documento.Tag = "importe_enganche_documento"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.clcMonto_Liquida_Vencimiento)
        Me.GroupBox2.Controls.Add(Me.lblMonto_Liquida_Vencimiento)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 296)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(344, 56)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = False
        '
        'clcMonto_Liquida_Vencimiento
        '
        Me.clcMonto_Liquida_Vencimiento.EditValue = "0"
        Me.clcMonto_Liquida_Vencimiento.Location = New System.Drawing.Point(160, 24)
        Me.clcMonto_Liquida_Vencimiento.MaxValue = 0
        Me.clcMonto_Liquida_Vencimiento.MinValue = 0
        Me.clcMonto_Liquida_Vencimiento.Name = "clcMonto_Liquida_Vencimiento"
        '
        'clcMonto_Liquida_Vencimiento.Properties
        '
        Me.clcMonto_Liquida_Vencimiento.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcMonto_Liquida_Vencimiento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto_Liquida_Vencimiento.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcMonto_Liquida_Vencimiento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto_Liquida_Vencimiento.Properties.Enabled = False
        Me.clcMonto_Liquida_Vencimiento.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMonto_Liquida_Vencimiento.Properties.Precision = 2
        Me.clcMonto_Liquida_Vencimiento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMonto_Liquida_Vencimiento.Size = New System.Drawing.Size(80, 19)
        Me.clcMonto_Liquida_Vencimiento.TabIndex = 1
        Me.clcMonto_Liquida_Vencimiento.Tag = "monto_liquida_vencimiento"
        '
        'lblMonto_Liquida_Vencimiento
        '
        Me.lblMonto_Liquida_Vencimiento.AutoSize = True
        Me.lblMonto_Liquida_Vencimiento.Location = New System.Drawing.Point(104, 24)
        Me.lblMonto_Liquida_Vencimiento.Name = "lblMonto_Liquida_Vencimiento"
        Me.lblMonto_Liquida_Vencimiento.Size = New System.Drawing.Size(47, 16)
        Me.lblMonto_Liquida_Vencimiento.TabIndex = 0
        Me.lblMonto_Liquida_Vencimiento.Tag = ""
        Me.lblMonto_Liquida_Vencimiento.Text = "&Pagar�:"
        Me.lblMonto_Liquida_Vencimiento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(496, 154)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(36, 16)
        Me.lblTotal.TabIndex = 29
        Me.lblTotal.Tag = ""
        Me.lblTotal.Text = "&Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTotal
        '
        Me.clcTotal.EditValue = "0"
        Me.clcTotal.Location = New System.Drawing.Point(536, 154)
        Me.clcTotal.MaxValue = 0
        Me.clcTotal.MinValue = 0
        Me.clcTotal.Name = "clcTotal"
        '
        'clcTotal.Properties
        '
        Me.clcTotal.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTotal.Properties.Enabled = False
        Me.clcTotal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTotal.Properties.Precision = 2
        Me.clcTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTotal.Size = New System.Drawing.Size(80, 19)
        Me.clcTotal.TabIndex = 30
        Me.clcTotal.Tag = "total"
        '
        'chkIvadesglosado1
        '
        Me.chkIvadesglosado1.EditValue = "False"
        Me.chkIvadesglosado1.Location = New System.Drawing.Point(464, 192)
        Me.chkIvadesglosado1.Name = "chkIvadesglosado1"
        '
        'chkIvadesglosado1.Properties
        '
        Me.chkIvadesglosado1.Properties.Caption = "Desea Deducir la Factura"
        Me.chkIvadesglosado1.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkIvadesglosado1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkIvadesglosado1.Size = New System.Drawing.Size(168, 19)
        Me.chkIvadesglosado1.TabIndex = 31
        Me.chkIvadesglosado1.Tag = "ivadesglosado"
        '
        'chkLiquida_Vencimiento
        '
        Me.chkLiquida_Vencimiento.EditValue = "False"
        Me.chkLiquida_Vencimiento.Location = New System.Drawing.Point(16, 288)
        Me.chkLiquida_Vencimiento.Name = "chkLiquida_Vencimiento"
        '
        'chkLiquida_Vencimiento.Properties
        '
        Me.chkLiquida_Vencimiento.Properties.Caption = "&Liquida al vencimiento a tiempo"
        Me.chkLiquida_Vencimiento.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkLiquida_Vencimiento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkLiquida_Vencimiento.Size = New System.Drawing.Size(184, 19)
        Me.chkLiquida_Vencimiento.TabIndex = 21
        Me.chkLiquida_Vencimiento.Tag = "liquida_vencimiento"
        '
        'clcMenos_Enganch
        '
        Me.clcMenos_Enganch.EditValue = "0"
        Me.clcMenos_Enganch.Location = New System.Drawing.Point(304, 64)
        Me.clcMenos_Enganch.MaxValue = 0
        Me.clcMenos_Enganch.MinValue = 0
        Me.clcMenos_Enganch.Name = "clcMenos_Enganch"
        '
        'clcMenos_Enganch.Properties
        '
        Me.clcMenos_Enganch.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcMenos_Enganch.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenos_Enganch.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcMenos_Enganch.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMenos_Enganch.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMenos_Enganch.Properties.Precision = 2
        Me.clcMenos_Enganch.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMenos_Enganch.Size = New System.Drawing.Size(80, 19)
        Me.clcMenos_Enganch.TabIndex = 100
        Me.clcMenos_Enganch.TabStop = False
        Me.clcMenos_Enganch.Tag = ""
        Me.clcMenos_Enganch.Visible = False
        '
        'lblEnganche1
        '
        Me.lblEnganche1.AutoSize = True
        Me.lblEnganche1.Location = New System.Drawing.Point(329, 136)
        Me.lblEnganche1.Name = "lblEnganche1"
        Me.lblEnganche1.Size = New System.Drawing.Size(63, 16)
        Me.lblEnganche1.TabIndex = 104
        Me.lblEnganche1.Tag = ""
        Me.lblEnganche1.Text = "&Enganche:"
        Me.lblEnganche1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEnganche1.Visible = False
        '
        'chkEnganche_en_Menos
        '
        Me.chkEnganche_en_Menos.EditValue = "False"
        Me.chkEnganche_en_Menos.Location = New System.Drawing.Point(304, 88)
        Me.chkEnganche_en_Menos.Name = "chkEnganche_en_Menos"
        '
        'chkEnganche_en_Menos.Properties
        '
        Me.chkEnganche_en_Menos.Properties.Caption = "Enganche en Menos"
        Me.chkEnganche_en_Menos.Properties.Enabled = False
        Me.chkEnganche_en_Menos.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkEnganche_en_Menos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkEnganche_en_Menos.Size = New System.Drawing.Size(128, 19)
        Me.chkEnganche_en_Menos.TabIndex = 101
        Me.chkEnganche_en_Menos.TabStop = False
        Me.chkEnganche_en_Menos.Tag = "enganche_en_menos"
        '
        'lblLeyendaDocumentos
        '
        Me.lblLeyendaDocumentos.Location = New System.Drawing.Point(64, 216)
        Me.lblLeyendaDocumentos.Name = "lblLeyendaDocumentos"
        Me.lblLeyendaDocumentos.Size = New System.Drawing.Size(160, 23)
        Me.lblLeyendaDocumentos.TabIndex = 15
        Me.lblLeyendaDocumentos.Text = "Documentos de = "
        Me.lblLeyendaDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLeyendaDocumentoFinal
        '
        Me.lblLeyendaDocumentoFinal.Location = New System.Drawing.Point(64, 240)
        Me.lblLeyendaDocumentoFinal.Name = "lblLeyendaDocumentoFinal"
        Me.lblLeyendaDocumentoFinal.Size = New System.Drawing.Size(160, 23)
        Me.lblLeyendaDocumentoFinal.TabIndex = 18
        Me.lblLeyendaDocumentoFinal.Text = "Documento de  ="
        Me.lblLeyendaDocumentoFinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDocumentoFinal
        '
        Me.lblDocumentoFinal.BackColor = System.Drawing.SystemColors.Window
        Me.lblDocumentoFinal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentoFinal.ForeColor = System.Drawing.Color.Black
        Me.lblDocumentoFinal.Location = New System.Drawing.Point(32, 240)
        Me.lblDocumentoFinal.Name = "lblDocumentoFinal"
        Me.lblDocumentoFinal.Size = New System.Drawing.Size(32, 23)
        Me.lblDocumentoFinal.TabIndex = 17
        Me.lblDocumentoFinal.Text = "0"
        Me.lblDocumentoFinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDocumentosIguales
        '
        Me.lblDocumentosIguales.BackColor = System.Drawing.SystemColors.Window
        Me.lblDocumentosIguales.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentosIguales.ForeColor = System.Drawing.Color.Black
        Me.lblDocumentosIguales.Location = New System.Drawing.Point(32, 216)
        Me.lblDocumentosIguales.Name = "lblDocumentosIguales"
        Me.lblDocumentosIguales.Size = New System.Drawing.Size(32, 23)
        Me.lblDocumentosIguales.TabIndex = 14
        Me.lblDocumentosIguales.Text = "0"
        Me.lblDocumentosIguales.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImporteUltimoDocumento
        '
        Me.lblImporteUltimoDocumento.Location = New System.Drawing.Point(232, 240)
        Me.lblImporteUltimoDocumento.Name = "lblImporteUltimoDocumento"
        Me.lblImporteUltimoDocumento.Size = New System.Drawing.Size(88, 23)
        Me.lblImporteUltimoDocumento.TabIndex = 19
        Me.lblImporteUltimoDocumento.Text = "0.00"
        Me.lblImporteUltimoDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Location = New System.Drawing.Point(232, 264)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 1)
        Me.Label9.TabIndex = 20
        '
        'lblDocumentosUltimoDocumento
        '
        Me.lblDocumentosUltimoDocumento.Location = New System.Drawing.Point(232, 264)
        Me.lblDocumentosUltimoDocumento.Name = "lblDocumentosUltimoDocumento"
        Me.lblDocumentosUltimoDocumento.Size = New System.Drawing.Size(88, 23)
        Me.lblDocumentosUltimoDocumento.TabIndex = 28
        Me.lblDocumentosUltimoDocumento.Text = "0.00"
        Me.lblDocumentosUltimoDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalDocumentos
        '
        Me.lblTotalDocumentos.Location = New System.Drawing.Point(232, 216)
        Me.lblTotalDocumentos.Name = "lblTotalDocumentos"
        Me.lblTotalDocumentos.Size = New System.Drawing.Size(88, 23)
        Me.lblTotalDocumentos.TabIndex = 16
        Me.lblTotalDocumentos.Text = "0.00"
        Me.lblTotalDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPagoInicial
        '
        Me.clcPagoInicial.EditValue = "0"
        Me.clcPagoInicial.Location = New System.Drawing.Point(192, 88)
        Me.clcPagoInicial.MaxValue = 0
        Me.clcPagoInicial.MinValue = 0
        Me.clcPagoInicial.Name = "clcPagoInicial"
        '
        'clcPagoInicial.Properties
        '
        Me.clcPagoInicial.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPagoInicial.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPagoInicial.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPagoInicial.Properties.Precision = 2
        Me.clcPagoInicial.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPagoInicial.Size = New System.Drawing.Size(80, 19)
        Me.clcPagoInicial.TabIndex = 5
        Me.clcPagoInicial.Tag = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(456, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 16)
        Me.Label3.TabIndex = 25
        Me.Label3.Tag = ""
        Me.Label3.Text = "Pa&go Inicial:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(104, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Tag = ""
        Me.Label4.Text = "&Importe Plan:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImportePlan
        '
        Me.clcImportePlan.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImportePlan.Location = New System.Drawing.Point(192, 112)
        Me.clcImportePlan.MaxValue = 0
        Me.clcImportePlan.MinValue = 0
        Me.clcImportePlan.Name = "clcImportePlan"
        '
        'clcImportePlan.Properties
        '
        Me.clcImportePlan.Properties.DisplayFormat.FormatString = "$##,###,#00.00"
        Me.clcImportePlan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImportePlan.Properties.EditFormat.FormatString = "$##,###,#00.00"
        Me.clcImportePlan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImportePlan.Properties.Enabled = False
        Me.clcImportePlan.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcImportePlan.Properties.Precision = 2
        Me.clcImportePlan.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImportePlan.Size = New System.Drawing.Size(80, 21)
        Me.clcImportePlan.TabIndex = 7
        Me.clcImportePlan.Tag = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(114, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Tag = ""
        Me.Label5.Text = "Pa&go Inicial:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPagoInicialDisplay
        '
        Me.clcPagoInicialDisplay.EditValue = "0"
        Me.clcPagoInicialDisplay.Location = New System.Drawing.Point(536, 86)
        Me.clcPagoInicialDisplay.MaxValue = 0
        Me.clcPagoInicialDisplay.MinValue = 0
        Me.clcPagoInicialDisplay.Name = "clcPagoInicialDisplay"
        '
        'clcPagoInicialDisplay.Properties
        '
        Me.clcPagoInicialDisplay.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcPagoInicialDisplay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPagoInicialDisplay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPagoInicialDisplay.Properties.Enabled = False
        Me.clcPagoInicialDisplay.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcPagoInicialDisplay.Properties.Precision = 2
        Me.clcPagoInicialDisplay.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPagoInicialDisplay.Size = New System.Drawing.Size(80, 20)
        Me.clcPagoInicialDisplay.TabIndex = 26
        Me.clcPagoInicialDisplay.Tag = ""
        '
        'clcImporteConDescuento
        '
        Me.clcImporteConDescuento.EditValue = "0"
        Me.clcImporteConDescuento.Location = New System.Drawing.Point(192, 112)
        Me.clcImporteConDescuento.MaxValue = 0
        Me.clcImporteConDescuento.MinValue = 0
        Me.clcImporteConDescuento.Name = "clcImporteConDescuento"
        '
        'clcImporteConDescuento.Properties
        '
        Me.clcImporteConDescuento.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporteConDescuento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteConDescuento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteConDescuento.Properties.Enabled = False
        Me.clcImporteConDescuento.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporteConDescuento.Properties.Precision = 2
        Me.clcImporteConDescuento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporteConDescuento.Size = New System.Drawing.Size(80, 21)
        Me.clcImporteConDescuento.TabIndex = 106
        Me.clcImporteConDescuento.Tag = ""
        Me.clcImporteConDescuento.Visible = False
        '
        'chkaplica_enganche
        '
        Me.chkaplica_enganche.EditValue = "False"
        Me.chkaplica_enganche.Location = New System.Drawing.Point(464, 208)
        Me.chkaplica_enganche.Name = "chkaplica_enganche"
        '
        'chkaplica_enganche.Properties
        '
        Me.chkaplica_enganche.Properties.Caption = "Aplica para Enganche"
        Me.chkaplica_enganche.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkaplica_enganche.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkaplica_enganche.Size = New System.Drawing.Size(168, 19)
        Me.chkaplica_enganche.TabIndex = 107
        Me.chkaplica_enganche.Tag = "aplica_enganche"
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(382, 232)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenio.TabIndex = 109
        Me.lblConvenio.Tag = ""
        Me.lblConvenio.Text = "Convenio:"
        Me.lblConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblConvenio.Visible = False
        '
        'lkpConveniosClientes
        '
        Me.lkpConveniosClientes.AllowAdd = False
        Me.lkpConveniosClientes.AutoReaload = True
        Me.lkpConveniosClientes.DataSource = Nothing
        Me.lkpConveniosClientes.DefaultSearchField = ""
        Me.lkpConveniosClientes.DisplayMember = "nombre_convenio"
        Me.lkpConveniosClientes.EditValue = Nothing
        Me.lkpConveniosClientes.Enabled = False
        Me.lkpConveniosClientes.Filtered = False
        Me.lkpConveniosClientes.InitValue = Nothing
        Me.lkpConveniosClientes.Location = New System.Drawing.Point(448, 232)
        Me.lkpConveniosClientes.MultiSelect = False
        Me.lkpConveniosClientes.Name = "lkpConveniosClientes"
        Me.lkpConveniosClientes.NullText = ""
        Me.lkpConveniosClientes.PopupWidth = CType(420, Long)
        Me.lkpConveniosClientes.ReadOnlyControl = False
        Me.lkpConveniosClientes.Required = False
        Me.lkpConveniosClientes.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConveniosClientes.SearchMember = ""
        Me.lkpConveniosClientes.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConveniosClientes.SelectAll = False
        Me.lkpConveniosClientes.Size = New System.Drawing.Size(184, 20)
        Me.lkpConveniosClientes.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConveniosClientes.TabIndex = 108
        Me.lkpConveniosClientes.TabStop = False
        Me.lkpConveniosClientes.Tag = ""
        Me.lkpConveniosClientes.ToolTip = Nothing
        Me.lkpConveniosClientes.ValueMember = "convenio"
        Me.lkpConveniosClientes.Visible = False
        '
        'chkFacturacionEspecial
        '
        Me.chkFacturacionEspecial.EditValue = "False"
        Me.chkFacturacionEspecial.Location = New System.Drawing.Point(464, 176)
        Me.chkFacturacionEspecial.Name = "chkFacturacionEspecial"
        '
        'chkFacturacionEspecial.Properties
        '
        Me.chkFacturacionEspecial.Properties.Caption = "Facturaci�n Especial"
        Me.chkFacturacionEspecial.Properties.Enabled = False
        Me.chkFacturacionEspecial.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkFacturacionEspecial.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkFacturacionEspecial.Size = New System.Drawing.Size(136, 19)
        Me.chkFacturacionEspecial.TabIndex = 110
        Me.chkFacturacionEspecial.Tag = "facturacion_especial"
        '
        'clcAnticipos
        '
        Me.clcAnticipos.EditValue = "0"
        Me.clcAnticipos.Location = New System.Drawing.Point(536, 131)
        Me.clcAnticipos.MaxValue = 0
        Me.clcAnticipos.MinValue = 0
        Me.clcAnticipos.Name = "clcAnticipos"
        '
        'clcAnticipos.Properties
        '
        Me.clcAnticipos.Properties.DisplayFormat.FormatString = "c2"
        Me.clcAnticipos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnticipos.Properties.EditFormat.FormatString = "c2"
        Me.clcAnticipos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnticipos.Properties.Enabled = False
        Me.clcAnticipos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcAnticipos.Properties.Precision = 2
        Me.clcAnticipos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAnticipos.Size = New System.Drawing.Size(80, 19)
        Me.clcAnticipos.TabIndex = 111
        Me.clcAnticipos.Tag = "anticipos"
        '
        'lblAnticipos
        '
        Me.lblAnticipos.AutoSize = True
        Me.lblAnticipos.Location = New System.Drawing.Point(473, 134)
        Me.lblAnticipos.Name = "lblAnticipos"
        Me.lblAnticipos.Size = New System.Drawing.Size(59, 16)
        Me.lblAnticipos.TabIndex = 112
        Me.lblAnticipos.Tag = ""
        Me.lblAnticipos.Text = "Anticipos:"
        Me.lblAnticipos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmVentasDatosCredito
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(642, 360)
        Me.Controls.Add(Me.lblAnticipos)
        Me.Controls.Add(Me.clcAnticipos)
        Me.Controls.Add(Me.chkFacturacionEspecial)
        Me.Controls.Add(Me.lblConvenio)
        Me.Controls.Add(Me.lkpConveniosClientes)
        Me.Controls.Add(Me.chkaplica_enganche)
        Me.Controls.Add(Me.clcPagoInicialDisplay)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.clcPagoInicial)
        Me.Controls.Add(Me.lblTotalDocumentos)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblDocumentosUltimoDocumento)
        Me.Controls.Add(Me.lblImporteUltimoDocumento)
        Me.Controls.Add(Me.lblLeyendaDocumentoFinal)
        Me.Controls.Add(Me.lblDocumentoFinal)
        Me.Controls.Add(Me.lblLeyendaDocumentos)
        Me.Controls.Add(Me.lblDocumentosIguales)
        Me.Controls.Add(Me.chkEnganche_en_Menos)
        Me.Controls.Add(Me.lblEnganche1)
        Me.Controls.Add(Me.clcMenos_Enganch)
        Me.Controls.Add(Me.chkLiquida_Vencimiento)
        Me.Controls.Add(Me.chkIvadesglosado1)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.clcTotal)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.chkEnganche_En_Documento)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblPlan)
        Me.Controls.Add(Me.lkpPlan)
        Me.Controls.Add(Me.lblFecha_Primer_Documento)
        Me.Controls.Add(Me.dteFecha_Primer_Documento)
        Me.Controls.Add(Me.lblNumero_Documentos)
        Me.Controls.Add(Me.clcNumero_Documentos)
        Me.Controls.Add(Me.lblImporte_Documentos)
        Me.Controls.Add(Me.clcImporte_Documentos)
        Me.Controls.Add(Me.lblImporte_Ultimo_Documento)
        Me.Controls.Add(Me.clcImporte_Ultimo_Documento)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lblMenos)
        Me.Controls.Add(Me.clcMenos)
        Me.Controls.Add(Me.lblInteres)
        Me.Controls.Add(Me.clcEnganche1)
        Me.Controls.Add(Me.clcIntereses)
        Me.Controls.Add(Me.clcImporteConDescuento)
        Me.Controls.Add(Me.clcImportePlan)
        Me.Name = "frmVentasDatosCredito"
        Me.Text = "frmVentasDatosCredito"
        Me.Controls.SetChildIndex(Me.clcImportePlan, 0)
        Me.Controls.SetChildIndex(Me.clcImporteConDescuento, 0)
        Me.Controls.SetChildIndex(Me.clcIntereses, 0)
        Me.Controls.SetChildIndex(Me.clcEnganche1, 0)
        Me.Controls.SetChildIndex(Me.lblInteres, 0)
        Me.Controls.SetChildIndex(Me.clcMenos, 0)
        Me.Controls.SetChildIndex(Me.lblMenos, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.clcImporte_Ultimo_Documento, 0)
        Me.Controls.SetChildIndex(Me.lblImporte_Ultimo_Documento, 0)
        Me.Controls.SetChildIndex(Me.clcImporte_Documentos, 0)
        Me.Controls.SetChildIndex(Me.lblImporte_Documentos, 0)
        Me.Controls.SetChildIndex(Me.clcNumero_Documentos, 0)
        Me.Controls.SetChildIndex(Me.lblNumero_Documentos, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Primer_Documento, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Primer_Documento, 0)
        Me.Controls.SetChildIndex(Me.lkpPlan, 0)
        Me.Controls.SetChildIndex(Me.lblPlan, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.chkEnganche_En_Documento, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.clcTotal, 0)
        Me.Controls.SetChildIndex(Me.lblTotal, 0)
        Me.Controls.SetChildIndex(Me.chkIvadesglosado1, 0)
        Me.Controls.SetChildIndex(Me.chkLiquida_Vencimiento, 0)
        Me.Controls.SetChildIndex(Me.clcMenos_Enganch, 0)
        Me.Controls.SetChildIndex(Me.lblEnganche1, 0)
        Me.Controls.SetChildIndex(Me.chkEnganche_en_Menos, 0)
        Me.Controls.SetChildIndex(Me.lblDocumentosIguales, 0)
        Me.Controls.SetChildIndex(Me.lblLeyendaDocumentos, 0)
        Me.Controls.SetChildIndex(Me.lblDocumentoFinal, 0)
        Me.Controls.SetChildIndex(Me.lblLeyendaDocumentoFinal, 0)
        Me.Controls.SetChildIndex(Me.lblImporteUltimoDocumento, 0)
        Me.Controls.SetChildIndex(Me.lblDocumentosUltimoDocumento, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        Me.Controls.SetChildIndex(Me.lblTotalDocumentos, 0)
        Me.Controls.SetChildIndex(Me.clcPagoInicial, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.clcPagoInicialDisplay, 0)
        Me.Controls.SetChildIndex(Me.chkaplica_enganche, 0)
        Me.Controls.SetChildIndex(Me.lkpConveniosClientes, 0)
        Me.Controls.SetChildIndex(Me.lblConvenio, 0)
        Me.Controls.SetChildIndex(Me.chkFacturacionEspecial, 0)
        Me.Controls.SetChildIndex(Me.clcAnticipos, 0)
        Me.Controls.SetChildIndex(Me.lblAnticipos, 0)
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMenos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcEnganche1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Primer_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcNumero_Documentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte_Documentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte_Ultimo_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEnganche_En_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dteFecha_Enganche_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte_Enganche_Documento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.clcMonto_Liquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIvadesglosado1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLiquida_Vencimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMenos_Enganch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEnganche_en_Menos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPagoInicial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImportePlan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPagoInicialDisplay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporteConDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkaplica_enganche.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFacturacionEspecial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcAnticipos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"

    Private Enum ValidacionesEdicion
        ValidaNumeroDocumentos = 1
        ValidaMontoLiquidaVencimiento = 2
    End Enum
    Private NumeroDocumentosPlan As Int32
    Private oVentas As New VillarrealBusiness.clsVentas
    Private oVentasDetalle As New VillarrealBusiness.clsVentasDetalle
    Private oPlanesCredito As New VillarrealBusiness.clsPlanesCredito
    Private oVariables As New VillarrealBusiness.clsVariables
    Private oMovimientosInventario As New VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventarioDetalle As New VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosCobrar As New VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As New VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oHistoricoCostos As New VillarrealBusiness.clsHisCostos
    Private oClientes As New VillarrealBusiness.clsClientes
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oCotizacionesArticulosPlanes As New VillarrealBusiness.clsCotizacionesArticulosPlanes
    '/* csequera 22 enero 2008
    Private oVistasEntradas As New VillarrealBusiness.clsVistasEntradas
    Private oVistasEntradasDetalle As New VillarrealBusiness.clsVistasEntradasDetalle
    Private oVistasSalidasDetalle As New VillarrealBusiness.clsVistasSalidasDetalle
    Private oMovimientosInventarioDetalleSeries As New VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oDescuentosEspecialesProgramados As New VillarrealBusiness.clsDescuentosEspecialesProgramados
    Private oArticulosExistencias As New VillarrealBusiness.clsArticulosExistencias

    Public oDescuentosEspecialesClientes As VillarrealBusiness.clsDescuentosEspecialesClientes
    Private oDescuentosEspecialesClientesDetalle As New VillarrealBusiness.clsDescuentosEspecialesClientesDetalle
    Private oPaquetesDetalle As New VillarrealBusiness.clsPaquetesDetalle
    Private oClientesConvenios As New VillarrealBusiness.clsClientesConvenios



    '/* csequera 22 enero 2008
    Private banCerrar As Boolean = False
    'DATOS DE LA VENTA
    Public odataVenta As DataSet
    Public odataArticulos As DataView
    Public persona As Char
    Public sucursal_dependencia As Boolean
    Public cotizacion As Double
    Public folio_plantilla As Long
    Public tipo_precio_venta As Long
    Public LimiteCreditoCliente As Double = 0
    '===============================================

    Private folio_movimiento As Long = 0
    Private folio_movimiento_detalle As Long = 0
    Private ConceptoVenta As String
    Private banConceptoVenta As Boolean = False
    Private ConceptoFactura As String
    Private banConceptoFactura As Boolean = False
    Private arrBodegas As New ArrayList
    Private arrFoliosMovimientos As New ArrayList
    Private precio_plan As Long = -1            'precio de venta configurado del plan
    Private precio_venta_sucursal As Long = -1  'precio de venta configurado de la sucursal 
    Private monto_precio_plan As Double = 0     'total con precio de venta del plan
    Private interes_plan As Double = 0          'interes del plan

    Private paso_enganche As Boolean = False
    Private paso_valor_documentos As Boolean = False
    Private paso_pago_inicial As Boolean = False
    Private paso_plan As Boolean = False
    Private paso_intereses As Boolean = False


    Private Plazo_Plan As Long
    Private paso_menos_calculado As Boolean = False

    Private paso_si_liquida_vencimiento As Boolean = False
    Private plan_fonacot As Boolean = False
    Private plan_paritaria As Boolean = False


    Private CobradorCredito As Long = 0
    Private TotalCostos As Double = 0
    Private Enganche As Double = 10 ' El porcentaje de enganche es 10%

    'Descuento Especial Cliente
    Public Plan_credito_descuento_especial_cliente As Long
    Public Porcentaje_Descuento_especial_cliente As Double
    Public FolioDescuento As Long

    ' Descuento Especial Programado
    'Private FolioDescuentoProgramado As Long
    Private Tiene_Descuento_Programado As Boolean = False
    Private FoliosDescuentosProgramados As DataTable


    ' manejador de precio expo
    Public Tiene_Precio_Expo As Boolean = False
    Public Precio_Expo As Long

    'Porcentajes de Menos para ambos grupos de articulos
    Private porcentaje_identificable As Double = 0
    Private porcentaje_no_identificable As Double = 0


    Private maneja_enajenacion As Boolean = True

    Private TotalCantidades As Long = 0

    Private SumaPreciosLista As Double = 0
    Private ImportePlan As Double = 0

    Public Paquete As Long = 0
    Public Plan_credito_Paquetes As Long = 0
    Private ConvenioCliente As Long = -1

    Private NumeroDocumentos As Long = 0
    Private IMPORTEDOCUMENTOS As Double = 0
    Private intereses As Double = 0
    Public SerieFactura As String = ""
    Public FacturaElectronica As Boolean = False

    Public ArticuloAnticipo As Long = 0
    Public TotalAnticipos As Double = 0

    Private porc_impuesto As Double = 0


    Private ReadOnly Property Porcentaje_Enganche() As Double
        Get
            Return Enganche / 100
        End Get
    End Property
    Private ReadOnly Property Plan() As Long
        Get
            Return PreparaValorLookup(Me.lkpPlan)
        End Get
    End Property
    Public WriteOnly Property Cobrador() As Long
        Set(ByVal Value As Long)
            CobradorCredito = Value
        End Set
    End Property
    Private ReadOnly Property ConvenioClienteLkp() As Long
        Get
            Return PreparaValorLookup(Me.lkpConveniosClientes)
        End Get
    End Property

#End Region

#Region "Transacciones"

    Private Sub frmVentasDatosCredito_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmVentasDatosCredito_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmVentasDatosCredito_EndUpdate() Handles MyBase.EndUpdate

        TinApp.Connection.Commit()

        If Action = Actions.Insert Then
            OwnerForm.banImprimirFac = True
            OwnerForm.lblFolio.text = odataVenta.Tables(0).Rows(0).Item("folio")
        End If

    End Sub

#End Region

#Region "Eventos de la Forma"

    Private Sub frmVentasDatosCredito_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed

        If banCerrar Then
            OwnerForm.Enabled = True
        End If

    End Sub
    Private Sub frmVentasDatosCredito_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        NumeroDocumentosPlan = 0
        ValidaUsuarioxEdicion(ValidacionesEdicion.ValidaMontoLiquidaVencimiento)


        CreaTablaDescuentosProgramados()

        Me.Text = "Datos de Cr�dito"

        Me.chkLiquida_Vencimiento.Enabled = False
        Me.chkEnganche_en_Menos.Visible = False




        'DAM 07/nov/09 se traera los porcentajes del menos de los 2 tipos de articulos
        'Me.porcentaje_identificable = CType(oVariables.TraeDatos("porcentaje_identificable", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)
        'Me.porcentaje_no_identificable = CType(oVariables.TraeDatos("porcentaje_no_identificable", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)
        'Me.maneja_enajenacion = CType(oVariables.TraeDatos("maneja_enajenacion", VillarrealBusiness.clsVariables.tipo_dato.Bit), Boolean)

        Select Case Action
            Case Actions.Insert
                'TraeConceptoVenta()
                'TraeConceptoFactura()

                Me.TraeDatosVariables()

                If Not banConceptoVenta Or Not banConceptoFactura Then
                    Me.tbrTools.Buttons.Item(0).Visible = False
                End If

                Me.clcMenos.EditValue = 0
                Me.clcIntereses.EditValue = 0
                Me.clcMonto_Liquida_Vencimiento.EditValue = 0
                Me.clcImporte_Documentos.EditValue = 0

        End Select

        Me.clcMenos.EditValue = OwnerForm.CalculaImporteTotalArticulosRegalo() 'CSEQUERA 04/01/2006
        Me.Top = 0
        Me.Left = 0
        Me.clcAnticipos.EditValue = TotalAnticipos
        Me.clcImporte.EditValue = OwnerForm.clcTotal.EditValue
        Me.clcImporte.EditValue = Me.clcImporte.EditValue - Me.clcMenos.EditValue
        Me.clcTotal.EditValue = Me.clcImporte.EditValue - Me.clcAnticipos.EditValue ' DAM 07/01/2013
        Me.dteFecha_Primer_Documento.DateTime = odataVenta.Tables(0).Rows(0).Item("fecha")

        RevisaFechaPrimerDocumento()


        Me.clcNumero_Documentos.EditValue = 1
        Me.chkIvadesglosado1.Checked = OwnerForm.chkIvadesglosado.Checked
        Me.dteFecha_Enganche_Documento.EditValue = System.DBNull.Value
        Me.dteFecha_Enganche_Documento.Enabled = False



        Me.clcEnganche1.Enabled = Not Me.sucursal_dependencia

        If Me.Paquete > 0 Then
            Me.lkpPlan.Enabled = False
            Me.lkpPlan.EditValue = Plan_credito_Paquetes

        End If

        If sucursal_dependencia Then
            Me.lblConvenio.Visible = True
            Me.lkpConveniosClientes.Visible = True

            Dim response As New Events
            Dim Cliente As Long = odataVenta.Tables(0).Rows(0).Item("cliente")

            response = Me.oClientesConvenios.Listado(Cliente)

            If Not response.ErrorFound Then

                Dim oDataSet As DataSet
                Dim Filas As Int32
                oDataSet = CType(response.Value, DataSet)
                Filas = oDataSet.Tables(0).Rows.Count
                If Filas > 0 Then


                    Me.lkpConveniosClientes.DataSource = oDataSet.Tables(0)

                    If Filas = 1 Then
                        ConvenioCliente = oDataSet.Tables(0).Rows(0).Item("convenio")
                        Me.lkpConveniosClientes.Enabled = False
                        Me.lkpConveniosClientes.EditValue = ConvenioCliente
                    Else

                        Me.lkpConveniosClientes.Enabled = True
                    End If
                End If

            End If
        End If
    End Sub
    Private Sub frmVentasDatosCredito_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

        Response = New Events

        If Me.chkIvadesglosado1.Checked = True Then Response = oClientes.ValidaRfc(odataVenta.Tables(0).Rows(0).Item("rfc"), persona)

        If Not Response.ErrorFound Then

            If (Me.clcImporte_Documentos.EditValue >= 100.0 Or sucursal_dependencia = True) Or Me.clcNumero_Documentos.EditValue = 1 Then

                If Me.clcImporte_Ultimo_Documento.EditValue <= 0 Then

                    Response.Ex = Nothing
                    Response.Message = "Revise el Importe de los documentos"
                    Response.Layer = Response.ErrorLayer.InterfaceLayer
                    Me.clcImporte_Documentos.Focus()

                Else

                    Dim cantidad As Double
                    cantidad = ((Me.clcNumero_Documentos.EditValue - 1) * Me.clcImporte_Documentos.EditValue) + Me.clcImporte_Ultimo_Documento.EditValue
                    If cantidad > Me.clcTotal.EditValue And sucursal_dependencia = False Then

                        Response.Ex = Nothing
                        Response.Message = "Revise el Importe de los documentos"
                        Response.Layer = Response.ErrorLayer.InterfaceLayer
                        Me.clcImporte_Documentos.Focus()

                    End If

                End If

            Else

                If plan_paritaria = False Then

                    Response.Ex = Nothing
                    Response.Message = "Revise el Importe de los documentos"
                    Response.Layer = Response.ErrorLayer.InterfaceLayer
                    clcImporte_Documentos.Focus()
                End If

            End If

        End If

        If Not Response.ErrorFound Then

            If Me.clcNumero_Documentos.EditValue > 1 And Me.clcImporte_Documentos.EditValue <= 0 Then

                Response.Ex = Nothing
                Response.Message = "El Importe de los documentos debe ser mayor a 0"
                Response.Layer = Response.ErrorLayer.InterfaceLayer
                Me.clcImporte_Documentos.Focus()

            End If

        End If

        If Not Response.ErrorFound Then

            If clcMonto_Liquida_Vencimiento.Value < 0 Then

                Response.Ex = Nothing
                Response.Message = "El Importe de liquida al vencimiento debe ser mayor a 0"
                Response.Layer = Response.ErrorLayer.InterfaceLayer

            End If

        End If

        If Not Response.ErrorFound Then Response = oVentas.ValidacionDatosCredito(Me.Action, Plan, Me.dteFecha_Primer_Documento.Text, Me.chkEnganche_En_Documento.Checked, Me.dteFecha_Enganche_Documento.Text)


        If Not Response.ErrorFound Then
            Response = ValidaPagosInicialDoctos()
        End If

        If Not Response.ErrorFound Then
            If Me.sucursal_dependencia And Me.ConvenioCliente < 1 Then
                Response.Ex = Nothing
                Response.Message = "El Convenio del cliente es Requerido"
                Response.Layer = Response.ErrorLayer.InterfaceLayer

            End If
        End If

        If Not Response.ErrorFound Then
            If Me.plan_fonacot = True Then
                Dim Subtotal_MenosEnganche As Double
                Dim porcentaje As Double

                Subtotal_MenosEnganche = (Me.clcPagoInicial.Value / clcImporte.Value)
                porcentaje = Subtotal_MenosEnganche * 100

                'DAM -  19-JUN-09 SE QUITO ESTA VALIDACION POR QUE AHORA SE PERMITIRA MAS PORCENTAJE DE PAGO 
                'INICIAL,  AUTORIZO EL CAMBIO SIDDHARTA
                'If porcentaje > 25.0 Then
                '    Response.Message = "El Pago Inicial es Mayor al Permitido para FONACOT"
                'End If

            End If


            If ValidaUsuarioxEdicion(ValidacionesEdicion.ValidaNumeroDocumentos, CType(Me.clcNumero_Documentos.EditValue, Int32)) = False Then
                Response.Message = "No se puede modificar el numero de documentos mayores al plazo"
                Response.Title = "Datos de Cr�dito"
                ValidacionDocumentos()

            End If
        End If

    End Sub
    Private Sub frmVentasDatosCredito_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Select Case Action

            Case Actions.Insert
                Response = GuardarVenta(Response)


        End Select

        If Not Response.ErrorFound Then

            banCerrar = True
            OwnerForm.banCerrarVenta = True
            OwnerForm.banCerrar = True
            OwnerForm.bVentaGuardada = True
            OwnerForm.CategoriasIdentificables = -2 ' Significa que limpiara la bandera para las categorias ya que la venta fue realizada

        Else

            OwnerForm.banCerrarVenta = False
            banCerrar = False

        End If

    End Sub
    Private Sub frmVentasDatosCredito_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        If Not banCerrar Then
            e.Cancel = True
        End If

    End Sub

#End Region

#Region "Eventos de Controles"

#Region "Lookup"

    Private Sub lkpPlan_LoadData(ByVal Initialize As Boolean) Handles lkpPlan.LoadData

        Dim Response As New Events
        Response = oPlanesCredito.Lookup(sucursal_dependencia, cotizacion, 1)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlan.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If

        Response = Nothing

    End Sub
    Private Sub lkpPlan_Format() Handles lkpPlan.Format
        Comunes.clsFormato.for_planes_credito_grl(Me.lkpPlan)
    End Sub
    Private Sub lkpPlan_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpPlan.EditValueChanged
        paso_plan = True

        If Plan <> -1 Then

            Me.paso_pago_inicial = False
            Me.paso_si_liquida_vencimiento = False

            Me.Plazo_Plan = Me.lkpPlan.GetValue("plazo")
            precio_plan = lkpPlan.GetValue("precio_venta")
            plan_fonacot = lkpPlan.GetValue("fonacot")
            interes_plan = lkpPlan.GetValue("interes")
            Me.Enganche = lkpPlan.GetValue("enganche")
            plan_paritaria = lkpPlan.GetValue("paritaria")
            NumeroDocumentosPlan = Me.lkpPlan.GetValue("plazo")

            Me.clcNumero_Documentos.EditValue = Me.lkpPlan.GetValue("plazo")



            precio_venta_sucursal = uti_SucursalPrecioVenta(Comunes.Common.Sucursal_Actual)

            If (precio_plan <> precio_venta_sucursal And plan_fonacot = False) Or Tiene_Descuento_Programado = True Or FolioDescuento > 0 Then

                Me.chkLiquida_Vencimiento.Checked = True
                Me.clcMonto_Liquida_Vencimiento.EditValue = Me.clcImporte_Documentos.EditValue

            Else

                Me.chkLiquida_Vencimiento.Checked = False
                Me.clcMonto_Liquida_Vencimiento.Value = 0

            End If

            Me.ObtenerDatosDescuentosPartidas()  '//

            CalculaLiquidaVencimiento()
            RecalculaImporteUltimoDocumento()



            Me.chkLiquida_Vencimiento.Checked = Not plan_fonacot
            'Me.clcMonto_Liquida_Vencimiento.Enabled = Not plan_fonacot

        Else

            Me.clcIntereses.EditValue = 0
            precio_plan = -1
            precio_venta_sucursal = -1
            Me.clcNumero_Documentos.EditValue = 1
            Me.clcImporte_Documentos.EditValue = 0
            monto_precio_plan = 0
            Me.chkLiquida_Vencimiento.Checked = False
            Me.clcMonto_Liquida_Vencimiento.EditValue = 0
            plan_fonacot = False = False
            plan_paritaria = False

            ImportePlan = 0 ' //

        End If




        CalcularMenosEnganche()

        'DAM - ES PARA ACTUALIZAR LAS ETIQUETAS DE DOCUMENTOS
        ActualizaEtiquetasDocumentos(Me.clcNumero_Documentos.EditValue, Me.clcImporte_Documentos.EditValue, Me.clcImporte_Ultimo_Documento.EditValue)


        Me.clcImporteConDescuento.Visible = Tiene_Descuento_Programado

        'Si por cuestiones de redondeo le falta un peso ... se le asigna al pago inicial
        'Dim suma_inicial_doctos As Double = (Me.clcPagoInicial.Value + (Me.clcNumero_Documentos.Value * Me.clcImporte_Documentos.Value))
        'If suma_inicial_doctos < Me.clcImportePlan.Value Then
        '    If (Me.clcImportePlan.Value - suma_inicial_doctos) = 1 Then
        '        Me.clcPagoInicial.Value = Me.clcPagoInicial.Value + 1
        '    End If
        'End If

        paso_plan = False

    End Sub

    Private Sub lkpConveniosClientes_Format() Handles lkpConveniosClientes.Format
        Comunes.clsFormato.for_convenios_ventas_grl(Me.lkpConveniosClientes)
    End Sub
    Private Sub lkpConveniosClientes_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConveniosClientes.EditValueChanged
        If ConvenioClienteLkp <> -1 Then
            ConvenioCliente = Me.lkpConveniosClientes.EditValue
        Else
            Me.ConvenioCliente = -1
        End If
    End Sub
#End Region

    Private Sub clcImporte_Documentos_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcImporte_Documentos.EditValueChanged

        If Me.clcImporte_Documentos.Text.Length = 0 Then
            Me.clcImporte_Documentos.EditValue = 0
        End If


        If clcImporte_Documentos.EditValue <> IMPORTEDOCUMENTOS Then
            paso_valor_documentos = True
            CalculaLiquidaVencimiento()
            RecalculaImporteUltimoDocumento()
            CalcularMenosEnganche()
            paso_valor_documentos = False

            IMPORTEDOCUMENTOS = clcImporte_Documentos.EditValue
        End If
    End Sub
    Private Sub clcNumero_Documentos_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcNumero_Documentos.EditValueChanged

        If Not clcNumero_Documentos.IsLoading Then
            If Me.clcNumero_Documentos.Text.Length = 0 Then
                Me.clcNumero_Documentos.EditValue = 1
            Else
                If IsNumeric(clcNumero_Documentos.EditValue) Then
                    If Me.clcNumero_Documentos.EditValue < 1 Then
                        Me.clcNumero_Documentos.EditValue = 1
                    End If
                    'Else
                    '    Me.clcNumero_Documentos.EditValue = 1
                End If

            End If

            If NumeroDocumentos <> Me.clcNumero_Documentos.EditValue Then

                'recalculo el importe de los documentos
                If Me.clcNumero_Documentos.EditValue = 1 Then

                    Me.clcImporte_Documentos.EditValue = 0
                    Me.clcImporte_Documentos.Enabled = False

                Else

                    Me.clcImporte_Documentos.Enabled = True

                End If

                '23-Dic-2013  DAM se ajusto para mejorar rendimiento
                If Not paso_plan Then
                    CalculaLiquidaVencimiento()
                    RecalculaImporteUltimoDocumento()
                End If
               

                Me.NumeroDocumentos = Me.clcNumero_Documentos.EditValue
                'DAM - ES PARA ACTUALIZAR LAS ETIQUETAS DE DOCUMENTOS
                ActualizaEtiquetasDocumentos(Me.clcNumero_Documentos.EditValue, Me.clcImporte_Documentos.EditValue, Me.clcImporte_Ultimo_Documento.EditValue)
            End If

        End If
    End Sub
    Private Sub clcIntereses_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcIntereses.EditValueChanged

        If Not CType(sender, Dipros.Editors.TINCalcEdit).IsLoading Then

            paso_intereses = True

            If Not IsNumeric(CType(sender, Dipros.Editors.TINCalcEdit).EditValue) Then
                clcIntereses.Value = 0
            End If

            If intereses <> clcIntereses.Value Then


                ' si tiene precio expo se recalcula el interes en los documentos, sino solo en el total
                'If Me.Tiene_Precio_Expo = True And Me.Precio_Expo = Me.precio_plan Then

                '23-Dic-2013  DAM se ajusto para mejorar rendimiento
                If Not paso_plan Then
                    CalculaLiquidaVencimiento()
                    RecalculaImporteUltimoDocumento()
                End If

                'Me.CalculaLiquidaVencimiento()
                'RecalculaImporteUltimoDocumento()
                ActualizaEtiquetasDocumentos(Me.clcNumero_Documentos.EditValue, Me.clcImporte_Documentos.EditValue, Me.clcImporte_Ultimo_Documento.EditValue)
                'Else
                '    RecalculaTotal()
                '    RecalculaImporteUltimoDocumento()
                'End If
                intereses = clcIntereses.Value

            End If

            paso_intereses = False

        End If

    End Sub

    Private Sub clcPagoInicial_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcPagoInicial.EditValueChanged
        If CType(sender, Dipros.Editors.TINCalcEdit).IsLoading = False Then
            ' If Me.clcPagoInicial.IsLoading = True Or Not IsNumeric(Me.clcPagoInicial.EditValue) Then Exit Sub
            If Not IsNumeric(CType(sender, Dipros.Editors.TINCalcEdit).EditValue) Then
                clcPagoInicial.Value = 0
            End If

            '23-Dic-2013  DAM se ajusto para mejorar rendimiento
            If Not paso_plan Then
                CalculaLiquidaVencimiento()
                RecalculaImporteUltimoDocumento()
            End If

            CalcularMenosEnganche()

            Me.clcPagoInicialDisplay.Value = Me.clcPagoInicial.Value
            ActualizaEtiquetasDocumentos(Me.clcNumero_Documentos.EditValue, Me.clcImporte_Documentos.EditValue, Me.clcImporte_Ultimo_Documento.EditValue)

        End If

    End Sub
    Private Sub clcMonto_Liquida_Vencimiento_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcMonto_Liquida_Vencimiento.EditValueChanged

        If Me.clcMonto_Liquida_Vencimiento.Text.Length = 0 Then

            Me.clcMonto_Liquida_Vencimiento.EditValue = 0

        End If

    End Sub
    Private Sub clcTotal_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcTotal.EditValueChanged
        RecalculaImporteUltimoDocumento()
    End Sub
    Private Sub clcNumero_Documentos_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcNumero_Documentos.Validated

        'Validar antes que cualquier cosa el numero de documentos
        If ValidaUsuarioxEdicion(ValidacionesEdicion.ValidaNumeroDocumentos, CType(Me.clcNumero_Documentos.EditValue, Int32)) = True Then
            If (Me.clcImporte_Documentos.EditValue >= 100.0 Or sucursal_dependencia = True) Or Me.clcNumero_Documentos.EditValue = 1 Then

                If Me.clcImporte_Ultimo_Documento.EditValue <= 0 Then

                    If Me.clcImporte_Ultimo_Documento.EditValue = 0 Then
                        ShowMessage(MessageType.MsgInformation, "El Importe del �ltimo documento debe ser mayor a 0", "Datos de Cr�dito", Nothing, False)
                    Else
                        ShowMessage(MessageType.MsgInformation, "Los Documentos exceden el Total a pagar", "Datos de Cr�dito", Nothing, False)
                    End If

                    Me.clcImporte_Documentos.Focus()

                Else

                    Dim cantidad As Double
                    cantidad = ((Me.clcNumero_Documentos.EditValue - 1) * Me.clcImporte_Documentos.EditValue) + Me.clcImporte_Ultimo_Documento.EditValue
                    If cantidad > Me.clcTotal.EditValue And sucursal_dependencia = False Then

                        ShowMessage(MessageType.MsgInformation, "Los Documentos exceden el Total a pagar", "Datos de Cr�dito", Nothing, False)
                        Me.clcNumero_Documentos.Focus()

                    End If

                End If

            Else

                If plan_paritaria = False Then
                    ShowMessage(MessageType.MsgInformation, "El Importe de los documentos debe ser minimo de $100.00", "Datos de Cr�dito", Nothing, False)
                    Me.clcImporte_Documentos.Focus()
                End If
            End If
        Else
            ShowMessage(MessageType.MsgInformation, "No se puede modificar el numero de documentos mayores al plazo", "Datos de Cr�dito", Nothing, False)
            ValidacionDocumentos()

        End If




    End Sub
    Private Sub clcImporte_Documentos_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcImporte_Documentos.Validated

        ' Si pulso salir ya no debe de pasar dos veces
        If Me.banCerrar Then Exit Sub



        If sucursal_dependencia Then

            Me.clcImporte_Documentos.EditValue = System.Math.Round(CType(Me.clcImporte_Documentos.EditValue, Double))

            'Else

            '    Me.clcImporte_Documentos.EditValue = CType(Me.clcImporte_Documentos.EditValue, Double)

        End If


        If (Me.clcImporte_Documentos.EditValue >= 100.0 Or sucursal_dependencia = True) Or Me.clcNumero_Documentos.EditValue = 1 Then

            If Me.clcImporte_Ultimo_Documento.EditValue <= 0 Then

                If Me.clcImporte_Ultimo_Documento.EditValue = 0 Then

                    ShowMessage(MessageType.MsgInformation, "El Importe del �ltimo documento debe ser mayor a 0", "Datos de Cr�dito", Nothing, False)
                Else
                    ShowMessage(MessageType.MsgInformation, "Los Documentos exceden el Total a pagar", "Datos de Cr�dito", Nothing, False)
                End If

            Else

                Dim cantidad As Double
                cantidad = ((Me.clcNumero_Documentos.EditValue - 1) * Me.clcImporte_Documentos.EditValue) + Me.clcImporte_Ultimo_Documento.EditValue
                If cantidad > Me.clcTotal.EditValue And sucursal_dependencia = False Then

                    ShowMessage(MessageType.MsgInformation, "Los Documentos exceden el Total a pagar", "Datos de Cr�dito", Nothing, False)

                End If
            End If
        Else

            If plan_paritaria = False Then
                ShowMessage(MessageType.MsgInformation, "El Importe de los documentos debe ser minimo de $100.00", "Datos de Cr�dito", Nothing, False)
            End If
        End If






    End Sub
    Private Sub clcMenos_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcMenos.Validated

        If (Me.clcImporte.EditValue - Me.clcMenos.EditValue) < CType(odataVenta.Tables(0).Rows(0).Item("totalminimo"), Double) Then

            ShowMessage(MessageType.MsgInformation, "El Menos sobrepasa el Total M�nimo", "Ventas", Nothing, False)
            Me.clcMenos.Focus()

        End If

    End Sub
    Private Sub chkEnganche_En_Documento_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEnganche_En_Documento.CheckedChanged

        If Me.chkEnganche_En_Documento.Checked = True Then

            Me.dteFecha_Enganche_Documento.Enabled = True
            If dteFecha_Enganche_Documento.EditValue Is System.DBNull.Value Then dteFecha_Enganche_Documento.DateTime = CDate(TinApp.FechaServidor).Date
            Me.clcEnganche1.EditValue = Me.clcPagoInicial.Value
            Me.clcImporte_Enganche_Documento.EditValue = Me.clcEnganche1.EditValue
            Me.clcMenos.Value = 0

        Else


            Me.dteFecha_Enganche_Documento.EditValue = System.DBNull.Value
            Me.clcImporte_Enganche_Documento.EditValue = 0
            Me.dteFecha_Enganche_Documento.Enabled = False

            If paso_menos_calculado = True Then
                Me.clcEnganche1.EditValue = 0
                Me.clcMenos.Value = Me.clcPagoInicial.Value
            End If
        End If

    End Sub
    Private Sub chkIvadesglosado1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIvadesglosado1.CheckedChanged

        OwnerForm.chkIvadesglosado.Checked = Me.chkIvadesglosado1.Checked
        odataVenta.Tables(0).Rows(0).Item("ivadesglosado") = Me.chkIvadesglosado1.Checked
        CalcularMenosEnganche()
    End Sub
    Private Sub chkLiquida_Vencimiento_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkLiquida_Vencimiento.CheckedChanged

        If Me.chkLiquida_Vencimiento.Checked = False Then
            Me.clcMonto_Liquida_Vencimiento.EditValue = 0
        End If

    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

        If e.Button.Text = "Salir" Then

            OwnerForm.banCerrarVenta = False
            Me.banCerrar = True
            Me.Close()

        End If

    End Sub
    Private Sub clcMonto_Liquida_Vencimiento_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles clcMonto_Liquida_Vencimiento.KeyPress

        paso_si_liquida_vencimiento = True

    End Sub
    Private Sub chkaplica_enganche_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkaplica_enganche.CheckedChanged
        CalcularMenosEnganche()
    End Sub

#End Region

#Region "Funcionalidad"
    Private Sub ValidacionDocumentos()

        Me.clcNumero_Documentos.Focus()
        Me.clcNumero_Documentos.EditValue = NumeroDocumentosPlan
    End Sub
    Private Function ValidaPrecioPlanArticulo(ByVal articulo As Long, ByVal precio As Double) As Boolean
        If precio = 0 Then
            ShowMessage(MessageType.MsgError, "El precio del articulo " + articulo.ToString() + " es 0", "Ventas", Nothing, False)
            Me.tbrTools.Buttons(0).Enabled = False
            ValidaPrecioPlanArticulo = True
        End If
    End Function
    Private Sub RecalculaTotal()

        'DAM 19 -OCT-07 SE MODIFICO PARA QUE AHORA TOME EL PAGO INICIA POR QUE AUN NO CATALOGA SI ES UN ENGANCHE
        ' O UN MENOS AL MOMENTO DE CALCULAR Y PINTAR LOS DATOS 
        Me.clcTotal.EditValue = Me.clcImporte.EditValue - Me.clcPagoInicial.Value + clcIntereses.EditValue - Me.clcAnticipos.EditValue   ' DAM 07/01/2013


    End Sub
    Private Sub RecalculaImporteUltimoDocumento()

        Me.clcImporte_Ultimo_Documento.EditValue = System.Math.Round(monto_precio_plan)

        'DAM - ES PARA ACTUALIZAR LAS ETIQUETAS DE DOCUMENTOS
        ActualizaEtiquetasDocumentos(Me.clcNumero_Documentos.EditValue, Me.clcImporte_Documentos.EditValue, Me.clcImporte_Ultimo_Documento.EditValue)

        If plan_fonacot = True Or (Me.Precio_Expo = Me.precio_plan) Then
            Me.clcMonto_Liquida_Vencimiento.EditValue = 0
        Else
            Dim importe_para_calculo As Double = IIf(Me.clcImporteConDescuento.Value > 0, Me.clcImporteConDescuento.Value, Me.clcImportePlan.Value)

            If precio_plan <> precio_venta_sucursal And paso_si_liquida_vencimiento = False Then
                If Me.clcNumero_Documentos.EditValue > 1 And Me.clcImporteConDescuento.Value = 0 Then
                    Me.clcMonto_Liquida_Vencimiento.EditValue = Me.clcImporte_Ultimo_Documento.EditValue - (SumaPreciosLista - importe_para_calculo)
                Else

                    'If Me.clcNumero_Documentos.EditValue = 1 Then
                    'Me.clcMonto_Liquida_Vencimiento.EditValue = importe_para_calculo - Me.clcPagoInicial.Value
                    'Else
                    Me.clcMonto_Liquida_Vencimiento.EditValue = Me.clcImporte_Ultimo_Documento.EditValue - (SumaPreciosLista - importe_para_calculo)
                    'End If

                End If
            End If

        End If

    End Sub
    Private Sub ObtenerDatosDescuentosPartidas()
        If Not FoliosDescuentosProgramados Is Nothing Then
            FoliosDescuentosProgramados.Clear()
        End If

        'recorrer todos los articulos con el precio del plan, y sacar la diferencia con el importe
        Dim i As Integer
        Dim importe_plan As Double = 0
        Dim articulo_grid As Long = 0
        Dim articulo_sobrepedido As Boolean
        Dim cantidad_articulo As Long = 0
        Dim partida As Long = 0
        Dim bodega As String = ""

        SumaPreciosLista = 0
        Me.tbrTools.Buttons(0).Enabled = True

        ' si hay articulos entra
        If Not odataArticulos Is Nothing Then
            'si tiene un plan entra
            If Plan > 0 Then
                For i = 0 To odataArticulos.Count - 1
                    articulo_grid = CType(odataArticulos.Item(i).Item("articulo"), Long)
                    articulo_sobrepedido = CType(odataArticulos.Item(i).Item("sobrepedido"), Long)
                    cantidad_articulo = CType(odataArticulos.Item(i).Item("cantidad"), Long)
                    partida = CType(odataArticulos.Item(i).Item("partida"), Long)
                    bodega = CType(odataArticulos.Item(i).Item("bodega"), String)  'DAM 

                    'DAM 19-OCT-07  SE METIO ESTA VALIDACION PARA QUE AL SUMAR SOLO TOME ARTICULOS VALIDOS
                    If odataArticulos(i).Item("control") <= 2 Then
                        Dim odataset As DataSet
                        Dim orow As DataRow

                        orow = Me.FoliosDescuentosProgramados.NewRow()
                        orow.Item("articulo") = articulo_grid
                        orow.Item("folio_descuento_especial_programado") = -1
                        orow.Item("precio_descuento_especial_programado") = 0
                        orow.Item("bodega") = bodega


                        'DAM 04-ENE-08  SE METIO ESTA VALIDACION PARA SABER DE DONDE TOMAR EL VALOR DE LOS PRECIOS 
                        ' SI TIENE COTIZACION 
                        If cotizacion > 0 Then

                            odataset = oCotizacionesArticulosPlanes.DespliegaDatos(cotizacion, articulo_grid, Me.Plan).Value
                            'DAM 31-Mar-12
                            odataArticulos.Item(i).Item("precio_pactado") = IIf(odataset.Tables(0).Rows.Count > 0, odataset.Tables(0).Rows(0).Item("importe_venta"), 0)

                            importe_plan = importe_plan * +IIf(odataset.Tables(0).Rows.Count > 0, odataset.Tables(0).Rows(0).Item("importe_venta") * cantidad_articulo, 0)
                        Else

                            Dim precio_normal_plan As Double = 0
                            Dim precio_lista_articulo As Double = 0

                            'Dim MINIMO As Long = 0
                            'Dim MAXIMO As Long = 100
                            'Randomize()
                            'Dim NUMERO As Long = CLng((MINIMO - MAXIMO) * Rnd() + MAXIMO)


                            precio_normal_plan = oArticulos.ArticuloPrecioVenta(articulo_grid, precio_plan, sucursal_dependencia, CDate(odataVenta.Tables(0).Rows(0).Item("fecha_pedido")), 0, True).Value
                            precio_lista_articulo = oArticulos.ArticuloPrecioVenta(articulo_grid, 0, sucursal_dependencia, CDate(odataVenta.Tables(0).Rows(0).Item("fecha_pedido")), 0).Value

                            If ValidaPrecioPlanArticulo(articulo_grid, precio_normal_plan) Then
                                Exit Sub
                            End If

                            'If NUMERO <= 10 And CType(odataVenta.Tables(0).Rows(0).Item("FECHA"), DateTime) < CDate("03-08-2010") Then
                            '    SumaPreciosLista = SumaPreciosLista + (precio_lista_articulo)
                            'Else
                            SumaPreciosLista = SumaPreciosLista + (precio_lista_articulo * cantidad_articulo)
                            'End If

                            If Me.Paquete > 0 Then
                                Dim response As Events
                                response = Me.oPaquetesDetalle.DespliegaDatos(Me.Paquete, partida)
                                odataset = CType(response.Value, DataSet)
                                If odataset.Tables(0).Rows.Count > 0 Then

                                    precio_lista_articulo = odataset.Tables(0).Rows(0).Item("precio_credito")

                                    'DAM 31-Mar-12
                                    odataArticulos.Item(i).Item("precio_pactado") = precio_lista_articulo

                                    importe_plan = importe_plan + (precio_lista_articulo * cantidad_articulo)

                                    If ValidaPrecioPlanArticulo(articulo_grid, precio_lista_articulo) Then
                                        Exit Sub
                                    End If
                                    orow.Item("precio_descuento_especial_programado") = precio_lista_articulo * cantidad_articulo

                                    Tiene_Descuento_Programado = True

                                End If
                            Else


                                If Me.FolioDescuento > 0 And Me.Plan = Me.Plan_credito_descuento_especial_cliente And Me.Porcentaje_Descuento_especial_cliente > 0 Then
                                    odataset = Me.oDescuentosEspecialesClientesDetalle.DespliegaDatos(Me.FolioDescuento, articulo_grid)
                                    If odataset.Tables(0).Rows.Count > 0 Then
                                        precio_lista_articulo = odataset.Tables(0).Rows(0).Item("precio_lista")
                                        'DAM 31-Mar-12
                                        odataArticulos.Item(i).Item("precio_pactado") = precio_lista_articulo

                                        importe_plan = importe_plan + (precio_lista_articulo * cantidad_articulo)

                                        If ValidaPrecioPlanArticulo(articulo_grid, precio_lista_articulo) Then
                                            Exit Sub
                                        End If
                                        ''Se modifico ya que ahora se puede poner cantidades en los descuentos especiales.
                                        orow.Item("precio_descuento_especial_programado") = (precio_lista_articulo - ((Me.Porcentaje_Descuento_especial_cliente / 100) * precio_lista_articulo)) * cantidad_articulo


                                        Tiene_Descuento_Programado = True
                                    End If

                                Else
                                    odataset = Me.oDescuentosEspecialesProgramados.DespliegaDatosVentas(articulo_grid, Me.Plan, Me.odataVenta.Tables(0).Rows(0).Item("fecha"))

                                    If odataset.Tables(0).Rows.Count > 0 Then
                                        precio_lista_articulo = odataset.Tables(0).Rows(0).Item("precio_lista")

                                        If ValidaPrecioPlanArticulo(articulo_grid, precio_lista_articulo) Then
                                            Exit Sub
                                        End If
                                        'DAM
                                        ' Se guarda en una Variable El Valor del folio del Descuento Programado
                                        orow.Item("folio_descuento_especial_programado") = odataset.Tables(0).Rows(0).Item("folio_descuento")

                                        If odataset.Tables(0).Rows(0).Item("aplica_existencia") = True And articulo_sobrepedido = False Then
                                            'DAM 31-Mar-12
                                            odataArticulos.Item(i).Item("precio_pactado") = precio_lista_articulo

                                            importe_plan = importe_plan + (precio_lista_articulo * cantidad_articulo) '- ((odataset.Tables(0).Rows(0).Item("porcentaje_descuento") / 100) * precio_lista_articulo)
                                            'orow.Item("precio_descuento_especial_programado") = (precio_lista_articulo * cantidad_articulo) - ((odataset.Tables(0).Rows(0).Item("porcentaje_descuento") / 100) * precio_lista_articulo * cantidad_articulo)
                                            orow.Item("precio_descuento_especial_programado") = odataset.Tables(0).Rows(0).Item("precio_credito") * cantidad_articulo

                                            Tiene_Descuento_Programado = True
                                        Else
                                            If odataset.Tables(0).Rows(0).Item("aplica_pedido_fabrica") = True Then
                                                'DAM 31-Mar-12
                                                odataArticulos.Item(i).Item("precio_pactado") = precio_lista_articulo

                                                importe_plan = importe_plan + (precio_lista_articulo * cantidad_articulo) '- ((odataset.Tables(0).Rows(0).Item("porcentaje_descuento") / 100) * precio_lista_articulo)
                                                'orow.Item("precio_descuento_especial_programado") = (precio_lista_articulo * cantidad_articulo) - ((odataset.Tables(0).Rows(0).Item("porcentaje_descuento") / 100) * precio_lista_articulo * cantidad_articulo)
                                                orow.Item("precio_descuento_especial_programado") = odataset.Tables(0).Rows(0).Item("precio_credito") * cantidad_articulo
                                                Tiene_Descuento_Programado = True
                                            Else

                                                'DAM 31-Mar-12
                                                odataArticulos.Item(i).Item("precio_pactado") = precio_normal_plan

                                                importe_plan = importe_plan + (precio_normal_plan * cantidad_articulo)
                                                orow.Item("precio_descuento_especial_programado") = precio_normal_plan
                                            End If
                                        End If
                                    Else
                                        'DAM 31-Mar-12
                                        odataArticulos.Item(i).Item("precio_pactado") = precio_normal_plan

                                        importe_plan = importe_plan + (precio_normal_plan * cantidad_articulo)

                                        If plan_fonacot = True Then

                                            If ValidaPrecioPlanArticulo(articulo_grid, precio_normal_plan) Then
                                                Exit Sub
                                            End If

                                            orow.Item("precio_descuento_especial_programado") = precio_normal_plan * cantidad_articulo
                                        Else
                                            If orow.Item("folio_descuento_especial_programado") > 0 Then
                                                orow.Item("precio_descuento_especial_programado") = precio_lista_articulo * cantidad_articulo
                                            Else
                                                orow.Item("precio_descuento_especial_programado") = precio_normal_plan * cantidad_articulo
                                            End If


                                        End If

                                    End If ' If odataset.Tables(0).Rows.Count > 0
                                End If  ' if Me.FolioDescuento > 0 And Me.Plan = Me.Plan_credito_descuento_especial_cliente

                            End If ' If Paquete > 0 Then
                        End If ' If cotizacion > 0 Then

                        Me.FoliosDescuentosProgramados.Rows.Add(orow)

                    End If '  If odataArticulos(i).Item("control") <= 2 

                Next
            End If ' If Plan > 0 Then
        End If


        ImportePlan = importe_plan

    End Sub
    Private Sub CalculaLiquidaVencimiento()

        Dim precio_para_calculo As Double

        Me.clcImporteConDescuento.Value = 0
        If Tiene_Descuento_Programado = True Then

            Dim row As DataRow
            For Each row In Me.FoliosDescuentosProgramados.Rows()
                Me.clcImporteConDescuento.Value = Me.clcImporteConDescuento.Value + row("precio_descuento_especial_programado")
            Next

            Me.clcImporteConDescuento.Value = System.Math.Round(Me.clcImporteConDescuento.Value)
            precio_para_calculo = Me.clcImporteConDescuento.Value
        Else
            precio_para_calculo = ImportePlan
        End If

        If paso_pago_inicial = False Then

            If Me.sucursal_dependencia = True Then
                Me.clcPagoInicial.EditValue = 0
            Else
                'DAM 17-OCT-07
                Me.clcPagoInicial.EditValue = System.Math.Round(precio_para_calculo * Porcentaje_Enganche)
                If Me.clcPagoInicial.EditValue > 0 Then
                    paso_pago_inicial = True
                End If

            End If

        End If

        ' ==================================================================

        If paso_intereses = False Then
            If interes_plan > 0 Then
                Me.clcIntereses.EditValue = Math.Round((precio_para_calculo - Me.clcPagoInicial.Value) * (interes_plan / 100))
            Else
                Me.clcIntereses.EditValue = 0
            End If
        End If
        ' ==================================================================


        If paso_valor_documentos = False Then
            ' CALCULO DE IMPORTE DE DOCUMENTOS
            '----------------------------------------------------
            ' 31-MAR-2012  AHORA SIEMPRE LOS INTERESES SE PRORRATEAN EN LOS DOCUMENTOS
            'If plan_fonacot = True Then
            '    Me.clcImporte_Documentos.EditValue = (precio_para_calculo - Me.clcPagoInicial.Value) / Me.clcNumero_Documentos.EditValue
            'Else
            If Me.clcNumero_Documentos.EditValue = 1 Then
                If Me.Tiene_Precio_Expo = True And Me.Precio_Expo = Me.precio_plan Then
                    Me.clcImporte_Documentos.EditValue = 0
                Else
                    Me.clcImporte_Documentos.EditValue = clcImporte.Value - Me.clcPagoInicial.Value - Me.clcAnticipos.EditValue + Me.clcIntereses.Value
                End If

            Else
                If Me.Tiene_Precio_Expo = True And Me.Precio_Expo = Me.precio_plan Then
                    'DAM 21 JUL 07 , AHORA YA NO SE UTILIZA EL ENGANCHE SINO EL PAGO INICIAL
                    Me.clcImporte_Documentos.EditValue = ((precio_para_calculo - Me.clcPagoInicial.Value - Me.clcAnticipos.EditValue) + Me.clcIntereses.Value) / Me.clcNumero_Documentos.EditValue
                Else
                    'DAM 21 JUL 07 , AHORA YA NO SE UTILIZA EL ENGANCHE SINO EL PAGO INICIAL
                    'DAM 17 JUL 10,  AHORA SE PREGUNTARA VERIFICA SI HAY INTERESES PARA INCLUIRLOS EN LOS DOCUMENTOS
                    If Me.clcIntereses.Value = 0 Then
                        Me.clcImporte_Documentos.EditValue = (precio_para_calculo - Me.clcPagoInicial.Value - Me.clcAnticipos.EditValue) / Me.clcNumero_Documentos.EditValue
                    Else
                        Me.clcImporte_Documentos.EditValue = ((precio_para_calculo - Me.clcPagoInicial.Value - Me.clcAnticipos.EditValue) + Me.clcIntereses.Value) / Me.clcNumero_Documentos.EditValue
                    End If

                End If

            End If
            'End If
            Me.clcImporte_Documentos.EditValue = System.Math.Round(Me.clcImporte_Documentos.EditValue)
        End If


        monto_precio_plan = Me.clcTotal.Value - (Me.clcImporte_Documentos.EditValue * (Me.clcNumero_Documentos.Value - 1))

        monto_precio_plan = System.Math.Round(monto_precio_plan)
        clcImportePlan.EditValue = System.Math.Round(ImportePlan)

        If plan_fonacot = True Or (Me.Tiene_Precio_Expo = True And Me.Precio_Expo = Me.precio_plan) Then
            clcImporte.Value = precio_para_calculo
        Else
            If Not Me.OwnerForm Is Nothing Then
                clcImporte.Value = OwnerForm.clcTotal.EditValue
            End If
        End If

    End Sub
    'Private Sub TraeConceptoVenta()

    '    ConceptoVenta = CType(oVariables.TraeDatos("concepto_venta", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoVenta <> "" Then

    '        banConceptoVenta = True

    '    Else

    '        ShowMessage(MessageType.MsgInformation, "El Concepto de Venta no esta definido", "Variables del Sistema", Nothing, False)

    '    End If

    'End Sub
    'Private Sub TraeConceptoFactura()

    '    ConceptoFactura = CType(oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
    '    If ConceptoFactura <> "" Then

    '        banConceptoFactura = True

    '    Else

    '        ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Cr�dito no esta definido", "Variables del Sistema", Nothing, False)

    '    End If

    'End Sub
    Private Sub GeneraListaBodegas()

        Dim i As Integer
        For i = 0 To Me.odataArticulos.Count - 1

            If arrBodegas.Contains(Me.odataArticulos.Table.Rows(i).Item("bodega")) = False Then

                arrBodegas.Add(Me.odataArticulos.Table.Rows(i).Item("bodega"))

            End If

        Next

        arrBodegas.Sort()

    End Sub
    Private Sub ActualizaEtiquetasDocumentos(ByVal meses As Long, ByVal Importedocumentos As Double, ByVal importedocumentofinal As Double)

        Dim documentos_iguales As Long
        documentos_iguales = meses - 1


        If meses > 1 Then
            Me.lblDocumentosIguales.Text = documentos_iguales.ToString
            lblLeyendaDocumentos.Text = "Documentos de $ " + System.Math.Round(Importedocumentos).ToString + " = "
            Me.lblTotalDocumentos.Text = "$ " + System.Math.Round(documentos_iguales * Importedocumentos).ToString
            Me.lblDocumentoFinal.Text = "1"
            Me.lblLeyendaDocumentoFinal.Text = "Documento de $ " + importedocumentofinal.ToString + " = "
            lblImporteUltimoDocumento.Text = "$ " + importedocumentofinal.ToString
            Me.lblDocumentosUltimoDocumento.Text = "$ " + (System.Math.Round(documentos_iguales * Importedocumentos) + importedocumentofinal).ToString
        Else
            Me.lblDocumentosIguales.Text = meses.ToString

            lblLeyendaDocumentos.Text = "Documentos de $ " + importedocumentofinal.ToString + " = "
            Me.lblTotalDocumentos.Text = "$ " + importedocumentofinal.ToString
            Me.lblDocumentoFinal.Text = "0"
            Me.lblLeyendaDocumentoFinal.Text = "Documento de $ 0 = "
            lblImporteUltimoDocumento.Text = "$ 0"
            Me.lblDocumentosUltimoDocumento.Text = "$ 0"

        End If
    End Sub
    Private Function GuardarVenta(ByRef response As Events) As Events


        Dim fecha_enganche As Date
        Dim fecha_1doc As Date
        If Me.dteFecha_Enganche_Documento.EditValue Is System.DBNull.Value Then

            fecha_enganche = CDate("01/01/1900")

        Else

            fecha_enganche = Me.dteFecha_Enganche_Documento.DateTime.Date

        End If

        If Me.dteFecha_Primer_Documento.EditValue Is System.DBNull.Value Then

            fecha_1doc = CDate("01/01/1900")

        Else

            fecha_1doc = Me.dteFecha_Primer_Documento.DateTime.Date

        End If

        Dim total_fac As Double
        total_fac = Me.clcTotal.EditValue + Me.clcEnganche1.EditValue
        'Obtengo Subtotal e Impuesto de la Venta a credito
        Dim subtotal As Double
        'Dim impuesto As Double
        'Dim porc_impuesto As Double
        'porc_impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)

        If porc_impuesto <> -1 Then

            porc_impuesto = 1 + (porc_impuesto / 100)
            'impuesto = total_fac * (porc_impuesto / 100)

        Else

            'ShowMessage(MessageType.MsgInformation, "El Porcentaje de Impuesto no esta definido", "Variables del Sistema", Nothing, False)
            response.Message = "El Porcentaje de Impuesto no esta definido"
        End If


        odataVenta.Tables(0).Rows(0).Item("folio") = CType(OwnerForm, frmVentas).TraeFolioPuntoVenta(Me.FacturaElectronica)

        subtotal = System.Math.Round(total_fac / porc_impuesto, 2)
        odataVenta.Tables(0).Rows(0).Item("subtotal") = subtotal
        odataVenta.Tables(0).Rows(0).Item("impuesto") = total_fac - subtotal
        odataVenta.Tables(0).Rows(0).Item("total") = total_fac
        Dim quincena_inicio As Long = -1
        Dim ano_inicio As Long = -1

        'response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        'If Not response.ErrorFound Then
        '    Dim odataset As DataSet
        '    odataset = CType(response.Value, DataSet)
            'quincena_inicio = CType(odataset.Tables(0).Rows(0).Item("quincena_envio"), Long)
            'ano_inicio = CType(odataset.Tables(0).Rows(0).Item("anio_envio"), Long)
        'End If






        '/* csequera 22  enero 2008
        ' ==============================================================================
        ' CODIGO PARA LA VENTA CON SALIDA A VISTA 
        ' ==============================================================================

        'Dim folio_movimiento_detalle_consecutivo As Long

        ''SI ES VENTA DE VISTA:GENERA UN MOVIMIENTO DE ENTRADA POR VISTA PARA CADA ART�CULO
        ''If Me.FolioVistaSalida > 0 Then
        'If CType(Me.OwnerForm(), frmVentas).FolioVistaSalida > 0 Then
        '    'If Me.tmaVentas.Count > 0 Then
        '    If CType(Me.OwnerForm(), frmVentas).tmaVentas.Count > 0 Then
        '        Dim oEvent As Events
        '        Dim FolioVistaEntrada As Integer = 0
        '        '                oEvent = oVistasEntradas.Insertar(FolioVistaEntrada, Me.FolioVistaSalida, Comunes.Common.Sucursal_Actual, Me.dteFecha.EditValue, Cliente, "VENTAS", TinApp.Connection.User, "Entrada Generada Por Punto Venta")
        '        oEvent = oVistasEntradas.Insertar(FolioVistaEntrada, CType(Me.OwnerForm(), frmVentas).FolioVistaSalida, Comunes.Common.Sucursal_Actual, CType(Me.OwnerForm(), frmVentas).dteFecha.EditValue, CType(Me.OwnerForm(), frmVentas).Cliente, "VENTAS", TinApp.Connection.User, "Entrada Generada Por Punto Venta")
        '        If oEvent.ErrorFound Then
        '            response = oEvent
        '        Else
        '            'With Me.tmaVentas
        '            With CType(Me.OwnerForm(), frmVentas).tmaVentas
        '                .MoveFirst()
        '                Do While Not .EOF
        '                    If .Item("partida_vistas") > 0 Then

        '                        Select Case .CurrentAction
        '                            Case Actions.Insert

        '                                'DAM 27/dic/07 Primero Valido Existencias y genero los Movimientos al Inventario
        '                                'response = oMovimientosInventarioDetalle.ValidacantidadArticulos(.Item("partida_vistas"), .Item("articulo"), 1, .Item("bodega"), ConceptoVistaEntrada, folio_movimiento, "I", "Entrada Vistas")
        '                                response = oMovimientosInventarioDetalle.ValidacantidadArticulos(.Item("partida_vistas"), .Item("articulo"), 1, .Item("bodega"), CType(Me.OwnerForm(), frmVentas).ConceptoVistaEntrada, folio_movimiento, "I", "Entrada Vistas")
        '                                If Not response.ErrorFound Then
        '                                    '    'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
        '                                    'GeneraMovimientoInventario(Action, response, Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistaEntrada, folio_movimiento, Me.dteFecha.DateTime, Sucursal_vista_salida, ConceptoVistaSalida, Me.FolioVistaSalida, "Movimiento generado desde Entrada a Vistas por Punto de Venta")
        '                                    GeneraMovimientoInventario(Action, response, Comunes.Common.Sucursal_Actual, .Item("bodega"), CType(Me.OwnerForm(), frmVentas).ConceptoVistaEntrada, folio_movimiento, CType(Me.OwnerForm(), frmVentas).dteFecha.EditValue, CType(Me.OwnerForm(), frmVentas).Sucursal_vista_salida, CType(Me.OwnerForm(), frmVentas).ConceptoVistaSalida, CType(Me.OwnerForm(), frmVentas).FolioVistaSalida, "Movimiento generado desde Entrada a Vistas por Punto de Venta")
        '                                End If
        '                                'If Not response.ErrorFound Then response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistaEntrada, folio_movimiento, dteFecha.EditValue, .Item("partida_vistas"), .Item("articulo"), 1, .Item("costo"), 1 * .Item("costo"), -1, folio_movimiento_detalle_consecutivo)
        '                                If Not response.ErrorFound Then response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), CType(Me.OwnerForm(), frmVentas).ConceptoVistaEntrada, folio_movimiento, CType(Me.OwnerForm(), frmVentas).dteFecha.EditValue, .Item("partida"), .Item("articulo"), 1, .Item("costo"), 1 * .Item("costo"), -1, folio_movimiento_detalle_consecutivo)

        '                                If .Item("numero_serie") <> Nothing Then
        '                                    If CType(.Item("numero_serie"), String).Trim.Length > 0 Then


        '                                        'If maneja_series Then
        '                                        'AQUI SE CAMBIA LA SERIE DE BODEGA EN EL HIS_SERIES
        '                                        'If Not response.ErrorFound Then response = oMovimientosInventarioDetalleSeries.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), Me.ConceptoVistaEntrada, FolioVistaEntrada, .Item("partida_vistas"), .Item("numero_serie"), .Item("articulo"))
        '                                        If Not response.ErrorFound Then response = oMovimientosInventarioDetalleSeries.Insertar(Comunes.Common.Sucursal_Actual, .Item("bodega"), CType(Me.OwnerForm(), frmVentas).ConceptoVistaEntrada, folio_movimiento, .Item("partida_vistas"), .Item("numero_serie"), .Item("articulo")) ''ojo parametro folio_movimiento

        '                                        ' SE MANDA LA SERIE A LA BODEGA DE ENTRADA
        '                                        If Not response.ErrorFound Then response = oMovimientosInventarioDetalleSeries.CambiarBodegaHisSeries(.Item("articulo"), .Item("numero_serie"), .Item("bodega"))
        '                                    End If
        '                                End If
        '                                If Not response.ErrorFound Then
        '                                    'response = Me.oVistasEntradasDetalle.Insertar(FolioVistaEntrada, .Item("partida_vistas"), .Item("articulo"), .Item("numero_serie"), .Item("bodega"), Me.ConceptoVistaEntrada, folio_movimiento)
        '                                    response = Me.oVistasEntradasDetalle.Insertar(FolioVistaEntrada, .Item("partida_vistas"), .Item("articulo"), .Item("numero_serie"), .Item("bodega"), CType(Me.OwnerForm(), frmVentas).ConceptoVistaEntrada, folio_movimiento)
        '                                End If

        '                        End Select
        '                    End If
        '                    .MoveNext()

        '                Loop
        '            End With
        '        End If
        '    End If
        'End If

        ' ==============================================================================
        ' FIN DE CODIGO PARA LA VENTA CON SALIDA A VISTA 
        ' ==============================================================================
        '*/ csequera 22  enero 2008

        Dim bEsEnajenacion As Boolean = EsEnajenacion()

        If bEsEnajenacion = True Or Me.chkIvadesglosado1.Checked = False Then odataVenta.Tables(0).Rows(0).Item("rfc") = "XAXX010101000"

        'GUARDA LA VENTA

        If Not response.ErrorFound Then '/* csequera 22  enero 2008
            response = oVentas.Insertar(odataVenta, Comunes.Common.Sucursal_Actual, SerieFactura, odataVenta.Tables(0).Rows(0).Item("fecha"), Comunes.Common.PuntoVenta_Actual, Me.clcImporte.EditValue, Me.clcMenos.EditValue, Me.clcEnganche1.EditValue, _
                    Me.clcIntereses.EditValue, Plan, fecha_1doc, Me.clcNumero_Documentos.EditValue, Me.clcImporte_Documentos.EditValue, Me.clcImporte_Ultimo_Documento.EditValue, Me.chkEnganche_En_Documento.Checked, _
                    fecha_enganche, Me.clcImporte_Enganche_Documento.EditValue, Me.chkLiquida_Vencimiento.Checked, Me.clcMonto_Liquida_Vencimiento.EditValue, Me.chkEnganche_en_Menos.Checked, sucursal_dependencia, folio_plantilla, quincena_inicio, ano_inicio, , FolioDescuento, Me.chkaplica_enganche.Checked, Me.Paquete, ConvenioCliente)
        End If '*/ csequera 22  enero 2008


        'GUARDA LA CXC
        If Not response.ErrorFound Then response = oMovimientosCobrar.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFactura, SerieFactura, odataVenta.Tables(0).Rows(0).Item("folio"), odataVenta.Tables(0).Rows(0).Item("cliente"), 0, odataVenta.Tables(0).Rows(0).Item("fecha"), -1, -1, Me.CobradorCredito, Me.clcNumero_Documentos.EditValue, odataVenta.Tables(0).Rows(0).Item("total"), 0, odataVenta.Tables(0).Rows(0).Item("subtotal"), odataVenta.Tables(0).Rows(0).Item("impuesto"), odataVenta.Tables(0).Rows(0).Item("total"), odataVenta.Tables(0).Rows(0).Item("total"), CDate("01/01/1900").Date, "", "", False, ConvenioCliente)

        'DAM 17-05-2006
        Dim observaciones As String
        observaciones = "Nota de Venta " + SerieFactura + "-" + CType(odataVenta.Tables(0).Rows(0).Item("folio"), String)

        If Not response.ErrorFound Then
            response = CType(OwnerForm, frmVentas).GuardaAnticipos(Comunes.Common.Sucursal_Actual, SerieFactura, CType(odataVenta.Tables(0).Rows(0).Item("folio"), Long))
        End If

        'GUARDA DOCUMENTOS DE CXC
        If Not response.ErrorFound Then response = GuardaDocumentos(response, observaciones, quincena_inicio, ano_inicio)

        'GUARDA ENCABEZADOS DE LOS MOVIMIENTOS
        If Not response.ErrorFound Then

            GeneraListaBodegas()
            Dim i As Integer = 0
            For i = 0 To arrBodegas.Count - 1

                GeneraMovimientoInventario(response, folio_movimiento, ConceptoVenta, arrBodegas(i))
                arrFoliosMovimientos.Add(folio_movimiento)
                folio_movimiento = 0

            Next

        End If

        'GUARDA DETALLES DE VENTAS Y DE MOVIMIENTOS
        If Not response.ErrorFound Then response = GuardaArticulos(response)

        'REVISA SI ES ENAJENACION
        If bEsEnajenacion And Not response.ErrorFound Then

            Dim factor_enajenacion As Double = 0
            Dim TotalVenta As Double
            ' Dim porcentaje_iva As Double


            ' porcentaje_iva = 1 + (porc_impuesto / 100)


            'calcula factor de enajenacion

            TotalVenta = Me.clcImporte.Value + Me.clcIntereses.Value
            factor_enajenacion = TotalCostos / (TotalVenta / porc_impuesto)  ' se le quita el iva a el total de la venta

            response = oVentas.EsEnajenacion(Comunes.Common.Sucursal_Actual, SerieFactura, CType(odataVenta.Tables(0).Rows(0).Item("folio"), Long), factor_enajenacion, ConceptoFactura, porc_impuesto)

        End If

        If Not response.ErrorFound Then
            response = CType(OwnerForm, frmVentas).ActualizaCotizacion(Me.cotizacion, Comunes.Common.Sucursal_Actual, SerieFactura, odataVenta.Tables(0).Rows(0).Item("folio"))
        End If


        If Not response.ErrorFound Then
            response = oDescuentosEspecialesClientes.ActualizarVentas(Me.FolioDescuento, True)
        End If


        If Not response.ErrorFound Then
            response = oClientes.RecalcularSaldosClientes(odataVenta.Tables(0).Rows(0).Item("cliente"))
        End If

        If Not response.ErrorFound And TotalAnticipos > 0 Then

            TotalAnticipos = TotalAnticipos * -1
            CType(Me.OwnerForm, frmVentas).InsertarArticuloAnticipo(Comunes.Common.Sucursal_Actual, SerieFactura, odataVenta.Tables(0).Rows(0).Item("folio"), odataArticulos.Table.Rows.Count + 1, ArticuloAnticipo, TotalAnticipos, TotalAnticipos, TotalAnticipos, 0, 1, Me.FolioDescuento, TotalAnticipos)
        End If

        Return response

    End Function
    Private Function GeneraMovimientoInventario(ByRef response As Events, ByRef folio As Long, ByVal concepto As String, ByVal bodega As String)

        response = oMovimientosInventario.Insertar(Comunes.Common.Sucursal_Actual, bodega, concepto, folio, _
                    CType(odataVenta.Tables(0).Rows(0).Item("fecha"), DateTime), Comunes.Common.Sucursal_Actual, SerieFactura, CType(odataVenta.Tables(0).Rows(0).Item("folio"), Long), "Movimiento generado desde el proceso de Ventas")

    End Function
    Private Function GuardaArticulos(ByVal response As Events) As Events

        Dim folio_mvto As Long
        Dim i As Integer
        Dim j As Integer
        Dim odataFila As DataSet
        Dim odatarow As DataRow
        Dim Costo_Detalle As Double = 0

        With Me.odataArticulos

            For i = 0 To .Table.Rows.Count - 1

                For j = 0 To arrBodegas.Count - 1

                    If arrBodegas(j) = .Table.Rows(i).Item("bodega") Then

                        folio_mvto = arrFoliosMovimientos(j)
                        Exit For

                    End If

                Next

                If odataArticulos(i).Item("control") <= 2 Then

                    odataFila = odataArticulos.Table.DataSet.Copy
                    odataFila.Tables(0).Rows.Clear()
                    odatarow = odataFila.Tables(0).NewRow()
                    odataFila.Tables(0).Rows.Add(odatarow)
                    odataFila.Tables(0).Rows(0).Item("partida") = .Table.Rows(i).Item("partida")
                    odataFila.Tables(0).Rows(0).Item("articulo") = .Table.Rows(i).Item("articulo")
                    odataFila.Tables(0).Rows(0).Item("n_articulo") = .Table.Rows(i).Item("n_articulo")
                    odataFila.Tables(0).Rows(0).Item("cantidad") = .Table.Rows(i).Item("cantidad")
                    odataFila.Tables(0).Rows(0).Item("preciounitario") = .Table.Rows(i).Item("preciounitario")
                    odataFila.Tables(0).Rows(0).Item("total") = .Table.Rows(i).Item("total")
                    odataFila.Tables(0).Rows(0).Item("sobrepedido") = .Table.Rows(i).Item("sobrepedido")
                    odataFila.Tables(0).Rows(0).Item("costo") = .Table.Rows(i).Item("costo")
                    odataFila.Tables(0).Rows(0).Item("surtido") = .Table.Rows(i).Item("surtido")
                    odataFila.Tables(0).Rows(0).Item("reparto") = .Table.Rows(i).Item("reparto")
                    odataFila.Tables(0).Rows(0).Item("bodega") = .Table.Rows(i).Item("bodega")
                    odataFila.Tables(0).Rows(0).Item("n_bodega") = .Table.Rows(i).Item("n_bodega")
                    odataFila.Tables(0).Rows(0).Item("precio_minimo") = .Table.Rows(i).Item("precio_minimo")
                    odataFila.Tables(0).Rows(0).Item("folio_historico_costo") = IIf(.Table.Rows(i).Item("folio_historico_costo") Is System.DBNull.Value, 0, .Table.Rows(i).Item("folio_historico_costo"))
                    odataFila.Tables(0).Rows(0).Item("precio_contado") = .Table.Rows(i).Item("precio_contado")
                    odataFila.Tables(0).Rows(0).Item("precio_pactado") = .Table.Rows(i).Item("precio_pactado")


                    ' Ajuste por precio promocion en EXPO  13 Marzo 2014

                    If Me.Tiene_Precio_Expo = True And Me.Precio_Expo = Me.precio_plan Then
                        odataFila.Tables(0).Rows(0).Item("precio_contado") = odataFila.Tables(0).Rows(0).Item("precio_pactado")
                    End If

                    '/* csequera 22 enero 2008
                    odataFila.Tables(0).Rows(0).Item("partida_vistas") = .Table.Rows(i).Item("partida_vistas")
                    '*/ csequera 22 enero 2008

                    odataFila.Tables(0).Rows(0).Item("descripcion_especial") = .Table.Rows(i).Item("descripcion_especial")


                    Dim folio_desc_programado As Long
                    Dim precio_desc_programado As Double

                    If Me.clcIntereses.Value > 0 Then
                        Dim interes As Double = Me.clcIntereses.Value


                        'ES PARA SABER CUANTOS ARTICULOS TIENE LA VENTA
                        If odataArticulos.Table.Rows.Count = 1 Then
                            odataFila.Tables(0).Rows(0).Item("preciounitario") = odataFila.Tables(0).Rows(0).Item("preciounitario") + (interes / odataFila.Tables(0).Rows(0).Item("cantidad"))
                            odataFila.Tables(0).Rows(0).Item("precio_contado") = odataFila.Tables(0).Rows(0).Item("precio_contado") + (interes / odataFila.Tables(0).Rows(0).Item("cantidad"))
                            ObtenerDescuentosProgramados(odataFila.Tables(0).Rows(0).Item("articulo"), odataFila.Tables(0).Rows(0).Item("bodega"), folio_desc_programado, precio_desc_programado, plan_fonacot, odataFila.Tables(0).Rows(0).Item("cantidad"), interes, odataArticulos.Table.Rows.Count)
                            odataFila.Tables(0).Rows(0).Item("precio_pactado") = odataFila.Tables(0).Rows(0).Item("precio_pactado") + intereses

                        Else
                            Dim total As Double = clcImporte.Value
                            Dim precio_Articulo_expo As Double
                            Dim porcentaje_articulo_intereses As Double
                            Dim porcentaje_intereses As Double

                            ObtenerDescuentosProgramados(odataFila.Tables(0).Rows(0).Item("articulo"), odataFila.Tables(0).Rows(0).Item("bodega"), folio_desc_programado, precio_desc_programado, plan_fonacot, odataFila.Tables(0).Rows(0).Item("cantidad"), 0, odataArticulos.Table.Rows.Count)


                            If plan_fonacot = True Then
                                precio_Articulo_expo = precio_desc_programado  ' Precio del Plan seleccionado

                            Else
                                If Me.Tiene_Precio_Expo = True And Me.Precio_Expo = Me.precio_plan Then
                                    precio_Articulo_expo = odataFila.Tables(0).Rows(0).Item("precio_contado") ' Precio de EXPO
                                Else
                                    precio_Articulo_expo = odataFila.Tables(0).Rows(0).Item("preciounitario")  ' Precio Lista 
                                End If
                            End If

                            porcentaje_articulo_intereses = (precio_Articulo_expo * odataFila.Tables(0).Rows(0).Item("cantidad")) / total
                            porcentaje_intereses = (porcentaje_articulo_intereses * interes) / odataFila.Tables(0).Rows(0).Item("cantidad")

                            odataFila.Tables(0).Rows(0).Item("preciounitario") = odataFila.Tables(0).Rows(0).Item("preciounitario") + porcentaje_intereses
                            odataFila.Tables(0).Rows(0).Item("precio_contado") = odataFila.Tables(0).Rows(0).Item("precio_contado") + porcentaje_intereses
                            odataFila.Tables(0).Rows(0).Item("precio_pactado") = odataFila.Tables(0).Rows(0).Item("precio_pactado") + porcentaje_intereses
                            precio_desc_programado = precio_desc_programado + porcentaje_intereses

                        End If
                    Else
                        ObtenerDescuentosProgramados(odataFila.Tables(0).Rows(0).Item("articulo"), odataFila.Tables(0).Rows(0).Item("bodega"), folio_desc_programado, precio_desc_programado, plan_fonacot, odataFila.Tables(0).Rows(0).Item("cantidad"), 0, odataArticulos.Table.Rows.Count)
                        odataFila.Tables(0).Rows(0).Item("precio_pactado") = precio_desc_programado
                    End If



                    response = New Events
                    If odataFila.Tables(0).Rows(0).Item("sobrepedido") = 0 Then response = oMovimientosInventarioDetalle.ValidacantidadArticulos(odataFila, .Table.Rows(i).Item("bodega"), ConceptoVenta, folio_mvto, "I", "Ventas")

                    If Not response.ErrorFound Then


                        Dim cantidades As Long
                        cantidades = odataFila.Tables(0).Rows(0).Item("cantidad")

                        ' Se metio este ciclo para obtener los costos de los articulos en las salidas, 
                        'ya que ahora se podran capturar cantidades mayores a 1
                        '--------------------------------------------------------------------------------
                        Do While cantidades > 0
                            TotalCantidades = TotalCantidades + 1


                            'si el articulo no es sobrepedido
                            If odataFila.Tables(0).Rows(0).Item("sobrepedido") = 0 Then

                                Dim oDataSet As DataSet
                                'si el concepto de la venta afecta costos
                                If OwnerForm.AfectaCostos Then

                                    ' Traer costo del historico de costos
                                    response = oHistoricoCostos.UltimoCostoArticulo(CType(odataFila.Tables(0).Rows(0).Item("articulo"), Long), CType(odataFila.Tables(0).Rows(0).Item("bodega"), String))
                                    If Not response.ErrorFound Then

                                        oDataSet = response.Value
                                        If oDataSet.Tables(0).Rows.Count > 0 Then

                                            odataFila.Tables(0).Rows(0).Item("costo") = oDataSet.Tables(0).Rows(0).Item("costo")

                                            If Not oDataSet.Tables(0).Rows(0).Item("folio") Is System.DBNull.Value Then
                                                odataFila.Tables(0).Rows(0).Item("folio_historico_costo") = oDataSet.Tables(0).Rows(0).Item("folio")
                                            Else
                                                odataFila.Tables(0).Rows(0).Item("folio_historico_costo") = -1
                                            End If

                                            'disminuyo el saldo del articulo en el historico de costos
                                            response = oHistoricoCostos.ActualizaSaldo(odataFila.Tables(0).Rows(0).Item("folio_historico_costo"), -1)
                                        End If

                                    End If

                                Else

                                    'Traer ultimo costo de articulos
                                    response = oArticulos.DespliegaDatos(CType(odataFila.Tables(0).Rows(0).Item("articulo"), Long))
                                    If Not response.ErrorFound Then

                                        oDataSet = response.Value
                                        If oDataSet.Tables(0).Rows.Count > 0 Then

                                            odataFila.Tables(0).Rows(0).Item("costo") = oDataSet.Tables(0).Rows(0).Item("ultimo_costo")
                                            odataFila.Tables(0).Rows(0).Item("folio_historico_costo") = -1

                                        End If
                                    End If

                                End If

                                oDataSet = Nothing
                                Costo_Detalle = odataFila.Tables(0).Rows(0).Item("costo")

                            Else
                                'si el articulo es sobrepedido
                                Costo_Detalle = odataFila.Tables(0).Rows(0).Item("costo")
                                odataFila.Tables(0).Rows(0).Item("folio_historico_costo") = -1
                            End If

                            '/*csequera 22 enero 2008
                            If Not response.ErrorFound Then response = Me.oVistasSalidasDetalle.ActualizarFacturada(CType(Me.OwnerForm, frmVentas).lkpFolioVistaSalida.EditValue, CType(TotalCantidades, Long), CType(odataFila.Tables(0).Rows(0).Item("articulo"), Long), True, Comunes.Common.Sucursal_Actual, SerieFactura, CType(CType(Me.OwnerForm, frmVentas).lblFolio.Text, Long))
                            '*/csequera 22 enero 2008

                            TotalCostos += odataFila.Tables(0).Rows(0).Item("costo")
                            If Not response.ErrorFound Then response = oMovimientosInventarioDetalle.Insertar(Comunes.Common.Sucursal_Actual, .Table.Rows(i).Item("bodega"), ConceptoVenta, folio_mvto, odataVenta.Tables(0).Rows(0).Item("fecha"), TotalCantidades, .Table.Rows(i).Item("articulo"), 1, Costo_Detalle, Costo_Detalle * 1, odataFila.Tables(0).Rows(0).Item("folio_historico_costo"), folio_movimiento_detalle)


                            ' Para revisar si se guardara la descripcion especial del articulo
                            Dim descripcion_especial As String = ""

                            If Me.chkFacturacionEspecial.Checked Then
                                descripcion_especial = odataFila.Tables(0).Rows(0).Item("descripcion_especial")
                            End If


                            'ObtenerDescuentosProgramados(odataFila.Tables(0).Rows(0).Item("articulo"), folio_desc_programado, precio_desc_programado, plan_fonacot, odataFila.Tables(0).Rows(0).Item("cantidad"), interes)

                            If Not response.ErrorFound Then response = oVentasDetalle.Insertar(odataFila, Comunes.Common.Sucursal_Actual, SerieFactura, odataVenta.Tables(0).Rows(0).Item("folio"), TotalCantidades, folio_movimiento_detalle, tipo_precio_venta, folio_desc_programado, precio_desc_programado, odataFila.Tables(0).Rows(0).Item("precio_pactado"), descripcion_especial)
                            folio_movimiento_detalle = 0


                            If Not response.ErrorFound Then
                                Dim columna_actualizar_entregar As String
                                Dim columna_actualizar_fisicas As String = "FI"

                                If odataFila.Tables(0).Rows(0).Item("sobrepedido") = 0 Then
                                    columna_actualizar_entregar = "EN"
                                    response = oArticulosExistencias.Actualizar(odataFila.Tables(0).Rows(0).Item("articulo"), odataFila.Tables(0).Rows(0).Item("bodega"), columna_actualizar_fisicas, -1, 0)
                                Else
                                    columna_actualizar_entregar = "EP"
                                End If


                                response = oArticulosExistencias.Actualizar(odataFila.Tables(0).Rows(0).Item("articulo"), odataFila.Tables(0).Rows(0).Item("bodega"), columna_actualizar_entregar, 1, 0)
                            End If



                            cantidades = cantidades - 1
                        Loop


                    Else

                        Exit For

                    End If

                End If  'fin del If odataArticulos(i).Item("control") <= 2
            Next

        End With

        Return response

    End Function
    Private Function GuardaDocumentos(ByVal response As Events, ByVal sObservaciones As String, ByVal quincena_inicio As Long, ByVal ano_inicio As Long) As Events

        Dim plazo As Integer
        Dim tipo_plazo As String
        tipo_plazo = CType(lkpPlan.GetValue("tipo_plazo"), String)

        'Se quita esta validacion a peticion de Hugo 27-Dic-13
        'If (sucursal_dependencia = True) Then
        '    tipo_plazo = "Quincenal"
        'End If

        Select Case tipo_plazo

            Case "Semanal"
                plazo = 7
            Case "Quincenal"
                plazo = 15
            Case "Mensual"
                plazo = 30

        End Select

        Dim fecha_vto As Date
        fecha_vto = Me.dteFecha_Primer_Documento.DateTime.Date
        Dim i As Integer = 1
        For i = 1 To Me.clcNumero_Documentos.EditValue - 1

            response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFactura, SerieFactura, odataVenta.Tables(0).Rows(0).Item("folio"), odataVenta.Tables(0).Rows(0).Item("cliente"), i, 0, odataVenta.Tables(0).Rows(0).Item("fecha"), Me.clcImporte_Documentos.EditValue, -1, -1, -1, -1, -1, -1, plazo, fecha_vto, Me.clcImporte_Documentos.EditValue, "", sObservaciones, 0, quincena_inicio, ano_inicio, sucursal_dependencia)
            Select Case plazo

                Case 30   'mensual
                    fecha_vto = fecha_vto.AddMonths(1)
                    If fecha_vto.Day > 15 Then

                        Dim dias_mes As Integer = 0
                        dias_mes = fecha_vto.DaysInMonth(fecha_vto.Year, fecha_vto.Month)
                        fecha_vto = CDate(dias_mes.ToString + "/" + fecha_vto.Month.ToString + "/" + fecha_vto.Year.ToString)

                    End If

                Case 15  'quincenal

                    fecha_vto = fecha_vto.AddDays(13) '13 por que febrero es el mes con menos dias posibles  = 28
                    If fecha_vto.Day < 15 Then

                        fecha_vto = fecha_vto.AddDays(15 - fecha_vto.Day)

                    Else

                        fecha_vto = fecha_vto.AddDays((fecha_vto.DaysInMonth(fecha_vto.Year, fecha_vto.Month)) - fecha_vto.Day)
                    End If

                Case 7    'semanal

                    fecha_vto = fecha_vto.Date.AddDays(plazo)

            End Select

            If quincena_inicio >= 24 Then

                ano_inicio += 1
                quincena_inicio = 1

            Else

                quincena_inicio += 1

            End If

        Next

        'en el ultimo movto de cobrar se mete el importe del ultimo doc
        response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFactura, SerieFactura, odataVenta.Tables(0).Rows(0).Item("folio"), odataVenta.Tables(0).Rows(0).Item("cliente"), i, 0, odataVenta.Tables(0).Rows(0).Item("fecha"), Me.clcImporte_Ultimo_Documento.EditValue, -1, -1, -1, -1, -1, -1, plazo, fecha_vto, Me.clcImporte_Ultimo_Documento.EditValue, "", sObservaciones, 0, quincena_inicio, ano_inicio, sucursal_dependencia)

        'SI tiene enganche, se mete en un documento 0
        If Me.chkEnganche_En_Documento.Checked = True Then  'SI enganche en documento elije de fecha de vto

            fecha_vto = Me.dteFecha_Enganche_Documento.DateTime.Date

        Else

            fecha_vto = CDate(TinApp.FechaServidor).Date

        End If

        If Me.clcEnganche1.EditValue > 0 Then

            response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFactura, SerieFactura, odataVenta.Tables(0).Rows(0).Item("folio"), odataVenta.Tables(0).Rows(0).Item("cliente"), 0, 0, odataVenta.Tables(0).Rows(0).Item("fecha"), Me.clcEnganche1.EditValue, -1, -1, -1, -1, -1, -1, 0, fecha_vto, Me.clcEnganche1.EditValue, "", sObservaciones, 0)

        End If

        Return response

    End Function
    Private Function EsEnajenacion() As Boolean

        ' DAM - 05-12-2013  reformas fiscales
        ' Si no esta configurado en variables para manejar enajenaciones se regresa false
        If maneja_enajenacion = False Then
            EsEnajenacion = False
            Exit Function
        End If


        EsEnajenacion = True
        'es credito -   ok

        'no tiene iva desglosado
        If Me.chkIvadesglosado1.Checked = True Then

            EsEnajenacion = False
            Exit Function

        End If

        'EL PLAZO DE VENTA SEA MAYOR A 12 MESES
        Dim tipo_plazo As String
        tipo_plazo = CType(lkpPlan.GetValue("tipo_plazo"), String)
        Dim meses As Double
        Select Case tipo_plazo

            Case "Semanal"
                meses = Me.clcNumero_Documentos.EditValue / 4
            Case "Quincenal"
                meses = Me.clcNumero_Documentos.EditValue / 2
            Case "Mensual"
                meses = Me.clcNumero_Documentos.EditValue / 1

        End Select
        If meses <= 12 Then

            EsEnajenacion = False
            Exit Function

        End If

        'EL 35 % DE LA VENTA SEA PACTADO DESPUES DEL 6 MES
        Dim total_35porc As Double
        Dim total_pagar As Double
        total_35porc = Me.clcTotal.EditValue * 0.35
        Select Case tipo_plazo

            Case "Semanal"
                total_pagar = ((Me.clcNumero_Documentos.EditValue - 7) * (Me.clcImporte_Documentos.EditValue * 4)) + (Me.clcImporte_Documentos.EditValue * 3) + Me.clcImporte_Ultimo_Documento.EditValue
            Case "Quincenal"
                total_pagar = ((Me.clcNumero_Documentos.EditValue - 7) * (Me.clcImporte_Documentos.EditValue * 2)) + Me.clcImporte_Documentos.EditValue + Me.clcImporte_Ultimo_Documento.EditValue
            Case "Mensual"
                total_pagar = ((Me.clcNumero_Documentos.EditValue - 7) * Me.clcImporte_Documentos.EditValue) + Me.clcImporte_Ultimo_Documento.EditValue

        End Select
        If total_pagar < total_35porc Then

            EsEnajenacion = False
            Exit Function

        End If

    End Function
    Private Sub CalcularMenosEnganche()
        If Me.chkaplica_enganche.Checked = False Then    ' ...................... A

            ' Si el check esta apagado entonces se revisar con el proceso normal para saber en donde cae el pago

            Dim Subtotal_MenosEnganche As Double
            Dim porcentaje As Double
            Dim Tipo_categoria As Long

            If Me.TotalAnticipos > 0 Then               ' ....................... B
                Me.clcMenos.Value = 0
                Me.clcEnganche1.Value = Me.clcPagoInicial.Value
                ActualizaEngancheMenos(Plazo_Plan)
                paso_menos_calculado = False
            Else

                If plan_fonacot = False Then            ' ....................... C
                    If Plazo_Plan < 13 Then             ' ....................... D

                        Subtotal_MenosEnganche = Me.clcPagoInicial.Value + (clcImporte_Ultimo_Documento.Value - clcMonto_Liquida_Vencimiento.Value)
                        porcentaje = (Subtotal_MenosEnganche / clcImporte.Value) * 100

                        'DAM 28-06-07 Variable para saber cual Categoria pertenece la venta
                        ' 0 Identificable
                        ' 1 No Identificable
                        ' -1 Vacio

                        If Not OwnerForm Is Nothing Then
                            Tipo_categoria = Me.OwnerForm.CategoriasIdentificables()
                        End If


                        If Tipo_categoria > -1 Then     ' ........................ E

                            If Me.chkIvadesglosado1.Checked Then
                                Me.clcMenos.Value = 0
                                Me.clcEnganche1.Value = Me.clcPagoInicial.Value
                                paso_menos_calculado = False
                            Else
                                Select Case Tipo_categoria
                                    Case 0 ' Identificable
                                        If porcentaje <= porcentaje_identificable Then
                                            Me.clcEnganche1.Value = 0
                                            Me.clcMenos.Value = Me.clcPagoInicial.Value
                                            paso_menos_calculado = True
                                        Else
                                            Me.clcMenos.Value = 0
                                            Me.clcEnganche1.Value = Me.clcPagoInicial.Value
                                            paso_menos_calculado = False
                                        End If
                                    Case 1 ' No Identificable
                                        If porcentaje <= porcentaje_no_identificable Then
                                            Me.clcEnganche1.Value = 0
                                            Me.clcMenos.Value = Me.clcPagoInicial.Value
                                            paso_menos_calculado = True
                                        Else
                                            Me.clcMenos.Value = 0
                                            Me.clcEnganche1.Value = Me.clcPagoInicial.Value
                                            paso_menos_calculado = False
                                        End If
                                End Select
                            End If

                            ActualizaEngancheMenos(Plazo_Plan)

                        End If ' TIPO CATEGORIA     ' ..............................  E
                    Else
                        If Me.clcImporte.Value > 0 Then
                            Subtotal_MenosEnganche = (Me.clcPagoInicial.Value / clcImporte.Value)
                        Else
                            Subtotal_MenosEnganche = 0
                        End If

                        porcentaje = Subtotal_MenosEnganche * 100
                        If Me.chkIvadesglosado1.Checked Then
                            Me.clcEnganche1.Value = Me.clcPagoInicial.Value
                            Me.clcMenos.Value = 0
                            paso_menos_calculado = False
                        Else
                            If porcentaje < 15 Then
                                Me.clcEnganche1.Value = 0
                                Me.clcMenos.Value = Me.clcPagoInicial.Value
                                paso_menos_calculado = True
                            Else
                                paso_menos_calculado = False
                                Me.clcMenos.Value = 0
                                Me.clcEnganche1.Value = Me.clcPagoInicial.Value
                            End If
                        End If

                        ActualizaEngancheMenos(Plazo_Plan)

                    End If
                Else
                    Me.clcEnganche1.Value = 0
                    Me.clcMenos.Value = Me.clcPagoInicial.Value
                    paso_menos_calculado = True

                    ActualizaEngancheMenos(Plazo_Plan)
                End If

            End If

        Else
            ' Si el check esta prendido entonces se asigna directamente el pago inicial al enganche
            Me.clcMenos.Value = 0
            Me.clcEnganche1.Value = Me.clcPagoInicial.Value
        End If    ' ................................................ A

    End Sub
    '/* csequera 22 enero 2008
    Private Function GeneraMovimientoInventario(ByVal accion As Actions, ByRef response As Events, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha_movimiento As Date, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String) '/*se sobrecargo la funcion para que esta funcione para guardar movimientos al inv de entradas de vistas */
        'If Guardado = False Then
        If CType(Me.OwnerForm, frmVentas).Guardado = False Then

            Select Case accion
                Case Actions.Insert, Actions.Delete
                    response = oMovimientosInventario.Insertar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                    'Guardado = True
                    CType(Me.OwnerForm, frmVentas).Guardado = True
                Case Actions.Update
                    response = oMovimientosInventario.Actualizar(sucursal, bodega, concepto, folio, fecha_movimiento, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
                    'Guardado = True
                    CType(Me.OwnerForm, frmVentas).Guardado = True
            End Select

        End If

        '/* csequera 22 enero 2008
    End Function
    Private Sub ActualizaEngancheMenos(ByVal plazo As Long)

        RecalculaTotal()

        'DAm se quito para que no hiciera el recalculo de nuevo de los doctos.  2/Dic/2008
        'CalculaLiquidaVencimiento()

        'DAM-11/JUN/08 SIEMPRE ACTIVO EL CHECK DE ENGANCHE EN DOCUMENTO
        'If plazo < 13 Then  'DAM 26 may

        '    If Me.clcEnganche1.EditValue <= 0 Then

        '        Me.chkEnganche_En_Documento.Checked = False
        '        Me.chkEnganche_En_Documento.Enabled = False

        '    Else

        '        If Me.clcMenos.EditValue <= 0 Then
        '            Me.chkEnganche_En_Documento.Enabled = True
        '        Else

        '            Me.chkEnganche_En_Documento.Checked = True
        '            Me.chkEnganche_En_Documento.Enabled = False

        '        End If

        '    End If
        'End If

        chkEnganche_En_Documento_CheckedChanged(Nothing, Nothing)
    End Sub
    Private Function ValidaPagosInicialDoctos() As Events
        Dim response As New Events

        Dim suma_inicial_doctos_liquida As Double
        Dim suma_inicial_doctos_ultimo_docto As Double

        Dim suma_doctos As Double
        Dim pagoinicial As Double
        Dim liquida_vencimiento As Double
        Dim ultimo_docto As Double
        Dim importeplan As Double

        If Me.clcNumero_Documentos.Value = 1 Then
            If Me.Tiene_Precio_Expo = True And Me.Precio_Expo = Me.precio_plan Then
                suma_doctos = Me.clcNumero_Documentos.Value * clcImporte_Ultimo_Documento.Value
            Else
                suma_doctos = Me.clcNumero_Documentos.Value * Me.clcImporte_Documentos.Value
            End If

            ultimo_docto = 0
        Else
            suma_doctos = ((Me.clcNumero_Documentos.Value - 1) * Me.clcImporte_Documentos.Value)
            ultimo_docto = clcImporte_Ultimo_Documento.Value
        End If

        pagoinicial = Me.clcPagoInicial.Value
        liquida_vencimiento = Me.clcMonto_Liquida_Vencimiento.Value


        'Se hacen los dos calculos para revision
        suma_inicial_doctos_ultimo_docto = suma_doctos + ultimo_docto + pagoinicial + Me.TotalAnticipos
        suma_inicial_doctos_liquida = suma_doctos + pagoinicial + liquida_vencimiento + Me.TotalAnticipos


        If Me.Tiene_Descuento_Programado Then
            importeplan = Me.clcImporteConDescuento.Value
        Else
            importeplan = clcImportePlan.Value
        End If


        If Not (suma_inicial_doctos_ultimo_docto >= clcImporte.Value Or suma_inicial_doctos_liquida >= importeplan) Then
            response.Message = "La Suma de los Documentos M�s el Pago Inicial es menor al Importe del Plan de Cr�dito"
        End If

        Return response
    End Function
    Private Sub RevisaFechaPrimerDocumento()
        If IsDate(Me.dteFecha_Primer_Documento.Text) Then
            Dim mes_siguiente As DateTime
            mes_siguiente = Me.dteFecha_Primer_Documento.DateTime.AddMonths(1)

            If Me.dteFecha_Primer_Documento.DateTime.Day <= 10 Then
                Me.dteFecha_Primer_Documento.DateTime = "15/" + mes_siguiente.Month.ToString + "/" + mes_siguiente.Year.ToString
            Else
                Me.dteFecha_Primer_Documento.DateTime = mes_siguiente.DaysInMonth(mes_siguiente.Year, mes_siguiente.Month).ToString + "/" + mes_siguiente.Month.ToString + "/" + mes_siguiente.Year.ToString
            End If

        Else
            Me.dteFecha_Primer_Documento.DateTime = "15/" + CDate(TinApp.FechaServidor).Date.Month.ToString + "/" + CDate(TinApp.FechaServidor).Date.Year.ToString
        End If
    End Sub
    Private Sub CreaTablaDescuentosProgramados()
        FoliosDescuentosProgramados = New DataTable
        FoliosDescuentosProgramados.Columns.Add("articulo")
        FoliosDescuentosProgramados.Columns.Add("folio_descuento_especial_programado")
        FoliosDescuentosProgramados.Columns.Add("precio_descuento_especial_programado")
        FoliosDescuentosProgramados.Columns.Add("bodega")
    End Sub
    Private Sub ObtenerDescuentosProgramados(ByVal articulo As Long, ByVal bodega As String, ByRef Folio_Desc_programado As Long, ByRef precio_desc_programado As Double, ByVal Precio_Fonacot As Boolean, ByVal cantidades As Long, ByVal intereses As Double, ByVal numeros_articulos As Long)
        Dim row As DataRow
        For Each row In Me.FoliosDescuentosProgramados.Rows()
            If row("articulo") = articulo And row("bodega") = bodega Then
                Folio_Desc_programado = row("folio_descuento_especial_programado")

                ' 06 Mayo 11
                'If Precio_Fonacot = False Then
                'precio_desc_programado = row("precio_descuento_especial_programado")
                'Else
                If numeros_articulos = 1 Then
                    precio_desc_programado = (row("precio_descuento_especial_programado") + intereses) / cantidades
                Else
                    precio_desc_programado = row("precio_descuento_especial_programado") / cantidades
                End If
                'End If

                Exit For
            End If
        Next
    End Sub

    Private Function ValidaUsuarioxEdicion(ByVal Validacion As ValidacionesEdicion, Optional ByVal Documentos As Int32 = 1) As Boolean
        Dim resultado As Boolean = True
        If TinApp.Connection.User.ToUpper = "SUPER" Then
            Select Case Validacion
                Case ValidacionesEdicion.ValidaMontoLiquidaVencimiento
                    Me.clcMonto_Liquida_Vencimiento.Enabled = True
                Case ValidacionesEdicion.ValidaNumeroDocumentos

            End Select


        Else
            Select Case Validacion
                Case ValidacionesEdicion.ValidaMontoLiquidaVencimiento
                    Me.clcMonto_Liquida_Vencimiento.Enabled = False
                Case ValidacionesEdicion.ValidaNumeroDocumentos
                    If Documentos > NumeroDocumentosPlan Then
                        resultado = False
                    End If
            End Select

        End If

        Return resultado

    End Function
    Private Sub TraeDatosVariables()
        Dim response As New Events

        response = oVariables.DespliegaDatos()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet

            oDataSet = response.Value

            'oDataSet.Tables(0).Rows(0).Item("concepto_cxc_factura_credito")
            Me.porcentaje_identificable = CType(oDataSet.Tables(0).Rows(0).Item("porcentaje_identificable"), Double)
            Me.porcentaje_no_identificable = CType(oDataSet.Tables(0).Rows(0).Item("porcentaje_no_identificable"), Double)
            Me.maneja_enajenacion = CType(oDataSet.Tables(0).Rows(0).Item("maneja_enajenacion"), Boolean)
            ConceptoVenta = CType(oDataSet.Tables(0).Rows(0).Item("concepto_venta"), String)
            ConceptoFactura = CType(oDataSet.Tables(0).Rows(0).Item("concepto_cxc_factura_credito"), String)
            porc_impuesto = CType(oDataSet.Tables(0).Rows(0).Item("impuesto"), Double)

            If ConceptoVenta <> "" Then
                banConceptoVenta = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Venta no esta definido", "Variables del Sistema", Nothing, False)
            End If



            If ConceptoFactura <> "" Then
                banConceptoFactura = True
            Else
                ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Cr�dito no esta definido", "Variables del Sistema", Nothing, False)
            End If

        End If
    End Sub

#End Region

End Class
