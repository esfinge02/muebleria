Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmMovimientosCajaPagoVales
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblImportePagar2 As System.Windows.Forms.Label
    Friend WithEvents clcFolioVale As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblImporteRecibido As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblImporteVale As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosCajaPagoVales))
        Me.lblImporteVale = New System.Windows.Forms.Label
        Me.lblImportePagar2 = New System.Windows.Forms.Label
        Me.clcFolioVale = New DevExpress.XtraEditors.CalcEdit
        Me.lblImporteRecibido = New System.Windows.Forms.Label
        Me.btnAceptar = New DevExpress.XtraEditors.SimpleButton
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton
        CType(Me.clcFolioVale.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(378, 28)
        '
        'lblImporteVale
        '
        Me.lblImporteVale.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImporteVale.Location = New System.Drawing.Point(232, 36)
        Me.lblImporteVale.Name = "lblImporteVale"
        Me.lblImporteVale.Size = New System.Drawing.Size(200, 32)
        Me.lblImporteVale.TabIndex = 60
        Me.lblImporteVale.Text = "0"
        Me.lblImporteVale.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImportePagar2
        '
        Me.lblImportePagar2.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImportePagar2.Location = New System.Drawing.Point(16, 36)
        Me.lblImportePagar2.Name = "lblImportePagar2"
        Me.lblImportePagar2.Size = New System.Drawing.Size(208, 32)
        Me.lblImportePagar2.TabIndex = 59
        Me.lblImportePagar2.Text = "Importe del Vale:"
        Me.lblImportePagar2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolioVale
        '
        Me.clcFolioVale.Location = New System.Drawing.Point(227, 82)
        Me.clcFolioVale.Name = "clcFolioVale"
        '
        'clcFolioVale.Properties
        '
        Me.clcFolioVale.Properties.DisplayFormat.FormatString = "n0"
        Me.clcFolioVale.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioVale.Properties.EditFormat.FormatString = "n0"
        Me.clcFolioVale.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioVale.Properties.MaskData.EditMask = "###,###,##0.00"
        Me.clcFolioVale.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolioVale.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolioVale.Size = New System.Drawing.Size(200, 33)
        Me.clcFolioVale.TabIndex = 62
        '
        'lblImporteRecibido
        '
        Me.lblImporteRecibido.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImporteRecibido.Location = New System.Drawing.Point(7, 83)
        Me.lblImporteRecibido.Name = "lblImporteRecibido"
        Me.lblImporteRecibido.Size = New System.Drawing.Size(208, 32)
        Me.lblImporteRecibido.TabIndex = 61
        Me.lblImporteRecibido.Text = "Folio:"
        Me.lblImporteRecibido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(105, 134)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(104, 32)
        Me.btnAceptar.TabIndex = 63
        Me.btnAceptar.Text = "&Aceptar"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(225, 134)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(104, 32)
        Me.btnSalir.TabIndex = 64
        Me.btnSalir.Text = "&Salir"
        '
        'frmMovimientosCajaPagoVales
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(434, 176)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.clcFolioVale)
        Me.Controls.Add(Me.lblImporteRecibido)
        Me.Controls.Add(Me.lblImporteVale)
        Me.Controls.Add(Me.lblImportePagar2)
        Me.Name = "frmMovimientosCajaPagoVales"
        Me.Text = "frmMovimientosCajaPagoVales"
        Me.Controls.SetChildIndex(Me.lblImportePagar2, 0)
        Me.Controls.SetChildIndex(Me.lblImporteVale, 0)
        Me.Controls.SetChildIndex(Me.lblImporteRecibido, 0)
        Me.Controls.SetChildIndex(Me.clcFolioVale, 0)
        Me.Controls.SetChildIndex(Me.btnAceptar, 0)
        Me.Controls.SetChildIndex(Me.btnSalir, 0)
        CType(Me.clcFolioVale.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oVales As VillarrealBusiness.clsVales
    Private oVariables As VillarrealBusiness.clsVariables


    Private dImporteVale As Double = 0
    Private bFolioValido As Boolean = False
    Private lcliente As Long = 0
    Private sConcepto As String = ""
    Private sConceptoValesVencidos As String = ""

    Public WriteOnly Property Concepto() As String
        Set(ByVal Value As String)
            sConcepto = Value
        End Set
    End Property
    Public Property ImporteVale() As String
        Get
            Return Me.lblImporteVale.Text.Replace("$", "")

        End Get
        Set(ByVal Value As String)
            Me.lblImporteVale.Text = Value
        End Set
    End Property
    Public ReadOnly Property FolioValido() As Boolean
        Get
            Return bFolioValido
        End Get
    End Property
    Public WriteOnly Property Cliente() As Long
        Set(ByVal Value As Long)
            lcliente = Value
        End Set
    End Property

    Private Sub frmMovimientosCajaPagoVales_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.tbrTools.Visible = False
        oVales = New VillarrealBusiness.clsVales
        oVariables = New VillarrealBusiness.clsVariables


        sConceptoValesVencidos = CType(oVariables.TraeDatos("concepto_vales_vencidos", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)


    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        CerrarForma()
    End Sub
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim response As New Events

        response = oVales.ValidaVale(Me.clcFolioVale.Value, lcliente, CType(Replace(Me.ImporteVale, "$", ""), Double))
        If Not response.ErrorFound Then

            Dim resultado As Long = response.Value()

            Select Case resultado
                Case 0 'Es correcto el vale
                    EsFolioValido()
                Case 1 ' El folio del Vale no Existe
                    bFolioValido = False
                    response.Message = "El folio del Vale no Existe"
                Case 2 ' El Saldo del Vale no es Suficiente
                    bFolioValido = False
                    response.Message = "El Saldo del Vale no es Suficiente"
                Case 3 ' La vigencia del Saldo ya Expiro.
                    If sConcepto = sConceptoValesVencidos Then
                        EsFolioValido()
                    Else
                        bFolioValido = False
                        response.Message = "La vigencia del Saldo ya Expiro"
                    End If

                Case 4 ' El vale no esta asignado al Cliente
                    bFolioValido = False
                    response.Message = "El vale no esta asignado al Cliente"

            End Select
            response.ShowMessage()
        Else

            response.ShowMessage()
        End If

    End Sub

    Private Sub EsFolioValido()
        bFolioValido = True
        CType(OwnerForm, frmMovimientosCaja).MostrarFormaPagarCajaCambio(Me.clcFolioVale.Value, CType(ImporteVale, Decimal))
        CerrarForma()
    End Sub
    Private Sub CerrarForma()
        Me.Close()
        CType(OwnerForm, frmMovimientosCaja).Enabled = True
    End Sub


End Class
