Imports System.Drawing
Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmConsultarNotaCargo
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblconcepto As System.Windows.Forms.Label
    Friend WithEvents lkpconcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConsultarNotaCargo))
        Me.lblconcepto = New System.Windows.Forms.Label
        Me.lkpconcepto = New Dipros.Editors.TINMultiLookup
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(907, 28)
        '
        'lblconcepto
        '
        Me.lblconcepto.AutoSize = True
        Me.lblconcepto.Location = New System.Drawing.Point(26, 64)
        Me.lblconcepto.Name = "lblconcepto"
        Me.lblconcepto.Size = New System.Drawing.Size(73, 16)
        Me.lblconcepto.TabIndex = 13
        Me.lblconcepto.Tag = ""
        Me.lblconcepto.Text = "&Movimiento:"
        Me.lblconcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpconcepto
        '
        Me.lkpconcepto.AllowAdd = False
        Me.lkpconcepto.AutoReaload = False
        Me.lkpconcepto.DataSource = Nothing
        Me.lkpconcepto.DefaultSearchField = ""
        Me.lkpconcepto.DisplayMember = "descripcion"
        Me.lkpconcepto.EditValue = Nothing
        Me.lkpconcepto.Enabled = False
        Me.lkpconcepto.Filtered = False
        Me.lkpconcepto.InitValue = Nothing
        Me.lkpconcepto.Location = New System.Drawing.Point(111, 64)
        Me.lkpconcepto.MultiSelect = False
        Me.lkpconcepto.Name = "lkpconcepto"
        Me.lkpconcepto.NullText = ""
        Me.lkpconcepto.PopupWidth = CType(480, Long)
        Me.lkpconcepto.Required = True
        Me.lkpconcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpconcepto.SearchMember = ""
        Me.lkpconcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpconcepto.SelectAll = False
        Me.lkpconcepto.Size = New System.Drawing.Size(173, 20)
        Me.lkpconcepto.TabIndex = 14
        Me.lkpconcepto.Tag = "concepto"
        Me.lkpconcepto.ToolTip = Nothing
        Me.lkpconcepto.ValueMember = "concepto"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(7, 136)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 19
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(111, 136)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.Enabled = False
        Me.txtObservaciones.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservaciones.Size = New System.Drawing.Size(432, 38)
        Me.txtObservaciones.TabIndex = 20
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(45, 112)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 17
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(111, 112)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "c2"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(115, 19)
        Me.clcImporte.TabIndex = 18
        Me.clcImporte.Tag = "total"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(65, 88)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 15
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(111, 88)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(115, 20)
        Me.dteFecha.TabIndex = 16
        Me.dteFecha.Tag = "fecha"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(55, 40)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 11
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cl&iente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(111, 40)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(480, Long)
        Me.lkpCliente.Required = True
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(432, 20)
        Me.lkpCliente.TabIndex = 12
        Me.lkpCliente.Tag = "cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'frmConsultarNotaCargo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(576, 184)
        Me.Controls.Add(Me.lblconcepto)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpconcepto)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lkpCliente)
        Me.Name = "frmConsultarNotaCargo"
        Me.Text = "frmConsultarNotaCargo"
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lkpconcepto, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblconcepto, 0)
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas
    Private oConceptos_cxc As VillarrealBusiness.clsConceptosCxc
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oVariables As VillarrealBusiness.clsVariables
    Private oReportes As VillarrealBusiness.Reportes
    Private Folio_Movimiento As Long = -1
    Private ImportePagar As Double
    Private banvalidar As Boolean = False
    Private fila_seleccionada As Integer
    Public ConceptoNotacargo As String
    Private banConceptoNotacargo As Boolean = False
    Public lSucursal As Long = -1
    Public sSerie As String = ""
    Public lFolio As String = -1
    Public sConcepto As String = ""
    Public lCliente As Long = -1



    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property

    Private ReadOnly Property Concepto() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpconcepto)
        End Get
    End Property

    Private ReadOnly Property ConceptoNotaCredito() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_nota_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property ConceptoFacturaCredito() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmConsultarNotaCargo_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmConsultarNotaCargo_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmConsultarNotaCargo_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmConsultarNotaCargo_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept


    End Sub
    Private Sub frmfrmConsultarNotaCargo_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oConceptos_cxc = New VillarrealBusiness.clsConceptosCxc
        oVariables = New VillarrealBusiness.clsVariables
        oReportes = New VillarrealBusiness.Reportes

        Me.Location = New Point(0, 0)
        Me.tbrTools.Buttons(0).Visible = False

    End Sub

    Private Sub frmConsultarNotaCargo_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oMovimientosCobrar.DespliegaDatos(lSucursal, sConcepto, sSerie, lFolio, lCliente)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If
    End Sub

    Private Sub frmNotasCargo_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = Me.oMovimientosCobrar.ValidacionNotaCargo(Action, lSucursal, Me.Cliente, Me.Concepto, Me.clcImporte.EditValue, Me.txtObservaciones.Text)
        'If Not Response.ErrorFound Then
        '    banvalidar = True
        'End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpconcepto.LoadData
        Dim Response As New Events
        ' es true para que solo muestre los conceptos que se pueden hacer en este modulo
        Response = oConceptos_cxc.Lookup("C", True)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpconcepto.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpConcepto_Format() Handles lkpconcepto.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpconcepto)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub



    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub

    Private Sub clcImporte_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcImporte.EditValueChanged
        ImportePagar = Me.clcImporte.EditValue
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    

    Private Sub TraeConceptoNotaCargo()
        Me.ConceptoNotacargo = CType(oVariables.TraeDatos("concepto_cxc_nota_cargo", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoNotacargo <> "" Then
            banConceptoNotacargo = True
        Else
            ShowMessage(MessageType.MsgInformation, "El Concepto de  CXC de Nota de Cargo no esta definido", "Variables del Sistema", Nothing, False)
        End If
    End Sub
#End Region



End Class
