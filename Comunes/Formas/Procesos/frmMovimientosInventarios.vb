Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms
Imports Comunes.clsUtilerias

Public Class frmMovimientosInventarios
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Concepto_Referencia As String
    Dim KS As Keys

    Public AfectaSaldos As Boolean
    Public Modificable_MovimientosInv As Boolean
    Public pTipoconcepto As Char
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grMovimientosInventarios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvMovimientosInventarios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaMovimientosInventarios As Dipros.Windows.TINMaster
    Friend WithEvents lblReferencia As System.Windows.Forms.Label
    Friend WithEvents txtConceptoReferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcFolioReferencia As Dipros.Editors.TINCalcEdit
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpConcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDepartamento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcvalidanumeroserie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcentro_pantalla_validaserie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnImpresion As System.Windows.Forms.ToolBarButton

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosInventarios))
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.grMovimientosInventarios = New DevExpress.XtraGrid.GridControl
        Me.grvMovimientosInventarios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDepartamento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcGrupo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcvalidanumeroserie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcentro_pantalla_validaserie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaMovimientosInventarios = New Dipros.Windows.TINMaster
        Me.lblReferencia = New System.Windows.Forms.Label
        Me.txtConceptoReferencia = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcFolioReferencia = New Dipros.Editors.TINCalcEdit
        Me.lkpConcepto = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.btnImpresion = New System.Windows.Forms.ToolBarButton
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grMovimientosInventarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvMovimientosInventarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConceptoReferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioReferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnImpresion})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2452, 28)
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(39, 56)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 4
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "Co&ncepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(64, 80)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 6
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(104, 80)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(48, 19)
        Me.clcFolio.TabIndex = 7
        Me.clcFolio.Tag = "folio"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(368, 32)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "13/03/2006"
        Me.dteFecha.Location = New System.Drawing.Point(416, 32)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Properties.ShowClear = False
        Me.dteFecha.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(10, 104)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 12
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(104, 104)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(405, 38)
        Me.txtObservaciones.TabIndex = 13
        Me.txtObservaciones.Tag = "observaciones"
        '
        'grMovimientosInventarios
        '
        '
        'grMovimientosInventarios.EmbeddedNavigator
        '
        Me.grMovimientosInventarios.EmbeddedNavigator.Name = ""
        Me.grMovimientosInventarios.Location = New System.Drawing.Point(8, 176)
        Me.grMovimientosInventarios.MainView = Me.grvMovimientosInventarios
        Me.grMovimientosInventarios.Name = "grMovimientosInventarios"
        Me.grMovimientosInventarios.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.grMovimientosInventarios.Size = New System.Drawing.Size(504, 219)
        Me.grMovimientosInventarios.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grMovimientosInventarios.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientosInventarios.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientosInventarios.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientosInventarios.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientosInventarios.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientosInventarios.TabIndex = 15
        Me.grMovimientosInventarios.Text = "MovimientosInventarios"
        '
        'grvMovimientosInventarios
        '
        Me.grvMovimientosInventarios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcArticulo, Me.grcDescripcion, Me.grcCantidad, Me.grcCosto, Me.grcImporte, Me.grcDepartamento, Me.grcGrupo, Me.grcNumeroSerie, Me.grcvalidanumeroserie, Me.grcentro_pantalla_validaserie})
        Me.grvMovimientosInventarios.GridControl = Me.grMovimientosInventarios
        Me.grvMovimientosInventarios.Name = "grvMovimientosInventarios"
        Me.grvMovimientosInventarios.OptionsCustomization.AllowFilter = False
        Me.grvMovimientosInventarios.OptionsCustomization.AllowGroup = False
        Me.grvMovimientosInventarios.OptionsCustomization.AllowSort = False
        Me.grvMovimientosInventarios.OptionsView.ShowFooter = True
        Me.grvMovimientosInventarios.OptionsView.ShowGroupPanel = False
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 0
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion_corta"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 1
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 2
        '
        'grcCosto
        '
        Me.grcCosto.Caption = "Costo"
        Me.grcCosto.DisplayFormat.FormatString = "c2"
        Me.grcCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCosto.FieldName = "costo"
        Me.grcCosto.Name = "grcCosto"
        Me.grcCosto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCosto.VisibleIndex = 4
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.SummaryItem.DisplayFormat = "{0:$ ###,###,##0.00}"
        Me.grcImporte.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporte.VisibleIndex = 5
        '
        'grcDepartamento
        '
        Me.grcDepartamento.Caption = "Departamento"
        Me.grcDepartamento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDepartamento.FieldName = "departamento"
        Me.grcDepartamento.Name = "grcDepartamento"
        '
        'grcGrupo
        '
        Me.grcGrupo.Caption = "Grupo"
        Me.grcGrupo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcGrupo.FieldName = "grupo"
        Me.grcGrupo.Name = "grcGrupo"
        '
        'grcNumeroSerie
        '
        Me.grcNumeroSerie.Caption = "N�mero Serie"
        Me.grcNumeroSerie.FieldName = "numero_serie"
        Me.grcNumeroSerie.Name = "grcNumeroSerie"
        Me.grcNumeroSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumeroSerie.VisibleIndex = 3
        '
        'grcvalidanumeroserie
        '
        Me.grcvalidanumeroserie.Caption = "Valida Serie"
        Me.grcvalidanumeroserie.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcvalidanumeroserie.FieldName = "validanumeroserie"
        Me.grcvalidanumeroserie.Name = "grcvalidanumeroserie"
        Me.grcvalidanumeroserie.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grcentro_pantalla_validaserie
        '
        Me.grcentro_pantalla_validaserie.Caption = "entro_pantalla_validaserie"
        Me.grcentro_pantalla_validaserie.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcentro_pantalla_validaserie.FieldName = "entro_pantalla_validaserie"
        Me.grcentro_pantalla_validaserie.Name = "grcentro_pantalla_validaserie"
        Me.grcentro_pantalla_validaserie.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'tmaMovimientosInventarios
        '
        Me.tmaMovimientosInventarios.BackColor = System.Drawing.Color.White
        Me.tmaMovimientosInventarios.CanDelete = True
        Me.tmaMovimientosInventarios.CanInsert = True
        Me.tmaMovimientosInventarios.CanUpdate = True
        Me.tmaMovimientosInventarios.Grid = Me.grMovimientosInventarios
        Me.tmaMovimientosInventarios.Location = New System.Drawing.Point(8, 152)
        Me.tmaMovimientosInventarios.Name = "tmaMovimientosInventarios"
        Me.tmaMovimientosInventarios.Size = New System.Drawing.Size(504, 23)
        Me.tmaMovimientosInventarios.TabIndex = 14
        Me.tmaMovimientosInventarios.Title = "Art�culos"
        Me.tmaMovimientosInventarios.UpdateTitle = "un Art�culo"
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(304, 80)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(67, 16)
        Me.lblReferencia.TabIndex = 8
        Me.lblReferencia.Tag = ""
        Me.lblReferencia.Text = "R&eferencia:"
        Me.lblReferencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConceptoReferencia
        '
        Me.txtConceptoReferencia.EditValue = ""
        Me.txtConceptoReferencia.Location = New System.Drawing.Point(376, 80)
        Me.txtConceptoReferencia.Name = "txtConceptoReferencia"
        '
        'txtConceptoReferencia.Properties
        '
        Me.txtConceptoReferencia.Properties.MaxLength = 5
        Me.txtConceptoReferencia.Size = New System.Drawing.Size(64, 20)
        Me.txtConceptoReferencia.TabIndex = 9
        Me.txtConceptoReferencia.Tag = "concepto_referencia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(448, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(9, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Tag = ""
        Me.Label2.Text = "/"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolioReferencia
        '
        Me.clcFolioReferencia.EditValue = "0"
        Me.clcFolioReferencia.Location = New System.Drawing.Point(464, 80)
        Me.clcFolioReferencia.MaxValue = 0
        Me.clcFolioReferencia.MinValue = 0
        Me.clcFolioReferencia.Name = "clcFolioReferencia"
        '
        'clcFolioReferencia.Properties
        '
        Me.clcFolioReferencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioReferencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioReferencia.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolioReferencia.Properties.MaxLength = 5
        Me.clcFolioReferencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolioReferencia.Size = New System.Drawing.Size(48, 19)
        Me.clcFolioReferencia.TabIndex = 11
        Me.clcFolioReferencia.Tag = "folio_referencia"
        '
        'lkpConcepto
        '
        Me.lkpConcepto.AllowAdd = False
        Me.lkpConcepto.AutoReaload = False
        Me.lkpConcepto.DataSource = Nothing
        Me.lkpConcepto.DefaultSearchField = ""
        Me.lkpConcepto.DisplayMember = "descripcion"
        Me.lkpConcepto.EditValue = Nothing
        Me.lkpConcepto.Filtered = False
        Me.lkpConcepto.InitValue = Nothing
        Me.lkpConcepto.Location = New System.Drawing.Point(104, 56)
        Me.lkpConcepto.MultiSelect = False
        Me.lkpConcepto.Name = "lkpConcepto"
        Me.lkpConcepto.NullText = ""
        Me.lkpConcepto.PopupWidth = CType(400, Long)
        Me.lkpConcepto.ReadOnlyControl = False
        Me.lkpConcepto.Required = False
        Me.lkpConcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConcepto.SearchMember = ""
        Me.lkpConcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConcepto.SelectAll = False
        Me.lkpConcepto.Size = New System.Drawing.Size(192, 20)
        Me.lkpConcepto.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConcepto.TabIndex = 5
        Me.lkpConcepto.Tag = "concepto"
        Me.lkpConcepto.ToolTip = Nothing
        Me.lkpConcepto.ValueMember = "concepto"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(49, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Bodega:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(104, 32)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(192, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 1
        Me.lkpBodega.Tag = "bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "bodega"
        '
        'btnImpresion
        '
        Me.btnImpresion.Enabled = False
        Me.btnImpresion.Text = "Reimprimir"
        Me.btnImpresion.ToolTipText = "Imprime el Movimiento al Inventario"
        Me.btnImpresion.Visible = False
        '
        'frmMovimientosInventarios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(522, 400)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpConcepto)
        Me.Controls.Add(Me.clcFolioReferencia)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblReferencia)
        Me.Controls.Add(Me.txtConceptoReferencia)
        Me.Controls.Add(Me.lblConcepto)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.grMovimientosInventarios)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.tmaMovimientosInventarios)
        Me.MasterControl = Me.tmaMovimientosInventarios
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmMovimientosInventarios"
        Me.Controls.SetChildIndex(Me.tmaMovimientosInventarios, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.grMovimientosInventarios, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto, 0)
        Me.Controls.SetChildIndex(Me.txtConceptoReferencia, 0)
        Me.Controls.SetChildIndex(Me.lblReferencia, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcFolioReferencia, 0)
        Me.Controls.SetChildIndex(Me.lkpConcepto, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grMovimientosInventarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvMovimientosInventarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConceptoReferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioReferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oMovimientosInventarios As VillarrealBusiness.clsMovimientosInventarios
    Private oMovimientosInventariosDetalle As VillarrealBusiness.clsMovimientosInventariosDetalle
    Private oMovimientosInventarioDetalleSeries As VillarrealBusiness.clsMovimientosInventariosDetalleSeries
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario
    Private oReportes As VillarrealBusiness.Reportes
    Private oHistCostos As VillarrealBusiness.clsHisCostos
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oArticulos As VillarrealBusiness.clsArticulos

    Friend ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property
    Friend ReadOnly Property Concepto() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConcepto)
        End Get
    End Property
    Private TotalCantidades As Long

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmMovimientosInventarios_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If VerificaPermisoExtendido(Me.OwnerForm.MenuOption.Name, "FECHA_MOV_INV") Then
        '    Me.dteFecha.Enabled = True
        'Else
        '    Me.dteFecha.Enabled = False
        'End IF
        Me.dteFecha.DateTime = TinApp.FechaServidor()
    End Sub

    Private Sub frmMovimientosInventarios_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmMovimientosInventarios_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmMovimientosInventarios_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
        ImprimirReporte()
    End Sub

    Private Sub frmMovimientosInventarios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oMovimientosInventarios.Insertar(Comunes.Common.Sucursal_Actual, Bodega, Concepto, Me.clcFolio.Value, Me.dteFecha.EditValue, CInt("-1"), Me.txtConceptoReferencia.Text, Me.clcFolioReferencia.Value, Me.txtObservaciones.Text)
            Case Actions.Update
                Response = oMovimientosInventarios.Actualizar(Comunes.Common.Sucursal_Actual, Bodega, Concepto, Me.clcFolio.Value, Me.dteFecha.EditValue, CInt("-1"), Me.txtConceptoReferencia.Text, Me.clcFolioReferencia.Value, Me.txtObservaciones.Text)
                'Case Actions.Delete
                '    Response = oMovimientosInventarios.Eliminar(Comunes.Common.Sucursal_Actual, Me.lkpBodega.EditValue, Me.lkpConcepto.EditValue, Me.clcFolio.Value)
        End Select
    End Sub
    Private Sub frmMovimientosInventarios_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oMovimientosInventarios.DespliegaDatos(OwnerForm.Value("bodega"), OwnerForm.Value("concepto"), OwnerForm.Value("folio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            Response = oMovimientosInventariosDetalle.ListadoMovimientosInventarioAlmacen(OwnerForm.Value("folio"), OwnerForm.Value("bodega"), OwnerForm.Value("concepto"))
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                tmaMovimientosInventarios.DataSource = oDataSet
            End If
        End If

        Select Case Action
            Case Actions.Update

                Me.btnImpresion.Enabled = True
                Me.btnImpresion.Visible = True

                If CType(Me.lkpConcepto.GetValue("modificable_movimientosinv"), Boolean) = False Then 'Modificable_MovimientosInv = False Then
                    tbrTools.Buttons.Item(0).Enabled = False
                    clcFolioReferencia.Enabled = False
                    txtConceptoReferencia.Enabled = False
                    txtObservaciones.Enabled = False
                End If
                'Case Actions.Delete
        End Select

        If CType(OwnerForm, brwMovimientosInventarios).banModificarConcepto = True Then
            Me.tbrTools.Buttons(0).Enabled = False
        Else
            Me.tbrTools.Buttons(0).Enabled = True
        End If
    End Sub
    Private Sub frmMovimientosInventarios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oMovimientosInventarios = New VillarrealBusiness.clsMovimientosInventarios
        oMovimientosInventariosDetalle = New VillarrealBusiness.clsMovimientosInventariosDetalle
        oMovimientosInventarioDetalleSeries = New VillarrealBusiness.clsMovimientosInventariosDetalleSeries
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario
        oReportes = New VillarrealBusiness.Reportes
        oHistCostos = New VillarrealBusiness.clsHisCostos
        oBodegas = New VillarrealBusiness.clsBodegas
        oArticulos = New VillarrealBusiness.clsArticulos

        If OwnerForm.banValidacion Then
            Me.tbrTools.Buttons.Item(0).Enabled = False
            Me.tmaMovimientosInventarios.Enabled = False
            Me.grMovimientosInventarios.Enabled = False
            Me.dteFecha.Enabled = False
            Me.clcFolioReferencia.Enabled = False
            Me.txtConceptoReferencia.Enabled = False
            Me.txtObservaciones.Enabled = False
        End If

        Select Case Action
            Case Actions.Insert
                Me.lkpConcepto.EditValue = OwnerForm.lkpConcepto.EditValue
                Me.lkpBodega.EditValue = OwnerForm.lkpBodega.EditValue
                Me.dteFecha.EditValue = CDate(TinApp.FechaServidor)
            Case Actions.Update
                Me.tmaMovimientosInventarios.Enabled = False
                grMovimientosInventarios.Enabled = False
                Me.dteFecha.Enabled = False
                'Case Actions.Delete
        End Select
        Me.lkpBodega.Enabled = False
        Me.lkpConcepto.Enabled = False


        'INICIALIZA DETAIL
        With Me.tmaMovimientosInventarios
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmMovimientosInventariosDetalle
            '.AddColumn("Partida", "System.Int32")
            .AddColumn("articulo", "System.Int32")
            .AddColumn("descripcion_corta")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("costo", "System.Double")
            .AddColumn("importe", "System.Double")
            .AddColumn("folio_historico_costo", "System.Int32")
            .AddColumn("maneja_series", "System.Boolean")
            .AddColumn("numero_serie")
            .AddColumn("departamento", "System.Int32")
            .AddColumn("grupo", "System.Int32")
            .AddColumn("validanumeroserie", "System.Boolean")
            .AddColumn("entro_pantalla_validaserie", "System.Boolean")
        End With

    End Sub
    Private Sub frmMovimientosInventarios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMovimientosInventarios.Validacion(Action, Me.lkpConcepto.EditValue, Me.grvMovimientosInventarios.RowCount)
    End Sub
    Private Sub frmMovimientosInventarios_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        Dim Costo_Detalle As Double = 0
        Dim folio_historico As Integer
        Me.tmaMovimientosInventarios.MoveFirst()
        Dim cont As Integer = 0
        Dim i As Integer
        Dim blExisteCapa As Boolean

        With tmaMovimientosInventarios
            Do While Not tmaMovimientosInventarios.EOF
                If Me.tmaMovimientosInventarios.Item("cantidad") > 0 Then

                    Select Case tmaMovimientosInventarios.CurrentAction
                        Case Actions.Insert

                            If Me.pTipoconcepto = "S" Then    '''EL MOVIMIENTO ES DE TIPO SALIDA
                                Response = oMovimientosInventariosDetalle.ValidacantidadArticulos(tmaMovimientosInventarios.SelectedRow, Me.lkpBodega.EditValue, Me.lkpConcepto.EditValue, 0, "I", "movimiento")

                                If Response.ErrorFound Then
                                    Exit Sub
                                End If
                                'ultimo costo, cant>=1
                                If Not Response.ErrorFound And AfectaSaldos = False And .Item("maneja_series") = False Then
                                    Costo_Detalle = .Item("costo")
                                    folio_historico = -1
                                End If

                                Dim cantidades As Long
                                cantidades = Me.tmaMovimientosInventarios.Item("cantidad")

                                ' Se metio este ciclo para obtener los costos de los articulos en las salidas, 
                                'ya que ahora se podran capturar cantidades mayores a 1
                                '--------------------------------------------------------------------------------
                                Do While cantidades > 0
                                    TotalCantidades = TotalCantidades + 1

                                    'costo de historial y disminuye, cant = 1
                                    If Not Response.ErrorFound And AfectaSaldos = True And .Item("maneja_series") = False Then
                                        Response = oHistCostos.UltimoCostoArticulo(CLng(.Item("articulo")), Bodega)

                                        If CType(Response.Value, DataSet).Tables(0).Rows.Count > 0 Then
                                            Costo_Detalle = CDbl(CType(Response.Value, DataSet).Tables(0).Rows(0).Item("costo"))
                                            folio_historico = CInt(CType(Response.Value, DataSet).Tables(0).Rows(0).Item("folio"))
                                        Else
                                            Response = oArticulos.DespliegaDatos(CLng(.Item("articulo")))
                                            Costo_Detalle = CDbl(CType(Response.Value, DataSet).Tables(0).Rows(0).Item("ultimo_costo"))
                                            folio_historico = -1
                                        End If

                                    End If

                                    'ultimo costo, cant =1
                                    If AfectaSaldos = False And .Item("maneja_series") = True Then
                                        Response = oArticulos.DespliegaDatos(CLng(.Item("articulo")))
                                        Costo_Detalle = CDbl(CType(Response.Value, DataSet).Tables(0).Rows(0).Item("ultimo_costo"))
                                        folio_historico = -1
                                    End If

                                    'costo de historial y disminute, cant = 1
                                    If Not Response.ErrorFound And AfectaSaldos = True And .Item("maneja_series") = True Then
                                        Response = oHistCostos.UltimoCostoArticulo(CLng(.Item("articulo")), Bodega)

                                        If CType(Response.Value, DataSet).Tables(0).Rows.Count > 0 Then
                                            Costo_Detalle = CDbl(CType(Response.Value, DataSet).Tables(0).Rows(0).Item("costo"))
                                            folio_historico = CInt(CType(Response.Value, DataSet).Tables(0).Rows(0).Item("folio"))
                                        Else
                                            Response = oArticulos.DespliegaDatos(CLng(.Item("articulo")))
                                            Costo_Detalle = CDbl(CType(Response.Value, DataSet).Tables(0).Rows(0).Item("ultimo_costo"))
                                            folio_historico = -1
                                        End If
                                    End If

                                    'SE ACTUALIZA EL SALDO EN EL HISTORICO DE COSTOS
                                    If Not Response.ErrorFound Then
                                        If folio_historico > 0 Then
                                            Response = oHistCostos.ActualizaSaldo(folio_historico, -1)
                                        End If
                                    End If


                                    If Not Response.ErrorFound Then
                                        InsertaDetalle(Response, Costo_Detalle, folio_historico, TotalCantidades, 1)
                                    End If


                                    If Response.ErrorFound Then
                                        Exit Sub
                                    End If

                                    cantidades = cantidades - 1

                                Loop

                                ' Se genera una salida al select para que tome el siguiente articulo.
                                Exit Select


                            Else  'EL MOVIMIENTO ES DE TIPO ENTRADA

                                If .Item("folio_historico_costo") Is System.DBNull.Value Then
                                    .Item("folio_historico_costo") = 0
                                End If

                                If AfectaSaldos Then
                                    If cont = 0 Then

                                        Response = oHistCostos.Insertar(.Item("folio_historico_costo"), CDate(TinApp.FechaServidor), .Item("articulo"), Bodega, .Item("cantidad"), .Item("costo"), .Item("cantidad"))
                                        Costo_Detalle = .Item("costo")
                                        folio_historico = .Item("folio_historico_costo")
                                    Else
                                        i = cont - 1
                                        blExisteCapa = False

                                        '''SE HACE UN RECORRIDO EN LOS ARTICULOS DE ESA ENTRADA PARA VER SI YA SE 
                                        '''HABIA INSERTADO UNA CAPA PARA ESE ARTICULO CON EL MISMO COSTO
                                        Do While i >= 0
                                            If .Item("articulo") = .DataSource.Tables(0).Rows(i).Item("articulo") And .Item("costo") = .DataSource.Tables(0).Rows(i).Item("costo") Then
                                                blExisteCapa = True
                                                Exit Do
                                            End If
                                            i = i - 1
                                        Loop

                                        '''SI EN LA MISMA ENTRADA YA EXISTE UNA CAPA PARA EL ARTICULO CON ESE MISMO COSTO
                                        '''SE ACTUALIZA EL SALDO Y LA CANTIDAD DE ESA CAPA
                                        If blExisteCapa = True Then
                                            folio_historico = .DataSource.Tables(0).Rows(i).Item("folio_historico_costo")
                                            .Item("folio_historico_costo") = folio_historico
                                            Response = oHistCostos.ActualizaSaldoCantidad(folio_historico, .Item("cantidad"), .Item("cantidad"))
                                        Else
                                            Response = oHistCostos.Insertar(.Item("folio_historico_costo"), CDate(TinApp.FechaServidor), .Item("articulo"), Bodega, .Item("cantidad"), .Item("costo"), .Item("cantidad"))
                                            Costo_Detalle = .Item("costo")
                                            folio_historico = .Item("folio_historico_costo")
                                        End If
                                    End If
                                    'Response = oHistCostos.ActualizaSaldo(folio_historico, CInt(tmaMovimientosInventarios.Item("cantidad")))
                                Else
                                    'Response = oArticulos.DespliegaDatos(CLng(.Item("articulo")))
                                    Costo_Detalle = Me.tmaMovimientosInventarios.Item("costo")
                                    folio_historico = -1
                                End If

                            End If
                            '==========================================================================================

                            If Not Response.ErrorFound Then
                                InsertaDetalle(Response, Costo_Detalle, folio_historico, tmaMovimientosInventarios.Item("Partida"), tmaMovimientosInventarios.Item("cantidad"))

                                '    'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
                                '    Response = oMovimientosInventariosDetalle.Insertar(Comunes.Common.Sucursal_Actual, Me.lkpBodega.EditValue, Me.lkpConcepto.EditValue, Me.clcFolio.Value, Me.dteFecha.EditValue, tmaMovimientosInventarios.Item("Partida"), tmaMovimientosInventarios.Item("articulo"), tmaMovimientosInventarios.Item("cantidad"), Costo_Detalle, Costo_Detalle * CLng(.Item("cantidad")), folio_historico, 0, System.DBNull.Value)


                                '    If Not Response.ErrorFound Then
                                '        Response = Me.oArticulos.ActualizarCostosArticuloMovimiento(tmaMovimientosInventarios.Item("articulo"), Costo_Detalle)
                                '    End If


                                '    If Not Response.ErrorFound Then
                                '        If tmaMovimientosInventarios.Item("maneja_series") And tmaMovimientosInventarios.Item("validanumeroserie") = True Then

                                '            If tmaMovimientosInventarios.Item("entro_pantalla_validaserie") = True And Me.pTipoconcepto = "S" Then
                                '                Response = Me.oMovimientosInventarioDetalleSeries.EliminarSerieMVC(tmaMovimientosInventarios.Item("articulo"), tmaMovimientosInventarios.Item("numero_serie"))
                                '                ' CODIGO PARA MOSTRAR UN MESAJE PERSONALIZADO SI HUBO UN ERROR 
                                '                If Response.ErrorFound Then
                                '                    If CType(CType(CType(Response.Ex, System.Exception).InnerException, System.Exception), System.Data.SqlClient.SqlException).Number = 515 Then
                                '                        Response.Message = "NO HAY UNA SERIE VALIDA DEL INVENTARIO INICIAL PARA EL ARTICULO:" & CType(tmaMovimientosInventarios.Item("ARTICULO"), String)
                                '                    End If
                                '                End If
                                '            End If

                                '            If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.Insertar(tmaMovimientosInventarios.SelectedRow, Comunes.Common.Sucursal_Actual, Me.lkpBodega.EditValue, Me.lkpConcepto.EditValue, Me.clcFolio.Value, tmaMovimientosInventarios.Item("Partida"), tmaMovimientosInventarios.Item("articulo"))
                                '        End If
                                '    End If

                            Else
                                Exit Do
                            End If


                    End Select
                    If Response.ErrorFound Then Exit Sub
                End If
                tmaMovimientosInventarios.MoveNext()
                cont = cont + 1

            Loop
        End With
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpConcepto_Format() Handles lkpConcepto.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConcepto)
    End Sub

    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpConcepto.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConcepto.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        ' Response = oBodegas.Lookup(Comunes.Common.Sucursal_Actual)        '//////////mostrar bodegas en base a sucursal, de almacen?/////////
        Response = oBodegas.LookupBodegasUsuarios(TinApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpConcepto_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConcepto.EditValueChanged
        AfectaSaldos = CType(Me.lkpConcepto.GetValue("afecta_saldos"), Boolean)
        Modificable_MovimientosInv = CType(Me.lkpConcepto.GetValue("modificable_movimientosinv"), Boolean)
        pTipoconcepto = Me.lkpConcepto.GetValue("tipo")
    End Sub

    Private Sub frmMovimientosInventarios_ToolBarClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles MyBase.ToolBarClick
        If e.Button Is Me.btnImpresion Then
            Me.ImprimirReporte()
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function AsignarCostoporConcepto(ByVal response As Dipros.Utils.Events) As Double
        Dim ultimo_costo As Double
        Dim costo_pedido_fabrica As Double


        ultimo_costo = CDbl(CType(response.Value, DataSet).Tables(0).Rows(0).Item("ultimo_costo"))
        costo_pedido_fabrica = CDbl(CType(response.Value, DataSet).Tables(0).Rows(0).Item("costo_pedido_fabrica"))

        If ultimo_costo > 0 Then
            Return ultimo_costo
        Else
            Return costo_pedido_fabrica
        End If
    End Function
    Private Function ImprimirReporte()
        Dim response As New Events
        response = oReportes.MovimientoInventario(Bodega, Concepto, Me.clcFolio.Value)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Movimiento al Inventario no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptMovimientoInventario

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                TinApp.ShowReport(Me.MdiParent, "Movimiento al Inventario", oReport, True, , , True)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Function
    Private Sub InsertaDetalle(ByRef Response As Events, ByVal Costo_Detalle As Double, ByVal folio_historico As Long, ByVal Partida As Long, ByVal Cantidad As Long)

        'DAM 24/ABR/07.- SE AGREGO UN NULL EN EL COSTO FLETE 
        Response = oMovimientosInventariosDetalle.Insertar(Comunes.Common.Sucursal_Actual, Me.lkpBodega.EditValue, Me.lkpConcepto.EditValue, Me.clcFolio.Value, Me.dteFecha.EditValue, Partida, tmaMovimientosInventarios.Item("articulo"), Cantidad, Costo_Detalle, Costo_Detalle * Cantidad, folio_historico, 0, System.DBNull.Value)


        If Not Response.ErrorFound Then
            Response = Me.oArticulos.ActualizarCostosArticuloMovimiento(tmaMovimientosInventarios.Item("articulo"), Costo_Detalle)
        End If


        If Not Response.ErrorFound Then
            If tmaMovimientosInventarios.Item("maneja_series") And tmaMovimientosInventarios.Item("validanumeroserie") = True Then

                If tmaMovimientosInventarios.Item("entro_pantalla_validaserie") = True And Me.pTipoconcepto = "S" Then
                    Response = Me.oMovimientosInventarioDetalleSeries.EliminarSerieMVC(tmaMovimientosInventarios.Item("articulo"), tmaMovimientosInventarios.Item("numero_serie"))
                    ' CODIGO PARA MOSTRAR UN MESAJE PERSONALIZADO SI HUBO UN ERROR 
                    If Response.ErrorFound Then
                        If CType(CType(CType(Response.Ex, System.Exception).InnerException, System.Exception), System.Data.SqlClient.SqlException).Number = 515 Then
                            Response.Message = "NO HAY UNA SERIE VALIDA DEL INVENTARIO INICIAL PARA EL ARTICULO:" & CType(tmaMovimientosInventarios.Item("ARTICULO"), String)
                        End If
                    End If
                End If

                If Not Response.ErrorFound Then Response = oMovimientosInventarioDetalleSeries.Insertar(tmaMovimientosInventarios.SelectedRow, Comunes.Common.Sucursal_Actual, Me.lkpBodega.EditValue, Me.lkpConcepto.EditValue, Me.clcFolio.Value, Partida, tmaMovimientosInventarios.Item("articulo"))
            End If
        End If

    End Sub

#End Region

 
    

End Class
