Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmMovimientosInventariosDetalle
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim Sucursal As Long
    Dim Bodega As String
    Dim Concepto As String
    Dim Folio As Long
    Dim Fecha As Date
    Dim KS As Keys

    Dim Cantidad1 As Boolean
    Dim AfectaSaldos As Boolean
    Dim ArticuloManejaSeries As Boolean
    Private articulo_temporal As Long = 0

#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents clcCosto As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lbldescripcion As System.Windows.Forms.Label
    Friend WithEvents clcFolioHist As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents chkManejaSeries As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents chkvalidanumeroserie As System.Windows.Forms.CheckBox
    Friend WithEvents chkentro_pantalla_validaserie As System.Windows.Forms.CheckBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosInventariosDetalle))
        Me.lblPartida = New System.Windows.Forms.Label
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.lblCosto = New System.Windows.Forms.Label
        Me.clcCosto = New Dipros.Editors.TINCalcEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lbldescripcion = New System.Windows.Forms.Label
        Me.clcFolioHist = New DevExpress.XtraEditors.CalcEdit
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.lblSerie = New System.Windows.Forms.Label
        Me.chkManejaSeries = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.chkvalidanumeroserie = New System.Windows.Forms.CheckBox
        Me.chkentro_pantalla_validaserie = New System.Windows.Forms.CheckBox
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioHist.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(52, 43)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 0
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "&Partida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(105, 41)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(46, 19)
        Me.clcPartida.TabIndex = 1
        Me.clcPartida.Tag = "Partida"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(49, 112)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 6
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "&Articulo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = False
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(105, 112)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(470, Long)
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(178, 20)
        Me.lkpArticulo.TabIndex = 7
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "articulo"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(42, 168)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(58, 16)
        Me.lblCantidad.TabIndex = 10
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "&Cantidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCantidad.Location = New System.Drawing.Point(105, 160)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "########0"
        Me.clcCantidad.Properties.MaxLength = 5
        Me.clcCantidad.Properties.Precision = 18
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Size = New System.Drawing.Size(46, 19)
        Me.clcCantidad.TabIndex = 11
        Me.clcCantidad.Tag = "cantidad"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(60, 189)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(40, 16)
        Me.lblCosto.TabIndex = 12
        Me.lblCosto.Tag = ""
        Me.lblCosto.Text = "&Costo:"
        Me.lblCosto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCosto
        '
        Me.clcCosto.EditValue = "0"
        Me.clcCosto.Location = New System.Drawing.Point(105, 184)
        Me.clcCosto.MaxValue = 0
        Me.clcCosto.MinValue = 0
        Me.clcCosto.Name = "clcCosto"
        '
        'clcCosto.Properties
        '
        Me.clcCosto.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcCosto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCosto.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCosto.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcCosto.Properties.Precision = 2
        Me.clcCosto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCosto.Size = New System.Drawing.Size(78, 19)
        Me.clcCosto.TabIndex = 13
        Me.clcCosto.Tag = "costo"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(47, 208)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 14
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(105, 208)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(78, 19)
        Me.clcImporte.TabIndex = 15
        Me.clcImporte.Tag = "importe"
        '
        'lbldescripcion
        '
        Me.lbldescripcion.Location = New System.Drawing.Point(105, 136)
        Me.lbldescripcion.Name = "lbldescripcion"
        Me.lbldescripcion.Size = New System.Drawing.Size(286, 20)
        Me.lbldescripcion.TabIndex = 9
        Me.lbldescripcion.Tag = "descripcion_corta"
        Me.lbldescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'clcFolioHist
        '
        Me.clcFolioHist.Location = New System.Drawing.Point(287, 184)
        Me.clcFolioHist.Name = "clcFolioHist"
        '
        'clcFolioHist.Properties
        '
        Me.clcFolioHist.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.clcFolioHist.Properties.NullText = "0"
        Me.clcFolioHist.Size = New System.Drawing.Size(75, 20)
        Me.clcFolioHist.TabIndex = 19
        Me.clcFolioHist.TabStop = False
        Me.clcFolioHist.Tag = "folio_historico_costo"
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(105, 232)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.MaxLength = 30
        Me.txtSerie.Size = New System.Drawing.Size(148, 20)
        Me.txtSerie.TabIndex = 17
        Me.txtSerie.Tag = "numero_serie"
        Me.txtSerie.Visible = False
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(59, 232)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(41, 16)
        Me.lblSerie.TabIndex = 16
        Me.lblSerie.Tag = ""
        Me.lblSerie.Text = "&Serie :"
        Me.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSerie.Visible = False
        '
        'chkManejaSeries
        '
        Me.chkManejaSeries.Location = New System.Drawing.Point(287, 160)
        Me.chkManejaSeries.Name = "chkManejaSeries"
        Me.chkManejaSeries.Size = New System.Drawing.Size(104, 16)
        Me.chkManejaSeries.TabIndex = 18
        Me.chkManejaSeries.TabStop = False
        Me.chkManejaSeries.Tag = "maneja_series"
        Me.chkManejaSeries.Text = "Maneja Series"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Tag = ""
        Me.Label2.Text = "Descripci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(105, 87)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(178, 20)
        Me.lkpGrupo.TabIndex = 5
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = Nothing
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(57, 90)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 4
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(105, 64)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(178, 20)
        Me.lkpDepartamento.TabIndex = 3
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(12, 67)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 2
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkvalidanumeroserie
        '
        Me.chkvalidanumeroserie.Enabled = False
        Me.chkvalidanumeroserie.Location = New System.Drawing.Point(120, 280)
        Me.chkvalidanumeroserie.Name = "chkvalidanumeroserie"
        Me.chkvalidanumeroserie.Size = New System.Drawing.Size(104, 16)
        Me.chkvalidanumeroserie.TabIndex = 59
        Me.chkvalidanumeroserie.TabStop = False
        Me.chkvalidanumeroserie.Tag = "validanumeroserie"
        Me.chkvalidanumeroserie.Text = "validanumeroserie"
        '
        'chkentro_pantalla_validaserie
        '
        Me.chkentro_pantalla_validaserie.Enabled = False
        Me.chkentro_pantalla_validaserie.Location = New System.Drawing.Point(120, 296)
        Me.chkentro_pantalla_validaserie.Name = "chkentro_pantalla_validaserie"
        Me.chkentro_pantalla_validaserie.Size = New System.Drawing.Size(104, 16)
        Me.chkentro_pantalla_validaserie.TabIndex = 60
        Me.chkentro_pantalla_validaserie.TabStop = False
        Me.chkentro_pantalla_validaserie.Tag = "entro_pantalla_validaserie"
        Me.chkentro_pantalla_validaserie.Text = "entro_pantalla"
        '
        'frmMovimientosInventariosDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(402, 264)
        Me.Controls.Add(Me.chkentro_pantalla_validaserie)
        Me.Controls.Add(Me.chkvalidanumeroserie)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.chkManejaSeries)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.clcFolioHist)
        Me.Controls.Add(Me.lbldescripcion)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.clcCantidad)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.clcCosto)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Name = "frmMovimientosInventariosDetalle"
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.clcCosto, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lbldescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcFolioHist, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.txtSerie, 0)
        Me.Controls.SetChildIndex(Me.chkManejaSeries, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.chkvalidanumeroserie, 0)
        Me.Controls.SetChildIndex(Me.chkentro_pantalla_validaserie, 0)
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioHist.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oHistCostos As New VillarrealBusiness.clsHisCostos
    Private oMovimientosInventariosDetalle As New VillarrealBusiness.clsMovimientosInventariosDetalle
    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property

    Private Modelo As String = ""
    Private pTipoconcepto As Char = ""

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmMovimientosInventariosDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmMovimientosInventariosDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.lkpDepartamento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("departamento")), Long)
        Me.lkpGrupo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("grupo")), Long)
        Me.lkpArticulo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("articulo")), Long)

    End Sub

    Private Sub frmMovimientosInventariosDetalle_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        AfectaSaldos = OwnerForm.AfectaSaldos

        Select Case Action
            Case Actions.Insert
                Me.clcPartida.Enabled = False

                Me.clcCosto.Enabled = False
                Me.clcImporte.Enabled = False
            Case Actions.Update
                Me.clcPartida.Enabled = False
                Me.lkpDepartamento.Enabled = False
                Me.lkpGrupo.Enabled = False
                Me.lkpArticulo.Enabled = False
                Me.clcCosto.Enabled = False
                Me.clcImporte.Enabled = False
            Case Actions.Delete
                Me.clcPartida.Enabled = False
                Me.lkpArticulo.Enabled = False
                Me.clcCosto.Enabled = False
                Me.clcImporte.Enabled = False
                Me.clcCantidad.Enabled = False
        End Select

        pTipoconcepto = CType(OwnerForm, frmMovimientosInventarios).pTipoconcepto

        If pTipoconcepto = "E" Then
            Me.clcCosto.Enabled = True
        Else
            Me.clcCosto.Enabled = False
        End If

    End Sub

    Private Sub frmMovimientosInventariosDetalle_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields
        If ValidaSerieCorrecta(Response) Then
            Response = oMovimientosInventariosDetalle.Validacion(Articulo, Me.clcCantidad.Value, Me.txtSerie.Visible, Me.txtSerie.Text, Me.clcCosto.Value, pTipoconcepto)
        End If
    End Sub

    Private Sub frmMovimientosInventariosDetalle_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))
    End Sub

    Private Sub frmMovimientosInventariosDetalle_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.clcFolioHist.Visible = False
        Me.chkManejaSeries.Visible = False
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo_FilterData(-1)
        Me.lkpGrupo_InitData(-1)
        Me.lbldescripcion.Text = ""

    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpArticulo.EditValue = Nothing
        'Me.lkpArticulo_FilterData(-1)
        'Me.lkpArticulo_InitData(-1)
        Me.lkpArticulo_LoadData(True)
        Me.lbldescripcion.Text = ""

    End Sub
    Private Sub lkpGrupo_FilterData(ByVal Value As Object) Handles lkpGrupo.FilterData
        Dim Response As New Events
        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
        End If
    End Sub
    Private Sub lkpGrupo_InitData(ByVal Value As Object) Handles lkpGrupo.InitData
        Dim Response As New Events

        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then
                Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            End If

        End If
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData
        Dim Response As New Events
        Response = oArticulos.Lookup(Departamento, Grupo, 0)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)
        End If
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    'Private Sub lkpArticulo_FilterData(ByVal Value As Object) Handles lkpArticulo.FilterData
    'Dim Response As New Events
    ' Response = oArticulos.Lookup(Departamento, Grupo, 0, Value)
    ' If Not Response.ErrorFound Then
    ' Dim oDataSet As DataSet
    ' oDataSet = Response.Value
    ' Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    'End If
    'End Sub

    'Private Sub lkpArticulo_InitData(ByVal Value As Object) Handles lkpArticulo.InitData
    'Dim Response As New Events

    'Response = oArticulos.Lookup(Departamento, Grupo, 0, , Value)
    'If Not Response.ErrorFound Then
    'Dim oDataSet As DataSet
    ' oDataSet = Response.Value
    'If oDataSet.Tables(0).Rows.Count > 0 Then
    '   Me.lkpArticulo.DataSource = oDataSet.Tables(0)
    ' End If

    'End If
    'End Sub

    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        If Me.lkpArticulo.DataSource Is Nothing Or Me.lkpArticulo.EditValue Is Nothing Then
            Me.clcCantidad.Value = 0
            Me.clcCantidad.Enabled = False
            Exit Sub
        Else
            Me.clcCantidad.Enabled = True
        End If

        If articulo_temporal <> Articulo Then articulo_temporal = Articulo
        Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
        Me.lkpArticulo.EditValue = articulo_temporal
        Me.lbldescripcion.Text = lkpArticulo.GetValue("descripcion_corta")

        Modelo = lkpArticulo.GetValue("modelo")
        DespliegueCosto_ValidaCantidad()

        Me.chkManejaSeries.Checked = lkpArticulo.GetValue("maneja_series")
        'si maneja series
        'DAM -- REVISA MANEJO SERIES OCT-08
        If lkpArticulo.GetValue("maneja_series") And Comunes.clsUtilerias.UsarSeries = True Then
            lblSerie.Visible = True
            txtSerie.Visible = True
            Me.clcCantidad.Value = 1
            Me.clcCantidad.Enabled = False
        Else
            lblSerie.Visible = False
            txtSerie.Visible = False
        End If

    End Sub

    Private Sub clcCantidad_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcCantidad.EditValueChanged
        RecalculaImporte()
    End Sub
    Private Sub clcCosto_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcCosto.EditValueChanged
        RecalculaImporte()
    End Sub

    Private Sub txtSerie_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSerie.KeyDown
        Me.chkvalidanumeroserie.Checked = False
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function DespliegueCosto_ValidaCantidad()
        Dim response As New Events

        'DAM -- REVISA MANEJO SERIES OCT-08
        If Comunes.clsUtilerias.UsarSeries = True Then
            ArticuloManejaSeries = CType(Me.lkpArticulo.GetValue("maneja_series"), Boolean)
        Else
            ArticuloManejaSeries = False
        End If

        '=======================================================================================================
        'Sin Manejo de Series
        '=======================================================================================================
       

        If pTipoconcepto = "E" Then
           

            response = oArticulos.DespliegaDatos(Articulo)
            Me.AsignarCostoporConcepto(response)

        Else

            'ultimo costo, cant>=1
            If AfectaSaldos = False And ArticuloManejaSeries = False Then
                Me.clcCantidad.Enabled = True

                Cantidad1 = False
                response = oArticulos.DespliegaDatos(Articulo)
                Me.AsignarCostoporConcepto(response)

            End If

            'costo de historial y disminuye, cant = 1
            If AfectaSaldos = True And ArticuloManejaSeries = False Then
                'Se comento esta linea para usar el campo cantidad para las Salidas
                '15 Junio 2010

                'Me.clcCantidad.Enabled = False
                Me.clcCantidad.EditValue = 1

                Cantidad1 = True
                response = oHistCostos.UltimoCostoArticulo(Articulo, CType(OwnerForm, frmMovimientosInventarios).Bodega)

                If CType(response.Value, DataSet).Tables(0).Rows.Count > 0 Then
                    clcCosto.Value = CDbl(CType(response.Value, DataSet).Tables(0).Rows(0).Item("costo"))
                    Me.clcFolioHist.Value = CInt(CType(response.Value, DataSet).Tables(0).Rows(0).Item("folio"))
                Else

                    response = oArticulos.DespliegaDatos(Articulo)
                    Me.AsignarCostoporConcepto(response)
                    Me.clcFolioHist.Value = -1
                End If

            End If

            '=======================================================================================================
            'Con Manejo de Series
            '=======================================================================================================

            ''ultimo costo, cant =1
            'If AfectaSaldos = False And ArticuloManejaSeries = True Then
            '    Me.clcCantidad.Enabled = False
            '    Me.clcCantidad.EditValue = 1

            '    Cantidad1 = True
            '    response = oArticulos.DespliegaDatos(Articulo)
            '    clcCosto.Value = CDbl(CType(response.Value, DataSet).Tables(0).Rows(0).Item("ultimo_costo"))

            'End If

            ''costo de historial y disminute, cant = 1
            'If AfectaSaldos = True And ArticuloManejaSeries = True Then
            '    Me.clcCantidad.Enabled = False
            '    Me.clcCantidad.EditValue = 1

            '    Cantidad1 = True
            '    response = oHistCostos.UltimoCostoArticulo(Articulo, CType(OwnerForm, frmMovimientosInventarios).Bodega)


            '    If CType(response.Value, DataSet).Tables(0).Rows.Count > 0 Then
            '        clcCosto.Value = CDbl(CType(response.Value, DataSet).Tables(0).Rows(0).Item("costo"))
            '        Me.clcFolioHist.Value = CInt(CType(response.Value, DataSet).Tables(0).Rows(0).Item("folio"))
            '    Else
            '        clcCosto.Value = 0
            '        Me.clcFolioHist.Value = -1
            '    End If

            'End If
        End If

    End Function
    Private Sub RecalculaImporte()
        Me.clcImporte.Value = Me.clcCantidad.Value * Me.clcCosto.Value
    End Sub
    Private Function ValidaSerieCorrecta(ByRef response_salida As Events) As Boolean
        Dim Response As Events
        If Me.txtSerie.Visible And Me.txtSerie.Text.Trim.Length > 0 And Me.chkvalidanumeroserie.Checked = False Then

            Dim ValidaHisSeries As Boolean
            ValidaHisSeries = Comunes.clsUtilerias.uti_hisSeries(Articulo, Me.txtSerie.Text, OwnerForm.pTipoconcepto, CType(OwnerForm, frmMovimientosInventarios).Bodega)
            Response = oMovimientosInventariosDetalle.ValidaSerieHisSeries(ValidaHisSeries)

            response_salida = Response

            If Response.ErrorFound Then

                Dim OFORM As New Comunes.frmRepartosValidaNumeroSerie
                OFORM.Articulo = Articulo
                OFORM.modelo = Modelo
                OFORM.Fila = -1 'en este proceso no se ocupa fila

                OFORM.OwnerForm = Me
                OFORM.MdiParent = Me.MdiParent
                Me.Enabled = False
                OFORM.Show()
            Else
                Me.chkvalidanumeroserie.Checked = True
            End If
        Else
            Return True

        End If
    End Function
    Public Sub LLenarNumeroSerieValido(ByVal fila As Long, ByVal articulo As Long, ByVal numero_serie As String, ByVal validanumeroserie As Boolean)

        Me.chkvalidanumeroserie.Checked = validanumeroserie
        If validanumeroserie Then
            Me.txtSerie.Text = numero_serie
            Me.chkentro_pantalla_validaserie.Checked = True
        End If

    End Sub

    Private Sub AsignarCostoporConcepto(ByVal response As Dipros.Utils.Events)
        Dim ultimo_costo As Double
        Dim costo_pedido_fabrica As Double


        ultimo_costo = CDbl(CType(response.Value, DataSet).Tables(0).Rows(0).Item("ultimo_costo"))
        costo_pedido_fabrica = CDbl(CType(response.Value, DataSet).Tables(0).Rows(0).Item("costo_pedido_fabrica"))

        If ultimo_costo > 0 Then
            Me.clcCosto.Value = ultimo_costo
        Else
            Me.clcCosto.Value = costo_pedido_fabrica
        End If
    End Sub
#End Region

End Class
