Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class brwMovimientosInventarios
    Inherits Dipros.Windows.frmTINGridNet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents dteFechai As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFechaf As DevExpress.XtraEditors.DateEdit
    Public WithEvents lkpConcepto As Dipros.Editors.TINMultiLookup
    Public WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.dteFechai = New DevExpress.XtraEditors.DateEdit
        Me.dteFechaf = New DevExpress.XtraEditors.DateEdit
        Me.lkpConcepto = New Dipros.Editors.TINMultiLookup
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FilterPanel.SuspendLayout()
        CType(Me.dteFechai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.lkpConcepto)
        Me.FilterPanel.Controls.Add(Me.lkpBodega)
        Me.FilterPanel.Controls.Add(Me.dteFechaf)
        Me.FilterPanel.Controls.Add(Me.dteFechai)
        Me.FilterPanel.Controls.Add(Me.lblConcepto)
        Me.FilterPanel.Controls.Add(Me.lblBodega)
        Me.FilterPanel.Controls.Add(Me.Label6)
        Me.FilterPanel.Controls.Add(Me.Label5)
        Me.FilterPanel.Controls.Add(Me.Label4)
        Me.FilterPanel.Controls.Add(Me.Label3)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(752, 85)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label3, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label4, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label5, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label6, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblConcepto, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.dteFechai, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.dteFechaf, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpConcepto, 0)
        '
        'FooterPanel
        '
        Me.FooterPanel.Location = New System.Drawing.Point(0, 361)
        Me.FooterPanel.Name = "FooterPanel"
        Me.FooterPanel.Size = New System.Drawing.Size(752, 36)
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "&Bodega:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "C&oncepto:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(16, 58)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 16)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "&Fecha:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(224, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(16, 16)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "A&l"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBodega
        '
        Me.lblBodega.Location = New System.Drawing.Point(352, 8)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(264, 16)
        Me.lblBodega.TabIndex = 7
        '
        'lblConcepto
        '
        Me.lblConcepto.Location = New System.Drawing.Point(352, 32)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(264, 16)
        Me.lblConcepto.TabIndex = 8
        '
        'dteFechai
        '
        Me.dteFechai.EditValue = New Date(2006, 3, 17, 0, 0, 0, 0)
        Me.dteFechai.Location = New System.Drawing.Point(80, 56)
        Me.dteFechai.Name = "dteFechai"
        '
        'dteFechai.Properties
        '
        Me.dteFechai.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechai.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechai.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechai.Properties.ShowClear = False
        Me.dteFechai.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFechai.Size = New System.Drawing.Size(96, 20)
        Me.dteFechai.TabIndex = 6
        '
        'dteFechaf
        '
        Me.dteFechaf.EditValue = New Date(2006, 3, 17, 0, 0, 0, 0)
        Me.dteFechaf.Location = New System.Drawing.Point(248, 56)
        Me.dteFechaf.Name = "dteFechaf"
        '
        'dteFechaf.Properties
        '
        Me.dteFechaf.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaf.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaf.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaf.Properties.ShowClear = False
        Me.dteFechaf.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.dteFechaf.Size = New System.Drawing.Size(96, 20)
        Me.dteFechaf.TabIndex = 8
        '
        'lkpConcepto
        '
        Me.lkpConcepto.AllowAdd = False
        Me.lkpConcepto.AutoReaload = False
        Me.lkpConcepto.DataSource = Nothing
        Me.lkpConcepto.DefaultSearchField = ""
        Me.lkpConcepto.DisplayMember = "descripcion"
        Me.lkpConcepto.EditValue = Nothing
        Me.lkpConcepto.Filtered = False
        Me.lkpConcepto.InitValue = Nothing
        Me.lkpConcepto.Location = New System.Drawing.Point(80, 32)
        Me.lkpConcepto.MultiSelect = False
        Me.lkpConcepto.Name = "lkpConcepto"
        Me.lkpConcepto.NullText = ""
        Me.lkpConcepto.PopupWidth = CType(400, Long)
        Me.lkpConcepto.ReadOnlyControl = False
        Me.lkpConcepto.Required = False
        Me.lkpConcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConcepto.SearchMember = ""
        Me.lkpConcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConcepto.SelectAll = False
        Me.lkpConcepto.Size = New System.Drawing.Size(264, 20)
        Me.lkpConcepto.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConcepto.TabIndex = 4
        Me.lkpConcepto.Tag = "concepto"
        Me.lkpConcepto.ToolTip = Nothing
        Me.lkpConcepto.ValueMember = "concepto"
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(80, 8)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(264, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 2
        Me.lkpBodega.Tag = "bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "bodega"
        '
        'brwMovimientosInventarios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CanDelete = False
        Me.ClientSize = New System.Drawing.Size(752, 397)
        Me.Name = "brwMovimientosInventarios"
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.dteFechai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oMovimientosInventarios As New VillarrealBusiness.clsMovimientosInventarios
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private oConceptosInventario As New VillarrealBusiness.clsConceptosInventario
    Public banValidacion As Boolean
    Public banModificarConcepto As Boolean = False

#Region "Eventos de la Forma"
    Private Sub brwMovimientosInventarios_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      

        FooterPanel.Visible = False
        dteFechai.EditValue = CDate(TinApp.FechaServidor())
        dteFechaf.EditValue = CDate(TinApp.FechaServidor())
        lkpBodega.EditValue = Comunes.clsUtilerias.uti_BodegaDeUsuario(TinApp.Connection.User)
    End Sub

    Private Sub brwMovimientosInventarios_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        If Validacion() Then Exit Sub
        Response = oMovimientosInventarios.Listado(Me.lkpBodega.EditValue, Me.lkpConcepto.EditValue, CDate(dteFechai.Text & " 00:00:00"), CDate(dteFechaf.Text & " 23:59:59"))
    End Sub

    Private Sub brwMovimientosInventarios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        If Me.lkpBodega.Text.Length = 0 Then
            '    Response.Message = "La Bodega es requerida"
            Dim oevent As New Events
            oevent.Layer = Dipros.Utils.Events.ErrorLayer.InterfaceLayer
            oevent.Message = "La Bodega es requerida"
            Response = oevent
            '   ShowMessage(MessageType.MsgInformation, "La Bodega es requerida")
            banValidacion = True
            Exit Sub
        Else
            banValidacion = False
        End If
        If Me.lkpConcepto.Text.Length = 0 Then
            Dim oevent As New Events
            oevent.Layer = Dipros.Utils.Events.ErrorLayer.InterfaceLayer
            oevent.Message = "El Concepto es requerido"
            Response = oevent
            'ShowMessage(MessageType.MsgInformation, "El Concepto es requerido")
            banValidacion = True
            Exit Sub
        Else
            banValidacion = False
        End If
    End Sub

#End Region
   


#Region "Eventos de Controles"
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TinApp.Connection.User)         '//////////mostrar bodegas en base a sucursal, de almacen?/////////
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodega_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodega.EditValueChanged
        Reload()
    End Sub

    Private Sub lkpConcepto_Format() Handles lkpConcepto.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpConcepto)
    End Sub
    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpConcepto.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConcepto.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpConcepto_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpConcepto.EditValueChanged
        Me.banModificarConcepto = Me.lkpConcepto.GetValue("modificable_movimientosinv")
        If banModificarConcepto = True Then
            Me.CanInsert = True
        Else
            Me.CanInsert = False
        End If
        Reload()

    End Sub

    Private Sub dteFechaf_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFechaf.EditValueChanged
        Reload()
    End Sub
    Private Sub dteFechai_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFechai.EditValueChanged
        Reload()
    End Sub

#End Region

#Region "funcionalidad"
    Private Function Validacion() As Boolean
        Validacion = True
        If Me.lkpBodega.Text = "" Then Exit Function
        If Me.lkpConcepto.Text = "" Then Exit Function
        Validacion = False
    End Function
#End Region

End Class
