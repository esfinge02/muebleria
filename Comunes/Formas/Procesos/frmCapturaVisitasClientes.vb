Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCapturaVisitasClientes
    Inherits Dipros.Windows.frmTINForm



#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio_Captura As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Captura As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dteFechaCorte As DevExpress.XtraEditors.DateEdit
    Friend WithEvents clcSucursal As DevExpress.XtraEditors.CalcEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCapturaVisitasClientes))
        Me.lblFolio_Captura = New System.Windows.Forms.Label
        Me.clcFolio_Captura = New Dipros.Editors.TINCalcEdit
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.dteFechaCorte = New DevExpress.XtraEditors.DateEdit
        Me.clcSucursal = New DevExpress.XtraEditors.CalcEdit
        CType(Me.clcFolio_Captura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(119, 50)
        '
        'lblFolio_Captura
        '
        Me.lblFolio_Captura.AutoSize = True
        Me.lblFolio_Captura.Location = New System.Drawing.Point(40, 40)
        Me.lblFolio_Captura.Name = "lblFolio_Captura"
        Me.lblFolio_Captura.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio_Captura.TabIndex = 0
        Me.lblFolio_Captura.Tag = ""
        Me.lblFolio_Captura.Text = "&Folio:"
        Me.lblFolio_Captura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Captura
        '
        Me.clcFolio_Captura.EditValue = "0"
        Me.clcFolio_Captura.Location = New System.Drawing.Point(80, 40)
        Me.clcFolio_Captura.MaxValue = 0
        Me.clcFolio_Captura.MinValue = 0
        Me.clcFolio_Captura.Name = "clcFolio_Captura"
        '
        'clcFolio_Captura.Properties
        '
        Me.clcFolio_Captura.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Captura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Captura.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Captura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Captura.Properties.Enabled = False
        Me.clcFolio_Captura.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Captura.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Captura.Size = New System.Drawing.Size(72, 19)
        Me.clcFolio_Captura.TabIndex = 1
        Me.clcFolio_Captura.Tag = "folio_captura"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(16, 120)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 4
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCobrador.Visible = False
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = False
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Enabled = False
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(80, 120)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = ""
        Me.lkpCobrador.PopupWidth = CType(400, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = False
        Me.lkpCobrador.Size = New System.Drawing.Size(288, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 5
        Me.lkpCobrador.Tag = "Cobrador"
        Me.lkpCobrador.ToolTip = Nothing
        Me.lkpCobrador.ValueMember = "Cobrador"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(24, 64)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 2
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(80, 63)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(288, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 3
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(32, 88)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "29/05/2008"
        Me.dteFecha.Location = New System.Drawing.Point(80, 88)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 7
        Me.dteFecha.Tag = "fecha"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Tag = ""
        Me.Label2.Text = "Fecha Corte:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label2.Visible = False
        '
        'dteFechaCorte
        '
        Me.dteFechaCorte.EditValue = "29/05/2008"
        Me.dteFechaCorte.Location = New System.Drawing.Point(80, 136)
        Me.dteFechaCorte.Name = "dteFechaCorte"
        '
        'dteFechaCorte.Properties
        '
        Me.dteFechaCorte.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCorte.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCorte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaCorte.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCorte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaCorte.Properties.Enabled = False
        Me.dteFechaCorte.Size = New System.Drawing.Size(95, 20)
        Me.dteFechaCorte.TabIndex = 9
        Me.dteFechaCorte.Tag = "fecha_corte"
        Me.dteFechaCorte.Visible = False
        '
        'clcSucursal
        '
        Me.clcSucursal.EditValue = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.clcSucursal.Location = New System.Drawing.Point(208, 144)
        Me.clcSucursal.Name = "clcSucursal"
        '
        'clcSucursal.Properties
        '
        Me.clcSucursal.Properties.Enabled = False
        Me.clcSucursal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSucursal.Size = New System.Drawing.Size(75, 20)
        Me.clcSucursal.TabIndex = 59
        Me.clcSucursal.TabStop = False
        Me.clcSucursal.Tag = "sucursal"
        '
        'frmCapturaVisitasClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(378, 120)
        Me.Controls.Add(Me.clcSucursal)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dteFechaCorte)
        Me.Controls.Add(Me.lblFolio_Captura)
        Me.Controls.Add(Me.clcFolio_Captura)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.lkpCobrador)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Name = "frmCapturaVisitasClientes"
        Me.Text = "Captura de Visitas"
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.clcFolio_Captura, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Captura, 0)
        Me.Controls.SetChildIndex(Me.dteFechaCorte, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcSucursal, 0)
        CType(Me.clcFolio_Captura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCapturaVisitasClientes As VillarrealBusiness.clsCapturaVisitasClientes
    Private oCobradores As VillarrealBusiness.clsCobradores
    Private oClientesCobradores As VillarrealBusiness.clsClientesCobradores
    Private oClientes As VillarrealBusiness.clsClientes

    Private Property Cobrador() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCobrador)
        End Get
        Set(ByVal Value As Long)
            Me.lkpCobrador.EditValue = Value
        End Set
    End Property

    Private Property FechaCorte() As DateTime
        Get
            Return Me.dteFechaCorte.DateTime
        End Get
        Set(ByVal Value As DateTime)
            Me.dteFechaCorte.EditValue = Value
        End Set
    End Property


    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCapturaVisitasClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oCapturaVisitasClientes.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oCapturaVisitasClientes.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oCapturaVisitasClientes.Eliminar(clcFolio_Captura.value)

        End Select
    End Sub

    Private Sub frmCapturaVisitasClientes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oCapturaVisitasClientes.DespliegaDatos(OwnerForm.Value("folio_captura"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            If Me.Cobrador = -1 And oDataSet.Tables(0).Rows.Count > 0 Then
                Me.lkpCobrador.EditValue = oDataSet.Tables(0).Rows(0).Item("cobrador")

            End If
        End If

    End Sub

    Private Sub frmCapturaVisitasClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCapturaVisitasClientes = New VillarrealBusiness.clsCapturaVisitasClientes
        oClientes = New VillarrealBusiness.clsClientes
        oCobradores = New VillarrealBusiness.clsCobradores
        oClientesCobradores = New VillarrealBusiness.clsClientesCobradores
        Me.Cobrador = CType(OwnerForm, brwCapturaVisitasClientes).Cobrador
        Me.FechaCorte = CType(OwnerForm, brwCapturaVisitasClientes).FechaCorte
        Me.clcSucursal.Value = CType(OwnerForm, brwCapturaVisitasClientes).Sucursal

        Me.dteFecha.EditValue = CType(Me.OwnerForm, brwCapturaVisitasClientes).dteFechaCorte.DateTime    'CDate(TINApp.FechaServidor)
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmCapturaVisitasClientes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCapturaVisitasClientes.Validacion(Action, Cobrador, Cliente, Me.dteFechaCorte.Text, Me.DataSource)
    End Sub

    Private Sub frmCapturaVisitasClientes_Localize() Handles MyBase.Localize
        Find(clcFolio_Captura.Tag, clcFolio_Captura.Value)

    End Sub

    Private Sub frmCapturaVisitasClientes_ReInitialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.ReInitialize
        Me.Cobrador = CType(OwnerForm, brwCapturaVisitasClientes).Cobrador
        Me.FechaCorte = CType(OwnerForm, brwCapturaVisitasClientes).FechaCorte
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        'Response = oClientes.LookupCliente()
        Response = oClientesCobradores.Lookup(Cobrador, Me.clcSucursal.Value)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub

    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub
    Private Sub lkpCobrador_LoadData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData
        Dim Response As New Events
        'Response = oClientesCobradores.DespliegaDatosCobradoresVentas(Cliente, Comunes.Common.Sucursal_Actual)
        Response = oCobradores.Lookup(Comunes.Common.Sucursal_Actual)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


   
End Class
