Imports Dipros.Utils

Public Class frmRepartosValidaNumeroSerie
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Private WithEvents txtmodelo As DevExpress.XtraEditors.TextEdit
    Private WithEvents clcArticulo As DevExpress.XtraEditors.CalcEdit
    Private WithEvents txtNumeroSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtEtiqueta As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepartosValidaNumeroSerie))
        Me.txtmodelo = New DevExpress.XtraEditors.TextEdit
        Me.clcArticulo = New DevExpress.XtraEditors.CalcEdit
        Me.txtNumeroSerie = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtEtiqueta = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcArticulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEtiqueta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(269, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'txtmodelo
        '
        Me.txtmodelo.EditValue = ""
        Me.txtmodelo.Location = New System.Drawing.Point(104, 74)
        Me.txtmodelo.Name = "txtmodelo"
        '
        'txtmodelo.Properties
        '
        Me.txtmodelo.Properties.Enabled = False
        Me.txtmodelo.Properties.MaxLength = 18
        Me.txtmodelo.Size = New System.Drawing.Size(120, 20)
        Me.txtmodelo.TabIndex = 3
        '
        'clcArticulo
        '
        Me.clcArticulo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcArticulo.Location = New System.Drawing.Point(104, 48)
        Me.clcArticulo.Name = "clcArticulo"
        '
        'clcArticulo.Properties
        '
        Me.clcArticulo.Properties.Enabled = False
        Me.clcArticulo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcArticulo.Size = New System.Drawing.Size(75, 20)
        Me.clcArticulo.TabIndex = 1
        '
        'txtNumeroSerie
        '
        Me.txtNumeroSerie.EditValue = ""
        Me.txtNumeroSerie.Location = New System.Drawing.Point(104, 126)
        Me.txtNumeroSerie.Name = "txtNumeroSerie"
        '
        'txtNumeroSerie.Properties
        '
        Me.txtNumeroSerie.Properties.MaxLength = 30
        Me.txtNumeroSerie.Size = New System.Drawing.Size(192, 20)
        Me.txtNumeroSerie.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(46, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Articulo:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "N�mero  Serie:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(49, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Modelo:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(42, 104)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Etiqueta:"
        '
        'txtEtiqueta
        '
        Me.txtEtiqueta.EditValue = ""
        Me.txtEtiqueta.Location = New System.Drawing.Point(104, 100)
        Me.txtEtiqueta.Name = "txtEtiqueta"
        '
        'txtEtiqueta.Properties
        '
        Me.txtEtiqueta.Properties.MaxLength = 30
        Me.txtEtiqueta.Size = New System.Drawing.Size(192, 20)
        Me.txtEtiqueta.TabIndex = 5
        '
        'frmRepartosValidaNumeroSerie
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(330, 160)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtEtiqueta)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtNumeroSerie)
        Me.Controls.Add(Me.clcArticulo)
        Me.Controls.Add(Me.txtmodelo)
        Me.Name = "frmRepartosValidaNumeroSerie"
        Me.Text = "V�lida N�mero Serie"
        Me.Controls.SetChildIndex(Me.txtmodelo, 0)
        Me.Controls.SetChildIndex(Me.clcArticulo, 0)
        Me.Controls.SetChildIndex(Me.txtNumeroSerie, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.txtEtiqueta, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcArticulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEtiqueta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oVentasDetalle As New VillarrealBusiness.clsVentasDetalle

    Private bValidoNumeroSerie As Boolean = False
    Private lfila As Long = -1

    Public WriteOnly Property Articulo() As Long
        Set(ByVal Value As Long)
            Me.clcArticulo.Value = Value
        End Set
    End Property
    Public WriteOnly Property modelo() As String
        Set(ByVal Value As String)
            Me.txtmodelo.Text = Value
        End Set
    End Property
    Private ReadOnly Property NumeroSerie() As String
        Get
            Return Me.txtNumeroSerie.Text
        End Get
    End Property
    Private ReadOnly Property ValidoNumeroSerie() As Boolean
        Get
            Return bValidoNumeroSerie
        End Get
    End Property
    Private ReadOnly Property Etiqueta() As String
        Get
            Return Me.txtEtiqueta.Text
        End Get
    End Property
    Public Property Fila() As Long
        Get
            Return lfila
        End Get
        Set(ByVal Value As Long)
            lfila = Value
        End Set
    End Property



    Private Sub frmRepartosValidaNumeroSerie_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Me.OwnerForm.enabled = True
        OwnerForm.LLenarNumeroSerieValido(Fila, Me.clcArticulo.Value, NumeroSerie, ValidoNumeroSerie)
    End Sub
    Private Sub frmRepartosValidaNumeroSerie_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If ValidaNumeroArticulo() Or ValidaNumeroArticuloVenta() Then
            bValidoNumeroSerie = True
        Else
            Response = New Events
            Response.Message = "El Numero de Serie No Valido con el Articulo En base a su Etiqueta"
        End If

    End Sub

    Private Sub txtEtiqueta_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEtiqueta.EditValueChanged

    End Sub
    Private Sub txtEtiqueta_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEtiqueta.TextChanged

    End Sub

    Private Function ValidaNumeroArticulo() As Boolean
        ValidaNumeroArticulo = False
        If Etiqueta.Trim.Length > 0 Then
            Dim cadena As String
            Dim largo As Integer

            If (Etiqueta.Trim.Length >= 8) Then
                largo = 8
            Else
                largo = Etiqueta.Trim.Length
            End If

            cadena = Etiqueta.Substring(0, largo)
            If IsNumeric(cadena) Then
                If CType(cadena, Long) = Me.clcArticulo.Value Then
                    ValidaNumeroArticulo = True
                End If
            End If

        End If
    End Function

    Private Function ValidaNumeroArticuloVenta() As Boolean
        Dim response As Events
        Dim odataset As DataSet

        response = oVentasDetalle.DespliegaDatosxFolioVentaDetalle(CType(Etiqueta, Long))
        If Not response.ErrorFound Then
            odataset = response.Value
            If odataset.Tables(0).Rows.Count >= 0 Then
                If odataset.Tables(0).Rows(0).Item("articulo") = Me.clcArticulo.Value Then
                    ValidaNumeroArticuloVenta = True
                End If
            End If
        End If
    End Function






End Class
