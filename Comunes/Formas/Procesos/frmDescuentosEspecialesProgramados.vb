Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmDescuentosEspecialesProgramados
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkSobrepedido As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblPlan As System.Windows.Forms.Label
    Friend WithEvents lkpPlan As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcPorcentaje As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkExistenciaActual As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcFolioDescuento As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents clcPrecioContado As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents clcPrecio_Credito As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents chkAgotarExistencias As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkAutorizar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblSolicita As System.Windows.Forms.Label
    Friend WithEvents lkpSolicita As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnAutorizar As System.Windows.Forms.ToolBarButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDescuentosEspecialesProgramados))
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.chkSobrepedido = New DevExpress.XtraEditors.CheckEdit
        Me.chkExistenciaActual = New DevExpress.XtraEditors.CheckEdit
        Me.lblPlan = New System.Windows.Forms.Label
        Me.lkpPlan = New Dipros.Editors.TINMultiLookup
        Me.clcPorcentaje = New DevExpress.XtraEditors.CalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkAgotarExistencias = New DevExpress.XtraEditors.CheckEdit
        Me.clcFolioDescuento = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.clcPrecioContado = New DevExpress.XtraEditors.CalcEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.clcPrecio_Credito = New DevExpress.XtraEditors.CalcEdit
        Me.chkAutorizar = New DevExpress.XtraEditors.CheckEdit
        Me.lblSolicita = New System.Windows.Forms.Label
        Me.lkpSolicita = New Dipros.Editors.TINMultiLookup
        Me.chkActivo = New DevExpress.XtraEditors.CheckEdit
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnAutorizar = New System.Windows.Forms.ToolBarButton
        CType(Me.chkSobrepedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkExistenciaActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.chkAgotarExistencias.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolioDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioContado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecio_Credito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAutorizar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnAutorizar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(480, 28)
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = True
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(112, 88)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(264, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 7
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(64, 88)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 6
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = True
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(112, 64)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(264, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 5
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(19, 64)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 4
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(56, 112)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 8
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Art�c&ulo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = True
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(112, 112)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(400, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = "modelo"
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.SearchMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(192, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 9
        Me.lkpArticulo.Tag = "Articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'chkSobrepedido
        '
        Me.chkSobrepedido.EditValue = "False"
        Me.chkSobrepedido.Location = New System.Drawing.Point(144, 24)
        Me.chkSobrepedido.Name = "chkSobrepedido"
        '
        'chkSobrepedido.Properties
        '
        Me.chkSobrepedido.Properties.Caption = "P&edido a Fabrica"
        Me.chkSobrepedido.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkSobrepedido.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkSobrepedido.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkSobrepedido.Size = New System.Drawing.Size(112, 19)
        Me.chkSobrepedido.TabIndex = 1
        Me.chkSobrepedido.Tag = "aplica_pedido_fabrica"
        '
        'chkExistenciaActual
        '
        Me.chkExistenciaActual.EditValue = True
        Me.chkExistenciaActual.Location = New System.Drawing.Point(16, 22)
        Me.chkExistenciaActual.Name = "chkExistenciaActual"
        '
        'chkExistenciaActual.Properties
        '
        Me.chkExistenciaActual.Properties.Caption = "Existencia Actual"
        Me.chkExistenciaActual.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkExistenciaActual.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkExistenciaActual.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkExistenciaActual.Size = New System.Drawing.Size(112, 19)
        Me.chkExistenciaActual.TabIndex = 0
        Me.chkExistenciaActual.Tag = "aplica_existencia"
        '
        'lblPlan
        '
        Me.lblPlan.AutoSize = True
        Me.lblPlan.Location = New System.Drawing.Point(16, 136)
        Me.lblPlan.Name = "lblPlan"
        Me.lblPlan.Size = New System.Drawing.Size(91, 16)
        Me.lblPlan.TabIndex = 11
        Me.lblPlan.Tag = ""
        Me.lblPlan.Text = "&Plan de cr�dito:"
        Me.lblPlan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPlan
        '
        Me.lkpPlan.AllowAdd = False
        Me.lkpPlan.AutoReaload = False
        Me.lkpPlan.DataSource = Nothing
        Me.lkpPlan.DefaultSearchField = ""
        Me.lkpPlan.DisplayMember = "descripcion"
        Me.lkpPlan.EditValue = Nothing
        Me.lkpPlan.Filtered = False
        Me.lkpPlan.InitValue = Nothing
        Me.lkpPlan.Location = New System.Drawing.Point(112, 136)
        Me.lkpPlan.MultiSelect = False
        Me.lkpPlan.Name = "lkpPlan"
        Me.lkpPlan.NullText = ""
        Me.lkpPlan.PopupWidth = CType(450, Long)
        Me.lkpPlan.ReadOnlyControl = False
        Me.lkpPlan.Required = False
        Me.lkpPlan.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlan.SearchMember = ""
        Me.lkpPlan.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlan.SelectAll = False
        Me.lkpPlan.Size = New System.Drawing.Size(192, 20)
        Me.lkpPlan.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPlan.TabIndex = 12
        Me.lkpPlan.Tag = "plan_credito"
        Me.lkpPlan.ToolTip = Nothing
        Me.lkpPlan.ValueMember = "plan_credito"
        '
        'clcPorcentaje
        '
        Me.clcPorcentaje.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPorcentaje.Location = New System.Drawing.Point(312, 115)
        Me.clcPorcentaje.Name = "clcPorcentaje"
        '
        'clcPorcentaje.Properties
        '
        Me.clcPorcentaje.Properties.DisplayFormat.FormatString = "n2"
        Me.clcPorcentaje.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.Properties.EditFormat.FormatString = "n2"
        Me.clcPorcentaje.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.Properties.Enabled = False
        Me.clcPorcentaje.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPorcentaje.Size = New System.Drawing.Size(48, 20)
        Me.clcPorcentaje.TabIndex = 25
        Me.clcPorcentaje.Tag = "porcentaje_descuento"
        Me.clcPorcentaje.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 162)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 16)
        Me.Label2.TabIndex = 14
        Me.Label2.Tag = ""
        Me.Label2.Text = "Precio Cr�dito:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(8, 264)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(416, 56)
        Me.gpbFechas.TabIndex = 22
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(114, 22)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Ini.TabIndex = 1
        Me.dteFecha_Ini.Tag = "fecha_inicio"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(68, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(260, 22)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Fin.TabIndex = 3
        Me.dteFecha_Fin.Tag = "fecha_final"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(220, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Has&ta: "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkSobrepedido)
        Me.GroupBox1.Controls.Add(Me.chkExistenciaActual)
        Me.GroupBox1.Controls.Add(Me.chkAgotarExistencias)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 208)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 56)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Aplica a"
        '
        'chkAgotarExistencias
        '
        Me.chkAgotarExistencias.EditValue = True
        Me.chkAgotarExistencias.Location = New System.Drawing.Point(280, 24)
        Me.chkAgotarExistencias.Name = "chkAgotarExistencias"
        '
        'chkAgotarExistencias.Properties
        '
        Me.chkAgotarExistencias.Properties.Caption = "Agotar Existencias"
        Me.chkAgotarExistencias.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkAgotarExistencias.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAgotarExistencias.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkAgotarExistencias.Size = New System.Drawing.Size(120, 19)
        Me.chkAgotarExistencias.TabIndex = 2
        Me.chkAgotarExistencias.Tag = "agotar_existencias"
        '
        'clcFolioDescuento
        '
        Me.clcFolioDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolioDescuento.Location = New System.Drawing.Point(112, 40)
        Me.clcFolioDescuento.Name = "clcFolioDescuento"
        '
        'clcFolioDescuento.Properties
        '
        Me.clcFolioDescuento.Properties.Enabled = False
        Me.clcFolioDescuento.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolioDescuento.Size = New System.Drawing.Size(72, 20)
        Me.clcFolioDescuento.TabIndex = 1
        Me.clcFolioDescuento.Tag = "folio_descuento"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(72, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "Folio:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2008, 6, 7, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(336, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = "fecha_creacion"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(288, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Tag = ""
        Me.Label6.Text = "Fecha:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecioContado
        '
        Me.clcPrecioContado.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPrecioContado.Location = New System.Drawing.Point(304, 160)
        Me.clcPrecioContado.Name = "clcPrecioContado"
        '
        'clcPrecioContado.Properties
        '
        Me.clcPrecioContado.Properties.DisplayFormat.FormatString = "n2"
        Me.clcPrecioContado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioContado.Properties.EditFormat.FormatString = "n2"
        Me.clcPrecioContado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecioContado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPrecioContado.Size = New System.Drawing.Size(72, 20)
        Me.clcPrecioContado.TabIndex = 17
        Me.clcPrecioContado.Tag = "precio_contado"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(210, 163)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(92, 16)
        Me.Label7.TabIndex = 16
        Me.Label7.Tag = ""
        Me.Label7.Text = "Precio Contado:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPrecio_Credito
        '
        Me.clcPrecio_Credito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPrecio_Credito.Location = New System.Drawing.Point(112, 160)
        Me.clcPrecio_Credito.Name = "clcPrecio_Credito"
        '
        'clcPrecio_Credito.Properties
        '
        Me.clcPrecio_Credito.Properties.DisplayFormat.FormatString = "n2"
        Me.clcPrecio_Credito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio_Credito.Properties.EditFormat.FormatString = "n2"
        Me.clcPrecio_Credito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPrecio_Credito.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPrecio_Credito.Size = New System.Drawing.Size(72, 20)
        Me.clcPrecio_Credito.TabIndex = 15
        Me.clcPrecio_Credito.Tag = "precio_credito"
        '
        'chkAutorizar
        '
        Me.chkAutorizar.Location = New System.Drawing.Point(352, 136)
        Me.chkAutorizar.Name = "chkAutorizar"
        '
        'chkAutorizar.Properties
        '
        Me.chkAutorizar.Properties.Caption = "Autorizar"
        Me.chkAutorizar.Properties.Enabled = False
        Me.chkAutorizar.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkAutorizar.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkAutorizar.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkAutorizar.Size = New System.Drawing.Size(72, 19)
        Me.chkAutorizar.TabIndex = 13
        Me.chkAutorizar.Tag = "autorizar"
        '
        'lblSolicita
        '
        Me.lblSolicita.AutoSize = True
        Me.lblSolicita.Location = New System.Drawing.Point(56, 184)
        Me.lblSolicita.Name = "lblSolicita"
        Me.lblSolicita.Size = New System.Drawing.Size(49, 16)
        Me.lblSolicita.TabIndex = 19
        Me.lblSolicita.Tag = ""
        Me.lblSolicita.Text = "Solicita:"
        Me.lblSolicita.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSolicita
        '
        Me.lkpSolicita.AllowAdd = False
        Me.lkpSolicita.AutoReaload = True
        Me.lkpSolicita.DataSource = Nothing
        Me.lkpSolicita.DefaultSearchField = ""
        Me.lkpSolicita.DisplayMember = "nombre"
        Me.lkpSolicita.EditValue = Nothing
        Me.lkpSolicita.Filtered = False
        Me.lkpSolicita.InitValue = Nothing
        Me.lkpSolicita.Location = New System.Drawing.Point(112, 184)
        Me.lkpSolicita.MultiSelect = False
        Me.lkpSolicita.Name = "lkpSolicita"
        Me.lkpSolicita.NullText = ""
        Me.lkpSolicita.PopupWidth = CType(400, Long)
        Me.lkpSolicita.ReadOnlyControl = False
        Me.lkpSolicita.Required = False
        Me.lkpSolicita.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSolicita.SearchMember = ""
        Me.lkpSolicita.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSolicita.SelectAll = True
        Me.lkpSolicita.Size = New System.Drawing.Size(264, 20)
        Me.lkpSolicita.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSolicita.TabIndex = 20
        Me.lkpSolicita.Tag = "solicita"
        Me.lkpSolicita.ToolTip = "Seleccione un solicitante"
        Me.lkpSolicita.ValueMember = "usuario"
        '
        'chkActivo
        '
        Me.chkActivo.Location = New System.Drawing.Point(352, 113)
        Me.chkActivo.Name = "chkActivo"
        '
        'chkActivo.Properties
        '
        Me.chkActivo.Properties.Caption = "Activo"
        Me.chkActivo.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkActivo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkActivo.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkActivo.Size = New System.Drawing.Size(72, 19)
        Me.chkActivo.TabIndex = 10
        Me.chkActivo.Tag = "activo"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(104, 328)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(320, 38)
        Me.txtObservaciones.TabIndex = 24
        Me.txtObservaciones.Tag = "observaciones"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 328)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(89, 16)
        Me.Label8.TabIndex = 23
        Me.Label8.Tag = ""
        Me.Label8.Text = "Observaciones:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAutorizar
        '
        Me.btnAutorizar.Enabled = False
        Me.btnAutorizar.Tag = "BTNDESCPROGAUTORIZAR"
        Me.btnAutorizar.Text = "Autorizar"
        '
        'frmDescuentosEspecialesProgramados
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(434, 380)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.chkActivo)
        Me.Controls.Add(Me.lblSolicita)
        Me.Controls.Add(Me.lkpSolicita)
        Me.Controls.Add(Me.chkAutorizar)
        Me.Controls.Add(Me.clcPrecio_Credito)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.clcPrecioContado)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.clcFolioDescuento)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcPorcentaje)
        Me.Controls.Add(Me.lblPlan)
        Me.Controls.Add(Me.lkpPlan)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label6)
        Me.Name = "frmDescuentosEspecialesProgramados"
        Me.Text = "frmDescuentosEspecialesProgramados"
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpPlan, 0)
        Me.Controls.SetChildIndex(Me.lblPlan, 0)
        Me.Controls.SetChildIndex(Me.clcPorcentaje, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.clcFolioDescuento, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioContado, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.clcPrecio_Credito, 0)
        Me.Controls.SetChildIndex(Me.chkAutorizar, 0)
        Me.Controls.SetChildIndex(Me.lkpSolicita, 0)
        Me.Controls.SetChildIndex(Me.lblSolicita, 0)
        Me.Controls.SetChildIndex(Me.chkActivo, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        CType(Me.chkSobrepedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkExistenciaActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.chkAgotarExistencias.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolioDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioContado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecio_Credito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAutorizar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oPlanesCredito As New VillarrealBusiness.clsPlanesCredito
    Private osucursales As New VillarrealBusiness.clsSucursales
    Private oUsuarios As VillarrealBusiness.clsUsuarios
    Private oDescuentosEspecialesProgramados As New VillarrealBusiness.clsDescuentosEspecialesProgramados


    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
    Private ReadOnly Property PlanCredito() As Long
        Get
            Return PreparaValorLookup(Me.lkpPlan)
        End Get
    End Property
    Private ReadOnly Property Solicita() As Long
        Get
            Return PreparaValorLookup(Me.lkpSolicita)
        End Get
    End Property



    Private articulo_temporal As Long = 0
    Private sucursal_dependencia As Boolean


#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmDescuentosEspecialesProgramados_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        sucursal_dependencia = CType(CType(osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)
        oUsuarios = New VillarrealBusiness.clsUsuarios

        Me.dteFecha.DateTime = CDate(TinApp.FechaServidor)
        Me.dteFecha_Ini.DateTime = CDate(TinApp.FechaServidor)
        Me.dteFecha_Fin.DateTime = DateAdd(DateInterval.Day, 1, CDate(TinApp.FechaServidor))


        If Not Comunes.Common.Identificadores Is Nothing Then
            If Comunes.Common.Identificadores.Length > 0 Then

                Dim Validaciones As Boolean() = VerificaPermisosExtendidos(Me.tbrTools.Buttons, Comunes.Common.Identificadores)

            End If
        End If


        Select Case Action
            Case Actions.Insert
                Me.chkActivo.Checked = True
        End Select
    End Sub

    Private Sub frmDescuentosEspecialesProgramados_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oDescuentosEspecialesProgramados.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oDescuentosEspecialesProgramados.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oDescuentosEspecialesProgramados.Eliminar(Me.DataSource)

        End Select
    End Sub

    Private Sub frmDescuentosEspecialesProgramados_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = Me.oDescuentosEspecialesProgramados.Validacion(Action, Articulo, Me.PlanCredito, Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text, Me.DataSource)
    End Sub

    Private Sub frmDescuentosEspecialesProgramados_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oDescuentosEspecialesProgramados.DespliegaDatos(OwnerForm.Value("folio_descuento"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            If Me.chkAutorizar.Checked Then

                If btnAutorizar.Enabled Then
                    Me.tbrTools.Buttons.Item(0).Enabled = True
                Else
                    Me.EnabledEdit(False)
                    Me.tbrTools.Buttons.Item(0).Enabled = False
                    Me.btnAutorizar.Enabled = False
                End If



            End If
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.btnAutorizar Then
            Me.chkAutorizar.Checked = True

        End If
    End Sub


#Region "Lookup"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData

        Dim Response As New Events

        Response = oDepartamentos.Lookup()

        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")
        Me.lkpGrupo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpGrupo.Text = Me.lkpGrupo.GetValue("descripcion")
        Me.lkpArticulo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData

        Dim Response As New Events

        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData

        Dim Response As New Events
        Response = oArticulos.Lookup(Departamento, Grupo)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged

        If Articulo <> -1 Then

            'Me.clcCantidad.Value = 1

            If articulo_temporal <> Articulo Then articulo_temporal = Articulo
            Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            Me.lkpArticulo.EditValue = articulo_temporal
            'Me.txtNArticulo.Text = Me.lkpArticulo.GetValue("descripcion_corta")
            'Me.txtmodelo.Text = Me.lkpArticulo.GetValue("modelo")
        End If

    End Sub


    Private Sub lkpPlan_LoadData(ByVal Initialize As Boolean) Handles lkpPlan.LoadData

        Dim Response As New Events
        Response = oPlanesCredito.LookupDescuentosProgamados(sucursal_dependencia, , 1)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlan.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If

        Response = Nothing

    End Sub
    Private Sub lkpPlan_Format() Handles lkpPlan.Format
        Comunes.clsFormato.for_planes_credito_grl(Me.lkpPlan)
    End Sub


#End Region

    Private Sub lkpSolicita_Format() Handles lkpSolicita.Format
        Comunes.clsFormato.for_usuarios_grl(Me.lkpSolicita)
    End Sub
    Private Sub lkpSolicita_LoadData(ByVal Initialize As Boolean) Handles lkpSolicita.LoadData
        Dim Response As New Events
        Response = oUsuarios.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSolicita.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    'Private Sub chkExistenciaActual_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExistenciaActual.CheckedChanged
    '    Me.chkSobrepedido.Checked = Not Me.chkExistenciaActual.Checked
    'End Sub
    'Private Sub chkSobrepedido_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSobrepedido.CheckedChanged
    '    Me.chkExistenciaActual.Checked = Not Me.chkSobrepedido.Checked
    'End Sub
#End Region



    Private Sub frmDescuentosEspecialesProgramados_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      
    End Sub
End Class
