Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmPolizaChequeDetalle
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents clcCargo As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcAbono As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcConsecutivo As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lkpCuentasContables As Dipros.Editors.TINMultiLookup
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPolizaChequeDetalle))
        Me.clcCargo = New DevExpress.XtraEditors.CalcEdit
        Me.clcAbono = New DevExpress.XtraEditors.CalcEdit
        Me.clcConsecutivo = New DevExpress.XtraEditors.CalcEdit
        Me.txtConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpCuentasContables = New Dipros.Editors.TINMultiLookup
        Me.Label4 = New System.Windows.Forms.Label
        CType(Me.clcCargo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcConsecutivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'clcCargo
        '
        Me.clcCargo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcCargo.Location = New System.Drawing.Point(16, 16)
        Me.clcCargo.Name = "clcCargo"
        Me.clcCargo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcCargo.Size = New System.Drawing.Size(75, 22)
        Me.clcCargo.TabIndex = 8
        Me.clcCargo.Tag = "cargo"
        '
        'clcAbono
        '
        Me.clcAbono.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcAbono.Location = New System.Drawing.Point(16, 44)
        Me.clcAbono.Name = "clcAbono"
        Me.clcAbono.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcAbono.Size = New System.Drawing.Size(75, 22)
        Me.clcAbono.TabIndex = 9
        Me.clcAbono.Tag = "abono"
        '
        'clcConsecutivo
        '
        Me.clcConsecutivo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcConsecutivo.Location = New System.Drawing.Point(88, 48)
        Me.clcConsecutivo.Name = "clcConsecutivo"
        '
        'clcConsecutivo.Properties
        '
        Me.clcConsecutivo.Properties.Enabled = False
        Me.clcConsecutivo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcConsecutivo.Size = New System.Drawing.Size(75, 22)
        Me.clcConsecutivo.TabIndex = 2
        Me.clcConsecutivo.Tag = "partida"
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(88, 176)
        Me.txtConcepto.Name = "txtConcepto"
        '
        'txtConcepto.Properties
        '
        Me.txtConcepto.Properties.MaxLength = 120
        Me.txtConcepto.Size = New System.Drawing.Size(232, 80)
        Me.txtConcepto.TabIndex = 11
        Me.txtConcepto.Tag = "concepto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 176)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Concepto:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(36, 48)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Partida:"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.EditValue = "C"
        Me.RadioGroup1.Location = New System.Drawing.Point(89, 96)
        Me.RadioGroup1.Name = "RadioGroup1"
        '
        'RadioGroup1.Properties
        '
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("C", "Cargo"), New DevExpress.XtraEditors.Controls.RadioGroupItem("A", "Abono")})
        Me.RadioGroup1.Size = New System.Drawing.Size(88, 72)
        Me.RadioGroup1.TabIndex = 6
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.clcCargo)
        Me.GroupBox1.Controls.Add(Me.clcAbono)
        Me.GroupBox1.Location = New System.Drawing.Point(177, 91)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(112, 78)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(52, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Tipo:"
        '
        'lkpCuentasContables
        '
        Me.lkpCuentasContables.AllowAdd = False
        Me.lkpCuentasContables.AutoReaload = False
        Me.lkpCuentasContables.DataSource = Nothing
        Me.lkpCuentasContables.DefaultSearchField = ""
        Me.lkpCuentasContables.DisplayMember = "cuentacontable"
        Me.lkpCuentasContables.EditValue = Nothing
        Me.lkpCuentasContables.Filtered = False
        Me.lkpCuentasContables.InitValue = Nothing
        Me.lkpCuentasContables.Location = New System.Drawing.Point(88, 72)
        Me.lkpCuentasContables.MultiSelect = False
        Me.lkpCuentasContables.Name = "lkpCuentasContables"
        Me.lkpCuentasContables.NullText = ""
        Me.lkpCuentasContables.PopupWidth = CType(400, Long)
        Me.lkpCuentasContables.Required = False
        Me.lkpCuentasContables.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCuentasContables.SearchMember = ""
        Me.lkpCuentasContables.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCuentasContables.SelectAll = False
        Me.lkpCuentasContables.Size = New System.Drawing.Size(136, 22)
        Me.lkpCuentasContables.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCuentasContables.TabIndex = 4
        Me.lkpCuentasContables.Tag = "cuenta_contable"
        Me.lkpCuentasContables.ToolTip = Nothing
        Me.lkpCuentasContables.ValueMember = "cuentacontable"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(36, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Cuenta:"
        '
        'frmPolizaChequeDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(330, 264)
        Me.Controls.Add(Me.lkpCuentasContables)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.RadioGroup1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtConcepto)
        Me.Controls.Add(Me.clcConsecutivo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Name = "frmPolizaChequeDetalle"
        Me.Text = "frmPolizaChequeDetalle"
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.clcConsecutivo, 0)
        Me.Controls.SetChildIndex(Me.txtConcepto, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.RadioGroup1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.lkpCuentasContables, 0)
        CType(Me.clcCargo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcConsecutivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCuentasContables As VillarrealBusiness.clsCuentasContables
    Private oPolizasCheques As VillarrealBusiness.clsPolizasCheques
#End Region

#Region "DIPROS Systems, Eventos de la Forma"


    Private Sub frmPolizaChequeDetalle_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
        OwnerForm.PolizaCuadrada()
    End Sub

    Private Sub frmPolizaChequeDetalle_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCuentasContables = New VillarrealBusiness.clsCuentasContables
        oPolizasCheques = New VillarrealBusiness.clsPolizasCheques

        Me.RadioGroup1.SelectedIndex = 1
        Me.RadioGroup1.SelectedIndex = 0
    End Sub

    Private Sub frmPolizaChequeDetalle_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

        If Me.clcCargo.Value > 0 Then
            Me.RadioGroup1.SelectedIndex = 0
        End If
        If Me.clcAbono.Value > 0 Then
            Me.RadioGroup1.SelectedIndex = 1
        End If
    End Sub

    Private Sub frmPolizaChequeDetalle_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        '@JGTO-18/04/2008: Se valida si el lkp de cuenta es nothing 
        Response = oPolizasCheques.Validacion(Action, _
        IIf(IsNothing(Me.lkpCuentasContables.EditValue), "", Me.lkpCuentasContables.EditValue), _
        RevisaImporte, Me.txtConcepto.Text)
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    Private Sub lkpCuentasContables_LoadData(ByVal Initialize As Boolean) Handles lkpCuentasContables.LoadData
        Dim response As Events
        response = oCuentasContables.Lookup(Comunes.Common.Sucursal_Actual, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCuentasContables.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpCuentasContables_Format() Handles lkpCuentasContables.Format
        Comunes.clsFormato.for_cuentascontables_grl(Me.lkpCuentasContables)
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
        Select Case RadioGroup1.SelectedIndex
            Case 0
                Me.clcCargo.Enabled = True
                Me.clcAbono.Enabled = False
                Me.clcAbono.Value = 0
            Case 1
                Me.clcAbono.Enabled = True
                Me.clcCargo.Enabled = False
                Me.clcCargo.Value = 0


        End Select
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Function RevisaImporte() As Boolean
        If Me.clcAbono.Value <= 0 And Me.clcCargo.Value <= 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region
   

End Class
