Imports Dipros.Utils
Imports Dipros.Utils.Common

Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms

Public Class frmEnvioInformacionConvenios
    Inherits Dipros.Windows.frmTINForm

    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oConvenios As VillarrealBusiness.clsConvenios


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grConvenios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcRfc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grQuincenaInicial As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grImporteMes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grQuincenaFinal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grNoEmpleado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grImporteQuincena As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaConvenio As Dipros.Windows.TINMaster
    Friend WithEvents dgConvenios As DevExpress.XtraGrid.GridControl
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents lblAno As System.Windows.Forms.Label
    Friend WithEvents lblNoQuincena As System.Windows.Forms.Label
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cboTipoConvenio As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents grTipoUno As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grFiller As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grNumDocto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcAno As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcnoquincena As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblNom As System.Windows.Forms.Label
    Friend WithEvents btnRuta As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btoGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtRuta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbrGenerarTexto As System.Windows.Forms.ToolBarButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEnvioInformacionConvenios))
        Me.dgConvenios = New DevExpress.XtraGrid.GridControl
        Me.grConvenios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcRfc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grQuincenaInicial = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grQuincenaFinal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grNoEmpleado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grImporteMes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grImporteQuincena = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grTipoUno = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grFiller = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grNumDocto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaConvenio = New Dipros.Windows.TINMaster
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.lblAno = New System.Windows.Forms.Label
        Me.lblNoQuincena = New System.Windows.Forms.Label
        Me.cboTipoConvenio = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        Me.clcAno = New DevExpress.XtraEditors.CalcEdit
        Me.clcnoquincena = New DevExpress.XtraEditors.CalcEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.txtRuta = New DevExpress.XtraEditors.TextEdit
        Me.lblNom = New System.Windows.Forms.Label
        Me.btnRuta = New DevExpress.XtraEditors.SimpleButton
        Me.btoGenerar = New DevExpress.XtraEditors.SimpleButton
        Me.tbrGenerarTexto = New System.Windows.Forms.ToolBarButton
        CType(Me.dgConvenios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grConvenios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoConvenio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcAno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcnoquincena.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRuta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbrGenerarTexto})
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(7488, 28)
        '
        'dgConvenios
        '
        '
        'dgConvenios.EmbeddedNavigator
        '
        Me.dgConvenios.EmbeddedNavigator.Name = ""
        Me.dgConvenios.Location = New System.Drawing.Point(11, 151)
        Me.dgConvenios.MainView = Me.grConvenios
        Me.dgConvenios.Name = "dgConvenios"
        Me.dgConvenios.Size = New System.Drawing.Size(844, 408)
        Me.dgConvenios.TabIndex = 11
        '
        'grConvenios
        '
        Me.grConvenios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcRfc, Me.grNombre, Me.grQuincenaInicial, Me.grQuincenaFinal, Me.grNoEmpleado, Me.grImporteMes, Me.grImporteQuincena, Me.grTipo, Me.grSaldo, Me.grTipoUno, Me.grFiller, Me.grNumDocto, Me.grcConcepto})
        Me.grConvenios.GridControl = Me.dgConvenios
        Me.grConvenios.GroupPanelText = "Convenios"
        Me.grConvenios.Name = "grConvenios"
        Me.grConvenios.OptionsView.ShowGroupPanel = False
        '
        'grcRfc
        '
        Me.grcRfc.Caption = "RFC"
        Me.grcRfc.FieldName = "rfc"
        Me.grcRfc.Name = "grcRfc"
        Me.grcRfc.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcRfc.VisibleIndex = 6
        '
        'grNombre
        '
        Me.grNombre.Caption = "Nombre"
        Me.grNombre.FieldName = "nombre"
        Me.grNombre.Name = "grNombre"
        Me.grNombre.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grNombre.VisibleIndex = 7
        '
        'grQuincenaInicial
        '
        Me.grQuincenaInicial.Caption = "Quincena Inicial"
        Me.grQuincenaInicial.FieldName = "quincena_inicial"
        Me.grQuincenaInicial.Name = "grQuincenaInicial"
        Me.grQuincenaInicial.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grQuincenaInicial.VisibleIndex = 0
        '
        'grQuincenaFinal
        '
        Me.grQuincenaFinal.Caption = "Quincena Final"
        Me.grQuincenaFinal.FieldName = "quincena_final"
        Me.grQuincenaFinal.Name = "grQuincenaFinal"
        Me.grQuincenaFinal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grQuincenaFinal.VisibleIndex = 1
        '
        'grNoEmpleado
        '
        Me.grNoEmpleado.Caption = "No. Empleado"
        Me.grNoEmpleado.FieldName = "no_empleado"
        Me.grNoEmpleado.Name = "grNoEmpleado"
        Me.grNoEmpleado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grNoEmpleado.VisibleIndex = 8
        '
        'grImporteMes
        '
        Me.grImporteMes.Caption = "Importe Mensual"
        Me.grImporteMes.DisplayFormat.FormatString = "N2"
        Me.grImporteMes.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grImporteMes.FieldName = "importe_mensual"
        Me.grImporteMes.Name = "grImporteMes"
        Me.grImporteMes.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grImporteMes.VisibleIndex = 4
        '
        'grImporteQuincena
        '
        Me.grImporteQuincena.Caption = "Importe Quincenal"
        Me.grImporteQuincena.DisplayFormat.FormatString = "n2"
        Me.grImporteQuincena.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grImporteQuincena.FieldName = "importe_quincenal"
        Me.grImporteQuincena.Name = "grImporteQuincena"
        Me.grImporteQuincena.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grImporteQuincena.VisibleIndex = 5
        '
        'grTipo
        '
        Me.grTipo.Caption = "Tipo Movto"
        Me.grTipo.FieldName = "tipo_movto"
        Me.grTipo.Name = "grTipo"
        Me.grTipo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grTipo.VisibleIndex = 3
        '
        'grSaldo
        '
        Me.grSaldo.Caption = "Saldo"
        Me.grSaldo.FieldName = "saldo"
        Me.grSaldo.Name = "grSaldo"
        Me.grSaldo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grSaldo.VisibleIndex = 9
        '
        'grTipoUno
        '
        Me.grTipoUno.Caption = "Tipo"
        Me.grTipoUno.FieldName = "tipo"
        Me.grTipoUno.Name = "grTipoUno"
        Me.grTipoUno.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grFiller
        '
        Me.grFiller.Caption = "Filler"
        Me.grFiller.FieldName = "filler"
        Me.grFiller.Name = "grFiller"
        Me.grFiller.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grNumDocto
        '
        Me.grNumDocto.Caption = "No. Documento"
        Me.grNumDocto.FieldName = "num_docto"
        Me.grNumDocto.Name = "grNumDocto"
        Me.grNumDocto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto_movto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.VisibleIndex = 2
        '
        'tmaConvenio
        '
        Me.tmaConvenio.BackColor = System.Drawing.Color.White
        Me.tmaConvenio.CanDelete = True
        Me.tmaConvenio.CanInsert = True
        Me.tmaConvenio.CanUpdate = True
        Me.tmaConvenio.Grid = Me.dgConvenios
        Me.tmaConvenio.Location = New System.Drawing.Point(11, 127)
        Me.tmaConvenio.Name = "tmaConvenio"
        Me.tmaConvenio.Size = New System.Drawing.Size(844, 23)
        Me.tmaConvenio.TabIndex = 10
        Me.tmaConvenio.Title = "Convenio"
        Me.tmaConvenio.UpdateTitle = "un Convenio"
        '
        'lblConvenio
        '
        Me.lblConvenio.Location = New System.Drawing.Point(16, 72)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(64, 16)
        Me.lblConvenio.TabIndex = 2
        Me.lblConvenio.Text = "Convenio :"
        '
        'lblAno
        '
        Me.lblAno.Location = New System.Drawing.Point(616, 40)
        Me.lblAno.Name = "lblAno"
        Me.lblAno.Size = New System.Drawing.Size(64, 16)
        Me.lblAno.TabIndex = 4
        Me.lblAno.Text = "A�o :"
        Me.lblAno.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNoQuincena
        '
        Me.lblNoQuincena.Location = New System.Drawing.Point(592, 64)
        Me.lblNoQuincena.Name = "lblNoQuincena"
        Me.lblNoQuincena.Size = New System.Drawing.Size(88, 16)
        Me.lblNoQuincena.TabIndex = 6
        Me.lblNoQuincena.Text = "No. Quincena :"
        Me.lblNoQuincena.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoConvenio
        '
        Me.cboTipoConvenio.EditValue = "A"
        Me.cboTipoConvenio.Location = New System.Drawing.Point(688, 80)
        Me.cboTipoConvenio.Name = "cboTipoConvenio"
        '
        'cboTipoConvenio.Properties
        '
        Me.cboTipoConvenio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoConvenio.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Alta", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Baja", "B", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Modificado", "M", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todos", "T", -1)})
        Me.cboTipoConvenio.Size = New System.Drawing.Size(72, 23)
        Me.cboTipoConvenio.TabIndex = 9
        Me.cboTipoConvenio.Tag = "tipo"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(88, 48)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(184, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = "Sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(88, 72)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = ""
        Me.lkpConvenio.PopupWidth = CType(400, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(448, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 3
        Me.lkpConvenio.Tag = "convenio"
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'clcAno
        '
        Me.clcAno.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcAno.Location = New System.Drawing.Point(688, 32)
        Me.clcAno.Name = "clcAno"
        '
        'clcAno.Properties
        '
        Me.clcAno.Properties.MaxLength = 4
        Me.clcAno.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcAno.Size = New System.Drawing.Size(40, 20)
        Me.clcAno.TabIndex = 5
        Me.clcAno.Tag = "ano"
        '
        'clcnoquincena
        '
        Me.clcnoquincena.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcnoquincena.Location = New System.Drawing.Point(688, 56)
        Me.clcnoquincena.Name = "clcnoquincena"
        '
        'clcnoquincena.Properties
        '
        Me.clcnoquincena.Properties.MaxLength = 2
        Me.clcnoquincena.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcnoquincena.Size = New System.Drawing.Size(32, 20)
        Me.clcnoquincena.TabIndex = 7
        Me.clcnoquincena.Tag = "no_quincena"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(24, 48)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 59
        Me.lblSucursal.Text = "Sucursal:"
        '
        'txtRuta
        '
        Me.txtRuta.EditValue = ""
        Me.txtRuta.Location = New System.Drawing.Point(88, 96)
        Me.txtRuta.Name = "txtRuta"
        '
        'txtRuta.Properties
        '
        Me.txtRuta.Properties.Enabled = False
        Me.txtRuta.Size = New System.Drawing.Size(448, 20)
        Me.txtRuta.TabIndex = 61
        Me.txtRuta.Tag = "nombre"
        '
        'lblNom
        '
        Me.lblNom.AutoSize = True
        Me.lblNom.Location = New System.Drawing.Point(21, 99)
        Me.lblNom.Name = "lblNom"
        Me.lblNom.Size = New System.Drawing.Size(59, 16)
        Me.lblNom.TabIndex = 60
        Me.lblNom.Text = "Ruta TXT:"
        '
        'btnRuta
        '
        Me.btnRuta.Location = New System.Drawing.Point(536, 96)
        Me.btnRuta.Name = "btnRuta"
        Me.btnRuta.Size = New System.Drawing.Size(32, 20)
        Me.btnRuta.TabIndex = 62
        Me.btnRuta.Text = "..."
        '
        'btoGenerar
        '
        Me.btoGenerar.Location = New System.Drawing.Point(776, 64)
        Me.btoGenerar.Name = "btoGenerar"
        Me.btoGenerar.Size = New System.Drawing.Size(75, 40)
        Me.btoGenerar.TabIndex = 63
        Me.btoGenerar.Text = "Refrescar"
        '
        'tbrGenerarTexto
        '
        Me.tbrGenerarTexto.Text = "Generar Texto"
        Me.tbrGenerarTexto.ToolTipText = "Generar un archivo de texto con la informaci�n"
        '
        'frmEnvioInformacionConvenios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(866, 564)
        Me.Controls.Add(Me.btoGenerar)
        Me.Controls.Add(Me.btnRuta)
        Me.Controls.Add(Me.txtRuta)
        Me.Controls.Add(Me.lblNom)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.dgConvenios)
        Me.Controls.Add(Me.clcnoquincena)
        Me.Controls.Add(Me.clcAno)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.cboTipoConvenio)
        Me.Controls.Add(Me.lblNoQuincena)
        Me.Controls.Add(Me.lblAno)
        Me.Controls.Add(Me.lblConvenio)
        Me.Controls.Add(Me.tmaConvenio)
        Me.Name = "frmEnvioInformacionConvenios"
        Me.Text = "Convenios"
        Me.Controls.SetChildIndex(Me.tmaConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblAno, 0)
        Me.Controls.SetChildIndex(Me.lblNoQuincena, 0)
        Me.Controls.SetChildIndex(Me.cboTipoConvenio, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.clcAno, 0)
        Me.Controls.SetChildIndex(Me.clcnoquincena, 0)
        Me.Controls.SetChildIndex(Me.dgConvenios, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblNom, 0)
        Me.Controls.SetChildIndex(Me.txtRuta, 0)
        Me.Controls.SetChildIndex(Me.btnRuta, 0)
        Me.Controls.SetChildIndex(Me.btoGenerar, 0)
        CType(Me.dgConvenios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grConvenios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoConvenio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcAno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcnoquincena.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRuta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"

    Private bValido As Boolean = True

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub frmConvenios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oConvenios = New VillarrealBusiness.clsConvenios
        With tmaConvenio
            .UpdateForm = New frmEnvioInformacionConveniosDetalle
            .AddColumn("rfc")
            .AddColumn("nombre")
            .AddColumn("quincena_inicial")
            .AddColumn("quincena_final")
            .AddColumn("no_empleado")
            .AddColumn("concepto_movto")
            .AddColumn("importe_mensual")
            .AddColumn("importe_quincenal")
            .AddColumn("tipo_movto")
            .AddColumn("saldo")
            .AddColumn("tipo")
            .AddColumn("filler")
            .AddColumn("num_docto")

        End With

        Me.tbrTools.Buttons(0).Enabled = False
        Me.tbrTools.Buttons(0).Visible = False

    End Sub

    Private Sub frmConvenios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub

    Private Sub frmEnvioInformacionConvenios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = New Events
        bValido = True

        If Me.tmaConvenio.Count = 0 Then
            Response.Message = "No hay informacion para enviar"
            bValido = False
            Exit Sub
        End If

        If Me.txtRuta.Text.Trim.Length = 0 Then
            Response.Message = "La ruta para generar el TXT no esta configurada"
            bValido = False
            Exit Sub
        End If

    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is tbrGenerarTexto And bValido Then
            GenerarTXT()
        End If
    End Sub

#End Region

#Region "Eventos de los Controles"
    Private Sub btnRuta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRuta.Click
        Dim folderBrowserDialog1 As FolderBrowserDialog
        folderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog

        folderBrowserDialog1.Description = "Selecciona el directorio que deseas usar. "
        folderBrowserDialog1.ShowNewFolderButton = False

        folderBrowserDialog1.RootFolder = Environment.SpecialFolder.Personal

        Dim result As DialogResult
        result = folderBrowserDialog1.ShowDialog

        If result = DialogResult.OK Then
            Me.txtRuta.Text = folderBrowserDialog1.SelectedPath
        End If


    End Sub

    Private Sub btoGenerar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btoGenerar.Click
        LlenarGrid()
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim oDataSet As DataSet
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub

    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim oDataSet As DataSet
        Dim Response As New Events
        Response = oConvenios.Lookup
        If Not Response.ErrorFound Then
            oDataSet = Response.Value
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


#End Region

#Region "Funcionalidad"

    Private Sub LlenarGrid()
        Dim reportes As New VillarrealBusiness.Reportes
        Dim dataset As New DataSet
        Dim oEvents As New Events


        oEvents = reportes.EnvioInformacionTXT(Sucursal, Convenio, Me.clcAno.Value, Me.clcnoquincena.Value, Me.cboTipoConvenio.EditValue)
        dataset = oEvents.Value
        Me.tmaConvenio.DataSource = dataset

        oEvents = Nothing
        dataset = Nothing

    End Sub

    Private Sub GenerarTXT()


        Dim Rutas As String = Me.txtRuta.Text
        Dim Archivo As String = "\Archivo" & Format(Today.Date, "ddmmyyyy") & ".txt"
        Dim VacioArchivo As Stream = Nothing
        Dim CadenaArchivo As StreamWriter = Nothing

        Dim LineaArchivo As String = String.Empty



        ' Genera a TXT
        Try
            If Directory.Exists(Rutas) = False Then ' si no existe la carpeta se crea
                Directory.CreateDirectory(Rutas)
            End If
            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            '            Rutas = "C:\Paso\Archivo" & Format(Today.Date, "ddMMyyyy") & ".txt" ' Se determina el nombre del archivo con la fecha actual
            If File.Exists(Rutas + Archivo) Then
                File.Delete(Rutas + Archivo)
                'VacioArchivo = File.Open(Rutas + Archivo, FileMode.Open) 'Abrimos el archivo
            End If

            VacioArchivo = File.Create(Rutas + Archivo) ' lo creamos
            CadenaArchivo = New StreamWriter(VacioArchivo, System.Text.Encoding.Default)

            ' Vacia en archivo


            With tmaConvenio
                .MoveFirst()

                Do While Not tmaConvenio.EOF

                    If tmaConvenio.CurrentAction <> Actions.Delete Then

                        Dim filler As String = tmaConvenio.Item("filler")
                        Dim rfc As String = tmaConvenio.Item("rfc")
                        Dim nombre As String = tmaConvenio.Item("nombre")
                        Dim concepto As String = tmaConvenio.Item("concepto_movto")
                        Dim importe_quincenal As String = tmaConvenio.Item("importe_quincenal")
                        Dim importe_mensual As String = tmaConvenio.Item("importe_mensual")

                        ' ==============================================================================
                        importe_quincenal = FormateaImportesNumericos(importe_quincenal)
                        importe_mensual = FormateaImportesNumericos(importe_mensual)


                        If tmaConvenio.CurrentAction <> Actions.None Then

                            If filler.Length < 7 Then
                                filler = filler.PadRight(7, " ")
                            End If

                            If rfc.Trim.Length < 13 Then
                                rfc = rfc.PadRight(13, " ")
                            End If

                            If nombre.Trim.Length < 28 Then
                                nombre = nombre.PadRight(28, " ")
                            End If

                            If concepto.Trim.Length < 2 Then
                                concepto = concepto.PadRight(2, " ")
                            End If

                        End If

                        'LineaArchivo = tmaConvenio.Item("tipo") + "|" + tmaConvenio.Item("filler") + "|" + tmaConvenio.Item("num_docto") + "|" + tmaConvenio.Item("quincena_inicial") + "|" + tmaConvenio.Item("quincena_final") + "|" + tmaConvenio.Item("concepto_movto") + "|" + tmaConvenio.Item("tipo_movto") + "|" + tmaConvenio.Item("importe_mensual") + "|" + tmaConvenio.Item("importe_quincenal") + "|" + tmaConvenio.Item("rfc") + "|" + tmaConvenio.Item("nombre")

                        'LineaArchivo = tmaConvenio.Item("tipo") + tmaConvenio.Item("filler") + tmaConvenio.Item("num_docto") + tmaConvenio.Item("quincena_inicial") + tmaConvenio.Item("quincena_final") + tmaConvenio.Item("concepto_movto") + tmaConvenio.Item("tipo_movto") + tmaConvenio.Item("importe_mensual") + tmaConvenio.Item("importe_quincenal") + tmaConvenio.Item("rfc") + tmaConvenio.Item("nombre")

                        LineaArchivo = tmaConvenio.Item("tipo") + _
                                    filler + _
                                    tmaConvenio.Item("num_docto") + _
                                    tmaConvenio.Item("quincena_inicial") + _
                                    tmaConvenio.Item("quincena_final") + _
                                    concepto + _
                                    tmaConvenio.Item("tipo_movto") + _
                                    importe_mensual + _
                                    importe_quincenal + _
                                    rfc + _
                                    nombre

                        CadenaArchivo.WriteLine(LineaArchivo)

                    End If
                    .MoveNext()
                Loop
            End With

            CadenaArchivo.Close()

            ShowMessage(MessageType.MsgInformation, "Archivo creado correctamente", Me.Title)
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, "Error al Guardar la ingormacion en el archivo.", Me.Title, ex)
            CadenaArchivo.Close()
        End Try

        ' Termina de Generar

    End Sub

    Private Function FormateaImportesNumericos(ByVal importe As String) As String
        If importe.IndexOf(".") = -1 Then
            importe = importe + "00"
        Else

            Dim cadenas As String() = importe.Split(".")
            Dim decimales As String = cadenas.GetValue(1)

            If decimales.Length > 2 Then
                importe = importe.Substring(0, importe.IndexOf(".") + 1 + 2)
            End If
            If decimales.Length = 2 Then
                importe = importe.Substring(0, importe.Length)
            End If
            If decimales.Length = 1 Then
                importe = importe.Substring(0, importe.IndexOf(".") + 1) + decimales.PadRight(2, "0")
            End If
            importe = (importe.Replace(".", "")).Replace(",", "")

        End If

        importe = importe.PadLeft(7, "0")

        Return importe
    End Function

#End Region


End Class
