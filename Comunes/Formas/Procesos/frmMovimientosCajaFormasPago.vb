Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmMovimientosCajaFormasPago
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblFormasPago As System.Windows.Forms.Label
    Friend WithEvents lkpFormasPago As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkManeja_Dolares As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcDolares As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDolares As System.Windows.Forms.Label
    Friend WithEvents lblFormapago As System.Windows.Forms.Label
    Friend WithEvents clcTipoCambio As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientosCajaFormasPago))
        Me.lblFormasPago = New System.Windows.Forms.Label
        Me.lkpFormasPago = New Dipros.Editors.TINMultiLookup
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.chkManeja_Dolares = New DevExpress.XtraEditors.CheckEdit
        Me.clcDolares = New Dipros.Editors.TINCalcEdit
        Me.lblDolares = New System.Windows.Forms.Label
        Me.lblFormapago = New System.Windows.Forms.Label
        Me.clcTipoCambio = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManeja_Dolares.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDolares.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTipoCambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblFormasPago
        '
        Me.lblFormasPago.AutoSize = True
        Me.lblFormasPago.Location = New System.Drawing.Point(16, 40)
        Me.lblFormasPago.Name = "lblFormasPago"
        Me.lblFormasPago.Size = New System.Drawing.Size(98, 16)
        Me.lblFormasPago.TabIndex = 0
        Me.lblFormasPago.Tag = ""
        Me.lblFormasPago.Text = "&Formas de Pago:"
        Me.lblFormasPago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFormasPago
        '
        Me.lkpFormasPago.AllowAdd = False
        Me.lkpFormasPago.AutoReaload = False
        Me.lkpFormasPago.DataSource = Nothing
        Me.lkpFormasPago.DefaultSearchField = ""
        Me.lkpFormasPago.DisplayMember = "descripcion"
        Me.lkpFormasPago.EditValue = Nothing
        Me.lkpFormasPago.Filtered = False
        Me.lkpFormasPago.InitValue = Nothing
        Me.lkpFormasPago.Location = New System.Drawing.Point(120, 40)
        Me.lkpFormasPago.MultiSelect = False
        Me.lkpFormasPago.Name = "lkpFormasPago"
        Me.lkpFormasPago.NullText = ""
        Me.lkpFormasPago.PopupWidth = CType(400, Long)
        Me.lkpFormasPago.ReadOnlyControl = False
        Me.lkpFormasPago.Required = False
        Me.lkpFormasPago.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFormasPago.SearchMember = ""
        Me.lkpFormasPago.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFormasPago.SelectAll = False
        Me.lkpFormasPago.Size = New System.Drawing.Size(264, 20)
        Me.lkpFormasPago.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFormasPago.TabIndex = 1
        Me.lkpFormasPago.Tag = "forma_pago"
        Me.lkpFormasPago.ToolTip = Nothing
        Me.lkpFormasPago.ValueMember = "forma_pago"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(61, 64)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 2
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(120, 64)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "########0.00"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "########0.00"
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(88, 19)
        Me.clcImporte.TabIndex = 3
        Me.clcImporte.Tag = "importe"
        '
        'chkManeja_Dolares
        '
        Me.chkManeja_Dolares.EditValue = "False"
        Me.chkManeja_Dolares.Location = New System.Drawing.Point(24, 144)
        Me.chkManeja_Dolares.Name = "chkManeja_Dolares"
        '
        'chkManeja_Dolares.Properties
        '
        Me.chkManeja_Dolares.Properties.Caption = "Maneja Dolares"
        Me.chkManeja_Dolares.Properties.Enabled = False
        Me.chkManeja_Dolares.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkManeja_Dolares.Size = New System.Drawing.Size(120, 19)
        Me.chkManeja_Dolares.TabIndex = 65
        Me.chkManeja_Dolares.Tag = "maneja_dolares"
        '
        'clcDolares
        '
        Me.clcDolares.EditValue = "0"
        Me.clcDolares.Location = New System.Drawing.Point(120, 88)
        Me.clcDolares.MaxValue = 0
        Me.clcDolares.MinValue = 0
        Me.clcDolares.Name = "clcDolares"
        '
        'clcDolares.Properties
        '
        Me.clcDolares.Properties.DisplayFormat.FormatString = "c2"
        Me.clcDolares.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDolares.Properties.EditFormat.FormatString = "########0.00"
        Me.clcDolares.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDolares.Properties.Enabled = False
        Me.clcDolares.Properties.MaskData.EditMask = "########0.00"
        Me.clcDolares.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDolares.Size = New System.Drawing.Size(88, 19)
        Me.clcDolares.TabIndex = 5
        Me.clcDolares.Tag = "dolares"
        '
        'lblDolares
        '
        Me.lblDolares.AutoSize = True
        Me.lblDolares.Location = New System.Drawing.Point(63, 88)
        Me.lblDolares.Name = "lblDolares"
        Me.lblDolares.Size = New System.Drawing.Size(51, 16)
        Me.lblDolares.TabIndex = 4
        Me.lblDolares.Tag = ""
        Me.lblDolares.Text = "&Dolares:"
        Me.lblDolares.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFormapago
        '
        Me.lblFormapago.Location = New System.Drawing.Point(152, 144)
        Me.lblFormapago.Name = "lblFormapago"
        Me.lblFormapago.TabIndex = 66
        Me.lblFormapago.Tag = "nombre_forma_pago"
        Me.lblFormapago.Text = "Label2"
        Me.lblFormapago.Visible = False
        '
        'clcTipoCambio
        '
        Me.clcTipoCambio.EditValue = "0"
        Me.clcTipoCambio.Location = New System.Drawing.Point(120, 112)
        Me.clcTipoCambio.MaxValue = 0
        Me.clcTipoCambio.MinValue = 0
        Me.clcTipoCambio.Name = "clcTipoCambio"
        '
        'clcTipoCambio.Properties
        '
        Me.clcTipoCambio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipoCambio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipoCambio.Properties.Enabled = False
        Me.clcTipoCambio.Properties.MaskData.EditMask = "########0.00"
        Me.clcTipoCambio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTipoCambio.Size = New System.Drawing.Size(88, 19)
        Me.clcTipoCambio.TabIndex = 67
        Me.clcTipoCambio.Tag = "tipo_cambio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 16)
        Me.Label2.TabIndex = 68
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Tipo de Cambio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmMovimientosCajaFormasPago
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(410, 140)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcTipoCambio)
        Me.Controls.Add(Me.lblFormapago)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.chkManeja_Dolares)
        Me.Controls.Add(Me.lblFormasPago)
        Me.Controls.Add(Me.lkpFormasPago)
        Me.Controls.Add(Me.clcDolares)
        Me.Controls.Add(Me.lblDolares)
        Me.Name = "frmMovimientosCajaFormasPago"
        Me.Text = "frmMovimientosCajaFormasPago"
        Me.Controls.SetChildIndex(Me.lblDolares, 0)
        Me.Controls.SetChildIndex(Me.clcDolares, 0)
        Me.Controls.SetChildIndex(Me.lkpFormasPago, 0)
        Me.Controls.SetChildIndex(Me.lblFormasPago, 0)
        Me.Controls.SetChildIndex(Me.chkManeja_Dolares, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lblFormapago, 0)
        Me.Controls.SetChildIndex(Me.clcTipoCambio, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManeja_Dolares.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDolares.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTipoCambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oFormasPago As VillarrealBusiness.clsFormasPagos
    Private oCajerosFondos As VillarrealBusiness.clsCajerosFondos
    Private Cajero As Long = 0
    Private oMovimientosCobrar_formaspago As New VillarrealBusiness.clsMovimientosCobrarFormasPago
    
    Private ReadOnly Property TieneCajero() As Boolean
        Get
            TieneCajero = Comunes.clsUtilerias.uti_Usuariocajero(TINApp.Connection.User, Cajero)
        End Get
    End Property

    Private ReadOnly Property Tipo_Cambio() As Double
        Get
            Return Comunes.clsUtilerias.uti_TipoCambio(Comunes.Common.Cajero, TINApp.FechaServidor)
        End Get
    End Property

    Private ReadOnly Property forma_pago() As Long
        Get
            Return PreparaValorLookup(Me.lkpFormasPago)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmMovimientosCajaFormasPago_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oFormasPago = New VillarrealBusiness.clsFormasPagos
        oCajerosFondos = New VillarrealBusiness.clsCajerosFondos

        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    Me.clcImporte.EditValue = Me.OwnerForm.calculaimporteformapagar()
                Case Actions.Update

                Case Actions.Delete

            End Select
        End With

    End Sub
    Private Sub frmMovimientosCajaFormasPago_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub
    Private Sub frmArticulosPrecios_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

    End Sub

    Private Sub frmMovimientosCajaFormasPago_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMovimientosCobrar_formaspago.Validacion(Action, forma_pago, Me.chkManeja_Dolares.EditValue, Me.clcImporte.EditValue, Me.clcDolares.EditValue, False, "")
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpFormasPago_Format() Handles lkpFormasPago.Format
        Comunes.clsFormato.for_formas_pagos_grl(Me.lkpFormasPago)
    End Sub
    Private Sub lkpFormasPago_LoadData(ByVal Initialize As Boolean) Handles lkpFormasPago.LoadData
        Dim Response As New Events
        Response = oFormasPago.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFormasPago.DataSource = oDataSet.Tables(0)
            If Me.lkpFormasPago.DataSource.Rows.Count > 0 And CType(Me.OwnerForm, frmMovimientosCaja).cboTipoCobro.EditValue <> "P" Then Me.lkpFormasPago.EditValue = 1
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpFormasPago_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFormasPago.EditValueChanged
        chkManeja_Dolares.EditValue = Me.lkpFormasPago.GetValue("maneja_dolares")
        Me.lblFormapago.Text = Me.lkpFormasPago.GetValue("descripcion")
    End Sub

    Private Sub chkManeja_Dolares_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkManeja_Dolares.CheckedChanged
        If Me.chkManeja_Dolares.Checked = True Then
            Me.clcImporte.Enabled = False
            clcDolares.EditValue = 0
            Me.clcDolares.Enabled = True
            Me.clcImporte.EditValue = 0
        Else
            Me.clcImporte.Enabled = True
            Me.clcDolares.Enabled = False
            Me.clcImporte.EditValue = 0
            Me.clcDolares.EditValue = 0
            Me.clcTipoCambio.EditValue = 0
        End If
    End Sub

    Private Sub clcDolares_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcDolares.EditValueChanged
        If Me.clcDolares.IsLoading = True Then Exit Sub
        If Not IsNumeric(Me.clcDolares.Text) Then
            clcDolares.EditValue = 0
        End If
        Me.clcImporte.EditValue = Me.clcDolares.EditValue * Tipo_Cambio
        Me.clcTipoCambio.EditValue = Tipo_Cambio

    End Sub
    Private Sub clcImporte_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcImporte.EditValueChanged

    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region



    Private Sub frmMovimientosCajaFormasPago_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
