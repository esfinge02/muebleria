Imports Dipros.Utils
Imports System.Drawing

Public Class frmCambiosCheques
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grCambiosCheques As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvCambiosCheques As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaCambiosCheques As Dipros.Windows.TINMaster
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroCheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcMonto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCaja As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcQuien As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambiosCheques))
        Me.Label2 = New System.Windows.Forms.Label
        Me.grCambiosCheques = New DevExpress.XtraGrid.GridControl
        Me.grvCambiosCheques = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBanco = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroCheque = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMonto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCaja = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcQuien = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaCambiosCheques = New Dipros.Windows.TINMaster
        CType(Me.grCambiosCheques, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvCambiosCheques, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2039, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(209, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Cambios de cheque en el d�a de hoy:"
        '
        'grCambiosCheques
        '
        '
        'grCambiosCheques.EmbeddedNavigator
        '
        Me.grCambiosCheques.EmbeddedNavigator.Name = ""
        Me.grCambiosCheques.Location = New System.Drawing.Point(16, 90)
        Me.grCambiosCheques.MainView = Me.grvCambiosCheques
        Me.grCambiosCheques.Name = "grCambiosCheques"
        Me.grCambiosCheques.Size = New System.Drawing.Size(656, 224)
        Me.grCambiosCheques.TabIndex = 3
        Me.grCambiosCheques.Text = "GridControl1"
        '
        'grvCambiosCheques
        '
        Me.grvCambiosCheques.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcFecha, Me.grcPartida, Me.grcBanco, Me.grcNumeroCheque, Me.grcMonto, Me.grcSucursal, Me.grcCaja, Me.grcQuien})
        Me.grvCambiosCheques.GridControl = Me.grCambiosCheques
        Me.grvCambiosCheques.Name = "grvCambiosCheques"
        Me.grvCambiosCheques.OptionsView.ColumnAutoWidth = False
        Me.grvCambiosCheques.OptionsView.ShowGroupPanel = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "Partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 0
        Me.grcFecha.Width = 93
        '
        'grcBanco
        '
        Me.grcBanco.Caption = "Banco"
        Me.grcBanco.FieldName = "banco"
        Me.grcBanco.Name = "grcBanco"
        Me.grcBanco.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcBanco.VisibleIndex = 1
        Me.grcBanco.Width = 224
        '
        'grcNumeroCheque
        '
        Me.grcNumeroCheque.Caption = "N�mero de Cheque"
        Me.grcNumeroCheque.FieldName = "numero_cheque"
        Me.grcNumeroCheque.Name = "grcNumeroCheque"
        Me.grcNumeroCheque.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumeroCheque.VisibleIndex = 2
        Me.grcNumeroCheque.Width = 200
        '
        'grcMonto
        '
        Me.grcMonto.Caption = "Monto"
        Me.grcMonto.DisplayFormat.FormatString = "C2"
        Me.grcMonto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcMonto.FieldName = "monto"
        Me.grcMonto.Name = "grcMonto"
        Me.grcMonto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMonto.VisibleIndex = 3
        Me.grcMonto.Width = 123
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        '
        'grcCaja
        '
        Me.grcCaja.Caption = "Caja"
        Me.grcCaja.FieldName = "caja"
        Me.grcCaja.Name = "grcCaja"
        '
        'grcQuien
        '
        Me.grcQuien.Caption = "Quien"
        Me.grcQuien.FieldName = "QUIEN"
        Me.grcQuien.Name = "grcQuien"
        '
        'tmaCambiosCheques
        '
        Me.tmaCambiosCheques.BackColor = System.Drawing.Color.White
        Me.tmaCambiosCheques.CanDelete = True
        Me.tmaCambiosCheques.CanInsert = True
        Me.tmaCambiosCheques.CanUpdate = True
        Me.tmaCambiosCheques.Grid = Me.grCambiosCheques
        Me.tmaCambiosCheques.Location = New System.Drawing.Point(16, 64)
        Me.tmaCambiosCheques.Name = "tmaCambiosCheques"
        Me.tmaCambiosCheques.Size = New System.Drawing.Size(656, 25)
        Me.tmaCambiosCheques.TabIndex = 1
        Me.tmaCambiosCheques.Title = ""
        Me.tmaCambiosCheques.UpdateTitle = "un Registro"
        '
        'frmCambiosCheques
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(682, 328)
        Me.Controls.Add(Me.grCambiosCheques)
        Me.Controls.Add(Me.tmaCambiosCheques)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmCambiosCheques"
        Me.Text = "frmCambiosCheques"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.tmaCambiosCheques, 0)
        Me.Controls.SetChildIndex(Me.grCambiosCheques, 0)
        CType(Me.grCambiosCheques, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvCambiosCheques, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCambioCheques As New VillarrealBusiness.clsCambioCheques
#End Region

#Region "DIPROS Systems, Eventos de la forma"
    Private Sub frmCambiosCheques_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
        With Me.tmaCambiosCheques
            .UpdateTitle = "un Cambio de Cheque"
            .UpdateForm = New frmCambioChequesDetalle
            .AddColumn("fecha", "System.DateTime")
            .AddColumn("banco")
            .AddColumn("numero_cheque")
            .AddColumn("monto", "System.Double")
            .AddColumn("sucursal")
            .AddColumn("caja")
            .AddColumn("QUIEN")
        End With
        Response = oCambioCheques.Listado(Comunes.Common.Sucursal_Actual, Comunes.Common.Caja_Actual, Today())
        If Not Response.ErrorFound Then
            Me.tmaCambiosCheques.DataSource = Response.Value
        End If
        tbrTools.Buttons(0).Visible = False
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region


End Class
