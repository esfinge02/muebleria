Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Public Class frmGeneracionNotasCargoDescuentosAnticipados
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEnajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfactor_enajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents btnLlenar As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnVaciar As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcImporteNotaCargo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tlbNotasCargo As System.Windows.Forms.ToolBar
    Friend WithEvents tblImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcBonificar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcClaveCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnGuardar As System.Windows.Forms.ToolBarButton
    Public WithEvents lblTipoventa As System.Windows.Forms.Label
    Friend WithEvents cboTipoventa As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents UcSolicitaFormaPago2 As Comunes.ucSolicitaFormaPago
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGeneracionNotasCargoDescuentosAnticipados))
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEnajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfactor_enajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteNotaCargo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcBonificar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClaveCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.tlbNotasCargo = New System.Windows.Forms.ToolBar
        Me.btnLlenar = New System.Windows.Forms.ToolBarButton
        Me.btnVaciar = New System.Windows.Forms.ToolBarButton
        Me.tblImprimir = New System.Windows.Forms.ToolBarButton
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.btnGuardar = New System.Windows.Forms.ToolBarButton
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboTipoventa = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.UcSolicitaFormaPago2 = New Comunes.ucSolicitaFormaPago
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnGuardar, Me.tblImprimir})
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4731, 28)
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(8, 182)
        Me.grFacturas.MainView = Me.grvFacturas
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grFacturas.Size = New System.Drawing.Size(792, 312)
        Me.grFacturas.TabIndex = 7
        Me.grFacturas.Text = "Articulos"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcPartida, Me.grcFactura, Me.grcCliente, Me.grcImporte, Me.grcFolioFactura, Me.grcEnajenacion, Me.grcfactor_enajenacion, Me.grcImporteNotaCargo, Me.grcBonificar, Me.grcClaveCliente})
        Me.grvFacturas.GridControl = Me.grFacturas
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsCustomization.AllowFilter = False
        Me.grvFacturas.OptionsCustomization.AllowGroup = False
        Me.grvFacturas.OptionsCustomization.AllowSort = False
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        Me.grvFacturas.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 31
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        Me.chkSeleccionar.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Inactive
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "clave_compuesta"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 1
        Me.grcFactura.Width = 100
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Nombre Cliente"
        Me.grcCliente.FieldName = "nombre_cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.VisibleIndex = 3
        Me.grcCliente.Width = 181
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe Nota Cr�dito"
        Me.grcImporte.DisplayFormat.FormatString = "$###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe_bonificar"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 118
        '
        'grcFolioFactura
        '
        Me.grcFolioFactura.Caption = "Folio Factura"
        Me.grcFolioFactura.FieldName = "folio_factura"
        Me.grcFolioFactura.Name = "grcFolioFactura"
        '
        'grcEnajenacion
        '
        Me.grcEnajenacion.Caption = "Enajenacion"
        Me.grcEnajenacion.FieldName = "enajenacion"
        Me.grcEnajenacion.Name = "grcEnajenacion"
        Me.grcEnajenacion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcfactor_enajenacion
        '
        Me.grcfactor_enajenacion.Caption = "Factor Enajenacion"
        Me.grcfactor_enajenacion.FieldName = "factor_enajenacion"
        Me.grcfactor_enajenacion.Name = "grcfactor_enajenacion"
        Me.grcfactor_enajenacion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcImporteNotaCargo
        '
        Me.grcImporteNotaCargo.Caption = "Importe Nota Cargo"
        Me.grcImporteNotaCargo.DisplayFormat.FormatString = "$###,##0.00"
        Me.grcImporteNotaCargo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteNotaCargo.FieldName = "importe_nota_cargo_con_notas_pagadas"
        Me.grcImporteNotaCargo.Name = "grcImporteNotaCargo"
        Me.grcImporteNotaCargo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteNotaCargo.VisibleIndex = 5
        Me.grcImporteNotaCargo.Width = 109
        '
        'grcBonificar
        '
        Me.grcBonificar.Caption = "Bonificar"
        Me.grcBonificar.DisplayFormat.FormatString = "$###,##0.00"
        Me.grcBonificar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcBonificar.FieldName = "bonificar"
        Me.grcBonificar.Name = "grcBonificar"
        Me.grcBonificar.VisibleIndex = 6
        Me.grcBonificar.Width = 72
        '
        'grcClaveCliente
        '
        Me.grcClaveCliente.Caption = "Cliente"
        Me.grcClaveCliente.FieldName = "cliente"
        Me.grcClaveCliente.Name = "grcClaveCliente"
        Me.grcClaveCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcClaveCliente.VisibleIndex = 2
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(23, 88)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 4
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 6, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(68, 88)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 5
        Me.dteFecha.Tag = "fecha"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(8, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(68, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(264, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'tlbNotasCargo
        '
        Me.tlbNotasCargo.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tlbNotasCargo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tlbNotasCargo.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnLlenar, Me.btnVaciar})
        Me.tlbNotasCargo.ButtonSize = New System.Drawing.Size(77, 22)
        Me.tlbNotasCargo.Dock = System.Windows.Forms.DockStyle.None
        Me.tlbNotasCargo.DropDownArrows = True
        Me.tlbNotasCargo.ImageList = Me.ilsToolbar
        Me.tlbNotasCargo.Location = New System.Drawing.Point(8, 150)
        Me.tlbNotasCargo.Name = "tlbNotasCargo"
        Me.tlbNotasCargo.ShowToolTips = True
        Me.tlbNotasCargo.Size = New System.Drawing.Size(792, 29)
        Me.tlbNotasCargo.TabIndex = 6
        Me.tlbNotasCargo.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right
        '
        'btnLlenar
        '
        Me.btnLlenar.ImageIndex = 4
        Me.btnLlenar.Text = "Todos"
        '
        'btnVaciar
        '
        Me.btnVaciar.ImageIndex = 1
        Me.btnVaciar.Text = "Ninguno"
        '
        'tblImprimir
        '
        Me.tblImprimir.Text = "Imprimir"
        Me.tblImprimir.ToolTipText = "Impresion del Reporte de Notas de Cargo"
        Me.tblImprimir.Visible = False
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(17, 66)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 2
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "clave_nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(68, 64)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(264, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 3
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = "Seleccione un cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'btnGuardar
        '
        Me.btnGuardar.ImageIndex = 0
        Me.btnGuardar.Text = "Guardar"
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(432, 40)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(95, 16)
        Me.lblTipoventa.TabIndex = 59
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "Opci�n de Pago:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTipoventa.Visible = False
        '
        'cboTipoventa
        '
        Me.cboTipoventa.EditValue = "D"
        Me.cboTipoventa.Location = New System.Drawing.Point(536, 37)
        Me.cboTipoventa.Name = "cboTipoventa"
        '
        'cboTipoventa.Properties
        '
        Me.cboTipoventa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoventa.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pago en una sola Exhibici�n", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pago en 1 Parcialidad", "C", -1)})
        Me.cboTipoventa.Size = New System.Drawing.Size(168, 23)
        Me.cboTipoventa.TabIndex = 60
        Me.cboTipoventa.Tag = ""
        Me.cboTipoventa.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(8, 136)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(792, 8)
        Me.GroupBox1.TabIndex = 62
        Me.GroupBox1.TabStop = False
        '
        'UcSolicitaFormaPago2
        '
        Me.UcSolicitaFormaPago2.Location = New System.Drawing.Point(424, 63)
        Me.UcSolicitaFormaPago2.Name = "UcSolicitaFormaPago2"
        Me.UcSolicitaFormaPago2.Size = New System.Drawing.Size(320, 80)
        Me.UcSolicitaFormaPago2.TabIndex = 63
        Me.UcSolicitaFormaPago2.Visible = False
        '
        'frmGeneracionNotasCargoDescuentosAnticipados
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(810, 500)
        Me.Controls.Add(Me.UcSolicitaFormaPago2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblTipoventa)
        Me.Controls.Add(Me.cboTipoventa)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.tlbNotasCargo)
        Me.Name = "frmGeneracionNotasCargoDescuentosAnticipados"
        Me.Text = "Generaci�n de Notas de Cargo por Descuentos Anticipados"
        Me.Controls.SetChildIndex(Me.tlbNotasCargo, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.cboTipoventa, 0)
        Me.Controls.SetChildIndex(Me.lblTipoventa, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.UcSolicitaFormaPago2, 0)
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas
    Public oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oMovimientosCobrarFormasPago As VillarrealBusiness.clsMovimientosCobrarFormasPago

    Private oVariables As VillarrealBusiness.clsVariables
    Private intCajero As Long

    Private sConceptoCargoPorDescuentosAnticipados As String = ""

    'Private sConceptoDescuentosAnticipados As String = ""
    Private sConceptoFacturaCredito As String = ""
    Private _BonificacionObservaciones As String = ""

    Private factura_electronica As Boolean = False
    Private SerieNCargo As String = ""
    Private SerieAbono As String = ""
    Private ConceptoAbono As String
    Private banConceptoAbono As Boolean = False
    Private banValidaGuardar As Boolean = False


    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property Cajero() As Long
        Get
            If Comunes.clsUtilerias.uti_Usuariocajero(CStr(TinApp.Connection.User), intCajero) = True Then
                Return intCajero
            Else
                Return -1
            End If
        End Get
    End Property
    Private ReadOnly Property Concepto_Cargo_Por_Descuentos_Anticipados() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_nota_cargo_descuento_anticipado", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

    Private ReadOnly Property Concepto_Descuentos_Anticipados() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_descuentos_anticipados", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property Tipo_Cambio() As Double
        Get
            Return Comunes.clsUtilerias.uti_TipoCambio(Comunes.Common.Cajero, TinApp.FechaServidor)
        End Get
    End Property
    Private ReadOnly Property Concepto_Factura_Credito() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property

    Public WriteOnly Property BonificacionObservaciones() As String
        Set(ByVal Value As String)
            _BonificacionObservaciones = Value
        End Set
    End Property




    Private folio_movimiento As Long = 0


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmGeneracionNotasCargoDescuentosAnticipados_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmGeneracionNotasCargoDescuentosAnticipados_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmGeneracionNotasCargoDescuentosAnticipados_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub

    Private Sub frmGeneracionNotasCargoDescuentosAnticipados_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oVariables = New VillarrealBusiness.clsVariables
        Me.dteFecha.EditValue = Format(CDate(TinApp.FechaServidor), "dd/MMM/yyyy")

        TraeConceptoAbono()

        ' Sucursales
        '-----------------------------
        Dim oDataSet As DataSet
        Response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        oDataSet = CType(Response.Value, DataSet)
        factura_electronica = CType(oDataSet.Tables(0).Rows(0).Item("factura_electronica"), Boolean)
        SerieNCargo = CType(oDataSet.Tables(0).Rows(0).Item("serie_nota_cargo_electronica"), String)
        SerieAbono = CType(oDataSet.Tables(0).Rows(0).Item("serie_recibos_electronica"), String)

        Me.lblTipoventa.Visible = factura_electronica
        Me.cboTipoventa.Visible = factura_electronica
        Me.UcSolicitaFormaPago2.Visible = factura_electronica

        '-----------------------------

        If Comunes.clsUtilerias.uti_Usuariocajero(TinApp.Connection.User, Comunes.Common.Cajero) = False Then
            Me.cboTipoventa.Value = "C"
            Me.cboTipoventa.Enabled = False
            ShowMessage(MessageType.MsgInformation, "Este usuario No tiene un Cajero asignado", Me.Title)
        End If


    End Sub
    Private Sub frmGeneracionNotasCargoDescuentosAnticipados_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub
    Private Sub frmGeneracionNotasCargoDescuentosAnticipados_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        banValidaGuardar = False

        Response = ValidaCliente()
        If Response.ErrorFound Then Exit Sub
        Response = ValidaBonificar()
        If Response.ErrorFound Then Exit Sub
        Response = ValidaGrid()
        'If Not Response.ErrorFound Then Response = ValidaCajero()
        If Response.ErrorFound Then Exit Sub
        Response = ValidaOpcionPago()
        If Response.ErrorFound Then Exit Sub

        banValidaGuardar = True
    End Sub
    Private Sub frmGeneracionNotasCargoDescuentosAnticipados_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0

        Me.tbrTools.Buttons(0).Enabled = False
        Me.tbrTools.Buttons(0).Visible = False

        If VerificaPermisoExtendido(Me.MenuOption.Name, "FECHA_N_C_DESC") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        Me.grFacturas.DataSource = Nothing
        If Me.Sucursal > 0 Then
            CargarFacturas()
        End If



    End Sub

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCaja()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        CargarFacturas()
    End Sub

    Private Sub dteFecha_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha.EditValueChanged
        CargarFacturas()
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button.Text = "Imprimir" Then
            ImprimeListadoNotasCargo()
        End If

        If e.Button Is Me.btnGuardar And banValidaGuardar Then
            MostrarFormaBonificacionObservaciones()
        End If
    End Sub
    Private Sub tlbNotasCargo_ButtonClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tlbNotasCargo.ButtonClick
        If e.Button Is btnLlenar Then
            Dim oRow As DataRow
            For Each oRow In CType(grvFacturas.DataSource, DataView).Table.Rows
                oRow.Item("seleccionar") = True

            Next
        ElseIf e.Button Is btnVaciar Then
            Dim oRow As DataRow
            For Each oRow In CType(grvFacturas.DataSource, DataView).Table.Rows
                oRow.Item("seleccionar") = False
            Next
        End If

    End Sub
    Private Sub grvFacturas_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles grvFacturas.ValidatingEditor
        If CType(sender, DevExpress.XtraGrid.Views.Grid.GridView).FocusedColumn Is Me.grcBonificar Then
            Dim FILA As Integer = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView).FocusedRowHandle
            Dim VALOR_NOTA_CARGO As Double = Me.grvFacturas.GetRowCellValue(FILA, Me.grcImporteNotaCargo)
            If e.Value > VALOR_NOTA_CARGO Then
                e.ErrorText = "La Bonificacion es Mayor al Importe de la Nota de Cargo"
                e.Valid = False
            End If
        End If

    End Sub

    Private Sub cboTipoventa_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoventa.EditValueChanged
        If Me.cboTipoventa.Value = "C" Then
            Me.UcSolicitaFormaPago2.EnabledAll = False
        Else
            Me.UcSolicitaFormaPago2.EnabledAll = True
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarFacturas()
        If cliente <= 0 Then Exit Sub

        Dim Response As Dipros.Utils.Events
        Try
            Response = oVentas.VentasCreditoVencidasSinNotaCargo(Sucursal, cliente, Me.dteFecha.EditValue)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grFacturas.DataSource = oDataSet.Tables(0)

                If Me.grFacturas.DataSource Is Nothing Then
                    tlbNotasCargo.Enabled = False
                Else
                    If CType(Me.grvFacturas.DataSource, DataView).Table.Rows.Count > 0 Then
                        tlbNotasCargo.Enabled = True
                    Else
                        tlbNotasCargo.Enabled = False
                    End If

                End If

                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Function ValidaGrid() As Events
        Dim oevent As New Events

        Me.grvFacturas.CloseEditor()
        Me.grvFacturas.UpdateCurrentRow()

        If CType(Me.grFacturas.DataSource, DataTable).Select("seleccionar = 1").Length <= 0 Then
            oevent.Ex = Nothing
            oevent.Message = "Se Debe Seleccionar Al Menos Una Factura"
        End If
        Return oevent
    End Function
    Private Function ValidaBonificar() As Events
        Dim oevent As New Events
        Dim orow As DataRow

        For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows

            If Not IsNumeric(orow("bonificar")) Then
                oevent.Ex = Nothing
                oevent.Message = "El importe a bonificar debe ser un valor num�rico"
                Return oevent
            End If
        Next
        Return oevent
    End Function
    Private Function ValidaCliente() As Events
        Dim oevent As New Events
        If cliente <= 0 Then
            oevent.Ex = Nothing
            oevent.Message = "El Cliente es Requerido"
        End If
        Return oevent
    End Function
    Private Function ValidaCajero() As Events
        Dim oevent As New Events
        If Cajero <= 0 Then
            oevent.Ex = Nothing
            oevent.Message = "El Usuario No Tiene un Cajero Asignado"
        End If
        Return oevent
    End Function
    Private Function ValidaOpcionPago() As Events
        Dim response As New Events

        If factura_electronica = True And Me.cboTipoventa.Value = "D" Then
            ValidaOpcionPago = ValidaFormaPago(Me.UcSolicitaFormaPago2.FormaPago)
            If ValidaOpcionPago.ErrorFound Then Exit Function

            If Me.UcSolicitaFormaPago2.SolicitaUltimosDigitos Then
                ValidaOpcionPago = ValidaUltimosDigitos(Me.UcSolicitaFormaPago2.UltimosDigitos)
                If ValidaOpcionPago.ErrorFound Then Exit Function
            End If
        End If
        Return response
    End Function
    Private Function ValidaUltimosDigitos(ByVal ultimos_digitos As String) As Events
        Dim oEvent As New Events
        If Len(ultimos_digitos) <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los Ultimos d�gitos son Requeridos"
            Return oEvent
        Else
            If ultimos_digitos.Length < 4 Then
                oEvent.Ex = Nothing
                oEvent.Message = "Los Ultimos 4 d�gitos son Requeridos"
                Return oEvent
            End If
        End If
        Return oEvent

    End Function
    Private Function ValidaFormaPago(ByVal FormaPago As Long) As Events
        Dim oEvent As New Events
        If FormaPago <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Forma de Pago es requerida"
            Return oEvent
        End If
        Return oEvent
    End Function


    Private Sub ImprimeListadoNotasCargo()
        'Dim Response As Events
        'Dim oReportes As New VillarrealBusiness.Reportes
        'Try
        '    'Response = oReportes.ListadoNotasDeCredito(Sucursal, dteFecha.EditValue)
        '    Response = oReportes.ListadoNotasDeCargo(Sucursal, dteFecha.EditValue)

        '    If Response.ErrorFound Then
        '        ShowMessage(MessageType.MsgInformation, "La Impresi�n de las Notas de Cargo no pueden Mostrarse")
        '    Else
        '        Dim oDataSet As DataSet
        '        oDataSet = Response.Value

        '        If oDataSet.Tables(0).Rows.Count > 0 Then
        '            'Dim oReport As New rptNotasCredito
        '            Dim oReport As New rptNotasCargo
        '            oReport.DataSource = oDataSet.Tables(0)

        '            TINApp.ShowReport(Me.MdiParent, "Impresi�n de Listado de Notas de Cargo", oReport)
        '            oReport = Nothing
        '        Else
        '            ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
        '        End If

        '        oDataSet = Nothing
        '    End If
        'Catch ex As Exception
        '    ShowMessage(MessageType.MsgError, ex.ToString, )
        'End Try

    End Sub
    Private Sub ImprimeNotaDeCargo(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            Response = oReportes.ImprimeNotaDeCargo(sucursal, concepto, serie, folio, cliente, True)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Las Notas de Cargo no pueden Mostrarse")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptNotaCargoDescuentosAnticipados     'Clientes.rptNotaCargo
                    oReport.DataSource = oDataSet.Tables(0)

                    TinApp.PrintReport(oReport)
                    'TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cargo " & CType(folio, String), oReport)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

#End Region

    Private Sub MostrarFormaBonificacionObservaciones()
        Dim oformVerificaBonificacionObservaciones As New frmMovimientosCajaBonificacionObservaciones
        oformVerificaBonificacionObservaciones.MdiParent = Me.MdiParent
        oformVerificaBonificacionObservaciones.OwnerForm = Me
        oformVerificaBonificacionObservaciones.VentanaPadreAnterior = frmMovimientosCajaBonificacionObservaciones.VentanaPadre.NotasCargoAnticipados
        oformVerificaBonificacionObservaciones.Title = "Observaciones de la Bonificaci�n"

        Me.Enabled = False
        oformVerificaBonificacionObservaciones.Show()
    End Sub

    Public Sub GuardarCambios()
        Dim impuesto As Double
        Dim subtotal As Double
        Dim iva As Double
        Dim dImporte_proporcional_costo As Double
        Dim dIVa As Double
        Dim Response As New Events
        sConceptoCargoPorDescuentosAnticipados = Me.Concepto_Cargo_Por_Descuentos_Anticipados
        sConceptoFacturaCredito = Concepto_Factura_Credito
        impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)

        Dim orow As DataRow
        For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows
            If Not Response.ErrorFound And orow("seleccionar") = True Then

                If impuesto <> -1 Then

                    subtotal = (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")) / (1 + (impuesto / 100))
                    iva = (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")) - subtotal

                    dIVa = 1 + (impuesto / 100)
                Else
                    Response.Message = "El Porcentaje de Impuesto no esta definido"
                    Exit Sub
                End If

                If Not factura_electronica Then
                    SerieNCargo = Comunes.clsUtilerias.uti_SerieCajaNotaCargo(Comunes.Common.Caja_Actual)
                End If


                If (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")) > 0 Then
                    If Not Response.ErrorFound Then
                        Response = oMovimientosCobrar.InsertarCajas(Sucursal, sConceptoCargoPorDescuentosAnticipados, SerieNCargo, orow("cliente"), 0, _
                                                          Me.dteFecha.DateTime, Comunes.Common.Caja_Actual, Cajero, System.DBNull.Value, 1, (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), 0, subtotal, iva, _
                                                            (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), System.DBNull.Value, " Cargo Por Descuento Anticipado", "", Comunes.Common.Sucursal_Actual, orow("folio_movimiento"))

                        ' -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO
                        Dim observaciones As String
                        observaciones = "Nota de Cargo por Descuentos Anticipados generada para la Venta " + CType(orow("serie"), String) + "-" + CType(orow("folio_factura"), String)

                        If orow("enajenacion") = True Then
                            ' - SE AGREGO ESTE CALCULO PARA EL IMPORTE PROPORCIONAL DE LOS DOCUMENTOS DE LA NOTA DE CARGO
                            dImporte_proporcional_costo = ((orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")) / dIVa) * orow("factor_enajenacion")
                            dImporte_proporcional_costo = dImporte_proporcional_costo * -1
                        Else
                            dImporte_proporcional_costo = 0

                        End If

                        If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.Insertar(Sucursal, sConceptoCargoPorDescuentosAnticipados, SerieNCargo, orow("folio_movimiento"), orow("cliente"), _
                             1, 0, Me.dteFecha.DateTime, (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), Sucursal, sConceptoFacturaCredito, orow("serie"), orow("folio_factura"), orow("cliente"), -1, 0, CDate(TinApp.FechaServidor), (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), "", observaciones, dImporte_proporcional_costo)

                        observaciones = Nothing
                    End If
                End If

                ' Checa si existe el documento al cual se le va aplicar la nota de cargo, y si existe se actualiza el bit de nota de credito impresa
                If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.ExisteDocumento(Sucursal, sConceptoFacturaCredito, orow("serie"), orow("folio_factura"), orow("cliente"), orow("numero_documentos"))
                Dim ExisteDocumento As Boolean
                ExisteDocumento = CType(Response.Value, Boolean)
                If Not Response.ErrorFound And ExisteDocumento = True Then Response = oVentas.ActualizaNotaCargoGenerada(Sucursal, orow("serie"), orow("folio_factura"), (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), Me.dteFecha.DateTime)

                'Guardar el Registro de la Bonificacion
                If orow("bonificar") > 0 Then
                    Response = oMovimientosCobrar.InsertarBonificacion(Sucursal, Comunes.Common.Caja_Actual, dteFecha.EditValue, cliente, "N", orow("bonificar"), Me._BonificacionObservaciones, Comunes.clsUtilerias.uti_SerieCajaNotaCargo(Comunes.Common.Caja_Actual), orow("folio_movimiento"))
                End If


                If Me.factura_electronica Then

                    If Me.cboTipoventa.Value = "D" Then

                        Dim FolioAbono As Long = 0
                        Dim CobradorAbono As Long

                        CobradorAbono = ObtenerCobradorAbonoElectronico(cliente, Sucursal)

                        'Se inserta un movimiento por cobrar de Tipo Abono electronico para pagar la nota de cargo electronica
                        Response = Me.oMovimientosCobrar.InsertarCajas(Sucursal, ConceptoAbono, SerieAbono, cliente, 0, Me.dteFecha.DateTime.Date, Common.Caja_Actual, Common.Cajero, CobradorAbono, 1, 0, (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), subtotal, iva, (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), 0, System.DBNull.Value, "Abono generado por nota de cargo electronica", "", Sucursal, FolioAbono)
                        If Not Response.ErrorFound Then
                            Response = oMovimientosCobrarDetalle.Insertar(Sucursal, ConceptoAbono, SerieAbono, FolioAbono, cliente, 1, 1, Me.dteFecha.DateTime.Date, (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), Sucursal, sConceptoCargoPorDescuentosAnticipados, SerieNCargo, orow("folio_movimiento"), cliente, 1, 0, Me.dteFecha.DateTime.Date, 0, "N", "Abono Generado por una Nota de Cargo Electronica", 0)
                        End If

                        If Not Response.ErrorFound Then
                            Dim dolares As Double = 0
                            Dim TipoCambio As Double = Me.Tipo_Cambio

                            If Me.UcSolicitaFormaPago2.ManejaDolares Then
                                dolares = (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")) / TipoCambio
                            End If

                            oMovimientosCobrarFormasPago = New VillarrealBusiness.clsMovimientosCobrarFormasPago

                            Response = oMovimientosCobrarFormasPago.Insertar(Sucursal, ConceptoAbono, SerieAbono, FolioAbono, cliente, Me.UcSolicitaFormaPago2.FormaPago, Common.Sucursal_Actual, Me.dteFecha.DateTime, Common.Caja_Actual, Common.Cajero, (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")), TipoCambio, dolares, Me.UcSolicitaFormaPago2.UltimosDigitos)
                        End If
                    End If


                    If Not Response.ErrorFound Then
                        'Se llena la nota de cargo Electronica
                        Response = Me.oMovimientosCobrar.LllenarNotaCargoCFDI(Sucursal, sConceptoCargoPorDescuentosAnticipados, SerieNCargo, orow("folio_movimiento"), cliente, True, Common.Sucursal_Actual)
                        'Response = Me.oMovimientosCobrar.LllenarNotaCargoCFDI(Sucursal, sConceptoCargoPorDescuentosAnticipados, SerieNCargo, orow("folio_movimiento"), cliente, orow("ivadesglosado"), Common.Sucursal_Actual)
                        If Response.ErrorFound Then
                            Response.ShowError()
                            Exit Sub
                        End If
                    End If
                    'Fin de movimientos para factura electronica
                End If

            End If
        Next

        If Not Me.factura_electronica Then
            For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows
                If Not Response.ErrorFound And orow("seleccionar") = True And (orow("importe_nota_cargo_con_notas_pagadas") - orow("bonificar")) > 0 Then
                    Me.ImprimeNotaDeCargo(Sucursal, sConceptoCargoPorDescuentosAnticipados, Comunes.clsUtilerias.uti_SerieCajaNotaCargo(Comunes.Common.Caja_Actual), orow("folio_movimiento"), orow("cliente"))
                End If
            Next

        End If
        Me.Close()
    End Sub

    Private Sub TraeConceptoAbono()
        ConceptoAbono = CType(oVariables.TraeDatos("concepto_cxc_abono", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoAbono <> "" Then
            banConceptoAbono = True
        Else
            ShowMessage(MessageType.MsgInformation, "El Concepto de CXC de Abono no esta definido", "Variables del Sistema", Nothing, False)
        End If
    End Sub

    Private Function ObtenerCobradorAbonoElectronico(ByVal cliente_abono As Long, ByVal sucursal_abono As Long) As Long

        Dim cajeros As New VillarrealBusiness.clsClientesCobradores
        Dim Response As Events

        Response = cajeros.DespliegaDatosCobradoresVentas(cliente_abono, sucursal_abono)
        If Not Response.ErrorFound Then
            Dim odataset As DataSet

            odataset = CType(Response.Value, DataSet)

            If odataset.Tables(0).Rows.Count = 0 Then
                Return 15
            Else
                Return CType(odataset.Tables(0).Rows(0).Item("cobrador"), Long)

            End If

        Else
            Return 15
        End If

    End Function
    Dim _btn
End Class