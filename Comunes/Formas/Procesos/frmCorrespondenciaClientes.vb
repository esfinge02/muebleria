Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCorrespondenciaClientes
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents listCamposTabla As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblCorrespondencia As System.Windows.Forms.Label
    Friend WithEvents clcCorrespondencia As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTextoCorrespondencia As Comunes.TINRichTextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCorrespondenciaClientes))
        Me.listCamposTabla = New DevExpress.XtraEditors.ListBoxControl
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.lblCorrespondencia = New System.Windows.Forms.Label
        Me.clcCorrespondencia = New Dipros.Editors.TINCalcEdit
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.txtTextoCorrespondencia = New Comunes.TINRichTextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Panel1 = New System.Windows.Forms.Panel
        CType(Me.listCamposTabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCorrespondencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4499, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'listCamposTabla
        '
        Me.listCamposTabla.ItemHeight = 15
        Me.listCamposTabla.Location = New System.Drawing.Point(8, 24)
        Me.listCamposTabla.Name = "listCamposTabla"
        Me.listCamposTabla.Size = New System.Drawing.Size(144, 424)
        Me.listCamposTabla.TabIndex = 4
        '
        'lblDescripcion
        '
        Me.lblDescripcion.Location = New System.Drawing.Point(66, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(96, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Text = "Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCorrespondencia
        '
        Me.lblCorrespondencia.Location = New System.Drawing.Point(58, 40)
        Me.lblCorrespondencia.Name = "lblCorrespondencia"
        Me.lblCorrespondencia.Size = New System.Drawing.Size(104, 16)
        Me.lblCorrespondencia.TabIndex = 0
        Me.lblCorrespondencia.Text = "Correspondencia:"
        Me.lblCorrespondencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCorrespondencia
        '
        Me.clcCorrespondencia.EditValue = "0"
        Me.clcCorrespondencia.Location = New System.Drawing.Point(168, 40)
        Me.clcCorrespondencia.MaxValue = 0
        Me.clcCorrespondencia.MinValue = 0
        Me.clcCorrespondencia.Name = "clcCorrespondencia"
        '
        'clcCorrespondencia.Properties
        '
        Me.clcCorrespondencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCorrespondencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCorrespondencia.Properties.Enabled = False
        Me.clcCorrespondencia.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCorrespondencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCorrespondencia.Size = New System.Drawing.Size(48, 19)
        Me.clcCorrespondencia.TabIndex = 1
        Me.clcCorrespondencia.Tag = "correspondencia"
        Me.clcCorrespondencia.ToolTip = "correspondencia"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(168, 64)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 25
        Me.txtDescripcion.Size = New System.Drawing.Size(432, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        Me.txtDescripcion.ToolTip = "descripcion"
        '
        'txtTextoCorrespondencia
        '
        Me.txtTextoCorrespondencia.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtTextoCorrespondencia.Location = New System.Drawing.Point(0, 0)
        Me.txtTextoCorrespondencia.Name = "txtTextoCorrespondencia"
        Me.txtTextoCorrespondencia.Size = New System.Drawing.Size(710, 422)
        Me.txtTextoCorrespondencia.TabIndex = 59
        Me.txtTextoCorrespondencia.TextRtf = "{\rtf1\ansi\ansicpg1252\deff0\deflang2058{\fonttbl{\f0\fnil\fcharset0 Times New R" & _
        "oman;}}" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "{\*\generator Riched20 5.40.11.2210;}\viewkind4\uc1\pard\f0\fs24\par" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "}" & _
        "" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(0)
        Me.txtTextoCorrespondencia.TextSimple = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.listCamposTabla)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 96)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(880, 456)
        Me.GroupBox1.TabIndex = 60
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Correspondencia"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtTextoCorrespondencia)
        Me.Panel1.Location = New System.Drawing.Point(160, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(712, 424)
        Me.Panel1.TabIndex = 60
        '
        'frmCorrespondenciaClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(890, 560)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.clcCorrespondencia)
        Me.Controls.Add(Me.lblCorrespondencia)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Name = "frmCorrespondenciaClientes"
        Me.Text = "frmCorrespondenciaClientes"
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblCorrespondencia, 0)
        Me.Controls.SetChildIndex(Me.clcCorrespondencia, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.listCamposTabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCorrespondencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oCorrespondenciaClientes As New VillarrealBusiness.clsCorrespondenciaClientes
    Private oCorrespondenciaClientesDetalle As New VillarrealBusiness.clsCorrespondenciaClientesDetalle
    Private oCamposCorrespondencia As VillarrealBusiness.clsCamposCorrespondencia

    Private aCampos() As String


#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCorrespondenciaClientes_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmCorrespondenciaClientes_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub
    Private Sub frmCorrespondenciaClientes_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub

    Private Sub frmArticulos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        BuscarCamposCorrespondencia()
        Select Case Action
            Case Actions.Insert
                Response = oCorrespondenciaClientes.Insertar(Me.DataSource, Me.txtTextoCorrespondencia.TextRtf)

            Case Actions.Update
                Response = oCorrespondenciaClientes.Actualizar(Me.DataSource, Me.txtTextoCorrespondencia.TextRtf)

            Case Actions.Delete
                Response = Me.oCorrespondenciaClientesDetalle.Eliminar(Me.clcCorrespondencia.Value)
                Response = oCorrespondenciaClientes.Eliminar(Me.clcCorrespondencia.EditValue)

        End Select

        If Response.ErrorFound Then Exit Sub

        'Guardo los campos de la carta
        AccionesDetalles(Response)



    End Sub
    Private Sub frmArticulos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

        Response = oCorrespondenciaClientes.DespliegaDatos(OwnerForm.Value("correspondencia"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            Me.txtTextoCorrespondencia.TextRtf = oDataSet.Tables(0).Rows(0).Item("texto_correspondencia")
        End If

    End Sub
    Private Sub frmArticulos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCamposCorrespondencia = New VillarrealBusiness.clsCamposCorrespondencia
        oCorrespondenciaClientes = New VillarrealBusiness.clsCorrespondenciaClientes
        LLenaCamposTablas()
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
                Me.txtTextoCorrespondencia.Enabled = False
        End Select

    End Sub
    Private Sub frmArticulos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCorrespondenciaClientes.Validacion(Action, Me.txtDescripcion.Text, Me.txtTextoCorrespondencia.TextSimple)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub listCamposTabla_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles listCamposTabla.DoubleClick
        Me.txtTextoCorrespondencia.TextSimple = Me.txtTextoCorrespondencia.TextSimple & " " & Me.listCamposTabla.SelectedValue
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub LLenaCamposTablas()

        Dim response As New Dipros.Utils.Events

        response = oCamposCorrespondencia.Listado
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            Dim i As Long

            odataset = response.Value
            Me.listCamposTabla.Items.Clear()
            For i = 1 To odataset.Tables(0).Rows.Count
                Me.listCamposTabla.Items.Add(odataset.Tables(0).Rows.Item(i - 1).Item("campo_correspondencia"))
            Next
        Else
            ShowMessage(MessageType.MsgError, "Error al Cargar los Campos de la Tabla")
        End If

    End Sub
    Private Sub BuscarCamposCorrespondencia()
        Dim acamposvalidos() As String
        Dim i As Long = 0
        Dim j As Long = 0
        Dim caracteres_invalidos As Char() = {" ", Chr(13), "-", Chr(10), ""}

        acamposvalidos = Me.txtTextoCorrespondencia.TextSimple.Split(caracteres_invalidos)


        For i = 0 To acamposvalidos.Length - 1
            If CType(acamposvalidos.GetValue(i), String).Trim.Length > 0 Then
                If CType(acamposvalidos.GetValue(i), String).Substring(0, 1) = "@" Then
                    ReDim Preserve aCampos(j)
                    aCampos(j) = acamposvalidos.GetValue(i)
                    j = j + 1
                End If
            End If
        Next


    End Sub

    Private Sub AccionesDetalles(ByRef response As Events)
        Dim I As Long

        With aCampos
            response = Me.oCorrespondenciaClientesDetalle.Eliminar(Me.clcCorrespondencia.Value)
            If Not response.ErrorFound Then
                For i = 0 To .Length - 1
                    Select Case Action
                        Case Actions.Insert, Actions.Update
                            response = Me.oCorrespondenciaClientesDetalle.Insertar(Me.clcCorrespondencia.Value, I + 1, .GetValue(I))
                        Case Actions.Delete
                    End Select
                Next

            End If
            
        End With
    End Sub

#End Region


    
End Class
