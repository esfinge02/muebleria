Imports Dipros.Utils
Public Class brwCapturaVisitasClientes
    Inherits Dipros.Windows.frmTINGridNet

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteFechaCorte As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label
        Me.dteFechaCorte = New DevExpress.XtraEditors.DateEdit
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.Label4 = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FilterPanel.SuspendLayout()
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        '
        'FooterPanel
        '
        Me.FooterPanel.Name = "FooterPanel"
        Me.FooterPanel.Size = New System.Drawing.Size(792, 36)
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.Label4)
        Me.FilterPanel.Controls.Add(Me.lkpSucursal)
        Me.FilterPanel.Controls.Add(Me.Label3)
        Me.FilterPanel.Controls.Add(Me.dteFechaCorte)
        Me.FilterPanel.Controls.Add(Me.lblCobrador)
        Me.FilterPanel.Controls.Add(Me.lkpCobrador)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(792, 69)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.dteFechaCorte, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label3, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label4, 0)
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(592, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Tag = ""
        Me.Label3.Text = "Fecha Corte:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaCorte
        '
        Me.dteFechaCorte.EditValue = "29/05/2008"
        Me.dteFechaCorte.Location = New System.Drawing.Point(664, 12)
        Me.dteFechaCorte.Name = "dteFechaCorte"
        '
        'dteFechaCorte.Properties
        '
        Me.dteFechaCorte.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCorte.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCorte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaCorte.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCorte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaCorte.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaCorte.TabIndex = 5
        Me.dteFechaCorte.Tag = "fecha_corte"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(16, 38)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(55, 16)
        Me.lblCobrador.TabIndex = 2
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = True
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(72, 37)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = ""
        Me.lkpCobrador.PopupWidth = CType(400, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = False
        Me.lkpCobrador.Size = New System.Drawing.Size(248, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 3
        Me.lkpCobrador.Tag = "Cobrador"
        Me.lkpCobrador.ToolTip = Nothing
        Me.lkpCobrador.ValueMember = "Cobrador"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Tag = ""
        Me.Label4.Text = "Sucursal:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(72, 12)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(248, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'brwCapturaVisitasClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(792, 468)
        Me.Name = "brwCapturaVisitasClientes"
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private oCobradores As New VillarrealBusiness.clsCobradores
    Private oCapturaVisitasClientes As New VillarrealBusiness.clsCapturaVisitasClientes
    Private oSucursales As New VillarrealBusiness.clsSucursales

    Public ReadOnly Property Cobrador() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCobrador)
        End Get

    End Property

    Public ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get

    End Property

    Public ReadOnly Property FechaCorte() As DateTime
        Get
            Return Me.dteFechaCorte.DateTime
        End Get


    End Property

    Private Sub brwCapturaVisitasClientes_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        Response = oCapturaVisitasClientes.Listado(Cobrador, Me.dteFechaCorte.DateTime, sucursal)
    End Sub
    Private Sub brwCapturaVisitasClientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FooterPanel.Visible = False
        dteFechaCorte.EditValue = CDate(TinApp.FechaServidor())
    End Sub



    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing

    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        Me.Reload(True)
    End Sub

    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub
    Private Sub lkpCobrador_LoadData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData
        Dim Response As New Events
        'Response = oClientesCobradores.DespliegaDatosCobradoresVentas(Cliente, Comunes.Common.Sucursal_Actual)
        Response = oCobradores.Lookup(Comunes.Common.Sucursal_Actual)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCobrador_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCobrador.EditValueChanged
        Me.Reload(True)
    End Sub

    Private Sub dteFechaCorte_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFechaCorte.EditValueChanged
        If IsDate(Me.dteFechaCorte.EditValue) And dteFechaCorte.IsLoading = False Then
            Me.Reload(True)
        End If
    End Sub


End Class
