Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Windows.Forms

Public Class DescuentosMasivosMegaCred
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grCuentasExcel As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvCuentasExcel As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnCargar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtArchivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnArchivo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConvenio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpCajero As Dipros.Editors.TINMultiLookup
    Friend WithEvents UcSolicitaFormaPago2 As Comunes.ucSolicitaFormaPago
    Friend WithEvents btnValidar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkValidado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grcExisteCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcExisteClienteConvenio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoMayor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents grcClavePago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNoEmpleado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcRFC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gbxQuincenaAnio As System.Windows.Forms.GroupBox
    Friend WithEvents lblQuincena As System.Windows.Forms.Label
    Friend WithEvents clcQuincena As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents clcAnio As Dipros.Editors.TINCalcEdit
    Friend WithEvents btnSubirProductos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblConvenioCambiado As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(DescuentosMasivosMegaCred))
        Me.grCuentasExcel = New DevExpress.XtraGrid.GridControl
        Me.grvCuentasExcel = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcConvenio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcExisteCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcExisteClienteConvenio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoMayor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClavePago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNoEmpleado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcRFC = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.txtArchivo = New DevExpress.XtraEditors.TextEdit
        Me.btnCargar = New DevExpress.XtraEditors.SimpleButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnArchivo = New DevExpress.XtraEditors.SimpleButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpCajero = New Dipros.Editors.TINMultiLookup
        Me.UcSolicitaFormaPago2 = New Comunes.ucSolicitaFormaPago
        Me.btnValidar = New DevExpress.XtraEditors.SimpleButton
        Me.chkValidado = New DevExpress.XtraEditors.CheckEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.gbxQuincenaAnio = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.clcAnio = New Dipros.Editors.TINCalcEdit
        Me.lblQuincena = New System.Windows.Forms.Label
        Me.clcQuincena = New Dipros.Editors.TINCalcEdit
        Me.btnSubirProductos = New DevExpress.XtraEditors.SimpleButton
        Me.lblConvenioCambiado = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        CType(Me.grCuentasExcel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvCuentasExcel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArchivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkValidado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.gbxQuincenaAnio.SuspendLayout()
        CType(Me.clcAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcQuincena.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(11001, 28)
        '
        'grCuentasExcel
        '
        '
        'grCuentasExcel.EmbeddedNavigator
        '
        Me.grCuentasExcel.EmbeddedNavigator.Name = ""
        Me.grCuentasExcel.Location = New System.Drawing.Point(8, 224)
        Me.grCuentasExcel.MainView = Me.grvCuentasExcel
        Me.grCuentasExcel.Name = "grCuentasExcel"
        Me.grCuentasExcel.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.grCuentasExcel.Size = New System.Drawing.Size(920, 296)
        Me.grCuentasExcel.Styles.AddReplace("Style3", New DevExpress.Utils.ViewStyleEx("Style3", "", New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.FromArgb(CType(255, Byte), CType(128, Byte), CType(0, Byte)), System.Drawing.Color.White, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCuentasExcel.Styles.AddReplace("Style2", New DevExpress.Utils.ViewStyleEx("Style2", "", New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.FromArgb(CType(0, Byte), CType(192, Byte), CType(0, Byte)), System.Drawing.Color.White, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCuentasExcel.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Brown, System.Drawing.Color.White, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grCuentasExcel.TabIndex = 14
        '
        'grvCuentasExcel
        '
        Me.grvCuentasExcel.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcConvenio, Me.GridColumn2, Me.grcCuenta, Me.grcCantidad, Me.grcSucursal, Me.grcExisteCliente, Me.grcExisteClienteConvenio, Me.grcSaldoMayor, Me.grcClavePago, Me.grcNoEmpleado, Me.grcRFC})
        Me.grvCuentasExcel.GridControl = Me.grCuentasExcel
        Me.grvCuentasExcel.Name = "grvCuentasExcel"
        Me.grvCuentasExcel.OptionsView.ShowFooter = True
        Me.grvCuentasExcel.OptionsView.ShowGroupPanel = False
        '
        'grcConvenio
        '
        Me.grcConvenio.Caption = "CONVENIO"
        Me.grcConvenio.FieldName = "CONVENIO"
        Me.grcConvenio.Name = "grcConvenio"
        Me.grcConvenio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConvenio.VisibleIndex = 0
        Me.grcConvenio.Width = 59
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "NOMBRE"
        Me.GridColumn2.FieldName = "NOMBRE"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 348
        '
        'grcCuenta
        '
        Me.grcCuenta.Caption = "CUENTA"
        Me.grcCuenta.FieldName = "CUENTA"
        Me.grcCuenta.Name = "grcCuenta"
        Me.grcCuenta.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCuenta.VisibleIndex = 3
        Me.grcCuenta.Width = 72
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "CANTIDAD"
        Me.grcCantidad.DisplayFormat.FormatString = "c2"
        Me.grcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcCantidad.FieldName = "CANTIDAD"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcCantidad.VisibleIndex = 7
        Me.grcCantidad.Width = 73
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "SUCURSAL"
        Me.grcSucursal.FieldName = "SUCURSAL"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursal.VisibleIndex = 1
        Me.grcSucursal.Width = 74
        '
        'grcExisteCliente
        '
        Me.grcExisteCliente.Caption = "EXISTE_CLIENTE"
        Me.grcExisteCliente.FieldName = "EXISTE_CLIENTE"
        Me.grcExisteCliente.Name = "grcExisteCliente"
        Me.grcExisteCliente.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcExisteCliente.Width = 54
        '
        'grcExisteClienteConvenio
        '
        Me.grcExisteClienteConvenio.Caption = "EXISTE_CLIENTE_CONVENIO"
        Me.grcExisteClienteConvenio.FieldName = "EXISTE_CLIENTE_CONVENIO"
        Me.grcExisteClienteConvenio.Name = "grcExisteClienteConvenio"
        Me.grcExisteClienteConvenio.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcExisteClienteConvenio.Width = 60
        '
        'grcSaldoMayor
        '
        Me.grcSaldoMayor.Caption = "SALDO_MAYOR"
        Me.grcSaldoMayor.FieldName = "SALDO_MAYOR"
        Me.grcSaldoMayor.Name = "grcSaldoMayor"
        Me.grcSaldoMayor.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoMayor.Width = 54
        '
        'grcClavePago
        '
        Me.grcClavePago.Caption = "CLAVE PAGO"
        Me.grcClavePago.FieldName = "CLAVE_PAGO"
        Me.grcClavePago.Name = "grcClavePago"
        Me.grcClavePago.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcClavePago.VisibleIndex = 4
        '
        'grcNoEmpleado
        '
        Me.grcNoEmpleado.Caption = "NO. EMPLEADO"
        Me.grcNoEmpleado.FieldName = "NO_EMPLEADO"
        Me.grcNoEmpleado.Name = "grcNoEmpleado"
        Me.grcNoEmpleado.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNoEmpleado.VisibleIndex = 5
        '
        'grcRFC
        '
        Me.grcRFC.Caption = "RFC"
        Me.grcRFC.FieldName = "RFC"
        Me.grcRFC.Name = "grcRFC"
        Me.grcRFC.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcRFC.VisibleIndex = 6
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'txtArchivo
        '
        Me.txtArchivo.EditValue = ""
        Me.txtArchivo.Location = New System.Drawing.Point(80, 125)
        Me.txtArchivo.Name = "txtArchivo"
        '
        'txtArchivo.Properties
        '
        Me.txtArchivo.Properties.Enabled = False
        Me.txtArchivo.Size = New System.Drawing.Size(736, 20)
        Me.txtArchivo.TabIndex = 4
        '
        'btnCargar
        '
        Me.btnCargar.Location = New System.Drawing.Point(872, 125)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(56, 20)
        Me.btnCargar.TabIndex = 6
        Me.btnCargar.Text = "Cargar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 128)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Archivo:"
        '
        'btnArchivo
        '
        Me.btnArchivo.Location = New System.Drawing.Point(816, 125)
        Me.btnArchivo.Name = "btnArchivo"
        Me.btnArchivo.Size = New System.Drawing.Size(56, 20)
        Me.btnArchivo.TabIndex = 5
        Me.btnArchivo.Text = "Archivo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(32, 152)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 16)
        Me.Label3.TabIndex = 7
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Cajero:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCajero
        '
        Me.lkpCajero.AllowAdd = False
        Me.lkpCajero.AutoReaload = False
        Me.lkpCajero.DataSource = Nothing
        Me.lkpCajero.DefaultSearchField = ""
        Me.lkpCajero.DisplayMember = "nombre"
        Me.lkpCajero.EditValue = Nothing
        Me.lkpCajero.Filtered = False
        Me.lkpCajero.InitValue = Nothing
        Me.lkpCajero.Location = New System.Drawing.Point(80, 149)
        Me.lkpCajero.MultiSelect = False
        Me.lkpCajero.Name = "lkpCajero"
        Me.lkpCajero.NullText = ""
        Me.lkpCajero.PopupWidth = CType(400, Long)
        Me.lkpCajero.ReadOnlyControl = False
        Me.lkpCajero.Required = False
        Me.lkpCajero.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCajero.SearchMember = ""
        Me.lkpCajero.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCajero.SelectAll = False
        Me.lkpCajero.Size = New System.Drawing.Size(304, 20)
        Me.lkpCajero.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCajero.TabIndex = 8
        Me.lkpCajero.Tag = ""
        Me.lkpCajero.ToolTip = "cajero"
        Me.lkpCajero.ValueMember = "cajero"
        '
        'UcSolicitaFormaPago2
        '
        Me.UcSolicitaFormaPago2.Location = New System.Drawing.Point(536, 40)
        Me.UcSolicitaFormaPago2.Name = "UcSolicitaFormaPago2"
        Me.UcSolicitaFormaPago2.Size = New System.Drawing.Size(384, 80)
        Me.UcSolicitaFormaPago2.TabIndex = 9
        '
        'btnValidar
        '
        Me.btnValidar.Location = New System.Drawing.Point(720, 149)
        Me.btnValidar.Name = "btnValidar"
        Me.btnValidar.Size = New System.Drawing.Size(88, 32)
        Me.btnValidar.TabIndex = 11
        Me.btnValidar.Text = "Validar"
        '
        'chkValidado
        '
        Me.chkValidado.Location = New System.Drawing.Point(824, 157)
        Me.chkValidado.Name = "chkValidado"
        '
        'chkValidado.Properties
        '
        Me.chkValidado.Properties.Caption = "Archivo V�lido"
        Me.chkValidado.Properties.Enabled = False
        Me.chkValidado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlText)
        Me.chkValidado.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.Color.Black)
        Me.chkValidado.Size = New System.Drawing.Size(104, 19)
        Me.chkValidado.TabIndex = 12
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 184)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(912, 33)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(192, Byte), CType(0, Byte))
        Me.Label6.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(256, 11)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(287, 18)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "El Convenio del Cliente no esta Asignado"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(128, Byte), CType(0, Byte))
        Me.Label5.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(600, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(300, 18)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "El Saldo del Cliente es Menor que el Abono"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Red
        Me.Label4.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(18, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(137, 18)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "El Cliente no Existe"
        '
        'gbxQuincenaAnio
        '
        Me.gbxQuincenaAnio.Controls.Add(Me.Label7)
        Me.gbxQuincenaAnio.Controls.Add(Me.clcAnio)
        Me.gbxQuincenaAnio.Controls.Add(Me.lblQuincena)
        Me.gbxQuincenaAnio.Controls.Add(Me.clcQuincena)
        Me.gbxQuincenaAnio.Location = New System.Drawing.Point(80, 57)
        Me.gbxQuincenaAnio.Name = "gbxQuincenaAnio"
        Me.gbxQuincenaAnio.Size = New System.Drawing.Size(304, 63)
        Me.gbxQuincenaAnio.TabIndex = 2
        Me.gbxQuincenaAnio.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(62, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(30, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Tag = ""
        Me.Label7.Text = "A�o:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAnio
        '
        Me.clcAnio.EditValue = "0"
        Me.clcAnio.Location = New System.Drawing.Point(96, 35)
        Me.clcAnio.MaxValue = 0
        Me.clcAnio.MinValue = 0
        Me.clcAnio.Name = "clcAnio"
        '
        'clcAnio.Properties
        '
        Me.clcAnio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcAnio.Properties.MaxLength = 4
        Me.clcAnio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAnio.Size = New System.Drawing.Size(48, 19)
        Me.clcAnio.TabIndex = 5
        Me.clcAnio.Tag = ""
        '
        'lblQuincena
        '
        Me.lblQuincena.AutoSize = True
        Me.lblQuincena.Location = New System.Drawing.Point(32, 12)
        Me.lblQuincena.Name = "lblQuincena"
        Me.lblQuincena.Size = New System.Drawing.Size(60, 16)
        Me.lblQuincena.TabIndex = 2
        Me.lblQuincena.Tag = ""
        Me.lblQuincena.Text = "Quincena:"
        Me.lblQuincena.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcQuincena
        '
        Me.clcQuincena.EditValue = "0"
        Me.clcQuincena.Location = New System.Drawing.Point(96, 12)
        Me.clcQuincena.MaxValue = 0
        Me.clcQuincena.MinValue = 0
        Me.clcQuincena.Name = "clcQuincena"
        '
        'clcQuincena.Properties
        '
        Me.clcQuincena.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincena.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincena.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcQuincena.Properties.MaxLength = 2
        Me.clcQuincena.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcQuincena.Size = New System.Drawing.Size(48, 19)
        Me.clcQuincena.TabIndex = 3
        Me.clcQuincena.Tag = ""
        '
        'btnSubirProductos
        '
        Me.btnSubirProductos.Location = New System.Drawing.Point(616, 149)
        Me.btnSubirProductos.Name = "btnSubirProductos"
        Me.btnSubirProductos.Size = New System.Drawing.Size(96, 32)
        Me.btnSubirProductos.TabIndex = 10
        Me.btnSubirProductos.Text = "Subir Productos"
        '
        'lblConvenioCambiado
        '
        Me.lblConvenioCambiado.AutoSize = True
        Me.lblConvenioCambiado.Location = New System.Drawing.Point(16, 37)
        Me.lblConvenioCambiado.Name = "lblConvenioCambiado"
        Me.lblConvenioCambiado.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenioCambiado.TabIndex = 0
        Me.lblConvenioCambiado.Tag = ""
        Me.lblConvenioCambiado.Text = "Convenio:"
        Me.lblConvenioCambiado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(80, 37)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = ""
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(304, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 1
        Me.lkpConvenio.Tag = ""
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'DescuentosMasivosMegaCred
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(938, 532)
        Me.Controls.Add(Me.lblConvenioCambiado)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.btnSubirProductos)
        Me.Controls.Add(Me.gbxQuincenaAnio)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkValidado)
        Me.Controls.Add(Me.btnValidar)
        Me.Controls.Add(Me.UcSolicitaFormaPago2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpCajero)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grCuentasExcel)
        Me.Controls.Add(Me.btnCargar)
        Me.Controls.Add(Me.txtArchivo)
        Me.Controls.Add(Me.btnArchivo)
        Me.Name = "DescuentosMasivosMegaCred"
        Me.Text = "DescuentosMasivosMegaCred"
        Me.Controls.SetChildIndex(Me.btnArchivo, 0)
        Me.Controls.SetChildIndex(Me.txtArchivo, 0)
        Me.Controls.SetChildIndex(Me.btnCargar, 0)
        Me.Controls.SetChildIndex(Me.grCuentasExcel, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpCajero, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.UcSolicitaFormaPago2, 0)
        Me.Controls.SetChildIndex(Me.btnValidar, 0)
        Me.Controls.SetChildIndex(Me.chkValidado, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.gbxQuincenaAnio, 0)
        Me.Controls.SetChildIndex(Me.btnSubirProductos, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioCambiado, 0)
        CType(Me.grCuentasExcel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvCuentasExcel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArchivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkValidado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.gbxQuincenaAnio.ResumeLayout(False)
        CType(Me.clcAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcQuincena.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oCajeros As VillarrealBusiness.clsCajeros
    Private oClientes As VillarrealBusiness.clsClientes
    Private oProductos As VillarrealBusiness.clsProductos
    Private oProductosDetalle As VillarrealBusiness.clsProductosDetalle
    Private oDescuentosMasivosMegaCred As VillarrealBusiness.clsDescuentosMasivosMegaCred
    Private oConvenios As VillarrealBusiness.clsConvenios



    Dim conexion As New OleDbConnection
    Dim comando As New OleDbCommand
    Dim adaptador As New OleDbDataAdapter

    Dim ExcelPath As String = ""
    Dim hoja As String = ""
    Dim num As Integer
    Dim extension As String = ""

    Private ReadOnly Property Cajero() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCajero)
        End Get
    End Property

    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property


    Private Sub DescuentosMasivosMegaCred_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub DescuentosMasivosMegaCred_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub DescuentosMasivosMegaCred_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub

    Private Sub DescuentosMasivosMegaCred_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCajeros = New VillarrealBusiness.clsCajeros
        oClientes = New VillarrealBusiness.clsClientes
        oProductos = New VillarrealBusiness.clsProductos
        oProductosDetalle = New VillarrealBusiness.clsProductosDetalle
        oConvenios = New VillarrealBusiness.clsConvenios

        oDescuentosMasivosMegaCred = New VillarrealBusiness.clsDescuentosMasivosMegaCred
    End Sub
    Private Sub DescuentosMasivosMegaCred_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oDescuentosMasivosMegaCred.Eliminar()

        If Not Response.ErrorFound Then
            Dim i As Long
            Dim cliente As Long
            Dim convenio As Long
            Dim importe_abono As Double
            Dim sucursal As Long

            For i = 0 To Me.grvCuentasExcel.RowCount - 1
                convenio = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcConvenio)
                cliente = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcCuenta)
                importe_abono = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcCantidad)
                sucursal = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcSucursal)

                Response = oDescuentosMasivosMegaCred.Insertar(convenio, cliente, sucursal, importe_abono)
                If Response.ErrorFound Then Exit Sub
            Next

            If Not Response.ErrorFound Then
                Response = oDescuentosMasivosMegaCred.ProcesarAbonosMegaCred(Common.Caja_Actual, Me.Cajero, Me.UcSolicitaFormaPago2.FormaPago, Me.UcSolicitaFormaPago2.UltimosDigitos)
            End If

            If Not Response.ErrorFound Then
                Response = Me.oProductos.ActualizarEstatus(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue, "A", CDate(TinApp.FechaServidor))
            End If

        End If
    End Sub
    Private Sub DescuentosMasivosMegaCred_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oDescuentosMasivosMegaCred.Validacion(Me.UcSolicitaFormaPago2.FormaPago, Me.UcSolicitaFormaPago2.SolicitaUltimosDigitos, Me.UcSolicitaFormaPago2.UltimosDigitos, Me.chkValidado.Checked, Me.Convenio, Me.Cajero)
    End Sub

    Private Sub btnCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargar.Click
        CargarClientes()
    End Sub
    Private Sub btnArchivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnArchivo.Click
        Dim openFD As New OpenFileDialog
        With openFD
            .Title = "Seleccionar archivos"
            .Filter = "Archivos de Excel(*.xlsx)|*.xlsx| Archivos Excel 97-2003 (*.xls)|*.xls"
            .Multiselect = False

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtArchivo.Text = .FileName

                ExcelPath = txtArchivo.Text.ToLower()
                num = Trim(txtArchivo.Text.ToLower()).Length
                extension = Trim(txtArchivo.Text.ToLower()).Substring(num - 4, 4)
            End If
        End With
    End Sub
    Private Sub btnValidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidar.Click
        Dim i As Long
        Dim cliente As Long
        Dim convenio As Long
        Dim importe_abono As Double

        Dim clave_pago As String
        Dim no_empleado As String
        Dim rfc As String

        Dim response As New Events
        Dim valido As Boolean = True


        For i = 0 To Me.grvCuentasExcel.RowCount - 1

            If Not (Me.grvCuentasExcel.GetRowCellValue(i, Me.grcConvenio) Is System.DBNull.Value) Then
                convenio = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcConvenio)

                cliente = IIf(Me.grvCuentasExcel.GetRowCellValue(i, Me.grcCuenta) Is System.DBNull.Value, 0, Me.grvCuentasExcel.GetRowCellValue(i, Me.grcCuenta))
                clave_pago = IIf(Me.grvCuentasExcel.GetRowCellValue(i, Me.grcClavePago) Is System.DBNull.Value, "", Me.grvCuentasExcel.GetRowCellValue(i, Me.grcClavePago))
                no_empleado = IIf(Me.grvCuentasExcel.GetRowCellValue(i, Me.grcNoEmpleado) Is System.DBNull.Value, "", Me.grvCuentasExcel.GetRowCellValue(i, Me.grcNoEmpleado))
                rfc = IIf(Me.grvCuentasExcel.GetRowCellValue(i, Me.grcRFC) Is System.DBNull.Value, "", Me.grvCuentasExcel.GetRowCellValue(i, Me.grcRFC))
                importe_abono = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcCantidad)


                response = oClientes.ValidarClienteConvenioMegaCred(cliente, convenio, clave_pago, no_empleado, rfc, importe_abono)
                If Not response.ErrorFound Then
                    Dim respuesta As Long

                    respuesta = response.Value

                    If respuesta > 0 Then
                        valido = False
                    End If

                    Me.grvCuentasExcel.SetRowCellValue(i, Me.grcCuenta, cliente)
                    Select Case respuesta
                        Case 1
                            Me.grvCuentasExcel.SetRowCellValue(i, Me.grcExisteCliente, True)
                        Case 2
                            Me.grvCuentasExcel.SetRowCellValue(i, Me.grcExisteClienteConvenio, True)
                        Case 3
                            Me.grvCuentasExcel.SetRowCellValue(i, Me.grcSaldoMayor, True)

                    End Select

                End If
            End If

        Next
        Me.grvCuentasExcel.UpdateCurrentRow()
        Me.grvCuentasExcel.CloseEditor()

        Me.chkValidado.Checked = valido
    End Sub

    Private Sub lkpcajero_Format() Handles lkpCajero.Format
        Comunes.clsFormato.for_cajeros_grl(Me.lkpCajero)
    End Sub
    Private Sub lkpcajero_LoadData(ByVal Initialize As Boolean) Handles lkpCajero.LoadData
        Dim response As Events

        response = oCajeros.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpCajero.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub
    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub

    Private Sub btnSubirProductos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubirProductos.Click
        Dim response As New Events
        response = Me.oDescuentosMasivosMegaCred.ValidaConvenio(Convenio)
        If Not response.ErrorFound Then
            SubirProductos()
        Else
            ShowMessage(MessageType.MsgError, response.Message, Me.Title)
        End If

    End Sub

    Private Sub grvCuentasExcel_RowCellStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles grvCuentasExcel.RowCellStyle
        Dim nColumExisteCliente As DevExpress.XtraGrid.Columns.GridColumn
        Dim nColumExisteClienteConvenio As DevExpress.XtraGrid.Columns.GridColumn
        Dim nColumSaldoMayor As DevExpress.XtraGrid.Columns.GridColumn


        nColumExisteCliente = Me.grvCuentasExcel.Columns.Item("EXISTE_CLIENTE")
        nColumExisteClienteConvenio = Me.grvCuentasExcel.Columns.Item("EXISTE_CLIENTE_CONVENIO")
        nColumSaldoMayor = Me.grvCuentasExcel.Columns.Item("SALDO_MAYOR")

        If Not Me.grvCuentasExcel.GetRowCellValue(e.RowHandle, nColumExisteCliente) Is DBNull.Value Then
            If Me.grvCuentasExcel.GetRowCellValue(e.RowHandle, nColumExisteCliente) = 1 Then
                e.CellStyle = Me.grCuentasExcel.Styles.Item("Style1")
            Else
                If Me.grvCuentasExcel.GetRowCellValue(e.RowHandle, nColumExisteClienteConvenio) = 1 Then
                    e.CellStyle = Me.grCuentasExcel.Styles.Item("Style2")
                Else
                    If Me.grvCuentasExcel.GetRowCellValue(e.RowHandle, nColumSaldoMayor) = 1 Then
                        e.CellStyle = Me.grCuentasExcel.Styles.Item("Style3")
                    End If
                End If
            End If
        End If


    End Sub


#Region "Funciones"

#Region "funcionalidad"
    Private Function CargarClientes() As Boolean
        Dim dsexcel As New DataSet
        Try
            Me.grCuentasExcel.DataSource = Nothing

            hoja = "Cuentas"

            If extension = ".xls" Then
                conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ExcelPath & "; Extended Properties= ""Excel 8.0;HDR=YES"""
            Else
                conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
            End If

            conexion.Open()
            comando.CommandText = "SELECT * FROM [" & hoja & "$]"
            comando.Connection = conexion
            adaptador.SelectCommand = comando
            conexion.Close()
            adaptador.Fill(dsexcel)

            RevisarFilasVacias(dsexcel.Tables(0))
            AgregarColumnasValidacion(dsexcel.Tables(0))

            Me.grCuentasExcel.DataSource = dsexcel.Tables(0)
            'Dim limite As Integer = dgvDatos.RowCount
            'ReDim arraycols(dgvDatos.Columns.Count)


        Catch ex As Exception
            MsgBox("El archivo no contiene la informaci�n requerida. Verifique la informaci�n por favor", MsgBoxStyle.Exclamation, "Ventas y comisiones")
        Finally
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try

    End Function
    Private Sub AgregarColumnasValidacion(ByRef oDataTable As DataTable)
        Dim oColumnExisteCliente As New DataColumn
        oColumnExisteCliente.DataType = System.Type.GetType("System.Decimal")
        oColumnExisteCliente.DefaultValue = 0
        oColumnExisteCliente.Caption = "Existe Cliente"
        oColumnExisteCliente.ColumnName = "EXISTE_CLIENTE"

        oDataTable.Columns.Add(oColumnExisteCliente)

        Dim oColumnExisteClienteConvenio As New DataColumn
        oColumnExisteClienteConvenio.DataType = System.Type.GetType("System.Decimal")
        oColumnExisteClienteConvenio.DefaultValue = 0
        oColumnExisteClienteConvenio.Caption = "EXISTE_CLIENTE_CONVENIO"
        oColumnExisteClienteConvenio.ColumnName = "EXISTE_CLIENTE_CONVENIO"

        oDataTable.Columns.Add(oColumnExisteClienteConvenio)


        Dim oColumnSaldoMayor As New DataColumn
        oColumnSaldoMayor.DataType = System.Type.GetType("System.Decimal")
        oColumnSaldoMayor.DefaultValue = 0
        oColumnSaldoMayor.Caption = "SALDO_MAYOR"
        oColumnSaldoMayor.ColumnName = "SALDO_MAYOR"


        oDataTable.Columns.Add(oColumnSaldoMayor)
    End Sub
    Private Sub RevisarFilasVacias(ByRef oTabla As DataTable)
        Dim i As Long

        For i = oTabla.Rows.Count - 1 To 0 Step -1
            If (oTabla.Rows(i).Item("convenio") Is System.DBNull.Value) And (oTabla.Rows(i).Item("nombre") Is System.DBNull.Value) Then
                oTabla.Rows(i).Delete()
            End If
        Next
    End Sub
#End Region

#Region "productos"
    Private Sub SubirProductos()
        ' Se valida que el archivo sea valido y que tenga filas el grid
        If Me.chkValidado.Checked And Me.grvCuentasExcel.RowCount > 0 Then
            If Me.clcQuincena.EditValue <= 0 Then
                ShowMessage(MessageType.MsgError, "La Quincena es Requerida", Me.Title)
                Exit Sub
            End If

            If Me.clcAnio.EditValue <= 0 Then
                ShowMessage(MessageType.MsgError, "El a�o es Requerido", Me.Title)
                Exit Sub
            End If

            Dim response As New Events
            Dim HuboModificacion As Boolean = False

            ' Se valida si ya existe el producto para el convenio, quincena y a�o capturados 
            response = oProductos.Existe(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue)
            If Not response.ErrorFound Then
                Dim existe As Long
                existe = response.Value

                TinApp.Connection.Begin()

                ' si no existe se inserta
                If existe = 0 Then
                    response = InsertaProducto()
                    HuboModificacion = True
                Else
                    ' si ya existe producto se pregunta si desea cambiar la informacion del producto
                    If ShowMessage(MessageType.MsgQuestion, "El producto ya existe para los datos capturados, Deseas reemplazar la informacion almacenada?", "Valida Productos") = Answer.MsgYes Then
                        response = ReemplazaProductos()
                        HuboModificacion = True
                    End If
                End If

                'Se verifica si no hubo algun error en los procesos de los productos
                If Not response.ErrorFound And HuboModificacion Then
                    TinApp.Connection.Commit()
                    ShowMessage(MessageType.MsgInformation, "Los Productos Fueron grabados correctamente", "Producots")
                Else
                    TinApp.Connection.Rollback()
                    ShowMessage(MessageType.MsgError, "Error al grabar los productos", "Producots")
                End If

            End If
        Else
            ShowMessage(MessageType.MsgError, "Archivo no v�lido", Me.Title)
        End If

    End Sub
    Private Function InsertaProducto() As Events
        Dim response As New Events

        'Mando a grabar el encabezado de la tabla de productos
        response = Me.oProductos.Insertar(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue, CDate(TinApp.FechaServidor), "P")
        If Not response.ErrorFound Then

            Dim i As Long
            Dim convenio_aplica As Object
            Dim cliente As Long
            Dim importe_abono As Double


            'Mando a grabar el detalle del producto en base al grid
            For i = 0 To Me.grvCuentasExcel.RowCount - 1

                convenio_aplica = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcConvenio)
                cliente = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcCuenta)
                importe_abono = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcCantidad)

                response = Me.oProductosDetalle.Insertar(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue, cliente, convenio_aplica, importe_abono)

                If response.ErrorFound Then
                    Exit Function
                End If
            Next
        End If

        Return response
    End Function
    Private Function ReemplazaProductos() As Events
        Dim response As New Events
        Dim i As Long
        Dim cliente As Long



        'Mando a eliminar el detalle del producto en base al grid
        For i = 0 To Me.grvCuentasExcel.RowCount - 1
            cliente = Me.grvCuentasExcel.GetRowCellValue(i, Me.grcCuenta)
            response = Me.oProductosDetalle.Eliminar(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue, cliente)

            If response.ErrorFound Then
                Exit Function
            End If

        Next

        'Mando a Eliminar el Encabezado del Producto
        If Not response.ErrorFound Then
            response = oProductos.Eliminar(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue)
        End If

        'Mando a Insertar los productos
        If Not response.ErrorFound Then
            response = InsertaProducto()
        End If

        Return response
    End Function
#End Region


#End Region

   
End Class

