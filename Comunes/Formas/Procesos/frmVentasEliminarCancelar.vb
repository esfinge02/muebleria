Public Class frmVentasEliminarCancelar
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtObservacionesCancelacion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnEliminar = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancelar = New DevExpress.XtraEditors.SimpleButton
        Me.txtObservacionesCancelacion = New DevExpress.XtraEditors.MemoEdit
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.txtObservacionesCancelacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(208, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "La Venta a Cancelar es del d�a de hoy."
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "�Desea Eliminar o Cancelar la Venta?"
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(32, 136)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.Text = "&Eliminar"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(120, 136)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "Ca&ncelar"
        '
        'txtObservacionesCancelacion
        '
        Me.txtObservacionesCancelacion.EditValue = ""
        Me.txtObservacionesCancelacion.Location = New System.Drawing.Point(8, 80)
        Me.txtObservacionesCancelacion.Name = "txtObservacionesCancelacion"
        Me.txtObservacionesCancelacion.Size = New System.Drawing.Size(208, 48)
        Me.txtObservacionesCancelacion.TabIndex = 3
        Me.txtObservacionesCancelacion.Tag = ""
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 23)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Observaciones:"
        '
        'frmVentasEliminarCancelar
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(224, 166)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtObservacionesCancelacion)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVentasEliminarCancelar"
        Me.Text = "Ventas"
        CType(Me.txtObservacionesCancelacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public OwnerForm As Object
    Private banEliminar As Boolean = False
    Private banCancelar As Boolean = False

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        banEliminar = True
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        banCancelar = True
        Me.Close()
    End Sub

    Private Sub frmVentasEliminarCancelar_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        If banEliminar Then
            OwnerForm.EliminarVenta(Me.txtObservacionesCancelacion.Text)
        End If
        If banCancelar Then
            OwnerForm.CancelarVenta(Me.txtObservacionesCancelacion.Text)
        End If
        OwnerForm.Enabled = True
    End Sub

    Private Sub frmVentasEliminarCancelar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Left = 100
        Me.Top = 100
    End Sub
End Class
