Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepVentasxFechaEntrega
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboRepartir As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Entrega As DevExpress.XtraEditors.DateEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepVentasxFechaEntrega))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.dteFecha_Entrega = New DevExpress.XtraEditors.DateEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboRepartir = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label21 = New System.Windows.Forms.Label
        CType(Me.dteFecha_Entrega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboRepartir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(234, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(48, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(112, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = True
        Me.lkpSucursal.Size = New System.Drawing.Size(232, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'dteFecha_Entrega
        '
        Me.dteFecha_Entrega.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Entrega.Location = New System.Drawing.Point(112, 64)
        Me.dteFecha_Entrega.Name = "dteFecha_Entrega"
        '
        'dteFecha_Entrega.Properties
        '
        Me.dteFecha_Entrega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Entrega.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Entrega.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Entrega.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Entrega.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Entrega.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Entrega.TabIndex = 3
        Me.dteFecha_Entrega.ToolTip = "Fecha Desde"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Fecha Entrega:"
        '
        'cboRepartir
        '
        Me.cboRepartir.EditValue = 1
        Me.cboRepartir.Location = New System.Drawing.Point(112, 88)
        Me.cboRepartir.Name = "cboRepartir"
        '
        'cboRepartir.Properties
        '
        Me.cboRepartir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboRepartir.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ma�ana", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tarde", 2, -1)})
        Me.cboRepartir.Size = New System.Drawing.Size(96, 20)
        Me.cboRepartir.TabIndex = 5
        Me.cboRepartir.Tag = "repartir"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(54, 88)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(50, 16)
        Me.Label21.TabIndex = 4
        Me.Label21.Tag = ""
        Me.Label21.Text = "H&orario:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmRepVentasxFechaEntrega
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 120)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.cboRepartir)
        Me.Controls.Add(Me.dteFecha_Entrega)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Name = "frmRepVentasxFechaEntrega"
        Me.Text = "frmRepFechaEntregaVentas"
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Entrega, 0)
        Me.Controls.SetChildIndex(Me.cboRepartir, 0)
        Me.Controls.SetChildIndex(Me.Label21, 0)
        CType(Me.dteFecha_Entrega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboRepartir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oreportes As VillarrealBusiness.Reportes



    Private ReadOnly Property Sucursal() As String
        Get
            If Me.lkpSucursal.EditValue = Nothing Then
                Return -1
            Else
                Return Me.lkpSucursal.EditValue
            End If
        End Get

    End Property

    Private Sub frmRepVentasxFechaEntrega_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oreportes = New VillarrealBusiness.Reportes

        Me.dteFecha_Entrega.EditValue = CDate(TinApp.FechaServidor)
    End Sub
    Private Sub frmRepVentasxFechaEntrega_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oreportes.VentasxFechaEntrega(Sucursal, Me.dteFecha_Entrega.EditValue, CType(Me.cboRepartir.Value, String))

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptVentasxFechaEntrega

                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)

                TinApp.ShowReport(Me.MdiParent, "Ventas por Fecha de Entrega", oReport)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub


    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub frmRepVentasxFechaEntrega_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oreportes.ValidaFecha(Me.dteFecha_Entrega.Text)
    End Sub
End Class
