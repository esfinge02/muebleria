Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepSaldosVencidos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblTipoVenta As System.Windows.Forms.Label
    Friend WithEvents cboTipoVenta As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpClienteInicio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente1 As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodegaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkSinAbono As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpPlanCredito As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblPlanCredito As System.Windows.Forms.Label
    Friend WithEvents clcMesesSinAbonar As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcFaltenLiquidar As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkParaCliente As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkImprimirDireccion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblConvenioCambiado As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepSaldosVencidos))
        Me.lblTipoVenta = New System.Windows.Forms.Label
        Me.cboTipoVenta = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpClienteInicio = New Dipros.Editors.TINMultiLookup
        Me.lblCliente1 = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.lkpBodegaSalida = New Dipros.Editors.TINMultiLookup
        Me.chkSinAbono = New DevExpress.XtraEditors.CheckEdit
        Me.lkpPlanCredito = New Dipros.Editors.TINMultiLookup
        Me.lblPlanCredito = New System.Windows.Forms.Label
        Me.clcMesesSinAbonar = New DevExpress.XtraEditors.CalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcFaltenLiquidar = New DevExpress.XtraEditors.CalcEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkParaCliente = New DevExpress.XtraEditors.CheckEdit
        Me.chkImprimirDireccion = New DevExpress.XtraEditors.CheckEdit
        Me.lblConvenioCambiado = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSinAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMesesSinAbonar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFaltenLiquidar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.chkParaCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkImprimirDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2092, 28)
        '
        'lblTipoVenta
        '
        Me.lblTipoVenta.AutoSize = True
        Me.lblTipoVenta.Location = New System.Drawing.Point(38, 139)
        Me.lblTipoVenta.Name = "lblTipoVenta"
        Me.lblTipoVenta.Size = New System.Drawing.Size(68, 16)
        Me.lblTipoVenta.TabIndex = 12
        Me.lblTipoVenta.Tag = ""
        Me.lblTipoVenta.Text = "&Tipo Venta:"
        Me.lblTipoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoVenta
        '
        Me.cboTipoVenta.EditValue = "T"
        Me.cboTipoVenta.Location = New System.Drawing.Point(111, 136)
        Me.cboTipoVenta.Name = "cboTipoVenta"
        '
        'cboTipoVenta.Properties
        '
        Me.cboTipoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoVenta.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enajenaci�n", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Normales", 3, -1)})
        Me.cboTipoVenta.Size = New System.Drawing.Size(136, 23)
        Me.cboTipoVenta.TabIndex = 13
        Me.cboTipoVenta.Tag = "tipo"
        Me.cboTipoVenta.ToolTip = "Tipo de Venta"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(46, 91)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 6
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lblSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSucursal.ForeColor = System.Drawing.Color.Black
        Me.lblSucursal.Location = New System.Drawing.Point(50, 43)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(111, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Ninguna)"
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(382, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = "Sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lkpClienteInicio
        '
        Me.lkpClienteInicio.AllowAdd = False
        Me.lkpClienteInicio.AutoReaload = False
        Me.lkpClienteInicio.BackColor = System.Drawing.SystemColors.Window
        Me.lkpClienteInicio.DataSource = Nothing
        Me.lkpClienteInicio.DefaultSearchField = ""
        Me.lkpClienteInicio.DisplayMember = "cliente"
        Me.lkpClienteInicio.EditValue = Nothing
        Me.lkpClienteInicio.Filtered = False
        Me.lkpClienteInicio.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpClienteInicio.ForeColor = System.Drawing.Color.Black
        Me.lkpClienteInicio.InitValue = Nothing
        Me.lkpClienteInicio.Location = New System.Drawing.Point(111, 64)
        Me.lkpClienteInicio.MultiSelect = False
        Me.lkpClienteInicio.Name = "lkpClienteInicio"
        Me.lkpClienteInicio.NullText = "(Todos)"
        Me.lkpClienteInicio.PopupWidth = CType(400, Long)
        Me.lkpClienteInicio.ReadOnlyControl = False
        Me.lkpClienteInicio.Required = False
        Me.lkpClienteInicio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteInicio.SearchMember = ""
        Me.lkpClienteInicio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteInicio.SelectAll = False
        Me.lkpClienteInicio.Size = New System.Drawing.Size(382, 20)
        Me.lkpClienteInicio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteInicio.TabIndex = 3
        Me.lkpClienteInicio.Tag = ""
        Me.lkpClienteInicio.ToolTip = "Cliente Desde"
        Me.lkpClienteInicio.ValueMember = "cliente"
        '
        'lblCliente1
        '
        Me.lblCliente1.AutoSize = True
        Me.lblCliente1.BackColor = System.Drawing.SystemColors.Window
        Me.lblCliente1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente1.ForeColor = System.Drawing.Color.Black
        Me.lblCliente1.Location = New System.Drawing.Point(59, 67)
        Me.lblCliente1.Name = "lblCliente1"
        Me.lblCliente1.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente1.TabIndex = 2
        Me.lblCliente1.Tag = ""
        Me.lblCliente1.Text = "&Cliente:"
        Me.lblCliente1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(65, 115)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "&Fecha:"
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 8, 2, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(111, 112)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(136, 23)
        Me.dteFecha.TabIndex = 9
        Me.dteFecha.ToolTip = "Fecha"
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = False
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(111, 88)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = "(Todos)"
        Me.lkpCobrador.PopupWidth = CType(300, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = True
        Me.lkpCobrador.Size = New System.Drawing.Size(382, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 7
        Me.lkpCobrador.Tag = "cobrador"
        Me.lkpCobrador.ToolTip = "Cobrador"
        Me.lkpCobrador.ValueMember = "cobrador"
        '
        'lkpBodegaSalida
        '
        Me.lkpBodegaSalida.AllowAdd = False
        Me.lkpBodegaSalida.AutoReaload = False
        Me.lkpBodegaSalida.DataSource = Nothing
        Me.lkpBodegaSalida.DefaultSearchField = ""
        Me.lkpBodegaSalida.DisplayMember = ""
        Me.lkpBodegaSalida.EditValue = Nothing
        Me.lkpBodegaSalida.Filtered = False
        Me.lkpBodegaSalida.InitValue = Nothing
        Me.lkpBodegaSalida.Location = New System.Drawing.Point(113, 88)
        Me.lkpBodegaSalida.MultiSelect = True
        Me.lkpBodegaSalida.Name = "lkpBodegaSalida"
        Me.lkpBodegaSalida.NullText = "(Todos)"
        Me.lkpBodegaSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegaSalida.ReadOnlyControl = False
        Me.lkpBodegaSalida.Required = False
        Me.lkpBodegaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaSalida.SearchMember = ""
        Me.lkpBodegaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaSalida.SelectAll = False
        Me.lkpBodegaSalida.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodegaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaSalida.TabIndex = 0
        Me.lkpBodegaSalida.ToolTip = Nothing
        Me.lkpBodegaSalida.ValueMember = ""
        '
        'chkSinAbono
        '
        Me.chkSinAbono.Location = New System.Drawing.Point(335, 147)
        Me.chkSinAbono.Name = "chkSinAbono"
        '
        'chkSinAbono.Properties
        '
        Me.chkSinAbono.Properties.Caption = "Sin Ningun Abono"
        Me.chkSinAbono.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkSinAbono.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkSinAbono.Size = New System.Drawing.Size(128, 22)
        Me.chkSinAbono.TabIndex = 18
        '
        'lkpPlanCredito
        '
        Me.lkpPlanCredito.AllowAdd = False
        Me.lkpPlanCredito.AutoReaload = False
        Me.lkpPlanCredito.DataSource = Nothing
        Me.lkpPlanCredito.DefaultSearchField = ""
        Me.lkpPlanCredito.DisplayMember = "Descripcion"
        Me.lkpPlanCredito.EditValue = Nothing
        Me.lkpPlanCredito.Filtered = False
        Me.lkpPlanCredito.InitValue = Nothing
        Me.lkpPlanCredito.Location = New System.Drawing.Point(111, 160)
        Me.lkpPlanCredito.MultiSelect = False
        Me.lkpPlanCredito.Name = "lkpPlanCredito"
        Me.lkpPlanCredito.NullText = "(Todos)"
        Me.lkpPlanCredito.PopupWidth = CType(400, Long)
        Me.lkpPlanCredito.ReadOnlyControl = False
        Me.lkpPlanCredito.Required = False
        Me.lkpPlanCredito.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlanCredito.SearchMember = ""
        Me.lkpPlanCredito.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlanCredito.SelectAll = False
        Me.lkpPlanCredito.Size = New System.Drawing.Size(191, 20)
        Me.lkpPlanCredito.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPlanCredito.TabIndex = 15
        Me.lkpPlanCredito.Tag = "plan_credito"
        Me.lkpPlanCredito.ToolTip = "Plan de Credito"
        Me.lkpPlanCredito.ValueMember = "plan_credito"
        '
        'lblPlanCredito
        '
        Me.lblPlanCredito.AutoSize = True
        Me.lblPlanCredito.Location = New System.Drawing.Point(13, 163)
        Me.lblPlanCredito.Name = "lblPlanCredito"
        Me.lblPlanCredito.Size = New System.Drawing.Size(93, 16)
        Me.lblPlanCredito.TabIndex = 14
        Me.lblPlanCredito.Tag = ""
        Me.lblPlanCredito.Text = "&Plan de Cr�dito:"
        Me.lblPlanCredito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMesesSinAbonar
        '
        Me.clcMesesSinAbonar.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcMesesSinAbonar.Location = New System.Drawing.Point(120, 18)
        Me.clcMesesSinAbonar.Name = "clcMesesSinAbonar"
        Me.clcMesesSinAbonar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcMesesSinAbonar.Size = New System.Drawing.Size(56, 20)
        Me.clcMesesSinAbonar.TabIndex = 1
        Me.clcMesesSinAbonar.ToolTip = "Meses Sin Abonar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(107, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Tag = ""
        Me.Label2.Text = "Meses Sin Abonar:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(44, 187)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(197, 16)
        Me.Label3.TabIndex = 16
        Me.Label3.Tag = ""
        Me.Label3.Text = "Documentos Faltantes de Liquidar:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFaltenLiquidar
        '
        Me.clcFaltenLiquidar.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFaltenLiquidar.Location = New System.Drawing.Point(246, 184)
        Me.clcFaltenLiquidar.Name = "clcFaltenLiquidar"
        Me.clcFaltenLiquidar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFaltenLiquidar.Size = New System.Drawing.Size(56, 20)
        Me.clcFaltenLiquidar.TabIndex = 17
        Me.clcFaltenLiquidar.ToolTip = "Documentos Faltantes"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.clcMesesSinAbonar)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(309, 152)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(184, 48)
        Me.GroupBox1.TabIndex = 66
        Me.GroupBox1.TabStop = False
        '
        'chkParaCliente
        '
        Me.chkParaCliente.Location = New System.Drawing.Point(400, 112)
        Me.chkParaCliente.Name = "chkParaCliente"
        '
        'chkParaCliente.Properties
        '
        Me.chkParaCliente.Properties.Caption = "Para Cliente"
        Me.chkParaCliente.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkParaCliente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkParaCliente.Size = New System.Drawing.Size(96, 22)
        Me.chkParaCliente.TabIndex = 11
        '
        'chkImprimirDireccion
        '
        Me.chkImprimirDireccion.Location = New System.Drawing.Point(260, 112)
        Me.chkImprimirDireccion.Name = "chkImprimirDireccion"
        '
        'chkImprimirDireccion.Properties
        '
        Me.chkImprimirDireccion.Properties.Caption = "Imprimir Direcci�n"
        Me.chkImprimirDireccion.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkImprimirDireccion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkImprimirDireccion.Size = New System.Drawing.Size(128, 22)
        Me.chkImprimirDireccion.TabIndex = 10
        '
        'lblConvenioCambiado
        '
        Me.lblConvenioCambiado.AutoSize = True
        Me.lblConvenioCambiado.Location = New System.Drawing.Point(46, 208)
        Me.lblConvenioCambiado.Name = "lblConvenioCambiado"
        Me.lblConvenioCambiado.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenioCambiado.TabIndex = 67
        Me.lblConvenioCambiado.Tag = ""
        Me.lblConvenioCambiado.Text = "Convenio:"
        Me.lblConvenioCambiado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(112, 208)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = "(Todos)"
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(192, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 68
        Me.lkpConvenio.Tag = ""
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'frmRepSaldosVencidos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(506, 236)
        Me.Controls.Add(Me.lblConvenioCambiado)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.chkImprimirDireccion)
        Me.Controls.Add(Me.chkParaCliente)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.clcFaltenLiquidar)
        Me.Controls.Add(Me.lkpPlanCredito)
        Me.Controls.Add(Me.lblPlanCredito)
        Me.Controls.Add(Me.chkSinAbono)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblCliente1)
        Me.Controls.Add(Me.lkpClienteInicio)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.lkpCobrador)
        Me.Controls.Add(Me.lblTipoVenta)
        Me.Controls.Add(Me.cboTipoVenta)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmRepSaldosVencidos"
        Me.Text = "frmRepSaldosVencidos"
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.cboTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.lkpClienteInicio, 0)
        Me.Controls.SetChildIndex(Me.lblCliente1, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.chkSinAbono, 0)
        Me.Controls.SetChildIndex(Me.lblPlanCredito, 0)
        Me.Controls.SetChildIndex(Me.lkpPlanCredito, 0)
        Me.Controls.SetChildIndex(Me.clcFaltenLiquidar, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.chkParaCliente, 0)
        Me.Controls.SetChildIndex(Me.chkImprimirDireccion, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioCambiado, 0)
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSinAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMesesSinAbonar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFaltenLiquidar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.chkParaCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkImprimirDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    'Private oClientes As VillarrealBusiness.clsClientes
    Private oClienteInicio As VillarrealBusiness.clsClientes
    Private oClienteFin As VillarrealBusiness.clsClientes
    Private oCobrador As VillarrealBusiness.clsCobradores
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oPlanesCredito As VillarrealBusiness.clsPlanesCredito
    Private oConvenios As VillarrealBusiness.clsConvenios


    Private ReadOnly Property Sucursal() As String
        Get
            If Me.lkpSucursal.EditValue = Nothing Then
                Return -1
            Else
                Return Me.lkpSucursal.EditValue
            End If
        End Get

    End Property

    Private ReadOnly Property ClienteInicio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteInicio)

        End Get
    End Property
    'Private ReadOnly Property ClienteFin() As Long
    '    Get
    '        Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteFin)
    '    End Get
    'End Property
    Private ReadOnly Property Cobrador() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCobrador)
        End Get
    End Property

    Private ReadOnly Property PlanCredito() As String
        Get
            If Me.lkpPlanCredito.EditValue = Nothing Then
                Return -1
            Else
                Return Me.lkpPlanCredito.EditValue
            End If
        End Get

    End Property
    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property




#End Region

#Region "DIPROS Systems, Eventos de la Forma"


    Private Sub frmRepSaldosVencidos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If Cobrador = -1 Then

            Response = oReportes.SaldosVencidos(CType(Me.Sucursal, Long), ClienteInicio, ClienteInicio, Me.dteFecha.EditValue, Me.cboTipoVenta.EditValue, CType(Me.PlanCredito, Long), Me.Cobrador, Me.chkSinAbono.EditValue, Me.clcFaltenLiquidar.EditValue, Me.clcMesesSinAbonar.EditValue, Me.chkParaCliente.Checked, Me.chkImprimirDireccion.Checked, Convenio)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte de Saldos Vencidos no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptSaldosVencidosAgrupado
                    oReport.Imprimir_Direccion = Me.chkImprimirDireccion.Checked
                    oReport.DataSource = oDataSet.Tables(0)
                    TinApp.ShowReport(Me.MdiParent, "Saldos Vencidos", oReport, , , , True)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing

            End If

        Else
            Response = oReportes.SaldosVencidos(CType(Me.Sucursal, Long), Me.ClienteInicio, Me.ClienteInicio, Me.dteFecha.EditValue, Me.cboTipoVenta.EditValue, CType(Me.PlanCredito, Long), Me.Cobrador, Me.chkSinAbono.EditValue, Me.clcFaltenLiquidar.EditValue, Me.clcMesesSinAbonar.EditValue, Me.chkParaCliente.Checked, Me.chkImprimirDireccion.Checked, Convenio)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte de Saldos Vencidos no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptSaldosVencidos
                    oReport.Imprimir_Direccion = Me.chkImprimirDireccion.Checked
                    oReport.DataSource = oDataSet.Tables(0)
                    TinApp.ShowReport(Me.MdiParent, "Saldos Vencidos", oReport, , , , True)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing

            End If
        End If
    End Sub

    Private Sub frmfrmRepSaldosVencidos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(CType(Me.Sucursal, Long), 1, 1, Me.PlanCredito, Me.clcFaltenLiquidar.EditValue, Me.clcMesesSinAbonar.EditValue)
    End Sub

    Private Sub frmRepEstadoDeCuenta_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oClienteInicio = New VillarrealBusiness.clsClientes
        oClienteFin = New VillarrealBusiness.clsClientes
        oSucursales = New VillarrealBusiness.clsSucursales
        oCobrador = New VillarrealBusiness.clsCobradores
        oPlanesCredito = New VillarrealBusiness.clsPlanesCredito
        oConvenios = New VillarrealBusiness.clsConvenios


        Me.cboTipoVenta.SelectedIndex = 0 'TODOS
        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpClienteInicio_LoadData(ByVal Initialize As Boolean) Handles lkpClienteInicio.LoadData
        Dim Response As New Events
        Response = oClienteInicio.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteInicio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpClienteInicio_Format() Handles lkpClienteInicio.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteInicio)
    End Sub
    Private Sub lkpClienteInicio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpClienteInicio.EditValueChanged
        'Dim Response As New Events
        'Response = oClienteInicio.LookupLlenado(False, lkpClienteInicio.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpClienteInicio.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub

    'Private Sub lkpClienteFin_LoaData(ByVal Initialize As Boolean)
    '    Dim Response As New Events
    '    Response = oClienteFin.LookupCliente()
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value
    '        Me.lkpClienteFin.DataSource = oDataSet.Tables(0)
    '        oDataSet = Nothing
    '    End If
    '    Response = Nothing
    'End Sub
    'Private Sub lkpClienteFin_Format()
    '    Comunes.clsFormato.for_clientes_grl(Me.lkpClienteFin)
    'End Sub
   

    Private Sub lkpCobrador_LoaData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData
        Dim Response As New Events
        Response = oCobrador.Lookup(Me.Sucursal)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
            Me.lkpCobrador.SelectAll = True

        End If
        Response = Nothing
    End Sub
    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub

    Private Sub lkpCobrador_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCobrador.EditValueChanged

    End Sub
    Private Sub chkSinAbono_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSinAbono.CheckedChanged

        Me.clcMesesSinAbonar.Enabled = Not Me.chkSinAbono.Checked
        Me.clcMesesSinAbonar.EditValue = 0


        clcFaltenLiquidar.Enabled = Not Me.chkSinAbono.Checked
        Me.clcFaltenLiquidar.Value = 0

        'IIf(Me.chkSinAbono.Checked, Me.clcFaltenLiquidar.Value = 0, Me.clcFaltenLiquidar.Value = 0)


    End Sub


    Private Sub lkpPlanCredito_Format() Handles lkpPlanCredito.Format
        Comunes.clsFormato.for_planes_credito_grl(Me.lkpPlanCredito)
    End Sub

    Private Sub lkpPlanCredito_LoadData(ByVal Initialize As Boolean) Handles lkpPlanCredito.LoadData
        Dim Response As New Events
        Response = oPlanesCredito.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlanCredito.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub


#End Region

End Class
