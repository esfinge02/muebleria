Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmRepOrdenesRecuperacion
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepOrdenesRecuperacion))
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(219, 28)
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(28, 64)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(44, 16)
        Me.lblStatus.TabIndex = 59
        Me.lblStatus.Tag = ""
        Me.lblStatus.Text = "&Status:"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboStatus
        '
        Me.cboStatus.EditValue = "P"
        Me.cboStatus.Location = New System.Drawing.Point(80, 64)
        Me.cboStatus.Name = "cboStatus"
        '
        'cboStatus.Properties
        '
        Me.cboStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboStatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelado", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("En Proceso", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Recuperado", "R", -1)})
        Me.cboStatus.Size = New System.Drawing.Size(96, 20)
        Me.cboStatus.TabIndex = 60
        Me.cboStatus.Tag = "status"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 67
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 36)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todas)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(296, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 68
        Me.lkpSucursal.Tag = "sucursal_factura"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'frmRepOrdenesRecuperacion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(386, 104)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.cboStatus)
        Me.Name = "frmRepOrdenesRecuperacion"
        Me.Text = "frmOrdenesRecuperacion"
        Me.Controls.SetChildIndex(Me.cboStatus, 0)
        Me.Controls.SetChildIndex(Me.lblStatus, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        CType(Me.cboStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oReportes As VillarrealBusiness.Reportes

    Private ReadOnly Property Sucursal() As Long
        Get
            Return PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property


    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


    Private Sub frmRepOrdenesRecuperacion_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oReportes = New VillarrealBusiness.Reportes
    End Sub

    Private Sub frmRepOrdenesRecuperacion_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.OrdenesRecuperacion(Sucursal, Me.cboStatus.Value)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Ordenes de Recuperación no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then

                Dim oReport As New rptOrdenesRecuperacion

                oReport.DataSource = oDataSet.Tables(0)
                TinApp.ShowReport(Me.MdiParent, "Ordenes de Recuperación", oReport)

                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
            End If

            oDataSet = Nothing
        End If
    End Sub
End Class
