Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCambiosPrecios
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpPrecios As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambiosPrecios))
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpPrecios = New Dipros.Editors.TINMultiLookup
        Me.lkpProveedor = New Dipros.Editors.TINMultiLookup
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(9, 136)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(368, 56)
        Me.gpbFechas.TabIndex = 8
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 23, 58, 58, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(240, 24)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Fin.TabIndex = 3
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2007, 1, 1, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(80, 24)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(88, 23)
        Me.dteFecha_Ini.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(34, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "De&sde: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(199, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Has&ta: "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 16)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "&Departamento:"
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(100, 56)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = "(Todos)"
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = True
        Me.lkpDepartamento.Size = New System.Drawing.Size(280, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 3
        Me.lkpDepartamento.ToolTip = "Departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(280, 192)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "Des&glosado"
        Me.chkDesglosado.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(94, 22)
        Me.chkDesglosado.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(51, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Precios"
        '
        'lkpPrecios
        '
        Me.lkpPrecios.AllowAdd = False
        Me.lkpPrecios.AutoReaload = False
        Me.lkpPrecios.DataSource = Nothing
        Me.lkpPrecios.DefaultSearchField = ""
        Me.lkpPrecios.DisplayMember = "nombre"
        Me.lkpPrecios.EditValue = Nothing
        Me.lkpPrecios.Filtered = False
        Me.lkpPrecios.InitValue = Nothing
        Me.lkpPrecios.Location = New System.Drawing.Point(100, 104)
        Me.lkpPrecios.MultiSelect = False
        Me.lkpPrecios.Name = "lkpPrecios"
        Me.lkpPrecios.NullText = "(Todos)"
        Me.lkpPrecios.PopupWidth = CType(400, Long)
        Me.lkpPrecios.ReadOnlyControl = False
        Me.lkpPrecios.Required = False
        Me.lkpPrecios.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPrecios.SearchMember = ""
        Me.lkpPrecios.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPrecios.SelectAll = True
        Me.lkpPrecios.Size = New System.Drawing.Size(280, 20)
        Me.lkpPrecios.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPrecios.TabIndex = 7
        Me.lkpPrecios.ToolTip = "Precios"
        Me.lkpPrecios.ValueMember = "precio"
        '
        'lkpProveedor
        '
        Me.lkpProveedor.AllowAdd = False
        Me.lkpProveedor.AutoReaload = False
        Me.lkpProveedor.DataSource = Nothing
        Me.lkpProveedor.DefaultSearchField = ""
        Me.lkpProveedor.DisplayMember = "nombre"
        Me.lkpProveedor.EditValue = Nothing
        Me.lkpProveedor.Filtered = False
        Me.lkpProveedor.InitValue = Nothing
        Me.lkpProveedor.Location = New System.Drawing.Point(100, 80)
        Me.lkpProveedor.MultiSelect = False
        Me.lkpProveedor.Name = "lkpProveedor"
        Me.lkpProveedor.NullText = "(Todos)"
        Me.lkpProveedor.PopupWidth = CType(420, Long)
        Me.lkpProveedor.ReadOnlyControl = False
        Me.lkpProveedor.Required = False
        Me.lkpProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpProveedor.SearchMember = ""
        Me.lkpProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpProveedor.SelectAll = True
        Me.lkpProveedor.Size = New System.Drawing.Size(280, 20)
        Me.lkpProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpProveedor.TabIndex = 5
        Me.lkpProveedor.ToolTip = Nothing
        Me.lkpProveedor.ValueMember = "proveedor"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(29, 80)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Pr&oveedor:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(40, 32)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(100, 32)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = True
        Me.lkpSucursal.Size = New System.Drawing.Size(280, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'frmCambiosPrecios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(386, 220)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lkpProveedor)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpPrecios)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.gpbFechas)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCambiosPrecios"
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.Controls.SetChildIndex(Me.lkpPrecios, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.lkpProveedor, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oPrecios As VillarrealBusiness.clsPrecios
    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
    Private oProveedores As VillarrealBusiness.clsProveedores
    Private oSucursales As VillarrealBusiness.clsSucursales



    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Proveedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(lkpProveedor)
        End Get
    End Property


    Private ReadOnly Property Precio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpPrecios)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmEntradasAlmacen_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.CambiosPrecios(Sucursal, Departamento, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue, Me.chkDesglosado.Checked, Precio, Proveedor)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Cambios de Precios no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptCambiosPrecios
                oDataSet = Response.Value

                oReport.DataSource = oDataSet.Tables(0)
                oReport.lblTipoPrecio.Visible = Me.chkDesglosado.Checked
                oReport.txtTipoPrecio.Visible = Me.chkDesglosado.Checked
                oReport.lblAnterior.Visible = Me.chkDesglosado.Checked
                oReport.tipo_precio1.Visible = Me.chkDesglosado.Checked
                oReport.lblNuevo.Visible = Me.chkDesglosado.Checked
                oReport.tipo_precio2.Visible = Me.chkDesglosado.Checked


                If Departamento > 0 Then
                    oReport.lblTitulo.Text = oReport.lblTitulo.Text & "       Departamento: " & Me.lkpDepartamento.DisplayText
                End If



                TINApp.ShowReport(Me.MdiParent, "Cambios de Precios", oReport, , , , True)

                oDataSet = Nothing
                oReport = Nothing
            Else

            ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
            End If
        End If

    End Sub

    Private Sub frmEntradasAlmacen_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oPrecios = New VillarrealBusiness.clsPrecios
        oProveedores = New VillarrealBusiness.clsProveedores
        oSucursales = New VillarrealBusiness.clsSucursales



        Me.dteFecha_Ini.EditValue = Format(CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2)), "dd/MMM/yyyy")
        Me.dteFecha_Fin.EditValue = Format(CDate(TINApp.FechaServidor), "dd/MMM/yyyy")
        Me.dteFecha_Fin.EditValue = Now

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#Region "Proveedores"
    Private Sub lkpProveedor_Format() Handles lkpProveedor.Format
        Comunes.clsFormato.for_proveedores_grl(Me.lkpProveedor)
    End Sub

    Private Sub lkpProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpProveedor.LoadData
        Dim Response As New Events
        Response = oProveedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpPrecios_LoadData(ByVal Initialize As Boolean) Handles lkpPrecios.LoadData
        Dim Response As New Events
        Response = oPrecios.Lookup

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPrecios.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpPrecios_Format() Handles lkpPrecios.Format
        Comunes.clsFormato.for_precios_grl(Me.lkpPrecios)
    End Sub

#End Region

    Private Sub lkpPrecios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lkpPrecios.Load

    End Sub
End Class
