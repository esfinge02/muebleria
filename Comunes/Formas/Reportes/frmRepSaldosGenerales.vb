Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmRepSaldosGenerales
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblTipoVenta As System.Windows.Forms.Label
    Friend WithEvents cboTipoVenta As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpClienteInicio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpClienteFin As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblClienteFin As System.Windows.Forms.Label
    Friend WithEvents lblCliente1 As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodegaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConvenioCambiado As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepSaldosGenerales))
        Me.lblTipoVenta = New System.Windows.Forms.Label
        Me.cboTipoVenta = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpClienteInicio = New Dipros.Editors.TINMultiLookup
        Me.lkpClienteFin = New Dipros.Editors.TINMultiLookup
        Me.lblClienteFin = New System.Windows.Forms.Label
        Me.lblCliente1 = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.lkpBodegaSalida = New Dipros.Editors.TINMultiLookup
        Me.lblConvenioCambiado = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1215, 28)
        '
        'lblTipoVenta
        '
        Me.lblTipoVenta.AutoSize = True
        Me.lblTipoVenta.Location = New System.Drawing.Point(34, 88)
        Me.lblTipoVenta.Name = "lblTipoVenta"
        Me.lblTipoVenta.Size = New System.Drawing.Size(68, 16)
        Me.lblTipoVenta.TabIndex = 6
        Me.lblTipoVenta.Tag = ""
        Me.lblTipoVenta.Text = "&Tipo Venta:"
        Me.lblTipoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoVenta
        '
        Me.cboTipoVenta.EditValue = ""
        Me.cboTipoVenta.Location = New System.Drawing.Point(112, 88)
        Me.cboTipoVenta.Name = "cboTipoVenta"
        '
        'cboTipoVenta.Properties
        '
        Me.cboTipoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoVenta.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", "T", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Normales", "N", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enajenaci�n", "E", -1)})
        Me.cboTipoVenta.Size = New System.Drawing.Size(136, 23)
        Me.cboTipoVenta.TabIndex = 7
        Me.cboTipoVenta.Tag = "tipo"
        Me.cboTipoVenta.ToolTip = "Tipo de Venta"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(42, 112)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 10
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(112, 163)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "&Desglosado"
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(128, 19)
        Me.chkDesglosado.TabIndex = 14
        Me.chkDesglosado.Tag = ""
        Me.chkDesglosado.ToolTip = "Saldos Generales Desglosados"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lblSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSucursal.ForeColor = System.Drawing.Color.Black
        Me.lblSucursal.Location = New System.Drawing.Point(46, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(112, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(368, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = "Sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lkpClienteInicio
        '
        Me.lkpClienteInicio.AllowAdd = False
        Me.lkpClienteInicio.AutoReaload = False
        Me.lkpClienteInicio.BackColor = System.Drawing.SystemColors.Window
        Me.lkpClienteInicio.DataSource = Nothing
        Me.lkpClienteInicio.DefaultSearchField = ""
        Me.lkpClienteInicio.DisplayMember = "cliente"
        Me.lkpClienteInicio.EditValue = Nothing
        Me.lkpClienteInicio.Filtered = False
        Me.lkpClienteInicio.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpClienteInicio.ForeColor = System.Drawing.Color.Black
        Me.lkpClienteInicio.InitValue = Nothing
        Me.lkpClienteInicio.Location = New System.Drawing.Point(112, 64)
        Me.lkpClienteInicio.MultiSelect = False
        Me.lkpClienteInicio.Name = "lkpClienteInicio"
        Me.lkpClienteInicio.NullText = ""
        Me.lkpClienteInicio.PopupWidth = CType(300, Long)
        Me.lkpClienteInicio.ReadOnlyControl = False
        Me.lkpClienteInicio.Required = False
        Me.lkpClienteInicio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteInicio.SearchMember = ""
        Me.lkpClienteInicio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteInicio.SelectAll = False
        Me.lkpClienteInicio.Size = New System.Drawing.Size(136, 20)
        Me.lkpClienteInicio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteInicio.TabIndex = 3
        Me.lkpClienteInicio.Tag = "cliente"
        Me.lkpClienteInicio.ToolTip = "Cliente Desde"
        Me.lkpClienteInicio.ValueMember = "cliente"
        '
        'lkpClienteFin
        '
        Me.lkpClienteFin.AllowAdd = False
        Me.lkpClienteFin.AutoReaload = False
        Me.lkpClienteFin.BackColor = System.Drawing.SystemColors.Window
        Me.lkpClienteFin.DataSource = Nothing
        Me.lkpClienteFin.DefaultSearchField = ""
        Me.lkpClienteFin.DisplayMember = "cliente"
        Me.lkpClienteFin.EditValue = Nothing
        Me.lkpClienteFin.Filtered = False
        Me.lkpClienteFin.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpClienteFin.ForeColor = System.Drawing.Color.Black
        Me.lkpClienteFin.InitValue = Nothing
        Me.lkpClienteFin.Location = New System.Drawing.Point(344, 64)
        Me.lkpClienteFin.MultiSelect = False
        Me.lkpClienteFin.Name = "lkpClienteFin"
        Me.lkpClienteFin.NullText = ""
        Me.lkpClienteFin.PopupWidth = CType(300, Long)
        Me.lkpClienteFin.ReadOnlyControl = False
        Me.lkpClienteFin.Required = False
        Me.lkpClienteFin.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteFin.SearchMember = ""
        Me.lkpClienteFin.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteFin.SelectAll = False
        Me.lkpClienteFin.Size = New System.Drawing.Size(136, 20)
        Me.lkpClienteFin.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteFin.TabIndex = 5
        Me.lkpClienteFin.Tag = ""
        Me.lkpClienteFin.ToolTip = "Cliente Hasta"
        Me.lkpClienteFin.ValueMember = "cliente"
        '
        'lblClienteFin
        '
        Me.lblClienteFin.AutoSize = True
        Me.lblClienteFin.BackColor = System.Drawing.SystemColors.Window
        Me.lblClienteFin.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClienteFin.ForeColor = System.Drawing.Color.Black
        Me.lblClienteFin.Location = New System.Drawing.Point(258, 64)
        Me.lblClienteFin.Name = "lblClienteFin"
        Me.lblClienteFin.Size = New System.Drawing.Size(82, 16)
        Me.lblClienteFin.TabIndex = 4
        Me.lblClienteFin.Tag = ""
        Me.lblClienteFin.Text = "Cliente &Hasta:"
        Me.lblClienteFin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCliente1
        '
        Me.lblCliente1.AutoSize = True
        Me.lblCliente1.BackColor = System.Drawing.SystemColors.Window
        Me.lblCliente1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente1.ForeColor = System.Drawing.Color.Black
        Me.lblCliente1.Location = New System.Drawing.Point(17, 64)
        Me.lblCliente1.Name = "lblCliente1"
        Me.lblCliente1.Size = New System.Drawing.Size(85, 16)
        Me.lblCliente1.TabIndex = 2
        Me.lblCliente1.Tag = ""
        Me.lblCliente1.Text = "Cliente &Desde:"
        Me.lblCliente1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(297, 88)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "&Fecha:"
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 8, 2, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(344, 88)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(136, 23)
        Me.dteFecha.TabIndex = 9
        Me.dteFecha.ToolTip = "Fecha"
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = False
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(112, 112)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = "(Todos)"
        Me.lkpCobrador.PopupWidth = CType(300, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = True
        Me.lkpCobrador.Size = New System.Drawing.Size(368, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 11
        Me.lkpCobrador.Tag = "cobrador"
        Me.lkpCobrador.ToolTip = "Cobrador"
        Me.lkpCobrador.ValueMember = "cobrador"
        '
        'lkpBodegaSalida
        '
        Me.lkpBodegaSalida.AllowAdd = False
        Me.lkpBodegaSalida.AutoReaload = False
        Me.lkpBodegaSalida.DataSource = Nothing
        Me.lkpBodegaSalida.DefaultSearchField = ""
        Me.lkpBodegaSalida.DisplayMember = ""
        Me.lkpBodegaSalida.EditValue = Nothing
        Me.lkpBodegaSalida.Filtered = False
        Me.lkpBodegaSalida.InitValue = Nothing
        Me.lkpBodegaSalida.Location = New System.Drawing.Point(113, 88)
        Me.lkpBodegaSalida.MultiSelect = True
        Me.lkpBodegaSalida.Name = "lkpBodegaSalida"
        Me.lkpBodegaSalida.NullText = "(Todos)"
        Me.lkpBodegaSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegaSalida.ReadOnlyControl = False
        Me.lkpBodegaSalida.Required = False
        Me.lkpBodegaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaSalida.SearchMember = ""
        Me.lkpBodegaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaSalida.SelectAll = False
        Me.lkpBodegaSalida.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodegaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaSalida.TabIndex = 0
        Me.lkpBodegaSalida.ToolTip = Nothing
        Me.lkpBodegaSalida.ValueMember = ""
        '
        'lblConvenioCambiado
        '
        Me.lblConvenioCambiado.AutoSize = True
        Me.lblConvenioCambiado.Location = New System.Drawing.Point(42, 136)
        Me.lblConvenioCambiado.Name = "lblConvenioCambiado"
        Me.lblConvenioCambiado.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenioCambiado.TabIndex = 12
        Me.lblConvenioCambiado.Tag = ""
        Me.lblConvenioCambiado.Text = "Convenio:"
        Me.lblConvenioCambiado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(112, 136)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = "(Todos)"
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(368, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 13
        Me.lkpConvenio.Tag = ""
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'frmRepSaldosGenerales
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(498, 188)
        Me.Controls.Add(Me.lblConvenioCambiado)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblClienteFin)
        Me.Controls.Add(Me.lkpClienteFin)
        Me.Controls.Add(Me.lblCliente1)
        Me.Controls.Add(Me.lkpClienteInicio)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.lkpCobrador)
        Me.Controls.Add(Me.lblTipoVenta)
        Me.Controls.Add(Me.cboTipoVenta)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Name = "frmRepSaldosGenerales"
        Me.Text = "frmRepSaldosGenerales"
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.cboTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.Controls.SetChildIndex(Me.lkpClienteInicio, 0)
        Me.Controls.SetChildIndex(Me.lblCliente1, 0)
        Me.Controls.SetChildIndex(Me.lkpClienteFin, 0)
        Me.Controls.SetChildIndex(Me.lblClienteFin, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioCambiado, 0)
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes

    Private oClienteInicio As VillarrealBusiness.clsClientes
    Private oClienteFin As VillarrealBusiness.clsClientes
    Private oCobrador As VillarrealBusiness.clsCobradores
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oConvenios As VillarrealBusiness.clsConvenios


    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)

        End Get

    End Property
    Private ReadOnly Property ClienteInicio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteInicio)

        End Get
    End Property
    Private ReadOnly Property ClienteFin() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteFin)
        End Get
    End Property
    Private ReadOnly Property Cobrador() As String
        Get
            If Me.lkpCobrador.EditValue = Nothing Then
                Return -1
            Else
                Return Me.lkpCobrador.EditValue
            End If
        End Get
    End Property
    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepSaldosGenerales_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If Me.chkDesglosado.Checked = True Then 'Reporte para desglose
            Response = oReportes.SaldosGeneralesDesglosados(CLng(Me.Sucursal), ClienteInicio, ClienteFin, Me.dteFecha.EditValue, Me.cboTipoVenta.EditValue, Me.chkDesglosado.Checked, Me.Cobrador, Me.Convenio)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte de Saldos Generales Desglosado Agrupado no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptSaldosGeneralesDesglosado
                    oReport.DataSource = oDataSet.Tables(0)
                    TinApp.ShowReport(Me.MdiParent, "Saldos Generales Desglosado", oReport)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If
        Else

            'Reporte sin desglose
            If Me.lkpCobrador.EditValue <> Nothing Then    'Si hay un cobrador seleccionado
                Response = oReportes.SaldosGeneralesAgrupadoCobradores(Me.Sucursal, ClienteInicio, ClienteFin, Me.dteFecha.EditValue, Me.cboTipoVenta.EditValue, Me.chkDesglosado.Checked, Me.Cobrador, Me.Convenio)

                If Response.ErrorFound Then
                    ShowMessage(MessageType.MsgInformation, "El Reporte de Saldos Generales Agrupado no se puede Mostrar")
                Else
                    Dim oDataSet As DataSet
                    oDataSet = Response.Value

                    If oDataSet.Tables(0).Rows.Count > 0 Then
                        Dim oReport As New rptSaldosGeneralesAgrupadoCobradores
                        oReport.DataSource = oDataSet.Tables(0)
                        TinApp.ShowReport(Me.MdiParent, "Saldos Generales Agrupado", oReport)
                        oReport = Nothing
                    Else
                        ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                    End If

                    oDataSet = Nothing
                End If

            Else
                'Que no tenga ni desglose ni cobrador seleccionado.
                Response = oReportes.SaldosGenerales(Me.Sucursal, ClienteInicio, ClienteFin, Me.dteFecha.EditValue, Me.cboTipoVenta.EditValue, Me.chkDesglosado.Checked, Me.Cobrador, Me.Convenio)
                If Response.ErrorFound Then
                    ShowMessage(MessageType.MsgInformation, "El Reporte de Saldos Generales no se puede Mostrar")
                Else
                    Dim oDataSet As DataSet
                    oDataSet = Response.Value

                    If oDataSet.Tables(0).Rows.Count > 0 Then
                        Dim oReport As New rptSaldosGenerales
                        oReport.DataSource = oDataSet.Tables(0)
                        TinApp.ShowReport(Me.MdiParent, "Saldos Generales", oReport)
                        oReport = Nothing
                    Else
                        ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                    End If

                    oDataSet = Nothing
                End If




            End If


        End If





    End Sub
    Private Sub frmfrmRepSaldosGenerales_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oReportes.Validacion(CLng(Sucursal), ClienteInicio, ClienteFin)
    End Sub
    Private Sub frmRepSaldosGenerales_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oClienteInicio = New VillarrealBusiness.clsClientes
        oClienteFin = New VillarrealBusiness.clsClientes
        oSucursales = New VillarrealBusiness.clsSucursales
        oCobrador = New VillarrealBusiness.clsCobradores
        oConvenios = New VillarrealBusiness.clsConvenios

        Me.cboTipoVenta.SelectedIndex = 0 'TODOS
        Me.dteFecha.EditValue = Now()

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"



    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpClienteInicio_LoadData(ByVal Initialize As Boolean) Handles lkpClienteInicio.LoadData
        Dim Response As New Events
        Response = oClienteInicio.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteInicio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpClienteInicio_Format() Handles lkpClienteInicio.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteInicio)
    End Sub
    Private Sub lkpClienteInicio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpClienteInicio.EditValueChanged
        'Dim Response As New Events
        'Response = oClienteInicio.LookupLlenado(False, lkpClienteInicio.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpClienteInicio.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub

    Private Sub lkpClienteFin_LoaData(ByVal Initialize As Boolean) Handles lkpClienteFin.LoadData
        Dim Response As New Events
        Response = oClienteFin.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteFin.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpClienteFin_Format() Handles lkpClienteFin.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteFin)
    End Sub
    Private Sub lkpClienteFin_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpClienteFin.EditValueChanged
        'Dim Response As New Events
        'Response = oClienteFin.LookupLlenado(False, lkpClienteFin.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpClienteFin.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub

    Private Sub lkpCobrador_LoaData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData
        Dim Response As New Events
        Response = oCobrador.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub

    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub



#End Region

#Region "DIPROS Systems, Funcionalidad"

    'Public Sub ActualizaGrid()
    '    Dim Response As Dipros.Utils.Events

    '    Response = oTraspasos.ListadoTraspasosDetalles(Me.lkpBodegaSalida.ToXML, BodegaEntrada)
    '    If Not Response.ErrorFound Then
    '        Dim oDataSet As DataSet
    '        oDataSet = Response.Value

    '        oDataSet.Relations.Add( _
    '         "TRASPASOS", _
    '         New DataColumn() {oDataSet.Tables(0).Columns("sucursal_salida"), _
    '                           oDataSet.Tables(0).Columns("bodega_salida"), _
    '                           oDataSet.Tables(0).Columns("concepto_salida"), _
    '                           oDataSet.Tables(0).Columns("folio_salida")}, _
    '         New DataColumn() {oDataSet.Tables(1).Columns("sucursal"), _
    '                           oDataSet.Tables(1).Columns("bodega"), _
    '                           oDataSet.Tables(1).Columns("concepto"), _
    '                           oDataSet.Tables(1).Columns("folio")}, False)

    '        grTraspasos.LevelDefaults.Add("TRASPASOS", grvArticulos)
    '        grvTraspasos.OptionsDetail.EnableMasterViewMode = True
    '        grvTraspasos.OptionsView.ShowChildrenInGroupPanel = True
    '        grTraspasos.DataSource = oDataSet.Tables(0)
    '    End If
    'End Sub

#End Region




    
    
End Class
