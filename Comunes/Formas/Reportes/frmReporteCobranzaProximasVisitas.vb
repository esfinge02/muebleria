Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmReporteCobranzaProximasVisitas
    Inherits Dipros.Windows.frmTINForm


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblTipoVenta As System.Windows.Forms.Label
    Friend WithEvents cboTipoVenta As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReporteCobranzaProximasVisitas))
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.lblTipoVenta = New System.Windows.Forms.Label
        Me.cboTipoVenta = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(317, 28)
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(20, 72)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 61
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = False
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(88, 72)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = "(Todos)"
        Me.lkpCobrador.PopupWidth = CType(400, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = False
        Me.lkpCobrador.Size = New System.Drawing.Size(288, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 62
        Me.lkpCobrador.Tag = "Cobrador"
        Me.lkpCobrador.ToolTip = Nothing
        Me.lkpCobrador.ValueMember = "Cobrador"
        '
        'lblTipoVenta
        '
        Me.lblTipoVenta.AutoSize = True
        Me.lblTipoVenta.Location = New System.Drawing.Point(12, 96)
        Me.lblTipoVenta.Name = "lblTipoVenta"
        Me.lblTipoVenta.Size = New System.Drawing.Size(68, 16)
        Me.lblTipoVenta.TabIndex = 63
        Me.lblTipoVenta.Tag = ""
        Me.lblTipoVenta.Text = "&Tipo Venta:"
        Me.lblTipoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoVenta
        '
        Me.cboTipoVenta.EditValue = 1
        Me.cboTipoVenta.Location = New System.Drawing.Point(88, 96)
        Me.cboTipoVenta.Name = "cboTipoVenta"
        '
        'cboTipoVenta.Properties
        '
        Me.cboTipoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoVenta.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enajenación", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Normales", 3, -1)})
        Me.cboTipoVenta.Size = New System.Drawing.Size(136, 20)
        Me.cboTipoVenta.TabIndex = 64
        Me.cboTipoVenta.Tag = "tipo"
        Me.cboTipoVenta.ToolTip = "Tipo de Venta"
        '
        'chkDesglosado
        '
        Me.chkDesglosado.EditValue = True
        Me.chkDesglosado.Location = New System.Drawing.Point(288, 96)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "&Desglosado"
        Me.chkDesglosado.Properties.Enabled = False
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(88, 19)
        Me.chkDesglosado.TabIndex = 66
        Me.chkDesglosado.TabStop = False
        Me.chkDesglosado.Tag = ""
        Me.chkDesglosado.ToolTip = "Sugerencias de Cobro Desglosadas"
        Me.chkDesglosado.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 120)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(75, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Fecha Corte:"
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(88, 120)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha_Fin.TabIndex = 3
        Me.dteFecha_Fin.ToolTip = "Fecha Corte"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(24, 51)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 59
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(88, 48)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todas)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = True
        Me.lkpSucursal.Size = New System.Drawing.Size(288, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 60
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'frmReporteCobranzaProximasVisitas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 144)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.lkpCobrador)
        Me.Controls.Add(Me.lblTipoVenta)
        Me.Controls.Add(Me.cboTipoVenta)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.dteFecha_Fin)
        Me.Controls.Add(Me.Label5)
        Me.Name = "frmReporteCobranzaProximasVisitas"
        Me.Text = "frmReporteCobranzaProximasVisitas"
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Fin, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.Controls.SetChildIndex(Me.cboTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private ocobradores As VillarrealBusiness.clsCobradores

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Cobrador() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCobrador)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmReporteCobranzaProximasVisitas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oSucursales = New VillarrealBusiness.clsSucursales
        ocobradores = New VillarrealBusiness.clsCobradores
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
    End Sub
    Private Sub frmReporteCobranzaProximasVisitas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If Me.chkDesglosado.EditValue = False Then 'SUGERENCIA DE COBRO
            'Response = oReportes.SugerenciaCobro(Sucursal, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue, Me.chkDesglosado.EditValue, cboTipoVenta.EditValue, Cobrador)

            'If Response.ErrorFound Then
            '    ShowMessage(MessageType.MsgInformation, "El Reporte de Sugerencia de Cobro no se puede Mostrar")
            'Else
            '    Dim oDataSet As DataSet
            '    oDataSet = Response.Value

            '    If oDataSet.Tables(0).Rows.Count > 0 Then

            '        Dim oReport As New rptSugerenciaCobro

            '        oReport.DataSource = oDataSet.Tables(0)
            '        TINApp.ShowReport(Me.MdiParent, "Sugerencia de Cobro", oReport)

            '        oReport = Nothing

            '    Else

            '        ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
            '    End If

            '    oDataSet = Nothing
            'End If
        Else 'SUGERENCIA DE COBRO DESGLOSADO
            Response = oReportes.SugerenciaCobroProximasVisitas(Sucursal, Me.dteFecha_Fin.EditValue, Me.chkDesglosado.EditValue, cboTipoVenta.EditValue, Cobrador)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte de Sugerencia de Cobro no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then

                    Dim oReport As New rptVisitasClientes
                    'oReport.Cobrador = Me.Cobrador

                    oReport.DataSource = oDataSet.Tables(0)
                    TinApp.ShowReport(Me.MdiParent, "Sugerencia de Cobro Desglosado", oReport, , , , True)

                    oReport = Nothing

                Else

                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
                End If

                oDataSet = Nothing
            End If
        End If

    End Sub
    Private Sub frmReporteCobranzaProximasVisitas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCobrador_LoadData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData

        Dim Response As New Events
        Response = ocobradores.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub

#End Region


   
End Class
