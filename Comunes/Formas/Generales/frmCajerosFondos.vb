Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCajerosFondos
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As System.Windows.Forms.Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código. 
    Friend WithEvents lblFondo As System.Windows.Forms.Label
    Friend WithEvents clcFondo As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTipo_Cambio As System.Windows.Forms.Label
    Friend WithEvents clcTipo_Cambio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTitulo As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCajerosFondos))
        Me.lblFondo = New System.Windows.Forms.Label
        Me.clcFondo = New Dipros.Editors.TINCalcEdit
        Me.lblTipo_Cambio = New System.Windows.Forms.Label
        Me.clcTipo_Cambio = New Dipros.Editors.TINCalcEdit
        Me.lblTitulo = New System.Windows.Forms.Label
        CType(Me.clcFondo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTipo_Cambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(117, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblFondo
        '
        Me.lblFondo.AutoSize = True
        Me.lblFondo.Location = New System.Drawing.Point(120, 56)
        Me.lblFondo.Name = "lblFondo"
        Me.lblFondo.Size = New System.Drawing.Size(42, 16)
        Me.lblFondo.TabIndex = 4
        Me.lblFondo.Tag = ""
        Me.lblFondo.Text = "&Fondo:"
        Me.lblFondo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFondo
        '
        Me.clcFondo.EditValue = "0"
        Me.clcFondo.Location = New System.Drawing.Point(168, 56)
        Me.clcFondo.MaxValue = 0
        Me.clcFondo.MinValue = 0
        Me.clcFondo.Name = "clcFondo"
        '
        'clcFondo.Properties
        '
        Me.clcFondo.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcFondo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFondo.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcFondo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFondo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcFondo.Properties.Precision = 2
        Me.clcFondo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFondo.Size = New System.Drawing.Size(96, 19)
        Me.clcFondo.TabIndex = 5
        Me.clcFondo.Tag = "fondo"
        '
        'lblTipo_Cambio
        '
        Me.lblTipo_Cambio.AutoSize = True
        Me.lblTipo_Cambio.Location = New System.Drawing.Point(72, 80)
        Me.lblTipo_Cambio.Name = "lblTipo_Cambio"
        Me.lblTipo_Cambio.Size = New System.Drawing.Size(95, 16)
        Me.lblTipo_Cambio.TabIndex = 6
        Me.lblTipo_Cambio.Tag = ""
        Me.lblTipo_Cambio.Text = "&Tipo de Cambio:"
        Me.lblTipo_Cambio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTipo_Cambio
        '
        Me.clcTipo_Cambio.EditValue = "0"
        Me.clcTipo_Cambio.Location = New System.Drawing.Point(168, 80)
        Me.clcTipo_Cambio.MaxValue = 0
        Me.clcTipo_Cambio.MinValue = 0
        Me.clcTipo_Cambio.Name = "clcTipo_Cambio"
        '
        'clcTipo_Cambio.Properties
        '
        Me.clcTipo_Cambio.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcTipo_Cambio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipo_Cambio.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcTipo_Cambio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipo_Cambio.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcTipo_Cambio.Properties.Precision = 2
        Me.clcTipo_Cambio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTipo_Cambio.Size = New System.Drawing.Size(96, 19)
        Me.clcTipo_Cambio.TabIndex = 7
        Me.clcTipo_Cambio.Tag = "tipo_cambio"
        '
        'lblTitulo
        '
        Me.lblTitulo.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.Location = New System.Drawing.Point(8, 32)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(360, 16)
        Me.lblTitulo.TabIndex = 65
        Me.lblTitulo.Text = "Introduzca el fondo y el tipo de cambio"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'frmCajerosFondos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 104)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.lblFondo)
        Me.Controls.Add(Me.clcFondo)
        Me.Controls.Add(Me.lblTipo_Cambio)
        Me.Controls.Add(Me.clcTipo_Cambio)
        Me.Name = "frmCajerosFondos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Fondo y Tipo de Cambio"
        Me.Title = "Fondo y Tipo de Cambio"
        Me.Controls.SetChildIndex(Me.clcTipo_Cambio, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Cambio, 0)
        Me.Controls.SetChildIndex(Me.clcFondo, 0)
        Me.Controls.SetChildIndex(Me.lblFondo, 0)
        Me.Controls.SetChildIndex(Me.lblTitulo, 0)
        CType(Me.clcFondo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTipo_Cambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCajerosFondos As VillarrealBusiness.clsCajerosFondos

    Private Entrar As Boolean = False
    Private Cajero As Long = 0

    Public ReadOnly Property EntrarMain() As Boolean
        Get
            Return Entrar
        End Get

    End Property

    Private ReadOnly Property TieneCajero() As Boolean
        Get
            TieneCajero = Comunes.clsUtilerias.uti_Usuariocajero(TINApp.Connection.User, Cajero)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCajerosFondos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If TieneCajero = True Then
            Response = oCajerosFondos.Insertar(Me.DataSource, Cajero, CDate(TINApp.FechaServidor).Day.ToString + "/" + CDate(TINApp.FechaServidor).Month.ToString + "/" + CDate(TINApp.FechaServidor).Year.ToString)
            Entrar = True
        Else
            ShowMessage(MessageType.MsgInformation, "El usuario no tiene asignado un cajero")
        End If

        'Case Actions.Update
        '    Response = oCajerosFondos.Actualizar(Me.DataSource)
        '    Entrar = True


    End Sub

    Private Sub frmCajerosFondos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCajerosFondos = New VillarrealBusiness.clsCajerosFondos

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmCajerosFondos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCajerosFondos.Validacion(Action, Me.clcTipo_Cambio.EditValue, Me.clcFondo.EditValue)
    End Sub

    Private Sub frmCajerosFondos_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
