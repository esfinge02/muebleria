Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms


Public Class frmSucursalActual
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSucursalActual))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblTitulo = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(337, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(12, 88)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 60
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(76, 88)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(284, 20)
        Me.lkpSucursal.TabIndex = 59
        Me.lkpSucursal.Tag = ""
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lblTitulo
        '
        Me.lblTitulo.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.Location = New System.Drawing.Point(8, 48)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(360, 16)
        Me.lblTitulo.TabIndex = 61
        Me.lblTitulo.Text = "Para iniciar el Sistema es necesario definir  una Sucursal"
        '
        'frmSucursalActual
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(378, 128)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSucursalActual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sucursal"
        Me.Title = "Sucursal"
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblTitulo, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private bConvenios As Long

    Public WriteOnly Property Convenios() As Boolean
        Set(ByVal Value As Boolean)
            Select Case Value
                Case False
                    bConvenios = 0
                Case True
                    bConvenios = 1
            End Select

        End Set
    End Property

    Private oSucursales As New VillarrealBusiness.clsSucursales

    Private Entrar As Boolean = False

    Public ReadOnly Property EntrarMain() As Boolean
        Get
            Return Entrar
        End Get

    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmSucursalActual_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales

    End Sub

    Private Sub frmSucursalActual_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If Me.lkpSucursal.EditValue <> Nothing Then
            Comunes.Common.Sucursal_Actual = Me.lkpSucursal.EditValue
            Comunes.clsUtilerias.GuardaArchivo(Me.lkpSucursal.EditValue)
            Entrar = True
        End If
    End Sub

    Private Sub frmSucursalActual_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

    End Sub


#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim response As Events
        response = oSucursales.Lookup(bConvenios)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing
    End Sub

#End Region



End Class
