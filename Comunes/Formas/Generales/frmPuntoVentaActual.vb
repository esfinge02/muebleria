Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmPuntoVentaActual
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents lblPuntoVenta As System.Windows.Forms.Label
    Friend WithEvents lkpPuntoVenta As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPuntoVentaActual))
        Me.lblTitulo = New System.Windows.Forms.Label
        Me.lblPuntoVenta = New System.Windows.Forms.Label
        Me.lkpPuntoVenta = New Dipros.Editors.TINMultiLookup
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(186, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblTitulo
        '
        Me.lblTitulo.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.Location = New System.Drawing.Point(8, 33)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(360, 16)
        Me.lblTitulo.TabIndex = 64
        Me.lblTitulo.Text = "Seleccione un Punto de Venta"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblPuntoVenta
        '
        Me.lblPuntoVenta.AutoSize = True
        Me.lblPuntoVenta.Location = New System.Drawing.Point(14, 73)
        Me.lblPuntoVenta.Name = "lblPuntoVenta"
        Me.lblPuntoVenta.Size = New System.Drawing.Size(94, 16)
        Me.lblPuntoVenta.TabIndex = 63
        Me.lblPuntoVenta.Tag = ""
        Me.lblPuntoVenta.Text = "&Punto de Venta:"
        Me.lblPuntoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPuntoVenta
        '
        Me.lkpPuntoVenta.AutoReaload = False
        Me.lkpPuntoVenta.DataSource = Nothing
        Me.lkpPuntoVenta.DefaultSearchField = ""
        Me.lkpPuntoVenta.DisplayMember = "descripcion"
        Me.lkpPuntoVenta.EditValue = Nothing
        Me.lkpPuntoVenta.InitValue = Nothing
        Me.lkpPuntoVenta.Location = New System.Drawing.Point(120, 72)
        Me.lkpPuntoVenta.MultiSelect = False
        Me.lkpPuntoVenta.Name = "lkpPuntoVenta"
        Me.lkpPuntoVenta.NullText = ""
        Me.lkpPuntoVenta.PopupWidth = CType(360, Long)
        Me.lkpPuntoVenta.Required = False
        Me.lkpPuntoVenta.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPuntoVenta.SearchMember = ""
        Me.lkpPuntoVenta.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPuntoVenta.SelectAll = False
        Me.lkpPuntoVenta.Size = New System.Drawing.Size(248, 20)
        Me.lkpPuntoVenta.TabIndex = 62
        Me.lkpPuntoVenta.Tag = ""
        Me.lkpPuntoVenta.ToolTip = Nothing
        Me.lkpPuntoVenta.ValueMember = "puntoventa"
        '
        'frmPuntoVentaActual
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(378, 128)
        Me.Controls.Add(Me.lblPuntoVenta)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.lkpPuntoVenta)
        Me.Name = "frmPuntoVentaActual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Punto de Venta"
        Me.Controls.SetChildIndex(Me.lkpPuntoVenta, 0)
        Me.Controls.SetChildIndex(Me.lblTitulo, 0)
        Me.Controls.SetChildIndex(Me.lblPuntoVenta, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oPuntosVenta As New VillarrealBusiness.clsPuntosVenta

    Private Entrar As Boolean = False

    Public ReadOnly Property EntrarMain() As Boolean
        Get
            Return Entrar
        End Get

    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub frmPuntoVentaActual_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If Me.lkpPuntoVenta.EditValue <> Nothing Then
            Comunes.Common.PuntoVenta_Actual = Me.lkpPuntoVenta.EditValue
            Comunes.clsUtilerias.GuardaArchivoPV(Me.lkpPuntoVenta.EditValue)
            Entrar = True
        End If
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub lkpPuntoVenta_Format() Handles lkpPuntoVenta.Format
        clsFormato.for_puntos_venta_grl(Me.lkpPuntoVenta)
    End Sub

    Private Sub lkpPuntoVenta_LoadData(ByVal Initialize As Boolean) Handles lkpPuntoVenta.LoadData
        Dim response As Events
        response = oPuntosVenta.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpPuntoVenta.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If
        response = Nothing
    End Sub
#End Region

End Class
