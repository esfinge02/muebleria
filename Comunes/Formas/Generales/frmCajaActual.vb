Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmCajaActual
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents lblPuntoVenta As System.Windows.Forms.Label
    Friend WithEvents lkpCaja As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCajaActual))
        Me.lblTitulo = New System.Windows.Forms.Label
        Me.lblPuntoVenta = New System.Windows.Forms.Label
        Me.lkpCaja = New Dipros.Editors.TINMultiLookup
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(195, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblTitulo
        '
        Me.lblTitulo.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.Location = New System.Drawing.Point(8, 40)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(360, 16)
        Me.lblTitulo.TabIndex = 65
        Me.lblTitulo.Text = "Seleccione una Caja"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblPuntoVenta
        '
        Me.lblPuntoVenta.AutoSize = True
        Me.lblPuntoVenta.Location = New System.Drawing.Point(32, 72)
        Me.lblPuntoVenta.Name = "lblPuntoVenta"
        Me.lblPuntoVenta.Size = New System.Drawing.Size(72, 16)
        Me.lblPuntoVenta.TabIndex = 0
        Me.lblPuntoVenta.Tag = ""
        Me.lblPuntoVenta.Text = "Caja &Actual:"
        Me.lblPuntoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCaja
        '
        Me.lkpCaja.AllowAdd = False
        Me.lkpCaja.AutoReaload = False
        Me.lkpCaja.DataSource = Nothing
        Me.lkpCaja.DefaultSearchField = ""
        Me.lkpCaja.DisplayMember = "descripcion"
        Me.lkpCaja.EditValue = Nothing
        Me.lkpCaja.Filtered = False
        Me.lkpCaja.InitValue = Nothing
        Me.lkpCaja.Location = New System.Drawing.Point(112, 72)
        Me.lkpCaja.MultiSelect = False
        Me.lkpCaja.Name = "lkpCaja"
        Me.lkpCaja.NullText = ""
        Me.lkpCaja.PopupWidth = CType(360, Long)
        Me.lkpCaja.Required = False
        Me.lkpCaja.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCaja.SearchMember = ""
        Me.lkpCaja.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCaja.SelectAll = False
        Me.lkpCaja.Size = New System.Drawing.Size(248, 20)
        Me.lkpCaja.TabIndex = 1
        Me.lkpCaja.Tag = ""
        Me.lkpCaja.ToolTip = Nothing
        Me.lkpCaja.ValueMember = "caja"
        '
        'frmCajaActual
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(378, 136)
        Me.Controls.Add(Me.lblPuntoVenta)
        Me.Controls.Add(Me.lkpCaja)
        Me.Controls.Add(Me.lblTitulo)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCajaActual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Caja"
        Me.Controls.SetChildIndex(Me.lblTitulo, 0)
        Me.Controls.SetChildIndex(Me.lkpCaja, 0)
        Me.Controls.SetChildIndex(Me.lblPuntoVenta, 0)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oCajas As New VillarrealBusiness.clsCajas

    Private Entrar As Boolean = False

    Public ReadOnly Property EntrarMain() As Boolean
        Get
            Return Entrar
        End Get

    End Property
#End Region

#Region "Eventos de la Forma"
    Private Sub frmCajaActual_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        If Me.lkpCaja.EditValue <> Nothing Then
            Comunes.Common.Caja_Actual = Me.lkpCaja.EditValue()
            Comunes.clsUtilerias.GuardaArchivoCaja(Me.lkpCaja.EditValue)
            Entrar = True
        End If
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub lkpCaja_Format() Handles lkpCaja.Format
        Comunes.clsFormato.for_cajas_grl(Me.lkpCaja)
    End Sub
    Private Sub lkpCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkpCaja.Load
        Dim Response As New Events
        Response = oCajas.LookUp
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCaja.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
#End Region

End Class
