Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmPreciosPlanes
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblPrecio As System.Windows.Forms.Label


		Friend WithEvents lblOrden As System.Windows.Forms.Label
		Friend WithEvents clcOrden As Dipros.Editors.TINCalcEdit 
    Friend WithEvents lkpPrecio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpPlanCredito As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblPrecioNombre As System.Windows.Forms.Label
    Friend WithEvents lblPlanCredito As System.Windows.Forms.Label
    Friend WithEvents lblPlan_Nombre As System.Windows.Forms.Label

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPreciosPlanes))
        Me.lblPrecio = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.clcOrden = New Dipros.Editors.TINCalcEdit
        Me.lkpPrecio = New Dipros.Editors.TINMultiLookup
        Me.lkpPlanCredito = New Dipros.Editors.TINMultiLookup
        Me.lblPrecioNombre = New System.Windows.Forms.Label
        Me.lblPlanCredito = New System.Windows.Forms.Label
        Me.lblPlan_Nombre = New System.Windows.Forms.Label
        CType(Me.clcOrden.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(60, 50)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lblPrecio
        '
        Me.lblPrecio.AutoSize = True
        Me.lblPrecio.Location = New System.Drawing.Point(61, 40)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(42, 16)
        Me.lblPrecio.TabIndex = 0
        Me.lblPrecio.Tag = ""
        Me.lblPrecio.Text = "&Precio:"
        Me.lblPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(60, 86)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(43, 16)
        Me.lblOrden.TabIndex = 4
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Orden:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcOrden
        '
        Me.clcOrden.EditValue = "0"
        Me.clcOrden.Location = New System.Drawing.Point(112, 86)
        Me.clcOrden.MaxValue = 0
        Me.clcOrden.MinValue = 0
        Me.clcOrden.Name = "clcOrden"
        '
        'clcOrden.Properties
        '
        Me.clcOrden.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcOrden.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOrden.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcOrden.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOrden.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcOrden.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcOrden.Size = New System.Drawing.Size(62, 19)
        Me.clcOrden.TabIndex = 5
        Me.clcOrden.Tag = "orden"
        '
        'lkpPrecio
        '
        Me.lkpPrecio.AutoReaload = False
        Me.lkpPrecio.DataSource = Nothing
        Me.lkpPrecio.DefaultSearchField = ""
        Me.lkpPrecio.DisplayMember = "nombre"
        Me.lkpPrecio.EditValue = Nothing
        Me.lkpPrecio.InitValue = Nothing
        Me.lkpPrecio.Location = New System.Drawing.Point(112, 38)
        Me.lkpPrecio.MultiSelect = False
        Me.lkpPrecio.Name = "lkpPrecio"
        Me.lkpPrecio.NullText = ""
        Me.lkpPrecio.PopupWidth = CType(400, Long)
        Me.lkpPrecio.Required = True
        Me.lkpPrecio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPrecio.SearchMember = ""
        Me.lkpPrecio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPrecio.SelectAll = False
        Me.lkpPrecio.Size = New System.Drawing.Size(191, 20)
        Me.lkpPrecio.TabIndex = 1
        Me.lkpPrecio.Tag = "precio"
        Me.lkpPrecio.ToolTip = Nothing
        Me.lkpPrecio.ValueMember = "precio"
        '
        'lkpPlanCredito
        '
        Me.lkpPlanCredito.AutoReaload = False
        Me.lkpPlanCredito.DataSource = Nothing
        Me.lkpPlanCredito.DefaultSearchField = ""
        Me.lkpPlanCredito.DisplayMember = "Descripcion"
        Me.lkpPlanCredito.EditValue = Nothing
        Me.lkpPlanCredito.InitValue = Nothing
        Me.lkpPlanCredito.Location = New System.Drawing.Point(112, 61)
        Me.lkpPlanCredito.MultiSelect = False
        Me.lkpPlanCredito.Name = "lkpPlanCredito"
        Me.lkpPlanCredito.NullText = ""
        Me.lkpPlanCredito.PopupWidth = CType(400, Long)
        Me.lkpPlanCredito.Required = True
        Me.lkpPlanCredito.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlanCredito.SearchMember = ""
        Me.lkpPlanCredito.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlanCredito.SelectAll = False
        Me.lkpPlanCredito.Size = New System.Drawing.Size(191, 20)
        Me.lkpPlanCredito.TabIndex = 3
        Me.lkpPlanCredito.Tag = "plan_credito"
        Me.lkpPlanCredito.ToolTip = Nothing
        Me.lkpPlanCredito.ValueMember = "plan_credito"
        '
        'lblPrecioNombre
        '
        Me.lblPrecioNombre.Location = New System.Drawing.Point(184, 120)
        Me.lblPrecioNombre.Name = "lblPrecioNombre"
        Me.lblPrecioNombre.Size = New System.Drawing.Size(112, 16)
        Me.lblPrecioNombre.TabIndex = 2
        Me.lblPrecioNombre.Tag = "precio_nombre"
        Me.lblPrecioNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPlanCredito
        '
        Me.lblPlanCredito.AutoSize = True
        Me.lblPlanCredito.Location = New System.Drawing.Point(16, 64)
        Me.lblPlanCredito.Name = "lblPlanCredito"
        Me.lblPlanCredito.Size = New System.Drawing.Size(93, 16)
        Me.lblPlanCredito.TabIndex = 2
        Me.lblPlanCredito.Tag = ""
        Me.lblPlanCredito.Text = "&Plan de Crédito:"
        Me.lblPlanCredito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPlan_Nombre
        '
        Me.lblPlan_Nombre.Location = New System.Drawing.Point(16, 120)
        Me.lblPlan_Nombre.Name = "lblPlan_Nombre"
        Me.lblPlan_Nombre.Size = New System.Drawing.Size(120, 16)
        Me.lblPlan_Nombre.TabIndex = 2
        Me.lblPlan_Nombre.Tag = "plan_nombre"
        Me.lblPlan_Nombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmPreciosPlanes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 112)
        Me.Controls.Add(Me.lkpPlanCredito)
        Me.Controls.Add(Me.lkpPrecio)
        Me.Controls.Add(Me.lblPrecio)
        Me.Controls.Add(Me.lblOrden)
        Me.Controls.Add(Me.clcOrden)
        Me.Controls.Add(Me.lblPrecioNombre)
        Me.Controls.Add(Me.lblPlanCredito)
        Me.Controls.Add(Me.lblPlan_Nombre)
        Me.Name = "frmPreciosPlanes"
        Me.Controls.SetChildIndex(Me.lblPlan_Nombre, 0)
        Me.Controls.SetChildIndex(Me.lblPlanCredito, 0)
        Me.Controls.SetChildIndex(Me.lblPrecioNombre, 0)
        Me.Controls.SetChildIndex(Me.clcOrden, 0)
        Me.Controls.SetChildIndex(Me.lblOrden, 0)
        Me.Controls.SetChildIndex(Me.lblPrecio, 0)
        Me.Controls.SetChildIndex(Me.lkpPrecio, 0)
        Me.Controls.SetChildIndex(Me.lkpPlanCredito, 0)
        CType(Me.clcOrden.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oPrecios As VillarrealBusiness.clsPrecios
    Private oPlanesCredito As VillarrealBusiness.clsPlanesCredito
    Private oPreciosPlanes As VillarrealBusiness.clsPreciosPlanes
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmPreciosPlanes_Accept(ByRef Response As Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub
    Private Sub frmPreciosPlanes_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

        Select Case Action
            Case Actions.Insert
            Case Actions.Update, Actions.Delete
                Me.lkpPlanCredito.Enabled = False
                Me.lkpPrecio.Enabled = False
        End Select

    End Sub
    Private Sub frmPreciosPlanes_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oPrecios = New VillarrealBusiness.clsPrecios
        oPlanesCredito = New VillarrealBusiness.clsPlanesCredito
        oPreciosPlanes = New VillarrealBusiness.clsPreciosPlanes

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmPreciosPlanes_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields

    End Sub

    'Private Sub frmPreciosPlanes_Localize() Handles MyBase.Localize
    '	Find("Unknow", CType("Replace by a control", Object))

    'End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpPlanCredito_Format() Handles lkpPlanCredito.Format
        Comunes.clsFormato.for_planes_credito_grl(Me.lkpPlanCredito)
    End Sub
    Private Sub lkpPlanCredito_LoadData(ByVal Initialize As Boolean) Handles lkpPlanCredito.LoadData
        Dim Response As New Events
        Response = oPlanesCredito.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlanCredito.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpPlanCredito_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpPlanCredito.EditValueChanged
        Me.lblPlan_Nombre.Text = Me.lkpPlanCredito.GetValue("descripcion")
    End Sub


    Private Sub lkpPrecio_Format() Handles lkpPrecio.Format
        Comunes.clsFormato.for_precios_grl(Me.lkpPrecio)
    End Sub
    Private Sub lkpPrecio_LoadData(ByVal Initialize As Boolean) Handles lkpPrecio.LoadData
        Dim Response As New Events
        Response = oPrecios.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPrecio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpPrecio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpPrecio.EditValueChanged
        Me.lblPrecioNombre.Text = Me.lkpPrecio.GetValue("nombre")
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
