Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class brwVentas
    Inherits Dipros.Windows.frmTINGridNet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblPuntoVenta As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblPuntoVenta = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FilterPanel.SuspendLayout()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrExtended
        '
        Me.tbrExtended.Name = "tbrExtended"
        Me.tbrExtended.Size = New System.Drawing.Size(940, 22)
        '
        'trbToolsData
        '
        Me.trbToolsData.Name = "trbToolsData"
        Me.trbToolsData.Size = New System.Drawing.Size(164, 28)
        '
        'FilterPanel
        '
        Me.FilterPanel.Controls.Add(Me.dteFecha)
        Me.FilterPanel.Controls.Add(Me.lblPuntoVenta)
        Me.FilterPanel.Controls.Add(Me.Label4)
        Me.FilterPanel.Controls.Add(Me.Label3)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(872, 61)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label3, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.Label4, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.lblPuntoVenta, 0)
        Me.FilterPanel.Controls.SetChildIndex(Me.dteFecha, 0)
        '
        'FooterPanel
        '
        Me.FooterPanel.Location = New System.Drawing.Point(0, 608)
        Me.FooterPanel.Name = "FooterPanel"
        Me.FooterPanel.Size = New System.Drawing.Size(872, 8)
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 23)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Punto de Venta:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(40, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 23)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Fecha:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPuntoVenta
        '
        Me.lblPuntoVenta.Location = New System.Drawing.Point(112, 8)
        Me.lblPuntoVenta.Name = "lblPuntoVenta"
        Me.lblPuntoVenta.Size = New System.Drawing.Size(432, 23)
        Me.lblPuntoVenta.TabIndex = 2
        Me.lblPuntoVenta.Text = "Punto de Venta"
        Me.lblPuntoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(CType(0, Long))
        Me.dteFecha.Location = New System.Drawing.Point(112, 32)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Size = New System.Drawing.Size(88, 20)
        Me.dteFecha.TabIndex = 4
        '
        'brwVentas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CanDelete = False
        Me.ClientSize = New System.Drawing.Size(872, 616)
        Me.Name = "brwVentas"
        Me.Text = "brwVentas"
        CType(Me.popTINGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    'Public AbrirPVenta As Boolean = False
    Public ActivarBrw As Boolean = True




    Private oVentas As New VillarrealBusiness.clsVentas
    Private oPuntoVenta As New VillarrealBusiness.clsPuntosVenta


#End Region

#Region "Eventos de la Forma"

    'Private Sub Ventas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
    '    Me.dteFecha.DateTime = CDate(TinApp.FechaServidor).Date
    'End Sub

    Private Sub brwVentas_LoadData(ByRef Response As Dipros.Utils.Events) Handles MyBase.LoadData
        'Me.dteFecha.DateTime = CDate(TinApp.FechaServidor).Date
        Response = clsUtilerias.ValidaFecha(Me.dteFecha.Text)
        If Response.ErrorFound Then Exit Sub
        Response = oVentas.Listado(Comunes.Common.PuntoVenta_Actual, Me.dteFecha.DateTime.Date, Common.Sucursal_Actual)
    End Sub

    Private Sub brwVentas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim response As New Events
        response = oPuntoVenta.DespliegaDatos(Comunes.Common.PuntoVenta_Actual)
        Me.lblPuntoVenta.Text = CType(response.Value, DataSet).Tables(0).Rows(0).Item("descripcion")
        response = Nothing

        Me.dteFecha.DateTime = CDate(TinApp.FechaServidor).Date

        Me.FooterPanel.Visible = False


        If VerificaPermisoExtendido(MenuOption.Name, "FECHA_VENTAS") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If

    End Sub



#End Region

#Region "Funcionalidad"

#End Region





End Class
