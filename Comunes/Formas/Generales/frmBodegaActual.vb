Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms

Public Class frmBodegaActual
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents clcSucursal As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBodegaActual))
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lblTitulo = New System.Windows.Forms.Label
        Me.clcSucursal = New Dipros.Editors.TINCalcEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(221, 28)
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "Descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(72, 88)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(320, Long)
        Me.lkpBodega.Required = True
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodega.TabIndex = 4
        Me.lkpBodega.Tag = "bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(16, 88)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 3
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTitulo
        '
        Me.lblTitulo.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.Location = New System.Drawing.Point(8, 40)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(360, 16)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Text = "Seleccione una Bodega"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'clcSucursal
        '
        Me.clcSucursal.EditValue = "0"
        Me.clcSucursal.Location = New System.Drawing.Point(72, 64)
        Me.clcSucursal.MaxValue = 0
        Me.clcSucursal.MinValue = 0
        Me.clcSucursal.Name = "clcSucursal"
        '
        'clcSucursal.Properties
        '
        Me.clcSucursal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.Enabled = False
        Me.clcSucursal.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcSucursal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSucursal.Size = New System.Drawing.Size(40, 19)
        Me.clcSucursal.TabIndex = 2
        Me.clcSucursal.Tag = "sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(10, 64)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 1
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmBodegaActual
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 120)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.clcSucursal)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lblBodega)
        Me.Name = "frmBodegaActual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bodega Actual"
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lblTitulo, 0)
        Me.Controls.SetChildIndex(Me.clcSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oBodegas As New VillarrealBusiness.clsBodegas
    Private Entrar As Boolean = False

    Public ReadOnly Property EntrarMain() As Boolean
        Get
            Return Entrar
        End Get

    End Property

    Public Property Sucursal() As Long
        Get
            Return Me.clcSucursal.EditValue
        End Get
        Set(ByVal Value As Long)
            Me.clcSucursal.EditValue = Value
        End Set

    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmBodegaActual_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        If Me.lkpBodega.EditValue <> Nothing Then
            Comunes.Common.Bodega_Actual = Me.lkpBodega.EditValue()
            Comunes.clsUtilerias.GuardaArchivoBodega(Me.lkpBodega.EditValue)
            Entrar = True
        End If

    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Public Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Me.Sucursal = Comunes.Common.Sucursal_Actual
        Me.clcSucursal.EditValue = Me.Sucursal

        If Me.clcSucursal.EditValue = 0 Or Me.clcSucursal.EditValue = -1 Then
            Response = oBodegas.LookupBodegasUsuarios(TinApp.Connection.User)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.lkpBodega.DataSource = oDataSet.Tables(0)
                oDataSet = Nothing
            End If
            Response = Nothing

        Else
            Response = oBodegas.LookupBodegasUsuarios(TinApp.Connection.User, Me.Sucursal)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.lkpBodega.DataSource = oDataSet.Tables(0)
                oDataSet = Nothing
            End If
            Response = Nothing
        End If

    End Sub

#End Region


End Class
