Imports Comunes.clsUtilerias
Imports Dipros.Utils.Common
Imports Dipros.Utils
Imports System.Windows.Forms

Public Class frmVariables
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys

    Private banActiva As Boolean = False
    Private InicioComisionesCobrador As DateTime
    Private FinComisionesCobrador As DateTime
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblEmpresa As System.Windows.Forms.Label
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
		Friend WithEvents txtTelefono As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblLogotipo As System.Windows.Forms.Label
    Friend WithEvents txtEmpresa As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtDireccion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents picLogotipo As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tbGenerales As System.Windows.Forms.TabPage
    Friend WithEvents tbConceptosInventario As System.Windows.Forms.TabPage
    Friend WithEvents lblConceptoCompra As System.Windows.Forms.Label
    Friend WithEvents lkpCompra As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpCancelacionCompra As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpCancelacionVenta As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lkpVenta As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lkpCancelacionDevolucionesProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpDevolucionesProveedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lkpCancelacionTraspaso As Dipros.Editors.TINMultiLookup
    Friend WithEvents tbPrecios As System.Windows.Forms.TabPage
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lkpPrecioExpo As Dipros.Editors.TINMultiLookup
    Friend WithEvents tbConceptosCXC As System.Windows.Forms.TabPage
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lkpFacturaCredito As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lkpFacturaContado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpNotaCredito As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpNotaCargo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpAbono As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcImpuesto As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents clcporcentaje_intereses_moratorios As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcDias_gracia As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lkpIntereses As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lkpPrecioContado As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents dteFechaCierre As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents clcDiasArmado As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcDiasInstalacion As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents clcFactorLimiteCredito As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents tbConceptosCXP As System.Windows.Forms.TabPage
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoNotaEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConceptoCXPPagoVariasFacturas As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoCXPPagoVariasFacturas As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents lkplkpConceptoCXPNotaCredito As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lkpDescuentosAnticipados As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents lkpInteresesEspeciales As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblMesesSinAbonarParaJuridico As System.Windows.Forms.Label
    Friend WithEvents clcMesesSinAbonarParaJuridico As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblHoraInicioComisionesCobrador As System.Windows.Forms.Label
    Friend WithEvents lblHoraFinComisionesCobrador As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents clcEngancheEnMenos As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents speInicioComisionesCobradoresHoras As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents speInicioComisionesCobradoresMinutos As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents speCierreComisionesCobradoresHoras As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents speCierreComisionesCobradoresMinutos As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblAbonosPorComision As System.Windows.Forms.Label
    Friend WithEvents lblConceptoCXPCargoPorFlete As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoCXPCargoPorFlete As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpConceptoCXPAbonoPorAnticipo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents lkpConceptosMovimientoChequera As Dipros.Editors.TINMultiLookup
    Friend WithEvents tbCotizaciones As System.Windows.Forms.TabPage
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents clcDiasVigenciaCotizacion As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents memPieCotizacion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents memEncabezadoCotizacion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents lkpFormaPagoContado As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents lkpVistasSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpVistasEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpConceptosMovimientoChequeraCxP As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents clcDias_gracia_nota_cargo As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents cboTipoInteres As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents lkpEntradaReparacion As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents lkpSalidaReparacion As Dipros.Editors.TINMultiLookup
    Friend WithEvents tbVales As System.Windows.Forms.TabPage
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lkpNCGastosAdministrativos As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpNCValesVencidos As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpFormaPagoVales As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcImporteGastosAdministrativos As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents lkpPrecioListaConvenio As Dipros.Editors.TINMultiLookup

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmVariables))
        Me.lblEmpresa = New System.Windows.Forms.Label
        Me.lblDireccion = New System.Windows.Forms.Label
        Me.lblTelefono = New System.Windows.Forms.Label
        Me.txtTelefono = New DevExpress.XtraEditors.TextEdit
        Me.lblLogotipo = New System.Windows.Forms.Label
        Me.txtEmpresa = New DevExpress.XtraEditors.MemoEdit
        Me.txtDireccion = New DevExpress.XtraEditors.MemoEdit
        Me.picLogotipo = New DevExpress.XtraEditors.PictureEdit
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tbGenerales = New System.Windows.Forms.TabPage
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.clcDias_gracia_nota_cargo = New DevExpress.XtraEditors.CalcEdit
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.lkpFormaPagoContado = New Dipros.Editors.TINMultiLookup
        Me.clcMesesSinAbonarParaJuridico = New DevExpress.XtraEditors.CalcEdit
        Me.lblMesesSinAbonarParaJuridico = New System.Windows.Forms.Label
        Me.dteFechaCierre = New DevExpress.XtraEditors.DateEdit
        Me.Label19 = New System.Windows.Forms.Label
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.Label16 = New System.Windows.Forms.Label
        Me.clcDias_gracia = New DevExpress.XtraEditors.CalcEdit
        Me.clcporcentaje_intereses_moratorios = New DevExpress.XtraEditors.CalcEdit
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.clcImpuesto = New DevExpress.XtraEditors.CalcEdit
        Me.clcDiasArmado = New DevExpress.XtraEditors.CalcEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.clcDiasInstalacion = New DevExpress.XtraEditors.CalcEdit
        Me.Label41 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.cboTipoInteres = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.clcFactorLimiteCredito = New DevExpress.XtraEditors.CalcEdit
        Me.tbVales = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label45 = New System.Windows.Forms.Label
        Me.lkpNCGastosAdministrativos = New Dipros.Editors.TINMultiLookup
        Me.Label44 = New System.Windows.Forms.Label
        Me.lkpNCValesVencidos = New Dipros.Editors.TINMultiLookup
        Me.Label47 = New System.Windows.Forms.Label
        Me.lkpFormaPagoVales = New Dipros.Editors.TINMultiLookup
        Me.Label46 = New System.Windows.Forms.Label
        Me.clcImporteGastosAdministrativos = New DevExpress.XtraEditors.CalcEdit
        Me.tbConceptosCXP = New System.Windows.Forms.TabPage
        Me.lkpConceptosMovimientoChequeraCxP = New Dipros.Editors.TINMultiLookup
        Me.Label39 = New System.Windows.Forms.Label
        Me.lkpConceptosMovimientoChequera = New Dipros.Editors.TINMultiLookup
        Me.Label33 = New System.Windows.Forms.Label
        Me.lblConceptoCXPCargoPorFlete = New System.Windows.Forms.Label
        Me.lkpConceptoCXPCargoPorFlete = New Dipros.Editors.TINMultiLookup
        Me.lblAbonosPorComision = New System.Windows.Forms.Label
        Me.lkpConceptoCXPAbonoPorAnticipo = New Dipros.Editors.TINMultiLookup
        Me.Label25 = New System.Windows.Forms.Label
        Me.lkplkpConceptoCXPNotaCredito = New Dipros.Editors.TINMultiLookup
        Me.lblConceptoCXPPagoVariasFacturas = New System.Windows.Forms.Label
        Me.lkpConceptoCXPPagoVariasFacturas = New Dipros.Editors.TINMultiLookup
        Me.Label23 = New System.Windows.Forms.Label
        Me.lkpConceptoNotaEntrada = New Dipros.Editors.TINMultiLookup
        Me.tbConceptosCXC = New System.Windows.Forms.TabPage
        Me.Label27 = New System.Windows.Forms.Label
        Me.lkpInteresesEspeciales = New Dipros.Editors.TINMultiLookup
        Me.Label26 = New System.Windows.Forms.Label
        Me.lkpDescuentosAnticipados = New Dipros.Editors.TINMultiLookup
        Me.Label17 = New System.Windows.Forms.Label
        Me.lkpIntereses = New Dipros.Editors.TINMultiLookup
        Me.Label13 = New System.Windows.Forms.Label
        Me.lkpAbono = New Dipros.Editors.TINMultiLookup
        Me.Label9 = New System.Windows.Forms.Label
        Me.lkpFacturaCredito = New Dipros.Editors.TINMultiLookup
        Me.Label10 = New System.Windows.Forms.Label
        Me.lkpFacturaContado = New Dipros.Editors.TINMultiLookup
        Me.Label11 = New System.Windows.Forms.Label
        Me.lkpNotaCredito = New Dipros.Editors.TINMultiLookup
        Me.Label12 = New System.Windows.Forms.Label
        Me.lkpNotaCargo = New Dipros.Editors.TINMultiLookup
        Me.tbConceptosInventario = New System.Windows.Forms.TabPage
        Me.Label42 = New System.Windows.Forms.Label
        Me.lkpEntradaReparacion = New Dipros.Editors.TINMultiLookup
        Me.Label43 = New System.Windows.Forms.Label
        Me.lkpSalidaReparacion = New Dipros.Editors.TINMultiLookup
        Me.Label38 = New System.Windows.Forms.Label
        Me.lkpVistasEntrada = New Dipros.Editors.TINMultiLookup
        Me.Label37 = New System.Windows.Forms.Label
        Me.lkpVistasSalida = New Dipros.Editors.TINMultiLookup
        Me.lblConceptoCompra = New System.Windows.Forms.Label
        Me.lkpCompra = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpCancelacionCompra = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpCancelacionVenta = New Dipros.Editors.TINMultiLookup
        Me.Label4 = New System.Windows.Forms.Label
        Me.lkpVenta = New Dipros.Editors.TINMultiLookup
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lkpCancelacionDevolucionesProveedor = New Dipros.Editors.TINMultiLookup
        Me.lkpDevolucionesProveedor = New Dipros.Editors.TINMultiLookup
        Me.Label7 = New System.Windows.Forms.Label
        Me.lkpCancelacionTraspaso = New Dipros.Editors.TINMultiLookup
        Me.tbPrecios = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.speCierreComisionesCobradoresMinutos = New DevExpress.XtraEditors.SpinEdit
        Me.speInicioComisionesCobradoresMinutos = New DevExpress.XtraEditors.SpinEdit
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.speInicioComisionesCobradoresHoras = New DevExpress.XtraEditors.SpinEdit
        Me.lblHoraFinComisionesCobrador = New System.Windows.Forms.Label
        Me.speCierreComisionesCobradoresHoras = New DevExpress.XtraEditors.SpinEdit
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.lblHoraInicioComisionesCobrador = New System.Windows.Forms.Label
        Me.clcEngancheEnMenos = New DevExpress.XtraEditors.CalcEdit
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lkpPrecioExpo = New Dipros.Editors.TINMultiLookup
        Me.lkpPrecioContado = New Dipros.Editors.TINMultiLookup
        Me.Label18 = New System.Windows.Forms.Label
        Me.tbCotizaciones = New System.Windows.Forms.TabPage
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.memPieCotizacion = New DevExpress.XtraEditors.MemoEdit
        Me.memEncabezadoCotizacion = New DevExpress.XtraEditors.MemoEdit
        Me.Label24 = New System.Windows.Forms.Label
        Me.clcDiasVigenciaCotizacion = New DevExpress.XtraEditors.CalcEdit
        Me.Label48 = New System.Windows.Forms.Label
        Me.lkpPrecioListaConvenio = New Dipros.Editors.TINMultiLookup
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmpresa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLogotipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.tbGenerales.SuspendLayout()
        CType(Me.clcDias_gracia_nota_cargo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMesesSinAbonarParaJuridico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaCierre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDias_gracia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcporcentaje_intereses_moratorios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImpuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDiasArmado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDiasInstalacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoInteres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFactorLimiteCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbVales.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.clcImporteGastosAdministrativos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbConceptosCXP.SuspendLayout()
        Me.tbConceptosCXC.SuspendLayout()
        Me.tbConceptosInventario.SuspendLayout()
        Me.tbPrecios.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.speCierreComisionesCobradoresMinutos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.speInicioComisionesCobradoresMinutos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.speInicioComisionesCobradoresHoras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.speCierreComisionesCobradoresHoras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcEngancheEnMenos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbCotizaciones.SuspendLayout()
        CType(Me.memPieCotizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.memEncabezadoCotizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDiasVigenciaCotizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(12059, 28)
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.Location = New System.Drawing.Point(63, 11)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(57, 16)
        Me.lblEmpresa.TabIndex = 0
        Me.lblEmpresa.Tag = ""
        Me.lblEmpresa.Text = "&Empresa:"
        Me.lblEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(60, 46)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(60, 16)
        Me.lblDireccion.TabIndex = 2
        Me.lblDireccion.Tag = ""
        Me.lblDireccion.Text = "&Dirección:"
        Me.lblDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(64, 84)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(56, 16)
        Me.lblTelefono.TabIndex = 4
        Me.lblTelefono.Tag = ""
        Me.lblTelefono.Text = "&Teléfono:"
        Me.lblTelefono.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono
        '
        Me.txtTelefono.EditValue = ""
        Me.txtTelefono.Location = New System.Drawing.Point(124, 81)
        Me.txtTelefono.Name = "txtTelefono"
        '
        'txtTelefono.Properties
        '
        Me.txtTelefono.Properties.MaxLength = 30
        Me.txtTelefono.Size = New System.Drawing.Size(136, 20)
        Me.txtTelefono.TabIndex = 5
        Me.txtTelefono.Tag = "telefonos"
        '
        'lblLogotipo
        '
        Me.lblLogotipo.Location = New System.Drawing.Point(484, 99)
        Me.lblLogotipo.Name = "lblLogotipo"
        Me.lblLogotipo.Size = New System.Drawing.Size(100, 16)
        Me.lblLogotipo.TabIndex = 21
        Me.lblLogotipo.Tag = ""
        Me.lblLogotipo.Text = "&Logotipo"
        Me.lblLogotipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtEmpresa
        '
        Me.txtEmpresa.EditValue = ""
        Me.txtEmpresa.Location = New System.Drawing.Point(124, 11)
        Me.txtEmpresa.Name = "txtEmpresa"
        '
        'txtEmpresa.Properties
        '
        Me.txtEmpresa.Properties.MaxLength = 100
        Me.txtEmpresa.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtEmpresa.Size = New System.Drawing.Size(312, 32)
        Me.txtEmpresa.TabIndex = 1
        Me.txtEmpresa.Tag = "empresa"
        '
        'txtDireccion
        '
        Me.txtDireccion.EditValue = ""
        Me.txtDireccion.Location = New System.Drawing.Point(124, 46)
        Me.txtDireccion.Name = "txtDireccion"
        '
        'txtDireccion.Properties
        '
        Me.txtDireccion.Properties.MaxLength = 100
        Me.txtDireccion.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtDireccion.Size = New System.Drawing.Size(312, 32)
        Me.txtDireccion.TabIndex = 3
        Me.txtDireccion.Tag = "direccion"
        '
        'picLogotipo
        '
        Me.picLogotipo.Location = New System.Drawing.Point(484, 11)
        Me.picLogotipo.Name = "picLogotipo"
        '
        'picLogotipo.Properties
        '
        Me.picLogotipo.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.picLogotipo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picLogotipo.Size = New System.Drawing.Size(104, 88)
        Me.picLogotipo.TabIndex = 16
        Me.picLogotipo.Tag = "logotipo"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tbGenerales)
        Me.TabControl1.Controls.Add(Me.tbPrecios)
        Me.TabControl1.Controls.Add(Me.tbVales)
        Me.TabControl1.Controls.Add(Me.tbConceptosCXP)
        Me.TabControl1.Controls.Add(Me.tbConceptosCXC)
        Me.TabControl1.Controls.Add(Me.tbConceptosInventario)
        Me.TabControl1.Controls.Add(Me.tbCotizaciones)
        Me.TabControl1.Location = New System.Drawing.Point(0, 32)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(608, 312)
        Me.TabControl1.TabIndex = 0
        '
        'tbGenerales
        '
        Me.tbGenerales.BackColor = System.Drawing.Color.White
        Me.tbGenerales.Controls.Add(Me.lblArticulo)
        Me.tbGenerales.Controls.Add(Me.lkpArticulo)
        Me.tbGenerales.Controls.Add(Me.clcDias_gracia_nota_cargo)
        Me.tbGenerales.Controls.Add(Me.Label40)
        Me.tbGenerales.Controls.Add(Me.Label36)
        Me.tbGenerales.Controls.Add(Me.lkpFormaPagoContado)
        Me.tbGenerales.Controls.Add(Me.clcMesesSinAbonarParaJuridico)
        Me.tbGenerales.Controls.Add(Me.lblMesesSinAbonarParaJuridico)
        Me.tbGenerales.Controls.Add(Me.dteFechaCierre)
        Me.tbGenerales.Controls.Add(Me.Label19)
        Me.tbGenerales.Controls.Add(Me.lblCobrador)
        Me.tbGenerales.Controls.Add(Me.lkpCobrador)
        Me.tbGenerales.Controls.Add(Me.Label16)
        Me.tbGenerales.Controls.Add(Me.clcDias_gracia)
        Me.tbGenerales.Controls.Add(Me.clcporcentaje_intereses_moratorios)
        Me.tbGenerales.Controls.Add(Me.Label15)
        Me.tbGenerales.Controls.Add(Me.Label14)
        Me.tbGenerales.Controls.Add(Me.clcImpuesto)
        Me.tbGenerales.Controls.Add(Me.txtEmpresa)
        Me.tbGenerales.Controls.Add(Me.txtDireccion)
        Me.tbGenerales.Controls.Add(Me.lblEmpresa)
        Me.tbGenerales.Controls.Add(Me.lblDireccion)
        Me.tbGenerales.Controls.Add(Me.picLogotipo)
        Me.tbGenerales.Controls.Add(Me.lblTelefono)
        Me.tbGenerales.Controls.Add(Me.txtTelefono)
        Me.tbGenerales.Controls.Add(Me.lblLogotipo)
        Me.tbGenerales.Controls.Add(Me.clcDiasArmado)
        Me.tbGenerales.Controls.Add(Me.Label20)
        Me.tbGenerales.Controls.Add(Me.Label21)
        Me.tbGenerales.Controls.Add(Me.clcDiasInstalacion)
        Me.tbGenerales.Controls.Add(Me.Label41)
        Me.tbGenerales.Controls.Add(Me.Label22)
        Me.tbGenerales.Controls.Add(Me.cboTipoInteres)
        Me.tbGenerales.Controls.Add(Me.clcFactorLimiteCredito)
        Me.tbGenerales.Location = New System.Drawing.Point(4, 22)
        Me.tbGenerales.Name = "tbGenerales"
        Me.tbGenerales.Size = New System.Drawing.Size(600, 286)
        Me.tbGenerales.TabIndex = 0
        Me.tbGenerales.Text = "Generales"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(21, 232)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(99, 16)
        Me.lblArticulo.TabIndex = 32
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Artículo Anticipo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = True
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(124, 232)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(400, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = "modelo"
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.SearchMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(268, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 33
        Me.lkpArticulo.Tag = "articulo_anticipos"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'clcDias_gracia_nota_cargo
        '
        Me.clcDias_gracia_nota_cargo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcDias_gracia_nota_cargo.Location = New System.Drawing.Point(396, 107)
        Me.clcDias_gracia_nota_cargo.Name = "clcDias_gracia_nota_cargo"
        Me.clcDias_gracia_nota_cargo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcDias_gracia_nota_cargo.Size = New System.Drawing.Size(75, 20)
        Me.clcDias_gracia_nota_cargo.TabIndex = 20
        Me.clcDias_gracia_nota_cargo.Tag = "dias_gracia_nota_cargo"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(240, 112)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(155, 16)
        Me.Label40.TabIndex = 19
        Me.Label40.Tag = ""
        Me.Label40.Text = "Días de Gracia Nota Cargo:"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(305, 159)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(85, 16)
        Me.Label36.TabIndex = 24
        Me.Label36.Tag = ""
        Me.Label36.Text = "Pago Contado:"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFormaPagoContado
        '
        Me.lkpFormaPagoContado.AllowAdd = False
        Me.lkpFormaPagoContado.AutoReaload = False
        Me.lkpFormaPagoContado.DataSource = Nothing
        Me.lkpFormaPagoContado.DefaultSearchField = ""
        Me.lkpFormaPagoContado.DisplayMember = "descripcion"
        Me.lkpFormaPagoContado.EditValue = Nothing
        Me.lkpFormaPagoContado.Filtered = False
        Me.lkpFormaPagoContado.InitValue = Nothing
        Me.lkpFormaPagoContado.Location = New System.Drawing.Point(396, 156)
        Me.lkpFormaPagoContado.MultiSelect = False
        Me.lkpFormaPagoContado.Name = "lkpFormaPagoContado"
        Me.lkpFormaPagoContado.NullText = ""
        Me.lkpFormaPagoContado.PopupWidth = CType(400, Long)
        Me.lkpFormaPagoContado.ReadOnlyControl = False
        Me.lkpFormaPagoContado.Required = False
        Me.lkpFormaPagoContado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFormaPagoContado.SearchMember = ""
        Me.lkpFormaPagoContado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFormaPagoContado.SelectAll = False
        Me.lkpFormaPagoContado.Size = New System.Drawing.Size(192, 20)
        Me.lkpFormaPagoContado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFormaPagoContado.TabIndex = 25
        Me.lkpFormaPagoContado.Tag = "pago_contado_caja"
        Me.lkpFormaPagoContado.ToolTip = Nothing
        Me.lkpFormaPagoContado.ValueMember = "forma_pago"
        '
        'clcMesesSinAbonarParaJuridico
        '
        Me.clcMesesSinAbonarParaJuridico.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcMesesSinAbonarParaJuridico.Location = New System.Drawing.Point(396, 206)
        Me.clcMesesSinAbonarParaJuridico.Name = "clcMesesSinAbonarParaJuridico"
        Me.clcMesesSinAbonarParaJuridico.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcMesesSinAbonarParaJuridico.Size = New System.Drawing.Size(75, 20)
        Me.clcMesesSinAbonarParaJuridico.TabIndex = 29
        Me.clcMesesSinAbonarParaJuridico.Tag = "meses_sin_abonar_para_juridico"
        Me.clcMesesSinAbonarParaJuridico.ToolTip = "meses sin abonar para juridico"
        '
        'lblMesesSinAbonarParaJuridico
        '
        Me.lblMesesSinAbonarParaJuridico.AutoSize = True
        Me.lblMesesSinAbonarParaJuridico.Location = New System.Drawing.Point(209, 209)
        Me.lblMesesSinAbonarParaJuridico.Name = "lblMesesSinAbonarParaJuridico"
        Me.lblMesesSinAbonarParaJuridico.Size = New System.Drawing.Size(181, 16)
        Me.lblMesesSinAbonarParaJuridico.TabIndex = 28
        Me.lblMesesSinAbonarParaJuridico.Tag = ""
        Me.lblMesesSinAbonarParaJuridico.Text = "&Meses Sin Abonar Para Juridico:"
        Me.lblMesesSinAbonarParaJuridico.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaCierre
        '
        Me.dteFechaCierre.EditValue = "10/04/2006"
        Me.dteFechaCierre.Location = New System.Drawing.Point(124, 181)
        Me.dteFechaCierre.Name = "dteFechaCierre"
        '
        'dteFechaCierre.Properties
        '
        Me.dteFechaCierre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCierre.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCierre.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaCierre.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFechaCierre.Size = New System.Drawing.Size(88, 23)
        Me.dteFechaCierre.TabIndex = 13
        Me.dteFechaCierre.Tag = "fecha_cierre"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(25, 184)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(95, 16)
        Me.Label19.TabIndex = 12
        Me.Label19.Tag = ""
        Me.Label19.Text = "&Fecha de Cierre:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(302, 134)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(88, 16)
        Me.lblCobrador.TabIndex = 22
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador casa:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = False
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(396, 131)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = ""
        Me.lkpCobrador.PopupWidth = CType(400, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = False
        Me.lkpCobrador.Size = New System.Drawing.Size(192, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 23
        Me.lkpCobrador.Tag = "cobrador_casa"
        Me.lkpCobrador.ToolTip = Nothing
        Me.lkpCobrador.ValueMember = "cobrador"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(267, 84)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(123, 16)
        Me.Label16.TabIndex = 17
        Me.Label16.Tag = ""
        Me.Label16.Text = "Intereses Moratorios:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDias_gracia
        '
        Me.clcDias_gracia.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcDias_gracia.Location = New System.Drawing.Point(124, 131)
        Me.clcDias_gracia.Name = "clcDias_gracia"
        Me.clcDias_gracia.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcDias_gracia.Size = New System.Drawing.Size(75, 20)
        Me.clcDias_gracia.TabIndex = 9
        Me.clcDias_gracia.Tag = "dias_gracia"
        '
        'clcporcentaje_intereses_moratorios
        '
        Me.clcporcentaje_intereses_moratorios.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcporcentaje_intereses_moratorios.Location = New System.Drawing.Point(396, 81)
        Me.clcporcentaje_intereses_moratorios.Name = "clcporcentaje_intereses_moratorios"
        Me.clcporcentaje_intereses_moratorios.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcporcentaje_intereses_moratorios.Size = New System.Drawing.Size(75, 20)
        Me.clcporcentaje_intereses_moratorios.TabIndex = 18
        Me.clcporcentaje_intereses_moratorios.Tag = "porcentaje_intereses_moratorios"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(31, 134)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(89, 16)
        Me.Label15.TabIndex = 8
        Me.Label15.Tag = ""
        Me.Label15.Text = "Días de Gracia:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(59, 111)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(61, 16)
        Me.Label14.TabIndex = 6
        Me.Label14.Tag = ""
        Me.Label14.Text = "&Impuesto:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImpuesto
        '
        Me.clcImpuesto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImpuesto.Location = New System.Drawing.Point(124, 107)
        Me.clcImpuesto.Name = "clcImpuesto"
        Me.clcImpuesto.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImpuesto.Size = New System.Drawing.Size(75, 20)
        Me.clcImpuesto.TabIndex = 7
        Me.clcImpuesto.Tag = "impuesto"
        '
        'clcDiasArmado
        '
        Me.clcDiasArmado.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcDiasArmado.Location = New System.Drawing.Point(124, 156)
        Me.clcDiasArmado.Name = "clcDiasArmado"
        Me.clcDiasArmado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcDiasArmado.Size = New System.Drawing.Size(75, 20)
        Me.clcDiasArmado.TabIndex = 11
        Me.clcDiasArmado.Tag = "dias_armado"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(12, 159)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(108, 16)
        Me.Label20.TabIndex = 10
        Me.Label20.Tag = ""
        Me.Label20.Text = "Días para Armado:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(8, 208)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(112, 16)
        Me.Label21.TabIndex = 14
        Me.Label21.Tag = ""
        Me.Label21.Text = "Días P/ Instalación:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDiasInstalacion
        '
        Me.clcDiasInstalacion.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcDiasInstalacion.Location = New System.Drawing.Point(124, 208)
        Me.clcDiasInstalacion.Name = "clcDiasInstalacion"
        Me.clcDiasInstalacion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcDiasInstalacion.Size = New System.Drawing.Size(75, 20)
        Me.clcDiasInstalacion.TabIndex = 15
        Me.clcDiasInstalacion.Tag = "dias_instalacion"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(342, 184)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(48, 16)
        Me.Label41.TabIndex = 26
        Me.Label41.Tag = ""
        Me.Label41.Text = "Interes:"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(512, 184)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(170, 16)
        Me.Label22.TabIndex = 30
        Me.Label22.Tag = ""
        Me.Label22.Text = "Factor para Límite de Crédito:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label22.Visible = False
        '
        'cboTipoInteres
        '
        Me.cboTipoInteres.EditValue = "D"
        Me.cboTipoInteres.Location = New System.Drawing.Point(396, 181)
        Me.cboTipoInteres.Name = "cboTipoInteres"
        '
        'cboTipoInteres.Properties
        '
        Me.cboTipoInteres.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoInteres.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diario", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mensual", "M", -1)})
        Me.cboTipoInteres.Size = New System.Drawing.Size(72, 23)
        Me.cboTipoInteres.TabIndex = 27
        Me.cboTipoInteres.Tag = "tipo_intereses"
        '
        'clcFactorLimiteCredito
        '
        Me.clcFactorLimiteCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFactorLimiteCredito.Location = New System.Drawing.Point(512, 208)
        Me.clcFactorLimiteCredito.Name = "clcFactorLimiteCredito"
        Me.clcFactorLimiteCredito.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFactorLimiteCredito.Size = New System.Drawing.Size(84, 20)
        Me.clcFactorLimiteCredito.TabIndex = 31
        Me.clcFactorLimiteCredito.TabStop = False
        Me.clcFactorLimiteCredito.Tag = "factor_limite_credito"
        Me.clcFactorLimiteCredito.Visible = False
        '
        'tbVales
        '
        Me.tbVales.BackColor = System.Drawing.Color.White
        Me.tbVales.Controls.Add(Me.GroupBox2)
        Me.tbVales.Controls.Add(Me.Label47)
        Me.tbVales.Controls.Add(Me.lkpFormaPagoVales)
        Me.tbVales.Controls.Add(Me.Label46)
        Me.tbVales.Controls.Add(Me.clcImporteGastosAdministrativos)
        Me.tbVales.Location = New System.Drawing.Point(4, 22)
        Me.tbVales.Name = "tbVales"
        Me.tbVales.Size = New System.Drawing.Size(600, 286)
        Me.tbVales.TabIndex = 7
        Me.tbVales.Text = "Vales"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label45)
        Me.GroupBox2.Controls.Add(Me.lkpNCGastosAdministrativos)
        Me.GroupBox2.Controls.Add(Me.Label44)
        Me.GroupBox2.Controls.Add(Me.lkpNCValesVencidos)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(584, 80)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Conceptos"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(72, 21)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(136, 16)
        Me.Label45.TabIndex = 0
        Me.Label45.Tag = ""
        Me.Label45.Text = "Gastos Administrativos:"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpNCGastosAdministrativos
        '
        Me.lkpNCGastosAdministrativos.AllowAdd = False
        Me.lkpNCGastosAdministrativos.AutoReaload = False
        Me.lkpNCGastosAdministrativos.DataSource = Nothing
        Me.lkpNCGastosAdministrativos.DefaultSearchField = ""
        Me.lkpNCGastosAdministrativos.DisplayMember = "descripcion"
        Me.lkpNCGastosAdministrativos.EditValue = Nothing
        Me.lkpNCGastosAdministrativos.Filtered = False
        Me.lkpNCGastosAdministrativos.InitValue = Nothing
        Me.lkpNCGastosAdministrativos.Location = New System.Drawing.Point(216, 21)
        Me.lkpNCGastosAdministrativos.MultiSelect = False
        Me.lkpNCGastosAdministrativos.Name = "lkpNCGastosAdministrativos"
        Me.lkpNCGastosAdministrativos.NullText = ""
        Me.lkpNCGastosAdministrativos.PopupWidth = CType(400, Long)
        Me.lkpNCGastosAdministrativos.ReadOnlyControl = False
        Me.lkpNCGastosAdministrativos.Required = False
        Me.lkpNCGastosAdministrativos.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpNCGastosAdministrativos.SearchMember = ""
        Me.lkpNCGastosAdministrativos.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpNCGastosAdministrativos.SelectAll = False
        Me.lkpNCGastosAdministrativos.Size = New System.Drawing.Size(280, 20)
        Me.lkpNCGastosAdministrativos.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpNCGastosAdministrativos.TabIndex = 1
        Me.lkpNCGastosAdministrativos.Tag = "concepto_gastos_administrativos"
        Me.lkpNCGastosAdministrativos.ToolTip = Nothing
        Me.lkpNCGastosAdministrativos.ValueMember = "concepto"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(117, 45)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(91, 16)
        Me.Label44.TabIndex = 2
        Me.Label44.Tag = ""
        Me.Label44.Text = "Vales Vencidos:"
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpNCValesVencidos
        '
        Me.lkpNCValesVencidos.AllowAdd = False
        Me.lkpNCValesVencidos.AutoReaload = False
        Me.lkpNCValesVencidos.DataSource = Nothing
        Me.lkpNCValesVencidos.DefaultSearchField = ""
        Me.lkpNCValesVencidos.DisplayMember = "descripcion"
        Me.lkpNCValesVencidos.EditValue = Nothing
        Me.lkpNCValesVencidos.Filtered = False
        Me.lkpNCValesVencidos.InitValue = Nothing
        Me.lkpNCValesVencidos.Location = New System.Drawing.Point(216, 45)
        Me.lkpNCValesVencidos.MultiSelect = False
        Me.lkpNCValesVencidos.Name = "lkpNCValesVencidos"
        Me.lkpNCValesVencidos.NullText = ""
        Me.lkpNCValesVencidos.PopupWidth = CType(400, Long)
        Me.lkpNCValesVencidos.ReadOnlyControl = False
        Me.lkpNCValesVencidos.Required = False
        Me.lkpNCValesVencidos.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpNCValesVencidos.SearchMember = ""
        Me.lkpNCValesVencidos.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpNCValesVencidos.SelectAll = False
        Me.lkpNCValesVencidos.Size = New System.Drawing.Size(280, 20)
        Me.lkpNCValesVencidos.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpNCValesVencidos.TabIndex = 3
        Me.lkpNCValesVencidos.Tag = "concepto_vales_vencidos"
        Me.lkpNCValesVencidos.ToolTip = Nothing
        Me.lkpNCValesVencidos.ValueMember = "concepto"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(61, 128)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(154, 16)
        Me.Label47.TabIndex = 3
        Me.Label47.Tag = ""
        Me.Label47.Text = "Forma de Pago para Vales:"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFormaPagoVales
        '
        Me.lkpFormaPagoVales.AllowAdd = False
        Me.lkpFormaPagoVales.AutoReaload = False
        Me.lkpFormaPagoVales.DataSource = Nothing
        Me.lkpFormaPagoVales.DefaultSearchField = ""
        Me.lkpFormaPagoVales.DisplayMember = "descripcion"
        Me.lkpFormaPagoVales.EditValue = Nothing
        Me.lkpFormaPagoVales.Filtered = False
        Me.lkpFormaPagoVales.InitValue = Nothing
        Me.lkpFormaPagoVales.Location = New System.Drawing.Point(224, 128)
        Me.lkpFormaPagoVales.MultiSelect = False
        Me.lkpFormaPagoVales.Name = "lkpFormaPagoVales"
        Me.lkpFormaPagoVales.NullText = ""
        Me.lkpFormaPagoVales.PopupWidth = CType(400, Long)
        Me.lkpFormaPagoVales.ReadOnlyControl = False
        Me.lkpFormaPagoVales.Required = False
        Me.lkpFormaPagoVales.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFormaPagoVales.SearchMember = ""
        Me.lkpFormaPagoVales.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFormaPagoVales.SelectAll = False
        Me.lkpFormaPagoVales.Size = New System.Drawing.Size(280, 20)
        Me.lkpFormaPagoVales.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFormaPagoVales.TabIndex = 4
        Me.lkpFormaPagoVales.Tag = "pago_vales_caja"
        Me.lkpFormaPagoVales.ToolTip = Nothing
        Me.lkpFormaPagoVales.ValueMember = "forma_pago"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(13, 104)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(202, 16)
        Me.Label46.TabIndex = 1
        Me.Label46.Tag = ""
        Me.Label46.Text = "Importe de Gastos Administrativos:"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporteGastosAdministrativos
        '
        Me.clcImporteGastosAdministrativos.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImporteGastosAdministrativos.Location = New System.Drawing.Point(224, 104)
        Me.clcImporteGastosAdministrativos.Name = "clcImporteGastosAdministrativos"
        '
        'clcImporteGastosAdministrativos.Properties
        '
        Me.clcImporteGastosAdministrativos.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporteGastosAdministrativos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteGastosAdministrativos.Properties.EditFormat.FormatString = "c2"
        Me.clcImporteGastosAdministrativos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteGastosAdministrativos.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImporteGastosAdministrativos.Size = New System.Drawing.Size(75, 20)
        Me.clcImporteGastosAdministrativos.TabIndex = 2
        Me.clcImporteGastosAdministrativos.Tag = "importe_gastos_administrativos"
        '
        'tbConceptosCXP
        '
        Me.tbConceptosCXP.BackColor = System.Drawing.Color.White
        Me.tbConceptosCXP.Controls.Add(Me.lkpConceptosMovimientoChequeraCxP)
        Me.tbConceptosCXP.Controls.Add(Me.Label39)
        Me.tbConceptosCXP.Controls.Add(Me.lkpConceptosMovimientoChequera)
        Me.tbConceptosCXP.Controls.Add(Me.Label33)
        Me.tbConceptosCXP.Controls.Add(Me.lblConceptoCXPCargoPorFlete)
        Me.tbConceptosCXP.Controls.Add(Me.lkpConceptoCXPCargoPorFlete)
        Me.tbConceptosCXP.Controls.Add(Me.lblAbonosPorComision)
        Me.tbConceptosCXP.Controls.Add(Me.lkpConceptoCXPAbonoPorAnticipo)
        Me.tbConceptosCXP.Controls.Add(Me.Label25)
        Me.tbConceptosCXP.Controls.Add(Me.lkplkpConceptoCXPNotaCredito)
        Me.tbConceptosCXP.Controls.Add(Me.lblConceptoCXPPagoVariasFacturas)
        Me.tbConceptosCXP.Controls.Add(Me.lkpConceptoCXPPagoVariasFacturas)
        Me.tbConceptosCXP.Controls.Add(Me.Label23)
        Me.tbConceptosCXP.Controls.Add(Me.lkpConceptoNotaEntrada)
        Me.tbConceptosCXP.Location = New System.Drawing.Point(4, 22)
        Me.tbConceptosCXP.Name = "tbConceptosCXP"
        Me.tbConceptosCXP.Size = New System.Drawing.Size(600, 286)
        Me.tbConceptosCXP.TabIndex = 5
        Me.tbConceptosCXP.Text = "Conceptos de CXP"
        '
        'lkpConceptosMovimientoChequeraCxP
        '
        Me.lkpConceptosMovimientoChequeraCxP.AllowAdd = False
        Me.lkpConceptosMovimientoChequeraCxP.AutoReaload = False
        Me.lkpConceptosMovimientoChequeraCxP.DataSource = Nothing
        Me.lkpConceptosMovimientoChequeraCxP.DefaultSearchField = ""
        Me.lkpConceptosMovimientoChequeraCxP.DisplayMember = "descripcion"
        Me.lkpConceptosMovimientoChequeraCxP.EditValue = Nothing
        Me.lkpConceptosMovimientoChequeraCxP.Filtered = False
        Me.lkpConceptosMovimientoChequeraCxP.InitValue = Nothing
        Me.lkpConceptosMovimientoChequeraCxP.Location = New System.Drawing.Point(184, 168)
        Me.lkpConceptosMovimientoChequeraCxP.MultiSelect = False
        Me.lkpConceptosMovimientoChequeraCxP.Name = "lkpConceptosMovimientoChequeraCxP"
        Me.lkpConceptosMovimientoChequeraCxP.NullText = ""
        Me.lkpConceptosMovimientoChequeraCxP.PopupWidth = CType(400, Long)
        Me.lkpConceptosMovimientoChequeraCxP.ReadOnlyControl = False
        Me.lkpConceptosMovimientoChequeraCxP.Required = False
        Me.lkpConceptosMovimientoChequeraCxP.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptosMovimientoChequeraCxP.SearchMember = ""
        Me.lkpConceptosMovimientoChequeraCxP.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptosMovimientoChequeraCxP.SelectAll = False
        Me.lkpConceptosMovimientoChequeraCxP.Size = New System.Drawing.Size(192, 20)
        Me.lkpConceptosMovimientoChequeraCxP.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptosMovimientoChequeraCxP.TabIndex = 13
        Me.lkpConceptosMovimientoChequeraCxP.Tag = "concepto_movimiento_chequera_cxp"
        Me.lkpConceptosMovimientoChequeraCxP.ToolTip = "Concepto de Movimiento de la Chequera"
        Me.lkpConceptosMovimientoChequeraCxP.ValueMember = "concepto"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(64, 168)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(111, 16)
        Me.Label39.TabIndex = 12
        Me.Label39.Text = "Cpto. de Mov. CxP:"
        '
        'lkpConceptosMovimientoChequera
        '
        Me.lkpConceptosMovimientoChequera.AllowAdd = False
        Me.lkpConceptosMovimientoChequera.AutoReaload = False
        Me.lkpConceptosMovimientoChequera.DataSource = Nothing
        Me.lkpConceptosMovimientoChequera.DefaultSearchField = ""
        Me.lkpConceptosMovimientoChequera.DisplayMember = "descripcion"
        Me.lkpConceptosMovimientoChequera.EditValue = Nothing
        Me.lkpConceptosMovimientoChequera.Filtered = False
        Me.lkpConceptosMovimientoChequera.InitValue = Nothing
        Me.lkpConceptosMovimientoChequera.Location = New System.Drawing.Point(184, 144)
        Me.lkpConceptosMovimientoChequera.MultiSelect = False
        Me.lkpConceptosMovimientoChequera.Name = "lkpConceptosMovimientoChequera"
        Me.lkpConceptosMovimientoChequera.NullText = ""
        Me.lkpConceptosMovimientoChequera.PopupWidth = CType(400, Long)
        Me.lkpConceptosMovimientoChequera.ReadOnlyControl = False
        Me.lkpConceptosMovimientoChequera.Required = False
        Me.lkpConceptosMovimientoChequera.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptosMovimientoChequera.SearchMember = ""
        Me.lkpConceptosMovimientoChequera.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptosMovimientoChequera.SelectAll = False
        Me.lkpConceptosMovimientoChequera.Size = New System.Drawing.Size(192, 20)
        Me.lkpConceptosMovimientoChequera.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptosMovimientoChequera.TabIndex = 11
        Me.lkpConceptosMovimientoChequera.Tag = "concepto_movimiento_chequera"
        Me.lkpConceptosMovimientoChequera.ToolTip = "Concepto de Movimiento de la Chequera"
        Me.lkpConceptosMovimientoChequera.ValueMember = "concepto"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(8, 144)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(167, 16)
        Me.Label33.TabIndex = 10
        Me.Label33.Text = "Cpto. de Mov. Dep. Bancario:"
        '
        'lblConceptoCXPCargoPorFlete
        '
        Me.lblConceptoCXPCargoPorFlete.AutoSize = True
        Me.lblConceptoCXPCargoPorFlete.Location = New System.Drawing.Point(85, 120)
        Me.lblConceptoCXPCargoPorFlete.Name = "lblConceptoCXPCargoPorFlete"
        Me.lblConceptoCXPCargoPorFlete.Size = New System.Drawing.Size(94, 16)
        Me.lblConceptoCXPCargoPorFlete.TabIndex = 8
        Me.lblConceptoCXPCargoPorFlete.Tag = ""
        Me.lblConceptoCXPCargoPorFlete.Text = "Cargo por Flete:"
        Me.lblConceptoCXPCargoPorFlete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoCXPCargoPorFlete
        '
        Me.lkpConceptoCXPCargoPorFlete.AllowAdd = False
        Me.lkpConceptoCXPCargoPorFlete.AutoReaload = False
        Me.lkpConceptoCXPCargoPorFlete.DataSource = Nothing
        Me.lkpConceptoCXPCargoPorFlete.DefaultSearchField = ""
        Me.lkpConceptoCXPCargoPorFlete.DisplayMember = "descripcion"
        Me.lkpConceptoCXPCargoPorFlete.EditValue = Nothing
        Me.lkpConceptoCXPCargoPorFlete.Filtered = False
        Me.lkpConceptoCXPCargoPorFlete.InitValue = Nothing
        Me.lkpConceptoCXPCargoPorFlete.Location = New System.Drawing.Point(184, 120)
        Me.lkpConceptoCXPCargoPorFlete.MultiSelect = False
        Me.lkpConceptoCXPCargoPorFlete.Name = "lkpConceptoCXPCargoPorFlete"
        Me.lkpConceptoCXPCargoPorFlete.NullText = ""
        Me.lkpConceptoCXPCargoPorFlete.PopupWidth = CType(400, Long)
        Me.lkpConceptoCXPCargoPorFlete.ReadOnlyControl = False
        Me.lkpConceptoCXPCargoPorFlete.Required = False
        Me.lkpConceptoCXPCargoPorFlete.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoCXPCargoPorFlete.SearchMember = ""
        Me.lkpConceptoCXPCargoPorFlete.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoCXPCargoPorFlete.SelectAll = False
        Me.lkpConceptoCXPCargoPorFlete.Size = New System.Drawing.Size(192, 20)
        Me.lkpConceptoCXPCargoPorFlete.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoCXPCargoPorFlete.TabIndex = 9
        Me.lkpConceptoCXPCargoPorFlete.Tag = "concepto_cxp_cargo_por_flete"
        Me.lkpConceptoCXPCargoPorFlete.ToolTip = "Concepto de Cargo por Flete"
        Me.lkpConceptoCXPCargoPorFlete.ValueMember = "concepto"
        '
        'lblAbonosPorComision
        '
        Me.lblAbonosPorComision.AutoSize = True
        Me.lblAbonosPorComision.Location = New System.Drawing.Point(60, 96)
        Me.lblAbonosPorComision.Name = "lblAbonosPorComision"
        Me.lblAbonosPorComision.Size = New System.Drawing.Size(119, 16)
        Me.lblAbonosPorComision.TabIndex = 6
        Me.lblAbonosPorComision.Tag = ""
        Me.lblAbonosPorComision.Text = "Abonos por Anticipo:"
        Me.lblAbonosPorComision.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoCXPAbonoPorAnticipo
        '
        Me.lkpConceptoCXPAbonoPorAnticipo.AllowAdd = False
        Me.lkpConceptoCXPAbonoPorAnticipo.AutoReaload = False
        Me.lkpConceptoCXPAbonoPorAnticipo.DataSource = Nothing
        Me.lkpConceptoCXPAbonoPorAnticipo.DefaultSearchField = ""
        Me.lkpConceptoCXPAbonoPorAnticipo.DisplayMember = "descripcion"
        Me.lkpConceptoCXPAbonoPorAnticipo.EditValue = Nothing
        Me.lkpConceptoCXPAbonoPorAnticipo.Filtered = False
        Me.lkpConceptoCXPAbonoPorAnticipo.InitValue = Nothing
        Me.lkpConceptoCXPAbonoPorAnticipo.Location = New System.Drawing.Point(184, 96)
        Me.lkpConceptoCXPAbonoPorAnticipo.MultiSelect = False
        Me.lkpConceptoCXPAbonoPorAnticipo.Name = "lkpConceptoCXPAbonoPorAnticipo"
        Me.lkpConceptoCXPAbonoPorAnticipo.NullText = ""
        Me.lkpConceptoCXPAbonoPorAnticipo.PopupWidth = CType(400, Long)
        Me.lkpConceptoCXPAbonoPorAnticipo.ReadOnlyControl = False
        Me.lkpConceptoCXPAbonoPorAnticipo.Required = False
        Me.lkpConceptoCXPAbonoPorAnticipo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoCXPAbonoPorAnticipo.SearchMember = ""
        Me.lkpConceptoCXPAbonoPorAnticipo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoCXPAbonoPorAnticipo.SelectAll = False
        Me.lkpConceptoCXPAbonoPorAnticipo.Size = New System.Drawing.Size(192, 20)
        Me.lkpConceptoCXPAbonoPorAnticipo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoCXPAbonoPorAnticipo.TabIndex = 7
        Me.lkpConceptoCXPAbonoPorAnticipo.Tag = "concepto_cxp_abono_comision"
        Me.lkpConceptoCXPAbonoPorAnticipo.ToolTip = "Concepto de Abonos por Comisión"
        Me.lkpConceptoCXPAbonoPorAnticipo.ValueMember = "concepto"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(83, 72)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(96, 16)
        Me.Label25.TabIndex = 4
        Me.Label25.Tag = ""
        Me.Label25.Text = "Nota de Crédito:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkplkpConceptoCXPNotaCredito
        '
        Me.lkplkpConceptoCXPNotaCredito.AllowAdd = False
        Me.lkplkpConceptoCXPNotaCredito.AutoReaload = False
        Me.lkplkpConceptoCXPNotaCredito.DataSource = Nothing
        Me.lkplkpConceptoCXPNotaCredito.DefaultSearchField = ""
        Me.lkplkpConceptoCXPNotaCredito.DisplayMember = "descripcion"
        Me.lkplkpConceptoCXPNotaCredito.EditValue = Nothing
        Me.lkplkpConceptoCXPNotaCredito.Filtered = False
        Me.lkplkpConceptoCXPNotaCredito.InitValue = Nothing
        Me.lkplkpConceptoCXPNotaCredito.Location = New System.Drawing.Point(184, 72)
        Me.lkplkpConceptoCXPNotaCredito.MultiSelect = False
        Me.lkplkpConceptoCXPNotaCredito.Name = "lkplkpConceptoCXPNotaCredito"
        Me.lkplkpConceptoCXPNotaCredito.NullText = ""
        Me.lkplkpConceptoCXPNotaCredito.PopupWidth = CType(400, Long)
        Me.lkplkpConceptoCXPNotaCredito.ReadOnlyControl = False
        Me.lkplkpConceptoCXPNotaCredito.Required = False
        Me.lkplkpConceptoCXPNotaCredito.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkplkpConceptoCXPNotaCredito.SearchMember = ""
        Me.lkplkpConceptoCXPNotaCredito.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkplkpConceptoCXPNotaCredito.SelectAll = False
        Me.lkplkpConceptoCXPNotaCredito.Size = New System.Drawing.Size(192, 20)
        Me.lkplkpConceptoCXPNotaCredito.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkplkpConceptoCXPNotaCredito.TabIndex = 5
        Me.lkplkpConceptoCXPNotaCredito.Tag = "concepto_cxp_nota_credito"
        Me.lkplkpConceptoCXPNotaCredito.ToolTip = "Concepto de la Nota de Crédito en CxP"
        Me.lkplkpConceptoCXPNotaCredito.ValueMember = "concepto"
        '
        'lblConceptoCXPPagoVariasFacturas
        '
        Me.lblConceptoCXPPagoVariasFacturas.AutoSize = True
        Me.lblConceptoCXPPagoVariasFacturas.Location = New System.Drawing.Point(44, 48)
        Me.lblConceptoCXPPagoVariasFacturas.Name = "lblConceptoCXPPagoVariasFacturas"
        Me.lblConceptoCXPPagoVariasFacturas.Size = New System.Drawing.Size(135, 16)
        Me.lblConceptoCXPPagoVariasFacturas.TabIndex = 2
        Me.lblConceptoCXPPagoVariasFacturas.Tag = ""
        Me.lblConceptoCXPPagoVariasFacturas.Text = "Pago a varias entradas:"
        Me.lblConceptoCXPPagoVariasFacturas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoCXPPagoVariasFacturas
        '
        Me.lkpConceptoCXPPagoVariasFacturas.AllowAdd = False
        Me.lkpConceptoCXPPagoVariasFacturas.AutoReaload = False
        Me.lkpConceptoCXPPagoVariasFacturas.DataSource = Nothing
        Me.lkpConceptoCXPPagoVariasFacturas.DefaultSearchField = ""
        Me.lkpConceptoCXPPagoVariasFacturas.DisplayMember = "descripcion"
        Me.lkpConceptoCXPPagoVariasFacturas.EditValue = Nothing
        Me.lkpConceptoCXPPagoVariasFacturas.Filtered = False
        Me.lkpConceptoCXPPagoVariasFacturas.InitValue = Nothing
        Me.lkpConceptoCXPPagoVariasFacturas.Location = New System.Drawing.Point(184, 48)
        Me.lkpConceptoCXPPagoVariasFacturas.MultiSelect = False
        Me.lkpConceptoCXPPagoVariasFacturas.Name = "lkpConceptoCXPPagoVariasFacturas"
        Me.lkpConceptoCXPPagoVariasFacturas.NullText = ""
        Me.lkpConceptoCXPPagoVariasFacturas.PopupWidth = CType(400, Long)
        Me.lkpConceptoCXPPagoVariasFacturas.ReadOnlyControl = False
        Me.lkpConceptoCXPPagoVariasFacturas.Required = False
        Me.lkpConceptoCXPPagoVariasFacturas.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoCXPPagoVariasFacturas.SearchMember = ""
        Me.lkpConceptoCXPPagoVariasFacturas.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoCXPPagoVariasFacturas.SelectAll = False
        Me.lkpConceptoCXPPagoVariasFacturas.Size = New System.Drawing.Size(192, 20)
        Me.lkpConceptoCXPPagoVariasFacturas.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoCXPPagoVariasFacturas.TabIndex = 3
        Me.lkpConceptoCXPPagoVariasFacturas.Tag = "concepto_cxp_pago_varias_facturas"
        Me.lkpConceptoCXPPagoVariasFacturas.ToolTip = "Concepto de pago a varias facturas"
        Me.lkpConceptoCXPPagoVariasFacturas.ValueMember = "concepto"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(80, 24)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(99, 16)
        Me.Label23.TabIndex = 0
        Me.Label23.Tag = ""
        Me.Label23.Text = "Nota de Entrada:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoNotaEntrada
        '
        Me.lkpConceptoNotaEntrada.AllowAdd = False
        Me.lkpConceptoNotaEntrada.AutoReaload = False
        Me.lkpConceptoNotaEntrada.DataSource = Nothing
        Me.lkpConceptoNotaEntrada.DefaultSearchField = ""
        Me.lkpConceptoNotaEntrada.DisplayMember = "descripcion"
        Me.lkpConceptoNotaEntrada.EditValue = Nothing
        Me.lkpConceptoNotaEntrada.Filtered = False
        Me.lkpConceptoNotaEntrada.InitValue = Nothing
        Me.lkpConceptoNotaEntrada.Location = New System.Drawing.Point(184, 24)
        Me.lkpConceptoNotaEntrada.MultiSelect = False
        Me.lkpConceptoNotaEntrada.Name = "lkpConceptoNotaEntrada"
        Me.lkpConceptoNotaEntrada.NullText = ""
        Me.lkpConceptoNotaEntrada.PopupWidth = CType(400, Long)
        Me.lkpConceptoNotaEntrada.ReadOnlyControl = False
        Me.lkpConceptoNotaEntrada.Required = False
        Me.lkpConceptoNotaEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoNotaEntrada.SearchMember = ""
        Me.lkpConceptoNotaEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoNotaEntrada.SelectAll = False
        Me.lkpConceptoNotaEntrada.Size = New System.Drawing.Size(192, 20)
        Me.lkpConceptoNotaEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoNotaEntrada.TabIndex = 1
        Me.lkpConceptoNotaEntrada.Tag = "concepto_cxp_nota_entrada"
        Me.lkpConceptoNotaEntrada.ToolTip = "Concepto Nota de Entrada"
        Me.lkpConceptoNotaEntrada.ValueMember = "concepto"
        '
        'tbConceptosCXC
        '
        Me.tbConceptosCXC.BackColor = System.Drawing.Color.White
        Me.tbConceptosCXC.Controls.Add(Me.Label27)
        Me.tbConceptosCXC.Controls.Add(Me.lkpInteresesEspeciales)
        Me.tbConceptosCXC.Controls.Add(Me.Label26)
        Me.tbConceptosCXC.Controls.Add(Me.lkpDescuentosAnticipados)
        Me.tbConceptosCXC.Controls.Add(Me.Label17)
        Me.tbConceptosCXC.Controls.Add(Me.lkpIntereses)
        Me.tbConceptosCXC.Controls.Add(Me.Label13)
        Me.tbConceptosCXC.Controls.Add(Me.lkpAbono)
        Me.tbConceptosCXC.Controls.Add(Me.Label9)
        Me.tbConceptosCXC.Controls.Add(Me.lkpFacturaCredito)
        Me.tbConceptosCXC.Controls.Add(Me.Label10)
        Me.tbConceptosCXC.Controls.Add(Me.lkpFacturaContado)
        Me.tbConceptosCXC.Controls.Add(Me.Label11)
        Me.tbConceptosCXC.Controls.Add(Me.lkpNotaCredito)
        Me.tbConceptosCXC.Controls.Add(Me.Label12)
        Me.tbConceptosCXC.Controls.Add(Me.lkpNotaCargo)
        Me.tbConceptosCXC.Location = New System.Drawing.Point(4, 22)
        Me.tbConceptosCXC.Name = "tbConceptosCXC"
        Me.tbConceptosCXC.Size = New System.Drawing.Size(600, 286)
        Me.tbConceptosCXC.TabIndex = 3
        Me.tbConceptosCXC.Text = "Conceptos de CXC"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(59, 180)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(121, 16)
        Me.Label27.TabIndex = 40
        Me.Label27.Tag = ""
        Me.Label27.Text = "Intereses Espec&iales:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpInteresesEspeciales
        '
        Me.lkpInteresesEspeciales.AllowAdd = False
        Me.lkpInteresesEspeciales.AutoReaload = False
        Me.lkpInteresesEspeciales.DataSource = Nothing
        Me.lkpInteresesEspeciales.DefaultSearchField = ""
        Me.lkpInteresesEspeciales.DisplayMember = "descripcion"
        Me.lkpInteresesEspeciales.EditValue = Nothing
        Me.lkpInteresesEspeciales.Filtered = False
        Me.lkpInteresesEspeciales.InitValue = Nothing
        Me.lkpInteresesEspeciales.Location = New System.Drawing.Point(188, 180)
        Me.lkpInteresesEspeciales.MultiSelect = False
        Me.lkpInteresesEspeciales.Name = "lkpInteresesEspeciales"
        Me.lkpInteresesEspeciales.NullText = ""
        Me.lkpInteresesEspeciales.PopupWidth = CType(400, Long)
        Me.lkpInteresesEspeciales.ReadOnlyControl = False
        Me.lkpInteresesEspeciales.Required = False
        Me.lkpInteresesEspeciales.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpInteresesEspeciales.SearchMember = ""
        Me.lkpInteresesEspeciales.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpInteresesEspeciales.SelectAll = False
        Me.lkpInteresesEspeciales.Size = New System.Drawing.Size(192, 20)
        Me.lkpInteresesEspeciales.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpInteresesEspeciales.TabIndex = 41
        Me.lkpInteresesEspeciales.Tag = "concepto_cxc_intereses_especiales"
        Me.lkpInteresesEspeciales.ToolTip = Nothing
        Me.lkpInteresesEspeciales.ValueMember = "concepto"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(40, 156)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(140, 16)
        Me.Label26.TabIndex = 38
        Me.Label26.Tag = ""
        Me.Label26.Text = "Descuen&tos Anticipados:"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDescuentosAnticipados
        '
        Me.lkpDescuentosAnticipados.AllowAdd = False
        Me.lkpDescuentosAnticipados.AutoReaload = False
        Me.lkpDescuentosAnticipados.DataSource = Nothing
        Me.lkpDescuentosAnticipados.DefaultSearchField = ""
        Me.lkpDescuentosAnticipados.DisplayMember = "descripcion"
        Me.lkpDescuentosAnticipados.EditValue = Nothing
        Me.lkpDescuentosAnticipados.Filtered = False
        Me.lkpDescuentosAnticipados.InitValue = Nothing
        Me.lkpDescuentosAnticipados.Location = New System.Drawing.Point(188, 156)
        Me.lkpDescuentosAnticipados.MultiSelect = False
        Me.lkpDescuentosAnticipados.Name = "lkpDescuentosAnticipados"
        Me.lkpDescuentosAnticipados.NullText = ""
        Me.lkpDescuentosAnticipados.PopupWidth = CType(400, Long)
        Me.lkpDescuentosAnticipados.ReadOnlyControl = False
        Me.lkpDescuentosAnticipados.Required = False
        Me.lkpDescuentosAnticipados.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDescuentosAnticipados.SearchMember = ""
        Me.lkpDescuentosAnticipados.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDescuentosAnticipados.SelectAll = False
        Me.lkpDescuentosAnticipados.Size = New System.Drawing.Size(192, 20)
        Me.lkpDescuentosAnticipados.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDescuentosAnticipados.TabIndex = 39
        Me.lkpDescuentosAnticipados.Tag = "concepto_cxc_descuentos_anticipados"
        Me.lkpDescuentosAnticipados.ToolTip = Nothing
        Me.lkpDescuentosAnticipados.ValueMember = "concepto"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(119, 132)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(61, 16)
        Me.Label17.TabIndex = 36
        Me.Label17.Tag = ""
        Me.Label17.Text = "&Intereses:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpIntereses
        '
        Me.lkpIntereses.AllowAdd = False
        Me.lkpIntereses.AutoReaload = False
        Me.lkpIntereses.DataSource = Nothing
        Me.lkpIntereses.DefaultSearchField = ""
        Me.lkpIntereses.DisplayMember = "descripcion"
        Me.lkpIntereses.EditValue = Nothing
        Me.lkpIntereses.Filtered = False
        Me.lkpIntereses.InitValue = Nothing
        Me.lkpIntereses.Location = New System.Drawing.Point(188, 132)
        Me.lkpIntereses.MultiSelect = False
        Me.lkpIntereses.Name = "lkpIntereses"
        Me.lkpIntereses.NullText = ""
        Me.lkpIntereses.PopupWidth = CType(400, Long)
        Me.lkpIntereses.ReadOnlyControl = False
        Me.lkpIntereses.Required = False
        Me.lkpIntereses.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpIntereses.SearchMember = ""
        Me.lkpIntereses.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpIntereses.SelectAll = False
        Me.lkpIntereses.Size = New System.Drawing.Size(192, 20)
        Me.lkpIntereses.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpIntereses.TabIndex = 37
        Me.lkpIntereses.Tag = "concepto_intereses"
        Me.lkpIntereses.ToolTip = Nothing
        Me.lkpIntereses.ValueMember = "concepto"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(136, 108)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(44, 16)
        Me.Label13.TabIndex = 34
        Me.Label13.Tag = ""
        Me.Label13.Text = "A&bono:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpAbono
        '
        Me.lkpAbono.AllowAdd = False
        Me.lkpAbono.AutoReaload = False
        Me.lkpAbono.DataSource = Nothing
        Me.lkpAbono.DefaultSearchField = ""
        Me.lkpAbono.DisplayMember = "descripcion"
        Me.lkpAbono.EditValue = Nothing
        Me.lkpAbono.Filtered = False
        Me.lkpAbono.InitValue = Nothing
        Me.lkpAbono.Location = New System.Drawing.Point(188, 108)
        Me.lkpAbono.MultiSelect = False
        Me.lkpAbono.Name = "lkpAbono"
        Me.lkpAbono.NullText = ""
        Me.lkpAbono.PopupWidth = CType(400, Long)
        Me.lkpAbono.ReadOnlyControl = False
        Me.lkpAbono.Required = False
        Me.lkpAbono.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpAbono.SearchMember = ""
        Me.lkpAbono.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpAbono.SelectAll = False
        Me.lkpAbono.Size = New System.Drawing.Size(192, 20)
        Me.lkpAbono.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpAbono.TabIndex = 35
        Me.lkpAbono.Tag = "concepto_cxc_abono"
        Me.lkpAbono.ToolTip = Nothing
        Me.lkpAbono.ValueMember = "concepto"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(69, 12)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(111, 16)
        Me.Label9.TabIndex = 26
        Me.Label9.Tag = ""
        Me.Label9.Text = "&Factura de Crédito:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFacturaCredito
        '
        Me.lkpFacturaCredito.AllowAdd = False
        Me.lkpFacturaCredito.AutoReaload = False
        Me.lkpFacturaCredito.DataSource = Nothing
        Me.lkpFacturaCredito.DefaultSearchField = ""
        Me.lkpFacturaCredito.DisplayMember = "descripcion"
        Me.lkpFacturaCredito.EditValue = Nothing
        Me.lkpFacturaCredito.Filtered = False
        Me.lkpFacturaCredito.InitValue = Nothing
        Me.lkpFacturaCredito.Location = New System.Drawing.Point(188, 12)
        Me.lkpFacturaCredito.MultiSelect = False
        Me.lkpFacturaCredito.Name = "lkpFacturaCredito"
        Me.lkpFacturaCredito.NullText = ""
        Me.lkpFacturaCredito.PopupWidth = CType(400, Long)
        Me.lkpFacturaCredito.ReadOnlyControl = False
        Me.lkpFacturaCredito.Required = False
        Me.lkpFacturaCredito.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFacturaCredito.SearchMember = ""
        Me.lkpFacturaCredito.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFacturaCredito.SelectAll = False
        Me.lkpFacturaCredito.Size = New System.Drawing.Size(192, 20)
        Me.lkpFacturaCredito.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFacturaCredito.TabIndex = 27
        Me.lkpFacturaCredito.Tag = "concepto_cxc_factura_credito"
        Me.lkpFacturaCredito.ToolTip = Nothing
        Me.lkpFacturaCredito.ValueMember = "concepto"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(63, 36)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(117, 16)
        Me.Label10.TabIndex = 28
        Me.Label10.Tag = ""
        Me.Label10.Text = "Fac&tura de Contado:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFacturaContado
        '
        Me.lkpFacturaContado.AllowAdd = False
        Me.lkpFacturaContado.AutoReaload = False
        Me.lkpFacturaContado.DataSource = Nothing
        Me.lkpFacturaContado.DefaultSearchField = ""
        Me.lkpFacturaContado.DisplayMember = "descripcion"
        Me.lkpFacturaContado.EditValue = Nothing
        Me.lkpFacturaContado.Filtered = False
        Me.lkpFacturaContado.InitValue = Nothing
        Me.lkpFacturaContado.Location = New System.Drawing.Point(188, 36)
        Me.lkpFacturaContado.MultiSelect = False
        Me.lkpFacturaContado.Name = "lkpFacturaContado"
        Me.lkpFacturaContado.NullText = ""
        Me.lkpFacturaContado.PopupWidth = CType(400, Long)
        Me.lkpFacturaContado.ReadOnlyControl = False
        Me.lkpFacturaContado.Required = False
        Me.lkpFacturaContado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFacturaContado.SearchMember = ""
        Me.lkpFacturaContado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFacturaContado.SelectAll = False
        Me.lkpFacturaContado.Size = New System.Drawing.Size(192, 20)
        Me.lkpFacturaContado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFacturaContado.TabIndex = 29
        Me.lkpFacturaContado.Tag = "concepto_cxc_factura_contado"
        Me.lkpFacturaContado.ToolTip = Nothing
        Me.lkpFacturaContado.ValueMember = "concepto"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(92, 60)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 16)
        Me.Label11.TabIndex = 30
        Me.Label11.Tag = ""
        Me.Label11.Text = "&Nota de Cargo:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpNotaCredito
        '
        Me.lkpNotaCredito.AllowAdd = False
        Me.lkpNotaCredito.AutoReaload = False
        Me.lkpNotaCredito.DataSource = Nothing
        Me.lkpNotaCredito.DefaultSearchField = ""
        Me.lkpNotaCredito.DisplayMember = "descripcion"
        Me.lkpNotaCredito.EditValue = Nothing
        Me.lkpNotaCredito.Filtered = False
        Me.lkpNotaCredito.InitValue = Nothing
        Me.lkpNotaCredito.Location = New System.Drawing.Point(188, 84)
        Me.lkpNotaCredito.MultiSelect = False
        Me.lkpNotaCredito.Name = "lkpNotaCredito"
        Me.lkpNotaCredito.NullText = ""
        Me.lkpNotaCredito.PopupWidth = CType(400, Long)
        Me.lkpNotaCredito.ReadOnlyControl = False
        Me.lkpNotaCredito.Required = False
        Me.lkpNotaCredito.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpNotaCredito.SearchMember = ""
        Me.lkpNotaCredito.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpNotaCredito.SelectAll = False
        Me.lkpNotaCredito.Size = New System.Drawing.Size(192, 20)
        Me.lkpNotaCredito.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpNotaCredito.TabIndex = 33
        Me.lkpNotaCredito.Tag = "concepto_cxc_nota_credito"
        Me.lkpNotaCredito.ToolTip = Nothing
        Me.lkpNotaCredito.ValueMember = "concepto"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(84, 84)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 16)
        Me.Label12.TabIndex = 32
        Me.Label12.Tag = ""
        Me.Label12.Text = "N&ota de Crédito:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpNotaCargo
        '
        Me.lkpNotaCargo.AllowAdd = False
        Me.lkpNotaCargo.AutoReaload = False
        Me.lkpNotaCargo.DataSource = Nothing
        Me.lkpNotaCargo.DefaultSearchField = ""
        Me.lkpNotaCargo.DisplayMember = "descripcion"
        Me.lkpNotaCargo.EditValue = Nothing
        Me.lkpNotaCargo.Filtered = False
        Me.lkpNotaCargo.InitValue = Nothing
        Me.lkpNotaCargo.Location = New System.Drawing.Point(188, 60)
        Me.lkpNotaCargo.MultiSelect = False
        Me.lkpNotaCargo.Name = "lkpNotaCargo"
        Me.lkpNotaCargo.NullText = ""
        Me.lkpNotaCargo.PopupWidth = CType(400, Long)
        Me.lkpNotaCargo.ReadOnlyControl = False
        Me.lkpNotaCargo.Required = False
        Me.lkpNotaCargo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpNotaCargo.SearchMember = ""
        Me.lkpNotaCargo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpNotaCargo.SelectAll = False
        Me.lkpNotaCargo.Size = New System.Drawing.Size(192, 20)
        Me.lkpNotaCargo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpNotaCargo.TabIndex = 31
        Me.lkpNotaCargo.Tag = "concepto_cxc_nota_cargo"
        Me.lkpNotaCargo.ToolTip = Nothing
        Me.lkpNotaCargo.ValueMember = "concepto"
        '
        'tbConceptosInventario
        '
        Me.tbConceptosInventario.BackColor = System.Drawing.Color.White
        Me.tbConceptosInventario.Controls.Add(Me.Label42)
        Me.tbConceptosInventario.Controls.Add(Me.lkpEntradaReparacion)
        Me.tbConceptosInventario.Controls.Add(Me.Label43)
        Me.tbConceptosInventario.Controls.Add(Me.lkpSalidaReparacion)
        Me.tbConceptosInventario.Controls.Add(Me.Label38)
        Me.tbConceptosInventario.Controls.Add(Me.lkpVistasEntrada)
        Me.tbConceptosInventario.Controls.Add(Me.Label37)
        Me.tbConceptosInventario.Controls.Add(Me.lkpVistasSalida)
        Me.tbConceptosInventario.Controls.Add(Me.lblConceptoCompra)
        Me.tbConceptosInventario.Controls.Add(Me.lkpCompra)
        Me.tbConceptosInventario.Controls.Add(Me.Label2)
        Me.tbConceptosInventario.Controls.Add(Me.lkpCancelacionCompra)
        Me.tbConceptosInventario.Controls.Add(Me.Label3)
        Me.tbConceptosInventario.Controls.Add(Me.lkpCancelacionVenta)
        Me.tbConceptosInventario.Controls.Add(Me.Label4)
        Me.tbConceptosInventario.Controls.Add(Me.lkpVenta)
        Me.tbConceptosInventario.Controls.Add(Me.Label5)
        Me.tbConceptosInventario.Controls.Add(Me.Label6)
        Me.tbConceptosInventario.Controls.Add(Me.lkpCancelacionDevolucionesProveedor)
        Me.tbConceptosInventario.Controls.Add(Me.lkpDevolucionesProveedor)
        Me.tbConceptosInventario.Controls.Add(Me.Label7)
        Me.tbConceptosInventario.Controls.Add(Me.lkpCancelacionTraspaso)
        Me.tbConceptosInventario.Location = New System.Drawing.Point(4, 22)
        Me.tbConceptosInventario.Name = "tbConceptosInventario"
        Me.tbConceptosInventario.Size = New System.Drawing.Size(600, 286)
        Me.tbConceptosInventario.TabIndex = 1
        Me.tbConceptosInventario.Text = "Conceptos de Inventario"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(72, 256)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(139, 16)
        Me.Label42.TabIndex = 62
        Me.Label42.Tag = ""
        Me.Label42.Text = "Entrada por Reparación:"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpEntradaReparacion
        '
        Me.lkpEntradaReparacion.AllowAdd = False
        Me.lkpEntradaReparacion.AutoReaload = False
        Me.lkpEntradaReparacion.DataSource = Nothing
        Me.lkpEntradaReparacion.DefaultSearchField = ""
        Me.lkpEntradaReparacion.DisplayMember = "descripcion"
        Me.lkpEntradaReparacion.EditValue = Nothing
        Me.lkpEntradaReparacion.Filtered = False
        Me.lkpEntradaReparacion.InitValue = Nothing
        Me.lkpEntradaReparacion.Location = New System.Drawing.Point(216, 256)
        Me.lkpEntradaReparacion.MultiSelect = False
        Me.lkpEntradaReparacion.Name = "lkpEntradaReparacion"
        Me.lkpEntradaReparacion.NullText = ""
        Me.lkpEntradaReparacion.PopupWidth = CType(400, Long)
        Me.lkpEntradaReparacion.ReadOnlyControl = False
        Me.lkpEntradaReparacion.Required = False
        Me.lkpEntradaReparacion.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEntradaReparacion.SearchMember = ""
        Me.lkpEntradaReparacion.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEntradaReparacion.SelectAll = False
        Me.lkpEntradaReparacion.Size = New System.Drawing.Size(264, 20)
        Me.lkpEntradaReparacion.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEntradaReparacion.TabIndex = 63
        Me.lkpEntradaReparacion.Tag = "concepto_entrada_reparacion"
        Me.lkpEntradaReparacion.ToolTip = Nothing
        Me.lkpEntradaReparacion.ValueMember = "concepto"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(78, 232)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(134, 16)
        Me.Label43.TabIndex = 60
        Me.Label43.Tag = ""
        Me.Label43.Text = "Salida por Reparación :"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSalidaReparacion
        '
        Me.lkpSalidaReparacion.AllowAdd = False
        Me.lkpSalidaReparacion.AutoReaload = False
        Me.lkpSalidaReparacion.DataSource = Nothing
        Me.lkpSalidaReparacion.DefaultSearchField = ""
        Me.lkpSalidaReparacion.DisplayMember = "descripcion"
        Me.lkpSalidaReparacion.EditValue = Nothing
        Me.lkpSalidaReparacion.Filtered = False
        Me.lkpSalidaReparacion.InitValue = Nothing
        Me.lkpSalidaReparacion.Location = New System.Drawing.Point(216, 232)
        Me.lkpSalidaReparacion.MultiSelect = False
        Me.lkpSalidaReparacion.Name = "lkpSalidaReparacion"
        Me.lkpSalidaReparacion.NullText = ""
        Me.lkpSalidaReparacion.PopupWidth = CType(400, Long)
        Me.lkpSalidaReparacion.ReadOnlyControl = False
        Me.lkpSalidaReparacion.Required = False
        Me.lkpSalidaReparacion.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSalidaReparacion.SearchMember = ""
        Me.lkpSalidaReparacion.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSalidaReparacion.SelectAll = False
        Me.lkpSalidaReparacion.Size = New System.Drawing.Size(264, 20)
        Me.lkpSalidaReparacion.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSalidaReparacion.TabIndex = 61
        Me.lkpSalidaReparacion.Tag = "concepto_salida_reparacion"
        Me.lkpSalidaReparacion.ToolTip = Nothing
        Me.lkpSalidaReparacion.ValueMember = "concepto"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(107, 208)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(105, 16)
        Me.Label38.TabIndex = 58
        Me.Label38.Tag = ""
        Me.Label38.Text = "Entrada por Vista:"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpVistasEntrada
        '
        Me.lkpVistasEntrada.AllowAdd = False
        Me.lkpVistasEntrada.AutoReaload = False
        Me.lkpVistasEntrada.DataSource = Nothing
        Me.lkpVistasEntrada.DefaultSearchField = ""
        Me.lkpVistasEntrada.DisplayMember = "descripcion"
        Me.lkpVistasEntrada.EditValue = Nothing
        Me.lkpVistasEntrada.Filtered = False
        Me.lkpVistasEntrada.InitValue = Nothing
        Me.lkpVistasEntrada.Location = New System.Drawing.Point(216, 208)
        Me.lkpVistasEntrada.MultiSelect = False
        Me.lkpVistasEntrada.Name = "lkpVistasEntrada"
        Me.lkpVistasEntrada.NullText = ""
        Me.lkpVistasEntrada.PopupWidth = CType(400, Long)
        Me.lkpVistasEntrada.ReadOnlyControl = False
        Me.lkpVistasEntrada.Required = False
        Me.lkpVistasEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpVistasEntrada.SearchMember = ""
        Me.lkpVistasEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpVistasEntrada.SelectAll = False
        Me.lkpVistasEntrada.Size = New System.Drawing.Size(264, 20)
        Me.lkpVistasEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpVistasEntrada.TabIndex = 59
        Me.lkpVistasEntrada.Tag = "concepto_vistas_entrada"
        Me.lkpVistasEntrada.ToolTip = Nothing
        Me.lkpVistasEntrada.ValueMember = "concepto"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(112, 184)
        Me.Label37.Name = "Label37"
        Me.Label37.TabIndex = 56
        Me.Label37.Tag = ""
        Me.Label37.Text = "Salida por Vista :"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpVistasSalida
        '
        Me.lkpVistasSalida.AllowAdd = False
        Me.lkpVistasSalida.AutoReaload = False
        Me.lkpVistasSalida.DataSource = Nothing
        Me.lkpVistasSalida.DefaultSearchField = ""
        Me.lkpVistasSalida.DisplayMember = "descripcion"
        Me.lkpVistasSalida.EditValue = Nothing
        Me.lkpVistasSalida.Filtered = False
        Me.lkpVistasSalida.InitValue = Nothing
        Me.lkpVistasSalida.Location = New System.Drawing.Point(216, 184)
        Me.lkpVistasSalida.MultiSelect = False
        Me.lkpVistasSalida.Name = "lkpVistasSalida"
        Me.lkpVistasSalida.NullText = ""
        Me.lkpVistasSalida.PopupWidth = CType(400, Long)
        Me.lkpVistasSalida.ReadOnlyControl = False
        Me.lkpVistasSalida.Required = False
        Me.lkpVistasSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpVistasSalida.SearchMember = ""
        Me.lkpVistasSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpVistasSalida.SelectAll = False
        Me.lkpVistasSalida.Size = New System.Drawing.Size(264, 20)
        Me.lkpVistasSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpVistasSalida.TabIndex = 57
        Me.lkpVistasSalida.Tag = "concepto_vistas_salida"
        Me.lkpVistasSalida.ToolTip = Nothing
        Me.lkpVistasSalida.ValueMember = "concepto"
        '
        'lblConceptoCompra
        '
        Me.lblConceptoCompra.AutoSize = True
        Me.lblConceptoCompra.Location = New System.Drawing.Point(160, 16)
        Me.lblConceptoCompra.Name = "lblConceptoCompra"
        Me.lblConceptoCompra.Size = New System.Drawing.Size(52, 16)
        Me.lblConceptoCompra.TabIndex = 42
        Me.lblConceptoCompra.Tag = ""
        Me.lblConceptoCompra.Text = "Compra:"
        Me.lblConceptoCompra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCompra
        '
        Me.lkpCompra.AllowAdd = False
        Me.lkpCompra.AutoReaload = False
        Me.lkpCompra.DataSource = Nothing
        Me.lkpCompra.DefaultSearchField = ""
        Me.lkpCompra.DisplayMember = "descripcion"
        Me.lkpCompra.EditValue = Nothing
        Me.lkpCompra.Filtered = False
        Me.lkpCompra.InitValue = Nothing
        Me.lkpCompra.Location = New System.Drawing.Point(216, 16)
        Me.lkpCompra.MultiSelect = False
        Me.lkpCompra.Name = "lkpCompra"
        Me.lkpCompra.NullText = ""
        Me.lkpCompra.PopupWidth = CType(400, Long)
        Me.lkpCompra.ReadOnlyControl = False
        Me.lkpCompra.Required = False
        Me.lkpCompra.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCompra.SearchMember = ""
        Me.lkpCompra.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCompra.SelectAll = False
        Me.lkpCompra.Size = New System.Drawing.Size(264, 20)
        Me.lkpCompra.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCompra.TabIndex = 43
        Me.lkpCompra.Tag = "concepto_compra"
        Me.lkpCompra.ToolTip = Nothing
        Me.lkpCompra.ValueMember = "concepto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(73, 160)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(139, 16)
        Me.Label2.TabIndex = 54
        Me.Label2.Tag = ""
        Me.Label2.Text = "Cancelación de Compra:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCancelacionCompra
        '
        Me.lkpCancelacionCompra.AllowAdd = False
        Me.lkpCancelacionCompra.AutoReaload = False
        Me.lkpCancelacionCompra.DataSource = Nothing
        Me.lkpCancelacionCompra.DefaultSearchField = ""
        Me.lkpCancelacionCompra.DisplayMember = "descripcion"
        Me.lkpCancelacionCompra.EditValue = Nothing
        Me.lkpCancelacionCompra.Filtered = False
        Me.lkpCancelacionCompra.InitValue = Nothing
        Me.lkpCancelacionCompra.Location = New System.Drawing.Point(216, 160)
        Me.lkpCancelacionCompra.MultiSelect = False
        Me.lkpCancelacionCompra.Name = "lkpCancelacionCompra"
        Me.lkpCancelacionCompra.NullText = ""
        Me.lkpCancelacionCompra.PopupWidth = CType(400, Long)
        Me.lkpCancelacionCompra.ReadOnlyControl = False
        Me.lkpCancelacionCompra.Required = False
        Me.lkpCancelacionCompra.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCancelacionCompra.SearchMember = ""
        Me.lkpCancelacionCompra.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCancelacionCompra.SelectAll = False
        Me.lkpCancelacionCompra.Size = New System.Drawing.Size(264, 20)
        Me.lkpCancelacionCompra.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCancelacionCompra.TabIndex = 55
        Me.lkpCancelacionCompra.Tag = "concepto_cancelacion_compra"
        Me.lkpCancelacionCompra.ToolTip = Nothing
        Me.lkpCancelacionCompra.ValueMember = "concepto"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(171, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 16)
        Me.Label3.TabIndex = 44
        Me.Label3.Tag = ""
        Me.Label3.Text = "Venta:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCancelacionVenta
        '
        Me.lkpCancelacionVenta.AllowAdd = False
        Me.lkpCancelacionVenta.AutoReaload = False
        Me.lkpCancelacionVenta.DataSource = Nothing
        Me.lkpCancelacionVenta.DefaultSearchField = ""
        Me.lkpCancelacionVenta.DisplayMember = "descripcion"
        Me.lkpCancelacionVenta.EditValue = Nothing
        Me.lkpCancelacionVenta.Filtered = False
        Me.lkpCancelacionVenta.InitValue = Nothing
        Me.lkpCancelacionVenta.Location = New System.Drawing.Point(216, 64)
        Me.lkpCancelacionVenta.MultiSelect = False
        Me.lkpCancelacionVenta.Name = "lkpCancelacionVenta"
        Me.lkpCancelacionVenta.NullText = ""
        Me.lkpCancelacionVenta.PopupWidth = CType(400, Long)
        Me.lkpCancelacionVenta.ReadOnlyControl = False
        Me.lkpCancelacionVenta.Required = False
        Me.lkpCancelacionVenta.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCancelacionVenta.SearchMember = ""
        Me.lkpCancelacionVenta.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCancelacionVenta.SelectAll = False
        Me.lkpCancelacionVenta.Size = New System.Drawing.Size(264, 20)
        Me.lkpCancelacionVenta.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCancelacionVenta.TabIndex = 47
        Me.lkpCancelacionVenta.Tag = "concepto_cancelacion_venta"
        Me.lkpCancelacionVenta.ToolTip = Nothing
        Me.lkpCancelacionVenta.ValueMember = "concepto"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(84, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(128, 16)
        Me.Label4.TabIndex = 46
        Me.Label4.Tag = ""
        Me.Label4.Text = "Cancelación de Venta:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpVenta
        '
        Me.lkpVenta.AllowAdd = False
        Me.lkpVenta.AutoReaload = False
        Me.lkpVenta.DataSource = Nothing
        Me.lkpVenta.DefaultSearchField = ""
        Me.lkpVenta.DisplayMember = "descripcion"
        Me.lkpVenta.EditValue = Nothing
        Me.lkpVenta.Filtered = False
        Me.lkpVenta.InitValue = Nothing
        Me.lkpVenta.Location = New System.Drawing.Point(216, 40)
        Me.lkpVenta.MultiSelect = False
        Me.lkpVenta.Name = "lkpVenta"
        Me.lkpVenta.NullText = ""
        Me.lkpVenta.PopupWidth = CType(400, Long)
        Me.lkpVenta.ReadOnlyControl = False
        Me.lkpVenta.Required = False
        Me.lkpVenta.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpVenta.SearchMember = ""
        Me.lkpVenta.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpVenta.SelectAll = False
        Me.lkpVenta.Size = New System.Drawing.Size(264, 20)
        Me.lkpVenta.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpVenta.TabIndex = 45
        Me.lkpVenta.Tag = "concepto_venta"
        Me.lkpVenta.ToolTip = Nothing
        Me.lkpVenta.ValueMember = "concepto"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(130, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 16)
        Me.Label5.TabIndex = 48
        Me.Label5.Tag = ""
        Me.Label5.Text = "Devoluciones:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(44, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(168, 16)
        Me.Label6.TabIndex = 50
        Me.Label6.Tag = ""
        Me.Label6.Text = "Cancelación de Devoluciones:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCancelacionDevolucionesProveedor
        '
        Me.lkpCancelacionDevolucionesProveedor.AllowAdd = False
        Me.lkpCancelacionDevolucionesProveedor.AutoReaload = False
        Me.lkpCancelacionDevolucionesProveedor.DataSource = Nothing
        Me.lkpCancelacionDevolucionesProveedor.DefaultSearchField = ""
        Me.lkpCancelacionDevolucionesProveedor.DisplayMember = "descripcion"
        Me.lkpCancelacionDevolucionesProveedor.EditValue = Nothing
        Me.lkpCancelacionDevolucionesProveedor.Filtered = False
        Me.lkpCancelacionDevolucionesProveedor.InitValue = Nothing
        Me.lkpCancelacionDevolucionesProveedor.Location = New System.Drawing.Point(216, 112)
        Me.lkpCancelacionDevolucionesProveedor.MultiSelect = False
        Me.lkpCancelacionDevolucionesProveedor.Name = "lkpCancelacionDevolucionesProveedor"
        Me.lkpCancelacionDevolucionesProveedor.NullText = ""
        Me.lkpCancelacionDevolucionesProveedor.PopupWidth = CType(400, Long)
        Me.lkpCancelacionDevolucionesProveedor.ReadOnlyControl = False
        Me.lkpCancelacionDevolucionesProveedor.Required = False
        Me.lkpCancelacionDevolucionesProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCancelacionDevolucionesProveedor.SearchMember = ""
        Me.lkpCancelacionDevolucionesProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCancelacionDevolucionesProveedor.SelectAll = False
        Me.lkpCancelacionDevolucionesProveedor.Size = New System.Drawing.Size(264, 20)
        Me.lkpCancelacionDevolucionesProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCancelacionDevolucionesProveedor.TabIndex = 51
        Me.lkpCancelacionDevolucionesProveedor.Tag = "concepto_cancelacion_devoluciones"
        Me.lkpCancelacionDevolucionesProveedor.ToolTip = Nothing
        Me.lkpCancelacionDevolucionesProveedor.ValueMember = "concepto"
        '
        'lkpDevolucionesProveedor
        '
        Me.lkpDevolucionesProveedor.AllowAdd = False
        Me.lkpDevolucionesProveedor.AutoReaload = False
        Me.lkpDevolucionesProveedor.DataSource = Nothing
        Me.lkpDevolucionesProveedor.DefaultSearchField = ""
        Me.lkpDevolucionesProveedor.DisplayMember = "descripcion"
        Me.lkpDevolucionesProveedor.EditValue = Nothing
        Me.lkpDevolucionesProveedor.Filtered = False
        Me.lkpDevolucionesProveedor.InitValue = Nothing
        Me.lkpDevolucionesProveedor.Location = New System.Drawing.Point(216, 88)
        Me.lkpDevolucionesProveedor.MultiSelect = False
        Me.lkpDevolucionesProveedor.Name = "lkpDevolucionesProveedor"
        Me.lkpDevolucionesProveedor.NullText = ""
        Me.lkpDevolucionesProveedor.PopupWidth = CType(400, Long)
        Me.lkpDevolucionesProveedor.ReadOnlyControl = False
        Me.lkpDevolucionesProveedor.Required = False
        Me.lkpDevolucionesProveedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDevolucionesProveedor.SearchMember = ""
        Me.lkpDevolucionesProveedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDevolucionesProveedor.SelectAll = False
        Me.lkpDevolucionesProveedor.Size = New System.Drawing.Size(264, 20)
        Me.lkpDevolucionesProveedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDevolucionesProveedor.TabIndex = 49
        Me.lkpDevolucionesProveedor.Tag = "concepto_devoluciones"
        Me.lkpDevolucionesProveedor.ToolTip = Nothing
        Me.lkpDevolucionesProveedor.ValueMember = "concepto"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 136)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(200, 16)
        Me.Label7.TabIndex = 52
        Me.Label7.Tag = ""
        Me.Label7.Text = "Cancelación de Traspaso de Salida:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCancelacionTraspaso
        '
        Me.lkpCancelacionTraspaso.AllowAdd = False
        Me.lkpCancelacionTraspaso.AutoReaload = False
        Me.lkpCancelacionTraspaso.DataSource = Nothing
        Me.lkpCancelacionTraspaso.DefaultSearchField = ""
        Me.lkpCancelacionTraspaso.DisplayMember = "descripcion"
        Me.lkpCancelacionTraspaso.EditValue = Nothing
        Me.lkpCancelacionTraspaso.Filtered = False
        Me.lkpCancelacionTraspaso.InitValue = Nothing
        Me.lkpCancelacionTraspaso.Location = New System.Drawing.Point(216, 136)
        Me.lkpCancelacionTraspaso.MultiSelect = False
        Me.lkpCancelacionTraspaso.Name = "lkpCancelacionTraspaso"
        Me.lkpCancelacionTraspaso.NullText = ""
        Me.lkpCancelacionTraspaso.PopupWidth = CType(400, Long)
        Me.lkpCancelacionTraspaso.ReadOnlyControl = False
        Me.lkpCancelacionTraspaso.Required = False
        Me.lkpCancelacionTraspaso.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCancelacionTraspaso.SearchMember = ""
        Me.lkpCancelacionTraspaso.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCancelacionTraspaso.SelectAll = False
        Me.lkpCancelacionTraspaso.Size = New System.Drawing.Size(264, 20)
        Me.lkpCancelacionTraspaso.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCancelacionTraspaso.TabIndex = 53
        Me.lkpCancelacionTraspaso.Tag = "concepto_cancelacion_traspaso"
        Me.lkpCancelacionTraspaso.ToolTip = Nothing
        Me.lkpCancelacionTraspaso.ValueMember = "concepto"
        '
        'tbPrecios
        '
        Me.tbPrecios.BackColor = System.Drawing.Color.White
        Me.tbPrecios.Controls.Add(Me.Label48)
        Me.tbPrecios.Controls.Add(Me.lkpPrecioListaConvenio)
        Me.tbPrecios.Controls.Add(Me.GroupBox1)
        Me.tbPrecios.Controls.Add(Me.clcEngancheEnMenos)
        Me.tbPrecios.Controls.Add(Me.Label28)
        Me.tbPrecios.Controls.Add(Me.Label8)
        Me.tbPrecios.Controls.Add(Me.lkpPrecioExpo)
        Me.tbPrecios.Controls.Add(Me.lkpPrecioContado)
        Me.tbPrecios.Controls.Add(Me.Label18)
        Me.tbPrecios.Location = New System.Drawing.Point(4, 22)
        Me.tbPrecios.Name = "tbPrecios"
        Me.tbPrecios.Size = New System.Drawing.Size(600, 286)
        Me.tbPrecios.TabIndex = 2
        Me.tbPrecios.Text = "Precios"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.speCierreComisionesCobradoresMinutos)
        Me.GroupBox1.Controls.Add(Me.speInicioComisionesCobradoresMinutos)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.Label29)
        Me.GroupBox1.Controls.Add(Me.speInicioComisionesCobradoresHoras)
        Me.GroupBox1.Controls.Add(Me.lblHoraFinComisionesCobrador)
        Me.GroupBox1.Controls.Add(Me.speCierreComisionesCobradoresHoras)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.Label32)
        Me.GroupBox1.Controls.Add(Me.lblHoraInicioComisionesCobrador)
        Me.GroupBox1.Location = New System.Drawing.Point(42, 128)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(516, 80)
        Me.GroupBox1.TabIndex = 83
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Horario Comisiones a Cobradores"
        '
        'speCierreComisionesCobradoresMinutos
        '
        Me.speCierreComisionesCobradoresMinutos.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.speCierreComisionesCobradoresMinutos.Location = New System.Drawing.Point(313, 48)
        Me.speCierreComisionesCobradoresMinutos.Name = "speCierreComisionesCobradoresMinutos"
        '
        'speCierreComisionesCobradoresMinutos.Properties
        '
        Me.speCierreComisionesCobradoresMinutos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.speCierreComisionesCobradoresMinutos.Properties.MaxValue = New Decimal(New Integer() {59, 0, 0, 0})
        Me.speCierreComisionesCobradoresMinutos.Properties.UseCtrlIncrement = False
        Me.speCierreComisionesCobradoresMinutos.Size = New System.Drawing.Size(48, 20)
        Me.speCierreComisionesCobradoresMinutos.TabIndex = 78
        '
        'speInicioComisionesCobradoresMinutos
        '
        Me.speInicioComisionesCobradoresMinutos.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.speInicioComisionesCobradoresMinutos.Location = New System.Drawing.Point(313, 24)
        Me.speInicioComisionesCobradoresMinutos.Name = "speInicioComisionesCobradoresMinutos"
        '
        'speInicioComisionesCobradoresMinutos.Properties
        '
        Me.speInicioComisionesCobradoresMinutos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.speInicioComisionesCobradoresMinutos.Properties.MaxValue = New Decimal(New Integer() {59, 0, 0, 0})
        Me.speInicioComisionesCobradoresMinutos.Properties.UseCtrlIncrement = False
        Me.speInicioComisionesCobradoresMinutos.Size = New System.Drawing.Size(48, 20)
        Me.speInicioComisionesCobradoresMinutos.TabIndex = 76
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(363, 26)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(47, 16)
        Me.Label31.TabIndex = 81
        Me.Label31.Tag = ""
        Me.Label31.Text = "&Minutos"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(254, 26)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(36, 16)
        Me.Label29.TabIndex = 79
        Me.Label29.Tag = ""
        Me.Label29.Text = "&Horas"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'speInicioComisionesCobradoresHoras
        '
        Me.speInicioComisionesCobradoresHoras.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.speInicioComisionesCobradoresHoras.Location = New System.Drawing.Point(202, 24)
        Me.speInicioComisionesCobradoresHoras.Name = "speInicioComisionesCobradoresHoras"
        '
        'speInicioComisionesCobradoresHoras.Properties
        '
        Me.speInicioComisionesCobradoresHoras.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.speInicioComisionesCobradoresHoras.Properties.MaxValue = New Decimal(New Integer() {23, 0, 0, 0})
        Me.speInicioComisionesCobradoresHoras.Properties.UseCtrlIncrement = False
        Me.speInicioComisionesCobradoresHoras.Size = New System.Drawing.Size(48, 20)
        Me.speInicioComisionesCobradoresHoras.TabIndex = 75
        '
        'lblHoraFinComisionesCobrador
        '
        Me.lblHoraFinComisionesCobrador.AutoSize = True
        Me.lblHoraFinComisionesCobrador.Location = New System.Drawing.Point(108, 50)
        Me.lblHoraFinComisionesCobrador.Name = "lblHoraFinComisionesCobrador"
        Me.lblHoraFinComisionesCobrador.Size = New System.Drawing.Size(90, 16)
        Me.lblHoraFinComisionesCobrador.TabIndex = 70
        Me.lblHoraFinComisionesCobrador.Tag = ""
        Me.lblHoraFinComisionesCobrador.Text = "H&ora de Cierre:"
        Me.lblHoraFinComisionesCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'speCierreComisionesCobradoresHoras
        '
        Me.speCierreComisionesCobradoresHoras.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.speCierreComisionesCobradoresHoras.Location = New System.Drawing.Point(202, 48)
        Me.speCierreComisionesCobradoresHoras.Name = "speCierreComisionesCobradoresHoras"
        '
        'speCierreComisionesCobradoresHoras.Properties
        '
        Me.speCierreComisionesCobradoresHoras.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.speCierreComisionesCobradoresHoras.Properties.MaxValue = New Decimal(New Integer() {23, 0, 0, 0})
        Me.speCierreComisionesCobradoresHoras.Properties.UseCtrlIncrement = False
        Me.speCierreComisionesCobradoresHoras.Size = New System.Drawing.Size(48, 20)
        Me.speCierreComisionesCobradoresHoras.TabIndex = 77
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(254, 50)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(36, 16)
        Me.Label30.TabIndex = 80
        Me.Label30.Tag = ""
        Me.Label30.Text = "&Horas"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(363, 50)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(47, 16)
        Me.Label32.TabIndex = 82
        Me.Label32.Tag = ""
        Me.Label32.Text = "&Minutos"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHoraInicioComisionesCobrador
        '
        Me.lblHoraInicioComisionesCobrador.AutoSize = True
        Me.lblHoraInicioComisionesCobrador.Location = New System.Drawing.Point(112, 26)
        Me.lblHoraInicioComisionesCobrador.Name = "lblHoraInicioComisionesCobrador"
        Me.lblHoraInicioComisionesCobrador.Size = New System.Drawing.Size(86, 16)
        Me.lblHoraInicioComisionesCobrador.TabIndex = 66
        Me.lblHoraInicioComisionesCobrador.Tag = ""
        Me.lblHoraInicioComisionesCobrador.Text = "&Hora de Inicio:"
        Me.lblHoraInicioComisionesCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEngancheEnMenos
        '
        Me.clcEngancheEnMenos.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcEngancheEnMenos.Location = New System.Drawing.Point(272, 96)
        Me.clcEngancheEnMenos.Name = "clcEngancheEnMenos"
        '
        'clcEngancheEnMenos.Properties
        '
        Me.clcEngancheEnMenos.Properties.MaxLength = 5
        Me.clcEngancheEnMenos.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcEngancheEnMenos.Size = New System.Drawing.Size(75, 20)
        Me.clcEngancheEnMenos.TabIndex = 72
        Me.clcEngancheEnMenos.Tag = "enganche_en_menos"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(148, 96)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(119, 16)
        Me.Label28.TabIndex = 71
        Me.Label28.Tag = ""
        Me.Label28.Text = "&Enganche en Menos:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(194, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 16)
        Me.Label8.TabIndex = 62
        Me.Label8.Tag = ""
        Me.Label8.Text = "Precio Expo:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPrecioExpo
        '
        Me.lkpPrecioExpo.AllowAdd = False
        Me.lkpPrecioExpo.AutoReaload = False
        Me.lkpPrecioExpo.DataSource = Nothing
        Me.lkpPrecioExpo.DefaultSearchField = ""
        Me.lkpPrecioExpo.DisplayMember = "nombre"
        Me.lkpPrecioExpo.EditValue = Nothing
        Me.lkpPrecioExpo.Filtered = False
        Me.lkpPrecioExpo.InitValue = Nothing
        Me.lkpPrecioExpo.Location = New System.Drawing.Point(272, 24)
        Me.lkpPrecioExpo.MultiSelect = False
        Me.lkpPrecioExpo.Name = "lkpPrecioExpo"
        Me.lkpPrecioExpo.NullText = ""
        Me.lkpPrecioExpo.PopupWidth = CType(250, Long)
        Me.lkpPrecioExpo.ReadOnlyControl = False
        Me.lkpPrecioExpo.Required = False
        Me.lkpPrecioExpo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPrecioExpo.SearchMember = ""
        Me.lkpPrecioExpo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPrecioExpo.SelectAll = False
        Me.lkpPrecioExpo.Size = New System.Drawing.Size(192, 20)
        Me.lkpPrecioExpo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPrecioExpo.TabIndex = 63
        Me.lkpPrecioExpo.Tag = "precio_expo"
        Me.lkpPrecioExpo.ToolTip = Nothing
        Me.lkpPrecioExpo.ValueMember = "precio"
        '
        'lkpPrecioContado
        '
        Me.lkpPrecioContado.AllowAdd = False
        Me.lkpPrecioContado.AutoReaload = False
        Me.lkpPrecioContado.DataSource = Nothing
        Me.lkpPrecioContado.DefaultSearchField = ""
        Me.lkpPrecioContado.DisplayMember = "nombre"
        Me.lkpPrecioContado.EditValue = Nothing
        Me.lkpPrecioContado.Filtered = False
        Me.lkpPrecioContado.InitValue = Nothing
        Me.lkpPrecioContado.Location = New System.Drawing.Point(272, 48)
        Me.lkpPrecioContado.MultiSelect = False
        Me.lkpPrecioContado.Name = "lkpPrecioContado"
        Me.lkpPrecioContado.NullText = ""
        Me.lkpPrecioContado.PopupWidth = CType(250, Long)
        Me.lkpPrecioContado.ReadOnlyControl = False
        Me.lkpPrecioContado.Required = False
        Me.lkpPrecioContado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPrecioContado.SearchMember = ""
        Me.lkpPrecioContado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPrecioContado.SelectAll = False
        Me.lkpPrecioContado.Size = New System.Drawing.Size(192, 20)
        Me.lkpPrecioContado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPrecioContado.TabIndex = 65
        Me.lkpPrecioContado.Tag = "precio_contado"
        Me.lkpPrecioContado.ToolTip = Nothing
        Me.lkpPrecioContado.ValueMember = "precio"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(175, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(92, 16)
        Me.Label18.TabIndex = 64
        Me.Label18.Tag = ""
        Me.Label18.Text = "Precio Contado:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbCotizaciones
        '
        Me.tbCotizaciones.BackColor = System.Drawing.Color.White
        Me.tbCotizaciones.Controls.Add(Me.Label35)
        Me.tbCotizaciones.Controls.Add(Me.Label34)
        Me.tbCotizaciones.Controls.Add(Me.memPieCotizacion)
        Me.tbCotizaciones.Controls.Add(Me.memEncabezadoCotizacion)
        Me.tbCotizaciones.Controls.Add(Me.Label24)
        Me.tbCotizaciones.Controls.Add(Me.clcDiasVigenciaCotizacion)
        Me.tbCotizaciones.Location = New System.Drawing.Point(4, 22)
        Me.tbCotizaciones.Name = "tbCotizaciones"
        Me.tbCotizaciones.Size = New System.Drawing.Size(600, 286)
        Me.tbCotizaciones.TabIndex = 6
        Me.tbCotizaciones.Text = "Cotizaciones"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(88, 144)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(86, 16)
        Me.Label35.TabIndex = 23
        Me.Label35.Tag = ""
        Me.Label35.Text = "Pie Cotización:"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(40, 72)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(135, 16)
        Me.Label34.TabIndex = 22
        Me.Label34.Tag = ""
        Me.Label34.Text = "Encabezado Cotización:"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'memPieCotizacion
        '
        Me.memPieCotizacion.EditValue = ""
        Me.memPieCotizacion.Location = New System.Drawing.Point(184, 144)
        Me.memPieCotizacion.Name = "memPieCotizacion"
        Me.memPieCotizacion.Size = New System.Drawing.Size(400, 64)
        Me.memPieCotizacion.TabIndex = 21
        Me.memPieCotizacion.Tag = "pie_cotizacion"
        '
        'memEncabezadoCotizacion
        '
        Me.memEncabezadoCotizacion.EditValue = ""
        Me.memEncabezadoCotizacion.Location = New System.Drawing.Point(184, 72)
        Me.memEncabezadoCotizacion.Name = "memEncabezadoCotizacion"
        Me.memEncabezadoCotizacion.Size = New System.Drawing.Size(400, 64)
        Me.memEncabezadoCotizacion.TabIndex = 20
        Me.memEncabezadoCotizacion.Tag = "encabezado_cotizacion"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(16, 40)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(159, 16)
        Me.Label24.TabIndex = 18
        Me.Label24.Tag = ""
        Me.Label24.Text = "Días vigencia de Cotización:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDiasVigenciaCotizacion
        '
        Me.clcDiasVigenciaCotizacion.EditValue = ""
        Me.clcDiasVigenciaCotizacion.Location = New System.Drawing.Point(184, 40)
        Me.clcDiasVigenciaCotizacion.Name = "clcDiasVigenciaCotizacion"
        Me.clcDiasVigenciaCotizacion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcDiasVigenciaCotizacion.Size = New System.Drawing.Size(75, 20)
        Me.clcDiasVigenciaCotizacion.TabIndex = 19
        Me.clcDiasVigenciaCotizacion.Tag = "dias_vigencia_cotizacion"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(134, 72)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(133, 16)
        Me.Label48.TabIndex = 66
        Me.Label48.Tag = ""
        Me.Label48.Text = "Precio Lista Convenios:"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPrecioListaConvenio
        '
        Me.lkpPrecioListaConvenio.AllowAdd = False
        Me.lkpPrecioListaConvenio.AutoReaload = False
        Me.lkpPrecioListaConvenio.DataSource = Nothing
        Me.lkpPrecioListaConvenio.DefaultSearchField = ""
        Me.lkpPrecioListaConvenio.DisplayMember = "nombre"
        Me.lkpPrecioListaConvenio.EditValue = Nothing
        Me.lkpPrecioListaConvenio.Filtered = False
        Me.lkpPrecioListaConvenio.InitValue = Nothing
        Me.lkpPrecioListaConvenio.Location = New System.Drawing.Point(272, 72)
        Me.lkpPrecioListaConvenio.MultiSelect = False
        Me.lkpPrecioListaConvenio.Name = "lkpPrecioListaConvenio"
        Me.lkpPrecioListaConvenio.NullText = ""
        Me.lkpPrecioListaConvenio.PopupWidth = CType(250, Long)
        Me.lkpPrecioListaConvenio.ReadOnlyControl = False
        Me.lkpPrecioListaConvenio.Required = False
        Me.lkpPrecioListaConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPrecioListaConvenio.SearchMember = ""
        Me.lkpPrecioListaConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPrecioListaConvenio.SelectAll = False
        Me.lkpPrecioListaConvenio.Size = New System.Drawing.Size(192, 20)
        Me.lkpPrecioListaConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPrecioListaConvenio.TabIndex = 67
        Me.lkpPrecioListaConvenio.Tag = "precio_lista_convenio"
        Me.lkpPrecioListaConvenio.ToolTip = Nothing
        Me.lkpPrecioListaConvenio.ValueMember = "precio"
        '
        'frmVariables
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(610, 344)
        Me.Controls.Add(Me.TabControl1)
        Me.MasterControlActive = "TINMaster"
        Me.Name = "frmVariables"
        Me.Text = "Variables del Sistema"
        Me.Controls.SetChildIndex(Me.TabControl1, 0)
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmpresa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLogotipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.tbGenerales.ResumeLayout(False)
        CType(Me.clcDias_gracia_nota_cargo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMesesSinAbonarParaJuridico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaCierre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDias_gracia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcporcentaje_intereses_moratorios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImpuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDiasArmado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDiasInstalacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoInteres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFactorLimiteCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbVales.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.clcImporteGastosAdministrativos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbConceptosCXP.ResumeLayout(False)
        Me.tbConceptosCXC.ResumeLayout(False)
        Me.tbConceptosInventario.ResumeLayout(False)
        Me.tbPrecios.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.speCierreComisionesCobradoresMinutos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.speInicioComisionesCobradoresMinutos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.speInicioComisionesCobradoresHoras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.speCierreComisionesCobradoresHoras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcEngancheEnMenos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbCotizaciones.ResumeLayout(False)
        CType(Me.memPieCotizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.memEncabezadoCotizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDiasVigenciaCotizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oVariables As VillarrealBusiness.clsVariables
    Private oConceptosInventario As VillarrealBusiness.clsConceptosInventario
    Private oConceptosCXC As VillarrealBusiness.clsConceptosCxc
    Private oPrecios As VillarrealBusiness.clsPrecios
    Private oPreciosPlanes As VillarrealBusiness.clsPreciosPlanes
    Private oCobradores As VillarrealBusiness.clsCobradores
    Private oConceptosCxp As VillarrealBusiness.clsConceptosCxp
    Private oConceptosMovimientosChequera As VillarrealBusiness.clsConceptosMovimientosChequera
    Private oFormasPago As VillarrealBusiness.clsFormasPagos
    Private oArticulos As VillarrealBusiness.clsArticulos



    Private ReadOnly Property concepto_cxc_factura_credito() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpFacturaCredito)
        End Get
    End Property

    Private ReadOnly Property concepto_cxc_factura_contado() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpFacturaContado)
        End Get
    End Property
    Private ReadOnly Property concepto_cxc_nota_cargo() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpNotaCargo)
        End Get
    End Property
    Private ReadOnly Property concepto_cxc_nota_credito() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpNotaCredito)
        End Get
    End Property
    Private ReadOnly Property concepto_cxc_abono() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpAbono)
        End Get
    End Property
    Private ReadOnly Property concepto_intereses() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpIntereses)
        End Get
    End Property
    Private ReadOnly Property concepto_cxc_descuentos_anticipados() As String
        Get
            Return PreparaValorLookupStr(Me.lkpDescuentosAnticipados)
        End Get
    End Property
    Private ReadOnly Property concepto_cxc_intereses_especiales() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpInteresesEspeciales)
        End Get
    End Property
    Private ReadOnly Property FormaPagoContado() As Long
        Get
            Return PreparaValorLookup(Me.lkpFormaPagoContado)
        End Get
    End Property
    Private ReadOnly Property ArticuloAnticipo() As Long
        Get
            Return PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmVariables_AbortUpdate() Handles MyBase.AbortUpdate
        TinApp.Connection.Rollback()
    End Sub
    Private Sub frmVariables_BeginUpdate() Handles MyBase.BeginUpdate
        TinApp.Connection.Begin()
    End Sub
    Private Sub frmVariables_EndUpdate() Handles MyBase.EndUpdate
        TinApp.Connection.Commit()
    End Sub

    Private Sub frmVariables_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim oEvent As New Events
        

        InicioComisionesCobrador = ConvertirHorasMinutosAFecha(CType(Me.speInicioComisionesCobradoresHoras.EditValue, Integer), CType(Me.speInicioComisionesCobradoresMinutos.EditValue, Integer))
        FinComisionesCobrador = ConvertirHorasMinutosAFecha(CType(Me.speCierreComisionesCobradoresHoras.EditValue, Integer), CType(Me.speCierreComisionesCobradoresMinutos.EditValue, Integer))

        Try

            Response = VerificaLookups()
            
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar actualizar los Valores. Revise que los parámetros hayan sido establecidos correctamente."
            Exit Sub
        End Try


        Try

            Response = oVariables.Actualizar(Me.DataSource, InicioComisionesCobrador, FinComisionesCobrador)

        Catch ex As Exception
            TinApp.Connection.Rollback()
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar actualizar los Valores. Revise que los parámetros hayan sido establecidos correctamente."
        End Try

    End Sub


    Private Sub frmVariables_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVariables = New VillarrealBusiness.clsVariables
        oConceptosInventario = New VillarrealBusiness.clsConceptosInventario
        oConceptosCXC = New VillarrealBusiness.clsConceptosCxc
        oPrecios = New VillarrealBusiness.clsPrecios
        oPreciosPlanes = New VillarrealBusiness.clsPreciosPlanes
        oCobradores = New VillarrealBusiness.clsCobradores
        oConceptosCxp = New VillarrealBusiness.clsConceptosCxp
        oConceptosMovimientosChequera = New VillarrealBusiness.clsConceptosMovimientosChequera
        oFormasPago = New VillarrealBusiness.clsFormasPagos
        oArticulos = New VillarrealBusiness.clsArticulos

        Me.Location = New System.Drawing.Point(0, 0)

        Response = oVariables.DespliegaDatos()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then


                With oDataSet

                    Me.TabControl1.SelectedTab = Me.tbVales
                    Me.lkpNCGastosAdministrativos.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_gastos_administrativos")), -1, .Tables(0).Rows(0).Item("concepto_gastos_administrativos"))
                    Me.lkpNCValesVencidos.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_vales_vencidos")), -1, .Tables(0).Rows(0).Item("concepto_vales_vencidos"))
                    Me.clcImporteGastosAdministrativos.Value = IIf(IsDBNull(.Tables(0).Rows(0).Item("importe_gastos_administrativos")), 0, .Tables(0).Rows(0).Item("importe_gastos_administrativos"))
                    Me.lkpFormaPagoVales.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("pago_vales_caja")), -1, .Tables(0).Rows(0).Item("pago_vales_caja"))

                    Me.TabControl1.SelectedTab = Me.tbConceptosCXP
                    Me.lkpConceptoNotaEntrada.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_cxp_nota_entrada")), -1, .Tables(0).Rows(0).Item("concepto_cxp_nota_entrada"))
                    Me.lkpConceptoCXPPagoVariasFacturas.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_cxp_pago_varias_facturas")), -1, .Tables(0).Rows(0).Item("concepto_cxp_pago_varias_facturas"))
                    Me.lkplkpConceptoCXPNotaCredito.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_cxp_nota_credito")), -1, .Tables(0).Rows(0).Item("concepto_cxp_nota_credito"))
                    Me.lkpConceptoCXPAbonoPorAnticipo.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_cxp_abono_comision")), -1, .Tables(0).Rows(0).Item("concepto_cxp_abono_comision"))
                    Me.lkpConceptoCXPCargoPorFlete.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_cxp_cargo_por_flete")), -1, .Tables(0).Rows(0).Item("concepto_cxp_cargo_por_flete"))
                    Me.lkpConceptosMovimientoChequera.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_movimiento_chequera")), -1, .Tables(0).Rows(0).Item("concepto_movimiento_chequera"))
                    Me.lkpConceptosMovimientoChequeraCxP.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("concepto_movimiento_chequera_cxp")), -1, .Tables(0).Rows(0).Item("concepto_movimiento_chequera_cxp"))

                    Me.TabControl1.SelectedTab = Me.tbPrecios
                    Me.lkpPrecioExpo.EditValue = .Tables(0).Rows(0).Item("precio_expo")
                    Me.lkpPrecioContado.EditValue = .Tables(0).Rows(0).Item("precio_contado")
                    Me.lkpPrecioListaConvenio.EditValue = .Tables(0).Rows(0).Item("precio_lista_convenio")

                    Me.TabControl1.SelectedTab = Me.tbConceptosInventario
                    Me.lkpCompra.EditValue = .Tables(0).Rows(0).Item("concepto_compra")
                    Me.lkpCancelacionCompra.EditValue = .Tables(0).Rows(0).Item("concepto_cancelacion_compra")
                    Me.lkpVenta.EditValue = .Tables(0).Rows(0).Item("concepto_venta")
                    Me.lkpCancelacionVenta.EditValue = .Tables(0).Rows(0).Item("concepto_cancelacion_venta")
                    Me.lkpDevolucionesProveedor.EditValue = .Tables(0).Rows(0).Item("concepto_devoluciones")
                    Me.lkpCancelacionDevolucionesProveedor.EditValue = .Tables(0).Rows(0).Item("concepto_cancelacion_devoluciones")
                    Me.lkpCancelacionTraspaso.EditValue = .Tables(0).Rows(0).Item("concepto_cancelacion_traspaso")
                    Me.lkpVistasSalida.EditValue = .Tables(0).Rows(0).Item("concepto_vistas_salida")
                    Me.lkpVistasEntrada.EditValue = .Tables(0).Rows(0).Item("concepto_vistas_entrada")

                    Me.lkpSalidaReparacion.EditValue = .Tables(0).Rows(0).Item("concepto_salida_reparacion")
                    Me.lkpEntradaReparacion.EditValue = .Tables(0).Rows(0).Item("concepto_entrada_reparacion")


                    Me.TabControl1.SelectedTab = Me.tbConceptosCXC
                    Me.lkpFacturaContado.EditValue = .Tables(0).Rows(0).Item("concepto_cxc_factura_contado")
                    Me.lkpFacturaCredito.EditValue = .Tables(0).Rows(0).Item("concepto_cxc_factura_credito")
                    Me.lkpNotaCargo.EditValue = .Tables(0).Rows(0).Item("concepto_cxc_nota_cargo")
                    Me.lkpNotaCredito.EditValue = .Tables(0).Rows(0).Item("concepto_cxc_nota_credito")
                    Me.lkpAbono.EditValue = .Tables(0).Rows(0).Item("concepto_cxc_abono")
                    Me.lkpIntereses.EditValue = .Tables(0).Rows(0).Item("concepto_intereses")
                    Me.lkpDescuentosAnticipados.EditValue = .Tables(0).Rows(0).Item("concepto_cxc_descuentos_anticipados")
                    Me.lkpInteresesEspeciales.EditValue = .Tables(0).Rows(0).Item("concepto_cxc_intereses_especiales")
                    Me.lkpFormaPagoContado.EditValue = .Tables(0).Rows(0).Item("pago_contado_caja")

                    Me.TabControl1.SelectedTab = Me.tbGenerales
                    Me.txtDireccion.Text = .Tables(0).Rows(0).Item("direccion")
                    Me.txtTelefono.Text = .Tables(0).Rows(0).Item("telefonos")
                    Me.picLogotipo.EditValue = .Tables(0).Rows(0).Item("logotipo")
                    Me.clcDias_gracia.EditValue = .Tables(0).Rows(0).Item("dias_gracia")
                    Me.clcImpuesto.EditValue = .Tables(0).Rows(0).Item("impuesto")
                    Me.clcporcentaje_intereses_moratorios.EditValue = .Tables(0).Rows(0).Item("porcentaje_intereses_moratorios")
                    Me.lkpCobrador.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("cobrador_casa")), -1, .Tables(0).Rows(0).Item("cobrador_casa"))
                    Me.dteFechaCierre.EditValue = .Tables(0).Rows(0).Item("fecha_cierre")
                    Me.txtEmpresa.Text = .Tables(0).Rows(0).Item("empresa")
                    Me.clcDiasArmado.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("dias_armado")), 0, .Tables(0).Rows(0).Item("dias_armado"))
                    Me.clcDiasInstalacion.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("dias_instalacion")), 0, .Tables(0).Rows(0).Item("dias_instalacion"))
                    Me.clcFactorLimiteCredito.EditValue = IIf(IsDBNull(.Tables(0).Rows(0).Item("factor_limite_credito")), 0, .Tables(0).Rows(0).Item("factor_limite_credito"))
                    Me.clcDiasVigenciaCotizacion.EditValue = .Tables(0).Rows(0).Item("dias_vigencia_cotizacion")
                    Me.clcMesesSinAbonarParaJuridico.EditValue = .Tables(0).Rows(0).Item("meses_sin_abonar_para_juridico")
                    InicioComisionesCobrador = .Tables(0).Rows(0).Item("hora_inicio_comisiones_cobrador")
                    FinComisionesCobrador = .Tables(0).Rows(0).Item("hora_fin_comisiones_cobrador")
                    Me.speInicioComisionesCobradoresHoras.EditValue = .Tables(0).Rows(0).Item("hora_inicio_comisiones_cobrador").hour
                    Me.speInicioComisionesCobradoresMinutos.EditValue = .Tables(0).Rows(0).Item("hora_inicio_comisiones_cobrador").minute
                    Me.speCierreComisionesCobradoresHoras.EditValue = .Tables(0).Rows(0).Item("hora_fin_comisiones_cobrador").hour
                    Me.speCierreComisionesCobradoresMinutos.EditValue = .Tables(0).Rows(0).Item("hora_fin_comisiones_cobrador").minute
                    Me.clcEngancheEnMenos.EditValue = .Tables(0).Rows(0).Item("enganche_en_menos")
                    Me.memEncabezadoCotizacion.EditValue = .Tables(0).Rows(0).Item("encabezado_cotizacion")
                    Me.memPieCotizacion.EditValue = .Tables(0).Rows(0).Item("pie_cotizacion")

                    Me.clcDias_gracia_nota_cargo.Value = .Tables(0).Rows(0).Item("dias_gracia_nota_cargo")
                    Me.cboTipoInteres.Value = .Tables(0).Rows(0).Item("tipo_intereses")
                    Me.lkpArticulo.EditValue = .Tables(0).Rows(0).Item("articulo_anticipos")



                End With
            End If


            oDataSet = Nothing
        End If
    End Sub
    Private Sub frmVariables_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oVariables.Validacion(Action, txtEmpresa.Text, txtDireccion.Text, txtTelefono.Text, Me.clcEngancheEnMenos.EditValue, InicioComisionesCobrador, FinComisionesCobrador, FormaPagoContado)
    End Sub

    Private Sub frmVariables_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))
    End Sub
    Private Sub frmVariables_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If Not banActiva Then
            Me.txtEmpresa.Focus()
            banActiva = True
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"

#Region "Conceptos de Inventario"

    Private Sub lkpCompra_Format() Handles lkpCompra.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpCompra)
    End Sub
    Private Sub lkpCompra_LoadData(ByVal Initialize As Boolean) Handles lkpCompra.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCompra.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpCancelacionCompra_Format() Handles lkpCancelacionCompra.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpCancelacionCompra)
    End Sub
    Private Sub lkpCancelacionCompra_LoadData(ByVal Initialize As Boolean) Handles lkpCancelacionCompra.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCancelacionCompra.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpVenta_Format() Handles lkpVenta.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpVenta)
    End Sub
    Private Sub lkpVenta_LoadData(ByVal Initialize As Boolean) Handles lkpVenta.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpVenta.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpCancelacionVenta_Format() Handles lkpCancelacionVenta.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpCancelacionVenta)
    End Sub
    Private Sub lkpCancelacionVenta_LoadData(ByVal Initialize As Boolean) Handles lkpCancelacionVenta.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCancelacionVenta.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpDevolucionesProveedor_Format() Handles lkpDevolucionesProveedor.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpDevolucionesProveedor)
    End Sub
    Private Sub lkpDevolucionesProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpDevolucionesProveedor.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDevolucionesProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpCancelacionDevolucionesProveedor_Format() Handles lkpCancelacionDevolucionesProveedor.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpCancelacionDevolucionesProveedor)
    End Sub
    Private Sub lkpCancelacionDevolucionesProveedor_LoadData(ByVal Initialize As Boolean) Handles lkpCancelacionDevolucionesProveedor.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCancelacionDevolucionesProveedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpCancelacionTraspaso_Format() Handles lkpCancelacionTraspaso.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpCancelacionTraspaso)
    End Sub
    Private Sub lkpCancelacionTraspaso_LoadData(ByVal Initialize As Boolean) Handles lkpCancelacionTraspaso.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCancelacionTraspaso.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub


    Private Sub lkpVistasEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpVistasEntrada.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpVistasEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpVistasEntrada_Format() Handles lkpVistasEntrada.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpVistasEntrada)
    End Sub


    Private Sub lkpVistasSalida_Format() Handles lkpVistasSalida.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpVistasSalida)
    End Sub
    Private Sub lkpVistasSalida_LoadData(ByVal Initialize As Boolean) Handles lkpVistasSalida.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpVistasSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub lkpEntradaReparacion_Format() Handles lkpEntradaReparacion.Format
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("E")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEntradaReparacion.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpEntradaReparacion_LoadData(ByVal Initialize As Boolean) Handles lkpEntradaReparacion.LoadData
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpEntradaReparacion)
    End Sub

    Private Sub lkpSalidaReparacion_LoadData(ByVal Initialize As Boolean) Handles lkpSalidaReparacion.LoadData
        Dim Response As New Events
        Response = oConceptosInventario.Lookup("S")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSalidaReparacion.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub lkpSalidaReparacion_Format() Handles lkpSalidaReparacion.Format
        Comunes.clsFormato.for_conceptos_inventario_grl(Me.lkpSalidaReparacion)
    End Sub


#End Region

#Region "Conceptos de CxC"
    Private Sub lkpNotaCargo_Format() Handles lkpNotaCargo.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpNotaCargo)
    End Sub
    Private Sub lkpNotaCargo_LoadData(ByVal Initialize As Boolean) Handles lkpNotaCargo.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("C")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpNotaCargo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpNotaCredito_Format() Handles lkpNotaCredito.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpNotaCredito)
    End Sub
    Private Sub lkpNotaCredito_LoadData(ByVal Initialize As Boolean) Handles lkpNotaCredito.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("A")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpNotaCredito.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpAbono_Format() Handles lkpAbono.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpAbono)
    End Sub
    Private Sub lkpAbono_LoadData(ByVal Initialize As Boolean) Handles lkpAbono.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("A")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpAbono.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpIntereses_Format() Handles lkpIntereses.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpIntereses)
    End Sub
    Private Sub lkpIntereses_LoadData(ByVal Initialize As Boolean) Handles lkpIntereses.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("C")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpIntereses.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDescuentosAnticipados_Format() Handles lkpDescuentosAnticipados.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpDescuentosAnticipados)
    End Sub
    Private Sub lkpDescuentosAnticipados_LoadData(ByVal Initialize As Boolean) Handles lkpDescuentosAnticipados.LoadData
        Dim response As Events
        response = Me.oConceptosCXC.Lookup("A")
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpDescuentosAnticipados.DataSource = oDataSet.Tables(0)
        End If
        response = Nothing
    End Sub

    Private Sub lkpFacturaContado_Format() Handles lkpFacturaContado.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpFacturaContado)
    End Sub
    Private Sub lkpFacturaContado_LoadData(ByVal Initialize As Boolean) Handles lkpFacturaContado.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("C")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFacturaContado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub lkpFacturaCredito_Format() Handles lkpFacturaCredito.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpFacturaCredito)
    End Sub
    Private Sub lkpFacturaCredito_LoadData(ByVal Initialize As Boolean) Handles lkpFacturaCredito.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("C")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFacturaCredito.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpInteresesEspeciales_Format() Handles lkpInteresesEspeciales.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpInteresesEspeciales)
    End Sub
    Private Sub lkpInteresesEspeciales_LoadData(ByVal Initialize As Boolean) Handles lkpInteresesEspeciales.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("C")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpInteresesEspeciales.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

#End Region

#Region "Conceptos de CxP"
    Private Sub lkpConceptoCXPPagoVariasFacturas_Format() Handles lkpConceptoCXPPagoVariasFacturas.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConceptoCXPPagoVariasFacturas)
    End Sub
    Private Sub lkpConceptoCXPPagoVariasFacturas_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoCXPPagoVariasFacturas.LoadData
        Dim response As Events
        response = oConceptosCxp.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoCXPPagoVariasFacturas.DataSource = oDataSet.Tables(0)
        End If
        response = Nothing
    End Sub

    Private Sub lkplkpConceptoCXPNotaCredito_Format() Handles lkplkpConceptoCXPNotaCredito.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkplkpConceptoCXPNotaCredito)
    End Sub
    Private Sub lkplkpConceptoCXPNotaCredito_LoadData(ByVal Initialize As Boolean) Handles lkplkpConceptoCXPNotaCredito.LoadData
        Dim response As Events
        response = oConceptosCxp.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkplkpConceptoCXPNotaCredito.DataSource = oDataSet.Tables(0)
        End If
        response = Nothing
    End Sub

    Private Sub lkpConceptoNotaEntrada_Format() Handles lkpConceptoNotaEntrada.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConceptoNotaEntrada)
    End Sub
    Private Sub lkpConceptoNotaEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoNotaEntrada.LoadData
        Dim response As Events
        response = oConceptosCxp.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoNotaEntrada.DataSource = oDataSet.Tables(0)
        End If
        response = Nothing
    End Sub

    Private Sub lkpConceptoCXPAbonoPorAnticipo_Format() Handles lkpConceptoCXPAbonoPorAnticipo.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConceptoCXPAbonoPorAnticipo)
    End Sub
    Private Sub lkpConceptoCXPAbonoPorAnticipo_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoCXPAbonoPorAnticipo.LoadData
        Dim response As Events
        response = oConceptosCxp.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoCXPAbonoPorAnticipo.DataSource = oDataSet.Tables(0)
        End If
        response = Nothing
    End Sub

    Private Sub lkpConceptoCXPCargoPorFlete_Format() Handles lkpConceptoCXPCargoPorFlete.Format
        Comunes.clsFormato.for_conceptos_cxp_grl(Me.lkpConceptoCXPCargoPorFlete)
    End Sub
    Private Sub lkpConceptoCXPCargoPorFlete_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoCXPCargoPorFlete.LoadData
        Dim response As Events
        response = oConceptosCxp.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptoCXPCargoPorFlete.DataSource = oDataSet.Tables(0)
        End If
        response = Nothing
    End Sub

    Private Sub lkpConceptoMovimientoChequera_Format() Handles lkpConceptosMovimientoChequera.Format
        Comunes.clsFormato.for_conceptos_movimientos_chequera_grl(Me.lkpConceptosMovimientoChequera)
    End Sub
    Private Sub lkpConceptoMovimientoChequera_LoadData(ByVal Initialize As Boolean) Handles lkpConceptosMovimientoChequera.LoadData
        Dim response As Events
        response = oConceptosMovimientosChequera.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptosMovimientoChequera.DataSource = oDataSet.Tables(0)
        End If
        response = Nothing
    End Sub

    Private Sub lkpConceptosMovimientoChequeraCxP_Format() Handles lkpConceptosMovimientoChequeraCxP.Format
        Comunes.clsFormato.for_conceptos_movimientos_chequera_grl(Me.lkpConceptosMovimientoChequeraCxP)
    End Sub
    Private Sub lkpConceptosMovimientoChequeraCxP_LoadData(ByVal Initialize As Boolean) Handles lkpConceptosMovimientoChequeraCxP.LoadData
        Dim response As Events
        response = oConceptosMovimientosChequera.Lookup
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpConceptosMovimientoChequeraCxP.DataSource = oDataSet.Tables(0)
        End If
        response = Nothing
    End Sub


#End Region

#Region "VALES"

    Private Sub lkpNCGastosAdministrativos_LoadData(ByVal Initialize As Boolean) Handles lkpNCGastosAdministrativos.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("C")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpNCGastosAdministrativos.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpNCGastosAdministrativos_Format() Handles lkpNCGastosAdministrativos.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpNCGastosAdministrativos)
    End Sub

    Private Sub lkpNCValesVencidos_LoadData(ByVal Initialize As Boolean) Handles lkpNCValesVencidos.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("C")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpNCValesVencidos.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub
    Private Sub lkpNCValesVencidos_Format() Handles lkpNCValesVencidos.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpNCValesVencidos)
    End Sub

    Private Sub lkpFormaPagoVales_LoadData(ByVal Initialize As Boolean) Handles lkpFormaPagoVales.LoadData
        Dim Response As New Events
        Response = oFormasPago.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFormaPagoVales.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpFormaPagoVales_Format() Handles lkpFormaPagoVales.Format
        Comunes.clsFormato.for_formas_pagos_grl(Me.lkpFormaPagoVales)
    End Sub


#End Region

    Private Sub lkpFormaPagoContado_LoadData(ByVal Initialize As Boolean) Handles lkpFormaPagoContado.LoadData
        Dim Response As New Events
        Response = oFormasPago.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFormaPagoContado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpFormaPagoContado_Format() Handles lkpFormaPagoContado.Format
        Comunes.clsFormato.for_formas_pagos_grl(Me.lkpFormaPagoContado)
    End Sub


    Private Sub lkpPrecioExpo_Format() Handles lkpPrecioExpo.Format
        Comunes.clsFormato.for_precios_grl(Me.lkpPrecioExpo)
    End Sub
    Private Sub lkpPrecioExpo_LoadData(ByVal Initialize As Boolean) Handles lkpPrecioExpo.LoadData
        Dim Response As New Events
        Response = oPrecios.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPrecioExpo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub lkpPrecioContado_Format() Handles lkpPrecioContado.Format
        Comunes.clsFormato.for_precios_grl(Me.lkpPrecioContado)
    End Sub
    Private Sub lkpPrecioContado_LoadData(ByVal Initialize As Boolean) Handles lkpPrecioContado.LoadData
        Dim Response As New Events
        Response = oPrecios.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPrecioContado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub lkpPrecioListaConvenio_Format() Handles lkpPrecioListaConvenio.Format
        Comunes.clsFormato.for_precios_grl(Me.lkpPrecioListaConvenio)
    End Sub

    Private Sub lkpPrecioListaConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpPrecioListaConvenio.LoadData
        Dim Response As New Events
        Response = oPrecios.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPrecioListaConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub


    Private Sub txtEmpresa_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oVariables.ValidaEmpresa(txtEmpresa.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub txtDireccion_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oVariables.ValidaDireccion(txtDireccion.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub txtTelefono_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtTelefono.Validating
        Dim oEvent As Dipros.Utils.Events
        If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
        oEvent = oVariables.ValidaTelefonos(txtTelefono.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub

    Private Sub lkpCobrador_LoadData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData
        Dim Response As New Events
        Response = oCobradores.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub

    Private Sub speCierreComisionesCobradoresHoras_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles speCierreComisionesCobradoresHoras.EditValueChanged
        Me.FinComisionesCobrador = ConvertirHorasMinutosAFecha(Me.speCierreComisionesCobradoresHoras.EditValue, Me.speCierreComisionesCobradoresMinutos.EditValue)
    End Sub
    Private Sub speCierreComisionesCobradoresMinutos_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles speCierreComisionesCobradoresMinutos.EditValueChanged
        Me.FinComisionesCobrador = ConvertirHorasMinutosAFecha(Me.speCierreComisionesCobradoresHoras.EditValue, Me.speCierreComisionesCobradoresMinutos.EditValue)
    End Sub
    Private Sub speInicioComisionesCobradoresHoras_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles speInicioComisionesCobradoresHoras.EditValueChanged
        Me.InicioComisionesCobrador = ConvertirHorasMinutosAFecha(Me.speInicioComisionesCobradoresHoras.EditValue, Me.speInicioComisionesCobradoresMinutos.EditValue)
    End Sub
    Private Sub speInicioComisionesCobradoresMinutos_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles speInicioComisionesCobradoresMinutos.EditValueChanged
        Me.InicioComisionesCobrador = ConvertirHorasMinutosAFecha(Me.speInicioComisionesCobradoresHoras.EditValue, Me.speInicioComisionesCobradoresMinutos.EditValue)
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData

        Dim Response As New Events
        Response = oArticulos.Lookup()
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"


    Public Function VerificaLookups() As Events
        'Lookups de General
        Me.lkpFacturaContado.EditValue = ValidaConcepto(Me.lkpFacturaContado.EditValue)
        'Lookups de Conceptos de CXC
        Me.lkpFacturaContado.EditValue = ValidaConcepto(Me.lkpFacturaContado.EditValue)
        Me.lkpFacturaCredito.EditValue = ValidaConcepto(Me.lkpFacturaCredito.EditValue)
        Me.lkpNotaCargo.EditValue = ValidaConcepto(Me.lkpNotaCargo.EditValue)
        Me.lkpNotaCredito.EditValue = ValidaConcepto(Me.lkpNotaCredito.EditValue)
        Me.lkpAbono.EditValue = ValidaConcepto(Me.lkpAbono.EditValue)
        Me.lkpIntereses.EditValue = ValidaConcepto(Me.lkpIntereses.EditValue)
        Me.lkpDescuentosAnticipados.EditValue = ValidaConcepto(Me.lkpDescuentosAnticipados.EditValue)
        Me.lkpInteresesEspeciales.EditValue = ValidaConcepto(Me.lkpInteresesEspeciales.EditValue)
        'Lookups de Conceptos de INVENTARIO
        Me.lkpCompra.EditValue = ValidaConcepto(Me.lkpCompra.EditValue)
        Me.lkpVenta.EditValue = ValidaConcepto(Me.lkpVenta.EditValue)
        Me.lkpCancelacionVenta.EditValue = ValidaConcepto(Me.lkpCancelacionVenta.EditValue)
        Me.lkpDevolucionesProveedor.EditValue = ValidaConcepto(Me.lkpDevolucionesProveedor.EditValue)
        Me.lkpCancelacionDevolucionesProveedor.EditValue = ValidaConcepto(Me.lkpCancelacionDevolucionesProveedor.EditValue)
        Me.lkpCancelacionTraspaso.EditValue = ValidaConcepto(Me.lkpCancelacionTraspaso.EditValue)
        Me.lkpCancelacionCompra.EditValue = ValidaConcepto(Me.lkpCancelacionCompra.EditValue)
        Me.lkpVistasSalida.EditValue = ValidaConcepto(Me.lkpVistasSalida.EditValue)
        Me.lkpVistasEntrada.EditValue = ValidaConcepto(Me.lkpVistasEntrada.EditValue)

        'Lookups de Conceptos de CXP
        Me.lkpConceptoNotaEntrada.EditValue = ValidaConcepto(Me.lkpConceptoNotaEntrada.EditValue)
        Me.lkpConceptoCXPPagoVariasFacturas.EditValue = ValidaConcepto(Me.lkpConceptoCXPPagoVariasFacturas.EditValue)
        Me.lkplkpConceptoCXPNotaCredito.EditValue = ValidaConcepto(Me.lkplkpConceptoCXPNotaCredito.EditValue)
        ''Lookups de Conceptos de PRECIOS
        Me.lkpPrecioExpo.EditValue = ValidaLookups(Me.lkpPrecioExpo.EditValue)
        Me.lkpPrecioContado.EditValue = ValidaLookups(Me.lkpPrecioContado.EditValue)

    End Function
    Public Function ValidaConcepto(ByVal concepto As String) As String

        If concepto = Nothing Or IsDBNull(concepto) Then
            'ValidaConcepto = ""
            ValidaConcepto = Nothing
        Else
            ValidaConcepto = concepto
        End If

    End Function
    Public Function ValidaLookups(ByVal valor As Long) As Long

        If valor = Nothing Or IsDBNull(valor) Then
            ValidaLookups = -1
        Else
            ValidaLookups = valor
        End If

    End Function


    Private Function ConvertirHorasMinutosAFecha(ByVal horas As Integer, ByVal minutos As Integer) As DateTime
        Dim Fecha As DateTime
        Fecha = CType(TinApp.FechaServidor, DateTime)

        Fecha = Fecha.AddHours(CType(horas - Fecha.Hour, Long))
        Fecha = Fecha.AddMinutes(CType(minutos - Fecha.Minute, Long))
        Fecha = Fecha.AddSeconds(CType(-Fecha.Second, Long))
      
        Return Fecha
    End Function

    Private Function ConvertirFechaAHorasMinutos()

        Dim Horas As Integer
        Dim Minutos As Integer

        Me.speInicioComisionesCobradoresHoras.EditValue = CType(InicioComisionesCobrador, DateTime).Hour 'CType(Me.dteInicioComisionesCobrador.EditValue, DateTime).Hour
        Me.speInicioComisionesCobradoresMinutos.EditValue = CType(InicioComisionesCobrador, DateTime).Minute
        Me.speCierreComisionesCobradoresHoras.EditValue = CType(FinComisionesCobrador, DateTime).Hour
        Me.speCierreComisionesCobradoresMinutos.EditValue = CType(FinComisionesCobrador, DateTime).Minute
    
    End Function

#End Region


   
   
End Class
