Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmPermisosIdentificadores
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grUsuarios As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvUsuarios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcTinUsuario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTinNombreUsuario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grPermisosExtendidos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvPermisosExtendidos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSistema As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcIdentificador As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPermitir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcUsuario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAplicar As System.Windows.Forms.ToolBarButton
    Friend WithEvents grcModificado As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPermisosIdentificadores))
        Me.grUsuarios = New DevExpress.XtraGrid.GridControl
        Me.grvUsuarios = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcTinUsuario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTinNombreUsuario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grPermisosExtendidos = New DevExpress.XtraGrid.GridControl
        Me.grvPermisosExtendidos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcUsuario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSistema = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcIdentificador = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPermitir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcModificado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnAplicar = New System.Windows.Forms.ToolBarButton
        CType(Me.grUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grPermisosExtendidos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvPermisosExtendidos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnAplicar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3358, 28)
        '
        'grUsuarios
        '
        '
        'grUsuarios.EmbeddedNavigator
        '
        Me.grUsuarios.EmbeddedNavigator.Name = ""
        Me.grUsuarios.Location = New System.Drawing.Point(0, 32)
        Me.grUsuarios.MainView = Me.grvUsuarios
        Me.grUsuarios.Name = "grUsuarios"
        Me.grUsuarios.Size = New System.Drawing.Size(336, 504)
        Me.grUsuarios.TabIndex = 59
        Me.grUsuarios.Text = "GridControl1"
        '
        'grvUsuarios
        '
        Me.grvUsuarios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcTinUsuario, Me.grcTinNombreUsuario})
        Me.grvUsuarios.GridControl = Me.grUsuarios
        Me.grvUsuarios.Name = "grvUsuarios"
        Me.grvUsuarios.OptionsBehavior.Editable = False
        Me.grvUsuarios.OptionsView.ShowGroupPanel = False
        '
        'grcTinUsuario
        '
        Me.grcTinUsuario.Caption = "Usuario"
        Me.grcTinUsuario.FieldName = "usuario"
        Me.grcTinUsuario.Name = "grcTinUsuario"
        Me.grcTinUsuario.VisibleIndex = 0
        Me.grcTinUsuario.Width = 78
        '
        'grcTinNombreUsuario
        '
        Me.grcTinNombreUsuario.Caption = "Nombre"
        Me.grcTinNombreUsuario.FieldName = "nombre"
        Me.grcTinNombreUsuario.Name = "grcTinNombreUsuario"
        Me.grcTinNombreUsuario.VisibleIndex = 1
        Me.grcTinNombreUsuario.Width = 268
        '
        'grPermisosExtendidos
        '
        '
        'grPermisosExtendidos.EmbeddedNavigator
        '
        Me.grPermisosExtendidos.EmbeddedNavigator.Name = ""
        Me.grPermisosExtendidos.Location = New System.Drawing.Point(344, 32)
        Me.grPermisosExtendidos.MainView = Me.grvPermisosExtendidos
        Me.grPermisosExtendidos.Name = "grPermisosExtendidos"
        Me.grPermisosExtendidos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.grPermisosExtendidos.Size = New System.Drawing.Size(600, 504)
        Me.grPermisosExtendidos.TabIndex = 60
        Me.grPermisosExtendidos.Text = "GridControl2"
        '
        'grvPermisosExtendidos
        '
        Me.grvPermisosExtendidos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcUsuario, Me.grcSistema, Me.grcIdentificador, Me.grcDescripcion, Me.grcPermitir, Me.grcModificado})
        Me.grvPermisosExtendidos.GridControl = Me.grPermisosExtendidos
        Me.grvPermisosExtendidos.Name = "grvPermisosExtendidos"
        Me.grvPermisosExtendidos.OptionsView.ShowGroupPanel = False
        '
        'grcUsuario
        '
        Me.grcUsuario.Caption = "Usuario"
        Me.grcUsuario.FieldName = "usuario"
        Me.grcUsuario.Name = "grcUsuario"
        Me.grcUsuario.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSistema
        '
        Me.grcSistema.Caption = "Sistema"
        Me.grcSistema.FieldName = "sistema"
        Me.grcSistema.Name = "grcSistema"
        Me.grcSistema.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSistema.VisibleIndex = 1
        Me.grcSistema.Width = 53
        '
        'grcIdentificador
        '
        Me.grcIdentificador.Caption = "Identificador"
        Me.grcIdentificador.FieldName = "identificador"
        Me.grcIdentificador.Name = "grcIdentificador"
        Me.grcIdentificador.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcIdentificador.VisibleIndex = 2
        Me.grcIdentificador.Width = 99
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 3
        Me.grcDescripcion.Width = 293
        '
        'grcPermitir
        '
        Me.grcPermitir.Caption = "Permitir"
        Me.grcPermitir.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcPermitir.FieldName = "permitir"
        Me.grcPermitir.Name = "grcPermitir"
        Me.grcPermitir.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPermitir.VisibleIndex = 0
        Me.grcPermitir.Width = 51
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grcModificado
        '
        Me.grcModificado.Caption = "Modificado"
        Me.grcModificado.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.grcModificado.FieldName = "modificado"
        Me.grcModificado.Name = "grcModificado"
        Me.grcModificado.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'btnAplicar
        '
        Me.btnAplicar.Text = "Aplicar"
        Me.btnAplicar.ToolTipText = "Aplica los cambios realizados en la seguridad"
        '
        'frmPermisosIdentificadores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(946, 540)
        Me.Controls.Add(Me.grUsuarios)
        Me.Controls.Add(Me.grPermisosExtendidos)
        Me.Name = "frmPermisosIdentificadores"
        Me.Text = "frmPermisosIdentificadores"
        Me.Controls.SetChildIndex(Me.grPermisosExtendidos, 0)
        Me.Controls.SetChildIndex(Me.grUsuarios, 0)
        CType(Me.grUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grPermisosExtendidos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvPermisosExtendidos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oUsuarios As VillarrealBusiness.clsUsuarios
    Private oPermisosExtendidos As VillarrealBusiness.clsIdentificadoresPermisos
    Private SinGuardar As Boolean = False


    Private Sub frmPermisosIdentificadores_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oUsuarios = New VillarrealBusiness.clsUsuarios
        oPermisosExtendidos = New VillarrealBusiness.clsIdentificadoresPermisos


        Me.tbrTools.Buttons(0).Enabled = False
        Me.tbrTools.Buttons(0).Visible = False
        'Me.tbrTools.Buttons(1).Enabled = False
        'Me.tbrTools.Buttons(1).Visible = False

        ObtenerUsuarios()


    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        Dim response As New Events


        If e.Button Is Me.btnAplicar Then
            response = Guardar()
        End If

        If response.ErrorFound Then
            response.ShowError()
        End If

    End Sub

    
    Private Sub grvUsuarios_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvUsuarios.FocusedRowChanged
        If e.FocusedRowHandle > -1 Then
            Dim usuario As String = Me.grvUsuarios.GetRowCellValue(e.FocusedRowHandle, Me.grcTinUsuario)

            If SinGuardar = True Then
                Guardar()
            End If
            ObtenerUsuariosPermisos(usuario)
        End If
    End Sub
    Private Sub grvPermisosExtendidos_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvPermisosExtendidos.CellValueChanging
        'Me.grvPermisosExtendidos.UpdateCurrentRow()
        'Me.grvPermisosExtendidos.CloseEditor()

        If (e.Column Is grcPermitir) Then
            'Or (e.Column Is grcDenegar)
            'Dim response As Events
            'Dim usuario As String
            'Dim identificador As String
            'Dim permitir As Boolean
            'Dim denegar As Boolean

            Me.grvPermisosExtendidos.SetRowCellValue(e.RowHandle, Me.grcModificado, True)
            SinGuardar = True

            'usuario = Me.grvPermisosExtendidos.GetRowCellValue(e.RowHandle, Me.grcUsuario)
            'identificador = Me.grvPermisosExtendidos.GetRowCellValue(e.RowHandle, Me.grcIdentificador)
            'permitir = Me.grvPermisosExtendidos.GetRowCellValue(e.RowHandle, Me.grcPermitir)
            'denegar = Me.grvPermisosExtendidos.GetRowCellValue(e.RowHandle, Me.grcDenegar)

            'response = oPermisosExtendidos.InsertarPermisos(usuario, TinApp.Prefix, identificador, permitir, denegar)
            'If response.ErrorFound Then
            '    ShowMessage(MessageType.MsgError, response.Message)
            'End If
        End If
    End Sub

    Private Sub ObtenerUsuarios()
        Dim response As Events

        response = oUsuarios.Lookup()
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = CType(response.Value, DataSet)

            Me.grUsuarios.DataSource = odataset.Tables(0)

        End If
    End Sub
    Private Sub ObtenerUsuariosPermisos(ByVal usuario As String)
        Dim response As Events

        Me.grPermisosExtendidos.DataSource = Nothing

        response = oPermisosExtendidos.DesplegarPermisos(usuario, TinApp.Prefix)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = CType(response.Value, DataSet)

            Me.grPermisosExtendidos.DataSource = odataset.Tables(0)

        End If
    End Sub
    Private Function Guardar() As Events
        ' ABRE LA TRANSACCION
        TinApp.Connection.Begin()


        Dim response As Events
        Dim Modificado As Boolean
        Dim usuario As String
        Dim identificador As String
        Dim permitir As Boolean
        Dim denegar As Boolean
        Me.grvPermisosExtendidos.UpdateCurrentRow()


        Dim i As Long

        For i = 0 To Me.grvPermisosExtendidos.RowCount

            Modificado = Me.grvPermisosExtendidos.GetRowCellValue(i, Me.grcModificado)
            If Modificado Then
                usuario = Me.grvPermisosExtendidos.GetRowCellValue(i, Me.grcUsuario)
                identificador = Me.grvPermisosExtendidos.GetRowCellValue(i, Me.grcIdentificador)
                permitir = Me.grvPermisosExtendidos.GetRowCellValue(i, Me.grcPermitir)
                denegar = Not permitir

                response = oPermisosExtendidos.InsertarPermisos(usuario, TinApp.Prefix, identificador, permitir, denegar)

                If response.ErrorFound Then
                    Exit For
                End If
            End If

        Next

        If response.ErrorFound Then
            TinApp.Connection.Rollback()
            Exit Function
        Else
            ' TERMINA LA TRANSACCION
            TinApp.Connection.Commit()
            SinGuardar = False
        End If


        Return response
    End Function









End Class
