Imports System.Data.Odbc

Public Class clsExportaRenglon
    Private strCampo As String
    Private strTipo As String
    Private iLongitud As Integer
    Private iPrecision As Integer
    Private strOrigen As String
    Private oParametro As OdbcParameter

    Friend ReadOnly Property Campo() As String
        Get
            Return strCampo
        End Get
    End Property

    Friend ReadOnly Property Origen() As String
        Get
            Return strOrigen
        End Get
    End Property

    Friend Function DefineRenglon() As String
        If iPrecision > 0 Then
            Return String.Format("{0} {1}({2}.{3})", strCampo, strTipo, iLongitud, iPrecision)
        ElseIf iLongitud > 0 Then
            Return String.Format("{0} {1}({2})", strCampo, strTipo, iLongitud)
        Else
            Return String.Format("{0} {1}", strCampo, strTipo)
        End If
    End Function


    Friend Function Formato(ByVal Contador As Integer)
        Select Case strTipo
            Case "I"
                Return "{" & Contador & "} "
            Case "C"
                Return "'{" & Contador & "}' "
        End Select
    End Function

    Friend Sub New(ByVal Campo As String, ByVal Tipo As String, ByVal Longitud As Integer, ByVal Precision As Integer, ByVal Origen As String)
        strCampo = Campo
        strTipo = Tipo
        iLongitud = Longitud
        iPrecision = Precision
        strOrigen = Origen
    End Sub

End Class
