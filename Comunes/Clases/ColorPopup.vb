Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors.Popup
Imports DevExpress.XtraTab
Imports DevExpress.XtraBars
Imports System.Windows.Forms
Imports System.Drawing

Public Class ColorPopup
    Dim _tabControl As XtraTabControl
    Dim _resultColor As Color
    Dim itemFontColor As BarItem
    Dim container As PopupControlContainer
    Dim rtPad As RichTextBox
    Public Sub New(ByVal container As PopupControlContainer, ByVal item As BarItem, ByVal rtPad As RichTextBox)
        Me.container = container
        Me.itemFontColor = item
        Me.rtPad = rtPad
        Me._resultColor = Color.Empty
        Me._tabControl = CreateTabControl()
        Me._tabControl.TabStop = False
        Me._tabControl.TabPages.AddRange(New XtraTabPage() {New XtraTabPage, New XtraTabPage, New XtraTabPage})
        Dim i As Integer
        For i = 0 To _tabControl.TabPages.Count - 1
            SetTabPageProperties(i)
        Next
        Me._tabControl.GetStyleByViewName("PageHeader").HAlignment = DevExpress.Utils.HorzAlignment.Center
        _tabControl.Dock = DockStyle.Fill
        Me.container.Controls.AddRange(New System.Windows.Forms.Control() {_tabControl})
        Me.container.Size = CalcFormSize()
    End Sub

    Private Function CreateTabControl() As XtraTabControl
        Return New XtraTabControl
    End Function

    Private Function CreateColorListBox() As ColorListBox
        Return New ColorListBox
    End Function

    Private Sub SetTabPageProperties(ByVal pageIndex As Integer)
        Dim tabPage As XtraTabPage = Me.tabControl.TabPages(pageIndex)
        Dim colorBox As ColorListBox = Nothing
        Dim control As BaseControl = Nothing
        Select Case pageIndex
            Case 0
                tabPage.Text = Localizer.Active.GetLocalizedString(StringId.ColorTabCustom)
                control = New ColorCellsControl
                Dim rItem As New DevExpress.XtraEditors.Repository.RepositoryItemColorEdit
                rItem.ShowColorDialog = False
                CType(control, ColorCellsControl).Properties = rItem
                AddHandler CType(control, ColorCellsControl).EnterColor, New EnterColorEventHandler(AddressOf OnEnterColor)
                control.Size = ColorCellsControlViewInfo.BestSize
            Case 1
                tabPage.Text = Localizer.Active.GetLocalizedString(StringId.ColorTabWeb)
                colorBox = CreateColorListBox()
                colorBox.Items.AddRange(ColorListBoxViewInfo.WebColors)
                AddHandler colorBox.EnterColor, New EnterColorEventHandler(AddressOf OnEnterColor)
                colorBox.Size = GetBestListBoxSize(colorBox)
                control = colorBox
            Case 2
                tabPage.Text = Localizer.Active.GetLocalizedString(StringId.ColorTabSystem)
                colorBox = CreateColorListBox()
                colorBox.Items.AddRange(ColorListBoxViewInfo.SystemColors)
                AddHandler colorBox.EnterColor, New EnterColorEventHandler(AddressOf OnEnterColor)
                colorBox.Size = GetBestListBoxSize(colorBox)
                control = colorBox
        End Select
        control.Dock = DockStyle.Fill
        control.BorderStyle = BorderStyles.NoBorder
        control.LookAndFeel.ParentLookAndFeel = itemFontColor.Manager.GetController().LookAndFeel
        tabPage.Controls.Add(control)
    End Sub

    Private Sub OnEnterColor(ByVal sender As Object, ByVal e As EnterColorEventArgs)
        _resultColor = e.Color
        OnEnterColor(e.Color)
    End Sub

    Public Sub OnEnterColor(ByVal clr As Color)
        container.HidePopup()
        rtPad.SelectionColor = clr
        Dim imIndex As Integer = itemFontColor.ImageIndex
        Dim im As Bitmap = itemFontColor.Images.Images(imIndex)
        Dim g As Graphics = Graphics.FromImage(im)
        Dim r As New Rectangle(7, 7, 8, 8)
        g.FillRectangle(New SolidBrush(clr), r)
        If imIndex = itemFontColor.Images.Images.Count - 1 Then
            itemFontColor.Images.Images.RemoveAt(imIndex)
        End If
        itemFontColor.Images.Images.Add(im)
        itemFontColor.ImageIndex = itemFontColor.Images.Images.Count - 1
    End Sub

    Private Function GetBestListBoxSize(ByVal colorBox As ColorListBox) As Size
        Return New Size(ColorCellsControlViewInfo.BestSize.Width, GetNearestBestClientHeight(ColorCellsControlViewInfo.BestSize.Height, colorBox))
    End Function

    Private Function GetNearestBestClientHeight(ByVal height As Integer, ByVal OwnerControl As ColorListBox) As Integer
        Dim rows As Integer = height / OwnerControl.ItemHeight
        If rows * OwnerControl.ItemHeight = height Then Return height
        Return (rows + 1) * OwnerControl.ItemHeight
    End Function

    Private ReadOnly Property CellsControl() As ColorCellsControl
        Get
            Return TabControl.TabPages(0).Controls(0)
        End Get
    End Property

    Private ReadOnly Property CustomColorsName() As String
        Get
            Return "CustomColors"
        End Get
    End Property

    Public ReadOnly Property TabControl() As XtraTabControl
        Get
            Return _tabControl
        End Get
    End Property

    Public ReadOnly Property ResultColor() As Color
        Get
            Return _resultColor
        End Get
    End Property

    Public Function CalcFormSize() As Size
        Dim size As Size = ColorCellsControlViewInfo.BestSize
        size.Height = GetNearestBestClientHeight(size.Height, TabControl.TabPages(2).Controls(0))
        Return TabControl.CalcSizeByPageClient(size)
    End Function
End Class
