Imports System.Data.Odbc

Public Class clsImportaDbfs

    Private oConexion As OdbcConnection
    Private oComando As OdbcCommand
    Private oLector As OdbcDataReader = Nothing
    Private strTabla As String
    Private strConsulta As String

    Public WriteOnly Property Directorio() As String
        Set(ByVal Value As String)
            oConexion.ConnectionString = String.Format("BackgroundFetch=S�;Collate=Machine;Exclusive=No;UID=;SourceType=DBF;Driver=Microsoft Visual FoxPro Driver;SourceDB={0}", Value)
        End Set
    End Property

    Public WriteOnly Property Archivo() As String
        Set(ByVal Value As String)
            strTabla = Value
        End Set
    End Property

    Public WriteOnly Property Consulta() As String
        Set(ByVal Value As String)
            strConsulta = Value
        End Set
    End Property

    Public Function RealizaConsulta() As OdbcDataReader
        If Not oLector Is Nothing Then Terminar()
        oConexion.Open()
        oComando.Connection = oConexion
        oComando.CommandText = strConsulta & " " & strTabla
        oLector = oComando.ExecuteReader
        Return oLector
    End Function

    Public Sub Terminar()
        If Not oLector Is Nothing Then oLector.Close()
        oConexion.Close()
        oLector = Nothing
    End Sub

    Public Sub New()
        oConexion = New OdbcConnection
        oComando = New OdbcCommand
    End Sub

    Protected Overrides Sub Finalize()
        If Not oLector Is Nothing Then Terminar()
        oComando = Nothing
        oConexion = Nothing
        MyBase.Finalize()
    End Sub

End Class
