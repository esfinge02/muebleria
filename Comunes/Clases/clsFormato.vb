Imports Dipros.Windows.Forms

Public Class clsFormato

#Region "Bodegas"

    Public Shared Sub for_bodegas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Bodega", "bodega", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "nombre_sucursal", 1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Descripci�n", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 300)
        'AddColumns(oGrid, "Contrase�a", "contrase�a", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "M�vil", "movil", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodega Principal", "bodega_principal", 4, DevExpress.Utils.FormatType.None, "")

    End Sub

    Public Shared Sub for_bodegas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Sucursal", "nombre_sucursal", 0, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oLookup, "Bodega", "bodega", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oLookup, "M�vil", "movil", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Bodega Principal", "bodega_principal", 4, DevExpress.Utils.FormatType.None, "")
    End Sub

    Public Shared Sub for_bodegas_traspasos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Sucursal", "nombre_sucursal", 0, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oLookup, "Bodega", "bodega", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "M�vil", "movil", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Bodega Principal", "bodega_principal", 4, DevExpress.Utils.FormatType.None, "")
    End Sub

#End Region

#Region "Departamentos"

    Public Shared Sub for_departamentos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Departamento", "departamento", 0, DevExpress.Utils.FormatType.None, "", 110)
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 400)
    End Sub


    Public Shared Sub for_departamentos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Departamento", "departamento", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
    End Sub

#End Region

#Region "GruposArticulos"

    Public Shared Sub for_grupos_articulos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Departamento", "departamento", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Clave", "grupo", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oGrid, "Grupo", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Departamento", "nombre_departamento", 2, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Factor de Ganancia", "factor_ganancia", 5, DevExpress.Utils.FormatType.None, "", 110)
        AddColumns(oGrid, "Factor de Ganancia Expo", "factor_ganancia_expo", 6, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Vendedor", "nombre_vendedor", 7, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

    Public Shared Sub for_grupos_articulos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Grupo", "grupo", 1, DevExpress.Utils.FormatType.None, "", 80)
        'AddColumns(oLookup, "Departamento", "departamento", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 3, DevExpress.Utils.FormatType.None, "", 200)
        'AddColumns(oLookup, "Descuento", 4, "descuento", DevExpress.Utils.FormatType.None, "")
        'AddColumns(oLookup, "Identificado", 5, "identificado", DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Factor de Ganancia", "factor_ganancia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Factor de Ganancia Expo", "factor_ganancia_expo", -1, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "GruposArticulosDescuentos"

    Public Sub for_grupos_articulos_descuentos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Grupo", "grupo", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Precio", "precio", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Porcentaje Descuento", "porcentaje_descuento", 2, DevExpress.Utils.FormatType.None, "")
    End Sub

#End Region

#Region "Articulos"

    Public Shared Sub for_articulos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Departamento", "nombre_departamento", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Grupo", "nombre_grupo", 1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Modelo", "modelo", 2, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Descripci�n", "descripcion_corta", 3, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Art�culo", "articulo", 4, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "C�digo De Barras", "codigo_barras", 5, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Unidad", "nombre_unidad", 6, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oGrid, "Proveedor", "nombre_proveedor", 7, DevExpress.Utils.FormatType.None, "", 150)

        AddColumns(oGrid, "Importaci�n", "importacion", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descontinuado", "descontinuado", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Maneja Series", "maneja_series", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Entrega A Domicilio", "entrega_domicilio", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Impuesto", "impuesto", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Impuesto Suntuario", "impuesto_suntuario", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "�ltimo Costo", "ultimo_costo", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "�ltima Compra", "ultima_compra", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Precio De Lista", "precio_lista", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cajas", "cajas", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "No Resurtir", "no_resurtir", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Pedimento", "pedimento", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Aduana", "aduana", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Requiere Armado", "requiere_armado", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha Importaci�n", "fecha_importacion", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tama�o Etiquetas Precios", "tamanio_etiquetas_precios", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Capacidad", "capacidad", -1, DevExpress.Utils.FormatType.None, "")


    End Sub

    Public Shared Sub for_articulos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Art�culo", "articulo", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "Modelo", "modelo", 1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oLookup, "Descripci�n", "descripcion_corta", 2, DevExpress.Utils.FormatType.None, "", 500)
        AddColumns(oLookup, "C�digo de Barras", "codigo_barras", -1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Departamento", "departamento", -1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Grupo", "grupo", -1, DevExpress.Utils.FormatType.None, "", 100)
    End Sub

    Public Shared Sub for_articulos_ubicaciones_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Art�culo", "articulo", 0, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Descripci�n", "descripcion_corta", 1, DevExpress.Utils.FormatType.None, "", 200)
        'AddColumns(oLookup, "Bodega", "bodega", 2, DevExpress.Utils.FormatType.None, "", 90)
        'AddColumns(oLookup, "Ubicaci�n", "ubicacion", 3, DevExpress.Utils.FormatType.None, "", 90)
        'AddColumns(oLookup, "Ubicaci�n", "n_ubicacion", 4, DevExpress.Utils.FormatType.None, "", 120)

    End Sub
#End Region

#Region "Unidades"
    Public Shared Sub for_unidades_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Unidad", "unidad", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Descripci�n", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 200)
    End Sub

#End Region

#Region "Proveedores"

    Public Shared Sub for_proveedores_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Clave", "proveedor", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Proveedor", "nombre", 1, DevExpress.Utils.FormatType.None, "", 280)
        AddColumns(oGrid, "Cuenta", "cuenta", 2, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "RFC", "rfc", 3, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Domicilio", "domicilio", 4, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Colonia", "colonia", 5, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Ciudad", "ciudad", 6, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Estado", "estado", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cp", "cp", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fonos", "telefonos", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Notas", "notas", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Contactos", "contactos", -1, DevExpress.Utils.FormatType.None, "")
    End Sub

    Public Shared Sub for_proveedores_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Proveedor", "proveedor", 1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 2, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oLookup, "RFC", "rfc", 3, DevExpress.Utils.FormatType.None, "", 150)

    End Sub

#End Region

#Region "Precios"
    Public Shared Sub for_precios_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Precio", "precio", 1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 2, DevExpress.Utils.FormatType.None, "", 120)

    End Sub
#End Region

#Region "ArticulosBodegas"

    Public Shared Sub for_articulos_bodegas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Art�culo", "articulo", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodega", "bodega", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Ubicaci�n", "ubicacion", 1, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "Clientes"

    Public Shared Sub for_clientes_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Cliente", "cliente", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Rfc", "rfc", 2, DevExpress.Utils.FormatType.None, "", 110)

        AddColumns(oGrid, "Aval", "aval", 3, DevExpress.Utils.FormatType.None, "", 110)

        AddColumns(oGrid, "Curp", "curp", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Domicilio", "domicilio", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cp", "cp", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Colonia", "nombre_colonia", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Municipio", "nombre_municipio", 7, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Estado", "nombre_estado", 8, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Activo", "activo", 9, DevExpress.Utils.FormatType.None, "")


    End Sub

    Public Shared Sub for_clientes_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Cliente", "cliente", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oLookup, "RFC", "rfc", 2, DevExpress.Utils.FormatType.None, "", 100)
        'AddColumns(oLookup, "Curp", "curp", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oLookup, "Domicilio", "domicilio", -1, DevExpress.Utils.FormatType.None, "")
        
    End Sub

    Public Shared Sub for_clientes_caja_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Cliente", "cliente", 0, DevExpress.Utils.FormatType.None, "", 65)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oLookup, "Apellidos Nombre", "apellidos_nombre", 2, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oLookup, "RFC", "rfc", 3, DevExpress.Utils.FormatType.None, "", 100)
    End Sub


    Public Shared Sub for_clientes_repartos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Cliente", "cliente", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "")
    End Sub

    Public Shared Sub for_tipos_vales_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Tipo Vale", "tipo", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
    End Sub
#End Region

#Region "cobradores"
    Public Shared Sub for_cobradores_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Cobrador", "cobrador", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Comisi�n", "comision", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Sucursal", "nombre_sucursal", 2, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

#Region "Convenios"
    Public Shared Sub for_convenios_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Convenio", "convenio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub
    Public Shared Sub for_convenios_ventas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Convenio", "convenio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre_convenio", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub
#End Region

#Region "vendedores"
    Public Shared Sub for_vendedores_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Vendedor", "vendedor", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oLookup, "Sueldo", "sueldo", -1, DevExpress.Utils.FormatType.None, "", 100)

    End Sub
#End Region

#Region "Empleados"
    Public Shared Sub for_empleados_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Empleado", "empleado", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Baja", "baja", 2, DevExpress.Utils.FormatType.None, "")
    End Sub

    Public Shared Sub for_empleados_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Empleado", "empleado", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Nombre", "nombre", 2, DevExpress.Utils.FormatType.None, "", 300)
    End Sub
#End Region

#Region "Sucursales"
    Public Shared Sub for_sucursales_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Sucursal", "sucursal", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Direcci�n", "direccion", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Colonia", "colonia", 3, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Cp", "cp", 4, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Ciudad", "ciudad", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Estado", "estado", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono", "telefono", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Gerente", "gerente", 8, DevExpress.Utils.FormatType.None, "", 200)
    End Sub

    Public Shared Sub for_sucursales_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Sucursal", "sucursal", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 2, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

    Public Shared Sub for_series_ventas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Serie", "serie", 1, DevExpress.Utils.FormatType.None, "")

    End Sub

    Public Shared Sub for_folio_ventas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Folio", "folio", 1, DevExpress.Utils.FormatType.None, "")
    End Sub


    Public Shared Sub for_opciones_cancelacion_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Opci�n", "opcion", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 305)
    End Sub


#End Region

#Region "Conceptos Inventario"
    Public Shared Sub for_conceptos_inventario_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Concepto", "concepto", 1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Descripci�n", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 250)

    End Sub

#End Region

#Region "Movimientos Inventarios"

    Public Shared Sub for_movimientos_inventarios_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Concepto", "concepto", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio", "folio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Concepto Referencia", "concepto_referencia", 2, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Folio Referencia", "folio_referencia", 3, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Usuario", "nombre_usuario", 4, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Observaciones", "observaciones", 5, DevExpress.Utils.FormatType.None, "", 360)

    End Sub

#End Region

    Public Shared Sub for_entradas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Entrada", "entrada", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.None, "", 80)
    End Sub

#Region "cajeros"
    Public Shared Sub for_cajeros_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Cajero", "cajero", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 200)
    End Sub
#End Region

    Public Shared Sub for_usuarios_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Usuario", "usuario", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Grupo", "grupo", 2, DevExpress.Utils.FormatType.None, "", 100)
    End Sub

    Public Shared Sub for_repartos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        oLookup.PopupWidth = 500
        AddColumns(oLookup, "Reparto", "reparto", 0, DevExpress.Utils.FormatType.None, "", 65)
        AddColumns(oLookup, "Fecha de Reparto", "fecha", 1, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 115)
        AddColumns(oLookup, "Sucursal", "sucursal_nombre", 2, DevExpress.Utils.FormatType.None, "", 65)
        AddColumns(oLookup, "Serie", "serie_factura", 3, DevExpress.Utils.FormatType.None, "", 50)
        AddColumns(oLookup, "Factura", "folio_factura", 4, DevExpress.Utils.FormatType.None, "", 65)
        AddColumns(oLookup, "Fecha de Factura", "fecha_factura", 5, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 115)

    End Sub

#Region "Usuarios Almacen"
    Public Shared Sub for_usuarios_almacen_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Usuario", "usuario", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre_usuario", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Bodega", "bodega", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodega", "bodega_descripcion", 2, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Permitir Todas las Bodegas", "permitir_todas", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Porcentaje Condonaci�n Intereses", "porcentaje_condonacion_intereses", 4, DevExpress.Utils.FormatType.None, "", 240)
        AddColumns(oGrid, "Abogado", "abogado", -1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Abogado", "nombre_abogado", 5, DevExpress.Utils.FormatType.None, "", 250)

    End Sub

#End Region

#Region "Puntos de Venta"

    Public Shared Sub for_puntos_venta_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Punto de Venta", "puntoventa", 0, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 150)
        'AddColumns(oLookup, "Serie", "serie", 2, DevExpress.Utils.FormatType.None, "", 50)
        'AddColumns(oLookup, "Folio", "folio", 3, DevExpress.Utils.FormatType.None, "", 80)
    End Sub

#End Region

#Region "Articulos Precios"
    Public Shared Sub for_articulos_precios_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Precio", "nombre", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Precio de Venta", "precio_venta", 1, DevExpress.Utils.FormatType.None, "", 100)
    End Sub
#End Region

#Region "Planes Credito"
    Public Shared Sub for_planes_credito_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Plan", "plan_credito", 0, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oLookup, "Plazo", "plazo", 2, DevExpress.Utils.FormatType.None, "", 60)
        AddColumns(oLookup, "Interes", "interes", 3, DevExpress.Utils.FormatType.None, "", 60)
        AddColumns(oLookup, "Tipo Venta", "tipo_venta", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Tipo Plazo", "tipo_plazo", 4, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Vigente", "vigente", 5, DevExpress.Utils.FormatType.None, "", 80)

    End Sub


#End Region

#Region "Concepto CXC"
    Public Shared Sub for_conceptos_cxc_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Tipo", "tipo", 2, DevExpress.Utils.FormatType.None, "", 50)

    End Sub

#End Region

#Region "Cajas"
    Public Shared Sub for_cajas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Caja", "caja", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 250)
        'AddColumns(oLookup, "Serie de Recibo", "serie_recibo", 2, DevExpress.Utils.FormatType.None, "", 100)
        'AddColumns(oLookup, "Folio de Recibo", "folio_recibo", 3, DevExpress.Utils.FormatType.None, "", 90)
    End Sub

#End Region

#Region "Nota de Cr�dito"
    Public Shared Sub for_notas_credito_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Sucursal", "nombre_sucursal", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Concepto", "descripcion_concepto", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Serie", "serie", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Folio", "folio", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Cliente", "nombre_cliente", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Documentos", "documentos", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Abono", "abono", 7, DevExpress.Utils.FormatType.None, "")



    End Sub

#End Region

#Region "Nota de Cargo"
    Public Shared Sub for_notas_cargo_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Sucursal", "nombre_sucursal", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Concepto", "descripcion_concepto", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Serie", "serie", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Folio", "folio", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Cliente", "nombre_cliente", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Documentos", "documentos", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Abono", "abono", 7, DevExpress.Utils.FormatType.None, "")



    End Sub

#End Region

#Region "Ventas"
    Public Shared Sub for_ventas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "n_sucursal", 0, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Serie", "serie", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Folio", "folio", 2, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Fecha", "fecha", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Vendedor", "vendedor", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Vendedor", "n_vendedor", 4, DevExpress.Utils.FormatType.None, "", 160)
        AddColumns(oGrid, "Cliente", "cliente", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cliente", "n_cliente", 5, DevExpress.Utils.FormatType.None, "", 160)
        AddColumns(oGrid, "Domicilio", "domicilio", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Colonia", "colonia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Ciudad", "ciudad", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cp", "cp", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Estado", "estado", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono1", "telefono1", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tel�fono2", "telefono2", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fax", "fax", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Curp", "curp", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "RFC", "rfc", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Tipo de Venta", "tipoventa", 6, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oGrid, "Ivadesglosado", "ivadesglosado", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Importe", "importe", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Menos", "menos", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Enganche", "enganche", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Interes", "interes", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Subtotal", "subtotal", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Impuesto", "impuesto", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Total", "total", 7, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00", 90)
        'AddColumns(oGrid, "Plan", "plan", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Plan", "n_plan", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Fecha_Primer_Documento", "fecha_primer_documento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Numero_Documentos", "numero_documentos", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Importe_Documentos", "importe_documentos", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        'AddColumns(oGrid, "Importe_Ultimo_Documento", "importe_ultimo_documento", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        'AddColumns(oGrid, "Enganche en Documento", "enganche_en_documento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Fecha_Enganche_Documento", "fecha_enganche_documento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Importe_Enganche_Documento", "importe_enganche_documento", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        'AddColumns(oGrid, "Liquida_Vencimiento", "liquida_vencimiento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Monto_Liquida_Vencimiento", "monto_liquida_vencimiento", -1, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        'AddColumns(oGrid, "Pedimento", "pedimento", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Aduana", "aduana", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Fecha de Importaci�n", "fecha_importacion", -1, DevExpress.Utils.FormatType.None, "")
        'AddColumns(oGrid, "Reparto", "reparto", -1, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

#Region "Dependencias"
    Public Shared Sub for_dependencias_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Convenio", "dependencia", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
    End Sub
#End Region

#Region "Plantilla"
    Public Shared Sub for_plantilla_trabajadores_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Clave Identificaci�n", "clave_identificacion", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Trabajador", "clave_empleado", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oLookup, "RFC", "rfc", 2, DevExpress.Utils.FormatType.None, "", 100)
    End Sub
    Public Shared Sub for_plantilla_reportes_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Reporte", "reporte", -1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oLookup, "predeterminado", "predeterminado", -1, DevExpress.Utils.FormatType.None, "", 100)
    End Sub
#End Region

    Public Shared Sub for_tama�o_etiqueta_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Tama�o", "tamanio", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 300)
    End Sub


#Region "cat_personal"
    Public Shared Sub for_personal_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Personal", "personal", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "")

    End Sub
#End Region

#Region "cat_centros_servicio"
    Public Shared Sub for_centros_servicio_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Centro", "centro", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Domicilio", "domicilio", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Contactos", "contactos", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Tel�fonos", "telefonos", 4, DevExpress.Utils.FormatType.None, "")

    End Sub
#End Region

#Region "Facturas"

    Public Shared Sub for_ventas_facturas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Sucursal", "nombre_sucursal", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Serie", "serie", 1, DevExpress.Utils.FormatType.None, "", 40)
        AddColumns(oLookup, "Folio", "folio", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Modelo", "Modelo", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 5, DevExpress.Utils.FormatType.None, "", 250)
    End Sub

#End Region

#Region "Municipios"

    Public Shared Sub for_municipios_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Municipio", "municipio", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Estado", "estado", -1, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Estado", "nombre_estado", 2, DevExpress.Utils.FormatType.None, "", 240)
    End Sub
    Public Shared Sub for_municipios_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Municipio", "municipio", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Municipio", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 240)
        AddColumns(oLookup, "Estado", "estado", -1, DevExpress.Utils.FormatType.None, "", 240)
        AddColumns(oLookup, "Estado", "nombre_estado", 2, DevExpress.Utils.FormatType.None, "", 240)

    End Sub
#End Region

#Region "cat_ubicaciones"
    Public Shared Sub for_ubicaciones_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Bodega", "bodega", 1, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oLookup, "Ubicaci�n", "ubicacion", 2, DevExpress.Utils.FormatType.None, "", 90)
        AddColumns(oLookup, "Descripci�n", "descripcion", 3, DevExpress.Utils.FormatType.None, "", 120)

    End Sub
#End Region

#Region "Choferes"
    Public Shared Sub for_choferes_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Chofer", "chofer", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oLookup, "Veh�culo", "vehiculo", 2, DevExpress.Utils.FormatType.None, "", 180)
        AddColumns(oLookup, "Placas", "placas", 3, DevExpress.Utils.FormatType.None, "", 80)
    End Sub
#End Region

#Region "Ciudades"

    Public Shared Sub for_ciudades_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Localidad", "ciudad", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Municipio", "nombre_municipio", 2, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Municipio", "municipio", -1, DevExpress.Utils.FormatType.None, "", 100)
    End Sub

#End Region

#Region "SolicitudTraspasos"

    Public Shared Sub for_solicitud_traspasos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Solicitud", "solicitud", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Bodega Origen", "bodega_salida", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Sucursal Destino", "nombre_sucursal_entrada", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Bodega Destino", "bodega_entrada", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Solicita", "nombre_solicita", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Captur�", "capturo", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha De Solicitud", "fecha_solicitud", 6, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oLookup, "Estatus", "estatus_vista", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Folio De Traspaso", "folio_traspaso", 8, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

#Region "Colonias"

    Public Shared Sub for_colonias_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Colonia", "colonia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "localidad", "ciudad", -1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Localidad", "nombre_ciudad", 2, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Municipio", "municipio", -1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Municipio", "nombre_municipio", 2, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "C�digo Postal", "codigo_postal", 3, DevExpress.Utils.FormatType.None, "", 100)
    End Sub

#End Region

#Region "Bancos"
    Public Shared Sub for_bancos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Banco", "banco", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Sucursal", "sucursal", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Direcci�n", "direccion", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Ejecutivo Cuenta", "ejecutivo_cuenta", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Gerente", "gerente", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Tel�fono 1", "tel1", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Tel�fono 2", "tel2", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fax", "fax", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Cuenta", "cuenta", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Observaciones", "observaciones", -1, DevExpress.Utils.FormatType.None, "")

    End Sub
#End Region

#Region "Chequeras"
    Public Shared Sub for_chequeras_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Banco", "nombre_banco", 0, DevExpress.Utils.FormatType.None, "", 155)
        AddColumns(oLookup, "Cuenta Bancaria", "cuenta_bancaria", 1, DevExpress.Utils.FormatType.Numeric, "", 135)
        AddColumns(oLookup, "Predeterminada", "predeterminada", 4, DevExpress.Utils.FormatType.None, "", 135)
        AddColumns(oLookup, "Cuenta Contable", "cuenta_contable", -1, DevExpress.Utils.FormatType.None, "", 180)
        AddColumns(oLookup, "Descripci�n de cuenta", "descripcion_cuenta", -1, DevExpress.Utils.FormatType.None, "", 200)
    End Sub
#End Region

#Region "Chequeras"
    Public Shared Sub for_tipos_depositos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Tipo", "Tipo_deposito", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Descripcion", "Descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
        'AddColumns(oLookup, "Predeterminada", "predeterminada", 4, DevExpress.Utils.FormatType.None, "", 135)

    End Sub

    Public Shared Sub for_tipos_depositos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Tipo", "Tipo_deposito", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Descripcin", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

#Region "Chequeras"
    Public Shared Sub for_cuentascontables_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "cuentacontable", "cuentacontable", 0, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
    End Sub
#End Region


#Region "Concepto CXP"
    Public Shared Sub for_conceptos_cxp_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Tipo", "tipo", 2, DevExpress.Utils.FormatType.None, "", 50)
        AddColumns(oLookup, "Folio", "folio", 3, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "Movimientos por Pagar"
    Public Shared Sub for_movimientos_pagar_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Folio", "folio", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Proveedor", "proveedor", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Entrada", "entrada", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", 4, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

#Region "PuntosVenta"

    Public Shared Sub for_puntos_venta_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Punto Venta", "puntoventa", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Serie", "serie", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio", "folio", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "sucursal", 4, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "Estados"
    Public Shared Sub for_estados_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Estado", "estado", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

#Region "Abogados"
    Public Shared Sub for_abogados_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Abogado", "abogado", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

#Region "Cotizaciones"
    Public Shared Sub for_cotizaciones_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Cotizaci�n", "cotizacion", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Sucursal", "sucursal", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Cliente", "cliente", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Observaciones", "observaciones", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Direcci�n", "direccion", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Sucursal Venta", "sucursal_venta", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Serie Venta", "serie_venta", 8, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Folio Venta", "folio_venta", 9, DevExpress.Utils.FormatType.None, "")
    End Sub

#End Region


    Public Shared Sub for_descuentos_especiales_clientes_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Folio", "folio_descuento", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Cliente", "nombre_cliente", 1, DevExpress.Utils.FormatType.None, "", 160)
    End Sub

    Public Shared Sub for_paquetes_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Paquete", "paquete", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 160)
    End Sub


#Region "Chequeras"
    Public Shared Sub for_acreedores_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Acreedor", "acreedor", 0, DevExpress.Utils.FormatType.None, "", 155)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.Numeric, "", 135)
        AddColumns(oLookup, "RFC", "rfc", 4, DevExpress.Utils.FormatType.None, "", 135)
    End Sub
#End Region


#Region "P�lizas"
    Public Shared Sub for_polizas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "P�liza", "poliza", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.Numeric, "", 235)
        AddColumns(oLookup, "Script", "script", -1, DevExpress.Utils.FormatType.None, "", 135)
    End Sub
#End Region


#Region "Documentos Cobrar Cliente"
    Public Shared Sub for_documentos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", -1, DevExpress.Utils.FormatType.Numeric, "")
        AddColumns(oLookup, "Partida", "partida", -1, DevExpress.Utils.FormatType.Numeric, "")
        AddColumns(oLookup, "Cliente", "cliente", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Importe", "importe", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Sucursal Referencia", "sucursal_referencia", -1, DevExpress.Utils.FormatType.Numeric, "")
        AddColumns(oLookup, "Concepto Referencia", "concepto_referencia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Serie Referencia", "serie_referencia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Folio Referencia", "folio_referencia", -1, DevExpress.Utils.FormatType.Numeric, "")
        AddColumns(oLookup, "Cliente Referencia", "cliente_referencia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Documento Referencia", "documento_referencia", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha Vencimiento", "fecha_vencimiento", -1, DevExpress.Utils.FormatType.None, "")



        AddColumns(oLookup, "Envio", "envio_despliega", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Concepto", "concepto", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Serie", "serie", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Folio", "folio", 4, DevExpress.Utils.FormatType.Numeric, "")
        AddColumns(oLookup, "Documento", "documento_despliega", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Saldo", "saldo", 6, DevExpress.Utils.FormatType.Numeric, "c2")

    End Sub
#End Region


#Region "TiposLlamadas"

    Public Sub for_tipos_llamadas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Tipo de Llamada", "tipo_llamada", 0, DevExpress.Utils.FormatType.Numeric, "", 90)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub
    Public Shared Sub for_tipos_llamadas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Tipo de Llamada", "tipo_llamada", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.Numeric, "", 200)

    End Sub

#End Region

#Region "Parentescos"

    Public Sub for_parentescos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Clave", "parentesco", 0, DevExpress.Utils.FormatType.Numeric, "", 90)
        AddColumns(oGrid, "Parentesco", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub
    Public Shared Sub for_parentescos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Clave", "parentesco", 0, DevExpress.Utils.FormatType.Numeric, "", 100)
        AddColumns(oLookup, "Parentesco", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub
#End Region

#Region "Mensajes Dejados"

    Public Sub for_mensaje_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "mensaje_dejado", 0, DevExpress.Utils.FormatType.Numeric, "", 90)
        AddColumns(oGrid, "Tipo de Llamada", "desc_llamada", 1, DevExpress.Utils.FormatType.Numeric, "", 200)
        AddColumns(oGrid, "Mensaje", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 200)

    End Sub
    Public Shared Sub for_mensaje_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Folio", "mensaje_dejado", 0, DevExpress.Utils.FormatType.Numeric, "", 90)
        AddColumns(oLookup, "Tipo de Llamada", "desc_llamada", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Mensaje", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 200)

    End Sub



#End Region

#Region "Telefonos llamadas caja"

    Public Shared Sub for_telefono_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Telefono", "tel", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Tipo", "tipo", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region


    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	CorrespondenciaClientes
    'DATE:		01/02/2007 00:00:00 a.m.
    '----------------------------------------------------------------------------------------------------------------

#Region "CorrespondenciaClientes"
    Public Shared Sub for_correspondencia_clientes_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Correspondencia", "correspondencia", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region


#Region "Depositos Bancarios"
    Public Shared Sub for_depositos_bancarios_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 0, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Consecutivo", "consecutivo", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Banco", "banco", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Chequera", "chequera", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio", "folio_movimiento_chequera", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Depositante", "depositante", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Importe", "importe", 6, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Aplicado", "aplicado_movimiento_chequera", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Insert�", "usuario_inserto", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Aplic�", "usuario_aplico", -1, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

#Region "Conceptos Movimientos Chequera"
    Public Shared Sub for_conceptos_movimientos_chequera_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "")

    End Sub
#End Region



    Public Shared Sub for_ordenes_servicio_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Orden de Servicio", "orden_servicio", 0, DevExpress.Utils.FormatType.None, "", 110)
        AddColumns(oLookup, "Articulo", "articulo", 1, DevExpress.Utils.FormatType.None, "", 130)
        AddColumns(oLookup, "Tipo Servicio", "tipo_servicio_vista", 2, DevExpress.Utils.FormatType.None, "", 130)
        AddColumns(oLookup, "Estatus", "estatus_vista", 3, DevExpress.Utils.FormatType.None, "", 130)
    End Sub

#Region "Vistas Salidas"

    Public Shared Sub for_vistas_salidas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio Salida", "folio_vista_salida", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "sucursal", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Fecha_Devolucion", "fecha_devolucion", 3, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Cliente", "cliente", 4, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Persona_Recibe", "persona_recibe", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodeguero", "bodeguero", 7, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Vista Entregada", "vista_entregada", 8, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Usuario", "nombre_usuario", 9, DevExpress.Utils.FormatType.None, "", 120)

        'AddColumns(oGrid, "Observaciones", "observaciones", 9, DevExpress.Utils.FormatType.None, "")
    End Sub

    Public Shared Sub for_vistas_salidas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Folio_Vista_Salida", "folio_vista_salida", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Sucursal", "sucursal", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Fecha", "fecha", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Cliente", "cliente", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Persona_Recibe", "persona_recibe", 5, DevExpress.Utils.FormatType.None, "")
    End Sub
#End Region

#Region "VistasEntradas"

    Public Shared Sub for_vistas_entradas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio Entrada", "folio_vista_entrada", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio Salida", "folio_vista_salida", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "sucursal", 2, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Fecha", "fecha", 3, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Cliente", "cliente", 4, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Persona Devuelve", "persona_devuelve", 5, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Usuario", "nombre_usuario", 6, DevExpress.Utils.FormatType.None, "", 120)

    End Sub

#End Region

#Region "Entradas y Salidas por Reparacion"

    Public Shared Sub for_entradas_salidas_reparacion_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Orden", "orden_servicio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 1, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Articulo", "articulo", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Modelo", "modelo", 2, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Descripci�n", "descripcion_corta", 4, DevExpress.Utils.FormatType.None, "", 150)
        AddColumns(oGrid, "Numero Serie", "numero_serie", 5, DevExpress.Utils.FormatType.None, "", 200)

        AddColumns(oGrid, "Fecha Salida", "fecha_salida_reparacion", 6, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Usuario Salida", "usuario_salida_reparacion", 7, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Fecha Entrada", "fecha_entrada_reparacion", 8, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Usuario Entrada", "usuario_entrada_reparacion", 9, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Estatus", "estatus", 10, DevExpress.Utils.FormatType.None, "", 200)


    End Sub
#End Region

    Public Shared Sub for_bodegueros_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Clave", "bodeguero", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Bodeguero", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
    End Sub


#Region "Cobradores"

    Public Shared Sub for_cobradores_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Cobrador", "cobrador", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Comisi�n", "comision", 2, DevExpress.Utils.FormatType.Numeric, "n2", 80)
        AddColumns(oGrid, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "nombre_sucursal", 3, DevExpress.Utils.FormatType.None, "", 250)

    End Sub

#End Region

#Region "CapturaVisitasClientes"

    Public Shared Sub for_captura_visitas_clientes_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio_captura", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cliente", "cliente", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Nombre Cliente", "nombre_cliente", 2, DevExpress.Utils.FormatType.None, "", 250)
        'AddColumns(oGrid, "Cobrador", "cobrador", 3, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Fecha Visita", "fecha", 4, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
    End Sub

#End Region

#Region "FormasPagos"

    Public Shared Sub for_formas_pagos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Forma de Pago", "forma_pago", 0, DevExpress.Utils.FormatType.None, "", 110)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Comisi�n", "comision", 2, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Maneja Dolares", "maneja_dolares", 3, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Predeterminada", "predeterminada", 4, DevExpress.Utils.FormatType.None, "", 100)

    End Sub

    Public Shared Sub for_formas_pagos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Forma de Pago", "forma_pago", 0, DevExpress.Utils.FormatType.None, "", 110)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 130)
        AddColumns(oLookup, "Comisi�n", "comision", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Maneja Dolares", "maneja_dolares", 3, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Predeterminada", "predeterminada", 4, DevExpress.Utils.FormatType.None, "", 100)

    End Sub

#End Region


#Region "Zonas"
    Public Shared Sub for_zonas_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Zona", "zona", 1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Descripci�n", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 200)
    End Sub

#End Region


#Region "DescuentosEspecialesProgramados"
    Public Shared Sub for_descuentos_especiales_programados_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio_descuento", 0, DevExpress.Utils.FormatType.Numeric, "", 80)
        AddColumns(oGrid, "Articulo", "articulo", 1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Plan Cr�dito", "plan_credito", 1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Fecha Creaci�n", "fecha_creacion", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 90)
        AddColumns(oGrid, "Precio Cr�dito", "precio_credito", 4, DevExpress.Utils.FormatType.Numeric, "c2", 100)
        AddColumns(oGrid, "Precio Contado", "precio_contado", 5, DevExpress.Utils.FormatType.Numeric, "c2", 100)

        AddColumns(oGrid, "Agotar Existencias", "agotar_existencias", 6, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Autorizar", "autorizar", 7, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Solicita", "solicita", 8, DevExpress.Utils.FormatType.None, "", 120)
        AddColumns(oGrid, "Activo", "activo", 9, DevExpress.Utils.FormatType.None, "", 80)
    End Sub

#End Region

#Region "Vales"
    Public Shared Sub for_vales_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio", 0, DevExpress.Utils.FormatType.Numeric, "", 80)
        AddColumns(oGrid, "Tipo Vale ", "tipo_vale", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 90)
        AddColumns(oGrid, "Cliente", "nombre_cliente", 3, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Importe", "importe", 4, DevExpress.Utils.FormatType.Numeric, "c2", 100)
        AddColumns(oGrid, "Saldo", "saldo", 5, DevExpress.Utils.FormatType.Numeric, "c2", 100)
        AddColumns(oGrid, "Vigencia", "vigencia", 6, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 90)
        AddColumns(oGrid, "Estatus", "estatus", 7, DevExpress.Utils.FormatType.None, "")
    End Sub

#End Region


#Region "Paquetes"
    Public Shared Sub for_paquetes_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Paquete", "paquete", 0, DevExpress.Utils.FormatType.Numeric, "n0", 80)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 240)
        AddColumns(oGrid, "Plan Cr�dito", "plan_credito", 2, DevExpress.Utils.FormatType.None, "", 160)
        AddColumns(oGrid, "Enganche", "enganche", 4, DevExpress.Utils.FormatType.None, "n2", 90)
        AddColumns(oGrid, "Contado", "precio_contado", 4, DevExpress.Utils.FormatType.None, "n2", 90)
        AddColumns(oGrid, "Credito", "precio_credito", 4, DevExpress.Utils.FormatType.None, "n2", 90)
    End Sub

#End Region

#Region "Identificadores Permisos"
    Public Shared Sub for_identificadores_permisos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "sistema", "sistema", -1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Identificador", "identificador", 0, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 240)
    End Sub

#End Region


    Public Shared Sub for_anticipos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Anticipo", "folio", 0, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oLookup, "Concepto", "concepto", 1, DevExpress.Utils.FormatType.None, "", 250)
    End Sub


#Region "Vendedores"

    Public Shared Sub for_vendedores_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Vendedor", "vendedor", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Sueldo", "sueldo", 3, DevExpress.Utils.FormatType.Numeric, "c2", 100)
        AddColumns(oGrid, "Meta", "meta", 4, DevExpress.Utils.FormatType.Numeric, "c2", 100)
        ' AddColumns(oGrid, "Saldo", "saldo", 5, DevExpress.Utils.FormatType.Numeric, "c2", 100)
        AddColumns(oGrid, "Sucursal", "nombre_sucursal", 2, DevExpress.Utils.FormatType.None, "c2", 130)
        AddColumns(oGrid, "Esquema Comisiones", "esquema_comisiones", 6, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region


    Public Shared Sub for_esquemas_comisiones_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Esquema", "esquema", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Nombre", "nombre", 2, DevExpress.Utils.FormatType.None, "", 200)

    End Sub



#Region "Tipos Direccion"
    Public Shared Sub for_tipos_direccion_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Tipo", "tipo_direccion", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

End Class
