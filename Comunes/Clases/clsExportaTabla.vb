Imports System.Data.Odbc
Imports System.IO

Public Class clsExportaTabla

    Private strTabla As String
    Private strDirectorio As String
    Private oConexion As OdbcConnection
    Private oComando As OdbcCommand
    Private oRenglones As ArrayList

    Private Function CadenaCreacion() As String
        Dim Cadena As String
        Dim oRenglon As clsExportaRenglon
        Dim oPrimeraVez As Boolean = True
        For Each oRenglon In oRenglones
            If oPrimeraVez Then
                oPrimeraVez = False
            Else
                Cadena = Cadena & ", "
            End If
            Cadena = Cadena & oRenglon.DefineRenglon
        Next
        Return String.Format("CREATE TABLE '{2}\{0}' ({1})", strTabla, Cadena, strDirectorio)
    End Function

    Private Function CadenaInsercion() As String
        Dim Campos, Valores As String
        Dim oRenglon As clsExportaRenglon
        Dim bPrimeraVez As Boolean = True
        Dim Contador As Integer = 0
        For Each oRenglon In oRenglones
            If bPrimeraVez Then
                bPrimeraVez = False
            Else
                Campos = Campos & ", "
                Valores = Valores & ", "
            End If
            Campos = Campos & oRenglon.Campo
            Valores = Valores & oRenglon.Formato(Contador)
            Contador += 1
        Next
        Return String.Format("INSERT INTO {0} ({1}) VALUES ({2})", strTabla, Campos, Valores)
    End Function

    Private Sub CreaTabla()
        Dim Comando As OdbcCommand
        Dim Archivo As New FileInfo(strDirectorio & "\" & strTabla & ".dbf")
        If Archivo.Exists Then
            Archivo.Delete()
        End If
        Comando = New OdbcCommand(CadenaCreacion, oConexion)
        Comando.ExecuteNonQuery()
        Comando = Nothing
        Archivo = Nothing
    End Sub

    Private Sub CreaComando()
        oComando = New OdbcCommand
        oComando.Connection = oConexion
    End Sub

    Public Sub Exporta(ByRef oRows() As DataRow)
        Dim oRow As DataRow
        Dim oRenglon As clsExportaRenglon
        Dim strFormatoInsercion As String
        Dim arrParametros(oRenglones.Count) As Object
        Dim iContador As Integer = 0
        oConexion.Open()
        CreaTabla()
        CreaComando()
        strFormatoInsercion = CadenaInsercion()
        For Each oRow In oRows
            iContador = 0
            For Each oRenglon In oRenglones
                arrParametros(iContador) = oRow(oRenglon.Origen)
                iContador += 1
            Next
            oComando.CommandText = String.Format(strFormatoInsercion, arrParametros)
            oComando.ExecuteNonQuery()
        Next
        oConexion.Close()
    End Sub

    Public Sub Renglon(ByVal Campo As String, ByVal Tipo As String, ByVal Longitud As Integer, ByVal Precision As Integer, ByVal Origen As String)
        Dim oRenglon As New clsExportaRenglon(Campo, Tipo, Longitud, Precision, Origen)
        oRenglones.Add(oRenglon)
    End Sub

    Public Sub New(ByVal Directorio As String, ByVal Tabla As String)
        If Not Directory.Exists(Directorio) Then
            Throw New Exception("No existe el directorio " & Directorio)
        End If
        oConexion = New OdbcConnection
        oRenglones = New ArrayList
        oConexion.ConnectionString = String.Format("BackgroundFetch=S�;Collate=Machine;Exclusive=No;UID=;SourceType=DBF;Driver=Microsoft Visual FoxPro Driver;SourceDB={0}", Directorio)
        strDirectorio = Directorio
        strTabla = Tabla
    End Sub

End Class
