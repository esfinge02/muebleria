'=============================================================
'Clases utilizadas para el manejo de las series de los
'Art�culos en las Formas
'=============================================================
#Region "Clases de Articulos"

Public Class clsArticulosSeries
    Private IdPartida As Long
    Private IdArticulo As String
    Private Series As clsSeries

#Region "Constructores"

    Sub New()
        Series = New clsSeries
        IdPartida = -1
        IdArticulo = -1
    End Sub

    Sub New(ByVal Partida As Long, ByVal Articulo As String)
        Series = New clsSeries
        IdPartida = Partida
        IdArticulo = Articulo
    End Sub


#End Region

#Region "Propiedades"

    Public ReadOnly Property Partida() As Long
        Get
            Return IdPartida
        End Get
    End Property

    Public ReadOnly Property Articulo() As String
        Get
            Return IdArticulo
        End Get
    End Property

    Public ReadOnly Property AllSeries() As clsSeries
        Get
            Return Series
        End Get
    End Property

#End Region

#Region "Funciones P�blicas"

    Public Sub AddSerie(ByVal Serie As String)
        Series.Add(Serie)
    End Sub

    Public Sub AddRange(ByVal RangeSeries As clsSeries)
        Series = RangeSeries
    End Sub

    Public Sub RemoveSeries()
        Series.RemoveALL()
    End Sub

#End Region

End Class

Public Class clsSeries
    Private AlstSerie As ArrayList

#Region "Contructores"

    Sub New()
        AlstSerie = New ArrayList
    End Sub

#End Region

#Region "Propiedades"

    Public ReadOnly Property SerieP(ByVal Index) As String
        Get
            Return AlstSerie.Item(Index)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Return AlstSerie.Count
        End Get
    End Property

#End Region

#Region "Funciones P�blicas"

    Public Sub Add(ByVal Serie As String)
        If Not Exist(Serie) Then
            AlstSerie.Add(Serie)
        End If
    End Sub

    Public Sub Remove(ByVal Serie As String)
        If Exist(Serie) Then
            AlstSerie.Remove(Serie)
        End If
    End Sub

    Public Sub RemoveALL()
        AlstSerie.Clear()
    End Sub

    Public Function Serie(ByVal Index As Long) As String

        Serie = AlstSerie.Item(Index)

    End Function



#End Region

#Region "Funciones Privadas"

    Private Function Exist(ByVal Serie As String) As Boolean
        Dim Index As Integer
        Index = AlstSerie.IndexOf(Serie)
        If Index = -1 Then
            Exist = False
        Else
            Exist = True
        End If

    End Function

#End Region



End Class



#End Region