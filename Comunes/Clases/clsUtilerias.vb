Imports System.Drawing
Imports System.Text.RegularExpressions

Public Class clsUtilerias
    Public Shared UsarSeries As Boolean = True

    Public Shared Function PreparaValorLookup(ByVal oLookup As Dipros.Editors.TINMultiLookup) As Long
        If oLookup.EditValue Is Nothing Then
            Return -1
        Else
            Return CType(oLookup.EditValue, Long)
        End If
    End Function
    Public Shared Function PreparaValorLookupBodega(ByVal oLookup As Dipros.Editors.TINMultiLookup) As String
        If oLookup.EditValue Is Nothing Then
            Return "-1"
        Else
            Return CType(oLookup.EditValue, String)
        End If
    End Function
    Public Shared Function PreparaValorLookupStr(ByVal oLookup As Dipros.Editors.TINMultiLookup) As String
        'If oLookup.EditValue Is Nothing Or IsDBNull(oLookup.EditValue) Then
        If oLookup.EditValue Is Nothing Then
            Return ""
        Else
            Return CType(oLookup.EditValue, String)
        End If
    End Function
    Public Shared Function PreparaValorLookupStrReporteSalidas(ByVal oLookup As Dipros.Editors.TINMultiLookup) As String
        'If oLookup.EditValue Is Nothing Or IsDBNull(oLookup.EditValue) Then
        If oLookup.EditValue Is Nothing Then
            Return "-1"
        Else
            Return CType(oLookup.EditValue, String)
        End If
    End Function
    Public Shared Function PreparaValorLookupStrReporteSalidas(ByVal oLookup As DevExpress.XtraEditors.ButtonEdit) As String
        'If oLookup.EditValue Is Nothing Or IsDBNull(oLookup.EditValue) Then
        If oLookup.EditValue Is Nothing Then
            Return "-1"
        Else
            Return CType(oLookup.EditValue, String)
        End If
    End Function
    Public Shared Function PreparaValorLookupStr(ByVal oLookup As DevExpress.XtraEditors.ButtonEdit) As String
        If oLookup.EditValue Is Nothing Then
            Return ""
        Else
            Return CType(oLookup.EditValue, String)
        End If
    End Function

    Public Shared Function ValidaMultiSeleccion(ByVal xml As String, ByVal tipo_lookup As String) As Dipros.Utils.Events
        Dim oevents As New Dipros.Utils.Events
        Dim cadena As String = Replace(Replace(Replace(Replace(Replace(xml, "<Raiz>", "", 1, 1, CompareMethod.Text), "</Raiz>", "", 1, 1), "<Seleccion>", "", 1, 1), "</Seleccion>", "", 1, 1), "<IdSeleccion />", "", 1, 1)

        If xml.Length = 0 Or xml = "<Raiz />" Then
            oevents.Message = "Al menos un(a) " & tipo_lookup & " es requerido(a)"
        End If
        Return oevents
    End Function
    Public Shared Function uti_ImageToByte(ByVal pichuella As System.Drawing.Image) As Byte()
        Dim ms As New System.IO.MemoryStream
        Dim data() As Byte
        If Not IsNothing(pichuella) Then
            pichuella.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
            ReDim data(ms.Length)
            ms.Position = 0
            ms.Read(data, 0, ms.Length)
        End If

        uti_ImageToByte = data

    End Function
    Public Shared Function uti_ByteToImage(ByVal huella As Byte()) As System.Drawing.Image
        Dim stmBLOBData As New System.IO.MemoryStream(huella)
        Dim imagen As System.Drawing.Image
        imagen = Image.FromStream(stmBLOBData)

        uti_ByteToImage = imagen

    End Function
    Public Shared Function uti_hisSeries(ByVal articulo As Long, ByVal numeroserie As String, ByVal tipo_movimiento As Char, ByVal bodega As String) As Boolean
        Dim clase As New VillarrealBusiness.clsUtilerias
        Dim oevent As New Dipros.Utils.Events
        oevent = clase.ChecarHis_Series(articulo, numeroserie, tipo_movimiento, bodega)
        uti_hisSeries = oevent.Value
    End Function

    Public Shared Function uti_BodegaDeUsuario(ByVal usuario As String) As String
        Dim oUsuarios As New VillarrealBusiness.clsUsuarios
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oUsuarios.SeleccionaUsuarioAlmacen(usuario)
        If Not oEvents.ErrorFound Then
            uti_BodegaDeUsuario = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("bodega"), String)
        End If

        oEvents = Nothing
        oUsuarios = Nothing
    End Function
    Public Shared Function uti_PermitirTodasBodegas(ByVal usuario As String) As Boolean
        Dim oUsuarios As New VillarrealBusiness.clsUsuarios
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oUsuarios.SeleccionaUsuarioAlmacen(usuario)
        If Not oEvents.ErrorFound Then
            uti_PermitirTodasBodegas = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("permitir_todas"), Boolean)
        End If

        oEvents = Nothing
        oUsuarios = Nothing

    End Function
    Public Shared Function GuardaArchivo(ByVal sucursal As Long)
        FileOpen(1, TinApp.Path & "\Sucursal_Actual.ini", OpenMode.Output, , OpenShare.Shared)
        'Write(1, CType(txtForm.Text, String).ToCharArray(0, txtForm.Text.Length))
        Try
            'Kill(TINApp.Path & "\Imagenes.ini")
            PrintLine(1, sucursal)
            'MsgBox("El archivo fue creado exitosamente!", MsgBoxStyle.Information, "Atenci�n")
        Catch ex As Exception

        Finally
            FileClose(1)
        End Try
    End Function
    Public Shared Function AbrirArchivo(ByRef sucursal As Long) As Boolean
        If Dir(TinApp.Path & "\Sucursal_Actual.ini") <> "" Then
            FileOpen(1, TinApp.Path & "\Sucursal_Actual.ini", OpenMode.Input) ' Open file.
            While Not EOF(1)   ' Loop until end of file.
                sucursal = LineInput(1)   ' Get characters.
            End While
            FileClose(1)
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function AbrirArchivoPV(ByRef puntoventa As Long) As Boolean
        If Dir(TinApp.Path & "\PuntoVenta_Actual.ini") <> "" Then
            FileOpen(1, TinApp.Path & "\PuntoVenta_Actual.ini", OpenMode.Input) ' Open file.
            While Not EOF(1)   ' Loop until end of file.
                puntoventa = LineInput(1)   ' Get characters.
            End While
            FileClose(1)
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function GuardaArchivoPV(ByVal puntoventa As Long)
        FileOpen(1, TinApp.Path & "\PuntoVenta_Actual.ini", OpenMode.Output, , OpenShare.Shared)
        Try
            PrintLine(1, puntoventa)
        Catch ex As Exception

        Finally
            FileClose(1)
        End Try
    End Function
    Public Shared Function AbrirArchivoCaja(ByRef caja As Long) As Boolean
        If Dir(TinApp.Path & "\Caja_Actual.ini") <> "" Then
            FileOpen(1, TinApp.Path & "\Caja_Actual.ini", OpenMode.Input) ' Open file.
            While Not EOF(1)   ' Loop until end of file.
                caja = LineInput(1)   ' Get characters.
            End While
            FileClose(1)
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function GuardaArchivoCaja(ByVal caja As Long)
        FileOpen(1, TinApp.Path & "\Caja_Actual.ini", OpenMode.Output, , OpenShare.Shared)
        Try
            PrintLine(1, caja)
        Catch ex As Exception

        Finally
            FileClose(1)
        End Try
    End Function
    Public Shared Function AbrirArchivoBodega(ByRef bodega As String) As Boolean
        If Dir(TinApp.Path & "\Bodega_Actual.ini") <> "" Then
            FileOpen(1, TinApp.Path & "\Bodega_Actual.ini", OpenMode.Input) ' Open file.
            While Not EOF(1)   ' Loop until end of file.
                bodega = LineInput(1)   ' Get characters.
            End While
            FileClose(1)
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function GuardaArchivoBodega(ByVal bodega As String)
        FileOpen(1, TinApp.Path & "\Bodega_Actual.ini", OpenMode.Output, , OpenShare.Shared)
        Try
            PrintLine(1, bodega)
        Catch ex As Exception

        Finally
            FileClose(1)
        End Try
    End Function
    Public Shared Function NombreMes(ByVal mes As Integer) As String
        Select Case mes
            Case 1
                NombreMes = "Enero"
            Case 2
                NombreMes = "Febrero"
            Case 3
                NombreMes = "Marzo"
            Case 4
                NombreMes = "Abril"
            Case 5
                NombreMes = "Mayo"
            Case 6
                NombreMes = "Junio"
            Case 7
                NombreMes = "Julio"
            Case 8
                NombreMes = "Agosto"
            Case 9
                NombreMes = "Septiembre"
            Case 10
                NombreMes = "Octubre"
            Case 11
                NombreMes = "Noviembre"
            Case 12
                NombreMes = "Diciembre"

        End Select
    End Function

    Public Shared Function uti_FolioPV(ByVal puntoventa As Long) As String
        Dim oPuntosVenta As New VillarrealBusiness.clsPuntosVenta
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oPuntosVenta.DespliegaDatos(puntoventa)
        If Not oEvents.ErrorFound Then
            uti_FolioPV = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("folio"), Long) + 1
        End If

        oEvents = Nothing
        oPuntosVenta = Nothing
    End Function
    Public Shared Function uti_SeriePV(ByVal puntoventa As Long) As String
        Dim oPuntosVenta As New VillarrealBusiness.clsPuntosVenta
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oPuntosVenta.DespliegaDatos(puntoventa)
        If Not oEvents.ErrorFound Then
            uti_SeriePV = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("serie"), String)
        End If

        oEvents = Nothing
        oPuntosVenta = Nothing
    End Function
    Public Shared Function uti_SerieCajasRecibo(ByVal caja As Long) As String
        Dim oCajas As New VillarrealBusiness.clsCajas
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oCajas.DespliegaDatos(caja)
        If Not oEvents.ErrorFound Then
            uti_SerieCajasRecibo = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("serie_recibo"), String)
        End If

        oEvents = Nothing
        oCajas = Nothing
    End Function
    Public Shared Function uti_SerieCajasNotaCredito(ByVal caja As Long) As String
        Dim oCajas As New VillarrealBusiness.clsCajas
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oCajas.DespliegaDatos(caja)
        If Not oEvents.ErrorFound Then
            uti_SerieCajasNotaCredito = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("serie_nota_credito"), String)
        End If

        oEvents = Nothing
        oCajas = Nothing
    End Function
    Public Shared Function uti_SerieCajaNotaCargo(ByVal caja As Long) As String
        Dim oCajas As New VillarrealBusiness.clsCajas
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oCajas.DespliegaDatos(caja)
        If Not oEvents.ErrorFound Then
            uti_SerieCajaNotaCargo = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("serie_nota_cargo"), String)
        End If

        oEvents = Nothing
        oCajas = Nothing
    End Function
    Public Shared Function uti_RutaImpresionTicket(ByVal caja As Long) As String
        Dim oCajas As New VillarrealBusiness.clsCajas
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oCajas.DespliegaDatos(caja)
        If Not oEvents.ErrorFound Then
            uti_RutaImpresionTicket = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("ruta_impresion_ticket"), String)
        End If

        oEvents = Nothing
        oCajas = Nothing
    End Function

    Public Shared Function uti_ObtenerTipoValeCancelacion() As Long
        Dim oTiposVales As New VillarrealBusiness.clsTiposVales
        Dim oEvents As New Dipros.Utils.Events
        Dim respuesta As Long = 0


        oEvents = oTiposVales.ObtenerTipoValeCancelacion()
        If Not oEvents.ErrorFound Then
            respuesta = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("tipo"), Long)
        End If
        oEvents = Nothing
        oTiposVales = Nothing

        Return respuesta
    End Function

    'Public Shared Function uti_SerieSucursalNotacargo() As String
    '    'Public Shared Function uti_SerieSucursalNotacargo(ByVal sucursal As Long) As String
    '    Dim oSucursales As New VillarrealBusiness.clsSucursales
    '    Dim oEvents As New Dipros.Utils.Events

    '    oEvents = oSucursales.DespliegaDatos(sucursal)
    '    If Not oEvents.ErrorFound Then
    '        uti_SerieSucursalNotacargo = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("serie_nota_cargo"), String)
    '    End If

    '    oEvents = Nothing
    '    oSucursales = Nothing
    'End Function
    Public Shared Function uti_SerieSucursalInteres(ByVal sucursal As Long) As String
        Dim oSucursales As New VillarrealBusiness.clsSucursales
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oSucursales.DespliegaDatos(sucursal)
        If Not oEvents.ErrorFound Then
            uti_SerieSucursalInteres = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("serie_intereses"), String)
        End If

        oEvents = Nothing
        oSucursales = Nothing
    End Function

    Public Shared Function uti_Usuariocajero(ByVal usuario As String, ByRef cajero As Long) As Boolean
        Dim oUsuarioscajeros As New VillarrealBusiness.clsUsuariosCajeros
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oUsuarioscajeros.DespliegaDatos(usuario, -1)
        If Not oEvents.ErrorFound Then
            If CType(oEvents.Value, DataSet).Tables(0).Rows.Count > 0 Then
                cajero = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("cajero"), String)
                uti_Usuariocajero = True
            Else
                uti_Usuariocajero = False
            End If
        Else
            uti_Usuariocajero = False
        End If

        oEvents = Nothing
        oUsuarioscajeros = Nothing
    End Function
    Public Shared Function uti_UsuarioPorcentajeCondonacionIntereses(ByVal usuario As String) As Double
        Dim oUsuarios As New VillarrealBusiness.clsUsuarios
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oUsuarios.SeleccionaUsuarioAlmacen(usuario)
        If Not oEvents.ErrorFound Then
            If CType(oEvents.Value, DataSet).Tables(0).Rows.Count > 0 Then
                uti_UsuarioPorcentajeCondonacionIntereses = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("porcentaje_condonacion_intereses"), Double)
            End If
        Else
            uti_UsuarioPorcentajeCondonacionIntereses = 0
        End If

        oEvents = Nothing
        oUsuarios = Nothing
    End Function
    Public Shared Function uti_AbogadoLogueado(ByVal usuario As String) As Long
        Dim oUsuarios As New VillarrealBusiness.clsUsuarios
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oUsuarios.SeleccionaUsuarioAlmacen(usuario)
        If Not oEvents.ErrorFound Then
            If CType(oEvents.Value, DataSet).Tables(0).Rows.Count > 0 Then
                uti_AbogadoLogueado = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("abogado"), Long)
            End If
        Else
            uti_AbogadoLogueado = 0
        End If

        oEvents = Nothing
        oUsuarios = Nothing
    End Function

    Public Shared Function uti_TipoCambio(ByVal cajero As Long, ByVal fecha As Date) As Double
        Dim oCajerosFondos As New VillarrealBusiness.clsCajerosFondos
        Dim oEvents As New Dipros.Utils.Events

        oEvents = oCajerosFondos.DespliegaDatos(cajero, fecha)
        If Not oEvents.ErrorFound Then
            If CType(oEvents.Value, DataSet).Tables(0).Rows.Count = 0 Then
                uti_TipoCambio = 0
            Else
                uti_TipoCambio = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("tipo_cambio"), Double)
            End If

        End If

        oEvents = Nothing
        oCajerosFondos = Nothing
    End Function
    Public Shared Function uti_ExisteCajerosFondos(ByVal usuario As String, ByVal fecha As Date) As Boolean
        Dim oCajerosFondos As New VillarrealBusiness.clsCajerosFondos
        Dim oEvents As New Dipros.Utils.Events
        Dim cajero As Long = 0

        Comunes.clsUtilerias.uti_Usuariocajero(usuario, cajero)
        oEvents = oCajerosFondos.Existe(cajero, fecha)

        If Not oEvents.ErrorFound Then
            uti_ExisteCajerosFondos = CType(oEvents.Value, Boolean)
        End If

        oEvents = Nothing
        oCajerosFondos = Nothing

    End Function

    Public Shared Sub InhabilitaColumnas(ByVal columna As DevExpress.XtraGrid.Columns.GridColumn)
        With columna
            .Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                               Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        End With
    End Sub
    Public Shared Sub HablilitaColumnas(ByVal columna As DevExpress.XtraGrid.Columns.GridColumn)
        With columna
            .Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                                            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm)), DevExpress.XtraGrid.Columns.ColumnOptions)
        End With
    End Sub

    Public Shared Function uti_SucursalPrecioVenta(ByVal sucursal As Long) As Long
        Dim oSucursales As New VillarrealBusiness.clsSucursales
        Dim oEvents As New Dipros.Utils.Events
        oEvents = oSucursales.DespliegaDatos(sucursal)
        If Not oEvents.ErrorFound Then
            uti_SucursalPrecioVenta = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("precio_venta"), String)
        End If
        oEvents = Nothing
        oSucursales = Nothing
    End Function
    Public Shared Function uti_SucursalPrecioMinimoVenta(ByVal sucursal As Long) As Long
        Dim oSucursales As New VillarrealBusiness.clsSucursales
        Dim oEvents As New Dipros.Utils.Events
        oEvents = oSucursales.DespliegaDatos(sucursal)
        If Not oEvents.ErrorFound Then
            uti_SucursalPrecioMinimoVenta = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("precio_minimo_venta"), String)
        End If
        oEvents = Nothing
        oSucursales = Nothing
    End Function

    Public Shared Function uti_SucursalDependencia(ByVal sucursal As Long) As Boolean
        Dim oSucursales As New VillarrealBusiness.clsSucursales
        Dim oEvents As New Dipros.Utils.Events
        oEvents = oSucursales.DespliegaDatos(sucursal)
        If Not oEvents.ErrorFound Then
            uti_SucursalDependencia = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)
        End If
        oEvents = Nothing
        oSucursales = Nothing
    End Function
    Public Shared Function uti_SucursalQuincenaEnvio(ByVal sucursal As Long) As Long
        Dim oSucursales As New VillarrealBusiness.clsSucursales
        Dim oEvents As New Dipros.Utils.Events
        oEvents = oSucursales.DespliegaDatos(sucursal)
        If Not oEvents.ErrorFound Then
            uti_SucursalQuincenaEnvio = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("quincena_envio"), Long)
        End If
        oEvents = Nothing
        oSucursales = Nothing
    End Function
    Public Shared Function ValidaFecha(ByVal fecha As String) As Dipros.Utils.Events
        Dim oEvent As New Dipros.Utils.Events


        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Dipros.Utils.Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha esta fuera del rango v�lido"
            oEvent.Layer = Dipros.Utils.Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Public Shared Function uti_VariablesPrecioContado() As Long
        Dim oVariables As New VillarrealBusiness.clsVariables
        Dim oEvents As New Dipros.Utils.Events
        oEvents = oVariables.DespliegaDatos
        If Not oEvents.ErrorFound Then
            If CType(oEvents.Value, DataSet).Tables(0).Rows.Count = 0 Then
                uti_VariablesPrecioContado = -1
            Else
                If CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("precio_contado") Is System.DBNull.Value Then
                    uti_VariablesPrecioContado = -1
                Else
                    uti_VariablesPrecioContado = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("precio_contado"), Long)
                End If
            End If

        End If
        oEvents = Nothing
        oVariables = Nothing
    End Function

    Public Shared Function uti_VariablesFechaCierre() As DateTime
        Dim oVariables As New VillarrealBusiness.clsVariables
        Dim oEvents As New Dipros.Utils.Events
        oEvents = oVariables.DespliegaDatos
        If Not oEvents.ErrorFound Then
            If CType(oEvents.Value, DataSet).Tables(0).Rows.Count = 0 Then
                uti_VariablesFechaCierre = Nothing
            Else
                If CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("fecha_cierre") Is System.DBNull.Value Then
                    uti_VariablesFechaCierre = Nothing
                Else
                    uti_VariablesFechaCierre = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("fecha_cierre"), DateTime)
                End If
            End If

        End If
        oEvents = Nothing
        oVariables = Nothing
    End Function
    Public Shared Function uti_ChequeraPredeterminada() As Long
        Dim oChequeras As New VillarrealBusiness.clsChequeras
        Dim oEvents As New Dipros.Utils.Events
        oEvents = oChequeras.TraerPredeterminada()
        If Not oEvents.ErrorFound Then
            If CType(oEvents.Value, DataSet).Tables(0).Rows.Count > 0 Then
                uti_ChequeraPredeterminada = CType(CType(oEvents.Value, DataSet).Tables(0).Rows(0).Item("cuenta_bancaria"), Long)
            Else
                uti_ChequeraPredeterminada = -1
            End If

        End If
        oEvents = Nothing
        oChequeras = Nothing
    End Function


    Public Shared Function obtenerSaldosVencidosInteresesCliente(ByVal cliente As Long, ByVal fecha As DateTime) As Dipros.Utils.Events
        Dim clase As New VillarrealBusiness.clsUtilerias
        Dim oevent As New Dipros.Utils.Events
        oevent = clase.obtenerSaldosVencidosInteresesCliente(cliente, fecha)
        obtenerSaldosVencidosInteresesCliente = oevent
    End Function
    Public Shared Function Obtener_Domicilios_Ventas_Repartos_Cliente(ByVal cliente As Long) As Dipros.Utils.Events
        Dim clase As New VillarrealBusiness.clsUtilerias
        Dim oevent As New Dipros.Utils.Events
        oevent = clase.Obtener_Domicilios_Ventas_Repartos_Cliente(cliente)
        Obtener_Domicilios_Ventas_Repartos_Cliente = oevent
    End Function
    Public Shared Function AbonosClienteJuridico(ByVal abogado As Long, ByVal cliente As Long) As Dipros.Utils.Events
        Dim clase As New VillarrealBusiness.clsUtilerias
        Dim oevent As New Dipros.Utils.Events
        oevent = clase.AbonosClientesJuridico(abogado, cliente)
        AbonosClienteJuridico = oevent
    End Function


    Public Shared Function FechaServidor() As Date
        Dim clase As New VillarrealBusiness.clsUtilerias
        Dim oevents As New Dipros.Utils.Events
        oevents = clase.FechaServidor()
        If Not oevents.ErrorFound Then
            If CType(oevents.Value, DataSet).Tables(0).Rows.Count > 0 Then
                FechaServidor = CType(CType(oevents.Value, DataSet).Tables(0).Rows(0).Item("fecha"), Date)
            Else
                FechaServidor = Date.Now
            End If

        End If
    End Function


    Public Shared Function VerificaPermisoExtendido(ByVal MenuOption_name As String, ByVal permiso_extendido As String) As Boolean
        Dim oSecurity As New Dipros.Utils.ExtendedSecurity(MenuOption_name)

        If CDate(TinApp.FechaServidor) < CDate("20-09-2014") Then
            If oSecurity.Extended(permiso_extendido) Then
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If

    End Function

    'Public Shared Function VerificaPermisosExtendidos(ByVal ControlesForm As Windows.Forms.Control.ControlCollection, ByVal Indentificador As String) As Boolean
    '    Dim Respuestas As Boolean
    '    Dim Controles As Windows.Forms.Control


    '    For Each Controles In ControlesForm

    '        If CType(Controles.Tag, String) = Indentificador Then
    '            Respuestas = True
    '            Exit For
    '        End If
    '    Next

    '    Return Respuestas


    'End Function
    Public Shared Function VerificaPermisosExtendidos(ByRef ControlesForm As Windows.Forms.ToolBar.ToolBarButtonCollection, ByVal Identificadores As String()) As Boolean()
        Dim Respuestas As Boolean()
        Dim Controles As Windows.Forms.ToolBarButton
        Dim Cadenas As String
        Dim index As Long = 0

        ReDim Respuestas(Identificadores.Length)

        For Each Cadenas In Identificadores
            For Each Controles In ControlesForm
                If CType(Controles.Tag, String) = Cadenas Then
                    Controles.Enabled = True
                    Controles.Visible = True
                    Respuestas.SetValue(True, index)
                End If
            Next
            index = index + 1
        Next



        Return Respuestas


    End Function
    Public Shared Function VerificaPermisosExtendidos(ByRef ControlesForm As Windows.Forms.ToolBar.ToolBarButtonCollection, ByVal Identificadores As String(), ByVal Condicion As Boolean, Optional ByVal BotonExcluir As String = "") As Boolean()
        Dim Respuestas As Boolean()
        Dim Controles As Windows.Forms.ToolBarButton
        Dim Cadenas As String
        Dim index As Long = 0
        Dim Encontrado As Boolean


        ReDim Respuestas(Identificadores.Length - 1)

        For Each Cadenas In Identificadores
            Encontrado = False
            For Each Controles In ControlesForm
                If CType(Controles.Tag, String) = Cadenas Then
                    If BotonExcluir = Cadenas Then
                        Controles.Enabled = True
                        Controles.Visible = True
                    Else
                        Controles.Enabled = Condicion
                        Controles.Visible = Condicion
                        Respuestas.SetValue(True, index)
                        Encontrado = True
                    End If


                End If
            Next
            If Encontrado = False Then
                Respuestas.SetValue(False, index)
            End If

            index = index + 1
        Next



        Return Respuestas


    End Function

    Public Shared Function ValidaRFC(ByVal rfc As String, ByVal TipoPersona As Char) As Boolean
        If TipoPersona = "F" Then
            If Regex.IsMatch(rfc, "^([A-Z\s]{4})\d{6}([A-Z\w]{3})$") Then
                Return True
            Else
                Return False
            End If
        Else
            If Regex.IsMatch(rfc, "^([A-Z\s]{3})\d{6}([A-Z\w]{3})$") Then
                Return True
            Else
                Return False
            End If
        End If

    End Function

    Public Shared Function EliminaFormatoTelefonico(ByVal Telefono As String) As String

        Dim cadena As String

        cadena = Telefono.Replace("(", "")
        cadena = cadena.Replace(")", "")
        cadena = cadena.Replace("-", "")

        Return cadena

    End Function

End Class

