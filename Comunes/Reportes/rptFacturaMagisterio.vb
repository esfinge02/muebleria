Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptFacturaMagisterio
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblColonia As DataDynamics.ActiveReports.Label = Nothing
	Private lblCP As DataDynamics.ActiveReports.Label = Nothing
	Private lblCiudad As DataDynamics.ActiveReports.Label = Nothing
	Private lblEstado As DataDynamics.ActiveReports.Label = Nothing
	Private lblTelefono1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblRFC As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private lblClaveCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolioVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCURP As DataDynamics.ActiveReports.Label = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTelefono2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTipoVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblLeyenda As DataDynamics.ActiveReports.Label = Nothing
	Private txtFechaPrimerDoc As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblNomVendedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblEnajenacion As DataDynamics.ActiveReports.Label = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioUnitario As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtObsequio As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCantidadLetra As DataDynamics.ActiveReports.Label = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private lblPedimento As DataDynamics.ActiveReports.Label = Nothing
	Private lblAduana As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaImportacion As DataDynamics.ActiveReports.Label = Nothing
	Private lblTipoCobro As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private lblInteres As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtInteres As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSubtotal As DataDynamics.ActiveReports.Label = Nothing
	Private txtSubtotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblMenos As DataDynamics.ActiveReports.Label = Nothing
	Private txtMenos As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEnganche As DataDynamics.ActiveReports.Label = Nothing
	Private txtEnganche As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFolioVenta2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblVendedor As DataDynamics.ActiveReports.Label = Nothing
	Private txtHoraVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblImpuesto As DataDynamics.ActiveReports.Label = Nothing
	Private txtImpuesto As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptFacturaMagisterio.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.lblCliente = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.lblDomicilio = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtFecha = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblColonia = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblCP = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblCiudad = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblEstado = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblTelefono1 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblRFC = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label4 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label7 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblClaveCliente = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblFolioVenta = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblCURP = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.lblTelefono2 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Label9 = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.lblTipoVenta = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.lblLeyenda = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Label)
		Me.txtFechaPrimerDoc = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.TextBox)
		Me.lblNomVendedor = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.Label)
		Me.lblEnajenacion = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.Label)
		Me.txtCantidad = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionArticulo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioUnitario = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtBodega = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtObsequio = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.lblCantidadLetra = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Label)
		Me.lblObservaciones = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblPedimento = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblAduana = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblFechaImportacion = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblTipoCobro = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.GroupFooter1.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblInteres = CType(Me.GroupFooter1.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtTotalFactura = CType(Me.GroupFooter1.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtInteres = CType(Me.GroupFooter1.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.lblSubtotal = CType(Me.GroupFooter1.Controls(10),DataDynamics.ActiveReports.Label)
		Me.txtSubtotal = CType(Me.GroupFooter1.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.lblMenos = CType(Me.GroupFooter1.Controls(12),DataDynamics.ActiveReports.Label)
		Me.txtMenos = CType(Me.GroupFooter1.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.lblEnganche = CType(Me.GroupFooter1.Controls(14),DataDynamics.ActiveReports.Label)
		Me.txtEnganche = CType(Me.GroupFooter1.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.GroupFooter1.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtImporte = CType(Me.GroupFooter1.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.lblFolioVenta2 = CType(Me.GroupFooter1.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblVendedor = CType(Me.GroupFooter1.Controls(19),DataDynamics.ActiveReports.Label)
		Me.txtHoraVenta = CType(Me.GroupFooter1.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.lblImpuesto = CType(Me.GroupFooter1.Controls(21),DataDynamics.ActiveReports.Label)
		Me.txtImpuesto = CType(Me.GroupFooter1.Controls(22),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

End Class
