Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptSaldosGeneralesAgrupadoCobradores
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader3 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter3 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoPorVencer As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoVencido As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoActual As DataDynamics.ActiveReports.Label = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private cobrador1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcobrador As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private txtnombre_cobrador As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNoCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoPorVencer As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoVencido As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoActual As DataDynamics.ActiveReports.TextBox = Nothing
	Private por_aplicar As DataDynamics.ActiveReports.TextBox = Nothing
	Private vencido1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private vencer2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private vencer1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private Line5 As DataDynamics.ActiveReports.Line = Nothing
	Private Line6 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptSaldosGeneralesAgrupadoCobradores.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader2 = CType(Me.Sections("GroupHeader2"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader3 = CType(Me.Sections("GroupHeader3"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter3 = CType(Me.Sections("GroupFooter3"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter2 = CType(Me.Sections("GroupFooter2"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.Label2 = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblSaldoPorVencer = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblSaldoVencido = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblSaldoActual = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.cobrador1 = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtcobrador = CType(Me.GroupHeader2.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line3 = CType(Me.GroupHeader2.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtnombre_cobrador = CType(Me.GroupHeader2.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtNoCliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtNombreCliente = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoPorVencer = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoVencido = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoActual = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.por_aplicar = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.vencido1 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.saldo1 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.vencer2 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.vencer1 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line4 = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.Line)
		Me.Line5 = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Line6 = CType(Me.GroupFooter1.Controls(6),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptSaldosGeneralesAgrupadoCobradores_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
