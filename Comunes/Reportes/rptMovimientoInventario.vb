Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptMovimientoInventario
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoPorVencer As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoVencido As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoActual As DataDynamics.ActiveReports.Label = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private txtSucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private filtro1 As DataDynamics.ActiveReports.Label = Nothing
	Private filtro2 As DataDynamics.ActiveReports.Label = Nothing
	Private filtro3 As DataDynamics.ActiveReports.Label = Nothing
	Private filtro4 As DataDynamics.ActiveReports.Label = Nothing
	Private filtro5 As DataDynamics.ActiveReports.Label = Nothing
	Private txtconcepto_movimiento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtreferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoPorVencer As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoVencido As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoActual As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtmodelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe_total As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptMovimientoInventario.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.Label2 = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblSaldoPorVencer = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblSaldoVencido = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblSaldoActual = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label26 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.txtSucursal = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.filtro1 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.filtro2 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.filtro3 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.filtro4 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.filtro5 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.txtconcepto_movimiento = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.txtreferencia = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.txtBodega = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.txtfolio = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.txtfecha = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreCliente = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoPorVencer = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoVencido = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoActual = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtcantidad = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtmodelo = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label29 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtObservaciones = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.importe_total = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label30 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptMovimientoInventario_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(oDataSet.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
