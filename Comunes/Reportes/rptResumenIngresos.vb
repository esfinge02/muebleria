Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptResumenIngresos
    Inherits ActiveReport
    Public table_formas_pago As DataTable

	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghTipo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghSucursal As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghCajero As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphEnajenacion As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphTipoAbono As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfTipoAbono As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfEnajenacion As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfCajero As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfSucursal As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfTipo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private lblA�o As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblCosto As DataDynamics.ActiveReports.Label = Nothing
	Private lblIva As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTipo As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Cajero As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEnajenacion As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTipo_Abono As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNoDocumentos As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtA�o As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtsaldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtIva As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtAbono As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private importe4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private costo4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private costo5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private SubFormasPago As DataDynamics.ActiveReports.SubReport = Nothing
	Private lblTotalTipo As DataDynamics.ActiveReports.Label = Nothing
	Private txtTipoImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTotalSinPromociones As DataDynamics.ActiveReports.Label = Nothing
	Private txttotal_sin_promocion As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private importe5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptResumenIngresos.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghTipo = CType(Me.Sections("ghTipo"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghSucursal = CType(Me.Sections("ghSucursal"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghCajero = CType(Me.Sections("ghCajero"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphEnajenacion = CType(Me.Sections("gphEnajenacion"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphTipoAbono = CType(Me.Sections("gphTipoAbono"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfTipoAbono = CType(Me.Sections("gfTipoAbono"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfEnajenacion = CType(Me.Sections("gfEnajenacion"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfCajero = CType(Me.Sections("gfCajero"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfSucursal = CType(Me.Sections("gfSucursal"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfTipo = CType(Me.Sections("gfTipo"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblCliente = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblObservaciones = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblA�o = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblSaldo = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblCosto = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblIva = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.txtTipo = CType(Me.ghTipo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.nombre_sucursal = CType(Me.ghSucursal.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Cajero = CType(Me.ghCajero.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtEnajenacion = CType(Me.gphEnajenacion.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtTipo_Abono = CType(Me.gphTipoAbono.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtCliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreCliente = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtObservaciones = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtNoDocumentos = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtA�o = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtsaldo = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtIva = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtAbono = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.gfTipoAbono.Controls(0),DataDynamics.ActiveReports.Label)
		Me.importe4 = CType(Me.gfTipoAbono.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.costo6 = CType(Me.gfTipoAbono.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.importe3 = CType(Me.gfEnajenacion.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label9 = CType(Me.gfEnajenacion.Controls(1),DataDynamics.ActiveReports.Label)
		Me.costo4 = CType(Me.gfEnajenacion.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.importe1 = CType(Me.gfSucursal.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label7 = CType(Me.gfSucursal.Controls(1),DataDynamics.ActiveReports.Label)
		Me.costo5 = CType(Me.gfSucursal.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.SubFormasPago = CType(Me.gfTipo.Controls(0),DataDynamics.ActiveReports.SubReport)
		Me.lblTotalTipo = CType(Me.gfTipo.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtTipoImporte = CType(Me.gfTipo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTotalSinPromociones = CType(Me.gfTipo.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txttotal_sin_promocion = CType(Me.gfTipo.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.importe5 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label12 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region


    Private Sub rptResumenIngresos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

    Private Sub ReportFooter_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportFooter.Format
       

    End Sub

    Private Sub gfTipo_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles gfTipo.Format

        Me.SubFormasPago.Report = New SubResumenIngresos
        CType(SubFormasPago.Report, SubResumenIngresos).DataSource = table_formas_pago
    End Sub

    Private Sub ghTipo_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ghTipo.Format
        If Me.txtTipo.Value = 1 Then
            lblTotalTipo.Visible = False
            Me.txtTipoImporte.Visible = False
            Me.SubFormasPago.Visible = True
            lblTotalSinPromociones.Visible = False
            txttotal_sin_promocion.Visible = False
            gfTipo.Height = 1.365
        Else
            lblTotalTipo.Visible = True
            Me.txtTipoImporte.Visible = True
            Me.SubFormasPago.Visible = False
            lblTotalSinPromociones.Visible = True
            txttotal_sin_promocion.Visible = True
            gfTipo.Height = 0.24
        End If
    End Sub

End Class
