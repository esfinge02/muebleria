Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptSugerenciaCobro
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblSucursal As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtSucursal2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private txtnombresucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtsaldo_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private interes_sucursal1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_sucursal2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private vencer2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line6 As DataDynamics.ActiveReports.Line = Nothing
	Private interes_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
        Me.LoadLayout(Me.GetType, "Comunes.rptSugerenciaCobro.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.lblSucursal = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblSaldo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label26 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtSucursal2 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtnombresucursal = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtsaldo_sucursal = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.interes_sucursal1 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.total_sucursal2 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.saldo1 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.vencer2 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line6 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.Line)
		Me.interes_sucursal = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.total_sucursal = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region

    Private Sub rptSugerenciaCobro_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
