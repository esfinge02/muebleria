Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptEtiquetas
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Shape5 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape3 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape2 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPrecioContado As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblPrecio1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPrecio1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEnganche1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblAbonos1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtEnganche1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtAbonos1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblPrecio2 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPrecio2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEnganche2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblAbonos2 As DataDynamics.ActiveReports.Label = Nothing
	Private txtEnganche2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtAbonos2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape4 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblPrecio3 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPrecio3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEnganche3 As DataDynamics.ActiveReports.Label = Nothing
	Private txtAbonos3 As DataDynamics.ActiveReports.Label = Nothing
	Private txtEnganche3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private abonos3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private abonos31 As DataDynamics.ActiveReports.Label = Nothing
	Private txtBonificacion As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblLeyenda As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblProveedor As DataDynamics.ActiveReports.Label = Nothing
	Private txtProveedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion_corta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionLarga As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImportacion As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptEtiquetas.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape5 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Shape3 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.Shape2 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Shape)
		Me.Shape1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Shape)
		Me.Label4 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtPrecioContado = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblPrecio1 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Label)
		Me.txtPrecio1 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.lblEnganche1 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblAbonos1 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtEnganche1 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtAbonos1 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.lblPrecio2 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Label)
		Me.txtPrecio2 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.lblEnganche2 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblAbonos2 = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.Label)
		Me.txtEnganche2 = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.txtAbonos2 = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.Shape4 = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.Shape)
		Me.lblPrecio3 = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.Label)
		Me.txtPrecio3 = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.lblEnganche3 = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.Label)
		Me.txtAbonos3 = CType(Me.Detail.Controls(22),DataDynamics.ActiveReports.Label)
		Me.txtEnganche3 = CType(Me.Detail.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.abonos3 = CType(Me.Detail.Controls(24),DataDynamics.ActiveReports.TextBox)
		Me.abonos31 = CType(Me.Detail.Controls(25),DataDynamics.ActiveReports.Label)
		Me.txtBonificacion = CType(Me.Detail.Controls(26),DataDynamics.ActiveReports.TextBox)
		Me.lblLeyenda = CType(Me.Detail.Controls(27),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.Detail.Controls(28),DataDynamics.ActiveReports.TextBox)
		Me.lblProveedor = CType(Me.Detail.Controls(29),DataDynamics.ActiveReports.Label)
		Me.txtProveedor = CType(Me.Detail.Controls(30),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.Detail.Controls(31),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.Detail.Controls(32),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.Detail.Controls(33),DataDynamics.ActiveReports.Label)
		Me.txtArticulo = CType(Me.Detail.Controls(34),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion_corta = CType(Me.Detail.Controls(35),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionLarga = CType(Me.Detail.Controls(36),DataDynamics.ActiveReports.TextBox)
		Me.txtImportacion = CType(Me.Detail.Controls(37),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region
End Class
