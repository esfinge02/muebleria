Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptResumenSalidasVentas
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghSucursal As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphTipo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphTipoVenta As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfTipoVenta As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfTipo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfSucursal As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private lblModelo As DataDynamics.ActiveReports.Label = Nothing
	Private Label37 As DataDynamics.ActiveReports.Label = Nothing
	Private n_bodega1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private n_tipoventa As DataDynamics.ActiveReports.TextBox = Nothing
	Private n_tipo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private cantidad1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtconcepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private costo2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label36 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private importe4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_sucursal1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptResumenSalidasVentas.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghSucursal = CType(Me.Sections("ghSucursal"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphTipo = CType(Me.Sections("gphTipo"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphTipoVenta = CType(Me.Sections("gphTipoVenta"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfTipoVenta = CType(Me.Sections("gpfTipoVenta"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfTipo = CType(Me.Sections("gpfTipo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfSucursal = CType(Me.Sections("gfSucursal"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblArticulo = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label32 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblModelo = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label37 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.n_bodega1 = CType(Me.ghSucursal.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.n_tipoventa = CType(Me.gphTipo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.n_tipo = CType(Me.gphTipoVenta.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreArticulo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtReferencia = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioVenta = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreGrupo = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtfecha = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.cantidad1 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtconcepto = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtModelo = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.costo1 = CType(Me.gpfTipoVenta.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.importe1 = CType(Me.gpfTipoVenta.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label33 = CType(Me.gpfTipoVenta.Controls(2),DataDynamics.ActiveReports.Label)
		Me.costo2 = CType(Me.gpfTipo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label34 = CType(Me.gpfTipo.Controls(1),DataDynamics.ActiveReports.Label)
		Me.TextBox1 = CType(Me.gpfTipo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.costo3 = CType(Me.gfSucursal.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.importe3 = CType(Me.gfSucursal.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label36 = CType(Me.gfSucursal.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.importe4 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.costo4 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.total_sucursal1 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptResumenSalidasVentas_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        Dim Response As Dipros.Utils.Events

        Response = oReportes.DatosEmpresa()

        If Response.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.txtEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(oDataSet.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
