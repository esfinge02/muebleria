Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptComprobanteFiscalSimplicado
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Picture1 As DataDynamics.ActiveReports.Picture = Nothing
	Private fecha1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFolioVenta As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private lblCP As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private lblCiudad As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private lblEstado As DataDynamics.ActiveReports.Label = Nothing
	Private lblClaveCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTelefono1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTelefono2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private Label21 As DataDynamics.ActiveReports.Label = Nothing
	Private lblCURP As DataDynamics.ActiveReports.Label = Nothing
	Private Label22 As DataDynamics.ActiveReports.Label = Nothing
	Private Label23 As DataDynamics.ActiveReports.Label = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private ciudad1 As DataDynamics.ActiveReports.Label = Nothing
	Private estado1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private cantidad1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cantidad2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioUnitario As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Picture2 As DataDynamics.ActiveReports.Picture = Nothing
	Private lblCantidadLetra As DataDynamics.ActiveReports.Label = Nothing
	Private total1 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptComprobanteFiscalSimplicado.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Picture1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.fecha1 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblFolioVenta = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label20 = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblDomicilio = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblCP = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblCiudad = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblEstado = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblClaveCliente = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblCliente = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblTelefono1 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblTelefono2 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label21 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.lblCURP = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Label22 = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.Label23 = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.Label24 = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Label)
		Me.ciudad1 = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Label)
		Me.estado1 = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.Label)
		Me.txtCantidad = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.cantidad1 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.cantidad2 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioUnitario = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Picture2 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblCantidadLetra = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.Label)
		Me.total1 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub PageFooter_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageFooter.Format

    End Sub

    Private Sub PageHeader_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Format

    End Sub
End Class
