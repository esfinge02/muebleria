Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptNotaDeCreditoCondonacionIVA

    Inherits ActiveReport

    Public Sub New()
        MyBase.New()
        InitializeReport()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private lblFolio As DataDynamics.ActiveReports.Label = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblIva As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private txtiva As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblImporteLetra As DataDynamics.ActiveReports.Label = Nothing
	Private nombre_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private sucursal_referencia_nombre As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDiaPagare As DataDynamics.ActiveReports.Label = Nothing
	Private lblMesPagare As DataDynamics.ActiveReports.Label = Nothing
	Private lblAņoPagare As DataDynamics.ActiveReports.Label = Nothing
	Private lblNombreCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblDomicilioCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblNumeroCuenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblAutorizado As DataDynamics.ActiveReports.Label = Nothing
	Private lblRfc As DataDynamics.ActiveReports.Label = Nothing
	Private lblSerie As DataDynamics.ActiveReports.Label = Nothing
	Private txtSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFoilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtFolioReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptNotaDeCreditoCondonacionIVA.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.lblFolio = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtFecha = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblObservaciones = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtImporte = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblIva = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtiva = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.lblImporteLetra = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Label)
		Me.nombre_sucursal = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.sucursal_referencia_nombre = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblDiaPagare = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblMesPagare = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblAņoPagare = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblNombreCliente = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblDomicilioCliente = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblNumeroCuenta = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblAutorizado = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblRfc = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblSerie = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.Label)
		Me.txtSerie = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.lblFoilio = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.Label)
		Me.txtFolioReferencia = CType(Me.Detail.Controls(22),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region


End Class
