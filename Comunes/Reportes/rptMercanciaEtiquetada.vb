Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptMercanciaEtiquetada
    Inherits ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeReport()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
    Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
    Private Label10 As DataDynamics.ActiveReports.Label = Nothing
    Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
    Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
    Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
    Private lblSubtitulo As DataDynamics.ActiveReports.Label = Nothing
    Private lblPrecioUnitario As DataDynamics.ActiveReports.Label = Nothing
    Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
    Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
    Private lblFechaFactura As DataDynamics.ActiveReports.Label = Nothing
    Private Label8 As DataDynamics.ActiveReports.Label = Nothing
    Private Label9 As DataDynamics.ActiveReports.Label = Nothing
    Private orden_compra As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
    Private nombre_concepto1 As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtFechaFactura As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtModelo As DataDynamics.ActiveReports.TextBox = Nothing
    Private modelo1 As DataDynamics.ActiveReports.TextBox = Nothing
    Private CheckBox1 As DataDynamics.ActiveReports.CheckBox = Nothing
    Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
    Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
    Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private Line1 As DataDynamics.ActiveReports.Line = Nothing
    Public Sub InitializeReport()
        Me.LoadLayout(Me.GetType, "Almacen.rptMercanciaEtiquetada.rpx")
        Me.ReportHeader = CType(Me.Sections("ReportHeader"), DataDynamics.ActiveReports.ReportHeader)
        Me.PageHeader = CType(Me.Sections("PageHeader"), DataDynamics.ActiveReports.PageHeader)
        Me.Detail = CType(Me.Sections("Detail"), DataDynamics.ActiveReports.Detail)
        Me.PageFooter = CType(Me.Sections("PageFooter"), DataDynamics.ActiveReports.PageFooter)
        Me.ReportFooter = CType(Me.Sections("ReportFooter"), DataDynamics.ActiveReports.ReportFooter)
        Me.Shape1 = CType(Me.PageHeader.Controls(0), DataDynamics.ActiveReports.Shape)
        Me.Label10 = CType(Me.PageHeader.Controls(1), DataDynamics.ActiveReports.Label)
        Me.picLogotipo = CType(Me.PageHeader.Controls(2), DataDynamics.ActiveReports.Picture)
        Me.txtEmpresa = CType(Me.PageHeader.Controls(3), DataDynamics.ActiveReports.TextBox)
        Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(4), DataDynamics.ActiveReports.TextBox)
        Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(5), DataDynamics.ActiveReports.TextBox)
        Me.lblTitulo = CType(Me.PageHeader.Controls(6), DataDynamics.ActiveReports.Label)
        Me.lblSubtitulo = CType(Me.PageHeader.Controls(7), DataDynamics.ActiveReports.Label)
        Me.lblPrecioUnitario = CType(Me.PageHeader.Controls(8), DataDynamics.ActiveReports.Label)
        Me.lblFactura = CType(Me.PageHeader.Controls(9), DataDynamics.ActiveReports.Label)
        Me.lblArticulo = CType(Me.PageHeader.Controls(10), DataDynamics.ActiveReports.Label)
        Me.lblFechaFactura = CType(Me.PageHeader.Controls(11), DataDynamics.ActiveReports.Label)
        Me.Label8 = CType(Me.PageHeader.Controls(12), DataDynamics.ActiveReports.Label)
        Me.Label9 = CType(Me.PageHeader.Controls(13), DataDynamics.ActiveReports.Label)
        Me.orden_compra = CType(Me.Detail.Controls(0), DataDynamics.ActiveReports.TextBox)
        Me.txtCantidad = CType(Me.Detail.Controls(1), DataDynamics.ActiveReports.TextBox)
        Me.nombre_concepto1 = CType(Me.Detail.Controls(2), DataDynamics.ActiveReports.TextBox)
        Me.txtFechaFactura = CType(Me.Detail.Controls(3), DataDynamics.ActiveReports.TextBox)
        Me.txtModelo = CType(Me.Detail.Controls(4), DataDynamics.ActiveReports.TextBox)
        Me.modelo1 = CType(Me.Detail.Controls(5), DataDynamics.ActiveReports.TextBox)
        Me.CheckBox1 = CType(Me.Detail.Controls(6), DataDynamics.ActiveReports.CheckBox)
        Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0), DataDynamics.ActiveReports.TextBox)
        Me.lblDeReporte = CType(Me.PageFooter.Controls(1), DataDynamics.ActiveReports.Label)
        Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2), DataDynamics.ActiveReports.TextBox)
        Me.Label4 = CType(Me.PageFooter.Controls(3), DataDynamics.ActiveReports.Label)
        Me.Label5 = CType(Me.PageFooter.Controls(4), DataDynamics.ActiveReports.Label)
        Me.Line1 = CType(Me.PageFooter.Controls(5), DataDynamics.ActiveReports.Line)
    End Sub

#End Region

    Private Sub rptMercanciaEtiquetada_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim response As New Dipros.Utils.Events
        Dim dibujo As Image
        Dim oDataSet As DataSet
        response = oReportes.DatosEmpresa()
        oDataSet = response.Value

        If oDataSet.Tables(0).Rows.Count > 0 Then

            Me.txtEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(oDataSet.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

    'Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format
    '    If Me.CheckBox1.Checked = True Then
    '        Detail.BackColor = Color.WhiteSmoke
    '    Else
    '        Detail.BackColor = Color.White
    '    End If
    'End Sub
End Class