Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptResumenContabilidad
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghSucursal As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphEnajenacion As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphTipo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfTipo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfEnajenacion As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfSucursal As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblA�o As DataDynamics.ActiveReports.Label = Nothing
	Private lblCosto As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private nombre_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEnajenacion As DataDynamics.ActiveReports.TextBox = Nothing
	Private enajenacion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTipo_Abono As DataDynamics.ActiveReports.TextBox = Nothing
	Private tipo_abono1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtA�o As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private costo6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private importe3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private costo4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private costo5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptResumenContabilidad.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghSucursal = CType(Me.Sections("ghSucursal"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphEnajenacion = CType(Me.Sections("gphEnajenacion"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphTipo = CType(Me.Sections("gphTipo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfTipo = CType(Me.Sections("gfTipo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfEnajenacion = CType(Me.Sections("gfEnajenacion"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfSucursal = CType(Me.Sections("gfSucursal"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblImporte = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblA�o = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblCosto = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.nombre_sucursal = CType(Me.ghSucursal.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtEnajenacion = CType(Me.gphEnajenacion.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.enajenacion1 = CType(Me.gphEnajenacion.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTipo_Abono = CType(Me.gphTipo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.tipo_abono1 = CType(Me.gphTipo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtA�o = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.importe4 = CType(Me.gfTipo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.gfTipo.Controls(1),DataDynamics.ActiveReports.Label)
		Me.costo6 = CType(Me.gfTipo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Line4 = CType(Me.gfTipo.Controls(3),DataDynamics.ActiveReports.Line)
		Me.importe3 = CType(Me.gfEnajenacion.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label9 = CType(Me.gfEnajenacion.Controls(1),DataDynamics.ActiveReports.Label)
		Me.costo4 = CType(Me.gfEnajenacion.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.gfEnajenacion.Controls(3),DataDynamics.ActiveReports.Line)
		Me.importe1 = CType(Me.gfSucursal.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label7 = CType(Me.gfSucursal.Controls(1),DataDynamics.ActiveReports.Label)
		Me.costo5 = CType(Me.gfSucursal.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Line3 = CType(Me.gfSucursal.Controls(3),DataDynamics.ActiveReports.Line)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region
End Class
