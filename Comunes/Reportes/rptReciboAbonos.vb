Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptReciboAbonos
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label17 As DataDynamics.ActiveReports.Label = Nothing
	Private Label22 As DataDynamics.ActiveReports.Label = Nothing
	Private txtrfc As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label23 As DataDynamics.ActiveReports.Label = Nothing
	Private txtFormaPago As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private txtFolio_Recibo As DataDynamics.ActiveReports.TextBox = Nothing
	Private rfc1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_impresion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private txtiva As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private tipoabono1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private importe5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private iva1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private subtotal1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private titulo_descuento1 As DataDynamics.ActiveReports.Label = Nothing
	Private saldo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label21 As DataDynamics.ActiveReports.Label = Nothing
	Private nombre_cajero1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtsaldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSerie As DataDynamics.ActiveReports.Label = Nothing
	Private txtSerieReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolioReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFolioReferencia As DataDynamics.ActiveReports.Label = Nothing
	Private subtotal2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private subtotal_intereses1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe_abono1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe_abono2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreSucursalReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSucursal As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtleyenda_iva As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptReciboAbonos.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Label10 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtCliente = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label11 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtNombreCliente = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblObservaciones = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblSaldo = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label17 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label22 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtrfc = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.Label23 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtFormaPago = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Line)
		Me.Label24 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.txtFolio_Recibo = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.rfc1 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.fecha_impresion1 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.Label13 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtiva = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.importe2 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.tipoabono1 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label20 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Label)
		Me.importe5 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.iva1 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.subtotal1 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.titulo_descuento1 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Label)
		Me.saldo1 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label21 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Label)
		Me.nombre_cajero1 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtsaldo = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.lblSerie = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtSerieReferencia = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.txtFolioReferencia = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.lblFolioReferencia = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.Label)
		Me.subtotal2 = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.subtotal_intereses1 = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.importe_abono1 = CType(Me.Detail.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.importe_abono2 = CType(Me.Detail.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreSucursalReferencia = CType(Me.Detail.Controls(24),DataDynamics.ActiveReports.TextBox)
		Me.lblSucursal = CType(Me.Detail.Controls(25),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.Detail.Controls(26),DataDynamics.ActiveReports.Label)
		Me.txtleyenda_iva = CType(Me.Detail.Controls(27),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

 
    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format
        Me.txtleyenda_iva.Text = Me.txtleyenda_iva.Text.TrimEnd()
    End Sub
End Class
