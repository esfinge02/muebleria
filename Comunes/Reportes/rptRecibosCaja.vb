Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptRecibosCaja
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private txtrfc As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio_Recibo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private rfc1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtiva As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private importe3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private tipoabono1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label17 As DataDynamics.ActiveReports.Label = Nothing
	Private nombrecliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblSerie As DataDynamics.ActiveReports.Label = Nothing
	Private txtSerieReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolioReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFolio As DataDynamics.ActiveReports.Label = Nothing
	Private txtleyenda_iva As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptRecibosCaja.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Label10 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtCliente = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label11 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtNombreCliente = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblObservaciones = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblSaldo = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtrfc = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio_Recibo = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.Label24 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.rfc1 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label13 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporte = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtiva = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.importe2 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.importe3 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Label15 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Label)
		Me.tipoabono1 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label17 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Label)
		Me.nombrecliente1 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.lblSerie = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Label)
		Me.txtSerieReferencia = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.txtFolioReferencia = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.lblFolio = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Label)
		Me.txtleyenda_iva = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region
End Class
