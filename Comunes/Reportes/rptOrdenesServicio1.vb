Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptOrdenesServicio
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphTipo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfTipo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblOrdenado As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTipo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaPromesa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEstatus As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtOrdenServicio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFalla As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNumeroSeguimientos As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptOrdenesServicio.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphTipo = CType(Me.Sections("gphTipo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfTipo = CType(Me.Sections("gpfTipo"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape1 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblOrdenado = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Line)
		Me.Label32 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtTipo = CType(Me.gphTipo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreArticulo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaPromesa = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtEstatus = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtOrdenServicio = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtFalla = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtNumeroSeguimientos = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptOrdenesServicio_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        sender = oReportes.DatosEmpresa()

        If CType(CType(sender, Dipros.Utils.Events).Value, DataSet).Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = CType(CType(sender, Dipros.Utils.Events).Value, DataSet).Tables(0).Rows(0)
            With oRow
                Me.txtEmpresa.Value = .Item("nombre_empresa")
                Me.txtDireccionEmpresa.Value = .Item("direccion_empresa")
                Me.txtTelefonosEmpresa.Value = .Item("telefonos_empresa")
                Me.picLogotipo.Image = Dipros.Utils.Draw.ByteToImage(.Item("logotipo_empresa"))
            End With
            oRow = Nothing
        End If
    End Sub
End Class
