Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptArticulosKardex
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
    End Sub

    Private ultimo As Long
    Private tot_ultimo_bodega As Long
    Private tot_ultimo_grantot As Long
    Public oData As DataSet

#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghArticulo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfArticulo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label20 As DataDynamics.ActiveReports.Label = Nothing
	Private Label21 As DataDynamics.ActiveReports.Label = Nothing
	Private Label22 As DataDynamics.ActiveReports.Label = Nothing
	Private Label23 As DataDynamics.ActiveReports.Label = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private txtInicialArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtUnidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private txtmodelo As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label35 As DataDynamics.ActiveReports.Label = Nothing
	Private Label36 As DataDynamics.ActiveReports.Label = Nothing
	Private Label37 As DataDynamics.ActiveReports.Label = Nothing
	Private txtMovimiento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtInicial As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEntrada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSalida As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFinal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private final1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private movimiento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private movimiento2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotArtSal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotArtEnt As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotArtFin As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line5 As DataDynamics.ActiveReports.Line = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private subConceptos As DataDynamics.ActiveReports.SubReport = Nothing
	Private SubEstadisticos As DataDynamics.ActiveReports.SubReport = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private subExistenciasTransito As DataDynamics.ActiveReports.SubReport = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptArticulosKardex.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghArticulo = CType(Me.Sections("ghArticulo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfArticulo = CType(Me.Sections("gfArticulo"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label11 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label20 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label21 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label22 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label23 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label24 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.txtArticulo = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionArticulo = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreGrupo = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.Label32 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Label33 = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.txtInicialArticulo = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.txtUnidad = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.Label34 = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Label)
		Me.txtmodelo = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.TextBox)
		Me.Label35 = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.Label)
		Me.Label36 = CType(Me.PageHeader.Controls(27),DataDynamics.ActiveReports.Label)
		Me.Label37 = CType(Me.PageHeader.Controls(28),DataDynamics.ActiveReports.Label)
		Me.txtMovimiento = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtInicial = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtReferencia = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtEntrada = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtSalida = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtFinal = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreBodega = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtSucursal = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.final1 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.movimiento1 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.movimiento2 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.gfArticulo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotArtSal = CType(Me.gfArticulo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotArtEnt = CType(Me.gfArticulo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotArtFin = CType(Me.gfArticulo.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line5 = CType(Me.gfArticulo.Controls(4),DataDynamics.ActiveReports.Line)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.subConceptos = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.SubReport)
		Me.SubEstadisticos = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.SubReport)
		Me.Label10 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.subExistenciasTransito = CType(Me.ReportFooter.Controls(3),DataDynamics.ActiveReports.SubReport)
	End Sub

#End Region

    Private Sub Detail_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.AfterPrint
        ultimo = CType(Me.txtFinal.Value, Long)
    End Sub

    Private Sub gfArticulo_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles gfArticulo.BeforePrint
        txtTotArtFin.Value = ultimo
        tot_ultimo_bodega = tot_ultimo_bodega + ultimo
    End Sub

    Private Sub rptResumenMovimientos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        tot_ultimo_bodega = 0
        tot_ultimo_grantot = 0

        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

    Private Sub ReportFooter_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportFooter.Format
        Me.subExistenciasTransito.Report = New subExistenciasTransito
        CType(subExistenciasTransito.Report, subExistenciasTransito).DataSource = oData.Tables(1)


        Me.subConceptos.Report = New subArticulosKardex_TotalesXConcepto
        CType(subConceptos.Report, subArticulosKardex_TotalesXConcepto).DataSource = oData.Tables(2)


        Me.SubEstadisticos.Report = New rptSubArticulosKardexEstadisticos
        CType(SubEstadisticos.Report, rptSubArticulosKardexEstadisticos).DataSource = oData.Tables(3)

    End Sub


End Class
