Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptNotaCargo
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private lblDiaPagare As DataDynamics.ActiveReports.Label = Nothing
	Private lblMesPagare As DataDynamics.ActiveReports.Label = Nothing
	Private lblAņoPagare As DataDynamics.ActiveReports.Label = Nothing
	Private lblNombreCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblDomicilioCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblNumeroCuenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblAutorizado As DataDynamics.ActiveReports.Label = Nothing
	Private lblRfc As DataDynamics.ActiveReports.Label = Nothing
	Private lblSerie As DataDynamics.ActiveReports.Label = Nothing
	Private txtSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFoilio As DataDynamics.ActiveReports.Label = Nothing
	Private txtFolioReferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private rfc1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFolio As DataDynamics.ActiveReports.Label = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblIva As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private iva1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe_letra1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptNotaCargo.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.lblDiaPagare = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.lblMesPagare = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblAņoPagare = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblNombreCliente = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblDomicilioCliente = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblNumeroCuenta = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblAutorizado = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblRfc = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblSerie = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtSerie = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.lblFoilio = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.txtFolioReferencia = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.rfc1 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.lblFolio = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtFecha = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblObservaciones = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtImporte = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblIva = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Label)
		Me.iva1 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.total1 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.importe_letra1 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region
End Class
