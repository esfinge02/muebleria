Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document
Imports System.Drawing

Public Class rptVisitasClientes
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghCobrador As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader3 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter3 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gfCobrador As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaVencimiento As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtsucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnombre_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtGuion As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcobrador As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_sucursal1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNoCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoActual As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaVencimiento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtInteres As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private total4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cargo3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalCobrador As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line7 As DataDynamics.ActiveReports.Line = Nothing
	Private total6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalSucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private vencer2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line6 As DataDynamics.ActiveReports.Line = Nothing
	Private saldo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private interes1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cargo2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptVisitasClientes.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader2 = CType(Me.Sections("GroupHeader2"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghCobrador = CType(Me.Sections("ghCobrador"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader3 = CType(Me.Sections("GroupHeader3"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter3 = CType(Me.Sections("GroupFooter3"),DataDynamics.ActiveReports.GroupFooter)
		Me.gfCobrador = CType(Me.Sections("gfCobrador"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter2 = CType(Me.Sections("GroupFooter2"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.Label2 = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblFactura = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblSaldo = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblFechaVencimiento = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label26 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label32 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.sucursal = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtsucursal = CType(Me.GroupHeader2.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtnombre_sucursal = CType(Me.GroupHeader2.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtGuion = CType(Me.GroupHeader2.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtcobrador = CType(Me.ghCobrador.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.ghCobrador.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.nombre_sucursal1 = CType(Me.ghCobrador.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtNoCliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtNombreCliente = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFactura = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoActual = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaVencimiento = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtInteres = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.total4 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.total3 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.TextBox3 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.total5 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.cargo3 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.gfCobrador.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.total2 = CType(Me.gfCobrador.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.total1 = CType(Me.gfCobrador.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalCobrador = CType(Me.gfCobrador.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line7 = CType(Me.gfCobrador.Controls(4),DataDynamics.ActiveReports.Line)
		Me.total6 = CType(Me.gfCobrador.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalSucursal = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.vencer2 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line6 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.Line)
		Me.saldo1 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.interes1 = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.cargo2 = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region

    Private Sub rptVisitasClientes_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        Dim oDataSet As DataSet
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then

            oDataSet = sender.Value
            Me.txtEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(oDataSet.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

End Class
