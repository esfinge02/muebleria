Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptDepositosBancarios
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private costo2 As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolio As DataDynamics.ActiveReports.Label = Nothing
	Private lblDiferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioLista As DataDynamics.ActiveReports.Label = Nothing
	Private lblenganche As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtfolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion_corta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioLista As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDiferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private CheckBox1 As DataDynamics.ActiveReports.CheckBox = Nothing
	Private capturo As DataDynamics.ActiveReports.TextBox = Nothing
	Private usuario_aplico As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTipoDeposito As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private importe3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptDepositosBancarios.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape8 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.costo2 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblFolio = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblDiferencia = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblFactura = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblPrecioVenta = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblPrecioLista = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblenganche = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtfolio = CType(Me.GroupHeader1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.importe5 = CType(Me.GroupHeader1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion_corta = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioLista = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioVenta = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDiferencia = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtFactura = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.CheckBox1 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.CheckBox)
		Me.capturo = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.usuario_aplico = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtTipoDeposito = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.importe4 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label16 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Line)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.importe3 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label12 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptDepositosBancarios_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
