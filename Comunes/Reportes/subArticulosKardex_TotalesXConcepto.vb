Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class subArticulosKardex_TotalesXConcepto
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private lblExistenciaInicial As DataDynamics.ActiveReports.Label = Nothing
	Private inicial_articulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtConcepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionConcepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotCon As DataDynamics.ActiveReports.TextBox = Nothing
	Private total1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotTot As DataDynamics.ActiveReports.TextBox = Nothing
	Private total2 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.subArticulosKardex_TotalesXConcepto.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Label2 = CType(Me.ReportHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.ReportHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.ReportHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.ReportHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblExistenciaInicial = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.Label)
		Me.inicial_articulo = CType(Me.GroupHeader1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtConcepto = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionConcepto = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotCon = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.total1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotTot = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.total2 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Dim iCantidadarticulos As Long
    Private Sub GroupHeader1_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupHeader1.Format
        iCantidadarticulos = 0
        If inicial_articulo.Value Is Nothing Then Exit Sub
        If inicial_articulo.Value = 0 Then
            lblExistenciaInicial.Visible = False
            inicial_articulo.Visible = False
        Else
            lblExistenciaInicial.Visible = True
            inicial_articulo.Visible = True
        End If
        iCantidadarticulos = inicial_articulo.Value
    End Sub

    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format
        iCantidadarticulos = iCantidadarticulos + total1.Value
    End Sub

    Private Sub ReportFooter_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportFooter.Format
        total2.Value = iCantidadarticulos
    End Sub
End Class
