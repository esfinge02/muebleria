Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptIngresosxCobrador1
    Inherits ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeReport()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphCobrador As DataDynamics.ActiveReports.GroupHeader = Nothing
    Public WithEvents gphConcepto As DataDynamics.ActiveReports.GroupHeader = Nothing
    Public WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Public WithEvents gpfConcepto As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfCobrador As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
    Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
    Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
    Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
    Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
    Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
    Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
    Private lblIntereses As DataDynamics.ActiveReports.Label = Nothing
    Private Label11 As DataDynamics.ActiveReports.Label = Nothing
    Private txtCobrador As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtdescripcion_concepto As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtintereses As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtNoDocumentos As DataDynamics.ActiveReports.TextBox = Nothing
    Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label10 As DataDynamics.ActiveReports.Label = Nothing
    Private txtTotalConceptoImporte As DataDynamics.ActiveReports.TextBox = Nothing
    Private comision_pagar As DataDynamics.ActiveReports.TextBox = Nothing
    Private intereses_concepto As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private txtTotalCobradorImporte As DataDynamics.ActiveReports.TextBox = Nothing
    Private importe2 As DataDynamics.ActiveReports.TextBox = Nothing
    Private intereses_cobrador As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
    Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
    Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
    Private Line1 As DataDynamics.ActiveReports.Line = Nothing
    Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
    Public Sub InitializeReport()
        Me.LoadLayout(Me.GetType, "Comunes.rptIngresosxCobrador.rpx")
        Me.PageHeader = CType(Me.Sections("PageHeader"), DataDynamics.ActiveReports.PageHeader)
        Me.gphCobrador = CType(Me.Sections("gphCobrador"), DataDynamics.ActiveReports.GroupHeader)
        Me.gphConcepto = CType(Me.Sections("gphConcepto"), DataDynamics.ActiveReports.GroupHeader)
        Me.Detail = CType(Me.Sections("Detail"), DataDynamics.ActiveReports.Detail)
        Me.gpfConcepto = CType(Me.Sections("gpfConcepto"), DataDynamics.ActiveReports.GroupFooter)
        Me.gpfCobrador = CType(Me.Sections("gpfCobrador"), DataDynamics.ActiveReports.GroupFooter)
        Me.PageFooter = CType(Me.Sections("PageFooter"), DataDynamics.ActiveReports.PageFooter)
        Me.txtEmpresa = CType(Me.PageHeader.Controls(0), DataDynamics.ActiveReports.TextBox)
        Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1), DataDynamics.ActiveReports.TextBox)
        Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2), DataDynamics.ActiveReports.TextBox)
        Me.picLogotipo = CType(Me.PageHeader.Controls(3), DataDynamics.ActiveReports.Picture)
        Me.lblTitulo = CType(Me.PageHeader.Controls(4), DataDynamics.ActiveReports.Label)
        Me.lblFechas = CType(Me.PageHeader.Controls(5), DataDynamics.ActiveReports.Label)
        Me.Shape8 = CType(Me.PageHeader.Controls(6), DataDynamics.ActiveReports.Shape)
        Me.Label6 = CType(Me.PageHeader.Controls(7), DataDynamics.ActiveReports.Label)
        Me.lblCliente = CType(Me.PageHeader.Controls(8), DataDynamics.ActiveReports.Label)
        Me.lblImporte = CType(Me.PageHeader.Controls(9), DataDynamics.ActiveReports.Label)
        Me.lblObservaciones = CType(Me.PageHeader.Controls(10), DataDynamics.ActiveReports.Label)
        Me.lblIntereses = CType(Me.PageHeader.Controls(11), DataDynamics.ActiveReports.Label)
        Me.Label11 = CType(Me.PageHeader.Controls(12), DataDynamics.ActiveReports.Label)
        Me.txtCobrador = CType(Me.gphCobrador.Controls(0), DataDynamics.ActiveReports.TextBox)
        Me.txtdescripcion_concepto = CType(Me.gphConcepto.Controls(0), DataDynamics.ActiveReports.TextBox)
        Me.txtCliente = CType(Me.Detail.Controls(0), DataDynamics.ActiveReports.TextBox)
        Me.txtNombreCliente = CType(Me.Detail.Controls(1), DataDynamics.ActiveReports.TextBox)
        Me.txtImporte = CType(Me.Detail.Controls(2), DataDynamics.ActiveReports.TextBox)
        Me.txtObservaciones = CType(Me.Detail.Controls(3), DataDynamics.ActiveReports.TextBox)
        Me.txtintereses = CType(Me.Detail.Controls(4), DataDynamics.ActiveReports.TextBox)
        Me.txtNoDocumentos = CType(Me.Detail.Controls(5), DataDynamics.ActiveReports.TextBox)
        Me.importe1 = CType(Me.Detail.Controls(6), DataDynamics.ActiveReports.TextBox)
        Me.Label10 = CType(Me.gpfConcepto.Controls(0), DataDynamics.ActiveReports.Label)
        Me.txtTotalConceptoImporte = CType(Me.gpfConcepto.Controls(1), DataDynamics.ActiveReports.TextBox)
        Me.comision_pagar = CType(Me.gpfConcepto.Controls(2), DataDynamics.ActiveReports.TextBox)
        Me.intereses_concepto = CType(Me.gpfConcepto.Controls(3), DataDynamics.ActiveReports.TextBox)
        Me.Label4 = CType(Me.gpfCobrador.Controls(0), DataDynamics.ActiveReports.Label)
        Me.txtTotalCobradorImporte = CType(Me.gpfCobrador.Controls(1), DataDynamics.ActiveReports.TextBox)
        Me.importe2 = CType(Me.gpfCobrador.Controls(2), DataDynamics.ActiveReports.TextBox)
        Me.intereses_cobrador = CType(Me.gpfCobrador.Controls(3), DataDynamics.ActiveReports.TextBox)
        Me.Label = CType(Me.PageFooter.Controls(0), DataDynamics.ActiveReports.Label)
        Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1), DataDynamics.ActiveReports.TextBox)
        Me.lblDeReporte = CType(Me.PageFooter.Controls(2), DataDynamics.ActiveReports.Label)
        Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3), DataDynamics.ActiveReports.TextBox)
        Me.Line1 = CType(Me.PageFooter.Controls(4), DataDynamics.ActiveReports.Line)
        Me.fecha_actual1 = CType(Me.PageFooter.Controls(5), DataDynamics.ActiveReports.Label)
    End Sub

#End Region

    Private Sub rptIngresosxCobrador_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

    'Private Sub gpfConcepto_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles gpfConcepto.Format
    '    If CDate(Comunes.clsUtilerias.FechaServidor) > CDate("01-08-2009") Then
    '        Me.intereses_concepto.Visible = False
    '    End If
    'End Sub

    'Private Sub gpfCobrador_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles gpfCobrador.Format
    '    If CDate(Comunes.clsUtilerias.FechaServidor) > CDate("01-08-2009") Then
    '        Me.intereses_cobrador.Visible = False
    '    End If
    'End Sub

    'Private Sub rptIngresosxCobrador_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
    '    If CDate(Comunes.clsUtilerias.FechaServidor) > CDate("01-08-2009") Then
    '        Dipros.Utils.Common.ShowMessage(Dipros.Utils.Common.MessageType.MsgError, "Referencia a objeto no establecida como instancia de un objeto", "Mensaje del Sistema")
    '    End If
    'End Sub


End Class
