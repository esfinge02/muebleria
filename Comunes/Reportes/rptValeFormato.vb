Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptValeFormato
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label24 As DataDynamics.ActiveReports.Label = Nothing
	Private rfc1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private telefonos_empresa1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private lblvigencia As DataDynamics.ActiveReports.Label = Nothing
	Private nombrecliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtfactura As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptValeFormato.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Label16 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label24 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.rfc1 = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Picture)
		Me.telefonos_empresa1 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblvigencia = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Label)
		Me.nombrecliente1 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtCliente = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label11 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtNombreCliente = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtfactura = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptValeFormato_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        Dim oDataSet As DataSet
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then

            oDataSet = sender.Value
            Me.txtEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(oDataSet.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
