Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptKardexMegaCred
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label37 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label36 As DataDynamics.ActiveReports.Label = Nothing
	Private Label35 As DataDynamics.ActiveReports.Label = Nothing
	Private Label38 As DataDynamics.ActiveReports.Label = Nothing
	Private movimiento2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private movimiento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtMovimiento As DataDynamics.ActiveReports.TextBox = Nothing
	Private Alias1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private movimiento3 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptKardexMegaCred.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label37 = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label36 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label35 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label38 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.movimiento2 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.movimiento1 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtMovimiento = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Alias1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.movimiento3 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptKardexMegaCred_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If

    End Sub
End Class
