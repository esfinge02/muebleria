Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptSubArticulosKardexEstadisticos
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtUltimaFechaCompra As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private final1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private final2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private final3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private final4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private final5 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptSubArticulosKardexEstadisticos.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Label4 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtUltimaFechaCompra = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Label5 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label7 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label9 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Label)
		Me.final1 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.final2 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.final3 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.final4 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.final5 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region
End Class
