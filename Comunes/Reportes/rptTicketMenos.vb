Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptTicketMenos
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnombre_empresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtrfc As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFolio As DataDynamics.ActiveReports.Label = Nothing
	Private lblLugarFecha As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtsucursalpago As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnombrecliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtRFCCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private txtfechaFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSerieFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTipoVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDescripcion As DataDynamics.ActiveReports.Label = Nothing
	Private txtdireccion_sucursal_abono As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfecha_impresion As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private txtfolio_electronico As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion As DataDynamics.ActiveReports.TextBox = Nothing
	Private documentos1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtmoratorios As DataDynamics.ActiveReports.TextBox = Nothing
	Private descripcion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private importe2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteLetras As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Public txtMetodoPago As DataDynamics.ActiveReports.TextBox = Nothing
	Public lblMetodoPago As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtLeyendaFormaPago As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Public txtSaldo As DataDynamics.ActiveReports.TextBox = Nothing
	Public lblSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comunes.rptTicketMenos.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtnombre_empresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtrfc = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.lblFolio = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblLugarFecha = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtsucursalpago = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.lblCliente = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.txtCliente = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.txtnombrecliente = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.txtRFCCliente = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label7 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.txtfechaFactura = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.txtSerieFolio = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.txtTipoVenta = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.lblDescripcion = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.txtdireccion_sucursal_abono = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.txtfecha_impresion = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.TextBox)
		Me.Label14 = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.Label)
		Me.txtfolio_electronico = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.documentos1 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtmoratorios = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.descripcion1 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.importe1 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label8 = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.Label)
		Me.TextBox1 = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Label9 = CType(Me.GroupFooter1.Controls(6),DataDynamics.ActiveReports.Label)
		Me.importe2 = CType(Me.GroupFooter1.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteLetras = CType(Me.GroupFooter1.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.GroupFooter1.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.GroupFooter1.Controls(10),DataDynamics.ActiveReports.Label)
		Me.txtMetodoPago = CType(Me.GroupFooter1.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.lblMetodoPago = CType(Me.GroupFooter1.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.txtLeyendaFormaPago = CType(Me.GroupFooter1.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.GroupFooter1.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldo = CType(Me.GroupFooter1.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.lblSaldo = CType(Me.GroupFooter1.Controls(16),DataDynamics.ActiveReports.Label)
		Me.TextBox3 = CType(Me.GroupFooter1.Controls(17),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Public RutaImpresora As String = ""

    Private Sub rptTicketMenos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value

            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")


            oDataSet = Nothing
        End If


        Me.Document.Printer.PrinterName = RutaImpresora

    End Sub
End Class
