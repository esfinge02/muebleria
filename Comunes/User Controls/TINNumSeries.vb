Public Class TINNumSeries
    Inherits System.Windows.Forms.UserControl
    Public Event ListChanged()
    Public Event ValidatingSerie(ByRef Cancel As Boolean, ByVal Serie As String)


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        Entrada = True

    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents btnRemove As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lstSeries As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Public WithEvents lkpSerie As DevExpress.XtraEditors.LookUpEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(TINNumSeries))
        Me.lstSeries = New DevExpress.XtraEditors.ListBoxControl
        Me.lblSerie = New System.Windows.Forms.Label
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemove = New DevExpress.XtraEditors.SimpleButton
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.lkpSerie = New DevExpress.XtraEditors.LookUpEdit
        CType(Me.lstSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkpSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lstSeries
        '
        Me.lstSeries.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstSeries.ItemHeight = 15
        Me.lstSeries.Location = New System.Drawing.Point(120, 8)
        Me.lstSeries.Name = "lstSeries"
        Me.lstSeries.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstSeries.Size = New System.Drawing.Size(120, 120)
        Me.lstSeries.TabIndex = 4
        '
        'lblSerie
        '
        Me.lblSerie.Location = New System.Drawing.Point(8, 8)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(104, 16)
        Me.lblSerie.TabIndex = 0
        Me.lblSerie.Text = "N�mero de Serie"
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(64, 48)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(24, 24)
        Me.btnAdd.TabIndex = 2
        '
        'btnRemove
        '
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.Location = New System.Drawing.Point(88, 48)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(24, 24)
        Me.btnRemove.TabIndex = 3
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(8, 24)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie.Size = New System.Drawing.Size(104, 20)
        Me.txtSerie.TabIndex = 1
        '
        'lkpSerie
        '
        Me.lkpSerie.EditValue = ""
        Me.lkpSerie.Location = New System.Drawing.Point(8, 24)
        Me.lkpSerie.Name = "lkpSerie"
        '
        'lkpSerie.Properties
        '
        Me.lkpSerie.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.lkpSerie.Properties.DisplayMember = "Serie"
        Me.lkpSerie.Properties.NullText = ""
        Me.lkpSerie.Properties.PopupWidth = 500
        Me.lkpSerie.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        Me.lkpSerie.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.lkpSerie.Properties.ValueMember = "Serie"
        Me.lkpSerie.Size = New System.Drawing.Size(106, 20)
        Me.lkpSerie.TabIndex = 60
        Me.lkpSerie.Tag = "idarticulo"
        Me.lkpSerie.ToolTip = "Clave del Art�culo"
        Me.lkpSerie.Visible = False
        '
        'TINNumSeries
        '
        Me.BackColor = System.Drawing.Color.FromArgb(CType(241, Byte), CType(241, Byte), CType(241, Byte))
        Me.Controls.Add(Me.lkpSerie)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.lstSeries)
        Me.Name = "TINNumSeries"
        Me.Size = New System.Drawing.Size(248, 136)
        CType(Me.lstSeries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkpSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Funciones P�blicas"

    Public Sub ListaAdd(ByVal Serie As String)
        If Not ExistItem(Serie) Then
            lstSeries.Items.Add(Serie)
            lstSeries.SelectedIndex = lstSeries.Items.IndexOf(Serie)
        End If
    End Sub

    Public Function RemoveLista()
        lstSeries.Items.Clear()
    End Function

#End Region

#Region "Funciones Privadas"

    Private Function ExistItem(ByVal Serie As String) As Boolean
        Dim Index As Integer
        Index = lstSeries.Items.IndexOf(Serie)

        If Index = -1 Then
            ExistItem = False
        Else
            ExistItem = True
        End If

    End Function

    Private Sub ListaRemove(ByVal Serie As String)
        If ExistItem(Serie) Then
            lstSeries.Items.Remove(Serie)
        End If
    End Sub


#End Region

#Region "Seleccion Series"

    Private Sub LimpiaCaja()
        If Me.Entrada Then
            Me.txtSerie.Text = ""
            Me.txtSerie.Focus()
        Else
            Me.lkpSerie.EditValue = Nothing
            Me.lkpSerie.Focus()
        End If
    End Sub


#End Region

#Region "Botones"

    Public Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim bCancel As Boolean = False
        RaiseEvent ValidatingSerie(bCancel, Me.txtSerie.Text.Trim)
        If bCancel Then Exit Sub

        Dim Serie As String

        If Me.Entrada Then
            Serie = Me.txtSerie.Text.Trim
        Else
            Serie = Me.lkpSerie.Text.Trim
        End If
        If Serie <> "" Then
            Me.ListaAdd(Serie)
            LimpiaCaja()
        End If
        RaiseEvent ListChanged()
    End Sub


    Public Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim Serie As String
        Serie = Me.lstSeries.SelectedItem

        Me.ListaRemove(Serie)
        RaiseEvent ListChanged()
    End Sub



#End Region

#Region "Property's"

    Public ReadOnly Property Count() As Integer
        Get
            Return lstSeries.Items.Count

        End Get
    End Property

    Public ReadOnly Property Items(ByVal Index As Integer) As String
        Get
            Return lstSeries.Items(Index)
        End Get
    End Property

    Private bEntrada As Boolean
    Public Property Entrada() As Boolean
        Get
            Return bEntrada
        End Get
        Set(ByVal Value As Boolean)
            bEntrada = Value
            Me.txtSerie.Visible = Value
            Me.lkpSerie.Visible = Not Value
        End Set
    End Property

    Private ReadOnly Property IsEntrada() As Boolean
        Get
            Return bEntrada
        End Get
    End Property


#End Region


End Class
