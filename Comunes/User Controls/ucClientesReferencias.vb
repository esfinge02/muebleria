Public Class ucClientesReferencias
    Inherits System.Windows.Forms.UserControl

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents pnlConfidenciales As System.Windows.Forms.Panel
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents lblExtension As System.Windows.Forms.Label
    Friend WithEvents lblTelefonoNextel As System.Windows.Forms.Label
    Friend WithEvents lblTelCasa As System.Windows.Forms.Label
    Friend WithEvents lblTelOficina As System.Windows.Forms.Label
    Friend WithEvents lblTelCelular As System.Windows.Forms.Label
    Friend WithEvents lblCodigoPostal As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents txtNombreReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDomicilioReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TinCalcEdit1 As Dipros.Editors.TINCalcEdit
    Friend WithEvents TinMultiLookup1 As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextEdit13 As DevExpress.XtraEditors.TextEdit
    Public WithEvents TinCalcEdit2 As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextEdit14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextEdit15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextEdit16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextEdit17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextEdit18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextEdit19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextEdit20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEstadoReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCiudadReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtColoniaReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNextelReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTelCasaReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTelOficinaReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCelularReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtTelCasaReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtTelOficinaReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtCorreoReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents txtTrabajaReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents txtParentescoReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents txtEstadoReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents txtCiudadReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents txtNombreReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents txtDomicilioReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents txtColoniaReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents txtOtrosReferencia2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtTelCasaReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTelOficinaReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCelularReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNombreReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDomicilioReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtColoniaReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNextelReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCorreoReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTrabajaReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtParentescoReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEstadoReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCiudadReferencia3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOtrosReferencia3 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents clcCodigoPostalRef1 As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtTrabajaReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCorreoReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcCodigoPostalRef2 As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtCelularReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNextelReferencia2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtParentescoReferencia1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcCodigoPostalRef3 As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblOtros As System.Windows.Forms.Label
    Friend WithEvents txtOtrosReferencia1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents clcExtensionReferencia1 As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcExtensionReferencia2 As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcExtensionReferencia3 As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.pnlConfidenciales = New System.Windows.Forms.Panel
        Me.clcExtensionReferencia1 = New Dipros.Editors.TINCalcEdit
        Me.lblOtros = New System.Windows.Forms.Label
        Me.txtOtrosReferencia1 = New DevExpress.XtraEditors.MemoEdit
        Me.lblExtension = New System.Windows.Forms.Label
        Me.txtNextelReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefonoNextel = New System.Windows.Forms.Label
        Me.lblTelCasa = New System.Windows.Forms.Label
        Me.txtTelCasaReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.lblTelOficina = New System.Windows.Forms.Label
        Me.txtTelOficinaReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.lblTelCelular = New System.Windows.Forms.Label
        Me.txtCelularReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.lblCodigoPostal = New System.Windows.Forms.Label
        Me.clcCodigoPostalRef1 = New Dipros.Editors.TINCalcEdit
        Me.lblEmail = New System.Windows.Forms.Label
        Me.txtCorreoReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.Label31 = New System.Windows.Forms.Label
        Me.txtTrabajaReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtParentescoReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtEstadoReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.Label25 = New System.Windows.Forms.Label
        Me.txtCiudadReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.Label27 = New System.Windows.Forms.Label
        Me.txtNombreReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtDomicilioReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtColoniaReferencia1 = New DevExpress.XtraEditors.TextEdit
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.clcExtensionReferencia2 = New Dipros.Editors.TINCalcEdit
        Me.Label59 = New System.Windows.Forms.Label
        Me.txtOtrosReferencia2 = New DevExpress.XtraEditors.MemoEdit
        Me.Label43 = New System.Windows.Forms.Label
        Me.txtNextelReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label44 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.txtTelCasaReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label46 = New System.Windows.Forms.Label
        Me.txtTelOficinaReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label47 = New System.Windows.Forms.Label
        Me.txtCelularReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label48 = New System.Windows.Forms.Label
        Me.clcCodigoPostalRef2 = New Dipros.Editors.TINCalcEdit
        Me.Label50 = New System.Windows.Forms.Label
        Me.txtCorreoReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label52 = New System.Windows.Forms.Label
        Me.txtTrabajaReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label53 = New System.Windows.Forms.Label
        Me.txtParentescoReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label54 = New System.Windows.Forms.Label
        Me.txtEstadoReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label55 = New System.Windows.Forms.Label
        Me.txtCiudadReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label56 = New System.Windows.Forms.Label
        Me.txtNombreReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label57 = New System.Windows.Forms.Label
        Me.txtDomicilioReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.Label58 = New System.Windows.Forms.Label
        Me.txtColoniaReferencia2 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.TinCalcEdit1 = New Dipros.Editors.TINCalcEdit
        Me.TinMultiLookup1 = New Dipros.Editors.TINMultiLookup
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.TextEdit13 = New DevExpress.XtraEditors.TextEdit
        Me.TinCalcEdit2 = New Dipros.Editors.TINCalcEdit
        Me.Label12 = New System.Windows.Forms.Label
        Me.TextEdit14 = New DevExpress.XtraEditors.TextEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.TextEdit15 = New DevExpress.XtraEditors.TextEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.TextEdit16 = New DevExpress.XtraEditors.TextEdit
        Me.Label15 = New System.Windows.Forms.Label
        Me.TextEdit17 = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.TextEdit18 = New DevExpress.XtraEditors.TextEdit
        Me.Label17 = New System.Windows.Forms.Label
        Me.TextEdit19 = New DevExpress.XtraEditors.TextEdit
        Me.Label18 = New System.Windows.Forms.Label
        Me.TextEdit20 = New DevExpress.XtraEditors.TextEdit
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.clcExtensionReferencia3 = New Dipros.Editors.TINCalcEdit
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtOtrosReferencia3 = New DevExpress.XtraEditors.MemoEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtNextelReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtTelCasaReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtTelOficinaReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtCelularReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label26 = New System.Windows.Forms.Label
        Me.clcCodigoPostalRef3 = New Dipros.Editors.TINCalcEdit
        Me.Label32 = New System.Windows.Forms.Label
        Me.txtCorreoReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtTrabajaReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label37 = New System.Windows.Forms.Label
        Me.txtParentescoReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label38 = New System.Windows.Forms.Label
        Me.txtEstadoReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label39 = New System.Windows.Forms.Label
        Me.txtCiudadReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label40 = New System.Windows.Forms.Label
        Me.txtNombreReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label41 = New System.Windows.Forms.Label
        Me.txtDomicilioReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.Label42 = New System.Windows.Forms.Label
        Me.txtColoniaReferencia3 = New DevExpress.XtraEditors.TextEdit
        Me.pnlConfidenciales.SuspendLayout()
        CType(Me.clcExtensionReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOtrosReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNextelReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelCasaReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelOficinaReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCelularReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCodigoPostalRef1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCorreoReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTrabajaReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtParentescoReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstadoReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudadReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilioReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColoniaReferencia1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.clcExtensionReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOtrosReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNextelReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelCasaReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelOficinaReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCelularReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCodigoPostalRef2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCorreoReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTrabajaReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtParentescoReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstadoReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudadReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilioReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColoniaReferencia2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TinCalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TinCalcEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.clcExtensionReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOtrosReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNextelReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelCasaReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelOficinaReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCelularReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCodigoPostalRef3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCorreoReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTrabajaReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtParentescoReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstadoReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudadReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilioReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColoniaReferencia3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlConfidenciales
        '
        Me.pnlConfidenciales.Controls.Add(Me.clcExtensionReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.lblOtros)
        Me.pnlConfidenciales.Controls.Add(Me.txtOtrosReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.lblExtension)
        Me.pnlConfidenciales.Controls.Add(Me.txtNextelReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.lblTelefonoNextel)
        Me.pnlConfidenciales.Controls.Add(Me.lblTelCasa)
        Me.pnlConfidenciales.Controls.Add(Me.txtTelCasaReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.lblTelOficina)
        Me.pnlConfidenciales.Controls.Add(Me.txtTelOficinaReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.lblTelCelular)
        Me.pnlConfidenciales.Controls.Add(Me.txtCelularReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.lblCodigoPostal)
        Me.pnlConfidenciales.Controls.Add(Me.clcCodigoPostalRef1)
        Me.pnlConfidenciales.Controls.Add(Me.lblEmail)
        Me.pnlConfidenciales.Controls.Add(Me.txtCorreoReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.Label31)
        Me.pnlConfidenciales.Controls.Add(Me.txtTrabajaReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.Label6)
        Me.pnlConfidenciales.Controls.Add(Me.txtParentescoReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.Label7)
        Me.pnlConfidenciales.Controls.Add(Me.txtEstadoReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.Label25)
        Me.pnlConfidenciales.Controls.Add(Me.txtCiudadReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.Label27)
        Me.pnlConfidenciales.Controls.Add(Me.txtNombreReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.Label28)
        Me.pnlConfidenciales.Controls.Add(Me.txtDomicilioReferencia1)
        Me.pnlConfidenciales.Controls.Add(Me.Label29)
        Me.pnlConfidenciales.Controls.Add(Me.txtColoniaReferencia1)
        Me.pnlConfidenciales.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlConfidenciales.Location = New System.Drawing.Point(0, 0)
        Me.pnlConfidenciales.Name = "pnlConfidenciales"
        Me.pnlConfidenciales.Size = New System.Drawing.Size(696, 310)
        Me.pnlConfidenciales.TabIndex = 0
        '
        'clcExtensionReferencia1
        '
        Me.clcExtensionReferencia1.EditValue = "0"
        Me.clcExtensionReferencia1.Location = New System.Drawing.Point(348, 81)
        Me.clcExtensionReferencia1.MaxValue = 0
        Me.clcExtensionReferencia1.MinValue = 0
        Me.clcExtensionReferencia1.Name = "clcExtensionReferencia1"
        '
        'clcExtensionReferencia1.Properties
        '
        Me.clcExtensionReferencia1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExtensionReferencia1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExtensionReferencia1.Properties.MaskData.EditMask = "#"
        Me.clcExtensionReferencia1.Properties.MaxLength = 5
        Me.clcExtensionReferencia1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcExtensionReferencia1.Size = New System.Drawing.Size(32, 19)
        Me.clcExtensionReferencia1.TabIndex = 13
        Me.clcExtensionReferencia1.Tag = "cp"
        '
        'lblOtros
        '
        Me.lblOtros.AutoSize = True
        Me.lblOtros.Location = New System.Drawing.Point(36, 224)
        Me.lblOtros.Name = "lblOtros"
        Me.lblOtros.Size = New System.Drawing.Size(35, 16)
        Me.lblOtros.TabIndex = 28
        Me.lblOtros.Tag = ""
        Me.lblOtros.Text = "Otro&s:"
        Me.lblOtros.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtOtrosReferencia1
        '
        Me.txtOtrosReferencia1.EditValue = ""
        Me.txtOtrosReferencia1.Location = New System.Drawing.Point(80, 224)
        Me.txtOtrosReferencia1.Name = "txtOtrosReferencia1"
        '
        'txtOtrosReferencia1.Properties
        '
        Me.txtOtrosReferencia1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOtrosReferencia1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtOtrosReferencia1.Size = New System.Drawing.Size(448, 48)
        Me.txtOtrosReferencia1.TabIndex = 29
        Me.txtOtrosReferencia1.Tag = "observaciones"
        '
        'lblExtension
        '
        Me.lblExtension.AutoSize = True
        Me.lblExtension.Location = New System.Drawing.Point(315, 80)
        Me.lblExtension.Name = "lblExtension"
        Me.lblExtension.Size = New System.Drawing.Size(27, 16)
        Me.lblExtension.TabIndex = 12
        Me.lblExtension.Tag = ""
        Me.lblExtension.Text = "Ex&t.:"
        Me.lblExtension.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNextelReferencia1
        '
        Me.txtNextelReferencia1.EditValue = ""
        Me.txtNextelReferencia1.Location = New System.Drawing.Point(611, 80)
        Me.txtNextelReferencia1.Name = "txtNextelReferencia1"
        '
        'txtNextelReferencia1.Properties
        '
        Me.txtNextelReferencia1.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtNextelReferencia1.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtNextelReferencia1.Properties.MaxLength = 10
        Me.txtNextelReferencia1.Size = New System.Drawing.Size(72, 20)
        Me.txtNextelReferencia1.TabIndex = 17
        Me.txtNextelReferencia1.Tag = "nextel"
        '
        'lblTelefonoNextel
        '
        Me.lblTelefonoNextel.AutoSize = True
        Me.lblTelefonoNextel.Location = New System.Drawing.Point(547, 80)
        Me.lblTelefonoNextel.Name = "lblTelefonoNextel"
        Me.lblTelefonoNextel.Size = New System.Drawing.Size(65, 16)
        Me.lblTelefonoNextel.TabIndex = 16
        Me.lblTelefonoNextel.Tag = ""
        Me.lblTelefonoNextel.Text = "Tel. &Nextel.:"
        Me.lblTelefonoNextel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTelCasa
        '
        Me.lblTelCasa.AutoSize = True
        Me.lblTelCasa.Location = New System.Drawing.Point(15, 80)
        Me.lblTelCasa.Name = "lblTelCasa"
        Me.lblTelCasa.Size = New System.Drawing.Size(56, 16)
        Me.lblTelCasa.TabIndex = 8
        Me.lblTelCasa.Tag = ""
        Me.lblTelCasa.Text = "Tel. &Casa:"
        Me.lblTelCasa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelCasaReferencia1
        '
        Me.txtTelCasaReferencia1.EditValue = ""
        Me.txtTelCasaReferencia1.Location = New System.Drawing.Point(80, 80)
        Me.txtTelCasaReferencia1.Name = "txtTelCasaReferencia1"
        '
        'txtTelCasaReferencia1.Properties
        '
        Me.txtTelCasaReferencia1.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelCasaReferencia1.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelCasaReferencia1.Properties.MaxLength = 10
        Me.txtTelCasaReferencia1.Size = New System.Drawing.Size(71, 20)
        Me.txtTelCasaReferencia1.TabIndex = 9
        Me.txtTelCasaReferencia1.Tag = "telefono1"
        '
        'lblTelOficina
        '
        Me.lblTelOficina.AutoSize = True
        Me.lblTelOficina.Location = New System.Drawing.Point(163, 80)
        Me.lblTelOficina.Name = "lblTelOficina"
        Me.lblTelOficina.Size = New System.Drawing.Size(65, 16)
        Me.lblTelOficina.TabIndex = 10
        Me.lblTelOficina.Tag = ""
        Me.lblTelOficina.Text = "Tel. &Oficina:"
        Me.lblTelOficina.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelOficinaReferencia1
        '
        Me.txtTelOficinaReferencia1.EditValue = ""
        Me.txtTelOficinaReferencia1.Location = New System.Drawing.Point(235, 80)
        Me.txtTelOficinaReferencia1.Name = "txtTelOficinaReferencia1"
        '
        'txtTelOficinaReferencia1.Properties
        '
        Me.txtTelOficinaReferencia1.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelOficinaReferencia1.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelOficinaReferencia1.Properties.MaxLength = 10
        Me.txtTelOficinaReferencia1.Size = New System.Drawing.Size(72, 20)
        Me.txtTelOficinaReferencia1.TabIndex = 11
        Me.txtTelOficinaReferencia1.Tag = "telefono2"
        '
        'lblTelCelular
        '
        Me.lblTelCelular.AutoSize = True
        Me.lblTelCelular.Location = New System.Drawing.Point(403, 80)
        Me.lblTelCelular.Name = "lblTelCelular"
        Me.lblTelCelular.Size = New System.Drawing.Size(49, 16)
        Me.lblTelCelular.TabIndex = 14
        Me.lblTelCelular.Tag = ""
        Me.lblTelCelular.Text = "Tel. &Cel.:"
        Me.lblTelCelular.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCelularReferencia1
        '
        Me.txtCelularReferencia1.EditValue = ""
        Me.txtCelularReferencia1.Location = New System.Drawing.Point(459, 80)
        Me.txtCelularReferencia1.Name = "txtCelularReferencia1"
        '
        'txtCelularReferencia1.Properties
        '
        Me.txtCelularReferencia1.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtCelularReferencia1.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtCelularReferencia1.Properties.MaxLength = 13
        Me.txtCelularReferencia1.Size = New System.Drawing.Size(72, 20)
        Me.txtCelularReferencia1.TabIndex = 15
        Me.txtCelularReferencia1.Tag = "fax"
        '
        'lblCodigoPostal
        '
        Me.lblCodigoPostal.AutoSize = True
        Me.lblCodigoPostal.Location = New System.Drawing.Point(443, 56)
        Me.lblCodigoPostal.Name = "lblCodigoPostal"
        Me.lblCodigoPostal.Size = New System.Drawing.Size(22, 16)
        Me.lblCodigoPostal.TabIndex = 6
        Me.lblCodigoPostal.Tag = ""
        Me.lblCodigoPostal.Text = "Cp&:"
        Me.lblCodigoPostal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCodigoPostalRef1
        '
        Me.clcCodigoPostalRef1.EditValue = "0"
        Me.clcCodigoPostalRef1.Location = New System.Drawing.Point(475, 56)
        Me.clcCodigoPostalRef1.MaxValue = 0
        Me.clcCodigoPostalRef1.MinValue = 0
        Me.clcCodigoPostalRef1.Name = "clcCodigoPostalRef1"
        '
        'clcCodigoPostalRef1.Properties
        '
        Me.clcCodigoPostalRef1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostalRef1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostalRef1.Properties.MaskData.EditMask = "#"
        Me.clcCodigoPostalRef1.Properties.MaxLength = 5
        Me.clcCodigoPostalRef1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCodigoPostalRef1.Size = New System.Drawing.Size(56, 19)
        Me.clcCodigoPostalRef1.TabIndex = 7
        Me.clcCodigoPostalRef1.Tag = "cp"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(35, 200)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(36, 16)
        Me.lblEmail.TabIndex = 26
        Me.lblEmail.Tag = ""
        Me.lblEmail.Text = "Email:"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCorreoReferencia1
        '
        Me.txtCorreoReferencia1.EditValue = ""
        Me.txtCorreoReferencia1.Location = New System.Drawing.Point(80, 200)
        Me.txtCorreoReferencia1.Name = "txtCorreoReferencia1"
        '
        'txtCorreoReferencia1.Properties
        '
        Me.txtCorreoReferencia1.Properties.MaxLength = 100
        Me.txtCorreoReferencia1.Size = New System.Drawing.Size(416, 20)
        Me.txtCorreoReferencia1.TabIndex = 27
        Me.txtCorreoReferencia1.Tag = "email"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(10, 176)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(61, 16)
        Me.Label31.TabIndex = 24
        Me.Label31.Tag = ""
        Me.Label31.Text = "Trabaja en:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTrabajaReferencia1
        '
        Me.txtTrabajaReferencia1.EditValue = ""
        Me.txtTrabajaReferencia1.Location = New System.Drawing.Point(80, 176)
        Me.txtTrabajaReferencia1.Name = "txtTrabajaReferencia1"
        '
        'txtTrabajaReferencia1.Properties
        '
        Me.txtTrabajaReferencia1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTrabajaReferencia1.Properties.MaxLength = 50
        Me.txtTrabajaReferencia1.Size = New System.Drawing.Size(300, 20)
        Me.txtTrabajaReferencia1.TabIndex = 25
        Me.txtTrabajaReferencia1.Tag = "trabaja_en_referencia"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 152)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 16)
        Me.Label6.TabIndex = 22
        Me.Label6.Tag = ""
        Me.Label6.Text = "Parentesco:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtParentescoReferencia1
        '
        Me.txtParentescoReferencia1.EditValue = ""
        Me.txtParentescoReferencia1.Location = New System.Drawing.Point(80, 152)
        Me.txtParentescoReferencia1.Name = "txtParentescoReferencia1"
        '
        'txtParentescoReferencia1.Properties
        '
        Me.txtParentescoReferencia1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtParentescoReferencia1.Properties.MaxLength = 50
        Me.txtParentescoReferencia1.Size = New System.Drawing.Size(300, 20)
        Me.txtParentescoReferencia1.TabIndex = 23
        Me.txtParentescoReferencia1.Tag = "parentesco_referencia"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(28, 128)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 16)
        Me.Label7.TabIndex = 20
        Me.Label7.Tag = ""
        Me.Label7.Text = "Estado:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstadoReferencia1
        '
        Me.txtEstadoReferencia1.EditValue = ""
        Me.txtEstadoReferencia1.Location = New System.Drawing.Point(80, 128)
        Me.txtEstadoReferencia1.Name = "txtEstadoReferencia1"
        '
        'txtEstadoReferencia1.Properties
        '
        Me.txtEstadoReferencia1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoReferencia1.Properties.MaxLength = 50
        Me.txtEstadoReferencia1.Size = New System.Drawing.Size(300, 20)
        Me.txtEstadoReferencia1.TabIndex = 21
        Me.txtEstadoReferencia1.Tag = "estado_referencia"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(28, 104)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(43, 16)
        Me.Label25.TabIndex = 18
        Me.Label25.Tag = ""
        Me.Label25.Text = "Ciudad:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudadReferencia1
        '
        Me.txtCiudadReferencia1.EditValue = ""
        Me.txtCiudadReferencia1.Location = New System.Drawing.Point(80, 104)
        Me.txtCiudadReferencia1.Name = "txtCiudadReferencia1"
        '
        'txtCiudadReferencia1.Properties
        '
        Me.txtCiudadReferencia1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudadReferencia1.Properties.MaxLength = 50
        Me.txtCiudadReferencia1.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudadReferencia1.TabIndex = 19
        Me.txtCiudadReferencia1.Tag = "ciudad_referencia"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(23, 8)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(48, 16)
        Me.Label27.TabIndex = 0
        Me.Label27.Tag = ""
        Me.Label27.Text = "Nombre:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombreReferencia1
        '
        Me.txtNombreReferencia1.EditValue = ""
        Me.txtNombreReferencia1.Location = New System.Drawing.Point(80, 8)
        Me.txtNombreReferencia1.Name = "txtNombreReferencia1"
        '
        'txtNombreReferencia1.Properties
        '
        Me.txtNombreReferencia1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreReferencia1.Properties.MaxLength = 100
        Me.txtNombreReferencia1.Size = New System.Drawing.Size(600, 20)
        Me.txtNombreReferencia1.TabIndex = 1
        Me.txtNombreReferencia1.Tag = "nombre_referencia"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(17, 32)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(54, 16)
        Me.Label28.TabIndex = 2
        Me.Label28.Tag = ""
        Me.Label28.Text = "Domici&lio:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilioReferencia1
        '
        Me.txtDomicilioReferencia1.EditValue = ""
        Me.txtDomicilioReferencia1.Location = New System.Drawing.Point(80, 32)
        Me.txtDomicilioReferencia1.Name = "txtDomicilioReferencia1"
        '
        'txtDomicilioReferencia1.Properties
        '
        Me.txtDomicilioReferencia1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDomicilioReferencia1.Properties.MaxLength = 50
        Me.txtDomicilioReferencia1.Size = New System.Drawing.Size(300, 20)
        Me.txtDomicilioReferencia1.TabIndex = 3
        Me.txtDomicilioReferencia1.Tag = "domicilio_referencia"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(25, 56)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(46, 16)
        Me.Label29.TabIndex = 4
        Me.Label29.Tag = ""
        Me.Label29.Text = "Colonia:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColoniaReferencia1
        '
        Me.txtColoniaReferencia1.EditValue = ""
        Me.txtColoniaReferencia1.Location = New System.Drawing.Point(80, 56)
        Me.txtColoniaReferencia1.Name = "txtColoniaReferencia1"
        '
        'txtColoniaReferencia1.Properties
        '
        Me.txtColoniaReferencia1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtColoniaReferencia1.Properties.MaxLength = 50
        Me.txtColoniaReferencia1.Size = New System.Drawing.Size(300, 20)
        Me.txtColoniaReferencia1.TabIndex = 5
        Me.txtColoniaReferencia1.Tag = "colonia_referencia"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(704, 336)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage1.Controls.Add(Me.pnlConfidenciales)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(696, 310)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Referencia 1"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(696, 310)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Referencia 2"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.TextEdit1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.TextEdit2)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.TextEdit3)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.TextEdit4)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.TextEdit11)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.TinCalcEdit1)
        Me.Panel1.Controls.Add(Me.TinMultiLookup1)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.TextEdit13)
        Me.Panel1.Controls.Add(Me.TinCalcEdit2)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.TextEdit14)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.TextEdit15)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.TextEdit16)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.TextEdit17)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.TextEdit18)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.TextEdit19)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.TextEdit20)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(696, 310)
        Me.Panel1.TabIndex = 63
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.Window
        Me.Panel3.Controls.Add(Me.clcExtensionReferencia2)
        Me.Panel3.Controls.Add(Me.Label59)
        Me.Panel3.Controls.Add(Me.txtOtrosReferencia2)
        Me.Panel3.Controls.Add(Me.Label43)
        Me.Panel3.Controls.Add(Me.txtNextelReferencia2)
        Me.Panel3.Controls.Add(Me.Label44)
        Me.Panel3.Controls.Add(Me.Label45)
        Me.Panel3.Controls.Add(Me.txtTelCasaReferencia2)
        Me.Panel3.Controls.Add(Me.Label46)
        Me.Panel3.Controls.Add(Me.txtTelOficinaReferencia2)
        Me.Panel3.Controls.Add(Me.Label47)
        Me.Panel3.Controls.Add(Me.txtCelularReferencia2)
        Me.Panel3.Controls.Add(Me.Label48)
        Me.Panel3.Controls.Add(Me.clcCodigoPostalRef2)
        Me.Panel3.Controls.Add(Me.Label50)
        Me.Panel3.Controls.Add(Me.txtCorreoReferencia2)
        Me.Panel3.Controls.Add(Me.Label52)
        Me.Panel3.Controls.Add(Me.txtTrabajaReferencia2)
        Me.Panel3.Controls.Add(Me.Label53)
        Me.Panel3.Controls.Add(Me.txtParentescoReferencia2)
        Me.Panel3.Controls.Add(Me.Label54)
        Me.Panel3.Controls.Add(Me.txtEstadoReferencia2)
        Me.Panel3.Controls.Add(Me.Label55)
        Me.Panel3.Controls.Add(Me.txtCiudadReferencia2)
        Me.Panel3.Controls.Add(Me.Label56)
        Me.Panel3.Controls.Add(Me.txtNombreReferencia2)
        Me.Panel3.Controls.Add(Me.Label57)
        Me.Panel3.Controls.Add(Me.txtDomicilioReferencia2)
        Me.Panel3.Controls.Add(Me.Label58)
        Me.Panel3.Controls.Add(Me.txtColoniaReferencia2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(696, 310)
        Me.Panel3.TabIndex = 183
        '
        'clcExtensionReferencia2
        '
        Me.clcExtensionReferencia2.EditValue = "0"
        Me.clcExtensionReferencia2.Location = New System.Drawing.Point(348, 81)
        Me.clcExtensionReferencia2.MaxValue = 0
        Me.clcExtensionReferencia2.MinValue = 0
        Me.clcExtensionReferencia2.Name = "clcExtensionReferencia2"
        '
        'clcExtensionReferencia2.Properties
        '
        Me.clcExtensionReferencia2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExtensionReferencia2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExtensionReferencia2.Properties.MaskData.EditMask = "#"
        Me.clcExtensionReferencia2.Properties.MaxLength = 5
        Me.clcExtensionReferencia2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcExtensionReferencia2.Size = New System.Drawing.Size(32, 19)
        Me.clcExtensionReferencia2.TabIndex = 74
        Me.clcExtensionReferencia2.Tag = "cp"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(36, 224)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(35, 16)
        Me.Label59.TabIndex = 89
        Me.Label59.Tag = ""
        Me.Label59.Text = "Otro&s:"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtOtrosReferencia2
        '
        Me.txtOtrosReferencia2.EditValue = ""
        Me.txtOtrosReferencia2.Location = New System.Drawing.Point(80, 224)
        Me.txtOtrosReferencia2.Name = "txtOtrosReferencia2"
        '
        'txtOtrosReferencia2.Properties
        '
        Me.txtOtrosReferencia2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOtrosReferencia2.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtOtrosReferencia2.Size = New System.Drawing.Size(448, 48)
        Me.txtOtrosReferencia2.TabIndex = 90
        Me.txtOtrosReferencia2.Tag = "observaciones"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(315, 80)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(27, 16)
        Me.Label43.TabIndex = 73
        Me.Label43.Tag = ""
        Me.Label43.Text = "Ex&t.:"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNextelReferencia2
        '
        Me.txtNextelReferencia2.EditValue = ""
        Me.txtNextelReferencia2.Location = New System.Drawing.Point(611, 80)
        Me.txtNextelReferencia2.Name = "txtNextelReferencia2"
        '
        'txtNextelReferencia2.Properties
        '
        Me.txtNextelReferencia2.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtNextelReferencia2.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtNextelReferencia2.Properties.MaxLength = 10
        Me.txtNextelReferencia2.Size = New System.Drawing.Size(72, 20)
        Me.txtNextelReferencia2.TabIndex = 78
        Me.txtNextelReferencia2.Tag = "nextel"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(547, 80)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(65, 16)
        Me.Label44.TabIndex = 77
        Me.Label44.Tag = ""
        Me.Label44.Text = "Tel. &Nextel.:"
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(15, 80)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(56, 16)
        Me.Label45.TabIndex = 69
        Me.Label45.Tag = ""
        Me.Label45.Text = "Tel. &Casa:"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelCasaReferencia2
        '
        Me.txtTelCasaReferencia2.EditValue = ""
        Me.txtTelCasaReferencia2.Location = New System.Drawing.Point(80, 80)
        Me.txtTelCasaReferencia2.Name = "txtTelCasaReferencia2"
        '
        'txtTelCasaReferencia2.Properties
        '
        Me.txtTelCasaReferencia2.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelCasaReferencia2.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelCasaReferencia2.Properties.MaxLength = 10
        Me.txtTelCasaReferencia2.Size = New System.Drawing.Size(71, 20)
        Me.txtTelCasaReferencia2.TabIndex = 70
        Me.txtTelCasaReferencia2.Tag = "telefono1"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(163, 80)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(65, 16)
        Me.Label46.TabIndex = 71
        Me.Label46.Tag = ""
        Me.Label46.Text = "Tel. &Oficina:"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelOficinaReferencia2
        '
        Me.txtTelOficinaReferencia2.EditValue = ""
        Me.txtTelOficinaReferencia2.Location = New System.Drawing.Point(235, 80)
        Me.txtTelOficinaReferencia2.Name = "txtTelOficinaReferencia2"
        '
        'txtTelOficinaReferencia2.Properties
        '
        Me.txtTelOficinaReferencia2.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelOficinaReferencia2.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelOficinaReferencia2.Properties.MaxLength = 10
        Me.txtTelOficinaReferencia2.Size = New System.Drawing.Size(72, 20)
        Me.txtTelOficinaReferencia2.TabIndex = 72
        Me.txtTelOficinaReferencia2.Tag = "telefono2"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(403, 80)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(49, 16)
        Me.Label47.TabIndex = 75
        Me.Label47.Tag = ""
        Me.Label47.Text = "Tel. &Cel.:"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCelularReferencia2
        '
        Me.txtCelularReferencia2.EditValue = ""
        Me.txtCelularReferencia2.Location = New System.Drawing.Point(459, 80)
        Me.txtCelularReferencia2.Name = "txtCelularReferencia2"
        '
        'txtCelularReferencia2.Properties
        '
        Me.txtCelularReferencia2.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtCelularReferencia2.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtCelularReferencia2.Properties.MaxLength = 13
        Me.txtCelularReferencia2.Size = New System.Drawing.Size(72, 20)
        Me.txtCelularReferencia2.TabIndex = 76
        Me.txtCelularReferencia2.Tag = "fax"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(443, 56)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(22, 16)
        Me.Label48.TabIndex = 67
        Me.Label48.Tag = ""
        Me.Label48.Text = "Cp&:"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCodigoPostalRef2
        '
        Me.clcCodigoPostalRef2.EditValue = "0"
        Me.clcCodigoPostalRef2.Location = New System.Drawing.Point(475, 56)
        Me.clcCodigoPostalRef2.MaxValue = 0
        Me.clcCodigoPostalRef2.MinValue = 0
        Me.clcCodigoPostalRef2.Name = "clcCodigoPostalRef2"
        '
        'clcCodigoPostalRef2.Properties
        '
        Me.clcCodigoPostalRef2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostalRef2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostalRef2.Properties.MaskData.EditMask = "#"
        Me.clcCodigoPostalRef2.Properties.MaxLength = 5
        Me.clcCodigoPostalRef2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCodigoPostalRef2.Size = New System.Drawing.Size(56, 19)
        Me.clcCodigoPostalRef2.TabIndex = 68
        Me.clcCodigoPostalRef2.Tag = "cp"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(35, 200)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(36, 16)
        Me.Label50.TabIndex = 87
        Me.Label50.Tag = ""
        Me.Label50.Text = "Email:"
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCorreoReferencia2
        '
        Me.txtCorreoReferencia2.EditValue = ""
        Me.txtCorreoReferencia2.Location = New System.Drawing.Point(80, 200)
        Me.txtCorreoReferencia2.Name = "txtCorreoReferencia2"
        '
        'txtCorreoReferencia2.Properties
        '
        Me.txtCorreoReferencia2.Properties.MaxLength = 100
        Me.txtCorreoReferencia2.Size = New System.Drawing.Size(416, 20)
        Me.txtCorreoReferencia2.TabIndex = 88
        Me.txtCorreoReferencia2.Tag = "email"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(10, 176)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(61, 16)
        Me.Label52.TabIndex = 85
        Me.Label52.Tag = ""
        Me.Label52.Text = "Trabaja en:"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTrabajaReferencia2
        '
        Me.txtTrabajaReferencia2.EditValue = ""
        Me.txtTrabajaReferencia2.Location = New System.Drawing.Point(80, 176)
        Me.txtTrabajaReferencia2.Name = "txtTrabajaReferencia2"
        '
        'txtTrabajaReferencia2.Properties
        '
        Me.txtTrabajaReferencia2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTrabajaReferencia2.Properties.MaxLength = 50
        Me.txtTrabajaReferencia2.Size = New System.Drawing.Size(300, 20)
        Me.txtTrabajaReferencia2.TabIndex = 86
        Me.txtTrabajaReferencia2.Tag = "trabaja_en_referencia"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(6, 152)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(65, 16)
        Me.Label53.TabIndex = 83
        Me.Label53.Tag = ""
        Me.Label53.Text = "Parentesco:"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtParentescoReferencia2
        '
        Me.txtParentescoReferencia2.EditValue = ""
        Me.txtParentescoReferencia2.Location = New System.Drawing.Point(80, 152)
        Me.txtParentescoReferencia2.Name = "txtParentescoReferencia2"
        '
        'txtParentescoReferencia2.Properties
        '
        Me.txtParentescoReferencia2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtParentescoReferencia2.Properties.MaxLength = 50
        Me.txtParentescoReferencia2.Size = New System.Drawing.Size(300, 20)
        Me.txtParentescoReferencia2.TabIndex = 84
        Me.txtParentescoReferencia2.Tag = "parentesco_referencia"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(28, 128)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(43, 16)
        Me.Label54.TabIndex = 81
        Me.Label54.Tag = ""
        Me.Label54.Text = "Estado:"
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstadoReferencia2
        '
        Me.txtEstadoReferencia2.EditValue = ""
        Me.txtEstadoReferencia2.Location = New System.Drawing.Point(80, 128)
        Me.txtEstadoReferencia2.Name = "txtEstadoReferencia2"
        '
        'txtEstadoReferencia2.Properties
        '
        Me.txtEstadoReferencia2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoReferencia2.Properties.MaxLength = 50
        Me.txtEstadoReferencia2.Size = New System.Drawing.Size(300, 20)
        Me.txtEstadoReferencia2.TabIndex = 82
        Me.txtEstadoReferencia2.Tag = "estado_referencia"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(28, 104)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(43, 16)
        Me.Label55.TabIndex = 79
        Me.Label55.Tag = ""
        Me.Label55.Text = "Ciudad:"
        Me.Label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudadReferencia2
        '
        Me.txtCiudadReferencia2.EditValue = ""
        Me.txtCiudadReferencia2.Location = New System.Drawing.Point(80, 104)
        Me.txtCiudadReferencia2.Name = "txtCiudadReferencia2"
        '
        'txtCiudadReferencia2.Properties
        '
        Me.txtCiudadReferencia2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudadReferencia2.Properties.MaxLength = 50
        Me.txtCiudadReferencia2.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudadReferencia2.TabIndex = 80
        Me.txtCiudadReferencia2.Tag = "ciudad_referencia"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(23, 8)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(48, 16)
        Me.Label56.TabIndex = 61
        Me.Label56.Tag = ""
        Me.Label56.Text = "Nombre:"
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombreReferencia2
        '
        Me.txtNombreReferencia2.EditValue = ""
        Me.txtNombreReferencia2.Location = New System.Drawing.Point(80, 8)
        Me.txtNombreReferencia2.Name = "txtNombreReferencia2"
        '
        'txtNombreReferencia2.Properties
        '
        Me.txtNombreReferencia2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreReferencia2.Properties.MaxLength = 100
        Me.txtNombreReferencia2.Size = New System.Drawing.Size(600, 20)
        Me.txtNombreReferencia2.TabIndex = 62
        Me.txtNombreReferencia2.Tag = "nombre_referencia"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(17, 32)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(54, 16)
        Me.Label57.TabIndex = 63
        Me.Label57.Tag = ""
        Me.Label57.Text = "Domici&lio:"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilioReferencia2
        '
        Me.txtDomicilioReferencia2.EditValue = ""
        Me.txtDomicilioReferencia2.Location = New System.Drawing.Point(80, 32)
        Me.txtDomicilioReferencia2.Name = "txtDomicilioReferencia2"
        '
        'txtDomicilioReferencia2.Properties
        '
        Me.txtDomicilioReferencia2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDomicilioReferencia2.Properties.MaxLength = 50
        Me.txtDomicilioReferencia2.Size = New System.Drawing.Size(300, 20)
        Me.txtDomicilioReferencia2.TabIndex = 64
        Me.txtDomicilioReferencia2.Tag = "domicilio_referencia"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(25, 56)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(46, 16)
        Me.Label58.TabIndex = 65
        Me.Label58.Tag = ""
        Me.Label58.Text = "Colonia:"
        Me.Label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColoniaReferencia2
        '
        Me.txtColoniaReferencia2.EditValue = ""
        Me.txtColoniaReferencia2.Location = New System.Drawing.Point(80, 56)
        Me.txtColoniaReferencia2.Name = "txtColoniaReferencia2"
        '
        'txtColoniaReferencia2.Properties
        '
        Me.txtColoniaReferencia2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtColoniaReferencia2.Properties.MaxLength = 50
        Me.txtColoniaReferencia2.Size = New System.Drawing.Size(300, 20)
        Me.txtColoniaReferencia2.TabIndex = 66
        Me.txtColoniaReferencia2.Tag = "colonia_referencia"
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = ""
        Me.TextEdit1.Location = New System.Drawing.Point(347, 96)
        Me.TextEdit1.Name = "TextEdit1"
        '
        'TextEdit1.Properties
        '
        Me.TextEdit1.Properties.MaxLength = 4
        Me.TextEdit1.Size = New System.Drawing.Size(32, 20)
        Me.TextEdit1.TabIndex = 182
        Me.TextEdit1.Tag = "extension"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(315, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 16)
        Me.Label1.TabIndex = 181
        Me.Label1.Tag = ""
        Me.Label1.Text = "Ex&t.:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit2
        '
        Me.TextEdit2.EditValue = ""
        Me.TextEdit2.Location = New System.Drawing.Point(611, 96)
        Me.TextEdit2.Name = "TextEdit2"
        '
        'TextEdit2.Properties
        '
        Me.TextEdit2.Properties.MaxLength = 10
        Me.TextEdit2.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit2.TabIndex = 180
        Me.TextEdit2.Tag = "nextel"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(547, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 179
        Me.Label2.Tag = ""
        Me.Label2.Text = "Tel. &Nextel.:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 16)
        Me.Label3.TabIndex = 173
        Me.Label3.Tag = ""
        Me.Label3.Text = "Tel. &Casa:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = ""
        Me.TextEdit3.Location = New System.Drawing.Point(83, 96)
        Me.TextEdit3.Name = "TextEdit3"
        '
        'TextEdit3.Properties
        '
        Me.TextEdit3.Properties.MaxLength = 10
        Me.TextEdit3.Size = New System.Drawing.Size(71, 20)
        Me.TextEdit3.TabIndex = 174
        Me.TextEdit3.Tag = "telefono1"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(163, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 16)
        Me.Label4.TabIndex = 175
        Me.Label4.Tag = ""
        Me.Label4.Text = "Tel. &Oficina:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = ""
        Me.TextEdit4.Location = New System.Drawing.Point(235, 96)
        Me.TextEdit4.Name = "TextEdit4"
        '
        'TextEdit4.Properties
        '
        Me.TextEdit4.Properties.MaxLength = 10
        Me.TextEdit4.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit4.TabIndex = 176
        Me.TextEdit4.Tag = "telefono2"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(403, 96)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 16)
        Me.Label5.TabIndex = 177
        Me.Label5.Tag = ""
        Me.Label5.Text = "Tel. &Cel.:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit11
        '
        Me.TextEdit11.EditValue = ""
        Me.TextEdit11.Location = New System.Drawing.Point(459, 96)
        Me.TextEdit11.Name = "TextEdit11"
        '
        'TextEdit11.Properties
        '
        Me.TextEdit11.Properties.MaxLength = 13
        Me.TextEdit11.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit11.TabIndex = 178
        Me.TextEdit11.Tag = "fax"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(443, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(22, 16)
        Me.Label8.TabIndex = 171
        Me.Label8.Tag = ""
        Me.Label8.Text = "Cp&:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TinCalcEdit1
        '
        Me.TinCalcEdit1.EditValue = "0"
        Me.TinCalcEdit1.Location = New System.Drawing.Point(475, 64)
        Me.TinCalcEdit1.MaxValue = 0
        Me.TinCalcEdit1.MinValue = 0
        Me.TinCalcEdit1.Name = "TinCalcEdit1"
        '
        'TinCalcEdit1.Properties
        '
        Me.TinCalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TinCalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TinCalcEdit1.Properties.Enabled = False
        Me.TinCalcEdit1.Properties.MaskData.EditMask = "#"
        Me.TinCalcEdit1.Properties.MaxLength = 5
        Me.TinCalcEdit1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.TinCalcEdit1.Size = New System.Drawing.Size(56, 19)
        Me.TinCalcEdit1.TabIndex = 172
        Me.TinCalcEdit1.Tag = "cp"
        '
        'TinMultiLookup1
        '
        Me.TinMultiLookup1.AllowAdd = False
        Me.TinMultiLookup1.AutoReaload = True
        Me.TinMultiLookup1.DataSource = Nothing
        Me.TinMultiLookup1.DefaultSearchField = ""
        Me.TinMultiLookup1.DisplayMember = "descripcion"
        Me.TinMultiLookup1.EditValue = Nothing
        Me.TinMultiLookup1.Filtered = False
        Me.TinMultiLookup1.InitValue = Nothing
        Me.TinMultiLookup1.Location = New System.Drawing.Point(80, 248)
        Me.TinMultiLookup1.MultiSelect = False
        Me.TinMultiLookup1.Name = "TinMultiLookup1"
        Me.TinMultiLookup1.NullText = ""
        Me.TinMultiLookup1.PopupWidth = CType(400, Long)
        Me.TinMultiLookup1.ReadOnlyControl = False
        Me.TinMultiLookup1.Required = False
        Me.TinMultiLookup1.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.TinMultiLookup1.SearchMember = ""
        Me.TinMultiLookup1.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.TinMultiLookup1.SelectAll = True
        Me.TinMultiLookup1.Size = New System.Drawing.Size(297, 20)
        Me.TinMultiLookup1.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.TinMultiLookup1.TabIndex = 82
        Me.TinMultiLookup1.Tag = "forma_pago"
        Me.TinMultiLookup1.ToolTip = "Seleccione una Forma de Pago"
        Me.TinMultiLookup1.ValueMember = "forma_pago"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(0, 248)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 16)
        Me.Label9.TabIndex = 81
        Me.Label9.Text = "Forma Pago:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(32, 224)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(36, 16)
        Me.Label10.TabIndex = 80
        Me.Label10.Tag = ""
        Me.Label10.Text = "Email:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(2, 272)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 16)
        Me.Label11.TabIndex = 79
        Me.Label11.Tag = ""
        Me.Label11.Text = "Ultimos Dig.:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit13
        '
        Me.TextEdit13.EditValue = ""
        Me.TextEdit13.Location = New System.Drawing.Point(80, 224)
        Me.TextEdit13.Name = "TextEdit13"
        '
        'TextEdit13.Properties
        '
        Me.TextEdit13.Properties.MaxLength = 100
        Me.TextEdit13.Size = New System.Drawing.Size(416, 20)
        Me.TextEdit13.TabIndex = 78
        Me.TextEdit13.Tag = "email"
        '
        'TinCalcEdit2
        '
        Me.TinCalcEdit2.EditValue = "0"
        Me.TinCalcEdit2.Location = New System.Drawing.Point(80, 272)
        Me.TinCalcEdit2.MaxValue = 0
        Me.TinCalcEdit2.MinValue = 0
        Me.TinCalcEdit2.Name = "TinCalcEdit2"
        '
        'TinCalcEdit2.Properties
        '
        Me.TinCalcEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TinCalcEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TinCalcEdit2.Properties.Enabled = False
        Me.TinCalcEdit2.Properties.MaskData.EditMask = "###,###,##0"
        Me.TinCalcEdit2.Properties.MaxLength = 4
        Me.TinCalcEdit2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.TinCalcEdit2.Size = New System.Drawing.Size(72, 19)
        Me.TinCalcEdit2.TabIndex = 77
        Me.TinCalcEdit2.Tag = "ultimos_digitos_cuenta"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(8, 200)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(61, 16)
        Me.Label12.TabIndex = 75
        Me.Label12.Tag = ""
        Me.Label12.Text = "Trabaja en:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit14
        '
        Me.TextEdit14.EditValue = ""
        Me.TextEdit14.Location = New System.Drawing.Point(80, 200)
        Me.TextEdit14.Name = "TextEdit14"
        '
        'TextEdit14.Properties
        '
        Me.TextEdit14.Properties.MaxLength = 50
        Me.TextEdit14.Size = New System.Drawing.Size(300, 20)
        Me.TextEdit14.TabIndex = 76
        Me.TextEdit14.Tag = "trabaja_en_referencia"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 176)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(65, 16)
        Me.Label13.TabIndex = 71
        Me.Label13.Tag = ""
        Me.Label13.Text = "Parentesco:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit15
        '
        Me.TextEdit15.EditValue = ""
        Me.TextEdit15.Location = New System.Drawing.Point(80, 176)
        Me.TextEdit15.Name = "TextEdit15"
        '
        'TextEdit15.Properties
        '
        Me.TextEdit15.Properties.MaxLength = 50
        Me.TextEdit15.Size = New System.Drawing.Size(300, 20)
        Me.TextEdit15.TabIndex = 72
        Me.TextEdit15.Tag = "parentesco_referencia"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(32, 152)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(43, 16)
        Me.Label14.TabIndex = 69
        Me.Label14.Tag = ""
        Me.Label14.Text = "Estado:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit16
        '
        Me.TextEdit16.EditValue = ""
        Me.TextEdit16.Location = New System.Drawing.Point(80, 152)
        Me.TextEdit16.Name = "TextEdit16"
        '
        'TextEdit16.Properties
        '
        Me.TextEdit16.Properties.MaxLength = 50
        Me.TextEdit16.Size = New System.Drawing.Size(300, 20)
        Me.TextEdit16.TabIndex = 70
        Me.TextEdit16.Tag = "estado_referencia"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(32, 128)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 16)
        Me.Label15.TabIndex = 67
        Me.Label15.Tag = ""
        Me.Label15.Text = "Ciudad:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit17
        '
        Me.TextEdit17.EditValue = ""
        Me.TextEdit17.Location = New System.Drawing.Point(80, 128)
        Me.TextEdit17.Name = "TextEdit17"
        '
        'TextEdit17.Properties
        '
        Me.TextEdit17.Properties.MaxLength = 50
        Me.TextEdit17.Size = New System.Drawing.Size(300, 20)
        Me.TextEdit17.TabIndex = 68
        Me.TextEdit17.Tag = "ciudad_referencia"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(31, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(48, 16)
        Me.Label16.TabIndex = 61
        Me.Label16.Tag = ""
        Me.Label16.Text = "Nombre:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit18
        '
        Me.TextEdit18.EditValue = ""
        Me.TextEdit18.Location = New System.Drawing.Point(87, 16)
        Me.TextEdit18.Name = "TextEdit18"
        '
        'TextEdit18.Properties
        '
        Me.TextEdit18.Properties.MaxLength = 100
        Me.TextEdit18.Size = New System.Drawing.Size(600, 20)
        Me.TextEdit18.TabIndex = 62
        Me.TextEdit18.Tag = "nombre_referencia"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(23, 40)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(54, 16)
        Me.Label17.TabIndex = 63
        Me.Label17.Tag = ""
        Me.Label17.Text = "Domici&lio:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit19
        '
        Me.TextEdit19.EditValue = ""
        Me.TextEdit19.Location = New System.Drawing.Point(87, 40)
        Me.TextEdit19.Name = "TextEdit19"
        '
        'TextEdit19.Properties
        '
        Me.TextEdit19.Properties.MaxLength = 50
        Me.TextEdit19.Size = New System.Drawing.Size(300, 20)
        Me.TextEdit19.TabIndex = 64
        Me.TextEdit19.Tag = "domicilio_referencia"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(31, 64)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(46, 16)
        Me.Label18.TabIndex = 65
        Me.Label18.Tag = ""
        Me.Label18.Text = "Colonia:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextEdit20
        '
        Me.TextEdit20.EditValue = ""
        Me.TextEdit20.Location = New System.Drawing.Point(87, 64)
        Me.TextEdit20.Name = "TextEdit20"
        '
        'TextEdit20.Properties
        '
        Me.TextEdit20.Properties.MaxLength = 50
        Me.TextEdit20.Size = New System.Drawing.Size(300, 20)
        Me.TextEdit20.TabIndex = 66
        Me.TextEdit20.Tag = "colonia_referencia"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Panel2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(696, 310)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Referencia 3"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.Window
        Me.Panel2.Controls.Add(Me.clcExtensionReferencia3)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.txtOtrosReferencia3)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.txtNextelReferencia3)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Controls.Add(Me.Label22)
        Me.Panel2.Controls.Add(Me.txtTelCasaReferencia3)
        Me.Panel2.Controls.Add(Me.Label23)
        Me.Panel2.Controls.Add(Me.txtTelOficinaReferencia3)
        Me.Panel2.Controls.Add(Me.Label24)
        Me.Panel2.Controls.Add(Me.txtCelularReferencia3)
        Me.Panel2.Controls.Add(Me.Label26)
        Me.Panel2.Controls.Add(Me.clcCodigoPostalRef3)
        Me.Panel2.Controls.Add(Me.Label32)
        Me.Panel2.Controls.Add(Me.txtCorreoReferencia3)
        Me.Panel2.Controls.Add(Me.Label36)
        Me.Panel2.Controls.Add(Me.txtTrabajaReferencia3)
        Me.Panel2.Controls.Add(Me.Label37)
        Me.Panel2.Controls.Add(Me.txtParentescoReferencia3)
        Me.Panel2.Controls.Add(Me.Label38)
        Me.Panel2.Controls.Add(Me.txtEstadoReferencia3)
        Me.Panel2.Controls.Add(Me.Label39)
        Me.Panel2.Controls.Add(Me.txtCiudadReferencia3)
        Me.Panel2.Controls.Add(Me.Label40)
        Me.Panel2.Controls.Add(Me.txtNombreReferencia3)
        Me.Panel2.Controls.Add(Me.Label41)
        Me.Panel2.Controls.Add(Me.txtDomicilioReferencia3)
        Me.Panel2.Controls.Add(Me.Label42)
        Me.Panel2.Controls.Add(Me.txtColoniaReferencia3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(696, 310)
        Me.Panel2.TabIndex = 63
        '
        'clcExtensionReferencia3
        '
        Me.clcExtensionReferencia3.EditValue = "0"
        Me.clcExtensionReferencia3.Location = New System.Drawing.Point(348, 81)
        Me.clcExtensionReferencia3.MaxValue = 0
        Me.clcExtensionReferencia3.MinValue = 0
        Me.clcExtensionReferencia3.Name = "clcExtensionReferencia3"
        '
        'clcExtensionReferencia3.Properties
        '
        Me.clcExtensionReferencia3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExtensionReferencia3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExtensionReferencia3.Properties.MaskData.EditMask = "#"
        Me.clcExtensionReferencia3.Properties.MaxLength = 5
        Me.clcExtensionReferencia3.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcExtensionReferencia3.Size = New System.Drawing.Size(32, 19)
        Me.clcExtensionReferencia3.TabIndex = 114
        Me.clcExtensionReferencia3.Tag = "cp"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(36, 224)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(35, 16)
        Me.Label19.TabIndex = 128
        Me.Label19.Tag = ""
        Me.Label19.Text = "Otro&s:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtOtrosReferencia3
        '
        Me.txtOtrosReferencia3.EditValue = ""
        Me.txtOtrosReferencia3.Location = New System.Drawing.Point(80, 224)
        Me.txtOtrosReferencia3.Name = "txtOtrosReferencia3"
        '
        'txtOtrosReferencia3.Properties
        '
        Me.txtOtrosReferencia3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOtrosReferencia3.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtOtrosReferencia3.Size = New System.Drawing.Size(448, 48)
        Me.txtOtrosReferencia3.TabIndex = 129
        Me.txtOtrosReferencia3.Tag = "observaciones"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(315, 80)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(27, 16)
        Me.Label20.TabIndex = 113
        Me.Label20.Tag = ""
        Me.Label20.Text = "Ex&t.:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNextelReferencia3
        '
        Me.txtNextelReferencia3.EditValue = ""
        Me.txtNextelReferencia3.Location = New System.Drawing.Point(611, 80)
        Me.txtNextelReferencia3.Name = "txtNextelReferencia3"
        '
        'txtNextelReferencia3.Properties
        '
        Me.txtNextelReferencia3.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtNextelReferencia3.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtNextelReferencia3.Properties.MaxLength = 10
        Me.txtNextelReferencia3.Size = New System.Drawing.Size(72, 20)
        Me.txtNextelReferencia3.TabIndex = 117
        Me.txtNextelReferencia3.Tag = "nextel"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(547, 80)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(65, 16)
        Me.Label21.TabIndex = 116
        Me.Label21.Tag = ""
        Me.Label21.Text = "Tel. &Nextel.:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(15, 80)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 16)
        Me.Label22.TabIndex = 109
        Me.Label22.Tag = ""
        Me.Label22.Text = "Tel. &Casa:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelCasaReferencia3
        '
        Me.txtTelCasaReferencia3.EditValue = ""
        Me.txtTelCasaReferencia3.Location = New System.Drawing.Point(80, 80)
        Me.txtTelCasaReferencia3.Name = "txtTelCasaReferencia3"
        '
        'txtTelCasaReferencia3.Properties
        '
        Me.txtTelCasaReferencia3.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelCasaReferencia3.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelCasaReferencia3.Properties.MaxLength = 10
        Me.txtTelCasaReferencia3.Size = New System.Drawing.Size(71, 20)
        Me.txtTelCasaReferencia3.TabIndex = 110
        Me.txtTelCasaReferencia3.Tag = "telefono1"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(163, 80)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(65, 16)
        Me.Label23.TabIndex = 111
        Me.Label23.Tag = ""
        Me.Label23.Text = "Tel. &Oficina:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelOficinaReferencia3
        '
        Me.txtTelOficinaReferencia3.EditValue = ""
        Me.txtTelOficinaReferencia3.Location = New System.Drawing.Point(235, 80)
        Me.txtTelOficinaReferencia3.Name = "txtTelOficinaReferencia3"
        '
        'txtTelOficinaReferencia3.Properties
        '
        Me.txtTelOficinaReferencia3.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelOficinaReferencia3.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelOficinaReferencia3.Properties.MaxLength = 10
        Me.txtTelOficinaReferencia3.Size = New System.Drawing.Size(72, 20)
        Me.txtTelOficinaReferencia3.TabIndex = 112
        Me.txtTelOficinaReferencia3.Tag = "telefono2"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(403, 80)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(49, 16)
        Me.Label24.TabIndex = 115
        Me.Label24.Tag = ""
        Me.Label24.Text = "Tel. &Cel.:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCelularReferencia3
        '
        Me.txtCelularReferencia3.EditValue = ""
        Me.txtCelularReferencia3.Location = New System.Drawing.Point(459, 80)
        Me.txtCelularReferencia3.Name = "txtCelularReferencia3"
        '
        'txtCelularReferencia3.Properties
        '
        Me.txtCelularReferencia3.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtCelularReferencia3.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtCelularReferencia3.Properties.MaxLength = 13
        Me.txtCelularReferencia3.Size = New System.Drawing.Size(72, 20)
        Me.txtCelularReferencia3.TabIndex = 116
        Me.txtCelularReferencia3.Tag = "fax"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(443, 56)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(22, 16)
        Me.Label26.TabIndex = 107
        Me.Label26.Tag = ""
        Me.Label26.Text = "Cp&:"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCodigoPostalRef3
        '
        Me.clcCodigoPostalRef3.EditValue = "0"
        Me.clcCodigoPostalRef3.Location = New System.Drawing.Point(475, 56)
        Me.clcCodigoPostalRef3.MaxValue = 0
        Me.clcCodigoPostalRef3.MinValue = 0
        Me.clcCodigoPostalRef3.Name = "clcCodigoPostalRef3"
        '
        'clcCodigoPostalRef3.Properties
        '
        Me.clcCodigoPostalRef3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostalRef3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostalRef3.Properties.MaskData.EditMask = "#"
        Me.clcCodigoPostalRef3.Properties.MaxLength = 5
        Me.clcCodigoPostalRef3.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCodigoPostalRef3.Size = New System.Drawing.Size(56, 19)
        Me.clcCodigoPostalRef3.TabIndex = 108
        Me.clcCodigoPostalRef3.Tag = "cp"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(35, 200)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(36, 16)
        Me.Label32.TabIndex = 126
        Me.Label32.Tag = ""
        Me.Label32.Text = "Email:"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCorreoReferencia3
        '
        Me.txtCorreoReferencia3.EditValue = ""
        Me.txtCorreoReferencia3.Location = New System.Drawing.Point(80, 200)
        Me.txtCorreoReferencia3.Name = "txtCorreoReferencia3"
        '
        'txtCorreoReferencia3.Properties
        '
        Me.txtCorreoReferencia3.Properties.MaxLength = 100
        Me.txtCorreoReferencia3.Size = New System.Drawing.Size(416, 20)
        Me.txtCorreoReferencia3.TabIndex = 127
        Me.txtCorreoReferencia3.Tag = "email"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(10, 176)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(61, 16)
        Me.Label36.TabIndex = 124
        Me.Label36.Tag = ""
        Me.Label36.Text = "Trabaja en:"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTrabajaReferencia3
        '
        Me.txtTrabajaReferencia3.EditValue = ""
        Me.txtTrabajaReferencia3.Location = New System.Drawing.Point(80, 176)
        Me.txtTrabajaReferencia3.Name = "txtTrabajaReferencia3"
        '
        'txtTrabajaReferencia3.Properties
        '
        Me.txtTrabajaReferencia3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTrabajaReferencia3.Properties.MaxLength = 50
        Me.txtTrabajaReferencia3.Size = New System.Drawing.Size(300, 20)
        Me.txtTrabajaReferencia3.TabIndex = 125
        Me.txtTrabajaReferencia3.Tag = "trabaja_en_referencia"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(6, 152)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(65, 16)
        Me.Label37.TabIndex = 122
        Me.Label37.Tag = ""
        Me.Label37.Text = "Parentesco:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtParentescoReferencia3
        '
        Me.txtParentescoReferencia3.EditValue = ""
        Me.txtParentescoReferencia3.Location = New System.Drawing.Point(80, 152)
        Me.txtParentescoReferencia3.Name = "txtParentescoReferencia3"
        '
        'txtParentescoReferencia3.Properties
        '
        Me.txtParentescoReferencia3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtParentescoReferencia3.Properties.MaxLength = 50
        Me.txtParentescoReferencia3.Size = New System.Drawing.Size(300, 20)
        Me.txtParentescoReferencia3.TabIndex = 123
        Me.txtParentescoReferencia3.Tag = "parentesco_referencia"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(28, 128)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(43, 16)
        Me.Label38.TabIndex = 120
        Me.Label38.Tag = ""
        Me.Label38.Text = "Estado:"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstadoReferencia3
        '
        Me.txtEstadoReferencia3.EditValue = ""
        Me.txtEstadoReferencia3.Location = New System.Drawing.Point(80, 128)
        Me.txtEstadoReferencia3.Name = "txtEstadoReferencia3"
        '
        'txtEstadoReferencia3.Properties
        '
        Me.txtEstadoReferencia3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoReferencia3.Properties.MaxLength = 50
        Me.txtEstadoReferencia3.Size = New System.Drawing.Size(300, 20)
        Me.txtEstadoReferencia3.TabIndex = 121
        Me.txtEstadoReferencia3.Tag = "estado_referencia"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(28, 104)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(43, 16)
        Me.Label39.TabIndex = 118
        Me.Label39.Tag = ""
        Me.Label39.Text = "Ciudad:"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudadReferencia3
        '
        Me.txtCiudadReferencia3.EditValue = ""
        Me.txtCiudadReferencia3.Location = New System.Drawing.Point(80, 104)
        Me.txtCiudadReferencia3.Name = "txtCiudadReferencia3"
        '
        'txtCiudadReferencia3.Properties
        '
        Me.txtCiudadReferencia3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudadReferencia3.Properties.MaxLength = 50
        Me.txtCiudadReferencia3.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudadReferencia3.TabIndex = 119
        Me.txtCiudadReferencia3.Tag = "ciudad_referencia"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(23, 8)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(48, 16)
        Me.Label40.TabIndex = 101
        Me.Label40.Tag = ""
        Me.Label40.Text = "Nombre:"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombreReferencia3
        '
        Me.txtNombreReferencia3.EditValue = ""
        Me.txtNombreReferencia3.Location = New System.Drawing.Point(80, 8)
        Me.txtNombreReferencia3.Name = "txtNombreReferencia3"
        '
        'txtNombreReferencia3.Properties
        '
        Me.txtNombreReferencia3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreReferencia3.Properties.MaxLength = 100
        Me.txtNombreReferencia3.Size = New System.Drawing.Size(600, 20)
        Me.txtNombreReferencia3.TabIndex = 102
        Me.txtNombreReferencia3.Tag = "nombre_referencia"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(17, 32)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(54, 16)
        Me.Label41.TabIndex = 103
        Me.Label41.Tag = ""
        Me.Label41.Text = "Domici&lio:"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilioReferencia3
        '
        Me.txtDomicilioReferencia3.EditValue = ""
        Me.txtDomicilioReferencia3.Location = New System.Drawing.Point(80, 32)
        Me.txtDomicilioReferencia3.Name = "txtDomicilioReferencia3"
        '
        'txtDomicilioReferencia3.Properties
        '
        Me.txtDomicilioReferencia3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDomicilioReferencia3.Properties.MaxLength = 50
        Me.txtDomicilioReferencia3.Size = New System.Drawing.Size(300, 20)
        Me.txtDomicilioReferencia3.TabIndex = 104
        Me.txtDomicilioReferencia3.Tag = "domicilio_referencia"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(25, 56)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(46, 16)
        Me.Label42.TabIndex = 105
        Me.Label42.Tag = ""
        Me.Label42.Text = "Colonia:"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColoniaReferencia3
        '
        Me.txtColoniaReferencia3.EditValue = ""
        Me.txtColoniaReferencia3.Location = New System.Drawing.Point(80, 56)
        Me.txtColoniaReferencia3.Name = "txtColoniaReferencia3"
        '
        'txtColoniaReferencia3.Properties
        '
        Me.txtColoniaReferencia3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtColoniaReferencia3.Properties.MaxLength = 50
        Me.txtColoniaReferencia3.Size = New System.Drawing.Size(300, 20)
        Me.txtColoniaReferencia3.TabIndex = 106
        Me.txtColoniaReferencia3.Tag = "colonia_referencia"
        '
        'ucClientesReferencias
        '
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "ucClientesReferencias"
        Me.Size = New System.Drawing.Size(704, 336)
        Me.pnlConfidenciales.ResumeLayout(False)
        CType(Me.clcExtensionReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOtrosReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNextelReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelCasaReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelOficinaReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCelularReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCodigoPostalRef1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCorreoReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTrabajaReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtParentescoReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstadoReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudadReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilioReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColoniaReferencia1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.clcExtensionReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOtrosReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNextelReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelCasaReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelOficinaReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCelularReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCodigoPostalRef2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCorreoReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTrabajaReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtParentescoReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstadoReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudadReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilioReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColoniaReferencia2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TinCalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TinCalcEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.clcExtensionReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOtrosReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNextelReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelCasaReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelOficinaReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCelularReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCodigoPostalRef3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCorreoReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTrabajaReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtParentescoReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstadoReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudadReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilioReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColoniaReferencia3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "referencia 1"

    Public Property NombreReferencia1() As String
        Get
            Return Me.txtNombreReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtNombreReferencia1.Text = Value
        End Set
    End Property

    Public Property DomicilioReferencia1() As String
        Get
            Return Me.txtDomicilioReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtDomicilioReferencia1.Text = Value
        End Set
    End Property

    Public Property ColoniaReferencia1() As String
        Get
            Return Me.txtColoniaReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtColoniaReferencia1.Text = Value
        End Set
    End Property

    Public Property CodigoPostalRef1() As Long
        Get
            Return Me.clcCodigoPostalRef1.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcCodigoPostalRef1.Value = Value
        End Set
    End Property

    Public Property TelefonoCasaRef1() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelCasaReferencia1.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTelCasaReferencia1.Text = Value
        End Set
    End Property

    Public Property TelefonoOficinaRef1() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelOficinaReferencia1.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTelOficinaReferencia1.Text = Value
        End Set
    End Property

    Public Property ExtensionRef1() As Long
        Get
            Return Me.clcExtensionReferencia1.Text
        End Get
        Set(ByVal Value As Long)
            Me.clcExtensionReferencia1.Text = Value
        End Set
    End Property

    Public Property CelularRef1() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtCelularReferencia1.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtCelularReferencia1.Text = Value
        End Set
    End Property

    Public Property NextelReferencia1() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtNextelReferencia1.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtNextelReferencia1.Text = Value
        End Set
    End Property

    Public Property CiudadReferencia1() As String
        Get
            Return Me.txtCiudadReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCiudadReferencia1.Text = Value
        End Set
    End Property

    Public Property EstadoReferencia1() As String
        Get
            Return Me.txtEstadoReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtEstadoReferencia1.Text = Value
        End Set
    End Property

    Public Property ParentescoReferencia1() As String
        Get
            Return Me.txtParentescoReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtParentescoReferencia1.Text = Value
        End Set
    End Property

    Public Property TrabajaReferencia1() As String
        Get
            Return Me.txtTrabajaReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtTrabajaReferencia1.Text = Value
        End Set
    End Property

    Public Property CorreoReferencia1() As String
        Get
            Return Me.txtCorreoReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCorreoReferencia1.Text = Value
        End Set
    End Property



    Public Property OtrosReferencia1() As String
        Get
            Return Me.txtOtrosReferencia1.Text
        End Get
        Set(ByVal Value As String)
            Me.txtOtrosReferencia1.Text = Value
        End Set
    End Property

#End Region


#Region "Referencia 2"

    Public Property NombreReferencia2() As String
        Get
            Return Me.txtNombreReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtNombreReferencia2.Text = Value
        End Set
    End Property

    Public Property DomicilioReferencia2() As String
        Get
            Return Me.txtDomicilioReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtDomicilioReferencia2.Text = Value
        End Set
    End Property

    Public Property ColoniaReferencia2() As String
        Get
            Return Me.txtColoniaReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtColoniaReferencia2.Text = Value
        End Set
    End Property

    Public Property CodigoPostalRef2() As Long
        Get
            Return Me.clcCodigoPostalRef2.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcCodigoPostalRef2.Value = Value
        End Set
    End Property

    Public Property TelefonoCasaRef2() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelCasaReferencia2.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTelCasaReferencia2.Text = Value
        End Set
    End Property

    Public Property TelefonoOficinaRef2() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelOficinaReferencia2.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTelOficinaReferencia2.Text = Value
        End Set
    End Property

    Public Property ExtensionRef2() As Long
        Get
            Return Me.clcExtensionReferencia2.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcExtensionReferencia2.Value = Value
        End Set
    End Property

    Public Property CelularRef2() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtCelularReferencia2.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtCelularReferencia2.Text = Value
        End Set
    End Property

    Public Property NextelReferencia2() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtNextelReferencia2.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtNextelReferencia2.Text = Value
        End Set
    End Property

    Public Property CiudadReferencia2() As String
        Get
            Return Me.txtCiudadReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCiudadReferencia2.Text = Value
        End Set
    End Property

    Public Property EstadoReferencia2() As String
        Get
            Return Me.txtEstadoReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtEstadoReferencia2.Text = Value
        End Set
    End Property

    Public Property ParentescoReferencia2() As String
        Get
            Return Me.txtParentescoReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtParentescoReferencia2.Text = Value
        End Set
    End Property

    Public Property TrabajaReferencia2() As String
        Get
            Return Me.txtTrabajaReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtTrabajaReferencia2.Text = Value
        End Set
    End Property

    Public Property CorreoReferencia2() As String
        Get
            Return Me.txtCorreoReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCorreoReferencia2.Text = Value
        End Set
    End Property



    Public Property OtrosReferencia2() As String
        Get
            Return Me.txtOtrosReferencia2.Text
        End Get
        Set(ByVal Value As String)
            Me.txtOtrosReferencia2.Text = Value
        End Set
    End Property

#End Region


#Region "Referencia 3"

    Public Property NombreReferencia3() As String
        Get
            Return Me.txtNombreReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtNombreReferencia3.Text = Value
        End Set
    End Property

    Public Property DomicilioReferencia3() As String
        Get
            Return Me.txtDomicilioReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtDomicilioReferencia3.Text = Value
        End Set
    End Property

    Public Property ColoniaReferencia3() As String
        Get
            Return Me.txtColoniaReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtColoniaReferencia3.Text = Value
        End Set
    End Property

    Public Property CodigoPostalRef3() As Long
        Get
            Return Me.clcCodigoPostalRef3.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcCodigoPostalRef3.Value = Value
        End Set
    End Property

    Public Property TelefonoCasaRef3() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelCasaReferencia3.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTelCasaReferencia3.Text = Value
        End Set
    End Property

    Public Property TelefonoOficinaRef3() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelOficinaReferencia3.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTelOficinaReferencia3.Text = Value
        End Set
    End Property

    Public Property ExtensionRef3() As Long
        Get
            Return Me.clcExtensionReferencia3.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcExtensionReferencia3.Value = Value
        End Set
    End Property

    Public Property CelularRef3() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtCelularReferencia3.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtCelularReferencia3.Text = Value
        End Set
    End Property

    Public Property NextelReferencia3() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtNextelReferencia3.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtNextelReferencia3.Text = Value
        End Set
    End Property

    Public Property CiudadReferencia3() As String
        Get
            Return Me.txtCiudadReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCiudadReferencia3.Text = Value
        End Set
    End Property

    Public Property EstadoReferencia3() As String
        Get
            Return Me.txtEstadoReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtEstadoReferencia3.Text = Value
        End Set
    End Property

    Public Property ParentescoReferencia3() As String
        Get
            Return Me.txtParentescoReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtParentescoReferencia3.Text = Value
        End Set
    End Property

    Public Property TrabajaReferencia3() As String
        Get
            Return Me.txtTrabajaReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtTrabajaReferencia3.Text = Value
        End Set
    End Property

    Public Property CorreoReferencia3() As String
        Get
            Return Me.txtCorreoReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCorreoReferencia3.Text = Value
        End Set
    End Property



    Public Property OtrosReferencia3() As String
        Get
            Return Me.txtOtrosReferencia3.Text
        End Get
        Set(ByVal Value As String)
            Me.txtOtrosReferencia3.Text = Value
        End Set
    End Property

#End Region


End Class
