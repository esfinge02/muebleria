Public Class TINImagen
    Inherits System.Windows.Forms.UserControl

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Imagen As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Imagen = New DevExpress.XtraEditors.PictureEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        CType(Me.Imagen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Imagen
        '
        Me.Imagen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Imagen.Location = New System.Drawing.Point(3, 16)
        Me.Imagen.Name = "Imagen"
        Me.Imagen.Size = New System.Drawing.Size(434, 357)
        Me.Imagen.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Imagen)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(440, 376)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Imagen"
        '
        'TINImagen
        '
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "TINImagen"
        Me.Size = New System.Drawing.Size(440, 376)
        CType(Me.Imagen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _Image As System.Drawing.Image

    Public Property Foto() As System.Drawing.Image
        Get
            Return Imagen.Image

        End Get
        Set(ByVal Value As System.Drawing.Image)
            _Image = Value
            Imagen.Image = _Image
        End Set
    End Property

    Public WriteOnly Property ModificarImagen() As Boolean
        Set(ByVal Value As Boolean)
            Me.Imagen.Properties.ShowMenu = Value
        End Set
    End Property

    Private Sub TINImagen_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load


    End Sub
End Class
