Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class ucClientesLaborales
    Inherits System.Windows.Forms.UserControl


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents pnlReferencias As System.Windows.Forms.Panel
    Friend WithEvents txtTelefonos_ocupacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAntiguedad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPuesto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPuesto As System.Windows.Forms.Label
    Friend WithEvents lblOcupacion As System.Windows.Forms.Label
    Friend WithEvents txtOcupacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblIngresos As System.Windows.Forms.Label
    Friend WithEvents clcIngresos As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents clcLimiteCredito As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblArea As System.Windows.Forms.Label
    Friend WithEvents txtArea As DevExpress.XtraEditors.TextEdit
    Private WithEvents Label14 As System.Windows.Forms.Label
    Private WithEvents Label13 As System.Windows.Forms.Label
    Private WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpLocalidad As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtCalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents clcCodigoPostal As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.pnlReferencias = New System.Windows.Forms.Panel
        Me.txtArea = New DevExpress.XtraEditors.TextEdit
        Me.lblArea = New System.Windows.Forms.Label
        Me.txtTelefonos_ocupacion = New DevExpress.XtraEditors.TextEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtAntiguedad = New DevExpress.XtraEditors.TextEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtDepto = New DevExpress.XtraEditors.TextEdit
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtPuesto = New DevExpress.XtraEditors.TextEdit
        Me.lblPuesto = New System.Windows.Forms.Label
        Me.lblOcupacion = New System.Windows.Forms.Label
        Me.txtOcupacion = New DevExpress.XtraEditors.TextEdit
        Me.lblIngresos = New System.Windows.Forms.Label
        Me.clcIngresos = New Dipros.Editors.TINCalcEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.clcLimiteCredito = New Dipros.Editors.TINCalcEdit
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.lkpLocalidad = New Dipros.Editors.TINMultiLookup
        Me.Label18 = New System.Windows.Forms.Label
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtCalle = New DevExpress.XtraEditors.TextEdit
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.clcCodigoPostal = New Dipros.Editors.TINCalcEdit
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.pnlReferencias.SuspendLayout()
        CType(Me.txtArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefonos_ocupacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAntiguedad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOcupacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIngresos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcLimiteCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCodigoPostal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlReferencias
        '
        Me.pnlReferencias.Controls.Add(Me.lkpEstado)
        Me.pnlReferencias.Controls.Add(Me.lkpLocalidad)
        Me.pnlReferencias.Controls.Add(Me.Label18)
        Me.pnlReferencias.Controls.Add(Me.lkpMunicipio)
        Me.pnlReferencias.Controls.Add(Me.Label22)
        Me.pnlReferencias.Controls.Add(Me.txtCalle)
        Me.pnlReferencias.Controls.Add(Me.Label23)
        Me.pnlReferencias.Controls.Add(Me.Label24)
        Me.pnlReferencias.Controls.Add(Me.clcCodigoPostal)
        Me.pnlReferencias.Controls.Add(Me.Label25)
        Me.pnlReferencias.Controls.Add(Me.Label27)
        Me.pnlReferencias.Controls.Add(Me.lkpColonia)
        Me.pnlReferencias.Controls.Add(Me.txtArea)
        Me.pnlReferencias.Controls.Add(Me.lblArea)
        Me.pnlReferencias.Controls.Add(Me.txtTelefonos_ocupacion)
        Me.pnlReferencias.Controls.Add(Me.Label14)
        Me.pnlReferencias.Controls.Add(Me.txtAntiguedad)
        Me.pnlReferencias.Controls.Add(Me.Label13)
        Me.pnlReferencias.Controls.Add(Me.txtDepto)
        Me.pnlReferencias.Controls.Add(Me.Label12)
        Me.pnlReferencias.Controls.Add(Me.txtPuesto)
        Me.pnlReferencias.Controls.Add(Me.lblPuesto)
        Me.pnlReferencias.Controls.Add(Me.lblOcupacion)
        Me.pnlReferencias.Controls.Add(Me.txtOcupacion)
        Me.pnlReferencias.Controls.Add(Me.lblIngresos)
        Me.pnlReferencias.Controls.Add(Me.clcIngresos)
        Me.pnlReferencias.Controls.Add(Me.Label9)
        Me.pnlReferencias.Controls.Add(Me.clcLimiteCredito)
        Me.pnlReferencias.Location = New System.Drawing.Point(0, 0)
        Me.pnlReferencias.Name = "pnlReferencias"
        Me.pnlReferencias.Size = New System.Drawing.Size(704, 336)
        Me.pnlReferencias.TabIndex = 0
        '
        'txtArea
        '
        Me.txtArea.EditValue = ""
        Me.txtArea.Location = New System.Drawing.Point(88, 104)
        Me.txtArea.Name = "txtArea"
        '
        'txtArea.Properties
        '
        Me.txtArea.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtArea.Size = New System.Drawing.Size(306, 20)
        Me.txtArea.TabIndex = 7
        Me.txtArea.Tag = "departamento"
        '
        'lblArea
        '
        Me.lblArea.AutoSize = True
        Me.lblArea.Location = New System.Drawing.Point(48, 112)
        Me.lblArea.Name = "lblArea"
        Me.lblArea.Size = New System.Drawing.Size(31, 16)
        Me.lblArea.TabIndex = 6
        Me.lblArea.Text = "Area:"
        '
        'txtTelefonos_ocupacion
        '
        Me.txtTelefonos_ocupacion.EditValue = ""
        Me.txtTelefonos_ocupacion.Location = New System.Drawing.Point(88, 152)
        Me.txtTelefonos_ocupacion.Name = "txtTelefonos_ocupacion"
        '
        'txtTelefonos_ocupacion.Properties
        '
        Me.txtTelefonos_ocupacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonos_ocupacion.Size = New System.Drawing.Size(144, 20)
        Me.txtTelefonos_ocupacion.TabIndex = 11
        Me.txtTelefonos_ocupacion.Tag = "telefonos_laboral"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(24, 152)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(57, 16)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "Telefonos:"
        '
        'txtAntiguedad
        '
        Me.txtAntiguedad.EditValue = ""
        Me.txtAntiguedad.Location = New System.Drawing.Point(88, 176)
        Me.txtAntiguedad.Name = "txtAntiguedad"
        '
        'txtAntiguedad.Properties
        '
        Me.txtAntiguedad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAntiguedad.Size = New System.Drawing.Size(80, 20)
        Me.txtAntiguedad.TabIndex = 13
        Me.txtAntiguedad.Tag = "antiguedad_laboral"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(16, 176)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(65, 16)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Antig�edad:"
        '
        'txtDepto
        '
        Me.txtDepto.EditValue = ""
        Me.txtDepto.Location = New System.Drawing.Point(88, 128)
        Me.txtDepto.Name = "txtDepto"
        '
        'txtDepto.Properties
        '
        Me.txtDepto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepto.Size = New System.Drawing.Size(306, 20)
        Me.txtDepto.TabIndex = 9
        Me.txtDepto.Tag = "departamento"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(48, 128)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 16)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Depto:"
        '
        'txtPuesto
        '
        Me.txtPuesto.EditValue = ""
        Me.txtPuesto.Location = New System.Drawing.Point(88, 80)
        Me.txtPuesto.Name = "txtPuesto"
        '
        'txtPuesto.Properties
        '
        Me.txtPuesto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPuesto.Size = New System.Drawing.Size(306, 20)
        Me.txtPuesto.TabIndex = 5
        Me.txtPuesto.Tag = "puesto"
        '
        'lblPuesto
        '
        Me.lblPuesto.AutoSize = True
        Me.lblPuesto.Location = New System.Drawing.Point(40, 88)
        Me.lblPuesto.Name = "lblPuesto"
        Me.lblPuesto.Size = New System.Drawing.Size(43, 16)
        Me.lblPuesto.TabIndex = 4
        Me.lblPuesto.Text = "Puesto:"
        '
        'lblOcupacion
        '
        Me.lblOcupacion.AutoSize = True
        Me.lblOcupacion.Location = New System.Drawing.Point(19, 8)
        Me.lblOcupacion.Name = "lblOcupacion"
        Me.lblOcupacion.Size = New System.Drawing.Size(61, 16)
        Me.lblOcupacion.TabIndex = 0
        Me.lblOcupacion.Tag = ""
        Me.lblOcupacion.Text = "Trabaja en:"
        Me.lblOcupacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtOcupacion
        '
        Me.txtOcupacion.EditValue = ""
        Me.txtOcupacion.Location = New System.Drawing.Point(89, 8)
        Me.txtOcupacion.Name = "txtOcupacion"
        '
        'txtOcupacion.Properties
        '
        Me.txtOcupacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOcupacion.Properties.MaxLength = 50
        Me.txtOcupacion.Size = New System.Drawing.Size(599, 20)
        Me.txtOcupacion.TabIndex = 1
        Me.txtOcupacion.Tag = "ocupacion"
        '
        'lblIngresos
        '
        Me.lblIngresos.AutoSize = True
        Me.lblIngresos.Location = New System.Drawing.Point(488, 112)
        Me.lblIngresos.Name = "lblIngresos"
        Me.lblIngresos.Size = New System.Drawing.Size(97, 16)
        Me.lblIngresos.TabIndex = 16
        Me.lblIngresos.Tag = ""
        Me.lblIngresos.Text = "In&gresos Mensual:"
        Me.lblIngresos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIngresos
        '
        Me.clcIngresos.EditValue = "0"
        Me.clcIngresos.Location = New System.Drawing.Point(600, 112)
        Me.clcIngresos.MaxValue = 0
        Me.clcIngresos.MinValue = 0
        Me.clcIngresos.Name = "clcIngresos"
        '
        'clcIngresos.Properties
        '
        Me.clcIngresos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIngresos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIngresos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcIngresos.Properties.Precision = 2
        Me.clcIngresos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIngresos.Size = New System.Drawing.Size(80, 19)
        Me.clcIngresos.TabIndex = 17
        Me.clcIngresos.Tag = "ingresos"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(496, 88)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(93, 16)
        Me.Label9.TabIndex = 14
        Me.Label9.Tag = ""
        Me.Label9.Text = "L�mite de Cr�dito:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcLimiteCredito
        '
        Me.clcLimiteCredito.EditValue = "0"
        Me.clcLimiteCredito.Location = New System.Drawing.Point(600, 88)
        Me.clcLimiteCredito.MaxValue = 0
        Me.clcLimiteCredito.MinValue = 0
        Me.clcLimiteCredito.Name = "clcLimiteCredito"
        '
        'clcLimiteCredito.Properties
        '
        Me.clcLimiteCredito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLimiteCredito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLimiteCredito.Properties.Enabled = False
        Me.clcLimiteCredito.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcLimiteCredito.Properties.Precision = 2
        Me.clcLimiteCredito.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcLimiteCredito.Size = New System.Drawing.Size(80, 19)
        Me.clcLimiteCredito.TabIndex = 15
        Me.clcLimiteCredito.Tag = "limite_credito"
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(88, 32)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.ReadOnlyControl = False
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(144, 20)
        Me.lkpEstado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEstado.TabIndex = 31
        Me.lkpEstado.Tag = "estado_referencia"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'lkpLocalidad
        '
        Me.lkpLocalidad.AllowAdd = False
        Me.lkpLocalidad.AutoReaload = True
        Me.lkpLocalidad.DataSource = Nothing
        Me.lkpLocalidad.DefaultSearchField = ""
        Me.lkpLocalidad.DisplayMember = "descripcion"
        Me.lkpLocalidad.EditValue = Nothing
        Me.lkpLocalidad.Filtered = False
        Me.lkpLocalidad.InitValue = Nothing
        Me.lkpLocalidad.Location = New System.Drawing.Point(544, 32)
        Me.lkpLocalidad.MultiSelect = False
        Me.lkpLocalidad.Name = "lkpLocalidad"
        Me.lkpLocalidad.NullText = ""
        Me.lkpLocalidad.PopupWidth = CType(300, Long)
        Me.lkpLocalidad.ReadOnlyControl = False
        Me.lkpLocalidad.Required = False
        Me.lkpLocalidad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpLocalidad.SearchMember = ""
        Me.lkpLocalidad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpLocalidad.SelectAll = False
        Me.lkpLocalidad.Size = New System.Drawing.Size(144, 20)
        Me.lkpLocalidad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpLocalidad.TabIndex = 35
        Me.lkpLocalidad.Tag = "ciudad_referencia"
        Me.lkpLocalidad.ToolTip = Nothing
        Me.lkpLocalidad.ValueMember = "ciudad"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(248, 40)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(55, 16)
        Me.Label18.TabIndex = 32
        Me.Label18.Tag = ""
        Me.Label18.Text = "Municipio:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = True
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(312, 32)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(300, Long)
        Me.lkpMunicipio.ReadOnlyControl = False
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(144, 20)
        Me.lkpMunicipio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMunicipio.TabIndex = 33
        Me.lkpMunicipio.Tag = "municipio_referencia"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(248, 64)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(33, 16)
        Me.Label22.TabIndex = 38
        Me.Label22.Tag = ""
        Me.Label22.Text = "Calle:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCalle
        '
        Me.txtCalle.EditValue = ""
        Me.txtCalle.Location = New System.Drawing.Point(288, 56)
        Me.txtCalle.Name = "txtCalle"
        '
        'txtCalle.Properties
        '
        Me.txtCalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCalle.Properties.MaxLength = 50
        Me.txtCalle.Size = New System.Drawing.Size(272, 20)
        Me.txtCalle.TabIndex = 39
        Me.txtCalle.Tag = "domicilio_referencia"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(32, 64)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(46, 16)
        Me.Label23.TabIndex = 36
        Me.Label23.Tag = ""
        Me.Label23.Text = "C&olonia:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(600, 64)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(22, 16)
        Me.Label24.TabIndex = 40
        Me.Label24.Tag = ""
        Me.Label24.Text = "Cp&:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCodigoPostal
        '
        Me.clcCodigoPostal.EditValue = "0"
        Me.clcCodigoPostal.Location = New System.Drawing.Point(632, 56)
        Me.clcCodigoPostal.MaxValue = 0
        Me.clcCodigoPostal.MinValue = 0
        Me.clcCodigoPostal.Name = "clcCodigoPostal"
        '
        'clcCodigoPostal.Properties
        '
        Me.clcCodigoPostal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostal.Properties.MaskData.EditMask = "#"
        Me.clcCodigoPostal.Properties.MaxLength = 5
        Me.clcCodigoPostal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCodigoPostal.Size = New System.Drawing.Size(56, 19)
        Me.clcCodigoPostal.TabIndex = 41
        Me.clcCodigoPostal.Tag = "cp_referencia"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(480, 40)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(56, 16)
        Me.Label25.TabIndex = 34
        Me.Label25.Tag = ""
        Me.Label25.Text = "Locali&dad:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(40, 40)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(43, 16)
        Me.Label27.TabIndex = 30
        Me.Label27.Tag = ""
        Me.Label27.Text = "E&stado:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = True
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(88, 56)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(300, Long)
        Me.lkpColonia.ReadOnlyControl = False
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(144, 20)
        Me.lkpColonia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpColonia.TabIndex = 37
        Me.lkpColonia.Tag = "colonia_referencia"
        Me.lkpColonia.ToolTip = Nothing
        Me.lkpColonia.ValueMember = "colonia"
        '
        'ucClientesLaborales
        '
        Me.Controls.Add(Me.pnlReferencias)
        Me.Name = "ucClientesLaborales"
        Me.Size = New System.Drawing.Size(704, 336)
        Me.pnlReferencias.ResumeLayout(False)
        CType(Me.txtArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefonos_ocupacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAntiguedad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOcupacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIngresos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcLimiteCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCodigoPostal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"

    Private oVariables As New VillarrealBusiness.clsVariables
    Private oCiudades As VillarrealBusiness.clsCiudades
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oColonias As New VillarrealBusiness.clsColonias
    Private oEstados As VillarrealBusiness.clsEstados

    Private tipo_captura As String

#End Region

#Region "Propiedades"

    Public Property TrabajaEn() As String
        Get
            Return txtOcupacion.Text
        End Get
        Set(ByVal Value As String)
            txtOcupacion.Text = Value
        End Set
    End Property

    Public Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
        Set(ByVal Value As Long)
            Me.lkpEstado.EditValue = Value
        End Set
    End Property

    Public Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
        Set(ByVal Value As Long)
            Me.lkpMunicipio.EditValue = Value
        End Set
    End Property

    Public Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpLocalidad)
        End Get
        Set(ByVal Value As Long)
            Me.lkpLocalidad.EditValue = Value
        End Set
    End Property

    Public Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
        Set(ByVal Value As Long)
            Me.lkpColonia.EditValue = Value
        End Set
    End Property

    Public Property Calle() As String
        Get
            Return Me.txtCalle.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCalle.Text = Value
        End Set
    End Property

    Public Property CodigoPostal() As Long
        Get
            Return Me.clcCodigoPostal.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcCodigoPostal.Value = Value
        End Set
    End Property

    Public Property Puesto() As String
        Get
            Return txtPuesto.Text
        End Get
        Set(ByVal Value As String)
            txtPuesto.Text = Value
        End Set
    End Property

    Public Property Area() As String
        Get
            Return txtArea.Text
        End Get
        Set(ByVal Value As String)
            txtArea.Text = Value
        End Set


    End Property

    Public Property Departamento() As String
        Get
            Return txtDepto.Text
        End Get
        Set(ByVal Value As String)
            txtDepto.Text = Value
        End Set
    End Property

    Public Property Telefonos() As String
        Get
            Return txtTelefonos_ocupacion.Text
        End Get
        Set(ByVal Value As String)
            txtTelefonos_ocupacion.Text = Value
        End Set
    End Property

    Public Property Antiguedad() As String
        Get
            Return txtAntiguedad.Text
        End Get
        Set(ByVal Value As String)
            txtAntiguedad.Text = Value
        End Set
    End Property

    Public Property LimiteCredito() As Double
        Get
            Return Me.clcLimiteCredito.EditValue
        End Get
        Set(ByVal Value As Double)
            Me.clcLimiteCredito.EditValue = Value
        End Set
    End Property

    Public Property Ingresos() As Double
        Get
            Return IIf(IsNumeric(Me.clcIngresos.EditValue), Me.clcIngresos.EditValue, 0)
        End Get
        Set(ByVal Value As Double)
            Me.clcIngresos.EditValue = Value
        End Set
    End Property

#End Region

#Region "Funcionalidad"

    Public WriteOnly Property TipoCaptura() As String
        Set(ByVal Value As String)
            tipo_captura = Value
        End Set
    End Property

    Private ReadOnly Property FactorLimiteCredito() As Long
        Get
            Return CLng(oVariables.TraeDatos("factor_limite_credito", VillarrealBusiness.clsVariables.tipo_dato.Entero))
        End Get
    End Property

    Private Sub clcIngresos_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcIngresos.EditValueChanged
        If Me.clcIngresos.IsLoading Then Exit Sub
        If Me.clcIngresos.EditValue Is Nothing Or Not IsNumeric(Me.clcIngresos.EditValue) Then Exit Sub

        If tipo_captura = "C" Then
            CalculaLimiteCredito()
        End If
    End Sub

    Private Sub CalculaLimiteCredito()
        Me.clcLimiteCredito.EditValue = Me.clcIngresos.EditValue * FactorLimiteCredito
    End Sub

#End Region

#Region "Eventos de Controles"

    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData

        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpMunicipio_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        Me.lkpLocalidad.EditValue = Nothing
        Me.lkpLocalidad_LoadData(True)
    End Sub

    Private Sub lkpLocalidad_Format() Handles lkpLocalidad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpLocalidad)
    End Sub
    Private Sub lkpLocalidad_LoadData(ByVal Initialize As Boolean) Handles lkpLocalidad.LoadData

        Dim Response As New Events
        Response = oCiudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpLocalidad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpLocalidad_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpLocalidad.EditValueChanged
        If Me.lkpLocalidad.EditValue Is Nothing Then Exit Sub

        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia_LoadData(True)
    End Sub

    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub
    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpColonia_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpColonia.EditValueChanged
        clcCodigoPostal.EditValue = lkpColonia.GetValue("codigo_postal")
    End Sub

#End Region

    Private Sub ucClientesLaborales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oEstados = New VillarrealBusiness.clsEstados
        oCiudades = New VillarrealBusiness.clsCiudades
        oMunicipios = New VillarrealBusiness.clsMunicipios
        oColonias = New VillarrealBusiness.clsColonias
    End Sub
End Class
