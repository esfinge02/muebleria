Imports DevExpress.XtraBars
Imports System.Drawing
Imports System.Drawing.Font
Imports System.Drawing.Text
Imports System.Windows.Forms
Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class TINRichTextBox
    Inherits System.Windows.Forms.UserControl

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        mChange = False
    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btnNegrita As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnCursiva As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSubrayado As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnAbrir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnIzquierda As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnGuardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDerecha As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnVineta As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarManager As DevExpress.XtraBars.BarManager
    Friend WithEvents btnFonts As DevExpress.XtraBars.BarEditItem
    Friend WithEvents cboFonts As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents cboSize As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents RepositoryItemColorEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemColorEdit
    Friend WithEvents btnColor As DevExpress.XtraBars.BarEditItem
    Friend WithEvents btnAColor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnCentro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PopupControlContainer1 As DevExpress.XtraBars.PopupControlContainer
    Friend WithEvents rtbEdit As System.Windows.Forms.RichTextBox
    Friend WithEvents btnSize As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents btnCopiar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnCortar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPegar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDeshacer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ilsToolbar As System.Windows.Forms.ImageList
    Friend WithEvents brToolbar As DevExpress.XtraBars.Bar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(TINRichTextBox))
        Me.BarManager = New DevExpress.XtraBars.BarManager
        Me.brToolbar = New DevExpress.XtraBars.Bar
        Me.btnAbrir = New DevExpress.XtraBars.BarButtonItem
        Me.btnGuardar = New DevExpress.XtraBars.BarButtonItem
        Me.btnFonts = New DevExpress.XtraBars.BarEditItem
        Me.cboFonts = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.btnSize = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.btnCopiar = New DevExpress.XtraBars.BarButtonItem
        Me.btnCortar = New DevExpress.XtraBars.BarButtonItem
        Me.btnPegar = New DevExpress.XtraBars.BarButtonItem
        Me.btnDeshacer = New DevExpress.XtraBars.BarButtonItem
        Me.btnNegrita = New DevExpress.XtraBars.BarButtonItem
        Me.btnCursiva = New DevExpress.XtraBars.BarButtonItem
        Me.btnSubrayado = New DevExpress.XtraBars.BarButtonItem
        Me.btnAColor = New DevExpress.XtraBars.BarButtonItem
        Me.PopupControlContainer1 = New DevExpress.XtraBars.PopupControlContainer
        Me.btnIzquierda = New DevExpress.XtraBars.BarButtonItem
        Me.btnCentro = New DevExpress.XtraBars.BarButtonItem
        Me.btnDerecha = New DevExpress.XtraBars.BarButtonItem
        Me.btnVineta = New DevExpress.XtraBars.BarButtonItem
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.ilsToolbar = New System.Windows.Forms.ImageList(Me.components)
        Me.btnColor = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemColorEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemColorEdit
        Me.cboSize = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.RepositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
        Me.rtbEdit = New System.Windows.Forms.RichTextBox
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboFonts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemColorEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboSize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.brToolbar})
        Me.BarManager.DockControls.Add(Me.barDockControlTop)
        Me.BarManager.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager.DockControls.Add(Me.barDockControlRight)
        Me.BarManager.Form = Me
        Me.BarManager.Images = Me.ilsToolbar
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnFonts, Me.btnNegrita, Me.btnCursiva, Me.btnSubrayado, Me.btnAbrir, Me.btnIzquierda, Me.btnGuardar, Me.btnCentro, Me.btnDerecha, Me.btnVineta, Me.btnColor, Me.btnAColor, Me.btnSize, Me.btnCopiar, Me.btnCortar, Me.btnPegar, Me.btnDeshacer})
        Me.BarManager.MaxItemId = 22
        Me.BarManager.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.cboFonts, Me.cboSize, Me.RepositoryItemSpinEdit1, Me.RepositoryItemColorEdit1, Me.RepositoryItemComboBox1})
        '
        'brToolbar
        '
        Me.brToolbar.BarName = "Custom 1"
        Me.brToolbar.DockCol = 0
        Me.brToolbar.DockRow = 0
        Me.brToolbar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.brToolbar.FloatLocation = New System.Drawing.Point(292, 212)
        Me.brToolbar.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnAbrir), New DevExpress.XtraBars.LinkPersistInfo(Me.btnGuardar), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, Me.btnFonts, "", True, True, True, 132), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, Me.btnSize, "", False, True, True, 49), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCopiar, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCortar), New DevExpress.XtraBars.LinkPersistInfo(Me.btnPegar), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDeshacer, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnNegrita, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCursiva), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSubrayado), New DevExpress.XtraBars.LinkPersistInfo(Me.btnAColor, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnIzquierda, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCentro), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDerecha), New DevExpress.XtraBars.LinkPersistInfo(Me.btnVineta, True)})
        Me.brToolbar.OptionsBar.AllowQuickCustomization = False
        Me.brToolbar.OptionsBar.DrawDragBorder = False
        Me.brToolbar.Text = "Custom 1"
        '
        'btnAbrir
        '
        Me.btnAbrir.Caption = "Abrir"
        Me.btnAbrir.Id = 6
        Me.btnAbrir.ImageIndex = 0
        Me.btnAbrir.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O))
        Me.btnAbrir.Name = "btnAbrir"
        '
        'btnGuardar
        '
        Me.btnGuardar.Caption = "Guardar"
        Me.btnGuardar.Id = 8
        Me.btnGuardar.ImageIndex = 1
        Me.btnGuardar.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S))
        Me.btnGuardar.Name = "btnGuardar"
        '
        'btnFonts
        '
        Me.btnFonts.Caption = "BarEditItem1"
        Me.btnFonts.Edit = Me.cboFonts
        Me.btnFonts.EditValue = ""
        Me.btnFonts.Id = 0
        Me.btnFonts.Name = "btnFonts"
        '
        'cboFonts
        '
        Me.cboFonts.AutoHeight = False
        Me.cboFonts.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboFonts.Name = "cboFonts"
        '
        'btnSize
        '
        Me.btnSize.Caption = "Size"
        Me.btnSize.Edit = Me.RepositoryItemComboBox1
        Me.btnSize.EditValue = New Decimal(New Integer() {12, 0, 0, 0})
        Me.btnSize.Id = 17
        Me.btnSize.Name = "btnSize"
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.DropDownRows = 11
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "36", "48", "72"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'btnCopiar
        '
        Me.btnCopiar.Caption = "Copiar"
        Me.btnCopiar.Id = 18
        Me.btnCopiar.ImageIndex = 2
        Me.btnCopiar.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C))
        Me.btnCopiar.Name = "btnCopiar"
        '
        'btnCortar
        '
        Me.btnCortar.Caption = "Cortar"
        Me.btnCortar.Id = 19
        Me.btnCortar.ImageIndex = 3
        Me.btnCortar.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X))
        Me.btnCortar.Name = "btnCortar"
        '
        'btnPegar
        '
        Me.btnPegar.Caption = "Pegar"
        Me.btnPegar.Id = 20
        Me.btnPegar.ImageIndex = 4
        Me.btnPegar.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V))
        Me.btnPegar.Name = "btnPegar"
        '
        'btnDeshacer
        '
        Me.btnDeshacer.Caption = "Deshacer"
        Me.btnDeshacer.Id = 21
        Me.btnDeshacer.ImageIndex = 5
        Me.btnDeshacer.Name = "btnDeshacer"
        '
        'btnNegrita
        '
        Me.btnNegrita.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.btnNegrita.Caption = "Negrita"
        Me.btnNegrita.Hint = "Negrita"
        Me.btnNegrita.Id = 3
        Me.btnNegrita.ImageIndex = 6
        Me.btnNegrita.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N))
        Me.btnNegrita.Name = "btnNegrita"
        '
        'btnCursiva
        '
        Me.btnCursiva.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.btnCursiva.Caption = "Cursiva"
        Me.btnCursiva.Hint = "Cursiva"
        Me.btnCursiva.Id = 4
        Me.btnCursiva.ImageIndex = 7
        Me.btnCursiva.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.K))
        Me.btnCursiva.Name = "btnCursiva"
        '
        'btnSubrayado
        '
        Me.btnSubrayado.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.btnSubrayado.Caption = "Subrayado"
        Me.btnSubrayado.Hint = "Subrayado"
        Me.btnSubrayado.Id = 5
        Me.btnSubrayado.ImageIndex = 8
        Me.btnSubrayado.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S))
        Me.btnSubrayado.Name = "btnSubrayado"
        '
        'btnAColor
        '
        Me.btnAColor.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.btnAColor.Caption = "Color"
        Me.btnAColor.DropDownControl = Me.PopupControlContainer1
        Me.btnAColor.Id = 16
        Me.btnAColor.ImageIndex = 9
        Me.btnAColor.Name = "btnAColor"
        '
        'PopupControlContainer1
        '
        Me.PopupControlContainer1.Location = New System.Drawing.Point(24, 296)
        Me.PopupControlContainer1.Manager = Me.BarManager
        Me.PopupControlContainer1.Name = "PopupControlContainer1"
        Me.PopupControlContainer1.Size = New System.Drawing.Size(24, 16)
        Me.PopupControlContainer1.TabIndex = 4
        Me.PopupControlContainer1.Visible = False
        '
        'btnIzquierda
        '
        Me.btnIzquierda.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.btnIzquierda.Caption = "Alienar a la Izquierda"
        Me.btnIzquierda.Description = "Alienar a la Izquierda"
        Me.btnIzquierda.Down = True
        Me.btnIzquierda.GroupIndex = 1
        Me.btnIzquierda.Hint = "Alienar a la Izquierda"
        Me.btnIzquierda.Id = 7
        Me.btnIzquierda.ImageIndex = 10
        Me.btnIzquierda.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I))
        Me.btnIzquierda.Name = "btnIzquierda"
        '
        'btnCentro
        '
        Me.btnCentro.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.btnCentro.Caption = "Centrar"
        Me.btnCentro.Description = "Centrar"
        Me.btnCentro.GroupIndex = 1
        Me.btnCentro.Hint = "Centrar"
        Me.btnCentro.Id = 9
        Me.btnCentro.ImageIndex = 11
        Me.btnCentro.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.T))
        Me.btnCentro.Name = "btnCentro"
        '
        'btnDerecha
        '
        Me.btnDerecha.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.btnDerecha.Caption = "Alinear a la Derecha"
        Me.btnDerecha.Description = "Alinear a la Derecha"
        Me.btnDerecha.GroupIndex = 1
        Me.btnDerecha.Hint = "Alinear a la Derecha"
        Me.btnDerecha.Id = 10
        Me.btnDerecha.ImageIndex = 12
        Me.btnDerecha.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D))
        Me.btnDerecha.Name = "btnDerecha"
        '
        'btnVineta
        '
        Me.btnVineta.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.btnVineta.Caption = "Vi�eta"
        Me.btnVineta.Description = "Vi�eta"
        Me.btnVineta.Hint = "Vi�eta"
        Me.btnVineta.Id = 11
        Me.btnVineta.ImageIndex = 13
        Me.btnVineta.Name = "btnVineta"
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilsToolbar.TransparentColor = System.Drawing.Color.Transparent
        '
        'btnColor
        '
        Me.btnColor.Caption = "BarEditItem2"
        Me.btnColor.Edit = Me.RepositoryItemColorEdit1
        Me.btnColor.Id = 15
        Me.btnColor.Name = "btnColor"
        '
        'RepositoryItemColorEdit1
        '
        Me.RepositoryItemColorEdit1.AutoHeight = False
        Me.RepositoryItemColorEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemColorEdit1.Name = "RepositoryItemColorEdit1"
        '
        'cboSize
        '
        Me.cboSize.AutoHeight = False
        Me.cboSize.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboSize.DropDownRows = 11
        Me.cboSize.Items.AddRange(New Object() {"8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "36", "48", "72"})
        Me.cboSize.Name = "cboSize"
        '
        'RepositoryItemSpinEdit1
        '
        Me.RepositoryItemSpinEdit1.AutoHeight = False
        Me.RepositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.RepositoryItemSpinEdit1.MaxLength = 2
        Me.RepositoryItemSpinEdit1.MaxValue = New Decimal(New Integer() {72, 0, 0, 0})
        Me.RepositoryItemSpinEdit1.MinValue = New Decimal(New Integer() {8, 0, 0, 0})
        Me.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1"
        Me.RepositoryItemSpinEdit1.UseCtrlIncrement = True
        '
        'rtbEdit
        '
        Me.rtbEdit.AcceptsTab = True
        Me.rtbEdit.AutoWordSelection = True
        Me.rtbEdit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbEdit.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtbEdit.Location = New System.Drawing.Point(0, 26)
        Me.rtbEdit.Name = "rtbEdit"
        Me.rtbEdit.Size = New System.Drawing.Size(592, 398)
        Me.rtbEdit.TabIndex = 8
        Me.rtbEdit.Text = ""
        '
        'TINRichTextBox
        '
        Me.Controls.Add(Me.rtbEdit)
        Me.Controls.Add(Me.PopupControlContainer1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "TINRichTextBox"
        Me.Size = New System.Drawing.Size(592, 424)
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboFonts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemColorEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboSize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Dim cp As ColorPopup
    Private mChange As Boolean

#End Region

#Region "DIPROS Systems, Propiedades"

    Public Property VisibleToolbar() As Boolean
        Get
            Return Me.brToolbar.Visible
        End Get
        Set(ByVal Value As Boolean)
            Me.brToolbar.Visible = Value
        End Set
    End Property



    Protected ReadOnly Property SelectFont() As Font
        Get
            If Not rtbEdit.SelectionFont Is Nothing Then Return rtbEdit.SelectionFont
            Return rtbEdit.Font
        End Get
    End Property

    Public Property TextRtf() As String
        Get
            Return Me.rtbEdit.Rtf
        End Get
        Set(ByVal Value As String)
            Me.rtbEdit.Rtf = Value
        End Set
    End Property

    Public Property TextSimple() As String
        Get
            Return Me.rtbEdit.Text
        End Get
        Set(ByVal Value As String)
            Me.rtbEdit.Text = Value
            'Me.rtbEdit.Rtf = Value
        End Set
    End Property

    Public ReadOnly Property Change() As Boolean
        Get
            Return mChange
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Inicializacion"

    Protected Sub InitFormat()
        btnNegrita.Down = SelectFont.Bold
        btnCursiva.Down = SelectFont.Italic
        btnSubrayado.Down = SelectFont.Underline
        btnVineta.Down = rtbEdit.SelectionBullet
        Select Case rtbEdit.SelectionAlignment
            Case HorizontalAlignment.Left
                btnIzquierda.Down = True
            Case HorizontalAlignment.Center
                btnCentro.Down = True
            Case HorizontalAlignment.Right
                btnDerecha.Down = True
        End Select
        btnFonts.EditValue = SelectFont.Name
        btnSize.EditValue = SelectFont.Size
        btnVineta.Down = rtbEdit.SelectionBullet
    End Sub

    Private Sub InicializaFamiliaFuentes()
        Dim fntCollection As InstalledFontCollection = New InstalledFontCollection
        Dim fntFamily() As FontFamily
        fntFamily = fntCollection.Families
        cboFonts.Items.Clear()

        Dim i As Integer = 0
        For i = 0 To fntFamily.Length - 1 Step i + 1
            cboFonts.Items.Add(fntFamily(i).Name)
        Next
    End Sub

    'Protected Sub InitEdit(ByVal b As Boolean)
    '    'iCut.Enabled = b
    '    'iCopy.Enabled = b
    '    'iClear.Enabled = b
    '    'iUndo.Enabled = rtPad.CanUndo
    '    'iSelectAll.Enabled = rtPad.CanSelect
    'End Sub

    Private Sub CreateColorPopup(ByVal container As PopupControlContainer)
        cp = New ColorPopup(container, btnAColor, rtbEdit)
    End Sub


#End Region

#Region "DIPROS Systems, Eventos los Controles"

    Private Sub TINRichTextBox_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CreateColorPopup(PopupControlContainer1)
        InicializaFamiliaFuentes()
        InitFormat()
    End Sub

    Private Sub rtbEdit_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtbEdit.SelectionChanged
        If CType(sender, RichTextBox).SelectedText.Length = 0 Then
            InitFormat()
        End If

    End Sub


    Private Sub btnAbrir_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAbrir.ItemClick
        Dim dlg As New OpenFileDialog
        dlg.Filter = "Documentos (*.rtf)|*.rtf"
        dlg.Title = "Abrir"
        If dlg.ShowDialog() = DialogResult.OK Then
            Try
                rtbEdit.LoadFile(dlg.FileName)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
        rtbEdit.Focus()
    End Sub
    Private Sub btnGuardar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnGuardar.ItemClick
        Dim dlg As New SaveFileDialog
        dlg.Filter = "Documentos  (*.rtf)|*.rtf"
        dlg.Title = "Guardar como"
        If dlg.ShowDialog() = DialogResult.OK Then
            rtbEdit.SaveFile(dlg.FileName, RichTextBoxStreamType.RichText)
        End If
        rtbEdit.Focus()
    End Sub
    Private Sub btnFonts_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFonts.EditValueChanged
        Dim sFont As String = btnFonts.EditValue

        rtbEdit.SelectionFont = New Font(sFont, SelectFont.Size, rtPadFontStyle())
        rtbEdit.Focus()
    End Sub
    Private Sub btnSize_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSize.EditValueChanged
        rtbEdit.SelectionFont = New Font(SelectFont.FontFamily.Name, btnSize.EditValue, rtPadFontStyle())
        rtbEdit.Focus()
    End Sub
    Private Sub btnCopiar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnCopiar.ItemClick
        If Me.rtbEdit.SelectionLength > 0 Then
            'copiarlo
            Me.rtbEdit.Copy()
        End If

    End Sub
    Private Sub btnCortar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnCortar.ItemClick
        If rtbEdit.SelectionLength > 0 Then
            ' cortarlo
            rtbEdit.Cut()
        End If

    End Sub
    Private Sub btnPegar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPegar.ItemClick
        rtbEdit.Paste()

    End Sub
    Private Sub btnDeshacer_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDeshacer.ItemClick
        ' si se puede deshacer
        If Me.rtbEdit.CanUndo Then
            ' deshacer
            rtbEdit.Undo()
        Else
            ShowMessage(MessageType.MsgError, "No se Puede Deshacer")
        End If

    End Sub
    Private Sub iFontStyle_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNegrita.ItemClick, btnCursiva.ItemClick, btnSubrayado.ItemClick
        rtbEdit.SelectionFont = New Font(SelectFont.FontFamily.Name, SelectFont.Size, rtPadFontStyle())
        rtbEdit.Focus()
    End Sub
    Private Sub btnAColor_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAColor.ItemClick
        rtbEdit.SelectionColor = cp.ResultColor
        rtbEdit.Focus()
    End Sub
    Private Sub btnIzquierda_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnIzquierda.ItemClick, btnDerecha.ItemClick, btnCentro.ItemClick
        If btnIzquierda.Down Then rtbEdit.SelectionAlignment = HorizontalAlignment.Left
        If btnCentro.Down Then rtbEdit.SelectionAlignment = HorizontalAlignment.Center
        If btnDerecha.Down Then rtbEdit.SelectionAlignment = HorizontalAlignment.Right
        rtbEdit.Focus()
    End Sub
    Private Sub btnVineta_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnVineta.ItemClick
        rtbEdit.SelectionBullet = Me.btnVineta.Down
        rtbEdit.Focus()
    End Sub
    Private Sub rtbEdit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rtbEdit.TextChanged
        mChange = True
    End Sub

#End Region

#Region "DIPROS Systems, Funciones"

    Private Function rtPadFontStyle() As FontStyle
        Dim fs As New FontStyle
        If Me.btnNegrita.Down Then fs = fs Or FontStyle.Bold
        If Me.btnCursiva.Down Then fs = fs Or FontStyle.Italic
        If Me.btnSubrayado.Down Then fs = fs Or FontStyle.Underline
        Return fs
    End Function

#End Region


    
End Class

