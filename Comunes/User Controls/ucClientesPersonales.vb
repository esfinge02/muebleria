Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class ucClientesPersonales
    Inherits System.Windows.Forms.UserControl

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Private WithEvents txtExtension As DevExpress.XtraEditors.TextEdit
    Private WithEvents lblExtension As System.Windows.Forms.Label
    Private WithEvents txtNextel As DevExpress.XtraEditors.TextEdit
    Private WithEvents lblTelefonoNextel As System.Windows.Forms.Label
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents Label7 As System.Windows.Forms.Label
    Private WithEvents Label12 As System.Windows.Forms.Label
    Private WithEvents Label13 As System.Windows.Forms.Label
    Private WithEvents Label14 As System.Windows.Forms.Label
    Private WithEvents Label15 As System.Windows.Forms.Label
    Private WithEvents Label16 As System.Windows.Forms.Label
    Private WithEvents Label17 As System.Windows.Forms.Label
    Private WithEvents Label18 As System.Windows.Forms.Label
    Private WithEvents Label20 As System.Windows.Forms.Label
    Private WithEvents Label21 As System.Windows.Forms.Label
    Private WithEvents Label22 As System.Windows.Forms.Label
    Private WithEvents Label23 As System.Windows.Forms.Label
    Private WithEvents Label24 As System.Windows.Forms.Label
    Private WithEvents Label25 As System.Windows.Forms.Label
    Private WithEvents Label27 As System.Windows.Forms.Label
    Private WithEvents Label28 As System.Windows.Forms.Label
    Private WithEvents Label29 As System.Windows.Forms.Label
    Private WithEvents Label30 As System.Windows.Forms.Label
    Private WithEvents Label31 As System.Windows.Forms.Label
    Private WithEvents Label33 As System.Windows.Forms.Label
    Private WithEvents lblCorreo As System.Windows.Forms.Label
    Private WithEvents cboDondeVive As DevExpress.XtraEditors.ImageComboBoxEdit
    Private WithEvents lblCasaVive As System.Windows.Forms.Label
    Private WithEvents lblPago As System.Windows.Forms.Label
    Private WithEvents txtTeCasa As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtTOficina As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtTCelular As DevExpress.XtraEditors.TextEdit
    Private WithEvents clcPago As Dipros.Editors.TINCalcEdit
    Private WithEvents txtCorreo As DevExpress.XtraEditors.TextEdit
    Private WithEvents cbxEstadoCivil As DevExpress.XtraEditors.ImageComboBoxEdit
    Private WithEvents lkpClienteDepende As Dipros.Editors.TINMultiLookup
    Private WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Private WithEvents clcSaldo As Dipros.Editors.TINCalcEdit
    Private WithEvents cbxTipoCobro As DevExpress.XtraEditors.ImageComboBoxEdit
    Private WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Private WithEvents txtYCalle As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtEntreCalle As DevExpress.XtraEditors.TextEdit
    Private WithEvents lkpLocalidad As Dipros.Editors.TINMultiLookup
    Private WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Private WithEvents txtRFC As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtCURP As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtCalle As DevExpress.XtraEditors.TextEdit
    Private WithEvents clcCodigoPostal As Dipros.Editors.TINCalcEdit
    Private WithEvents txtExterior As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtInterior As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtPaterno As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtMaterno As DevExpress.XtraEditors.TextEdit
    Private WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    Private WithEvents lblEtiquetaJuridico As System.Windows.Forms.Label
    Private WithEvents chkLocalizado As DevExpress.XtraEditors.CheckEdit
    Private WithEvents lbl As System.Windows.Forms.Label
    Private WithEvents txtUsuarioNoLocalizable As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtmotivos_no_localizable As DevExpress.XtraEditors.MemoEdit
    Private WithEvents txtEmpresa As DevExpress.XtraEditors.TextEdit
    Private WithEvents lblNombre As System.Windows.Forms.Label
    Private WithEvents lblPaterno As System.Windows.Forms.Label
    Private WithEvents lblMaterno As System.Windows.Forms.Label
    Private WithEvents chkNoPagaComision As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents dteFechaNacimiento As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFechaNacimiento As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.clcPago = New Dipros.Editors.TINCalcEdit
        Me.lblPago = New System.Windows.Forms.Label
        Me.lblCasaVive = New System.Windows.Forms.Label
        Me.cboDondeVive = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblCorreo = New System.Windows.Forms.Label
        Me.txtCorreo = New DevExpress.XtraEditors.TextEdit
        Me.txtExtension = New DevExpress.XtraEditors.TextEdit
        Me.lblExtension = New System.Windows.Forms.Label
        Me.txtNextel = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefonoNextel = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lbl = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.cbxEstadoCivil = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.txtUsuarioNoLocalizable = New DevExpress.XtraEditors.TextEdit
        Me.txtmotivos_no_localizable = New DevExpress.XtraEditors.MemoEdit
        Me.lblEtiquetaJuridico = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.lkpClienteDepende = New Dipros.Editors.TINMultiLookup
        Me.chkNoPagaComision = New DevExpress.XtraEditors.CheckEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.Label14 = New System.Windows.Forms.Label
        Me.clcSaldo = New Dipros.Editors.TINCalcEdit
        Me.Label15 = New System.Windows.Forms.Label
        Me.cbxTipoCobro = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtYCalle = New DevExpress.XtraEditors.TextEdit
        Me.txtEntreCalle = New DevExpress.XtraEditors.TextEdit
        Me.lkpLocalidad = New Dipros.Editors.TINMultiLookup
        Me.Label18 = New System.Windows.Forms.Label
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lblNombre = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtRFC = New DevExpress.XtraEditors.TextEdit
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtCURP = New DevExpress.XtraEditors.TextEdit
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtCalle = New DevExpress.XtraEditors.TextEdit
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.clcCodigoPostal = New Dipros.Editors.TINCalcEdit
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtTeCasa = New DevExpress.XtraEditors.TextEdit
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtTOficina = New DevExpress.XtraEditors.TextEdit
        Me.Label30 = New System.Windows.Forms.Label
        Me.txtTCelular = New DevExpress.XtraEditors.TextEdit
        Me.Label31 = New System.Windows.Forms.Label
        Me.txtExterior = New DevExpress.XtraEditors.TextEdit
        Me.Label33 = New System.Windows.Forms.Label
        Me.txtInterior = New DevExpress.XtraEditors.TextEdit
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.txtPaterno = New DevExpress.XtraEditors.TextEdit
        Me.lblPaterno = New System.Windows.Forms.Label
        Me.txtMaterno = New DevExpress.XtraEditors.TextEdit
        Me.lblMaterno = New System.Windows.Forms.Label
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.chkLocalizado = New DevExpress.XtraEditors.CheckEdit
        Me.txtEmpresa = New DevExpress.XtraEditors.TextEdit
        Me.dteFechaNacimiento = New DevExpress.XtraEditors.DateEdit
        Me.lblFechaNacimiento = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.clcPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDondeVive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCorreo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExtension.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNextel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxEstadoCivil.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsuarioNoLocalizable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtmotivos_no_localizable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNoPagaComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxTipoCobro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYCalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEntreCalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRFC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCURP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCodigoPostal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTeCasa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTOficina.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTCelular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPaterno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMaterno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLocalizado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmpresa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaNacimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblFechaNacimiento)
        Me.Panel1.Controls.Add(Me.dteFechaNacimiento)
        Me.Panel1.Controls.Add(Me.clcPago)
        Me.Panel1.Controls.Add(Me.lblPago)
        Me.Panel1.Controls.Add(Me.lblCasaVive)
        Me.Panel1.Controls.Add(Me.cboDondeVive)
        Me.Panel1.Controls.Add(Me.lblCorreo)
        Me.Panel1.Controls.Add(Me.txtCorreo)
        Me.Panel1.Controls.Add(Me.txtExtension)
        Me.Panel1.Controls.Add(Me.lblExtension)
        Me.Panel1.Controls.Add(Me.txtNextel)
        Me.Panel1.Controls.Add(Me.lblTelefonoNextel)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.lbl)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.cbxEstadoCivil)
        Me.Panel1.Controls.Add(Me.txtUsuarioNoLocalizable)
        Me.Panel1.Controls.Add(Me.txtmotivos_no_localizable)
        Me.Panel1.Controls.Add(Me.lblEtiquetaJuridico)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.lkpClienteDepende)
        Me.Panel1.Controls.Add(Me.chkNoPagaComision)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.lkpSucursal)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.clcSaldo)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.cbxTipoCobro)
        Me.Panel1.Controls.Add(Me.lkpEstado)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.txtYCalle)
        Me.Panel1.Controls.Add(Me.txtEntreCalle)
        Me.Panel1.Controls.Add(Me.lkpLocalidad)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.lkpMunicipio)
        Me.Panel1.Controls.Add(Me.lblNombre)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.txtRFC)
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.txtCURP)
        Me.Panel1.Controls.Add(Me.Label22)
        Me.Panel1.Controls.Add(Me.txtCalle)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Controls.Add(Me.Label24)
        Me.Panel1.Controls.Add(Me.clcCodigoPostal)
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.Label27)
        Me.Panel1.Controls.Add(Me.Label28)
        Me.Panel1.Controls.Add(Me.txtTeCasa)
        Me.Panel1.Controls.Add(Me.Label29)
        Me.Panel1.Controls.Add(Me.txtTOficina)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.txtTCelular)
        Me.Panel1.Controls.Add(Me.Label31)
        Me.Panel1.Controls.Add(Me.txtExterior)
        Me.Panel1.Controls.Add(Me.Label33)
        Me.Panel1.Controls.Add(Me.txtInterior)
        Me.Panel1.Controls.Add(Me.txtNombre)
        Me.Panel1.Controls.Add(Me.txtPaterno)
        Me.Panel1.Controls.Add(Me.lblPaterno)
        Me.Panel1.Controls.Add(Me.txtMaterno)
        Me.Panel1.Controls.Add(Me.lblMaterno)
        Me.Panel1.Controls.Add(Me.lkpColonia)
        Me.Panel1.Controls.Add(Me.chkLocalizado)
        Me.Panel1.Controls.Add(Me.txtEmpresa)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(704, 336)
        Me.Panel1.TabIndex = 0
        '
        'clcPago
        '
        Me.clcPago.EditValue = "0"
        Me.clcPago.Location = New System.Drawing.Point(392, 280)
        Me.clcPago.MaxValue = 0
        Me.clcPago.MinValue = 0
        Me.clcPago.Name = "clcPago"
        '
        'clcPago.Properties
        '
        Me.clcPago.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPago.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPago.Properties.Enabled = False
        Me.clcPago.Properties.MaskData.EditMask = "#"
        Me.clcPago.Properties.MaxLength = 20
        Me.clcPago.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPago.Size = New System.Drawing.Size(72, 19)
        Me.clcPago.TabIndex = 60
        Me.clcPago.Tag = "pago_casa"
        '
        'lblPago
        '
        Me.lblPago.AutoSize = True
        Me.lblPago.Location = New System.Drawing.Point(312, 280)
        Me.lblPago.Name = "lblPago"
        Me.lblPago.Size = New System.Drawing.Size(74, 16)
        Me.lblPago.TabIndex = 59
        Me.lblPago.Tag = ""
        Me.lblPago.Text = "Su pago es $:"
        Me.lblPago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCasaVive
        '
        Me.lblCasaVive.AutoSize = True
        Me.lblCasaVive.Location = New System.Drawing.Point(40, 280)
        Me.lblCasaVive.Name = "lblCasaVive"
        Me.lblCasaVive.Size = New System.Drawing.Size(128, 16)
        Me.lblCasaVive.TabIndex = 57
        Me.lblCasaVive.Tag = ""
        Me.lblCasaVive.Text = "La casa donde vives es :"
        Me.lblCasaVive.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboDondeVive
        '
        Me.cboDondeVive.EditValue = "P"
        Me.cboDondeVive.Location = New System.Drawing.Point(184, 280)
        Me.cboDondeVive.Name = "cboDondeVive"
        '
        'cboDondeVive.Properties
        '
        Me.cboDondeVive.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboDondeVive.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Propia", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Rentada", "R", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cr�dito", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Familiar", "F", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Infonavit", "I", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pagandola", "G", -1)})
        Me.cboDondeVive.Size = New System.Drawing.Size(112, 23)
        Me.cboDondeVive.TabIndex = 58
        Me.cboDondeVive.Tag = "tipo_casa"
        '
        'lblCorreo
        '
        Me.lblCorreo.AutoSize = True
        Me.lblCorreo.Location = New System.Drawing.Point(40, 256)
        Me.lblCorreo.Name = "lblCorreo"
        Me.lblCorreo.Size = New System.Drawing.Size(42, 16)
        Me.lblCorreo.TabIndex = 53
        Me.lblCorreo.Tag = ""
        Me.lblCorreo.Text = "Correo:"
        Me.lblCorreo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCorreo
        '
        Me.txtCorreo.EditValue = ""
        Me.txtCorreo.Location = New System.Drawing.Point(88, 256)
        Me.txtCorreo.Name = "txtCorreo"
        '
        'txtCorreo.Properties
        '
        Me.txtCorreo.Properties.MaxLength = 80
        Me.txtCorreo.Size = New System.Drawing.Size(312, 20)
        Me.txtCorreo.TabIndex = 54
        Me.txtCorreo.Tag = ""
        '
        'txtExtension
        '
        Me.txtExtension.EditValue = ""
        Me.txtExtension.Location = New System.Drawing.Point(360, 168)
        Me.txtExtension.Name = "txtExtension"
        '
        'txtExtension.Properties
        '
        Me.txtExtension.Properties.MaxLength = 4
        Me.txtExtension.Size = New System.Drawing.Size(32, 20)
        Me.txtExtension.TabIndex = 35
        Me.txtExtension.Tag = "extension"
        '
        'lblExtension
        '
        Me.lblExtension.AutoSize = True
        Me.lblExtension.Location = New System.Drawing.Point(328, 168)
        Me.lblExtension.Name = "lblExtension"
        Me.lblExtension.Size = New System.Drawing.Size(27, 16)
        Me.lblExtension.TabIndex = 34
        Me.lblExtension.Tag = ""
        Me.lblExtension.Text = "Ex&t.:"
        Me.lblExtension.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNextel
        '
        Me.txtNextel.EditValue = ""
        Me.txtNextel.Location = New System.Drawing.Point(624, 168)
        Me.txtNextel.Name = "txtNextel"
        '
        'txtNextel.Properties
        '
        Me.txtNextel.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtNextel.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtNextel.Properties.MaxLength = 10
        Me.txtNextel.Size = New System.Drawing.Size(72, 20)
        Me.txtNextel.TabIndex = 39
        Me.txtNextel.Tag = "nextel"
        '
        'lblTelefonoNextel
        '
        Me.lblTelefonoNextel.AutoSize = True
        Me.lblTelefonoNextel.Location = New System.Drawing.Point(560, 168)
        Me.lblTelefonoNextel.Name = "lblTelefonoNextel"
        Me.lblTelefonoNextel.Size = New System.Drawing.Size(65, 16)
        Me.lblTelefonoNextel.TabIndex = 38
        Me.lblTelefonoNextel.Tag = ""
        Me.lblTelefonoNextel.Text = "Tel. &Nextel.:"
        Me.lblTelefonoNextel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(248, 192)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 16)
        Me.Label1.TabIndex = 42
        Me.Label1.Tag = ""
        Me.Label1.Text = "Edo. Civil:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl
        '
        Me.lbl.AutoSize = True
        Me.lbl.Location = New System.Drawing.Point(552, 264)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(124, 16)
        Me.lbl.TabIndex = 56
        Me.lbl.Tag = ""
        Me.lbl.Text = "&Usuario No Localizable:"
        Me.lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbl.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(408, 264)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(124, 16)
        Me.Label7.TabIndex = 55
        Me.Label7.Tag = ""
        Me.Label7.Text = "&Motivos No Localizable:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label7.Visible = False
        '
        'cbxEstadoCivil
        '
        Me.cbxEstadoCivil.EditValue = "S"
        Me.cbxEstadoCivil.Location = New System.Drawing.Point(312, 192)
        Me.cbxEstadoCivil.Name = "cbxEstadoCivil"
        '
        'cbxEstadoCivil.Properties
        '
        Me.cbxEstadoCivil.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxEstadoCivil.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Casado (a)", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Soltero (a)", "S", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Otro", "O", -1)})
        Me.cbxEstadoCivil.Size = New System.Drawing.Size(144, 23)
        Me.cbxEstadoCivil.TabIndex = 43
        Me.cbxEstadoCivil.TabStop = False
        Me.cbxEstadoCivil.Tag = "estado_civil"
        '
        'txtUsuarioNoLocalizable
        '
        Me.txtUsuarioNoLocalizable.EditValue = ""
        Me.txtUsuarioNoLocalizable.Location = New System.Drawing.Point(464, 240)
        Me.txtUsuarioNoLocalizable.Name = "txtUsuarioNoLocalizable"
        '
        'txtUsuarioNoLocalizable.Properties
        '
        Me.txtUsuarioNoLocalizable.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuarioNoLocalizable.Properties.MaxLength = 25
        Me.txtUsuarioNoLocalizable.Size = New System.Drawing.Size(80, 20)
        Me.txtUsuarioNoLocalizable.TabIndex = 51
        Me.txtUsuarioNoLocalizable.Tag = "usuario_no_localizable"
        Me.txtUsuarioNoLocalizable.Visible = False
        '
        'txtmotivos_no_localizable
        '
        Me.txtmotivos_no_localizable.EditValue = ""
        Me.txtmotivos_no_localizable.Location = New System.Drawing.Point(464, 216)
        Me.txtmotivos_no_localizable.Name = "txtmotivos_no_localizable"
        '
        'txtmotivos_no_localizable.Properties
        '
        Me.txtmotivos_no_localizable.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtmotivos_no_localizable.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtmotivos_no_localizable.Size = New System.Drawing.Size(80, 24)
        Me.txtmotivos_no_localizable.TabIndex = 48
        Me.txtmotivos_no_localizable.Tag = "motivos_no_localizable"
        Me.txtmotivos_no_localizable.Visible = False
        '
        'lblEtiquetaJuridico
        '
        Me.lblEtiquetaJuridico.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiquetaJuridico.ForeColor = System.Drawing.Color.Red
        Me.lblEtiquetaJuridico.Location = New System.Drawing.Point(88, 240)
        Me.lblEtiquetaJuridico.Name = "lblEtiquetaJuridico"
        Me.lblEtiquetaJuridico.Size = New System.Drawing.Size(304, 16)
        Me.lblEtiquetaJuridico.TabIndex = 50
        Me.lblEtiquetaJuridico.Tag = ""
        Me.lblEtiquetaJuridico.Text = "El Cliente tiene un Proceso Jur�dico"
        Me.lblEtiquetaJuridico.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEtiquetaJuridico.Visible = False
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(576, 288)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 24)
        Me.Label12.TabIndex = 61
        Me.Label12.Tag = ""
        Me.Label12.Text = "Cliente dependencia:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label12.Visible = False
        '
        'lkpClienteDepende
        '
        Me.lkpClienteDepende.AllowAdd = False
        Me.lkpClienteDepende.AutoReaload = False
        Me.lkpClienteDepende.DataSource = Nothing
        Me.lkpClienteDepende.DefaultSearchField = ""
        Me.lkpClienteDepende.DisplayMember = "nombre"
        Me.lkpClienteDepende.EditValue = Nothing
        Me.lkpClienteDepende.Enabled = False
        Me.lkpClienteDepende.Filtered = False
        Me.lkpClienteDepende.InitValue = Nothing
        Me.lkpClienteDepende.Location = New System.Drawing.Point(88, 240)
        Me.lkpClienteDepende.MultiSelect = False
        Me.lkpClienteDepende.Name = "lkpClienteDepende"
        Me.lkpClienteDepende.NullText = ""
        Me.lkpClienteDepende.PopupWidth = CType(520, Long)
        Me.lkpClienteDepende.ReadOnlyControl = False
        Me.lkpClienteDepende.Required = False
        Me.lkpClienteDepende.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteDepende.SearchMember = ""
        Me.lkpClienteDepende.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteDepende.SelectAll = False
        Me.lkpClienteDepende.Size = New System.Drawing.Size(300, 20)
        Me.lkpClienteDepende.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteDepende.TabIndex = 148
        Me.lkpClienteDepende.Tag = "cliente_dependencias"
        Me.lkpClienteDepende.ToolTip = Nothing
        Me.lkpClienteDepende.ValueMember = "Cliente"
        Me.lkpClienteDepende.Visible = False
        '
        'chkNoPagaComision
        '
        Me.chkNoPagaComision.Location = New System.Drawing.Point(545, 216)
        Me.chkNoPagaComision.Name = "chkNoPagaComision"
        '
        'chkNoPagaComision.Properties
        '
        Me.chkNoPagaComision.Properties.Caption = "No Paga Comisi�n"
        Me.chkNoPagaComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkNoPagaComision.Size = New System.Drawing.Size(128, 20)
        Me.chkNoPagaComision.TabIndex = 49
        Me.chkNoPagaComision.Tag = "no_paga_comision"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(30, 216)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 16)
        Me.Label13.TabIndex = 46
        Me.Label13.Tag = ""
        Me.Label13.Text = "Sucursal:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(88, 216)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(300, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 47
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(502, 192)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(36, 16)
        Me.Label14.TabIndex = 44
        Me.Label14.Tag = ""
        Me.Label14.Text = "Saldo:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldo.Location = New System.Drawing.Point(545, 192)
        Me.clcSaldo.MaxValue = 0
        Me.clcSaldo.MinValue = 0
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSaldo.Properties.Precision = 2
        Me.clcSaldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSaldo.Size = New System.Drawing.Size(95, 19)
        Me.clcSaldo.TabIndex = 45
        Me.clcSaldo.Tag = "saldo"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(2, 192)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(76, 16)
        Me.Label15.TabIndex = 40
        Me.Label15.Tag = ""
        Me.Label15.Text = "Tipo de cobro:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxTipoCobro
        '
        Me.cbxTipoCobro.EditValue = "O"
        Me.cbxTipoCobro.Location = New System.Drawing.Point(88, 192)
        Me.cbxTipoCobro.Name = "cbxTipoCobro"
        '
        'cbxTipoCobro.Properties
        '
        Me.cbxTipoCobro.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTipoCobro.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ocurre", "O", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cobrar", "C", -1)})
        Me.cbxTipoCobro.Size = New System.Drawing.Size(144, 23)
        Me.cbxTipoCobro.TabIndex = 41
        Me.cbxTipoCobro.Tag = "tipo_cobro"
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(89, 72)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.ReadOnlyControl = False
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(144, 20)
        Me.lkpEstado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEstado.TabIndex = 11
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(292, 120)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(13, 16)
        Me.Label16.TabIndex = 22
        Me.Label16.Tag = ""
        Me.Label16.Text = "y:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(47, 120)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 16)
        Me.Label17.TabIndex = 20
        Me.Label17.Tag = ""
        Me.Label17.Text = "Entre:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtYCalle
        '
        Me.txtYCalle.EditValue = ""
        Me.txtYCalle.Location = New System.Drawing.Point(313, 120)
        Me.txtYCalle.Name = "txtYCalle"
        '
        'txtYCalle.Properties
        '
        Me.txtYCalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtYCalle.Properties.MaxLength = 30
        Me.txtYCalle.Size = New System.Drawing.Size(192, 20)
        Me.txtYCalle.TabIndex = 23
        Me.txtYCalle.Tag = "ycalle"
        '
        'txtEntreCalle
        '
        Me.txtEntreCalle.EditValue = ""
        Me.txtEntreCalle.Location = New System.Drawing.Point(89, 120)
        Me.txtEntreCalle.Name = "txtEntreCalle"
        '
        'txtEntreCalle.Properties
        '
        Me.txtEntreCalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEntreCalle.Properties.MaxLength = 30
        Me.txtEntreCalle.Size = New System.Drawing.Size(192, 20)
        Me.txtEntreCalle.TabIndex = 21
        Me.txtEntreCalle.Tag = "entrecalle"
        '
        'lkpLocalidad
        '
        Me.lkpLocalidad.AllowAdd = False
        Me.lkpLocalidad.AutoReaload = True
        Me.lkpLocalidad.DataSource = Nothing
        Me.lkpLocalidad.DefaultSearchField = ""
        Me.lkpLocalidad.DisplayMember = "descripcion"
        Me.lkpLocalidad.EditValue = Nothing
        Me.lkpLocalidad.Filtered = False
        Me.lkpLocalidad.InitValue = Nothing
        Me.lkpLocalidad.Location = New System.Drawing.Point(545, 72)
        Me.lkpLocalidad.MultiSelect = False
        Me.lkpLocalidad.Name = "lkpLocalidad"
        Me.lkpLocalidad.NullText = ""
        Me.lkpLocalidad.PopupWidth = CType(300, Long)
        Me.lkpLocalidad.ReadOnlyControl = False
        Me.lkpLocalidad.Required = False
        Me.lkpLocalidad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpLocalidad.SearchMember = ""
        Me.lkpLocalidad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpLocalidad.SelectAll = False
        Me.lkpLocalidad.Size = New System.Drawing.Size(144, 20)
        Me.lkpLocalidad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpLocalidad.TabIndex = 15
        Me.lkpLocalidad.Tag = "ciudad"
        Me.lkpLocalidad.ToolTip = Nothing
        Me.lkpLocalidad.ValueMember = "ciudad"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(248, 72)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(55, 16)
        Me.Label18.TabIndex = 12
        Me.Label18.Tag = ""
        Me.Label18.Text = "Municipio:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = True
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(313, 72)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(300, Long)
        Me.lkpMunicipio.ReadOnlyControl = False
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(144, 20)
        Me.lkpMunicipio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMunicipio.TabIndex = 13
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(14, 19)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(64, 16)
        Me.lblNombre.TabIndex = 0
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre (s):"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(60, 40)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(24, 16)
        Me.Label20.TabIndex = 6
        Me.Label20.Tag = ""
        Me.Label20.Text = "R&fc:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRFC
        '
        Me.txtRFC.EditValue = ""
        Me.txtRFC.Location = New System.Drawing.Point(89, 40)
        Me.txtRFC.Name = "txtRFC"
        '
        'txtRFC.Properties
        '
        Me.txtRFC.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRFC.Properties.MaxLength = 15
        Me.txtRFC.Size = New System.Drawing.Size(120, 20)
        Me.txtRFC.TabIndex = 7
        Me.txtRFC.Tag = "rfc"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(273, 40)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(32, 16)
        Me.Label21.TabIndex = 8
        Me.Label21.Tag = ""
        Me.Label21.Text = "C&urp:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCURP
        '
        Me.txtCURP.EditValue = ""
        Me.txtCURP.Location = New System.Drawing.Point(313, 40)
        Me.txtCURP.Name = "txtCURP"
        '
        'txtCURP.Properties
        '
        Me.txtCURP.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCURP.Properties.MaxLength = 20
        Me.txtCURP.Size = New System.Drawing.Size(120, 20)
        Me.txtCURP.TabIndex = 9
        Me.txtCURP.Tag = "curp"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(272, 96)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(33, 16)
        Me.Label22.TabIndex = 18
        Me.Label22.Tag = ""
        Me.Label22.Text = "Calle:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCalle
        '
        Me.txtCalle.EditValue = ""
        Me.txtCalle.Location = New System.Drawing.Point(313, 96)
        Me.txtCalle.Name = "txtCalle"
        '
        'txtCalle.Properties
        '
        Me.txtCalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCalle.Properties.MaxLength = 50
        Me.txtCalle.Size = New System.Drawing.Size(312, 20)
        Me.txtCalle.TabIndex = 19
        Me.txtCalle.Tag = "domicilio"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(36, 96)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(46, 16)
        Me.Label23.TabIndex = 16
        Me.Label23.Tag = ""
        Me.Label23.Text = "C&olonia:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(496, 144)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(22, 16)
        Me.Label24.TabIndex = 28
        Me.Label24.Tag = ""
        Me.Label24.Text = "Cp&:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCodigoPostal
        '
        Me.clcCodigoPostal.EditValue = "0"
        Me.clcCodigoPostal.Location = New System.Drawing.Point(528, 144)
        Me.clcCodigoPostal.MaxValue = 0
        Me.clcCodigoPostal.MinValue = 0
        Me.clcCodigoPostal.Name = "clcCodigoPostal"
        '
        'clcCodigoPostal.Properties
        '
        Me.clcCodigoPostal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostal.Properties.MaskData.EditMask = "#"
        Me.clcCodigoPostal.Properties.MaxLength = 5
        Me.clcCodigoPostal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCodigoPostal.Size = New System.Drawing.Size(56, 19)
        Me.clcCodigoPostal.TabIndex = 29
        Me.clcCodigoPostal.Tag = "cp"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(482, 72)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(56, 16)
        Me.Label25.TabIndex = 14
        Me.Label25.Tag = ""
        Me.Label25.Text = "Locali&dad:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(40, 72)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(43, 16)
        Me.Label27.TabIndex = 10
        Me.Label27.Tag = ""
        Me.Label27.Text = "E&stado:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(23, 168)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(56, 16)
        Me.Label28.TabIndex = 30
        Me.Label28.Tag = ""
        Me.Label28.Text = "Tel. &Casa:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTeCasa
        '
        Me.txtTeCasa.EditValue = ""
        Me.txtTeCasa.Location = New System.Drawing.Point(89, 168)
        Me.txtTeCasa.Name = "txtTeCasa"
        '
        'txtTeCasa.Properties
        '
        Me.txtTeCasa.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTeCasa.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTeCasa.Properties.MaxLength = 10
        Me.txtTeCasa.Size = New System.Drawing.Size(71, 20)
        Me.txtTeCasa.TabIndex = 31
        Me.txtTeCasa.Tag = "telefono1"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(176, 168)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(65, 16)
        Me.Label29.TabIndex = 32
        Me.Label29.Tag = ""
        Me.Label29.Text = "Tel. &Oficina:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTOficina
        '
        Me.txtTOficina.EditValue = ""
        Me.txtTOficina.Location = New System.Drawing.Point(248, 168)
        Me.txtTOficina.Name = "txtTOficina"
        '
        'txtTOficina.Properties
        '
        Me.txtTOficina.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTOficina.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTOficina.Properties.MaxLength = 10
        Me.txtTOficina.Size = New System.Drawing.Size(72, 20)
        Me.txtTOficina.TabIndex = 33
        Me.txtTOficina.Tag = "telefono2"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(416, 168)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(49, 16)
        Me.Label30.TabIndex = 36
        Me.Label30.Tag = ""
        Me.Label30.Text = "Tel. &Cel.:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTCelular
        '
        Me.txtTCelular.EditValue = ""
        Me.txtTCelular.Location = New System.Drawing.Point(472, 168)
        Me.txtTCelular.Name = "txtTCelular"
        '
        'txtTCelular.Properties
        '
        Me.txtTCelular.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTCelular.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTCelular.Properties.MaxLength = 13
        Me.txtTCelular.Size = New System.Drawing.Size(72, 20)
        Me.txtTCelular.TabIndex = 37
        Me.txtTCelular.Tag = "fax"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(34, 144)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(46, 16)
        Me.Label31.TabIndex = 24
        Me.Label31.Tag = ""
        Me.Label31.Text = "Ex&terior:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtExterior
        '
        Me.txtExterior.EditValue = ""
        Me.txtExterior.Location = New System.Drawing.Point(89, 144)
        Me.txtExterior.Name = "txtExterior"
        '
        'txtExterior.Properties
        '
        Me.txtExterior.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExterior.Properties.MaxLength = 10
        Me.txtExterior.Size = New System.Drawing.Size(80, 20)
        Me.txtExterior.TabIndex = 25
        Me.txtExterior.Tag = "numero_exterior"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(257, 144)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(43, 16)
        Me.Label33.TabIndex = 26
        Me.Label33.Tag = ""
        Me.Label33.Text = "&Interior:"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtInterior
        '
        Me.txtInterior.EditValue = ""
        Me.txtInterior.Location = New System.Drawing.Point(313, 144)
        Me.txtInterior.Name = "txtInterior"
        '
        'txtInterior.Properties
        '
        Me.txtInterior.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtInterior.Properties.MaxLength = 10
        Me.txtInterior.Size = New System.Drawing.Size(78, 20)
        Me.txtInterior.TabIndex = 27
        Me.txtInterior.Tag = "numero_interior"
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(89, 16)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Properties.MaxLength = 40
        Me.txtNombre.Size = New System.Drawing.Size(144, 20)
        Me.txtNombre.TabIndex = 1
        Me.txtNombre.Tag = "nombres"
        '
        'txtPaterno
        '
        Me.txtPaterno.EditValue = ""
        Me.txtPaterno.Location = New System.Drawing.Point(313, 16)
        Me.txtPaterno.Name = "txtPaterno"
        '
        'txtPaterno.Properties
        '
        Me.txtPaterno.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPaterno.Properties.MaxLength = 30
        Me.txtPaterno.Size = New System.Drawing.Size(144, 20)
        Me.txtPaterno.TabIndex = 3
        Me.txtPaterno.Tag = "paterno"
        '
        'lblPaterno
        '
        Me.lblPaterno.AutoSize = True
        Me.lblPaterno.Location = New System.Drawing.Point(241, 19)
        Me.lblPaterno.Name = "lblPaterno"
        Me.lblPaterno.Size = New System.Drawing.Size(61, 16)
        Me.lblPaterno.TabIndex = 2
        Me.lblPaterno.Tag = ""
        Me.lblPaterno.Text = "A. Pater&no:"
        Me.lblPaterno.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMaterno
        '
        Me.txtMaterno.EditValue = ""
        Me.txtMaterno.Location = New System.Drawing.Point(545, 16)
        Me.txtMaterno.Name = "txtMaterno"
        '
        'txtMaterno.Properties
        '
        Me.txtMaterno.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMaterno.Properties.MaxLength = 30
        Me.txtMaterno.Size = New System.Drawing.Size(144, 20)
        Me.txtMaterno.TabIndex = 5
        Me.txtMaterno.Tag = "materno"
        '
        'lblMaterno
        '
        Me.lblMaterno.AutoSize = True
        Me.lblMaterno.Location = New System.Drawing.Point(473, 19)
        Me.lblMaterno.Name = "lblMaterno"
        Me.lblMaterno.Size = New System.Drawing.Size(63, 16)
        Me.lblMaterno.TabIndex = 4
        Me.lblMaterno.Tag = ""
        Me.lblMaterno.Text = "A. Matern&o:"
        Me.lblMaterno.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = True
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(89, 96)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(300, Long)
        Me.lkpColonia.ReadOnlyControl = False
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(144, 20)
        Me.lkpColonia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpColonia.TabIndex = 17
        Me.lkpColonia.Tag = "colonia"
        Me.lkpColonia.ToolTip = Nothing
        Me.lkpColonia.ValueMember = "colonia"
        '
        'chkLocalizado
        '
        Me.chkLocalizado.EditValue = True
        Me.chkLocalizado.Location = New System.Drawing.Point(544, 240)
        Me.chkLocalizado.Name = "chkLocalizado"
        '
        'chkLocalizado.Properties
        '
        Me.chkLocalizado.Properties.Caption = "Localizado"
        Me.chkLocalizado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkLocalizado.Size = New System.Drawing.Size(128, 20)
        Me.chkLocalizado.TabIndex = 52
        Me.chkLocalizado.Tag = "localizado"
        '
        'txtEmpresa
        '
        Me.txtEmpresa.EditValue = ""
        Me.txtEmpresa.Location = New System.Drawing.Point(88, 16)
        Me.txtEmpresa.Name = "txtEmpresa"
        '
        'txtEmpresa.Properties
        '
        Me.txtEmpresa.Properties.MaxLength = 100
        Me.txtEmpresa.Size = New System.Drawing.Size(600, 20)
        Me.txtEmpresa.TabIndex = 154
        Me.txtEmpresa.Tag = ""
        Me.txtEmpresa.Visible = False
        '
        'dteFechaNacimiento
        '
        Me.dteFechaNacimiento.EditValue = New Date(2015, 6, 5, 0, 0, 0, 0)
        Me.dteFechaNacimiento.Location = New System.Drawing.Point(560, 40)
        Me.dteFechaNacimiento.Name = "dteFechaNacimiento"
        '
        'dteFechaNacimiento.Properties
        '
        Me.dteFechaNacimiento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaNacimiento.Size = New System.Drawing.Size(128, 23)
        Me.dteFechaNacimiento.TabIndex = 155
        Me.dteFechaNacimiento.Tag = "fecha_nacimiento"
        '
        'lblFechaNacimiento
        '
        Me.lblFechaNacimiento.AutoSize = True
        Me.lblFechaNacimiento.Location = New System.Drawing.Point(440, 48)
        Me.lblFechaNacimiento.Name = "lblFechaNacimiento"
        Me.lblFechaNacimiento.Size = New System.Drawing.Size(112, 16)
        Me.lblFechaNacimiento.TabIndex = 156
        Me.lblFechaNacimiento.Tag = ""
        Me.lblFechaNacimiento.Text = "Fecha de Nacimento:"
        Me.lblFechaNacimiento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ucClientesPersonales
        '
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ucClientesPersonales"
        Me.Size = New System.Drawing.Size(704, 336)
        Me.Panel1.ResumeLayout(False)
        CType(Me.clcPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDondeVive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCorreo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExtension.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNextel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxEstadoCivil.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsuarioNoLocalizable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtmotivos_no_localizable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNoPagaComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxTipoCobro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYCalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEntreCalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRFC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCURP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCodigoPostal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTeCasa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTOficina.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTCelular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPaterno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMaterno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLocalizado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmpresa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaNacimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oEstados As New VillarrealBusiness.clsEstados
    Private oMunicipios As New VillarrealBusiness.clsMunicipios
    Private ociudades As New VillarrealBusiness.clsCiudades
    Private oColonias As New VillarrealBusiness.clsColonias
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oClientes As New VillarrealBusiness.clsClientes
    Private oClientesCobradores As New VillarrealBusiness.clsClientesCobradores


    Private forma_main As System.Windows.Forms.Form
    Private tecla_escape As Object
    Public Accion As Dipros.Utils.Common.Actions




#Region "Propiedades"

    Public Property Empresa() As String
        Get
            Return Me.txtEmpresa.Text
        End Get
        Set(ByVal Value As String)
            Me.txtEmpresa.Text = Value
        End Set
    End Property

    Public WriteOnly Property MostrarEmpresa() As Boolean
        Set(ByVal Value As Boolean)
            Me.txtEmpresa.Visible = Value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me.txtNombre.Text
        End Get
        Set(ByVal Value As String)
            Me.txtNombre.Text = Value
        End Set
    End Property

    Public Property FechaNacimiento() As DateTime
        Get
            Return Me.dteFechaNacimiento.EditValue
        End Get
        Set(ByVal Value As DateTime)
            Me.dteFechaNacimiento.EditValue = Value
        End Set
    End Property

    Public WriteOnly Property EtiquetaNombre() As String
        Set(ByVal Value As String)
            Me.lblNombre.Text = Value
        End Set
    End Property

    Public WriteOnly Property MostrarCampoNombre() As Boolean
        Set(ByVal Value As Boolean)
            Me.txtNombre.Visible = Value
        End Set
    End Property



    Public Property Paterno() As String
        Get
            Return Me.txtPaterno.Text
        End Get
        Set(ByVal Value As String)
            Me.txtPaterno.Text = Value
        End Set
    End Property

    Public WriteOnly Property MostrarCampoPaterno() As Boolean
        Set(ByVal Value As Boolean)
            Me.txtPaterno.Visible = Value
        End Set
    End Property

    Public WriteOnly Property MostrarEtiquetaPaterno() As Boolean
        Set(ByVal Value As Boolean)
            Me.lblPaterno.Visible = Value
        End Set
    End Property


    Public Property Materno() As String
        Get
            Return Me.txtMaterno.Text
        End Get
        Set(ByVal Value As String)
            Me.txtMaterno.Text = Value
        End Set
    End Property

    Public WriteOnly Property MostrarCampoMaterno() As Boolean
        Set(ByVal Value As Boolean)
            Me.txtMaterno.Visible = Value
        End Set
    End Property

    Public WriteOnly Property MostrarEtiquetaMaterno() As Boolean
        Set(ByVal Value As Boolean)
            Me.lblMaterno.Visible = Value
        End Set
    End Property

    Public Property RFC() As String
        Get
            Return Me.txtRFC.Text
        End Get
        Set(ByVal Value As String)
            Me.txtRFC.Text = Value
        End Set
    End Property

    Public Property CURPCliente() As String
        Get
            Return Me.txtCURP.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCURP.Text = Value
        End Set
    End Property

    Public Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
        Set(ByVal Value As Long)
            Me.lkpEstado.EditValue = Value
        End Set
    End Property

    Public Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
        Set(ByVal Value As Long)
            Me.lkpMunicipio.EditValue = Value
        End Set
    End Property

    Public Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpLocalidad)
        End Get
        Set(ByVal Value As Long)
            Me.lkpLocalidad.EditValue = Value
        End Set
    End Property

    Public Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
        Set(ByVal Value As Long)
            Me.lkpColonia.EditValue = Value
        End Set
    End Property

    Public Property Calle() As String
        Get
            Return Me.txtCalle.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCalle.Text = Value
        End Set
    End Property

    Public Property EntreCalles() As String
        Get
            Return Me.txtEntreCalle.Text
        End Get
        Set(ByVal Value As String)
            Me.txtEntreCalle.Text = Value
        End Set
    End Property

    Public Property YCalles() As String
        Get
            Return Me.txtYCalle.Text
        End Get
        Set(ByVal Value As String)
            Me.txtYCalle.Text = Value
        End Set
    End Property

    Public Property Exterior() As String
        Get
            Return Me.txtExterior.Text
        End Get
        Set(ByVal Value As String)
            Me.txtExterior.Text = Value
        End Set
    End Property

    Public Property Interior() As String
        Get
            Return Me.txtInterior.Text
        End Get
        Set(ByVal Value As String)
            Me.txtInterior.Text = Value
        End Set
    End Property

    Public Property CodigoPostal() As Long
        Get
            Return Me.clcCodigoPostal.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcCodigoPostal.Value = Value
        End Set
    End Property

    Public Property TelefonoCasa() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTeCasa.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTeCasa.Text = Value
        End Set
    End Property

    Public Property TelefonoOficina() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTOficina.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTOficina.Text = Value
        End Set
    End Property

    Public Property Extension() As String
        Get
            Return Me.txtExtension.Text
        End Get
        Set(ByVal Value As String)
            Me.txtExtension.Text = Value
        End Set
    End Property

    Public Property TelefonoCelular() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTCelular.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTCelular.Text = Value
        End Set
    End Property


    Public Property TelefonoNextel() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtNextel.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtNextel.Text = Value
        End Set
    End Property

    Public Property TipoCobro() As String
        Get
            Return Me.cbxTipoCobro.Value
        End Get
        Set(ByVal Value As String)
            Me.cbxTipoCobro.Value = Value
        End Set
    End Property

    Public Property EstadoCivil() As String
        Get
            Return Me.cbxEstadoCivil.EditValue
        End Get
        Set(ByVal Value As String)
            Me.cbxEstadoCivil.EditValue = Value
        End Set
    End Property

    Public Property Sueldo() As String
        Get
            Return Me.clcSaldo.Text
        End Get
        Set(ByVal Value As String)
            Me.clcSaldo.Text = Value
        End Set
    End Property

    Public Property ClienteDepende() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteDepende)
        End Get
        Set(ByVal Value As Long)
            Me.lkpClienteDepende.EditValue = Value
        End Set
    End Property

    Public WriteOnly Property HabilitaCliente() As Long
        Set(ByVal Value As Long)
            Me.lkpSucursal.Enabled = Value
        End Set
    End Property


    Public Property Correo() As String
        Get
            Return Me.txtCorreo.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCorreo.Text = Value
        End Set
    End Property

    Public Property TipoCasa() As String
        Get
            Return Me.cboDondeVive.EditValue
        End Get
        Set(ByVal Value As String)
            Me.cboDondeVive.EditValue = Value
        End Set
    End Property

    Public Property PagoCasa() As Double
        Get
            Return Me.clcPago.EditValue
        End Get
        Set(ByVal Value As Double)
            Me.clcPago.Value = Value
        End Set
    End Property


    Public Property MotivoNoLocalizable() As String
        Get
            Return Me.txtmotivos_no_localizable.Text
        End Get
        Set(ByVal Value As String)
            If Value.Trim.Length > 0 Then
                Me.txtmotivos_no_localizable.Text = Value
            Else
                Me.txtmotivos_no_localizable.Text = ""
            End If

        End Set
    End Property

    Public Property MostratEtiquetaJuridico() As Boolean
        Get
            Return Me.lblEtiquetaJuridico.Visible
        End Get
        Set(ByVal Value As Boolean)
            Me.lblEtiquetaJuridico.Visible = Value
        End Set
    End Property

    Public Property EtiquetaJuridico() As String
        Get
            Return Me.lblEtiquetaJuridico.Text
        End Get
        Set(ByVal Value As String)
            Me.lblEtiquetaJuridico.Text = Value
        End Set
    End Property

    Public Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
        Set(ByVal Value As Long)
            Me.lkpSucursal.EditValue = Value
        End Set
    End Property

    Public WriteOnly Property HabilitaSucursal() As Long
        Set(ByVal Value As Long)
            Me.lkpSucursal.Enabled = Value
        End Set
    End Property

    Public Property SucursalDependencia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteDepende.EditValue)
        End Get
        Set(ByVal Value As Long)
            Me.lkpClienteDepende.EditValue = Value
        End Set
    End Property

    Public Property Localizado() As Boolean
        Get
            Return chkLocalizado.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkLocalizado.Checked = Value
        End Set
    End Property

    Public Property NoPagaComision() As Boolean
        Get
            Return chkNoPagaComision.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkNoPagaComision.Checked = Value
        End Set
    End Property

    Public Property UsuarioNoLocalizable() As String
        Get
            Return txtUsuarioNoLocalizable.Text
        End Get
        Set(ByVal Value As String)
            txtUsuarioNoLocalizable.Text = Value
        End Set
    End Property

    Public Property Saldo() As Double
        Get
            Return clcSaldo.EditValue
        End Get
        Set(ByVal Value As Double)
            clcSaldo.EditValue = Value
        End Set
    End Property



    Public Property FormaMain() As System.Windows.Forms.Form
        Get
            Return forma_main
        End Get
        Set(ByVal Value As System.Windows.Forms.Form)
            forma_main = Value
        End Set
    End Property

    Public WriteOnly Property TeclaEscape() As Object
        Set(ByVal Value As Object)

        End Set
    End Property

#End Region

#Region "Eventos de Controles"



    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData

        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpMunicipio_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        Me.lkpLocalidad.EditValue = Nothing
        Me.lkpLocalidad_LoadData(True)
    End Sub

    Private Sub lkpLocalidad_Format() Handles lkpLocalidad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpLocalidad)
    End Sub
    Private Sub lkpLocalidad_LoadData(ByVal Initialize As Boolean) Handles lkpLocalidad.LoadData

        Dim Response As New Events
        Response = ociudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpLocalidad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpLocalidad_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpLocalidad.EditValueChanged
        If Me.lkpLocalidad.EditValue Is Nothing Then Exit Sub

        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia_LoadData(True)
    End Sub

    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub
    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpColonia_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpColonia.EditValueChanged
        clcCodigoPostal.EditValue = lkpColonia.GetValue("codigo_postal")
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData

        Dim response As Events
        response = oSucursales.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        Else
            response.ShowError()
        End If

        response = Nothing

    End Sub

    Private Sub lkpClienteDepende_Format() Handles lkpClienteDepende.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteDepende)
    End Sub
    Private Sub lkpClienteDepende_LoadData(ByVal Initialize As Boolean) Handles lkpClienteDepende.LoadData
        Dim Response As New Events
        Response = oClientes.LookupPlantilla()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteDepende.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub chkLocalizado_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLocalizado.EditValueChanged
        'If CType(sender, DevExpress.XtraEditors.CheckEdit).Checked = False And Entro_Despliega_localizado = False Then
        '    Motivo()
        'End If

        If CType(sender, DevExpress.XtraEditors.CheckEdit).Checked = False Then
            Me.txtUsuarioNoLocalizable.Text = ""
            Me.txtmotivos_no_localizable.Text = ""
        End If
    End Sub

    Private Sub txtDomicilio_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCalle.Validating
        Dim oEvent As Dipros.Utils.Events

        If Accion <> Actions.Insert Or tecla_escape = System.Windows.Forms.Keys.Escape Then Exit Sub
        oEvent = oClientes.ValidaDomicilio(txtCalle.Text)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub clcCodigoPostal_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles clcCodigoPostal.Validating
        Dim oEvent As Dipros.Utils.Events
        If Accion <> Actions.Insert Or tecla_escape = System.Windows.Forms.Keys.Escape Then Exit Sub
        oEvent = oClientes.ValidaCp(clcCodigoPostal.Value)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            e.Cancel = True
        End If
    End Sub
    Private Sub cboDondeVive_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDondeVive.EditValueChanged
        If Me.TipoCasa = "P" Or Me.TipoCasa = "F" Then
            Me.clcPago.Enabled = False
            Me.clcPago.EditValue = 0
        Else
            Me.clcPago.Enabled = True
        End If
    End Sub

#End Region

#Region "Funcionalidad"
    Private Sub Motivo()

        Dim frmModal As New Comunes.frmClientesMotivoNoLocalizable
        With frmModal
            .OwnerForm = Me
            .MdiParent = FormaMain
            .Show()
        End With
        Me.Enabled = False
    End Sub
#End Region







End Class
