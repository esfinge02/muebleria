Imports Dipros.Utils

Public Class ucClientesAval
    Inherits System.Windows.Forms.UserControl

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents pnlAval As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lkpClienteAval As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents clcA�os_aval As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtParentesco_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtEstado_aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtCiudad_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblAval As System.Windows.Forms.Label
    Friend WithEvents txtAval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDomicilio_Aval As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia_Aval As System.Windows.Forms.Label
    Friend WithEvents txtColonia_Aval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCodigoPostal As System.Windows.Forms.Label
    Friend WithEvents lblExtension As System.Windows.Forms.Label
    Friend WithEvents txtNextel As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefonoNextel As System.Windows.Forms.Label
    Friend WithEvents lblTelCasa As System.Windows.Forms.Label
    Friend WithEvents txtTelCasa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelOficina As System.Windows.Forms.Label
    Friend WithEvents txtTelOficina As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelCelular As System.Windows.Forms.Label
    Friend WithEvents txtCelular As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblEstadoCivil As System.Windows.Forms.Label
    Friend WithEvents cboEstaCivil As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblCorreoAval As System.Windows.Forms.Label
    Friend WithEvents txtCorreoAval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents clcCodigoPostal As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcExtension As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.pnlAval = New System.Windows.Forms.Panel
        Me.clcExtension = New Dipros.Editors.TINCalcEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblCorreoAval = New System.Windows.Forms.Label
        Me.txtCorreoAval = New DevExpress.XtraEditors.TextEdit
        Me.lblEstadoCivil = New System.Windows.Forms.Label
        Me.cboEstaCivil = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblExtension = New System.Windows.Forms.Label
        Me.txtNextel = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefonoNextel = New System.Windows.Forms.Label
        Me.lblTelCasa = New System.Windows.Forms.Label
        Me.txtTelCasa = New DevExpress.XtraEditors.TextEdit
        Me.lblTelOficina = New System.Windows.Forms.Label
        Me.txtTelOficina = New DevExpress.XtraEditors.TextEdit
        Me.lblTelCelular = New System.Windows.Forms.Label
        Me.txtCelular = New DevExpress.XtraEditors.TextEdit
        Me.lblCodigoPostal = New System.Windows.Forms.Label
        Me.clcCodigoPostal = New Dipros.Editors.TINCalcEdit
        Me.Label19 = New System.Windows.Forms.Label
        Me.lkpClienteAval = New Dipros.Editors.TINMultiLookup
        Me.clcA�os_aval = New DevExpress.XtraEditors.CalcEdit
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtParentesco_Aval = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtEstado_aval = New DevExpress.XtraEditors.TextEdit
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtCiudad_Aval = New DevExpress.XtraEditors.TextEdit
        Me.lblAval = New System.Windows.Forms.Label
        Me.txtAval = New DevExpress.XtraEditors.TextEdit
        Me.lblDomicilio_Aval = New System.Windows.Forms.Label
        Me.txtDomicilio_Aval = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia_Aval = New System.Windows.Forms.Label
        Me.txtColonia_Aval = New DevExpress.XtraEditors.TextEdit
        Me.Label18 = New System.Windows.Forms.Label
        Me.pnlAval.SuspendLayout()
        CType(Me.clcExtension.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCorreoAval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstaCivil.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNextel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelCasa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelOficina.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCelular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCodigoPostal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcA�os_aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtParentesco_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado_aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia_Aval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlAval
        '
        Me.pnlAval.Controls.Add(Me.clcExtension)
        Me.pnlAval.Controls.Add(Me.lblObservaciones)
        Me.pnlAval.Controls.Add(Me.txtObservaciones)
        Me.pnlAval.Controls.Add(Me.lblCorreoAval)
        Me.pnlAval.Controls.Add(Me.txtCorreoAval)
        Me.pnlAval.Controls.Add(Me.lblEstadoCivil)
        Me.pnlAval.Controls.Add(Me.cboEstaCivil)
        Me.pnlAval.Controls.Add(Me.lblExtension)
        Me.pnlAval.Controls.Add(Me.txtNextel)
        Me.pnlAval.Controls.Add(Me.lblTelefonoNextel)
        Me.pnlAval.Controls.Add(Me.lblTelCasa)
        Me.pnlAval.Controls.Add(Me.txtTelCasa)
        Me.pnlAval.Controls.Add(Me.lblTelOficina)
        Me.pnlAval.Controls.Add(Me.txtTelOficina)
        Me.pnlAval.Controls.Add(Me.lblTelCelular)
        Me.pnlAval.Controls.Add(Me.txtCelular)
        Me.pnlAval.Controls.Add(Me.lblCodigoPostal)
        Me.pnlAval.Controls.Add(Me.clcCodigoPostal)
        Me.pnlAval.Controls.Add(Me.Label19)
        Me.pnlAval.Controls.Add(Me.lkpClienteAval)
        Me.pnlAval.Controls.Add(Me.clcA�os_aval)
        Me.pnlAval.Controls.Add(Me.Label17)
        Me.pnlAval.Controls.Add(Me.txtParentesco_Aval)
        Me.pnlAval.Controls.Add(Me.Label16)
        Me.pnlAval.Controls.Add(Me.txtEstado_aval)
        Me.pnlAval.Controls.Add(Me.Label15)
        Me.pnlAval.Controls.Add(Me.txtCiudad_Aval)
        Me.pnlAval.Controls.Add(Me.lblAval)
        Me.pnlAval.Controls.Add(Me.txtAval)
        Me.pnlAval.Controls.Add(Me.lblDomicilio_Aval)
        Me.pnlAval.Controls.Add(Me.txtDomicilio_Aval)
        Me.pnlAval.Controls.Add(Me.lblColonia_Aval)
        Me.pnlAval.Controls.Add(Me.txtColonia_Aval)
        Me.pnlAval.Controls.Add(Me.Label18)
        Me.pnlAval.Location = New System.Drawing.Point(0, 0)
        Me.pnlAval.Name = "pnlAval"
        Me.pnlAval.Size = New System.Drawing.Size(704, 336)
        Me.pnlAval.TabIndex = 8
        '
        'clcExtension
        '
        Me.clcExtension.EditValue = "0"
        Me.clcExtension.Location = New System.Drawing.Point(352, 118)
        Me.clcExtension.MaxValue = 0
        Me.clcExtension.MinValue = 0
        Me.clcExtension.Name = "clcExtension"
        '
        'clcExtension.Properties
        '
        Me.clcExtension.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExtension.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcExtension.Properties.MaskData.EditMask = "#"
        Me.clcExtension.Properties.MaxLength = 5
        Me.clcExtension.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcExtension.Size = New System.Drawing.Size(40, 19)
        Me.clcExtension.TabIndex = 15
        Me.clcExtension.Tag = ""
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(6, 272)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(83, 16)
        Me.lblObservaciones.TabIndex = 32
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "Observacione&s:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(96, 269)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservaciones.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservaciones.Size = New System.Drawing.Size(600, 43)
        Me.txtObservaciones.TabIndex = 33
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblCorreoAval
        '
        Me.lblCorreoAval.AutoSize = True
        Me.lblCorreoAval.Location = New System.Drawing.Point(40, 248)
        Me.lblCorreoAval.Name = "lblCorreoAval"
        Me.lblCorreoAval.Size = New System.Drawing.Size(42, 16)
        Me.lblCorreoAval.TabIndex = 30
        Me.lblCorreoAval.Tag = ""
        Me.lblCorreoAval.Text = "Correo:"
        Me.lblCorreoAval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCorreoAval
        '
        Me.txtCorreoAval.EditValue = ""
        Me.txtCorreoAval.Location = New System.Drawing.Point(88, 244)
        Me.txtCorreoAval.Name = "txtCorreoAval"
        '
        'txtCorreoAval.Properties
        '
        Me.txtCorreoAval.Properties.MaxLength = 80
        Me.txtCorreoAval.Size = New System.Drawing.Size(312, 20)
        Me.txtCorreoAval.TabIndex = 31
        Me.txtCorreoAval.Tag = "correo_aval"
        '
        'lblEstadoCivil
        '
        Me.lblEstadoCivil.AutoSize = True
        Me.lblEstadoCivil.Location = New System.Drawing.Point(24, 192)
        Me.lblEstadoCivil.Name = "lblEstadoCivil"
        Me.lblEstadoCivil.Size = New System.Drawing.Size(55, 16)
        Me.lblEstadoCivil.TabIndex = 24
        Me.lblEstadoCivil.Tag = ""
        Me.lblEstadoCivil.Text = "Edo. Civil:"
        Me.lblEstadoCivil.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstaCivil
        '
        Me.cboEstaCivil.EditValue = "S"
        Me.cboEstaCivil.Location = New System.Drawing.Point(88, 191)
        Me.cboEstaCivil.Name = "cboEstaCivil"
        '
        'cboEstaCivil.Properties
        '
        Me.cboEstaCivil.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstaCivil.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Casado (a)", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Soltero (a)", "S", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Otro", "O", -1)})
        Me.cboEstaCivil.Size = New System.Drawing.Size(120, 23)
        Me.cboEstaCivil.TabIndex = 25
        Me.cboEstaCivil.TabStop = False
        Me.cboEstaCivil.Tag = "estadocivil"
        '
        'lblExtension
        '
        Me.lblExtension.AutoSize = True
        Me.lblExtension.Location = New System.Drawing.Point(320, 117)
        Me.lblExtension.Name = "lblExtension"
        Me.lblExtension.Size = New System.Drawing.Size(27, 16)
        Me.lblExtension.TabIndex = 14
        Me.lblExtension.Tag = ""
        Me.lblExtension.Text = "Ex&t.:"
        Me.lblExtension.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNextel
        '
        Me.txtNextel.EditValue = ""
        Me.txtNextel.Location = New System.Drawing.Point(616, 117)
        Me.txtNextel.Name = "txtNextel"
        '
        'txtNextel.Properties
        '
        Me.txtNextel.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNextel.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtNextel.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtNextel.Properties.MaxLength = 10
        Me.txtNextel.Size = New System.Drawing.Size(72, 20)
        Me.txtNextel.TabIndex = 19
        Me.txtNextel.Tag = "nextel"
        '
        'lblTelefonoNextel
        '
        Me.lblTelefonoNextel.AutoSize = True
        Me.lblTelefonoNextel.Location = New System.Drawing.Point(552, 117)
        Me.lblTelefonoNextel.Name = "lblTelefonoNextel"
        Me.lblTelefonoNextel.Size = New System.Drawing.Size(65, 16)
        Me.lblTelefonoNextel.TabIndex = 18
        Me.lblTelefonoNextel.Tag = ""
        Me.lblTelefonoNextel.Text = "Tel. &Nextel.:"
        Me.lblTelefonoNextel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTelCasa
        '
        Me.lblTelCasa.AutoSize = True
        Me.lblTelCasa.Location = New System.Drawing.Point(24, 117)
        Me.lblTelCasa.Name = "lblTelCasa"
        Me.lblTelCasa.Size = New System.Drawing.Size(56, 16)
        Me.lblTelCasa.TabIndex = 10
        Me.lblTelCasa.Tag = ""
        Me.lblTelCasa.Text = "Tel. &Casa:"
        Me.lblTelCasa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelCasa
        '
        Me.txtTelCasa.EditValue = ""
        Me.txtTelCasa.Location = New System.Drawing.Point(88, 116)
        Me.txtTelCasa.Name = "txtTelCasa"
        '
        'txtTelCasa.Properties
        '
        Me.txtTelCasa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelCasa.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelCasa.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelCasa.Properties.MaxLength = 10
        Me.txtTelCasa.Size = New System.Drawing.Size(71, 20)
        Me.txtTelCasa.TabIndex = 11
        Me.txtTelCasa.Tag = ""
        '
        'lblTelOficina
        '
        Me.lblTelOficina.AutoSize = True
        Me.lblTelOficina.Location = New System.Drawing.Point(168, 117)
        Me.lblTelOficina.Name = "lblTelOficina"
        Me.lblTelOficina.Size = New System.Drawing.Size(65, 16)
        Me.lblTelOficina.TabIndex = 12
        Me.lblTelOficina.Tag = ""
        Me.lblTelOficina.Text = "Tel. &Oficina:"
        Me.lblTelOficina.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelOficina
        '
        Me.txtTelOficina.EditValue = ""
        Me.txtTelOficina.Location = New System.Drawing.Point(240, 117)
        Me.txtTelOficina.Name = "txtTelOficina"
        '
        'txtTelOficina.Properties
        '
        Me.txtTelOficina.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelOficina.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelOficina.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelOficina.Properties.MaxLength = 10
        Me.txtTelOficina.Size = New System.Drawing.Size(72, 20)
        Me.txtTelOficina.TabIndex = 13
        Me.txtTelOficina.Tag = ""
        '
        'lblTelCelular
        '
        Me.lblTelCelular.AutoSize = True
        Me.lblTelCelular.Location = New System.Drawing.Point(408, 117)
        Me.lblTelCelular.Name = "lblTelCelular"
        Me.lblTelCelular.Size = New System.Drawing.Size(49, 16)
        Me.lblTelCelular.TabIndex = 16
        Me.lblTelCelular.Tag = ""
        Me.lblTelCelular.Text = "Tel. &Cel.:"
        Me.lblTelCelular.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCelular
        '
        Me.txtCelular.EditValue = ""
        Me.txtCelular.Location = New System.Drawing.Point(464, 117)
        Me.txtCelular.Name = "txtCelular"
        '
        'txtCelular.Properties
        '
        Me.txtCelular.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCelular.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtCelular.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtCelular.Properties.MaxLength = 13
        Me.txtCelular.Size = New System.Drawing.Size(72, 20)
        Me.txtCelular.TabIndex = 17
        Me.txtCelular.Tag = "fax"
        '
        'lblCodigoPostal
        '
        Me.lblCodigoPostal.AutoSize = True
        Me.lblCodigoPostal.Location = New System.Drawing.Point(600, 72)
        Me.lblCodigoPostal.Name = "lblCodigoPostal"
        Me.lblCodigoPostal.Size = New System.Drawing.Size(22, 16)
        Me.lblCodigoPostal.TabIndex = 6
        Me.lblCodigoPostal.Tag = ""
        Me.lblCodigoPostal.Text = "Cp&:"
        Me.lblCodigoPostal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCodigoPostal
        '
        Me.clcCodigoPostal.EditValue = "0"
        Me.clcCodigoPostal.Location = New System.Drawing.Point(632, 67)
        Me.clcCodigoPostal.MaxValue = 0
        Me.clcCodigoPostal.MinValue = 0
        Me.clcCodigoPostal.Name = "clcCodigoPostal"
        '
        'clcCodigoPostal.Properties
        '
        Me.clcCodigoPostal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCodigoPostal.Properties.MaskData.EditMask = "#"
        Me.clcCodigoPostal.Properties.MaxLength = 5
        Me.clcCodigoPostal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCodigoPostal.Size = New System.Drawing.Size(56, 19)
        Me.clcCodigoPostal.TabIndex = 7
        Me.clcCodigoPostal.Tag = ""
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(37, 19)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(44, 16)
        Me.Label19.TabIndex = 0
        Me.Label19.Tag = ""
        Me.Label19.Text = "Cuenta:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpClienteAval
        '
        Me.lkpClienteAval.AllowAdd = False
        Me.lkpClienteAval.AutoReaload = False
        Me.lkpClienteAval.DataSource = Nothing
        Me.lkpClienteAval.DefaultSearchField = ""
        Me.lkpClienteAval.DisplayMember = "cliente"
        Me.lkpClienteAval.EditValue = Nothing
        Me.lkpClienteAval.Filtered = False
        Me.lkpClienteAval.InitValue = Nothing
        Me.lkpClienteAval.Location = New System.Drawing.Point(89, 16)
        Me.lkpClienteAval.MultiSelect = False
        Me.lkpClienteAval.Name = "lkpClienteAval"
        Me.lkpClienteAval.NullText = ""
        Me.lkpClienteAval.PopupWidth = CType(520, Long)
        Me.lkpClienteAval.ReadOnlyControl = False
        Me.lkpClienteAval.Required = False
        Me.lkpClienteAval.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteAval.SearchMember = ""
        Me.lkpClienteAval.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteAval.SelectAll = False
        Me.lkpClienteAval.Size = New System.Drawing.Size(152, 20)
        Me.lkpClienteAval.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteAval.TabIndex = 1
        Me.lkpClienteAval.Tag = "cliente_aval"
        Me.lkpClienteAval.ToolTip = Nothing
        Me.lkpClienteAval.ValueMember = "Cliente"
        '
        'clcA�os_aval
        '
        Me.clcA�os_aval.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcA�os_aval.Location = New System.Drawing.Point(616, 192)
        Me.clcA�os_aval.Name = "clcA�os_aval"
        Me.clcA�os_aval.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcA�os_aval.Size = New System.Drawing.Size(75, 20)
        Me.clcA�os_aval.TabIndex = 27
        Me.clcA�os_aval.Tag = "anios_aval"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(16, 224)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(65, 16)
        Me.Label17.TabIndex = 28
        Me.Label17.Tag = ""
        Me.Label17.Text = "Parentesco:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtParentesco_Aval
        '
        Me.txtParentesco_Aval.EditValue = ""
        Me.txtParentesco_Aval.Location = New System.Drawing.Point(89, 219)
        Me.txtParentesco_Aval.Name = "txtParentesco_Aval"
        '
        'txtParentesco_Aval.Properties
        '
        Me.txtParentesco_Aval.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtParentesco_Aval.Properties.MaxLength = 50
        Me.txtParentesco_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtParentesco_Aval.TabIndex = 29
        Me.txtParentesco_Aval.Tag = "parentesco_aval"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(39, 168)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(43, 16)
        Me.Label16.TabIndex = 22
        Me.Label16.Tag = ""
        Me.Label16.Text = "Estado:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEstado_aval
        '
        Me.txtEstado_aval.EditValue = ""
        Me.txtEstado_aval.Location = New System.Drawing.Point(89, 166)
        Me.txtEstado_aval.Name = "txtEstado_aval"
        '
        'txtEstado_aval.Properties
        '
        Me.txtEstado_aval.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstado_aval.Properties.MaxLength = 50
        Me.txtEstado_aval.Size = New System.Drawing.Size(300, 20)
        Me.txtEstado_aval.TabIndex = 23
        Me.txtEstado_aval.Tag = "estado_aval"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(38, 144)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 16)
        Me.Label15.TabIndex = 20
        Me.Label15.Tag = ""
        Me.Label15.Text = "Ciudad:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCiudad_Aval
        '
        Me.txtCiudad_Aval.EditValue = ""
        Me.txtCiudad_Aval.Location = New System.Drawing.Point(89, 141)
        Me.txtCiudad_Aval.Name = "txtCiudad_Aval"
        '
        'txtCiudad_Aval.Properties
        '
        Me.txtCiudad_Aval.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudad_Aval.Properties.MaxLength = 50
        Me.txtCiudad_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtCiudad_Aval.TabIndex = 21
        Me.txtCiudad_Aval.Tag = "ciudad_aval"
        '
        'lblAval
        '
        Me.lblAval.AutoSize = True
        Me.lblAval.Location = New System.Drawing.Point(32, 44)
        Me.lblAval.Name = "lblAval"
        Me.lblAval.Size = New System.Drawing.Size(48, 16)
        Me.lblAval.TabIndex = 2
        Me.lblAval.Tag = ""
        Me.lblAval.Text = "Nombre:"
        Me.lblAval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAval
        '
        Me.txtAval.EditValue = ""
        Me.txtAval.Location = New System.Drawing.Point(89, 41)
        Me.txtAval.Name = "txtAval"
        '
        'txtAval.Properties
        '
        Me.txtAval.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAval.Properties.MaxLength = 100
        Me.txtAval.Size = New System.Drawing.Size(600, 20)
        Me.txtAval.TabIndex = 3
        Me.txtAval.Tag = "aval"
        '
        'lblDomicilio_Aval
        '
        Me.lblDomicilio_Aval.AutoSize = True
        Me.lblDomicilio_Aval.Location = New System.Drawing.Point(26, 69)
        Me.lblDomicilio_Aval.Name = "lblDomicilio_Aval"
        Me.lblDomicilio_Aval.Size = New System.Drawing.Size(54, 16)
        Me.lblDomicilio_Aval.TabIndex = 4
        Me.lblDomicilio_Aval.Tag = ""
        Me.lblDomicilio_Aval.Text = "Domici&lio:"
        Me.lblDomicilio_Aval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio_Aval
        '
        Me.txtDomicilio_Aval.EditValue = ""
        Me.txtDomicilio_Aval.Location = New System.Drawing.Point(89, 66)
        Me.txtDomicilio_Aval.Name = "txtDomicilio_Aval"
        '
        'txtDomicilio_Aval.Properties
        '
        Me.txtDomicilio_Aval.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDomicilio_Aval.Properties.MaxLength = 50
        Me.txtDomicilio_Aval.Size = New System.Drawing.Size(479, 20)
        Me.txtDomicilio_Aval.TabIndex = 5
        Me.txtDomicilio_Aval.Tag = "domicilio_aval"
        '
        'lblColonia_Aval
        '
        Me.lblColonia_Aval.AutoSize = True
        Me.lblColonia_Aval.Location = New System.Drawing.Point(35, 94)
        Me.lblColonia_Aval.Name = "lblColonia_Aval"
        Me.lblColonia_Aval.Size = New System.Drawing.Size(46, 16)
        Me.lblColonia_Aval.TabIndex = 8
        Me.lblColonia_Aval.Tag = ""
        Me.lblColonia_Aval.Text = "Colonia:"
        Me.lblColonia_Aval.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtColonia_Aval
        '
        Me.txtColonia_Aval.EditValue = ""
        Me.txtColonia_Aval.Location = New System.Drawing.Point(89, 91)
        Me.txtColonia_Aval.Name = "txtColonia_Aval"
        '
        'txtColonia_Aval.Properties
        '
        Me.txtColonia_Aval.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtColonia_Aval.Properties.MaxLength = 50
        Me.txtColonia_Aval.Size = New System.Drawing.Size(300, 20)
        Me.txtColonia_Aval.TabIndex = 9
        Me.txtColonia_Aval.Tag = "colonia_aval"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(312, 195)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(282, 16)
        Me.Label18.TabIndex = 26
        Me.Label18.Tag = ""
        Me.Label18.Text = "En caso de ser conocido, tiempo de conocerlo en a�os:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ucClientesAval
        '
        Me.Controls.Add(Me.pnlAval)
        Me.Name = "ucClientesAval"
        Me.Size = New System.Drawing.Size(704, 336)
        Me.pnlAval.ResumeLayout(False)
        CType(Me.clcExtension.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCorreoAval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstaCivil.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNextel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelCasa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelOficina.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCelular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCodigoPostal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcA�os_aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtParentesco_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado_aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia_Aval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oClientes As New VillarrealBusiness.clsClientes
    Private ClienteID As Long = 0

#Region "Propiedades"




    Public Property Cliente() As Long
        Get
            Return ClienteID
        End Get
        Set(ByVal Value As Long)
            ClienteID = Value
        End Set
    End Property

    Public Property ClienteAval() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteAval)
        End Get
        Set(ByVal Value As Long)
            Me.lkpClienteAval.EditValue = Value
        End Set
    End Property

    Public Property NombreAval() As String
        Get
            Return Me.txtAval.Text
        End Get
        Set(ByVal Value As String)
            Me.txtAval.Text = Value
        End Set
    End Property

    Public Property DomicilioAval() As String
        Get
            Return Me.txtDomicilio_Aval.Text
        End Get
        Set(ByVal Value As String)
            Me.txtDomicilio_Aval.Text = Value
        End Set
    End Property

    Public Property ColoniaAval() As String
        Get
            Return Me.txtColonia_Aval.Text
        End Get
        Set(ByVal Value As String)
            Me.txtColonia_Aval.Text = Value
        End Set
    End Property

    Public Property CodigoPostal() As Long
        Get
            Return Me.clcCodigoPostal.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcCodigoPostal.EditValue = Value
        End Set
    End Property

    Public Property TelefonoCasa() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelCasa.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTelCasa.Text = Value
        End Set
    End Property

    Public Property TelefonoOficina() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelOficina.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtTelOficina.Text = Value
        End Set
    End Property

    Public Property Extension() As Long
        Get
            Return Me.clcExtension.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcExtension.Text = Value
        End Set
    End Property

    Public Property TelefonoCelular() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtCelular.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtCelular.Text = Value
        End Set
    End Property

    Public Property TelefonoNextel() As String
        Get
            Return Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtNextel.Text)
        End Get
        Set(ByVal Value As String)
            Me.txtNextel.Text = Value
        End Set
    End Property

    Public Property CiudadAval() As String
        Get
            Return Me.txtCiudad_Aval.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCiudad_Aval.Text = Value
        End Set
    End Property

    Public Property EstadoAval() As String
        Get
            Return Me.txtEstado_aval.Text
        End Get
        Set(ByVal Value As String)
            Me.txtEstado_aval.Text = Value
        End Set
    End Property

    Public Property EstadoCivil() As String
        Get
            Return Me.cboEstaCivil.Text
        End Get
        Set(ByVal Value As String)
            Me.cboEstaCivil.Text = Value
        End Set
    End Property

    Public Property TiempoConocerlo() As String
        Get
            Return Me.clcA�os_aval.Text
        End Get
        Set(ByVal Value As String)
            Me.clcA�os_aval.Text = Value
        End Set
    End Property

    Public Property ParentescoAval() As String
        Get
            Return Me.txtParentesco_Aval.Text
        End Get
        Set(ByVal Value As String)
            Me.txtParentesco_Aval.Text = Value
        End Set
    End Property

    Public Property CorreoAval() As String
        Get
            Return Me.txtCorreoAval.Text
        End Get
        Set(ByVal Value As String)
            Me.txtCorreoAval.Text = Value
        End Set
    End Property

    Public Property Observaciones() As String
        Get
            Return Me.txtObservaciones.Text
        End Get
        Set(ByVal Value As String)
            Me.txtObservaciones.Text = Value
        End Set
    End Property

    Public Property AniosConocerlo() As Long
        Get
            Return Me.clcA�os_aval.Value
        End Get
        Set(ByVal Value As Long)
            Me.clcA�os_aval.EditValue = Value
        End Set
    End Property

#End Region

    Private Sub lkpClienteAval_Format() Handles lkpClienteAval.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteAval)
    End Sub

    Private Sub lkpClienteAval_LoadData(ByVal Initialize As Boolean) Handles lkpClienteAval.LoadData

        Dim Response As New Events
        Response = oClientes.LookupAvalClienteExcluido(Cliente)
        'Response = oClientes.LookupRepartoClienteExcluido(, Cliente)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteAval.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If

        Response = Nothing

    End Sub
    Private Sub lkpClienteAval_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpClienteAval.EditValueChanged
        If ClienteAval = -1 Then
            Me.txtAval.Enabled = True
            Me.txtDomicilio_Aval.Enabled = True
            Me.txtColonia_Aval.Enabled = True
            Me.txtCiudad_Aval.Enabled = True
            Me.txtEstado_aval.Enabled = True
            Me.txtTelCasa.Enabled = True

            Me.txtAval.Text = ""
            Me.txtDomicilio_Aval.Text = ""
            Me.txtColonia_Aval.Text = ""
            Me.txtCiudad_Aval.Text = ""
            Me.txtEstado_aval.Text = ""
            Me.txtParentesco_Aval.Text = ""
            Me.txtTelCasa.Text = ""

        Else
            Me.txtAval.Enabled = False
            Me.txtDomicilio_Aval.Enabled = False
            Me.txtColonia_Aval.Enabled = False
            Me.txtCiudad_Aval.Enabled = False
            Me.txtEstado_aval.Enabled = False
            Me.txtTelCasa.Enabled = False

            Me.txtAval.Text = Me.lkpClienteAval.GetValue("nombre")
            Me.txtDomicilio_Aval.Text = Me.lkpClienteAval.GetValue("direccion_aval_resultado")
            Me.txtColonia_Aval.Text = Me.lkpClienteAval.GetValue("nombre_colonia")
            Me.txtCiudad_Aval.Text = Me.lkpClienteAval.GetValue("ciudad")
            Me.txtEstado_aval.Text = Me.lkpClienteAval.GetValue("nombre_estado")
            Me.txtTelCasa.Text = Me.lkpClienteAval.GetValue("telefono1")
        End If
    End Sub



End Class
