Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class ucSolicitaFormaPago
    Inherits System.Windows.Forms.UserControl

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Private WithEvents lkpFormasPago As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFolioVale As System.Windows.Forms.Label
    Private WithEvents txtUltimosDigitos As DevExpress.XtraEditors.TextEdit
    Private WithEvents clcFolioVale As DevExpress.XtraEditors.CalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblFolioVale = New System.Windows.Forms.Label
        Me.clcFolioVale = New DevExpress.XtraEditors.CalcEdit
        Me.txtUltimosDigitos = New DevExpress.XtraEditors.TextEdit
        Me.Label33 = New System.Windows.Forms.Label
        Me.lkpFormasPago = New Dipros.Editors.TINMultiLookup
        Me.GroupBox1.SuspendLayout()
        CType(Me.clcFolioVale.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUltimosDigitos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Tag = ""
        Me.Label2.Text = "Forma Pago:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblFolioVale)
        Me.GroupBox1.Controls.Add(Me.clcFolioVale)
        Me.GroupBox1.Controls.Add(Me.txtUltimosDigitos)
        Me.GroupBox1.Controls.Add(Me.Label33)
        Me.GroupBox1.Controls.Add(Me.lkpFormasPago)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(320, 80)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Forma de Pago"
        '
        'lblFolioVale
        '
        Me.lblFolioVale.AutoSize = True
        Me.lblFolioVale.Location = New System.Drawing.Point(168, 52)
        Me.lblFolioVale.Name = "lblFolioVale"
        Me.lblFolioVale.Size = New System.Drawing.Size(58, 16)
        Me.lblFolioVale.TabIndex = 88
        Me.lblFolioVale.Tag = ""
        Me.lblFolioVale.Text = "Folio Vale:"
        Me.lblFolioVale.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolioVale
        '
        Me.clcFolioVale.Location = New System.Drawing.Point(232, 50)
        Me.clcFolioVale.Name = "clcFolioVale"
        '
        'clcFolioVale.Properties
        '
        Me.clcFolioVale.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioVale.Size = New System.Drawing.Size(75, 20)
        Me.clcFolioVale.TabIndex = 87
        Me.clcFolioVale.Tag = "folio_vale"
        '
        'txtUltimosDigitos
        '
        Me.txtUltimosDigitos.EditValue = ""
        Me.txtUltimosDigitos.Location = New System.Drawing.Point(104, 50)
        Me.txtUltimosDigitos.Name = "txtUltimosDigitos"
        '
        'txtUltimosDigitos.Properties
        '
        Me.txtUltimosDigitos.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltimosDigitos.Properties.MaxLength = 4
        Me.txtUltimosDigitos.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtUltimosDigitos.Size = New System.Drawing.Size(60, 20)
        Me.txtUltimosDigitos.TabIndex = 86
        Me.txtUltimosDigitos.Tag = "ultimos_digitos_cuenta"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(8, 50)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(83, 16)
        Me.Label33.TabIndex = 85
        Me.Label33.Tag = ""
        Me.Label33.Text = "Ultimos D�gitos:"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFormasPago
        '
        Me.lkpFormasPago.AllowAdd = False
        Me.lkpFormasPago.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lkpFormasPago.AutoReaload = False
        Me.lkpFormasPago.DataSource = Nothing
        Me.lkpFormasPago.DefaultSearchField = ""
        Me.lkpFormasPago.DisplayMember = "descripcion"
        Me.lkpFormasPago.EditValue = Nothing
        Me.lkpFormasPago.Filtered = False
        Me.lkpFormasPago.InitValue = Nothing
        Me.lkpFormasPago.Location = New System.Drawing.Point(104, 26)
        Me.lkpFormasPago.MultiSelect = False
        Me.lkpFormasPago.Name = "lkpFormasPago"
        Me.lkpFormasPago.NullText = ""
        Me.lkpFormasPago.PopupWidth = CType(400, Long)
        Me.lkpFormasPago.ReadOnlyControl = False
        Me.lkpFormasPago.Required = False
        Me.lkpFormasPago.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFormasPago.SearchMember = ""
        Me.lkpFormasPago.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFormasPago.SelectAll = True
        Me.lkpFormasPago.Size = New System.Drawing.Size(208, 20)
        Me.lkpFormasPago.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFormasPago.TabIndex = 83
        Me.lkpFormasPago.Tag = "forma_pago"
        Me.lkpFormasPago.ToolTip = "Seleccione una Forma de Pago"
        Me.lkpFormasPago.ValueMember = "forma_pago"
        '
        'ucSolicitaFormaPago
        '
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "ucSolicitaFormaPago"
        Me.Size = New System.Drawing.Size(320, 80)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.clcFolioVale.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUltimosDigitos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oformasPago As New VillarrealBusiness.clsFormasPagos
    Private Solicita_Ultimos_Digitos As Boolean = False
    Private Solicita_Folio_Vale As Boolean = False
    Private Maneja_Dolares As Boolean = False
    Private KeyAscii As Short

    Public ReadOnly Property FormaPago() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFormasPago)
        End Get
    End Property
    Public ReadOnly Property SolicitaUltimosDigitos() As Boolean
        Get
            Return Solicita_Ultimos_Digitos
        End Get
    End Property
    Public ReadOnly Property SolicitaFolioVale() As Boolean
        Get
            Return Solicita_Folio_Vale
        End Get
    End Property
    Public ReadOnly Property UltimosDigitos() As String
        Get
            Return Me.txtUltimosDigitos.Text
        End Get
    End Property
    Public ReadOnly Property ManejaDolares() As Boolean
        Get
            Return Maneja_Dolares
        End Get
    End Property
    Public ReadOnly Property FolioVale() As Long
        Get
            Return Me.clcFolioVale.EditValue
        End Get
    End Property

    Public WriteOnly Property SizeAll() As Single
        Set(ByVal Value As Single)
            Me.Label2.Font = New System.Drawing.Font(Label2.Font.Name, Value, Label2.Font.Style, Label2.Font.Unit)
            Me.Label33.Font = New System.Drawing.Font(Label33.Font.Name, Value, Label33.Font.Style, Label33.Font.Unit)
            lkpFormasPago.Font = New System.Drawing.Font(lkpFormasPago.Font.Name, Value, lkpFormasPago.Font.Style, lkpFormasPago.Font.Unit)
            Me.txtUltimosDigitos.Font = New System.Drawing.Font(Me.txtUltimosDigitos.Font.Name, Value, txtUltimosDigitos.Font.Style, txtUltimosDigitos.Font.Unit)
        End Set
    End Property
    Public WriteOnly Property EnabledAll() As Boolean
        Set(ByVal Value As Boolean)
            Me.lkpFormasPago.Enabled = Value
            Me.txtUltimosDigitos.Enabled = Value

            If Value = False Then
                ClearAll()
            End If
        End Set
    End Property





    Private Sub lkpFormasPago_Format() Handles lkpFormasPago.Format
        Comunes.clsFormato.for_formas_pagos_grl(Me.lkpFormasPago)
    End Sub
    Private Sub lkpFormasPago_LoadData(ByVal Initialize As Boolean) Handles lkpFormasPago.LoadData
        Dim Response As New Events
        Response = oformasPago.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpFormasPago.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpFormasPago_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpFormasPago.EditValueChanged
        If Me.FormaPago > 0 Then
            Solicita_Ultimos_Digitos = Me.lkpFormasPago.GetValue("solicitar_ulitmos_digitos")
            Maneja_Dolares = Me.lkpFormasPago.GetValue("maneja_dolares")
            Solicita_Folio_Vale = Me.lkpFormasPago.GetValue("solicita_folio_vale")

            If Solicita_Ultimos_Digitos = True Then
                Me.txtUltimosDigitos.Enabled = True               
            Else
                Me.txtUltimosDigitos.Text = ""
                Me.txtUltimosDigitos.Enabled = False
            End If

            If Me.Solicita_Folio_Vale Then
                Me.clcFolioVale.Enabled = True
            Else
                Me.clcFolioVale.Enabled = False
                Me.clcFolioVale.EditValue = 0
            End If
        End If
    End Sub
    Private Sub txtUltimosDigitos_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUltimosDigitos.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If

    End Sub


    Private Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub ClearAll()
        Me.lkpFormasPago.EditValue = Nothing
        Me.txtUltimosDigitos.Text = ""
    End Sub





End Class
