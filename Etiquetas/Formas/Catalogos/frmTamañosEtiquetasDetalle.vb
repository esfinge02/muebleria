Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmTamañosEtiquetasDetalle
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
    Friend WithEvents lblPlantilla As System.Windows.Forms.Label
    Friend WithEvents lkpPlantilla As Dipros.Editors.TINMultiLookup

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTamañosEtiquetasDetalle))
        Me.lblPlantilla = New System.Windows.Forms.Label
        Me.lkpPlantilla = New Dipros.Editors.TINMultiLookup
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(98, 50)
        '
        'lblPlantilla
        '
        Me.lblPlantilla.AutoSize = True
        Me.lblPlantilla.Location = New System.Drawing.Point(16, 40)
        Me.lblPlantilla.Name = "lblPlantilla"
        Me.lblPlantilla.Size = New System.Drawing.Size(52, 16)
        Me.lblPlantilla.TabIndex = 2
        Me.lblPlantilla.Tag = ""
        Me.lblPlantilla.Text = "&Plantilla:"
        Me.lblPlantilla.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPlantilla
        '
        Me.lkpPlantilla.AutoReaload = False
        Me.lkpPlantilla.DataSource = Nothing
        Me.lkpPlantilla.DefaultSearchField = ""
        Me.lkpPlantilla.DisplayMember = "nombre"
        Me.lkpPlantilla.EditValue = Nothing
        Me.lkpPlantilla.InitValue = Nothing
        Me.lkpPlantilla.Location = New System.Drawing.Point(72, 40)
        Me.lkpPlantilla.MultiSelect = False
        Me.lkpPlantilla.Name = "lkpPlantilla"
        Me.lkpPlantilla.NullText = ""
        Me.lkpPlantilla.PopupWidth = CType(300, Long)
        Me.lkpPlantilla.Required = True
        Me.lkpPlantilla.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlantilla.SearchMember = ""
        Me.lkpPlantilla.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlantilla.SelectAll = True
        Me.lkpPlantilla.Size = New System.Drawing.Size(200, 20)
        Me.lkpPlantilla.TabIndex = 59
        Me.lkpPlantilla.Tag = "plantilla"
        Me.lkpPlantilla.ToolTip = Nothing
        Me.lkpPlantilla.ValueMember = "nombre"
        '
        'frmTamañosEtiquetasDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(282, 72)
        Me.Controls.Add(Me.lkpPlantilla)
        Me.Controls.Add(Me.lblPlantilla)
        Me.Name = "frmTamañosEtiquetasDetalle"
        Me.Controls.SetChildIndex(Me.lblPlantilla, 0)
        Me.Controls.SetChildIndex(Me.lkpPlantilla, 0)
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oVariables As VillarrealBusiness.clsVariables
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
	Private Sub frmTamañosEtiquetasDetalle_Accept(ByRef Response As Events) Handles MyBase.Accept
		With OwnerForm.MasterControl
	            Select Case Action
        	        Case Actions.Insert
                	    .AddRow(Me.DataSource)
	                Case Actions.Update
	                    .UpdateRow(Me.DataSource)
        	        Case Actions.Delete
                	    .DeleteRow()
        	    End Select
	        End With
	End Sub

	Private Sub frmTamañosEtiquetasDetalle_DisplayFields(ByRef Response As Events) Handles MyBase.DisplayFields
	        Me.DataSource = OwnerForm.MasterControl.SelectedRow
	End Sub

	Private Sub frmTamañosEtiquetasDetalle_Initialize(ByRef Response As Events) Handles MyBase.Initialize
        oVariables = New VillarrealBusiness.clsVariables

		Select Case Action
			Case Actions.Insert
			Case Actions.Update
			Case Actions.Delete
		End Select
	End Sub

	Private Sub frmTamañosEtiquetasDetalle_ValidateFields(ByRef Response As Events) Handles MyBase.ValidateFields

	End Sub

	Private Sub frmTamañosEtiquetasDetalle_Localize() Handles MyBase.Localize
		Find("Unknow", CType("Replace by a control", Object))

	End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpPlantilla_Format() Handles lkpPlantilla.Format
        Comunes.clsFormato.for_plantilla_reportes_grl(Me.lkpPlantilla)
    End Sub
    Private Sub lkpPlantilla_LoadData(ByVal Initialize As Boolean) Handles lkpPlantilla.LoadData
        Dim Response As New Events
        Response = oVariables.LookupPlantillasReportesEtiquetasPreciosSinAsignar()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlantilla.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

End Class
