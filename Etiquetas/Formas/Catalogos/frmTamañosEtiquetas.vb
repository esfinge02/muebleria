Imports Dipros.Utils.Common

Public Class frmTamañosEtiquetas
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblTamaño As System.Windows.Forms.Label
		Friend WithEvents clcTamaño As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grPlantillas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvOrdenesCompra As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcPlantilla As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaPlantillas As Dipros.Windows.TINMaster
    Friend WithEvents chkAlmacen As DevExpress.XtraEditors.CheckEdit

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTamañosEtiquetas))
        Me.lblTamaño = New System.Windows.Forms.Label
        Me.clcTamaño = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.grPlantillas = New DevExpress.XtraGrid.GridControl
        Me.grvOrdenesCompra = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPlantilla = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaPlantillas = New Dipros.Windows.TINMaster
        Me.chkAlmacen = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcTamaño.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grPlantillas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAlmacen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblTamaño
        '
        Me.lblTamaño.AutoSize = True
        Me.lblTamaño.Location = New System.Drawing.Point(35, 40)
        Me.lblTamaño.Name = "lblTamaño"
        Me.lblTamaño.Size = New System.Drawing.Size(53, 16)
        Me.lblTamaño.TabIndex = 0
        Me.lblTamaño.Tag = ""
        Me.lblTamaño.Text = "&Tamaño:"
        Me.lblTamaño.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTamaño
        '
        Me.clcTamaño.EditValue = "0"
        Me.clcTamaño.Location = New System.Drawing.Point(96, 40)
        Me.clcTamaño.MaxValue = 0
        Me.clcTamaño.MinValue = 0
        Me.clcTamaño.Name = "clcTamaño"
        '
        'clcTamaño.Properties
        '
        Me.clcTamaño.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcTamaño.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTamaño.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcTamaño.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTamaño.Properties.Enabled = False
        Me.clcTamaño.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTamaño.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTamaño.Size = New System.Drawing.Size(64, 19)
        Me.clcTamaño.TabIndex = 1
        Me.clcTamaño.Tag = "tamanio"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(16, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(96, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 30
        Me.txtDescripcion.Size = New System.Drawing.Size(180, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'grPlantillas
        '
        '
        'grPlantillas.EmbeddedNavigator
        '
        Me.grPlantillas.EmbeddedNavigator.Name = ""
        Me.grPlantillas.Location = New System.Drawing.Point(16, 120)
        Me.grPlantillas.MainView = Me.grvOrdenesCompra
        Me.grPlantillas.Name = "grPlantillas"
        Me.grPlantillas.Size = New System.Drawing.Size(256, 136)
        Me.grPlantillas.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grPlantillas.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPlantillas.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPlantillas.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPlantillas.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPlantillas.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grPlantillas.TabIndex = 5
        Me.grPlantillas.TabStop = False
        Me.grPlantillas.Text = "Articulos"
        '
        'grvOrdenesCompra
        '
        Me.grvOrdenesCompra.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPlantilla})
        Me.grvOrdenesCompra.GridControl = Me.grPlantillas
        Me.grvOrdenesCompra.Name = "grvOrdenesCompra"
        Me.grvOrdenesCompra.OptionsBehavior.Editable = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowFilter = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowGroup = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowSort = False
        Me.grvOrdenesCompra.OptionsView.ShowGroupPanel = False
        '
        'grcPlantilla
        '
        Me.grcPlantilla.Caption = "Plantilla"
        Me.grcPlantilla.FieldName = "plantilla"
        Me.grcPlantilla.Name = "grcPlantilla"
        Me.grcPlantilla.VisibleIndex = 0
        Me.grcPlantilla.Width = 227
        '
        'tmaPlantillas
        '
        Me.tmaPlantillas.BackColor = System.Drawing.Color.White
        Me.tmaPlantillas.CanDelete = True
        Me.tmaPlantillas.CanInsert = True
        Me.tmaPlantillas.CanUpdate = False
        Me.tmaPlantillas.Grid = Me.grPlantillas
        Me.tmaPlantillas.Location = New System.Drawing.Point(16, 96)
        Me.tmaPlantillas.Name = "tmaPlantillas"
        Me.tmaPlantillas.Size = New System.Drawing.Size(256, 23)
        Me.tmaPlantillas.TabIndex = 59
        Me.tmaPlantillas.TabStop = False
        Me.tmaPlantillas.Title = ""
        Me.tmaPlantillas.UpdateTitle = "un Registro"
        '
        'chkAlmacen
        '
        Me.chkAlmacen.Location = New System.Drawing.Point(176, 40)
        Me.chkAlmacen.Name = "chkAlmacen"
        '
        'chkAlmacen.Properties
        '
        Me.chkAlmacen.Properties.Caption = "Entrada Almacen"
        Me.chkAlmacen.Size = New System.Drawing.Size(120, 19)
        Me.chkAlmacen.TabIndex = 60
        Me.chkAlmacen.Tag = "entrada_almacen"
        '
        'frmTamañosEtiquetas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(282, 264)
        Me.Controls.Add(Me.chkAlmacen)
        Me.Controls.Add(Me.tmaPlantillas)
        Me.Controls.Add(Me.grPlantillas)
        Me.Controls.Add(Me.lblTamaño)
        Me.Controls.Add(Me.clcTamaño)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmTamañosEtiquetas"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcTamaño, 0)
        Me.Controls.SetChildIndex(Me.lblTamaño, 0)
        Me.Controls.SetChildIndex(Me.grPlantillas, 0)
        Me.Controls.SetChildIndex(Me.tmaPlantillas, 0)
        Me.Controls.SetChildIndex(Me.chkAlmacen, 0)
        CType(Me.clcTamaño.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grPlantillas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAlmacen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oTamañosEtiquetas As VillarrealBusiness.clsTamaniosEtiquetas
    Private oTamañosEtiquetasDetalle As VillarrealBusiness.clsTamaniosEtiquetasDetalle
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmTamañosEtiquetas_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmTamañosEtiquetas_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmTamañosEtiquetas_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmTamañosEtiquetas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oTamañosEtiquetas.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oTamañosEtiquetas.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oTamañosEtiquetas.Eliminar(clcTamaño.Value)

        End Select
    End Sub
    Private Sub frmTamañosEtiquetas_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        With Me.tmaPlantillas
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        If Not Response.ErrorFound Then Response = oTamañosEtiquetasDetalle.Insertar(.SelectedRow, Me.clcTamaño.EditValue)
                    Case Actions.Delete
                        If Not Response.ErrorFound Then Response = oTamañosEtiquetasDetalle.Eliminar(Me.clcTamaño.EditValue, .Item("plantilla"))
                End Select
                .MoveNext()
            Loop

        End With
    End Sub
    Private Sub frmTamañosEtiquetas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oTamañosEtiquetas.DespliegaDatos(OwnerForm.Value("tamanio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

        If Not Response.ErrorFound Then Response = oTamañosEtiquetasDetalle.Listado(Me.clcTamaño.EditValue)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(Response.Value, DataSet)
            Me.tmaPlantillas.DataSource = oDataSet
        End If


    End Sub
    Private Sub frmTamañosEtiquetas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oTamañosEtiquetas = New VillarrealBusiness.clsTamaniosEtiquetas
        oTamañosEtiquetasDetalle = New VillarrealBusiness.clsTamaniosEtiquetasDetalle

        With Me.tmaPlantillas
            .UpdateTitle = "una Plantilla"
            .UpdateForm = New frmTamañosEtiquetasDetalle
            .AddColumn("plantilla")
        End With

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmTamañosEtiquetas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oTamañosEtiquetas.Validacion(Action, Me.txtDescripcion.Text)
        If Not Response.ErrorFound Then Response = oTamañosEtiquetasDetalle.Validacion(Action, CType(Me.tmaPlantillas.DataSource, DataSet))
    End Sub

    Private Sub frmTamañosEtiquetas_Localize() Handles MyBase.Localize
        Find("tamanio", CType(Me.clcTamaño.EditValue, Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region




End Class
