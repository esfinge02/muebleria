Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Código generado por el Diseñador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicialización después de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código.
    Friend WithEvents mnuCatTamañosEtiquetas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatEtiquetas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepImpresionEtiquetas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuUtiVariables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatTamañosEtiquetas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatEtiquetas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepImpresionEtiquetas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuUtiVariables = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiVariables, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatTamañosEtiquetas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatEtiquetas)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepImpresionEtiquetas)})
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatTamañosEtiquetas, Me.mnuCatEtiquetas, Me.mnuRepImpresionEtiquetas, Me.mnuUtiVariables, Me.BarStaticItem1})
        Me.BarManager.MaxItemId = 110
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'mnuCatTamañosEtiquetas
        '
        Me.mnuCatTamañosEtiquetas.Caption = "&Tamaños de Etiquetas"
        Me.mnuCatTamañosEtiquetas.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatTamañosEtiquetas.Id = 105
        Me.mnuCatTamañosEtiquetas.ImageIndex = 0
        Me.mnuCatTamañosEtiquetas.Name = "mnuCatTamañosEtiquetas"
        '
        'mnuCatEtiquetas
        '
        Me.mnuCatEtiquetas.Caption = "C&onfiguración de Etiquetas"
        Me.mnuCatEtiquetas.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatEtiquetas.Id = 106
        Me.mnuCatEtiquetas.ImageIndex = 1
        Me.mnuCatEtiquetas.Name = "mnuCatEtiquetas"
        '
        'mnuRepImpresionEtiquetas
        '
        Me.mnuRepImpresionEtiquetas.Caption = "Impresión de Etiquetas"
        Me.mnuRepImpresionEtiquetas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepImpresionEtiquetas.Id = 107
        Me.mnuRepImpresionEtiquetas.ImageIndex = 2
        Me.mnuRepImpresionEtiquetas.Name = "mnuRepImpresionEtiquetas"
        '
        'mnuUtiVariables
        '
        Me.mnuUtiVariables.Caption = "Variables del Sistema"
        Me.mnuUtiVariables.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuUtiVariables.Id = 108
        Me.mnuUtiVariables.Name = "mnuUtiVariables"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(82, 474)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Offset = 8
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 109
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnología .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"

    'Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    Dim osucursales As New VillarrealBusiness.clsSucursales
    '    Dim response As Dipros.Utils.Events
    '    response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

    '    If Not response.ErrorFound Then
    '        Dim odataset As DataSet

    '        odataset = response.Value

    '        Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
    '    End If
    'End Sub

#End Region

#Region "Catalogos"

    Public Sub mnuCatEtiquetas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatEtiquetas.ItemClick
        Dim oForm As New Comunes.frmEtiquetas
        With oForm
            .MenuOption = e.Item
            .Title = "Configuración de Etiquetas en Base al Precio de Lista"
            .MdiParent = Me
            .Show()
        End With
    End Sub
    Public Sub mnuCatTamañosEtiquetas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatTamañosEtiquetas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oTamañosEtiquetas As New VillarrealBusiness.clsTamaniosEtiquetas
        With oGrid
            .Title = "Tamaños de Etiquetas"
            .UpdateTitle = "un Tamaño de Etiqueta"
            .DataSource = AddressOf oTamañosEtiquetas.Listado
            .UpdateForm = New frmTamañosEtiquetas
            .Format = AddressOf for_tamaños_etiquetas_grs
            .MdiParent = Me
            .Show()
        End With
        oTamañosEtiquetas = Nothing
    End Sub

#End Region

#Region "Reportes"

    Public Sub mnuRepImpresionEtiquetas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepImpresionEtiquetas.ItemClick
        Dim oForm As New frmImpresionEtiquetas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Impresión de Etiquetas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

#End Region

#Region "Herramientas"

    Public Sub mnuUtiVariables_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuUtiVariables.ItemClick
        Dim oForm As New Comunes.frmVariables
        oForm.MdiParent = Me
        oForm.Show()
    End Sub

#End Region

 
End Class
