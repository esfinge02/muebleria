Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmImpresionEtiquetas
    Inherits Dipros.Windows.frmTINForm

#Region " Código generado por el Diseñador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicialización después de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents gpDepartamento As System.Windows.Forms.GroupBox
    Friend WithEvents optDepartamento As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents optGrupo As System.Windows.Forms.RadioButton
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento2 As Dipros.Editors.TINMultiLookup
    Friend WithEvents optArticulos As System.Windows.Forms.RadioButton
    Friend WithEvents gpGrupo As System.Windows.Forms.GroupBox
    Friend WithEvents gpArticulos As System.Windows.Forms.GroupBox
    Friend WithEvents grvOrdenesCompra As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grArticulos As DevExpress.XtraGrid.GridControl
    Friend WithEvents tmaArticulos As Dipros.Windows.TINMaster
    Friend WithEvents chkExistencias As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpPlantilla As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lkpEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblEntrada As System.Windows.Forms.Label
    Friend WithEvents chkTodos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents optEntrada As System.Windows.Forms.RadioButton
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmImpresionEtiquetas))
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.gpDepartamento = New System.Windows.Forms.GroupBox
        Me.optDepartamento = New System.Windows.Forms.RadioButton
        Me.gpGrupo = New System.Windows.Forms.GroupBox
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.Label6 = New System.Windows.Forms.Label
        Me.lkpDepartamento2 = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.optGrupo = New System.Windows.Forms.RadioButton
        Me.gpArticulos = New System.Windows.Forms.GroupBox
        Me.optArticulos = New System.Windows.Forms.RadioButton
        Me.tmaArticulos = New Dipros.Windows.TINMaster
        Me.grArticulos = New DevExpress.XtraGrid.GridControl
        Me.grvOrdenesCompra = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkExistencias = New DevExpress.XtraEditors.CheckEdit
        Me.lkpPlantilla = New Dipros.Editors.TINMultiLookup
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkTodos = New DevExpress.XtraEditors.CheckEdit
        Me.lkpEntrada = New Dipros.Editors.TINMultiLookup
        Me.lblEntrada = New System.Windows.Forms.Label
        Me.optEntrada = New System.Windows.Forms.RadioButton
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gpDepartamento.SuspendLayout()
        Me.gpGrupo.SuspendLayout()
        Me.gpArticulos.SuspendLayout()
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkExistencias.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.chkTodos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(383, 28)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "D&epartamento: "
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = False
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(112, 22)
        Me.lkpDepartamento.MultiSelect = True
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = "(Todos)"
        Me.lkpDepartamento.PopupWidth = CType(400, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = True
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SelectAll = True
        Me.lkpDepartamento.Size = New System.Drawing.Size(368, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 7
        Me.lkpDepartamento.ToolTip = Nothing
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'gpDepartamento
        '
        Me.gpDepartamento.Controls.Add(Me.lkpDepartamento)
        Me.gpDepartamento.Controls.Add(Me.Label2)
        Me.gpDepartamento.Controls.Add(Me.optDepartamento)
        Me.gpDepartamento.Location = New System.Drawing.Point(16, 128)
        Me.gpDepartamento.Name = "gpDepartamento"
        Me.gpDepartamento.Size = New System.Drawing.Size(496, 56)
        Me.gpDepartamento.TabIndex = 3
        Me.gpDepartamento.TabStop = False
        '
        'optDepartamento
        '
        Me.optDepartamento.Checked = True
        Me.optDepartamento.Location = New System.Drawing.Point(16, -4)
        Me.optDepartamento.Name = "optDepartamento"
        Me.optDepartamento.Size = New System.Drawing.Size(120, 24)
        Me.optDepartamento.TabIndex = 5
        Me.optDepartamento.TabStop = True
        Me.optDepartamento.Text = "&Departamento"
        '
        'gpGrupo
        '
        Me.gpGrupo.Controls.Add(Me.lkpGrupo)
        Me.gpGrupo.Controls.Add(Me.Label6)
        Me.gpGrupo.Controls.Add(Me.lkpDepartamento2)
        Me.gpGrupo.Controls.Add(Me.Label3)
        Me.gpGrupo.Controls.Add(Me.optGrupo)
        Me.gpGrupo.Location = New System.Drawing.Point(16, 192)
        Me.gpGrupo.Name = "gpGrupo"
        Me.gpGrupo.Size = New System.Drawing.Size(496, 78)
        Me.gpGrupo.TabIndex = 4
        Me.gpGrupo.TabStop = False
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = False
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Enabled = False
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(112, 46)
        Me.lkpGrupo.MultiSelect = True
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = "(Todos)"
        Me.lkpGrupo.PopupWidth = CType(400, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SelectAll = True
        Me.lkpGrupo.Size = New System.Drawing.Size(368, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 12
        Me.lkpGrupo.ToolTip = "Grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(65, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 16)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Gr&upo:"
        '
        'lkpDepartamento2
        '
        Me.lkpDepartamento2.AllowAdd = False
        Me.lkpDepartamento2.AutoReaload = False
        Me.lkpDepartamento2.DataSource = Nothing
        Me.lkpDepartamento2.DefaultSearchField = ""
        Me.lkpDepartamento2.DisplayMember = "nombre"
        Me.lkpDepartamento2.EditValue = Nothing
        Me.lkpDepartamento2.Enabled = False
        Me.lkpDepartamento2.Filtered = False
        Me.lkpDepartamento2.InitValue = Nothing
        Me.lkpDepartamento2.Location = New System.Drawing.Point(112, 22)
        Me.lkpDepartamento2.MultiSelect = True
        Me.lkpDepartamento2.Name = "lkpDepartamento2"
        Me.lkpDepartamento2.NullText = "(Todos)"
        Me.lkpDepartamento2.PopupWidth = CType(400, Long)
        Me.lkpDepartamento2.ReadOnlyControl = False
        Me.lkpDepartamento2.Required = True
        Me.lkpDepartamento2.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento2.SearchMember = ""
        Me.lkpDepartamento2.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento2.SelectAll = True
        Me.lkpDepartamento2.Size = New System.Drawing.Size(368, 20)
        Me.lkpDepartamento2.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento2.TabIndex = 10
        Me.lkpDepartamento2.ToolTip = Nothing
        Me.lkpDepartamento2.ValueMember = "departamento"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 16)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Depar&tamento:"
        '
        'optGrupo
        '
        Me.optGrupo.Location = New System.Drawing.Point(16, -4)
        Me.optGrupo.Name = "optGrupo"
        Me.optGrupo.Size = New System.Drawing.Size(64, 24)
        Me.optGrupo.TabIndex = 8
        Me.optGrupo.Text = "&Grupo"
        '
        'gpArticulos
        '
        Me.gpArticulos.Controls.Add(Me.optArticulos)
        Me.gpArticulos.Controls.Add(Me.tmaArticulos)
        Me.gpArticulos.Controls.Add(Me.grArticulos)
        Me.gpArticulos.Location = New System.Drawing.Point(16, 344)
        Me.gpArticulos.Name = "gpArticulos"
        Me.gpArticulos.Size = New System.Drawing.Size(496, 192)
        Me.gpArticulos.TabIndex = 6
        Me.gpArticulos.TabStop = False
        '
        'optArticulos
        '
        Me.optArticulos.Location = New System.Drawing.Point(16, -4)
        Me.optArticulos.Name = "optArticulos"
        Me.optArticulos.Size = New System.Drawing.Size(80, 24)
        Me.optArticulos.TabIndex = 17
        Me.optArticulos.Text = "Artícu&los"
        '
        'tmaArticulos
        '
        Me.tmaArticulos.BackColor = System.Drawing.Color.White
        Me.tmaArticulos.CanDelete = True
        Me.tmaArticulos.CanInsert = True
        Me.tmaArticulos.CanUpdate = True
        Me.tmaArticulos.Enabled = False
        Me.tmaArticulos.Grid = Me.grArticulos
        Me.tmaArticulos.Location = New System.Drawing.Point(16, 24)
        Me.tmaArticulos.Name = "tmaArticulos"
        Me.tmaArticulos.Size = New System.Drawing.Size(464, 23)
        Me.tmaArticulos.TabIndex = 12
        Me.tmaArticulos.TabStop = False
        Me.tmaArticulos.Title = "Artículos"
        Me.tmaArticulos.UpdateTitle = "un Registro"
        '
        'grArticulos
        '
        '
        'grArticulos.EmbeddedNavigator
        '
        Me.grArticulos.EmbeddedNavigator.Name = ""
        Me.grArticulos.Enabled = False
        Me.grArticulos.Location = New System.Drawing.Point(16, 48)
        Me.grArticulos.MainView = Me.grvOrdenesCompra
        Me.grArticulos.Name = "grArticulos"
        Me.grArticulos.Size = New System.Drawing.Size(464, 136)
        Me.grArticulos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grArticulos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArticulos.TabIndex = 13
        Me.grArticulos.TabStop = False
        Me.grArticulos.Text = "Articulos"
        '
        'grvOrdenesCompra
        '
        Me.grvOrdenesCompra.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcArticulo, Me.grcDescripcion, Me.grcCantidad, Me.grcModelo})
        Me.grvOrdenesCompra.GridControl = Me.grArticulos
        Me.grvOrdenesCompra.Name = "grvOrdenesCompra"
        Me.grvOrdenesCompra.OptionsBehavior.Editable = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowFilter = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowGroup = False
        Me.grvOrdenesCompra.OptionsCustomization.AllowSort = False
        Me.grvOrdenesCompra.OptionsView.ShowGroupPanel = False
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Artículo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.VisibleIndex = 0
        Me.grcArticulo.Width = 68
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripción"
        Me.grcDescripcion.FieldName = "descripcion_corta"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.VisibleIndex = 2
        Me.grcDescripcion.Width = 227
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.VisibleIndex = 3
        '
        'chkExistencias
        '
        Me.chkExistencias.EditValue = True
        Me.chkExistencias.Location = New System.Drawing.Point(24, 72)
        Me.chkExistencias.Name = "chkExistencias"
        '
        'chkExistencias.Properties
        '
        Me.chkExistencias.Properties.Caption = "Artículos con Existencia&s"
        Me.chkExistencias.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkExistencias.Size = New System.Drawing.Size(176, 19)
        Me.chkExistencias.TabIndex = 2
        '
        'lkpPlantilla
        '
        Me.lkpPlantilla.AllowAdd = False
        Me.lkpPlantilla.AutoReaload = False
        Me.lkpPlantilla.DataSource = Nothing
        Me.lkpPlantilla.DefaultSearchField = ""
        Me.lkpPlantilla.DisplayMember = "nombre"
        Me.lkpPlantilla.EditValue = Nothing
        Me.lkpPlantilla.Filtered = False
        Me.lkpPlantilla.InitValue = Nothing
        Me.lkpPlantilla.Location = New System.Drawing.Point(80, 38)
        Me.lkpPlantilla.MultiSelect = False
        Me.lkpPlantilla.Name = "lkpPlantilla"
        Me.lkpPlantilla.NullText = ""
        Me.lkpPlantilla.PopupWidth = CType(300, Long)
        Me.lkpPlantilla.ReadOnlyControl = False
        Me.lkpPlantilla.Required = False
        Me.lkpPlantilla.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlantilla.SearchMember = ""
        Me.lkpPlantilla.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlantilla.SelectAll = True
        Me.lkpPlantilla.Size = New System.Drawing.Size(216, 20)
        Me.lkpPlantilla.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPlantilla.TabIndex = 1
        Me.lkpPlantilla.ToolTip = Nothing
        Me.lkpPlantilla.ValueMember = "nombre"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Pla&ntilla:"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(26, 96)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 3
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "Descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(80, 94)
        Me.lkpBodega.MultiSelect = True
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = "(Todos)"
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = True
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = True
        Me.lkpBodega.Size = New System.Drawing.Size(216, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 4
        Me.lkpBodega.Tag = "Bodega"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkTodos)
        Me.GroupBox1.Controls.Add(Me.lkpEntrada)
        Me.GroupBox1.Controls.Add(Me.lblEntrada)
        Me.GroupBox1.Controls.Add(Me.optEntrada)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 280)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(496, 56)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'chkTodos
        '
        Me.chkTodos.Location = New System.Drawing.Point(224, 24)
        Me.chkTodos.Name = "chkTodos"
        '
        'chkTodos.Properties
        '
        Me.chkTodos.Properties.Caption = "&Todos"
        Me.chkTodos.Properties.Enabled = False
        Me.chkTodos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkTodos.Size = New System.Drawing.Size(64, 19)
        Me.chkTodos.TabIndex = 16
        '
        'lkpEntrada
        '
        Me.lkpEntrada.AllowAdd = False
        Me.lkpEntrada.AutoReaload = False
        Me.lkpEntrada.DataSource = Nothing
        Me.lkpEntrada.DefaultSearchField = ""
        Me.lkpEntrada.DisplayMember = "entrada"
        Me.lkpEntrada.EditValue = Nothing
        Me.lkpEntrada.Enabled = False
        Me.lkpEntrada.Filtered = False
        Me.lkpEntrada.InitValue = Nothing
        Me.lkpEntrada.Location = New System.Drawing.Point(112, 22)
        Me.lkpEntrada.MultiSelect = False
        Me.lkpEntrada.Name = "lkpEntrada"
        Me.lkpEntrada.NullText = ""
        Me.lkpEntrada.PopupWidth = CType(400, Long)
        Me.lkpEntrada.ReadOnlyControl = False
        Me.lkpEntrada.Required = True
        Me.lkpEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEntrada.SearchMember = ""
        Me.lkpEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEntrada.SelectAll = False
        Me.lkpEntrada.Size = New System.Drawing.Size(104, 20)
        Me.lkpEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEntrada.TabIndex = 15
        Me.lkpEntrada.Tag = ""
        Me.lkpEntrada.ToolTip = Nothing
        Me.lkpEntrada.ValueMember = "entrada"
        '
        'lblEntrada
        '
        Me.lblEntrada.AutoSize = True
        Me.lblEntrada.Location = New System.Drawing.Point(56, 24)
        Me.lblEntrada.Name = "lblEntrada"
        Me.lblEntrada.Size = New System.Drawing.Size(52, 16)
        Me.lblEntrada.TabIndex = 14
        Me.lblEntrada.Text = "Ent&rada:"
        '
        'optEntrada
        '
        Me.optEntrada.Location = New System.Drawing.Point(16, -4)
        Me.optEntrada.Name = "optEntrada"
        Me.optEntrada.Size = New System.Drawing.Size(72, 24)
        Me.optEntrada.TabIndex = 13
        Me.optEntrada.Text = "E&ntrada"
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 1
        '
        'frmImpresionEtiquetas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(522, 544)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lkpPlantilla)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.chkExistencias)
        Me.Controls.Add(Me.gpGrupo)
        Me.Controls.Add(Me.gpDepartamento)
        Me.Controls.Add(Me.gpArticulos)
        Me.Controls.Add(Me.GroupBox1)
        Me.MasterControl = Me.tmaArticulos
        Me.MasterControlActive = "tmaOrdenesCompra"
        Me.Name = "frmImpresionEtiquetas"
        Me.Text = "frmImpresionEtiquetas"
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.gpArticulos, 0)
        Me.Controls.SetChildIndex(Me.gpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.gpGrupo, 0)
        Me.Controls.SetChildIndex(Me.chkExistencias, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.lkpPlantilla, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.gpDepartamento.ResumeLayout(False)
        Me.gpGrupo.ResumeLayout(False)
        Me.gpArticulos.ResumeLayout(False)
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvOrdenesCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkExistencias.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.chkTodos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
    Private oGrupos As VillarrealBusiness.clsGruposArticulos
    Private oReportes As VillarrealBusiness.Reportes
    Private oVariables As VillarrealBusiness.clsVariables
    Private oBodegas As VillarrealBusiness.clsBodegas
    Private oEntradas As VillarrealBusiness.clsEntradas

    Private seleccionar_todo As Boolean

    Private ReadOnly Property TamañoEtiquetaPrecios() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpPlantilla)
        End Get
    End Property
    Private ReadOnly Property Entrada() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEntrada)
        End Get
    End Property

    Private opcion As Long = 1
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmImpresionEtiquetas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oGrupos = New VillarrealBusiness.clsGruposArticulos
        oReportes = New VillarrealBusiness.Reportes
        oVariables = New VillarrealBusiness.clsVariables
        oBodegas = New VillarrealBusiness.clsBodegas
        oEntradas = New VillarrealBusiness.clsEntradas

        Me.lkpBodega.SelectAll = True

        With Me.tmaArticulos
            .UpdateTitle = "un Artículo"
            .UpdateForm = New frmImpresionEtiquetasArticulos
            .AddColumn("articulo", "System.Int32")
            .AddColumn("descripcion_corta")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("modelo")
        End With
    End Sub
    Private Sub frmImpresionEtiquetas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim campos(1) As String
        campos(0) = "articulo"
        campos(1) = "cantidad"
        Select Case opcion

            Case 1      ' LA OPCION DE DEPARTAMENTO
                Response = oReportes.ImpresionEtiquetasPrecios(Me.lkpDepartamento.ToXML, Me.lkpDepartamento.ToXML, opcion, Me.chkExistencias.Checked, TamañoEtiquetaPrecios, Me.lkpBodega.ToXML, Entrada, Me.chkTodos.Checked)

            Case 2      ' LA OPCION DE GRUPO
                Response = oReportes.ImpresionEtiquetasPrecios(Me.lkpDepartamento2.ToXML, Me.lkpGrupo.ToXML, opcion, Me.chkExistencias.Checked, TamañoEtiquetaPrecios, Me.lkpBodega.ToXML, Entrada, Me.chkTodos.Checked)

            Case 3      ' LA OPCION DE ARTICULOS
                Response = oReportes.ImpresionEtiquetasPrecios(Me.lkpDepartamento.ToXML, Me.lkpDepartamento.ToXML, opcion, Me.chkExistencias.Checked, TamañoEtiquetaPrecios, Me.lkpBodega.ToXML, Entrada, Me.chkTodos.Checked, Me.tmaArticulos.ToXML(campos))

            Case 4      ' LA OPCION DE ENTRADA
                Response = oReportes.ImpresionEtiquetasPrecios(Me.lkpDepartamento.ToXML, Me.lkpDepartamento.ToXML, opcion, Me.chkExistencias.Checked, TamañoEtiquetaPrecios, Me.lkpBodega.ToXML, Entrada, Me.chkTodos.Checked, Me.tmaArticulos.ToXML(campos))

                'Exit Sub

        End Select

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            ' If oDataSet.Tables(0).Rows.Count > 0 Then

            Dim oReport As New rptEtiquetas
            oReport.DataSource = oDataSet.Tables(0)
            TINApp.ShowReport(Me.MdiParent, "Impresión de Etiquetas de Precios", oReport, , , , True)

            oReport = Nothing

            'Else
            '    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Información")
            'End If

            oDataSet = Nothing
        End If



    End Sub
    Private Sub frmImpresionEtiquetas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oReportes.ValidaTamañoEtiquetaPrecios(TamañoEtiquetaPrecios)
        'Response = oReportes.ValidaTamañoEtiquetaPrecios(TamañoEtiquetaPrecios)
        Select Case opcion

            Case 1      ' LA OPCION DE DEPARTAMENTO
                'Response = oReportes.ImpresionEtiquetasPrecios(Me.lkpDepartamento.ToXML, Me.lkpDepartamento.ToXML, opcion, Me.chkExistencias.Checked, TamañoEtiquetaPrecios, Me.lkpBodega.ToXML, Entrada, Me.chkTodos.Checked)

            Case 2      ' LA OPCION DE GRUPO
                'Response = oReportes.ImpresionEtiquetasPrecios(Me.lkpDepartamento2.ToXML, Me.lkpGrupo.ToXML, opcion, Me.chkExistencias.Checked, TamañoEtiquetaPrecios, Me.lkpBodega.ToXML, Entrada, Me.chkTodos.Checked)

            Case 3      ' LA OPCION DE ARTICULOS
                Response = oReportes.ValidacionArticulosIncluidos(Me.grvOrdenesCompra.RowCount)
                'Response = oReportes.ImpresionEtiquetasPrecios(Me.lkpDepartamento.ToXML, Me.lkpDepartamento.ToXML, opcion, Me.chkExistencias.Checked, TamañoEtiquetaPrecios, Me.lkpBodega.ToXML, Entrada, Me.chkTodos.Checked, Me.tmaArticulos.ToXML(campos))

            Case 4      ' LA OPCION DE ENTRADA
                'Response = oReportes.ImpresionEtiquetasPrecios(Me.lkpDepartamento.ToXML, Me.lkpDepartamento.ToXML, opcion, Me.chkExistencias.Checked, TamañoEtiquetaPrecios, Me.lkpBodega.ToXML, Entrada, Me.chkTodos.Checked, Me.tmaArticulos.ToXML(campos))

        End Select








    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            Me.lkpBodega.SelectAll = seleccionar_todo
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpDepartamento2_Format() Handles lkpDepartamento2.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento2)
    End Sub
    Private Sub lkpDepartamento2_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento2.LoadData
        Dim Response As New Events
        Response = oDepartamentos.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento2.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDepartamento2_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento2.EditValueChanged
        lkpGrupo_LoadData(True)
        lkpGrupo.SelectAll = True
    End Sub

    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData
        Dim Response As New Events
        Response = oGrupos.MultiLookup(Me.lkpDepartamento2.ToXML)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

    Private Sub lkpPlantilla_Format() Handles lkpPlantilla.Format
        Comunes.clsFormato.for_plantilla_reportes_grl(Me.lkpPlantilla)
    End Sub
    Private Sub lkpPlantilla_LoadData(ByVal Initialize As Boolean) Handles lkpPlantilla.LoadData
        Dim Response As New Events
        Response = oVariables.LookupPlantillasReportesEtiquetasPrecios()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlantilla.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpEntrada_Format() Handles lkpEntrada.Format
        Comunes.clsFormato.for_entradas_grl(Me.lkpEntrada)
    End Sub
    Private Sub lkpEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpEntrada.LoadData
        Dim response As Events
        Me.lkpEntrada.EditValue = Nothing
        response = oEntradas.Lookup(, True, -1)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpEntrada.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub chkExistencias_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExistencias.CheckedChanged
        If Me.chkExistencias.Checked = True Then
            Me.lkpBodega.Enabled = True
        Else
            seleccionar_todo = True
            Me.lkpBodega_LoadData(True)
            Me.lkpBodega.Enabled = False
        End If
    End Sub

    Private Sub optDepartamento_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDepartamento.CheckedChanged
        If optDepartamento.Checked = True Then
            opcion = 1
            Me.optGrupo.Checked = False
            Me.optEntrada.Checked = False
            Me.optArticulos.Checked = False
            Me.lkpDepartamento.Enabled = True
        Else
            Me.lkpDepartamento.Enabled = False
        End If
    End Sub
    Private Sub optGrupo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optGrupo.CheckedChanged
        If optGrupo.Checked = True Then
            opcion = 2
            Me.optDepartamento.Checked = False
            Me.optArticulos.Checked = False
            Me.optEntrada.Checked = False
            Me.lkpDepartamento2.Enabled = True
            Me.lkpGrupo.Enabled = True
        Else
            Me.lkpDepartamento2.Enabled = False
            Me.lkpGrupo.Enabled = False
        End If
    End Sub
    Private Sub optArticulos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optArticulos.CheckedChanged
        If optArticulos.Checked = True Then
            opcion = 3
            Me.optDepartamento.Checked = False
            Me.optGrupo.Checked = False
            Me.optEntrada.Checked = False
            Me.tmaArticulos.Enabled = True
            Me.grArticulos.Enabled = True
        Else
            Me.tmaArticulos.Enabled = False
            Me.grArticulos.Enabled = False
        End If

    End Sub
    Private Sub optEntrada_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optEntrada.CheckedChanged
        If optEntrada.Checked = True Then
            opcion = 4
            Me.optGrupo.Checked = False
            Me.optDepartamento.Checked = False
            Me.optArticulos.Checked = False
            Me.lkpEntrada.Enabled = True
            Me.chkTodos.Enabled = True
        Else
            Me.lkpEntrada.Enabled = False
            Me.chkTodos.Enabled = False
        End If

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region


End Class
