Module modInicio

    Public TINApp As New Dipros.Windows.Application

    'DESCRIPCION: Función de inicio del sistema
    'DESARROLLO: DIPROS SYSTEMS
    'FECHA: 18/02/2006 00:00:00
    Public Sub Main()
        TINApp.Connection = New Dipros.Data.Data
        TINApp.Application = "Etiquetas"
        TINApp.Prefix = "ETI"

        If Not TINApp.Connection.Trusted Then End
        If Not TINApp.Login() Then End

        'Inicializa la capa de negocios
        VillarrealBusiness.BusinessEnvironment.Connection = TINApp.Connection

        Comunes.Common.Aplicacion = TINApp

        'Inicia la aplicación
        Dim oMain As New frmMain
        Application.Run(oMain)
    End Sub

End Module
