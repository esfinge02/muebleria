Imports Dipros.Windows.Forms

Module modFormatos

#Region "TamañosEtiquetas"

    Public Sub for_tamaños_etiquetas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Tamaño", "tamanio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripción", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

End Module
