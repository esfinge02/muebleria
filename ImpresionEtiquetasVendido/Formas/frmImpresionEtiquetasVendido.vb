Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmImpresionEtiquetasVendido
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grArticulos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvArticulos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkTodas As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grcImprimir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkImprimir As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcImpresa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkImpresa As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbRefrescar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents DteFecha As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents tmMandarImprimirEtiquetas As System.Windows.Forms.Timer
    Friend WithEvents dteFechaVenta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbMercanciaEtiquetada As System.Windows.Forms.ToolBarButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmImpresionEtiquetasVendido))
        Me.grArticulos = New DevExpress.XtraGrid.GridControl
        Me.grvArticulos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcImprimir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkImprimir = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcImpresa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkImpresa = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.DteFecha = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkTodas = New DevExpress.XtraEditors.CheckEdit
        Me.tbImprimir = New System.Windows.Forms.ToolBarButton
        Me.tbRefrescar = New System.Windows.Forms.ToolBarButton
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.lblBodega = New System.Windows.Forms.Label
        Me.tmMandarImprimirEtiquetas = New System.Windows.Forms.Timer(Me.components)
        Me.dteFechaVenta = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbMercanciaEtiquetada = New System.Windows.Forms.ToolBarButton
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkImprimir, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkImpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DteFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTodas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbImprimir, Me.tbRefrescar, Me.tbMercanciaEtiquetada})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5911, 28)
        '
        'grArticulos
        '
        '
        'grArticulos.EmbeddedNavigator
        '
        Me.grArticulos.EmbeddedNavigator.Name = ""
        Me.grArticulos.Location = New System.Drawing.Point(13, 64)
        Me.grArticulos.MainView = Me.grvArticulos
        Me.grArticulos.Name = "grArticulos"
        Me.grArticulos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkImprimir, Me.chkImpresa, Me.DteFecha})
        Me.grArticulos.Size = New System.Drawing.Size(840, 488)
        Me.grArticulos.TabIndex = 59
        Me.grArticulos.Text = "Articulos"
        '
        'grvArticulos
        '
        Me.grvArticulos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcImprimir, Me.grcImpresa, Me.grcFactura, Me.grcFecha, Me.grcArticulo, Me.grcDescripcion})
        Me.grvArticulos.GridControl = Me.grArticulos
        Me.grvArticulos.Name = "grvArticulos"
        Me.grvArticulos.OptionsCustomization.AllowFilter = False
        Me.grvArticulos.OptionsCustomization.AllowGroup = False
        Me.grvArticulos.OptionsCustomization.AllowSort = False
        Me.grvArticulos.OptionsView.ShowGroupPanel = False
        Me.grvArticulos.OptionsView.ShowIndicator = False
        '
        'grcImprimir
        '
        Me.grcImprimir.Caption = "Imprimir"
        Me.grcImprimir.ColumnEdit = Me.chkImprimir
        Me.grcImprimir.FieldName = "imprimir"
        Me.grcImprimir.Name = "grcImprimir"
        Me.grcImprimir.VisibleIndex = 0
        Me.grcImprimir.Width = 55
        '
        'chkImprimir
        '
        Me.chkImprimir.AutoHeight = False
        Me.chkImprimir.Name = "chkImprimir"
        '
        'grcImpresa
        '
        Me.grcImpresa.Caption = "Impresa"
        Me.grcImpresa.ColumnEdit = Me.chkImpresa
        Me.grcImpresa.FieldName = "impresa"
        Me.grcImpresa.Name = "grcImpresa"
        Me.grcImpresa.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImpresa.VisibleIndex = 1
        Me.grcImpresa.Width = 55
        '
        'chkImpresa
        '
        Me.chkImpresa.AutoHeight = False
        Me.chkImpresa.Name = "chkImpresa"
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 2
        Me.grcFactura.Width = 91
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.ColumnEdit = Me.DteFecha
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy hh:mm tt"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha_factura"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 150
        '
        'DteFecha
        '
        Me.DteFecha.AutoHeight = False
        Me.DteFecha.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DteFecha.DisplayFormat.FormatString = "dd/MMM/yyyy hh:mm tt"
        Me.DteFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.DteFecha.Name = "DteFecha"
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcArticulo.VisibleIndex = 4
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 5
        Me.grcDescripcion.Width = 372
        '
        'chkTodas
        '
        Me.chkTodas.Location = New System.Drawing.Point(688, 39)
        Me.chkTodas.Name = "chkTodas"
        '
        'chkTodas.Properties
        '
        Me.chkTodas.Properties.Caption = "Incluir Etiquetas Impresas"
        Me.chkTodas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkTodas.Size = New System.Drawing.Size(160, 19)
        Me.chkTodas.TabIndex = 60
        Me.chkTodas.ToolTip = "Incluye las etiquetas ya impresas"
        '
        'tbImprimir
        '
        Me.tbImprimir.ImageIndex = 6
        Me.tbImprimir.Text = "Imprimir"
        '
        'tbRefrescar
        '
        Me.tbRefrescar.ImageIndex = 7
        Me.tbRefrescar.Text = "Refrescar"
        '
        'Timer1
        '
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = True
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "Descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(72, 37)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = True
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(328, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 62
        Me.lkpBodega.Tag = "bodega"
        Me.lkpBodega.ToolTip = "bodega"
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(16, 40)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(54, 16)
        Me.lblBodega.TabIndex = 61
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "&Bodega :"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tmMandarImprimirEtiquetas
        '
        '
        'dteFechaVenta
        '
        Me.dteFechaVenta.EditValue = New Date(2008, 5, 15, 0, 0, 0, 0)
        Me.dteFechaVenta.Location = New System.Drawing.Point(528, 37)
        Me.dteFechaVenta.Name = "dteFechaVenta"
        '
        'dteFechaVenta.Properties
        '
        Me.dteFechaVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaVenta.Properties.DisplayFormat.FormatString = "dd-MMM-yyyy"
        Me.dteFechaVenta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaVenta.Properties.EditFormat.FormatString = "dd-MMM-yyyy"
        Me.dteFechaVenta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaVenta.Size = New System.Drawing.Size(88, 20)
        Me.dteFechaVenta.TabIndex = 63
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(448, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 16)
        Me.Label2.TabIndex = 64
        Me.Label2.Tag = ""
        Me.Label2.Text = "Fecha Venta:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbMercanciaEtiquetada
        '
        Me.tbMercanciaEtiquetada.Text = "Reporte de Mc�a.  Etiquetada"
        '
        'frmImpresionEtiquetasVendido
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(866, 560)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dteFechaVenta)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.grArticulos)
        Me.Controls.Add(Me.chkTodas)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimizeBox = True
        Me.Name = "frmImpresionEtiquetasVendido"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Imprimir Etiquetas de Vendido"
        Me.Title = "Imprimir Etiquetas de Vendido"
        Me.Controls.SetChildIndex(Me.chkTodas, 0)
        Me.Controls.SetChildIndex(Me.grArticulos, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.dteFechaVenta, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        CType(Me.grArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkImprimir, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkImpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DteFecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTodas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oVentas As VillarrealBusiness.clsVentas
    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle
    Private oVariables As VillarrealBusiness.clsVariables
    Private oBodegas As New VillarrealBusiness.clsBodegas

    Private ReadOnly Property MinutosActualizarEtiquetasVendido() As Long
        Get
            Return oVariables.TraeDatos("minutos_actualizar_etiquetas_vendido", VillarrealBusiness.clsVariables.tipo_dato.Entero)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmImpresionEtiquetasVendido_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmImpresionEtiquetasVendido_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub
    Private Sub frmImpresionEtiquetasVendido_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmImpresionEtiquetasVendido_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVentas = New VillarrealBusiness.clsVentas
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        oVariables = New VillarrealBusiness.clsVariables
        oBodegas = New VillarrealBusiness.clsBodegas

        Me.dteFechaVenta.EditValue = CDate(TINApp.FechaServidor)

        Me.lkpBodega.EditValue = Comunes.clsUtilerias.uti_BodegaDeUsuario(TINApp.Connection.User)

        Me.tbrTools.Buttons(0).Visible = False
        Me.tbrTools.Buttons(1).Visible = False

        If MinutosActualizarEtiquetasVendido > 0 Then
            Me.Timer1.Interval = MinutosActualizarEtiquetasVendido * 60000  'hay 60 000 milisegundos en un minuto
        Else
            Me.Timer1.Interval = 60000
        End If
        Me.tmMandarImprimirEtiquetas.Interval = 30 * 60000

        Me.Timer1.Start()
        Me.tmMandarImprimirEtiquetas.Start()

        CargarFacturas()
    End Sub
    Private Sub frmImpresionEtiquetasVendido_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Dim orow As DataRow
        For Each orow In CType(Me.grArticulos.DataSource, DataTable).Rows
            If Not Response.ErrorFound And orow("imprimir") = True Then

                Response = oVentasDetalle.ActualizaImpresaEtiquetaVendido(orow("sucursal"), orow("serie"), orow("folio_factura"), orow("partida"))

            End If
        Next

        CargarFacturas()
    End Sub
    Private Sub frmImpresionEtiquetasVendido_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        Dim response As New Events
        Dim orow As DataRow

        Me.grvArticulos.CloseEditor()
        Me.grvArticulos.UpdateCurrentRow()



        If e.Button Is Me.tbImprimir Then
            'Imprimir Etiquetas
            If ShowMessage(MessageType.MsgQuestion, "�Desea Imprimir las Etiquetas?") = Answer.MsgYes Then

                Me.frmImpresionEtiquetasVendido_BeginUpdate()
                For Each orow In CType(Me.grArticulos.DataSource, DataTable).Rows
                    If orow("imprimir") = True Then
                        response = oVentasDetalle.ActualizaImpresaEtiquetaVendido(orow("sucursal"), orow("serie"), orow("folio"), orow("partida"))

                        If response.ErrorFound Then
                            Me.frmImpresionEtiquetasVendido_AbortUpdate()
                            response.ShowMessage()
                            Exit Sub
                        End If
                    End If
                Next


                ImprimirEtiquetas(response)
                If Not response.ErrorFound Then Me.frmImpresionEtiquetasVendido_EndUpdate()
                CargarFacturas()
            End If

            response = Nothing
        End If

        If e.Button Is Me.tbMercanciaEtiquetada Then
            Dim oForm As New Comunes.frmMercanciaEtiquetada
            oForm.OwnerForm = Me

            oForm.Title = "Mercanc�a Etiquetada"
            oForm.Action = Actions.Report
 'Formulario Centrado
            oForm.Top =  (Screen.PrimaryScreen.WorkingArea.Height - Me.Height) \ 2
            oForm.Left = (Screen.PrimaryScreen.WorkingArea.Width - Me.Width) \ 2
            oForm.Show()
        End If

        If e.Button Is Me.tbRefrescar Then
            CargarFacturas()
        End If

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.grvArticulos.CloseEditor()
        Me.grvArticulos.UpdateCurrentRow()

        CargarFacturas()
    End Sub
    Private Sub tmMandarImprimirEtiquetas_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmMandarImprimirEtiquetas.Tick
        ShowMessage(MessageType.MsgInformation, "Mandar a imprimir etiquetas.")
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub chkTodas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodas.CheckedChanged
        CargarFacturas()
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


    Private Sub lkpBodega_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodega.EditValueChanged
        CargarFacturas()
    End Sub
    Private Sub dteFechaVenta_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFechaVenta.EditValueChanged
        If IsDate(Me.dteFechaVenta.EditValue) Then
            CargarFacturas()
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarFacturas()


        Dim Response As Dipros.Utils.Events
        Try
            If Me.lkpBodega.EditValue = "" Or Me.lkpBodega.EditValue = Nothing Then
                Response = oVentas.EtiquetasPorImprimir(Comunes.Common.Bodega_Actual, Me.dteFechaVenta.DateTime, Me.chkTodas.Checked)
            Else
                Response = oVentas.EtiquetasPorImprimir(Me.lkpBodega.EditValue, Me.dteFechaVenta.DateTime, Me.chkTodas.Checked)
            End If

            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grArticulos.DataSource = oDataSet.Tables(0)

                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Function ImprimirEtiquetas(ByRef oevent As Events) As Events
        Dim oDataTable As DataRow()
        Try

            oDataTable = CType(Me.grArticulos.DataSource, DataTable).Select("imprimir = True")

            Dim oReport As New rptImpresionEtiquetasVendido

            oReport.DataSource = oDataTable


            TINApp.ShowReport(Me.MdiParent, "Etiquetas de Vendido", oReport, , , , True)

        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
            oevent.Message = ex.ToString
        End Try

    End Function
#End Region


End Class
