Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptImpresionEtiquetasVendido
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private lblArticulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVendido As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private txtDescripcionGrupo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfechahora As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_vendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private leyenda_reparto As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private horario As DataDynamics.ActiveReports.TextBox = Nothing
	Private articulos1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private articulo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "ImpresionEtiquetasVendido.rptImpresionEtiquetasVendido.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblFactura = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblCliente = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFecha = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblArticulo = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtFactura = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtCliente = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtVendido = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Label2 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Label)
		Me.txtDescripcionGrupo = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.txtfechahora = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.nombre_vendedor = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.leyenda_reparto = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.Label)
		Me.horario = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.articulos1 = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.articulo1 = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region


End Class
