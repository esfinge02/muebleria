Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptReporteDeComisionesConDevoluciones
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphVentas As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfVentas As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblVendedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblNotas As DataDynamics.ActiveReports.Label = Nothing
	Private lblVentasMayor13 As DataDynamics.ActiveReports.Label = Nothing
	Private lblVentaMenor13 As DataDynamics.ActiveReports.Label = Nothing
	Private lblVentaTotal As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private lblSueldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblAdeudo As DataDynamics.ActiveReports.Label = Nothing
	Private lblAPagar As DataDynamics.ActiveReports.Label = Nothing
	Private Label17 As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private sucursal_nombre1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtVendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreVendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasMenor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasMayor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNotas As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentaTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNotasMenor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtComisionMenor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNotasMayor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtComisionMayor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSueldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtAdeudo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtAPagar As DataDynamics.ActiveReports.TextBox = Nothing
	Private comision_menor_131 As DataDynamics.ActiveReports.TextBox = Nothing
	Private comision_menor_132 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalNotas As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVentasMayor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVentasMenor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVentaTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalNotasMenor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalComisionMenor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalNotasMayor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalMayor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private comision_menor_133 As DataDynamics.ActiveReports.TextBox = Nothing
	Private comision_cancelada_menor_131 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comisiones.rptReporteDeComisionesConDevoluciones.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphVentas = CType(Me.Sections("gphVentas"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfVentas = CType(Me.Sections("gpfVentas"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblVendedor = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblNotas = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblVentasMayor13 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblVentaMenor13 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblVentaTotal = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Line)
		Me.Label12 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Line3 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Line)
		Me.Label14 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Line4 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Line)
		Me.lblSueldo = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.lblAdeudo = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.lblAPagar = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Label)
		Me.Label17 = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.gphVentas.Controls(0),DataDynamics.ActiveReports.Label)
		Me.sucursal_nombre1 = CType(Me.gphVentas.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtVendedor = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreVendedor = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasMenor13 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasMayor13 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtNotas = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtVentaTotal = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtNotasMenor13 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtComisionMenor13 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtNotasMayor13 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtComisionMayor13 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtSueldo = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtAdeudo = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.txtAPagar = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.comision_menor_131 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.comision_menor_132 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.gpfVentas.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalNotas = CType(Me.gpfVentas.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVentasMayor = CType(Me.gpfVentas.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVentasMenor = CType(Me.gpfVentas.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVentaTotal = CType(Me.gpfVentas.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalNotasMenor = CType(Me.gpfVentas.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalComisionMenor13 = CType(Me.gpfVentas.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalNotasMayor13 = CType(Me.gpfVentas.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalMayor13 = CType(Me.gpfVentas.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.comision_menor_133 = CType(Me.gpfVentas.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.comision_cancelada_menor_131 = CType(Me.gpfVentas.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Line)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub Detail_Format(ByVal sender As Object, _
    ByVal e As System.EventArgs) Handles Detail.Format

        If Me.DataSource Is Nothing _
        OrElse Me.DataSource.Rows.count = 0 Then Exit Sub

        With Me.DataSource.Rows(0)
            lblTitulo.Text &= " - Sucursal: " & IIf(IsDBNull(.Item("Sucursal_Nombre")), "", .Item("Sucursal_Nombre"))
        End With

    End Sub


End Class
