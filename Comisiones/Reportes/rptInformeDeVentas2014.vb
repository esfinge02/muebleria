Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptInformeDeVentas2014
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphVentas As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfVentas As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblVendedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblNotas As DataDynamics.ActiveReports.Label = Nothing
	Private lblVentasMayor13 As DataDynamics.ActiveReports.Label = Nothing
	Private lblVentaMenor13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal_Contado As DataDynamics.ActiveReports.Label = Nothing
	Private lblFonacot As DataDynamics.ActiveReports.Label = Nothing
	Private lblCanceladasFonact As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotalFonacot As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotalVentasNetas As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private txtVendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreVendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasMenor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasMayor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtventas_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtventas_fonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtventas_total As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnotas_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtxnotas_menor_13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnotas_mayor_13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnotas_fonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtventas_total_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNotasFonactot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNotasCanceladasFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasCanceladasFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVantasFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasTotalFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private ventas_totales_fonacot1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private notas_total_vendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalVentasMayor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVentasMenor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalventas_fonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalventas_total As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalventas_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private notas_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private notas_menor_13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private notas_mayor_13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private notas_fonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txttotal_venta_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVentasFonacotTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalCanceladasFonacotTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVentaFonacotTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotralVentasCanceladasFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalFonacotTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private ventas_fonacot1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private notas_total As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comisiones.rptInformeDeVentas2014.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphVentas = CType(Me.Sections("gphVentas"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfVentas = CType(Me.Sections("gpfVentas"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblVendedor = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblNotas = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblVentasMayor13 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblVentaMenor13 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblTotal_Contado = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblFonacot = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblCanceladasFonact = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblTotalFonacot = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblTotalVentasNetas = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.gphVentas.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.gphVentas.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtVendedor = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreVendedor = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasMenor13 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasMayor13 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtventas_contado = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtventas_fonacot = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtventas_total = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtnotas_contado = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtxnotas_menor_13 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtnotas_mayor_13 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtnotas_fonacot = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtventas_total_contado = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.txtNotasFonactot = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.txtNotasCanceladasFonacot = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasCanceladasFonacot = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtVantasFonacot = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasTotalFonacot = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.ventas_totales_fonacot1 = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.notas_total_vendedor = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.gpfVentas.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalVentasMayor = CType(Me.gpfVentas.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVentasMenor = CType(Me.gpfVentas.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalventas_fonacot = CType(Me.gpfVentas.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalventas_total = CType(Me.gpfVentas.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalventas_contado = CType(Me.gpfVentas.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.notas_contado = CType(Me.gpfVentas.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.notas_menor_13 = CType(Me.gpfVentas.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.notas_mayor_13 = CType(Me.gpfVentas.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.notas_fonacot = CType(Me.gpfVentas.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txttotal_venta_contado = CType(Me.gpfVentas.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVentasFonacotTotal = CType(Me.gpfVentas.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalCanceladasFonacotTotal = CType(Me.gpfVentas.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVentaFonacotTotal = CType(Me.gpfVentas.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.txtTotralVentasCanceladasFonacot = CType(Me.gpfVentas.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalFonacotTotal = CType(Me.gpfVentas.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.ventas_fonacot1 = CType(Me.gpfVentas.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.notas_total = CType(Me.gpfVentas.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Line)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

   

    
End Class
