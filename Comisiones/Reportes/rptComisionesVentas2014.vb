Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptComisionesVentas2014
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghEsquema As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfEsquema As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblVendedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblNotas As DataDynamics.ActiveReports.Label = Nothing
	Private lblVentasMayor13 As DataDynamics.ActiveReports.Label = Nothing
	Private lblVentaMenor13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal_Contado As DataDynamics.ActiveReports.Label = Nothing
	Private lblFonacot As DataDynamics.ActiveReports.Label = Nothing
	Private lblCanceladasFonact As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotalFonacot As DataDynamics.ActiveReports.Label = Nothing
	Private lblNumeroEmpleado As DataDynamics.ActiveReports.Label = Nothing
	Private lblPorCredito As DataDynamics.ActiveReports.Label = Nothing
	Private lblComisionCredito As DataDynamics.ActiveReports.Label = Nothing
	Private lblVentasFonacot As DataDynamics.ActiveReports.Label = Nothing
	Private lblCanceladasFonacto As DataDynamics.ActiveReports.Label = Nothing
	Private lblNetoFonacot As DataDynamics.ActiveReports.Label = Nothing
	Private lblPorFonacot As DataDynamics.ActiveReports.Label = Nothing
	Private lblComisionFonacot As DataDynamics.ActiveReports.Label = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private txtVendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreVendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasMenor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasMayor13 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtventas_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtventas_fonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtventas_total As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtventas_total_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasCanceladasFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVantasFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasTotalFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentaCanceladaFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNetoFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private ventas_totales_fonacot4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private comision_contados1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private comision_contados2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private ventas_totales_fonacot5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private ventas_totales_fonacot7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private comision_contados3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalVentasMenor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalventas_fonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalventas_total As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalventas_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txttotal_venta_contado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVentaFonacotTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotralVentasCanceladasFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalFonacotTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalCanceladasFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasNetoFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVentasComisionFonacot As DataDynamics.ActiveReports.TextBox = Nothing
	Private ventas_totales_fonacot6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private ventas_totales_fonacot8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private comision_total_pagar1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comisiones.rptComisionesVentas2014.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghEsquema = CType(Me.Sections("ghEsquema"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfEsquema = CType(Me.Sections("gfEsquema"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblVendedor = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblNotas = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblVentasMayor13 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblVentaMenor13 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblTotal_Contado = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblFonacot = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblCanceladasFonact = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblTotalFonacot = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblNumeroEmpleado = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblPorCredito = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblComisionCredito = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.lblVentasFonacot = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.lblCanceladasFonacto = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.lblNetoFonacot = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.lblPorFonacot = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Label)
		Me.lblComisionFonacot = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.ghEsquema.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.ghEsquema.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtVendedor = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreVendedor = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasMenor13 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasMayor13 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtventas_contado = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtventas_fonacot = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtventas_total = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtventas_total_contado = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasCanceladasFonacot = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtVantasFonacot = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasTotalFonacot = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtVentaCanceladaFonacot = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.txtNetoFonacot = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.ventas_totales_fonacot4 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.comision_contados1 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.comision_contados2 = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.ventas_totales_fonacot5 = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.ventas_totales_fonacot7 = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.comision_contados3 = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.gfEsquema.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalVentasMenor = CType(Me.gfEsquema.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalventas_fonacot = CType(Me.gfEsquema.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalventas_total = CType(Me.gfEsquema.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalventas_contado = CType(Me.gfEsquema.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txttotal_venta_contado = CType(Me.gfEsquema.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVentaFonacotTotal = CType(Me.gfEsquema.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtTotralVentasCanceladasFonacot = CType(Me.gfEsquema.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalFonacotTotal = CType(Me.gfEsquema.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalCanceladasFonacot = CType(Me.gfEsquema.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasNetoFonacot = CType(Me.gfEsquema.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtVentasComisionFonacot = CType(Me.gfEsquema.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.ventas_totales_fonacot6 = CType(Me.gfEsquema.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.ventas_totales_fonacot8 = CType(Me.gfEsquema.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.comision_total_pagar1 = CType(Me.gfEsquema.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.Label16 = CType(Me.gfEsquema.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Line)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptComisionesVentas2014_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            'Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            'Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            'Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
