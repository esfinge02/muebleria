Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptComisionNetaPagar
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghSucursal As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfSucursal As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private ShapeEncabe As DataDynamics.ActiveReports.Shape = Nothing
	Private lblNumeroEmpleado As DataDynamics.ActiveReports.Label = Nothing
	Private lblNombreVendedor As DataDynamics.ActiveReports.Label = Nothing
	Private lblComisionVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblSueldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblComisionPagar As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoPendiente As DataDynamics.ActiveReports.Label = Nothing
	Private lblComisionNetaPagar As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblSucursal As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNoEmpleado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreVendedor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtComisionVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSueldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtComisionPagar As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoPendiente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtComisionNeta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotales As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalSueldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalPagar As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalSaldo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalNeta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaHora As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Comisiones.rptComisionNetaPagar.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghSucursal = CType(Me.Sections("ghSucursal"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfSucursal = CType(Me.Sections("gfSucursal"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.ShapeEncabe = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Shape)
		Me.lblNumeroEmpleado = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblNombreVendedor = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblComisionVenta = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblSueldo = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblComisionPagar = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblSaldoPendiente = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblComisionNetaPagar = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Picture)
		Me.lblSucursal = CType(Me.ghSucursal.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.ghSucursal.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtNoEmpleado = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreVendedor = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtComisionVenta = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtSueldo = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtComisionPagar = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoPendiente = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtComisionNeta = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtTotales = CType(Me.gfSucursal.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVenta = CType(Me.gfSucursal.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalSueldo = CType(Me.gfSucursal.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalPagar = CType(Me.gfSucursal.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalSaldo = CType(Me.gfSucursal.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalNeta = CType(Me.gfSucursal.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaHora = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region
End Class
