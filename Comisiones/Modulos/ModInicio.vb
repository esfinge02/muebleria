Imports System.Globalization
Imports System.Threading

Module ModInicio
    Public TINApp As New Dipros.Windows.Application
    Public Sub Main()
        TINApp.Connection = New Dipros.Data.Data
        TINApp.Application = "Comisiones a Vendedores"
        TINApp.Prefix = "CV"

        ' ================================================================================
        Dim oStyle As New DevExpress.Utils.ViewStyle
        oStyle.BackColor = System.Drawing.Color.Beige
        oStyle.Options = oStyle.Options And (DevExpress.Utils.StyleOptions.UseBackColor Or DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseForeColor)
        oStyle.ForeColor = System.Drawing.Color.Black

        TINApp.Options.StyleDisabled = oStyle
        ' ================================================================================

        If Not TINApp.Connection.Trusted Then End
        If Not TINApp.Login() Then End

        'Inicializa la capa de negocios
        VillarrealBusiness.BusinessEnvironment.Connection = TINApp.Connection

        ''----------------------------------------------------------------------------------
        'Selecciono por default la cultura de M�xico como configuracion del programa
        Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")

        Dim oMicultura As New CultureInfo("es-MX")
        Dim instance As New DateTimeFormatInfo
        Dim value As String

        value = "hh:mm:ss:tt"
        instance.LongTimePattern = value
        oMicultura.DateTimeFormat.LongTimePattern = instance.LongTimePattern

        Thread.CurrentThread.CurrentCulture = oMicultura

        ''----------------------------------------------------------------------------------

        'Paso de conexion y frmMain a Comunes
        Comunes.Common.Aplicacion = TINApp

       
        'Dim oMain As New frmCalculoComisionesVendedores
        Dim oMain As New frmMain

        Application.Run(oMain)

    End Sub
End Module
