Imports Dipros.Windows.Forms

Module ModFormatos
    Public Sub for_esquemas_comisiones_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Esquema", "esquema", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre del Esquema", "nombre", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "% Contados", "porcentaje_comisiones_contados", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "% Cr�ditos", "porcentaje_comisiones_creditos", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "% Fonacot", "porcentaje_comisiones_fonacot", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "% General", "porcentaje_comisiones_general", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Restar Sueldo", "restar_sueldo", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Usa Tabla de Rangos de Ventas", "usar_tabla_rangos_credito", 7, DevExpress.Utils.FormatType.None, "")
    End Sub
End Module
