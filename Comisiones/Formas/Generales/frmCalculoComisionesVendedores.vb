Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCalculoComisionesVendedores
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarItem1 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItem2 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItem3 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcRfc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cmdInsertar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdActualizar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkNoPagaComision As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents pnlExclusiones As System.Windows.Forms.Panel
    Friend WithEvents cmdGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pnlVendedores As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grClientes As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvClientes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grVendedores As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvVendedores As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSueldoVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkActivo As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcMeta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcActivo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblSueldo As System.Windows.Forms.Label
    Friend WithEvents cmdActualizarVendedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdGuardarVendedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdCancelarVendedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gpbClientes As System.Windows.Forms.GroupBox
    Friend WithEvents gpbVendedores As System.Windows.Forms.GroupBox
    Friend WithEvents clcMeta As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcVendedor As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtNombreVendedor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkActivoVendedor As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcSueldo As Dipros.Editors.TINCalcEdit
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdEliminarVendedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pnlCalcular As System.Windows.Forms.Panel
    Friend WithEvents gpbCalcular As System.Windows.Forms.GroupBox
    Friend WithEvents lblDesde As System.Windows.Forms.Label
    Friend WithEvents dteDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents gpbPorcentajes As System.Windows.Forms.GroupBox
    Friend WithEvents lbl13meses As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdCalcular As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dteHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents clcHasta13 As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcMas13 As Dipros.Editors.TINCalcEdit
    Friend WithEvents cmdReportes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents chkDevoluciones As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCalculoComisionesVendedores))
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.NavBarItem1 = New DevExpress.XtraNavBar.NavBarItem
        Me.NavBarItem2 = New DevExpress.XtraNavBar.NavBarItem
        Me.NavBarItem3 = New DevExpress.XtraNavBar.NavBarItem
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.pnlExclusiones = New System.Windows.Forms.Panel
        Me.gpbClientes = New System.Windows.Forms.GroupBox
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.cmdInsertar = New DevExpress.XtraEditors.SimpleButton
        Me.chkNoPagaComision = New DevExpress.XtraEditors.CheckEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.txtNombres = New DevExpress.XtraEditors.TextEdit
        Me.cmdActualizar = New DevExpress.XtraEditors.SimpleButton
        Me.lblNombre = New System.Windows.Forms.Label
        Me.cmdGuardar = New DevExpress.XtraEditors.SimpleButton
        Me.cmdCancelar = New DevExpress.XtraEditors.SimpleButton
        Me.grClientes = New DevExpress.XtraGrid.GridControl
        Me.grvClientes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcRfc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.pnlVendedores = New System.Windows.Forms.Panel
        Me.gpbVendedores = New System.Windows.Forms.GroupBox
        Me.chkActivoVendedor = New DevExpress.XtraEditors.CheckEdit
        Me.clcMeta = New Dipros.Editors.TINCalcEdit
        Me.clcVendedor = New Dipros.Editors.TINCalcEdit
        Me.txtNombreVendedor = New DevExpress.XtraEditors.TextEdit
        Me.clcSueldo = New Dipros.Editors.TINCalcEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblSueldo = New System.Windows.Forms.Label
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmdGuardarVendedor = New DevExpress.XtraEditors.SimpleButton
        Me.cmdActualizarVendedor = New DevExpress.XtraEditors.SimpleButton
        Me.cmdCancelarVendedor = New DevExpress.XtraEditors.SimpleButton
        Me.cmdEliminarVendedor = New DevExpress.XtraEditors.SimpleButton
        Me.grVendedores = New DevExpress.XtraGrid.GridControl
        Me.grvVendedores = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcVendedor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreVendedor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSueldoVendedor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcMeta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcActivo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkActivo = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.pnlCalcular = New System.Windows.Forms.Panel
        Me.cmdCalcular = New DevExpress.XtraEditors.SimpleButton
        Me.gpbCalcular = New System.Windows.Forms.GroupBox
        Me.chkDevoluciones = New DevExpress.XtraEditors.CheckEdit
        Me.gpbPorcentajes = New System.Windows.Forms.GroupBox
        Me.clcHasta13 = New Dipros.Editors.TINCalcEdit
        Me.lbl13meses = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcMas13 = New Dipros.Editors.TINCalcEdit
        Me.lblDesde = New System.Windows.Forms.Label
        Me.dteDesde = New DevExpress.XtraEditors.DateEdit
        Me.dteHasta = New DevExpress.XtraEditors.DateEdit
        Me.lblHasta = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.cmdReportes = New DevExpress.XtraEditors.SimpleButton
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlExclusiones.SuspendLayout()
        Me.gpbClientes.SuspendLayout()
        CType(Me.chkNoPagaComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlVendedores.SuspendLayout()
        Me.gpbVendedores.SuspendLayout()
        CType(Me.chkActivoVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMeta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSueldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grVendedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvVendedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActivo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCalcular.SuspendLayout()
        Me.gpbCalcular.SuspendLayout()
        CType(Me.chkDevoluciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbPorcentajes.SuspendLayout()
        CType(Me.clcHasta13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMas13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.AllowDrop = True
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.NavBarItem1, Me.NavBarItem2, Me.NavBarItem3})
        Me.NavBarControl1.LargeImages = Me.ImageList1
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 0)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(144, 454)
        Me.NavBarControl1.SmallImages = Me.ImageList1
        Me.NavBarControl1.TabIndex = 0
        Me.NavBarControl1.Text = "NavBarControl1"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.NavigationPaneViewInfoRegistrator
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = "Comisiones"
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
        Me.NavBarGroup1.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItem1), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItem2), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItem3)})
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'NavBarItem1
        '
        Me.NavBarItem1.Caption = "Exclusiones"
        Me.NavBarItem1.LargeImageIndex = 0
        Me.NavBarItem1.Name = "NavBarItem1"
        Me.NavBarItem1.SmallImageIndex = 0
        '
        'NavBarItem2
        '
        Me.NavBarItem2.Caption = "Vendedores"
        Me.NavBarItem2.LargeImageIndex = 1
        Me.NavBarItem2.Name = "NavBarItem2"
        '
        'NavBarItem3
        '
        Me.NavBarItem3.Caption = "Calcular"
        Me.NavBarItem3.LargeImageIndex = 2
        Me.NavBarItem3.Name = "NavBarItem3"
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'pnlExclusiones
        '
        Me.pnlExclusiones.Controls.Add(Me.gpbClientes)
        Me.pnlExclusiones.Controls.Add(Me.grClientes)
        Me.pnlExclusiones.Controls.Add(Me.Label2)
        Me.pnlExclusiones.Location = New System.Drawing.Point(144, 8)
        Me.pnlExclusiones.Name = "pnlExclusiones"
        Me.pnlExclusiones.Size = New System.Drawing.Size(632, 448)
        Me.pnlExclusiones.TabIndex = 1
        Me.pnlExclusiones.Visible = False
        '
        'gpbClientes
        '
        Me.gpbClientes.Controls.Add(Me.lkpCliente)
        Me.gpbClientes.Controls.Add(Me.cmdInsertar)
        Me.gpbClientes.Controls.Add(Me.chkNoPagaComision)
        Me.gpbClientes.Controls.Add(Me.lblCliente)
        Me.gpbClientes.Controls.Add(Me.txtNombres)
        Me.gpbClientes.Controls.Add(Me.cmdActualizar)
        Me.gpbClientes.Controls.Add(Me.lblNombre)
        Me.gpbClientes.Controls.Add(Me.cmdGuardar)
        Me.gpbClientes.Controls.Add(Me.cmdCancelar)
        Me.gpbClientes.Location = New System.Drawing.Point(328, 16)
        Me.gpbClientes.Name = "gpbClientes"
        Me.gpbClientes.Size = New System.Drawing.Size(288, 136)
        Me.gpbClientes.TabIndex = 76
        Me.gpbClientes.TabStop = False
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "cliente"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(52, 54)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(350, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(136, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 78
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'cmdInsertar
        '
        Me.cmdInsertar.Location = New System.Drawing.Point(24, 16)
        Me.cmdInsertar.Name = "cmdInsertar"
        Me.cmdInsertar.TabIndex = 77
        Me.cmdInsertar.Text = "Insertar"
        '
        'chkNoPagaComision
        '
        Me.chkNoPagaComision.Location = New System.Drawing.Point(52, 104)
        Me.chkNoPagaComision.Name = "chkNoPagaComision"
        '
        'chkNoPagaComision.Properties
        '
        Me.chkNoPagaComision.Properties.Caption = "No Paga Comisi�n"
        Me.chkNoPagaComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkNoPagaComision.Size = New System.Drawing.Size(192, 20)
        Me.chkNoPagaComision.TabIndex = 76
        Me.chkNoPagaComision.Tag = ""
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(8, 56)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(43, 16)
        Me.lblCliente.TabIndex = 72
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombres
        '
        Me.txtNombres.EditValue = ""
        Me.txtNombres.Location = New System.Drawing.Point(52, 78)
        Me.txtNombres.Name = "txtNombres"
        '
        'txtNombres.Properties
        '
        Me.txtNombres.Properties.Enabled = False
        Me.txtNombres.Properties.MaxLength = 100
        Me.txtNombres.Size = New System.Drawing.Size(223, 20)
        Me.txtNombres.TabIndex = 75
        Me.txtNombres.Tag = "nombre"
        '
        'cmdActualizar
        '
        Me.cmdActualizar.Location = New System.Drawing.Point(104, 16)
        Me.cmdActualizar.Name = "cmdActualizar"
        Me.cmdActualizar.TabIndex = 77
        Me.cmdActualizar.Text = "Actualizar"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(3, 80)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(48, 16)
        Me.lblNombre.TabIndex = 74
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdGuardar
        '
        Me.cmdGuardar.Enabled = False
        Me.cmdGuardar.Location = New System.Drawing.Point(24, 16)
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.TabIndex = 77
        Me.cmdGuardar.Text = "Guardar"
        Me.cmdGuardar.Visible = False
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Enabled = False
        Me.cmdCancelar.Location = New System.Drawing.Point(104, 16)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.TabIndex = 77
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.Visible = False
        '
        'grClientes
        '
        '
        'grClientes.EmbeddedNavigator
        '
        Me.grClientes.EmbeddedNavigator.Name = ""
        Me.grClientes.Location = New System.Drawing.Point(8, 24)
        Me.grClientes.MainView = Me.grvClientes
        Me.grClientes.Name = "grClientes"
        Me.grClientes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grClientes.Size = New System.Drawing.Size(312, 408)
        Me.grClientes.TabIndex = 70
        Me.grClientes.Text = "Clientes"
        '
        'grvClientes
        '
        Me.grvClientes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcCliente, Me.grcNombreCliente, Me.grcRfc})
        Me.grvClientes.GridControl = Me.grClientes
        Me.grvClientes.Name = "grvClientes"
        Me.grvClientes.OptionsCustomization.AllowFilter = False
        Me.grvClientes.OptionsCustomization.AllowGroup = False
        Me.grvClientes.OptionsView.ShowGroupPanel = False
        Me.grvClientes.OptionsView.ShowIndicator = False
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.VisibleIndex = 0
        Me.grcCliente.Width = 53
        '
        'grcNombreCliente
        '
        Me.grcNombreCliente.Caption = "Nombre"
        Me.grcNombreCliente.FieldName = "nombre"
        Me.grcNombreCliente.Name = "grcNombreCliente"
        Me.grcNombreCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreCliente.VisibleIndex = 1
        Me.grcNombreCliente.Width = 172
        '
        'grcRfc
        '
        Me.grcRfc.Caption = "RFC"
        Me.grcRfc.FieldName = "rfc"
        Me.grcRfc.Name = "grcRfc"
        Me.grcRfc.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcRfc.VisibleIndex = 2
        Me.grcRfc.Width = 85
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 17)
        Me.Label2.TabIndex = 71
        Me.Label2.Text = "C&LIENTES"
        '
        'pnlVendedores
        '
        Me.pnlVendedores.Controls.Add(Me.gpbVendedores)
        Me.pnlVendedores.Controls.Add(Me.grVendedores)
        Me.pnlVendedores.Controls.Add(Me.Label1)
        Me.pnlVendedores.Location = New System.Drawing.Point(144, 8)
        Me.pnlVendedores.Name = "pnlVendedores"
        Me.pnlVendedores.Size = New System.Drawing.Size(632, 448)
        Me.pnlVendedores.TabIndex = 2
        Me.pnlVendedores.Visible = False
        '
        'gpbVendedores
        '
        Me.gpbVendedores.Controls.Add(Me.chkActivoVendedor)
        Me.gpbVendedores.Controls.Add(Me.clcMeta)
        Me.gpbVendedores.Controls.Add(Me.clcVendedor)
        Me.gpbVendedores.Controls.Add(Me.txtNombreVendedor)
        Me.gpbVendedores.Controls.Add(Me.clcSueldo)
        Me.gpbVendedores.Controls.Add(Me.Label6)
        Me.gpbVendedores.Controls.Add(Me.lblSueldo)
        Me.gpbVendedores.Controls.Add(Me.lblVendedor)
        Me.gpbVendedores.Controls.Add(Me.Label4)
        Me.gpbVendedores.Controls.Add(Me.cmdGuardarVendedor)
        Me.gpbVendedores.Controls.Add(Me.cmdActualizarVendedor)
        Me.gpbVendedores.Controls.Add(Me.cmdCancelarVendedor)
        Me.gpbVendedores.Controls.Add(Me.cmdEliminarVendedor)
        Me.gpbVendedores.Location = New System.Drawing.Point(328, 16)
        Me.gpbVendedores.Name = "gpbVendedores"
        Me.gpbVendedores.Size = New System.Drawing.Size(288, 176)
        Me.gpbVendedores.TabIndex = 77
        Me.gpbVendedores.TabStop = False
        '
        'chkActivoVendedor
        '
        Me.chkActivoVendedor.Location = New System.Drawing.Point(72, 152)
        Me.chkActivoVendedor.Name = "chkActivoVendedor"
        '
        'chkActivoVendedor.Properties
        '
        Me.chkActivoVendedor.Properties.Caption = "Activo"
        Me.chkActivoVendedor.Properties.Enabled = False
        Me.chkActivoVendedor.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkActivoVendedor.Size = New System.Drawing.Size(72, 19)
        Me.chkActivoVendedor.TabIndex = 89
        Me.chkActivoVendedor.Tag = "activo"
        '
        'clcMeta
        '
        Me.clcMeta.EditValue = "0"
        Me.clcMeta.Location = New System.Drawing.Point(64, 127)
        Me.clcMeta.MaxValue = 0
        Me.clcMeta.MinValue = 0
        Me.clcMeta.Name = "clcMeta"
        '
        'clcMeta.Properties
        '
        Me.clcMeta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMeta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMeta.Properties.Enabled = False
        Me.clcMeta.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcMeta.Properties.Precision = 2
        Me.clcMeta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMeta.Size = New System.Drawing.Size(70, 19)
        Me.clcMeta.TabIndex = 87
        Me.clcMeta.Tag = "meta"
        '
        'clcVendedor
        '
        Me.clcVendedor.EditValue = "0"
        Me.clcVendedor.Location = New System.Drawing.Point(64, 55)
        Me.clcVendedor.MaxValue = 0
        Me.clcVendedor.MinValue = 0
        Me.clcVendedor.Name = "clcVendedor"
        '
        'clcVendedor.Properties
        '
        Me.clcVendedor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcVendedor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcVendedor.Properties.Enabled = False
        Me.clcVendedor.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcVendedor.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcVendedor.Size = New System.Drawing.Size(46, 19)
        Me.clcVendedor.TabIndex = 84
        Me.clcVendedor.Tag = "vendedor"
        '
        'txtNombreVendedor
        '
        Me.txtNombreVendedor.EditValue = ""
        Me.txtNombreVendedor.Location = New System.Drawing.Point(64, 78)
        Me.txtNombreVendedor.Name = "txtNombreVendedor"
        '
        'txtNombreVendedor.Properties
        '
        Me.txtNombreVendedor.Properties.Enabled = False
        Me.txtNombreVendedor.Properties.MaxLength = 60
        Me.txtNombreVendedor.Size = New System.Drawing.Size(216, 20)
        Me.txtNombreVendedor.TabIndex = 85
        Me.txtNombreVendedor.Tag = "nombre"
        '
        'clcSueldo
        '
        Me.clcSueldo.EditValue = "0"
        Me.clcSueldo.Location = New System.Drawing.Point(64, 103)
        Me.clcSueldo.MaxValue = 0
        Me.clcSueldo.MinValue = 0
        Me.clcSueldo.Name = "clcSueldo"
        '
        'clcSueldo.Properties
        '
        Me.clcSueldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSueldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSueldo.Properties.Enabled = False
        Me.clcSueldo.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSueldo.Properties.Precision = 2
        Me.clcSueldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSueldo.Size = New System.Drawing.Size(70, 19)
        Me.clcSueldo.TabIndex = 86
        Me.clcSueldo.Tag = "sueldo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(29, 128)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 16)
        Me.Label6.TabIndex = 81
        Me.Label6.Tag = ""
        Me.Label6.Text = "&Meta:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSueldo
        '
        Me.lblSueldo.AutoSize = True
        Me.lblSueldo.Location = New System.Drawing.Point(18, 104)
        Me.lblSueldo.Name = "lblSueldo"
        Me.lblSueldo.Size = New System.Drawing.Size(43, 16)
        Me.lblSueldo.TabIndex = 79
        Me.lblSueldo.Tag = ""
        Me.lblSueldo.Text = "&Sueldo:"
        Me.lblSueldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(5, 56)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(56, 16)
        Me.lblVendedor.TabIndex = 72
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "Ven&dedor:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 74
        Me.Label4.Tag = ""
        Me.Label4.Text = "&Nombre:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdGuardarVendedor
        '
        Me.cmdGuardarVendedor.Location = New System.Drawing.Point(24, 16)
        Me.cmdGuardarVendedor.Name = "cmdGuardarVendedor"
        Me.cmdGuardarVendedor.TabIndex = 77
        Me.cmdGuardarVendedor.Text = "Guardar"
        Me.cmdGuardarVendedor.Visible = False
        '
        'cmdActualizarVendedor
        '
        Me.cmdActualizarVendedor.Location = New System.Drawing.Point(24, 16)
        Me.cmdActualizarVendedor.Name = "cmdActualizarVendedor"
        Me.cmdActualizarVendedor.TabIndex = 77
        Me.cmdActualizarVendedor.Text = "Actualizar"
        '
        'cmdCancelarVendedor
        '
        Me.cmdCancelarVendedor.Location = New System.Drawing.Point(104, 16)
        Me.cmdCancelarVendedor.Name = "cmdCancelarVendedor"
        Me.cmdCancelarVendedor.TabIndex = 77
        Me.cmdCancelarVendedor.Text = "Cancelar"
        Me.cmdCancelarVendedor.Visible = False
        '
        'cmdEliminarVendedor
        '
        Me.cmdEliminarVendedor.Location = New System.Drawing.Point(104, 16)
        Me.cmdEliminarVendedor.Name = "cmdEliminarVendedor"
        Me.cmdEliminarVendedor.TabIndex = 77
        Me.cmdEliminarVendedor.Text = "Eliminar"
        '
        'grVendedores
        '
        '
        'grVendedores.EmbeddedNavigator
        '
        Me.grVendedores.EmbeddedNavigator.Name = ""
        Me.grVendedores.Location = New System.Drawing.Point(8, 24)
        Me.grVendedores.MainView = Me.grvVendedores
        Me.grVendedores.Name = "grVendedores"
        Me.grVendedores.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkActivo})
        Me.grVendedores.Size = New System.Drawing.Size(312, 408)
        Me.grVendedores.TabIndex = 72
        Me.grVendedores.Text = "Vendedores"
        '
        'grvVendedores
        '
        Me.grvVendedores.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcVendedor, Me.grcNombreVendedor, Me.grcSueldoVendedor, Me.grcSucursal, Me.grcNombreSucursal, Me.grcMeta, Me.grcActivo})
        Me.grvVendedores.GridControl = Me.grVendedores
        Me.grvVendedores.Name = "grvVendedores"
        Me.grvVendedores.OptionsCustomization.AllowFilter = False
        Me.grvVendedores.OptionsCustomization.AllowGroup = False
        Me.grvVendedores.OptionsCustomization.AllowSort = False
        Me.grvVendedores.OptionsView.ShowGroupPanel = False
        Me.grvVendedores.OptionsView.ShowIndicator = False
        '
        'grcVendedor
        '
        Me.grcVendedor.Caption = "Vendedor"
        Me.grcVendedor.FieldName = "vendedor"
        Me.grcVendedor.Name = "grcVendedor"
        Me.grcVendedor.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcVendedor.VisibleIndex = 0
        Me.grcVendedor.Width = 61
        '
        'grcNombreVendedor
        '
        Me.grcNombreVendedor.Caption = "Nombre"
        Me.grcNombreVendedor.FieldName = "nombre"
        Me.grcNombreVendedor.Name = "grcNombreVendedor"
        Me.grcNombreVendedor.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreVendedor.VisibleIndex = 1
        Me.grcNombreVendedor.Width = 52
        '
        'grcSueldoVendedor
        '
        Me.grcSueldoVendedor.Caption = "Sueldo"
        Me.grcSueldoVendedor.DisplayFormat.FormatString = "$###,##0.00"
        Me.grcSueldoVendedor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSueldoVendedor.FieldName = "sueldo"
        Me.grcSueldoVendedor.Name = "grcSueldoVendedor"
        Me.grcSueldoVendedor.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSueldoVendedor.VisibleIndex = 2
        Me.grcSueldoVendedor.Width = 48
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursal.VisibleIndex = 3
        Me.grcNombreSucursal.Width = 56
        '
        'grcMeta
        '
        Me.grcMeta.Caption = "Meta"
        Me.grcMeta.DisplayFormat.FormatString = "$###,##0.00"
        Me.grcMeta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcMeta.FieldName = "meta"
        Me.grcMeta.Name = "grcMeta"
        Me.grcMeta.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcMeta.VisibleIndex = 4
        Me.grcMeta.Width = 37
        '
        'grcActivo
        '
        Me.grcActivo.Caption = "Activo"
        Me.grcActivo.ColumnEdit = Me.chkActivo
        Me.grcActivo.FieldName = "activo"
        Me.grcActivo.Name = "grcActivo"
        Me.grcActivo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcActivo.VisibleIndex = 5
        Me.grcActivo.Width = 20
        '
        'chkActivo
        '
        Me.chkActivo.AutoHeight = False
        Me.chkActivo.Name = "chkActivo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 17)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "&VENDEDORES"
        '
        'pnlCalcular
        '
        Me.pnlCalcular.Controls.Add(Me.cmdCalcular)
        Me.pnlCalcular.Controls.Add(Me.gpbCalcular)
        Me.pnlCalcular.Controls.Add(Me.cmdReportes)
        Me.pnlCalcular.Location = New System.Drawing.Point(144, 8)
        Me.pnlCalcular.Name = "pnlCalcular"
        Me.pnlCalcular.Size = New System.Drawing.Size(632, 448)
        Me.pnlCalcular.TabIndex = 3
        Me.pnlCalcular.Visible = False
        '
        'cmdCalcular
        '
        Me.cmdCalcular.Enabled = False
        Me.cmdCalcular.Location = New System.Drawing.Point(400, 40)
        Me.cmdCalcular.Name = "cmdCalcular"
        Me.cmdCalcular.Size = New System.Drawing.Size(88, 23)
        Me.cmdCalcular.TabIndex = 78
        Me.cmdCalcular.Text = "Calcular"
        '
        'gpbCalcular
        '
        Me.gpbCalcular.Controls.Add(Me.chkDevoluciones)
        Me.gpbCalcular.Controls.Add(Me.gpbPorcentajes)
        Me.gpbCalcular.Controls.Add(Me.lblDesde)
        Me.gpbCalcular.Controls.Add(Me.dteDesde)
        Me.gpbCalcular.Controls.Add(Me.dteHasta)
        Me.gpbCalcular.Controls.Add(Me.lblHasta)
        Me.gpbCalcular.Controls.Add(Me.lkpSucursal)
        Me.gpbCalcular.Controls.Add(Me.lblSucursal)
        Me.gpbCalcular.Location = New System.Drawing.Point(48, 32)
        Me.gpbCalcular.Name = "gpbCalcular"
        Me.gpbCalcular.Size = New System.Drawing.Size(336, 192)
        Me.gpbCalcular.TabIndex = 0
        Me.gpbCalcular.TabStop = False
        Me.gpbCalcular.Text = "Valores para C�lculo"
        '
        'chkDevoluciones
        '
        Me.chkDevoluciones.EditValue = "False"
        Me.chkDevoluciones.Location = New System.Drawing.Point(184, 160)
        Me.chkDevoluciones.Name = "chkDevoluciones"
        '
        'chkDevoluciones.Properties
        '
        Me.chkDevoluciones.Properties.Caption = "Incluir &Devoluciones"
        Me.chkDevoluciones.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkDevoluciones.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkDevoluciones.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkDevoluciones.Size = New System.Drawing.Size(128, 19)
        Me.chkDevoluciones.TabIndex = 74
        Me.chkDevoluciones.Tag = "devoluciones"
        '
        'gpbPorcentajes
        '
        Me.gpbPorcentajes.Controls.Add(Me.clcHasta13)
        Me.gpbPorcentajes.Controls.Add(Me.lbl13meses)
        Me.gpbPorcentajes.Controls.Add(Me.Label3)
        Me.gpbPorcentajes.Controls.Add(Me.clcMas13)
        Me.gpbPorcentajes.Location = New System.Drawing.Point(16, 72)
        Me.gpbPorcentajes.Name = "gpbPorcentajes"
        Me.gpbPorcentajes.Size = New System.Drawing.Size(296, 80)
        Me.gpbPorcentajes.TabIndex = 69
        Me.gpbPorcentajes.TabStop = False
        Me.gpbPorcentajes.Text = "Porcentajes:"
        '
        'clcHasta13
        '
        Me.clcHasta13.EditValue = "0"
        Me.clcHasta13.Location = New System.Drawing.Point(112, 26)
        Me.clcHasta13.MaxValue = 0
        Me.clcHasta13.MinValue = 0
        Me.clcHasta13.Name = "clcHasta13"
        '
        'clcHasta13.Properties
        '
        Me.clcHasta13.Properties.DisplayFormat.FormatString = "n2"
        Me.clcHasta13.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcHasta13.Properties.EditFormat.FormatString = "n2"
        Me.clcHasta13.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcHasta13.Properties.Precision = 2
        Me.clcHasta13.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcHasta13.Size = New System.Drawing.Size(72, 19)
        Me.clcHasta13.TabIndex = 71
        Me.clcHasta13.Tag = "documentos"
        '
        'lbl13meses
        '
        Me.lbl13meses.AutoSize = True
        Me.lbl13meses.Location = New System.Drawing.Point(16, 27)
        Me.lbl13meses.Name = "lbl13meses"
        Me.lbl13meses.Size = New System.Drawing.Size(89, 16)
        Me.lbl13meses.TabIndex = 70
        Me.lbl13meses.Tag = ""
        Me.lbl13meses.Text = "Hasta 13 Meses:"
        Me.lbl13meses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 72
        Me.Label3.Tag = ""
        Me.Label3.Text = "M�s de 13 Meses:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMas13
        '
        Me.clcMas13.EditValue = "0"
        Me.clcMas13.Location = New System.Drawing.Point(112, 48)
        Me.clcMas13.MaxValue = 0
        Me.clcMas13.MinValue = 0
        Me.clcMas13.Name = "clcMas13"
        '
        'clcMas13.Properties
        '
        Me.clcMas13.Properties.DisplayFormat.FormatString = "n2"
        Me.clcMas13.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMas13.Properties.EditFormat.FormatString = "n2"
        Me.clcMas13.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMas13.Properties.MaskData.EditMask = "0"
        Me.clcMas13.Properties.Precision = 2
        Me.clcMas13.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMas13.Size = New System.Drawing.Size(72, 19)
        Me.clcMas13.TabIndex = 73
        Me.clcMas13.Tag = "documentos"
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(24, 24)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(40, 16)
        Me.lblDesde.TabIndex = 64
        Me.lblDesde.Tag = ""
        Me.lblDesde.Text = "Desde:"
        Me.lblDesde.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteDesde
        '
        Me.dteDesde.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteDesde.Location = New System.Drawing.Point(64, 22)
        Me.dteDesde.Name = "dteDesde"
        '
        'dteDesde.Properties
        '
        Me.dteDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteDesde.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteDesde.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteDesde.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteDesde.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteDesde.Size = New System.Drawing.Size(96, 23)
        Me.dteDesde.TabIndex = 65
        Me.dteDesde.Tag = ""
        '
        'dteHasta
        '
        Me.dteHasta.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteHasta.Location = New System.Drawing.Point(216, 22)
        Me.dteHasta.Name = "dteHasta"
        '
        'dteHasta.Properties
        '
        Me.dteHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteHasta.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteHasta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteHasta.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteHasta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteHasta.Size = New System.Drawing.Size(96, 23)
        Me.dteHasta.TabIndex = 67
        Me.dteHasta.Tag = ""
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(176, 24)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(37, 16)
        Me.lblHasta.TabIndex = 66
        Me.lblHasta.Tag = ""
        Me.lblHasta.Text = "Hasta:"
        Me.lblHasta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(64, 48)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = True
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(248, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 68
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(13, 48)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 16)
        Me.lblSucursal.TabIndex = 68
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdReportes
        '
        Me.cmdReportes.Location = New System.Drawing.Point(400, 72)
        Me.cmdReportes.Name = "cmdReportes"
        Me.cmdReportes.Size = New System.Drawing.Size(88, 23)
        Me.cmdReportes.TabIndex = 79
        Me.cmdReportes.Text = "Reportes"
        '
        'frmCalculoComisionesVendedores
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(776, 454)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.pnlVendedores)
        Me.Controls.Add(Me.pnlCalcular)
        Me.Controls.Add(Me.pnlExclusiones)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmCalculoComisionesVendedores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "C�lculo de Comisiones de Vendedores"
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlExclusiones.ResumeLayout(False)
        Me.gpbClientes.ResumeLayout(False)
        CType(Me.chkNoPagaComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlVendedores.ResumeLayout(False)
        Me.gpbVendedores.ResumeLayout(False)
        CType(Me.chkActivoVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMeta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSueldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grVendedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvVendedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActivo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCalcular.ResumeLayout(False)
        Me.gpbCalcular.ResumeLayout(False)
        CType(Me.chkDevoluciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbPorcentajes.ResumeLayout(False)
        CType(Me.clcHasta13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMas13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oSucursales As VillarrealBusiness.clsSucursales '@JGTO-18/04/2008: Se a�ade filtro

    Private oClientes As New VillarrealBusiness.clsClientes
    Private oVendedores As New VillarrealBusiness.clsVendedores

    Private lngCliente As Long
    Private lngSucursalVendedor As Long

    Private filaCliente As Long
    Private moverFilaCliente As Boolean = True


    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCalculoComisionesVendedores_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        oSucursales = New VillarrealBusiness.clsSucursales '@JGTO-18/04/2008: Se a�ade filtro

        Me.clcHasta13.EditValue = 0.0
        Me.clcMas13.EditValue = 0.0
        Me.pnlExclusiones.BringToFront()
    End Sub

    Private Sub NavBarItem1_LinkClicked(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem1.LinkClicked
        Me.pnlVendedores.Visible = False
        Me.pnlCalcular.Visible = False
        Me.pnlExclusiones.Visible = True


        CargarClientesNoPaganComisiones()
    End Sub
    Private Sub NavBarItem2_LinkClicked(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem2.LinkClicked
        Me.pnlExclusiones.Visible = False
        Me.pnlCalcular.Visible = False
        Me.pnlVendedores.Visible = True

        CargarVendedores()
    End Sub
    Private Sub NavBarItem3_LinkClicked(ByVal sender As Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem3.LinkClicked
        Me.pnlExclusiones.Visible = False
        Me.pnlVendedores.Visible = False
        Me.pnlCalcular.Visible = True

        Me.dteHasta.EditValue = CDate(TINApp.FechaServidor)
        Me.dteDesde.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    '@JGTO-18/04/2008: Se incluye filtro por sucursal
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.ClientesNoPaganComisiones(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        If Me.lkpCliente.EditValue Is Nothing Then Exit Sub

        lngCliente = CLng(Me.lkpCliente.EditValue)
        Me.txtNombres.Text = Me.lkpCliente.GetValue("nombre")
    End Sub

    Private Sub grvClientes_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvClientes.FocusedRowChanged
        lngCliente = Me.grvClientes.GetRowCellValue(e.FocusedRowHandle, Me.grcCliente)
        Me.lkpCliente.EditValue = Me.grvClientes.GetRowCellValue(e.FocusedRowHandle, Me.grcCliente)
        Me.txtNombres.Text = Me.grvClientes.GetRowCellValue(e.FocusedRowHandle, Me.grcNombreCliente)
        Me.chkNoPagaComision.Checked = True

        Me.lkpCliente.Enabled = False
        Me.txtNombres.Enabled = False
        Me.chkNoPagaComision.Enabled = False

        If moverFilaCliente = True Then
            filaCliente = e.FocusedRowHandle
        End If
    End Sub
    Private Sub grvVendedores_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvVendedores.FocusedRowChanged

        Me.clcVendedor.EditValue = Me.grvVendedores.GetRowCellValue(e.FocusedRowHandle, Me.grcVendedor)
        Me.txtNombreVendedor.Text = Me.grvVendedores.GetRowCellValue(e.FocusedRowHandle, Me.grcNombreVendedor)
        Me.clcSueldo.EditValue = Me.grvVendedores.GetRowCellValue(e.FocusedRowHandle, Me.grcSueldoVendedor)
        Me.clcMeta.EditValue = Me.grvVendedores.GetRowCellValue(e.FocusedRowHandle, Me.grcMeta)
        Me.chkActivoVendedor.Checked = Me.grvVendedores.GetRowCellValue(e.FocusedRowHandle, Me.grcActivo)

        lngSucursalVendedor = Me.grvVendedores.GetRowCellValue(e.FocusedRowHandle, Me.grcSucursal)

    End Sub

    Private Sub cmdInsertar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsertar.Click
        Me.lkpCliente.Enabled = True
        Me.lkpCliente.EditValue = Nothing
        Me.txtNombres.Text = ""

        Me.cmdInsertar.Enabled = False
        Me.cmdInsertar.Visible = False
        Me.cmdActualizar.Enabled = False
        Me.cmdActualizar.Visible = False
        Me.cmdGuardar.Enabled = True
        Me.cmdGuardar.Visible = True
        Me.cmdCancelar.Enabled = True
        Me.cmdCancelar.Visible = True

        Me.grClientes.Enabled = False

    End Sub
    Private Sub cmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActualizar.Click

        Me.chkNoPagaComision.Checked = False

        Me.cmdInsertar.Enabled = False
        Me.cmdInsertar.Visible = False
        Me.cmdActualizar.Enabled = False
        Me.cmdActualizar.Visible = False
        Me.cmdGuardar.Enabled = True
        Me.cmdGuardar.Visible = True
        Me.cmdCancelar.Enabled = True
        Me.cmdCancelar.Visible = True

        Me.grClientes.Enabled = False
    End Sub
    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        Dim Response As New Events

        Me.cmdInsertar.Enabled = True
        Me.cmdInsertar.Visible = True
        Me.cmdActualizar.Enabled = True
        Me.cmdActualizar.Visible = True
        Me.cmdGuardar.Enabled = False
        Me.cmdGuardar.Visible = False
        Me.cmdCancelar.Enabled = False
        Me.cmdCancelar.Visible = False

        Response = oClientes.ActualizaNoPagaComision(lngCliente, Me.chkNoPagaComision.Checked)
        If Response.ErrorFound Then
            Response.Ex = Nothing
            Response.Message = "No se pudo Actualizar el Cliente"
            Response.ShowError()
        End If

        Me.lkpCliente.Enabled = False
        Me.chkNoPagaComision.Checked = True

        CargarClientesNoPaganComisiones()
        moverFilaCliente = False
        Me.grvClientes.MoveLast()
        Me.grvClientes.MoveFirst()
        moverFilaCliente = True
        Me.grClientes.Focus()

        Me.grvClientes.MoveBy(filaCliente)

        Me.lkpCliente_LoadData(True)
        Me.grClientes.Enabled = True
    End Sub
    Private Sub cmdCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click

        Me.cmdInsertar.Enabled = True
        Me.cmdInsertar.Visible = True
        Me.cmdActualizar.Enabled = True
        Me.cmdActualizar.Visible = True
        Me.cmdGuardar.Enabled = False
        Me.cmdGuardar.Visible = False
        Me.cmdCancelar.Enabled = False
        Me.cmdCancelar.Visible = False

        Me.lkpCliente.Enabled = False
        Me.chkNoPagaComision.Checked = True

        moverFilaCliente = False
        Me.grvClientes.MoveLast()
        Me.grvClientes.MoveFirst()
        moverFilaCliente = True
        Me.grvClientes.MoveBy(filaCliente)

        Me.grClientes.Focus()

        Me.grClientes.Enabled = True
    End Sub
    Private Sub grvClientes_EndSorting(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvClientes.EndSorting
        If Me.grvClientes.RowCount > 0 Then
            Me.grvClientes.MoveBy(filaCliente)
        End If


    End Sub

    Private Sub cmdActualizarVendedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActualizarVendedor.Click
        Me.cmdActualizarVendedor.Visible = False
        Me.cmdEliminarVendedor.Visible = False
        Me.cmdGuardarVendedor.Visible = True
        Me.cmdCancelarVendedor.Visible = True

        Me.clcSueldo.Enabled = True
        Me.clcMeta.Enabled = True
        Me.chkActivoVendedor.Enabled = True

        Me.grVendedores.Enabled = False
    End Sub
    Private Sub cmdCancelarVendedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelarVendedor.Click
        Me.cmdActualizarVendedor.Visible = True
        Me.cmdEliminarVendedor.Visible = True
        Me.cmdGuardarVendedor.Visible = False
        Me.cmdCancelarVendedor.Visible = False

        Me.clcSueldo.Enabled = False
        Me.clcMeta.Enabled = False
        Me.chkActivoVendedor.Enabled = False

        Me.grvVendedores.MoveLast()
        Me.grvVendedores.MoveFirst()
        Me.grVendedores.Focus()

        Me.grVendedores.Enabled = True

    End Sub
    Private Sub cmdGuardarVendedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardarVendedor.Click
        Dim Response As New Events

        Me.cmdActualizarVendedor.Visible = True
        Me.cmdEliminarVendedor.Visible = True
        Me.cmdGuardarVendedor.Visible = False
        Me.cmdCancelarVendedor.Visible = False

        Response = oVendedores.Actualizar(Me.clcVendedor.EditValue, Me.txtNombreVendedor.Text, Me.clcSueldo.EditValue, Me.lngSucursalVendedor, Me.clcMeta.EditValue, Me.chkActivoVendedor.Checked, "", -1)
        If Response.ErrorFound Then
            Response.Ex = Nothing
            Response.Message = "No se pudo Actualizar el Vendedor"
            Response.ShowError()
        End If

        Me.clcSueldo.Enabled = False
        Me.clcMeta.Enabled = False
        Me.chkActivoVendedor.Enabled = False

        CargarVendedores()
        Me.grvVendedores.MoveLast()
        Me.grvVendedores.MoveFirst()
        Me.grVendedores.Focus()

        Me.grVendedores.Enabled = True

    End Sub
    Private Sub cmdEliminarVendedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEliminarVendedor.Click

        If ShowMessage(MessageType.MsgQuestion, "�Est� seguro de Eliminar este Vendedor?") = Answer.MsgYes Then
            Dim Response As New Events
            Response = oVendedores.Eliminar(Me.clcVendedor.EditValue)
            If Response.ErrorFound Then
                Response.Ex = Nothing
                Response.Message = "No se pudo Eliminar el Vendedor"
                Response.ShowError()
            End If

            CargarVendedores()
            Me.grvVendedores.MoveLast()
            Me.grvVendedores.MoveFirst()
            Me.grVendedores.Focus()

            Response = Nothing
        End If

    End Sub

    Private Sub cmdReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportes.Click
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim Response As New Events

        Response = oReportes.Validacion(Me.dteDesde.Text, Me.dteHasta.Text)
        If Response.ErrorFound Then
            Response.ShowError()
            Exit Sub
        End If


        Response = oReportes.InformeDeVentas(Me.dteDesde.DateTime, _
           Me.dteHasta.DateTime, Me.clcHasta13.EditValue, _
           Me.clcMas13.EditValue, Me.chkDevoluciones.Checked, _
           Sucursal)

        If Me.chkDevoluciones.Checked = False Then

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Los Reportes no se Pueden Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then

                    Dim oReport As New rptInformeDeVentas
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Informe de Ventas", oReport)
                    oReport = Nothing

                    If ComprobarMesCompleto() = True Then
                        Dim oReport2 As New rptReporteDeComisiones
                        oReport2.DataSource = oDataSet.Tables(0)
                        TINApp.ShowReport(Me.MdiParent, "Reporte de Comisiones", oReport2)
                        oReport2 = Nothing
                    End If

                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If
        Else

            ' Response = oReportes.InformeDeVentas(Me.dteDesde.DateTime, _
            ' Me.dteHasta.DateTime, Me.clcHasta13.EditValue, _
            ' Me.clcMas13.EditValue, Me.chkDevoluciones.Checked, _
            'Sucursal)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Los Reportes no se Pueden Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then


                    Dim oReport As New rptInformeDeVentas
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Informe de Ventas", oReport)
                    oReport = Nothing

                    Dim oReport2 As New rptInformeDeVentasYDevoluciones
                    oReport2.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Informe de Devoluciones ", oReport2)
                    oReport2 = Nothing

                    If ComprobarMesCompleto() = True Then
                        Dim oReport3 As New rptReporteDeComisionesConDevoluciones
                        oReport3.DataSource = oDataSet.Tables(0)
                        TINApp.ShowReport(Me.MdiParent, "Reporte de Comisiones", oReport3)
                        oReport3 = Nothing
                    End If

                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If

        End If

        Response = Nothing
        oReportes = Nothing

    End Sub
    Private Sub cmdCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCalcular.Click

        '@JGTO-18/04/2008: Se valida la sucursal
        Dim oEvent As Dipros.Utils.Events
        oEvent = oVendedores.ValidaSucursal(lkpSucursal.EditValue)
        If oEvent.ErrorFound Then
            oEvent.ShowError()
            lkpSucursal.Focus()
            Exit Sub
        End If

        If ShowMessage(MessageType.MsgQuestion, "Se Guardar�n las Comisiones, �Desea Continuar?") = Answer.MsgYes Then
            GuardarComisiones()
        End If

    End Sub

    Private Sub clcHasta13_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcHasta13.EditValueChanged
        If Me.clcHasta13.EditValue Is Nothing Then Exit Sub

        If Not IsNumeric(Me.clcHasta13.EditValue) Then Me.clcHasta13.EditValue = 0.0
    End Sub
    Private Sub clcMas13_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcMas13.EditValueChanged
        If Me.clcMas13.EditValue Is Nothing Then Exit Sub

        If Not IsNumeric(Me.clcMas13.EditValue) Then Me.clcMas13.EditValue = 0.0
    End Sub

    Private Sub dteDesde_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteDesde.EditValueChanged
        If Me.dteDesde.EditValue Is Nothing Then Exit Sub

        If ComprobarMesCompleto() = True Then
            Me.cmdCalcular.Enabled = True
        Else
            Me.cmdCalcular.Enabled = False
        End If
    End Sub
    Private Sub dteHasta_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteHasta.EditValueChanged
        If Me.dteHasta.EditValue Is Nothing Then Exit Sub

        If ComprobarMesCompleto() = True Then
            Me.cmdCalcular.Enabled = True
        Else
            Me.cmdCalcular.Enabled = False
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarClientesNoPaganComisiones()
        Dim Response As New Events

        Response = oClientes.ClientesNoPaganComisiones(True)

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.grClientes.DataSource = oDataSet.Tables(0)
        Else
            Response.ShowError()
        End If

        Response = Nothing
    End Sub
    Private Sub CargarVendedores()
        Dim Response As New Events

        Response = oVendedores.Listado

        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.grVendedores.DataSource = oDataSet.Tables(0)
        Else
            Response.ShowError()
        End If

        Response = Nothing
    End Sub
    Private Sub GuardarComisiones()
        Dim Response As New Events
        Dim oVendedores As New VillarrealBusiness.clsVendedores

        Response = oVendedores.GuardarComisiones( _
        Me.dteDesde.DateTime, Me.dteHasta.DateTime, Me.clcHasta13.EditValue, _
        Me.clcMas13.EditValue, lkpSucursal.EditValue)

        If Response.ErrorFound Then
            Response.ShowError()
            Exit Sub
        Else
            ShowMessage(MessageType.MsgInformation, "Las Comisiones Han Sido Guardadas")
        End If

        Response = Nothing
        oVendedores = Nothing
    End Sub
    Private Function ComprobarMesCompleto() As Boolean
        Dim mesDesde As Integer
        Dim mesHasta As Integer
        Dim anioDesde As Integer
        Dim anioHasta As Integer
        Dim diaDesde As Integer
        Dim diaHasta As Integer
        Dim ultimoDiaMes As Integer

        mesDesde = Me.dteDesde.DateTime.Month
        mesHasta = Me.dteHasta.DateTime.Month
        anioDesde = Me.dteDesde.DateTime.Year
        anioHasta = Me.dteHasta.DateTime.Year
        diaDesde = Me.dteDesde.DateTime.Day
        diaHasta = Me.dteHasta.DateTime.Day
        ultimoDiaMes = Date.DaysInMonth(Me.dteDesde.DateTime.Year, Me.dteDesde.DateTime.Month)

        If anioDesde = anioHasta And mesDesde = mesHasta And diaDesde = 1 And diaHasta = ultimoDiaMes Then
            Return True
        Else
            Return False
        End If

    End Function


#End Region



End Class

