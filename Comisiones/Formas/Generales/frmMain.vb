Imports Dipros.Utils.Common
Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuProRepartos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuUtiVariables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuCatVendedores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatExclusiones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepInformeVentas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepComisionesVentas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatConfiguracionComisiones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatEsquemasComisiones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepComisionNetaPagar As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuProRepartos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuUtiVariables = New DevExpress.XtraBars.BarButtonItem
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuCatVendedores = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatExclusiones = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepInformeVentas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepComisionesVentas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatConfiguracionComisiones = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatEsquemasComisiones = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepComisionNetaPagar = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuProRepartos, Me.mnuUtiVariables, Me.BarStaticItem1, Me.mnuCatVendedores, Me.mnuCatExclusiones, Me.mnuRepInformeVentas, Me.mnuRepComisionesVentas, Me.mnuCatConfiguracionComisiones, Me.mnuCatEsquemasComisiones, Me.mnuRepComisionNetaPagar})
        Me.BarManager.MaxItemId = 127
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiVariables, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatVendedores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatExclusiones), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatConfiguracionComisiones), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatEsquemasComisiones)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepInformeVentas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepComisionesVentas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepComisionNetaPagar)})
        '
        'mnuProRepartos
        '
        Me.mnuProRepartos.Caption = "Re&partos"
        Me.mnuProRepartos.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProRepartos.Id = 111
        Me.mnuProRepartos.ImageIndex = 3
        Me.mnuProRepartos.Name = "mnuProRepartos"
        '
        'mnuUtiVariables
        '
        Me.mnuUtiVariables.Caption = "Variables del Sistema"
        Me.mnuUtiVariables.CategoryGuid = New System.Guid("e67dee89-e6ea-421f-9dfc-aa13a54292da")
        Me.mnuUtiVariables.Id = 115
        Me.mnuUtiVariables.Name = "mnuUtiVariables"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 116
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'mnuCatVendedores
        '
        Me.mnuCatVendedores.Caption = "Vendedores"
        Me.mnuCatVendedores.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatVendedores.Id = 120
        Me.mnuCatVendedores.Name = "mnuCatVendedores"
        '
        'mnuCatExclusiones
        '
        Me.mnuCatExclusiones.Caption = "Exclusiones"
        Me.mnuCatExclusiones.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatExclusiones.Id = 121
        Me.mnuCatExclusiones.Name = "mnuCatExclusiones"
        '
        'mnuRepInformeVentas
        '
        Me.mnuRepInformeVentas.Caption = "Informe de Ventas"
        Me.mnuRepInformeVentas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepInformeVentas.Id = 122
        Me.mnuRepInformeVentas.Name = "mnuRepInformeVentas"
        '
        'mnuRepComisionesVentas
        '
        Me.mnuRepComisionesVentas.Caption = "Comisiones de Ventas"
        Me.mnuRepComisionesVentas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepComisionesVentas.Id = 123
        Me.mnuRepComisionesVentas.Name = "mnuRepComisionesVentas"
        '
        'mnuCatConfiguracionComisiones
        '
        Me.mnuCatConfiguracionComisiones.Caption = "Configurador de Comisiones"
        Me.mnuCatConfiguracionComisiones.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatConfiguracionComisiones.Id = 124
        Me.mnuCatConfiguracionComisiones.Name = "mnuCatConfiguracionComisiones"
        '
        'mnuCatEsquemasComisiones
        '
        Me.mnuCatEsquemasComisiones.Caption = "Esquemas de Comisiones"
        Me.mnuCatEsquemasComisiones.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatEsquemasComisiones.Id = 125
        Me.mnuCatEsquemasComisiones.Name = "mnuCatEsquemasComisiones"
        '
        'mnuRepComisionNetaPagar
        '
        Me.mnuRepComisionNetaPagar.Caption = "Comisiones Netas a Pagar"
        Me.mnuRepComisionNetaPagar.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepComisionNetaPagar.Id = 126
        Me.mnuRepComisionNetaPagar.Name = "mnuRepComisionNetaPagar"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"


    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim osucursales As New VillarrealBusiness.clsSucursales
        'Dim obodegas As New VillarrealBusiness.clsBodegas
        'Dim response As Dipros.Utils.Events
        'Dim response2 As Dipros.Utils.Events
        'Dim odataset As DataSet
        'Dim odataset2 As DataSet

        'Try
        '    response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        '    If Not response.ErrorFound Then
        '        odataset = response.Value
        '    End If

        '    response2 = obodegas.DespliegaDatos(Comunes.Common.Bodega_Actual)
        '    If Not response2.ErrorFound Then
        '        odataset2 = response2.Value
        '    End If

        '    If Not response.ErrorFound And Not response2.ErrorFound Then
        '        Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre") + "          Bodega Actual: " + odataset2.Tables(0).Rows(0).Item("descripcion")
        '    End If
        'Catch ex As Exception
        '    ShowMessage(MessageType.MsgInformation, "Ocurr�o un error al buscar la sucursal y bodega actual")
        'End Try
    End Sub

#End Region

#Region "Catalogos"
    'Public Sub mnuCatBodegueros_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
    '    Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
    '    Dim oBodegueros As New VillarrealBusiness.clsBodegueros
    '    With oGrid
    '        .Title = "Bodegueros"
    '        .UpdateTitle = "Bodegueros"
    '        .DataSource = AddressOf oBodegueros.Listado
    '        .UpdateForm = New frmBodegueros
    '        .Format = AddressOf for_bodegueros_grs
    '        .MdiParent = Me
    '        .Show()
    '    End With
    '    oBodegueros = Nothing
    'End Sub
    'Public Sub mnuCatChoferes_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
    '    Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
    '    Dim oChoferes As New VillarrealBusiness.clsChoferes
    '    With oGrid
    '        .Title = "Choferes"
    '        .UpdateTitle = "Choferes"
    '        .DataSource = AddressOf oChoferes.Listado
    '        .UpdateForm = New frmChoferes
    '        .Format = AddressOf for_choferes_grs
    '        .MdiParent = Me
    '        .Show()
    '    End With
    '    oChoferes = Nothing
    'End Sub
    'Public Sub mnuCatGruposEventualidades_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
    '    Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
    '    Dim oGruposEventualidades As New VillarrealBusiness.clsGruposEventualidades
    '    With oGrid
    '        .Title = "Grupos de Eventualidades"
    '        .UpdateTitle = "Grupos de Eventualidades"
    '        .DataSource = AddressOf oGruposEventualidades.Listado
    '        .UpdateForm = New frmGruposEventualidades
    '        .Format = AddressOf for_grupos_eventualidades_grs
    '        .MdiParent = Me
    '        .Show()
    '    End With
    '    oGruposEventualidades = Nothing
    'End Sub
    'Public Sub mnuCatEventualidades_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
    '    Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
    '    Dim oEventualidades As New VillarrealBusiness.clsEventualidades
    '    With oGrid
    '        .Title = "Eventualidades"
    '        .UpdateTitle = "Eventualidades"
    '        .DataSource = AddressOf oEventualidades.Listado
    '        .UpdateForm = New frmEventualidades
    '        .Format = AddressOf for_eventualidades_grs
    '        .MdiParent = Me
    '        .Show()
    '    End With
    '    oEventualidades = Nothing
    'End Sub

    Public Sub mnuCatVendedores_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatVendedores.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oVendedores As New VillarrealBusiness.clsVendedores
        With oGrid
            .Title = "Vendedores"
            .UpdateTitle = "Vendedores"
            .Size = New Size(850, 643)
            .DataSource = AddressOf oVendedores.Listado
            .UpdateForm = New Comunes.frmVendedores
            .Format = AddressOf Comunes.clsFormato.for_vendedores_grs
            .MdiParent = Me
            .Show()
        End With
        oVendedores = Nothing
    End Sub

    Public Sub mnuCatExclusiones_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatExclusiones.ItemClick
        Dim oform As New frmExclusiones
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Exclusiones"
        oform.Action = Dipros.Utils.Common.Actions.None
        oform.Show()
    End Sub

    Public Sub mnuCatConfiguracionComisiones_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatConfiguracionComisiones.ItemClick
        Dim oform As New frmConfiguradorComisiones
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Configurador de Comisiones"
        oform.Action = Dipros.Utils.Common.Actions.Accept
        oform.Show()
    End Sub

    Public Sub mnuCatEsquemasComisiones_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatEsquemasComisiones.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oEsquemasComisiones As New VillarrealBusiness.clsEsquemasComisiones
        With oGrid
            .Title = "Esquema de Comisiones"
            .UpdateTitle = "Esquema"
            .Size = New Size(795, 643)
            .DataSource = AddressOf oEsquemasComisiones.Listado
            .UpdateForm = New frmEsquemasComisiones
            .Format = AddressOf for_esquemas_comisiones_grs
            .MdiParent = Me
            .Show()
        End With
        oEsquemasComisiones = Nothing
    End Sub


#End Region


#Region "Reportes"

    Public Sub mnuRepInformeVentas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepInformeVentas.ItemClick
        Dim oform As New frmInformeVentas
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Informe de Ventas"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub

    Public Sub mnuRepComisionesVentas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepComisionesVentas.ItemClick
        Dim oform As New frmComisionesVentas
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Comisiones de Ventas"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub

    Public Sub mnuRepComisionNetaPagar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepComisionNetaPagar.ItemClick
        Dim oform As New Comisiones.frmComisionNetaPagar
        oform.MdiParent = Me
        oform.MenuOption = e.Item
        oform.Title = "Comisi�n Neta a Pagar"
        oform.Action = Dipros.Utils.Common.Actions.Report
        oform.Show()
    End Sub

#End Region

#Region "Herramientas"

    Public Sub mnuUtiVariables_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuUtiVariables.ItemClick
        Dim oForm As New Comunes.frmVariables
        oForm.MdiParent = Me
        oForm.Show()
    End Sub
#End Region



    
   
   
End Class
