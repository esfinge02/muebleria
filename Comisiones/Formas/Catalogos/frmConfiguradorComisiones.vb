Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmConfiguradorComisiones
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grRangoComisiones As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvRangoDescuentos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcLimiteInferiorVentas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaRangos As Dipros.Windows.TINMaster
    Friend WithEvents grcLimiteSuperiorVentas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPorcentajeComision As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemCalcEditPorcentaje As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConfiguradorComisiones))
        Me.grRangoComisiones = New DevExpress.XtraGrid.GridControl
        Me.grvRangoDescuentos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcLimiteInferiorVentas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcLimiteSuperiorVentas = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPorcentajeComision = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEditPorcentaje = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.tmaRangos = New Dipros.Windows.TINMaster
        CType(Me.grRangoComisiones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvRangoDescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEditPorcentaje, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1806, 28)
        '
        'grRangoComisiones
        '
        '
        'grRangoComisiones.EmbeddedNavigator
        '
        Me.grRangoComisiones.EmbeddedNavigator.Name = ""
        Me.grRangoComisiones.Location = New System.Drawing.Point(8, 80)
        Me.grRangoComisiones.MainView = Me.grvRangoDescuentos
        Me.grRangoComisiones.Name = "grRangoComisiones"
        Me.grRangoComisiones.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1, Me.RepositoryItemCalcEditPorcentaje})
        Me.grRangoComisiones.Size = New System.Drawing.Size(632, 272)
        Me.grRangoComisiones.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grRangoComisiones.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRangoComisiones.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRangoComisiones.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRangoComisiones.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRangoComisiones.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grRangoComisiones.TabIndex = 90
        Me.grRangoComisiones.TabStop = False
        Me.grRangoComisiones.Text = "Ventas"
        '
        'grvRangoDescuentos
        '
        Me.grvRangoDescuentos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcLimiteInferiorVentas, Me.grcLimiteSuperiorVentas, Me.grcPorcentajeComision})
        Me.grvRangoDescuentos.GridControl = Me.grRangoComisiones
        Me.grvRangoDescuentos.Name = "grvRangoDescuentos"
        Me.grvRangoDescuentos.OptionsBehavior.Editable = False
        Me.grvRangoDescuentos.OptionsCustomization.AllowFilter = False
        Me.grvRangoDescuentos.OptionsCustomization.AllowGroup = False
        Me.grvRangoDescuentos.OptionsCustomization.AllowSort = False
        Me.grvRangoDescuentos.OptionsView.ShowFooter = True
        Me.grvRangoDescuentos.OptionsView.ShowGroupPanel = False
        '
        'grcLimiteInferiorVentas
        '
        Me.grcLimiteInferiorVentas.Caption = "Limite Inferior de Ventas"
        Me.grcLimiteInferiorVentas.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcLimiteInferiorVentas.FieldName = "limite_inferior_ventas"
        Me.grcLimiteInferiorVentas.Name = "grcLimiteInferiorVentas"
        Me.grcLimiteInferiorVentas.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcLimiteInferiorVentas.VisibleIndex = 0
        Me.grcLimiteInferiorVentas.Width = 250
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatString = "c2"
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.EditFormat.FormatString = "c2"
        Me.RepositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'grcLimiteSuperiorVentas
        '
        Me.grcLimiteSuperiorVentas.Caption = "Limite Superior de Ventas"
        Me.grcLimiteSuperiorVentas.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcLimiteSuperiorVentas.FieldName = "limite_superior_ventas"
        Me.grcLimiteSuperiorVentas.Name = "grcLimiteSuperiorVentas"
        Me.grcLimiteSuperiorVentas.VisibleIndex = 1
        Me.grcLimiteSuperiorVentas.Width = 243
        '
        'grcPorcentajeComision
        '
        Me.grcPorcentajeComision.Caption = "% Comisi�n"
        Me.grcPorcentajeComision.ColumnEdit = Me.RepositoryItemCalcEditPorcentaje
        Me.grcPorcentajeComision.FieldName = "porcentaje_comision"
        Me.grcPorcentajeComision.Name = "grcPorcentajeComision"
        Me.grcPorcentajeComision.VisibleIndex = 2
        Me.grcPorcentajeComision.Width = 125
        '
        'RepositoryItemCalcEditPorcentaje
        '
        Me.RepositoryItemCalcEditPorcentaje.AutoHeight = False
        Me.RepositoryItemCalcEditPorcentaje.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEditPorcentaje.Name = "RepositoryItemCalcEditPorcentaje"
        '
        'tmaRangos
        '
        Me.tmaRangos.BackColor = System.Drawing.Color.White
        Me.tmaRangos.CanDelete = True
        Me.tmaRangos.CanInsert = True
        Me.tmaRangos.CanUpdate = True
        Me.tmaRangos.Grid = Me.grRangoComisiones
        Me.tmaRangos.Location = New System.Drawing.Point(8, 48)
        Me.tmaRangos.Name = "tmaRangos"
        Me.tmaRangos.Size = New System.Drawing.Size(611, 23)
        Me.tmaRangos.TabIndex = 89
        Me.tmaRangos.TabStop = False
        Me.tmaRangos.Title = "Rango de Comisiones"
        Me.tmaRangos.UpdateTitle = "Rango de Comisi�n"
        '
        'frmConfiguradorComisiones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(658, 380)
        Me.Controls.Add(Me.grRangoComisiones)
        Me.Controls.Add(Me.tmaRangos)
        Me.Name = "frmConfiguradorComisiones"
        Me.Text = "frmConfiguradorComisiones"
        Me.Controls.SetChildIndex(Me.tmaRangos, 0)
        Me.Controls.SetChildIndex(Me.grRangoComisiones, 0)
        CType(Me.grRangoComisiones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvRangoDescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEditPorcentaje, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oVariables As VillarrealBusiness.clsVariables
    Private oConfiguradorComisiones As VillarrealBusiness.clsConfiguracionComisiones


    Private Sub frmConfiguradorComisiones_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub

    Private Sub frmConfiguradorComisiones_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmConfiguradorComisiones_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmConfiguradorComisiones_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        '     Response = oVariables.ActualizarComisionContados(Me.clcComisionContados.EditValue)

        Response = oConfiguradorComisiones.Eliminar()

        If Not Response.ErrorFound Then
            With Me.tmaRangos
                .MoveFirst()

                Do While Not .EOF
                    Select Case .CurrentAction
                        Case Actions.Insert, Actions.None, Actions.Update
                            Response = oConfiguradorComisiones.Insertar(.SelectedRow.Tables(0).Rows(0).Item("partida"), .SelectedRow.Tables(0).Rows(0).Item("limite_inferior_ventas"), .SelectedRow.Tables(0).Rows(0).Item("limite_superior_ventas"), .SelectedRow.Tables(0).Rows(0).Item("porcentaje_comision"))
                            If Response.ErrorFound Then Exit Do
                           
                    End Select
                    .MoveNext()
                Loop
            End With
        End If


        If Not Response.ErrorFound Then
            Close()
        End If
    End Sub

    Private Sub frmConfiguradorComisiones_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oVariables = New VillarrealBusiness.clsVariables
        oConfiguradorComisiones = New VillarrealBusiness.clsConfiguracionComisiones

        Dim ComisionContado As Decimal = CType(oVariables.TraeDatos("porcentaje_comision_contados", VillarrealBusiness.clsVariables.tipo_dato.Float), Decimal)
        '     Me.clcComisionContados.Value = ComisionContado

        ConfiguraMaster()

        Dim odataset = New DataSet
        Response = oConfiguradorComisiones.Desplega
        If Not Response.ErrorFound Then
            odataset = CType(Response.Value, DataSet)
            Me.tmaRangos.DataSource = odataset
        End If

    End Sub

    Private Sub frmConfiguradorComisiones_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oConfiguradorComisiones.ValidacionRangos(CType(Me.tmaRangos.DataSource, DataSet).Tables(0))
    End Sub



    Private Sub ConfiguraMaster()
        With Me.tmaRangos
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmConfiguradorComisionesDetalle

            .AddColumn("limite_inferior_ventas", "System.Double")
            .AddColumn("limite_superior_ventas", "System.Double")
            .AddColumn("porcentaje_comision", "System.Double")
        End With
    End Sub

   

  



End Class
