Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmExclusiones
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents pnlExclusiones As System.Windows.Forms.Panel
    Friend WithEvents gpbClientes As System.Windows.Forms.GroupBox
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents cmdInsertar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkNoPagaComision As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtNombres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cmdActualizar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents cmdGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grClientes As DevExpress.XtraGrid.GridControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grvClientes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcRfc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExclusiones))
        Me.pnlExclusiones = New System.Windows.Forms.Panel
        Me.gpbClientes = New System.Windows.Forms.GroupBox
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.cmdInsertar = New DevExpress.XtraEditors.SimpleButton
        Me.chkNoPagaComision = New DevExpress.XtraEditors.CheckEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.txtNombres = New DevExpress.XtraEditors.TextEdit
        Me.cmdActualizar = New DevExpress.XtraEditors.SimpleButton
        Me.lblNombre = New System.Windows.Forms.Label
        Me.cmdGuardar = New DevExpress.XtraEditors.SimpleButton
        Me.cmdCancelar = New DevExpress.XtraEditors.SimpleButton
        Me.grClientes = New DevExpress.XtraGrid.GridControl
        Me.grvClientes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcRfc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.pnlExclusiones.SuspendLayout()
        Me.gpbClientes.SuspendLayout()
        CType(Me.chkNoPagaComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(963, 28)
        '
        'pnlExclusiones
        '
        Me.pnlExclusiones.Controls.Add(Me.gpbClientes)
        Me.pnlExclusiones.Controls.Add(Me.grClientes)
        Me.pnlExclusiones.Controls.Add(Me.Label2)
        Me.pnlExclusiones.Location = New System.Drawing.Point(0, 32)
        Me.pnlExclusiones.Name = "pnlExclusiones"
        Me.pnlExclusiones.Size = New System.Drawing.Size(632, 544)
        Me.pnlExclusiones.TabIndex = 59
        '
        'gpbClientes
        '
        Me.gpbClientes.Controls.Add(Me.lkpCliente)
        Me.gpbClientes.Controls.Add(Me.cmdInsertar)
        Me.gpbClientes.Controls.Add(Me.chkNoPagaComision)
        Me.gpbClientes.Controls.Add(Me.lblCliente)
        Me.gpbClientes.Controls.Add(Me.txtNombres)
        Me.gpbClientes.Controls.Add(Me.cmdActualizar)
        Me.gpbClientes.Controls.Add(Me.lblNombre)
        Me.gpbClientes.Controls.Add(Me.cmdGuardar)
        Me.gpbClientes.Controls.Add(Me.cmdCancelar)
        Me.gpbClientes.Location = New System.Drawing.Point(328, 16)
        Me.gpbClientes.Name = "gpbClientes"
        Me.gpbClientes.Size = New System.Drawing.Size(288, 136)
        Me.gpbClientes.TabIndex = 76
        Me.gpbClientes.TabStop = False
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "cliente"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(52, 54)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(350, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(136, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 78
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'cmdInsertar
        '
        Me.cmdInsertar.Location = New System.Drawing.Point(24, 16)
        Me.cmdInsertar.Name = "cmdInsertar"
        Me.cmdInsertar.TabIndex = 77
        Me.cmdInsertar.Text = "Insertar"
        '
        'chkNoPagaComision
        '
        Me.chkNoPagaComision.Location = New System.Drawing.Point(52, 104)
        Me.chkNoPagaComision.Name = "chkNoPagaComision"
        '
        'chkNoPagaComision.Properties
        '
        Me.chkNoPagaComision.Properties.Caption = "No Paga Comisi�n"
        Me.chkNoPagaComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkNoPagaComision.Size = New System.Drawing.Size(192, 20)
        Me.chkNoPagaComision.TabIndex = 76
        Me.chkNoPagaComision.Tag = ""
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(8, 56)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 72
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombres
        '
        Me.txtNombres.EditValue = ""
        Me.txtNombres.Location = New System.Drawing.Point(52, 78)
        Me.txtNombres.Name = "txtNombres"
        '
        'txtNombres.Properties
        '
        Me.txtNombres.Properties.Enabled = False
        Me.txtNombres.Properties.MaxLength = 100
        Me.txtNombres.Size = New System.Drawing.Size(223, 20)
        Me.txtNombres.TabIndex = 75
        Me.txtNombres.Tag = "nombre"
        '
        'cmdActualizar
        '
        Me.cmdActualizar.Location = New System.Drawing.Point(104, 16)
        Me.cmdActualizar.Name = "cmdActualizar"
        Me.cmdActualizar.TabIndex = 77
        Me.cmdActualizar.Text = "Actualizar"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(3, 80)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(53, 16)
        Me.lblNombre.TabIndex = 74
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdGuardar
        '
        Me.cmdGuardar.Enabled = False
        Me.cmdGuardar.Location = New System.Drawing.Point(24, 16)
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.TabIndex = 77
        Me.cmdGuardar.Text = "Guardar"
        Me.cmdGuardar.Visible = False
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Enabled = False
        Me.cmdCancelar.Location = New System.Drawing.Point(104, 16)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.TabIndex = 77
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.Visible = False
        '
        'grClientes
        '
        '
        'grClientes.EmbeddedNavigator
        '
        Me.grClientes.EmbeddedNavigator.Name = ""
        Me.grClientes.Location = New System.Drawing.Point(8, 24)
        Me.grClientes.MainView = Me.grvClientes
        Me.grClientes.Name = "grClientes"
        Me.grClientes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grClientes.Size = New System.Drawing.Size(312, 512)
        Me.grClientes.TabIndex = 70
        Me.grClientes.Text = "Clientes"
        '
        'grvClientes
        '
        Me.grvClientes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcCliente, Me.grcNombreCliente, Me.grcRfc})
        Me.grvClientes.GridControl = Me.grClientes
        Me.grvClientes.Name = "grvClientes"
        Me.grvClientes.OptionsCustomization.AllowFilter = False
        Me.grvClientes.OptionsCustomization.AllowGroup = False
        Me.grvClientes.OptionsView.ShowGroupPanel = False
        Me.grvClientes.OptionsView.ShowIndicator = False
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.VisibleIndex = 0
        Me.grcCliente.Width = 53
        '
        'grcNombreCliente
        '
        Me.grcNombreCliente.Caption = "Nombre"
        Me.grcNombreCliente.FieldName = "nombre"
        Me.grcNombreCliente.Name = "grcNombreCliente"
        Me.grcNombreCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreCliente.VisibleIndex = 1
        Me.grcNombreCliente.Width = 172
        '
        'grcRfc
        '
        Me.grcRfc.Caption = "RFC"
        Me.grcRfc.FieldName = "rfc"
        Me.grcRfc.Name = "grcRfc"
        Me.grcRfc.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcRfc.VisibleIndex = 2
        Me.grcRfc.Width = 85
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 17)
        Me.Label2.TabIndex = 71
        Me.Label2.Text = "C&LIENTES"
        '
        'frmExclusiones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(634, 580)
        Me.Controls.Add(Me.pnlExclusiones)
        Me.Name = "frmExclusiones"
        Me.Text = "frmExclusiones"
        Me.Controls.SetChildIndex(Me.pnlExclusiones, 0)
        Me.pnlExclusiones.ResumeLayout(False)
        Me.gpbClientes.ResumeLayout(False)
        CType(Me.chkNoPagaComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private oClientes As New VillarrealBusiness.clsClientes
    Private lngCliente As Long

    Private filaCliente As Long
    Private moverFilaCliente As Boolean = True


    Private Sub frmExclusiones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.tbrTools.Buttons.Item(0).Enabled = False
        Me.tbrTools.Buttons.Item(0).Visible = False

        CargarClientesNoPaganComisiones()
    End Sub


    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.ClientesNoPaganComisiones(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        If Me.lkpCliente.EditValue Is Nothing Then Exit Sub

        lngCliente = CLng(Me.lkpCliente.EditValue)
        Me.txtNombres.Text = Me.lkpCliente.GetValue("nombre")
    End Sub

    Private Sub grvClientes_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvClientes.FocusedRowChanged
        lngCliente = Me.grvClientes.GetRowCellValue(e.FocusedRowHandle, Me.grcCliente)
        Me.lkpCliente.EditValue = Me.grvClientes.GetRowCellValue(e.FocusedRowHandle, Me.grcCliente)
        Me.txtNombres.Text = Me.grvClientes.GetRowCellValue(e.FocusedRowHandle, Me.grcNombreCliente)
        Me.chkNoPagaComision.Checked = True

        Me.lkpCliente.Enabled = False
        Me.txtNombres.Enabled = False
        Me.chkNoPagaComision.Enabled = False

        If moverFilaCliente = True Then
            filaCliente = e.FocusedRowHandle
        End If
    End Sub

    Private Sub cmdInsertar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsertar.Click
        Me.lkpCliente.Enabled = True
        Me.lkpCliente.EditValue = Nothing
        Me.txtNombres.Text = ""

        Me.cmdInsertar.Enabled = False
        Me.cmdInsertar.Visible = False
        Me.cmdActualizar.Enabled = False
        Me.cmdActualizar.Visible = False
        Me.cmdGuardar.Enabled = True
        Me.cmdGuardar.Visible = True
        Me.cmdCancelar.Enabled = True
        Me.cmdCancelar.Visible = True

        Me.grClientes.Enabled = False

    End Sub
    Private Sub cmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActualizar.Click

        Me.chkNoPagaComision.Checked = False

        Me.cmdInsertar.Enabled = False
        Me.cmdInsertar.Visible = False
        Me.cmdActualizar.Enabled = False
        Me.cmdActualizar.Visible = False
        Me.cmdGuardar.Enabled = True
        Me.cmdGuardar.Visible = True
        Me.cmdCancelar.Enabled = True
        Me.cmdCancelar.Visible = True

        Me.grClientes.Enabled = False
    End Sub
    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        Dim Response As New Events

        Me.cmdInsertar.Enabled = True
        Me.cmdInsertar.Visible = True
        Me.cmdActualizar.Enabled = True
        Me.cmdActualizar.Visible = True
        Me.cmdGuardar.Enabled = False
        Me.cmdGuardar.Visible = False
        Me.cmdCancelar.Enabled = False
        Me.cmdCancelar.Visible = False

        Response = oClientes.ActualizaNoPagaComision(lngCliente, Me.chkNoPagaComision.Checked)
        If Response.ErrorFound Then
            Response.Ex = Nothing
            Response.Message = "No se pudo Actualizar el Cliente"
            Response.ShowError()
        End If

        Me.lkpCliente.Enabled = False
        Me.chkNoPagaComision.Checked = True

        CargarClientesNoPaganComisiones()
        moverFilaCliente = False
        Me.grvClientes.MoveLast()
        Me.grvClientes.MoveFirst()
        moverFilaCliente = True
        Me.grClientes.Focus()

        Me.grvClientes.MoveBy(filaCliente)

        Me.lkpCliente_LoadData(True)
        Me.grClientes.Enabled = True
    End Sub
    Private Sub cmdCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click

        Me.cmdInsertar.Enabled = True
        Me.cmdInsertar.Visible = True
        Me.cmdActualizar.Enabled = True
        Me.cmdActualizar.Visible = True
        Me.cmdGuardar.Enabled = False
        Me.cmdGuardar.Visible = False
        Me.cmdCancelar.Enabled = False
        Me.cmdCancelar.Visible = False

        Me.lkpCliente.Enabled = False
        Me.chkNoPagaComision.Checked = True

        moverFilaCliente = False
        Me.grvClientes.MoveLast()
        Me.grvClientes.MoveFirst()
        moverFilaCliente = True
        Me.grvClientes.MoveBy(filaCliente)

        Me.grClientes.Focus()

        Me.grClientes.Enabled = True
    End Sub
    Private Sub grvClientes_EndSorting(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvClientes.EndSorting
        If Me.grvClientes.RowCount > 0 Then
            Me.grvClientes.MoveBy(filaCliente)
        End If


    End Sub

    Private Sub CargarClientesNoPaganComisiones()
        Dim Response As New Events

        Response = oClientes.ClientesNoPaganComisiones(True)

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.grClientes.DataSource = oDataSet.Tables(0)
        Else
            Response.ShowError()
        End If

        Response = Nothing
    End Sub
End Class
