Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmConfiguradorComisionesDetalle
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblLimiteInferiorVentas As System.Windows.Forms.Label
    Friend WithEvents lblLimiteSuperiorVentas As System.Windows.Forms.Label
    Friend WithEvents clcLimiteSuperiorVentas As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcLimiteInferiorVentas As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPorcentajeComision As System.Windows.Forms.Label
    Friend WithEvents clcPorcentajeComision As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConfiguradorComisionesDetalle))
        Me.lblLimiteInferiorVentas = New System.Windows.Forms.Label
        Me.clcLimiteInferiorVentas = New Dipros.Editors.TINCalcEdit
        Me.lblLimiteSuperiorVentas = New System.Windows.Forms.Label
        Me.clcLimiteSuperiorVentas = New Dipros.Editors.TINCalcEdit
        Me.lblPorcentajeComision = New System.Windows.Forms.Label
        Me.clcPorcentajeComision = New Dipros.Editors.TINCalcEdit
        CType(Me.clcLimiteInferiorVentas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcLimiteSuperiorVentas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPorcentajeComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(273, 28)
        '
        'lblLimiteInferiorVentas
        '
        Me.lblLimiteInferiorVentas.AutoSize = True
        Me.lblLimiteInferiorVentas.Location = New System.Drawing.Point(16, 40)
        Me.lblLimiteInferiorVentas.Name = "lblLimiteInferiorVentas"
        Me.lblLimiteInferiorVentas.Size = New System.Drawing.Size(146, 16)
        Me.lblLimiteInferiorVentas.TabIndex = 61
        Me.lblLimiteInferiorVentas.Tag = ""
        Me.lblLimiteInferiorVentas.Text = "Limite Inferior de Ventas:"
        Me.lblLimiteInferiorVentas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcLimiteInferiorVentas
        '
        Me.clcLimiteInferiorVentas.EditValue = "0"
        Me.clcLimiteInferiorVentas.Location = New System.Drawing.Point(168, 40)
        Me.clcLimiteInferiorVentas.MaxValue = 0
        Me.clcLimiteInferiorVentas.MinValue = 0
        Me.clcLimiteInferiorVentas.Name = "clcLimiteInferiorVentas"
        '
        'clcLimiteInferiorVentas.Properties
        '
        Me.clcLimiteInferiorVentas.Properties.DisplayFormat.FormatString = "c2"
        Me.clcLimiteInferiorVentas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLimiteInferiorVentas.Properties.EditFormat.FormatString = "n2"
        Me.clcLimiteInferiorVentas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLimiteInferiorVentas.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcLimiteInferiorVentas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcLimiteInferiorVentas.Size = New System.Drawing.Size(136, 19)
        Me.clcLimiteInferiorVentas.TabIndex = 62
        Me.clcLimiteInferiorVentas.Tag = "limite_inferior_ventas"
        '
        'lblLimiteSuperiorVentas
        '
        Me.lblLimiteSuperiorVentas.AutoSize = True
        Me.lblLimiteSuperiorVentas.Location = New System.Drawing.Point(8, 64)
        Me.lblLimiteSuperiorVentas.Name = "lblLimiteSuperiorVentas"
        Me.lblLimiteSuperiorVentas.Size = New System.Drawing.Size(152, 16)
        Me.lblLimiteSuperiorVentas.TabIndex = 63
        Me.lblLimiteSuperiorVentas.Tag = ""
        Me.lblLimiteSuperiorVentas.Text = "Limite Superior de Ventas:"
        Me.lblLimiteSuperiorVentas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcLimiteSuperiorVentas
        '
        Me.clcLimiteSuperiorVentas.EditValue = "0"
        Me.clcLimiteSuperiorVentas.Location = New System.Drawing.Point(168, 64)
        Me.clcLimiteSuperiorVentas.MaxValue = 0
        Me.clcLimiteSuperiorVentas.MinValue = 0
        Me.clcLimiteSuperiorVentas.Name = "clcLimiteSuperiorVentas"
        '
        'clcLimiteSuperiorVentas.Properties
        '
        Me.clcLimiteSuperiorVentas.Properties.DisplayFormat.FormatString = "c2"
        Me.clcLimiteSuperiorVentas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLimiteSuperiorVentas.Properties.EditFormat.FormatString = "n2"
        Me.clcLimiteSuperiorVentas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLimiteSuperiorVentas.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcLimiteSuperiorVentas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcLimiteSuperiorVentas.Size = New System.Drawing.Size(136, 19)
        Me.clcLimiteSuperiorVentas.TabIndex = 64
        Me.clcLimiteSuperiorVentas.Tag = "limite_superior_ventas"
        '
        'lblPorcentajeComision
        '
        Me.lblPorcentajeComision.AutoSize = True
        Me.lblPorcentajeComision.Location = New System.Drawing.Point(88, 88)
        Me.lblPorcentajeComision.Name = "lblPorcentajeComision"
        Me.lblPorcentajeComision.Size = New System.Drawing.Size(75, 16)
        Me.lblPorcentajeComision.TabIndex = 65
        Me.lblPorcentajeComision.Tag = ""
        Me.lblPorcentajeComision.Text = "% Comisi�n:"
        Me.lblPorcentajeComision.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPorcentajeComision
        '
        Me.clcPorcentajeComision.EditValue = "0"
        Me.clcPorcentajeComision.Location = New System.Drawing.Point(168, 88)
        Me.clcPorcentajeComision.MaxValue = 0
        Me.clcPorcentajeComision.MinValue = 0
        Me.clcPorcentajeComision.Name = "clcPorcentajeComision"
        '
        'clcPorcentajeComision.Properties
        '
        Me.clcPorcentajeComision.Properties.DisplayFormat.FormatString = "n2"
        Me.clcPorcentajeComision.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentajeComision.Properties.EditFormat.FormatString = "n2"
        Me.clcPorcentajeComision.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentajeComision.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPorcentajeComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPorcentajeComision.Size = New System.Drawing.Size(56, 19)
        Me.clcPorcentajeComision.TabIndex = 66
        Me.clcPorcentajeComision.Tag = "porcentaje_comision"
        '
        'frmConfiguradorComisionesDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(314, 116)
        Me.Controls.Add(Me.lblPorcentajeComision)
        Me.Controls.Add(Me.clcPorcentajeComision)
        Me.Controls.Add(Me.lblLimiteSuperiorVentas)
        Me.Controls.Add(Me.clcLimiteSuperiorVentas)
        Me.Controls.Add(Me.lblLimiteInferiorVentas)
        Me.Controls.Add(Me.clcLimiteInferiorVentas)
        Me.Name = "frmConfiguradorComisionesDetalle"
        Me.Text = "frmConfiguradorComisionesDetalle"
        Me.Controls.SetChildIndex(Me.clcLimiteInferiorVentas, 0)
        Me.Controls.SetChildIndex(Me.lblLimiteInferiorVentas, 0)
        Me.Controls.SetChildIndex(Me.clcLimiteSuperiorVentas, 0)
        Me.Controls.SetChildIndex(Me.lblLimiteSuperiorVentas, 0)
        Me.Controls.SetChildIndex(Me.clcPorcentajeComision, 0)
        Me.Controls.SetChildIndex(Me.lblPorcentajeComision, 0)
        CType(Me.clcLimiteInferiorVentas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcLimiteSuperiorVentas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPorcentajeComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oConfiguradorComisiones As New VillarrealBusiness.clsConfiguracionComisiones

    Private Sub frmConfiguradorComisionesDetalle_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
    End Sub

    Private Sub frmConfiguradorComisionesDetalle_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oConfiguradorComisiones.Validacion(Action, Me.clcLimiteInferiorVentas.EditValue, Me.clcLimiteSuperiorVentas.EditValue, Me.clcPorcentajeComision.EditValue)
    End Sub

    Private Sub frmConfiguradorComisionesDetalle_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

    End Sub
End Class
