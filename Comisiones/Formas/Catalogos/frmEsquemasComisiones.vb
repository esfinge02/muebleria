Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmEsquemasComisiones
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblComisionContados As System.Windows.Forms.Label
    Friend WithEvents clcComisionContados As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clcComisionGeneral As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents clcEsquema As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents chkUsarTabla As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkRestarSueldo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents clcComisionCreditos As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcComisionFonacot As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEsquemasComisiones))
        Me.lblComisionContados = New System.Windows.Forms.Label
        Me.clcComisionContados = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcComisionCreditos = New Dipros.Editors.TINCalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcComisionFonacot = New Dipros.Editors.TINCalcEdit
        Me.chkUsarTabla = New DevExpress.XtraEditors.CheckEdit
        Me.chkRestarSueldo = New DevExpress.XtraEditors.CheckEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.clcComisionGeneral = New Dipros.Editors.TINCalcEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.clcEsquema = New DevExpress.XtraEditors.CalcEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcComisionContados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcComisionCreditos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcComisionFonacot.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUsarTabla.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRestarSueldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcComisionGeneral.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcEsquema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(23, 50)
        '
        'lblComisionContados
        '
        Me.lblComisionContados.AutoSize = True
        Me.lblComisionContados.Location = New System.Drawing.Point(16, 128)
        Me.lblComisionContados.Name = "lblComisionContados"
        Me.lblComisionContados.Size = New System.Drawing.Size(130, 16)
        Me.lblComisionContados.TabIndex = 6
        Me.lblComisionContados.Tag = ""
        Me.lblComisionContados.Text = "% Comisi�n Contados:"
        Me.lblComisionContados.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcComisionContados
        '
        Me.clcComisionContados.EditValue = "0"
        Me.clcComisionContados.Location = New System.Drawing.Point(152, 129)
        Me.clcComisionContados.MaxValue = 0
        Me.clcComisionContados.MinValue = 0
        Me.clcComisionContados.Name = "clcComisionContados"
        '
        'clcComisionContados.Properties
        '
        Me.clcComisionContados.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComisionContados.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComisionContados.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcComisionContados.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcComisionContados.Size = New System.Drawing.Size(72, 19)
        Me.clcComisionContados.TabIndex = 7
        Me.clcComisionContados.Tag = "porcentaje_comisiones_contados"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 178)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(124, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Tag = ""
        Me.Label2.Text = "% Comisi�n Cr�ditos:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcComisionCreditos
        '
        Me.clcComisionCreditos.EditValue = "0"
        Me.clcComisionCreditos.Location = New System.Drawing.Point(152, 176)
        Me.clcComisionCreditos.MaxValue = 0
        Me.clcComisionCreditos.MinValue = 0
        Me.clcComisionCreditos.Name = "clcComisionCreditos"
        '
        'clcComisionCreditos.Properties
        '
        Me.clcComisionCreditos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComisionCreditos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComisionCreditos.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcComisionCreditos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcComisionCreditos.Size = New System.Drawing.Size(72, 19)
        Me.clcComisionCreditos.TabIndex = 11
        Me.clcComisionCreditos.Tag = "porcentaje_comisiones_creditos"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(24, 152)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 16)
        Me.Label3.TabIndex = 8
        Me.Label3.Tag = ""
        Me.Label3.Text = "% Comisi�n Fonacot:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcComisionFonacot
        '
        Me.clcComisionFonacot.EditValue = "0"
        Me.clcComisionFonacot.Location = New System.Drawing.Point(152, 152)
        Me.clcComisionFonacot.MaxValue = 0
        Me.clcComisionFonacot.MinValue = 0
        Me.clcComisionFonacot.Name = "clcComisionFonacot"
        '
        'clcComisionFonacot.Properties
        '
        Me.clcComisionFonacot.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComisionFonacot.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComisionFonacot.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcComisionFonacot.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcComisionFonacot.Size = New System.Drawing.Size(72, 19)
        Me.clcComisionFonacot.TabIndex = 9
        Me.clcComisionFonacot.Tag = "porcentaje_comisiones_fonacot"
        '
        'chkUsarTabla
        '
        Me.chkUsarTabla.Location = New System.Drawing.Point(152, 200)
        Me.chkUsarTabla.Name = "chkUsarTabla"
        '
        'chkUsarTabla.Properties
        '
        Me.chkUsarTabla.Properties.Caption = "Usar Tabla de Rangos para Creditos"
        Me.chkUsarTabla.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkUsarTabla.Size = New System.Drawing.Size(216, 19)
        Me.chkUsarTabla.TabIndex = 12
        Me.chkUsarTabla.Tag = "usar_tabla_rangos_credito"
        '
        'chkRestarSueldo
        '
        Me.chkRestarSueldo.Location = New System.Drawing.Point(152, 224)
        Me.chkRestarSueldo.Name = "chkRestarSueldo"
        '
        'chkRestarSueldo.Properties
        '
        Me.chkRestarSueldo.Properties.Caption = "Restar Sueldo"
        Me.chkRestarSueldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkRestarSueldo.Size = New System.Drawing.Size(128, 19)
        Me.chkRestarSueldo.TabIndex = 13
        Me.chkRestarSueldo.Tag = "restar_sueldo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(122, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Tag = ""
        Me.Label4.Text = "% Comisi�n General:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcComisionGeneral
        '
        Me.clcComisionGeneral.EditValue = "0"
        Me.clcComisionGeneral.Location = New System.Drawing.Point(152, 106)
        Me.clcComisionGeneral.MaxValue = 0
        Me.clcComisionGeneral.MinValue = 0
        Me.clcComisionGeneral.Name = "clcComisionGeneral"
        '
        'clcComisionGeneral.Properties
        '
        Me.clcComisionGeneral.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComisionGeneral.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComisionGeneral.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcComisionGeneral.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcComisionGeneral.Size = New System.Drawing.Size(72, 19)
        Me.clcComisionGeneral.TabIndex = 5
        Me.clcComisionGeneral.Tag = "porcentaje_comisiones_general"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(88, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Tag = ""
        Me.Label5.Text = "Esquema:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEsquema
        '
        Me.clcEsquema.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcEsquema.Location = New System.Drawing.Point(152, 40)
        Me.clcEsquema.Name = "clcEsquema"
        '
        'clcEsquema.Properties
        '
        Me.clcEsquema.Properties.Enabled = False
        Me.clcEsquema.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcEsquema.Size = New System.Drawing.Size(72, 20)
        Me.clcEsquema.TabIndex = 1
        Me.clcEsquema.Tag = "esquema"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(88, 68)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Tag = ""
        Me.Label6.Text = "Nombre:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(152, 64)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 150
        Me.txtNombre.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtNombre.Size = New System.Drawing.Size(224, 38)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'frmEsquemasComisiones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 252)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.clcEsquema)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.clcComisionGeneral)
        Me.Controls.Add(Me.chkUsarTabla)
        Me.Controls.Add(Me.chkRestarSueldo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.clcComisionFonacot)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcComisionCreditos)
        Me.Controls.Add(Me.lblComisionContados)
        Me.Controls.Add(Me.clcComisionContados)
        Me.Name = "frmEsquemasComisiones"
        Me.Text = "frmEsquemasComisiones"
        Me.Controls.SetChildIndex(Me.clcComisionContados, 0)
        Me.Controls.SetChildIndex(Me.lblComisionContados, 0)
        Me.Controls.SetChildIndex(Me.clcComisionCreditos, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcComisionFonacot, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.chkRestarSueldo, 0)
        Me.Controls.SetChildIndex(Me.chkUsarTabla, 0)
        Me.Controls.SetChildIndex(Me.clcComisionGeneral, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.clcEsquema, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        CType(Me.clcComisionContados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcComisionCreditos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcComisionFonacot.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUsarTabla.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRestarSueldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcComisionGeneral.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcEsquema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oEsquemasComisiones As VillarrealBusiness.clsEsquemasComisiones


    Private Sub frmEsquemasComisiones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUsarTabla.CheckedChanged
        If Me.chkUsarTabla.Checked Then
            Me.clcComisionCreditos.Enabled = False
            Me.clcComisionCreditos.EditValue = 0
        Else
            Me.clcComisionCreditos.Enabled = True
        End If
    End Sub

    Private Sub frmEsquemasComisiones_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oEsquemasComisiones.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oEsquemasComisiones.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oEsquemasComisiones.Eliminar(Me.clcEsquema.Value)

        End Select
    End Sub

    Private Sub frmEsquemasComisiones_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oEsquemasComisiones = New VillarrealBusiness.clsEsquemasComisiones
    End Sub

    Private Sub frmEsquemasComisiones_Localize() Handles MyBase.Localize
        Find("esquema", Me.clcEsquema.EditValue)
    End Sub


    Private Sub frmEsquemasComisiones_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oEsquemasComisiones.DespliegaDatos(OwnerForm.Value("esquema"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub
End Class
