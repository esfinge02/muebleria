Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmComisionNetaPagar
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents gpbCalcular As System.Windows.Forms.GroupBox
    Friend WithEvents lblDesde As System.Windows.Forms.Label
    Friend WithEvents dteDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmComisionNetaPagar))
        Me.gpbCalcular = New System.Windows.Forms.GroupBox
        Me.lblDesde = New System.Windows.Forms.Label
        Me.dteDesde = New DevExpress.XtraEditors.DateEdit
        Me.dteHasta = New DevExpress.XtraEditors.DateEdit
        Me.lblHasta = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.gpbCalcular.SuspendLayout()
        CType(Me.dteDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'gpbCalcular
        '
        Me.gpbCalcular.Controls.Add(Me.lblDesde)
        Me.gpbCalcular.Controls.Add(Me.dteDesde)
        Me.gpbCalcular.Controls.Add(Me.dteHasta)
        Me.gpbCalcular.Controls.Add(Me.lblHasta)
        Me.gpbCalcular.Location = New System.Drawing.Point(8, 72)
        Me.gpbCalcular.Name = "gpbCalcular"
        Me.gpbCalcular.Size = New System.Drawing.Size(336, 56)
        Me.gpbCalcular.TabIndex = 59
        Me.gpbCalcular.TabStop = False
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(16, 24)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(43, 16)
        Me.lblDesde.TabIndex = 64
        Me.lblDesde.Tag = ""
        Me.lblDesde.Text = "Desde:"
        Me.lblDesde.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteDesde
        '
        Me.dteDesde.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteDesde.Location = New System.Drawing.Point(64, 22)
        Me.dteDesde.Name = "dteDesde"
        '
        'dteDesde.Properties
        '
        Me.dteDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteDesde.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteDesde.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteDesde.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteDesde.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteDesde.Size = New System.Drawing.Size(96, 23)
        Me.dteDesde.TabIndex = 65
        Me.dteDesde.Tag = ""
        '
        'dteHasta
        '
        Me.dteHasta.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteHasta.Location = New System.Drawing.Point(216, 22)
        Me.dteHasta.Name = "dteHasta"
        '
        'dteHasta.Properties
        '
        Me.dteHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteHasta.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteHasta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteHasta.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteHasta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteHasta.Size = New System.Drawing.Size(96, 23)
        Me.dteHasta.TabIndex = 67
        Me.dteHasta.Tag = ""
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(176, 24)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(41, 16)
        Me.lblHasta.TabIndex = 66
        Me.lblHasta.Tag = ""
        Me.lblHasta.Text = "Hasta:"
        Me.lblHasta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(72, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(TODAS)"
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = True
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(272, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 70
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(8, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 69
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmComisionNetaPagar
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(354, 140)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.gpbCalcular)
        Me.Name = "frmComisionNetaPagar"
        Me.Text = "Reporte de Comisi�n Neta Pagar"
        Me.Controls.SetChildIndex(Me.gpbCalcular, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.gpbCalcular.ResumeLayout(False)
        CType(Me.dteDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oReportes As New VillarrealBusiness.Reportes
    Private oSucursales As New VillarrealBusiness.clsSucursales


    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private Sub frmInformeVentas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.ComisionesDeVentas2014(Me.dteDesde.DateTime, _
          Me.dteHasta.DateTime, Sucursal, False)


        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se Puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then

                Dim oReport As New rptComisionNetaPagar
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Comisiones de Ventas", oReport)
                oReport = Nothing




            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If
    End Sub
    Private Sub frmInformeVentas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.dteDesde.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteHasta.EditValue = CDate(TINApp.FechaServidor)
    End Sub
    Private Sub frmInformeVentas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Me.dteDesde.Text, Me.dteHasta.Text)
        If Response.ErrorFound Then
            Response.ShowError()
            Exit Sub
        End If
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub





End Class
