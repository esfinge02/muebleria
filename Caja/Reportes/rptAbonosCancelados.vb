Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptAbonosCancelados
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghSucursal As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfSucursal As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private lblBodega As DataDynamics.ActiveReports.Label = Nothing
	Private txtBodega As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDocumento As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_concepto1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_cancelacion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cajero1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private txtGranTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Caja.rptAbonosCancelados.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghSucursal = CType(Me.Sections("ghSucursal"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfSucursal = CType(Me.Sections("gfSucursal"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label26 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Line)
		Me.Label32 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label33 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label34 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblBodega = CType(Me.ghSucursal.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtBodega = CType(Me.ghSucursal.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.importe = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreCliente = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDocumento = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.nombre_concepto1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtCliente = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.fecha_cancelacion1 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.cajero1 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Label5 = CType(Me.gfSucursal.Controls(0),DataDynamics.ActiveReports.Label)
		Me.importe1 = CType(Me.gfSucursal.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Label6 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtGranTotal = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptAbonosCancelados_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
