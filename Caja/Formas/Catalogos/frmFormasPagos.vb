Imports Dipros.Utils.Common

Public Class frmFormasPagos
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblForma_Pago As System.Windows.Forms.Label
    Friend WithEvents clcForma_Pago As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblComision As System.Windows.Forms.Label
    Friend WithEvents clcComision As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkManeja_Dolares As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkPredeterminada As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chksolicitar_ulitmos_digitos As DevExpress.XtraEditors.CheckEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmFormasPagos))
        Me.lblForma_Pago = New System.Windows.Forms.Label
        Me.clcForma_Pago = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblComision = New System.Windows.Forms.Label
        Me.clcComision = New Dipros.Editors.TINCalcEdit
        Me.chkManeja_Dolares = New DevExpress.XtraEditors.CheckEdit
        Me.chkPredeterminada = New DevExpress.XtraEditors.CheckEdit
        Me.chksolicitar_ulitmos_digitos = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcForma_Pago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManeja_Dolares.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPredeterminada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chksolicitar_ulitmos_digitos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(145, 28)
        '
        'lblForma_Pago
        '
        Me.lblForma_Pago.AutoSize = True
        Me.lblForma_Pago.Location = New System.Drawing.Point(16, 40)
        Me.lblForma_Pago.Name = "lblForma_Pago"
        Me.lblForma_Pago.Size = New System.Drawing.Size(75, 16)
        Me.lblForma_Pago.TabIndex = 0
        Me.lblForma_Pago.Tag = ""
        Me.lblForma_Pago.Text = "&Forma Pago:"
        Me.lblForma_Pago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcForma_Pago
        '
        Me.clcForma_Pago.EditValue = "0"
        Me.clcForma_Pago.Location = New System.Drawing.Point(97, 40)
        Me.clcForma_Pago.MaxValue = 0
        Me.clcForma_Pago.MinValue = 0
        Me.clcForma_Pago.Name = "clcForma_Pago"
        '
        'clcForma_Pago.Properties
        '
        Me.clcForma_Pago.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcForma_Pago.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcForma_Pago.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcForma_Pago.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcForma_Pago.Properties.Enabled = False
        Me.clcForma_Pago.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcForma_Pago.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcForma_Pago.Size = New System.Drawing.Size(55, 19)
        Me.clcForma_Pago.TabIndex = 1
        Me.clcForma_Pago.Tag = "forma_pago"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(19, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(97, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(300, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'lblComision
        '
        Me.lblComision.AutoSize = True
        Me.lblComision.Location = New System.Drawing.Point(32, 86)
        Me.lblComision.Name = "lblComision"
        Me.lblComision.Size = New System.Drawing.Size(59, 16)
        Me.lblComision.TabIndex = 4
        Me.lblComision.Tag = ""
        Me.lblComision.Text = "C&omisi�n:"
        Me.lblComision.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcComision
        '
        Me.clcComision.EditValue = "0"
        Me.clcComision.Location = New System.Drawing.Point(97, 86)
        Me.clcComision.MaxValue = 0
        Me.clcComision.MinValue = 0
        Me.clcComision.Name = "clcComision"
        '
        'clcComision.Properties
        '
        Me.clcComision.Properties.DisplayFormat.FormatString = "###,###,##0.00"
        Me.clcComision.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComision.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcComision.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcComision.Properties.MaskData.EditMask = "########0.00"
        Me.clcComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcComision.Size = New System.Drawing.Size(55, 19)
        Me.clcComision.TabIndex = 5
        Me.clcComision.Tag = "comision"
        '
        'chkManeja_Dolares
        '
        Me.chkManeja_Dolares.Location = New System.Drawing.Point(96, 112)
        Me.chkManeja_Dolares.Name = "chkManeja_Dolares"
        '
        'chkManeja_Dolares.Properties
        '
        Me.chkManeja_Dolares.Properties.Caption = "Maneja Do&lares"
        Me.chkManeja_Dolares.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkManeja_Dolares.Size = New System.Drawing.Size(120, 19)
        Me.chkManeja_Dolares.TabIndex = 6
        Me.chkManeja_Dolares.Tag = "maneja_dolares"
        '
        'chkPredeterminada
        '
        Me.chkPredeterminada.Location = New System.Drawing.Point(96, 136)
        Me.chkPredeterminada.Name = "chkPredeterminada"
        '
        'chkPredeterminada.Properties
        '
        Me.chkPredeterminada.Properties.Caption = "Predeterminada"
        Me.chkPredeterminada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkPredeterminada.Size = New System.Drawing.Size(104, 19)
        Me.chkPredeterminada.TabIndex = 7
        Me.chkPredeterminada.Tag = "predeterminada"
        Me.chkPredeterminada.ToolTip = "Pr&edeterminada"
        '
        'chksolicitar_ulitmos_digitos
        '
        Me.chksolicitar_ulitmos_digitos.Location = New System.Drawing.Point(96, 160)
        Me.chksolicitar_ulitmos_digitos.Name = "chksolicitar_ulitmos_digitos"
        '
        'chksolicitar_ulitmos_digitos.Properties
        '
        Me.chksolicitar_ulitmos_digitos.Properties.Caption = "Solicitar Ultimos Digitos"
        Me.chksolicitar_ulitmos_digitos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chksolicitar_ulitmos_digitos.Size = New System.Drawing.Size(168, 19)
        Me.chksolicitar_ulitmos_digitos.TabIndex = 8
        Me.chksolicitar_ulitmos_digitos.Tag = "solicitar_ulitmos_digitos"
        Me.chksolicitar_ulitmos_digitos.ToolTip = "Si se marca , en caja se solicitaran los ultimos digitos"
        '
        'frmFormasPagos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(410, 188)
        Me.Controls.Add(Me.chksolicitar_ulitmos_digitos)
        Me.Controls.Add(Me.chkPredeterminada)
        Me.Controls.Add(Me.lblForma_Pago)
        Me.Controls.Add(Me.clcForma_Pago)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblComision)
        Me.Controls.Add(Me.clcComision)
        Me.Controls.Add(Me.chkManeja_Dolares)
        Me.Name = "frmFormasPagos"
        Me.Controls.SetChildIndex(Me.chkManeja_Dolares, 0)
        Me.Controls.SetChildIndex(Me.clcComision, 0)
        Me.Controls.SetChildIndex(Me.lblComision, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcForma_Pago, 0)
        Me.Controls.SetChildIndex(Me.lblForma_Pago, 0)
        Me.Controls.SetChildIndex(Me.chkPredeterminada, 0)
        Me.Controls.SetChildIndex(Me.chksolicitar_ulitmos_digitos, 0)
        CType(Me.clcForma_Pago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManeja_Dolares.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPredeterminada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chksolicitar_ulitmos_digitos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oFormasPagos As VillarrealBusiness.clsFormasPagos
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmFormasPagos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim oDataSet As DataSet
        Dim BitPrimerRegistro As Boolean = False
        Dim mensaje As Boolean = False

        BitPrimerRegistro = oFormasPagos.PrimerRegistro(Me.clcForma_Pago.EditValue, BitPrimerRegistro)
        'ShowMessage(MessageType.MsgInformation, Bit_ERROR, "Prueba")

        Select Case Action
            Case Actions.Insert
                Response = oFormasPagos.Insertar(Me.DataSource, BitPrimerRegistro, mensaje)

            Case Actions.Update
                Response = oFormasPagos.Actualizar(Me.DataSource, BitPrimerRegistro, mensaje)

            Case Actions.Delete
                If Me.chkPredeterminada.EditValue = True Then
                    If BitPrimerRegistro = False Then
                        mensaje = True
                    End If

                End If
                If mensaje = False Then
                    Response = oFormasPagos.Eliminar(clcForma_Pago.Value)
                End If

        End Select
        MostrarMensaje(Action, mensaje)
    End Sub

    Private Sub frmFormasPagos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oFormasPagos.DespliegaDatos(OwnerForm.Value("forma_pago"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmFormasPagos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oFormasPagos = New VillarrealBusiness.clsFormasPagos

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmFormasPagos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oFormasPagos.Validacion(Action, txtDescripcion.Text)
    End Sub

    Private Sub frmFormasPagos_Localize() Handles MyBase.Localize
        Find("forma_pago", CType(Me.clcForma_Pago.EditValue, Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub MostrarMensaje(ByVal Action As Actions, ByVal mensaje As Boolean)
        If mensaje = True Then
            Select Case Action
                Case Actions.Insert
                    ShowMessage(MessageType.MsgInformation, "La forma de Pago se asign� como Predeterminada")
                Case Actions.Update
                    ShowMessage(MessageType.MsgInformation, "La forma de Pago se asign� como Predeterminada")
                Case Actions.Delete
                    ShowMessage(MessageType.MsgInformation, "No se puede borrar una forma de Pago Predeterminada")
            End Select



        End If
    End Sub
#End Region

End Class
