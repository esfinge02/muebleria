Imports Dipros.Utils.Common

Public Class frmCajas
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblCaja As System.Windows.Forms.Label
		Friend WithEvents clcCaja As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblSerie_Recibo As System.Windows.Forms.Label
		Friend WithEvents txtSerie_Recibo As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblFolio_Recibo As System.Windows.Forms.Label
		Friend WithEvents clcFolio_Recibo As Dipros.Editors.TINCalcEdit 
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSerie_Nota_Credito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcFolio_Nota_Credito As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFolioNotaCargo As System.Windows.Forms.Label
    Friend WithEvents lblSerieNotaCargo As System.Windows.Forms.Label
    Friend WithEvents clcFolioNotaCargo As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtSerieNotaCargo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grpNotaCredito As System.Windows.Forms.GroupBox
    Friend WithEvents grpRecibo As System.Windows.Forms.GroupBox
    Friend WithEvents grpNotaCargo As System.Windows.Forms.GroupBox
    Friend WithEvents lblPuertoImpresion As System.Windows.Forms.Label
    Friend WithEvents txtPuertoImpresion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtruta_impresion_ticket As DevExpress.XtraEditors.MemoEdit

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCajas))
        Me.lblCaja = New System.Windows.Forms.Label
        Me.clcCaja = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblSerie_Recibo = New System.Windows.Forms.Label
        Me.txtSerie_Recibo = New DevExpress.XtraEditors.TextEdit
        Me.lblFolio_Recibo = New System.Windows.Forms.Label
        Me.clcFolio_Recibo = New Dipros.Editors.TINCalcEdit
        Me.txtSerie_Nota_Credito = New DevExpress.XtraEditors.TextEdit
        Me.clcFolio_Nota_Credito = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.grpNotaCredito = New System.Windows.Forms.GroupBox
        Me.grpRecibo = New System.Windows.Forms.GroupBox
        Me.grpNotaCargo = New System.Windows.Forms.GroupBox
        Me.lblFolioNotaCargo = New System.Windows.Forms.Label
        Me.lblSerieNotaCargo = New System.Windows.Forms.Label
        Me.clcFolioNotaCargo = New Dipros.Editors.TINCalcEdit
        Me.txtSerieNotaCargo = New DevExpress.XtraEditors.TextEdit
        Me.lblPuertoImpresion = New System.Windows.Forms.Label
        Me.txtPuertoImpresion = New DevExpress.XtraEditors.MemoEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtruta_impresion_ticket = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie_Recibo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio_Recibo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie_Nota_Credito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio_Nota_Credito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpNotaCredito.SuspendLayout()
        Me.grpRecibo.SuspendLayout()
        Me.grpNotaCargo.SuspendLayout()
        CType(Me.clcFolioNotaCargo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerieNotaCargo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPuertoImpresion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtruta_impresion_ticket.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(100, 50)
        '
        'lblCaja
        '
        Me.lblCaja.AutoSize = True
        Me.lblCaja.Location = New System.Drawing.Point(47, 40)
        Me.lblCaja.Name = "lblCaja"
        Me.lblCaja.Size = New System.Drawing.Size(34, 16)
        Me.lblCaja.TabIndex = 0
        Me.lblCaja.Tag = ""
        Me.lblCaja.Text = "Ca&ja:"
        Me.lblCaja.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCaja
        '
        Me.clcCaja.EditValue = "0"
        Me.clcCaja.Location = New System.Drawing.Point(85, 40)
        Me.clcCaja.MaxValue = 0
        Me.clcCaja.MinValue = 0
        Me.clcCaja.Name = "clcCaja"
        '
        'clcCaja.Properties
        '
        Me.clcCaja.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCaja.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCaja.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCaja.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCaja.Properties.Enabled = False
        Me.clcCaja.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCaja.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCaja.Size = New System.Drawing.Size(54, 19)
        Me.clcCaja.TabIndex = 1
        Me.clcCaja.Tag = "caja"
        Me.clcCaja.ToolTip = "Caja"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(9, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(85, 64)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(324, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        Me.txtDescripcion.ToolTip = "descripción de la caja"
        '
        'lblSerie_Recibo
        '
        Me.lblSerie_Recibo.AutoSize = True
        Me.lblSerie_Recibo.Location = New System.Drawing.Point(20, 20)
        Me.lblSerie_Recibo.Name = "lblSerie_Recibo"
        Me.lblSerie_Recibo.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie_Recibo.TabIndex = 0
        Me.lblSerie_Recibo.Tag = ""
        Me.lblSerie_Recibo.Text = "&Serie:"
        Me.lblSerie_Recibo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerie_Recibo
        '
        Me.txtSerie_Recibo.EditValue = ""
        Me.txtSerie_Recibo.Location = New System.Drawing.Point(61, 18)
        Me.txtSerie_Recibo.Name = "txtSerie_Recibo"
        '
        'txtSerie_Recibo.Properties
        '
        Me.txtSerie_Recibo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie_Recibo.Properties.MaxLength = 3
        Me.txtSerie_Recibo.Size = New System.Drawing.Size(54, 20)
        Me.txtSerie_Recibo.TabIndex = 1
        Me.txtSerie_Recibo.Tag = "serie_recibo"
        Me.txtSerie_Recibo.ToolTip = "Serie de recibo"
        '
        'lblFolio_Recibo
        '
        Me.lblFolio_Recibo.AutoSize = True
        Me.lblFolio_Recibo.Location = New System.Drawing.Point(22, 43)
        Me.lblFolio_Recibo.Name = "lblFolio_Recibo"
        Me.lblFolio_Recibo.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio_Recibo.TabIndex = 2
        Me.lblFolio_Recibo.Tag = ""
        Me.lblFolio_Recibo.Text = "&Folio:"
        Me.lblFolio_Recibo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Recibo
        '
        Me.clcFolio_Recibo.EditValue = "0"
        Me.clcFolio_Recibo.Location = New System.Drawing.Point(61, 42)
        Me.clcFolio_Recibo.MaxValue = 0
        Me.clcFolio_Recibo.MinValue = 0
        Me.clcFolio_Recibo.Name = "clcFolio_Recibo"
        '
        'clcFolio_Recibo.Properties
        '
        Me.clcFolio_Recibo.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Recibo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Recibo.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Recibo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Recibo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Recibo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Recibo.Size = New System.Drawing.Size(54, 19)
        Me.clcFolio_Recibo.TabIndex = 3
        Me.clcFolio_Recibo.Tag = "folio_recibo"
        Me.clcFolio_Recibo.ToolTip = "Folio de recibos"
        '
        'txtSerie_Nota_Credito
        '
        Me.txtSerie_Nota_Credito.EditValue = ""
        Me.txtSerie_Nota_Credito.Location = New System.Drawing.Point(57, 18)
        Me.txtSerie_Nota_Credito.Name = "txtSerie_Nota_Credito"
        '
        'txtSerie_Nota_Credito.Properties
        '
        Me.txtSerie_Nota_Credito.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie_Nota_Credito.Properties.MaxLength = 3
        Me.txtSerie_Nota_Credito.Size = New System.Drawing.Size(54, 20)
        Me.txtSerie_Nota_Credito.TabIndex = 1
        Me.txtSerie_Nota_Credito.Tag = "serie_nota_credito"
        Me.txtSerie_Nota_Credito.ToolTip = "Serie de notas de crédito"
        '
        'clcFolio_Nota_Credito
        '
        Me.clcFolio_Nota_Credito.EditValue = "0"
        Me.clcFolio_Nota_Credito.Location = New System.Drawing.Point(57, 42)
        Me.clcFolio_Nota_Credito.MaxValue = 0
        Me.clcFolio_Nota_Credito.MinValue = 0
        Me.clcFolio_Nota_Credito.Name = "clcFolio_Nota_Credito"
        '
        'clcFolio_Nota_Credito.Properties
        '
        Me.clcFolio_Nota_Credito.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Nota_Credito.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Nota_Credito.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Nota_Credito.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Nota_Credito.Size = New System.Drawing.Size(54, 19)
        Me.clcFolio_Nota_Credito.TabIndex = 3
        Me.clcFolio_Nota_Credito.Tag = "folio_nota_credito"
        Me.clcFolio_Nota_Credito.ToolTip = "Folio de notas de crédito"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Tag = ""
        Me.Label2.Text = "S&erie:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Tag = ""
        Me.Label3.Text = "F&olio:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grpNotaCredito
        '
        Me.grpNotaCredito.Controls.Add(Me.Label3)
        Me.grpNotaCredito.Controls.Add(Me.Label2)
        Me.grpNotaCredito.Controls.Add(Me.clcFolio_Nota_Credito)
        Me.grpNotaCredito.Controls.Add(Me.txtSerie_Nota_Credito)
        Me.grpNotaCredito.Location = New System.Drawing.Point(145, 88)
        Me.grpNotaCredito.Name = "grpNotaCredito"
        Me.grpNotaCredito.Size = New System.Drawing.Size(128, 72)
        Me.grpNotaCredito.TabIndex = 5
        Me.grpNotaCredito.TabStop = False
        Me.grpNotaCredito.Text = "Nota de Crédito"
        '
        'grpRecibo
        '
        Me.grpRecibo.Controls.Add(Me.txtSerie_Recibo)
        Me.grpRecibo.Controls.Add(Me.clcFolio_Recibo)
        Me.grpRecibo.Controls.Add(Me.lblSerie_Recibo)
        Me.grpRecibo.Controls.Add(Me.lblFolio_Recibo)
        Me.grpRecibo.Location = New System.Drawing.Point(9, 88)
        Me.grpRecibo.Name = "grpRecibo"
        Me.grpRecibo.Size = New System.Drawing.Size(128, 72)
        Me.grpRecibo.TabIndex = 4
        Me.grpRecibo.TabStop = False
        Me.grpRecibo.Text = "Recibo"
        '
        'grpNotaCargo
        '
        Me.grpNotaCargo.Controls.Add(Me.lblFolioNotaCargo)
        Me.grpNotaCargo.Controls.Add(Me.lblSerieNotaCargo)
        Me.grpNotaCargo.Controls.Add(Me.clcFolioNotaCargo)
        Me.grpNotaCargo.Controls.Add(Me.txtSerieNotaCargo)
        Me.grpNotaCargo.Location = New System.Drawing.Point(281, 88)
        Me.grpNotaCargo.Name = "grpNotaCargo"
        Me.grpNotaCargo.Size = New System.Drawing.Size(128, 72)
        Me.grpNotaCargo.TabIndex = 6
        Me.grpNotaCargo.TabStop = False
        Me.grpNotaCargo.Text = "Nota de Cargo"
        '
        'lblFolioNotaCargo
        '
        Me.lblFolioNotaCargo.AutoSize = True
        Me.lblFolioNotaCargo.Location = New System.Drawing.Point(18, 43)
        Me.lblFolioNotaCargo.Name = "lblFolioNotaCargo"
        Me.lblFolioNotaCargo.Size = New System.Drawing.Size(35, 16)
        Me.lblFolioNotaCargo.TabIndex = 2
        Me.lblFolioNotaCargo.Tag = ""
        Me.lblFolioNotaCargo.Text = "Fo&lio:"
        Me.lblFolioNotaCargo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSerieNotaCargo
        '
        Me.lblSerieNotaCargo.AutoSize = True
        Me.lblSerieNotaCargo.Location = New System.Drawing.Point(16, 20)
        Me.lblSerieNotaCargo.Name = "lblSerieNotaCargo"
        Me.lblSerieNotaCargo.Size = New System.Drawing.Size(37, 16)
        Me.lblSerieNotaCargo.TabIndex = 0
        Me.lblSerieNotaCargo.Tag = ""
        Me.lblSerieNotaCargo.Text = "Ser&ie:"
        Me.lblSerieNotaCargo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolioNotaCargo
        '
        Me.clcFolioNotaCargo.EditValue = "0"
        Me.clcFolioNotaCargo.Location = New System.Drawing.Point(57, 42)
        Me.clcFolioNotaCargo.MaxValue = 0
        Me.clcFolioNotaCargo.MinValue = 0
        Me.clcFolioNotaCargo.Name = "clcFolioNotaCargo"
        '
        'clcFolioNotaCargo.Properties
        '
        Me.clcFolioNotaCargo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioNotaCargo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolioNotaCargo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolioNotaCargo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolioNotaCargo.Size = New System.Drawing.Size(54, 19)
        Me.clcFolioNotaCargo.TabIndex = 3
        Me.clcFolioNotaCargo.Tag = "folio_nota_cargo"
        Me.clcFolioNotaCargo.ToolTip = "Folio de notas de cargo"
        '
        'txtSerieNotaCargo
        '
        Me.txtSerieNotaCargo.EditValue = ""
        Me.txtSerieNotaCargo.Location = New System.Drawing.Point(57, 18)
        Me.txtSerieNotaCargo.Name = "txtSerieNotaCargo"
        '
        'txtSerieNotaCargo.Properties
        '
        Me.txtSerieNotaCargo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerieNotaCargo.Properties.MaxLength = 3
        Me.txtSerieNotaCargo.Size = New System.Drawing.Size(54, 20)
        Me.txtSerieNotaCargo.TabIndex = 1
        Me.txtSerieNotaCargo.Tag = "serie_nota_cargo"
        Me.txtSerieNotaCargo.ToolTip = "Serie de notas de cargo"
        '
        'lblPuertoImpresion
        '
        Me.lblPuertoImpresion.AutoSize = True
        Me.lblPuertoImpresion.Location = New System.Drawing.Point(8, 169)
        Me.lblPuertoImpresion.Name = "lblPuertoImpresion"
        Me.lblPuertoImpresion.Size = New System.Drawing.Size(104, 16)
        Me.lblPuertoImpresion.TabIndex = 7
        Me.lblPuertoImpresion.Tag = ""
        Me.lblPuertoImpresion.Text = "Puerto Impresión:"
        Me.lblPuertoImpresion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPuertoImpresion
        '
        Me.txtPuertoImpresion.EditValue = ""
        Me.txtPuertoImpresion.Location = New System.Drawing.Point(117, 169)
        Me.txtPuertoImpresion.Name = "txtPuertoImpresion"
        '
        'txtPuertoImpresion.Properties
        '
        Me.txtPuertoImpresion.Properties.MaxLength = 100
        Me.txtPuertoImpresion.Size = New System.Drawing.Size(291, 48)
        Me.txtPuertoImpresion.TabIndex = 8
        Me.txtPuertoImpresion.Tag = "puerto_impresion"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 224)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(148, 16)
        Me.Label4.TabIndex = 9
        Me.Label4.Tag = ""
        Me.Label4.Text = "Ruta Impresión de Ticket:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtruta_impresion_ticket
        '
        Me.txtruta_impresion_ticket.EditValue = ""
        Me.txtruta_impresion_ticket.Location = New System.Drawing.Point(160, 224)
        Me.txtruta_impresion_ticket.Name = "txtruta_impresion_ticket"
        '
        'txtruta_impresion_ticket.Properties
        '
        Me.txtruta_impresion_ticket.Properties.MaxLength = 100
        Me.txtruta_impresion_ticket.Size = New System.Drawing.Size(248, 48)
        Me.txtruta_impresion_ticket.TabIndex = 10
        Me.txtruta_impresion_ticket.Tag = "ruta_impresion_ticket"
        '
        'frmCajas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(418, 284)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtruta_impresion_ticket)
        Me.Controls.Add(Me.lblPuertoImpresion)
        Me.Controls.Add(Me.txtPuertoImpresion)
        Me.Controls.Add(Me.grpNotaCargo)
        Me.Controls.Add(Me.grpRecibo)
        Me.Controls.Add(Me.grpNotaCredito)
        Me.Controls.Add(Me.lblCaja)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.clcCaja)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmCajas"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcCaja, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblCaja, 0)
        Me.Controls.SetChildIndex(Me.grpNotaCredito, 0)
        Me.Controls.SetChildIndex(Me.grpRecibo, 0)
        Me.Controls.SetChildIndex(Me.grpNotaCargo, 0)
        Me.Controls.SetChildIndex(Me.txtPuertoImpresion, 0)
        Me.Controls.SetChildIndex(Me.lblPuertoImpresion, 0)
        Me.Controls.SetChildIndex(Me.txtruta_impresion_ticket, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        CType(Me.clcCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie_Recibo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio_Recibo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie_Nota_Credito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio_Nota_Credito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpNotaCredito.ResumeLayout(False)
        Me.grpRecibo.ResumeLayout(False)
        Me.grpNotaCargo.ResumeLayout(False)
        CType(Me.clcFolioNotaCargo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerieNotaCargo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPuertoImpresion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtruta_impresion_ticket.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCajas As VillarrealBusiness.clsCajas
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
	Private Sub frmCajas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
		Select Case Action
		Case Actions.Insert
						Response = oCajas.Insertar(Me.DataSource)

		Case Actions.Update
						Response = oCajas.Actualizar(Me.DataSource)

		Case Actions.Delete
						Response = oCajas.Eliminar(clcCaja.value)

		End Select
	End Sub

	Private Sub frmCajas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
		Response = oCajas.DespliegaDatos(OwnerForm.Value("caja"))
		If Not Response.ErrorFound Then
			Dim oDataSet As DataSet
			oDataSet = Response.Value
			Me.DataSource = oDataSet
		End If

        
	End Sub

	Private Sub frmCajas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
		oCajas = New VillarrealBusiness.clsCajas

		Select Case Action
			Case Actions.Insert
			Case Actions.Update
			Case Actions.Delete
		End Select
	End Sub

	Private Sub frmCajas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCajas.Validacion(Action, Me.txtDescripcion.Text, Me.txtSerie_Recibo.Text, Me.txtSerie_Nota_Credito.Text, Me.txtSerieNotaCargo.Text)
	End Sub

	Private Sub frmCajas_Localize() Handles MyBase.Localize
        Find("caja", Me.clcCaja.Value)

	End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"


#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

    

End Class
