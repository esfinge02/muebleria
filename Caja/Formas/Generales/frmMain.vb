Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuCatCajas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatCajeros As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosCajas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatFormasPagos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenIngresos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepIngresosxCobrador As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepIngresosxFormaPago As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuRepVentasCobradas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCancelacionAbonos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCambioCheques As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProDepositosBancarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuResumenIngresosContabilidad As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCapturaVisitasClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSugerenciaCobroProximasVisitas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMovimientosCliente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProGenerarNotaCredito As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProNotasCargoDescuentosAnticipados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProSubClientes As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuProNotasCredito As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProNotasCargo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDepositosBancarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepAbonosCancelados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatTiposDepositos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProImpresionNCGeneral As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProTimbrarFacturas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProReimpresionAbonosCFS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatEjemplo As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatCajas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatCajeros = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosCajas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatFormasPagos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenIngresos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuResumenIngresosContabilidad = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepIngresosxCobrador = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepIngresosxFormaPago = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuRepVentasCobradas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCancelacionAbonos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCambioCheques = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProDepositosBancarios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCapturaVisitasClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSugerenciaCobroProximasVisitas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMovimientosCliente = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProGenerarNotaCredito = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProNotasCargoDescuentosAnticipados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProSubClientes = New DevExpress.XtraBars.BarSubItem
        Me.mnuProNotasCredito = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProNotasCargo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDepositosBancarios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepAbonosCancelados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatTiposDepositos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProImpresionNCGeneral = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProTimbrarFacturas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProReimpresionAbonosCFS = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatEjemplo = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatCajas, Me.mnuCatCajeros, Me.mnuProMovimientosCajas, Me.mnuCatFormasPagos, Me.mnuRepResumenIngresos, Me.mnuResumenIngresosContabilidad, Me.mnuRepIngresosxCobrador, Me.mnuRepIngresosxFormaPago, Me.BarStaticItem1, Me.mnuRepVentasCobradas, Me.mnuProCancelacionAbonos, Me.mnuProCambioCheques, Me.mnuProDepositosBancarios, Me.mnuProCapturaVisitasClientes, Me.mnuRepSugerenciaCobroProximasVisitas, Me.mnuProMovimientosCliente, Me.mnuProGenerarNotaCredito, Me.mnuProNotasCargoDescuentosAnticipados, Me.mnuProSubClientes, Me.mnuProNotasCredito, Me.mnuProNotasCargo, Me.mnuRepDepositosBancarios, Me.mnuRepAbonosCancelados, Me.mnuCatTiposDepositos, Me.mnuProImpresionNCGeneral, Me.mnuProTimbrarFacturas, Me.mnuProReimpresionAbonosCFS, Me.mnuCatEjemplo})
        Me.BarManager.MaxItemId = 137
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCajas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCajeros), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatFormasPagos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatTiposDepositos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatEjemplo)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCambioCheques), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCancelacionAbonos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProDepositosBancarios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosCajas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCapturaVisitasClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMovimientosCliente), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProSubClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProImpresionNCGeneral), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProTimbrarFacturas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProReimpresionAbonosCFS)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenIngresos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuResumenIngresosContabilidad), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepIngresosxCobrador), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepIngresosxFormaPago), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepVentasCobradas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSugerenciaCobroProximasVisitas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDepositosBancarios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepAbonosCancelados)})
        '
        'mnuCatCajas
        '
        Me.mnuCatCajas.Caption = "Ca&jas"
        Me.mnuCatCajas.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCajas.Id = 105
        Me.mnuCatCajas.ImageIndex = 0
        Me.mnuCatCajas.Name = "mnuCatCajas"
        '
        'mnuCatCajeros
        '
        Me.mnuCatCajeros.Caption = "Caj&eros"
        Me.mnuCatCajeros.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCajeros.Id = 106
        Me.mnuCatCajeros.ImageIndex = 3
        Me.mnuCatCajeros.Name = "mnuCatCajeros"
        '
        'mnuProMovimientosCajas
        '
        Me.mnuProMovimientosCajas.Caption = "&Movimientos de Caja"
        Me.mnuProMovimientosCajas.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProMovimientosCajas.Id = 108
        Me.mnuProMovimientosCajas.ImageIndex = 2
        Me.mnuProMovimientosCajas.Name = "mnuProMovimientosCajas"
        '
        'mnuCatFormasPagos
        '
        Me.mnuCatFormasPagos.Caption = "&Formas de Pago"
        Me.mnuCatFormasPagos.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatFormasPagos.Id = 109
        Me.mnuCatFormasPagos.ImageIndex = 4
        Me.mnuCatFormasPagos.Name = "mnuCatFormasPagos"
        '
        'mnuRepResumenIngresos
        '
        Me.mnuRepResumenIngresos.Caption = "Resumen de In&gresos"
        Me.mnuRepResumenIngresos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenIngresos.Id = 111
        Me.mnuRepResumenIngresos.ImageIndex = 7
        Me.mnuRepResumenIngresos.Name = "mnuRepResumenIngresos"
        '
        'mnuResumenIngresosContabilidad
        '
        Me.mnuResumenIngresosContabilidad.Caption = "Re&sumen de Contabilidad"
        Me.mnuResumenIngresosContabilidad.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuResumenIngresosContabilidad.Id = 112
        Me.mnuResumenIngresosContabilidad.ImageIndex = 5
        Me.mnuResumenIngresosContabilidad.Name = "mnuResumenIngresosContabilidad"
        Me.mnuResumenIngresosContabilidad.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'mnuRepIngresosxCobrador
        '
        Me.mnuRepIngresosxCobrador.Caption = "&Ingresos por Cobrador"
        Me.mnuRepIngresosxCobrador.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepIngresosxCobrador.Id = 113
        Me.mnuRepIngresosxCobrador.ImageIndex = 6
        Me.mnuRepIngresosxCobrador.Name = "mnuRepIngresosxCobrador"
        '
        'mnuRepIngresosxFormaPago
        '
        Me.mnuRepIngresosxFormaPago.Caption = "Ingres&os por Forma de Pago"
        Me.mnuRepIngresosxFormaPago.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepIngresosxFormaPago.Id = 115
        Me.mnuRepIngresosxFormaPago.ImageIndex = 8
        Me.mnuRepIngresosxFormaPago.Name = "mnuRepIngresosxFormaPago"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(43, 475)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Offset = 1
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 116
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'mnuRepVentasCobradas
        '
        Me.mnuRepVentasCobradas.Caption = "V&entas Cobradas"
        Me.mnuRepVentasCobradas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepVentasCobradas.Id = 117
        Me.mnuRepVentasCobradas.ImageIndex = 9
        Me.mnuRepVentasCobradas.Name = "mnuRepVentasCobradas"
        '
        'mnuProCancelacionAbonos
        '
        Me.mnuProCancelacionAbonos.Caption = "Cancelaci�n de Abonos"
        Me.mnuProCancelacionAbonos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCancelacionAbonos.Id = 118
        Me.mnuProCancelacionAbonos.ImageIndex = 10
        Me.mnuProCancelacionAbonos.Name = "mnuProCancelacionAbonos"
        '
        'mnuProCambioCheques
        '
        Me.mnuProCambioCheques.Caption = "C&ambio de Cheques"
        Me.mnuProCambioCheques.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCambioCheques.Id = 119
        Me.mnuProCambioCheques.ImageIndex = 12
        Me.mnuProCambioCheques.Name = "mnuProCambioCheques"
        '
        'mnuProDepositosBancarios
        '
        Me.mnuProDepositosBancarios.Caption = "&Dep�sitos Bancarios"
        Me.mnuProDepositosBancarios.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProDepositosBancarios.Id = 120
        Me.mnuProDepositosBancarios.ImageIndex = 11
        Me.mnuProDepositosBancarios.Name = "mnuProDepositosBancarios"
        '
        'mnuProCapturaVisitasClientes
        '
        Me.mnuProCapturaVisitasClientes.Caption = "Captura Visitas a Clientes"
        Me.mnuProCapturaVisitasClientes.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProCapturaVisitasClientes.Id = 121
        Me.mnuProCapturaVisitasClientes.Name = "mnuProCapturaVisitasClientes"
        '
        'mnuRepSugerenciaCobroProximasVisitas
        '
        Me.mnuRepSugerenciaCobroProximasVisitas.Caption = "Sugerencia Cobro Proximas Visitas"
        Me.mnuRepSugerenciaCobroProximasVisitas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSugerenciaCobroProximasVisitas.Id = 122
        Me.mnuRepSugerenciaCobroProximasVisitas.Name = "mnuRepSugerenciaCobroProximasVisitas"
        '
        'mnuProMovimientosCliente
        '
        Me.mnuProMovimientosCliente.Caption = "Movimientos de Clientes"
        Me.mnuProMovimientosCliente.Id = 124
        Me.mnuProMovimientosCliente.Name = "mnuProMovimientosCliente"
        '
        'mnuProGenerarNotaCredito
        '
        Me.mnuProGenerarNotaCredito.Caption = "Generar Notas de Cr�dito por Descuentos Anticipados"
        Me.mnuProGenerarNotaCredito.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProGenerarNotaCredito.Id = 125
        Me.mnuProGenerarNotaCredito.Name = "mnuProGenerarNotaCredito"
        '
        'mnuProNotasCargoDescuentosAnticipados
        '
        Me.mnuProNotasCargoDescuentosAnticipados.Caption = "Generar Notas de Cargo por Descuentos Anticipados"
        Me.mnuProNotasCargoDescuentosAnticipados.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProNotasCargoDescuentosAnticipados.Id = 126
        Me.mnuProNotasCargoDescuentosAnticipados.Name = "mnuProNotasCargoDescuentosAnticipados"
        '
        'mnuProSubClientes
        '
        Me.mnuProSubClientes.Caption = "Clientes"
        Me.mnuProSubClientes.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProSubClientes.Id = 127
        Me.mnuProSubClientes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProGenerarNotaCredito), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProNotasCargoDescuentosAnticipados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProNotasCredito), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProNotasCargo)})
        Me.mnuProSubClientes.Name = "mnuProSubClientes"
        '
        'mnuProNotasCredito
        '
        Me.mnuProNotasCredito.Caption = "Notas de Cr�dito"
        Me.mnuProNotasCredito.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProNotasCredito.Id = 128
        Me.mnuProNotasCredito.Name = "mnuProNotasCredito"
        '
        'mnuProNotasCargo
        '
        Me.mnuProNotasCargo.Caption = "Notas de Cargo"
        Me.mnuProNotasCargo.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProNotasCargo.Id = 129
        Me.mnuProNotasCargo.Name = "mnuProNotasCargo"
        '
        'mnuRepDepositosBancarios
        '
        Me.mnuRepDepositosBancarios.Caption = "Depositos Bancarios"
        Me.mnuRepDepositosBancarios.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDepositosBancarios.Id = 130
        Me.mnuRepDepositosBancarios.Name = "mnuRepDepositosBancarios"
        '
        'mnuRepAbonosCancelados
        '
        Me.mnuRepAbonosCancelados.Caption = "Abonos Cancelados"
        Me.mnuRepAbonosCancelados.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepAbonosCancelados.Id = 131
        Me.mnuRepAbonosCancelados.Name = "mnuRepAbonosCancelados"
        '
        'mnuCatTiposDepositos
        '
        Me.mnuCatTiposDepositos.Caption = "Tipos Depositos"
        Me.mnuCatTiposDepositos.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatTiposDepositos.Id = 132
        Me.mnuCatTiposDepositos.Name = "mnuCatTiposDepositos"
        '
        'mnuProImpresionNCGeneral
        '
        Me.mnuProImpresionNCGeneral.Caption = "Impresi�n de la NC General"
        Me.mnuProImpresionNCGeneral.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProImpresionNCGeneral.Id = 133
        Me.mnuProImpresionNCGeneral.Name = "mnuProImpresionNCGeneral"
        '
        'mnuProTimbrarFacturas
        '
        Me.mnuProTimbrarFacturas.Caption = "Timbrar Facturas"
        Me.mnuProTimbrarFacturas.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProTimbrarFacturas.Id = 134
        Me.mnuProTimbrarFacturas.Name = "mnuProTimbrarFacturas"
        '
        'mnuProReimpresionAbonosCFS
        '
        Me.mnuProReimpresionAbonosCFS.Caption = "Reimpresiones Abonos y CFS"
        Me.mnuProReimpresionAbonosCFS.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProReimpresionAbonosCFS.Id = 135
        Me.mnuProReimpresionAbonosCFS.Name = "mnuProReimpresionAbonosCFS"
        '
        'mnuCatEjemplo
        '
        Me.mnuCatEjemplo.Caption = "Ejemplo"
        Me.mnuCatEjemplo.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatEjemplo.Id = 136
        Me.mnuCatEjemplo.Name = "mnuCatEjemplo"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(688, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub frmMain_LoadOptions() Handles MyBase.LoadOptions
        With TINApp.Options.Security
            '---------------------------------------------------------------------------------------------
            .Options("mnuProMovimientosCajas").AddAction("FECHA_CAJA_CAJA", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)

        End With
    End Sub

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objEventArgs As DevExpress.XtraBars.ItemClickEventArgs
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim response As Dipros.Utils.Events
        Dim oPermisosEspeciales As New VillarrealBusiness.clsIdentificadoresPermisos


        If Comunes.clsUtilerias.AbrirArchivo(Comunes.Common.Sucursal_Actual) = False Then
            Me.mnuProcesos.Enabled = False
        End If
        If Comunes.clsUtilerias.AbrirArchivoCaja(Comunes.Common.Caja_Actual) = False Then
            Me.mnuProcesos.Enabled = False
        End If
        If Comunes.clsUtilerias.uti_Usuariocajero(TINApp.Connection.User, Comunes.Common.Cajero) = False Then
            Me.mnuProcesos.Enabled = False
            Me.mnuProMovimientosCajas.Enabled = False

        End If


        response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value

            Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre") + "       Caja Actual: " + Comunes.Common.Caja_Actual.ToString
        End If

        ' Para obtener los permisos especiales de la Toolbar

        response = oPermisosEspeciales.ListadoPermisos(TINApp.Connection.User, TINApp.Prefix)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            Dim Row As DataRow
            Dim i As Long = 0
            Dim Cadena_identificadores As String

            odataset = response.Value
            If odataset.Tables(0).Rows.Count > 0 Then
                ReDim Comunes.Common.Identificadores(odataset.Tables(0).Rows.Count() - 1)

                For Each Row In odataset.Tables(0).Rows
                    Comunes.Common.Identificadores.SetValue(Row("identificador"), i)
                    i = i + 1
                    'If i = 1 Then
                    '    Cadena_identificadores = Row("identificador")
                    'Else
                    '    Cadena_identificadores = Cadena_identificadores + "," + Row("identificador")
                    'End If
                Next
                'ShowMessage(MessageType.MsgInformation, "Ids : " + Cadena_identificadores, "Permisos de Devoluciones")

            Else
                'ShowMessage(MessageType.MsgInformation, "No hay registros de Permisos para este usuario.")
            End If
        End If

        objEventArgs = New DevExpress.XtraBars.ItemClickEventArgs(mnuProMovimientosCajas, mnuProMovimientosCajas.Links(0))
        mnuProMovimientosCajas_ItemClick(Me.mnuProMovimientosCajas, objEventArgs)

    End Sub

#Region "Catalogos"
    Public Sub mnuCatCajas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCajas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCajas As New VillarrealBusiness.clsCajas
        With oGrid
            .Title = "Cajas"
            .UpdateTitle = "Cajas"
            .DataSource = AddressOf oCajas.Listado
            .UpdateForm = New frmCajas
            .Format = AddressOf for_cajas_grs
            .MdiParent = Me
            .Show()
        End With
        oCajas = Nothing
    End Sub

    Public Sub mnuCatCajeros_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCajeros.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCajeros As New VillarrealBusiness.clsCajeros
        With oGrid
            .Title = "Cajeros"
            .UpdateTitle = "Cajeros"
            .DataSource = AddressOf oCajeros.Listado
            .UpdateForm = New frmCajeros
            .Format = AddressOf for_cajeros_grs
            .MdiParent = Me
            .Show()
        End With
        oCajeros = Nothing
    End Sub

    Public Sub mnuCatFormasPagos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatFormasPagos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oFormasPagos As New VillarrealBusiness.clsFormasPagos
        With oGrid
            .Title = "Formas de Pagos"
            .UpdateTitle = "Forma de Pago"
            .DataSource = AddressOf oFormasPagos.Listado
            .UpdateForm = New frmFormasPagos
            .Format = AddressOf Comunes.clsFormato.for_formas_pagos_grs
            .MdiParent = Me
            .Show()
        End With
        oFormasPagos = Nothing
    End Sub

    Private Sub mnuCatTiposDepositos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatTiposDepositos.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oTipos As New VillarrealBusiness.clsTiposDepositos
        With oGrid
            .Title = "Tipos de Depositos"
            .UpdateTitle = "Tipo de Deposito"
            .DataSource = AddressOf oTipos.Listado
            .UpdateForm = New Comunes.frmTiposDepositos
            .Format = AddressOf for_tipos_depositos_grs
            .MdiParent = Me
            .Show()
        End With
        oTipos = Nothing
    End Sub


    Private Sub mnuCatEjemplo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatEjemplo.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oTipos As New VillarrealBusiness.clsTiposDepositos
        With oGrid
            .Title = "Tipos de Depositos"
            .UpdateTitle = "Tipo de Deposito"
            .DataSource = AddressOf oTipos.Listado
            .UpdateForm = New Comunes.frmTiposDepositos
            .Format = AddressOf for_tipos_depositos_grs
            .MdiParent = Me
            .Show()
        End With
        oTipos = Nothing
    End Sub

#End Region

#Region "Procesos"
    Public Sub mnuProMovimientosCajas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosCajas.ItemClick
        If Me.mnuProcesos.Enabled = False Then Exit Sub
        Dim frmmovimientos_cajas As New Comunes.frmMovimientosCaja

        With frmmovimientos_cajas
            .MenuOption = e.Item
            .MdiParent = Me
            .EntroCaja = True
            .Text = "Movimientos de Caja"
            .Show()
        End With

    End Sub
    Private Sub mnuProCancelacionAbonos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCancelacionAbonos.ItemClick
        Dim frmcancelacion As New frmCancelacionAbonos

        With frmcancelacion
            .MenuOption = e.Item
            .MdiParent = Me
            .Text = "Cancelaci�n de Abonos"
            .Show()
        End With

    End Sub

    Public Sub mnuProCambioCheques_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCambioCheques.ItemClick
        Dim ofrmCambiosCheques As New Comunes.frmCambiosCheques
        With ofrmCambiosCheques
            .MenuOption = e.Item
            .MdiParent = Me
            .Text = "Cambio de Cheques"
            .Show()
        End With
    End Sub

    Public Sub mnuProDepositosBancarios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProDepositosBancarios.ItemClick
        Dim oGrid As New Comunes.brwDepositosBancarios
        With oGrid
            .MenuOption = e.Item
            .Title = "Dep�sitos Bancarios"
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Public Sub mnuProCapturaVisitasClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCapturaVisitasClientes.ItemClick
        Dim oGrid As New Comunes.brwCapturaVisitasClientes
        With oGrid
            .MenuOption = e.Item
            .Title = "Captura de Visitas de Clientes"
            .UpdateTitle = "Visita de Clientes"
            .UpdateForm = New Comunes.frmCapturaVisitasClientes
            .Format = AddressOf Comunes.clsFormato.for_captura_visitas_clientes_grs
            .MdiParent = Me
            .Show()
        End With


    End Sub


    Public Sub mnuProMovimientosCliente_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMovimientosCliente.ItemClick
        Dim oForm As New Comunes.frmMovimientosClientes
        With oForm
            .MenuOption = e.Item
            .Title = "Movimientos del Cliente"
            .MdiParent = Me
            .Show()
        End With
    End Sub


    Public Sub mnuProGenerarNotaCredito_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProGenerarNotaCredito.ItemClick
        Dim oForm As New Comunes.frmGeneracionNotasCredito
        oForm.MenuOption = e.Item
        oForm.MdiParent = Me
        oForm.Title = "Generar Notas de Cr�dito por Descuentos Anticipados"
        oForm.Show()
    End Sub
    Public Sub mnuProNotasCargoDescuentosAnticipados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProNotasCargoDescuentosAnticipados.ItemClick
        Dim oForm As New Comunes.frmGeneracionNotasCargoDescuentosAnticipados
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Notas de Cargo por Descuentos Anticipados"
            .Show()
        End With
    End Sub
    Public Sub mnuProNotasCredito_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProNotasCredito.ItemClick
        Dim oForm As New Comunes.frmNotasCredito
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Notas de Cr�dito"
            .Show()
        End With
    End Sub
    Public Sub mnuProNotasCargo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProNotasCargo.ItemClick
        Dim oForm As New Comunes.frmNotasCargo
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Notas de Cargo"
            .Show()
        End With
    End Sub

    Public Sub mnuProImpresionNCGeneral_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProImpresionNCGeneral.ItemClick
        Dim oForm As New frmImprimeNotaCargoGeneral
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Impresi�n de Notas de Cargo Generales"
            .Show()
        End With
    End Sub

    Private Sub mnuProTimbrarFacturas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProTimbrarFacturas.ItemClick
        Dim oForm As New Comunes.frmTimbrarFacturas
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Timbrado de Facturas"
            .Show()
        End With
    End Sub

    Public Sub mnuProReimpresionAbonosCFS_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProReimpresionAbonosCFS.ItemClick
        Dim oForm As New Comunes.frmReimpresionRecibosCFS
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Reimpresi�n de Abonos y CFS"
            .Show()
        End With
    End Sub
#End Region

#Region "Reportes"

    Public Sub mnuRepSugerenciaCobroProximasVisitas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSugerenciaCobroProximasVisitas.ItemClick
        Dim oForm As New Comunes.frmReporteCobranzaProximasVisitas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Sugerencia de Cobro Proximas Visitas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepResumenIngresos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenIngresos.ItemClick
        Dim oForm As New Comunes.frmResumenIngresos
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Ingresos"
        oForm.Action = Actions.Report
        oForm.Ingresos = True
        oForm.Show()
    End Sub
    Public Sub mnuRepIngresosxCobrador_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepIngresosxCobrador.ItemClick
        Dim oForm As New Comunes.frmIngresosxCobrador
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ingresos por Cobrador"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepIngresosxFormaPago_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepIngresosxFormaPago.ItemClick
        Dim oForm As New Comunes.frmIngresosxFormaPago
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ingresos por Forma de Pago"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepVentasCobradas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepVentasCobradas.ItemClick
        Dim oForm As New frmVentasCobradas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ventas Cobradas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuResumenIngresosContabilidad_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuResumenIngresosContabilidad.ItemClick
        Dim oForm As New Comunes.frmResumenContabilidad
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Ingresos de Contabilidad"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepDepositosBancarios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDepositosBancarios.ItemClick
        Dim oForm As New Comunes.frmRepDepositosBancarios
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Depostios Bancarios"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepAbonosCancelados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepAbonosCancelados.ItemClick
        Dim oForm As New frmAbonosCancelados
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Abonos Cancelados"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

#End Region

#Region "Herramientas"

#End Region







   
End Class
