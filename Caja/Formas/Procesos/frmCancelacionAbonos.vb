Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCancelacionAbonos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcFolio As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtCajero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCaja As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtFolioAbono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSerieAbono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnBuscarAbono As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dteFechaAbono As DevExpress.XtraEditors.DateEdit
    Friend WithEvents clcImporte As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCancelacionAbonos))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.clcFolio = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit
        Me.clcImporte = New DevExpress.XtraEditors.CalcEdit
        Me.dteFechaAbono = New DevExpress.XtraEditors.DateEdit
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.txtCaja = New DevExpress.XtraEditors.TextEdit
        Me.txtCuenta = New DevExpress.XtraEditors.TextEdit
        Me.txtFolioAbono = New DevExpress.XtraEditors.TextEdit
        Me.txtSerieAbono = New DevExpress.XtraEditors.TextEdit
        Me.txtCajero = New DevExpress.XtraEditors.TextEdit
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnBuscarAbono = New DevExpress.XtraEditors.SimpleButton
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFolioAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerieAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCajero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1404, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(14, 24)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(72, 24)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = True
        Me.lkpSucursal.Size = New System.Drawing.Size(296, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblCliente)
        Me.GroupBox1.Controls.Add(Me.lkpCliente)
        Me.GroupBox1.Controls.Add(Me.txtSerie)
        Me.GroupBox1.Controls.Add(Me.clcFolio)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lkpSucursal)
        Me.GroupBox1.Controls.Add(Me.lblSucursal)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(384, 104)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(72, 72)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie.Properties.MaxLength = 3
        Me.txtSerie.Size = New System.Drawing.Size(56, 20)
        Me.txtSerie.TabIndex = 5
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio.Location = New System.Drawing.Point(192, 72)
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.MaxLength = 9
        Me.clcFolio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolio.Size = New System.Drawing.Size(56, 20)
        Me.clcFolio.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(152, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Folio:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Serie:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtConcepto)
        Me.GroupBox2.Controls.Add(Me.clcImporte)
        Me.GroupBox2.Controls.Add(Me.dteFechaAbono)
        Me.GroupBox2.Controls.Add(Me.txtNombre)
        Me.GroupBox2.Controls.Add(Me.txtCaja)
        Me.GroupBox2.Controls.Add(Me.txtCuenta)
        Me.GroupBox2.Controls.Add(Me.txtFolioAbono)
        Me.GroupBox2.Controls.Add(Me.txtSerieAbono)
        Me.GroupBox2.Controls.Add(Me.txtCajero)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(8, 152)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(512, 144)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(302, 69)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 16)
        Me.Label10.TabIndex = 17
        Me.Label10.Tag = ""
        Me.Label10.Text = "Concepto:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(368, 64)
        Me.txtConcepto.Name = "txtConcepto"
        '
        'txtConcepto.Properties
        '
        Me.txtConcepto.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.txtConcepto.Size = New System.Drawing.Size(128, 20)
        Me.txtConcepto.TabIndex = 16
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImporte.Location = New System.Drawing.Point(368, 104)
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.clcImporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImporte.Size = New System.Drawing.Size(128, 20)
        Me.clcImporte.TabIndex = 15
        '
        'dteFechaAbono
        '
        Me.dteFechaAbono.EditValue = New Date(2007, 6, 15, 0, 0, 0, 0)
        Me.dteFechaAbono.Location = New System.Drawing.Point(96, 64)
        Me.dteFechaAbono.Name = "dteFechaAbono"
        '
        'dteFechaAbono.Properties
        '
        Me.dteFechaAbono.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaAbono.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaAbono.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaAbono.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.dteFechaAbono.Size = New System.Drawing.Size(128, 20)
        Me.dteFechaAbono.TabIndex = 5
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(96, 40)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.txtNombre.Size = New System.Drawing.Size(128, 20)
        Me.txtNombre.TabIndex = 3
        '
        'txtCaja
        '
        Me.txtCaja.EditValue = ""
        Me.txtCaja.Location = New System.Drawing.Point(368, 40)
        Me.txtCaja.Name = "txtCaja"
        '
        'txtCaja.Properties
        '
        Me.txtCaja.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.txtCaja.Size = New System.Drawing.Size(128, 20)
        Me.txtCaja.TabIndex = 13
        '
        'txtCuenta
        '
        Me.txtCuenta.EditValue = ""
        Me.txtCuenta.Location = New System.Drawing.Point(96, 16)
        Me.txtCuenta.Name = "txtCuenta"
        '
        'txtCuenta.Properties
        '
        Me.txtCuenta.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.txtCuenta.Size = New System.Drawing.Size(128, 20)
        Me.txtCuenta.TabIndex = 1
        '
        'txtFolioAbono
        '
        Me.txtFolioAbono.EditValue = ""
        Me.txtFolioAbono.Location = New System.Drawing.Point(96, 112)
        Me.txtFolioAbono.Name = "txtFolioAbono"
        '
        'txtFolioAbono.Properties
        '
        Me.txtFolioAbono.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.txtFolioAbono.Size = New System.Drawing.Size(128, 20)
        Me.txtFolioAbono.TabIndex = 9
        '
        'txtSerieAbono
        '
        Me.txtSerieAbono.EditValue = ""
        Me.txtSerieAbono.Location = New System.Drawing.Point(96, 88)
        Me.txtSerieAbono.Name = "txtSerieAbono"
        '
        'txtSerieAbono.Properties
        '
        Me.txtSerieAbono.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.txtSerieAbono.Size = New System.Drawing.Size(128, 20)
        Me.txtSerieAbono.TabIndex = 7
        '
        'txtCajero
        '
        Me.txtCajero.EditValue = ""
        Me.txtCajero.Location = New System.Drawing.Point(368, 16)
        Me.txtCajero.Name = "txtCajero"
        '
        'txtCajero.Properties
        '
        Me.txtCajero.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.txtCajero.Size = New System.Drawing.Size(128, 20)
        Me.txtCajero.TabIndex = 11
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(309, 104)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(53, 16)
        Me.Label12.TabIndex = 14
        Me.Label12.Tag = ""
        Me.Label12.Text = "Importe:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(320, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 16)
        Me.Label11.TabIndex = 10
        Me.Label11.Tag = ""
        Me.Label11.Text = "Cajero:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(328, 40)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(34, 16)
        Me.Label9.TabIndex = 12
        Me.Label9.Tag = ""
        Me.Label9.Text = "Caja:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(10, 69)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Tag = ""
        Me.Label8.Text = "Fecha Abono:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(14, 93)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(76, 16)
        Me.Label7.TabIndex = 6
        Me.Label7.Tag = ""
        Me.Label7.Text = "Serie Abono:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 117)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 16)
        Me.Label6.TabIndex = 8
        Me.Label6.Tag = ""
        Me.Label6.Text = "Folio Abono:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(37, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Tag = ""
        Me.Label5.Text = "Nombre:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(42, 21)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Tag = ""
        Me.Label4.Text = "Cuenta:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnBuscarAbono
        '
        Me.btnBuscarAbono.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.btnBuscarAbono.Location = New System.Drawing.Point(416, 40)
        Me.btnBuscarAbono.Name = "btnBuscarAbono"
        Me.btnBuscarAbono.Size = New System.Drawing.Size(96, 32)
        Me.btnBuscarAbono.TabIndex = 1
        Me.btnBuscarAbono.Text = "&Buscar Abono"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(22, 48)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 2
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(72, 48)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(800, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(296, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 3
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = "Seleccione un cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'frmCancelacionAbonos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(530, 304)
        Me.Controls.Add(Me.btnBuscarAbono)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmCancelacionAbonos"
        Me.Text = "frmCancelacionAbonos"
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.btnBuscarAbono, 0)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFolioAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerieAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCajero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private AbonoEncontrado As Boolean = False


    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oMovimientosCobrar As New VillarrealBusiness.clsMovimientosCobrar
    Private oClientes As New VillarrealBusiness.clsClientes

    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property

    Private FolioElectronico As Long

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCancelacionAbonos_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmCancelacionAbonos_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmCancelacionAbonos_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        Dim response As New Events
        response = oClientes.Recalcula_saldo_documentos(cliente)

    End Sub

    Private Sub frmCancelacionAbonos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
    End Sub
    Private Sub frmCancelacionAbonos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oMovimientosCobrar.CancelarAbono(Me.Sucursal, Me.txtConcepto.Text, Me.txtSerieAbono.Text, Me.clcFolio.Value, CType(Me.txtCuenta.Text, Long), CDate(TINApp.FechaServidor))

        If Not Response.ErrorFound Then
            Response = Me.oMovimientosCobrar.CancelarTimbrado(Me.txtSerieAbono.Text, Me.clcFolio.Value, Me.FolioElectronico)
        End If
    End Sub
    Private Sub frmCancelacionAbonos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = New Events

        If AbonoEncontrado = False Then

            Response.Message = "El Abono es Requerido"
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCancelacionAbonos()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_caja_grl(Me.lkpCliente)
    End Sub


    Private Sub btnBuscarAbono_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarAbono.Click
        BuscarAbono()
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub BuscarAbono()
        Dim oresponse As Events

        oresponse = oMovimientosCobrar.BuscaAbono(Sucursal, cliente, Me.txtSerie.Text, Me.clcFolio.Value)
        If Not oresponse.ErrorFound = True Then
            Dim oDataSet As DataSet
            oDataSet = oresponse.Value

            'DAM - 16/06/2007
            'Verifico si se encontro el abono para prender la bandero de encontrado
            ' y asignar los datos correspondientes del abono, en caso contrario pongo la bandera en falso
            ' y limpio los controles con datos anteriores
            If oDataSet.Tables(0).Rows.Count > 0 Then

                If CType(oDataSet.Tables(0).Rows(0).Item("estatus_general_abono"), Boolean) = True Then
                    If oDataSet.Tables(0).Rows(0).Item("dias") <= 2 Then
                        AbonoEncontrado = True
                        Me.txtCuenta.Text = oDataSet.Tables(0).Rows(0).Item("cliente")
                        Me.txtNombre.Text = oDataSet.Tables(0).Rows(0).Item("nom_cliente")
                        Me.dteFechaAbono.DateTime = oDataSet.Tables(0).Rows(0).Item("fecha")
                        Me.txtSerieAbono.Text = oDataSet.Tables(0).Rows(0).Item("serie")
                        Me.txtFolioAbono.Text = oDataSet.Tables(0).Rows(0).Item("folio")
                        Me.txtCaja.Text = oDataSet.Tables(0).Rows(0).Item("caja")
                        Me.txtCajero.Text = oDataSet.Tables(0).Rows(0).Item("cajero")
                        Me.txtConcepto.Text = oDataSet.Tables(0).Rows(0).Item("concepto")
                        Me.clcImporte.Text = oDataSet.Tables(0).Rows(0).Item("importe")
                        FolioElectronico = oDataSet.Tables(0).Rows(0).Item("folio_factura_electronica")
                    Else
                        ShowMessage(MessageType.MsgError, "No se permite cancelar un abono si ya tiene m�s de 2 d�as que se realiz�")
                    End If
                Else
                    ShowMessage(MessageType.MsgError, "No se permite cancelar un abono si la factura � nota de cargo ya esta cancelada")
                End If
            Else
                AbonoEncontrado = False
                Me.txtCuenta.Text = ""
                Me.txtNombre.Text = ""
                Me.dteFechaAbono.DateTime = Now.Date
                Me.txtSerieAbono.Text = ""
                Me.txtFolioAbono.Text = ""
                Me.txtCaja.Text = ""
                Me.txtCajero.Text = ""
                Me.txtConcepto.Text = ""
                Me.clcImporte.Text = 0
                FolioElectronico = 0
                End If


            End If
    End Sub
#End Region



End Class

