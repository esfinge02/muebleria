Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmAutorizacionesBonificacionesInteres
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCaja As System.Windows.Forms.Label
    Friend WithEvents lkpCaja As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents chkUtilizada As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grMovimientos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvMovimientos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcChecar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkChecar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSaldoVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcInteres As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumentos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcenajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfactor_enajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcUltimoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNotaCargo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPermitePagarUltimoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents clcImporteDocumentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblIntereses As System.Windows.Forms.Label
    Friend WithEvents clcIntereses As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcBonificacion As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAutorizacionesBonificacionesInteres))
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblCaja = New System.Windows.Forms.Label
        Me.lkpCaja = New Dipros.Editors.TINMultiLookup
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcBonificacion = New Dipros.Editors.TINCalcEdit
        Me.chkUtilizada = New DevExpress.XtraEditors.CheckEdit
        Me.grMovimientos = New DevExpress.XtraGrid.GridControl
        Me.grvMovimientos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcChecar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkChecar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSaldoVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcInteres = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumentos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcenajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfactor_enajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcUltimoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNotaCargo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPermitePagarUltimoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label10 = New System.Windows.Forms.Label
        Me.clcImporteDocumentos = New Dipros.Editors.TINCalcEdit
        Me.lblIntereses = New System.Windows.Forms.Label
        Me.clcIntereses = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcBonificacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUtilizada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkChecar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporteDocumentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2535, 28)
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(63, 40)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 0
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(104, 40)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(80, 19)
        Me.clcFolio.TabIndex = 1
        Me.clcFolio.Tag = "folio"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(51, 63)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 2
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(104, 62)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(368, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 3
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblCaja
        '
        Me.lblCaja.AutoSize = True
        Me.lblCaja.Location = New System.Drawing.Point(64, 86)
        Me.lblCaja.Name = "lblCaja"
        Me.lblCaja.Size = New System.Drawing.Size(34, 16)
        Me.lblCaja.TabIndex = 4
        Me.lblCaja.Tag = ""
        Me.lblCaja.Text = "Ca&ja:"
        Me.lblCaja.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCaja
        '
        Me.lkpCaja.AllowAdd = False
        Me.lkpCaja.AutoReaload = False
        Me.lkpCaja.DataSource = Nothing
        Me.lkpCaja.DefaultSearchField = ""
        Me.lkpCaja.DisplayMember = "descripcion"
        Me.lkpCaja.EditValue = Nothing
        Me.lkpCaja.Filtered = False
        Me.lkpCaja.InitValue = Nothing
        Me.lkpCaja.Location = New System.Drawing.Point(104, 85)
        Me.lkpCaja.MultiSelect = False
        Me.lkpCaja.Name = "lkpCaja"
        Me.lkpCaja.NullText = ""
        Me.lkpCaja.PopupWidth = CType(400, Long)
        Me.lkpCaja.ReadOnlyControl = False
        Me.lkpCaja.Required = False
        Me.lkpCaja.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCaja.SearchMember = ""
        Me.lkpCaja.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCaja.SelectAll = False
        Me.lkpCaja.Size = New System.Drawing.Size(368, 20)
        Me.lkpCaja.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCaja.TabIndex = 5
        Me.lkpCaja.Tag = "Caja"
        Me.lkpCaja.ToolTip = Nothing
        Me.lkpCaja.ValueMember = "Caja"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(328, 40)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "02/06/2008"
        Me.dteFecha.Location = New System.Drawing.Point(376, 39)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 9
        Me.dteFecha.Tag = "fecha"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(23, 112)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(75, 16)
        Me.lblImporte.TabIndex = 6
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Bonificaci�n:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcBonificacion
        '
        Me.clcBonificacion.EditValue = "0"
        Me.clcBonificacion.Location = New System.Drawing.Point(104, 108)
        Me.clcBonificacion.MaxValue = 0
        Me.clcBonificacion.MinValue = 0
        Me.clcBonificacion.Name = "clcBonificacion"
        '
        'clcBonificacion.Properties
        '
        Me.clcBonificacion.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcBonificacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBonificacion.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcBonificacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcBonificacion.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcBonificacion.Properties.Precision = 2
        Me.clcBonificacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcBonificacion.Size = New System.Drawing.Size(96, 19)
        Me.clcBonificacion.TabIndex = 7
        Me.clcBonificacion.Tag = "importe"
        '
        'chkUtilizada
        '
        Me.chkUtilizada.EditValue = "False"
        Me.chkUtilizada.Location = New System.Drawing.Point(400, 112)
        Me.chkUtilizada.Name = "chkUtilizada"
        '
        'chkUtilizada.Properties
        '
        Me.chkUtilizada.Properties.Caption = "Utilizada"
        Me.chkUtilizada.Properties.Enabled = False
        Me.chkUtilizada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkUtilizada.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.Highlight)
        Me.chkUtilizada.Size = New System.Drawing.Size(72, 19)
        Me.chkUtilizada.TabIndex = 10
        Me.chkUtilizada.Tag = "utilizada"
        '
        'grMovimientos
        '
        Me.grMovimientos.Anchor = System.Windows.Forms.AnchorStyles.None
        '
        'grMovimientos.EmbeddedNavigator
        '
        Me.grMovimientos.EmbeddedNavigator.Name = ""
        Me.grMovimientos.Location = New System.Drawing.Point(9, 132)
        Me.grMovimientos.MainView = Me.grvMovimientos
        Me.grMovimientos.Name = "grMovimientos"
        Me.grMovimientos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkChecar})
        Me.grMovimientos.Size = New System.Drawing.Size(752, 163)
        Me.grMovimientos.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grMovimientos.TabIndex = 11
        Me.grMovimientos.TabStop = False
        Me.grMovimientos.Text = "GridControl1"
        '
        'grvMovimientos
        '
        Me.grvMovimientos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcChecar, Me.grcFecha, Me.grcDocumento, Me.grcSaldoDocumento, Me.grcSaldoVenta, Me.grcSucursal, Me.grcConcepto, Me.grcSerie, Me.grcFolio, Me.grcCliente, Me.grcInteres, Me.grcDocumentos, Me.grcenajenacion, Me.grcfactor_enajenacion, Me.grcNombreSucursal, Me.grcUltimoDocumento, Me.grcNotaCargo, Me.grcPermitePagarUltimoDocumento})
        Me.grvMovimientos.GridControl = Me.grMovimientos
        Me.grvMovimientos.GroupPanelText = "Precios"
        Me.grvMovimientos.Name = "grvMovimientos"
        Me.grvMovimientos.OptionsView.ShowGroupPanel = False
        Me.grvMovimientos.OptionsView.ShowIndicator = False
        '
        'grcChecar
        '
        Me.grcChecar.Caption = "Incluir"
        Me.grcChecar.ColumnEdit = Me.chkChecar
        Me.grcChecar.FieldName = "checar"
        Me.grcChecar.Name = "grcChecar"
        Me.grcChecar.Options = CType((DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcChecar.VisibleIndex = 0
        Me.grcChecar.Width = 76
        '
        'chkChecar
        '
        Me.chkChecar.AutoHeight = False
        Me.chkChecar.Name = "chkChecar"
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 4
        Me.grcFecha.Width = 82
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Documento"
        Me.grcDocumento.DisplayFormat.FormatString = "n"
        Me.grcDocumento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDocumento.FieldName = "documento"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.Width = 74
        '
        'grcSaldoDocumento
        '
        Me.grcSaldoDocumento.Caption = "Saldo Documento"
        Me.grcSaldoDocumento.DisplayFormat.FormatString = "c2"
        Me.grcSaldoDocumento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoDocumento.FieldName = "saldo_documento"
        Me.grcSaldoDocumento.Name = "grcSaldoDocumento"
        Me.grcSaldoDocumento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoDocumento.VisibleIndex = 8
        Me.grcSaldoDocumento.Width = 104
        '
        'grcSaldoVenta
        '
        Me.grcSaldoVenta.Caption = "Saldo Venta"
        Me.grcSaldoVenta.DisplayFormat.FormatString = "c2"
        Me.grcSaldoVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcSaldoVenta.FieldName = "saldo_venta"
        Me.grcSaldoVenta.Name = "grcSaldoVenta"
        Me.grcSaldoVenta.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSaldoVenta.VisibleIndex = 6
        Me.grcSaldoVenta.Width = 78
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto Factura"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.Width = 107
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie Factura"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.VisibleIndex = 2
        Me.grcSerie.Width = 83
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio Factura"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 3
        Me.grcFolio.Width = 81
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcInteres
        '
        Me.grcInteres.Caption = "Interes"
        Me.grcInteres.DisplayFormat.FormatString = "c2"
        Me.grcInteres.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcInteres.FieldName = "interes"
        Me.grcInteres.Name = "grcInteres"
        Me.grcInteres.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcInteres.VisibleIndex = 7
        Me.grcInteres.Width = 65
        '
        'grcDocumentos
        '
        Me.grcDocumentos.Caption = "Documento"
        Me.grcDocumentos.FieldName = "documentos"
        Me.grcDocumentos.Name = "grcDocumentos"
        Me.grcDocumentos.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumentos.VisibleIndex = 5
        '
        'grcenajenacion
        '
        Me.grcenajenacion.Caption = "enajenacion"
        Me.grcenajenacion.FieldName = "enajenacion"
        Me.grcenajenacion.Name = "grcenajenacion"
        Me.grcenajenacion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcfactor_enajenacion
        '
        Me.grcfactor_enajenacion.Caption = "factor_enajenacion"
        Me.grcfactor_enajenacion.FieldName = "factor_enajenacion"
        Me.grcfactor_enajenacion.Name = "grcfactor_enajenacion"
        Me.grcfactor_enajenacion.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "nombre_sucursal"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursal.VisibleIndex = 1
        '
        'grcUltimoDocumento
        '
        Me.grcUltimoDocumento.Caption = "Ultimo Documento"
        Me.grcUltimoDocumento.ColumnEdit = Me.chkChecar
        Me.grcUltimoDocumento.FieldName = "ultima_letra"
        Me.grcUltimoDocumento.Name = "grcUltimoDocumento"
        Me.grcUltimoDocumento.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcNotaCargo
        '
        Me.grcNotaCargo.Caption = "Nota Cargo"
        Me.grcNotaCargo.FieldName = "nota_cargo_descuento_anticipado"
        Me.grcNotaCargo.Name = "grcNotaCargo"
        Me.grcNotaCargo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcPermitePagarUltimoDocumento
        '
        Me.grcPermitePagarUltimoDocumento.Caption = "Permite Pagar Ultimo Documento"
        Me.grcPermitePagarUltimoDocumento.ColumnEdit = Me.chkChecar
        Me.grcPermitePagarUltimoDocumento.FieldName = "permite_pagar_ultimo_documento"
        Me.grcPermitePagarUltimoDocumento.Name = "grcPermitePagarUltimoDocumento"
        Me.grcPermitePagarUltimoDocumento.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(536, 328)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(115, 16)
        Me.Label10.TabIndex = 14
        Me.Label10.Tag = ""
        Me.Label10.Text = "&Monto Documentos:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporteDocumentos
        '
        Me.clcImporteDocumentos.EditValue = "0"
        Me.clcImporteDocumentos.Location = New System.Drawing.Point(656, 328)
        Me.clcImporteDocumentos.MaxValue = 0
        Me.clcImporteDocumentos.MinValue = 0
        Me.clcImporteDocumentos.Name = "clcImporteDocumentos"
        '
        'clcImporteDocumentos.Properties
        '
        Me.clcImporteDocumentos.Properties.DisplayFormat.FormatString = "C2"
        Me.clcImporteDocumentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteDocumentos.Properties.EditFormat.FormatString = "C2"
        Me.clcImporteDocumentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporteDocumentos.Properties.Enabled = False
        Me.clcImporteDocumentos.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporteDocumentos.Properties.Precision = 2
        Me.clcImporteDocumentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporteDocumentos.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcImporteDocumentos.Size = New System.Drawing.Size(104, 19)
        Me.clcImporteDocumentos.TabIndex = 15
        Me.clcImporteDocumentos.Tag = ""
        '
        'lblIntereses
        '
        Me.lblIntereses.AutoSize = True
        Me.lblIntereses.Location = New System.Drawing.Point(592, 352)
        Me.lblIntereses.Name = "lblIntereses"
        Me.lblIntereses.Size = New System.Drawing.Size(61, 16)
        Me.lblIntereses.TabIndex = 16
        Me.lblIntereses.Tag = ""
        Me.lblIntereses.Text = "In&tereses:"
        Me.lblIntereses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcIntereses
        '
        Me.clcIntereses.EditValue = "0"
        Me.clcIntereses.Location = New System.Drawing.Point(656, 352)
        Me.clcIntereses.MaxValue = 0
        Me.clcIntereses.MinValue = 0
        Me.clcIntereses.Name = "clcIntereses"
        '
        'clcIntereses.Properties
        '
        Me.clcIntereses.Properties.DisplayFormat.FormatString = "C2"
        Me.clcIntereses.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.EditFormat.FormatString = "C2"
        Me.clcIntereses.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.Enabled = False
        Me.clcIntereses.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcIntereses.Properties.Precision = 2
        Me.clcIntereses.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIntereses.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcIntereses.Size = New System.Drawing.Size(104, 19)
        Me.clcIntereses.TabIndex = 17
        Me.clcIntereses.Tag = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(600, 304)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Importe:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(656, 304)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "C2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "C2"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ActiveCaption)
        Me.clcImporte.Size = New System.Drawing.Size(104, 19)
        Me.clcImporte.TabIndex = 13
        Me.clcImporte.Tag = ""
        '
        'frmAutorizacionesBonificacionesInteres
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(770, 376)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.clcImporteDocumentos)
        Me.Controls.Add(Me.lblIntereses)
        Me.Controls.Add(Me.clcIntereses)
        Me.Controls.Add(Me.grMovimientos)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblCaja)
        Me.Controls.Add(Me.lkpCaja)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcBonificacion)
        Me.Controls.Add(Me.chkUtilizada)
        Me.Name = "frmAutorizacionesBonificacionesInteres"
        Me.Controls.SetChildIndex(Me.chkUtilizada, 0)
        Me.Controls.SetChildIndex(Me.clcBonificacion, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpCaja, 0)
        Me.Controls.SetChildIndex(Me.lblCaja, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.grMovimientos, 0)
        Me.Controls.SetChildIndex(Me.clcIntereses, 0)
        Me.Controls.SetChildIndex(Me.lblIntereses, 0)
        Me.Controls.SetChildIndex(Me.clcImporteDocumentos, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcBonificacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUtilizada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkChecar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporteDocumentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oAutorizacionesBonificacionesInteres As VillarrealBusiness.clsAutorizacionesBonificacionesInteres
    Private oClientes As VillarrealBusiness.clsClientes
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oCajas As VillarrealBusiness.clsCajas


    Private oTabla As DataTable

    Private ReadOnly Property cliente() As Long
        Get
            Return PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property caja() As Long
        Get
            Return PreparaValorLookup(Me.lkpCaja)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmAutorizacionesBonificacionesInteres_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oAutorizacionesBonificacionesInteres.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oAutorizacionesBonificacionesInteres.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oAutorizacionesBonificacionesInteres.Eliminar(clcFolio.value)

        End Select
    End Sub

    Private Sub frmAutorizacionesBonificacionesInteres_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oAutorizacionesBonificacionesInteres.DespliegaDatos(OwnerForm.Value("folio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If


        Me.lkpCliente.Enabled = Not Me.chkUtilizada.Checked
        Me.lkpCaja.Enabled = Not Me.chkUtilizada.Checked
        Me.grMovimientos.Enabled = Not Me.chkUtilizada.Checked
        Me.clcBonificacion.Enabled = Not Me.chkUtilizada.Checked
        Me.tbrTools.Buttons(0).Enabled = Not Me.chkUtilizada.Checked


    End Sub

    Private Sub frmAutorizacionesBonificacionesInteres_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oAutorizacionesBonificacionesInteres = New VillarrealBusiness.clsAutorizacionesBonificacionesInteres
        oClientes = New VillarrealBusiness.clsClientes
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oCajas = New VillarrealBusiness.clsCajas

        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmAutorizacionesBonificacionesInteres_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oAutorizacionesBonificacionesInteres.Validacion(Action, cliente, Caja, Me.clcIntereses.Value, Me.clcBonificacion.Value, Me.dteFecha.Text)
    End Sub

    Private Sub frmAutorizacionesBonificacionesInteres_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        Llenargrid()
    End Sub

    Private Sub lkpCaja_Format() Handles lkpCaja.Format
        Comunes.clsFormato.for_cajas_grl(Me.lkpCaja)
    End Sub
    Private Sub lkpCaja_LoadData(ByVal Initialize As Boolean) Handles lkpCaja.LoadData
        Dim Response As New Events
        Response = oCajas.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCaja.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

    Private Sub grvMovimientos_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvMovimientos.CellValueChanging
        'Me.clcMonto.EditValue = 0
        'Me.clcMonto.Enabled = True
        Dim i As Integer = 0
        Dim j As Integer = Me.grvMovimientos.FocusedRowHandle
        'Dim k As Integer = 0
        'Dim fila_predeterminada As Long = 0
        Dim contador As Long = 0

        Me.grvMovimientos.UpdateCurrentRow()
        'fila_predeterminada = Me.FilaPredeterminadaFormaPago()


        'If Me.cboTipoCobro.EditValue <> "A" Then

        '    For i = 0 To Me.grvMovimientos.RowCount - 1
        '        If i <> j Then
        '            Me.grvMovimientos.SetRowCellValue(i, Me.grcChecar, False)
        '        Else
        '            Me.grvMovimientos.SetRowCellValue(i, Me.grcChecar, True)
        '        End If
        '    Next

        '    Me.clcMonto.EditValue = Me.grvMovimientos.GetRowCellValue(j, Me.grcSaldoDocumento)
        '    Me.clcMonto.Enabled = False
        '    grvMovimientos.UpdateCurrentRow()


        '    AsignarMonto_FormaPago(fila_predeterminada, Me.clcMonto.EditValue)

        'Else
        If e.Column Is Me.grcChecar Then

            'Validacion para que no se puedan seleccionar documentos de diferentes facturas
            If Me.grvMovimientos.GetRowCellValue(j, Me.grcChecar) = False Then
                If ValidaAbonoDiferentesFacturas(j) = True Then
                    ShowMessage(MessageType.MsgInformation, "Debe seleccionar documentos de una misma factura")
                    Me.grvMovimientos.SetRowCellValue(j, Me.grcChecar, False)
                End If
            End If


            '' Validacion para Checar si ya se genero la Nota de Cargo
            'If Me.grvMovimientos.GetRowCellValue(e.RowHandle, Me.grcUltimoDocumento) = True And Me.grvMovimientos.GetRowCellValue(e.RowHandle, Me.grcPermitePagarUltimoDocumento) = False Then
            '    ShowMessage(MessageType.MsgInformation, "No se ha generado la Nota de Cargo para este Documento")
            '    Me.grvMovimientos.SetRowCellValue(j, Me.grcChecar, False)
            '    Exit Sub

            'End If

            Me.clcImporteDocumentos.EditValue = 0
            Me.clcIntereses.Value = 0


            For i = 0 To Me.grvMovimientos.RowCount - 1
                If e.RowHandle = i And e.Value = True Then
                    Me.clcImporteDocumentos.EditValue = Me.clcImporteDocumentos.EditValue + Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                    'DAM 25/Oct/2007 ahora los intereses se obtienes dependiendo la documento seleccionado
                    Me.clcIntereses.Value = Me.clcIntereses.Value + Me.grvMovimientos.GetRowCellValue(i, Me.grcInteres)
                    'Me.clcNotaCargo.Value = Me.clcNotaCargo.Value + Me.grvMovimientos.GetRowCellValue(i, Me.grcNotaCargo)
                    contador += 1
                Else
                    If e.RowHandle <> i And grvMovimientos.GetRowCellValue(i, Me.grcChecar) = True Then
                        Me.clcImporteDocumentos.EditValue = Me.clcImporteDocumentos.EditValue + Me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
                        Me.clcIntereses.Value = Me.clcIntereses.Value + Me.grvMovimientos.GetRowCellValue(i, Me.grcInteres)
                        'Me.clcNotaCargo.Value = Me.clcNotaCargo.Value + Me.grvMovimientos.GetRowCellValue(i, Me.grcNotaCargo)
                        contador += 1
                    End If
                End If
            Next


            'AsignarMonto_FormaPago(fila_predeterminada, Me.clcImporteDocumentos.EditValue + Me.clcIntereses.Value - Me.clcBonificar.Value)


            'CalculaMonto()
        End If
        'End If

        'Modificado_monto = False


    End Sub
    Private Sub grvMovimientos_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvMovimientos.CellValueChanged
        Me.grvMovimientos.CloseEditor()
    End Sub
    Private Sub grvMovimientos_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvMovimientos.FocusedRowChanged
        If e.FocusedRowHandle < 0 Then Exit Sub
        'RevisaFactura(e.FocusedRowHandle)
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub Llenargrid()
        Dim response As New Events
        Dim odataset As New DataSet

        response = Me.oMovimientosCobrar.DespliegaDatosVentas(Comunes.Common.Sucursal_Actual, cliente, "A", Me.dteFecha.EditValue, False)
        If Not response.ErrorFound Then
            odataset = response.Value
            oTabla = odataset.Tables(0)
            Me.grMovimientos.DataSource = oTabla

            'Me.clcMonto.EditValue = 0
            'Me.clcMonto.Enabled = True
            Me.clcBonificacion.Value = 0
            Me.clcIntereses.Value = 0
            Me.clcImporteDocumentos.Value = 0
            Me.clcImporte.Value = 0

            'If Me.cboTipoCobro.EditValue <> "A" And odataset.Tables(0).Rows.Count > 0 Then

            '    Me.clcMonto.EditValue = oTabla.Rows(0).Item("saldo_documento")
            '    Me.clcMonto.Enabled = False
            '    If Me.cboTipoCobro.EditValue = "P" Then
            '        Me.txtconcepto.Enabled = True
            '        Me.txtconcepto.Text = "Descuento por Pronto Pago"
            '    End If

            '    If Me.cboTipoCobro.EditValue = "N" Then
            '        Me.txtconcepto.Enabled = True
            '        Me.txtconcepto.Text = "NOTA DE CARGO"
            '    End If

            'End If
        End If
        'response = oFormasPagos.MovimientosCaja
        'If Not response.ErrorFound Then
        '    Me.tmaFormaspago.DataSource = response.Value
        '    AgregarTipoCambio()
        'End If

        CalcularTotalDocumentos()
        Me.grvMovimientos.FocusedRowHandle = -1
        odataset = Nothing

    End Sub
    Public Sub CalcularTotalDocumentos()
        Dim i As Integer = 0
        Dim intereses As Double = 0
        Dim importe As Double = 0
        If Me.oTabla Is Nothing Or IsNumeric(Me.oTabla.Rows.Count) = False Or IsDBNull(Me.oTabla.Rows.Count) Then
            MsgBox("valor nothing  en el contador de filas de la tabla")
            Exit Sub
        End If
        For i = 0 To Me.oTabla.Rows.Count - 1
            'intereses = System.Math.Round(intereses, 2) + System.Math.Round(oTabla.Rows(i).Item("interes"), 2)  ' Me.grvMovimientos.GetRowCellValue(i, Me.grcInteres)
            importe = importe + oTabla.Rows(i).Item("saldo_documento") 'me.grvMovimientos.GetRowCellValue(i, Me.grcSaldoDocumento)
        Next
        'Me.clcIntereses.EditValue = intereses
        Me.clcImporte.EditValue = importe
        Me.clcBonificacion.EditValue = 0
        'Me.clcMonto.Value = intereses

    End Sub
    Private Function ValidaAbonoDiferentesFacturas(ByVal fila_actual As Integer) As Boolean
        Dim i As Long = 0
        Dim bExisteOtraFacturaSeleccionada As Boolean = False

        For i = 0 To Me.grvMovimientos.RowCount - 1
            If Me.grvMovimientos.GetRowCellValue(i, Me.grcChecar) Then
                ' Revisa si el renglon actual es de la misma factura de otro rengl�n seleccionado
                If (Me.grvMovimientos.GetRowCellValue(i, Me.grcSucursal) = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcSucursal) And Me.grvMovimientos.GetRowCellValue(i, Me.grcSerie) = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcSerie) And Me.grvMovimientos.GetRowCellValue(i, Me.grcFolio) = Me.grvMovimientos.GetRowCellValue(fila_actual, Me.grcFolio)) Then
                    'si son de la misma factura 
                    bExisteOtraFacturaSeleccionada = False
                Else
                    'si no son de la misma factura 
                    bExisteOtraFacturaSeleccionada = True
                    ValidaAbonoDiferentesFacturas = True
                End If
            End If
        Next

        ValidaAbonoDiferentesFacturas = bExisteOtraFacturaSeleccionada

    End Function
#End Region


   
End Class
