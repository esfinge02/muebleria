Imports Dipros.Windows.Forms

Module ModFormatos

#Region "Cajas"

    Public Sub for_cajas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Caja", "caja", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripción", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 250)
    End Sub

#End Region


#Region "Tipos Depositos"

    Public Sub for_tipos_depositos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Tipo Deposito", "Tipo_deposito", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripción", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Tipo", "tipo", 2, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

#Region "Cajeros"

    Public Sub for_cajeros_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Cajero", "cajero", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 250)

    End Sub

#End Region



#Region "CambioCheques"
    Public Sub for_cambio_cheques_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Partida", "partida", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 1, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy")
        AddColumns(oGrid, "Banco", "banco", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Numero_Cheque", "numero_cheque", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Monto", "monto", 4, DevExpress.Utils.FormatType.Numeric, "$###,###,##0.00")
        AddColumns(oGrid, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Caja", "caja", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Quien", "QUIEN", -1, DevExpress.Utils.FormatType.None, "")

    End Sub
#End Region





End Module
