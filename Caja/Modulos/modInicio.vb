Imports System.Globalization
Imports System.Threading

Module modInicio
    Public TINApp As New Dipros.Windows.Application

    'DESCRIPCION: Funci�n de inicio del sistema
    'DESARROLLO: DIPROS SYSTEMS
    'FECHA: 18/02/2006 00:00:00
    Public Sub Main()
        TINApp.Connection = New Dipros.Data.Data
        TINApp.Application = "Caja"
        TINApp.Prefix = "CAJ"

        If Not TINApp.Connection.Trusted Then End
        If Not TINApp.Login() Then End

        'Inicializa la capa de negocios
        VillarrealBusiness.BusinessEnvironment.Connection = TINApp.Connection

        'Paso de conexion y frmMain a Comunes
        Comunes.Common.Aplicacion = TINApp

        ''----------------------------------------------------------------------------------
        'Selecciono por default la cultura de M�xico como configuracion del programa
        Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")

        Dim oMicultura As New CultureInfo("es-MX")
        Dim instance As New DateTimeFormatInfo
        Dim value As String

        value = "hh:mm:ss:tt"
        instance.LongTimePattern = value
        oMicultura.DateTimeFormat.LongTimePattern = instance.LongTimePattern

        Thread.CurrentThread.CurrentCulture = oMicultura

        ''----------------------------------------------------------------------------------

        Dim oMain As New frmMain
        Dim blAvanza As Boolean = False

        'PEDIR LA SUCURSAL ACTUAL
        If Comunes.clsUtilerias.AbrirArchivo(Comunes.Common.Sucursal_Actual) = False Then
            If TINApp.Connection.User.ToUpper = "SUPER" Then
                Dim oForm As New Comunes.frmSucursalActual
                'oMain.Enabled = False
                Application.Run(oForm)

                If oForm.EntrarMain = True Then
                    'Application.Run(oMain)
                    blAvanza = True
                Else
                    If MsgBox("No hay una Sucursal Definida. �Desea continuar?", MsgBoxStyle.YesNo, "Mensaje del Sistema") = MsgBoxResult.Yes Then
                        blAvanza = True
                    Else
                        blAvanza = False
                        Exit Sub
                    End If
                End If
            Else
                MsgBox("No hay una Sucursal Definida y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
                blAvanza = False
                Exit Sub
            End If
        Else

            'Application.Run(oMain)
            blAvanza = True
        End If

        'PEDIR LA CAJA ACTUAL
        If blAvanza = True Then
            If Comunes.clsUtilerias.AbrirArchivoCaja(Comunes.Common.Caja_Actual) = False Then
                If TINApp.Connection.User = "SUPER" Then
                    Dim oForm As New Comunes.frmCajaActual
                    'oMain.Enabled = False
                    Application.Run(oForm)

                    If oForm.EntrarMain = True Then

                        'Application.Run(oMain)
                        blAvanza = True
                    Else
                        If MsgBox("No hay una Caja Definida. �Desea continuar?", MsgBoxStyle.YesNo, "Mensaje del Sistema") = MsgBoxResult.Yes Then
                            blAvanza = True
                        Else
                            blAvanza = False
                            Exit Sub
                        End If
                    End If
                Else
                    MsgBox("No hay una Caja Definida y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
                    blAvanza = False
                    Exit Sub
                End If
            Else
                'Application.Run(oMain)
                blAvanza = True
            End If
        End If


        'CHECAR SI EL USUARIO TIENE UN CAJERO ASIGNADO, SOLO EL USUARIO SUPER PUEDE ENTRAR SIN CAJERO ASIGNADO
        If blAvanza = True Then
            If Comunes.clsUtilerias.uti_Usuariocajero(TINApp.Connection.User, Comunes.Common.Cajero) = False Then
                If TINApp.Connection.User = "SUPER" Then
                    If MsgBox("El Usuario No Tiene un Cajero Definido. �Desea continuar?", MsgBoxStyle.YesNo, "Mensaje del Sistema") = MsgBoxResult.Yes Then
                        'blAvanza = True
                        Application.Run(oMain)
                    Else
                        blAvanza = False
                        Exit Sub
                    End If
                Else
                    MsgBox("El Usuario No Tiene un Cajero Definido", MsgBoxStyle.Information, "Mensaje del Sistema")
                    Exit Sub
                End If
            Else

                'PEDIR EL FONDO Y EL TIPO DE CAMBIO DE HOY, PARA LOS USUARIOS QUE TIENEN UN CAJERO ASIGNADO
                If Comunes.clsUtilerias.uti_ExisteCajerosFondos(TINApp.Connection.User, CDate(TINApp.FechaServidor).Day.ToString + "/" + CDate(TINApp.FechaServidor).Month.ToString + "/" + CDate(TINApp.FechaServidor).Year.ToString) = True And blAvanza = True Then
                    Try
                        Application.Run(oMain)
                    Catch ex As Exception

                    End Try

                Else
                    Dim oformCajeroFondo As New Comunes.frmCajerosFondos
                    Application.Run(oformCajeroFondo)
                    If oformCajeroFondo.EntrarMain = True Then
                        Application.Run(oMain)
                    Else
                        MsgBox("No hay un Fondo y un Tipo de Cambio Definido", MsgBoxStyle.Information, "Mensaje del Sistema")
                    End If
                End If
            End If
        End If


    End Sub
End Module
