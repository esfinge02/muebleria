'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsReportes
'DATE:		09/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Reportes"
Public Class clsReportes

    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	re_ordenes_compra
    'DATE:		09/03/2006 00:00:00
    '----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, clsDataEnvironment - re_ordenes_compra"
    Public Function re_ordenes_compra(ByVal orden As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ordenes_compra]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = orden

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_pendientes_arribar"
    Public Function re_pendientes_arribar(ByVal PROVEEDOR As Long, ByVal SUCURSAL As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_pendientes_arribar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Int, 4, "PROVEEDOR"))
                .Item("@PROVEEDOR").Value = PROVEEDOR

                .Add(New System.Data.SqlClient.SqlParameter("@SUCURSAL", System.Data.SqlDbType.Int, 4, "SUCURSAL"))
                .Item("@SUCURSAL").Value = SUCURSAL

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_entradas_almacen"
    Public Function re_entradas_almacen(ByVal BODEGA As String, ByVal PROVEEDOR As Long, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal mercancia As Integer, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_entradas_almacen]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.VarChar, 8, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Int, 4, "PROVEEDOR"))
                .Item("@PROVEEDOR").Value = PROVEEDOR
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@mercancia", System.Data.SqlDbType.Int, 4))
                .Item("@mercancia").Value = mercancia
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_existencias_nuevo"
    Public Function re_existencias_nuevo(ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal Todo As Boolean, ByVal Proveedor As Long, ByVal Tipo As Int16, ByVal CorteProveedor As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_existencias_nuevo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@Todo", System.Data.SqlDbType.Bit, 1, "Todo"))
                .Item("@Todo").Value = Todo
                .Add(New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 4, "Proveedor"))
                .Item("@Proveedor").Value = Proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.Int, 4, "Tipo"))
                .Item("@Tipo").Value = Tipo
                .Add(New System.Data.SqlClient.SqlParameter("@CorteProveedor", System.Data.SqlDbType.Bit, 1, "CorteProveedor"))
                .Item("@CorteProveedor").Value = CorteProveedor
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

    '#Region "DIPROS Systems, clsDataEnvironment - re_existencias_multiple"
    '    Public Function re_existencias_multiple(ByVal BODEGA As String, ByVal DEPARTAMENTO As String, ByVal GRUPO As String, _
    '    ByVal PROVEEDOR As String, ByVal Todo As Long, ByVal incluir_precios As Boolean, ByVal incluir_costos As Boolean, _
    '    ByVal incluir_existencias As Boolean, ByVal incluir_series As Boolean) As Events

    '        Dim oCommand As New System.Data.SqlClient.SqlCommand
    '        Dim oEvent As New Events

    '        Try
    '            oCommand.CommandText = "[re_existencias_multiple]"
    '            With oCommand.Parameters
    '                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.Text, 0, "BODEGA"))
    '                .Item("@BODEGA").Value = BODEGA
    '                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Text, 0, "DEPARTAMENTO"))
    '                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
    '                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Text, 0, "GRUPO"))
    '                .Item("@GRUPO").Value = GRUPO
    '                .Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Text, 0, "PROVEEDOR"))
    '                .Item("@PROVEEDOR").Value = PROVEEDOR
    '                .Add(New System.Data.SqlClient.SqlParameter("@Todo", System.Data.SqlDbType.Bit, 4, "Todo"))
    '                .Item("@Todo").Value = Todo
    '                .Add(New System.Data.SqlClient.SqlParameter("@incluir_precios", System.Data.SqlDbType.Bit, 1, "incluir_precios"))
    '                .Item("@incluir_precios").Value = incluir_precios
    '                .Add(New System.Data.SqlClient.SqlParameter("@incluir_costos", System.Data.SqlDbType.Bit, 1, "incluir_costos"))
    '                .Item("@incluir_costos").Value = incluir_costos
    '                .Add(New System.Data.SqlClient.SqlParameter("@incluir_existencias", System.Data.SqlDbType.Bit, 1, "incluir_existencias"))
    '                .Item("@incluir_existencias").Value = incluir_existencias
    '                .Add(New System.Data.SqlClient.SqlParameter("@incluir_series", System.Data.SqlDbType.Bit, 1, "@incluir_series"))
    '                .Item("@incluir_series").Value = incluir_series

    '            End With
    '            oEvent.Value = Connection.GetDataSet(oCommand)
    '        Catch ex As Exception
    '            oEvent.Ex = ex
    '            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '            oEvent.Layer = Events.ErrorLayer.DataLayer
    '        Finally
    '            oCommand = Nothing
    '        End Try

    '        Return oEvent
    '    End Function
    '#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_existencias_multiplenuevo"
    Public Function re_existencias_multiplenuevo(ByVal sucursal As Long, ByVal BODEGA As String, ByVal DEPARTAMENTO As Long, _
    ByVal GRUPO As Long, ByVal PROVEEDOR As Long, ByVal Todo As Boolean, ByVal incluir_precios As Boolean, ByVal incluir_costos As Boolean, _
    ByVal incluir_existencias As Boolean, ByVal incluir_series As Boolean, ByVal Vendedor As Long) As Events

        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_existencias_multiplenuevo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.VarChar, 5, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Int, 4, "PROVEEDOR"))
                .Item("@PROVEEDOR").Value = PROVEEDOR

                .Add(New System.Data.SqlClient.SqlParameter("@Vendedor", System.Data.SqlDbType.Int, 4, "Vendedor"))
                .Item("@Vendedor").Value = Vendedor


                .Add(New System.Data.SqlClient.SqlParameter("@Todo", System.Data.SqlDbType.Bit, 1, "Todo"))
                .Item("@Todo").Value = Todo
                .Add(New System.Data.SqlClient.SqlParameter("@incluir_precios", System.Data.SqlDbType.Bit, 1, "incluir_precios"))
                .Item("@incluir_precios").Value = incluir_precios
                .Add(New System.Data.SqlClient.SqlParameter("@incluir_costos", System.Data.SqlDbType.Bit, 1, "incluir_costos"))
                .Item("@incluir_costos").Value = incluir_costos
                .Add(New System.Data.SqlClient.SqlParameter("@incluir_existencias", System.Data.SqlDbType.Bit, 1, "incluir_existencias"))
                .Item("@incluir_existencias").Value = incluir_existencias
                .Add(New System.Data.SqlClient.SqlParameter("@incluir_series", System.Data.SqlDbType.Bit, 1, "@incluir_series"))
                .Item("@incluir_series").Value = incluir_series

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - sp_datos_empresa_sel"


    Public Function sp_datos_empresa_sel() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_datos_empresa_sel]"

            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_movimientos_referencia"
    Public Function re_movimientos_referencia(ByVal CONCEPTO As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal desglosado As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_movimientos_referencia]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.Text, 0, "BODEGA"))
                '.Item("@BODEGA").Value = BODEGA
                '.Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Text, 0, "DEPARTAMENTO"))
                '.Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                '.Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Text, 0, "GRUPO"))
                '.Item("@GRUPO").Value = GRUPO
                '.Add(New System.Data.SqlClient.SqlParameter("@ARTICULO", System.Data.SqlDbType.Text, 0, "ARTICULO"))
                '.Item("@ARTICULO").Value = ARTICULO
                .Add(New System.Data.SqlClient.SqlParameter("@CONCEPTO", System.Data.SqlDbType.Text, 0, "CONCEPTO"))
                .Item("@CONCEPTO").Value = CONCEPTO
                .Add(New System.Data.SqlClient.SqlParameter("@FECHAI", System.Data.SqlDbType.DateTime, 8, "FECHAI"))
                .Item("@FECHAI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHAF", System.Data.SqlDbType.DateTime, 8, "FECHAF"))
                .Item("@FECHAF").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@DESGLOSADO", System.Data.SqlDbType.Bit, 4, "DESGLOSADO"))
                .Item("@DESGLOSADO").Value = desglosado
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_articulosquedados"
    Public Function re_articulos_quedados(ByVal bodega As String, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal fecha_ini As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_articulos_quedados]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@FECHAI", System.Data.SqlDbType.DateTime, 8, "FECHAI"))
                .Item("@FECHAI").Value = fecha_ini
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_articulos_pedidos_fabrica"
    'JMARTINEZ 28Oct2007: Agregar filtro de incluidos en orden de compra..
    Public Function re_articulos_pedidos_fabrica(ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal PROVEEDOR As Long, ByVal incluido As Integer, ByVal fecha_ini As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_articulos_pedidos_fabrica]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                '.Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Int, 4, "PROVEEDOR"))
                .Item("@PROVEEDOR").Value = PROVEEDOR
                .Add(New System.Data.SqlClient.SqlParameter("@INCLUIDO", System.Data.SqlDbType.Int, 4, "INCLUIDO"))
                .Item("@INCLUIDO").Value = incluido
                .Add(New System.Data.SqlClient.SqlParameter("@FECHAI", System.Data.SqlDbType.DateTime, 8, "FECHAI"))
                .Item("@FECHAI").Value = fecha_ini
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_devolucionmercancias"
    Public Function re_devolucion_mercancia(ByVal bodega As String, ByVal devolucion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_devolucion_mercancia]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_devolucionmercancias"
    Public Function sp_imprimir_etiquetas_devolucion_proveedor(ByVal devolucion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_imprimir_etiquetas_devolucion_proveedor]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_costeoinventario"
    Public Function re_valor_del_inventario(ByVal SUCURSAL As Long, ByVal BODEGA As String, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal FECHA As Date, ByVal DESGLOSADO As Long, ByVal MERCANCIA As Integer, ByVal ORDENAMIENTO As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_valor_del_inventario]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@SUCURSAL", System.Data.SqlDbType.Int, 4, "SUCURSAL"))
                .Item("@SUCURSAL").Value = SUCURSAL
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.Text, 0, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA", System.Data.SqlDbType.DateTime, 8, "FECHA"))
                .Item("@FECHA").Value = FECHA
                .Add(New System.Data.SqlClient.SqlParameter("@DESGLOSADO", System.Data.SqlDbType.Bit, 4, "DESGLOSADO"))
                .Item("@DESGLOSADO").Value = DESGLOSADO
                .Add(New System.Data.SqlClient.SqlParameter("@MERCANCIA", System.Data.SqlDbType.Int, 4, "MERCANCIA"))
                .Item("@mercancia").Value = MERCANCIA
                .Add(New System.Data.SqlClient.SqlParameter("@ordenamiento", System.Data.SqlDbType.Int, 4, "ordenamiento"))
                .Item("@ordenamiento").Value = ORDENAMIENTO
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_salidas_por_tienda"
    Public Function re_salidas_por_tienda(ByVal CONCEPTO_VENTA As String, ByVal BODEGA As String, ByVal DEPARTAMENTO As String, ByVal GRUPO As String, ByVal mercancia As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_salidas_por_tienda]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@CONCEPTO_VENTA", System.Data.SqlDbType.Text, 0, "CONCEPTO_VENTA"))
                .Item("@CONCEPTO_VENTA").Value = CONCEPTO_VENTA
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.Text, 0, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Text, 0, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Text, 0, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@mercancia", System.Data.SqlDbType.Int, 4, "mercancia"))
                .Item("@mercancia").Value = mercancia
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_resumensalidas"
    Public Function re_resumensalidas(ByVal sucursal As Long, ByVal BODEGA As String, _
    ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal CONCEPTO As String, _
    ByVal fechai As Date, ByVal fechaf As Date, ByVal tipo As Long, _
    ByVal proveedor As Long, ByVal titulo As String, ByVal TipoNormales As String, ByVal tipo_mercancia As Int32) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_resumensalidas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.VarChar, 5, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@CONCEPTO", System.Data.SqlDbType.VarChar, 5, "CONCEPTO"))
                .Item("@CONCEPTO").Value = CONCEPTO
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fechai
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fechaf
                .Add(New System.Data.SqlClient.SqlParameter("@TIPO", System.Data.SqlDbType.Int, 4))
                .Item("@TIPO").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Int, 4, "PROVEEDOR"))
                .Item("@PROVEEDOR").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@titulo", System.Data.SqlDbType.VarChar, 200, "titulo"))
                .Item("@titulo").Value = titulo
                .Add(New System.Data.SqlClient.SqlParameter("@TIPO_NORMALES", System.Data.SqlDbType.Char, 1, "tipo_normales"))
                .Item("@tipo_normales").Value = TipoNormales
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_mercancia", System.Data.SqlDbType.Int, 4, "tipo_mercancia"))
                .Item("@tipo_mercancia").Value = tipo_mercancia

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_movimientos_nuevo"
    Public Function re_movimientos_nuevo(ByVal BODEGA As String, ByVal departamento As Long, ByVal grupo As Long, ByVal ARTICULO As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_movimientos_nuevo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.VarChar, 5, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
                .Add(New System.Data.SqlClient.SqlParameter("@ARTICULO", System.Data.SqlDbType.Int, 4, "ARTICULO"))
                .Item("@ARTICULO").Value = ARTICULO
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_fin
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_traer_movimientos_para_envio_dependencia"
    Public Function re_traer_movimientos_para_envio_dependencia(ByVal convenio As Integer, ByVal quincena As Integer, ByVal anio As Integer, ByVal tipo As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_traer_movimientos_para_envio_dependencia]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena", System.Data.SqlDbType.Int, 4, "quincena"))
                .Item("@quincena").Value = quincena
                .Add(New System.Data.SqlClient.SqlParameter("@anio", System.Data.SqlDbType.Int, 4, "anio"))
                .Item("@anio").Value = anio
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = tipo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_resumen_proveedor"
    Public Function re_resumen_proveedor(ByVal BODEGA As String, ByVal PROVEEDOR As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal DESGLOSADO As Boolean, ByVal mercancia As Long, ByVal concepto As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_resumen_proveedor]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.VarChar, 4, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Int, 4, "PROVEEDOR"))
                .Item("@PROVEEDOR").Value = PROVEEDOR
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@DESGLOSADO", System.Data.SqlDbType.Bit, 1, "DESGLOSADO"))
                .Item("@DESGLOSADO").Value = DESGLOSADO
                .Add(New System.Data.SqlClient.SqlParameter("@mercancia", System.Data.SqlDbType.Int, 4, "mercancia"))
                .Item("@mercancia").Value = mercancia
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_traspasos_salidas"
    Public Function re_traspasos_salidas(ByVal traspaso As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_traspasos_salidas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspaso", System.Data.SqlDbType.Int, 4, "traspaso"))
                .Item("@traspaso").Value = traspaso

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_traspasos_vale_entrada"
    Public Function re_traspasos_vale_entrada(ByVal traspasos_imprimir As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_traspasos_vale_entrada]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspasos_imprimir", System.Data.SqlDbType.VarChar, 300, "traspasos_imprimir"))
                .Item("@traspasos_imprimir").Value = traspasos_imprimir

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_traspasos_salidas"
    Public Function re_entradas_almacen_proceso(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_entradas_almacen_proceso]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_repartos"
    Public Function re_repartos(ByVal CHOFER As String, ByVal CLIENTE As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo_reporte As Long, ByVal sucursal As Long, ByVal Repartir As Int16, ByVal SoloChoferesSucursal As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_repartos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@CHOFER", System.Data.SqlDbType.Text, 0, "CHOFER"))
                .Item("@CHOFER").Value = CHOFER
                .Add(New System.Data.SqlClient.SqlParameter("@CLIENTE", System.Data.SqlDbType.Text, 0, "CLIENTE"))
                .Item("@CLIENTE").Value = CLIENTE
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_reporte", System.Data.SqlDbType.Int, 4, "tipo_reporte"))
                .Item("@tipo_reporte").Value = tipo_reporte
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@Repartir", System.Data.SqlDbType.Int, 4, "Repartir"))
                .Item("@Repartir").Value = Repartir
                .Add(New System.Data.SqlClient.SqlParameter("@SoloChoferesSucursal", System.Data.SqlDbType.Bit, 1, "SoloChoferesSucursal"))
                .Item("@SoloChoferesSucursal").Value = SoloChoferesSucursal


            End With

            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_repartos"
    Public Function re_envio_reparto(ByVal fecha As Date, ByVal sucursal As Long, ByVal turno As Int16) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_envio_reparto]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@turno", System.Data.SqlDbType.Int, 4, "turno"))
                .Item("@turno").Value = turno



            End With

            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region
#Region "DIPROS Systems, clsDataEnvironment - re_repartos_vale"
    Public Function re_repartos_vale(ByVal reparto As Long, ByVal chofer As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_repartos_vale]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@chofer", System.Data.SqlDbType.Bit, 1, "chofer"))
                .Item("@chofer").Value = chofer

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ventas_credito_por_fecha"
    Public Function re_ventas_credito_por_fecha(ByVal sucursal As Long, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ventas_credito_por_fecha]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_nota_de_credito"
    Public Function re_nota_de_credito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, Optional ByVal iva_desglosado As Boolean = True) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_nota_de_credito]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@iva_desglosado", System.Data.SqlDbType.Bit, 1, "iva_desglosado"))
                .Item("@iva_desglosado").Value = iva_desglosado


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_nota_de_credito"
    Public Function re_nota_de_credito_multiple(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal cadena_folios As String, ByVal cadena_clientes As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_nota_de_credito_multiple]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@cadena_folios", System.Data.SqlDbType.VarChar, 1000, "cadena_folios"))
                .Item("@cadena_folios").Value = cadena_folios
                .Add(New System.Data.SqlClient.SqlParameter("@cadena_clientes", System.Data.SqlDbType.VarChar, 1000, "cadena_clientes"))
                .Item("@cadena_clientes").Value = cadena_clientes



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_nota_de_cargo"
    Public Function re_nota_de_cargo(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal iva_desglosado As String, ByVal ReimpresionCFS As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_nota_de_cargo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1, "ivadesglosado"))
                .Item("@ivadesglosado").Value = iva_desglosado

                .Add(New System.Data.SqlClient.SqlParameter("@ReimpresionCFS", System.Data.SqlDbType.Bit, 1, "ReimpresionCFS"))
                .Item("@ReimpresionCFS").Value = ReimpresionCFS

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_articulos_mas_vendidos"
    Public Function re_articulos_mas_vendidos(ByVal sucursal As Long, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal cantidad As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal Proveedor As Long, ByVal Orden As Long, ByVal TipoOrden As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_articulos_mas_vendidos]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@CANTIDAD", System.Data.SqlDbType.Int, 4, "CANTIDAD"))
                .Item("@CANTIDAD").Value = cantidad
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 4, "Proveedor"))
                .Item("@Proveedor").Value = Proveedor

                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = Orden

                .Add(New System.Data.SqlClient.SqlParameter("@TipoOrden", System.Data.SqlDbType.Int, 4, "TipoOrden"))
                .Item("@TipoOrden").Value = TipoOrden


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_articulos_mas_vendidos_grupo"
    Public Function re_articulos_mas_vendidos_grupo(ByVal sucursal As Long, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal cantidad As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal Proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_articulos_mas_vendidos_grupo]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@CANTIDAD", System.Data.SqlDbType.Int, 4, "CANTIDAD"))
                .Item("@CANTIDAD").Value = cantidad
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 4, "Proveedor"))
                .Item("@Proveedor").Value = Proveedor


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_notas_canceladas"
    Public Function re_notas_canceladas(ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_notas_canceladas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_diferencia_de_precios"
    Public Function re_diferencia_de_precios(ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_diferencia_de_precios]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ingresosxcobrador"
    Public Function re_ingresosxcobrador(ByVal COBRADOR As Long, ByVal tipo_venta As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal cajero As Long, ByVal sucursal As Long, ByVal concentrado As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ingresosxcobrador]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@COBRADOR", System.Data.SqlDbType.Int, 4, "COBRADOR"))
                .Item("@COBRADOR").Value = COBRADOR
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concentrado", System.Data.SqlDbType.Bit, 4, "concentrado"))
                .Item("@concentrado").Value = concentrado
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ingresos_por_forma_pago"
    Public Function re_ingresos_por_forma_pago(ByVal Sucursal As Long, ByVal CAJA As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ingresos_por_forma_pago]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@CAJA", System.Data.SqlDbType.Int, 4, "CAJA"))
                .Item("@CAJA").Value = CAJA
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_diario_ventas"
    Public Function re_diario_ventas(ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal canceladas As Boolean, ByVal sucursal As Long, Optional ByVal ventas_expo As Boolean = False, Optional ByVal convenio As Long = -1) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_diario_ventas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final
                .Add(New System.Data.SqlClient.SqlParameter("@canceladas", System.Data.SqlDbType.Bit, 1, "canceladas"))
                .Item("@canceladas").Value = canceladas
                .Add(New System.Data.SqlClient.SqlParameter("@ventas_expo", System.Data.SqlDbType.Bit, 1, "ventas_expo"))
                .Item("@ventas_expo").Value = ventas_expo
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_depositos_bancarios"
    Public Function re_depositos_bancarios(ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_depositos_bancarios]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_diario_ventas_detallado"
    Public Function re_diario_ventas_detallado(ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal canceladas As Boolean, ByVal sucursal As Long, Optional ByVal ventas_expo As Boolean = False, Optional ByVal convenio As Long = -1) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_diario_ventas_detallado]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final
                .Add(New System.Data.SqlClient.SqlParameter("@canceladas", System.Data.SqlDbType.Bit, 1, "canceladas"))
                .Item("@canceladas").Value = canceladas
                .Add(New System.Data.SqlClient.SqlParameter("@ventas_expo", System.Data.SqlDbType.Bit, 1, "ventas_expo"))
                .Item("@ventas_expo").Value = ventas_expo
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_resumen_ingresos"
    Public Function re_resumen_ingresos(ByVal CAJERO As Long, ByVal tipo_venta As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal sucursal As Long, ByVal SucursalCobradoEn As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_resumen_ingresos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@CAJERO", System.Data.SqlDbType.Int, 4, "CAJERO"))
                .Item("@CAJERO").Value = CAJERO
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@SUCURSAL", System.Data.SqlDbType.Int, 4, "SUCURSAL"))
                .Item("@SUCURSAL").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@SUCURSAL_COBRA", System.Data.SqlDbType.Int, 4, "SUCURSAL_COBRA"))
                .Item("@SUCURSAL_COBRA").Value = SucursalCobradoEn
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_resumen_contabilidad"
    Public Function re_resumen_contabilidad(ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal sucursal As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_resumen_contabilidad]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@SUCURSAL", System.Data.SqlDbType.Int, 4, "SUCURSAL"))
                .Item("@SUCURSAL").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region



#Region "DIPROS Systems, clsDataEnvironment - re_movimientos_cobrar_detalle_cajas_sel"
    Public Function re_movimientos_cobrar_detalle_cajas_sel(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_movimientos_cobrar_detalle_cajas_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@documento", System.Data.SqlDbType.Int, 4, "documento"))
                .Item("@documento").Value = documento

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region



#Region "DIPROS Systems, clsDataEnvironment - re_recibo_abonos"
    Public Function re_recibo_abonos(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal serie_referencia As String, ByVal folio_referencia As Integer, ByVal documento_referencia As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_recibo_abonos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@serie_referencia", System.Data.SqlDbType.Char, 3, "serie_referencia"))
                .Item("@serie_referencia").Value = serie_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@documento_referencia", System.Data.SqlDbType.Int, 4, "documento_referencia"))
                .Item("@documento_referencia").Value = documento_referencia

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_acumulado_clientes"
    Public Function re_acumulado_clientes(ByVal NumClientes As Long, ByVal Fechai As Date, ByVal Fechaf As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_acumulado_clientes]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@NumClientes", System.Data.SqlDbType.Int, 0, "NumClientes"))
                .Item("@NumClientes").Value = NumClientes
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = Fechai
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = Fechaf
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region



    Public Function re_recordatorio_saldo(ByVal Cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_recordatorio_saldo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Cliente
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



#Region "DIPROS Systems, clsDataEnvironment - Estados de Cuenta"
    'JMARTINEZ 26Oct2006
    'Se agrega el modo cliente
    'Se quita el tipo_venta
    Public Function re_estado_cuenta(ByVal Cliente As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal ampliado As Boolean, ByVal modo_cliente As Boolean, ByVal sucursal As Long, ByVal SaldoActual As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_estado_cuenta]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@ampliado", System.Data.SqlDbType.Bit, 1, "ampliado"))
                .Item("@ampliado").Value = ampliado
                .Add(New System.Data.SqlClient.SqlParameter("@modo_cliente", System.Data.SqlDbType.Bit, 1, "modo_cliente"))
                .Item("@modo_cliente").Value = modo_cliente
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@SaldoActual", System.Data.SqlDbType.Bit, 1, "SaldoActual"))
                .Item("@SaldoActual").Value = SaldoActual
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function re_estado_cuenta_proveedor(ByVal Proveedor As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_estado_cuenta_proveedor]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Proveedor
                '.Add(New System.Data.SqlClient.SqlParameter("@proveedor_fin", System.Data.SqlDbType.Int, 4, "proveedor_fin"))
                '.Item("@proveedor_fin").Value = Proveedor

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_impresion_etiquetas_precios"
    Public Function re_impresion_etiquetas_precios(ByVal DEPARTAMENTO As String, ByVal GRUPO As String, ByVal opcion As Long, ByVal con_existencias As Boolean, ByVal tamanio_etiqueta_precios As String, ByVal BODEGA As String, ByVal entrada As Long, ByVal todos As Boolean, Optional ByVal ARTICULOS As String = "<Raiz />") As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_impresion_etiquetas_precios]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.Text, 0, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Text, 0, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Text, 0, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@opcion", System.Data.SqlDbType.Int, 4, "opcion"))
                .Item("@opcion").Value = opcion
                .Add(New System.Data.SqlClient.SqlParameter("@con_existencias", System.Data.SqlDbType.Bit, 1, "con_existencias"))
                .Item("@con_existencias").Value = con_existencias
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio_etiqueta_precios", System.Data.SqlDbType.VarChar, 30, "tamanio_etiqueta_precios"))
                .Item("@tamanio_etiqueta_precios").Value = tamanio_etiqueta_precios
                .Add(New System.Data.SqlClient.SqlParameter("@ARTICULOS", System.Data.SqlDbType.Text, 0, "ARTICULOS"))
                .Item("@ARTICULOS").Value = ARTICULOS
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@todos", System.Data.SqlDbType.Bit, 1, "todos"))
                .Item("@todos").Value = todos

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function re_impresion_etiquetas_precios2(ByVal Entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_impresion_etiquetas_precios2]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = Entrada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_articulos_kardex"
    Public Function re_articulos_kardex(ByVal bodega As String, ByVal ARTICULO As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_articulos_kardex]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.Text, 0, "BODEGA"))
                .Item("@BODEGA").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@ARTICULO", System.Data.SqlDbType.Int, 4, "ARTICULO"))
                .Item("@ARTICULO").Value = ARTICULO
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_fin
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ordenes_servicio_vale "
    Public Function re_ordenes_servicio_vale(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ordenes_servicio_vale]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ordenes_servicio_etiqueta "
    Public Function re_ordenes_servicio_etiqueta(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ordenes_servicio_etiqueta]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_kardex_megacred "
    Public Function re_kardex_megacred(ByVal codigo_barras As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_kardex_megacred]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@codigo_barras", System.Data.SqlDbType.VarChar, 20, "codigo_barras"))
                .Item("@codigo_barras").Value = codigo_barras

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region



#Region "DIPROS Systems, clsDataEnvironment - re_stocks_de_articulos"
    Public Function re_stocks_de_articulos(ByVal DEPARTAMENTO As Long, ByVal proveedor As Long, ByVal stock_minimo As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_stocks_de_articulos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@stock_minimo", System.Data.SqlDbType.Bit, 1, "stock_minimo"))
                .Item("@stock_minimo").Value = stock_minimo


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_estadisticas_ventas"
    Public Function re_estadisticas_ventas(ByVal sucursal As Long, ByVal bodega As String, ByVal departamento As Long, ByVal grupo As Long, ByVal proveedor As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_estadisticas_ventas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.VarChar, 4, "BODEGA"))
                .Item("@BODEGA").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = grupo

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_inventariofisico_conteo1"
    Public Function re_inventariofisico_conteo1(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_inventariofisico_conteo1]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_inventariofisico_conteo2"
    Public Function re_inventariofisico_conteo2(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_inventariofisico_conteo2]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_inventariofisico_conteo1_xubica"
    Public Function re_inventariofisico_conteo1_xubica(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_inventariofisico_conteo1_xubica]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_inventariofisico_conteo2_xubica"
    Public Function re_inventariofisico_conteo2_xubica(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_inventariofisico_conteo2_xubica]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_inventariofisico_diferencias"
    Public Function re_inventariofisico_diferencias(ByVal bodega As String, ByVal fecha As DateTime, ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_inventariofisico_diferencias]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@fechainventario", System.Data.SqlDbType.DateTime, 8))
                .Item("@fechainventario").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_inventariofisico_diferencias_xubica"
    Public Function re_inventariofisico_diferencias_xubica(ByVal bodega As String, ByVal fecha As DateTime, ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_inventariofisico_diferencias_xubica]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@fechainventario", System.Data.SqlDbType.DateTime, 8))
                .Item("@fechainventario").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_traspasos_entradas_salidas"
    Public Function re_traspasos_entradas_salidas(ByVal bodega_origen As String, ByVal bodega_destino As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal desglosado As Boolean, ByVal solo_diferencias As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_traspasos_entradas_salidas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_origen", System.Data.SqlDbType.VarChar, 5, "bodega_origen"))
                .Item("@bodega_origen").Value = bodega_origen
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_destino", System.Data.SqlDbType.VarChar, 5, "bodega_destino"))
                .Item("@bodega_destino").Value = bodega_destino
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@solo_diferencias", System.Data.SqlDbType.Int, 4, "solo_diferencias"))
                .Item("@solo_diferencias").Value = solo_diferencias

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_resumen_entradas"
    'JAMP:10/Sep/2007 Cambio en filtros de Resumen de Entrada
    Public Function re_resumen_entradas(ByVal SUCURSAL As Long, ByVal BODEGA As String, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal PROVEEDOR As Long, ByVal concepto As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo_agrupado As Long, ByVal desglosado As Boolean, ByVal TipoProveedor As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_resumen_entradas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@SUCURSAL", System.Data.SqlDbType.Int, 4, "SUCURSAL"))
                .Item("@SUCURSAL").Value = SUCURSAL
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.Text, 0, "BODEGA"))
                .Item("@BODEGA").Value = BODEGA
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@DEPARTAMENTO").Value = DEPARTAMENTO
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@PROVEEDOR").Value = PROVEEDOR
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_agrupado", System.Data.SqlDbType.Int, 4, "tipo_agrupado"))
                .Item("@tipo_agrupado").Value = tipo_agrupado
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@TipoProveedor", System.Data.SqlDbType.VarChar, 1, "TipoProveedor"))
                .Item("@TipoProveedor").Value = TipoProveedor
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_informe_de_ventas"
    Public Function re_informe_de_ventas(ByVal fecha_ini As Date, _
    ByVal fecha_fin As Date, ByVal menor_13 As Double, _
    ByVal mayor_13 As Double, ByVal incluir_devoluciones As Boolean, _
    ByVal sucursal As Long) As Events

        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_informe_de_ventas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@menor_13", System.Data.SqlDbType.Decimal, 18, "menor_13"))
                .Item("@menor_13").Value = menor_13
                .Add(New System.Data.SqlClient.SqlParameter("@mayor_13", System.Data.SqlDbType.Decimal, 18, "mayor_13"))
                .Item("@mayor_13").Value = mayor_13
                .Add(New System.Data.SqlClient.SqlParameter("@incluir_devoluciones", System.Data.SqlDbType.Bit, 1, "incluir_devoluciones"))
                .Item("@incluir_devoluciones").Value = incluir_devoluciones
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal")) '@JGTO-18/04/2008: Se añade filtro
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region



#Region "DIPROS Systems, clsDataEnvironment - re_informe_de_ventas_2014"
    Public Function re_informe_de_ventas_2014(ByVal fecha_ini As Date, _
    ByVal fecha_fin As Date, ByVal sucursal As Long) As Events

        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_informe_de_ventas_2014]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_informe_de_ventas_2014"
    Public Function re_comisiones_de_ventas_2014(ByVal fecha_ini As Date, _
    ByVal fecha_fin As Date, ByVal sucursal As Long, ByVal aplica_comision As Boolean) As Events

        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_comisiones_de_ventas_2014]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@aplica_comision", System.Data.SqlDbType.Bit, 1, "aplica_comision"))
                .Item("@aplica_comision").Value = aplica_comision


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_solicitud_traspasos"
    Public Function re_solicitud_traspasos(ByVal solicitud As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_solicitud_traspasos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = solicitud

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ordenes_servicio"
    Public Function re_ordenes_servicio(ByVal sucursal As Long, ByVal tipo_reporte As String, ByVal FECHA_INI As Date, ByVal FECHA_FIN As Date, ByVal CLIENTE As String, ByVal PROVEEDOR As String, ByVal CENTRO_SERVICIO As String, ByVal RECIBE As String, ByVal tipo_servicio As String, ByVal estatus As Char, ByVal FechaPromesa As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ordenes_servicio]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_reporte", System.Data.SqlDbType.VarChar, 2, "tipo_reporte"))
                .Item("@tipo_reporte").Value = tipo_reporte
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = FECHA_INI
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = FECHA_FIN
                .Add(New System.Data.SqlClient.SqlParameter("@CLIENTE", System.Data.SqlDbType.Text, 0, "CLIENTE"))
                .Item("@CLIENTE").Value = CLIENTE
                .Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Text, 0, "PROVEEDOR"))
                .Item("@PROVEEDOR").Value = PROVEEDOR
                .Add(New System.Data.SqlClient.SqlParameter("@CENTRO_SERVICIO", System.Data.SqlDbType.Text, 0, "CENTRO_SERVICIO"))
                .Item("@CENTRO_SERVICIO").Value = CENTRO_SERVICIO
                .Add(New System.Data.SqlClient.SqlParameter("@RECIBE", System.Data.SqlDbType.Text, 0, "RECIBE"))
                .Item("@RECIBE").Value = RECIBE
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_servicio", System.Data.SqlDbType.Char, 1, "tipo_servicio"))
                .Item("@tipo_servicio").Value = tipo_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = estatus
                .Add(New System.Data.SqlClient.SqlParameter("@FechaPromesa", System.Data.SqlDbType.Bit, 1, "FechaPromesa"))
                .Item("@FechaPromesa").Value = FechaPromesa



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_articulos_con_mas_fallas"
    Public Function re_articulos_con_mas_fallas(ByVal GRUPO As Long, ByVal cantidad As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal desglosado As Boolean, ByVal Sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_articulos_con_mas_fallas]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@Sucursal", System.Data.SqlDbType.Int, 4, "Sucursal"))
                .Item("@Sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@CANTIDAD", System.Data.SqlDbType.Int, 4, "CANTIDAD"))
                .Item("@CANTIDAD").Value = cantidad
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_articulos_cambio_fisico"
    Public Function re_articulos_cambio_fisico(ByVal GRUPO As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal desglosado As Boolean, ByVal Sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_articulos_cambio_fisico]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@Sucursal", System.Data.SqlDbType.Int, 4, "Sucursal"))
                .Item("@Sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = GRUPO
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ordenes_servicio_sin_solucion"
    Public Function re_ordenes_servicio_sin_solucion(ByVal FECHA_INI As Date, ByVal FECHA_FIN As Date, ByVal tipo_servicio As String, ByVal centro_servicio As Long, ByVal Sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ordenes_servicio_sin_solucion]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = FECHA_INI
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = FECHA_FIN

                '.Add(New System.Data.SqlClient.SqlParameter("@PROVEEDOR", System.Data.SqlDbType.Text, 0, "PROVEEDOR"))
                '.Item("@PROVEEDOR").Value = PROVEEDOR
                .Add(New System.Data.SqlClient.SqlParameter("@CENTRO_SERVICIO", System.Data.SqlDbType.Int, 4, "CENTRO_SERVICIO"))
                .Item("@CENTRO_SERVICIO").Value = centro_servicio
                '.Add(New System.Data.SqlClient.SqlParameter("@RECIBE", System.Data.SqlDbType.Text, 0, "RECIBE"))
                '.Item("@RECIBE").Value = RECIBE
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_servicio", System.Data.SqlDbType.Char, 1, "tipo_servicio"))
                .Item("@tipo_servicio").Value = tipo_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@Sucursal", System.Data.SqlDbType.Int, 4, "Sucursal"))
                .Item("@Sucursal").Value = Sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_saldos_generales"
    Public Function re_saldos_generales_cobrador(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha As DateTime, ByVal tipo_venta As Char, ByVal desglosado As Boolean, ByVal cobrador As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_saldos_generales_cobrador]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_inicio", System.Data.SqlDbType.Int, 4, "cliente_inicio"))
                .Item("@cliente_inicio").Value = cliente_inicio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_fin", System.Data.SqlDbType.Int, 4, "cliente_fin"))
                .Item("@cliente_fin").Value = cliente_fin
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Char, 1, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_saldos_generales_agrupado"
    Public Function re_saldos_generales_agrupado(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha As DateTime, ByVal tipo_venta As Char, ByVal desglosado As Boolean, ByVal cobrador As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_saldos_generales_agrupado]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_inicio", System.Data.SqlDbType.Int, 4, "cliente_inicio"))
                .Item("@cliente_inicio").Value = cliente_inicio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_fin", System.Data.SqlDbType.Int, 4, "cliente_fin"))
                .Item("@cliente_fin").Value = cliente_fin
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Char, 1, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_saldos_generales_desglosado"
    Public Function re_saldos_generales_desglosado(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha As DateTime, ByVal tipo_venta As Char, ByVal desglosado As Boolean, ByVal cobrador As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_saldos_generales_desglosado]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_inicio", System.Data.SqlDbType.Int, 4, "cliente_inicio"))
                .Item("@cliente_inicio").Value = cliente_inicio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_fin", System.Data.SqlDbType.Int, 4, "cliente_fin"))
                .Item("@cliente_fin").Value = cliente_fin
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Char, 1, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_movimientos_clientes"
    Public Function re_movimientos_clientes(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha_inicio As DateTime, ByVal fecha_fin As DateTime, ByVal tipo_venta As Integer, ByVal cobrador As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_movimientos_clientes]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_desde", System.Data.SqlDbType.Int, 4, "cliente_inicio"))
                .Item("@cliente_desde").Value = cliente_inicio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_hasta", System.Data.SqlDbType.Int, 4, "cliente_fin"))
                .Item("@cliente_hasta").Value = cliente_fin
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_inicio"))
                .Item("@fecha_ini").Value = fecha_inicio
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@COBRADOR").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_saldos_vencidos"
    Public Function re_saldos_vencidos(ByVal sucursal As Integer, ByVal cliente_desde As Integer, ByVal cliente_hasta As Integer, ByVal fecha_vencimiento As DateTime, ByVal tipo As Integer, ByVal plan_credito As Integer, ByVal cobrador As Long, ByVal sin_abono As Byte, ByVal falten_liquidar As Long, ByVal meses_sin_abonar As Long, ByVal para_cliente As Boolean, ByVal imprimir_direccion As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_saldos_vencidos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_desde", System.Data.SqlDbType.Int, 4, "cliente_desde"))
                .Item("@cliente_desde").Value = cliente_desde
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_hasta", System.Data.SqlDbType.Int, 4, "cliente_hasta"))
                .Item("@cliente_hasta").Value = cliente_hasta
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_vencimiento", System.Data.SqlDbType.DateTime, 8, "fecha_vencimiento"))
                .Item("@fecha_vencimiento").Value = fecha_vencimiento
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Int, 4, "tipo"))
                .Item("@tipo").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@sin_abono", System.Data.SqlDbType.Bit, 1, "sin_abono"))
                .Item("@sin_abono").Value = sin_abono
                .Add(New System.Data.SqlClient.SqlParameter("@falten_liquidar", System.Data.SqlDbType.Int, 4, "falten_liquidar"))
                .Item("@falten_liquidar").Value = falten_liquidar
                .Add(New System.Data.SqlClient.SqlParameter("@meses_sin_abonar", System.Data.SqlDbType.Int, 4, "meses_sin_abonar"))
                .Item("@meses_sin_abonar").Value = meses_sin_abonar
                .Add(New System.Data.SqlClient.SqlParameter("@para_cliente", System.Data.SqlDbType.Bit, 1, "para_cliente"))
                .Item("@para_cliente").Value = para_cliente
                .Add(New System.Data.SqlClient.SqlParameter("@imprimir_direccion", System.Data.SqlDbType.Bit, 1, "imprimir_direccion"))
                .Item("@imprimir_direccion").Value = imprimir_direccion
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_resumen_movimientos"
    Public Function re_resumen_movimientos(ByVal sucursal As Integer, ByVal concepto As String, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal tipo_venta As Integer, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_resumen_movimientos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_documentos_vencer"
    Public Function re_documentos_vencer(ByVal sucursal As Long, ByVal cliente As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal tipo_venta As Integer, ByVal cobrador As Long, ByVal tipo_vencimiento As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_documentos_vencer]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_vencimiento", System.Data.SqlDbType.Int, 4, "tipo_vencimiento"))
                .Item("@tipo_vencimiento").Value = tipo_vencimiento
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_documentos_vencer ejecutivo"
    Public Function re_documentos_vencer_ejecutivo(ByVal cliente As Long, ByVal tipo_vencimiento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_documentos_vencer_ejecutivo]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                '.Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                '.Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                '.Item("@fecha_ini").Value = fecha_ini
                '.Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                '.Item("@fecha_fin").Value = fecha_fin
                '.Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                '.Item("@tipo_venta").Value = tipo_venta
                '.Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                '.Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_vencimiento", System.Data.SqlDbType.Int, 4, "tipo_vencimiento"))
                .Item("@tipo_vencimiento").Value = tipo_vencimiento

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Consolidado"
    Public Function re_consolidado(ByVal cliente As Long, ByVal tipo_vencimiento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_consolidado]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                '.Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                '.Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                '.Item("@fecha_ini").Value = fecha_ini
                '.Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                '.Item("@fecha_fin").Value = fecha_fin
                '.Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                '.Item("@tipo_venta").Value = tipo_venta
                '.Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                '.Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_vencimiento", System.Data.SqlDbType.Int, 4, "tipo_vencimiento"))
                .Item("@tipo_vencimiento").Value = tipo_vencimiento

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_anual_clientes"
    Public Function re_reporte_anual(ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo_venta As Integer, ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_reporte_anual]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_desde", System.Data.SqlDbType.Int, 4, "cliente_desde"))
                .Item("@cliente_desde").Value = cliente_inicio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_hasta", System.Data.SqlDbType.Int, 4, "cliente_hasta"))
                .Item("@cliente_hasta").Value = cliente_fin
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal_actual

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_auxiliar_factura"
    Public Function re_auxiliar_factura(ByVal sucursal As Integer, ByVal cliente As Integer, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_auxiliar_factura]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ventas_con_saldos"
    Public Function re_ventas_con_saldos(ByVal sucursal As Long, ByVal cobrador As Long, ByVal tipo As Integer, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal saldos_vencidos As Boolean, ByVal sin_abono As Boolean, ByVal numero_abonos As Long, ByVal desglosado As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ventas_con_saldos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.Text.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.Text.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Int, 4, "tipo"))
                .Item("@tipo").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@sin_abono", System.Data.SqlDbType.Text.Bit, 1, "sin_abono"))
                .Item("@sin_abono").Value = sin_abono
                .Add(New System.Data.SqlClient.SqlParameter("@numero_abonos", System.Data.SqlDbType.Text.Int, 4, "numero_abonos"))
                .Item("@numero_abonos").Value = numero_abonos
                .Add(New System.Data.SqlClient.SqlParameter("@saldos_vencidos", System.Data.SqlDbType.Text.Bit, 1, "saldos_vencidos"))
                .Item("@saldos_vencidos").Value = saldos_vencidos
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Text.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_mercancia_en_transito"
    Public Function re_mercancia_en_transito(ByVal bodega As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_mercancia_en_transito]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_imprime_cheque"
    Public Function re_imprime_cheque_acreedores_diversos(ByVal banco As Long, ByVal cuenta_bancaria As String, ByVal folio_cheque As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_imprime_cheque_acreedores_diversos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Int, 4, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = cuenta_bancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio_cheque", System.Data.SqlDbType.Int, 4, "folio_cheque"))
                .Item("@folio_cheque").Value = folio_cheque


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function re_imprime_cheque_movimientos_pagar(ByVal banco As Long, ByVal cuenta_bancaria As String, ByVal folio_cheque As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_imprime_cheque_movimientos_pagar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Int, 4, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = cuenta_bancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio_cheque", System.Data.SqlDbType.Int, 4, "folio_cheque"))
                .Item("@folio_cheque").Value = folio_cheque


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function re_imprime_cheque_movimientos_chequera(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_cheque As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_imprime_cheque_movimientos_chequera]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Int, 4, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio_cheque", System.Data.SqlDbType.Int, 4, "folio_cheque"))
                .Item("@folio_cheque").Value = folio_cheque

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function re_imprime_cheque_pago_facturas_proveedores(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_cheque As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_imprime_cheque_pago_facturas_proveedores]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Int, 4, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio_cheque", System.Data.SqlDbType.Int, 4, "folio_cheque"))
                .Item("@folio_cheque").Value = folio_cheque

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_antiguedades_saldos_desglosado"
    Public Function re_antiguedades_saldos_desglosado(ByVal proveedor As Long, ByVal fecha_corte As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_antiguedades_saldos_desglosado]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_corte", System.Data.SqlDbType.DateTime, 8, "fecha_corte"))
                .Item("@fecha_corte").Value = fecha_corte

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_antiguedades_saldos"
    Public Function re_antiguedades_saldos(ByVal proveedor As Long, ByVal fecha_corte As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_antiguedades_saldos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_corte", System.Data.SqlDbType.DateTime, 8, "fecha_corte"))
                .Item("@fecha_corte").Value = fecha_corte

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_antiguedades_saldos_cliente_desglosado"
    Public Function re_antiguedades_saldos_cliente_desglosado(ByVal Sucursal As Long, ByVal cliente As Long, ByVal fecha_corte As Date, ByVal Cobrador As Long, ByVal Vencido As Long, ByVal SoloSaldosVencidos As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_antiguedades_saldos_cliente_desglosado]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_corte", System.Data.SqlDbType.DateTime, 8, "fecha_corte"))
                .Item("@fecha_corte").Value = fecha_corte
                .Add(New System.Data.SqlClient.SqlParameter("@Sucursal", System.Data.SqlDbType.Int, 4, "Sucursal"))
                .Item("@Sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@Cobrador", System.Data.SqlDbType.Int, 4, "Cobrador"))
                .Item("@Cobrador").Value = Cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@Vencido", System.Data.SqlDbType.Int, 4, "Vencido"))
                .Item("@Vencido").Value = Vencido
                .Add(New System.Data.SqlClient.SqlParameter("@SoloSaldosVencidos", System.Data.SqlDbType.Bit, 1, "SoloSaldosVencidos"))
                .Item("@SoloSaldosVencidos").Value = SoloSaldosVencidos
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_antiguedades_saldos_cliente"
    Public Function re_antiguedades_saldos_cliente(ByVal Sucursal As Long, ByVal cliente As Long, ByVal fecha_corte As Date, ByVal Cobrador As Long, ByVal Vencido As Long, ByVal SoloSaldosVencidos As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_antiguedades_saldos_cliente]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_corte", System.Data.SqlDbType.DateTime, 8, "fecha_corte"))
                .Item("@fecha_corte").Value = fecha_corte
                .Add(New System.Data.SqlClient.SqlParameter("@Sucursal", System.Data.SqlDbType.Int, 4, "Sucursal"))
                .Item("@Sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@Cobrador", System.Data.SqlDbType.Int, 4, "Cobrador"))
                .Item("@Cobrador").Value = Cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@Vencido", System.Data.SqlDbType.Int, 4, "Vencido"))
                .Item("@Vencido").Value = Vencido
                .Add(New System.Data.SqlClient.SqlParameter("@SoloSaldosVencidos", System.Data.SqlDbType.Bit, 1, "SoloSaldosVencidos"))
                .Item("@SoloSaldosVencidos").Value = SoloSaldosVencidos
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Impresion de Cheques"
    Public Function sp_actualiza_cheque_tipo_movimientos(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_movimiento As Long, ByVal folio_cheque As Long, ByVal concepto As String, ByVal Tipo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_actualiza_cheque_tipo_movimientos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Int, 4, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio_movimiento
                .Add(New System.Data.SqlClient.SqlParameter("@cheque", System.Data.SqlDbType.Int, 4, "cheque"))
                .Item("@cheque").Value = folio_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Int, 4, "tipo"))
                .Item("@tipo").Value = Tipo

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_movimientos(ByVal banco As Long, ByVal cuenta_bancaria As String, ByVal Tipo As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_tipos_movimientos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Int, 4, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuenta_bancaria
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Int, 4, "tipo"))
                .Item("@tipo").Value = Tipo


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Programación de Pagos"
    Public Function sp_movimientos_programacion_pagos(ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_movimientos_programacion_pagos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_actualiza_programacion_chequera_tipo_movimientos(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_movimiento As Long, ByVal concepto As String, ByVal Tipo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_actualiza_programacion_chequera_tipo_movimientos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Int, 4, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio_movimiento
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Int, 4, "tipo"))
                .Item("@tipo").Value = Tipo

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - Pagos por Fecha de Vencimiento"
    Public Function re_pagos_fecha_vencimiento(ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal tipo_movimiento As Long, ByVal tipo_estatus As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_pagos_fecha_vencimiento]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_movimiento", System.Data.SqlDbType.Int, 4, "tipo_movimiento"))
                .Item("@tipo_movimiento").Value = tipo_movimiento
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_estatus", System.Data.SqlDbType.Int, 4, "tipo_estatus"))
                .Item("@tipo_estatus").Value = tipo_estatus

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Vencimientos de CxP"
    Public Function re_vencimientos_cxp(ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal tipo_estatus As Long, ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_vencimientos_cxp]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_estatus", System.Data.SqlDbType.Int, 4, "tipo_estatus"))
                .Item("@tipo_estatus").Value = tipo_estatus
                .Add(New SqlClient.SqlParameter("@proveedor", SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Movimientos Chequera"
    Public Function re_movimientos_chequera(ByVal chequera As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_movimientos_chequera]"
            With oCommand.Parameters
                '@ACH-25/06/07: Se modificó para que aceptara varias chequeras
                .Add(New SqlClient.SqlParameter("@chequera", SqlDbType.Int, 4, "chequera"))
                '/@ACH-25/06/07
                .Item("@chequera").Value = chequera
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Mercancia Pendiente Entrega"
    Public Function re_mercancia_pendiente_entrega(ByVal sucursal As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_mercancia_pendiente_entrega]"
            With oCommand.Parameters
                .Add(New SqlClient.SqlParameter("@sucursal", SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Sugerencia Cobro"
    Public Function re_sugerencia_cobro(ByVal sucursal As Long, ByVal fecha_fin As DateTime, ByVal desglosado As Boolean, ByVal intTipoVenta As Integer, ByVal cobrador As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_sugerencia_cobro]"
            With oCommand.Parameters
                .Add(New SqlClient.SqlParameter("@sucursal", SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = intTipoVenta
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - sugerencia cobro proxima visitas"
    Public Function re_sugerencia_cobro_proximas_visitas(ByVal sucursal As Long, ByVal fecha_fin As DateTime, ByVal desglosado As Boolean, ByVal intTipoVenta As Integer, ByVal cobrador As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_sugerencia_cobro_proximas_visitas]"
            With oCommand.Parameters
                .Add(New SqlClient.SqlParameter("@sucursal", SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = intTipoVenta
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Pagos Efectivo Clientes"
    Public Function re_pagos_efectivo_clientes(ByVal sucursal As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal cantidad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_pagos_efectivo_clientes]"
            With oCommand.Parameters
                .Add(New SqlClient.SqlParameter("@sucursal", SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Money, 8, "cantidad"))
                .Item("@cantidad").Value = cantidad

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Traer Relacion  de Pago a Proveedores"

    Public Function sp_traer_relacion_pago_proveedores(ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traer_relacion_pago_proveedores]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                '.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                '.Item("@QUIEN").Value = Connection.User



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - Relacion  de Pago a Proveedores"

    Public Function re_relacion_pago_proveedores(ByVal fecha As Date, ByVal desglosado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_relacion_pago_proveedores]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_impresion_pago_facturas_proveedores"

    Public Function re_impresion_pago_facturas_proveedores(ByVal fecha As DateTime, ByVal folio_cheque As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_impresion_pago_facturas_proveedores]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@folio_cheque", System.Data.SqlDbType.Int, 4, "folio_cheque"))
                .Item("@folio_cheque").Value = folio_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "Dipros Systems -- Polizas"


#Region "DIPROS Systems, clsDataEnvironment - Poliza de Diario de Ventas Enajenacion"

    Public Function re_poliza_diario_ventas_enajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_diario_ventas_enajenacion]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza

                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - Poliza de Diario de Ventas Normales"

    Public Function re_poliza_diario_ventas_normales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_diario_ventas_normales]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza de Cobranza de Intereses"

    Public Function re_poliza_cobranza_intereses_enajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_cobranza_intereses_enajenacion]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function re_poliza_cobranza_intereses_normales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_cobranza_intereses_normales]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza de Ingresos por Intereses de Enajenación"

    Public Function re_poliza_ingresos_intereses_enajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_ingresos_intereses_enajenacion]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza de Ingresos por Intereses de Normales"

    Public Function re_poliza_ingresos_intereses_normales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_ingresos_intereses_normales]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza de Descuento por Pronto Pago Normal"

    Public Function re_poliza_descuento_por_pronto_pago_normales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_descuento_por_pronto_pago_normales]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza de Descuento por Pronto Pago Enajenación"

    Public Function re_poliza_descuento_por_pronto_pago_enajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_descuento_por_pronto_pago_enajenacion]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza Abono Cliente Pago Puntual"

    Public Function re_poliza_abono_cliente_pago_enajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_abono_cliente_pago_enajenacion]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza Abono Cliente Pago Puntual y Contado"

    Public Function re_poliza_abono_cliente_pago_normales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_abono_cliente_pago_normales]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region


#Region "DIPROS Systems, clsDataEnvironment - Póliza Compras Personas Morales"

    Public Function re_poliza_compras_personas_morales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_compras_personas_morales]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza Compras Personas Fisicas"

    Public Function re_poliza_compras_personas_fisicas(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_compras_personas_fisicas]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza Devoluciones al Proveedor"

    Public Function re_poliza_devoluciones_proveeedor(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_devoluciones_proveeedor]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Póliza Traspasos"

    Public Function re_poliza_traspasos(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_traspasos]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

    Public Function re_poliza_cancelacion_intereses_especiales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_cancelacion_intereses_especiales]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function re_poliza_descuentos_ventas_convenio(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_descuentos_ventas_convenio]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function re_poliza_descuentos_ventas_volumen(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_descuentos_ventas_volumen]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function re_poliza_nota_credito_bonificacion_precio_tasa_iva(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_nota_credito_bonificacion_precio_tasa_iva]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function re_poliza_fletes_otros_gastos(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_fletes_otros_gastos]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha


                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#Region "DIPROS Systems, clsDataEnvironment - Ejecutar Poliza"

    Public Function re_ejecuta_polizasFlete(ByVal entrada As Long, ByVal fecha As DateTime, ByVal llena_poliza As Boolean, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_poliza_fletes]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function re_ejecuta_polizas(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_ejecuta_polizas]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@llena_poliza", System.Data.SqlDbType.Bit, 1, "llena_poliza"))
                .Item("@llena_poliza").Value = llena_poliza
                .Add(New System.Data.SqlClient.SqlParameter("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                .Item("@poliza").Value = poliza

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - sp_polizas_grl Poliza"

    Public Function sp_polizas_grl(ByVal flete As Long, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_polizas_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@flete ", System.Data.SqlDbType.Int, 4, "flete "))
                .Item("@flete ").Value = flete
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#End Region

#Region "DIPROS Systems, clsDataEnvironment - Sugerencia Cobro"
    Public Function re_documentos_por_anio(ByVal sucursal As Long, ByVal anio As Long, ByVal TipoVenta As Integer, ByVal alafecha As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_documentos_por_anio]"
            With oCommand.Parameters
                .Add(New SqlClient.SqlParameter("@sucursal", SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New SqlClient.SqlParameter("@anio", SqlDbType.Int, 4, "anio"))
                .Item("@anio").Value = anio

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Int, 4, "tipo_venta"))
                .Item("@tipo_venta").Value = TipoVenta

                .Add(New SqlClient.SqlParameter("@alafecha ", SqlDbType.DateTime, 8, "alafecha "))
                .Item("@alafecha ").Value = alafecha


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - Resumen de Salidas Detallado"
    Public Function re_resumensalidasdetallado(ByVal bodega As String, ByVal departamento As Long, ByVal grupo As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_resumensalidasdetallado]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 4, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Int, 4, "tipo"))
                .Item("@tipo").Value = tipo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_clientes_en_juridico_por_estatus"

    Public Function re_clientes_en_juridico_por_estatus(ByVal sucursal As String, ByVal abogado As String, ByVal status As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_clientes_en_juridico_por_estatus]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Text, 0, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Text, 0, "abogado"))
                .Item("@abogado").Value = abogado
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.VarChar, 2, "status"))
                .Item("@status").Value = status
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_abonos_de_clientes_en_juridico"

    Public Function re_abonos_de_clientes_en_juridico(ByVal sucursal As String, ByVal abogado As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_abonos_de_clientes_en_juridico]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Text, 0, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Text, 0, "abogado"))
                .Item("@abogado").Value = abogado
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_comisiones_a_abogados"

    Public Function re_comisiones_a_abogados(ByVal sucursal As String, ByVal abogado As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_comisiones_a_abogados]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Text, 0, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Text, 0, "abogado"))
                .Item("@abogado").Value = abogado
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_abonos_de_clientes_en_juridico"

    Public Function re_imprime_correspndencia(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_imprime_correspndencia]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_abonos_de_clientes_en_juridico"

    Public Function re_facturas_canceladas(ByVal sucursal As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo As Char, ByVal convenio As Long, ByVal facturas_timbradas As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_facturas_canceladas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@facturas_timbradas", System.Data.SqlDbType.Bit, 1, "facturas_timbradas"))
                .Item("@facturas_timbradas").Value = facturas_timbradas
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_facturas_surtidas_sobrepedido"

    Public Function re_facturas_surtidas_sobrepedido(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_facturas_surtidas_sobrepedido]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_cuentas_no_localizables"
    Public Function re_cuentas_no_localizables(ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_cuentas_no_localizables]"
            With oCommand.Parameters
                .Add(New SqlClient.SqlParameter("@sucursal", SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ventas_cobradas"

    Public Function re_ventas_cobradas(ByVal sucursal As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ventas_cobradas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_abonos_cancelados"

    Public Function re_abonos_cancelados(ByVal sucursal As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_abonos_cancelados]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_INI", System.Data.SqlDbType.DateTime, 8, "FECHA_INI"))
                .Item("@FECHA_INI").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@FECHA_FIN", System.Data.SqlDbType.DateTime, 8, "FECHA_FIN"))
                .Item("@FECHA_FIN").Value = fecha_fin
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_series_articulos"

    Public Function re_series_articulos(ByVal departamento As Long, ByVal grupo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_series_articulos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_beneficiarios"
    Public Function re_beneficiarios( _
    ByVal beneficiario_inicial As String, ByVal beneficiario_final As String, _
    ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, _
    ByVal Cuenta_Bancaria As String) As Events

        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_beneficiarios]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@beneficiario_inicial", System.Data.SqlDbType.VarChar, 100, "beneficiario_inicial"))
                .Item("@beneficiario_inicial").Value = beneficiario_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@beneficiario_final", System.Data.SqlDbType.VarChar, 100, "beneficiario_final"))
                .Item("@beneficiario_final").Value = beneficiario_final
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final
                '@JGTO-18/04/2008: Se añade filtro por cuenta
                .Add(New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@Cuenta_Bancaria").Value = Cuenta_Bancaria

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_movimientos_no_aplicados"
    Public Function re_movimientos_no_aplicados(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_movimientos_no_aplicados]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_mercancias_transito"

    Public Function re_mercancias_transito(ByVal bodega As String, ByVal fecha_desde As DateTime, ByVal fecha_hasta As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_mercancias_transito]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.Text, 0, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_desde", System.Data.SqlDbType.DateTime, 8, "fecha_desde"))
                .Item("@fecha_desde").Value = fecha_desde
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_hasta", System.Data.SqlDbType.DateTime, 8, "fecha_hasta"))
                .Item("@fecha_hasta").Value = fecha_hasta

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_cambios_precios"

    Public Function re_cambios_precios(ByVal Sucursal As Long, ByVal Departamento As Long, ByVal fecha_desde As DateTime, ByVal fecha_hasta As DateTime, ByVal Desglosado As Boolean, ByVal Precio As Long, ByVal Proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_cambios_precios]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Departamento
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_desde", System.Data.SqlDbType.DateTime, 8, "fecha_desde"))
                .Item("@fecha_desde").Value = fecha_desde
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_hasta", System.Data.SqlDbType.DateTime, 8, "fecha_hasta"))
                .Item("@fecha_hasta").Value = fecha_hasta
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = Desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
                .Item("@precio").Value = Precio
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_cotizaciones"

    Public Function re_cotizaciones(ByVal cotizacion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_cotizaciones]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_fletes_otroscargos"

    Public Function re_fletes_otroscargos(ByVal sucursal As Long, ByVal incluido_factura As Boolean, ByVal desde As Date, ByVal hasta As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_fletes_otroscargos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@incluido_factura", System.Data.SqlDbType.Bit, 1, "incluido_factura"))
                .Item("@incluido_factura").Value = incluido_factura
                .Add(New System.Data.SqlClient.SqlParameter("@desde", System.Data.SqlDbType.DateTime, 8, "desde"))
                .Item("@desde").Value = desde
                .Add(New System.Data.SqlClient.SqlParameter("@hasta", System.Data.SqlDbType.DateTime, 8, "hasta"))
                .Item("@hasta").Value = hasta

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_ventas_diarias"
    Public Function re_ventas_diarias(ByVal mes As String, ByVal anio As String, ByVal desglosado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ventas_diarias]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@mes", System.Data.SqlDbType.VarChar, 2, "mes"))
                .Item("@mes").Value = mes
                .Add(New System.Data.SqlClient.SqlParameter("@anio", System.Data.SqlDbType.VarChar, 4, "anio"))
                .Item("@anio").Value = anio
                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Bit, 1, "desglosado"))
                .Item("@desglosado").Value = desglosado


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_vistas_salidas"
    Public Function re_vistas_salidas(ByVal folio_vista_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_vistas_salidas]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.VarChar, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_vistas_salidas"
    Public Function re_vistas_entradas(ByVal folio_vista_entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_vistas_entradas]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_entrada", System.Data.SqlDbType.VarChar, 4, "folio_vista_entrada"))
                .Item("@folio_vista_entrada").Value = folio_vista_entrada


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_mercancia_etiquetada"
    Public Function re_mercancia_etiquetada(ByVal bodega As String, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_mercancia_etiquetada]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA", System.Data.SqlDbType.VarChar, 8, "BODEGA"))
                .Item("@BODEGA").Value = bodega

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function re_ventas_fecha_entrega(ByVal sucursal As Long, ByVal fecha_entrega As DateTime, ByVal horario As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ventas_fecha_entrega]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_entrega", System.Data.SqlDbType.DateTime, 8, "fecha_entrega"))
                .Item("@fecha_entrega").Value = fecha_entrega
                .Add(New System.Data.SqlClient.SqlParameter("@horario", System.Data.SqlDbType.Char, 1, "horario"))
                .Item("@horario").Value = horario


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "Ordenes de Recuperacion"

    Public Function re_ordenes_recuperacion(ByVal sucursal As Long, ByVal status As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ordenes_recuperacion]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = status


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function re_ordenes_recuperacion_vale(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ordenes_recuperacion_vale]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_movimiento_inventario"
    Public Function re_movimiento_inventario(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_movimiento_inventario]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_movimiento_inventario"
    Public Function re_movimiento_inventario_cancelacion_venta(ByVal sucursal As Long, ByVal concepto As String, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_movimiento_inventario_cancelacion_venta]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_referencia", System.Data.SqlDbType.Int, 4, "sucursal_referencia"))
                .Item("@sucursal_referencia").Value = sucursal_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = concepto_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_clientes_sel"
    Public Function re_clientes_sel(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_clientes_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_mercancia_vistas_almacen"

    Public Function re_mercancia_vistas_almacen(ByVal sucursal As Long, ByVal desglosado As Boolean, ByVal status As Int16, ByVal fechaini As DateTime, ByVal fechafin As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_mercancia_vistas_almacen]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fechaini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fechafin
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Int, 4, "status"))
                .Item("@status").Value = status

                .Add(New System.Data.SqlClient.SqlParameter("@desglosado", System.Data.SqlDbType.Int, 4, "desglosado"))
                .Item("@desglosado").Value = desglosado



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_confirmacion_saldos_cobrador"
    Public Function re_confirmacion_saldos_cobrador(ByVal Sucursal As Long, ByVal Cobrador As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_confirmacion_saldos_cobrador]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@Sucursal", System.Data.SqlDbType.Int, 4, "Sucursal"))
                .Item("@Sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@Cobrador", System.Data.SqlDbType.Int, 4, "Cobrador"))
                .Item("@Cobrador").Value = Cobrador

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_reporte_estadistico"
    Public Function re_reporte_estadistico(ByVal Departamento As Long, ByVal Grupo As Long, ByVal Proveedor As Long, ByVal Existencias As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_reporte_estadistico]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = Grupo
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@Existencias", System.Data.SqlDbType.Bit, 1, "Existencias"))
                .Item("@Existencias").Value = Existencias
                'Existencias
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_utilidad_ventas"
    Public Function re_utilidad_ventas(ByVal Departamento As Long, ByVal Grupo As Long, ByVal Proveedor As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal ConcentradoPor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_utilidad_ventas]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = Grupo
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Proveedor

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final

                .Add(New System.Data.SqlClient.SqlParameter("@concentradopor", System.Data.SqlDbType.Int, 4, "concentradopor"))
                .Item("@concentradopor").Value = ConcentradoPor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_diferencias_costos_pedido_fabrica"
    Public Function re_diferencias_costos_pedido_fabrica(ByVal Sucursal As Long, ByVal Fecha_ini As DateTime, ByVal Fecha_fin As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_diferencias_costos_pedido_fabrica]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = Fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = Fecha_fin

                'Existencias
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_ventas_vendedor"
    Public Function re_ventas_vendedor(ByVal Sucursal As Long, ByVal Fecha_ini As DateTime, ByVal Fecha_fin As DateTime, ByVal Departamento As Long, ByVal Grupo As String, ByVal Proveedor As Long, ByVal Vendedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ventas_vendedor]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = Fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = Fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Text, 0, "grupo"))
                .Item("@grupo").Value = Grupo
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = Vendedor


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_descuentos_especiales_programados"
    Public Function re_descuentos_especiales_programados(ByVal Departamento As Long, ByVal Grupo As Long, ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal SoloVigentes As Boolean, ByVal SoloPendientesAutorizar As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_descuentos_especiales_programados]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = Grupo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final

                .Add(New System.Data.SqlClient.SqlParameter("@SoloVigentes", System.Data.SqlDbType.Bit, 1, "SoloVigentes"))
                .Item("@SoloVigentes").Value = SoloVigentes

                .Add(New System.Data.SqlClient.SqlParameter("@SoloPendientesAutorizar", System.Data.SqlDbType.Bit, 1, "SoloPendientesAutorizar"))
                .Item("@SoloPendientesAutorizar").Value = SoloPendientesAutorizar

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_diferencia_costos_devolucion_proveedor"

    Public Function re_diferencia_costos_devolucion_proveedor(ByVal TipoOrden As Int16, ByVal fechaini As DateTime, ByVal fechafin As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_diferencia_costos_devolucion_proveedor]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fechaini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fechafin

                .Add(New System.Data.SqlClient.SqlParameter("@TipoOrden", System.Data.SqlDbType.Int, 4, "TipoOrden"))
                .Item("@TipoOrden").Value = TipoOrden

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - sp_ordenes_servicio_detalle_reporte_grs"

    Public Function re_ordenes_servicio_detalle_reporte_grs(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_ordenes_servicio_detalle_reporte_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_imprime_vale"
    Public Function re_imprime_vale(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_imprime_vale]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

    Public Function re_ventas_cancelaciones(ByVal sucursal As Long, ByVal fecha_inicial As Date, ByVal fecha_final As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_ventas_cancelaciones]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_final
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#Region "DIPROS Systems, clsDataEnvironment - re_reporte_vales"
    Public Function re_reporte_vales(ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal vencidos As Boolean, ByVal Estatus As Char, ByVal TipoVale As Long, ByVal fecha_corte As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_reporte_vales]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final
                .Add(New System.Data.SqlClient.SqlParameter("@vencidos", System.Data.SqlDbType.Bit, 1, "vencidos"))
                .Item("@vencidos").Value = vencidos

                .Add(New System.Data.SqlClient.SqlParameter("@Estatus", System.Data.SqlDbType.Char, 1, "Estatus"))
                .Item("@Estatus").Value = Estatus
                .Add(New System.Data.SqlClient.SqlParameter("@TipoVale", System.Data.SqlDbType.Int, 4, "TipoVale"))
                .Item("@TipoVale").Value = TipoVale

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_corte", System.Data.SqlDbType.DateTime, 8, "fecha_corte"))
                .Item("@fecha_corte").Value = fecha_corte
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

    Public Function re_devoluciones_proveedor(ByVal bodega As String, ByVal Proveedor As Long, ByVal Desglosado As Boolean, ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal Estatus As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_devoluciones_proveedor]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 4, "Proveedor"))
                .Item("@Proveedor").Value = Proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@Desglosado", System.Data.SqlDbType.Bit, 1, "Desglosado"))
                .Item("@Desglosado").Value = Desglosado
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_final

                .Add(New System.Data.SqlClient.SqlParameter("@Estatus", System.Data.SqlDbType.Int, 4, "Estatus"))
                .Item("@Estatus").Value = Estatus

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

#Region "DIPROS Systems, clsDataEnvironment - re_bonificaciones"
    Public Function re_bonificaciones(ByVal Sucursal As Long, ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal Tipo As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_bonificaciones]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = Tipo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_facturas_pendientes_timbrar"
    Public Function re_facturas_pendientes_timbrar(ByVal Sucursal As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_facturas_pendientes_timbrar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_recibos_cancelados"
    Public Function re_recibos_cancelados(ByVal Sucursal As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_recibos_cancelados]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_anticipos"
    Public Function re_anticipos(ByVal cliente As Long, ByVal tipo As Long, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal tipo_pago As Int32) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_anticipos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Int, 4, "tipo"))
                .Item("@tipo").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@FechaIni", System.Data.SqlDbType.DateTime, 8, "FechaIni"))
                .Item("@FechaIni").Value = FechaIni
                .Add(New System.Data.SqlClient.SqlParameter("@FechaFin", System.Data.SqlDbType.DateTime, 8, "FechaFin"))
                .Item("@FechaFin").Value = FechaFin
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Int, 4, "tipo_pago"))
                .Item("@tipo_pago").Value = tipo_pago

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_recibo_ticket"
    Public Function re_recibo_ticket(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal serie_referencia As String, ByVal folio_referencia As Integer, ByVal documento_referencia As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_recibo_ticket]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                '.Add(New System.Data.SqlClient.SqlParameter("@serie_referencia", System.Data.SqlDbType.Char, 3, "serie_referencia"))
                '.Item("@serie_referencia").Value = serie_referencia
                '.Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                '.Item("@folio_referencia").Value = folio_referencia
                '.Add(New System.Data.SqlClient.SqlParameter("@documento_referencia", System.Data.SqlDbType.Int, 4, "documento_referencia"))
                '.Item("@documento_referencia").Value = documento_referencia

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_recibo_ticket_promocion"
    Public Function re_recibo_ticket_promocion(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_recibo_ticket_promocion]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_recibo_menos"
    Public Function re_recibo_menos(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal serie_referencia As String, ByVal folio_referencia As Integer, ByVal documento_referencia As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_recibo_menos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio



            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region



#Region "DIPROS Systems, clsDataEnvironment - re_menos"
    Public Function re_menos(ByVal sucursal As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal cajero As Long, ByVal cancelados As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_menos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final

                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero

                .Add(New System.Data.SqlClient.SqlParameter("@cancelados", System.Data.SqlDbType.Bit, 1, "cancelados"))
                .Item("@cancelados").Value = cancelados


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


#Region "DIPROS Systems, clsDataEnvironment - re_pedidos_fabrica_surtidos_fecha"
    Public Function re_pedidos_fabrica_surtidos_fecha(ByVal sucursal As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_pedidos_fabrica_surtidos_fecha]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final

                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - sp_envio_informacion_txt"

    Public Function sp_envio_informacion_txt(ByVal sucursal As Long, ByVal convenio As Long, ByVal año As Long, ByVal no_quincena As Long, ByVal tipo As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvents As New Events
        Try
            oCommand.CommandText = "[sp_envio_informacion_txt]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@año", System.Data.SqlDbType.Int, 4, "año"))
                .Item("@año").Value = año
                .Add(New System.Data.SqlClient.SqlParameter("@no_quincena", System.Data.SqlDbType.Int, 4, "no_quincena"))
                .Item("@no_quincena").Value = no_quincena
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = tipo
            End With
            oEvents.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvents.Ex = ex
            oEvents.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvents.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvents
    End Function
#End Region

#Region "DIPROS Systems, clsDataEnvironment - re_llamadas"
    Public Function re_llamadas(ByVal sucursal As Long, ByVal tipo_llamada As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[re_llamadas]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = tipo_llamada
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = fecha_inicial
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = fecha_final

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
#End Region


End Class

#End Region

