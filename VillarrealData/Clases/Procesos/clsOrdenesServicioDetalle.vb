'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesServicioDetalle
'DATE:		02/06/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - OrdenesServicioDetalle"
Public Class clsOrdenesServicioDetalle
    Public Function sp_ordenes_servicio_detalle_ins(ByRef oData As DataSet, ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Item("@orden_servicio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "Partida")
                .Item("@partida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@persona", System.Data.SqlDbType.VarChar, 100, "persona"))
                .Item("@persona").Value = Connection.GetValue(oData, "persona")
                .Add(New System.Data.SqlClient.SqlParameter("@capturo", System.Data.SqlDbType.VarChar, 100, "capturo"))
                .Item("@capturo").Value = Connection.GetValue(oData, "capturo")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_promesa", System.Data.SqlDbType.DateTime, 8, "fecha_promesa"))
                .Item("@fecha_promesa").Value = Connection.GetValue(oData, "fecha_promesa")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            'Connection.SetValue(oData, "orden_servicio", oCommand.Parameters.Item("@orden_servicio").Value)
            Connection.SetValue(oData, "partida", oCommand.Parameters.Item("@partida").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_detalle_upd(ByRef oData As DataSet, ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "Partida")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@persona", System.Data.SqlDbType.VarChar, 100, "persona"))
                .Item("@persona").Value = Connection.GetValue(oData, "persona")
                .Add(New System.Data.SqlClient.SqlParameter("@capturo", System.Data.SqlDbType.VarChar, 100, "capturo"))
                .Item("@capturo").Value = Connection.GetValue(oData, "capturo")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_promesa", System.Data.SqlDbType.DateTime, 8, "fecha_promesa"))
                .Item("@fecha_promesa").Value = Connection.GetValue(oData, "fecha_promesa")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_detalle_del(ByVal orden_servicio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_detalle_exs(ByVal orden_servicio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_detalle_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_detalle_sel(ByVal orden_servicio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_detalle_grs(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


