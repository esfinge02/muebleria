'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosCobrarFormasPago
'DATE:		24/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - MovimientosCobrarFormasPago"
Public Class clsMovimientosCobrarFormasPago
    Public Function sp_movimientos_cobrar_formas_pago_ins(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long, ByVal sucursal_pago As Long, ByVal fecha As Date, ByVal caja As Long, ByVal cajero As Long, ByVal monto As Double, ByVal tipo_cambio As Double, ByVal dolares As Long, ByVal ultimos_digitos_cuenta As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_formas_pago_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_documento", System.Data.SqlDbType.Int, 4, "sucursal_documento"))
                .Item("@sucursal_documento").Value = sucursal_documento
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_documento", System.Data.SqlDbType.VarChar, 5, "concepto_documento"))
                .Item("@concepto_documento").Value = concepto_documento
                .Add(New System.Data.SqlClient.SqlParameter("@serie_documento", System.Data.SqlDbType.Char, 3, "serie_documento"))
                .Item("@serie_documento").Value = serie_documento
                .Add(New System.Data.SqlClient.SqlParameter("@folio_documento", System.Data.SqlDbType.Int, 4, "folio_documento"))
                .Item("@folio_documento").Value = folio_documento
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_documento", System.Data.SqlDbType.Int, 4, "cliente_documento"))
                .Item("@cliente_documento").Value = cliente_documento
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_pago", System.Data.SqlDbType.Int, 4, "sucursal_pago"))
                .Item("@sucursal_pago").Value = sucursal_pago
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
                .Item("@caja").Value = caja
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Money, 8, "monto"))
                .Item("@monto").Value = monto
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cambio", System.Data.SqlDbType.Money, 8, "tipo_cambio"))
                .Item("@tipo_cambio").Value = tipo_cambio
                .Add(New System.Data.SqlClient.SqlParameter("@dolares", System.Data.SqlDbType.Int, 4, "dolares"))
                .Item("@dolares").Value = dolares
                .Add(New System.Data.SqlClient.SqlParameter("@ultimos_digitos_cuenta", System.Data.SqlDbType.VarChar, 4, "ultimos_digitos_cuenta"))
                .Item("@ultimos_digitos_cuenta").Value = ultimos_digitos_cuenta
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_formas_pago_upd(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long, ByVal sucursal_pago As Long, ByVal fecha As Date, ByVal caja As Long, ByVal cajero As Long, ByVal monto As Double, ByVal tipo_cambio As Double, ByVal dolares As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_formas_pago_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_documento", System.Data.SqlDbType.Int, 4, "sucursal_documento"))
                .Item("@sucursal_documento").Value = sucursal_documento
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_documento", System.Data.SqlDbType.VarChar, 5, "concepto_documento"))
                .Item("@concepto_documento").Value = concepto_documento
                .Add(New System.Data.SqlClient.SqlParameter("@serie_documento", System.Data.SqlDbType.Char, 3, "serie_documento"))
                .Item("@serie_documento").Value = serie_documento
                .Add(New System.Data.SqlClient.SqlParameter("@folio_documento", System.Data.SqlDbType.Int, 4, "folio_documento"))
                .Item("@folio_documento").Value = folio_documento
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_documento", System.Data.SqlDbType.Int, 4, "cliente_documento"))
                .Item("@cliente_documento").Value = cliente_documento
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_pago", System.Data.SqlDbType.Int, 4, "sucursal_pago"))
                .Item("@sucursal_pago").Value = sucursal_pago
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
                .Item("@caja").Value = caja
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Money, 8, "monto"))
                .Item("@monto").Value = monto
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cambio", System.Data.SqlDbType.Money, 8, "tipo_cambio"))
                .Item("@tipo_cambio").Value = tipo_cambio
                .Add(New System.Data.SqlClient.SqlParameter("@dolares", System.Data.SqlDbType.Int, 4, "dolares"))
                .Item("@dolares").Value = dolares
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_formas_pago_del(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_formas_pago_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_documento", System.Data.SqlDbType.Int, 4, "sucursal_documento"))
                .Item("@sucursal_documento").Value = sucursal_documento
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_documento", System.Data.SqlDbType.VarChar, 5, "concepto_documento"))
                .Item("@concepto_documento").Value = concepto_documento
                .Add(New System.Data.SqlClient.SqlParameter("@serie_documento", System.Data.SqlDbType.Char, 3, "serie_documento"))
                .Item("@serie_documento").Value = serie_documento
                .Add(New System.Data.SqlClient.SqlParameter("@folio_documento", System.Data.SqlDbType.Int, 4, "folio_documento"))
                .Item("@folio_documento").Value = folio_documento
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_documento", System.Data.SqlDbType.Int, 4, "cliente_documento"))
                .Item("@cliente_documento").Value = cliente_documento
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_formas_pago_exs(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_formas_pago_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_documento", System.Data.SqlDbType.Int, 4, "sucursal_documento"))
                .Item("@sucursal_documento").Value = sucursal_documento
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_documento", System.Data.SqlDbType.VarChar, 5, "concepto_documento"))
                .Item("@concepto_documento").Value = concepto_documento
                .Add(New System.Data.SqlClient.SqlParameter("@serie_documento", System.Data.SqlDbType.Char, 3, "serie_documento"))
                .Item("@serie_documento").Value = serie_documento
                .Add(New System.Data.SqlClient.SqlParameter("@folio_documento", System.Data.SqlDbType.Int, 4, "folio_documento"))
                .Item("@folio_documento").Value = folio_documento
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_documento", System.Data.SqlDbType.Int, 4, "cliente_documento"))
                .Item("@cliente_documento").Value = cliente_documento
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_formas_pago_sel(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_formas_pago_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_documento", System.Data.SqlDbType.Int, 4, "sucursal_documento"))
                .Item("@sucursal_documento").Value = sucursal_documento
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_documento", System.Data.SqlDbType.VarChar, 5, "concepto_documento"))
                .Item("@concepto_documento").Value = concepto_documento
                .Add(New System.Data.SqlClient.SqlParameter("@serie_documento", System.Data.SqlDbType.Char, 3, "serie_documento"))
                .Item("@serie_documento").Value = serie_documento
                .Add(New System.Data.SqlClient.SqlParameter("@folio_documento", System.Data.SqlDbType.Int, 4, "folio_documento"))
                .Item("@folio_documento").Value = folio_documento
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_documento", System.Data.SqlDbType.Int, 4, "cliente_documento"))
                .Item("@cliente_documento").Value = cliente_documento
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_formas_pago_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_formas_pago_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region



