
Imports Dipros.Utils

Public Class clsDescuentosEspecialesProgramados

    Public Function sp_descuentos_especiales_programados_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_programados_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = Connection.GetValue(oData, "folio_descuento")
                .Item("@folio_descuento").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_creacion", System.Data.SqlDbType.DateTime, 8, "fecha_creacion"))
                .Item("@fecha_creacion").Value = Connection.GetValue(oData, "fecha_creacion")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_descuento", System.Data.SqlDbType.Float, 8, "porcentaje_descuento"))
                .Item("@porcentaje_descuento").Value = Connection.GetValue(oData, "porcentaje_descuento")
                .Add(New System.Data.SqlClient.SqlParameter("@aplica_existencia", System.Data.SqlDbType.Bit, 1, "aplica_existencia"))
                .Item("@aplica_existencia").Value = Connection.GetValue(oData, "aplica_existencia")
                .Add(New System.Data.SqlClient.SqlParameter("@aplica_pedido_fabrica", System.Data.SqlDbType.Bit, 1, "aplica_pedido_fabrica"))
                .Item("@aplica_pedido_fabrica").Value = Connection.GetValue(oData, "aplica_pedido_fabrica")

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicio", System.Data.SqlDbType.DateTime, 8, "fecha_inicio"))
                .Item("@fecha_inicio").Value = Connection.GetValue(oData, "fecha_inicio")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = Connection.GetValue(oData, "fecha_final")

                .Add(New System.Data.SqlClient.SqlParameter("@precio_contado", System.Data.SqlDbType.Float, 8, "precio_contado"))
                .Item("@precio_contado").Value = Connection.GetValue(oData, "precio_contado")

                .Add(New System.Data.SqlClient.SqlParameter("@precio_credito", System.Data.SqlDbType.Float, 8, "precio_credito"))
                .Item("@precio_credito").Value = Connection.GetValue(oData, "precio_credito")


                .Add(New System.Data.SqlClient.SqlParameter("@agotar_existencias", System.Data.SqlDbType.Bit, 1, "agotar_existencias"))
                .Item("@agotar_existencias").Value = Connection.GetValue(oData, "agotar_existencias")
                .Add(New System.Data.SqlClient.SqlParameter("@autorizar", System.Data.SqlDbType.Bit, 1, "autorizar"))
                .Item("@autorizar").Value = Connection.GetValue(oData, "autorizar")
                .Add(New System.Data.SqlClient.SqlParameter("@solicita", System.Data.SqlDbType.VarChar, 15, "solicita"))
                .Item("@solicita").Value = Connection.GetValue(oData, "solicita")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 2000, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = Connection.GetValue(oData, "activo")


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio_descuento", oCommand.Parameters.Item("@folio_descuento").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_programados_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_programados_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = Connection.GetValue(oData, "folio_descuento")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_creacion", System.Data.SqlDbType.DateTime, 8, "fecha_creacion"))
                .Item("@fecha_creacion").Value = Connection.GetValue(oData, "fecha_creacion")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_descuento", System.Data.SqlDbType.Float, 8, "porcentaje_descuento"))
                .Item("@porcentaje_descuento").Value = Connection.GetValue(oData, "porcentaje_descuento")
                .Add(New System.Data.SqlClient.SqlParameter("@aplica_existencia", System.Data.SqlDbType.Bit, 1, "aplica_existencia"))
                .Item("@aplica_existencia").Value = Connection.GetValue(oData, "aplica_existencia")
                .Add(New System.Data.SqlClient.SqlParameter("@aplica_pedido_fabrica", System.Data.SqlDbType.Bit, 1, "aplica_pedido_fabrica"))
                .Item("@aplica_pedido_fabrica").Value = Connection.GetValue(oData, "aplica_pedido_fabrica")

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicio", System.Data.SqlDbType.DateTime, 8, "fecha_inicio"))
                .Item("@fecha_inicio").Value = Connection.GetValue(oData, "fecha_inicio")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = Connection.GetValue(oData, "fecha_final")

                .Add(New System.Data.SqlClient.SqlParameter("@precio_contado", System.Data.SqlDbType.Float, 8, "precio_contado"))
                .Item("@precio_contado").Value = Connection.GetValue(oData, "precio_contado")

                .Add(New System.Data.SqlClient.SqlParameter("@precio_credito", System.Data.SqlDbType.Float, 8, "precio_credito"))
                .Item("@precio_credito").Value = Connection.GetValue(oData, "precio_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@agotar_existencias", System.Data.SqlDbType.Bit, 1, "agotar_existencias"))
                .Item("@agotar_existencias").Value = Connection.GetValue(oData, "agotar_existencias")
                .Add(New System.Data.SqlClient.SqlParameter("@autorizar", System.Data.SqlDbType.Bit, 1, "autorizar"))
                .Item("@autorizar").Value = Connection.GetValue(oData, "autorizar")
                .Add(New System.Data.SqlClient.SqlParameter("@solicita", System.Data.SqlDbType.VarChar, 15, "solicita"))
                .Item("@solicita").Value = Connection.GetValue(oData, "solicita")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 2000, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = Connection.GetValue(oData, "activo")


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_programados_del(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_programados_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = Connection.GetValue(oData, "folio_descuento")


            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_programados_valida_exs(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_programados_valida_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_creacion", System.Data.SqlDbType.DateTime, 8, "fecha_creacion"))
                .Item("@fecha_creacion").Value = Connection.GetValue(oData, "fecha_creacion")
            End With
            oEvent.Value = Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_programados_sel(ByVal folio_descuento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_programados_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_programados_ventas_sel(ByVal articulo As Long, ByVal plan_credito As Long, ByVal fecha_venta As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_programados_ventas_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_venta", System.Data.SqlDbType.DateTime, 8, "fecha_venta"))
                .Item("@fecha_venta").Value = fecha_venta

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_programados_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_programados_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
