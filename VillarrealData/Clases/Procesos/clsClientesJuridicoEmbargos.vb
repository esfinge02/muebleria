'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientesJuridicoEmbargos
'DATE:		30/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - ClientesJuridicoEmbargos"
Public Class clsClientesJuridicoEmbargos
    Public Function sp_clientes_juridico_embargos_ins(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_embargos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@embargo", System.Data.SqlDbType.Int, 4, "embargo"))
                .Item("@embargo").Value = Connection.GetValue(oData, "embargo")
                .Item("@embargo").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_mercancia", System.Data.SqlDbType.Text, 0, "descripcion_mercancia"))
                .Item("@descripcion_mercancia").Value = Connection.GetValue(oData, "descripcion_mercancia")
                .Add(New System.Data.SqlClient.SqlParameter("@valor_estimado", System.Data.SqlDbType.Money, 8, "valor_estimado"))
                .Item("@valor_estimado").Value = Connection.GetValue(oData, "valor_estimado")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "embargo", oCommand.Parameters.Item("@embargo").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_embargos_upd(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_embargos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@embargo", System.Data.SqlDbType.Int, 4, "embargo"))
                .Item("@embargo").Value = Connection.GetValue(oData, "embargo")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_mercancia", System.Data.SqlDbType.Text, 0, "descripcion_mercancia"))
                .Item("@descripcion_mercancia").Value = Connection.GetValue(oData, "descripcion_mercancia")
                .Add(New System.Data.SqlClient.SqlParameter("@valor_estimado", System.Data.SqlDbType.Money, 8, "valor_estimado"))
                .Item("@valor_estimado").Value = Connection.GetValue(oData, "valor_estimado")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_embargos_del(ByVal folio_juridico As Long, ByVal embargo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_embargos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@embargo", System.Data.SqlDbType.Int, 4, "embargo"))
                .Item("@embargo").Value = embargo

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_embargos_exs(ByVal folio_juridico As Long, ByVal embargo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_embargos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@embargo", System.Data.SqlDbType.Int, 4, "embargo"))
                .Item("@embargo").Value = embargo

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_embargos_sel(ByVal folio_juridico As Long, ByVal embargo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_embargos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@embargo", System.Data.SqlDbType.Int, 4, "embargo"))
                .Item("@embargo").Value = embargo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_embargos_grs(ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_embargos_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


