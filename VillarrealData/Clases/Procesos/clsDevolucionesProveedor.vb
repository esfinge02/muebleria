'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDevolucionesProveedor
'DATE:		23/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - DevolucionesProveedor"
Public Class clsDevolucionesProveedor
    Public Function sp_devoluciones_proveedor_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = Connection.GetValue(oData, "devolucion")
                .Item("@devolucion").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@conducto", System.Data.SqlDbType.VarChar, 80, "conducto"))
                .Item("@conducto").Value = Connection.GetValue(oData, "conducto")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.Nvarchar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "devolucion", oCommand.Parameters.Item("@devolucion").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_devoluciones_proveedor_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = Connection.GetValue(oData, "devolucion")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@conducto", System.Data.SqlDbType.VarChar, 80, "conducto"))
                .Item("@conducto").Value = Connection.GetValue(oData, "conducto")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.Nvarchar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_devoluciones_proveedor_del(ByVal bodega As String, ByVal devolucion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_devoluciones_proveedor_exs(ByVal bodega As String, ByVal devolucion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_devoluciones_proveedor_sel(ByVal bodega As String, ByVal devolucion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_devoluciones_proveedor_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_actualiza_enviado_devoluciones_proveedor(ByVal bodega As String, ByVal devolucion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_enviado_devoluciones_proveedor]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_orden_servicio_asignada_devolucion_ex(ByVal orden_servicio As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_orden_servicio_asignada_devolucion_ex]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_devoluciones_proveedor_actualiza_orden_servicio(ByVal orden_servicio As Long, ByVal folio_movimiento_entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_actualiza_orden_servicio]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@folio_movimiento_entrada", System.Data.SqlDbType.Int, 4, "folio_movimiento_entrada"))
                .Item("@folio_movimiento_entrada").Value = folio_movimiento_entrada

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_devoluciones_proveedor_actualiza_nota_aplicada(ByVal bodega As String, ByVal devolucion As Long, ByVal nota_aplicada As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_actualiza_nota_aplicada]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion

                .Add(New System.Data.SqlClient.SqlParameter("@nota_aplicada", System.Data.SqlDbType.Bit, 1, "nota_aplicada"))
                .Item("@nota_aplicada").Value = nota_aplicada

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_devoluciones_proveedor_actualiza_aplicado_inventario(ByVal bodega As String, ByVal devolucion As Long, ByVal aplicado_inventario As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_actualiza_aplicado_inventario]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion
                .Add(New System.Data.SqlClient.SqlParameter("@aplicado_inventario", System.Data.SqlDbType.Bit, 1, "aplicado_inventario"))
                .Item("@aplicado_inventario").Value = aplicado_inventario

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_devoluciones_proveedor_actualiza_enviado_proveedor(ByVal bodega As String, ByVal devolucion As Long, ByVal enviado_proveedor As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_devoluciones_proveedor_actualiza_enviado_proveedor]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@devolucion", System.Data.SqlDbType.Int, 4, "devolucion"))
                .Item("@devolucion").Value = devolucion
                .Add(New System.Data.SqlClient.SqlParameter("@enviado_proveedor", System.Data.SqlDbType.Bit, 1, "enviado_proveedor"))
                .Item("@enviado_proveedor").Value = enviado_proveedor

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


