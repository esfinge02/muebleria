'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsHisArticulosPrecios
'DATE:		09/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - HisArticulosPrecios"
Public Class clsHisArticulosPrecios
    Public Function sp_his_articulos_precios_ins(ByRef folio As Long, ByVal articulo As Long, ByVal precio As Long, ByVal importe_original As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_his_articulos_precios_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = 0
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
                .Item("@precio").Value = precio
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@importe").Value = importe_original
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio = oCommand.Parameters.Item("@folio").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_his_articulos_precios_upd(ByRef folio As Long, ByVal articulo As String, ByVal fecha_actualizado As Date, ByVal precio As Long, ByVal importe_original As Double, ByVal importe_actualizado As Double) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_his_articulos_precios_upd]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = folio
    '            .Item("@folio").Direction = ParameterDirection.InputOutput
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.VarChar, 20, "articulo"))
    '            .Item("@articulo").Value = articulo
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha_actualizado", System.Data.SqlDbType.Datetime, 8, "fecha_actualizado"))
    '            .Item("@fecha_actualizado").Value = fecha_actualizado
    '            .Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
    '            .Item("@precio").Value = precio
    '            .Add(New System.Data.SqlClient.SqlParameter("@importe_original", System.Data.SqlDbType.Money, 8, "importe_original"))
    '            .Item("@importe_original").Value = importe_original
    '            .Add(New System.Data.SqlClient.SqlParameter("@importe_actualizado", System.Data.SqlDbType.Money, 8, "importe_actualizado"))
    '            .Item("@importe_actualizado").Value = importe_actualizado
    '            .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
    '            .Item("@QUIEN").Value = Connection.User

    '        End With
    '        Connection.Execute(oCommand)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_his_articulos_precios_del(ByVal folio As Long, ByVal articulo As Long, ByVal fecha_actualizado As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_his_articulos_precios_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_actualizado", System.Data.SqlDbType.DateTime, 8, "fecha_actualizado"))
                .Item("@fecha_actualizado").Value = fecha_actualizado

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_his_articulos_precios_exs(ByVal folio As Long, ByVal articulo As Long, ByVal fecha_actualizado As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_his_articulos_precios_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_actualizado", System.Data.SqlDbType.DateTime, 8, "fecha_actualizado"))
                .Item("@fecha_actualizado").Value = fecha_actualizado

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_his_articulos_precios_sel(ByVal folio As Long, ByVal articulo As Long, ByVal fecha_actualizado As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_his_articulos_precios_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_actualizado", System.Data.SqlDbType.DateTime, 8, "fecha_actualizado"))
                .Item("@fecha_actualizado").Value = fecha_actualizado

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_his_articulos_precios_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_his_articulos_precios_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


