'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosInventariosDetalleSeries
'DATE:		15/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - MovimientosInventariosDetalleSeries"
Public Class clsMovimientosInventariosDetalleSeries
    Public Function sp_movimientos_inventarios_detalle_series_ins(ByRef oData As DataSet, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_series_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = Connection.GetValue(oData, "numero_serie")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_detalle_series_ins(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal numero_serie As String, ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_series_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_detalle_series_upd(ByRef oData As DataSet, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_series_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = Connection.GetValue(oData, "numero_serie")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_detalle_series_del(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal numero_serie As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_series_del]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_detalle_series_exs(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal numero_serie As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_series_exs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_detalle_series_sel(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal numero_serie As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_series_sel]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_detalle_series_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_series_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_detalle_series_repartos(ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal numero_serie As String, ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_detalle_series_repartos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.VarChar, 5, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_movimientos_inventarios_detalle_entradas_series_ins(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida_movimiento As Long, ByVal articulo As Long, ByVal entrada As Long, ByVal partida_entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_entradas_series_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida_movimiento", System.Data.SqlDbType.Int, 4, "partida_movimiento"))
                .Item("@partida_movimiento").Value = partida_movimiento
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida_entrada", System.Data.SqlDbType.Int, 4, "partida_entrada"))
                .Item("@partida_entrada").Value = partida_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_his_series_cambiar_bodega(ByVal articulo As Long, ByVal numero_serie As String, ByVal bodega As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_his_series_cambiar_bodega]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_detalle_series_elimina_mvc_del(ByVal articulo As Long, ByVal numero_serie As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_detalle_series_elimina_mvc_del]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region
