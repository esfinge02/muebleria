'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsSolicitudTraspasos
'DATE:		23/06/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - SolicitudTraspasos"
Public Class clsSolicitudTraspasos
    Public Function sp_solicitud_traspasos_ins(ByRef oData As DataSet, ByVal bodega_entrada As String, ByVal bodega_salida As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = Connection.GetValue(oData, "solicitud")
                .Item("@solicitud").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_salida", System.Data.SqlDbType.VarChar, 5, "bodega_salida"))
                .Item("@bodega_salida").Value = bodega_salida
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_entrada", System.Data.SqlDbType.Int, 4, "sucursal_entrada"))
                .Item("@sucursal_entrada").Value = Connection.GetValue(oData, "sucursal_entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = bodega_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@solicita", System.Data.SqlDbType.Int, 4, "solicita"))
                .Item("@solicita").Value = Connection.GetValue(oData, "solicita")
                .Add(New System.Data.SqlClient.SqlParameter("@capturo", System.Data.SqlDbType.VarChar, 100, "capturo"))
                .Item("@capturo").Value = Connection.GetValue(oData, "capturo")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_solicitud", System.Data.SqlDbType.DateTime, 8, "fecha_solicitud"))
                .Item("@fecha_solicitud").Value = Connection.GetValue(oData, "fecha_solicitud")
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 2, "estatus"))
                .Item("@estatus").Value = Connection.GetValue(oData, "estatus")

                .Add(New System.Data.SqlClient.SqlParameter("@Autorizo", System.Data.SqlDbType.Int, 4, "Autorizo"))
                .Item("@Autorizo").Value = Connection.GetValue(oData, "Autorizo")

                .Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.Text, 0, "Observaciones"))
                .Item("@Observaciones").Value = Connection.GetValue(oData, "Observaciones")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "solicitud", oCommand.Parameters.Item("@solicitud").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_solicitud_traspasos_upd(ByRef oData As DataSet, ByVal bodega_entrada As String, ByVal bodega_salida As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = Connection.GetValue(oData, "solicitud")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_salida", System.Data.SqlDbType.VarChar, 5, "bodega_salida"))
                .Item("@bodega_salida").Value = bodega_salida
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_entrada", System.Data.SqlDbType.Int, 4, "sucursal_entrada"))
                .Item("@sucursal_entrada").Value = Connection.GetValue(oData, "sucursal_entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = bodega_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@solicita", System.Data.SqlDbType.Int, 4, "solicita"))
                .Item("@solicita").Value = Connection.GetValue(oData, "solicita")
                .Add(New System.Data.SqlClient.SqlParameter("@capturo", System.Data.SqlDbType.VarChar, 100, "capturo"))
                .Item("@capturo").Value = Connection.GetValue(oData, "capturo")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_solicitud", System.Data.SqlDbType.DateTime, 8, "fecha_solicitud"))
                .Item("@fecha_solicitud").Value = Connection.GetValue(oData, "fecha_solicitud")
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 2, "estatus"))
                .Item("@estatus").Value = Connection.GetValue(oData, "estatus")

                .Add(New System.Data.SqlClient.SqlParameter("@Autorizo", System.Data.SqlDbType.Int, 4, "Autorizo"))
                .Item("@Autorizo").Value = Connection.GetValue(oData, "Autorizo")

                .Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.Text, 0, "Observaciones"))
                .Item("@Observaciones").Value = Connection.GetValue(oData, "Observaciones")


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_solicitud_traspasos_del(ByVal solicitud As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = solicitud

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_solicitud_traspasos_exs(ByVal solicitud As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = solicitud

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_solicitud_traspasos_sel(ByVal solicitud As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = solicitud

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_solicitud_traspasos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_solicitud_traspasos_grl(ByVal usuario As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = usuario

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_solicitud_traspasos_articulos(ByVal solicitud As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_articulos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = solicitud

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_solicitud_traspasos_cambiar_estatus(ByVal solicitud As Long, ByVal estatus As String, ByVal folio_traspaso As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_solicitud_traspasos_cambiar_estatus]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = solicitud
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 2, "estatus"))
                .Item("@estatus").Value = estatus
                .Add(New System.Data.SqlClient.SqlParameter("@folio_traspaso", System.Data.SqlDbType.Int, 4, "folio_traspaso"))
                .Item("@folio_traspaso").Value = folio_traspaso

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


