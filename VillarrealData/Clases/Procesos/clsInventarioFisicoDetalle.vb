'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsInventarioFisicoDetalle
'DATE:		08/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - InventarioFisicoDetalle"
Public Class clsInventarioFisicoDetalle
    Public Function sp_inventario_fisico_detalle_ins(ByVal inventario As Long, ByVal articulo As Long, ByVal conteo_1 As Double, ByVal conteo_2 As Double, ByVal contador As Long, ByVal supervisor As Long, ByVal ubicacion As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@conteo_1", System.Data.SqlDbType.Float, 8, "conteo_1"))
                .Item("@conteo_1").Value = conteo_1
                .Add(New System.Data.SqlClient.SqlParameter("@conteo_2", System.Data.SqlDbType.Float, 8, "conteo_2"))
                .Item("@conteo_2").Value = conteo_2
                .Add(New System.Data.SqlClient.SqlParameter("@contador", System.Data.SqlDbType.Int, 4, "contador"))
                .Item("@contador").Value = contador
                .Add(New System.Data.SqlClient.SqlParameter("@supervisor", System.Data.SqlDbType.Int, 4, "supervisor"))
                .Item("@supervisor").Value = supervisor
                .Add(New System.Data.SqlClient.SqlParameter("@ubicacion", System.Data.SqlDbType.VarChar, 15, "ubicacion"))
                .Item("@ubicacion").Value = ubicacion
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_inventario_fisico_detalle_upd(ByVal inventario As Long, ByVal articulo As Long, ByVal conteo_1 As Double, ByVal conteo_2 As Double) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_inventario_fisico_detalle_upd]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
    '            .Item("@inventario").Value = inventario
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = articulo
    '            .Add(New System.Data.SqlClient.SqlParameter("@conteo_1", System.Data.SqlDbType.Float, 8, "conteo_1"))
    '            .Item("@conteo_1").Value = conteo_1
    '            .Add(New System.Data.SqlClient.SqlParameter("@conteo_2", System.Data.SqlDbType.Float, 8, "conteo_2"))
    '            .Item("@conteo_2").Value = conteo_2
    '            .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
    '            .Item("@QUIEN").Value = Connection.User

    '        End With
    '        Connection.Execute(oCommand)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_inventario_fisico_detalle_del(ByVal inventario As Long, ByVal articulo As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_inventario_fisico_detalle_del]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
    '            .Item("@inventario").Value = inventario
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = articulo

    '        End With
    '        Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_inventario_fisico_detalle_exs(ByVal inventario As Long, ByVal articulo As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_inventario_fisico_detalle_exs]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
    '            .Item("@inventario").Value = inventario
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = articulo

    '        End With
    '        oEvent.Value = Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_inventario_fisico_detalle_sel(ByVal inventario As Long, ByVal articulo As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_inventario_fisico_detalle_sel]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
    '            .Item("@inventario").Value = inventario
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = articulo
    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_inventario_fisico_detalle_grs(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_detalle_grs_xubica(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_detalle_grs_xubica]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_detalle_listadoarticulos(ByVal articulos As String, ByVal conteo1 As Double, ByVal conteo2 As Double, ByVal contador As Long, ByVal supervisor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_detalle_listadoarticulos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@ARTICULOS", System.Data.SqlDbType.Text, 0))
                .Item("@ARTICULOS").Value = articulos
                .Add(New System.Data.SqlClient.SqlParameter("@conteo1", System.Data.SqlDbType.Float, 8))
                .Item("@conteo1").Value = conteo1
                .Add(New System.Data.SqlClient.SqlParameter("@conteo2", System.Data.SqlDbType.Float, 8))
                .Item("@conteo2").Value = conteo2
                .Add(New System.Data.SqlClient.SqlParameter("@contador", System.Data.SqlDbType.Int, 4))
                .Item("@contador").Value = contador
                .Add(New System.Data.SqlClient.SqlParameter("@supervisor", System.Data.SqlDbType.Int, 4))
                .Item("@supervisor").Value = supervisor
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_detalle_listadoarticulos_xubica(ByVal articulos As String, ByVal conteo1 As Double, ByVal conteo2 As Double, ByVal contador As Long, ByVal supervisor As Long, ByVal ubicacion As String, ByVal bodega As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_detalle_listadoarticulos_xubica]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@ARTICULOS", System.Data.SqlDbType.Text, 0))
                .Item("@ARTICULOS").Value = articulos
                .Add(New System.Data.SqlClient.SqlParameter("@conteo1", System.Data.SqlDbType.Float, 8))
                .Item("@conteo1").Value = conteo1
                .Add(New System.Data.SqlClient.SqlParameter("@conteo2", System.Data.SqlDbType.Float, 8))
                .Item("@conteo2").Value = conteo2
                .Add(New System.Data.SqlClient.SqlParameter("@contador", System.Data.SqlDbType.Int, 4))
                .Item("@contador").Value = contador
                .Add(New System.Data.SqlClient.SqlParameter("@supervisor", System.Data.SqlDbType.Int, 4))
                .Item("@supervisor").Value = supervisor
                .Add(New System.Data.SqlClient.SqlParameter("@ubicaciones", System.Data.SqlDbType.Text, 0))
                .Item("@ubicaciones").Value = ubicacion
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5))
                .Item("@bodega").Value = bodega
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


