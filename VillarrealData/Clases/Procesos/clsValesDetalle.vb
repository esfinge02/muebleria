Imports Dipros.Utils

Public Class clsValesDetalle

    Public Function sp_vales_detalle_ins(ByVal folio As Long, ByVal sucursal_recibo As String, ByVal concepto_recibo As String, ByVal serie_recibo As String, ByVal folio_recibo As Long, ByVal fecha As DateTime, ByVal importe As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_detalle_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_recibo", System.Data.SqlDbType.Char, 10, "sucursal_recibo"))
                .Item("@sucursal_recibo").Value = sucursal_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_recibo", System.Data.SqlDbType.VarChar, 5, "concepto_recibo"))
                .Item("@concepto_recibo").Value = concepto_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.VarChar, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vales_detalle_upd(ByVal folio As Long, ByVal sucursal_recibo As String, ByVal concepto_recibo As String, ByVal serie_recibo As String, ByVal folio_recibo As Long, ByVal fecha As DateTime, ByVal importe As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_detalle_upd]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_recibo", System.Data.SqlDbType.Char, 10, "sucursal_recibo"))
                .Item("@sucursal_recibo").Value = sucursal_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_recibo", System.Data.SqlDbType.VarChar, 5, "concepto_recibo"))
                .Item("@concepto_recibo").Value = concepto_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.VarChar, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vales_detalle_del(ByVal folio As Long, ByVal sucursal_recibo As String, ByVal concepto_recibo As String, ByVal serie_recibo As String, ByVal folio_recibo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_recibo", System.Data.SqlDbType.Char, 10, "sucursal_recibo"))
                .Item("@sucursal_recibo").Value = sucursal_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_recibo", System.Data.SqlDbType.VarChar, 5, "concepto_recibo"))
                .Item("@concepto_recibo").Value = concepto_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.VarChar, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo


            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vales_detalle_grs(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
