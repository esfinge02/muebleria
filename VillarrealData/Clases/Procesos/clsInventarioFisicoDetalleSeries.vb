'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsInventarioFisicoDetalleSeries
'DATE:		08/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - InventarioFisicoDetalleSeries"
Public Class clsInventarioFisicoDetalleSeries
    Public Function sp_inventario_fisico_detalle_series_ins(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_detalle_series_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_inventario_fisico_detalle_series_upd(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_inventario_fisico_detalle_series_upd]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
    '            .Item("@inventario").Value = inventario
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = articulo
    '            .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
    '            .Item("@numero_serie").Value = numero_serie
    '            .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
    '            .Item("@QUIEN").Value = Connection.User

    '        End With
    '        Connection.Execute(oCommand)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_inventario_fisico_detalle_series_del(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_inventario_fisico_detalle_series_del]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
    '            .Item("@inventario").Value = inventario
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = articulo
    '            .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
    '            .Item("@numero_serie").Value = numero_serie

    '        End With
    '        Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_inventario_fisico_detalle_series_exs(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_inventario_fisico_detalle_series_exs]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
    '            .Item("@inventario").Value = inventario
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = articulo
    '            .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
    '            .Item("@numero_serie").Value = numero_serie

    '        End With
    '        oEvent.Value = Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_inventario_fisico_detalle_series_sel(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_inventario_fisico_detalle_series_sel]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
    '            .Item("@inventario").Value = inventario
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = articulo
    '            .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
    '            .Item("@numero_serie").Value = numero_serie

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_inventario_fisico_detalle_series_grs(ByVal inventario As Long, ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_detalle_series_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


