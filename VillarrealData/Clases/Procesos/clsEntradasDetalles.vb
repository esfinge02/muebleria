'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradasDetalles
'DATE:		09/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - EntradasDetalles"
Public Class clsEntradasDetalles
    Public Function sp_entradas_detalles_ins(ByRef oData As DataSet, ByVal entrada As Long, ByVal bodega As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_detalles_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
                .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@costo").Value = Connection.GetValue(oData, "costo")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_historico_costo", System.Data.SqlDbType.Int, 4, "folio_historico_costo"))
                .Item("@folio_historico_costo").Value = Connection.GetValue(oData, "folio_historico_costo")
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = Connection.GetValue(oData, "orden")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@sobre_pedido", System.Data.SqlDbType.Bit, 1, "sobre_pedido"))
                .Item("@sobre_pedido").Value = Connection.GetValue(oData, "sobre_pedido")

                .Add(New System.Data.SqlClient.SqlParameter("@costo_flete", System.Data.SqlDbType.Money, 8, "costo_flete"))
                .Item("@costo_flete").Value = Connection.GetValue(oData, "costo_flete")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                If IsDBNull(Connection.GetValue(oData, "precio_lista")) Then
                    .Add(New System.Data.SqlClient.SqlParameter("@precio_lista", System.Data.SqlDbType.Money, 8, "precio_lista"))
                    .Item("@precio_lista").Value = 0
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@precio_lista", System.Data.SqlDbType.Money, 8, "precio_lista"))
                    .Item("@precio_lista").Value = Connection.GetValue(oData, "precio_lista")
                End If
                .Add(New System.Data.SqlClient.SqlParameter("@bprecio_lista", System.Data.SqlDbType.Bit, 1, "bprecio_lista"))
                .Item("@bprecio_lista").Value = Connection.GetValue(oData, "bprecio_lista")
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_detalles_upd(ByRef oData As DataSet, ByVal entrada As Long, ByVal bodega As String, ByVal costear As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_detalles_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
                .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@costo").Value = Connection.GetValue(oData, "costo")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_historico_costo", System.Data.SqlDbType.Int, 4, "folio_historico_costo"))
                .Item("@folio_historico_costo").Value = Connection.GetValue(oData, "folio_historico_costo")
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = Connection.GetValue(oData, "orden")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@sobre_pedido", System.Data.SqlDbType.Bit, 1, "sobre_pedido"))
                .Item("@sobre_pedido").Value = Connection.GetValue(oData, "sobre_pedido")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad_anterior", System.Data.SqlDbType.Float, 8, "cantidad_anterior"))
                .Item("@cantidad_anterior").Value = Connection.GetValue(oData, "cantidad_anterior")
                .Add(New System.Data.SqlClient.SqlParameter("@costear", System.Data.SqlDbType.Bit, 1, "costear"))
                .Item("@costear").Value = costear

                .Add(New System.Data.SqlClient.SqlParameter("@costo_flete", System.Data.SqlDbType.Money, 8, "costo_flete"))
                .Item("@costo_flete").Value = Connection.GetValue(oData, "costo_flete")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                If IsDBNull(Connection.GetValue(oData, "precio_lista")) Then
                    .Add(New System.Data.SqlClient.SqlParameter("@precio_lista", System.Data.SqlDbType.Money, 8, "precio_lista"))
                    .Item("@precio_lista").Value = 0
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@precio_lista", System.Data.SqlDbType.Money, 8, "precio_lista"))
                    .Item("@precio_lista").Value = Connection.GetValue(oData, "precio_lista")
                End If
                .Add(New System.Data.SqlClient.SqlParameter("@bprecio_lista", System.Data.SqlDbType.Bit, 1, "bprecio_lista"))
                .Item("@bprecio_lista").Value = Connection.GetValue(oData, "bprecio_lista")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_detalles_del(ByVal entrada As Long, ByVal partida As Long, ByVal cantidad As Long, ByVal articulo As String, ByVal orden As Long, ByVal bodega As String, ByVal sobre_pedido As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_detalles_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
                .Item("@cantidad").Value = cantidad
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = orden
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@sobre_pedido", System.Data.SqlDbType.Bit, 1, "sobre_pedido"))
                .Item("@sobre_pedido").Value = sobre_pedido
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_detalles_exs(ByVal entrada As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_detalles_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_detalles_sel(ByVal entrada As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_detalles_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_detalles_grs(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_detalles_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_detalle_orden_compra_grs(ByVal orden As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_detalle_orden_compra_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = orden
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_detalles_checar_series(ByVal entrada As Long, ByVal partida As Long, ByVal articulo As String, ByVal cantidad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_detalles_checar_series]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
                .Item("@cantidad").Value = cantidad
            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
End Class
#End Region


