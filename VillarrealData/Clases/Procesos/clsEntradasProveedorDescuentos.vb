'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradasProveedorDescuentos
'DATE:		14/04/2007 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - EntradasProveedorDescuentos"
Public Class clsEntradasProveedorDescuentos
    Public Function sp_entradas_proveedor_descuentos_ins(ByVal entrada As Long, ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_proveedor_descuentos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada 'Connection.GetValue(oData, "entrada")
                '.Item("@entrada").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
                .Item("@descuento").Value = Connection.GetValue(oData, "descuento")
                .Item("@descuento").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje", System.Data.SqlDbType.Decimal, 9, "porcentaje"))
                .Item("@porcentaje").Value = Connection.GetValue(oData, "porcentaje")

                .Add(New System.Data.SqlClient.SqlParameter("@antes_iva", System.Data.SqlDbType.Bit, 1, "antes_iva"))
                .Item("@antes_iva").Value = Connection.GetValue(oData, "antes_iva")
                .Add(New System.Data.SqlClient.SqlParameter("@pronto_pago", System.Data.SqlDbType.Bit, 1, "pronto_pago"))
                .Item("@pronto_pago").Value = Connection.GetValue(oData, "pronto_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@importe_descuento", System.Data.SqlDbType.Money, 8, "importe_descuento"))
                .Item("@importe_descuento").Value = Connection.GetValue(oData, "importe_descuento")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            'descuento = oCommand.Parameters.Item("@descuento").Value
            'Connection.SetValue(oData, "entrada", oCommand.Parameters.Item("@entrada").Value)
            Connection.SetValue(oData, "descuento", oCommand.Parameters.Item("@descuento").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_proveedor_descuentos_upd(ByVal entrada As Long, ByVal oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_proveedor_descuentos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada 'Connection.GetValue(oData, "entrada")
                '.Item("@entrada").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
                .Item("@descuento").Value = Connection.GetValue(oData, "descuento")
                '.Item("@descuento").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje", System.Data.SqlDbType.Decimal, 9, "porcentaje"))
                .Item("@porcentaje").Value = Connection.GetValue(oData, "porcentaje")

                .Add(New System.Data.SqlClient.SqlParameter("@antes_iva", System.Data.SqlDbType.Bit, 1, "antes_iva"))
                .Item("@antes_iva").Value = Connection.GetValue(oData, "antes_iva")
                .Add(New System.Data.SqlClient.SqlParameter("@pronto_pago", System.Data.SqlDbType.Bit, 1, "pronto_pago"))
                .Item("@pronto_pago").Value = Connection.GetValue(oData, "pronto_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@importe_descuento", System.Data.SqlDbType.Money, 8, "importe_descuento"))
                .Item("@importe_descuento").Value = Connection.GetValue(oData, "importe_descuento")


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_proveedor_descuentos_del(ByVal entrada As Long, ByVal oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_proveedor_descuentos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
                .Item("@descuento").Value = Connection.GetValue(oData, "descuento")
                '.Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
                '.Item("@descuento").Value = descuento

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_proveedor_descuentos_exs(ByVal entrada As Long, ByVal descuento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_proveedor_descuentos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
                .Item("@descuento").Value = descuento

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_proveedor_descuentos_sel(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_proveedor_descuentos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_proveedor_descuentos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_proveedor_descuentos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


