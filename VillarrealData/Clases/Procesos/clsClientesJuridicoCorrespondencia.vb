'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientesJuridicoCorrespondencia
'DATE:		18/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - ClientesJuridicoCorrespondencia"
Public Class clsClientesJuridicoCorrespondencia
    Public Function sp_clientes_juridico_correspondencia_ins(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_correspondencia_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico_correspondencia", System.Data.SqlDbType.Int, 4, "folio_juridico_correspondencia"))
                .Item("@folio_juridico_correspondencia").Value = Connection.GetValue(oData, "folio_juridico_correspondencia")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_correspondencia", System.Data.SqlDbType.Text, 0, "descripcion_correspondencia"))
                .Item("@descripcion_correspondencia").Value = Connection.GetValue(oData, "descripcion_correspondencia")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@correspondencia", System.Data.SqlDbType.Int, 4, "correspondencia"))
                .Item("@correspondencia").Value = Connection.GetValue(oData, "correspondencia")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_correspondencia_upd(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_correspondencia_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico_correspondencia", System.Data.SqlDbType.Int, 4, "folio_juridico_correspondencia"))
                .Item("@folio_juridico_correspondencia").Value = Connection.GetValue(oData, "folio_juridico_correspondencia")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_correspondencia", System.Data.SqlDbType.Text, 0, "descripcion_correspondencia"))
                .Item("@descripcion_correspondencia").Value = Connection.GetValue(oData, "descripcion_correspondencia")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@correspondencia", System.Data.SqlDbType.Int, 4, "correspondencia"))
                .Item("@correspondencia").Value = Connection.GetValue(oData, "correspondencia")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_correspondencia_del(ByVal folio_juridico As Long, ByVal folio_juridico_correspondencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_correspondencia_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico_correspondencia", System.Data.SqlDbType.Int, 4, "folio_juridico_correspondencia"))
                .Item("@folio_juridico_correspondencia").Value = folio_juridico_correspondencia

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_correspondencia_exs(ByVal folio_juridico As Long, ByVal folio_juridico_correspondencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_correspondencia_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico_correspondencia", System.Data.SqlDbType.Int, 4, "folio_juridico_correspondencia"))
                .Item("@folio_juridico_correspondencia").Value = folio_juridico_correspondencia

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_correspondencia_sel(ByVal folio_juridico As Long, ByVal folio_juridico_correspondencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_correspondencia_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico_correspondencia", System.Data.SqlDbType.Int, 4, "folio_juridico_correspondencia"))
                .Item("@folio_juridico_correspondencia").Value = folio_juridico_correspondencia

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_correspondencia_grs(ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_correspondencia_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


