'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosCobrar
'DATE:		19/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - MovimientosCobrar"
Public Class clsMovimientosCobrar
    Public Function sp_movimientos_cobrar_ins(ByRef oData As DataSet, ByVal genera_comision_a_cobrador As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = Connection.GetValue(oData, "serie")
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
                .Item("@caja").Value = Connection.GetValue(oData, "caja")
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = Connection.GetValue(oData, "cajero")
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = Connection.GetValue(oData, "cobrador")
                .Add(New System.Data.SqlClient.SqlParameter("@documentos", System.Data.SqlDbType.Int, 4, "documentos"))
                .Item("@documentos").Value = Connection.GetValue(oData, "documentos")
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = Connection.GetValue(oData, "cargo")
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = Connection.GetValue(oData, "abono")
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = Connection.GetValue(oData, "total")
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = Connection.GetValue(oData, "saldo")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                'If Connection.GetValue(oData, "fecha_cancelacion") = CDate("01/01/1900") Then
                '    .Item("@fecha_cancelacion").Value = System.DBNull.Value
                'Else
                .Item("@fecha_cancelacion").Value = Connection.GetValue(oData, "fecha_cancelacion")
                'End If
                .Add(New System.Data.SqlClient.SqlParameter("@genera_comision_a_cobrador", System.Data.SqlDbType.Bit, 1, "genera_comision_a_cobrador"))
                .Item("@genera_comision_a_cobrador").Value = Connection.GetValue(oData, "genera_comision_a_cobrador")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 300, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = Connection.GetValue(oData, "estatus")
                .Add(New System.Data.SqlClient.SqlParameter("@genera_comision_a_cobrador", System.Data.SqlDbType.Bit, 1, "genera_comision_a_cobrador"))
                .Item("@genera_comision_a_cobrador").Value = genera_comision_a_cobrador

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_ins(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal partida As Long, _
    ByVal fecha As Date, ByVal caja As Long, ByVal cajero As Long, ByVal cobrador As Long, ByVal documentos As Long, ByVal cargo As Double, ByVal abono As Double, ByVal subtotal As Double, ByVal impuesto As Double, ByVal total As Double, ByVal saldo As Double, _
    ByVal fecha_cancelacion As Date, ByVal observaciones As String, ByVal estatus As String, ByVal genera_comision_a_cobrador As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
                .Item("@caja").Value = caja
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@documentos", System.Data.SqlDbType.Int, 4, "documentos"))
                .Item("@documentos").Value = documentos
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = cargo
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = abono
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = subtotal
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                .Item("@impuesto").Value = impuesto
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = total
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = saldo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                If fecha_cancelacion = CDate("01/01/1900").Date Then
                    .Item("@fecha_cancelacion").Value = System.DBNull.Value
                Else
                    .Item("@fecha_cancelacion").Value = fecha_cancelacion
                End If
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 300, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = estatus
                .Add(New System.Data.SqlClient.SqlParameter("@genera_comision_a_cobrador", System.Data.SqlDbType.Bit, 1, "genera_comision_a_cobrador"))
                .Item("@genera_comision_a_cobrador").Value = genera_comision_a_cobrador

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_cajas_ins(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal cliente As Long, ByVal partida As Long, _
       ByVal fecha As Date, ByVal caja As Long, ByVal cajero As Object, ByVal cobrador As Object, ByVal documentos As Long, ByVal cargo As Double, ByVal abono As Double, ByVal subtotal As Double, ByVal impuesto As Double, ByVal total As Double, ByVal saldo As Double, _
       ByVal fecha_cancelacion As Object, ByVal observaciones As String, ByVal estatus As String, ByRef folio As Long, ByVal sucursal_actual As Long, ByVal TieneIvaDesglosadoFacturaNotaCargo As Boolean, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_cajas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
                .Item("@caja").Value = caja
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@documentos", System.Data.SqlDbType.Int, 4, "documentos"))
                .Item("@documentos").Value = documentos
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = cargo
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = abono
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = subtotal
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                .Item("@impuesto").Value = impuesto
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = total
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = saldo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))

                If Not fecha_cancelacion Is System.DBNull.Value Then
                    If fecha_cancelacion = CDate("01/01/1900").Date Then
                        .Item("@fecha_cancelacion").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_cancelacion").Value = fecha_cancelacion
                    End If
                Else
                    .Item("@fecha_cancelacion").Value = fecha_cancelacion
                End If

                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 300, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = estatus
                '.Add(New System.Data.SqlClient.SqlParameter("@genera_comision_a_cobrador", System.Data.SqlDbType.Bit, 1, "genera_comision_a_cobrador"))
                '.Item("@genera_comision_a_cobrador").Value = genera_comision_a_cobrador

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual

                .Add(New System.Data.SqlClient.SqlParameter("@TieneIvaDesglosadoFacturaNotaCargo", System.Data.SqlDbType.Bit, 1, "TieneIvaDesglosadoFacturaNotaCargo"))
                .Item("@TieneIvaDesglosadoFacturaNotaCargo").Value = TieneIvaDesglosadoFacturaNotaCargo

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            Connection.Execute(oCommand)
            folio = oCommand.Parameters.Item("@folio").Value
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_upd(ByRef oData As DataSet, ByVal genera_comision_a_cobrador As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = Connection.GetValue(oData, "serie")
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
                .Item("@caja").Value = Connection.GetValue(oData, "caja")
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = Connection.GetValue(oData, "cajero")
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = Connection.GetValue(oData, "cobrador")
                .Add(New System.Data.SqlClient.SqlParameter("@documentos", System.Data.SqlDbType.Int, 4, "documentos"))
                .Item("@documentos").Value = Connection.GetValue(oData, "documentos")
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = Connection.GetValue(oData, "cargo")
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = Connection.GetValue(oData, "abono")
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = Connection.GetValue(oData, "total")
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = Connection.GetValue(oData, "saldo")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = Connection.GetValue(oData, "fecha_cancelacion")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 300, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = Connection.GetValue(oData, "estatus")
                .Add(New System.Data.SqlClient.SqlParameter("@genera_comision_a_cobrador", System.Data.SqlDbType.Bit, 1, "genera_comision_a_cobrador"))
                .Item("@genera_comision_a_cobrador").Value = genera_comision_a_cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_del(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_exs(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_sel(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_saldos_con_mov_referencia(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_saldos_con_mov_referencia ]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_ventas_sel(ByVal sucursal As Long, ByVal cliente As String, ByVal tipo_movimiento As Char, ByVal fecha As Date, ByVal abono_proximo As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_ventas_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_movimiento", System.Data.SqlDbType.Char, 1, "tipo_movimiento"))
                .Item("@tipo_movimiento").Value = tipo_movimiento
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@abono_proximo", System.Data.SqlDbType.Bit, 1, "abono_proximo"))
                .Item("@abono_proximo").Value = abono_proximo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_movimientos_detalle_sel(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_movimientos_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_movimientos_sel(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_movimientos_sel]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_movimientos_filtro_sel(ByVal cliente As Long, ByVal saldo As Boolean, ByVal sucursal As Boolean, ByVal numero_sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_movimientos_filtro_sel]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Bit, 1, "saldo"))
                .Item("@saldo").Value = saldo
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Bit, 1, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@numero_sucursal", System.Data.SqlDbType.Int, 4, "numero_sucursal"))
                .Item("@numero_sucursal").Value = numero_sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_saldos_movimientos_cliente(ByVal cliente As Long, ByVal SaldoMayorCero As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_saldos_movimientos_cliente]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Bit, 1, "saldo"))
                .Item("@saldo").Value = SaldoMayorCero


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_notas_credito_grl(ByVal serie As String, ByVal concepto_nota_credito As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_notas_credito_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_nota_credito", System.Data.SqlDbType.VarChar, 5, "concepto_nota_credito"))
                .Item("@concepto_nota_credito").Value = concepto_nota_credito

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_notas_cargo_grl(ByVal serie As String, ByVal concepto_nota_cargo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_notas_cargo_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_nota_cargo", System.Data.SqlDbType.VarChar, 5, "concepto_nota_cargo"))
                .Item("@concepto_nota_cargo").Value = concepto_nota_cargo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_cancelar_nota_credito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal fecha_cancelacion As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_cancelar_nota_credito]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = fecha_cancelacion
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cobrar_cancelar_nota_cargo(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal fecha_cancelacion As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_cancelar_nota_cargo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = fecha_cancelacion
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_movimientos_cobrar_buscar_abono(ByVal sucursal As Long, ByVal cliente As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_buscar_abono]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie_abono", System.Data.SqlDbType.Char, 3, "serie_abono"))
                .Item("@serie_abono").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio_abono", System.Data.SqlDbType.Int, 4, "folio_abono"))
                .Item("@folio_abono").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_movimientos_cobrar_buscar_abono_reimpresion(ByVal sucursal As Long, ByVal cliente As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_buscar_abono_reimpresion]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie_abono", System.Data.SqlDbType.Char, 3, "serie_abono"))
                .Item("@serie_abono").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio_abono", System.Data.SqlDbType.Int, 4, "folio_abono"))
                .Item("@folio_abono").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bonificaciones_ins(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As DateTime, ByVal cliente As Long, ByVal tipo As Char, ByVal importe As Long, ByVal observaciones As String, ByVal serie_recibo As String, ByVal folio_recibo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bonificaciones_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
                .Item("@caja").Value = caja
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 300, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_notas_cargo_por_cliente_grs(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_notas_cargo_por_cliente_grs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_cliente_sel(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cliente_sel]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_existe_enganches_vencidos_exs(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_existe_enganches_vencidos_exs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_condonacion_iva_sel(ByVal sucursal As Long, ByVal mes As Int32, ByVal anio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_condonacion_iva_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@mes", System.Data.SqlDbType.Int, 4, "mes"))
                .Item("@mes").Value = mes
                .Add(New System.Data.SqlClient.SqlParameter("@anio", System.Data.SqlDbType.Int, 4, "anio"))
                .Item("@anio").Value = anio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_notas_cargo_generales_exs(ByVal sucursal As Long, ByVal fecha As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_notas_cargo_generales_exs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_notas_cargo_generales_ins(ByVal sucursal As Long, ByVal fecha As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_notas_cargo_generales_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function sp_recibo_abono_cfdi_ins(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal tipo As String, ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_recibo_abono_cfdi_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto

                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = tipo

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_nota_de_credito_cfdi_ins(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal iva_desglosado As Long, ByVal sucursal_actual As Long, ByVal imprimir_nota As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_nota_de_credito_cfdi_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto

                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

                .Add(New System.Data.SqlClient.SqlParameter("@iva_desglosado", System.Data.SqlDbType.Bit, 1, "iva_desglosado"))
                .Item("@iva_desglosado").Value = iva_desglosado

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual

                .Add(New System.Data.SqlClient.SqlParameter("@imprimir_nota", System.Data.SqlDbType.Bit, 1, "imprimir_nota"))
                .Item("@imprimir_nota").Value = imprimir_nota

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_nota_de_cargo_cfdi_ins(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal iva_desglosado As Boolean, ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_nota_de_cargo_cfdi_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto

                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1, "ivadesglosado"))
                .Item("@ivadesglosado").Value = iva_desglosado

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual


            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_nota_de_credito_vale_cfdi_ins(ByVal sucursal As Long, ByVal folio_vale As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_nota_de_credito_vale_cfdi_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@folio_vale", System.Data.SqlDbType.Int, 4, "folio_vale"))
                .Item("@folio_vale").Value = folio_vale

                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = Connection.User

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_reimprime_recibos(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_reimprime_recibos]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto


                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_movimientos_cobrar_actualizar_observaciones(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal observaciones As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_actualizar_observaciones]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_abonos", System.Data.SqlDbType.VarChar, 300, "observaciones_abonos"))
                .Item("@observaciones_abonos").Value = observaciones


            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cancela_cfdi(ByVal serie As String, ByVal folio As Long, ByVal folio_electronico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cancela_cfdi]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@facturaId", System.Data.SqlDbType.Int, 4, "facturaId"))
                .Item("@facturaId").Value = folio_electronico

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
End Class
#End Region


