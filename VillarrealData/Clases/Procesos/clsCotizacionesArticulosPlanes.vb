'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCotizacionesArticulosPlanes
'DATE:		31/08/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - CotizacionesArticulosPlanes"
Public Class clsCotizacionesArticulosPlanes
    Public Function sp_cotizaciones_articulos_planes_ins(ByVal cotizacion As Long, ByVal articulo As Long, ByVal plan_credito As Long, ByVal importe_venta As Double, ByVal total As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_articulos_planes_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito
                .Add(New System.Data.SqlClient.SqlParameter("@importe_venta", System.Data.SqlDbType.Money, 8, "importe_venta"))
                .Item("@importe_venta").Value = importe_venta
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = total
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_articulos_planes_upd(ByVal cotizacion As Long, ByVal articulo As Long, ByVal plan_credito As Long, ByVal importe_venta As Double, ByVal total As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_articulos_planes_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito
                .Add(New System.Data.SqlClient.SqlParameter("@importe_venta", System.Data.SqlDbType.Money, 8, "importe_venta"))
                .Item("@importe_venta").Value = importe_venta
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = total
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_articulos_planes_del(ByVal cotizacion As Long, ByVal articulo As Long, ByVal plan_credito As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_articulos_planes_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_articulos_planes_exs(ByVal cotizacion As Long, ByVal articulo As Long, ByVal plan_credito As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_articulos_planes_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_articulos_planes_sel(ByVal cotizacion As Long, ByVal articulo As Long, ByVal plan_credito As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_articulos_planes_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_articulos_planes_grs(ByVal cotizacion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_articulos_planes_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


