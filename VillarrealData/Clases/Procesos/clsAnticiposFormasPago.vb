Imports Dipros.Utils

Public Class clsAnticiposFormasPago
    Public Function sp_anticipos_formas_pago_ins(ByVal folio As Long, ByRef partida As Long, ByVal forma_pago As Long, ByVal tipo_cambio As Double, ByVal dolares As Long, ByVal importe As Double, ByVal ultimos_digitos_cuenta As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_formas_pago_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Item("@partida").Direction = ParameterDirection.InputOutput


                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cambio", System.Data.SqlDbType.Decimal, 9, "tipo_cambio"))
                .Item("@tipo_cambio").Value = tipo_cambio

                .Add(New System.Data.SqlClient.SqlParameter("@dolares", System.Data.SqlDbType.Int, 4, "dolares"))
                .Item("@dolares").Value = dolares

                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@ultimos_digitos_cuenta", System.Data.SqlDbType.VarChar, 4, "ultimos_digitos_cuenta"))
                .Item("@ultimos_digitos_cuenta").Value = ultimos_digitos_cuenta


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            partida = oCommand.Parameters.Item("@partida").Value
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent


    End Function

    Public Function sp_anticipos_formas_pago_upd(ByVal folio As Long, ByVal partida As Long, ByVal forma_pago As Long, ByVal tipo_cambio As Double, ByVal dolares As Long, ByVal importe As Double, ByVal ultimos_digitos_cuenta As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_formas_pago_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cambio", System.Data.SqlDbType.Decimal, 9, "tipo_cambio"))
                .Item("@tipo_cambio").Value = tipo_cambio

                .Add(New System.Data.SqlClient.SqlParameter("@dolares", System.Data.SqlDbType.Int, 4, "dolares"))
                .Item("@dolares").Value = dolares

                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@ultimos_digitos_cuenta", System.Data.SqlDbType.VarChar, 4, "ultimos_digitos_cuenta"))
                .Item("@ultimos_digitos_cuenta").Value = ultimos_digitos_cuenta

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent


    End Function

    Public Function sp_anticipos_formas_pago_del(ByVal folio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_formas_pago_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_anticipos_formas_pago_sel(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_formas_pago_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
