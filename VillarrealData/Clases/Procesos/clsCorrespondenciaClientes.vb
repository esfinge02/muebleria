'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCorrespondenciaClientes
'DATE:		01/02/2007 00:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - CorrespondenciaClientes"
Public Class clsCorrespondenciaClientes
    Public Function sp_correspondencia_clientes_ins(ByRef oData As DataSet, ByVal texto_correspondencia As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_correspondencia_clientes_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@correspondencia", System.Data.SqlDbType.Int, 4, "correspondencia"))
                .Item("@correspondencia").Value = Connection.GetValue(oData, "correspondencia")
                .Item("@correspondencia").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 80, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@texto_correspondencia", System.Data.SqlDbType.Text, 0, "texto_correspondencia"))
                .Item("@texto_correspondencia").Value = texto_correspondencia 'Connection.GetValue(oData, "texto_correspondencia")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "correspondencia", oCommand.Parameters.Item("@correspondencia").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_correspondencia_clientes_upd(ByRef oData As DataSet, ByVal texto_correspondencia As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_correspondencia_clientes_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@correspondencia", System.Data.SqlDbType.Int, 4, "correspondencia"))
                .Item("@correspondencia").Value = Connection.GetValue(oData, "correspondencia")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 80, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@texto_correspondencia", System.Data.SqlDbType.Text, 0, "texto_correspondencia"))
                .Item("@texto_correspondencia").Value = texto_correspondencia 'Connection.GetValue(oData, "texto_correspondencia")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_correspondencia_clientes_del(ByVal correspondencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_correspondencia_clientes_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@correspondencia", System.Data.SqlDbType.Int, 4, "correspondencia"))
                .Item("@correspondencia").Value = correspondencia

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_correspondencia_clientes_exs(ByVal correspondencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_correspondencia_clientes_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@correspondencia", System.Data.SqlDbType.Int, 4, "correspondencia"))
                .Item("@correspondencia").Value = correspondencia

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_correspondencia_clientes_sel(ByVal correspondencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_correspondencia_clientes_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@correspondencia", System.Data.SqlDbType.Int, 4, "correspondencia"))
                .Item("@correspondencia").Value = correspondencia

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_correspondencia_clientes_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_correspondencia_clientes_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_correspondencia_clientes_lkp() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_correspondencia_clientes_lkp]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


