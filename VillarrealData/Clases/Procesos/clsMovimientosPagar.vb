'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosPagar
'DATE:		20/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - MovimientosPagar"
Public Class clsMovimientosPagar
 

    Public Function sp_movimientos_pagar_ins(ByRef oData As DataSet, ByVal tipo_pago As Object, ByVal cargo As Object, ByVal abono As Object, ByVal banco As Object, ByVal status As String, ByVal abono_cuenta As Boolean, ByVal folio_poliza_cheque As Long, Optional ByVal folio_pago_proveedores As Long = 0) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
                .Item("@concepto").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = Connection.GetValue(oData, "entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_vencimiento", System.Data.SqlDbType.DateTime, 8, "fecha_vencimiento"))
                .Item("@fecha_vencimiento").Value = Connection.GetValue(oData, "fecha_vencimiento")

                .Add(New System.Data.SqlClient.SqlParameter("@plazo", System.Data.SqlDbType.Int, 4, "plazo"))
                .Item("@plazo").Value = Connection.GetValue(oData, "plazo")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = Connection.GetValue(oData, "concepto_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = Connection.GetValue(oData, "folio_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@es_padre", System.Data.SqlDbType.Bit, 1, "es_padre"))
                .Item("@es_padre").Value = Connection.GetValue(oData, "es_padre")
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = cargo
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = abono
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = Connection.GetValue(oData, "iva")
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = Connection.GetValue(oData, "total")
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = Connection.GetValue(oData, "saldo")
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
                .Item("@tipo_pago").Value = tipo_pago
                .Add(New System.Data.SqlClient.SqlParameter("@referencia_transferencia", System.Data.SqlDbType.VarChar, 50, "referencia_transferencia"))
                .Item("@referencia_transferencia").Value = Connection.GetValue(oData, "referencia_transferencia")
                .Add(New System.Data.SqlClient.SqlParameter("@cheque", System.Data.SqlDbType.Int, 4, "cheque"))
                .Item("@cheque").Value = Connection.GetValue(oData, "cheque")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = Connection.GetValue(oData, "cuenta_bancaria")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@formulo", System.Data.SqlDbType.VarChar, 30, "formulo"))
                .Item("@formulo").Value = Connection.GetValue(oData, "formulo")
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 30, "autorizo"))
                .Item("@autorizo").Value = Connection.GetValue(oData, "autorizo")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.VarChar, 12, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")

                .Add(New System.Data.SqlClient.SqlParameter("@abono_cuenta", System.Data.SqlDbType.Bit, 1, "abono_cuenta"))
                .Item("@abono_cuenta").Value = abono_cuenta

                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago_proveedores", System.Data.SqlDbType.Int, 4, "folio_pago_proveedores"))
                .Item("@folio_pago_proveedores").Value = folio_pago_proveedores

                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "concepto", oCommand.Parameters.Item("@concepto").Value)
            Connection.SetValue(oData, "folio", oCommand.Parameters.Item("@folio").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_pagar_ins(ByVal concepto As String, ByRef folio As Long, ByVal proveedor As Long, ByVal entrada As Long, ByVal fecha As Date, ByVal fecha_vencimiento As Date, _
    ByVal plazo As Integer, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal es_padre As Boolean, ByVal cargo As Double, ByVal abono As Double, ByVal subtotal As Double, ByVal iva As Double, _
    ByVal total As Double, ByVal saldo As Double, ByVal tipo_pago As Char, ByVal banco As Long, ByVal cuenta_bancaria As Object, ByVal cheque As Long, ByVal observaciones As String, ByVal formulo As String, ByVal autorizo As String, ByVal status As String, ByVal folio_factura As String, ByVal abono_cuenta As Boolean, ByVal folio_pago_proveedores As Long, ByVal folio_poliza_cheque As Long) As Events

        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Item("@concepto").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_vencimiento", System.Data.SqlDbType.DateTime, 8, "fecha_vencimiento"))
                .Item("@fecha_vencimiento").Value = fecha_vencimiento
                .Add(New System.Data.SqlClient.SqlParameter("@plazo", System.Data.SqlDbType.Int, 4, "plazo"))
                .Item("@plazo").Value = plazo
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = concepto_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@es_padre", System.Data.SqlDbType.Bit, 1, "es_padre"))
                .Item("@es_padre").Value = es_padre
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = cargo
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = abono
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = subtotal
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = iva
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = total
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = saldo

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
                .Item("@tipo_pago").Value = tipo_pago

                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = cuenta_bancaria
                .Add(New System.Data.SqlClient.SqlParameter("@cheque", System.Data.SqlDbType.Int, 4, "cheque"))
                .Item("@cheque").Value = cheque



                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@formulo", System.Data.SqlDbType.VarChar, 30, "formulo"))
                .Item("@formulo").Value = formulo
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 30, "autorizo"))
                .Item("@autorizo").Value = autorizo
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.VarChar, 12, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@abono_cuenta", System.Data.SqlDbType.Bit, 1, "abono_cuenta"))
                .Item("@abono_cuenta").Value = abono_cuenta
                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago_proveedores", System.Data.SqlDbType.Int, 4, "folio_pago_proveedores"))
                .Item("@folio_pago_proveedores").Value = folio_pago_proveedores

                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            concepto = oCommand.Parameters.Item("@concepto").Value
            folio = oCommand.Parameters.Item("@folio").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_movimientos_pagar_abono_por_comision_ins(ByVal concepto As String, ByRef folio As Long, ByVal proveedor As Long, ByVal entrada As Long, ByVal fecha As Date, ByVal fecha_vencimiento As Date, _
                            ByVal plazo As Integer, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal es_padre As Boolean, ByVal cargo As Double, ByVal abono As Double, ByVal subtotal As Double, ByVal iva As Double, _
                            ByVal total As Double, ByVal saldo As Double, ByVal observaciones As String, ByVal formulo As String, ByVal autorizo As String, ByVal status As String, ByVal folio_factura As String, ByVal abono_cuenta As Boolean, ByVal folio_pago_proveedores As Long) As Events

        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_abono_por_comision_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Item("@concepto").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_vencimiento", System.Data.SqlDbType.DateTime, 8, "fecha_vencimiento"))
                .Item("@fecha_vencimiento").Value = fecha_vencimiento
                .Add(New System.Data.SqlClient.SqlParameter("@plazo", System.Data.SqlDbType.Int, 4, "plazo"))
                .Item("@plazo").Value = plazo
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = concepto_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@es_padre", System.Data.SqlDbType.Bit, 1, "es_padre"))
                .Item("@es_padre").Value = es_padre
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = cargo
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = abono
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = subtotal
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = iva
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = total
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = saldo

                '.Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
                '.Item("@tipo_pago").Value = tipo_pago

                '.Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                '.Item("@banco").Value = banco
                '.Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                '.Item("@cuenta_bancaria").Value = cuenta_bancaria
                '.Add(New System.Data.SqlClient.SqlParameter("@cheque", System.Data.SqlDbType.Int, 4, "cheque"))
                '.Item("@cheque").Value = cheque



                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@formulo", System.Data.SqlDbType.VarChar, 30, "formulo"))
                .Item("@formulo").Value = formulo
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 30, "autorizo"))
                .Item("@autorizo").Value = autorizo
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.VarChar, 12, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@abono_cuenta", System.Data.SqlDbType.Bit, 1, "abono_cuenta"))
                .Item("@abono_cuenta").Value = abono_cuenta
                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago_proveedores", System.Data.SqlDbType.Int, 4, "folio_pago_proveedores"))
                .Item("@folio_pago_proveedores").Value = folio_pago_proveedores
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            concepto = oCommand.Parameters.Item("@concepto").Value
            folio = oCommand.Parameters.Item("@folio").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_pagar_upd(ByRef oData As DataSet, ByVal tipo_pago As Object, ByVal cargo As Object, ByVal abono As Object, ByVal banco As Object, ByVal status As String, ByVal ActualizarCheque As Boolean, ByVal folio_poliza_cheque As Long) As Events ', ByVal abono_cuenta As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = Connection.GetValue(oData, "entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_vencimiento", System.Data.SqlDbType.DateTime, 8, "fecha_vencimiento"))
                .Item("@fecha_vencimiento").Value = Connection.GetValue(oData, "fecha_vencimiento")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo", System.Data.SqlDbType.Int, 4, "plazo"))
                .Item("@plazo").Value = Connection.GetValue(oData, "plazo")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = Connection.GetValue(oData, "concepto_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = Connection.GetValue(oData, "folio_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@es_padre", System.Data.SqlDbType.Bit, 1, "es_padre"))
                .Item("@es_padre").Value = Connection.GetValue(oData, "es_padre")
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = cargo
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = abono
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = Connection.GetValue(oData, "iva")
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = Connection.GetValue(oData, "total")
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = Connection.GetValue(oData, "saldo")
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
                .Item("@tipo_pago").Value = tipo_pago
                .Add(New System.Data.SqlClient.SqlParameter("@referencia_transferencia", System.Data.SqlDbType.VarChar, 50, "referencia_transferencia"))
                .Item("@referencia_transferencia").Value = Connection.GetValue(oData, "referencia_transferencia")
                .Add(New System.Data.SqlClient.SqlParameter("@cheque", System.Data.SqlDbType.Int, 4, "cheque"))
                .Item("@cheque").Value = Connection.GetValue(oData, "cheque")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = Connection.GetValue(oData, "cuenta_bancaria")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@formulo", System.Data.SqlDbType.VarChar, 30, "formulo"))
                .Item("@formulo").Value = Connection.GetValue(oData, "formulo")
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 30, "autorizo"))
                .Item("@autorizo").Value = Connection.GetValue(oData, "autorizo")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.VarChar, 12, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@abono_cuenta", System.Data.SqlDbType.Bit, 1, "abono_cuenta"))
                .Item("@abono_cuenta").Value = Connection.GetValue(oData, "abono_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@actualizacheque", System.Data.SqlDbType.Bit, 1, "actualizacheque"))
                .Item("@actualizacheque").Value = ActualizarCheque
                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_pagar_del(ByVal concepto As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_pagar_exs(ByVal concepto As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_pagar_sel(ByVal concepto As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_pagar_grs(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_pagar_actualiza_folio_referencia(ByVal concepto As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_actualiza_folio_referencia]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_pagar_grl(ByVal concepto As String, ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_llena_pago_varias_facturas(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_llena_pago_varias_facturas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    'sp_movimientos_pagar_asignar_abono_por_comision

    Public Function sp_movimientos_pagar_asignar_abono_por_comision(ByVal concepto As String, ByRef folio As Long, ByVal proveedor As Long, ByVal entrada As Long, _
                                                                    ByVal concepto_referencia As String, ByVal folio_referencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_pagar_asignar_abono_por_comision]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = concepto_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


