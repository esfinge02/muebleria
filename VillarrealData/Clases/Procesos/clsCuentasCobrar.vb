'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCuentasCobrar
'DATE:		10/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - CuentasCobrar"
Public Class clsCuentasCobrar
    'Public Function sp_cuentas_cobrar_ins(ByVal sucursal As Long, ByRef folio As Long, ByVal serie As String, ByVal cliente As Long, ByVal concepto As String, ByVal fecha As Date, ByVal tipo_pago As String, ByVal monto As Double, ByVal saldo As Double, ByVal numero_documentos As Long, ByVal fecha_cancelacion As Date, ByVal observaciones As String, ByVal status As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cuentas_cobrar_ins]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
    '            .Item("@sucursal").Value = sucursal
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = folio
    '            .Item("@folio").Direction = ParameterDirection.InputOutput
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
    '            .Item("@serie").Value = serie
    '            .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
    '            .Item("@cliente").Value = cliente
    '            .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Char, 3, "concepto"))
    '            .Item("@concepto").Value = concepto
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
    '            .Item("@fecha").Value = fecha
    '            .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
    '            .Item("@tipo_pago").Value = tipo_pago
    '            .Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Money, 8, "monto"))
    '            .Item("@monto").Value = monto
    '            .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
    '            .Item("@saldo").Value = saldo
    '            .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
    '            .Item("@numero_documentos").Value = numero_documentos
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.Datetime, 8, "fecha_cancelacion"))
    '            .Item("@fecha_cancelacion").Value = fecha_cancelacion
    '            .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 100, "observaciones"))
    '            .Item("@observaciones").Value = observaciones
    '            .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
    '            .Item("@status").Value = status
    '            .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
    '            .Item("@QUIEN").Value = Connection.User

    '        End With
    '        Connection.Execute(oCommand)
    '        folio = oCommand.Parameters.Item("@folio").Value

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_cuentas_cobrar_ins(ByRef oData As DataSet) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cuentas_cobrar_ins]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
    '            .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = Connection.GetValue(oData, "folio")
    '            .Item("@folio").Direction = ParameterDirection.InputOutput
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
    '            .Item("@serie").Value = Connection.GetValue(oData, "serie")
    '            .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
    '            .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
    '            .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Char, 3, "concepto"))
    '            .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
    '            .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
    '            .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
    '            .Item("@tipo_pago").Value = Connection.GetValue(oData, "tipo_pago")
    '            .Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Money, 8, "monto"))
    '            .Item("@monto").Value = Connection.GetValue(oData, "monto")
    '            .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
    '            .Item("@saldo").Value = Connection.GetValue(oData, "saldo")
    '            .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
    '            .Item("@numero_documentos").Value = Connection.GetValue(oData, "numero_documentos")
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
    '            .Item("@fecha_cancelacion").Value = Connection.GetValue(oData, "fecha_cancelacion")
    '            .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 100, "observaciones"))
    '            .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
    '            .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
    '            .Item("@status").Value = Connection.GetValue(oData, "status")
    '            .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
    '            .Item("@QUIEN").Value = Connection.User

    '        End With
    '        Connection.Execute(oCommand)
    '        Connection.SetValue(oData, "folio", oCommand.Parameters.Item("@folio").Value)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_cuentas_cobrar_upd(ByVal sucursal As Long, ByRef folio As Long, ByVal serie As String, ByVal cliente As Long, ByVal concepto As String, ByVal fecha As Date, ByVal tipo_pago As String, ByVal monto As Double, ByVal saldo As Double, ByVal numero_documentos As Long, ByVal fecha_cancelacion As Date, ByVal observaciones As String, ByVal status As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cuentas_cobrar_upd]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
    '            .Item("@sucursal").Value = sucursal
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = folio
    '            .Item("@folio").Direction = ParameterDirection.InputOutput
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
    '            .Item("@serie").Value = serie
    '            .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
    '            .Item("@cliente").Value = cliente
    '            .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Char, 3, "concepto"))
    '            .Item("@concepto").Value = concepto
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
    '            .Item("@fecha").Value = fecha
    '            .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
    '            .Item("@tipo_pago").Value = tipo_pago
    '            .Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Money, 8, "monto"))
    '            .Item("@monto").Value = monto
    '            .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
    '            .Item("@saldo").Value = saldo
    '            .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
    '            .Item("@numero_documentos").Value = numero_documentos
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.Datetime, 8, "fecha_cancelacion"))
    '            .Item("@fecha_cancelacion").Value = fecha_cancelacion
    '            .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 100, "observaciones"))
    '            .Item("@observaciones").Value = observaciones
    '            .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
    '            .Item("@status").Value = status
    '            .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
    '            .Item("@QUIEN").Value = Connection.User

    '        End With
    '        Connection.Execute(oCommand)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_cuentas_cobrar_upd(ByRef oData As DataSet) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cuentas_cobrar_upd]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
    '            .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = Connection.GetValue(oData, "folio")
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
    '            .Item("@serie").Value = Connection.GetValue(oData, "serie")
    '            .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
    '            .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
    '            .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Char, 3, "concepto"))
    '            .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
    '            .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
    '            .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
    '            .Item("@tipo_pago").Value = Connection.GetValue(oData, "tipo_pago")
    '            .Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Money, 8, "monto"))
    '            .Item("@monto").Value = Connection.GetValue(oData, "monto")
    '            .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
    '            .Item("@saldo").Value = Connection.GetValue(oData, "saldo")
    '            .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
    '            .Item("@numero_documentos").Value = Connection.GetValue(oData, "numero_documentos")
    '            .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
    '            .Item("@fecha_cancelacion").Value = Connection.GetValue(oData, "fecha_cancelacion")
    '            .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 100, "observaciones"))
    '            .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
    '            .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
    '            .Item("@status").Value = Connection.GetValue(oData, "status")
    '            .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
    '            .Item("@QUIEN").Value = Connection.User

    '        End With
    '        Connection.Execute(oCommand)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_cuentas_cobrar_del(ByVal sucursal As Long, ByVal folio As Long, ByVal serie As String, ByVal cliente As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cuentas_cobrar_del]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
    '            .Item("@sucursal").Value = sucursal
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = folio
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
    '            .Item("@serie").Value = serie
    '            .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
    '            .Item("@cliente").Value = cliente

    '        End With
    '        Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_cuentas_cobrar_exs(ByVal sucursal As Long, ByVal folio As Long, ByVal serie As String, ByVal cliente As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cuentas_cobrar_exs]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
    '            .Item("@sucursal").Value = sucursal
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = folio
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
    '            .Item("@serie").Value = serie
    '            .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
    '            .Item("@cliente").Value = cliente

    '        End With
    '        oEvent.Value = Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_cuentas_cobrar_sel(ByVal sucursal As Long, ByVal folio As Long, ByVal serie As String, ByVal cliente As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cuentas_cobrar_sel]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
    '            .Item("@sucursal").Value = sucursal
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = folio
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
    '            .Item("@serie").Value = serie
    '            .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
    '            .Item("@cliente").Value = cliente

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_cuentas_cobrar_grs() As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cuentas_cobrar_grs]"
    '        With oCommand.Parameters

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function



End Class
#End Region


