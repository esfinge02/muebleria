Imports Dipros.Utils


Public Class clsPrefacturas

    Public Function sp_prefacturas_ins(ByRef oData As DataSet, ByVal sucursal As Long, ByVal fecha As DateTime, ByVal importe As Double, ByVal menos As Double, ByVal enganche As Double, _
ByVal interes As Double, ByVal plan As Long, ByVal fecha_primer_documento As Date, ByVal numero_documentos As Long, ByVal importe_documentos As Double, ByVal importe_ultimo_documento As Double, _
ByVal enganche_en_documento As Boolean, ByVal fecha_enganche_documento As Date, ByVal importe_enganche_documento As Double, ByVal liquida_vencimiento As Boolean, ByVal monto_liquida_vencimiento As Double, _
ByVal enganche_en_menos As Boolean, ByVal sucursal_dependencia As Boolean, ByVal folio_plantilla As Long, ByVal quincena_inicio As Long, ByVal ano_inicio As Long, ByVal punto_venta As Long, ByVal FolioDescuentoCliente As Long, _
ByVal aplica_enganche As Boolean, ByVal paquete As Long, ByVal convenio As Long, ByVal cancelacion_automatica As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_prefacturas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_prefactura", System.Data.SqlDbType.Int, 4, "folio_prefactura"))
                .Item("@folio_prefactura").Value = Connection.GetValue(oData, "folio_prefactura")
                .Item("@folio_prefactura").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 80, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 50, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.VarChar, 50, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono1", System.Data.SqlDbType.VarChar, 13, "telefono1"))
                .Item("@telefono1").Value = Connection.GetValue(oData, "telefono1")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono2", System.Data.SqlDbType.VarChar, 13, "telefono2"))
                .Item("@telefono2").Value = Connection.GetValue(oData, "telefono2")
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 13, "fax"))
                .Item("@fax").Value = Connection.GetValue(oData, "fax")
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@tipoventa", System.Data.SqlDbType.Char, 1, "tipoventa"))
                .Item("@tipoventa").Value = Connection.GetValue(oData, "tipoventa")
                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1, "ivadesglosado"))
                .Item("@ivadesglosado").Value = Connection.GetValue(oData, "ivadesglosado")

                .Add(New System.Data.SqlClient.SqlParameter("@pedimento", System.Data.SqlDbType.VarChar, 30, "pedimento"))
                .Item("@pedimento").Value = Connection.GetValue(oData, "pedimento")
                .Add(New System.Data.SqlClient.SqlParameter("@aduana", System.Data.SqlDbType.VarChar, 50, "aduana"))
                .Item("@aduana").Value = Connection.GetValue(oData, "aduana")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_importacion", System.Data.SqlDbType.DateTime, 8, "fecha_importacion"))
                .Item("@fecha_importacion").Value = Connection.GetValue(oData, "fecha_importacion")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_reparto", System.Data.SqlDbType.VarChar, 80, "domicilio_reparto"))
                .Item("@domicilio_reparto").Value = Connection.GetValue(oData, "domicilio_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_reparto", System.Data.SqlDbType.VarChar, 50, "colonia_reparto"))
                .Item("@colonia_reparto").Value = Connection.GetValue(oData, "colonia_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_reparto", System.Data.SqlDbType.VarChar, 50, "ciudad_reparto"))
                .Item("@ciudad_reparto").Value = Connection.GetValue(oData, "ciudad_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@estado_reparto", System.Data.SqlDbType.VarChar, 50, "estado_reparto"))
                .Item("@estado_reparto").Value = Connection.GetValue(oData, "estado_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_reparto", System.Data.SqlDbType.VarChar, 13, "telefono_reparto"))
                .Item("@telefono_reparto").Value = Connection.GetValue(oData, "telefono_reparto")

                .Add(New System.Data.SqlClient.SqlParameter("@horario_reparto", System.Data.SqlDbType.Char, 1, "horario_reparto"))
                .Item("@horario_reparto").Value = Connection.GetValue(oData, "horario_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_reparto", System.Data.SqlDbType.DateTime, 8, "fecha_reparto"))
                .Item("@fecha_reparto").Value = Connection.GetValue(oData, "fecha_reparto")

                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_reparto", System.Data.SqlDbType.Text, 0, "observaciones_reparto"))
                .Item("@observaciones_reparto").Value = Connection.GetValue(oData, "observaciones_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_venta", System.Data.SqlDbType.Text, 0, "observaciones_venta"))
                .Item("@observaciones_venta").Value = Connection.GetValue(oData, "observaciones_venta")

                If Connection.GetValue(oData, "tipoventa") = "D" Then   'CONTADO
                    .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                    .Item("@importe").Value = Connection.GetValue(oData, "total") - menos
                    .Add(New System.Data.SqlClient.SqlParameter("@menos", System.Data.SqlDbType.Money, 8, "menos"))
                    .Item("@menos").Value = 0 ' El menos va en 0 Cero
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Money, 8, "enganche"))
                    .Item("@enganche").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Money, 8, "interes"))
                    .Item("@interes").Value = System.DBNull.Value

                    .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                    .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                    .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                    .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                    .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                    .Item("@total").Value = Connection.GetValue(oData, "total") - menos



                    .Add(New System.Data.SqlClient.SqlParameter("@plan", System.Data.SqlDbType.Int, 4, "plan"))
                    .Item("@plan").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_primer_documento", System.Data.SqlDbType.DateTime, 8, "fecha_primer_documento"))
                    .Item("@fecha_primer_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
                    .Item("@numero_documentos").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_documentos", System.Data.SqlDbType.Money, 8, "importe_documentos"))
                    .Item("@importe_documentos").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_ultimo_documento", System.Data.SqlDbType.Money, 8, "importe_ultimo_documento"))
                    .Item("@importe_ultimo_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_documento", System.Data.SqlDbType.Bit, 1, "enganche_en_documento"))
                    .Item("@enganche_en_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_enganche_documento", System.Data.SqlDbType.DateTime, 8, "fecha_enganche_documento"))
                    .Item("@fecha_enganche_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_enganche_documento", System.Data.SqlDbType.Money, 8, "importe_enganche_documento"))
                    .Item("@importe_enganche_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@liquida_vencimiento", System.Data.SqlDbType.Bit, 1, "liquida_vencimiento"))
                    .Item("@liquida_vencimiento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@monto_liquida_vencimiento", System.Data.SqlDbType.Money, 8, "monto_liquida_vencimiento"))
                    .Item("@monto_liquida_vencimiento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Bit, 1, "enganche_en_menos"))
                    .Item("@enganche_en_menos").Value = System.DBNull.Value
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                    .Item("@importe").Value = importe
                    .Add(New System.Data.SqlClient.SqlParameter("@menos", System.Data.SqlDbType.Money, 8, "menos"))
                    .Item("@menos").Value = menos
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Money, 8, "enganche"))
                    .Item("@enganche").Value = enganche
                    .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Money, 8, "interes"))
                    .Item("@interes").Value = interes
                    .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                    .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                    .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                    .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                    .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                    .Item("@total").Value = Connection.GetValue(oData, "total")

                    .Add(New System.Data.SqlClient.SqlParameter("@plan", System.Data.SqlDbType.Int, 4, "plan"))
                    .Item("@plan").Value = plan
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_primer_documento", System.Data.SqlDbType.DateTime, 8, "fecha_primer_documento"))
                    If fecha_primer_documento = CDate("01/01/1900") Then
                        .Item("@fecha_primer_documento").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_primer_documento").Value = fecha_primer_documento
                    End If
                    .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
                    .Item("@numero_documentos").Value = numero_documentos
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_documentos", System.Data.SqlDbType.Money, 8, "importe_documentos"))
                    .Item("@importe_documentos").Value = importe_documentos
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_ultimo_documento", System.Data.SqlDbType.Money, 8, "importe_ultimo_documento"))
                    .Item("@importe_ultimo_documento").Value = importe_ultimo_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_documento", System.Data.SqlDbType.Bit, 1, "enganche_en_documento"))
                    .Item("@enganche_en_documento").Value = enganche_en_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_enganche_documento", System.Data.SqlDbType.DateTime, 8, "fecha_enganche_documento"))
                    If fecha_enganche_documento = CDate("01/01/1900") Then
                        .Item("@fecha_enganche_documento").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_enganche_documento").Value = fecha_enganche_documento
                    End If
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_enganche_documento", System.Data.SqlDbType.Money, 8, "importe_enganche_documento"))
                    .Item("@importe_enganche_documento").Value = importe_enganche_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@liquida_vencimiento", System.Data.SqlDbType.Bit, 1, "liquida_vencimiento"))
                    .Item("@liquida_vencimiento").Value = liquida_vencimiento
                    .Add(New System.Data.SqlClient.SqlParameter("@monto_liquida_vencimiento", System.Data.SqlDbType.Money, 8, "monto_liquida_vencimiento"))
                    .Item("@monto_liquida_vencimiento").Value = monto_liquida_vencimiento
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Bit, 1, "enganche_en_menos"))
                    .Item("@enganche_en_menos").Value = enganche_en_menos
                End If

                If sucursal_dependencia Then
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_pedido", System.Data.SqlDbType.DateTime, 8, "fecha_pedido"))
                    .Item("@fecha_pedido").Value = Connection.GetValue(oData, "fecha_pedido")
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_pedido", System.Data.SqlDbType.Int, 4, "folio_pedido"))
                    .Item("@folio_pedido").Value = Connection.GetValue(oData, "folio_pedido")
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_plantilla", System.Data.SqlDbType.Int, 4, "folio_plantilla"))
                    .Item("@folio_plantilla").Value = folio_plantilla
                    .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                    .Item("@quincena_inicio").Value = quincena_inicio
                    .Add(New System.Data.SqlClient.SqlParameter("@ano_inicio", System.Data.SqlDbType.Int, 4, "ano_inicio"))
                    .Item("@ano_inicio").Value = ano_inicio
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_pedido", System.Data.SqlDbType.DateTime, 8, "fecha_pedido"))
                    .Item("@fecha_pedido").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_pedido", System.Data.SqlDbType.Int, 4, "folio_pedido"))
                    .Item("@folio_pedido").Value = -1
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_plantilla", System.Data.SqlDbType.Int, 4, "folio_plantilla"))
                    .Item("@folio_plantilla").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                    .Item("@quincena_inicio").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@ano_inicio", System.Data.SqlDbType.Int, 4, "ano_inicio"))
                    .Item("@ano_inicio").Value = System.DBNull.Value
                End If
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = Connection.GetValue(oData, "cotizacion")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = Connection.GetValue(oData, "folio_vista_salida")

                .Add(New System.Data.SqlClient.SqlParameter("@punto_venta", System.Data.SqlDbType.Int, 4, "punto_venta"))
                .Item("@punto_venta").Value = punto_venta

                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento_especial_clientes", System.Data.SqlDbType.Int, 4, "folio_descuento_especial_clientes"))
                .Item("@folio_descuento_especial_clientes").Value = FolioDescuentoCliente




                .Add(New System.Data.SqlClient.SqlParameter("@aplica_enganche", System.Data.SqlDbType.Bit, 1, "aplica_enganche"))
                .Item("@aplica_enganche").Value = aplica_enganche

                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = paquete

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@cancelacion_automatica", System.Data.SqlDbType.Bit, 1, "cancelacion_automatica"))
                .Item("@cancelacion_automatica").Value = cancelacion_automatica

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio_prefactura", oCommand.Parameters.Item("@folio_prefactura").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_prefacturas_upd(ByRef oData As DataSet, ByVal sucursal As Long, ByVal fecha As DateTime, ByVal importe As Double, ByVal menos As Double, ByVal enganche As Double, _
    ByVal interes As Double, ByVal plan As Long, ByVal fecha_primer_documento As Date, ByVal numero_documentos As Long, ByVal importe_documentos As Double, ByVal importe_ultimo_documento As Double, _
    ByVal enganche_en_documento As Boolean, ByVal fecha_enganche_documento As Date, ByVal importe_enganche_documento As Double, ByVal liquida_vencimiento As Boolean, ByVal monto_liquida_vencimiento As Double, _
    ByVal enganche_en_menos As Boolean, ByVal sucursal_dependencia As Boolean, ByVal folio_plantilla As Long, ByVal quincena_inicio As Long, ByVal ano_inicio As Long, ByVal punto_venta As Long, ByVal FolioDescuentoCliente As Long, _
    ByVal aplica_enganche As Boolean, ByVal paquete As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_prefacturas_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 80, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 50, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.VarChar, 50, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono1", System.Data.SqlDbType.VarChar, 13, "telefono1"))
                .Item("@telefono1").Value = Connection.GetValue(oData, "telefono1")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono2", System.Data.SqlDbType.VarChar, 13, "telefono2"))
                .Item("@telefono2").Value = Connection.GetValue(oData, "telefono2")
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 13, "fax"))
                .Item("@fax").Value = Connection.GetValue(oData, "fax")
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@tipoventa", System.Data.SqlDbType.Char, 1, "tipoventa"))
                .Item("@tipoventa").Value = Connection.GetValue(oData, "tipoventa")
                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1, "ivadesglosado"))
                .Item("@ivadesglosado").Value = Connection.GetValue(oData, "ivadesglosado")

                .Add(New System.Data.SqlClient.SqlParameter("@pedimento", System.Data.SqlDbType.VarChar, 30, "pedimento"))
                .Item("@pedimento").Value = Connection.GetValue(oData, "pedimento")
                .Add(New System.Data.SqlClient.SqlParameter("@aduana", System.Data.SqlDbType.VarChar, 50, "aduana"))
                .Item("@aduana").Value = Connection.GetValue(oData, "aduana")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_importacion", System.Data.SqlDbType.DateTime, 8, "fecha_importacion"))
                .Item("@fecha_importacion").Value = Connection.GetValue(oData, "fecha_importacion")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_reparto", System.Data.SqlDbType.VarChar, 80, "domicilio_reparto"))
                .Item("@domicilio_reparto").Value = Connection.GetValue(oData, "domicilio_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_reparto", System.Data.SqlDbType.VarChar, 50, "colonia_reparto"))
                .Item("@colonia_reparto").Value = Connection.GetValue(oData, "colonia_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_reparto", System.Data.SqlDbType.VarChar, 50, "ciudad_reparto"))
                .Item("@ciudad_reparto").Value = Connection.GetValue(oData, "ciudad_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@estado_reparto", System.Data.SqlDbType.VarChar, 50, "estado_reparto"))
                .Item("@estado_reparto").Value = Connection.GetValue(oData, "estado_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_reparto", System.Data.SqlDbType.VarChar, 13, "telefono_reparto"))
                .Item("@telefono_reparto").Value = Connection.GetValue(oData, "telefono_reparto")

                .Add(New System.Data.SqlClient.SqlParameter("@horario_reparto", System.Data.SqlDbType.Char, 1, "horario_reparto"))
                .Item("@horario_reparto").Value = Connection.GetValue(oData, "horario_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_reparto", System.Data.SqlDbType.DateTime, 8, "fecha_reparto"))
                .Item("@fecha_reparto").Value = Connection.GetValue(oData, "fecha_reparto")

                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_reparto", System.Data.SqlDbType.Text, 0, "observaciones_reparto"))
                .Item("@observaciones_reparto").Value = Connection.GetValue(oData, "observaciones_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_venta", System.Data.SqlDbType.Text, 0, "observaciones_venta"))
                .Item("@observaciones_venta").Value = Connection.GetValue(oData, "observaciones_venta")

                If Connection.GetValue(oData, "tipoventa") = "D" Then   'CONTADO
                    .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                    .Item("@importe").Value = Connection.GetValue(oData, "total") - menos
                    .Add(New System.Data.SqlClient.SqlParameter("@menos", System.Data.SqlDbType.Money, 8, "menos"))
                    .Item("@menos").Value = 0 ' El menos va en 0 Cero
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Money, 8, "enganche"))
                    .Item("@enganche").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Money, 8, "interes"))
                    .Item("@interes").Value = System.DBNull.Value

                    .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                    .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                    .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                    .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                    .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                    .Item("@total").Value = Connection.GetValue(oData, "total") - menos



                    .Add(New System.Data.SqlClient.SqlParameter("@plan", System.Data.SqlDbType.Int, 4, "plan"))
                    .Item("@plan").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_primer_documento", System.Data.SqlDbType.DateTime, 8, "fecha_primer_documento"))
                    .Item("@fecha_primer_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
                    .Item("@numero_documentos").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_documentos", System.Data.SqlDbType.Money, 8, "importe_documentos"))
                    .Item("@importe_documentos").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_ultimo_documento", System.Data.SqlDbType.Money, 8, "importe_ultimo_documento"))
                    .Item("@importe_ultimo_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_documento", System.Data.SqlDbType.Bit, 1, "enganche_en_documento"))
                    .Item("@enganche_en_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_enganche_documento", System.Data.SqlDbType.DateTime, 8, "fecha_enganche_documento"))
                    .Item("@fecha_enganche_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_enganche_documento", System.Data.SqlDbType.Money, 8, "importe_enganche_documento"))
                    .Item("@importe_enganche_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@liquida_vencimiento", System.Data.SqlDbType.Bit, 1, "liquida_vencimiento"))
                    .Item("@liquida_vencimiento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@monto_liquida_vencimiento", System.Data.SqlDbType.Money, 8, "monto_liquida_vencimiento"))
                    .Item("@monto_liquida_vencimiento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Bit, 1, "enganche_en_menos"))
                    .Item("@enganche_en_menos").Value = System.DBNull.Value
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                    .Item("@importe").Value = importe
                    .Add(New System.Data.SqlClient.SqlParameter("@menos", System.Data.SqlDbType.Money, 8, "menos"))
                    .Item("@menos").Value = menos
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Money, 8, "enganche"))
                    .Item("@enganche").Value = enganche
                    .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Money, 8, "interes"))
                    .Item("@interes").Value = interes
                    .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                    .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                    .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                    .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                    .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                    .Item("@total").Value = Connection.GetValue(oData, "total")

                    .Add(New System.Data.SqlClient.SqlParameter("@plan", System.Data.SqlDbType.Int, 4, "plan"))
                    .Item("@plan").Value = plan
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_primer_documento", System.Data.SqlDbType.DateTime, 8, "fecha_primer_documento"))
                    If fecha_primer_documento = CDate("01/01/1900") Then
                        .Item("@fecha_primer_documento").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_primer_documento").Value = fecha_primer_documento
                    End If
                    .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
                    .Item("@numero_documentos").Value = numero_documentos
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_documentos", System.Data.SqlDbType.Money, 8, "importe_documentos"))
                    .Item("@importe_documentos").Value = importe_documentos
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_ultimo_documento", System.Data.SqlDbType.Money, 8, "importe_ultimo_documento"))
                    .Item("@importe_ultimo_documento").Value = importe_ultimo_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_documento", System.Data.SqlDbType.Bit, 1, "enganche_en_documento"))
                    .Item("@enganche_en_documento").Value = enganche_en_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_enganche_documento", System.Data.SqlDbType.DateTime, 8, "fecha_enganche_documento"))
                    If fecha_enganche_documento = CDate("01/01/1900") Then
                        .Item("@fecha_enganche_documento").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_enganche_documento").Value = fecha_enganche_documento
                    End If
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_enganche_documento", System.Data.SqlDbType.Money, 8, "importe_enganche_documento"))
                    .Item("@importe_enganche_documento").Value = importe_enganche_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@liquida_vencimiento", System.Data.SqlDbType.Bit, 1, "liquida_vencimiento"))
                    .Item("@liquida_vencimiento").Value = liquida_vencimiento
                    .Add(New System.Data.SqlClient.SqlParameter("@monto_liquida_vencimiento", System.Data.SqlDbType.Money, 8, "monto_liquida_vencimiento"))
                    .Item("@monto_liquida_vencimiento").Value = monto_liquida_vencimiento
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Bit, 1, "enganche_en_menos"))
                    .Item("@enganche_en_menos").Value = enganche_en_menos
                End If

                If sucursal_dependencia Then
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_pedido", System.Data.SqlDbType.DateTime, 8, "fecha_pedido"))
                    .Item("@fecha_pedido").Value = Connection.GetValue(oData, "fecha_pedido")
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_pedido", System.Data.SqlDbType.Int, 4, "folio_pedido"))
                    .Item("@folio_pedido").Value = Connection.GetValue(oData, "folio_pedido")
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_plantilla", System.Data.SqlDbType.Int, 4, "folio_plantilla"))
                    .Item("@folio_plantilla").Value = folio_plantilla
                    .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                    .Item("@quincena_inicio").Value = quincena_inicio
                    .Add(New System.Data.SqlClient.SqlParameter("@ano_inicio", System.Data.SqlDbType.Int, 4, "ano_inicio"))
                    .Item("@ano_inicio").Value = ano_inicio
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_pedido", System.Data.SqlDbType.DateTime, 8, "fecha_pedido"))
                    .Item("@fecha_pedido").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_pedido", System.Data.SqlDbType.Int, 4, "folio_pedido"))
                    .Item("@folio_pedido").Value = -1
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_plantilla", System.Data.SqlDbType.Int, 4, "folio_plantilla"))
                    .Item("@folio_plantilla").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                    .Item("@quincena_inicio").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@ano_inicio", System.Data.SqlDbType.Int, 4, "ano_inicio"))
                    .Item("@ano_inicio").Value = System.DBNull.Value
                End If
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = Connection.GetValue(oData, "cotizacion")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = Connection.GetValue(oData, "folio_vista_salida")

                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento_especial_clientes", System.Data.SqlDbType.Int, 4, "folio_descuento_especial_clientes"))
                .Item("@folio_descuento_especial_clientes").Value = FolioDescuentoCliente


                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura_cancelada", System.Data.SqlDbType.Int, 4, "sucursal_factura_cancelada"))
                .Item("@sucursal_factura_cancelada").Value = Connection.GetValue(oData, "sucursal_factura_cancelada")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_cancelada", System.Data.SqlDbType.Char, 3, "serie_factura_cancelada"))
                .Item("@serie_factura_cancelada").Value = Connection.GetValue(oData, "serie_factura_cancelada")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_cancelada", System.Data.SqlDbType.Int, 4, "folio_factura_cancelada"))
                .Item("@folio_factura_cancelada").Value = Connection.GetValue(oData, "folio_factura_cancelada")

                .Add(New System.Data.SqlClient.SqlParameter("@aplica_enganche", System.Data.SqlDbType.Bit, 1, "aplica_enganche"))
                .Item("@aplica_enganche").Value = aplica_enganche

                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = paquete

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_prefacturas_sel(ByVal folio_prefactura As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_prefacturas_sel]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@folio_prefactura", System.Data.SqlDbType.Int, 4, "folio_prefactura"))
                .Item("@folio_prefactura").Value = folio_prefactura

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_prefacturas_grs(ByVal sucursal As Long, ByVal fecha As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_prefacturas_grs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
