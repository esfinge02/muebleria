'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEnviosConvenios
'DATE:		06/11/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - EnviosConvenios"
Public Class clsEnviosConvenios
    Public Function sp_envios_convenios_ins(ByRef sucursal As Long, ByRef convenio As Long, ByRef quincena_envio As Long, ByRef anio_envio As Long, ByRef cliente As Long, ByRef importe As Long, ByRef tipo As Char, ByRef cancelado As Boolean, ByRef fecha_cancelacion As DateTime, ByRef usuario_cancelacion As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_envios_convenios_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Item("@sucursal").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Item("@convenio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = quincena_envio
                .Item("@quincena_envio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
                .Item("@anio_envio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Item("@cliente").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@cancelado", System.Data.SqlDbType.Bit, 1, "cancelado"))
                .Item("@cancelado").Value = cancelado
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = fecha_cancelacion
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_cancelacion", System.Data.SqlDbType.VarChar, 15, "usuario_cancelacion"))
                .Item("@usuario_cancelacion").Value = usuario_cancelacion
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = Connection.GetValue(oData, "convenio")
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = Connection.GetValue(oData, "quincena_envio")
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = Connection.GetValue(oData, "anio_envio")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = Connection.GetValue(oData, "importe")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = Connection.GetValue(oData, "tipo")
                .Add(New System.Data.SqlClient.SqlParameter("@cancelado", System.Data.SqlDbType.Bit, 1, "cancelado"))
                .Item("@cancelado").Value = Connection.GetValue(oData, "cancelado")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = Connection.GetValue(oData, "fecha_cancelacion")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_cancelacion", System.Data.SqlDbType.VarChar, 15, "usuario_cancelacion"))
                .Item("@usuario_cancelacion").Value = Connection.GetValue(oData, "usuario_cancelacion")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_del(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = quincena_envio
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_exs(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = quincena_envio
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_sel(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = quincena_envio
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_actualiza_fechas_envios(ByVal sucursal As Long, ByVal anio_envio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_fechas_envios]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


