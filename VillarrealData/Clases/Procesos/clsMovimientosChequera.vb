'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosChequera
'DATE:		08/09/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - MovimientosChequera"
Public Class clsMovimientosChequera
    Public Function sp_movimientos_chequera_ins(ByRef oData As DataSet, ByRef saldo_actual As Double, ByVal actualizacheque As Boolean, ByVal Folio_Poliza_Cheque As Long, Optional ByVal folio_pago_proveedores As Long = 0) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_chequera_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = Connection.GetValue(oData, "cuentabancaria")
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Item("@folio").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@factura", System.Data.SqlDbType.Char, 50, "factura"))
                .Item("@factura").Value = Connection.GetValue(oData, "factura")
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = Connection.GetValue(oData, "importe")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = Connection.GetValue(oData, "tipo")
                .Add(New System.Data.SqlClient.SqlParameter("@generar_cheque", System.Data.SqlDbType.Bit, 1, "generar_cheque"))
                .Item("@generar_cheque").Value = Connection.GetValue(oData, "generar_cheque")
                .Add(New System.Data.SqlClient.SqlParameter("@beneficiario", System.Data.SqlDbType.VarChar, 100, "beneficiario"))
                .Item("@beneficiario").Value = Connection.GetValue(oData, "beneficiario")
                .Add(New System.Data.SqlClient.SqlParameter("@cheque", System.Data.SqlDbType.Int, 4, "cheque"))
                .Item("@cheque").Value = Connection.GetValue(oData, "cheque")
                .Add(New System.Data.SqlClient.SqlParameter("@abono_a_cuenta", System.Data.SqlDbType.Bit, 1, "abono_a_cuenta"))
                .Item("@abono_a_cuenta").Value = Connection.GetValue(oData, "abono_a_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@formulo", System.Data.SqlDbType.VarChar, 100, "formulo"))
                .Item("@formulo").Value = Connection.GetValue(oData, "formulo")
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 100, "autorizo"))
                .Item("@autorizo").Value = Connection.GetValue(oData, "autorizo")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 100, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")

                .Add(New System.Data.SqlClient.SqlParameter("@saldo_actual", System.Data.SqlDbType.Money, 8, "saldo_actual"))
                .Item("@saldo_actual").Value = saldo_actual
                .Item("@saldo_actual").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@actualizacheque", System.Data.SqlDbType.Bit, 1, "actualizacheque"))
                .Item("@actualizacheque").Value = actualizacheque
                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago_proveedores", System.Data.SqlDbType.Int, 4, "folio_pago_proveedores"))
                .Item("@folio_pago_proveedores").Value = folio_pago_proveedores

                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = Folio_Poliza_Cheque

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_movimiento", System.Data.SqlDbType.Int, 4, "concepto_movimiento"))
                .Item("@concepto_movimiento").Value = Connection.GetValue(oData, "concepto_movimiento")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio", oCommand.Parameters.Item("@folio").Value)
            saldo_actual = oCommand.Parameters.Item("@saldo_actual").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_chequera_ins(ByVal banco As Long, ByVal cuentabancaria As String, ByRef folio As Long, ByVal fecha As DateTime, ByVal factura As String, _
    ByVal importe As Double, ByVal tipo As Char, ByVal generar_cheque As Boolean, ByVal beneficiario As String, ByVal cheque As Long, ByVal abono_a_cuenta As Boolean, _
    ByVal formulo As String, ByVal autorizo As String, ByVal concepto As String, ByRef saldo_actual As Double, ByVal actualizacheque As Boolean, ByVal folio_pago_proveedores As Long, ByVal ConceptoMovimiento As Long, ByVal Folio_Poliza_Cheque As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_chequera_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Item("@folio").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@factura", System.Data.SqlDbType.Char, 50, "factura"))
                .Item("@factura").Value = factura
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@generar_cheque", System.Data.SqlDbType.Bit, 1, "generar_cheque"))
                .Item("@generar_cheque").Value = generar_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@beneficiario", System.Data.SqlDbType.VarChar, 100, "beneficiario"))
                .Item("@beneficiario").Value = beneficiario
                .Add(New System.Data.SqlClient.SqlParameter("@cheque", System.Data.SqlDbType.Int, 4, "cheque"))
                .Item("@cheque").Value = cheque
                .Add(New System.Data.SqlClient.SqlParameter("@abono_a_cuenta", System.Data.SqlDbType.Bit, 1, "abono_a_cuenta"))
                .Item("@abono_a_cuenta").Value = abono_a_cuenta
                .Add(New System.Data.SqlClient.SqlParameter("@formulo", System.Data.SqlDbType.VarChar, 100, "formulo"))
                .Item("@formulo").Value = formulo
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 100, "autorizo"))
                .Item("@autorizo").Value = autorizo
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 100, "concepto"))
                .Item("@concepto").Value = concepto

                .Add(New System.Data.SqlClient.SqlParameter("@saldo_actual", System.Data.SqlDbType.Money, 8, "saldo_actual"))
                .Item("@saldo_actual").Value = saldo_actual
                .Item("@saldo_actual").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@actualizacheque", System.Data.SqlDbType.Bit, 1, "actualizacheque"))
                .Item("@actualizacheque").Value = actualizacheque

                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago_proveedores", System.Data.SqlDbType.Int, 4, "folio_pago_proveedores"))
                .Item("@folio_pago_proveedores").Value = folio_pago_proveedores

                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = Folio_Poliza_Cheque

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_movimiento", System.Data.SqlDbType.Int, 4, "concepto_movimiento"))
                .Item("@concepto_movimiento").Value = ConceptoMovimiento

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio = oCommand.Parameters.Item("@folio").Value
            saldo_actual = oCommand.Parameters.Item("@saldo_actual").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_chequera_upd(ByRef oData As DataSet, ByVal importe_anterior As Double, ByRef saldo_actual As Double, ByVal actualizacheque As Boolean, ByVal Folio_Poliza_Cheque As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_chequera_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = Connection.GetValue(oData, "cuentabancaria")
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@factura", System.Data.SqlDbType.Char, 10, "factura"))
                .Item("@factura").Value = Connection.GetValue(oData, "factura")
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = Connection.GetValue(oData, "importe")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = Connection.GetValue(oData, "tipo")
                .Add(New System.Data.SqlClient.SqlParameter("@generar_cheque", System.Data.SqlDbType.Bit, 1, "generar_cheque"))
                .Item("@generar_cheque").Value = Connection.GetValue(oData, "generar_cheque")
                .Add(New System.Data.SqlClient.SqlParameter("@beneficiario", System.Data.SqlDbType.VarChar, 100, "beneficiario"))
                .Item("@beneficiario").Value = Connection.GetValue(oData, "beneficiario")
                .Add(New System.Data.SqlClient.SqlParameter("@cheque", System.Data.SqlDbType.Int, 4, "cheque"))
                .Item("@cheque").Value = Connection.GetValue(oData, "cheque")
                .Add(New System.Data.SqlClient.SqlParameter("@abono_a_cuenta", System.Data.SqlDbType.Bit, 1, "abono_a_cuenta"))
                .Item("@abono_a_cuenta").Value = Connection.GetValue(oData, "abono_a_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@formulo", System.Data.SqlDbType.VarChar, 100, "formulo"))
                .Item("@formulo").Value = Connection.GetValue(oData, "formulo")
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 100, "autorizo"))
                .Item("@autorizo").Value = Connection.GetValue(oData, "autorizo")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 100, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")

                .Add(New System.Data.SqlClient.SqlParameter("@importe_anterior", System.Data.SqlDbType.Money, 8, "importe_anterior"))
                .Item("@importe_anterior").Value = importe_anterior

                .Add(New System.Data.SqlClient.SqlParameter("@saldo_actual", System.Data.SqlDbType.Money, 8, "saldo_actual"))
                .Item("@saldo_actual").Value = saldo_actual
                .Item("@saldo_actual").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@actualizacheque", System.Data.SqlDbType.Bit, 1, "actualizacheque"))
                .Item("@actualizacheque").Value = actualizacheque

                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_movimiento", System.Data.SqlDbType.Int, 4, "concepto_movimiento"))
                .Item("@concepto_movimiento").Value = Connection.GetValue(oData, "concepto_movimiento")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            saldo_actual = oCommand.Parameters.Item("@saldo_actual").Value
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_chequera_del(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Long, ByRef saldo_actual As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_chequera_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@saldo_actual", System.Data.SqlDbType.Money, 8, "saldo_actual"))
                .Item("@saldo_actual").Value = saldo_actual
                .Item("@saldo_actual").Direction = ParameterDirection.InputOutput


            End With
            Connection.Execute(oCommand)
            saldo_actual = oCommand.Parameters.Item("@saldo_actual").Value
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_chequera_exs(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_chequera_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_chequera_sel(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_chequera_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_chequera_grs(ByVal banco As Long, ByVal cuentabancaria As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_chequera_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuentabancaria", System.Data.SqlDbType.VarChar, 20, "cuentabancaria"))
                .Item("@cuentabancaria").Value = cuentabancaria
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_chequera_beneficiario_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_chequera_beneficiario_grl]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


