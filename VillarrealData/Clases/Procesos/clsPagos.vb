'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPagos
'DATE:		10/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Pagos"
Public Class clsPagos

    Public Function sp_pagos_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pagos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = Connection.GetValue(oData, "sucursal_abono")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = Connection.GetValue(oData, "folio_recibo")
                .Item("@folio_recibo").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = Connection.GetValue(oData, "serie_recibo")
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = Connection.GetValue(oData, "cajero")
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = Connection.GetValue(oData, "cobrador")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = Connection.GetValue(oData, "fecha_cancelacion")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio_recibo", oCommand.Parameters.Item("@folio_recibo").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pagos_ins(ByVal sucursal_abono As Long, ByRef folio_recibo As Long, ByVal serie_recibo As String, ByVal cajero As Long, ByVal cobrador As Long, ByVal cliente As Long, ByVal fecha_cancelacion As Date, ByVal status As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pagos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = sucursal_abono
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo
                .Item("@folio_recibo").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = fecha_cancelacion
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio_recibo = oCommand.Parameters.Item("@folio_recibo").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pagos_upd(ByVal sucursal_abono As Long, ByRef folio_recibo As Long, ByVal serie_recibo As String, ByVal cajero As Long, ByVal cobrador As Long, ByVal cliente As Long, ByVal fecha_cancelacion As Date, ByVal status As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pagos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = sucursal_abono
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo
                .Item("@folio_recibo").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = cobrador
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = fecha_cancelacion
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pagos_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pagos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = Connection.GetValue(oData, "sucursal_abono")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = Connection.GetValue(oData, "folio_recibo")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = Connection.GetValue(oData, "serie_recibo")
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = Connection.GetValue(oData, "cajero")
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador", System.Data.SqlDbType.Int, 4, "cobrador"))
                .Item("@cobrador").Value = Connection.GetValue(oData, "cobrador")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cancelacion", System.Data.SqlDbType.DateTime, 8, "fecha_cancelacion"))
                .Item("@fecha_cancelacion").Value = Connection.GetValue(oData, "fecha_cancelacion")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pagos_del(ByVal sucursal_abono As Long, ByVal folio_recibo As Long, ByVal serie_recibo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pagos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = sucursal_abono
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pagos_exs(ByVal sucursal_abono As Long, ByVal folio_recibo As Long, ByVal serie_recibo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pagos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = sucursal_abono
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pagos_sel(ByVal sucursal_abono As Long, ByVal folio_recibo As Long, ByVal serie_recibo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pagos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_abono", System.Data.SqlDbType.Int, 4, "sucursal_abono"))
                .Item("@sucursal_abono").Value = sucursal_abono
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = folio_recibo
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = serie_recibo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pagos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pagos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class

#End Region


