'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsLlamadasClientes
'DATE:		17/01/2007 00:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - LlamadasClientes"
Public Class clsLlamadasClientes
    Public Function sp_llamadas_clientes_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_llamadas_clientes_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Item("@cliente").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_hora", System.Data.SqlDbType.Datetime, 8, "fecha_hora"))
                .Item("@fecha_hora").Value = Connection.GetValue(oData, "fecha_hora")
                .Item("@fecha_hora").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = Connection.GetValue(oData, "tipo_llamada")

                .Add(New System.Data.SqlClient.SqlParameter("@persona_contesta", System.Data.SqlDbType.VarChar, 50, "persona_contesta"))
                .Item("@persona_contesta").Value = Connection.GetValue(oData, "persona_contesta")
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje_dejado", System.Data.SqlDbType.Text, 0, "mensaje_dejado"))

                .Item("@mensaje_recibido").Value = Connection.GetValue(oData, "mensaje_recibido")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_llamada", System.Data.SqlDbType.VarChar, 15, "usuario_llamada"))
                .Item("@usuario_llamada").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_capturo", System.Data.SqlDbType.VarChar, 15, "usuario_capturo"))
                .Item("@usuario_capturo").Value = Connection.GetValue(oData, "usuario_capturo")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_promesa_pago", System.Data.SqlDbType.DateTime, 8, "fecha_promesa_pago"))
                .Item("@fecha_promesa_pago").Value = Connection.GetValue(oData, "fecha_promesa_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco", System.Data.SqlDbType.Int, 4, "parentesco"))
                .Item("@parentesco").Value = Connection.GetValue(oData, "parentesco")

                .Add(New System.Data.SqlClient.SqlParameter("@telefono", System.Data.SqlDbType.VarChar, 25, "telefono"))
                .Item("@telefono").Value = Connection.GetValue(oData, "telefono")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_telefono", System.Data.SqlDbType.Char, 1, "tipo_telefono"))
                .Item("@tipo_telefono").Value = Connection.GetValue(oData, "tipo_telefono")

                .Add(New System.Data.SqlClient.SqlParameter("@id_mensaje", System.Data.SqlDbType.Int, 4, "id_mensaje"))
                .Item("@id_mensaje").Value = Connection.GetValue(oData, "id_mensaje")
                .Item("@mensaje_dejado").Value = Connection.GetValue(oData, "mensaje_dejado")
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje_recibido", System.Data.SqlDbType.Text, 0, "mensaje_recibido"))


            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "cliente", oCommand.Parameters.Item("@cliente").Value)
            Connection.SetValue(oData, "fecha_hora", oCommand.Parameters.Item("@fecha_hora").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_llamadas_clientes_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_llamadas_clientes_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Item("@cliente").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_hora", System.Data.SqlDbType.DateTime, 8, "fecha_hora"))
                .Item("@fecha_hora").Value = Connection.GetValue(oData, "fecha_hora")
                .Item("@fecha_hora").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = Connection.GetValue(oData, "tipo_llamada")

                .Add(New System.Data.SqlClient.SqlParameter("@persona_contesta", System.Data.SqlDbType.VarChar, 50, "persona_contesta"))
                .Item("@persona_contesta").Value = Connection.GetValue(oData, "persona_contesta")
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje_dejado", System.Data.SqlDbType.Text, 0, "mensaje_dejado"))

                .Item("@mensaje_recibido").Value = Connection.GetValue(oData, "mensaje_recibido")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_llamada", System.Data.SqlDbType.VarChar, 15, "usuario_llamada"))
                .Item("@usuario_llamada").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_capturo", System.Data.SqlDbType.VarChar, 15, "usuario_capturo"))
                .Item("@usuario_capturo").Value = Connection.GetValue(oData, "usuario_capturo")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_promesa_pago", System.Data.SqlDbType.DateTime, 8, "fecha_promesa_pago"))
                .Item("@fecha_promesa_pago").Value = Connection.GetValue(oData, "fecha_promesa_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco", System.Data.SqlDbType.Int, 4, "parentesco"))
                .Item("@parentesco").Value = Connection.GetValue(oData, "parentesco")

                .Add(New System.Data.SqlClient.SqlParameter("@telefono", System.Data.SqlDbType.VarChar, 25, "telefono"))
                .Item("@telefono").Value = Connection.GetValue(oData, "telefono")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_telefono", System.Data.SqlDbType.Char, 1, "tipo_telefono"))
                .Item("@tipo_telefono").Value = Connection.GetValue(oData, "tipo_telefono")

                .Add(New System.Data.SqlClient.SqlParameter("@id_mensaje", System.Data.SqlDbType.Int, 4, "id_mensaje"))
                .Item("@id_mensaje").Value = Connection.GetValue(oData, "id_mensaje")
                .Item("@mensaje_dejado").Value = Connection.GetValue(oData, "mensaje_dejado")
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje_recibido", System.Data.SqlDbType.Text, 0, "mensaje_recibido"))

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_llamadas_clientes_del(ByVal cliente As Long, ByVal fecha_hora As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_llamadas_clientes_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_hora", System.Data.SqlDbType.Datetime, 8, "fecha_hora"))
                .Item("@fecha_hora").Value = fecha_hora

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_llamadas_clientes_exs(ByVal cliente As Long, ByVal fecha_hora As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_llamadas_clientes_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_hora", System.Data.SqlDbType.Datetime, 8, "fecha_hora"))
                .Item("@fecha_hora").Value = fecha_hora

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_llamadas_clientes_sel(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_llamadas_clientes_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                '.Add(New System.Data.SqlClient.SqlParameter("@fecha_hora", System.Data.SqlDbType.DateTime, 8, "fecha_hora"))
                '.Item("@fecha_hora").Value = fecha_hora

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_llamadas_clientes_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_llamadas_clientes_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


