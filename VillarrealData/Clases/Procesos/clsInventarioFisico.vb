'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsInventarioFisico
'DATE:		08/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - InventarioFisico"
Public Class clsInventarioFisico
    Public Function sp_inventario_fisico_ins(ByRef inventario As Long, ByVal bodega As String, ByVal fecha As Date, ByVal status As String, ByVal observaciones As String, ByVal tipo_inventario As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
                .Item("@inventario").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_inventario", System.Data.SqlDbType.Char, 1, "tipo_inventario"))
                .Item("@tipo_inventario").Value = tipo_inventario
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            inventario = oCommand.Parameters.Item("@inventario").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_upd(ByRef inventario As Long, ByVal bodega As String, ByVal fecha As Date, ByVal status As String, ByVal observaciones As String, ByVal tipo_inventario As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
                '.Item("@inventario").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_inventario", System.Data.SqlDbType.Char, 1, "tipo_inventario"))
                .Item("@tipo_inventario").Value = tipo_inventario
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_del(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_exs(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_sel(ByVal inventario As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_grs(ByVal tipo_inventario As Char, Optional ByVal bodega As String = "-1") As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_inventario", System.Data.SqlDbType.Char, 1, "tipo_inventario"))
                .Item("@tipo_inventario").Value = tipo_inventario
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_inventario_fisico_ajustar(ByVal sucursal As Long, ByVal bodega As String, ByVal inventario As Long, ByVal fecha As DateTime, ByVal conceptoentrada As String, ByVal conceptosalida As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_inventario_fisico_ajustar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@inventario", System.Data.SqlDbType.Int, 4, "inventario"))
                .Item("@inventario").Value = inventario
                .Add(New System.Data.SqlClient.SqlParameter("@fechainventario", System.Data.SqlDbType.DateTime, 8))
                .Item("@fechainventario").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@conceptoentrada", System.Data.SqlDbType.VarChar, 5))
                .Item("@conceptoentrada").Value = conceptoentrada
                .Add(New System.Data.SqlClient.SqlParameter("@conceptosalida", System.Data.SqlDbType.VarChar, 5))
                .Item("@conceptosalida").Value = conceptosalida
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function

End Class
#End Region


