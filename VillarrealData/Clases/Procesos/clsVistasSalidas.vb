'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVistasSalidas
'DATE:		20/12/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - VistasSalidas"
Public Class clsVistasSalidas
    Public Function sp_vistas_salidas_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = Connection.GetValue(oData, "folio_vista_salida")
                .Item("@folio_vista_salida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_devolucion", System.Data.SqlDbType.Datetime, 8, "fecha_devolucion"))
                .Item("@fecha_devolucion").Value = Connection.GetValue(oData, "fecha_devolucion")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@persona_recibe", System.Data.SqlDbType.VarChar, 50, "persona_recibe"))
                .Item("@persona_recibe").Value = Connection.GetValue(oData, "persona_recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@placas", System.Data.SqlDbType.VarChar, 10, "placas"))
                .Item("@placas").Value = Connection.GetValue(oData, "placas")
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = Connection.GetValue(oData, "bodeguero")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_autorizo", System.Data.SqlDbType.VarChar, 15, "usuario_autorizo"))
                .Item("@usuario_autorizo").Value = Connection.GetValue(oData, "usuario_autorizo")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@vista_entregada", System.Data.SqlDbType.Bit, 1, "vista_entregada"))
                .Item("@vista_entregada").Value = Connection.GetValue(oData, "vista_entregada")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio_vista_salida", oCommand.Parameters.Item("@folio_vista_salida").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = Connection.GetValue(oData, "folio_vista_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_devolucion", System.Data.SqlDbType.Datetime, 8, "fecha_devolucion"))
                .Item("@fecha_devolucion").Value = Connection.GetValue(oData, "fecha_devolucion")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@persona_recibe", System.Data.SqlDbType.VarChar, 50, "persona_recibe"))
                .Item("@persona_recibe").Value = Connection.GetValue(oData, "persona_recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@placas", System.Data.SqlDbType.VarChar, 10, "placas"))
                .Item("@placas").Value = Connection.GetValue(oData, "placas")
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = Connection.GetValue(oData, "bodeguero")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_autorizo", System.Data.SqlDbType.VarChar, 15, "usuario_autorizo"))
                .Item("@usuario_autorizo").Value = Connection.GetValue(oData, "usuario_autorizo")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@vista_entregada", System.Data.SqlDbType.Bit, 1, "vista_entregada"))
                .Item("@vista_entregada").Value = Connection.GetValue(oData, "vista_entregada")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_del(ByVal folio_vista_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_exs(ByVal folio_vista_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_sel(ByVal folio_vista_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_grl(ByVal vista_entregada As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_grl]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@vista_entregada", System.Data.SqlDbType.Bit, 1, "vista_entregada"))
                .Item("@vista_entregada").Value = vista_entregada
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


