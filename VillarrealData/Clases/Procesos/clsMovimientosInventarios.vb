'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosInventarios
'DATE:		15/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - MovimientosInventarios"
Public Class clsMovimientosInventarios
    Public Function sp_movimientos_inventarios_ins(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha As DateTime, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_referencia", System.Data.SqlDbType.Int, 4, "sucursal_referencia"))
                .Item("@sucursal_referencia").Value = sucursal_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = concepto_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio = oCommand.Parameters.Item("@folio").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_upd(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha As DateTime, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_referencia", System.Data.SqlDbType.Int, 4, "sucursal_referencia"))
                .Item("@sucursal_referencia").Value = sucursal_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = concepto_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_del(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_del]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                '.Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_exs(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_movimientos_inventarios_sel(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_sel]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_movimientos_inventarios_grs() As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_movimientos_inventarios_grs]"
    '        With oCommand.Parameters

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_movimientos_inventarios_grs(ByVal bodega As String, ByVal concepto As String, ByVal fechai As DateTime, ByVal fechaf As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_inventarios_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@fechai", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fechai").Value = fechai
                .Add(New System.Data.SqlClient.SqlParameter("@fechaf", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fechaf").Value = fechaf
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function



End Class
#End Region


