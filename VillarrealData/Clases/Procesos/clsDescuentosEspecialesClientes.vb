Imports Dipros.Utils

Public Class clsDescuentosEspecialesClientes

    Public Function sp_descuentos_especiales_clientes_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = Connection.GetValue(oData, "folio_descuento")
                .Item("@folio_descuento").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")

                .Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Money, 8, "monto"))
                .Item("@monto").Value = Connection.GetValue(oData, "monto")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_descuento", System.Data.SqlDbType.Float, 8, "porcentaje_descuento"))
                .Item("@porcentaje_descuento").Value = Connection.GetValue(oData, "porcentaje_descuento")
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@utilizada", System.Data.SqlDbType.Bit, 1, "utilizada"))
                .Item("@utilizada").Value = Connection.GetValue(oData, "utilizada")

                .Add(New System.Data.SqlClient.SqlParameter("@usuario_autoriza", System.Data.SqlDbType.VarChar, 15, "usuario_autoriza"))
                .Item("@usuario_autoriza").Value = Connection.GetValue(oData, "usuario_autoriza")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_captura", System.Data.SqlDbType.VarChar, 15, "usuario_captura"))
                .Item("@usuario_captura").Value = Connection.GetValue(oData, "usuario_captura")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_captura", System.Data.SqlDbType.DateTime, 8, "fecha_captura"))
                .Item("@fecha_captura").Value = Connection.GetValue(oData, "fecha_captura")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio_descuento", oCommand.Parameters.Item("@folio_descuento").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = Connection.GetValue(oData, "folio_descuento")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")

                .Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Money, 8, "monto"))
                .Item("@monto").Value = Connection.GetValue(oData, "monto")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_descuento", System.Data.SqlDbType.Float, 8, "porcentaje_descuento"))
                .Item("@porcentaje_descuento").Value = Connection.GetValue(oData, "porcentaje_descuento")
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@utilizada", System.Data.SqlDbType.Bit, 1, "utilizada"))
                .Item("@utilizada").Value = Connection.GetValue(oData, "utilizada")

                .Add(New System.Data.SqlClient.SqlParameter("@usuario_autoriza", System.Data.SqlDbType.VarChar, 15, "usuario_autoriza"))
                .Item("@usuario_autoriza").Value = Connection.GetValue(oData, "usuario_autoriza")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_captura", System.Data.SqlDbType.VarChar, 15, "usuario_captura"))
                .Item("@usuario_captura").Value = Connection.GetValue(oData, "usuario_captura")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_captura", System.Data.SqlDbType.DateTime, 8, "fecha_captura"))
                .Item("@fecha_captura").Value = Connection.GetValue(oData, "fecha_captura")


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_actualiza_ventas_upd(ByVal folio_descuento As Long, ByVal utilizada As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_actualiza_ventas_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento
                .Add(New System.Data.SqlClient.SqlParameter("@utilizada", System.Data.SqlDbType.Bit, 1, "utilizada"))
                .Item("@utilizada").Value = utilizada
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_del(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = Connection.GetValue(oData, "folio_descuento")


            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_sel(ByVal folio_descuento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_lkp(ByVal cliente As Long, ByVal utilizada As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_lkp]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@utilizada", System.Data.SqlDbType.Bit, 1, "utilizada"))
                .Item("@utilizada").Value = utilizada
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
