'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVistasSalidasDetalle
'DATE:		20/12/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - VistasSalidasDetalle"
Public Class clsVistasSalidasDetalle
    Public Function sp_vistas_salidas_detalle_ins(ByRef oData As DataSet, ByRef folio_vista_salida As Long, ByVal facturada As Boolean, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal concepto_inventario_salida As String, ByVal folio_inventario_salida As Long, ByVal partida As Long, ByVal numero_serie As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida
                '.Item("@folio_vista_salida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida 'Connection.GetValue(oData, "partida")
                '.Item("@partida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie 'Connection.GetValue(oData, "numero_serie")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_salida", System.Data.SqlDbType.VarChar, 5, "bodega_salida"))
                .Item("@bodega_salida").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@facturada", System.Data.SqlDbType.Bit, 1, "facturada"))
                .Item("@facturada").Value = facturada
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_inventario_salida", System.Data.SqlDbType.VarChar, 5, "concepto_inventario_salida"))
                .Item("@concepto_inventario_salida").Value = concepto_inventario_salida
                .Add(New System.Data.SqlClient.SqlParameter("@folio_inventario_salida", System.Data.SqlDbType.Int, 4, "folio_inventario_salida"))
                .Item("@folio_inventario_salida").Value = folio_inventario_salida
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio_vista_salida = oCommand.Parameters.Item("@folio_vista_salida").Value
            Connection.SetValue(oData, "partida", oCommand.Parameters.Item("@partida").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_detalle_upd(ByRef oData As DataSet, ByRef folio_vista_salida As Long, ByVal facturada As Boolean, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal concepto_inventario_salida As String, ByVal folio_inventario_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida
                .Item("@folio_vista_salida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = Connection.GetValue(oData, "numero_serie")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_salida", System.Data.SqlDbType.VarChar, 5, "bodega_salida"))
                .Item("@bodega_salida").Value = Connection.GetValue(oData, "bodega_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@facturada", System.Data.SqlDbType.Bit, 1, "facturada"))
                .Item("@facturada").Value = facturada
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_inventario_salida", System.Data.SqlDbType.VarChar, 5, "concepto_inventario_salida"))
                .Item("@concepto_inventario_salida").Value = concepto_inventario_salida
                .Add(New System.Data.SqlClient.SqlParameter("@folio_inventario_salida", System.Data.SqlDbType.Int, 4, "folio_inventario_salida"))
                .Item("@folio_inventario_salida").Value = folio_inventario_salida
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_detalle_del(ByVal folio_vista_salida As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_detalle_exs(ByVal folio_vista_salida As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_detalle_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_vistas_salidas_detalle_sel(ByVal folio_vista_salida As Long, ByVal partida As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_vistas_salidas_detalle_sel]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
    '            .Item("@folio_vista_salida").Value = folio_vista_salida
    '            .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
    '            .Item("@partida").Value = partida

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_vistas_salidas_detalle_grs(ByVal folio_vista_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_salidas_detalle_factura_upd(ByVal folio_vista_salida As Long, ByVal partida As Long, ByVal articulo As Long, ByVal facturada As Boolean, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_salidas_detalle_factura_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@facturada", System.Data.SqlDbType.Bit, 1, "facturada"))
                .Item("@facturada").Value = facturada
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function





    ''csequera 19 enero 2008

    'Public Function sp_vistas_salidas_busca_folio_sel(ByVal folio As Long, ByVal partida As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_vistas_salidas_busca_folio_sel]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = folio
    '            .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
    '            .Item("@partida").Value = partida
    '        End With
    '        Connection.Execute(oCommand)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function
End Class
#End Region


