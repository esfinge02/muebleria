'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesRecuperacion
'DATE:		11/10/2007 0:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - OrdenesRecuperacion"
Public Class clsOrdenesRecuperacion
    Public Function sp_ordenes_recuperacion_ins(ByRef oData As DataSet, ByVal sucursal As Long, ByVal elabora As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@elabora", System.Data.SqlDbType.VarChar, 15, "elabora"))
                .Item("@elabora").Value = Connection.User 'elabora
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_recibe", System.Data.SqlDbType.VarChar, 5, "bodega_recibe"))
                .Item("@bodega_recibe").Value = Connection.GetValue(oData, "bodega_recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@persona_recibe", System.Data.SqlDbType.VarChar, 15, "persona_recibe"))
                .Item("@persona_recibe").Value = Connection.GetValue(oData, "persona_recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_recibe", System.Data.SqlDbType.DateTime, 8, "fecha_recibe"))
                .Item("@fecha_recibe").Value = Connection.GetValue(oData, "fecha_recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = Connection.GetValue(oData, "sucursal_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = Connection.GetValue(oData, "serie_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@persona_recoje", System.Data.SqlDbType.VarChar, 15, "persona_recoje"))
                .Item("@persona_recoje").Value = Connection.GetValue(oData, "persona_recoje")
                .Add(New System.Data.SqlClient.SqlParameter("@persona_inspecciona", System.Data.SqlDbType.VarChar, 15, "persona_inspecciona"))
                .Item("@persona_inspecciona").Value = Connection.GetValue(oData, "persona_inspecciona")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inspecciona", System.Data.SqlDbType.DateTime, 8, "fecha_inspecciona"))
                .Item("@fecha_inspecciona").Value = Connection.GetValue(oData, "fecha_inspecciona")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_inspeccion", System.Data.SqlDbType.Text, 0, "observaciones_inspeccion"))
                .Item("@observaciones_inspeccion").Value = Connection.GetValue(oData, "observaciones_inspeccion")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@quien", System.Data.SqlDbType.VarChar, 15, "quien"))
                .Item("@quien").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio", oCommand.Parameters.Item("@folio").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_upd(ByRef oData As DataSet, ByVal presona_recibe As String, ByVal persona_recoje As String, ByVal persona_inspecciona As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                '.Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                '.Add(New System.Data.SqlClient.SqlParameter("@elabora", System.Data.SqlDbType.VarChar, 15, "elabora"))
                '.Item("@elabora").Value = Connection.GetValue(oData, "elabora")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_recibe", System.Data.SqlDbType.VarChar, 5, "bodega_recibe"))
                .Item("@bodega_recibe").Value = Connection.GetValue(oData, "bodega_recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@persona_recibe", System.Data.SqlDbType.VarChar, 15, "persona_recibe"))
                .Item("@persona_recibe").Value = presona_recibe ' Connection.GetValue(oData, "persona_recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_recibe", System.Data.SqlDbType.DateTime, 8, "fecha_recibe"))
                .Item("@fecha_recibe").Value = Connection.GetValue(oData, "fecha_recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = Connection.GetValue(oData, "sucursal_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = Connection.GetValue(oData, "serie_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@persona_recoje", System.Data.SqlDbType.VarChar, 15, "persona_recoje"))
                .Item("@persona_recoje").Value = persona_recoje 'Connection.GetValue(oData, "persona_recoje")
                .Add(New System.Data.SqlClient.SqlParameter("@persona_inspecciona", System.Data.SqlDbType.VarChar, 15, "persona_inspecciona"))
                .Item("@persona_inspecciona").Value = persona_inspecciona ' Connection.GetValue(oData, "persona_inspecciona")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inspecciona", System.Data.SqlDbType.DateTime, 8, "fecha_inspecciona"))
                .Item("@fecha_inspecciona").Value = Connection.GetValue(oData, "fecha_inspecciona")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_inspeccion", System.Data.SqlDbType.Text, 0, "observaciones_inspeccion"))
                .Item("@observaciones_inspeccion").Value = Connection.GetValue(oData, "observaciones_inspeccion")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@quien", System.Data.SqlDbType.VarChar, 15, "quien"))
                .Item("@quien").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_del(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@quien", System.Data.SqlDbType.VarChar, 15, "quien"))
                .Item("@quien").Value = Connection.User

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_exs(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_sel(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


