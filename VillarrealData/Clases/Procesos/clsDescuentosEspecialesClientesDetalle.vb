Imports Dipros.Utils

Public Class clsDescuentosEspecialesClientesDetalle

    Public Function sp_descuentos_especiales_clientes_detalle_ins(ByVal folio_descuento As Long, ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Int, 4, "cantidad"))
                .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_detalle_upd(ByVal folio_descuento As Long, ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Int, 4, "cantidad"))
                .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_detalle_del(ByVal folio_descuento As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_detalle_grs(ByVal folio_descuento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_descuentos_especiales_clientes_detalle_sel(ByVal folio_descuento As Long, ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_descuentos_especiales_clientes_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
