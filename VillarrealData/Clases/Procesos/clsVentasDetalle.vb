'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVentasDetalle
'DATE:		10/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - VentasDetalle"
Public Class clsVentasDetalle

    '/* csequera enero 2008
    Public Function sp_ventas_detalle_ins(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long, ByVal folio_movimiento As Long, ByVal tipo_precio_venta As Long, ByVal folio_descuento_especial_programado As Long, ByVal precio_descuento_especial_programado As Double, ByVal precio_pactado As Double, ByVal descripcion_anterior As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Item("@partida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
                .Item("@cantidad").Value = 1 'Connection.GetValue(oData, "cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@preciounitario", System.Data.SqlDbType.Money, 8, "preciounitario"))
                .Item("@preciounitario").Value = Connection.GetValue(oData, "preciounitario")
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = Connection.GetValue(oData, "precio_contado")
                .Add(New System.Data.SqlClient.SqlParameter("@sobrepedido", System.Data.SqlDbType.Bit, 1, "sobrepedido"))
                .Item("@sobrepedido").Value = Connection.GetValue(oData, "sobrepedido")
                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@costo").Value = Connection.GetValue(oData, "costo")
                .Add(New System.Data.SqlClient.SqlParameter("@surtido", System.Data.SqlDbType.Bit, 1, "surtido"))
                .Item("@surtido").Value = Connection.GetValue(oData, "surtido")
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Bit, 1, "reparto"))
                .Item("@reparto").Value = Connection.GetValue(oData, "reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_movimiento", System.Data.SqlDbType.Int, 4, "folio_movimiento"))
                .Item("@folio_movimiento").Value = folio_movimiento
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_precio_venta", System.Data.SqlDbType.Int, 4, "tipo_precio_venta"))
                .Item("@tipo_precio_venta").Value = tipo_precio_venta
                .Add(New System.Data.SqlClient.SqlParameter("@partida_vistas", System.Data.SqlDbType.Int, 4, "partida_vistas"))
                .Item("@partida_vistas").Value = Connection.GetValue(oData, "partida_vistas")

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_anterior", System.Data.SqlDbType.Text, 0, "descripcion_anterior"))
                .Item("@descripcion_anterior").Value = descripcion_anterior

                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento_especial_programado", System.Data.SqlDbType.Int, 4, "folio_descuento_especial_programado"))
                .Item("@folio_descuento_especial_programado").Value = folio_descuento_especial_programado
                .Add(New System.Data.SqlClient.SqlParameter("@precio_descuento_especial_programado", System.Data.SqlDbType.Money, 8, "precio_descuento_especial_programado"))
                .Item("@precio_descuento_especial_programado").Value = precio_descuento_especial_programado

                .Add(New System.Data.SqlClient.SqlParameter("@precio_pactado", System.Data.SqlDbType.Money, 8, "precio_pactado"))
                .Item("@precio_pactado").Value = precio_pactado

                .Add(New System.Data.SqlClient.SqlParameter("@impresa_etiqueta_vendido", System.Data.SqlDbType.Bit, 1, "impresa_etiqueta_vendido"))
                .Item("@impresa_etiqueta_vendido").Value = 0

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "partida", oCommand.Parameters.Item("@partida").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalle_ins(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long, ByVal articulo As Long, ByVal preciounitario As Double, _
    ByVal precio_contado As Double, ByVal sobrepedido As Boolean, ByVal costo As Double, ByVal surtido As Boolean, ByVal reparto As Boolean, ByVal bodega As String, ByVal folio_movimiento As Long, ByVal tipo_precio_venta As Long, ByVal partida_vistas As Long, _
    ByVal folio_descuento_especial_programado As Long, ByVal precio_descuento_especial_programado As Double, ByVal precio_pactado As Double, ByVal descripcion_anterior As String, ByVal impresa_etiqueta_vendido As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Item("@partida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
                .Item("@cantidad").Value = 1 'Connection.GetValue(oData, "cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@preciounitario", System.Data.SqlDbType.Money, 8, "preciounitario"))
                .Item("@preciounitario").Value = preciounitario
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = precio_contado
                .Add(New System.Data.SqlClient.SqlParameter("@sobrepedido", System.Data.SqlDbType.Bit, 1, "sobrepedido"))
                .Item("@sobrepedido").Value = sobrepedido
                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@costo").Value = costo
                .Add(New System.Data.SqlClient.SqlParameter("@surtido", System.Data.SqlDbType.Bit, 1, "surtido"))
                .Item("@surtido").Value = surtido
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Bit, 1, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@folio_movimiento", System.Data.SqlDbType.Int, 4, "folio_movimiento"))
                .Item("@folio_movimiento").Value = folio_movimiento
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_precio_venta", System.Data.SqlDbType.Int, 4, "tipo_precio_venta"))
                .Item("@tipo_precio_venta").Value = tipo_precio_venta
                .Add(New System.Data.SqlClient.SqlParameter("@partida_vistas", System.Data.SqlDbType.Int, 4, "partida_vistas"))
                .Item("@partida_vistas").Value = partida_vistas

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_anterior", System.Data.SqlDbType.Text, 0, "descripcion_anterior"))
                .Item("@descripcion_anterior").Value = descripcion_anterior

                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento_especial_programado", System.Data.SqlDbType.Int, 4, "folio_descuento_especial_programado"))
                .Item("@folio_descuento_especial_programado").Value = folio_descuento_especial_programado
                .Add(New System.Data.SqlClient.SqlParameter("@precio_descuento_especial_programado", System.Data.SqlDbType.Money, 8, "precio_descuento_especial_programado"))
                .Item("@precio_descuento_especial_programado").Value = precio_descuento_especial_programado

                .Add(New System.Data.SqlClient.SqlParameter("@precio_pactado", System.Data.SqlDbType.Money, 8, "precio_pactado"))
                .Item("@precio_pactado").Value = precio_pactado

                .Add(New System.Data.SqlClient.SqlParameter("@impresa_etiqueta_vendido", System.Data.SqlDbType.Bit, 1, "impresa_etiqueta_vendido"))
                .Item("@impresa_etiqueta_vendido").Value = impresa_etiqueta_vendido

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalle_upd(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal precio_pactado As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
                .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@preciounitario", System.Data.SqlDbType.Money, 8, "preciounitario"))
                .Item("@preciounitario").Value = Connection.GetValue(oData, "preciounitario")
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = Connection.GetValue(oData, "total")
                .Add(New System.Data.SqlClient.SqlParameter("@sobrepedido", System.Data.SqlDbType.Bit, 1, "sobrepedido"))
                .Item("@sobrepedido").Value = Connection.GetValue(oData, "sobrepedido")
                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@costo").Value = Connection.GetValue(oData, "costo")
                .Add(New System.Data.SqlClient.SqlParameter("@surtido", System.Data.SqlDbType.Bit, 1, "surtido"))
                .Item("@surtido").Value = Connection.GetValue(oData, "surtido")
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Bit, 1, "reparto"))
                .Item("@reparto").Value = Connection.GetValue(oData, "reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@precio_pactado", System.Data.SqlDbType.Money, 8, "precio_pactado"))
                .Item("@precio_pactado").Value = precio_pactado
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    '*/ csequera enero 2008
    Public Function sp_ventas_detalle_del(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalle_exs(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalle_sel(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_ventas_detalle_ordenes_recuperacion_sel(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_ordenes_recuperacion_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalle_grs(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalles_repartos_pendientes_grs(ByVal sucursal As Long, ByVal bodega As String, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalles_repartos_pendientes_grs]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                '.Item("@sucursal").Value = sucursal
                '.Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                '.Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalles_repartos_pendientes_filtro_grs(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalles_repartos_pendientes_filtro_grs]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                '.Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = -1
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalle_articulos_existencias_grs(ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_articulos_existencias_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalle_impresa_etiqueta_vendido_upd(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_impresa_etiqueta_vendido_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_ventas_cajas_articulos_factura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_cajas_articulos_factura]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_cambia_bodega_venta(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long, ByVal bodega_anterior As String, ByVal bodega_nueva As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cambia_bodega_venta]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_anterior", System.Data.SqlDbType.VarChar, 5, "bodega_anterior"))
                .Item("@bodega_anterior").Value = bodega_anterior
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_nueva", System.Data.SqlDbType.VarChar, 5, "bodega_nueva"))
                .Item("@bodega_nueva").Value = bodega_nueva
            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_detalle_folio_venta_detalle_sel(ByVal folio_venta_detalle As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_detalle_folio_venta_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_venta_detalle", System.Data.SqlDbType.Int, 4, "folio_venta_detalle"))
                .Item("@folio_venta_detalle").Value = folio_venta_detalle

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


