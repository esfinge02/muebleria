'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVentas
'DATE:		10/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Ventas"
Public Class clsVentas
    Public Function sp_ventas_ins(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal fecha As DateTime, ByVal importe As Double, ByVal menos As Double, ByVal enganche As Double, _
    ByVal interes As Double, ByVal plan As Long, ByVal fecha_primer_documento As Date, ByVal numero_documentos As Long, ByVal importe_documentos As Double, ByVal importe_ultimo_documento As Double, _
    ByVal enganche_en_documento As Boolean, ByVal fecha_enganche_documento As Date, ByVal importe_enganche_documento As Double, ByVal liquida_vencimiento As Boolean, ByVal monto_liquida_vencimiento As Double, _
    ByVal enganche_en_menos As Boolean, ByVal sucursal_dependencia As Boolean, ByVal folio_plantilla As Long, ByVal quincena_inicio As Long, ByVal ano_inicio As Long, ByVal punto_venta As Long, ByVal FolioDescuentoCliente As Long, _
    ByVal aplica_enganche As Boolean, ByVal paquete As Long, ByVal convenio As Long, ByVal TotalAnticipos As Double, ByVal Apartado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 80, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 50, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.VarChar, 50, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono1", System.Data.SqlDbType.VarChar, 13, "telefono1"))
                .Item("@telefono1").Value = Connection.GetValue(oData, "telefono1")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono2", System.Data.SqlDbType.VarChar, 13, "telefono2"))
                .Item("@telefono2").Value = Connection.GetValue(oData, "telefono2")
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 13, "fax"))
                .Item("@fax").Value = Connection.GetValue(oData, "fax")
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@tipoventa", System.Data.SqlDbType.Char, 1, "tipoventa"))
                .Item("@tipoventa").Value = Connection.GetValue(oData, "tipoventa")
                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1, "ivadesglosado"))
                .Item("@ivadesglosado").Value = Connection.GetValue(oData, "ivadesglosado")

                .Add(New System.Data.SqlClient.SqlParameter("@pedimento", System.Data.SqlDbType.VarChar, 30, "pedimento"))
                .Item("@pedimento").Value = Connection.GetValue(oData, "pedimento")
                .Add(New System.Data.SqlClient.SqlParameter("@aduana", System.Data.SqlDbType.VarChar, 50, "aduana"))
                .Item("@aduana").Value = Connection.GetValue(oData, "aduana")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_importacion", System.Data.SqlDbType.DateTime, 8, "fecha_importacion"))
                .Item("@fecha_importacion").Value = Connection.GetValue(oData, "fecha_importacion")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_reparto", System.Data.SqlDbType.VarChar, 80, "domicilio_reparto"))
                .Item("@domicilio_reparto").Value = Connection.GetValue(oData, "domicilio_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_reparto", System.Data.SqlDbType.VarChar, 50, "colonia_reparto"))
                .Item("@colonia_reparto").Value = Connection.GetValue(oData, "colonia_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_reparto", System.Data.SqlDbType.VarChar, 50, "ciudad_reparto"))
                .Item("@ciudad_reparto").Value = Connection.GetValue(oData, "ciudad_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@estado_reparto", System.Data.SqlDbType.VarChar, 50, "estado_reparto"))
                .Item("@estado_reparto").Value = Connection.GetValue(oData, "estado_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_reparto", System.Data.SqlDbType.VarChar, 13, "telefono_reparto"))
                .Item("@telefono_reparto").Value = Connection.GetValue(oData, "telefono_reparto")

                .Add(New System.Data.SqlClient.SqlParameter("@horario_reparto", System.Data.SqlDbType.Char, 1, "horario_reparto"))
                .Item("@horario_reparto").Value = Connection.GetValue(oData, "horario_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_reparto", System.Data.SqlDbType.DateTime, 8, "fecha_reparto"))
                .Item("@fecha_reparto").Value = Connection.GetValue(oData, "fecha_reparto")

                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_reparto", System.Data.SqlDbType.Text, 0, "observaciones_reparto"))
                .Item("@observaciones_reparto").Value = Connection.GetValue(oData, "observaciones_reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_venta", System.Data.SqlDbType.Text, 0, "observaciones_venta"))
                .Item("@observaciones_venta").Value = Connection.GetValue(oData, "observaciones_venta")

                If Connection.GetValue(oData, "tipoventa") = "D" Then   'CONTADO
                    .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                    .Item("@importe").Value = Connection.GetValue(oData, "total") - menos - TotalAnticipos
                    .Add(New System.Data.SqlClient.SqlParameter("@menos", System.Data.SqlDbType.Money, 8, "menos"))
                    .Item("@menos").Value = 0 ' El menos va en 0 Cero
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Money, 8, "enganche"))
                    .Item("@enganche").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Money, 8, "interes"))
                    .Item("@interes").Value = System.DBNull.Value

                    .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                    .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                    .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                    .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                    .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                    .Item("@total").Value = Connection.GetValue(oData, "total") - menos - TotalAnticipos

                    .Add(New System.Data.SqlClient.SqlParameter("@plan", System.Data.SqlDbType.Int, 4, "plan"))
                    .Item("@plan").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_primer_documento", System.Data.SqlDbType.DateTime, 8, "fecha_primer_documento"))
                    .Item("@fecha_primer_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
                    .Item("@numero_documentos").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_documentos", System.Data.SqlDbType.Money, 8, "importe_documentos"))
                    .Item("@importe_documentos").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_ultimo_documento", System.Data.SqlDbType.Money, 8, "importe_ultimo_documento"))
                    .Item("@importe_ultimo_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_documento", System.Data.SqlDbType.Bit, 1, "enganche_en_documento"))
                    .Item("@enganche_en_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_enganche_documento", System.Data.SqlDbType.DateTime, 8, "fecha_enganche_documento"))
                    .Item("@fecha_enganche_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_enganche_documento", System.Data.SqlDbType.Money, 8, "importe_enganche_documento"))
                    .Item("@importe_enganche_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@liquida_vencimiento", System.Data.SqlDbType.Bit, 1, "liquida_vencimiento"))
                    .Item("@liquida_vencimiento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@monto_liquida_vencimiento", System.Data.SqlDbType.Money, 8, "monto_liquida_vencimiento"))
                    .Item("@monto_liquida_vencimiento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Bit, 1, "enganche_en_menos"))
                    .Item("@enganche_en_menos").Value = System.DBNull.Value
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                    .Item("@importe").Value = importe
                    .Add(New System.Data.SqlClient.SqlParameter("@menos", System.Data.SqlDbType.Money, 8, "menos"))
                    .Item("@menos").Value = menos
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Money, 8, "enganche"))
                    .Item("@enganche").Value = enganche
                    .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Money, 8, "interes"))
                    .Item("@interes").Value = interes
                    .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                    .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                    .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                    .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                    .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                    .Item("@total").Value = Connection.GetValue(oData, "total")

                    .Add(New System.Data.SqlClient.SqlParameter("@plan", System.Data.SqlDbType.Int, 4, "plan"))
                    .Item("@plan").Value = plan
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_primer_documento", System.Data.SqlDbType.DateTime, 8, "fecha_primer_documento"))
                    If fecha_primer_documento = CDate("01/01/1900") Then
                        .Item("@fecha_primer_documento").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_primer_documento").Value = fecha_primer_documento
                    End If
                    .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
                    .Item("@numero_documentos").Value = numero_documentos
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_documentos", System.Data.SqlDbType.Money, 8, "importe_documentos"))
                    .Item("@importe_documentos").Value = importe_documentos
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_ultimo_documento", System.Data.SqlDbType.Money, 8, "importe_ultimo_documento"))
                    .Item("@importe_ultimo_documento").Value = importe_ultimo_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_documento", System.Data.SqlDbType.Bit, 1, "enganche_en_documento"))
                    .Item("@enganche_en_documento").Value = enganche_en_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_enganche_documento", System.Data.SqlDbType.DateTime, 8, "fecha_enganche_documento"))
                    If fecha_enganche_documento = CDate("01/01/1900") Then
                        .Item("@fecha_enganche_documento").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_enganche_documento").Value = fecha_enganche_documento
                    End If
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_enganche_documento", System.Data.SqlDbType.Money, 8, "importe_enganche_documento"))
                    .Item("@importe_enganche_documento").Value = importe_enganche_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@liquida_vencimiento", System.Data.SqlDbType.Bit, 1, "liquida_vencimiento"))
                    .Item("@liquida_vencimiento").Value = liquida_vencimiento
                    .Add(New System.Data.SqlClient.SqlParameter("@monto_liquida_vencimiento", System.Data.SqlDbType.Money, 8, "monto_liquida_vencimiento"))
                    .Item("@monto_liquida_vencimiento").Value = monto_liquida_vencimiento
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Bit, 1, "enganche_en_menos"))
                    .Item("@enganche_en_menos").Value = enganche_en_menos
                End If

                If sucursal_dependencia Then
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_pedido", System.Data.SqlDbType.DateTime, 8, "fecha_pedido"))
                    .Item("@fecha_pedido").Value = Connection.GetValue(oData, "fecha_pedido")
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_pedido", System.Data.SqlDbType.Int, 4, "folio_pedido"))
                    .Item("@folio_pedido").Value = Connection.GetValue(oData, "folio_pedido")
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_plantilla", System.Data.SqlDbType.Int, 4, "folio_plantilla"))
                    .Item("@folio_plantilla").Value = folio_plantilla
                    .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                    .Item("@quincena_inicio").Value = quincena_inicio
                    .Add(New System.Data.SqlClient.SqlParameter("@ano_inicio", System.Data.SqlDbType.Int, 4, "ano_inicio"))
                    .Item("@ano_inicio").Value = ano_inicio
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_pedido", System.Data.SqlDbType.DateTime, 8, "fecha_pedido"))
                    .Item("@fecha_pedido").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_pedido", System.Data.SqlDbType.Int, 4, "folio_pedido"))
                    .Item("@folio_pedido").Value = -1
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_plantilla", System.Data.SqlDbType.Int, 4, "folio_plantilla"))
                    .Item("@folio_plantilla").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                    .Item("@quincena_inicio").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@ano_inicio", System.Data.SqlDbType.Int, 4, "ano_inicio"))
                    .Item("@ano_inicio").Value = System.DBNull.Value
                End If
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = Connection.GetValue(oData, "cotizacion")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = Connection.GetValue(oData, "folio_vista_salida")

                .Add(New System.Data.SqlClient.SqlParameter("@punto_venta", System.Data.SqlDbType.Int, 4, "punto_venta"))
                .Item("@punto_venta").Value = punto_venta

                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento_especial_clientes", System.Data.SqlDbType.Int, 4, "folio_descuento_especial_clientes"))
                .Item("@folio_descuento_especial_clientes").Value = FolioDescuentoCliente


                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura_cancelada", System.Data.SqlDbType.Int, 4, "sucursal_factura_cancelada"))
                .Item("@sucursal_factura_cancelada").Value = Connection.GetValue(oData, "sucursal_factura_cancelada")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_cancelada", System.Data.SqlDbType.Char, 3, "serie_factura_cancelada"))
                .Item("@serie_factura_cancelada").Value = Connection.GetValue(oData, "serie_factura_cancelada")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_cancelada", System.Data.SqlDbType.Int, 4, "folio_factura_cancelada"))
                .Item("@folio_factura_cancelada").Value = Connection.GetValue(oData, "folio_factura_cancelada")

                .Add(New System.Data.SqlClient.SqlParameter("@aplica_enganche", System.Data.SqlDbType.Bit, 1, "aplica_enganche"))
                .Item("@aplica_enganche").Value = aplica_enganche

                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = paquete

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@facturacion_especial", System.Data.SqlDbType.Bit, 1, "facturacion_especial"))
                .Item("@facturacion_especial").Value = Connection.GetValue(oData, "facturacion_especial")

                .Add(New System.Data.SqlClient.SqlParameter("@es_anticipo", System.Data.SqlDbType.Bit, 1, "es_anticipo"))
                .Item("@es_anticipo").Value = False

                .Add(New System.Data.SqlClient.SqlParameter("@apartado", System.Data.SqlDbType.Bit, 1, "apartado"))
                .Item("@apartado").Value = Apartado


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio", oCommand.Parameters.Item("@folio").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function sp_ventas_ins(ByVal sucursal As Long, ByVal serie As String, ByRef folio As Long, ByVal fecha As DateTime, ByVal vendedor As Long, ByVal cliente As Long, ByVal domicilio As String, ByVal colonia As String, ByVal ciudad As String, ByVal municipio As String, _
        ByVal cp As Long, ByVal estado As String, ByVal telefono1 As String, ByVal telefono2 As String, ByVal fax As String, ByVal curp As String, ByVal rfc As String, ByVal tipoventa As Char, ByVal ivadesglosado As Boolean, ByVal pedimento As String, ByVal aduana As String, _
        ByVal importe As Double, ByVal menos As Double, ByVal enganche As Double, ByVal subtotal As Double, ByVal impuesto As Double, ByVal total As Double, _
        ByVal interes As Double, ByVal plan As Long, ByVal fecha_primer_documento As Date, ByVal numero_documentos As Long, ByVal importe_documentos As Double, ByVal importe_ultimo_documento As Double, _
        ByVal enganche_en_documento As Boolean, ByVal fecha_enganche_documento As Date, ByVal importe_enganche_documento As Double, ByVal liquida_vencimiento As Boolean, ByVal monto_liquida_vencimiento As Double, _
        ByVal enganche_en_menos As Boolean, ByVal sucursal_dependencia As Boolean, ByVal folio_plantilla As Long, ByVal quincena_inicio As Long, ByVal ano_inicio As Long, ByVal punto_venta As Long, ByVal FolioDescuentoCliente As Long, _
        ByVal aplica_enganche As Boolean, ByVal paquete As Long, ByVal convenio As Long, ByVal apartado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Item("@folio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 80, "domicilio"))
                .Item("@domicilio").Value = domicilio
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 50, "colonia"))
                .Item("@colonia").Value = colonia
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
                .Item("@ciudad").Value = ciudad
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.VarChar, 50, "municipio"))
                .Item("@municipio").Value = municipio
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = cp
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
                .Item("@estado").Value = estado
                .Add(New System.Data.SqlClient.SqlParameter("@telefono1", System.Data.SqlDbType.VarChar, 13, "telefono1"))
                .Item("@telefono1").Value = telefono1
                .Add(New System.Data.SqlClient.SqlParameter("@telefono2", System.Data.SqlDbType.VarChar, 13, "telefono2"))
                .Item("@telefono2").Value = telefono2
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 13, "fax"))
                .Item("@fax").Value = fax
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = curp
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = rfc
                .Add(New System.Data.SqlClient.SqlParameter("@tipoventa", System.Data.SqlDbType.Char, 1, "tipoventa"))
                .Item("@tipoventa").Value = tipoventa
                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1, "ivadesglosado"))
                .Item("@ivadesglosado").Value = ivadesglosado

                .Add(New System.Data.SqlClient.SqlParameter("@pedimento", System.Data.SqlDbType.VarChar, 30, "pedimento"))
                .Item("@pedimento").Value = pedimento
                .Add(New System.Data.SqlClient.SqlParameter("@aduana", System.Data.SqlDbType.VarChar, 50, "aduana"))
                .Item("@aduana").Value = aduana

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_importacion", System.Data.SqlDbType.DateTime, 8, "fecha_importacion"))
                .Item("@fecha_importacion").Value = System.DBNull.Value
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_reparto", System.Data.SqlDbType.VarChar, 80, "domicilio_reparto"))
                .Item("@domicilio_reparto").Value = ""
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_reparto", System.Data.SqlDbType.VarChar, 50, "colonia_reparto"))
                .Item("@colonia_reparto").Value = ""
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_reparto", System.Data.SqlDbType.VarChar, 50, "ciudad_reparto"))
                .Item("@ciudad_reparto").Value = ""
                .Add(New System.Data.SqlClient.SqlParameter("@estado_reparto", System.Data.SqlDbType.VarChar, 50, "estado_reparto"))
                .Item("@estado_reparto").Value = ""
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_reparto", System.Data.SqlDbType.VarChar, 13, "telefono_reparto"))
                .Item("@telefono_reparto").Value = ""
                .Add(New System.Data.SqlClient.SqlParameter("@horario_reparto", System.Data.SqlDbType.Char, 1, "horario_reparto"))
                .Item("@horario_reparto").Value = ""
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_reparto", System.Data.SqlDbType.DateTime, 8, "fecha_reparto"))
                .Item("@fecha_reparto").Value = System.DBNull.Value
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_reparto", System.Data.SqlDbType.Text, 0, "observaciones_reparto"))
                .Item("@observaciones_reparto").Value = ""

                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_venta", System.Data.SqlDbType.Text, 0, "observaciones_venta"))
                .Item("@observaciones_venta").Value = "Factura generada por Anticipo"

                If tipoventa = "D" Then   'CONTADO
                    .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                    .Item("@importe").Value = total - menos
                    .Add(New System.Data.SqlClient.SqlParameter("@menos", System.Data.SqlDbType.Money, 8, "menos"))
                    .Item("@menos").Value = 0 ' El menos va en 0 Cero
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Money, 8, "enganche"))
                    .Item("@enganche").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Money, 8, "interes"))
                    .Item("@interes").Value = System.DBNull.Value

                    .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                    .Item("@subtotal").Value = subtotal
                    .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                    .Item("@impuesto").Value = impuesto
                    .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                    .Item("@total").Value = total - menos



                    .Add(New System.Data.SqlClient.SqlParameter("@plan", System.Data.SqlDbType.Int, 4, "plan"))
                    .Item("@plan").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_primer_documento", System.Data.SqlDbType.DateTime, 8, "fecha_primer_documento"))
                    .Item("@fecha_primer_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
                    .Item("@numero_documentos").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_documentos", System.Data.SqlDbType.Money, 8, "importe_documentos"))
                    .Item("@importe_documentos").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_ultimo_documento", System.Data.SqlDbType.Money, 8, "importe_ultimo_documento"))
                    .Item("@importe_ultimo_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_documento", System.Data.SqlDbType.Bit, 1, "enganche_en_documento"))
                    .Item("@enganche_en_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_enganche_documento", System.Data.SqlDbType.DateTime, 8, "fecha_enganche_documento"))
                    .Item("@fecha_enganche_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_enganche_documento", System.Data.SqlDbType.Money, 8, "importe_enganche_documento"))
                    .Item("@importe_enganche_documento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@liquida_vencimiento", System.Data.SqlDbType.Bit, 1, "liquida_vencimiento"))
                    .Item("@liquida_vencimiento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@monto_liquida_vencimiento", System.Data.SqlDbType.Money, 8, "monto_liquida_vencimiento"))
                    .Item("@monto_liquida_vencimiento").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Bit, 1, "enganche_en_menos"))
                    .Item("@enganche_en_menos").Value = System.DBNull.Value
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                    .Item("@importe").Value = importe
                    .Add(New System.Data.SqlClient.SqlParameter("@menos", System.Data.SqlDbType.Money, 8, "menos"))
                    .Item("@menos").Value = menos
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Money, 8, "enganche"))
                    .Item("@enganche").Value = enganche
                    .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Money, 8, "interes"))
                    .Item("@interes").Value = interes
                    .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                    .Item("@subtotal").Value = subtotal
                    .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Money, 8, "impuesto"))
                    .Item("@impuesto").Value = impuesto
                    .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                    .Item("@total").Value = total

                    .Add(New System.Data.SqlClient.SqlParameter("@plan", System.Data.SqlDbType.Int, 4, "plan"))
                    .Item("@plan").Value = plan
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_primer_documento", System.Data.SqlDbType.DateTime, 8, "fecha_primer_documento"))
                    If fecha_primer_documento = CDate("01/01/1900") Then
                        .Item("@fecha_primer_documento").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_primer_documento").Value = fecha_primer_documento
                    End If
                    .Add(New System.Data.SqlClient.SqlParameter("@numero_documentos", System.Data.SqlDbType.Int, 4, "numero_documentos"))
                    .Item("@numero_documentos").Value = numero_documentos
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_documentos", System.Data.SqlDbType.Money, 8, "importe_documentos"))
                    .Item("@importe_documentos").Value = importe_documentos
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_ultimo_documento", System.Data.SqlDbType.Money, 8, "importe_ultimo_documento"))
                    .Item("@importe_ultimo_documento").Value = importe_ultimo_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_documento", System.Data.SqlDbType.Bit, 1, "enganche_en_documento"))
                    .Item("@enganche_en_documento").Value = enganche_en_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_enganche_documento", System.Data.SqlDbType.DateTime, 8, "fecha_enganche_documento"))
                    If fecha_enganche_documento = CDate("01/01/1900") Then
                        .Item("@fecha_enganche_documento").Value = System.DBNull.Value
                    Else
                        .Item("@fecha_enganche_documento").Value = fecha_enganche_documento
                    End If
                    .Add(New System.Data.SqlClient.SqlParameter("@importe_enganche_documento", System.Data.SqlDbType.Money, 8, "importe_enganche_documento"))
                    .Item("@importe_enganche_documento").Value = importe_enganche_documento
                    .Add(New System.Data.SqlClient.SqlParameter("@liquida_vencimiento", System.Data.SqlDbType.Bit, 1, "liquida_vencimiento"))
                    .Item("@liquida_vencimiento").Value = liquida_vencimiento
                    .Add(New System.Data.SqlClient.SqlParameter("@monto_liquida_vencimiento", System.Data.SqlDbType.Money, 8, "monto_liquida_vencimiento"))
                    .Item("@monto_liquida_vencimiento").Value = monto_liquida_vencimiento
                    .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Bit, 1, "enganche_en_menos"))
                    .Item("@enganche_en_menos").Value = enganche_en_menos
                End If

                If sucursal_dependencia Then
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_pedido", System.Data.SqlDbType.DateTime, 8, "fecha_pedido"))
                    .Item("@fecha_pedido").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_pedido", System.Data.SqlDbType.Int, 4, "folio_pedido"))
                    .Item("@folio_pedido").Value = -1
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_plantilla", System.Data.SqlDbType.Int, 4, "folio_plantilla"))
                    .Item("@folio_plantilla").Value = folio_plantilla
                    .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                    .Item("@quincena_inicio").Value = quincena_inicio
                    .Add(New System.Data.SqlClient.SqlParameter("@ano_inicio", System.Data.SqlDbType.Int, 4, "ano_inicio"))
                    .Item("@ano_inicio").Value = ano_inicio
                Else
                    .Add(New System.Data.SqlClient.SqlParameter("@fecha_pedido", System.Data.SqlDbType.DateTime, 8, "fecha_pedido"))
                    .Item("@fecha_pedido").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_pedido", System.Data.SqlDbType.Int, 4, "folio_pedido"))
                    .Item("@folio_pedido").Value = -1
                    .Add(New System.Data.SqlClient.SqlParameter("@folio_plantilla", System.Data.SqlDbType.Int, 4, "folio_plantilla"))
                    .Item("@folio_plantilla").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                    .Item("@quincena_inicio").Value = System.DBNull.Value
                    .Add(New System.Data.SqlClient.SqlParameter("@ano_inicio", System.Data.SqlDbType.Int, 4, "ano_inicio"))
                    .Item("@ano_inicio").Value = System.DBNull.Value
                End If
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = -1
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = -1

                .Add(New System.Data.SqlClient.SqlParameter("@punto_venta", System.Data.SqlDbType.Int, 4, "punto_venta"))
                .Item("@punto_venta").Value = punto_venta

                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento_especial_clientes", System.Data.SqlDbType.Int, 4, "folio_descuento_especial_clientes"))
                .Item("@folio_descuento_especial_clientes").Value = FolioDescuentoCliente


                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura_cancelada", System.Data.SqlDbType.Int, 4, "sucursal_factura_cancelada"))
                .Item("@sucursal_factura_cancelada").Value = -1
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_cancelada", System.Data.SqlDbType.Char, 3, "serie_factura_cancelada"))
                .Item("@serie_factura_cancelada").Value = ""
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_cancelada", System.Data.SqlDbType.Int, 4, "folio_factura_cancelada"))
                .Item("@folio_factura_cancelada").Value = -1

                .Add(New System.Data.SqlClient.SqlParameter("@aplica_enganche", System.Data.SqlDbType.Bit, 1, "aplica_enganche"))
                .Item("@aplica_enganche").Value = aplica_enganche

                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = paquete

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@facturacion_especial", System.Data.SqlDbType.Bit, 1, "facturacion_especial"))
                .Item("@facturacion_especial").Value = False

                .Add(New System.Data.SqlClient.SqlParameter("@es_anticipo", System.Data.SqlDbType.Bit, 1, "es_anticipo"))
                .Item("@es_anticipo").Value = True

                .Add(New System.Data.SqlClient.SqlParameter("@apartado", System.Data.SqlDbType.Bit, 1, "apartado"))
                .Item("@apartado").Value = apartado


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio = oCommand.Parameters.Item("@folio").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function





    Public Function sp_ventas_exs(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_sel(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_grs(ByVal puntoventa As Long, ByVal fecha As Date, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@puntoventa", System.Data.SqlDbType.Int, 4, "puntoventa"))
                .Item("@puntoventa").Value = puntoventa
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_grs_ejecutivo(ByVal sucursal As Long, ByVal serie As String, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_grs_ejecutivo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_facturas_grl(Optional ByVal sucursal As Long = -1, Optional ByVal bodega As String = "-1") As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_facturas_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_facturas_canceladas_grl(Optional ByVal sucursal As Long = -1) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_facturas_canceladas_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_facturas_cliente_grl(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_facturas_cliente_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_credito_por_fecha_grs(ByVal sucursal As Long, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_credito_por_fecha_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_credito_vencidas_sin_nota_cargo_grs(ByVal sucursal As Long, ByVal cliente As Long, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_credito_vencidas_sin_nota_cargo_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_impresa_nota_credito_upd(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal importe_ncr As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_impresa_nota_credito_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@importe_ncr", System.Data.SqlDbType.Int, 4, "importe_ncr"))
                .Item("@importe_ncr").Value = importe_ncr

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_impresa_nota_cargo_upd(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal importe_ncargo As Double, ByVal fecha As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_impresa_nota_cargo_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@importe_ncargo", System.Data.SqlDbType.Int, 4, "importe_ncargo"))
                .Item("@importe_ncargo").Value = importe_ncargo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_fecha_upd(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_fecha_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_validacancelar(ByVal folio_venta As Long, ByVal sucursal As Long, ByVal serie As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_validacancelar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_venta", System.Data.SqlDbType.Int, 4, "folio_venta"))
                .Item("@folio_venta").Value = folio_venta
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_cobrar_cancelacion_eliminada(ByVal sucursal As Long, ByVal serie As String, ByVal folio_venta As Long, ByVal concepto As String, ByVal cliente As Long, ByVal observaciones As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_cobrar_cancelacion_eliminada]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio_venta", System.Data.SqlDbType.Int, 4, "folio_venta"))
                .Item("@folio_venta").Value = folio_venta
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_cancelacion", System.Data.SqlDbType.NVarChar, 30, "usuario_cancelacion"))
                .Item("@usuario_cancelacion").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_cancelacion", System.Data.SqlDbType.Text, 0, "observaciones_cancelacion"))
                .Item("@observaciones_cancelacion").Value = observaciones
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_cobrar_cancelacion_cancelada(ByVal sucursal As Long, ByVal serie As String, ByVal folio_venta As Long, ByVal concepto As String, ByVal cliente As Long, ByVal observaciones As String, ByVal importe_ncr As Double, ByVal opcion_cancelacion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_cobrar_cancelacion_cancelada]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio_venta", System.Data.SqlDbType.Int, 4, "folio_venta"))
                .Item("@folio_venta").Value = folio_venta
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_cancelacion", System.Data.SqlDbType.NVarChar, 30, "usuario_cancelacion"))
                .Item("@usuario_cancelacion").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_cancelacion", System.Data.SqlDbType.Text, 0, "observaciones_cancelacion"))
                .Item("@observaciones_cancelacion").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@importe_ncr", System.Data.SqlDbType.Money, 8, "importe_ncr"))
                .Item("@importe_ncr").Value = importe_ncr
                .Add(New System.Data.SqlClient.SqlParameter("@opcion_cancelacion", System.Data.SqlDbType.Int, 4, "opcion_cancelacion"))
                .Item("@opcion_cancelacion").Value = opcion_cancelacion
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_etiquetas_por_imprimir_grs(ByVal bodega As String, ByVal fecha As Date, ByVal todas As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_etiquetas_por_imprimir_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@todas", System.Data.SqlDbType.Int, 4, "todas"))
                .Item("@todas").Value = todas

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_esenajenacion(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal factor_enajenacion As Double, ByVal ConceptoFactura As String, ByVal porcentaje_iva As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_esenajenacion]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@factor_enajenacion", System.Data.SqlDbType.Float, 8, "factor_enajenacion"))
                .Item("@factor_enajenacion").Value = factor_enajenacion

                .Add(New System.Data.SqlClient.SqlParameter("@ConceptoFactura", System.Data.SqlDbType.VarChar, 5, "ConceptoFactura"))
                .Item("@ConceptoFactura").Value = ConceptoFactura

                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_iva", System.Data.SqlDbType.Float, 8, "porcentaje_iva"))
                .Item("@porcentaje_iva").Value = porcentaje_iva


            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
    Public Function re_factura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal concepto As String, ByVal ivadesglosado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_factura]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_factura", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto_factura").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1))
                .Item("@ivadesglosado").Value = ivadesglosado
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function re_factura_magisterio(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal concepto As String, ByVal ivadesglosado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[re_factura_magisterio]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_factura", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto_factura").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1))
                .Item("@ivadesglosado").Value = ivadesglosado
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_credito_por_cliente_grs(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_credito_por_cliente_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_credito_por_cliente_traspasos_grs(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_credito_por_cliente_traspasos_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_sucursal_grl(ByVal cancelada As Boolean, ByVal megacred As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_sucursal_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cancelada", System.Data.SqlDbType.Bit, 1, "cancelada"))
                .Item("@cancelada").Value = cancelada
                .Add(New System.Data.SqlClient.SqlParameter("@megacred", System.Data.SqlDbType.Bit, 1, "megacred"))
                .Item("@megacred").Value = megacred
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_serie_grl(ByVal sucursal As Long, ByVal cancelada As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_serie_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cancelada", System.Data.SqlDbType.Bit, 1, "cancelada"))
                .Item("@cancelada").Value = cancelada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_folio_grl(ByVal sucursal As Long, ByVal serie As String, ByVal cancelada As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_folio_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@cancelada", System.Data.SqlDbType.Bit, 1, "cancelada"))
                .Item("@cancelada").Value = cancelada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_cargos_por_cliente_grs(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cargos_por_cliente_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_vendedor_upd(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal vendedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_vendedor_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_convenio_upd(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_convenio_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_ventas_sobrepedido_actualizar_entrada(ByVal orden_compra As Long, ByVal entrada As Long, ByVal articulo As Long, ByVal bodega As String, ByVal cantidad As Long, ByVal costo As Double, ByVal folio_historico_costo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_sobrepedido_actualizar_entrada]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_compra", System.Data.SqlDbType.Int, 4, "orden_compra"))
                .Item("@orden_compra").Value = orden_compra
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Int, 4, "cantidad"))
                .Item("@cantidad").Value = cantidad
                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@costo").Value = costo
                .Add(New System.Data.SqlClient.SqlParameter("@folio_historico_costo", System.Data.SqlDbType.Int, 4, "folio_historico_costo"))
                .Item("@folio_historico_costo").Value = folio_historico_costo
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ventas_cancelaciones_ins(ByVal fecha As DateTime, ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal observaciones As String, ByVal importe_gastos As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_cancelaciones_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.VarChar, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@importe_gastos", System.Data.SqlDbType.Money, 8, "importe_gastos"))
                .Item("@importe_gastos").Value = importe_gastos
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = Connection.User


            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_ventas_cambia_quincena_anio_envio(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal quincena_inicio As Long, ByVal anio_inicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_cambia_quincena_anio_envio]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_inicio", System.Data.SqlDbType.Int, 4, "quincena_inicio"))
                .Item("@quincena_inicio").Value = quincena_inicio
                .Add(New System.Data.SqlClient.SqlParameter("@anio_inicio", System.Data.SqlDbType.Int, 4, "anio_inicio"))
                .Item("@anio_inicio").Value = anio_inicio

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_traerarticulos_ventas(ByVal cotizacion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traerarticulos_ventas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traerarticulos_descuentos_clientes_ventas(ByVal folio_descuento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traerarticulos_descuentos_clientes_ventas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_descuento", System.Data.SqlDbType.Int, 4, "folio_descuento"))
                .Item("@folio_descuento").Value = folio_descuento

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traerarticulos_paquetes_ventas(ByVal paquete As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traerarticulos_paquetes_ventas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = paquete

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function sp_actualiza_venta_cotizacion(ByVal cotizacion As Double, ByVal sucursal As Double, ByVal serie As Char, ByVal folio As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_venta_cotizacion]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_venta", System.Data.SqlDbType.Int, 4, "sucursal_venta"))
                .Item("@sucursal_venta").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie_venta", System.Data.SqlDbType.Char, 3, "serie_venta"))
                .Item("@serie_venta").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio_venta", System.Data.SqlDbType.Int, 4, "folio_venta"))
                .Item("@folio_venta").Value = folio



            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_valida_recuperacion_repartos_venta(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_valida_recuperacion_repartos_venta]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    'Public Function sp_revision_por_factura(ByVal sucursal As Long, ByVal serie As Char, ByVal folio As Long, ByRef articulos_no_entregados As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_revision_por_factura]"
    '        With oCommand.Parameters

    '            .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
    '            .Item("@sucursal").Value = sucursal
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
    '            .Item("@serie").Value = serie
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = folio
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulos_no_entregados", System.Data.SqlDbType.Int, 4, "articulos_no_entregados"))
    '            .Item("@articulos_no_entregados").Value = articulos_no_entregados
    '            .Item("@articulos_no_entregados").Direction = ParameterDirection.InputOutput

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '        articulos_no_entregados = oCommand.Parameters.Item("@articulos_no_entregados").Value

    '        Dim odataset As DataSet


    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_ventas_ejecutivo_sel(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_ejecutivo_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_obtiene_abonos_factura_sel(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_obtiene_abonos_factura_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_obtener_facturas_timbrar(ByVal obtiene_serie As Boolean, ByVal sucursal As Long, Optional ByVal serie_filtrar As String = "") As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_obtener_facturas_timbrar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@obtiene_serie", System.Data.SqlDbType.Bit, 1, "obtiene_serie"))
                .Item("@obtiene_serie").Value = obtiene_serie
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie_filtrar", System.Data.SqlDbType.VarChar, 3, "serie_filtrar"))
                .Item("@serie_filtrar").Value = serie_filtrar

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_factura_cfdi_ins(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal concepto_factura As String, ByVal ivadesglosado As Boolean, ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_factura_cfdi_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_factura", System.Data.SqlDbType.VarChar, 5, "concepto_factura"))
                .Item("@concepto_factura").Value = concepto_factura

                .Add(New System.Data.SqlClient.SqlParameter("@ivadesglosado", System.Data.SqlDbType.Bit, 1, "ivadesglosado"))
                .Item("@ivadesglosado").Value = ivadesglosado

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual


                .Add(New System.Data.SqlClient.SqlParameter("@quien", System.Data.SqlDbType.VarChar, 15, "quien"))
                .Item("@quien").Value = Connection.User

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function
    Public Function sp_nota_de_cargo_moratorios_cfdi_ins(ByVal Sucursal As Long, ByVal Fecha As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_nota_de_cargo_moratorios_cfdi_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Fecha

                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = Connection.User


            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_facturas_buscar_abono_menos_reimpresion(ByVal sucursal As Long, ByVal cliente As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_facturas_buscar_abono_menos_reimpresion]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region
