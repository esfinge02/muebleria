'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPagoFacturasProveedoresDetalle
'DATE:		04/10/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - PagoFacturasProveedoresDetalle"
Public Class clsPagoFacturasProveedoresDetalle
    Public Function sp_pago_facturas_proveedores_detalle_ins(ByVal folio As Long, ByVal concepto As String, ByRef partida As Long, ByVal folio_factura As Long, ByVal concepto_factura As String, ByVal importe As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pago_facturas_proveedores_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Item("@partida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_factura", System.Data.SqlDbType.VarChar, 5, "concepto_factura"))
                .Item("@concepto_factura").Value = concepto_factura
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            partida = oCommand.Parameters.Item("@partida").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pago_facturas_proveedores_detalle_upd(ByVal folio As Long, ByVal concepto As String, ByRef partida As Long, ByVal folio_factura As Long, ByVal concepto_factura As String, ByVal importe As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pago_facturas_proveedores_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Item("@partida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_factura", System.Data.SqlDbType.VarChar, 5, "concepto_factura"))
                .Item("@concepto_factura").Value = concepto_factura
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pago_facturas_proveedores_detalle_del(ByVal folio As Long, ByVal concepto As String, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pago_facturas_proveedores_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pago_facturas_proveedores_detalle_exs(ByVal folio As Long, ByVal concepto As String, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pago_facturas_proveedores_detalle_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pago_facturas_proveedores_detalle_sel(ByVal folio As Long, ByVal concepto As String, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pago_facturas_proveedores_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_pago_facturas_proveedores_detalle_grs(ByVal folio As Long, ByVal concepto As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_pago_facturas_proveedores_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 5, "concepto"))
                .Item("@concepto").Value = concepto
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


