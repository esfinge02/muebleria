'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesServicio
'DATE:		31/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - OrdenesServicio"
Public Class clsOrdenesServicio
    Public Function sp_ordenes_servicio_ins(ByRef oData As DataSet, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal partida_factura As Long, ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = Connection.GetValue(oData, "orden_servicio")
                .Item("@orden_servicio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_servicio", System.Data.SqlDbType.Char, 1, "tipo_servicio"))
                .Item("@tipo_servicio").Value = Connection.GetValue(oData, "tipo_servicio")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_promesa", System.Data.SqlDbType.DateTime, 8, "fecha_promesa"))
                .Item("@fecha_promesa").Value = Connection.GetValue(oData, "fecha_promesa")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = Connection.GetValue(oData, "numero_serie")
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@garantia", System.Data.SqlDbType.Int, 4, "garantia"))
                .Item("@garantia").Value = Connection.GetValue(oData, "garantia")
                .Add(New System.Data.SqlClient.SqlParameter("@recibe", System.Data.SqlDbType.Int, 4, "recibe"))
                .Item("@recibe").Value = Connection.GetValue(oData, "recibe")
                .Add(New System.Data.SqlClient.SqlParameter("@centro_servicio", System.Data.SqlDbType.Int, 4, "centro_servicio"))
                .Item("@centro_servicio").Value = Connection.GetValue(oData, "centro_servicio")
                .Add(New System.Data.SqlClient.SqlParameter("@falla", System.Data.SqlDbType.Text, 0, "falla"))
                .Item("@falla").Value = Connection.GetValue(oData, "falla")
                .Add(New System.Data.SqlClient.SqlParameter("@enviado_con", System.Data.SqlDbType.VarChar, 50, "enviado_con"))
                .Item("@enviado_con").Value = Connection.GetValue(oData, "enviado_con")

                .Add(New System.Data.SqlClient.SqlParameter("@orden_cliente", System.Data.SqlDbType.Bit, 1, "orden_cliente"))
                .Item("@orden_cliente").Value = Connection.GetValue(oData, "orden_cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@direccion_cliente", System.Data.SqlDbType.VarChar, 100, "direccion_cliente"))
                .Item("@direccion_cliente").Value = Connection.GetValue(oData, "direccion_cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_cliente", System.Data.SqlDbType.VarChar, 25, "telefono_cliente"))
                .Item("@telefono_cliente").Value = Connection.GetValue(oData, "telefono_cliente")

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura


                .Add(New System.Data.SqlClient.SqlParameter("@partida_factura", System.Data.SqlDbType.Int, 4, "partida_factura"))
                .Item("@partida_factura").Value = partida_factura


                .Add(New System.Data.SqlClient.SqlParameter("@fecha_factura", System.Data.SqlDbType.DateTime, 8, "fecha_factura"))
                .Item("@fecha_factura").Value = Connection.GetValue(oData, "fecha_factura")

                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = Connection.GetValue(oData, "estatus")
                .Add(New System.Data.SqlClient.SqlParameter("@solucion", System.Data.SqlDbType.VarChar, 1000, "solucion"))
                .Item("@solucion").Value = Connection.GetValue(oData, "solucion")
                .Add(New System.Data.SqlClient.SqlParameter("@cambio_autorizado", System.Data.SqlDbType.Bit, 1, "cambio_autorizado"))
                .Item("@cambio_autorizado").Value = Connection.GetValue(oData, "cambio_autorizado")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@juridico", System.Data.SqlDbType.Bit, 1, "juridico"))
                .Item("@juridico").Value = Connection.GetValue(oData, "juridico")

                .Add(New System.Data.SqlClient.SqlParameter("@mercancia_taller", System.Data.SqlDbType.Bit, 1, "mercancia_taller"))
                .Item("@mercancia_taller").Value = Connection.GetValue(oData, "mercancia_taller")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "orden_servicio", oCommand.Parameters.Item("@orden_servicio").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    '@ACH-22/06/07: Modifiqué el método de sp_reparto_ordenes_servicio_ins para que acepte el CAMPO 'juridico'
    Public Function sp_reparto_ordenes_servicio_ins(ByVal orden_servicio As Long, ByVal sucursal As Long, _
    ByVal tipo_servicio As Char, ByVal fecha As DateTime, ByVal articulo As Long, ByVal numero_serie As String, _
     ByVal cliente As Long, ByVal direccion_cliente As String, ByVal sucursal_factura As Long, _
     ByVal serie_factura As String, ByVal folio_factura As Long, ByVal partida_factura As Long, _
     ByVal fecha_factura As DateTime, ByVal juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_reparto_ordenes_servicio_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Item("@orden_servicio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_servicio", System.Data.SqlDbType.Char, 1, "tipo_servicio"))
                .Item("@tipo_servicio").Value = tipo_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie
                '.Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                '.Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@direccion_cliente", System.Data.SqlDbType.VarChar, 100, "direccion_cliente"))
                .Item("@direccion_cliente").Value = direccion_cliente
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@partida_factura", System.Data.SqlDbType.Int, 4, "partida_factura"))
                .Item("@partida_factura").Value = partida_factura
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_factura", System.Data.SqlDbType.DateTime, 8, "fecha_factura"))
                .Item("@fecha_factura").Value = fecha_factura
                .Add(New System.Data.SqlClient.SqlParameter("@juridico", System.Data.SqlDbType.Bit, 1, "juridico"))
                .Item("@juridico").Value = juridico

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            'Connection.SetValue( oData, "orden_servicio", oCommand.Parameters.Item("@orden_servicio").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    '/@ACH-22/06/07

    Public Function sp_ordenes_servicio_upd(ByRef oData As DataSet, ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = Connection.GetValue(oData, "orden_servicio")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_servicio", System.Data.SqlDbType.Char, 1, "tipo_servicio"))
                .Item("@tipo_servicio").Value = Connection.GetValue(oData, "tipo_servicio")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_promesa", System.Data.SqlDbType.DateTime, 8, "fecha_promesa"))
                .Item("@fecha_promesa").Value = Connection.GetValue(oData, "fecha_promesa")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo  'Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@falla", System.Data.SqlDbType.Text, 0, "falla"))
                .Item("@falla").Value = Connection.GetValue(oData, "falla")
                .Add(New System.Data.SqlClient.SqlParameter("@enviado_con", System.Data.SqlDbType.VarChar, 50, "enviado_con"))
                .Item("@enviado_con").Value = Connection.GetValue(oData, "enviado_con")

                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = Connection.GetValue(oData, "estatus")
                .Add(New System.Data.SqlClient.SqlParameter("@solucion", System.Data.SqlDbType.VarChar, 1000, "solucion"))
                .Item("@solucion").Value = Connection.GetValue(oData, "solucion")
                .Add(New System.Data.SqlClient.SqlParameter("@cambio_autorizado", System.Data.SqlDbType.Bit, 1, "cambio_autorizado"))
                .Item("@cambio_autorizado").Value = Connection.GetValue(oData, "cambio_autorizado")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@juridico", System.Data.SqlDbType.Bit, 1, "juridico"))
                .Item("@juridico").Value = Connection.GetValue(oData, "juridico")

                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")

                .Add(New System.Data.SqlClient.SqlParameter("@centro_servicio", System.Data.SqlDbType.Int, 4, "centro_servicio"))
                .Item("@centro_servicio").Value = Connection.GetValue(oData, "centro_servicio")

                .Add(New System.Data.SqlClient.SqlParameter("@direccion_cliente", System.Data.SqlDbType.VarChar, 100, "direccion_cliente"))
                .Item("@direccion_cliente").Value = Connection.GetValue(oData, "direccion_cliente")

                .Add(New System.Data.SqlClient.SqlParameter("@telefono_cliente", System.Data.SqlDbType.VarChar, 25, "telefono_cliente"))
                .Item("@telefono_cliente").Value = Connection.GetValue(oData, "telefono_cliente")

                .Add(New System.Data.SqlClient.SqlParameter("@mercancia_taller", System.Data.SqlDbType.Bit, 1, "mercancia_taller"))
                .Item("@mercancia_taller").Value = Connection.GetValue(oData, "mercancia_taller")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_del(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function sp_ordenes_servicio_exs(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_sel(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_grl(ByVal tipo_servicio As Char, ByVal estatus As Char, ByVal devoluciones_proveedor As Boolean, ByVal Bodega_Devolucion As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_servicio", System.Data.SqlDbType.Char, 1, "tipo_servicio"))
                .Item("@tipo_servicio").Value = tipo_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = estatus
                .Add(New System.Data.SqlClient.SqlParameter("@devoluciones_proveedor", System.Data.SqlDbType.Bit, 1, "devoluciones_proveedor"))
                .Item("@devoluciones_proveedor").Value = devoluciones_proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@Bodega", System.Data.SqlDbType.VarChar, 5, "Bodega"))
                .Item("@Bodega").Value = Bodega_Devolucion

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_ordenes_servicio_reimpresion_upd(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_reimpresion_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_actualiza_orden_servicio_folio_movimientos(ByVal orden_servicio As Long, ByVal folio_movimiento As Long, ByVal Tipo As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_orden_servicio_folio_movimientos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@folio_movimiento", System.Data.SqlDbType.Int, 4, "folio_movimiento"))
                .Item("@folio_movimiento").Value = folio_movimiento

                .Add(New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.Bit, 1, "Tipo"))
                .Item("@Tipo").Value = Tipo
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_actualiza_usuario_termino_upd(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_actualiza_usuario_termino_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


