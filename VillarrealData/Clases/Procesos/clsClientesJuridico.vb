'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientesJuridico
'DATE:		18/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - ClientesJuridico"
Public Class clsClientesJuridico
    Public Function sp_clientes_juridico_ins(ByRef folio_juridico As Long, ByVal abogado As Long, ByVal cliente As Long, ByVal saldo_asignar As Double, ByVal fecha_asignacion As Date, ByVal status As String, ByVal no_recibir_abonos As Boolean, ByVal observaciones As String, ByVal cliente_reformado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Item("@folio_juridico").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Int, 4, "abogado"))
                .Item("@abogado").Value = abogado
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@saldo_asignar", System.Data.SqlDbType.Money, 8, "saldo_asignar"))
                .Item("@saldo_asignar").Value = saldo_asignar
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_asignacion", System.Data.SqlDbType.DateTime, 8, "fecha_asignacion"))
                .Item("@fecha_asignacion").Value = fecha_asignacion
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@no_recibir_abonos", System.Data.SqlDbType.Bit, 1, "no_recibir_abonos"))
                .Item("@no_recibir_abonos").Value = no_recibir_abonos
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_reformado", System.Data.SqlDbType.Bit, 1, "cliente_reformado"))
                .Item("@cliente_reformado").Value = cliente_reformado
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio_juridico = oCommand.Parameters.Item("@folio_juridico").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_asignar_clientes_juridico_ins(ByVal abogado As Long, ByVal cliente As Long, ByVal saldo_asignar As Double, ByVal fecha_asignacion As Date, ByVal status As String, ByVal no_recibir_abonos As Boolean, ByVal observaciones As String, ByVal cliente_reformado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_asignar_clientes_juridico_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Int, 4, "abogado"))
                .Item("@abogado").Value = abogado
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@saldo_asignar", System.Data.SqlDbType.Money, 8, "saldo_asignar"))
                .Item("@saldo_asignar").Value = saldo_asignar
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_asignacion", System.Data.SqlDbType.DateTime, 8, "fecha_asignacion"))
                .Item("@fecha_asignacion").Value = fecha_asignacion
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@no_recibir_abonos", System.Data.SqlDbType.Bit, 1, "no_recibir_abonos"))
                .Item("@no_recibir_abonos").Value = no_recibir_abonos
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_reformado", System.Data.SqlDbType.Bit, 1, "cliente_reformado"))
                .Item("@cliente_reformado").Value = cliente_reformado
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            '  folio_juridico = oCommand.Parameters.Item("@folio_juridico").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_upd(ByRef folio_juridico As Long, ByVal abogado As Long, ByVal cliente As Long, ByVal saldo_asignar As Double, ByVal fecha_asignacion As Date, ByVal status As String, ByVal no_recibir_abonos As Boolean, ByVal observaciones As String, ByVal cliente_reformado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Int, 4, "abogado"))
                .Item("@abogado").Value = abogado
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@saldo_asignar", System.Data.SqlDbType.Money, 8, "saldo_asignar"))
                .Item("@saldo_asignar").Value = saldo_asignar
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_asignacion", System.Data.SqlDbType.DateTime, 8, "fecha_asignacion"))
                .Item("@fecha_asignacion").Value = fecha_asignacion
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@no_recibir_abonos", System.Data.SqlDbType.Bit, 1, "no_recibir_abonos"))
                .Item("@no_recibir_abonos").Value = no_recibir_abonos
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_reformado", System.Data.SqlDbType.Bit, 1, "cliente_reformado"))
                .Item("@cliente_reformado").Value = cliente_reformado
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_del(ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_exs(ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_sel(ByVal folio_juridico As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_juridico", System.Data.SqlDbType.Int, 4, "folio_juridico"))
                .Item("@folio_juridico").Value = folio_juridico

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_juridico_grs(ByVal abogados As String, ByVal todos As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_juridico_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@abogados", System.Data.SqlDbType.Text, 0, "abogados"))
                .Item("@abogados").Value = abogados
                .Add(New System.Data.SqlClient.SqlParameter("@todos", System.Data.SqlDbType.Bit, 1, "todos"))
                .Item("@todos").Value = todos
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_abonos_de_clientes_para_asignar_a_juridico(ByVal sucursal As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_abonos_de_clientes_para_asignar_a_juridico]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Text, 0, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_notas_cargo_pendientes_asignar_clientes_exs(ByVal cliente As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_notas_cargo_pendientes_asignar_clientes_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function




End Class
#End Region


