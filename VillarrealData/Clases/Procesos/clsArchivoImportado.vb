
Imports Dipros.Utils

Public Class clsArchivoImportado
    Public Function sp_traer_movimientos_importados(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal nombre_tabla As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traer_movimientos_importados]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@quincena", System.Data.SqlDbType.Int, 4, "quincena"))
                .Item("@quincena").Value = quincena
                .Add(New System.Data.SqlClient.SqlParameter("@anio", System.Data.SqlDbType.Int, 4, "anio"))
                .Item("@anio").Value = anio
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_tabla", System.Data.SqlDbType.VarChar, 50, "nombre_tabla"))
                .Item("@nombre_tabla").Value = nombre_tabla

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function ca_actualiza_movimientos_importados(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal sucursal As Long, ByVal cliente As Long, ByVal importe As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[ca_actualiza_movimientos_importados]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena", System.Data.SqlDbType.Int, 4, "quincena"))
                .Item("@quincena").Value = quincena
                .Add(New System.Data.SqlClient.SqlParameter("@anio", System.Data.SqlDbType.Int, 4, "anio"))
                .Item("@anio").Value = anio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Int, 4, "importe"))
                .Item("@importe").Value = importe

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
