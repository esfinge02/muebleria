'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCotizaciones
'DATE:		01/08/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Cotizaciones"
Public Class clsCotizaciones
    Public Function sp_cotizaciones_ins(ByRef cotizacion As Long, ByVal sucursal As Long, ByVal fecha As Date, ByVal cliente As Long, ByVal nombre As String, ByVal observaciones As String, ByVal direccion As String, ByVal titulo As String, ByVal precio_contado As Boolean, ByVal atencion As String, ByVal autorizo As String, ByVal puesto As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Item("@cotizacion").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = nombre
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 250, "direccion"))
                .Item("@direccion").Value = direccion
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal_venta", System.Data.SqlDbType.Int, 4, "sucursal_venta"))
                '.Item("@sucursal_venta").Value = sucursal_venta
                '.Add(New System.Data.SqlClient.SqlParameter("@serie_venta", System.Data.SqlDbType.Char, 3, "serie_venta"))
                '.Item("@serie_venta").Value = serie_venta
                '.Add(New System.Data.SqlClient.SqlParameter("@folio_venta", System.Data.SqlDbType.Int, 4, "folio_venta"))
                '.Item("@folio_venta").Value = folio_venta
                .Add(New System.Data.SqlClient.SqlParameter("@titulo", System.Data.SqlDbType.VarChar, 20, "titulo"))
                .Item("@titulo").Value = titulo
                .Add(New System.Data.SqlClient.SqlParameter("@precio_contado", System.Data.SqlDbType.Bit, 1, "precio_contado"))
                .Item("@precio_contado").Value = precio_contado
                .Add(New System.Data.SqlClient.SqlParameter("@atencion", System.Data.SqlDbType.VarChar, 100, "atencion"))
                .Item("@atencion").Value = atencion
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 100, "autorizo"))
                .Item("@autorizo").Value = autorizo
                .Add(New System.Data.SqlClient.SqlParameter("@puesto", System.Data.SqlDbType.VarChar, 50, "puesto"))
                .Item("@puesto").Value = puesto
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            cotizacion = oCommand.Parameters.Item("@cotizacion").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_upd(ByRef cotizacion As Long, ByVal sucursal As Long, ByVal fecha As Date, ByVal cliente As Long, ByVal nombre As String, ByVal observaciones As String, ByVal direccion As String, ByVal titulo As String, ByVal precio_contado As Boolean, ByVal atencion As String, ByVal autorizo As String, ByVal puesto As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = nombre
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 250, "direccion"))
                .Item("@direccion").Value = direccion
                '.Add(New System.Data.SqlClient.SqlParameter("@sucursal_venta", System.Data.SqlDbType.Int, 4, "sucursal_venta"))
                '.Item("@sucursal_venta").Value = sucursal_venta
                '.Add(New System.Data.SqlClient.SqlParameter("@serie_venta", System.Data.SqlDbType.Char, 3, "serie_venta"))
                '.Item("@serie_venta").Value = serie_venta
                '.Add(New System.Data.SqlClient.SqlParameter("@folio_venta", System.Data.SqlDbType.Int, 4, "folio_venta"))
                '.Item("@folio_venta").Value = folio_venta
                .Add(New System.Data.SqlClient.SqlParameter("@titulo", System.Data.SqlDbType.VarChar, 20, "titulo"))
                .Item("@titulo").Value = titulo
                .Add(New System.Data.SqlClient.SqlParameter("@precio_contado", System.Data.SqlDbType.Bit, 1, "precio_contado"))
                .Item("@precio_contado").Value = precio_contado
                .Add(New System.Data.SqlClient.SqlParameter("@atencion", System.Data.SqlDbType.VarChar, 100, "atencion"))
                .Item("@atencion").Value = atencion
                .Add(New System.Data.SqlClient.SqlParameter("@autorizo", System.Data.SqlDbType.VarChar, 100, "autorizo"))
                .Item("@autorizo").Value = autorizo
                .Add(New System.Data.SqlClient.SqlParameter("@puesto", System.Data.SqlDbType.VarChar, 50, "puesto"))
                .Item("@puesto").Value = puesto
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_del(ByVal cotizacion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_exs(ByVal cotizacion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_sel(ByVal cotizacion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cotizaciones_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cotizaciones_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


