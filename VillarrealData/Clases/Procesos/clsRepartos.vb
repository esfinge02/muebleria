'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsRepartos
'DATE:		03/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Repartos"
Public Class clsRepartos
    Public Function sp_repartos_ins(ByVal oData As DataSet, ByVal fecha As String, ByRef reparto As Long, ByVal sucursal_actual As Long, ByVal status As String, ByVal sucursal_factura As Long, ByVal factura As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal_actual
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Item("@reparto").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha 'Connection.GetValue(oData, "fecha")

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_entrega", System.Data.SqlDbType.DateTime, 8, "fecha_entrega"))
                .Item("@fecha_entrega").Value = System.DBNull.Value

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = factura
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = Connection.GetValue(oData, "serie_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@repartir", System.Data.SqlDbType.Int, 4, "repartir"))
                .Item("@repartir").Value = Connection.GetValue(oData, "repartir")
                .Add(New System.Data.SqlClient.SqlParameter("@chofer", System.Data.SqlDbType.Int, 4, "chofer"))
                .Item("@chofer").Value = Connection.GetValue(oData, "chofer")
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = Connection.GetValue(oData, "bodeguero")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.VarChar, 2, "status"))
                .Item("@status").Value = status
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 50, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono", System.Data.SqlDbType.VarChar, 13, "telefono"))
                .Item("@telefono").Value = Connection.GetValue(oData, "telefono")

                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrega", System.Data.SqlDbType.VarChar, 5, "bodega_entrega"))
                .Item("@bodega_entrega").Value = Connection.GetValue(oData, "bodega_entrega")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

            reparto = oCommand.Parameters.Item("@reparto").Value


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = Connection.GetValue(oData, "reparto")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = Connection.GetValue(oData, "sucursal_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = Connection.GetValue(oData, "serie_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@repartir", System.Data.SqlDbType.Int, 4, "repartir"))
                .Item("@repartir").Value = Connection.GetValue(oData, "repartir")
                .Add(New System.Data.SqlClient.SqlParameter("@chofer", System.Data.SqlDbType.Int, 4, "chofer"))
                .Item("@chofer").Value = Connection.GetValue(oData, "chofer")
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = Connection.GetValue(oData, "bodeguero")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.VarChar, 2, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_del(ByVal sucursal As Long, ByVal reparto As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_exs(ByVal sucursal As Long, ByVal reparto As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_sel(ByVal reparto As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_grl(ByVal reparto As Long, ByVal cliente As Long, ByVal AnioActual As Boolean, ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_grl]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@anioactual", System.Data.SqlDbType.Bit, 1, "anioactual"))
                .Item("@anioactual").Value = AnioActual
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_elimina_reparto(ByVal reparto As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_elimina_reparto]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cerrar_reparto(ByVal reparto As Long, ByVal fecha_entrega As Date, ByVal recibio As String, ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cerrar_reparto]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_entrega", System.Data.SqlDbType.DateTime, 8, "fecha_entrega"))
                .Item("@fecha_entrega").Value = fecha_entrega
                .Add(New System.Data.SqlClient.SqlParameter("@recibio", System.Data.SqlDbType.VarChar, 50, "recibio"))
                .Item("@recibio").Value = recibio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_cierre_grs(ByVal sucursal As Long, ByVal fecha As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_cierre_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
End Class
#End Region


