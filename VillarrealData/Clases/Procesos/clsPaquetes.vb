Imports Dipros.Utils

Public Class clsPaquetes

    Public Function sp_paquetes_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_paquetes_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = Connection.GetValue(oData, "paquete")
                .Item("@paquete").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 100, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")

                .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Decimal, 9, "enganche"))
                .Item("@enganche").Value = Connection.GetValue(oData, "enganche")

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = Connection.GetValue(oData, "fecha_inicial")

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = Connection.GetValue(oData, "fecha_final")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "paquete", oCommand.Parameters.Item("@paquete").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_paquetes_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_paquetes_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = Connection.GetValue(oData, "paquete")

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 100, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")

                .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Decimal, 9, "enganche"))
                .Item("@enganche").Value = Connection.GetValue(oData, "enganche")

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_inicial", System.Data.SqlDbType.DateTime, 8, "fecha_inicial"))
                .Item("@fecha_inicial").Value = Connection.GetValue(oData, "fecha_inicial")

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_final", System.Data.SqlDbType.DateTime, 8, "fecha_final"))
                .Item("@fecha_final").Value = Connection.GetValue(oData, "fecha_final")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_paquetes_del(ByVal paquete As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_paquetes_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = paquete

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_paquetes_sel(ByVal paquete As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_paquetes_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@paquete", System.Data.SqlDbType.Int, 4, "paquete"))
                .Item("@paquete").Value = paquete

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_paquetes_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_paquetes_grl]"
            With oCommand.Parameters
               

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_paquetes_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_paquetes_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
