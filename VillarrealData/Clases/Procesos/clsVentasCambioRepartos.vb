Imports Dipros.Utils

Public Class clsVentasCambioReparto
    Public Function sp_datos_reparto_cambio_sel(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_datos_reparto_cambio_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try
        oCommand = Nothing
        Return oEvent
    End Function

    Public Function sp_ventas_cambio_reparto_ins(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal fecha_reparto_original As Date, ByVal horario_original As Char, ByVal domicilio_original As String, ByVal colonia_original As String, ByVal ciudad_original As String, ByVal estado_original As String, ByVal telefono_original As String, ByVal observaciones_original As String)
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_cambio_reparto_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 4, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 12, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_reparto_original", System.Data.SqlDbType.DateTime, 8, "fecha_reparto_original"))
                .Item("@fecha_reparto_original").Value = fecha_reparto_original
                .Add(New System.Data.SqlClient.SqlParameter("@horario_original", System.Data.SqlDbType.Char, 1, "horario_original"))
                .Item("@horario_original").Value = horario_original
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_original", System.Data.SqlDbType.Char, 80, "domicilio_original"))
                .Item("@domicilio_original").Value = domicilio_original
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_original", System.Data.SqlDbType.Char, 50, "colonia_original"))
                .Item("@colonia_original").Value = colonia_original
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_original", System.Data.SqlDbType.Char, 50, "ciudad_original"))
                .Item("@ciudad_original").Value = ciudad_original
                .Add(New System.Data.SqlClient.SqlParameter("@estado_original", System.Data.SqlDbType.Char, 50, "estado_original"))
                .Item("@estado_original").Value = estado_original
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_original", System.Data.SqlDbType.Char, 13, "telefono_original"))
                .Item("@telefono_original").Value = telefono_original
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_original", System.Data.SqlDbType.Text, 0, "observaciones_original"))
                .Item("@observaciones_original").Value = observaciones_original
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_cambio", System.Data.SqlDbType.Char, 15, "usuario_cambio"))
                .Item("@usuario_cambio").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try
        oCommand = Nothing
        Return oEvent
    End Function

    Public Function sp_ventas_cambio_reparto_upd(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal fecha_reparto As Date, ByVal horario_reparto As Char, ByVal domicilio_reparto As String, ByVal colonia_reparto As String, ByVal ciudad_reparto As String, ByVal estado_reparto As String, ByVal telefono_reparto As String, ByVal observaciones_reparto As String)
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ventas_cambio_reparto_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 4, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 12, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_reparto", System.Data.SqlDbType.DateTime, 8, "fecha_reparto"))
                .Item("@fecha_reparto").Value = fecha_reparto
                .Add(New System.Data.SqlClient.SqlParameter("@horario_reparto", System.Data.SqlDbType.Char, 1, "horario_reparto"))
                .Item("@horario_reparto").Value = horario_reparto
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_reparto", System.Data.SqlDbType.Char, 80, "domicilio_reparto"))
                .Item("@domicilio_reparto").Value = domicilio_reparto
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_reparto", System.Data.SqlDbType.Char, 50, "colonia_reparto"))
                .Item("@colonia_reparto").Value = colonia_reparto
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_reparto", System.Data.SqlDbType.Char, 50, "ciudad_reparto"))
                .Item("@ciudad_reparto").Value = ciudad_reparto
                .Add(New System.Data.SqlClient.SqlParameter("@estado_reparto", System.Data.SqlDbType.Char, 50, "estado_reparto"))
                .Item("@estado_reparto").Value = estado_reparto
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_reparto", System.Data.SqlDbType.Char, 13, "telefono_reparto"))
                .Item("@telefono_reparto").Value = telefono_reparto
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_reparto", System.Data.SqlDbType.Text, 0, "observaciones_reparto"))
                .Item("@observaciones_reparto").Value = observaciones_reparto
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_cambio", System.Data.SqlDbType.Char, 15, "usuario_cambio"))
                .Item("@usuario_cambio").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try
        oCommand = Nothing
        Return oEvent
    End Function

End Class

