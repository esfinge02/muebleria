'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradas
'DATE:		06/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils
#Region "DIPROS Systems, DataEnvironment - Entradas"
Public Class clsEntradas
    Public Function sp_entradas_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = Connection.GetValue(oData, "entrada")
                .Item("@entrada").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = Connection.GetValue(oData, "orden")
                .Add(New System.Data.SqlClient.SqlParameter("@referencia", System.Data.SqlDbType.VarChar, 15, "referencia"))
                .Item("@referencia").Value = Connection.GetValue(oData, "referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@pedimento", System.Data.SqlDbType.VarChar, 30, "pedimento"))
                .Item("@pedimento").Value = Connection.GetValue(oData, "pedimento")
                .Add(New System.Data.SqlClient.SqlParameter("@aduana", System.Data.SqlDbType.VarChar, 50, "aduana"))
                .Item("@aduana").Value = Connection.GetValue(oData, "aduana")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_importacion", System.Data.SqlDbType.DateTime, 8, "fecha_importacion"))
                .Item("@fecha_importacion").Value = Connection.GetValue(oData, "fecha_importacion")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_1", System.Data.SqlDbType.Decimal, 5, "plazo_pago_1"))
                .Item("@plazo_pago_1").Value = Connection.GetValue(oData, "plazo_pago_1")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_2", System.Data.SqlDbType.Decimal, 5, "plazo_pago_2"))
                .Item("@plazo_pago_2").Value = Connection.GetValue(oData, "plazo_pago_2")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_3", System.Data.SqlDbType.Decimal, 5, "plazo_pago_3"))
                .Item("@plazo_pago_3").Value = Connection.GetValue(oData, "plazo_pago_3")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_4", System.Data.SqlDbType.Decimal, 5, "plazo_pago_4"))
                .Item("@plazo_pago_4").Value = Connection.GetValue(oData, "plazo_pago_4")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_5", System.Data.SqlDbType.Decimal, 5, "plazo_pago_5"))
                .Item("@plazo_pago_5").Value = Connection.GetValue(oData, "plazo_pago_5")

                .Add(New System.Data.SqlClient.SqlParameter("@flete", System.Data.SqlDbType.Money, 8, "flete"))
                .Item("@flete").Value = Connection.GetValue(oData, "flete")
                .Add(New System.Data.SqlClient.SqlParameter("@otros_gastos", System.Data.SqlDbType.Money, 8, "otros_gastos"))
                .Item("@otros_gastos").Value = Connection.GetValue(oData, "otros_gastos")
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = Connection.GetValue(oData, "acreedor")
                .Add(New System.Data.SqlClient.SqlParameter("@flete_incluido", System.Data.SqlDbType.Bit, 1, "flete_incluido"))
                .Item("@flete_incluido").Value = Connection.GetValue(oData, "flete_incluido")

                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_factura", System.Data.SqlDbType.DateTime, 8, "fecha_factura"))
                .Item("@fecha_factura").Value = Connection.GetValue(oData, "fecha_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@seguro", System.Data.SqlDbType.Money, 8, "seguro"))
                .Item("@seguro").Value = Connection.GetValue(oData, "seguro")
                .Add(New System.Data.SqlClient.SqlParameter("@maniobras", System.Data.SqlDbType.Money, 8, "maniobras"))
                .Item("@maniobras").Value = Connection.GetValue(oData, "maniobras")
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = Connection.GetValue(oData, "iva")
                .Add(New System.Data.SqlClient.SqlParameter("@iva_retenido", System.Data.SqlDbType.Money, 8, "iva_retenido"))
                .Item("@iva_retenido").Value = Connection.GetValue(oData, "iva_retenido")
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = Connection.GetValue(oData, "total")

                .Add(New System.Data.SqlClient.SqlParameter("@condiciones_pago", System.Data.SqlDbType.VarChar, 80, "condiciones_pago"))
                .Item("@condiciones_pago").Value = Connection.GetValue(oData, "condiciones_pago")
            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "entrada", oCommand.Parameters.Item("@entrada").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_entradas_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = Connection.GetValue(oData, "entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = Connection.GetValue(oData, "orden")
                .Add(New System.Data.SqlClient.SqlParameter("@referencia", System.Data.SqlDbType.VarChar, 15, "referencia"))
                .Item("@referencia").Value = Connection.GetValue(oData, "referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@pedimento", System.Data.SqlDbType.VarChar, 30, "pedimento"))
                .Item("@pedimento").Value = Connection.GetValue(oData, "pedimento")
                .Add(New System.Data.SqlClient.SqlParameter("@aduana", System.Data.SqlDbType.VarChar, 50, "aduana"))
                .Item("@aduana").Value = Connection.GetValue(oData, "aduana")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_importacion", System.Data.SqlDbType.DateTime, 8, "fecha_importacion"))
                .Item("@fecha_importacion").Value = Connection.GetValue(oData, "fecha_importacion")
                .Add(New System.Data.SqlClient.SqlParameter("@costeado", System.Data.SqlDbType.Bit, 1, "costeado"))
                .Item("@costeado").Value = Connection.GetValue(oData, "costeado")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_1", System.Data.SqlDbType.Int, 4, "plazo_pago_1"))
                .Item("@plazo_pago_1").Value = Connection.GetValue(oData, "plazo_pago_1")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_2", System.Data.SqlDbType.Int, 4, "plazo_pago_2"))
                .Item("@plazo_pago_2").Value = Connection.GetValue(oData, "plazo_pago_2")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_3", System.Data.SqlDbType.Int, 4, "plazo_pago_3"))
                .Item("@plazo_pago_3").Value = Connection.GetValue(oData, "plazo_pago_3")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_4", System.Data.SqlDbType.Int, 4, "plazo_pago_4"))
                .Item("@plazo_pago_4").Value = Connection.GetValue(oData, "plazo_pago_4")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_5", System.Data.SqlDbType.Int, 4, "plazo_pago_5"))
                .Item("@plazo_pago_5").Value = Connection.GetValue(oData, "plazo_pago_5")

                .Add(New System.Data.SqlClient.SqlParameter("@flete", System.Data.SqlDbType.Money, 8, "flete"))
                .Item("@flete").Value = Connection.GetValue(oData, "flete")
                .Add(New System.Data.SqlClient.SqlParameter("@otros_gastos", System.Data.SqlDbType.Money, 8, "otros_gastos"))
                .Item("@otros_gastos").Value = Connection.GetValue(oData, "otros_gastos")
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = Connection.GetValue(oData, "acreedor")
                .Add(New System.Data.SqlClient.SqlParameter("@flete_incluido", System.Data.SqlDbType.Bit, 1, "flete_incluido"))
                .Item("@flete_incluido").Value = Connection.GetValue(oData, "flete_incluido")

                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_factura", System.Data.SqlDbType.DateTime, 8, "fecha_factura"))
                .Item("@fecha_factura").Value = Connection.GetValue(oData, "fecha_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@seguro", System.Data.SqlDbType.Money, 8, "seguro"))
                .Item("@seguro").Value = Connection.GetValue(oData, "seguro")
                .Add(New System.Data.SqlClient.SqlParameter("@maniobras", System.Data.SqlDbType.Money, 8, "maniobras"))
                .Item("@maniobras").Value = Connection.GetValue(oData, "maniobras")
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = Connection.GetValue(oData, "subtotal")
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = Connection.GetValue(oData, "iva")
                .Add(New System.Data.SqlClient.SqlParameter("@iva_retenido", System.Data.SqlDbType.Money, 8, "iva_retenido"))
                .Item("@iva_retenido").Value = Connection.GetValue(oData, "iva_retenido")
                .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Money, 8, "total"))
                .Item("@total").Value = Connection.GetValue(oData, "total")
                .Add(New System.Data.SqlClient.SqlParameter("@condiciones_pago", System.Data.SqlDbType.VarChar, 80, "condiciones_pago"))
                .Item("@condiciones_pago").Value = Connection.GetValue(oData, "condiciones_pago")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_entradas_del(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_entradas_exs(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_entradas_sel(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_entradas_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_entradas_grl(ByVal bodega As String, Optional ByVal costeada As Boolean = False, Optional ByVal proveedor As Long = -1) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@costeada", System.Data.SqlDbType.Bit, 1, "costeada"))
                .Item("@costeada").Value = costeada
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_entrada_sel(ByVal entrada As Long, ByVal departamento As Long, ByVal grupo As Long, ByVal bodega As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_entrada_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_departamento_articulos_entrada_sel(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamento_articulos_entrada_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_grupo_articulos_entrada_sel(ByVal entrada As Long, ByVal departamento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupo_articulos_entrada_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_entradas_articulos_surtimiento(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_articulos_surtimiento]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_eliminar_entrada_costeada_del(ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_eliminar_entrada_costeada_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada
                .Add(New System.Data.SqlClient.SqlParameter("@solicita", System.Data.SqlDbType.VarChar, 15, "solicita"))
                .Item("@solicita").Value = Connection.User

            End With
            'Connection.Execute(oCommand)
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


        'Public Function sp_entradas_traer_condiciones_pago_fechas(ByVal fecha As Date, ByVal total As Long, ByVal plazo1 As Long, ByVal plazo2 As Long, ByVal plazo3 As Long, ByVal plazo4 As Long, ByVal plazo5 As Long, ByVal descuentoProntoPago1 As Long, ByVal descuentoProntoPago2 As Long, ByVal descuentoProntoPago3 As Long, ByVal descuentoProntoPago4 As Long, ByVal descuentoProntoPago5 As Long) As Events
        '    Dim oCommand As New System.Data.SqlClient.SqlCommand
        '    Dim oEvent As New Events
        '    Try
        '        oCommand.CommandText = "[sp_entradas_traer_condiciones_pago_fechas]"
        '        With oCommand.Parameters

        '            .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
        '            .Item("@fecha").Value = fecha
        '            .Add(New System.Data.SqlClient.SqlParameter("@total", System.Data.SqlDbType.Int, 4, "total"))
        '            .Item("@total").Value = total
        '            .Add(New System.Data.SqlClient.SqlParameter("@plazo1", System.Data.SqlDbType.Int, 4, "plazo1"))
        '            .Item("@plazo1").Value = plazo1
        '            .Add(New System.Data.SqlClient.SqlParameter("@plazo2", System.Data.SqlDbType.Int, 4, "plazo2"))
        '            .Item("@plazo2").Value = plazo2
        '            .Add(New System.Data.SqlClient.SqlParameter("@plazo3", System.Data.SqlDbType.Int, 4, "plazo3"))
        '            .Item("@plazo3").Value = plazo3
        '            .Add(New System.Data.SqlClient.SqlParameter("@plazo4", System.Data.SqlDbType.Int, 4, "plazo4"))
        '            .Item("@plazo4").Value = plazo4
        '            .Add(New System.Data.SqlClient.SqlParameter("@plazo5", System.Data.SqlDbType.Int, 4, "plazo5"))
        '            .Item("@plazo5").Value = plazo5

        '        End With
        '        oEvent.Value = Connection.GetDataSet(oCommand)
        '    Catch ex As Exception
        '        oEvent.Ex = ex
        '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
        '        oEvent.Layer = Events.ErrorLayer.DataLayer
        '    Finally
        '        oCommand = Nothing
        '    End Try

        '    Return oEvent
        'End Function

End Class
#End Region


