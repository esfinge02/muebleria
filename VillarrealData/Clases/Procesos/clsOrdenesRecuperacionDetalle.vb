'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesRecuperacionDetalle
'DATE:		11/10/2007 0:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - OrdenesRecuperacionDetalle"
Public Class clsOrdenesRecuperacionDetalle
    '    Public Function sp_ordenes_recuperacion_detalle_ins(ByRef oData As DataSet, ByVal Folio As Long) As Events
    '        Dim oCommand As New System.Data.SqlClient.SqlCommand
    '        Dim oEvent As New Events
    '        Try
    '            oCommand.CommandText = "[sp_ordenes_recuperacion_detalle_ins]"
    '            With oCommand.Parameters
    '                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '                .Item("@folio").Value = Folio ' Connection.GetValue(oData, "folio")
    '                .Item("@folio").Direction = ParameterDirection.InputOutput
    '                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
    '                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Char, 10, "partida"))
    '                .Item("@partida").Value = Connection.GetValue(oData, "partida")
    '                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
    '                .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")
    '                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.VarChar, 30, "serie"))
    '                .Item("@serie").Value = Connection.GetValue(oData, "serie")
    '                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
    '                .Item("@costo").Value = Connection.GetValue(oData, "costo")

    '            End With
    '            Connection.Execute(oCommand)
    '            Connection.SetValue(oData, "folio", oCommand.Parameters.Item("@folio").Value)

    '        Catch ex As Exception
    '            oEvent.Ex = ex
    '            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '            oEvent.Layer = Events.ErrorLayer.DataLayer
    '        Finally
    '            oCommand = Nothing
    '        End Try

    '        Return oEvent
    '    End Function

    Public Function sp_ordenes_recuperacion_detalle_ins(ByRef oData As DataRow, ByVal Folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Folio ' oData("folio")
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Char, 10, "partida"))
                .Item("@partida").Value = oData("partida")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = oData("articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Int, 4, "cantidad"))
                .Item("@cantidad").Value = oData("cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.VarChar, 30, "serie"))
                .Item("@serie").Value = oData("serie")
                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@costo").Value = oData("costo")

                '.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                '.Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_ordenes_recuperacion_detalle_upd(ByRef oData As DataSet, ByVal Folio As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_ordenes_recuperacion_detalle_upd]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
    '            .Item("@folio").Value = Folio 'Connection.GetValue(oData, "folio")
    '            .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
    '            .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
    '            .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Char, 10, "partida"))
    '            .Item("@partida").Value = Connection.GetValue(oData, "partida")
    '            .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
    '            .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")
    '            .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.VarChar, 30, "serie"))
    '            .Item("@serie").Value = Connection.GetValue(oData, "serie")
    '            .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
    '            .Item("@costo").Value = Connection.GetValue(oData, "costo")

    '        End With
    '        Connection.Execute(oCommand)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_ordenes_recuperacion_detalle_upd(ByRef oData As DataRow, ByVal Folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Folio 'oData("folio")
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Char, 10, "partida"))
                .Item("@partida").Value = oData("partida")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = oData("articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Int, 4, "cantidad"))
                .Item("@cantidad").Value = oData("cantidad")
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.VarChar, 30, "serie"))
                .Item("@serie").Value = oData("serie")
                .Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
                .Item("@costo").Value = oData("costo")

                '.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                '.Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_detalle_del(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_detalle_exs(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_detalle_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_detalle_sel(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_recuperacion_detalle_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_recuperacion_detalle_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region



