'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesCompra
'DATE:		06/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - OrdenesCompra"
Public Class clsOrdenesCompra
	Public Function sp_ordenes_compra_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_ordenes_compra_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
				.Item("@orden").Value = Connection.GetValue(oData, "orden")
				.Item("@orden").Direction = ParameterDirection.InputOutput
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
				.Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
				.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
				.Item("@fecha").Value = Connection.GetValue(oData, "fecha")
				.Add(New System.Data.SqlClient.SqlParameter("@fecha_promesa", System.Data.SqlDbType.Datetime, 8, "fecha_promesa"))
				.Item("@fecha_promesa").Value = Connection.GetValue(oData, "fecha_promesa")
				.Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
				.Item("@bodega").Value = Connection.GetValue(oData, "bodega")
				.Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
				.Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
				.Add(New System.Data.SqlClient.SqlParameter("@folio_proveedor", System.Data.SqlDbType.VarChar, 50, "folio_proveedor"))
				.Item("@folio_proveedor").Value = Connection.GetValue(oData, "folio_proveedor")
				.Add(New System.Data.SqlClient.SqlParameter("@condiciones", System.Data.SqlDbType.VarChar, 80, "condiciones"))
				.Item("@condiciones").Value = Connection.GetValue(oData, "condiciones")
				.Add(New System.Data.SqlClient.SqlParameter("@agente", System.Data.SqlDbType.VarChar, 50, "agente"))
				.Item("@agente").Value = Connection.GetValue(oData, "agente")
				.Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@sobre_pedido", System.Data.SqlDbType.Bit, 1, "sobre_pedido"))
                .Item("@sobre_pedido").Value = Connection.GetValue(oData, "sobre_pedido")
				.Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@anticipo", System.Data.SqlDbType.Money, 8, "anticipo"))
                .Item("@anticipo").Value = Connection.GetValue(oData, "anticipo")

                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_1", System.Data.SqlDbType.Int, 4, "plazo_pago_1"))
                .Item("@plazo_pago_1").Value = Connection.GetValue(oData, "plazo_pago_1")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_2", System.Data.SqlDbType.Int, 4, "plazo_pago_2"))
                .Item("@plazo_pago_2").Value = Connection.GetValue(oData, "plazo_pago_2")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_3", System.Data.SqlDbType.Int, 4, "plazo_pago_3"))
                .Item("@plazo_pago_3").Value = Connection.GetValue(oData, "plazo_pago_3")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_4", System.Data.SqlDbType.Int, 4, "plazo_pago_4"))
                .Item("@plazo_pago_4").Value = Connection.GetValue(oData, "plazo_pago_4")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_5", System.Data.SqlDbType.Int, 4, "plazo_pago_5"))
                .Item("@plazo_pago_5").Value = Connection.GetValue(oData, "plazo_pago_5")

				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)
			Connection.SetValue(oData, "orden", oCommand.Parameters.Item("@orden").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_ordenes_compra_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_ordenes_compra_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
				.Item("@orden").Value = Connection.GetValue(oData, "orden")
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
				.Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
				.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
				.Item("@fecha").Value = Connection.GetValue(oData, "fecha")
				.Add(New System.Data.SqlClient.SqlParameter("@fecha_promesa", System.Data.SqlDbType.Datetime, 8, "fecha_promesa"))
				.Item("@fecha_promesa").Value = Connection.GetValue(oData, "fecha_promesa")
				.Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
				.Item("@bodega").Value = Connection.GetValue(oData, "bodega")
				.Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
				.Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
				.Add(New System.Data.SqlClient.SqlParameter("@folio_proveedor", System.Data.SqlDbType.VarChar, 50, "folio_proveedor"))
				.Item("@folio_proveedor").Value = Connection.GetValue(oData, "folio_proveedor")
				.Add(New System.Data.SqlClient.SqlParameter("@condiciones", System.Data.SqlDbType.VarChar, 80, "condiciones"))
				.Item("@condiciones").Value = Connection.GetValue(oData, "condiciones")
				.Add(New System.Data.SqlClient.SqlParameter("@agente", System.Data.SqlDbType.VarChar, 50, "agente"))
				.Item("@agente").Value = Connection.GetValue(oData, "agente")
				.Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@sobre_pedido", System.Data.SqlDbType.Bit, 1, "sobre_pedido"))
                .Item("@sobre_pedido").Value = Connection.GetValue(oData, "sobre_pedido")
				.Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 2, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@anticipo", System.Data.SqlDbType.Money, 8, "anticipo"))
                .Item("@anticipo").Value = Connection.GetValue(oData, "anticipo")

                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_1", System.Data.SqlDbType.Int, 4, "plazo_pago_1"))
                .Item("@plazo_pago_1").Value = Connection.GetValue(oData, "plazo_pago_1")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_2", System.Data.SqlDbType.Int, 4, "plazo_pago_2"))
                .Item("@plazo_pago_2").Value = Connection.GetValue(oData, "plazo_pago_2")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_3", System.Data.SqlDbType.Int, 4, "plazo_pago_3"))
                .Item("@plazo_pago_3").Value = Connection.GetValue(oData, "plazo_pago_3")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_4", System.Data.SqlDbType.Int, 4, "plazo_pago_4"))
                .Item("@plazo_pago_4").Value = Connection.GetValue(oData, "plazo_pago_4")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo_pago_5", System.Data.SqlDbType.Int, 4, "plazo_pago_5"))
                .Item("@plazo_pago_5").Value = Connection.GetValue(oData, "plazo_pago_5")

				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_ordenes_compra_del(ByVal orden as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_ordenes_compra_del]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
				.Item("@orden").Value = orden

			End With
			Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
	
		Return oEvent
	End Function

	Public Function sp_ordenes_compra_exs(ByVal orden as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_ordenes_compra_exs]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
				.Item("@orden").Value = orden

			End With
			oEvent.Value = Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

	Public Function sp_ordenes_compra_sel(ByVal orden as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_ordenes_compra_sel]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
				.Item("@orden").Value = orden

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

    Public Function sp_ordenes_compra_grs(ByVal tipo_status As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_compra_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_status", System.Data.SqlDbType.Int, 4, "tipo_status"))
                .Item("@tipo_status").Value = tipo_status

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_compra_grl(Optional ByVal proveedor As Long = -1) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_compra_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_compra_cerradas_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_compra_cerradas_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_compra_cerrar(ByVal orden As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_compra_cerrar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = orden

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_compra_abrir(ByVal orden As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_compra_abrir]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = orden

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_compra_cambiar_status(ByVal orden As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_compra_cambiar_status]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = orden

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function ca_despliega_surtimientos(ByVal orden As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[ca_despliega_surtimientos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
                .Item("@orden").Value = orden

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


