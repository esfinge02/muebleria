'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAcreedoresDiversos
'DATE:		20/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - AcreedoresDiversos"
Public Class clsAcreedoresDiversos
    Public Function sp_acreedores_diversos_ins(ByRef oData As DataSet, ByVal actualizacheque As Boolean, ByVal subtotal As Double, ByVal iva As Double, ByVal rfc As String, ByVal Folio_Poliza_Cheque As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_diversos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago", System.Data.SqlDbType.Int, 4, "folio_pago"))
                .Item("@folio_pago").Value = Connection.GetValue(oData, "folio_pago")
                .Item("@folio_pago").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_programacion", System.Data.SqlDbType.DateTime, 8, "fecha_programacion"))
                .Item("@fecha_programacion").Value = Connection.GetValue(oData, "fecha_programacion")
                .Add(New System.Data.SqlClient.SqlParameter("@numero_documento", System.Data.SqlDbType.Int, 4, "numero_documento"))
                .Item("@numero_documento").Value = Connection.GetValue(oData, "numero_documento")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_documento", System.Data.SqlDbType.DateTime, 8, "fecha_documento"))
                .Item("@fecha_documento").Value = Connection.GetValue(oData, "fecha_documento")
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = Connection.GetValue(oData, "importe")

                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = Connection.GetValue(oData, "acreedor")

                .Add(New System.Data.SqlClient.SqlParameter("@beneficiario", System.Data.SqlDbType.VarChar, 70, "beneficiario"))
                .Item("@beneficiario").Value = Connection.GetValue(oData, "beneficiario")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
                .Item("@tipo_pago").Value = Connection.GetValue(oData, "tipo_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@referencia_transferencia", System.Data.SqlDbType.Int, 4, "referencia_transferencia"))
                .Item("@referencia_transferencia").Value = Connection.GetValue(oData, "referencia_transferencia")
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_pago", System.Data.SqlDbType.DateTime, 8, "fecha_pago"))
                .Item("@fecha_pago").Value = Connection.GetValue(oData, "fecha_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = Connection.GetValue(oData, "cuenta_bancaria")
                '@ACH-27/06/07: Se agreg� un campo a la tabla
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_movimiento", System.Data.SqlDbType.Int, 4, "concepto_movimiento"))
                .Item("@concepto_movimiento").Value = Connection.GetValue(oData, "concepto_movimiento")
                '/@ACH-27/06/07
                .Add(New System.Data.SqlClient.SqlParameter("@folio_cheque", System.Data.SqlDbType.Int, 4, "folio_cheque"))
                .Item("@folio_cheque").Value = Connection.GetValue(oData, "folio_cheque")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_deposito", System.Data.SqlDbType.DateTime, 8, "fecha_deposito"))
                .Item("@fecha_deposito").Value = Connection.GetValue(oData, "fecha_deposito")
                .Add(New System.Data.SqlClient.SqlParameter("@abono_cuenta", System.Data.SqlDbType.Bit, 1, "abono_cuenta"))
                .Item("@abono_cuenta").Value = Connection.GetValue(oData, "abono_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Text, 0, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = subtotal
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = iva
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 50, "rfc"))
                .Item("@rfc").Value = rfc


                .Add(New System.Data.SqlClient.SqlParameter("@actualizacheque", System.Data.SqlDbType.Bit, 1, "actualizacheque"))
                .Item("@actualizacheque").Value = actualizacheque

                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = Folio_Poliza_Cheque

                '   22/jul/06 Quit� el control status
                '.Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                '.Item("@status").Value = Connection.GetValue(oData, "status")
                '   22/jul/06 Quit� el control poliza
                '.Add(New System.Data.SqlClient.22/jul/06 Quit� el control("@poliza", System.Data.SqlDbType.Int, 4, "poliza"))
                '.Item("@poliza").Value = Connection.GetValue(oData, "poliza")
                '   22/jul/06 Quit� el control estado_poliza
                '.Add(New System.Data.SqlClient.SqlParameter("@estado_poliza", System.Data.SqlDbType.Bit, 1, "estado_poliza"))
                '.Item("@estado_poliza").Value = Connection.GetValue(oData, "estado_poliza")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "folio_pago", oCommand.Parameters.Item("@folio_pago").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_diversos_ins(ByRef folio_pago As Long, ByVal fecha_programacion As DateTime, ByVal numero_documento As Long, ByVal fecha_documento As DateTime, _
    ByVal importe As Double, ByVal acreedor As Long, ByVal beneficiario As String, ByVal tipo_pago As Char, ByVal referencia_transferencia As Long, _
    ByVal banco As Long, ByVal fecha_pago As DateTime, ByVal cuenta_bancaria As String, ByVal ConceptoMovimiento As Long, ByVal folio_cheque As Long, ByVal fecha_deposito As DateTime, _
    ByVal abono_cuenta As Boolean, ByVal concepto As String, ByVal actualizacheque As Boolean, ByVal subtotal As Double, ByVal iva As Double, ByVal rfc As String, ByVal Folio_Poliza_Cheque As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_diversos_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago", System.Data.SqlDbType.Int, 4, "folio_pago"))
                .Item("@folio_pago").Value = folio_pago
                .Item("@folio_pago").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_programacion", System.Data.SqlDbType.DateTime, 8, "fecha_programacion"))
                .Item("@fecha_programacion").Value = fecha_programacion
                .Add(New System.Data.SqlClient.SqlParameter("@numero_documento", System.Data.SqlDbType.Int, 4, "numero_documento"))
                .Item("@numero_documento").Value = numero_documento
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_documento", System.Data.SqlDbType.DateTime, 8, "fecha_documento"))
                .Item("@fecha_documento").Value = fecha_documento

                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = acreedor
                .Add(New System.Data.SqlClient.SqlParameter("@beneficiario", System.Data.SqlDbType.VarChar, 70, "beneficiario"))
                .Item("@beneficiario").Value = beneficiario

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Char, 1, "tipo_pago"))
                .Item("@tipo_pago").Value = tipo_pago
                .Add(New System.Data.SqlClient.SqlParameter("@referencia_transferencia", System.Data.SqlDbType.Int, 4, "referencia_transferencia"))
                .Item("@referencia_transferencia").Value = referencia_transferencia
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_pago", System.Data.SqlDbType.DateTime, 8, "fecha_pago"))
                .Item("@fecha_pago").Value = fecha_pago
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = cuenta_bancaria
                '@ACH-27/06/07: Se agreg� un campo a la tabla
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_movimiento", System.Data.SqlDbType.Int, 4, "concepto_movimiento"))
                .Item("@concepto_movimiento").Value = ConceptoMovimiento
                '/@ACH-27/06/07
                .Add(New System.Data.SqlClient.SqlParameter("@folio_cheque", System.Data.SqlDbType.Int, 4, "folio_cheque"))
                .Item("@folio_cheque").Value = folio_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_deposito", System.Data.SqlDbType.DateTime, 8, "fecha_deposito"))
                .Item("@fecha_deposito").Value = fecha_deposito
                .Add(New System.Data.SqlClient.SqlParameter("@abono_cuenta", System.Data.SqlDbType.Bit, 1, "abono_cuenta"))
                .Item("@abono_cuenta").Value = abono_cuenta

                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Text, 0, "concepto"))
                .Item("@concepto").Value = concepto

                .Add(New System.Data.SqlClient.SqlParameter("@actualizacheque", System.Data.SqlDbType.Bit, 1, "actualizacheque"))
                .Item("@actualizacheque").Value = actualizacheque

                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = subtotal
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = iva
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 50, "rfc"))
                .Item("@rfc").Value = rfc

                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = Folio_Poliza_Cheque

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio_pago = oCommand.Parameters.Item("@folio_pago").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_diversos_upd(ByRef oData As DataSet, ByVal actualizacheque As Boolean, ByVal importe_anterior As Double, ByVal subtotal As Double, ByVal iva As Double, ByVal rfc As String, ByVal folio_poliza_cheque As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_diversos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago", System.Data.SqlDbType.Int, 4, "folio_pago"))
                .Item("@folio_pago").Value = Connection.GetValue(oData, "folio_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_programacion", System.Data.SqlDbType.DateTime, 8, "fecha_programacion"))
                .Item("@fecha_programacion").Value = Connection.GetValue(oData, "fecha_programacion")
                .Add(New System.Data.SqlClient.SqlParameter("@numero_documento", System.Data.SqlDbType.Int, 4, "numero_documento"))
                .Item("@numero_documento").Value = Connection.GetValue(oData, "numero_documento")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_documento", System.Data.SqlDbType.DateTime, 8, "fecha_documento"))
                .Item("@fecha_documento").Value = Connection.GetValue(oData, "fecha_documento")
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = Connection.GetValue(oData, "importe")
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = Connection.GetValue(oData, "acreedor")
                .Add(New System.Data.SqlClient.SqlParameter("@beneficiario", System.Data.SqlDbType.VarChar, 70, "beneficiario"))
                .Item("@beneficiario").Value = Connection.GetValue(oData, "beneficiario")                '22 de julio 2006 Quit� genera transferencia
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.NVarChar, 1, "tipo_pago"))
                .Item("@tipo_pago").Value = Connection.GetValue(oData, "tipo_pago")

                .Add(New System.Data.SqlClient.SqlParameter("@referencia_transferencia", System.Data.SqlDbType.Int, 4, "referencia_transferencia"))
                .Item("@referencia_transferencia").Value = Connection.GetValue(oData, "referencia_transferencia")

                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_pago", System.Data.SqlDbType.DateTime, 8, "fecha_pago"))
                .Item("@fecha_pago").Value = Connection.GetValue(oData, "fecha_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = Connection.GetValue(oData, "cuenta_bancaria")
                '@ACH-27/06/07: Se agreg� un campo a la tabla
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_movimiento", System.Data.SqlDbType.Int, 4, "concepto_movimiento"))
                .Item("@concepto_movimiento").Value = Connection.GetValue(oData, "concepto_movimiento")
                '/@ACH-27/06/07
                .Add(New System.Data.SqlClient.SqlParameter("@folio_cheque", System.Data.SqlDbType.Int, 4, "folio_cheque"))
                .Item("@folio_cheque").Value = Connection.GetValue(oData, "folio_cheque")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_deposito", System.Data.SqlDbType.DateTime, 8, "fecha_deposito"))
                .Item("@fecha_deposito").Value = Connection.GetValue(oData, "fecha_deposito")
                .Add(New System.Data.SqlClient.SqlParameter("@abono_cuenta", System.Data.SqlDbType.Bit, 1, "abono_cuenta"))
                .Item("@abono_cuenta").Value = Connection.GetValue(oData, "abono_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Text, 0, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")

                .Add(New System.Data.SqlClient.SqlParameter("@actualizacheque", System.Data.SqlDbType.Bit, 1, "actualizacheque"))
                .Item("@actualizacheque").Value = actualizacheque
                .Add(New System.Data.SqlClient.SqlParameter("@importe_anterior", System.Data.SqlDbType.Int, 4, "importe_anterior"))
                .Item("@importe_anterior").Value = importe_anterior
                .Add(New System.Data.SqlClient.SqlParameter("@subtotal", System.Data.SqlDbType.Money, 8, "subtotal"))
                .Item("@subtotal").Value = subtotal
                .Add(New System.Data.SqlClient.SqlParameter("@iva", System.Data.SqlDbType.Money, 8, "iva"))
                .Item("@iva").Value = iva
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 50, "rfc"))
                .Item("@rfc").Value = rfc

                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_diversos_del(ByVal folio_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_diversos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago", System.Data.SqlDbType.Int, 4, "folio_pago"))
                .Item("@folio_pago").Value = folio_pago

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_diversos_exs(ByVal folio_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_diversos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago", System.Data.SqlDbType.Int, 4, "folio_pago"))
                .Item("@folio_pago").Value = folio_pago

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_diversos_sel(ByVal folio_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_diversos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_pago", System.Data.SqlDbType.Int, 4, "folio_pago"))
                .Item("@folio_pago").Value = folio_pago

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_diversos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_diversos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region
