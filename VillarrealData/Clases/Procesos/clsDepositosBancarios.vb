'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDepositosBancarios
'DATE:		30/06/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - DepositosBancarios"
Public Class clsDepositosBancarios

    Private oCommandInsert As New System.Data.SqlClient.SqlCommand
    Private oCommandUpdate As New System.Data.SqlClient.SqlCommand
    Private oCommandUpdateAplicado As New System.Data.SqlClient.SqlCommand
    Private oCommandDelete As New System.Data.SqlClient.SqlCommand
    Private oCommandExists As New System.Data.SqlClient.SqlCommand
    Private oCommandSelect As New System.Data.SqlClient.SqlCommand
    Private oCommandGridSelect As New System.Data.SqlClient.SqlCommand

    Public Sub New()

        With oCommandInsert
            .CommandText = "[sp_depositos_bancarios_ins]"
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@consecutivo", System.Data.SqlDbType.Int, 4, "consecutivo"))
            .Parameters.Item("@consecutivo").Direction = ParameterDirection.InputOutput
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@chequera", System.Data.SqlDbType.VarChar, 20, "chequera"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@folio_movimiento_chequera", System.Data.SqlDbType.Int, 4, "folio_movimiento_chequera"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@depositante", System.Data.SqlDbType.VarChar, 30, "depositante"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Float, 8, "importe"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@aplicado_movimiento_chequera", System.Data.SqlDbType.Bit, 1, "aplicado_movimiento_chequera"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@usuario_inserto", System.Data.SqlDbType.VarChar, 15, "usuario_inserto"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@usuario_aplico", System.Data.SqlDbType.VarChar, 15, "usuario_aplico"))

            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo_deposito", System.Data.SqlDbType.Int, 4, "Tipo_deposito"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha_deposito", System.Data.SqlDbType.DateTime, 8, "fecha_deposito"))

            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))

        End With

        With oCommandUpdate
            .CommandText = "[sp_depositos_bancarios_upd]"
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@consecutivo", System.Data.SqlDbType.Int, 4, "consecutivo"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@chequera", System.Data.SqlDbType.VarChar, 20, "chequera"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@folio_movimiento_chequera", System.Data.SqlDbType.Int, 4, "folio_movimiento_chequera"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@depositante", System.Data.SqlDbType.VarChar, 30, "depositante"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Float, 8, "importe"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@aplicado_movimiento_chequera", System.Data.SqlDbType.Bit, 1, "aplicado_movimiento_chequera"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@usuario_inserto", System.Data.SqlDbType.VarChar, 15, "usuario_inserto"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@usuario_aplico", System.Data.SqlDbType.VarChar, 15, "usuario_aplico"))

            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo_deposito", System.Data.SqlDbType.Int, 4, "Tipo_deposito"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha_deposito", System.Data.SqlDbType.DateTime, 8, "fecha_deposito"))

            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))

        End With

        With oCommandUpdateAplicado
            .CommandText = "[sp_depositos_bancarios_aplicado_upd]"
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@consecutivo", System.Data.SqlDbType.Int, 4, "consecutivo"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@usuario_aplico", System.Data.SqlDbType.VarChar, 15, "usuario_aplico"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))

            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))

        End With

        With oCommandDelete
            .CommandText = "[sp_depositos_bancarios_del]"
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@consecutivo", System.Data.SqlDbType.Int, 4, "consecutivo"))

        End With

        With oCommandExists
            .CommandText = "[sp_depositos_bancarios_exs]"
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@consecutivo", System.Data.SqlDbType.Int, 4, "consecutivo"))

        End With

        With oCommandSelect
            .CommandText = "[sp_depositos_bancarios_sel]"
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@consecutivo", System.Data.SqlDbType.Int, 4, "consecutivo"))

        End With

        With oCommandGridSelect
            .CommandText = "[sp_depositos_bancarios_grs]"
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@aplicados", System.Data.SqlDbType.Int, 4, "aplicados"))
        End With

    End Sub

    Public Function sp_depositos_bancarios_ins(ByRef oData As DataSet) As Events
        Dim oEvent As New Events
        Try
            With oCommandInsert.Parameters
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Item("@consecutivo").Value = Connection.GetValue(oData, "consecutivo")
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Item("@chequera").Value = Connection.GetValue(oData, "chequera")
                .Item("@folio_movimiento_chequera").Value = Connection.GetValue(oData, "folio_movimiento_chequera")
                .Item("@depositante").Value = Connection.GetValue(oData, "depositante")
                .Item("@importe").Value = Connection.GetValue(oData, "importe")
                .Item("@aplicado_movimiento_chequera").Value = Connection.GetValue(oData, "aplicado_movimiento_chequera")
                .Item("@usuario_inserto").Value = Connection.User
                .Item("@usuario_aplico").Value = Connection.GetValue(oData, "usuario_aplico")

                .Item("@Tipo_deposito").Value = Connection.GetValue(oData, "Tipo_deposito")
                .Item("@fecha_deposito").Value = Connection.GetValue(oData, "fecha_deposito")

                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommandInsert)
            Connection.SetValue(oData, "consecutivo", oCommandInsert.Parameters.Item("@consecutivo").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandInsert.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_depositos_bancarios_upd(ByRef oData As DataSet) As Events
        Dim oEvent As New Events
        Try
            With oCommandUpdate.Parameters
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Item("@consecutivo").Value = Connection.GetValue(oData, "consecutivo")
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Item("@chequera").Value = Connection.GetValue(oData, "chequera")
                .Item("@folio_movimiento_chequera").Value = Connection.GetValue(oData, "folio_movimiento_chequera")
                .Item("@depositante").Value = Connection.GetValue(oData, "depositante")
                .Item("@importe").Value = Connection.GetValue(oData, "importe")
                .Item("@aplicado_movimiento_chequera").Value = Connection.GetValue(oData, "aplicado_movimiento_chequera")
                .Item("@usuario_inserto").Value = Connection.GetValue(oData, "usuario_inserto")
                .Item("@usuario_aplico").Value = Connection.GetValue(oData, "usuario_aplico")

                .Item("@Tipo_deposito").Value = Connection.GetValue(oData, "Tipo_deposito")
                .Item("@fecha_deposito").Value = Connection.GetValue(oData, "fecha_deposito")

                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommandUpdate)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandUpdate.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_depositos_bancarios_aplicado_upd(ByVal sucursal As Long, ByVal fecha As DateTime, ByVal consecutivo As Long, ByVal folio As Long) As Events
        Dim oEvent As New Events
        Try
            With oCommandUpdateAplicado.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@fecha").Value = fecha
                .Item("@consecutivo").Value = consecutivo
                .Item("@usuario_aplico").Value = Connection.User
                .Item("@folio").Value = folio
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommandUpdateAplicado)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandUpdateAplicado.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function


    Public Function sp_depositos_bancarios_del(ByVal sucursal As Long, ByVal fecha As Date, ByVal consecutivo As Long) As Events
        Dim oEvent As New Events
        Try
            With oCommandDelete.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@fecha").Value = fecha
                .Item("@consecutivo").Value = consecutivo

            End With
            Connection.Execute(oCommandDelete)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandDelete.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_depositos_bancarios_exs(ByVal sucursal As Long, ByVal fecha As Date, ByVal consecutivo As Long) As Events
        Dim oEvent As New Events
        Try
            With oCommandExists.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@fecha").Value = fecha
                .Item("@consecutivo").Value = consecutivo

            End With
            oEvent.Value = Connection.Execute(oCommandExists)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandExists.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_depositos_bancarios_sel(ByVal sucursal As Long, ByVal fecha As Date, ByVal consecutivo As Long) As Events
        Dim oEvent As New Events
        Try
            oCommandSelect.CommandText = "[sp_depositos_bancarios_sel]"
            With oCommandSelect.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@fecha").Value = fecha
                .Item("@consecutivo").Value = consecutivo

            End With
            oEvent.Value = Connection.GetDataSet(oCommandSelect)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandSelect.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_depositos_bancarios_grs(ByVal sucursal As Long, ByVal fecha As DateTime, ByVal aplicados As Long) As Events
        Dim oEvent As New Events
        Try
            With oCommandGridSelect.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@fecha").Value = fecha
                .Item("@aplicados").Value = aplicados

            End With
            oEvent.Value = Connection.GetDataSet(oCommandGridSelect)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandGridSelect.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function



End Class
#End Region


