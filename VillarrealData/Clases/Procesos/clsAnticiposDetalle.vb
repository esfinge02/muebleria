Imports Dipros.Utils

Public Class clsAnticiposDetalle

    Public Function sp_anticipos_detalle_ins(ByVal folio As Long, ByVal sucursal_factura_aplicado As Long, ByVal serie_factura_aplicado As String, ByVal folio_factura_aplicado As Long, ByVal fecha As DateTime, ByVal importe As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_detalle_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura_aplicado", System.Data.SqlDbType.Int, 4, "sucursal_factura_aplicado"))
                .Item("@sucursal_factura_aplicado").Value = sucursal_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_aplicado", System.Data.SqlDbType.VarChar, 3, "serie_factura_aplicado"))
                .Item("@serie_factura_aplicado").Value = serie_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_aplicado", System.Data.SqlDbType.Int, 4, "folio_factura_aplicado"))
                .Item("@folio_factura_aplicado").Value = folio_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_anticipos_detalle_upd(ByVal folio As Long, ByVal sucursal_factura_aplicado As Long, ByVal serie_factura_aplicado As String, ByVal folio_factura_aplicado As Long, ByVal fecha As DateTime, ByVal importe As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_detalle_upd]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura_aplicado", System.Data.SqlDbType.Int, 4, "sucursal_factura_aplicado"))
                .Item("@sucursal_factura_aplicado").Value = sucursal_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_aplicado", System.Data.SqlDbType.VarChar, 3, "serie_factura_aplicado"))
                .Item("@serie_factura_aplicado").Value = serie_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_aplicado", System.Data.SqlDbType.Int, 4, "folio_factura_aplicado"))
                .Item("@folio_factura_aplicado").Value = folio_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_anticipos_detalle_del(ByVal folio As Long, ByVal sucursal_factura_aplicado As Long, ByVal serie_factura_aplicado As String, ByVal folio_factura_aplicado As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura_aplicado", System.Data.SqlDbType.Int, 4, "sucursal_factura_aplicado"))
                .Item("@sucursal_factura_aplicado").Value = sucursal_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_aplicado", System.Data.SqlDbType.VarChar, 3, "serie_factura_aplicado"))
                .Item("@serie_factura_aplicado").Value = serie_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_aplicado", System.Data.SqlDbType.Int, 4, "folio_factura_aplicado"))
                .Item("@folio_factura_aplicado").Value = folio_factura_aplicado


            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_anticipos_detalle_grs(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_anticipos_detalle_ventas_grs(ByVal sucursal_factura_aplicado As Long, ByVal serie_factura_aplicado As String, ByVal folio_factura_aplicado As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_anticipos_detalle_ventas_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura_aplicado", System.Data.SqlDbType.Int, 4, "sucursal_factura_aplicado"))
                .Item("@sucursal_factura_aplicado").Value = sucursal_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_aplicado", System.Data.SqlDbType.VarChar, 3, "serie_factura_aplicado"))
                .Item("@serie_factura_aplicado").Value = serie_factura_aplicado
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_aplicado", System.Data.SqlDbType.Int, 4, "folio_factura_aplicado"))
                .Item("@folio_factura_aplicado").Value = folio_factura_aplicado
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
