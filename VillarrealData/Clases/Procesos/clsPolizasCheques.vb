'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPolizasCheques
'DATE:		11/04/2008 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - PolizasCheques"
Public Class clsPolizasCheques
    Public Function sp_polizas_cheques_ins(ByRef oData As DataSet, ByVal folio_poliza_cheque As Long, ByVal tipo_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_polizas_cheques_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_contable", System.Data.SqlDbType.VarChar, 40, "cuenta_contable"))
                .Item("@cuenta_contable").Value = Connection.GetValue(oData, "cuenta_contable")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 120, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = Connection.GetValue(oData, "cargo")
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = Connection.GetValue(oData, "abono")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_pago", System.Data.SqlDbType.Int, 4, "tipo_pago"))
                .Item("@tipo_pago").Value = tipo_pago
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_polizas_cheques_upd(ByRef oData As DataSet, ByVal folio_poliza_cheque As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_polizas_cheques_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_contable", System.Data.SqlDbType.VarChar, 40, "cuenta_contable"))
                .Item("@cuenta_contable").Value = Connection.GetValue(oData, "cuenta_contable")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.VarChar, 120, "concepto"))
                .Item("@concepto").Value = Connection.GetValue(oData, "concepto")
                .Add(New System.Data.SqlClient.SqlParameter("@cargo", System.Data.SqlDbType.Money, 8, "cargo"))
                .Item("@cargo").Value = Connection.GetValue(oData, "cargo")
                .Add(New System.Data.SqlClient.SqlParameter("@abono", System.Data.SqlDbType.Money, 8, "abono"))
                .Item("@abono").Value = Connection.GetValue(oData, "abono")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_polizas_cheques_del(ByVal folio_poliza_cheque As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_polizas_cheques_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_polizas_cheques_exs(ByVal folio_poliza_cheque As Long, ByVal partida As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_polizas_cheques_exs]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
    '            .Item("@folio_poliza_cheque").Value = folio_poliza_cheque
    '            .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
    '            .Item("@partida").Value = partida

    '        End With
    '        oEvent.Value = Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_polizas_cheques_siguiente_folio() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_polizas_cheques_siguiente_folio]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                '.Item("@folio_poliza_cheque").Value = folio_poliza_cheque
                '.Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                '.Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_polizas_cheques_sel(ByVal folio_poliza_cheque As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_polizas_cheques_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_poliza_cheque", System.Data.SqlDbType.Int, 4, "folio_poliza_cheque"))
                .Item("@folio_poliza_cheque").Value = folio_poliza_cheque
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_polizas_cheques_grs() As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_polizas_cheques_grs]"
    '        With oCommand.Parameters

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function



End Class
#End Region


