'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsTraspasos
'DATE:		15/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Traspasos"
Public Class clsTraspasos
    Public Function sp_traspasos_ins(ByRef oData As DataSet, ByVal sucursal_salida As Long, ByVal folio_salida As Long, ByVal cancelado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspaso", System.Data.SqlDbType.Int, 4, "traspaso"))
                .Item("@traspaso").Value = Connection.GetValue(oData, "traspaso")
                .Item("@traspaso").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_salida", System.Data.SqlDbType.Int, 4, "sucursal_salida"))
                .Item("@sucursal_salida").Value = sucursal_salida
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_salida", System.Data.SqlDbType.VarChar, 5, "bodega_salida"))
                .Item("@bodega_salida").Value = Connection.GetValue(oData, "bodega_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_salida", System.Data.SqlDbType.VarChar, 5, "concepto_salida"))
                .Item("@concepto_salida").Value = Connection.GetValue(oData, "concepto_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_salida", System.Data.SqlDbType.Int, 4, "folio_salida"))
                .Item("@folio_salida").Value = folio_salida
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_salida", System.Data.SqlDbType.Text, 0, "observaciones_salida"))
                .Item("@observaciones_salida").Value = Connection.GetValue(oData, "observaciones_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_entrada", System.Data.SqlDbType.Int, 4, "sucursal_entrada"))
                .Item("@sucursal_entrada").Value = Connection.GetValue(oData, "sucursal_entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = Connection.GetValue(oData, "bodega_entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@cancelado", System.Data.SqlDbType.Bit, 1, "cancelado"))
                .Item("@cancelado").Value = cancelado
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@chofer", System.Data.SqlDbType.Int, 4, "chofer"))
                .Item("@chofer").Value = Connection.GetValue(oData, "chofer")
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = Connection.GetValue(oData, "solicitud")

                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = Connection.GetValue(oData, "bodeguero")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "traspaso", oCommand.Parameters.Item("@traspaso").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traspasos_upd(ByRef oData As DataSet, ByVal sucursal_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspaso", System.Data.SqlDbType.Int, 4, "traspaso"))
                .Item("@traspaso").Value = Connection.GetValue(oData, "traspaso")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_salida", System.Data.SqlDbType.Int, 4, "sucursal_salida"))
                .Item("@sucursal_salida").Value = sucursal_salida
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_salida", System.Data.SqlDbType.VarChar, 5, "bodega_salida"))
                .Item("@bodega_salida").Value = Connection.GetValue(oData, "bodega_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_salida", System.Data.SqlDbType.VarChar, 5, "concepto_salida"))
                .Item("@concepto_salida").Value = Connection.GetValue(oData, "concepto_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_salida", System.Data.SqlDbType.Int, 4, "folio_salida"))
                .Item("@folio_salida").Value = Connection.GetValue(oData, "folio_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_salida", System.Data.SqlDbType.Text, 0, "observaciones_salida"))
                .Item("@observaciones_salida").Value = Connection.GetValue(oData, "observaciones_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_entrada", System.Data.SqlDbType.Int, 4, "sucursal_entrada"))
                .Item("@sucursal_entrada").Value = Connection.GetValue(oData, "sucursal_entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = Connection.GetValue(oData, "bodega_entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@status", System.Data.SqlDbType.Char, 1, "status"))
                .Item("@status").Value = Connection.GetValue(oData, "status")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@chofer", System.Data.SqlDbType.Int, 4, "chofer"))
                .Item("@chofer").Value = Connection.GetValue(oData, "chofer")
                .Add(New System.Data.SqlClient.SqlParameter("@solicitud", System.Data.SqlDbType.Int, 4, "solicitud"))
                .Item("@solicitud").Value = Connection.GetValue(oData, "solicitud")

                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = Connection.GetValue(oData, "bodeguero")


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traspasos_entrada_upd(ByVal traspaso As Long, ByVal concepto_entrada As String, ByVal folio_entrada As Long, ByVal observaciones_entrada As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_entrada_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspaso", System.Data.SqlDbType.Int, 4, "traspaso"))
                .Item("@traspaso").Value = traspaso
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_entrada", System.Data.SqlDbType.VarChar, 5, "concepto_entrada"))
                .Item("@concepto_entrada").Value = concepto_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@folio_entrada", System.Data.SqlDbType.Int, 4, "folio_entrada"))
                .Item("@folio_entrada").Value = folio_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_entrada", System.Data.SqlDbType.Text, 0, "observaciones_entrada"))
                .Item("@observaciones_entrada").Value = observaciones_entrada


            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_traspasos_folio_salida(ByVal traspaso As Long, ByVal folio_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_folio_salida]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspaso", System.Data.SqlDbType.Int, 4, "traspaso"))
                .Item("@traspaso").Value = traspaso
                .Add(New System.Data.SqlClient.SqlParameter("@folio_salida", System.Data.SqlDbType.Int, 4, "folio_salida"))
                .Item("@folio_salida").Value = folio_salida

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traspasos_del(ByVal traspaso As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspaso", System.Data.SqlDbType.Int, 4, "traspaso"))
                .Item("@traspaso").Value = traspaso

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traspasos_exs(ByVal traspaso As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspaso", System.Data.SqlDbType.Int, 4, "traspaso"))
                .Item("@traspaso").Value = traspaso

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traspasos_sel(ByVal traspaso As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@traspaso", System.Data.SqlDbType.Int, 4, "traspaso"))
                .Item("@traspaso").Value = traspaso
                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.NVarChar, 30, "usuario"))
                .Item("@usuario").Value = Connection.User
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traspasos_grs(ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traspasos_detalles(ByVal bodega_salida As String, ByVal bodega_entrada As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_detalles]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@BODEGA_SALIDA", System.Data.SqlDbType.Text, 0, "BODEGA_SALIDA"))
                .Item("@BODEGA_SALIDA").Value = bodega_salida
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = bodega_entrada
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traspasos_salidas_actualizaexistencias_ins(ByVal bodega_entrada As String, ByVal articulo As Long, ByVal cantidad As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_salidas_actualizaexistencias_ins]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@bodega_salida", System.Data.SqlDbType.VarChar, 5, "bodega_salida"))
                '.Item("@bodega_salida").Value = bodega_salida
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = bodega_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Decimal, 9, "cantidad"))
                .Item("@cantidad").Value = cantidad
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function

    Public Function sp_traspasos_salidas_actualizaexistencias_del(ByVal bodega_entrada As String, ByVal articulo As Long, ByVal cantidad As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traspasos_salidas_actualizaexistencias_del]"
            With oCommand.Parameters
                '.Add(New System.Data.SqlClient.SqlParameter("@bodega_salida", System.Data.SqlDbType.VarChar, 5, "bodega_salida"))
                '.Item("@bodega_salida").Value = bodega_salida
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = bodega_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Decimal, 9, "cantidad"))
                .Item("@cantidad").Value = cantidad
                '.Add(New System.Data.SqlClient.SqlParameter("@cancelacion", System.Data.SqlDbType.Bit, 1, "cancelacion"))
                '.Item("@cancelacion").Value = cancelacion

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function

End Class
#End Region


