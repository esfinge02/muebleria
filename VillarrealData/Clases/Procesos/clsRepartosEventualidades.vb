'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsRepartosEventualidades
'DATE:		17/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - RepartosEventualidades"
Public Class clsRepartosEventualidades
    Public Function sp_repartos_eventualidades_ins(ByRef oData As DataSet, ByVal sucursal As Long, ByVal reparto As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_eventualidades_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
                .Item("@eventualidad").Value = Connection.GetValue(oData, "eventualidad")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@capturo", System.Data.SqlDbType.VarChar, 15, "capturo"))
                .Item("@capturo").Value = Connection.GetValue(oData, "capturo")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
           

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_eventualidades_upd(ByRef oData As DataSet, ByVal sucursal As Long, ByVal reparto As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_eventualidades_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
                .Item("@eventualidad").Value = Connection.GetValue(oData, "eventualidad")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@capturo", System.Data.SqlDbType.VarChar, 15, "capturo"))
                .Item("@capturo").Value = Connection.GetValue(oData, "capturo")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_eventualidades_del(ByVal reparto As Long, ByVal eventualidad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_eventualidades_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
                .Item("@eventualidad").Value = eventualidad

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_eventualidades_exs(ByVal reparto As Long, ByVal eventualidad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_eventualidades_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
                .Item("@eventualidad").Value = eventualidad

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_eventualidades_sel(ByVal reparto As Long, ByVal eventualidad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_eventualidades_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
                .Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
                .Item("@eventualidad").Value = eventualidad

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_repartos_eventualidades_grs(ByVal reparto As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_repartos_eventualidades_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@reparto", System.Data.SqlDbType.Int, 4, "reparto"))
                .Item("@reparto").Value = reparto
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


