Imports Dipros.Utils

Public Class clsVales
    Public Function sp_vales_ins(ByRef folio As Long, ByVal fecha As DateTime, ByVal cliente As Long, ByVal sucursal_factura As Long, ByVal serie_factura As String, _
    ByVal folio_factura As Long, ByVal importe As Double, ByVal saldo As Double, ByVal estatus As Char, ByVal vigencia As DateTime, ByVal tipo_vale As Long, _
    ByVal convenio As Long, ByVal anio_convenio As Long, ByVal observaciones As String, ByVal nombre_titular As String, ByVal nombre_autoriza_canjear As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Item("@folio").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura

                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura

                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura


                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = importe


                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = estatus

                .Add(New System.Data.SqlClient.SqlParameter("@vigencia", System.Data.SqlDbType.DateTime, 8, "vigencia"))
                .Item("@vigencia").Value = vigencia

                .Add(New System.Data.SqlClient.SqlParameter("@usuario_genera", System.Data.SqlDbType.VarChar, 15, "usuario_genera"))
                .Item("@usuario_genera").Value = Connection.User

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_vale", System.Data.SqlDbType.Int, 4, "tipo_vale"))
                .Item("@tipo_vale").Value = tipo_vale

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@anio_convenio", System.Data.SqlDbType.Int, 4, "anio_convenio"))
                .Item("@anio_convenio").Value = anio_convenio

                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 100, "observaciones"))
                .Item("@observaciones").Value = observaciones


                .Add(New System.Data.SqlClient.SqlParameter("@nombre_titular", System.Data.SqlDbType.VarChar, 100, "nombre_titular"))
                .Item("@nombre_titular").Value = nombre_titular


                .Add(New System.Data.SqlClient.SqlParameter("@nombre_autoriza_canjear", System.Data.SqlDbType.VarChar, 100, "nombre_autoriza_canjear"))
                .Item("@nombre_autoriza_canjear").Value = nombre_autoriza_canjear

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            folio = oCommand.Parameters.Item("@folio").Value
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent


    End Function

    Public Function sp_vales_upd(ByVal folio As Long, ByVal fecha As DateTime, ByVal cliente As Long, ByVal sucursal_factura As Long, ByVal serie_factura As String, _
    ByVal folio_factura As Long, ByVal importe As Double, ByVal saldo As Double, ByVal estatus As Char, ByVal vigencia As DateTime, ByVal tipo_vale As Long, _
    ByVal convenio As Long, ByVal anio_convenio As Long, ByVal observaciones As String, ByVal nombre_titular As String, ByVal nombre_autoriza_canjear As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio


                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura

                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura

                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura


                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe

                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = saldo


                .Add(New System.Data.SqlClient.SqlParameter("@estatus", System.Data.SqlDbType.Char, 1, "estatus"))
                .Item("@estatus").Value = estatus

                .Add(New System.Data.SqlClient.SqlParameter("@vigencia", System.Data.SqlDbType.DateTime, 8, "vigencia"))
                .Item("@vigencia").Value = vigencia

                .Add(New System.Data.SqlClient.SqlParameter("@usuario_genera", System.Data.SqlDbType.VarChar, 15, "usuario_genera"))
                .Item("@usuario_genera").Value = Connection.User


                .Add(New System.Data.SqlClient.SqlParameter("@tipo_vale", System.Data.SqlDbType.Int, 4, "tipo_vale"))
                .Item("@tipo_vale").Value = tipo_vale

                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio

                .Add(New System.Data.SqlClient.SqlParameter("@anio_convenio", System.Data.SqlDbType.Int, 4, "anio_convenio"))
                .Item("@anio_convenio").Value = anio_convenio

                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 100, "observaciones"))
                .Item("@observaciones").Value = observaciones


                .Add(New System.Data.SqlClient.SqlParameter("@nombre_titular", System.Data.SqlDbType.VarChar, 100, "nombre_titular"))
                .Item("@nombre_titular").Value = nombre_titular


                .Add(New System.Data.SqlClient.SqlParameter("@nombre_autoriza_canjear", System.Data.SqlDbType.VarChar, 100, "nombre_autoriza_canjear"))
                .Item("@nombre_autoriza_canjear").Value = nombre_autoriza_canjear

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent


    End Function

    Public Function sp_vales_del(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vales_validacion_sel(ByVal folio As Long, ByVal cliente As Long, ByVal importe_pago As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_validacion_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@importe_pago", System.Data.SqlDbType.Money, 8, "importe_pago"))
                .Item("@importe_pago").Value = importe_pago

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vales_sel(ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vales_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_grl]"
            With oCommand.Parameters


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vales_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vales_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_valida_factura_cliente_sel(ByVal Cliente As Long, ByVal SucursalVenta As Long, ByVal SerieVenta As String, ByVal FolioVenta As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_valida_factura_cliente_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Cliente

                .Add(New System.Data.SqlClient.SqlParameter("@SucursalVenta", System.Data.SqlDbType.Int, 4, "SucursalVenta"))
                .Item("@SucursalVenta").Value = SucursalVenta


                .Add(New System.Data.SqlClient.SqlParameter("@SerieVenta", System.Data.SqlDbType.VarChar, 3, "SerieVenta"))
                .Item("@SerieVenta").Value = SerieVenta


                .Add(New System.Data.SqlClient.SqlParameter("@FolioVenta", System.Data.SqlDbType.Int, 4, "FolioVenta"))
                .Item("@FolioVenta").Value = FolioVenta

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
