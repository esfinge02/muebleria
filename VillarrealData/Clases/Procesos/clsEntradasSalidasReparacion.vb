Imports Dipros.Utils

Public Class clsEntradasSalidasReparacion

    Public Function sp_entradas_salidas_GeneraSalida_upd(ByVal orden_servicio As Long, ByVal fecha_salida_reparacion As DateTime, ByVal observaciones_salida_reparacion As String, ByVal BodegueroSalida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_salidas_GeneraSalida_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                '.Add(New System.Data.SqlClient.SqlParameter("@folio_movimiento_salida", System.Data.SqlDbType.Int, 4, "folio_movimiento_salida"))
                '.Item("@folio_movimiento_salida").Value = folio_movimiento_salida
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_salida_reparacion", System.Data.SqlDbType.DateTime, 8, "fecha_salida_reparacion"))
                .Item("@fecha_salida_reparacion").Value = fecha_salida_reparacion
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_salida_reparacion", System.Data.SqlDbType.Text, 0, "observaciones_salida_reparacion"))
                .Item("@observaciones_salida_reparacion").Value = observaciones_salida_reparacion

                .Add(New System.Data.SqlClient.SqlParameter("@BodegueroSalida", System.Data.SqlDbType.Int, 4, "BodegueroSalida"))
                .Item("@BodegueroSalida").Value = BodegueroSalida

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_entradas_salidas_GeneraEntrada_upd(ByVal orden_servicio As Long, ByVal fecha_entrada_reparacion As DateTime, ByVal observaciones_entrada_reparacion As String, ByVal BodegueroEntrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_salidas_GeneraEntrada_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
                '.Add(New System.Data.SqlClient.SqlParameter("@folio_movimiento_entrada", System.Data.SqlDbType.Int, 4, "folio_movimiento_entrada"))
                '.Item("@folio_movimiento_entrada").Value = folio_movimiento_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_entrada_reparacion", System.Data.SqlDbType.DateTime, 8, "fecha_entrada_reparacion"))
                .Item("@fecha_entrada_reparacion").Value = fecha_entrada_reparacion
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_entrada_reparacion", System.Data.SqlDbType.Text, 0, "observaciones_entrada_reparacion"))
                .Item("@observaciones_entrada_reparacion").Value = observaciones_entrada_reparacion

                .Add(New System.Data.SqlClient.SqlParameter("@BodegueroEntrada", System.Data.SqlDbType.Int, 4, "BodegueroEntrada"))
                .Item("@BodegueroEntrada").Value = BodegueroEntrada

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_entradas_salidas_reparacion_sel(ByVal orden_servicio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_entradas_salidas_reparacion_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@orden_servicio", System.Data.SqlDbType.Int, 4, "orden_servicio"))
                .Item("@orden_servicio").Value = orden_servicio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ordenes_servicio_reparaciones_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ordenes_servicio_reparaciones_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
