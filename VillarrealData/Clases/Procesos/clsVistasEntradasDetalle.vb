'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVistasEntradasDetalle
'DATE:		28/12/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - VistasEntradasDetalle"
Public Class clsVistasEntradasDetalle
    Public Function sp_vistas_entradas_detalle_ins(ByVal folio_vista_entrada As Long, ByVal partida As Long, ByVal articulo As Long, ByVal numero_serie As String, ByVal bodega_entrada As String, ByVal concepto_inventario_entrada As String, ByVal folio_inventario_entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_entradas_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_entrada", System.Data.SqlDbType.Int, 4, "folio_vista_entrada"))
                .Item("@folio_vista_entrada").Value = folio_vista_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = bodega_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_inventario_entrada", System.Data.SqlDbType.VarChar, 5, "concepto_inventario_entrada"))
                .Item("@concepto_inventario_entrada").Value = concepto_inventario_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@folio_inventario_entrada", System.Data.SqlDbType.Int, 4, "folio_inventario_entrada"))
                .Item("@folio_inventario_entrada").Value = folio_inventario_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_entradas_detalle_upd(ByVal folio_vista_entrada As Long, ByVal partida As Long, ByVal articulo As Long, ByVal numero_serie As String, ByVal bodega_entrada As String, ByVal concepto_inventario_entrada As String, ByVal folio_inventario_entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_entradas_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_entrada", System.Data.SqlDbType.Int, 4, "folio_vista_entrada"))
                .Item("@folio_vista_entrada").Value = folio_vista_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@numero_serie", System.Data.SqlDbType.VarChar, 30, "numero_serie"))
                .Item("@numero_serie").Value = numero_serie
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_entrada", System.Data.SqlDbType.VarChar, 5, "bodega_entrada"))
                .Item("@bodega_entrada").Value = bodega_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_inventario_entrada", System.Data.SqlDbType.VarChar, 5, "concepto_inventario_entrada"))
                .Item("@concepto_inventario_entrada").Value = concepto_inventario_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@folio_inventario_entrada", System.Data.SqlDbType.Int, 4, "folio_inventario_entrada"))
                .Item("@folio_inventario_entrada").Value = folio_inventario_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_entradas_detalle_del(ByVal folio_vista_entrada As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_entradas_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_entrada", System.Data.SqlDbType.Int, 4, "folio_vista_entrada"))
                .Item("@folio_vista_entrada").Value = folio_vista_entrada
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_vistas_entradas_detalle_exs(ByVal folio_vista_entrada As Long, ByVal partida As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_vistas_entradas_detalle_exs]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_entrada", System.Data.SqlDbType.Int, 4, "folio_vista_entrada"))
    '            .Item("@folio_vista_entrada").Value = folio_vista_entrada
    '            .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
    '            .Item("@partida").Value = partida

    '        End With
    '        oEvent.Value = Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_vistas_entradas_detalle_sel(ByVal folio_vista_entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_entradas_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_entrada", System.Data.SqlDbType.Int, 4, "folio_vista_entrada"))
                .Item("@folio_vista_entrada").Value = folio_vista_entrada
                '.Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                '.Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_entradas_detalle_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_entradas_detalle_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vistas_entradas_articulos_sin_entregar_facturar(ByVal folio_vista_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_entradas_articulos_sin_entregar_facturar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_vistas_entradas_articulos_sin_entregar_facturar_pto_venta(ByVal folio_vista_salida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vistas_entradas_articulos_sin_entregar_facturar_pto_venta]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_vista_salida", System.Data.SqlDbType.Int, 4, "folio_vista_salida"))
                .Item("@folio_vista_salida").Value = folio_vista_salida


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


