'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCambioCheques
'DATE:		22/06/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - CambioCheques"
Public Class clsCambioCheques

    'Public Connection As Dipros.Data.Data

	Private oCommandInsert As New System.Data.SqlClient.SqlCommand
	Private oCommandUpdate As New System.Data.SqlClient.SqlCommand
	Private oCommandDelete As New System.Data.SqlClient.SqlCommand
	Private oCommandExists As New System.Data.SqlClient.SqlCommand
	Private oCommandSelect As New System.Data.SqlClient.SqlCommand
	Private oCommandGridSelect As New System.Data.SqlClient.SqlCommand

	Public Sub New()

		With oCommandInsert
			.CommandText = "[sp_cambio_cheques_ins]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
			.Parameters.Item("@sucursal").Direction = ParameterDirection.InputOutput
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
			.Parameters.Item("@caja").Direction = ParameterDirection.InputOutput
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
			.Parameters.Item("@fecha").Direction = ParameterDirection.InputOutput
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
			.Parameters.Item("@partida").Direction = ParameterDirection.InputOutput
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.VarChar, 25, "banco"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@numero_cheque", System.Data.SqlDbType.VarChar, 40, "numero_cheque"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Float, 8, "monto"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))

		End With

		With oCommandUpdate
			.CommandText = "[sp_cambio_cheques_upd]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.VarChar, 25, "banco"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@numero_cheque", System.Data.SqlDbType.VarChar, 40, "numero_cheque"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@monto", System.Data.SqlDbType.Float, 8, "monto"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))

		End With

		With oCommandDelete
			.CommandText = "[sp_cambio_cheques_del]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))

		End With

		With oCommandExists
			.CommandText = "[sp_cambio_cheques_exs]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))

		End With

		With oCommandSelect
			.CommandText = "[sp_cambio_cheques_sel]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.Datetime, 8, "fecha"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))

		End With

		With oCommandGridSelect
            .CommandText = "[sp_cambio_cheques_grs]"
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
            .Parameters.Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))

		End With

	End Sub

    Public Function sp_cambio_cheques_ins(ByVal sucursal_actual As Long, ByVal caja_actual As Long, ByRef oData As DataSet) As Events
        Dim oEvent As New Events
        Try
            With oCommandInsert.Parameters
                .Item("@sucursal").Value = sucursal_actual
                .Item("@caja").Value = caja_actual
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Item("@partida").Value = 1
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Item("@numero_cheque").Value = Connection.GetValue(oData, "numero_cheque")
                .Item("@monto").Value = Connection.GetValue(oData, "monto")
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommandInsert)
            Connection.SetValue(oData, "sucursal", oCommandInsert.Parameters.Item("@sucursal").Value)
            Connection.SetValue(oData, "caja", oCommandInsert.Parameters.Item("@caja").Value)
            Connection.SetValue(oData, "fecha", oCommandInsert.Parameters.Item("@fecha").Value)
            Connection.SetValue(oData, "partida", oCommandInsert.Parameters.Item("@partida").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandInsert.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_cambio_cheques_upd(ByRef oData As DataSet) As Events
        Dim oEvent As New Events
        Try
            With oCommandUpdate.Parameters
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Item("@caja").Value = Connection.GetValue(oData, "caja")
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Item("@numero_cheque").Value = Connection.GetValue(oData, "numero_cheque")
                .Item("@monto").Value = Connection.GetValue(oData, "monto")
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommandUpdate)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandUpdate.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_cambio_cheques_del(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As Date, ByVal partida As Long) As Events
        Dim oEvent As New Events
        Try
            With oCommandDelete.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@caja").Value = caja
                .Item("@fecha").Value = fecha
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommandDelete)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandDelete.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_cambio_cheques_exs(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As Date, ByVal partida As Long) As Events
        Dim oEvent As New Events
        Try
            With oCommandExists.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@caja").Value = caja
                .Item("@fecha").Value = fecha
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommandExists)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandExists.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_cambio_cheques_sel(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As Date, ByVal partida As Long) As Events
        Dim oEvent As New Events
        Try
            oCommandSelect.CommandText = "[sp_cambio_cheques_sel]"
            With oCommandSelect.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@caja").Value = caja
                .Item("@fecha").Value = fecha
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommandSelect)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandSelect.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

    Public Function sp_cambio_cheques_grs(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As DateTime) As Events
        Dim oEvent As New Events
        Try
            With oCommandGridSelect.Parameters
                .Item("@sucursal").Value = sucursal
                .Item("@caja").Value = caja
                .Item("@fecha").Value = fecha
            End With
            oEvent.Value = Connection.GetDataSet(oCommandGridSelect)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandGridSelect.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function



End Class
#End Region


