'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEnviosConveniosDetalle
'DATE:		06/11/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - EnviosConveniosDetalle"
Public Class clsEnviosConveniosDetalle
    Public Function sp_envios_convenios_detalle_ins(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByRef anio_envio As Long, ByRef cliente As Long, ByRef partida As Long, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal serie_referencia As String, ByVal folio_referencia As Long, ByVal cliente_referencia As Long, ByVal documento_referencia As Long, ByVal importe As Long, ByVal afectado As Boolean, ByVal cancelado As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Item("@sucursal").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Item("@convenio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = quincena_envio
                .Item("@quincena_envio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
                .Item("@anio_envio").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Item("@cliente").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Item("@partida").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_referencia", System.Data.SqlDbType.Int, 4, "sucursal_referencia"))
                .Item("@sucursal_referencia").Value = sucursal_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = concepto_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@serie_referencia", System.Data.SqlDbType.Char, 3, "serie_referencia"))
                .Item("@serie_referencia").Value = serie_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_referencia", System.Data.SqlDbType.Int, 4, "cliente_referencia"))
                .Item("@cliente_referencia").Value = cliente_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@documento_referencia", System.Data.SqlDbType.Int, 4, "documento_referencia"))
                .Item("@documento_referencia").Value = documento_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = importe
                .Add(New System.Data.SqlClient.SqlParameter("@afectado", System.Data.SqlDbType.Bit, 1, "afectado"))
                .Item("@afectado").Value = afectado
                .Add(New System.Data.SqlClient.SqlParameter("@cancelado", System.Data.SqlDbType.Bit, 1, "cancelado"))
                .Item("@cancelado").Value = cancelado
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_detalle_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = Connection.GetValue(oData, "convenio")
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = Connection.GetValue(oData, "quincena_envio")
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = Connection.GetValue(oData, "anio_envio")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = Connection.GetValue(oData, "partida")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_referencia", System.Data.SqlDbType.Int, 4, "sucursal_referencia"))
                .Item("@sucursal_referencia").Value = Connection.GetValue(oData, "sucursal_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = Connection.GetValue(oData, "concepto_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_referencia", System.Data.SqlDbType.Char, 3, "serie_referencia"))
                .Item("@serie_referencia").Value = Connection.GetValue(oData, "serie_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = Connection.GetValue(oData, "folio_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_referencia", System.Data.SqlDbType.Int, 4, "cliente_referencia"))
                .Item("@cliente_referencia").Value = Connection.GetValue(oData, "cliente_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@documento_referencia", System.Data.SqlDbType.Int, 4, "documento_referencia"))
                .Item("@documento_referencia").Value = Connection.GetValue(oData, "documento_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@importe", System.Data.SqlDbType.Money, 8, "importe"))
                .Item("@importe").Value = Connection.GetValue(oData, "importe")
                .Add(New System.Data.SqlClient.SqlParameter("@afectado", System.Data.SqlDbType.Bit, 1, "afectado"))
                .Item("@afectado").Value = Connection.GetValue(oData, "afectado")
                .Add(New System.Data.SqlClient.SqlParameter("@cancelado", System.Data.SqlDbType.Bit, 1, "cancelado"))
                .Item("@cancelado").Value = Connection.GetValue(oData, "cancelado")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_detalle_del(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = quincena_envio
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_detalle_exs(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_detalle_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = quincena_envio
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_detalle_sel(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long, ByVal partida As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = quincena_envio
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = anio_envio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_envios_convenios_detalle_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_envios_convenios_detalle_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


