'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVariables
'DATE:		09/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Variables"
Public Class clsVariables
    Public Function sp_variables_upd(ByRef oData As DataSet, ByVal InicioComisionesCobrador As DateTime, ByVal FinComisionesCobrador As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_variables_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@empresa", System.Data.SqlDbType.VarChar, 100, "empresa"))
                .Item("@empresa").Value = Connection.GetValue(oData, "empresa")
                .Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 100, "direccion"))
                .Item("@direccion").Value = Connection.GetValue(oData, "direccion")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos", System.Data.SqlDbType.VarChar, 30, "telefonos"))
                .Item("@telefonos").Value = Connection.GetValue(oData, "telefonos")
                .Add(New System.Data.SqlClient.SqlParameter("@logotipo", System.Data.SqlDbType.Image, 0, "logotipo"))
                .Item("@logotipo").Value = Connection.GetValue(oData, "logotipo")
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Float, 8, "impuesto"))
                .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_compra", System.Data.SqlDbType.VarChar, 5, "concepto_compra"))
                .Item("@concepto_compra").Value = Connection.GetValue(oData, "concepto_compra")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cancelacion_compra", System.Data.SqlDbType.VarChar, 5, "concepto_cancelacion_compra"))
                .Item("@concepto_cancelacion_compra").Value = Connection.GetValue(oData, "concepto_cancelacion_compra")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_venta", System.Data.SqlDbType.VarChar, 5, "concepto_venta"))
                .Item("@concepto_venta").Value = Connection.GetValue(oData, "concepto_venta")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cancelacion_venta", System.Data.SqlDbType.VarChar, 5, "concepto_cancelacion_venta"))
                .Item("@concepto_cancelacion_venta").Value = Connection.GetValue(oData, "concepto_cancelacion_venta")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_devoluciones", System.Data.SqlDbType.VarChar, 5, "concepto_devoluciones"))
                .Item("@concepto_devoluciones").Value = Connection.GetValue(oData, "concepto_devoluciones")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cancelacion_devoluciones", System.Data.SqlDbType.VarChar, 5, "concepto_cancelacion_devoluciones"))
                .Item("@concepto_cancelacion_devoluciones").Value = Connection.GetValue(oData, "concepto_cancelacion_devoluciones")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cancelacion_traspaso", System.Data.SqlDbType.VarChar, 5, "concepto_cancelacion_traspaso"))
                .Item("@concepto_cancelacion_traspaso").Value = Connection.GetValue(oData, "concepto_cancelacion_traspaso")
                .Add(New System.Data.SqlClient.SqlParameter("@precio_expo", System.Data.SqlDbType.Int, 4, "precio_expo"))
                .Item("@precio_expo").Value = Connection.GetValue(oData, "precio_expo")
                .Add(New System.Data.SqlClient.SqlParameter("@precio_contado", System.Data.SqlDbType.Int, 4, "precio_contado"))
                .Item("@precio_contado").Value = Connection.GetValue(oData, "precio_contado")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxc_factura_contado", System.Data.SqlDbType.VarChar, 5, "concepto_cxc_factura_contado"))
                .Item("@concepto_cxc_factura_contado").Value = Connection.GetValue(oData, "concepto_cxc_factura_contado")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxc_factura_credito", System.Data.SqlDbType.VarChar, 5, "concepto_cxc_factura_credito"))
                .Item("@concepto_cxc_factura_credito").Value = Connection.GetValue(oData, "concepto_cxc_factura_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxc_nota_cargo", System.Data.SqlDbType.VarChar, 5, "concepto_cxc_nota_cargo"))
                .Item("@concepto_cxc_nota_cargo").Value = Connection.GetValue(oData, "concepto_cxc_nota_cargo")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxc_nota_credito", System.Data.SqlDbType.VarChar, 5, "concepto_cxc_nota_credito"))
                .Item("@concepto_cxc_nota_credito").Value = Connection.GetValue(oData, "concepto_cxc_nota_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxc_abono", System.Data.SqlDbType.VarChar, 5, "concepto_cxc_abono"))
                .Item("@concepto_cxc_abono").Value = Connection.GetValue(oData, "concepto_cxc_abono")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_intereses", System.Data.SqlDbType.VarChar, 5, "concepto_intereses"))
                .Item("@concepto_intereses").Value = Connection.GetValue(oData, "concepto_intereses")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxc_descuentos_anticipados", System.Data.SqlDbType.VarChar, 5, "concepto_cxc_descuentos_anticipados"))
                .Item("@concepto_cxc_descuentos_anticipados").Value = Connection.GetValue(oData, "concepto_cxc_descuentos_anticipados")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxc_intereses_especiales", System.Data.SqlDbType.VarChar, 5, "concepto_cxc_intereses_especiales"))
                .Item("@concepto_cxc_intereses_especiales").Value = Connection.GetValue(oData, "concepto_cxc_intereses_especiales")

                .Add(New System.Data.SqlClient.SqlParameter("@dias_gracia", System.Data.SqlDbType.Int, 4, "dias_gracia"))
                .Item("@dias_gracia").Value = Connection.GetValue(oData, "dias_gracia")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_intereses_moratorios", System.Data.SqlDbType.Float, 8, "porcentaje_intereses_moratorios"))
                .Item("@porcentaje_intereses_moratorios").Value = Connection.GetValue(oData, "porcentaje_intereses_moratorios")
                .Add(New System.Data.SqlClient.SqlParameter("@cobrador_casa", System.Data.SqlDbType.Int, 4, "cobrador_casa"))
                .Item("@cobrador_casa").Value = Connection.GetValue(oData, "cobrador_casa")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_cierre", System.Data.SqlDbType.DateTime, 8, "fecha_cierre"))
                .Item("@fecha_cierre").Value = Connection.GetValue(oData, "fecha_cierre")
                .Add(New System.Data.SqlClient.SqlParameter("@dias_armado", System.Data.SqlDbType.Int, 4, "dias_armado"))
                .Item("@dias_armado").Value = Connection.GetValue(oData, "dias_armado")
                .Add(New System.Data.SqlClient.SqlParameter("@dias_instalacion", System.Data.SqlDbType.Int, 4, "dias_instalacion"))
                .Item("@dias_instalacion").Value = Connection.GetValue(oData, "dias_instalacion")
                .Add(New System.Data.SqlClient.SqlParameter("@factor_limite_credito", System.Data.SqlDbType.Int, 4, "factor_limite_credito"))
                .Item("@factor_limite_credito").Value = Connection.GetValue(oData, "factor_limite_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxp_nota_entrada", System.Data.SqlDbType.VarChar, 5, "concepto_cxp_nota_entrada"))
                .Item("@concepto_cxp_nota_entrada").Value = Connection.GetValue(oData, "concepto_cxp_nota_entrada")
                .Add(New System.Data.SqlClient.SqlParameter("@dias_vigencia_cotizacion", System.Data.SqlDbType.Int, 4, "dias_vigencia_cotizacion"))
                .Item("@dias_vigencia_cotizacion").Value = Connection.GetValue(oData, "dias_vigencia_cotizacion")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxp_pago_varias_facturas", System.Data.SqlDbType.VarChar, 5, "concepto_cxp_pago_varias_facturas"))
                .Item("@concepto_cxp_pago_varias_facturas").Value = Connection.GetValue(oData, "concepto_cxp_pago_varias_facturas")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxp_nota_credito", System.Data.SqlDbType.VarChar, 5, "concepto_cxp_nota_credito"))
                .Item("@concepto_cxp_nota_credito").Value = Connection.GetValue(oData, "concepto_cxp_nota_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@meses_sin_abonar_para_juridico", System.Data.SqlDbType.Int, 4, "meses_sin_abonar_para_juridico"))
                .Item("@meses_sin_abonar_para_juridico").Value = Connection.GetValue(oData, "meses_sin_abonar_para_juridico")

                .Add(New System.Data.SqlClient.SqlParameter("@hora_inicio_comisiones_cobrador", System.Data.SqlDbType.DateTime, 8, "hora_inicio_comisiones_cobrador"))
                .Item("@hora_inicio_comisiones_cobrador").Value = InicioComisionesCobrador 'Connection.GetValue(oData, "hora_inicio_comisiones_cobrador")
                .Add(New System.Data.SqlClient.SqlParameter("@hora_fin_comisiones_cobrador", System.Data.SqlDbType.DateTime, 8, "hora_fin_comisiones_cobrador"))
                .Item("@hora_fin_comisiones_cobrador").Value = FinComisionesCobrador 'Connection.GetValue(oData, "hora_fin_comisiones_cobrador")

                .Add(New System.Data.SqlClient.SqlParameter("@enganche_en_menos", System.Data.SqlDbType.Float, 8, "enganche_en_menos"))
                .Item("@enganche_en_menos").Value = Connection.GetValue(oData, "enganche_en_menos")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxp_abono_comision", System.Data.SqlDbType.VarChar, 5, "concepto_cxp_abono_comision"))
                .Item("@concepto_cxp_abono_comision").Value = Connection.GetValue(oData, "concepto_cxp_abono_comision")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_cxp_cargo_por_flete", System.Data.SqlDbType.VarChar, 5, "concepto_cxp_cargo_por_flete"))
                .Item("@concepto_cxp_cargo_por_flete").Value = Connection.GetValue(oData, "concepto_cxp_cargo_por_flete")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_movimiento_chequera", System.Data.SqlDbType.Int, 4, "concepto_movimiento_chequera"))
                .Item("@concepto_movimiento_chequera").Value = Connection.GetValue(oData, "concepto_movimiento_chequera")

                .Add(New System.Data.SqlClient.SqlParameter("@encabezado_cotizacion", System.Data.SqlDbType.Text, 0, "encabezado_cotizacion"))
                .Item("@encabezado_cotizacion").Value = Connection.GetValue(oData, "encabezado_cotizacion")
                .Add(New System.Data.SqlClient.SqlParameter("@pie_cotizacion", System.Data.SqlDbType.Text, 0, "pie_cotizacion"))
                .Item("@pie_cotizacion").Value = Connection.GetValue(oData, "pie_cotizacion")
                .Add(New System.Data.SqlClient.SqlParameter("@pago_contado_caja", System.Data.SqlDbType.Int, 4, "pago_contado_caja"))
                .Item("@pago_contado_caja").Value = Connection.GetValue(oData, "pago_contado_caja")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_vistas_salida", System.Data.SqlDbType.VarChar, 5, "concepto_vistas_salida"))
                .Item("@concepto_vistas_salida").Value = Connection.GetValue(oData, "concepto_vistas_salida")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_vistas_entrada", System.Data.SqlDbType.VarChar, 5, "concepto_vistas_entrada"))
                .Item("@concepto_vistas_entrada").Value = Connection.GetValue(oData, "concepto_vistas_entrada")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_movimiento_chequera_cxp", System.Data.SqlDbType.Int, 4, "concepto_movimiento_chequera_cxp"))
                .Item("@concepto_movimiento_chequera_cxp").Value = Connection.GetValue(oData, "concepto_movimiento_chequera_cxp")


                .Add(New System.Data.SqlClient.SqlParameter("@dias_gracia_nota_cargo", System.Data.SqlDbType.Int, 4, "dias_gracia_nota_cargo"))
                .Item("@dias_gracia_nota_cargo").Value = Connection.GetValue(oData, "dias_gracia_nota_cargo")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_intereses", System.Data.SqlDbType.Char, 1, "tipo_intereses"))
                .Item("@tipo_intereses").Value = Connection.GetValue(oData, "tipo_intereses")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_salida_reparacion", System.Data.SqlDbType.VarChar, 5, "concepto_salida_reparacion"))
                .Item("@concepto_salida_reparacion").Value = Connection.GetValue(oData, "concepto_salida_reparacion")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_entrada_reparacion", System.Data.SqlDbType.VarChar, 5, "concepto_entrada_reparacion"))
                .Item("@concepto_entrada_reparacion").Value = Connection.GetValue(oData, "concepto_entrada_reparacion")


                .Add(New System.Data.SqlClient.SqlParameter("@concepto_gastos_administrativos", System.Data.SqlDbType.VarChar, 5, "concepto_gastos_administrativos"))
                .Item("@concepto_gastos_administrativos").Value = Connection.GetValue(oData, "concepto_gastos_administrativos")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_vales_vencidos", System.Data.SqlDbType.VarChar, 5, "concepto_vales_vencidos"))
                .Item("@concepto_vales_vencidos").Value = Connection.GetValue(oData, "concepto_vales_vencidos")
                .Add(New System.Data.SqlClient.SqlParameter("@importe_gastos_administrativos", System.Data.SqlDbType.Money, 8, "importe_gastos_administrativos"))
                .Item("@importe_gastos_administrativos").Value = Connection.GetValue(oData, "importe_gastos_administrativos")
                .Add(New System.Data.SqlClient.SqlParameter("@pago_vales_caja", System.Data.SqlDbType.Int, 4, "pago_vales_caja"))
                .Item("@pago_vales_caja").Value = Connection.GetValue(oData, "pago_vales_caja")


                .Add(New System.Data.SqlClient.SqlParameter("@articulo_anticipos", System.Data.SqlDbType.Int, 4, "articulo_anticipos"))
                .Item("@articulo_anticipos").Value = Connection.GetValue(oData, "articulo_anticipos")

                .Add(New System.Data.SqlClient.SqlParameter("@precio_lista_convenio", System.Data.SqlDbType.Int, 4, "precio_lista_convenio"))
                .Item("@precio_lista_convenio").Value = Connection.GetValue(oData, "precio_lista_convenio")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_variables_sel() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_variables_sel]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_variables_lkp(ByVal datafield As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_variables_lkp]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@datafield", System.Data.SqlDbType.VarChar, 50, "datafield"))
                .Item("@datafield").Value = datafield
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_reportes_ventas_etiquetas_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_reportes_ventas_etiquetas_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_reportes_ventas_etiquetas_plantillas_sin_asignar() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_reportes_ventas_etiquetas_plantillas_sin_asignar]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_variables_porcentaje_comision_contados_upd(ByVal porcentaje_comision_contados As Decimal)
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_variables_porcentaje_comision_contados_upd]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comision_contados", System.Data.SqlDbType.Decimal, 9, "porcentaje_comision_contados"))
                .Item("@porcentaje_comision_contados").Value = porcentaje_comision_contados
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


