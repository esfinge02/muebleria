Imports Dipros.Utils

Public Class clsEsquemasComisiones
    Public Function sp_esquemas_comisiones_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_esquemas_comisiones_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@esquema", System.Data.SqlDbType.Int, 4, "esquema"))
                .Item("@esquema").Value = Connection.GetValue(oData, "esquema")
                .Item("@esquema").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 150, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comisiones_contados", System.Data.SqlDbType.Decimal, 9, "porcentaje_comisiones_contados"))
                .Item("@porcentaje_comisiones_contados").Value = Connection.GetValue(oData, "porcentaje_comisiones_contados")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comisiones_creditos", System.Data.SqlDbType.Decimal, 9, "porcentaje_comisiones_creditos"))
                .Item("@porcentaje_comisiones_creditos").Value = Connection.GetValue(oData, "porcentaje_comisiones_creditos")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comisiones_fonacot", System.Data.SqlDbType.Decimal, 9, "porcentaje_comisiones_fonacot"))
                .Item("@porcentaje_comisiones_fonacot").Value = Connection.GetValue(oData, "porcentaje_comisiones_fonacot")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comisiones_general", System.Data.SqlDbType.Decimal, 9, "porcentaje_comisiones_general"))
                .Item("@porcentaje_comisiones_general").Value = Connection.GetValue(oData, "porcentaje_comisiones_general")
                .Add(New System.Data.SqlClient.SqlParameter("@restar_sueldo", System.Data.SqlDbType.Bit, 1, "restar_sueldo"))
                .Item("@restar_sueldo").Value = Connection.GetValue(oData, "restar_sueldo")
                .Add(New System.Data.SqlClient.SqlParameter("@usar_tabla_rangos_credito", System.Data.SqlDbType.Bit, 1, "usar_tabla_rangos_credito"))
                .Item("@usar_tabla_rangos_credito").Value = Connection.GetValue(oData, "usar_tabla_rangos_credito")



                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "esquema", oCommand.Parameters.Item("@esquema").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_esquemas_comisiones_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_esquemas_comisiones_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@esquema", System.Data.SqlDbType.Int, 4, "esquema"))
                .Item("@esquema").Value = Connection.GetValue(oData, "esquema")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 150, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comisiones_contados", System.Data.SqlDbType.Decimal, 9, "porcentaje_comisiones_contados"))
                .Item("@porcentaje_comisiones_contados").Value = Connection.GetValue(oData, "porcentaje_comisiones_contados")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comisiones_creditos", System.Data.SqlDbType.Decimal, 9, "porcentaje_comisiones_creditos"))
                .Item("@porcentaje_comisiones_creditos").Value = Connection.GetValue(oData, "porcentaje_comisiones_creditos")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comisiones_fonacot", System.Data.SqlDbType.Decimal, 9, "porcentaje_comisiones_fonacot"))
                .Item("@porcentaje_comisiones_fonacot").Value = Connection.GetValue(oData, "porcentaje_comisiones_fonacot")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comisiones_general", System.Data.SqlDbType.Decimal, 9, "porcentaje_comisiones_general"))
                .Item("@porcentaje_comisiones_general").Value = Connection.GetValue(oData, "porcentaje_comisiones_general")
                .Add(New System.Data.SqlClient.SqlParameter("@restar_sueldo", System.Data.SqlDbType.Bit, 1, "restar_sueldo"))
                .Item("@restar_sueldo").Value = Connection.GetValue(oData, "restar_sueldo")
                .Add(New System.Data.SqlClient.SqlParameter("@usar_tabla_rangos_credito", System.Data.SqlDbType.Bit, 1, "usar_tabla_rangos_credito"))
                .Item("@usar_tabla_rangos_credito").Value = Connection.GetValue(oData, "usar_tabla_rangos_credito")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_esquemas_comisiones_del(ByVal esquema As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_esquemas_comisiones_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@esquema", System.Data.SqlDbType.Int, 4, "esquema"))
                .Item("@esquema").Value = esquema

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_esquemas_comisiones_sel(ByVal esquema As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_esquemas_comisiones_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@esquema", System.Data.SqlDbType.Int, 4, "esquema"))
                .Item("@esquema").Value = esquema

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_esquemas_comisiones_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_esquemas_comisiones_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_esquemas_comisiones_grl(ByVal esquema As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_esquemas_comisiones_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@esquema", System.Data.SqlDbType.Int, 4, "esquema"))
                .Item("@esquema").Value = esquema
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
