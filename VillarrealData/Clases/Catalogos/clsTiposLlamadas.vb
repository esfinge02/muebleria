'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsTiposLlamadas
'DATE:		17/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - TiposLlamadas"
Public Class clsTiposLlamadas
    Public Function sp_tipos_llamadas_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tipos_llamadas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = Connection.GetValue(oData, "tipo_llamada")
                .Item("@tipo_llamada").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 40, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "tipo_llamada", oCommand.Parameters.Item("@tipo_llamada").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_llamadas_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tipos_llamadas_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = Connection.GetValue(oData, "tipo_llamada")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 40, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_llamadas_del(ByVal tipo_llamada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tipos_llamadas_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = tipo_llamada

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_llamadas_exs(ByVal tipo_llamada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tipos_llamadas_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = tipo_llamada

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_llamadas_sel(ByVal tipo_llamada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tipos_llamadas_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = tipo_llamada

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_llamadas_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tipos_llamadas_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_llamadas_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tipos_llamadas_grl]"
            'With oCommand.Parameters
            '    .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Int, 4, "sucursal_dependencia"))
            '    .Item("@sucursal_dependencia").Value = sucursal_dependencia
            'End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


