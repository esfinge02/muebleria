'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsConceptosMovimientosChequera
'DATE:		26/06/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - ConceptosMovimientosChequera"
Public Class clsConceptosMovimientosChequera
    Private oCommandInsert As New System.Data.SqlClient.SqlCommand
	Private oCommandUpdate As New System.Data.SqlClient.SqlCommand
	Private oCommandDelete As New System.Data.SqlClient.SqlCommand
	Private oCommandExists As New System.Data.SqlClient.SqlCommand
	Private oCommandSelect As New System.Data.SqlClient.SqlCommand
    Private oCommandGridSelect As New System.Data.SqlClient.SqlCommand
    Private oCommandLookup As New System.Data.SqlClient.SqlCommand

	Public Sub New()

		With oCommandInsert
			.CommandText = "[sp_conceptos_movimientos_chequera_ins]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Int, 4, "concepto"))
			.Parameters.Item("@concepto").Direction = ParameterDirection.InputOutput
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 40, "descripcion"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))

		End With

		With oCommandUpdate
			.CommandText = "[sp_conceptos_movimientos_chequera_upd]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Int, 4, "concepto"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 40, "descripcion"))
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))

		End With

		With oCommandDelete
			.CommandText = "[sp_conceptos_movimientos_chequera_del]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Int, 4, "concepto"))

		End With

		With oCommandExists
			.CommandText = "[sp_conceptos_movimientos_chequera_exs]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Int, 4, "concepto"))

		End With

		With oCommandSelect
			.CommandText = "[sp_conceptos_movimientos_chequera_sel]"
			.Parameters.Add(New System.Data.SqlClient.SqlParameter("@concepto", System.Data.SqlDbType.Int, 4, "concepto"))

		End With

		With oCommandGridSelect
			.CommandText = "[sp_conceptos_movimientos_chequera_grs]"

		End With

        With oCommandLookup
            .CommandText = "[sp_conceptos_movimientos_chequera_grl]"

        End With

	End Sub

	Public Function sp_conceptos_movimientos_chequera_ins(ByRef oData As DataSet) As Events
		Dim oEvent As New Events
		Try
			With oCommandInsert.Parameters
				.Item("@concepto").Value = Connection.GetValue(oData, "concepto")
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommandInsert)
			Connection.SetValue(oData, "concepto", oCommandInsert.Parameters.Item("@concepto").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandInsert.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		End Try

		Return oEvent
	End Function

	Public Function sp_conceptos_movimientos_chequera_upd(ByRef oData As DataSet) As Events
		Dim oEvent As New Events
		Try
			With oCommandUpdate.Parameters
				.Item("@concepto").Value = Connection.GetValue(oData, "concepto")
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommandUpdate)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandUpdate.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_conceptos_movimientos_chequera_del(ByVal concepto as long) As Events
		Dim oEvent As New Events
		Try
			With oCommandDelete.Parameters
				.Item("@concepto").Value = concepto

			End With
			Connection.Execute(oCommandDelete)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandDelete.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		End Try
	
		Return oEvent
	End Function

	Public Function sp_conceptos_movimientos_chequera_exs(ByVal concepto as long) As Events
		Dim oEvent As New Events
		Try
			With oCommandExists.Parameters
				.Item("@concepto").Value = concepto

			End With
			oEvent.Value = Connection.Execute(oCommandExists)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandExists.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		End Try
		
		Return oEvent
	End Function

	Public Function sp_conceptos_movimientos_chequera_sel(ByVal concepto as long) As Events
		Dim oEvent As New Events
		Try
			oCommandSelect.CommandText = "[sp_conceptos_movimientos_chequera_sel]"
			With oCommandSelect.Parameters
				.Item("@concepto").Value = concepto

			End With
			oEvent.Value = Connection.GetDataSet(oCommandSelect)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandSelect.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		End Try

		Return oEvent
	End Function

	Public Function sp_conceptos_movimientos_chequera_grs() As Events
		Dim oEvent As New Events
		Try
			With oCommandGridSelect.Parameters

			End With
			oEvent.Value = Connection.GetDataSet(oCommandGridSelect)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandGridSelect.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		End Try
		
		Return oEvent
	End Function

    Public Function sp_conceptos_movimientos_chequera_grl() As Events
        Dim oEvent As New Events
        Try
            With oCommandLookup.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommandLookup)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommandLookup.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        End Try

        Return oEvent
    End Function

End Class
#End Region


