'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsChequeras
'DATE:		19/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Chequeras"
Public Class clsChequeras
    Public Function sp_chequeras_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Smallint, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Item("@banco").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = Connection.GetValue(oData, "cuenta_bancaria")
                .Item("@cuenta_bancaria").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = Connection.GetValue(oData, "saldo")
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Add(New System.Data.SqlClient.SqlParameter("@predeterminada", System.Data.SqlDbType.Bit, 1, "predeterminada"))
                .Item("@predeterminada").Value = Connection.GetValue(oData, "predeterminada")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_contable", System.Data.SqlDbType.VarChar, 40, "cuenta_contable"))
                .Item("@cuenta_contable").Value = Connection.GetValue(oData, "cuenta_contable")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_cuenta", System.Data.SqlDbType.VarChar, 100, "descripcion_cuenta"))
                .Item("@descripcion_cuenta").Value = Connection.GetValue(oData, "descripcion_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

            Connection.SetValue(oData, "cuenta_bancaria", oCommand.Parameters.Item("@cuenta_bancaria").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_chequeras_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Smallint, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = Connection.GetValue(oData, "cuenta_bancaria")
                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = Connection.GetValue(oData, "saldo")
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = Connection.GetValue(oData, "folio")
                .Add(New System.Data.SqlClient.SqlParameter("@predeterminada", System.Data.SqlDbType.Bit, 1, "predeterminada"))
                .Item("@predeterminada").Value = Connection.GetValue(oData, "predeterminada")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_contable", System.Data.SqlDbType.VarChar, 40, "cuenta_contable"))
                .Item("@cuenta_contable").Value = Connection.GetValue(oData, "cuenta_contable")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_cuenta", System.Data.SqlDbType.VarChar, 100, "descripcion_cuenta"))
                .Item("@descripcion_cuenta").Value = Connection.GetValue(oData, "descripcion_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_chequeras_del(ByVal cuenta_bancaria As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_del]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = cuenta_bancaria

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_chequeras_exs(ByVal cuenta_bancaria As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_exs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = cuenta_bancaria

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_chequeras_sel(ByVal cuenta_bancaria As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_sel]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cuenta_bancaria", System.Data.SqlDbType.VarChar, 20, "cuenta_bancaria"))
                .Item("@cuenta_bancaria").Value = cuenta_bancaria

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_chequeras_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_chequeras_grl(ByVal banco As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    '@ACH-25/06/07: Se cre� este m�todo para obtener las chequeras para varios bancos
    Public Function sp_chequeras_multi_grl(ByVal banco As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_multi_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Text, 0, "banco"))
                .Item("@banco").Value = banco
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    '/@ACH-25/06/07
    Public Function sp_chequeras_predeterminada() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_chequeras_predeterminada]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los par�metros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region




