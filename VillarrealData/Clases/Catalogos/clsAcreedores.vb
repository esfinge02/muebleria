'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAcreedores
'DATE:		16/10/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Acreedores"
Public Class clsAcreedores
    Public Function sp_acreedores_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = Connection.GetValue(oData, "acreedor")
                .Item("@acreedor").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos", System.Data.SqlDbType.VarChar, 50, "telefonos"))
                .Item("@telefonos").Value = Connection.GetValue(oData, "telefonos")
                .Add(New System.Data.SqlClient.SqlParameter("@baja", System.Data.SqlDbType.Bit, 1, "baja"))
                .Item("@baja").Value = Connection.GetValue(oData, "baja")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 40, "cuenta"))
                .Item("@cuenta").Value = Connection.GetValue(oData, "cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "acreedor", oCommand.Parameters.Item("@acreedor").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = Connection.GetValue(oData, "acreedor")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos", System.Data.SqlDbType.VarChar, 50, "telefonos"))
                .Item("@telefonos").Value = Connection.GetValue(oData, "telefonos")
                .Add(New System.Data.SqlClient.SqlParameter("@baja", System.Data.SqlDbType.Bit, 1, "baja"))
                .Item("@baja").Value = Connection.GetValue(oData, "baja")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 40, "cuenta"))
                .Item("@cuenta").Value = Connection.GetValue(oData, "cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_del(ByVal acreedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = acreedor

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_exs(ByVal acreedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = acreedor

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_sel(ByVal acreedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@acreedor", System.Data.SqlDbType.Int, 4, "acreedor"))
                .Item("@acreedor").Value = acreedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_acreedores_grl(Optional ByVal tipo As Long = -1) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_acreedores_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Int, 4, "tipo"))
                .Item("@tipo").Value = tipo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


