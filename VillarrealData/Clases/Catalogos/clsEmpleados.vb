'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEmpleados
'DATE:		04/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Empleados"
Public Class clsEmpleados
	Public Function sp_empleados_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_empleados_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@empleado", System.Data.SqlDbType.Int, 4, "empleado"))
				.Item("@empleado").Value = Connection.GetValue(oData, "empleado")
				.Item("@empleado").Direction = ParameterDirection.InputOutput
				.Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 60, "nombre"))
				.Item("@nombre").Value = Connection.GetValue(oData, "nombre")
				.Add(New System.Data.SqlClient.SqlParameter("@baja", System.Data.SqlDbType.Bit, 1, "baja"))
				.Item("@baja").Value = Connection.GetValue(oData, "baja")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)
			Connection.SetValue(oData, "empleado", oCommand.Parameters.Item("@empleado").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_empleados_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_empleados_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@empleado", System.Data.SqlDbType.Int, 4, "empleado"))
				.Item("@empleado").Value = Connection.GetValue(oData, "empleado")
				.Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 60, "nombre"))
				.Item("@nombre").Value = Connection.GetValue(oData, "nombre")
				.Add(New System.Data.SqlClient.SqlParameter("@baja", System.Data.SqlDbType.Bit, 1, "baja"))
				.Item("@baja").Value = Connection.GetValue(oData, "baja")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_empleados_del(ByVal empleado as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_empleados_del]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@empleado", System.Data.SqlDbType.Int, 4, "empleado"))
				.Item("@empleado").Value = empleado

			End With
			Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
	
		Return oEvent
	End Function

	Public Function sp_empleados_exs(ByVal empleado as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_empleados_exs]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@empleado", System.Data.SqlDbType.Int, 4, "empleado"))
				.Item("@empleado").Value = empleado

			End With
			oEvent.Value = Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

	Public Function sp_empleados_sel(ByVal empleado as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_empleados_sel]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@empleado", System.Data.SqlDbType.Int, 4, "empleado"))
				.Item("@empleado").Value = empleado

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_empleados_grs() As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_empleados_grs]"
			With oCommand.Parameters

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
        Return oEvent
	End Function

    Public Function sp_empleados_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_empleados_grl]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function

End Class
#End Region


