'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsProveedoresCuentasBancarias
'DATE:		19/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - ProveedoresCuentasBancarias"
Public Class clsProveedoresCuentasBancarias
    Public Function sp_proveedores_cuentas_bancarias_ins(ByRef oData As DataSet, ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_cuentas_bancarias_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Item("@banco").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.VarChar, 20, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@plaza", System.Data.SqlDbType.VarChar, 5, "plaza"))
                .Item("@plaza").Value = Connection.GetValue(oData, "plaza")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 20, "cuenta"))
                .Item("@cuenta").Value = Connection.GetValue(oData, "cuenta")
                .Item("@cuenta").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "banco", oCommand.Parameters.Item("@banco").Value)
            Connection.SetValue(oData, "cuenta", oCommand.Parameters.Item("@cuenta").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_cuentas_bancarias_upd(ByRef oData As DataSet, ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_cuentas_bancarias_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.VarChar, 20, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@plaza", System.Data.SqlDbType.VarChar, 5, "plaza"))
                .Item("@plaza").Value = Connection.GetValue(oData, "plaza")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 20, "cuenta"))
                .Item("@cuenta").Value = Connection.GetValue(oData, "cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_cuentas_bancarias_del(ByVal proveedor As Long, ByVal banco As Long, ByVal cuenta As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_cuentas_bancarias_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 20, "cuenta"))
                .Item("@cuenta").Value = cuenta

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_cuentas_bancarias_exs(ByVal proveedor As Long, ByVal banco As Long, ByVal cuenta As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_cuentas_bancarias_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 20, "cuenta"))
                .Item("@cuenta").Value = cuenta

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_cuentas_bancarias_sel(ByVal proveedor As Long, ByVal banco As Long, ByVal cuenta As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_cuentas_bancarias_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 20, "cuenta"))
                .Item("@cuenta").Value = cuenta

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_cuentas_bancarias_grs(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_cuentas_bancarias_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


