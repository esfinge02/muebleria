Imports Dipros.Utils

Public Class clsVendedoresEsquemas
    Public Function sp_vendedores_esquemas_ins(ByVal vendedor As Long, ByVal esquema As Long, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_esquemas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor

                .Add(New System.Data.SqlClient.SqlParameter("@esquema", System.Data.SqlDbType.Int, 4, "esquema"))
                .Item("@esquema").Value = esquema
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vendedores_esquemas_del(ByVal vendedor As Long, ByVal esquema As Long, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_esquemas_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor
                .Add(New System.Data.SqlClient.SqlParameter("@esquema", System.Data.SqlDbType.Int, 4, "esquema"))
                .Item("@esquema").Value = esquema
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_cajeros_sel(ByVal cajero As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_cajeros_sel]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
    '            .Item("@cajero").Value = cajero

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_vendedores_esquemas_grs(ByVal vendedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_esquemas_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
