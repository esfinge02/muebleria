'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPlanesCredito
'DATE:		03/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - PlanesCredito"
Public Class clsPlanesCredito
    Public Function sp_planes_credito_ins(ByRef oData As DataSet, ByVal tipo_venta As Char, ByVal tipo_plazo As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_planes_credito_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
                .Item("@plan_credito").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo", System.Data.SqlDbType.Decimal, 9, "plazo"))
                .Item("@plazo").Value = Connection.GetValue(oData, "plazo")
                .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Decimal, 9, "interes"))
                .Item("@interes").Value = Connection.GetValue(oData, "interes")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Char, 1, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_plazo", System.Data.SqlDbType.Char, 1, "tipo_plazo"))
                .Item("@tipo_plazo").Value = tipo_plazo
                .Add(New System.Data.SqlClient.SqlParameter("@precio_venta", System.Data.SqlDbType.Int, 4, "precio_venta"))
                .Item("@precio_venta").Value = Connection.GetValue(oData, "precio_venta")
                .Add(New System.Data.SqlClient.SqlParameter("@plan_dependencia", System.Data.SqlDbType.Bit, 1, "plan_dependencia"))
                .Item("@plan_dependencia").Value = Connection.GetValue(oData, "plan_dependencia")
                .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Decimal, 9, "enganche"))
                .Item("@enganche").Value = Connection.GetValue(oData, "enganche")
                .Add(New System.Data.SqlClient.SqlParameter("@vigente", System.Data.SqlDbType.Bit, 1, "vigente"))
                .Item("@vigente").Value = Connection.GetValue(oData, "vigente")
                .Add(New System.Data.SqlClient.SqlParameter("@fonacot", System.Data.SqlDbType.Bit, 1, "fonacot"))
                .Item("@fonacot").Value = Connection.GetValue(oData, "fonacot")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "plan_credito", oCommand.Parameters.Item("@plan_credito").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_planes_credito_upd(ByRef oData As DataSet, ByVal tipo_venta As Char, ByVal tipo_plazo As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_planes_credito_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@plazo", System.Data.SqlDbType.Decimal, 9, "plazo"))
                .Item("@plazo").Value = Connection.GetValue(oData, "plazo")
                .Add(New System.Data.SqlClient.SqlParameter("@interes", System.Data.SqlDbType.Decimal, 9, "interes"))
                .Item("@interes").Value = Connection.GetValue(oData, "interes")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_venta", System.Data.SqlDbType.Char, 1, "tipo_venta"))
                .Item("@tipo_venta").Value = tipo_venta
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_plazo", System.Data.SqlDbType.Char, 1, "tipo_plazo"))
                .Item("@tipo_plazo").Value = tipo_plazo
                .Add(New System.Data.SqlClient.SqlParameter("@precio_venta", System.Data.SqlDbType.Int, 4, "precio_venta"))
                .Item("@precio_venta").Value = Connection.GetValue(oData, "precio_venta")
                .Add(New System.Data.SqlClient.SqlParameter("@plan_dependencia", System.Data.SqlDbType.Bit, 1, "plan_dependencia"))
                .Item("@plan_dependencia").Value = Connection.GetValue(oData, "plan_dependencia")
                .Add(New System.Data.SqlClient.SqlParameter("@enganche", System.Data.SqlDbType.Decimal, 9, "enganche"))
                .Item("@enganche").Value = Connection.GetValue(oData, "enganche")
                .Add(New System.Data.SqlClient.SqlParameter("@vigente", System.Data.SqlDbType.Bit, 1, "vigente"))
                .Item("@vigente").Value = Connection.GetValue(oData, "vigente")
                .Add(New System.Data.SqlClient.SqlParameter("@fonacot", System.Data.SqlDbType.Bit, 1, "fonacot"))
                .Item("@fonacot").Value = Connection.GetValue(oData, "fonacot")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_planes_credito_del(ByVal plan_credito As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_planes_credito_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_planes_credito_exs(ByVal plan_credito As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_planes_credito_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_planes_credito_sel(ByVal plan_credito As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_planes_credito_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_planes_credito_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_planes_credito_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_planes_credito_grl(ByVal sucursal_dependencia As Boolean, ByVal cotizacion As Double, ByVal vigente As Long, ByVal plan_credito As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_planes_credito_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Bit, 1, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = sucursal_dependencia
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@vigente", System.Data.SqlDbType.Int, 4, "vigente"))
                .Item("@vigente").Value = vigente
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_planes_credito_descuentos_programados_grl(ByVal sucursal_dependencia As Boolean, ByVal cotizacion As Double, ByVal vigente As Long, ByVal plan_credito As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_planes_credito_descuentos_programados_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Bit, 1, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = sucursal_dependencia
                .Add(New System.Data.SqlClient.SqlParameter("@cotizacion", System.Data.SqlDbType.Int, 4, "cotizacion"))
                .Item("@cotizacion").Value = cotizacion
                .Add(New System.Data.SqlClient.SqlParameter("@vigente", System.Data.SqlDbType.Int, 4, "vigente"))
                .Item("@vigente").Value = vigente
                .Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
                .Item("@plan_credito").Value = plan_credito

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


