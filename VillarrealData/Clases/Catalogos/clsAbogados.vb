'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAbogados
'DATE:		16/01/2007 00:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Abogados"
Public Class clsAbogados
    Public Function sp_abogados_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_abogados_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Int, 4, "abogado"))
                .Item("@abogado").Value = Connection.GetValue(oData, "abogado")
                .Item("@abogado").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 100, "direccion"))
                .Item("@direccion").Value = Connection.GetValue(oData, "direccion")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@codigo_postal", System.Data.SqlDbType.Int, 4, "codigo_postal"))
                .Item("@codigo_postal").Value = Connection.GetValue(oData, "codigo_postal")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_casa", System.Data.SqlDbType.VarChar, 25, "telefono_casa"))
                .Item("@telefono_casa").Value = Connection.GetValue(oData, "telefono_casa")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_oficina", System.Data.SqlDbType.VarChar, 25, "telefono_oficina"))
                .Item("@telefono_oficina").Value = Connection.GetValue(oData, "telefono_oficina")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_celular", System.Data.SqlDbType.VarChar, 25, "telefono_celular"))
                .Item("@telefono_celular").Value = Connection.GetValue(oData, "telefono_celular")
                .Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.VarChar, 50, "email"))
                .Item("@email").Value = Connection.GetValue(oData, "email")
                .Add(New System.Data.SqlClient.SqlParameter("@comision", System.Data.SqlDbType.Float, 8, "comision"))
                .Item("@comision").Value = Connection.GetValue(oData, "comision")

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "abogado", oCommand.Parameters.Item("@abogado").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_abogados_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_abogados_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Int, 4, "abogado"))
                .Item("@abogado").Value = Connection.GetValue(oData, "abogado")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 100, "direccion"))
                .Item("@direccion").Value = Connection.GetValue(oData, "direccion")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@codigo_postal", System.Data.SqlDbType.Int, 4, "codigo_postal"))
                .Item("@codigo_postal").Value = Connection.GetValue(oData, "codigo_postal")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_casa", System.Data.SqlDbType.VarChar, 25, "telefono_casa"))
                .Item("@telefono_casa").Value = Connection.GetValue(oData, "telefono_casa")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_oficina", System.Data.SqlDbType.VarChar, 25, "telefono_oficina"))
                .Item("@telefono_oficina").Value = Connection.GetValue(oData, "telefono_oficina")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_celular", System.Data.SqlDbType.VarChar, 25, "telefono_celular"))
                .Item("@telefono_celular").Value = Connection.GetValue(oData, "telefono_celular")
                .Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.VarChar, 50, "email"))
                .Item("@email").Value = Connection.GetValue(oData, "email")
                .Add(New System.Data.SqlClient.SqlParameter("@comision", System.Data.SqlDbType.Float, 8, "comision"))
                .Item("@comision").Value = Connection.GetValue(oData, "comision")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_abogados_del(ByVal abogado As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_abogados_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Int, 4, "abogado"))
                .Item("@abogado").Value = abogado

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_abogados_exs(ByVal abogado As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_abogados_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Int, 4, "abogado"))
                .Item("@abogado").Value = abogado

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_abogados_sel(ByVal abogado As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_abogados_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@abogado", System.Data.SqlDbType.Int, 4, "abogado"))
                .Item("@abogado").Value = abogado

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_abogados_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_abogados_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_abogados_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_abogados_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


