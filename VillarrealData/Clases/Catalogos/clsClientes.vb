'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientes
'DATE:		03/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Clientes"
Public Class clsClientes

    Public Function sp_clientes_ins(ByRef oData As DataSet, ByVal rfc_completo As Boolean, ByVal tipo_captura As Char, ByVal fecha_nacimiento As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Item("@cliente").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@nombres", System.Data.SqlDbType.VarChar, 40, "nombres"))
                .Item("@nombres").Value = Connection.GetValue(oData, "nombres")
                .Add(New System.Data.SqlClient.SqlParameter("@paterno", System.Data.SqlDbType.VarChar, 30, "paterno"))
                .Item("@paterno").Value = Connection.GetValue(oData, "paterno")
                .Add(New System.Data.SqlClient.SqlParameter("@materno", System.Data.SqlDbType.VarChar, 30, "materno"))
                .Item("@materno").Value = Connection.GetValue(oData, "materno")

                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_nacimiento", System.Data.SqlDbType.DateTime, 8, "fecha_nacimiento"))
                .Item("@fecha_nacimiento").Value = fecha_nacimiento

                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@numero_exterior", System.Data.SqlDbType.VarChar, 10, "numero_exterior"))
                .Item("@numero_exterior").Value = Connection.GetValue(oData, "numero_exterior")
                .Add(New System.Data.SqlClient.SqlParameter("@numero_interior", System.Data.SqlDbType.VarChar, 10, "numero_interior"))
                .Item("@numero_interior").Value = Connection.GetValue(oData, "numero_interior")
                .Add(New System.Data.SqlClient.SqlParameter("@entrecalle", System.Data.SqlDbType.VarChar, 30, "entrecalle"))
                .Item("@entrecalle").Value = Connection.GetValue(oData, "entrecalle")
                .Add(New System.Data.SqlClient.SqlParameter("@ycalle", System.Data.SqlDbType.VarChar, 30, "ycalle"))
                .Item("@ycalle").Value = Connection.GetValue(oData, "ycalle")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono1", System.Data.SqlDbType.VarChar, 25, "telefono1"))
                .Item("@telefono1").Value = Connection.GetValue(oData, "telefono1")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono2", System.Data.SqlDbType.VarChar, 25, "telefono2"))
                .Item("@telefono2").Value = Connection.GetValue(oData, "telefono2")
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 13, "fax"))
                .Item("@fax").Value = Connection.GetValue(oData, "fax")
                .Add(New System.Data.SqlClient.SqlParameter("@ocupacion", System.Data.SqlDbType.VarChar, 50, "ocupacion"))
                .Item("@ocupacion").Value = Connection.GetValue(oData, "ocupacion")
                .Add(New System.Data.SqlClient.SqlParameter("@ingresos", System.Data.SqlDbType.Money, 8, "ingresos"))
                .Item("@ingresos").Value = Connection.GetValue(oData, "ingresos")

                .Add(New System.Data.SqlClient.SqlParameter("@puesto", System.Data.SqlDbType.VarChar, 80, "puesto"))
                .Item("@puesto").Value = Connection.GetValue(oData, "puesto")
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.VarChar, 80, "departamento"))
                .Item("@departamento").Value = Connection.GetValue(oData, "departamento")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos_laboral", System.Data.SqlDbType.VarChar, 25, "telefonos_laboral"))
                .Item("@telefonos_laboral").Value = Connection.GetValue(oData, "telefonos_laboral")
                .Add(New System.Data.SqlClient.SqlParameter("@antiguedad_laboral", System.Data.SqlDbType.VarChar, 25, "antiguedad_laboral"))
                .Item("@antiguedad_laboral").Value = Connection.GetValue(oData, "antiguedad_laboral")

                .Add(New System.Data.SqlClient.SqlParameter("@notas", System.Data.SqlDbType.Text, 0, "notas"))
                .Item("@notas").Value = Connection.GetValue(oData, "notas")

                .Add(New System.Data.SqlClient.SqlParameter("@cliente_aval", System.Data.SqlDbType.Int, 4, "cliente_aval"))
                .Item("@cliente_aval").Value = Connection.GetValue(oData, "cliente_aval")

                .Add(New System.Data.SqlClient.SqlParameter("@aval", System.Data.SqlDbType.VarChar, 100, "aval"))
                .Item("@aval").Value = Connection.GetValue(oData, "aval")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_aval", System.Data.SqlDbType.VarChar, 50, "domicilio_aval"))
                .Item("@domicilio_aval").Value = Connection.GetValue(oData, "domicilio_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_aval", System.Data.SqlDbType.VarChar, 50, "colonia_aval"))
                .Item("@colonia_aval").Value = Connection.GetValue(oData, "colonia_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_aval", System.Data.SqlDbType.VarChar, 13, "telefono_aval"))
                .Item("@telefono_aval").Value = Connection.GetValue(oData, "telefono_aval")

                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_aval", System.Data.SqlDbType.VarChar, 60, "ciudad_aval"))
                .Item("@ciudad_aval").Value = Connection.GetValue(oData, "ciudad_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@estado_aval", System.Data.SqlDbType.VarChar, 60, "estado_aval"))
                .Item("@estado_aval").Value = Connection.GetValue(oData, "estado_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@anios_aval", System.Data.SqlDbType.Int, 4, "anios_aval"))
                .Item("@anios_aval").Value = Connection.GetValue(oData, "anios_aval")

                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_aval", System.Data.SqlDbType.VarChar, 50, "parentesco_aval"))
                .Item("@parentesco_aval").Value = Connection.GetValue(oData, "parentesco_aval")


                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cobro", System.Data.SqlDbType.Char, 1, "tipo_cobro"))
                .Item("@tipo_cobro").Value = Connection.GetValue(oData, "tipo_cobro")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_alta", System.Data.SqlDbType.DateTime, 8, "fecha_alta"))
                .Item("@fecha_alta").Value = Connection.GetValue(oData, "fecha_alta")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@persona", System.Data.SqlDbType.Char, 1, "persona"))
                .Item("@persona").Value = Connection.GetValue(oData, "persona")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@no_paga_comision", System.Data.SqlDbType.Bit, 1, "no_paga_comision"))
                .Item("@no_paga_comision").Value = Connection.GetValue(oData, "no_paga_comision")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc_completo", System.Data.SqlDbType.Bit, 1, "rfc_completo"))
                .Item("@rfc_completo").Value = rfc_completo
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_dependencias", System.Data.SqlDbType.Int, 4, "cliente_dependencias"))
                .Item("@cliente_dependencias").Value = Connection.GetValue(oData, "cliente_dependencias")
                .Add(New System.Data.SqlClient.SqlParameter("@limite_credito", System.Data.SqlDbType.Money, 8, "limite_credito"))
                .Item("@limite_credito").Value = Connection.GetValue(oData, "limite_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_captura", System.Data.SqlDbType.Char, 1, "tipo_captura"))
                .Item("@tipo_captura").Value = tipo_captura

                .Add(New System.Data.SqlClient.SqlParameter("@localizado", System.Data.SqlDbType.Bit, 1, "localizado"))
                .Item("@localizado").Value = Connection.GetValue(oData, "localizado")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_conyuge", System.Data.SqlDbType.VarChar, 80, "nombre_conyuge"))
                .Item("@nombre_conyuge").Value = Connection.GetValue(oData, "nombre_conyuge")
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_conyuge", System.Data.SqlDbType.VarChar, 50, "trabaja_en_conyuge"))
                .Item("@trabaja_en_conyuge").Value = Connection.GetValue(oData, "trabaja_en_conyuge")
                .Add(New System.Data.SqlClient.SqlParameter("@puesto_conyuge", System.Data.SqlDbType.VarChar, 80, "puesto_conyuge"))
                .Item("@puesto_conyuge").Value = Connection.GetValue(oData, "puesto_conyuge")
                .Add(New System.Data.SqlClient.SqlParameter("@departamento_conyuge", System.Data.SqlDbType.VarChar, 80, "departamento_conyuge"))
                .Item("@departamento_conyuge").Value = Connection.GetValue(oData, "departamento_conyuge")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos_laboral_conyuge", System.Data.SqlDbType.VarChar, 25, "telefonos_laboral_conyuge"))
                .Item("@telefonos_laboral_conyuge").Value = Connection.GetValue(oData, "telefonos_laboral_conyuge")

                .Add(New System.Data.SqlClient.SqlParameter("@motivos_no_localizable", System.Data.SqlDbType.Text, 0, "motivos_no_localizable"))
                .Item("@motivos_no_localizable").Value = Connection.GetValue(oData, "motivos_no_localizable")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_no_localizable", System.Data.SqlDbType.VarChar, 15, "usuario_no_localizable"))
                .Item("@usuario_no_localizable").Value = Connection.GetValue(oData, "usuario_no_localizable")

                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = Connection.GetValue(oData, "activo")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                .Add(New System.Data.SqlClient.SqlParameter("@estado_civil", System.Data.SqlDbType.Char, 1, "estado_civil"))
                .Item("@estado_civil").Value = Connection.GetValue(oData, "estado_civil")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_referencia", System.Data.SqlDbType.VarChar, 80, "nombre_referencia"))
                .Item("@nombre_referencia").Value = Connection.GetValue(oData, "nombre_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_referencia", System.Data.SqlDbType.VarChar, 50, "domicilio_referencia"))
                .Item("@domicilio_referencia").Value = Connection.GetValue(oData, "domicilio_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_referencia", System.Data.SqlDbType.VarChar, 50, "colonia_referencia"))
                .Item("@colonia_referencia").Value = Connection.GetValue(oData, "colonia_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_referencia", System.Data.SqlDbType.VarChar, 13, "telefono_referencia"))
                .Item("@telefono_referencia").Value = Connection.GetValue(oData, "telefono_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_referencia", System.Data.SqlDbType.VarChar, 60, "ciudad_referencia"))
                .Item("@ciudad_referencia").Value = Connection.GetValue(oData, "ciudad_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@estado_referencia", System.Data.SqlDbType.VarChar, 60, "estado_referencia"))
                .Item("@estado_referencia").Value = Connection.GetValue(oData, "estado_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_referencia", System.Data.SqlDbType.VarChar, 50, "trabaja_en_referencia"))
                .Item("@trabaja_en_referencia").Value = Connection.GetValue(oData, "trabaja_en_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_referencia", System.Data.SqlDbType.VarChar, 50, "parentesco_referencia"))
                .Item("@parentesco_referencia").Value = Connection.GetValue(oData, "parentesco_referencia")

                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = Connection.GetValue(oData, "forma_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@ultimos_digitos_cuenta", System.Data.SqlDbType.Int, 4, "ultimos_digitos_cuenta"))
                .Item("@ultimos_digitos_cuenta").Value = Connection.GetValue(oData, "ultimos_digitos_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.VarChar, 100, "email"))
                .Item("@email").Value = Connection.GetValue(oData, "email")

                .Add(New System.Data.SqlClient.SqlParameter("@facturacion_especial", System.Data.SqlDbType.Bit, 1, "facturacion_especial"))
                .Item("@facturacion_especial").Value = Connection.GetValue(oData, "facturacion_especial")
            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "cliente", oCommand.Parameters.Item("@cliente").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function sp_clientes_ins(ByRef cliente As Long, ByVal nombre As String, ByVal nombres As String, ByVal paterno As String, ByVal materno As String, ByVal rfc As String, _
    ByVal curp As String, ByVal domicilio As String, ByVal numero_exterior As String, ByVal numero_interior As String, ByVal entrecalle As String, ByVal ycalle As String, ByVal colonia As Long, _
    ByVal estado As Long, ByVal ciudad As Long, ByVal municipio As Long, ByVal cp As Long, ByVal telefono1 As String, ByVal telefono2 As String, ByVal fax As String, ByVal ocupacion As String, ByVal ingresos As Double, _
    ByVal puesto As String, ByVal departamento As String, ByVal telefonos_laboral As String, ByVal antiguedad_laboral As String, ByVal notas As String, ByVal cliente_aval As Long, ByVal aval As String, _
    ByVal domicilio_aval As String, ByVal colonia_aval As String, ByVal telefono_aval As String, ByVal ciudad_aval As String, ByVal estado_aval As String, ByVal anios_aval As Long, ByVal parentesco_aval As String, _
    ByVal tipo_cobro As Char, ByVal fecha_alta As DateTime, ByVal persona As Char, ByVal sucursal As Long, ByVal no_paga_comision As Boolean, ByVal rfc_completo As Boolean, ByVal cliente_dependencias As Long, ByVal limite_credito As Double, _
    ByVal tipo_captura As Char, ByVal localizado As Boolean, ByVal nombre_conyuge As String, ByVal trabaja_en_conyuge As String, ByVal puesto_conyuge As String, ByVal departamento_conyuge As String, ByVal telefonos_laboral_conyuge As String, _
    ByVal motivos_no_localizable As String, ByVal usuario_no_localizable As String, ByVal activo As Boolean, ByVal estado_civil As Char, ByVal nombre_referencia As String, ByVal domicilio_referencia As String, ByVal colonia_referencia As String, _
    ByVal telefono_referencia As String, ByVal ciudad_referencia As String, ByVal estado_referencia As String, ByVal trabaja_en_referencia As String, ByVal parentesco_referencia As String, ByVal forma_pago As Long, _
    ByVal ultimos_digitos_cuenta As Long, ByVal email As String, ByVal facturacion_especial As Boolean, ByVal fecha_nacimiento As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Item("@cliente").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = nombre
                .Add(New System.Data.SqlClient.SqlParameter("@nombres", System.Data.SqlDbType.VarChar, 40, "nombres"))
                .Item("@nombres").Value = nombres
                .Add(New System.Data.SqlClient.SqlParameter("@paterno", System.Data.SqlDbType.VarChar, 30, "paterno"))
                .Item("@paterno").Value = paterno
                .Add(New System.Data.SqlClient.SqlParameter("@materno", System.Data.SqlDbType.VarChar, 30, "materno"))
                .Item("@materno").Value = materno

                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = rfc
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = curp
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = domicilio
                .Add(New System.Data.SqlClient.SqlParameter("@numero_exterior", System.Data.SqlDbType.VarChar, 10, "numero_exterior"))
                .Item("@numero_exterior").Value = numero_exterior
                .Add(New System.Data.SqlClient.SqlParameter("@numero_interior", System.Data.SqlDbType.VarChar, 10, "numero_interior"))
                .Item("@numero_interior").Value = numero_interior
                .Add(New System.Data.SqlClient.SqlParameter("@entrecalle", System.Data.SqlDbType.VarChar, 30, "entrecalle"))
                .Item("@entrecalle").Value = entrecalle
                .Add(New System.Data.SqlClient.SqlParameter("@ycalle", System.Data.SqlDbType.VarChar, 30, "ycalle"))
                .Item("@ycalle").Value = ycalle
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = colonia
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = cp
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = ciudad
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio

                .Add(New System.Data.SqlClient.SqlParameter("@telefono1", System.Data.SqlDbType.VarChar, 25, "telefono1"))
                .Item("@telefono1").Value = telefono1
                .Add(New System.Data.SqlClient.SqlParameter("@telefono2", System.Data.SqlDbType.VarChar, 25, "telefono2"))
                .Item("@telefono2").Value = telefono2
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 13, "fax"))
                .Item("@fax").Value = fax
                .Add(New System.Data.SqlClient.SqlParameter("@ocupacion", System.Data.SqlDbType.VarChar, 50, "ocupacion"))
                .Item("@ocupacion").Value = ocupacion
                .Add(New System.Data.SqlClient.SqlParameter("@ingresos", System.Data.SqlDbType.Money, 8, "ingresos"))
                .Item("@ingresos").Value = ingresos

                .Add(New System.Data.SqlClient.SqlParameter("@puesto", System.Data.SqlDbType.VarChar, 80, "puesto"))
                .Item("@puesto").Value = puesto
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.VarChar, 80, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos_laboral", System.Data.SqlDbType.VarChar, 25, "telefonos_laboral"))
                .Item("@telefonos_laboral").Value = telefonos_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@antiguedad_laboral", System.Data.SqlDbType.VarChar, 25, "antiguedad_laboral"))
                .Item("@antiguedad_laboral").Value = antiguedad_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@notas", System.Data.SqlDbType.Text, 0, "notas"))
                .Item("@notas").Value = notas

                .Add(New System.Data.SqlClient.SqlParameter("@cliente_aval", System.Data.SqlDbType.Int, 4, "cliente_aval"))
                .Item("@cliente_aval").Value = cliente_aval
                .Add(New System.Data.SqlClient.SqlParameter("@aval", System.Data.SqlDbType.VarChar, 100, "aval"))
                .Item("@aval").Value = aval
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_aval", System.Data.SqlDbType.VarChar, 50, "domicilio_aval"))
                .Item("@domicilio_aval").Value = domicilio_aval
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_aval", System.Data.SqlDbType.VarChar, 50, "colonia_aval"))
                .Item("@colonia_aval").Value = colonia_aval
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_aval", System.Data.SqlDbType.VarChar, 13, "telefono_aval"))
                .Item("@telefono_aval").Value = telefono_aval

                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_aval", System.Data.SqlDbType.VarChar, 60, "ciudad_aval"))
                .Item("@ciudad_aval").Value = ciudad_aval
                .Add(New System.Data.SqlClient.SqlParameter("@estado_aval", System.Data.SqlDbType.VarChar, 60, "estado_aval"))
                .Item("@estado_aval").Value = estado_aval
                .Add(New System.Data.SqlClient.SqlParameter("@anios_aval", System.Data.SqlDbType.Int, 4, "anios_aval"))
                .Item("@anios_aval").Value = anios_aval
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_aval", System.Data.SqlDbType.VarChar, 50, "parentesco_aval"))
                .Item("@parentesco_aval").Value = parentesco_aval

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cobro", System.Data.SqlDbType.Char, 1, "tipo_cobro"))
                .Item("@tipo_cobro").Value = tipo_cobro
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_alta", System.Data.SqlDbType.DateTime, 8, "fecha_alta"))
                .Item("@fecha_alta").Value = fecha_alta
                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@persona", System.Data.SqlDbType.Char, 1, "persona"))
                .Item("@persona").Value = persona
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@no_paga_comision", System.Data.SqlDbType.Bit, 1, "no_paga_comision"))
                .Item("@no_paga_comision").Value = no_paga_comision
                .Add(New System.Data.SqlClient.SqlParameter("@rfc_completo", System.Data.SqlDbType.Bit, 1, "rfc_completo"))
                .Item("@rfc_completo").Value = rfc_completo
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_dependencias", System.Data.SqlDbType.Int, 4, "cliente_dependencias"))
                .Item("@cliente_dependencias").Value = cliente_dependencias
                .Add(New System.Data.SqlClient.SqlParameter("@limite_credito", System.Data.SqlDbType.Money, 8, "limite_credito"))
                .Item("@limite_credito").Value = limite_credito
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_captura", System.Data.SqlDbType.Char, 1, "tipo_captura"))
                .Item("@tipo_captura").Value = tipo_captura


                .Add(New System.Data.SqlClient.SqlParameter("@localizado", System.Data.SqlDbType.Bit, 1, "localizado"))
                .Item("@localizado").Value = localizado
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_conyuge", System.Data.SqlDbType.VarChar, 80, "nombre_conyuge"))
                .Item("@nombre_conyuge").Value = nombre_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_conyuge", System.Data.SqlDbType.VarChar, 50, "trabaja_en_conyuge"))
                .Item("@trabaja_en_conyuge").Value = trabaja_en_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@puesto_conyuge", System.Data.SqlDbType.VarChar, 80, "puesto_conyuge"))
                .Item("@puesto_conyuge").Value = puesto_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@departamento_conyuge", System.Data.SqlDbType.VarChar, 80, "departamento_conyuge"))
                .Item("@departamento_conyuge").Value = departamento_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos_laboral_conyuge", System.Data.SqlDbType.VarChar, 25, "telefonos_laboral_conyuge"))
                .Item("@telefonos_laboral_conyuge").Value = telefonos_laboral_conyuge

                .Add(New System.Data.SqlClient.SqlParameter("@motivos_no_localizable", System.Data.SqlDbType.Text, 0, "motivos_no_localizable"))
                .Item("@motivos_no_localizable").Value = motivos_no_localizable
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_no_localizable", System.Data.SqlDbType.VarChar, 15, "usuario_no_localizable"))
                .Item("@usuario_no_localizable").Value = usuario_no_localizable

                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = activo

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                .Add(New System.Data.SqlClient.SqlParameter("@estado_civil", System.Data.SqlDbType.Char, 1, "estado_civil"))
                .Item("@estado_civil").Value = estado_civil



                .Add(New System.Data.SqlClient.SqlParameter("@nombre_referencia", System.Data.SqlDbType.VarChar, 80, "nombre_referencia"))
                .Item("@nombre_referencia").Value = nombre_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_referencia", System.Data.SqlDbType.VarChar, 50, "domicilio_referencia"))
                .Item("@domicilio_referencia").Value = domicilio_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_referencia", System.Data.SqlDbType.VarChar, 50, "colonia_referencia"))
                .Item("@colonia_referencia").Value = colonia_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_referencia", System.Data.SqlDbType.VarChar, 13, "telefono_referencia"))
                .Item("@telefono_referencia").Value = telefono_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_referencia", System.Data.SqlDbType.VarChar, 60, "ciudad_referencia"))
                .Item("@ciudad_referencia").Value = ciudad_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@estado_referencia", System.Data.SqlDbType.VarChar, 60, "estado_referencia"))
                .Item("@estado_referencia").Value = estado_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_referencia", System.Data.SqlDbType.VarChar, 50, "trabaja_en_referencia"))
                .Item("@trabaja_en_referencia").Value = trabaja_en_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_referencia", System.Data.SqlDbType.VarChar, 50, "parentesco_referencia"))
                .Item("@parentesco_referencia").Value = parentesco_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago
                .Add(New System.Data.SqlClient.SqlParameter("@ultimos_digitos_cuenta", System.Data.SqlDbType.Int, 4, "ultimos_digitos_cuenta"))
                .Item("@ultimos_digitos_cuenta").Value = ultimos_digitos_cuenta
                .Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.VarChar, 100, "email"))
                .Item("@email").Value = email

                .Add(New System.Data.SqlClient.SqlParameter("@facturacion_especial", System.Data.SqlDbType.Bit, 1, "facturacion_especial"))
                .Item("@facturacion_especial").Value = facturacion_especial
            End With
            Connection.Execute(oCommand)
            cliente = oCommand.Parameters.Item("@cliente").Value


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_clientes_ins_plantilla(ByRef oData As DataSet, ByVal nombre As String, ByVal rfc As String, ByVal nombres As String, ByVal paterno As String, ByVal materno As String, ByVal fecha_alta As DateTime, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal colonia As Long, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_ins_plantilla]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Item("@cliente").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = nombre
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = rfc
                .Add(New System.Data.SqlClient.SqlParameter("@nombres", System.Data.SqlDbType.VarChar, 40, "nombres"))
                .Item("@nombres").Value = nombres
                .Add(New System.Data.SqlClient.SqlParameter("@paterno", System.Data.SqlDbType.VarChar, 30, "paterno"))
                .Item("@paterno").Value = paterno
                .Add(New System.Data.SqlClient.SqlParameter("@materno", System.Data.SqlDbType.VarChar, 30, "materno"))
                .Item("@materno").Value = materno
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_alta", System.Data.SqlDbType.DateTime, 8, "fecha_alta"))
                .Item("@fecha_alta").Value = fecha_alta

                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = ciudad
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = colonia

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                '.Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                '.Item("@activo").Value = Connection.GetValue(oData, "activo")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User



            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "cliente", oCommand.Parameters.Item("@cliente").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_upd(ByRef oData As DataSet, ByVal rfc_completo As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Connection.GetValue(oData, "cliente")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@nombres", System.Data.SqlDbType.VarChar, 40, "nombres"))
                .Item("@nombres").Value = Connection.GetValue(oData, "nombres")
                .Add(New System.Data.SqlClient.SqlParameter("@paterno", System.Data.SqlDbType.VarChar, 30, "paterno"))
                .Item("@paterno").Value = Connection.GetValue(oData, "paterno")
                .Add(New System.Data.SqlClient.SqlParameter("@materno", System.Data.SqlDbType.VarChar, 30, "materno"))
                .Item("@materno").Value = Connection.GetValue(oData, "materno")

                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@numero_exterior", System.Data.SqlDbType.VarChar, 10, "numero_exterior"))
                .Item("@numero_exterior").Value = Connection.GetValue(oData, "numero_exterior")
                .Add(New System.Data.SqlClient.SqlParameter("@numero_interior", System.Data.SqlDbType.VarChar, 10, "numero_interior"))
                .Item("@numero_interior").Value = Connection.GetValue(oData, "numero_interior")

                .Add(New System.Data.SqlClient.SqlParameter("@entrecalle", System.Data.SqlDbType.VarChar, 30, "entrecalle"))
                .Item("@entrecalle").Value = Connection.GetValue(oData, "entrecalle")
                .Add(New System.Data.SqlClient.SqlParameter("@ycalle", System.Data.SqlDbType.VarChar, 30, "ycalle"))
                .Item("@ycalle").Value = Connection.GetValue(oData, "ycalle")

                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")

                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")

                .Add(New System.Data.SqlClient.SqlParameter("@telefono1", System.Data.SqlDbType.VarChar, 25, "telefono1"))
                .Item("@telefono1").Value = Connection.GetValue(oData, "telefono1")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono2", System.Data.SqlDbType.VarChar, 25, "telefono2"))
                .Item("@telefono2").Value = Connection.GetValue(oData, "telefono2")
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 13, "fax"))
                .Item("@fax").Value = Connection.GetValue(oData, "fax")
                .Add(New System.Data.SqlClient.SqlParameter("@ocupacion", System.Data.SqlDbType.VarChar, 50, "ocupacion"))
                .Item("@ocupacion").Value = Connection.GetValue(oData, "ocupacion")
                .Add(New System.Data.SqlClient.SqlParameter("@ingresos", System.Data.SqlDbType.Money, 8, "ingresos"))
                .Item("@ingresos").Value = Connection.GetValue(oData, "ingresos")

                .Add(New System.Data.SqlClient.SqlParameter("@puesto", System.Data.SqlDbType.VarChar, 80, "puesto"))
                .Item("@puesto").Value = Connection.GetValue(oData, "puesto")
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.VarChar, 80, "departamento"))
                .Item("@departamento").Value = Connection.GetValue(oData, "departamento")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos_laboral", System.Data.SqlDbType.VarChar, 25, "telefonos_laboral"))
                .Item("@telefonos_laboral").Value = Connection.GetValue(oData, "telefonos_laboral")
                .Add(New System.Data.SqlClient.SqlParameter("@antiguedad_laboral", System.Data.SqlDbType.VarChar, 25, "antiguedad_laboral"))
                .Item("@antiguedad_laboral").Value = Connection.GetValue(oData, "antiguedad_laboral")
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_aval", System.Data.SqlDbType.VarChar, 50, "parentesco_aval"))
                .Item("@parentesco_aval").Value = Connection.GetValue(oData, "parentesco_aval")

                .Add(New System.Data.SqlClient.SqlParameter("@notas", System.Data.SqlDbType.Text, 0, "notas"))
                .Item("@notas").Value = Connection.GetValue(oData, "notas")
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_aval", System.Data.SqlDbType.Int, 4, "cliente_aval"))
                .Item("@cliente_aval").Value = Connection.GetValue(oData, "cliente_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@aval", System.Data.SqlDbType.VarChar, 100, "aval"))
                .Item("@aval").Value = Connection.GetValue(oData, "aval")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_aval", System.Data.SqlDbType.VarChar, 50, "domicilio_aval"))
                .Item("@domicilio_aval").Value = Connection.GetValue(oData, "domicilio_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_aval", System.Data.SqlDbType.VarChar, 50, "colonia_aval"))
                .Item("@colonia_aval").Value = Connection.GetValue(oData, "colonia_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_aval", System.Data.SqlDbType.VarChar, 13, "telefono_aval"))
                .Item("@telefono_aval").Value = Connection.GetValue(oData, "telefono_aval")

                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_aval", System.Data.SqlDbType.VarChar, 60, "ciudad_aval"))
                .Item("@ciudad_aval").Value = Connection.GetValue(oData, "ciudad_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@estado_aval", System.Data.SqlDbType.VarChar, 60, "estado_aval"))
                .Item("@estado_aval").Value = Connection.GetValue(oData, "estado_aval")
                .Add(New System.Data.SqlClient.SqlParameter("@anios_aval", System.Data.SqlDbType.Int, 4, "anios_aval"))
                .Item("@anios_aval").Value = Connection.GetValue(oData, "anios_aval")

                .Add(New System.Data.SqlClient.SqlParameter("@anios_aval", System.Data.SqlDbType.Int, 4, "anios_aval"))


                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_aval", System.Data.SqlDbType.VarChar, 50, "parentesco_aval"))
                .Item("@anios_aval").Value = Connection.GetValue(oData, "parentesco_aval")

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cobro", System.Data.SqlDbType.Char, 1, "tipo_cobro"))
                .Item("@tipo_cobro").Value = Connection.GetValue(oData, "tipo_cobro")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_alta", System.Data.SqlDbType.DateTime, 8, "fecha_alta"))
                .Item("@fecha_alta").Value = Connection.GetValue(oData, "fecha_alta")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@persona", System.Data.SqlDbType.Char, 1, "persona"))
                .Item("@persona").Value = Connection.GetValue(oData, "persona")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@no_paga_comision", System.Data.SqlDbType.Bit, 1, "no_paga_comision"))
                .Item("@no_paga_comision").Value = Connection.GetValue(oData, "no_paga_comision")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc_completo", System.Data.SqlDbType.Bit, 1, "rfc_completo"))
                .Item("@rfc_completo").Value = rfc_completo
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_dependencias", System.Data.SqlDbType.Int, 4, "cliente_dependencias"))
                .Item("@cliente_dependencias").Value = Connection.GetValue(oData, "cliente_dependencias")
                .Add(New System.Data.SqlClient.SqlParameter("@limite_credito", System.Data.SqlDbType.Money, 8, "limite_credito"))
                .Item("@limite_credito").Value = Connection.GetValue(oData, "limite_credito")

                .Add(New System.Data.SqlClient.SqlParameter("@localizado", System.Data.SqlDbType.Bit, 1, "localizado"))
                .Item("@localizado").Value = Connection.GetValue(oData, "localizado")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_conyuge", System.Data.SqlDbType.VarChar, 80, "nombre_conyuge"))
                .Item("@nombre_conyuge").Value = Connection.GetValue(oData, "nombre_conyuge")
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_conyuge", System.Data.SqlDbType.VarChar, 50, "trabaja_en_conyuge"))
                .Item("@trabaja_en_conyuge").Value = Connection.GetValue(oData, "trabaja_en_conyuge")
                .Add(New System.Data.SqlClient.SqlParameter("@puesto_conyuge", System.Data.SqlDbType.VarChar, 80, "puesto_conyuge"))
                .Item("@puesto_conyuge").Value = Connection.GetValue(oData, "puesto_conyuge")
                .Add(New System.Data.SqlClient.SqlParameter("@departamento_conyuge", System.Data.SqlDbType.VarChar, 80, "departamento_conyuge"))
                .Item("@departamento_conyuge").Value = Connection.GetValue(oData, "departamento_conyuge")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos_laboral_conyuge", System.Data.SqlDbType.VarChar, 25, "telefonos_laboral_conyuge"))
                .Item("@telefonos_laboral_conyuge").Value = Connection.GetValue(oData, "telefonos_laboral_conyuge")

                .Add(New System.Data.SqlClient.SqlParameter("@motivos_no_localizable", System.Data.SqlDbType.Text, 0, "motivos_no_localizable"))
                .Item("@motivos_no_localizable").Value = Connection.GetValue(oData, "motivos_no_localizable")
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_no_localizable", System.Data.SqlDbType.VarChar, 15, "usuario_no_localizable"))
                .Item("@usuario_no_localizable").Value = Connection.GetValue(oData, "usuario_no_localizable")

                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = Connection.GetValue(oData, "activo")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User


                .Add(New System.Data.SqlClient.SqlParameter("@estado_civil", System.Data.SqlDbType.Char, 1, "estado_civil"))
                .Item("@estado_civil").Value = Connection.GetValue(oData, "estado_civil")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_referencia", System.Data.SqlDbType.VarChar, 80, "nombre_referencia"))
                .Item("@nombre_referencia").Value = Connection.GetValue(oData, "nombre_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_referencia", System.Data.SqlDbType.VarChar, 50, "domicilio_referencia"))
                .Item("@domicilio_referencia").Value = Connection.GetValue(oData, "domicilio_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_referencia", System.Data.SqlDbType.VarChar, 50, "colonia_referencia"))
                .Item("@colonia_referencia").Value = Connection.GetValue(oData, "colonia_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_referencia", System.Data.SqlDbType.VarChar, 13, "telefono_referencia"))
                .Item("@telefono_referencia").Value = Connection.GetValue(oData, "telefono_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_referencia", System.Data.SqlDbType.VarChar, 60, "ciudad_referencia"))
                .Item("@ciudad_referencia").Value = Connection.GetValue(oData, "ciudad_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@estado_referencia", System.Data.SqlDbType.VarChar, 60, "estado_referencia"))
                .Item("@estado_referencia").Value = Connection.GetValue(oData, "estado_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_referencia", System.Data.SqlDbType.VarChar, 50, "trabaja_en_referencia"))
                .Item("@trabaja_en_referencia").Value = Connection.GetValue(oData, "trabaja_en_referencia")
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_referencia", System.Data.SqlDbType.VarChar, 50, "parentesco_referencia"))
                .Item("@parentesco_referencia").Value = Connection.GetValue(oData, "parentesco_referencia")


                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = Connection.GetValue(oData, "forma_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@ultimos_digitos_cuenta", System.Data.SqlDbType.Int, 4, "ultimos_digitos_cuenta"))
                .Item("@ultimos_digitos_cuenta").Value = Connection.GetValue(oData, "ultimos_digitos_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.VarChar, 100, "email"))
                .Item("@email").Value = Connection.GetValue(oData, "email")
                .Add(New System.Data.SqlClient.SqlParameter("@facturacion_especial", System.Data.SqlDbType.Bit, 1, "facturacion_especial"))
                .Item("@facturacion_especial").Value = Connection.GetValue(oData, "facturacion_especial")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_upd(ByVal cliente As Long, ByVal nombre As String, ByVal nombres As String, ByVal paterno As String, ByVal materno As String, ByVal rfc As String, ByVal curp As String, ByVal domicilio As String, _
    ByVal numero_exterior As String, ByVal numero_interior As String, ByVal entrecalle As String, ByVal ycalle As String, ByVal colonia As Long, ByVal estado As Long, ByVal ciudad As Long, ByVal municipio As Long, ByVal cp As Long, _
   ByVal telefono1 As String, ByVal telefono2 As String, ByVal fax As String, ByVal ocupacion As String, ByVal ingresos As Double, ByVal puesto As String, ByVal departamento As String, ByVal telefonos_laboral As String, _
   ByVal antiguedad_laboral As String, ByVal notas As String, ByVal cliente_aval As Long, ByVal aval As String, ByVal domicilio_aval As String, ByVal colonia_aval As String, ByVal telefono_aval As String, ByVal ciudad_aval As String, _
   ByVal estado_aval As String, ByVal anios_aval As Long, ByVal parentesco_aval As String, ByVal tipo_cobro As Char, ByVal fecha_alta As DateTime, ByVal persona As Char, ByVal sucursal As Long, ByVal no_paga_comision As Boolean, ByVal rfc_completo As Boolean, _
   ByVal cliente_dependencias As Long, ByVal limite_credito As Double, ByVal localizado As Boolean, ByVal nombre_conyuge As String, ByVal trabaja_en_conyuge As String, ByVal puesto_conyuge As String, ByVal departamento_conyuge As String, _
   ByVal telefonos_laboral_conyuge As String, ByVal motivos_no_localizable As String, ByVal usuario_no_localizable As String, ByVal activo As Boolean, ByVal estado_civil As Char, ByVal nombre_referencia As String, ByVal domicilio_referencia As String, ByVal colonia_referencia As String, _
    ByVal telefono_referencia As String, ByVal ciudad_referencia As String, ByVal estado_referencia As String, ByVal trabaja_en_referencia As String, ByVal parentesco_referencia As String, ByVal forma_pago As Long, _
    ByVal ultimos_digitos_cuenta As Long, ByVal email As String, ByVal facturacion_especial As Boolean, ByVal fecha_nacimiento As DateTime) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = nombre
                .Add(New System.Data.SqlClient.SqlParameter("@nombres", System.Data.SqlDbType.VarChar, 40, "nombres"))
                .Item("@nombres").Value = nombres
                .Add(New System.Data.SqlClient.SqlParameter("@paterno", System.Data.SqlDbType.VarChar, 30, "paterno"))
                .Item("@paterno").Value = paterno
                .Add(New System.Data.SqlClient.SqlParameter("@materno", System.Data.SqlDbType.VarChar, 30, "materno"))
                .Item("@materno").Value = materno

                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = rfc
                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = curp
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_nacimiento", System.Data.SqlDbType.DateTime, 8, "fecha_nacimiento"))
                .Item("@fecha_nacimiento").Value = fecha_nacimiento

                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = domicilio
                .Add(New System.Data.SqlClient.SqlParameter("@numero_exterior", System.Data.SqlDbType.VarChar, 10, "numero_exterior"))
                .Item("@numero_exterior").Value = numero_exterior
                .Add(New System.Data.SqlClient.SqlParameter("@numero_interior", System.Data.SqlDbType.VarChar, 10, "numero_interior"))
                .Item("@numero_interior").Value = numero_interior

                .Add(New System.Data.SqlClient.SqlParameter("@entrecalle", System.Data.SqlDbType.VarChar, 30, "entrecalle"))
                .Item("@entrecalle").Value = entrecalle
                .Add(New System.Data.SqlClient.SqlParameter("@ycalle", System.Data.SqlDbType.VarChar, 30, "ycalle"))
                .Item("@ycalle").Value = ycalle

                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = colonia
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = cp
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = ciudad
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio

                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado

                .Add(New System.Data.SqlClient.SqlParameter("@telefono1", System.Data.SqlDbType.VarChar, 25, "telefono1"))
                .Item("@telefono1").Value = telefono1
                .Add(New System.Data.SqlClient.SqlParameter("@telefono2", System.Data.SqlDbType.VarChar, 25, "telefono2"))
                .Item("@telefono2").Value = telefono2
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 13, "fax"))
                .Item("@fax").Value = fax
                .Add(New System.Data.SqlClient.SqlParameter("@ocupacion", System.Data.SqlDbType.VarChar, 50, "ocupacion"))
                .Item("@ocupacion").Value = ocupacion
                .Add(New System.Data.SqlClient.SqlParameter("@ingresos", System.Data.SqlDbType.Money, 8, "ingresos"))
                .Item("@ingresos").Value = ingresos

                .Add(New System.Data.SqlClient.SqlParameter("@puesto", System.Data.SqlDbType.VarChar, 80, "puesto"))
                .Item("@puesto").Value = puesto
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.VarChar, 80, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos_laboral", System.Data.SqlDbType.VarChar, 25, "telefonos_laboral"))
                .Item("@telefonos_laboral").Value = telefonos_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@antiguedad_laboral", System.Data.SqlDbType.VarChar, 25, "antiguedad_laboral"))
                .Item("@antiguedad_laboral").Value = antiguedad_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@notas", System.Data.SqlDbType.Text, 0, "notas"))
                .Item("@notas").Value = notas

                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_aval", System.Data.SqlDbType.VarChar, 50, "parentesco_aval"))
                .Item("@parentesco_aval").Value = parentesco_aval


                .Add(New System.Data.SqlClient.SqlParameter("@cliente_aval", System.Data.SqlDbType.Int, 4, "cliente_aval"))
                .Item("@cliente_aval").Value = cliente_aval
                .Add(New System.Data.SqlClient.SqlParameter("@aval", System.Data.SqlDbType.VarChar, 100, "aval"))
                .Item("@aval").Value = aval
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_aval", System.Data.SqlDbType.VarChar, 50, "domicilio_aval"))
                .Item("@domicilio_aval").Value = domicilio_aval
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_aval", System.Data.SqlDbType.VarChar, 50, "colonia_aval"))
                .Item("@colonia_aval").Value = colonia_aval
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_aval", System.Data.SqlDbType.VarChar, 13, "telefono_aval"))
                .Item("@telefono_aval").Value = telefono_aval

                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_aval", System.Data.SqlDbType.VarChar, 60, "ciudad_aval"))
                .Item("@ciudad_aval").Value = ciudad_aval
                .Add(New System.Data.SqlClient.SqlParameter("@estado_aval", System.Data.SqlDbType.VarChar, 60, "estado_aval"))
                .Item("@estado_aval").Value = estado_aval
                .Add(New System.Data.SqlClient.SqlParameter("@anios_aval", System.Data.SqlDbType.Int, 4, "anios_aval"))
                .Item("@anios_aval").Value = anios_aval

                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cobro", System.Data.SqlDbType.Char, 1, "tipo_cobro"))
                .Item("@tipo_cobro").Value = tipo_cobro
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_alta", System.Data.SqlDbType.DateTime, 8, "fecha_alta"))
                .Item("@fecha_alta").Value = fecha_alta
                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@persona", System.Data.SqlDbType.Char, 1, "persona"))
                .Item("@persona").Value = persona

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@no_paga_comision", System.Data.SqlDbType.Bit, 1, "no_paga_comision"))
                .Item("@no_paga_comision").Value = no_paga_comision
                .Add(New System.Data.SqlClient.SqlParameter("@rfc_completo", System.Data.SqlDbType.Bit, 1, "rfc_completo"))
                .Item("@rfc_completo").Value = rfc_completo
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_dependencias", System.Data.SqlDbType.Int, 4, "cliente_dependencias"))
                .Item("@cliente_dependencias").Value = cliente_dependencias
                .Add(New System.Data.SqlClient.SqlParameter("@limite_credito", System.Data.SqlDbType.Money, 8, "limite_credito"))
                .Item("@limite_credito").Value = limite_credito

                .Add(New System.Data.SqlClient.SqlParameter("@localizado", System.Data.SqlDbType.Bit, 1, "localizado"))
                .Item("@localizado").Value = localizado
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_conyuge", System.Data.SqlDbType.VarChar, 80, "nombre_conyuge"))
                .Item("@nombre_conyuge").Value = nombre_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_conyuge", System.Data.SqlDbType.VarChar, 50, "trabaja_en_conyuge"))
                .Item("@trabaja_en_conyuge").Value = trabaja_en_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@puesto_conyuge", System.Data.SqlDbType.VarChar, 80, "puesto_conyuge"))
                .Item("@puesto_conyuge").Value = puesto_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@departamento_conyuge", System.Data.SqlDbType.VarChar, 80, "departamento_conyuge"))
                .Item("@departamento_conyuge").Value = departamento_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos_laboral_conyuge", System.Data.SqlDbType.VarChar, 25, "telefonos_laboral_conyuge"))
                .Item("@telefonos_laboral_conyuge").Value = telefonos_laboral_conyuge


                .Add(New System.Data.SqlClient.SqlParameter("@motivos_no_localizable", System.Data.SqlDbType.Text, 0, "motivos_no_localizable"))
                .Item("@motivos_no_localizable").Value = motivos_no_localizable
                .Add(New System.Data.SqlClient.SqlParameter("@usuario_no_localizable", System.Data.SqlDbType.VarChar, 15, "usuario_no_localizable"))
                .Item("@usuario_no_localizable").Value = usuario_no_localizable

                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = activo

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

                .Add(New System.Data.SqlClient.SqlParameter("@estado_civil", System.Data.SqlDbType.Char, 1, "estado_civil"))
                .Item("@estado_civil").Value = estado_civil

                .Add(New System.Data.SqlClient.SqlParameter("@nombre_referencia", System.Data.SqlDbType.VarChar, 80, "nombre_referencia"))
                .Item("@nombre_referencia").Value = nombre_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_referencia", System.Data.SqlDbType.VarChar, 50, "domicilio_referencia"))
                .Item("@domicilio_referencia").Value = domicilio_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_referencia", System.Data.SqlDbType.VarChar, 50, "colonia_referencia"))
                .Item("@colonia_referencia").Value = colonia_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_referencia", System.Data.SqlDbType.VarChar, 13, "telefono_referencia"))
                .Item("@telefono_referencia").Value = telefono_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_referencia", System.Data.SqlDbType.VarChar, 60, "ciudad_referencia"))
                .Item("@ciudad_referencia").Value = ciudad_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@estado_referencia", System.Data.SqlDbType.VarChar, 60, "estado_referencia"))
                .Item("@estado_referencia").Value = estado_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_referencia", System.Data.SqlDbType.VarChar, 50, "trabaja_en_referencia"))
                .Item("@trabaja_en_referencia").Value = trabaja_en_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_referencia", System.Data.SqlDbType.VarChar, 50, "parentesco_referencia"))
                .Item("@parentesco_referencia").Value = parentesco_referencia


                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago
                .Add(New System.Data.SqlClient.SqlParameter("@ultimos_digitos_cuenta", System.Data.SqlDbType.Int, 4, "ultimos_digitos_cuenta"))
                .Item("@ultimos_digitos_cuenta").Value = ultimos_digitos_cuenta
                .Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.VarChar, 100, "email"))
                .Item("@email").Value = email

                .Add(New System.Data.SqlClient.SqlParameter("@facturacion_especial", System.Data.SqlDbType.Bit, 1, "facturacion_especial"))
                .Item("@facturacion_especial").Value = facturacion_especial
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_del(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_exs(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_sel(ByVal cliente As Long, ByVal sucursal_caja As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_caja", System.Data.SqlDbType.Int, 4, "sucursal_caja"))
                .Item("@sucursal_caja").Value = sucursal_caja

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_clientes_datos_juridico(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_datos_juridico]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grs(ByVal estado As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grl_reparto(ByVal sucursal_dependencia As Boolean, ByVal cliente_exluido As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grl_reparto]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Bit, 1, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = sucursal_dependencia
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_exluido", System.Data.SqlDbType.Int, 4, "cliente_exluido"))
                .Item("@cliente_exluido").Value = cliente_exluido

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grl_avales(ByVal cliente_exluido As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grl_avales]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente_exluido", System.Data.SqlDbType.Int, 4, "cliente_exluido"))
                .Item("@cliente_exluido").Value = cliente_exluido

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_clientes_grl_plantilla(ByVal dependencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grl_plantilla]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grl_caja() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grl_caja]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_clientes_grl_caja_telefono_llamada() As Events
    '   Dim oCommand As New System.Data.SqlClient.SqlCommand
    '  Dim oEvent As New Events
    ' Try
    '    oCommand.CommandText = "[sp_con_tel_llamada_grl]"
    '   With oCommand.Parameters
    '  End With
    ' oEvent.Value = Connection.GetDataSet(oCommand)
    'Catch ex As Exception
    '   oEvent.Ex = ex
    '  oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    ' oEvent.Layer = Events.ErrorLayer.DataLayer
    'Finally
    '   oCommand = Nothing
    'End Try

    'Return oEvent
    'End Function

    Public Function sp_clientes_cancelacion_abonos_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_cancelacion_abonos_grl]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_disponibles_juridico_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_disponibles_juridico_grl]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grl_llenado(ByVal sucursal_dependencia As Boolean, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grl_llenado]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Bit, 1, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = sucursal_dependencia
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grl_cliente(ByVal sucursal_actual As Long, ByVal anioactual As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grl_cliente]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual

                .Add(New System.Data.SqlClient.SqlParameter("@anioactual", System.Data.SqlDbType.Bit, 1, "anioactual"))
                .Item("@anioactual").Value = anioactual
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grl_ejecutivo(ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grl_ejecutivo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_convenios_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_convenios_grl]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_grl_fecha_venta() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_grl_fecha_venta]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_exs_plantilla(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_exs_plantilla]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_clientes_no_pagan_comisiones(ByVal no_pagan_comision As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_no_pagan_comisiones]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@no_pagan_comision", System.Data.SqlDbType.Bit, 1, "no_pagan_comision"))
                .Item("@no_pagan_comision").Value = no_pagan_comision

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_no_paga_comision_upd(ByVal cliente As Long, ByVal no_paga_comision As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_no_paga_comision_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@no_paga_comision", System.Data.SqlDbType.Int, 4, "no_paga_comision"))
                .Item("@no_paga_comision").Value = no_paga_comision

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_datos_adicionales_ins(ByVal Cliente As Long, ByVal telefono_nextel As String, ByVal extension As Long, ByVal tipo_casa As Char, ByVal pago_casa As Double, _
    ByVal area_laboral As String, ByVal direccion_conyuge As String, ByVal email_conyuge As String, ByVal telefono_conyuge As String, ByVal direccion_trabajo_conyuge As String, ByVal cp_aval As Long, ByVal telefono_celular_aval As String, _
    ByVal telefono_nextel_aval As String, ByVal telefono_oficina_aval As String, ByVal extension_aval As Long, ByVal estado_civil_aval As Char, ByVal email_aval As String, ByVal observaciones_aval As String, ByVal cp_referencia As Long, _
    ByVal telefono_celular_referencia As String, ByVal telefono_nextel_referencia As String, ByVal telefono_oficina_referencia As String, ByVal extension_referencia As Long, ByVal otros_referencia As String, ByVal nombre_referencia2 As String, _
    ByVal domicilio_referencia2 As String, ByVal colonia_referencia2 As String, ByVal telefono_referencia2 As String, ByVal ciudad_referencia2 As String, ByVal estado_referencia2 As String, ByVal cp_referencia2 As String, ByVal trabaja_en_referencia2 As String, _
    ByVal parentesco_referencia2 As String, ByVal telefono_celular_referencia2 As String, ByVal telefono_nextel_referencia2 As String, ByVal telefono_oficina_referencia2 As String, ByVal extension_referencia2 As String, ByVal otros_referencia2 As String, _
    ByVal nombre_referencia3 As String, ByVal domicilio_referencia3 As String, ByVal colonia_referencia3 As String, ByVal telefono_referencia3 As String, ByVal ciudad_referencia3 As String, ByVal estado_referencia3 As String, ByVal cp_referencia3 As String, _
    ByVal trabaja_en_referencia3 As String, ByVal parentesco_referencia3 As String, ByVal telefono_celular_referencia3 As String, ByVal telefono_nextel_referencia3 As String, ByVal telefono_oficina_referencia3 As String, ByVal extension_referencia3 As String, _
    ByVal otros_referencia3 As String, ByVal persona_autoriza As String, ByVal fecha_actualizacion As DateTime, ByVal email_referencia As String, ByVal email_referencia2 As String, ByVal email_referencia3 As String, _
    ByVal direccion_laboral As String, ByVal estado_laboral As Long, ByVal ciudad_laboral As Long, ByVal municipio_laboral As Long, ByVal colonia_laboral As Long, ByVal cp_laboral As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_clientes_datos_adicionales_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = Cliente
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_nextel", System.Data.SqlDbType.VarChar, 25, "telefono_nextel"))
                .Item("@telefono_nextel").Value = telefono_nextel
                .Add(New System.Data.SqlClient.SqlParameter("@extension", System.Data.SqlDbType.Int, 4, "extension"))
                .Item("@extension").Value = extension
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_casa", System.Data.SqlDbType.Char, 1, "tipo_casa"))
                .Item("@tipo_casa").Value = tipo_casa
                .Add(New System.Data.SqlClient.SqlParameter("@pago_casa", System.Data.SqlDbType.Money, 8, "pago_casa"))
                .Item("@pago_casa").Value = pago_casa

                .Add(New System.Data.SqlClient.SqlParameter("@direccion_laboral", System.Data.SqlDbType.VarChar, 50, "direccion_laboral"))
                .Item("@direccion_laboral").Value = direccion_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@estado_laboral", System.Data.SqlDbType.Int, 4, "estado_laboral"))
                .Item("@estado_laboral").Value = estado_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@municipio_laboral", System.Data.SqlDbType.Int, 4, "municipio_laboral"))
                .Item("@municipio_laboral").Value = municipio_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_laboral", System.Data.SqlDbType.Int, 4, "ciudad_laboral"))
                .Item("@ciudad_laboral").Value = ciudad_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_laboral", System.Data.SqlDbType.Int, 4, "colonia_laboral"))
                .Item("@colonia_laboral").Value = colonia_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@cp_laboral", System.Data.SqlDbType.Int, 4, "cp_laboral"))
                .Item("@cp_laboral").Value = cp_laboral

                .Add(New System.Data.SqlClient.SqlParameter("@area_laboral", System.Data.SqlDbType.VarChar, 40, "area_laboral"))
                .Item("@area_laboral").Value = area_laboral
                .Add(New System.Data.SqlClient.SqlParameter("@direccion_conyuge", System.Data.SqlDbType.VarChar, 50, "direccion_conyuge"))
                .Item("@direccion_conyuge").Value = direccion_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@email_conyuge", System.Data.SqlDbType.VarChar, 100, "email_conyuge"))
                .Item("@email_conyuge").Value = email_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_conyuge", System.Data.SqlDbType.VarChar, 13, "telefono_conyuge"))
                .Item("@telefono_conyuge").Value = telefono_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@direccion_trabajo_conyuge", System.Data.SqlDbType.VarChar, 50, "direccion_trabajo_conyuge"))
                .Item("@direccion_trabajo_conyuge").Value = direccion_trabajo_conyuge
                .Add(New System.Data.SqlClient.SqlParameter("@cp_aval", System.Data.SqlDbType.Int, 4, "cp_aval"))
                .Item("@cp_aval").Value = cp_aval
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_celular_aval", System.Data.SqlDbType.VarChar, 13, "telefono_celular_aval"))
                .Item("@telefono_celular_aval").Value = telefono_celular_aval
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_nextel_aval", System.Data.SqlDbType.VarChar, 13, "telefono_nextel_aval"))
                .Item("@telefono_nextel_aval").Value = telefono_nextel_aval
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_oficina_aval", System.Data.SqlDbType.VarChar, 13, "telefono_oficina_aval"))
                .Item("@telefono_oficina_aval").Value = telefono_oficina_aval
                .Add(New System.Data.SqlClient.SqlParameter("@extension_aval", System.Data.SqlDbType.Int, 4, "extension_aval"))
                .Item("@extension_aval").Value = extension_aval
                .Add(New System.Data.SqlClient.SqlParameter("@estado_civil_aval", System.Data.SqlDbType.Char, 1, "estado_civil_aval"))
                .Item("@estado_civil_aval").Value = estado_civil_aval
                .Add(New System.Data.SqlClient.SqlParameter("@email_aval", System.Data.SqlDbType.VarChar, 100, "email_aval"))
                .Item("@email_aval").Value = email_aval
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones_aval", System.Data.SqlDbType.Text, 0, "observaciones_aval"))
                .Item("@observaciones_aval").Value = observaciones_aval
                .Add(New System.Data.SqlClient.SqlParameter("@cp_referencia", System.Data.SqlDbType.Int, 4, "cp_referencia"))
                .Item("@cp_referencia").Value = cp_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_celular_referencia", System.Data.SqlDbType.VarChar, 13, "telefono_celular_referencia"))
                .Item("@telefono_celular_referencia").Value = telefono_celular_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_nextel_referencia", System.Data.SqlDbType.VarChar, 13, "telefono_nextel_referencia"))
                .Item("@telefono_nextel_referencia").Value = telefono_nextel_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_oficina_referencia", System.Data.SqlDbType.VarChar, 13, "telefono_oficina_referencia"))
                .Item("@telefono_oficina_referencia").Value = telefono_oficina_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@extension_referencia", System.Data.SqlDbType.Int, 4, "extension_referencia"))
                .Item("@extension_referencia").Value = extension_referencia
                .Add(New System.Data.SqlClient.SqlParameter("@otros_referencia", System.Data.SqlDbType.VarChar, 50, "otros_referencia"))
                .Item("@otros_referencia").Value = otros_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@email_referencia", System.Data.SqlDbType.VarChar, 100, "email_referencia"))
                .Item("@email_referencia").Value = email_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@nombre_referencia2", System.Data.SqlDbType.VarChar, 80, "nombre_referencia2"))
                .Item("@nombre_referencia2").Value = nombre_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_referencia2", System.Data.SqlDbType.VarChar, 50, "domicilio_referencia2"))
                .Item("@domicilio_referencia2").Value = domicilio_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_referencia2", System.Data.SqlDbType.VarChar, 50, "colonia_referencia2"))
                .Item("@colonia_referencia2").Value = colonia_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_referencia2", System.Data.SqlDbType.VarChar, 13, "telefono_referencia2"))
                .Item("@telefono_referencia2").Value = telefono_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_referencia2", System.Data.SqlDbType.VarChar, 60, "ciudad_referencia2"))
                .Item("@ciudad_referencia2").Value = ciudad_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@estado_referencia2", System.Data.SqlDbType.VarChar, 60, "estado_referencia2"))
                .Item("@estado_referencia2").Value = estado_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@cp_referencia2", System.Data.SqlDbType.Int, 4, "cp_referencia2"))
                .Item("@cp_referencia2").Value = cp_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_referencia2", System.Data.SqlDbType.VarChar, 50, "trabaja_en_referencia2"))
                .Item("@trabaja_en_referencia2").Value = trabaja_en_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_referencia2", System.Data.SqlDbType.VarChar, 50, "parentesco_referencia2"))
                .Item("@parentesco_referencia2").Value = parentesco_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_celular_referencia2", System.Data.SqlDbType.VarChar, 13, "telefono_celular_referencia2"))
                .Item("@telefono_celular_referencia2").Value = telefono_celular_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_nextel_referencia2", System.Data.SqlDbType.VarChar, 13, "telefono_nextel_referencia2"))
                .Item("@telefono_nextel_referencia2").Value = telefono_nextel_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_oficina_referencia2", System.Data.SqlDbType.VarChar, 13, "telefono_oficina_referencia2"))
                .Item("@telefono_oficina_referencia2").Value = telefono_oficina_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@extension_referencia2", System.Data.SqlDbType.Int, 4, "extension_referencia2"))
                .Item("@extension_referencia2").Value = extension_referencia2
                .Add(New System.Data.SqlClient.SqlParameter("@otros_referencia2", System.Data.SqlDbType.VarChar, 50, "otros_referencia2"))
                .Item("@otros_referencia2").Value = otros_referencia2

                .Add(New System.Data.SqlClient.SqlParameter("@email_referencia2", System.Data.SqlDbType.VarChar, 100, "email_referencia2"))
                .Item("@email_referencia2").Value = email_referencia2


                .Add(New System.Data.SqlClient.SqlParameter("@nombre_referencia3", System.Data.SqlDbType.VarChar, 80, "nombre_referencia3"))
                .Item("@nombre_referencia3").Value = nombre_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio_referencia3", System.Data.SqlDbType.VarChar, 50, "domicilio_referencia3"))
                .Item("@domicilio_referencia3").Value = domicilio_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@colonia_referencia3", System.Data.SqlDbType.VarChar, 50, "colonia_referencia3"))
                .Item("@colonia_referencia3").Value = colonia_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_referencia3", System.Data.SqlDbType.VarChar, 13, "telefono_referencia3"))
                .Item("@telefono_referencia3").Value = telefono_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad_referencia3", System.Data.SqlDbType.VarChar, 60, "ciudad_referencia3"))
                .Item("@ciudad_referencia3").Value = ciudad_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@estado_referencia3", System.Data.SqlDbType.VarChar, 60, "estado_referencia3"))
                .Item("@estado_referencia3").Value = estado_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@cp_referencia3", System.Data.SqlDbType.Int, 4, "cp_referencia3"))
                .Item("@cp_referencia3").Value = cp_referencia3

                .Add(New System.Data.SqlClient.SqlParameter("@trabaja_en_referencia3", System.Data.SqlDbType.VarChar, 50, "trabaja_en_referencia3"))
                .Item("@trabaja_en_referencia3").Value = trabaja_en_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco_referencia3", System.Data.SqlDbType.VarChar, 50, "parentesco_referencia3"))
                .Item("@parentesco_referencia3").Value = parentesco_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_celular_referencia3", System.Data.SqlDbType.VarChar, 13, "telefono_celular_referencia3"))
                .Item("@telefono_celular_referencia3").Value = telefono_celular_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_nextel_referencia3", System.Data.SqlDbType.VarChar, 13, "telefono_nextel_referencia3"))
                .Item("@telefono_nextel_referencia3").Value = telefono_nextel_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_oficina_referencia3", System.Data.SqlDbType.VarChar, 13, "telefono_oficina_referencia3"))
                .Item("@telefono_oficina_referencia3").Value = telefono_oficina_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@extension_referencia3", System.Data.SqlDbType.Int, 4, "extension_referencia3"))
                .Item("@extension_referencia3").Value = extension_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@otros_referencia3", System.Data.SqlDbType.VarChar, 50, "otros_referencia3"))
                .Item("@otros_referencia3").Value = otros_referencia3
                .Add(New System.Data.SqlClient.SqlParameter("@email_referencia3", System.Data.SqlDbType.VarChar, 100, "email_referencia3"))
                .Item("@email_referencia3").Value = email_referencia3

                .Add(New System.Data.SqlClient.SqlParameter("@persona_autoriza", System.Data.SqlDbType.VarChar, 25, "persona_autoriza"))
                .Item("@persona_autoriza").Value = persona_autoriza

                .Add(New System.Data.SqlClient.SqlParameter("@fecha_actualizacion", System.Data.SqlDbType.DateTime, 8, "fecha_actualizacion"))
                .Item("@fecha_actualizacion").Value = fecha_actualizacion


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

    Public Function RecalcularSaldosClientes(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[RecalcularSaldosClientes]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function recalcula_saldo_documentos(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[recalcula_saldo_documentos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_valida_cliente_convenio_MegaCred(ByRef cliente As Long, ByVal convenio As Long, ByVal clave_pago As String, ByVal no_empleado As String, ByVal rfc As String, ByVal importe_abono As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_valida_cliente_convenio_MegaCred]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Item("@cliente").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@clave_pago", System.Data.SqlDbType.VarChar, 50, "clave_pago"))
                .Item("@clave_pago").Value = clave_pago
                .Add(New System.Data.SqlClient.SqlParameter("@no_empleado", System.Data.SqlDbType.VarChar, 10, "no_empleado"))
                .Item("@no_empleado").Value = no_empleado
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = rfc
                .Add(New System.Data.SqlClient.SqlParameter("@importe_abono", System.Data.SqlDbType.Money, 8, "importe_abono"))
                .Item("@importe_abono").Value = importe_abono
            End With
            oEvent.Value = Connection.Execute(oCommand)
            cliente = oCommand.Parameters.Item("@cliente").Value
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


