'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCajerosFondos
'DATE:		22/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - CajerosFondos"
Public Class clsCajerosFondos
    Public Function sp_cajeros_fondos_ins(ByRef oData As DataSet, ByVal cajero As Long, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cajeros_fondos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha
                .Add(New System.Data.SqlClient.SqlParameter("@fondo", System.Data.SqlDbType.Money, 8, "fondo"))
                .Item("@fondo").Value = Connection.GetValue(oData, "fondo")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cambio", System.Data.SqlDbType.Money, 8, "tipo_cambio"))
                .Item("@tipo_cambio").Value = Connection.GetValue(oData, "tipo_cambio")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cajeros_fondos_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cajeros_fondos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = Connection.GetValue(oData, "cajero")
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = Connection.GetValue(oData, "fecha")
                .Add(New System.Data.SqlClient.SqlParameter("@fondo", System.Data.SqlDbType.Money, 8, "fondo"))
                .Item("@fondo").Value = Connection.GetValue(oData, "fondo")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_cambio", System.Data.SqlDbType.Money, 8, "tipo_cambio"))
                .Item("@tipo_cambio").Value = Connection.GetValue(oData, "tipo_cambio")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cajeros_fondos_del(ByVal cajero As Long, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cajeros_fondos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cajeros_fondos_exs(ByVal cajero As Long, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cajeros_fondos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cajeros_fondos_sel(ByVal cajero As Long, ByVal fecha As Date) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cajeros_fondos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cajero", System.Data.SqlDbType.Int, 4, "cajero"))
                .Item("@cajero").Value = cajero
                .Add(New System.Data.SqlClient.SqlParameter("@fecha", System.Data.SqlDbType.DateTime, 8, "fecha"))
                .Item("@fecha").Value = fecha

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_cajeros_fondos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cajeros_fondos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


