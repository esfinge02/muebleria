'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAutorizacionesVentasPedidoFabrica
'DATE:		30/08/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - AutorizacionesVentasPedidoFabrica"
Public Class clsAutorizacionesVentasPedidoFabrica
	Public Function sp_autorizaciones_ventas_pedido_fabrica_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_autorizaciones_ventas_pedido_fabrica_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@folio_autorizacion", System.Data.SqlDbType.Int, 4, "folio_autorizacion"))
				.Item("@folio_autorizacion").Value = Connection.GetValue(oData, "folio_autorizacion")
				.Add(New System.Data.SqlClient.SqlParameter("@fecha_autorizacion", System.Data.SqlDbType.Datetime, 8, "fecha_autorizacion"))
				.Item("@fecha_autorizacion").Value = Connection.GetValue(oData, "fecha_autorizacion")
				.Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
				.Item("@cliente").Value = Connection.GetValue(oData, "cliente")
				.Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
				.Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")
				.Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
				.Item("@articulo").Value = Connection.GetValue(oData, "articulo")
				.Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
				.Item("@costo").Value = Connection.GetValue(oData, "costo")
				.Add(New System.Data.SqlClient.SqlParameter("@utilizada", System.Data.SqlDbType.Bit, 1, "utilizada"))
				.Item("@utilizada").Value = Connection.GetValue(oData, "utilizada")
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
				.Item("@sucursal_factura").Value = Connection.GetValue(oData, "sucursal_factura")
				.Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
				.Item("@serie_factura").Value = Connection.GetValue(oData, "serie_factura")
				.Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")

                .Add(New System.Data.SqlClient.SqlParameter("@bodega_pedido_fabrica ", System.Data.SqlDbType.VarChar, 5, "bodega_pedido_fabrica "))
                .Item("@bodega_pedido_fabrica ").Value = Connection.GetValue(oData, "bodega_pedido_fabrica")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Int, 4, "cantidad"))
                .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")

				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_autorizaciones_ventas_pedido_fabrica_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_autorizaciones_ventas_pedido_fabrica_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@folio_autorizacion", System.Data.SqlDbType.Int, 4, "folio_autorizacion"))
				.Item("@folio_autorizacion").Value = Connection.GetValue(oData, "folio_autorizacion")
				.Add(New System.Data.SqlClient.SqlParameter("@fecha_autorizacion", System.Data.SqlDbType.Datetime, 8, "fecha_autorizacion"))
				.Item("@fecha_autorizacion").Value = Connection.GetValue(oData, "fecha_autorizacion")
				.Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
				.Item("@cliente").Value = Connection.GetValue(oData, "cliente")
				.Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
				.Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")
				.Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
				.Item("@articulo").Value = Connection.GetValue(oData, "articulo")
				.Add(New System.Data.SqlClient.SqlParameter("@costo", System.Data.SqlDbType.Money, 8, "costo"))
				.Item("@costo").Value = Connection.GetValue(oData, "costo")
				.Add(New System.Data.SqlClient.SqlParameter("@utilizada", System.Data.SqlDbType.Bit, 1, "utilizada"))
				.Item("@utilizada").Value = Connection.GetValue(oData, "utilizada")
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
				.Item("@sucursal_factura").Value = Connection.GetValue(oData, "sucursal_factura")
				.Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
				.Item("@serie_factura").Value = Connection.GetValue(oData, "serie_factura")
				.Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = Connection.GetValue(oData, "folio_factura")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_pedido_fabrica ", System.Data.SqlDbType.VarChar, 5, "bodega_pedido_fabrica "))
                .Item("@bodega_pedido_fabrica ").Value = Connection.GetValue(oData, "bodega_pedido_fabrica")
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Int, 4, "cantidad"))
                .Item("@cantidad").Value = Connection.GetValue(oData, "cantidad")

				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
    Public Function sp_autorizaciones_ventas_pedido_fabrica_del(ByVal folio_autorizacion As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_autorizaciones_ventas_pedido_fabrica_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_autorizacion", System.Data.SqlDbType.Int, 4, "folio_autorizacion"))
                .Item("@folio_autorizacion").Value = folio_autorizacion
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_autorizaciones_ventas_pedido_fabrica_exs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_autorizaciones_ventas_pedido_fabrica_exs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_autorizaciones_ventas_pedido_fabrica_sel(ByVal folio_autorizacion As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_autorizaciones_ventas_pedido_fabrica_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_autorizacion", System.Data.SqlDbType.Int, 4, "folio_autorizacion"))
                .Item("@folio_autorizacion").Value = folio_autorizacion
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_autorizaciones_ventas_pedido_fabrica_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_autorizaciones_ventas_pedido_fabrica_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_autoriza_ventas_pedido_fabrica_puntoventa_sel(ByVal folio_autorizacion As Long, ByVal cliente As Long, ByVal vendedor As Long, ByVal articulo As Long, ByVal cantidad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_autoriza_ventas_pedido_fabrica_puntoventa_sel]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@folio_autorizacion", System.Data.SqlDbType.Int, 4, "folio_autorizacion"))
                .Item("@folio_autorizacion").Value = folio_autorizacion
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Int, 4, "cantidad"))
                .Item("@cantidad").Value = cantidad

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_autorizaciones_ventas_pedido_fabrica_punto_venta_upd(ByVal folio_autorizacion As Long, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_autorizaciones_ventas_pedido_fabrica_punto_venta_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@folio_autorizacion", System.Data.SqlDbType.Int, 4, "folio_autorizacion"))
                .Item("@folio_autorizacion").Value = folio_autorizacion
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_factura", System.Data.SqlDbType.Int, 4, "sucursal_factura"))
                .Item("@sucursal_factura").Value = sucursal_factura
                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura", System.Data.SqlDbType.Char, 3, "serie_factura"))
                .Item("@serie_factura").Value = serie_factura
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura", System.Data.SqlDbType.Int, 4, "folio_factura"))
                .Item("@folio_factura").Value = folio_factura
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


