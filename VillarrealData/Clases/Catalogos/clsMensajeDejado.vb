Imports Dipros.Utils
'Imports Dipros.Utils.Common


Public Class clsMensajeDejado

    Public Function sp_mensaje_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_mensaje_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje", System.Data.SqlDbType.Int, 8, "mensaje"))
                .Item("@mensaje").Value = Connection.GetValue(oData, "mensaje")
                .Add(New System.Data.SqlClient.SqlParameter("@llamada", System.Data.SqlDbType.Int, 8, "llamada"))
                .Item("@llamada").Value = Connection.GetValue(oData, "llamada")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.NVarChar, 40, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_mensaje_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_mensaje_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje", System.Data.SqlDbType.Int, 8, "mensaje"))
                .Item("@mensaje").Value = Connection.GetValue(oData, "mensaje")
                .Add(New System.Data.SqlClient.SqlParameter("@llamada", System.Data.SqlDbType.Int, 8, "llamada"))
                .Item("@llamada").Value = Connection.GetValue(oData, "llamada")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.NVarChar, 40, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_mensaje_del(ByVal mensaje As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_mensaje_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje", System.Data.SqlDbType.Int, 4, "mensaje"))
                .Item("@mensaje").Value = mensaje

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_mensaje_sel(ByVal mensaje As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_mensaje_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje", System.Data.SqlDbType.Int, 4, "mensaje"))
                .Item("@mensaje").Value = mensaje

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_mensaje_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_mensaje_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_mensaje_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_mensaje_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_lkpmensaje_grl(ByVal tipo_llamada As Integer) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_lkpmensaje_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_llamada", System.Data.SqlDbType.Int, 4, "tipo_llamada"))
                .Item("@tipo_llamada").Value = tipo_llamada
                'ShowMessage(MessageType.MsgInformation, "tipo llamada en la clase " & tipo_llamada)
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function
End Class
