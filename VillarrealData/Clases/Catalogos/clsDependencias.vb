'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDependencias
'DATE:		16/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Dependencias"
Public Class clsDependencias
	Public Function sp_dependencias_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_dependencias_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
				.Item("@dependencia").Value = Connection.GetValue(oData, "dependencia")
				.Item("@dependencia").Direction = ParameterDirection.InputOutput
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
				.Add(New System.Data.SqlClient.SqlParameter("@concepto_deduccion", System.Data.SqlDbType.VarChar, 20, "concepto_deduccion"))
                .Item("@concepto_deduccion").Value = Connection.GetValue(oData, "concepto_deduccion")
                .Add(New System.Data.SqlClient.SqlParameter("@consulta", System.Data.SqlDbType.Text, 0, "consulta"))
                .Item("@consulta").Value = Connection.GetValue(oData, "consulta")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)
			Connection.SetValue(oData, "dependencia", oCommand.Parameters.Item("@dependencia").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_dependencias_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_dependencias_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
				.Item("@dependencia").Value = Connection.GetValue(oData, "dependencia")
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
				.Add(New System.Data.SqlClient.SqlParameter("@concepto_deduccion", System.Data.SqlDbType.VarChar, 20, "concepto_deduccion"))
                .Item("@concepto_deduccion").Value = Connection.GetValue(oData, "concepto_deduccion")
                .Add(New System.Data.SqlClient.SqlParameter("@consulta", System.Data.SqlDbType.Text, 0, "consulta"))
                .Item("@consulta").Value = Connection.GetValue(oData, "consulta")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_dependencias_del(ByVal dependencia as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_dependencias_del]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
				.Item("@dependencia").Value = dependencia

			End With
			Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
	
		Return oEvent
	End Function

	Public Function sp_dependencias_exs(ByVal dependencia as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_dependencias_exs]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
				.Item("@dependencia").Value = dependencia

			End With
			oEvent.Value = Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

	Public Function sp_dependencias_sel(ByVal dependencia as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_dependencias_sel]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
				.Item("@dependencia").Value = dependencia

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_dependencias_grs() As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_dependencias_grs]"
			With oCommand.Parameters

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

    Public Function sp_dependencias_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_dependencias_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


