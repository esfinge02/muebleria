'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsProveedores
'DATE:		01/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Proveedores"
Public Class clsProveedores
    Public Function sp_proveedores_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Item("@proveedor").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 30, "cuenta"))
                .Item("@cuenta").Value = Connection.GetValue(oData, "cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 30, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos", System.Data.SqlDbType.VarChar, 50, "telefonos"))
                .Item("@telefonos").Value = Connection.GetValue(oData, "telefonos")
                .Add(New System.Data.SqlClient.SqlParameter("@notas", System.Data.SqlDbType.Text, 0, "notas"))
                .Item("@notas").Value = Connection.GetValue(oData, "notas")
                .Add(New System.Data.SqlClient.SqlParameter("@contactos", System.Data.SqlDbType.VarChar, 100, "contactos"))
                .Item("@contactos").Value = Connection.GetValue(oData, "contactos")

                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = Connection.GetValue(oData, "saldo")

                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@persona", System.Data.SqlDbType.Char, 1, "persona"))
                .Item("@persona").Value = Connection.GetValue(oData, "persona")

                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = Connection.GetValue(oData, "tipo")

                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = Connection.GetValue(oData, "activo")

                .Add(New System.Data.SqlClient.SqlParameter("@Alias", System.Data.SqlDbType.VarChar, 10, "Alias"))
                .Item("@Alias").Value = Connection.GetValue(oData, "Alias")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "proveedor", oCommand.Parameters.Item("@proveedor").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.VarChar, 30, "cuenta"))
                .Item("@cuenta").Value = Connection.GetValue(oData, "cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = Connection.GetValue(oData, "rfc")
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = Connection.GetValue(oData, "domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 30, "colonia"))
                .Item("@colonia").Value = Connection.GetValue(oData, "colonia")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
                .Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = Connection.GetValue(oData, "cp")
                .Add(New System.Data.SqlClient.SqlParameter("@telefonos", System.Data.SqlDbType.VarChar, 50, "telefonos"))
                .Item("@telefonos").Value = Connection.GetValue(oData, "telefonos")
                .Add(New System.Data.SqlClient.SqlParameter("@notas", System.Data.SqlDbType.Text, 0, "notas"))
                .Item("@notas").Value = Connection.GetValue(oData, "notas")
                .Add(New System.Data.SqlClient.SqlParameter("@contactos", System.Data.SqlDbType.VarChar, 100, "contactos"))
                .Item("@contactos").Value = Connection.GetValue(oData, "contactos")

                .Add(New System.Data.SqlClient.SqlParameter("@saldo", System.Data.SqlDbType.Money, 8, "saldo"))
                .Item("@saldo").Value = Connection.GetValue(oData, "saldo")

                .Add(New System.Data.SqlClient.SqlParameter("@curp", System.Data.SqlDbType.VarChar, 20, "curp"))
                .Item("@curp").Value = Connection.GetValue(oData, "curp")
                .Add(New System.Data.SqlClient.SqlParameter("@persona", System.Data.SqlDbType.Char, 1, "persona"))
                .Item("@persona").Value = Connection.GetValue(oData, "persona")

                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = Connection.GetValue(oData, "tipo")

                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = Connection.GetValue(oData, "activo")

                .Add(New System.Data.SqlClient.SqlParameter("@Alias", System.Data.SqlDbType.VarChar, 10, "Alias"))
                .Item("@Alias").Value = Connection.GetValue(oData, "Alias")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_del(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_exs(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_sel(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function sp_proveedores_grl(ByVal utilizar_filtro As Boolean, ByVal activo As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = activo
                .Add(New System.Data.SqlClient.SqlParameter("@utilizar_filtro", System.Data.SqlDbType.Bit, 1, "utilizar_filtro"))
                .Item("@utilizar_filtro").Value = utilizar_filtro
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_existencias_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_existencias_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


