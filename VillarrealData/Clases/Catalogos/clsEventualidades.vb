'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEventualidades
'DATE:		03/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Eventualidades"
Public Class clsEventualidades
	Public Function sp_eventualidades_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_eventualidades_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
				.Item("@grupo").Value = Connection.GetValue(oData, "grupo")
				.Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
				.Item("@eventualidad").Value = Connection.GetValue(oData, "eventualidad")
				.Item("@eventualidad").Direction = ParameterDirection.InputOutput
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 100, "descripcion"))
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)
			Connection.SetValue(oData, "eventualidad", oCommand.Parameters.Item("@eventualidad").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_eventualidades_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_eventualidades_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
				.Item("@grupo").Value = Connection.GetValue(oData, "grupo")
				.Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
				.Item("@eventualidad").Value = Connection.GetValue(oData, "eventualidad")
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 100, "descripcion"))
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_eventualidades_del(ByVal eventualidad as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_eventualidades_del]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
				.Item("@eventualidad").Value = eventualidad

			End With
			Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
	
		Return oEvent
	End Function

	Public Function sp_eventualidades_exs(ByVal eventualidad as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_eventualidades_exs]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
				.Item("@eventualidad").Value = eventualidad

			End With
			oEvent.Value = Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

	Public Function sp_eventualidades_sel(ByVal eventualidad as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_eventualidades_sel]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@eventualidad", System.Data.SqlDbType.Int, 4, "eventualidad"))
				.Item("@eventualidad").Value = eventualidad

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_eventualidades_grs() As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_eventualidades_grs]"
			With oCommand.Parameters

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

    Public Function sp_eventualidades_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_eventualidades_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


