'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCamposCorrespondencia
'DATE:		31/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - CamposCorrespondencia"
Public Class clsCamposCorrespondencia
    Public Function sp_campos_correspondencia_ins(ByRef campo_correspondencia As String, ByVal nombre_tabla As String, ByVal campo_tabla As String, ByVal tipo_dato As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_campos_correspondencia_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@campo_correspondencia", System.Data.SqlDbType.VarChar, 50, "campo_correspondencia"))
                .Item("@campo_correspondencia").Value = campo_correspondencia
                .Item("@campo_correspondencia").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_tabla", System.Data.SqlDbType.VarChar, 50, "nombre_tabla"))
                .Item("@nombre_tabla").Value = nombre_tabla
                .Add(New System.Data.SqlClient.SqlParameter("@campo_tabla", System.Data.SqlDbType.VarChar, 50, "campo_tabla"))
                .Item("@campo_tabla").Value = campo_tabla
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_dato", System.Data.SqlDbType.Char, 1, "tipo_dato"))
                .Item("@tipo_dato").Value = tipo_dato
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            campo_correspondencia = oCommand.Parameters.Item("@campo_correspondencia").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_campos_correspondencia_upd(ByVal campo_correspondencia As String, ByVal nombre_tabla As String, ByVal campo_tabla As String, ByVal tipo_dato As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_campos_correspondencia_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@campo_correspondencia", System.Data.SqlDbType.VarChar, 50, "campo_correspondencia"))
                .Item("@campo_correspondencia").Value = campo_correspondencia
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_tabla", System.Data.SqlDbType.VarChar, 50, "nombre_tabla"))
                .Item("@nombre_tabla").Value = nombre_tabla
                .Add(New System.Data.SqlClient.SqlParameter("@campo_tabla", System.Data.SqlDbType.VarChar, 50, "campo_tabla"))
                .Item("@campo_tabla").Value = campo_tabla
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_dato", System.Data.SqlDbType.Char, 1, "tipo_dato"))
                .Item("@tipo_dato").Value = tipo_dato
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_campos_correspondencia_del(ByVal campo_correspondencia As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_campos_correspondencia_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@campo_correspondencia", System.Data.SqlDbType.VarChar, 50, "campo_correspondencia"))
                .Item("@campo_correspondencia").Value = campo_correspondencia

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_campos_correspondencia_exs(ByVal campo_correspondencia As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_campos_correspondencia_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@campo_correspondencia", System.Data.SqlDbType.VarChar, 50, "campo_correspondencia"))
                .Item("@campo_correspondencia").Value = campo_correspondencia

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_campos_correspondencia_sel(ByVal campo_correspondencia As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_campos_correspondencia_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@campo_correspondencia", System.Data.SqlDbType.VarChar, 50, "campo_correspondencia"))
                .Item("@campo_correspondencia").Value = campo_correspondencia

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_campos_correspondencia_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_campos_correspondencia_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


