'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsGruposArticulos
'DATE:		24/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - GruposArticulos"
Public Class clsGruposArticulos
    Public Function sp_grupos_articulos_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = Connection.GetValue(oData, "grupo")
                .Item("@grupo").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Connection.GetValue(oData, "departamento")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                '.Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Float, 8, "descuento"))
                '.Item("@descuento").Value = Connection.GetValue(oData, "descuento")
                '.Add(New System.Data.SqlClient.SqlParameter("@no_identificable", System.Data.SqlDbType.Bit, 1, "no_identificable"))
                '.Item("@no_identificable").Value = Connection.GetValue(oData, "no_identificable")
                .Add(New System.Data.SqlClient.SqlParameter("@factor_ganancia", System.Data.SqlDbType.Decimal, 9, "factor_ganancia"))
                .Item("@factor_ganancia").Value = Connection.GetValue(oData, "factor_ganancia")
                .Add(New System.Data.SqlClient.SqlParameter("@factor_ganancia_expo", System.Data.SqlDbType.Decimal, 9, "factor_ganancia_expo"))
                .Item("@factor_ganancia_expo").Value = Connection.GetValue(oData, "factor_ganancia_expo")
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "grupo", oCommand.Parameters.Item("@grupo").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_grupos_articulos_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = Connection.GetValue(oData, "grupo")
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Connection.GetValue(oData, "departamento")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                '.Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Float, 8, "descuento"))
                '.Item("@descuento").Value = Connection.GetValue(oData, "descuento")
                '.Add(New System.Data.SqlClient.SqlParameter("@no_identificable", System.Data.SqlDbType.Bit, 1, "no_identificable"))
                '.Item("@no_identificable").Value = Connection.GetValue(oData, "no_identificable")
                .Add(New System.Data.SqlClient.SqlParameter("@factor_ganancia", System.Data.SqlDbType.Decimal, 5, "factor_ganancia"))
                .Item("@factor_ganancia").Value = Connection.GetValue(oData, "factor_ganancia")
                .Add(New System.Data.SqlClient.SqlParameter("@factor_ganancia_expo", System.Data.SqlDbType.Decimal, 5, "factor_ganancia_expo"))
                .Item("@factor_ganancia_expo").Value = Connection.GetValue(oData, "factor_ganancia_expo")
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_grupos_articulos_del(ByVal grupo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_grupos_articulos_exs(ByVal grupo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_grupos_articulos_sel(ByVal grupo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_grupos_articulos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_grupos_articulos_grl(ByVal departamento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_grupos_articulos_fijo_grl(ByVal departamento As Long, ByVal grupo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_fijo_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_grupos_articulos_departamentos_grl(ByVal departamento As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_departamentos_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Text, 0, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = departamento
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_gruposarticulos_departamentosexistencias_grl(ByVal departamento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_gruposarticulos_departamentosexistencias_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = departamento
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_grupos_articulos_precios_sel(ByVal grupo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_grupos_articulos_precios_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_actualiza_precio_lista_grupos(ByVal grupo As Long, ByVal factor_ganancia As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_precio_lista_grupos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
                .Add(New System.Data.SqlClient.SqlParameter("@factor_ganancia", System.Data.SqlDbType.Decimal, 5, "factor_ganancia"))
                .Item("@factor_ganancia").Value = factor_ganancia
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_actualiza_precio_expo_grupo(ByVal grupo As Long, ByVal ganancia_expo As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_precio_expo]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
                .Add(New System.Data.SqlClient.SqlParameter("@ganancia_expo", System.Data.SqlDbType.Float, 8, "ganancia_expo"))
                .Item("@ganancia_expo").Value = ganancia_expo
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


