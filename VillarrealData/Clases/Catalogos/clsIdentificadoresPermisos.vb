Imports Dipros.Utils

Public Class clsIdentificadoresPermisos

    Public Function sp_identificadores_permisos_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_identificadores_permisos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = Connection.GetValue(oData, "sistema")

                .Add(New System.Data.SqlClient.SqlParameter("@identificador", System.Data.SqlDbType.VarChar, 25, "identificador"))
                .Item("@identificador").Value = Connection.GetValue(oData, "identificador")

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 30, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_identificadores_permisos_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_identificadores_permisos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = Connection.GetValue(oData, "sistema")

                .Add(New System.Data.SqlClient.SqlParameter("@identificador", System.Data.SqlDbType.VarChar, 25, "identificador"))
                .Item("@identificador").Value = Connection.GetValue(oData, "identificador")

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 30, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_identificadores_permisos_del(ByVal sistema As String, ByVal identificador As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_identificadores_permisos_del]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = sistema

                .Add(New System.Data.SqlClient.SqlParameter("@identificador", System.Data.SqlDbType.VarChar, 25, "identificador"))
                .Item("@identificador").Value = identificador

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_identificadores_permisos_exs(ByVal sistema As String, ByVal identificador As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_identificadores_permisos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = sistema

                .Add(New System.Data.SqlClient.SqlParameter("@identificador", System.Data.SqlDbType.VarChar, 25, "identificador"))
                .Item("@identificador").Value = identificador

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_identificadores_permisos_sel(ByVal sistema As String, ByVal identificador As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_identificadores_permisos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = sistema

                .Add(New System.Data.SqlClient.SqlParameter("@identificador", System.Data.SqlDbType.VarChar, 25, "identificador"))
                .Item("@identificador").Value = identificador

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_identificadores_permisos_grs(ByVal sistema As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_identificadores_permisos_grs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = sistema


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_permisos_extendidos_ins(ByVal usuario As String, ByVal sistema As String, ByVal identificador As String, ByVal permitir As Boolean, ByVal denegar As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_permisos_extendidos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = usuario

                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = sistema

                .Add(New System.Data.SqlClient.SqlParameter("@identificador", System.Data.SqlDbType.VarChar, 25, "identificador"))
                .Item("@identificador").Value = identificador

                .Add(New System.Data.SqlClient.SqlParameter("@permitir", System.Data.SqlDbType.Bit, 1, "permitir"))
                .Item("@permitir").Value = permitir

                .Add(New System.Data.SqlClient.SqlParameter("@denegar", System.Data.SqlDbType.Bit, 1, "denegar"))
                .Item("@denegar").Value = denegar

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_permisos_extendidos_grs(ByVal usuario As String, ByVal sistema As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_permisos_extendidos_grs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = sistema

                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = usuario

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_permisos_extendidos_usuario_grs(ByVal usuario As String, ByVal sistema As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_permisos_extendidos_usuario_grs]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sistema", System.Data.SqlDbType.VarChar, 10, "sistema"))
                .Item("@sistema").Value = sistema

                .Add(New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 15, "usuario"))
                .Item("@usuario").Value = usuario

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
