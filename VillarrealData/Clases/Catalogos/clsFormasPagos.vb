'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsFormasPagos
'DATE:		22/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils
Imports Dipros.Utils.Common

#Region "DIPROS Systems, DataEnvironment - FormasPagos"
Public Class clsFormasPagos
    Public Function sp_formas_pagos_ins(ByRef oData As DataSet, ByRef BitPrimerRegistro As Boolean, ByRef mensaje As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_formas_pagos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = Connection.GetValue(oData, "forma_pago")
                .Item("@forma_pago").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@comision", System.Data.SqlDbType.Decimal, 5, "comision"))
                .Item("@comision").Value = Connection.GetValue(oData, "comision")
                .Add(New System.Data.SqlClient.SqlParameter("@maneja_dolares", System.Data.SqlDbType.Bit, 1, "maneja_dolares"))
                .Item("@maneja_dolares").Value = Connection.GetValue(oData, "maneja_dolares")
                .Add(New System.Data.SqlClient.SqlParameter("@predeterminada", System.Data.SqlDbType.Bit, 1, "predeterminada"))
                .Item("@predeterminada").Value = Connection.GetValue(oData, "predeterminada")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@BitPrimerRegistro", System.Data.SqlDbType.Bit, 1, "BitPrimerRegistro"))
                .Item("@BitPrimerRegistro").Value = BitPrimerRegistro
                .Item("@BitPrimerRegistro").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje", System.Data.SqlDbType.Bit, 1, "mensaje"))
                .Item("@mensaje").Value = mensaje
                .Item("@mensaje").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@solicitar_ulitmos_digitos", System.Data.SqlDbType.Bit, 1, "solicitar_ulitmos_digitos"))
                .Item("@solicitar_ulitmos_digitos").Value = Connection.GetValue(oData, "solicitar_ulitmos_digitos")
            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "forma_pago", oCommand.Parameters.Item("@forma_pago").Value)
            BitPrimerRegistro = oCommand.Parameters.Item("@BitPrimerRegistro").Value
            mensaje = oCommand.Parameters.Item("@mensaje").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent ', BitPrimerRegistro,  mensaje)
      
    End Function

    Public Function sp_formas_pagos_upd(ByRef oData As DataSet, ByRef BitPrimerRegistro As Boolean, ByRef mensaje As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_formas_pagos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = Connection.GetValue(oData, "forma_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@comision", System.Data.SqlDbType.Decimal, 5, "comision"))
                .Item("@comision").Value = Connection.GetValue(oData, "comision")
                .Add(New System.Data.SqlClient.SqlParameter("@maneja_dolares", System.Data.SqlDbType.Bit, 1, "maneja_dolares"))
                .Item("@maneja_dolares").Value = Connection.GetValue(oData, "maneja_dolares")
                .Add(New System.Data.SqlClient.SqlParameter("@predeterminada", System.Data.SqlDbType.Bit, 1, "predeterminada"))
                .Item("@predeterminada").Value = Connection.GetValue(oData, "predeterminada")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.NVarChar, 30, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@BitPrimerRegistro", System.Data.SqlDbType.Bit, 1, "BitPrimerRegistro"))
                .Item("@BitPrimerRegistro").Value = BitPrimerRegistro
                .Item("@BitPrimerRegistro").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@mensaje", System.Data.SqlDbType.Bit, 1, "mensaje"))
                .Item("@mensaje").Value = mensaje
                .Item("@mensaje").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@solicitar_ulitmos_digitos", System.Data.SqlDbType.Bit, 1, "solicitar_ulitmos_digitos"))
                .Item("@solicitar_ulitmos_digitos").Value = Connection.GetValue(oData, "solicitar_ulitmos_digitos")

            End With
            Connection.Execute(oCommand)
            BitPrimerRegistro = oCommand.Parameters.Item("@BitPrimerRegistro").Value
            mensaje = oCommand.Parameters.Item("@mensaje").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_formas_pagos_del(ByVal forma_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_formas_pagos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_formas_pagos_exs(ByVal forma_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_formas_pagos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_formas_pagos_sel(ByVal forma_pago As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_formas_pagos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_formas_pagos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_formas_pagos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_formas_pagos_movimientos_caja() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_formas_pagos_movimientos_caja]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_formas_pagos_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_formas_pagos_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_formas_pagos_primer_registro(ByVal forma_pago As Integer, ByRef BitPrimerRegistro As Boolean) As Boolean
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Try
            oCommand.CommandText = "[sp_formas_pagos_primer_registro]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@forma_pago", System.Data.SqlDbType.Int, 4, "forma_pago"))
                .Item("@forma_pago").Value = forma_pago
                .Add(New System.Data.SqlClient.SqlParameter("@BitPrimerRegistro", System.Data.SqlDbType.Bit, 1, "BitPrimerRegistro"))
                .Item("@BitPrimerRegistro").Value = BitPrimerRegistro
                .Item("@BitPrimerRegistro").Direction = ParameterDirection.InputOutput

                '.Item("@forma_pago").Direction = ParameterDirection.InputOutput
                '.Add(New System.Data.SqlClient.SqlParameter("@predeterminada", System.Data.SqlDbType.Bit, 1, "predeterminada"))
                '.Item("@predeterminada").Value = Connection.GetValue(oData, "predeterminada")
            End With
            Connection.Execute(oCommand)
            BitPrimerRegistro = oCommand.Parameters.Item("@BitPrimerRegistro").Value

        Catch ex As Exception

        Finally
            oCommand = Nothing
        End Try

        Return BitPrimerRegistro

    End Function


End Class
#End Region


