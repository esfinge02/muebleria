'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsBancos
'DATE:		19/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Bancos"
Public Class clsBancos
    Public Function sp_bancos_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bancos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Smallint, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Item("@banco").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 30, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.VarChar, 30, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 35, "direccion"))
                .Item("@direccion").Value = Connection.GetValue(oData, "direccion")
                .Add(New System.Data.SqlClient.SqlParameter("@ejecutivo_cuenta", System.Data.SqlDbType.VarChar, 35, "ejecutivo_cuenta"))
                .Item("@ejecutivo_cuenta").Value = Connection.GetValue(oData, "ejecutivo_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@gerente", System.Data.SqlDbType.VarChar, 35, "gerente"))
                .Item("@gerente").Value = Connection.GetValue(oData, "gerente")
                .Add(New System.Data.SqlClient.SqlParameter("@tel1", System.Data.SqlDbType.VarChar, 20, "tel1"))
                .Item("@tel1").Value = Connection.GetValue(oData, "tel1")
                .Add(New System.Data.SqlClient.SqlParameter("@tel2", System.Data.SqlDbType.VarChar, 20, "tel2"))
                .Item("@tel2").Value = Connection.GetValue(oData, "tel2")
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 20, "fax"))
                .Item("@fax").Value = Connection.GetValue(oData, "fax")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.Int, 4, "cuenta"))
                .Item("@cuenta").Value = Connection.GetValue(oData, "cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "banco", oCommand.Parameters.Item("@banco").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bancos_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bancos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.Smallint, 2, "banco"))
                .Item("@banco").Value = Connection.GetValue(oData, "banco")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 30, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.VarChar, 30, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 35, "direccion"))
                .Item("@direccion").Value = Connection.GetValue(oData, "direccion")
                .Add(New System.Data.SqlClient.SqlParameter("@ejecutivo_cuenta", System.Data.SqlDbType.VarChar, 35, "ejecutivo_cuenta"))
                .Item("@ejecutivo_cuenta").Value = Connection.GetValue(oData, "ejecutivo_cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@gerente", System.Data.SqlDbType.VarChar, 35, "gerente"))
                .Item("@gerente").Value = Connection.GetValue(oData, "gerente")
                .Add(New System.Data.SqlClient.SqlParameter("@tel1", System.Data.SqlDbType.VarChar, 20, "tel1"))
                .Item("@tel1").Value = Connection.GetValue(oData, "tel1")
                .Add(New System.Data.SqlClient.SqlParameter("@tel2", System.Data.SqlDbType.VarChar, 20, "tel2"))
                .Item("@tel2").Value = Connection.GetValue(oData, "tel2")
                .Add(New System.Data.SqlClient.SqlParameter("@fax", System.Data.SqlDbType.VarChar, 20, "fax"))
                .Item("@fax").Value = Connection.GetValue(oData, "fax")
                .Add(New System.Data.SqlClient.SqlParameter("@cuenta", System.Data.SqlDbType.Int, 4, "cuenta"))
                .Item("@cuenta").Value = Connection.GetValue(oData, "cuenta")
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.Text, 0, "observaciones"))
                .Item("@observaciones").Value = Connection.GetValue(oData, "observaciones")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bancos_del(ByVal banco As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bancos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bancos_exs(ByVal banco As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bancos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bancos_sel(ByVal banco As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bancos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@banco", System.Data.SqlDbType.SmallInt, 2, "banco"))
                .Item("@banco").Value = banco

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bancos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bancos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bancos_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bancos_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


