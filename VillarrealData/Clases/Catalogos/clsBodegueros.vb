'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsBodegueros
'DATE:		23/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Bodegueros"
Public Class clsBodegueros
    Public Function sp_bodegueros_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bodegueros_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = Connection.GetValue(oData, "bodeguero")
                .Item("@bodeguero").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 60, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "bodeguero", oCommand.Parameters.Item("@bodeguero").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bodegueros_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bodegueros_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = Connection.GetValue(oData, "bodeguero")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 60, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bodegueros_del(ByVal bodeguero As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bodegueros_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = bodeguero

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bodegueros_exs(ByVal bodeguero As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bodegueros_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = bodeguero

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bodegueros_sel(ByVal bodeguero As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bodegueros_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodeguero", System.Data.SqlDbType.Int, 4, "bodeguero"))
                .Item("@bodeguero").Value = bodeguero

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bodegueros_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bodegueros_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_bodegueros_grl(ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_bodegueros_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_actual", System.Data.SqlDbType.Int, 4, "sucursal_actual"))
                .Item("@sucursal_actual").Value = sucursal_actual
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


