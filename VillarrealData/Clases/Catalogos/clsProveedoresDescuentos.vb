'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsProveedoresDescuentos
'DATE:		12/04/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - ProveedoresDescuentos"
Public Class clsProveedoresDescuentos
    Public Function sp_proveedores_descuentos_ins(ByRef oData As DataSet, ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_descuentos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
                .Item("@descuento").Value = Connection.GetValue(oData, "descuento")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje", System.Data.SqlDbType.Decimal, 9, "porcentaje"))
                .Item("@porcentaje").Value = Connection.GetValue(oData, "porcentaje")
                .Add(New System.Data.SqlClient.SqlParameter("@antes_iva", System.Data.SqlDbType.Bit, 1, "antes_iva"))
                .Item("@antes_iva").Value = Connection.GetValue(oData, "antes_iva")
                .Add(New System.Data.SqlClient.SqlParameter("@pronto_pago", System.Data.SqlDbType.Bit, 1, "pronto_pago"))
                .Item("@pronto_pago").Value = Connection.GetValue(oData, "pronto_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_descuentos_upd(ByRef oData As DataSet, ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_descuentos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
                .Item("@descuento").Value = Connection.GetValue(oData, "descuento")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 100, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje", System.Data.SqlDbType.Decimal, 9, "porcentaje"))
                .Item("@porcentaje").Value = Connection.GetValue(oData, "porcentaje")
                .Add(New System.Data.SqlClient.SqlParameter("@antes_iva", System.Data.SqlDbType.Bit, 1, "antes_iva"))
                .Item("@antes_iva").Value = Connection.GetValue(oData, "antes_iva")
                .Add(New System.Data.SqlClient.SqlParameter("@pronto_pago", System.Data.SqlDbType.Bit, 1, "pronto_pago"))
                .Item("@pronto_pago").Value = Connection.GetValue(oData, "pronto_pago")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_proveedores_descuentos_del(ByVal proveedor As Long, ByVal descuento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_descuentos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor
                .Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
                .Item("@descuento").Value = descuento

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_proveedores_descuentos_exs(ByVal proveedor As Long, ByVal descuento As Long) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_proveedores_descuentos_exs]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
    '            .Item("@proveedor").Value = proveedor
    '            .Add(New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Int, 4, "descuento"))
    '            .Item("@descuento").Value = descuento

    '        End With
    '        oEvent.Value = Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_proveedores_descuentos_sel(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_descuentos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_proveedores_descuentos_para_insercion_de_entradas_sel(ByVal proveedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_proveedores_descuentos_para_insercion_de_entradas_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = proveedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


   


End Class
#End Region


