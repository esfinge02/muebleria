'DESCRIPTION:	clsTiposparentesco
'DATE:		10/01/2014 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Tiposparentesco"

Public Class clsTipoParentesco

    Public Function sp_tipo_parentesco_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_parentesco_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco", System.Data.SqlDbType.Int, 8, "parentesco"))
                .Item("@parentesco").Value = Connection.GetValue(oData, "parentesco")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.NVarChar, 40, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "parentesco", oCommand.Parameters.Item("@parentesco").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_parentesco_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_parentesco_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco", System.Data.SqlDbType.Int, 8, "parentesco"))
                .Item("@parentesco").Value = Connection.GetValue(oData, "parentesco")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.NVarChar, 40, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_parentesco_del(ByVal parentesco As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_parentesco_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco", System.Data.SqlDbType.Int, 4, "parentesco"))
                .Item("@parentesco").Value = parentesco

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_parentesco_exs(ByVal parentesco As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tipos_parentesco_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco", System.Data.SqlDbType.Int, 4, "parentesco"))
                .Item("@parentesco").Value = parentesco

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_parentesco_sel(ByVal parentesco As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_parentesco_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@parentesco", System.Data.SqlDbType.Int, 4, "parentesco"))
                .Item("@parentesco").Value = parentesco

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_parentesco_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_parentesco_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tipos_parentesco_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_parentesco_grl]"
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region

