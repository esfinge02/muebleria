'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEtiquetas
'DATE:		19/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Etiquetas"
Public Class clsEtiquetas
	Public Function sp_etiquetas_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_etiquetas_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@desde", System.Data.SqlDbType.Decimal, 9, "desde"))
				.Item("@desde").Value = Connection.GetValue(oData, "desde")
				.Add(New System.Data.SqlClient.SqlParameter("@hasta", System.Data.SqlDbType.Decimal, 9, "hasta"))
				.Item("@hasta").Value = Connection.GetValue(oData, "hasta")
				.Add(New System.Data.SqlClient.SqlParameter("@plan1", System.Data.SqlDbType.Int, 4, "plan1"))
				.Item("@plan1").Value = Connection.GetValue(oData, "plan1")
				.Add(New System.Data.SqlClient.SqlParameter("@plan2", System.Data.SqlDbType.Int, 4, "plan2"))
				.Item("@plan2").Value = Connection.GetValue(oData, "plan2")
				.Add(New System.Data.SqlClient.SqlParameter("@plan3", System.Data.SqlDbType.Int, 4, "plan3"))
				.Item("@plan3").Value = Connection.GetValue(oData, "plan3")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

    Public Function sp_etiquetas_ins(ByVal desde As Double, ByVal hasta As Double, ByVal plan1 As Long, ByVal plan2 As Long, ByVal plan3 As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_etiquetas_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@desde", System.Data.SqlDbType.Decimal, 9, "desde"))
                .Item("@desde").Value = desde
                .Add(New System.Data.SqlClient.SqlParameter("@hasta", System.Data.SqlDbType.Decimal, 9, "hasta"))
                .Item("@hasta").Value = hasta
                .Add(New System.Data.SqlClient.SqlParameter("@plan1", System.Data.SqlDbType.Int, 4, "plan1"))
                .Item("@plan1").Value = plan1
                .Add(New System.Data.SqlClient.SqlParameter("@plan2", System.Data.SqlDbType.Int, 4, "plan2"))
                .Item("@plan2").Value = plan2
                .Add(New System.Data.SqlClient.SqlParameter("@plan3", System.Data.SqlDbType.Int, 4, "plan3"))
                .Item("@plan3").Value = plan3
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_etiquetas_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_etiquetas_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@desde", System.Data.SqlDbType.Decimal, 9, "desde"))
                .Item("@desde").Value = Connection.GetValue(oData, "desde")
                .Add(New System.Data.SqlClient.SqlParameter("@hasta", System.Data.SqlDbType.Decimal, 9, "hasta"))
                .Item("@hasta").Value = Connection.GetValue(oData, "hasta")
                .Add(New System.Data.SqlClient.SqlParameter("@plan1", System.Data.SqlDbType.Int, 4, "plan1"))
                .Item("@plan1").Value = Connection.GetValue(oData, "plan1")
                .Add(New System.Data.SqlClient.SqlParameter("@plan2", System.Data.SqlDbType.Int, 4, "plan2"))
                .Item("@plan2").Value = Connection.GetValue(oData, "plan2")
                .Add(New System.Data.SqlClient.SqlParameter("@plan3", System.Data.SqlDbType.Int, 4, "plan3"))
                .Item("@plan3").Value = Connection.GetValue(oData, "plan3")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_etiquetas_del() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_etiquetas_del]"
            With oCommand.Parameters

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_etiquetas_exs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_etiquetas_exs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_etiquetas_sel() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_etiquetas_sel]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_etiquetas_grs(ByVal numero_filas As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_etiquetas_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@numero_filas", System.Data.SqlDbType.Int, 4, "numero_filas"))
                .Item("@numero_filas").Value = numero_filas

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


