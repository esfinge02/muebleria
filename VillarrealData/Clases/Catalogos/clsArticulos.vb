'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsArticulos
'DATE:		01/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Articulos"
Public Class clsArticulos

    Public Function sp_articulos_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Item("@articulo").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@codigo_barras", System.Data.SqlDbType.VarChar, 20, "codigo_barras"))
                .Item("@codigo_barras").Value = Connection.GetValue(oData, "codigo_barras")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_corta", System.Data.SqlDbType.VarChar, 50, "descripcion_corta"))
                .Item("@descripcion_corta").Value = Connection.GetValue(oData, "descripcion_corta")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 5000, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Connection.GetValue(oData, "departamento")
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = Connection.GetValue(oData, "grupo")
                .Add(New System.Data.SqlClient.SqlParameter("@unidad", System.Data.SqlDbType.VarChar, 3, "unidad"))
                .Item("@unidad").Value = Connection.GetValue(oData, "unidad")
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@ultimo_costo_sin_flete", System.Data.SqlDbType.Money, 8, "ultimo_costo_sin_flete"))
                .Item("@ultimo_costo_sin_flete").Value = Connection.GetValue(oData, "ultimo_costo_sin_flete")
                .Add(New System.Data.SqlClient.SqlParameter("@ultimo_costo", System.Data.SqlDbType.Money, 8, "ultimo_costo"))
                .Item("@ultimo_costo").Value = Connection.GetValue(oData, "ultimo_costo")
                .Add(New System.Data.SqlClient.SqlParameter("@ultima_compra", System.Data.SqlDbType.Datetime, 8, "ultima_compra"))
                .Item("@ultima_compra").Value = Connection.GetValue(oData, "ultima_compra")
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Decimal, 9, "impuesto"))
                .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto_suntuario", System.Data.SqlDbType.Decimal, 9, "impuesto_suntuario"))
                .Item("@impuesto_suntuario").Value = Connection.GetValue(oData, "impuesto_suntuario")
                .Add(New System.Data.SqlClient.SqlParameter("@importacion", System.Data.SqlDbType.Bit, 1, "importacion"))
                .Item("@importacion").Value = Connection.GetValue(oData, "importacion")
                .Add(New System.Data.SqlClient.SqlParameter("@descontinuado", System.Data.SqlDbType.Bit, 1, "descontinuado"))
                .Item("@descontinuado").Value = Connection.GetValue(oData, "descontinuado")
                .Add(New System.Data.SqlClient.SqlParameter("@maneja_series", System.Data.SqlDbType.Bit, 1, "maneja_series"))
                .Item("@maneja_series").Value = Connection.GetValue(oData, "maneja_series")
                .Add(New System.Data.SqlClient.SqlParameter("@entrega_domicilio", System.Data.SqlDbType.Bit, 1, "entrega_domicilio"))
                .Item("@entrega_domicilio").Value = Connection.GetValue(oData, "entrega_domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@precio_lista", System.Data.SqlDbType.Money, 8, "precio_lista"))
                .Item("@precio_lista").Value = Connection.GetValue(oData, "precio_lista")
                .Add(New System.Data.SqlClient.SqlParameter("@cajas", System.Data.SqlDbType.Int, 4, "cajas"))
                .Item("@cajas").Value = Connection.GetValue(oData, "cajas")
                .Add(New System.Data.SqlClient.SqlParameter("@no_resurtir", System.Data.SqlDbType.Bit, 1, "no_resurtir"))
                .Item("@no_resurtir").Value = Connection.GetValue(oData, "no_resurtir")
                .Add(New System.Data.SqlClient.SqlParameter("@requiere_armado", System.Data.SqlDbType.Bit, 1, "requiere_armado"))
                .Item("@requiere_armado").Value = Connection.GetValue(oData, "requiere_armado")
                .Add(New System.Data.SqlClient.SqlParameter("@requiere_instalacion", System.Data.SqlDbType.Bit, 1, "requiere_instalacion"))
                .Item("@requiere_instalacion").Value = Connection.GetValue(oData, "requiere_instalacion")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo_regalo", System.Data.SqlDbType.Bit, 1, "articulo_regalo"))
                .Item("@articulo_regalo").Value = Connection.GetValue(oData, "articulo_regalo")
                .Add(New System.Data.SqlClient.SqlParameter("@meses_garantia", System.Data.SqlDbType.Int, 4, "meses_garantia"))
                .Item("@meses_garantia").Value = Connection.GetValue(oData, "meses_garantia")
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio_etiqueta_precios", System.Data.SqlDbType.VarChar, 30, "tamanio_etiqueta_precios"))
                .Item("@tamanio_etiqueta_precios").Value = Connection.GetValue(oData, "tamanio_etiqueta_precios")
                .Add(New System.Data.SqlClient.SqlParameter("@capacidad", System.Data.SqlDbType.VarChar, 6, "capacidad"))
                .Item("@capacidad").Value = Connection.GetValue(oData, "capacidad")
                .Add(New System.Data.SqlClient.SqlParameter("@modelo", System.Data.SqlDbType.VarChar, 18, "modelo"))
                .Item("@modelo").Value = Connection.GetValue(oData, "modelo")
                .Add(New System.Data.SqlClient.SqlParameter("@stock_minimo", System.Data.SqlDbType.Int, 4, "stock_minimo"))
                .Item("@stock_minimo").Value = Connection.GetValue(oData, "stock_minimo")
                .Add(New System.Data.SqlClient.SqlParameter("@stock_maximo", System.Data.SqlDbType.Int, 4, "stock_maximo"))
                .Item("@stock_maximo").Value = Connection.GetValue(oData, "stock_maximo")
                .Add(New System.Data.SqlClient.SqlParameter("@ligero_uso", System.Data.SqlDbType.Bit, 1, "ligero_uso"))
                .Item("@ligero_uso").Value = Connection.GetValue(oData, "ligero_uso")
                .Add(New System.Data.SqlClient.SqlParameter("@pedido_fabrica", System.Data.SqlDbType.Bit, 1, "pedido_fabrica"))
                .Item("@pedido_fabrica").Value = Connection.GetValue(oData, "pedido_fabrica")
                .Add(New System.Data.SqlClient.SqlParameter("@costo_pedido_fabrica", System.Data.SqlDbType.Money, 8, "costo_pedido_fabrica"))
                .Item("@costo_pedido_fabrica").Value = Connection.GetValue(oData, "costo_pedido_fabrica")

                .Add(New System.Data.SqlClient.SqlParameter("@bodega_pedido_fabrica", System.Data.SqlDbType.VarChar, 5, "bodega_pedido_fabrica"))
                .Item("@bodega_pedido_fabrica").Value = Connection.GetValue(oData, "bodega_pedido_fabrica")

                .Add(New System.Data.SqlClient.SqlParameter("@marca", System.Data.SqlDbType.VarChar, 25, "marca"))
                .Item("@marca").Value = Connection.GetValue(oData, "marca")
                .Add(New System.Data.SqlClient.SqlParameter("@clasificacion", System.Data.SqlDbType.VarChar, 25, "clasificacion"))
                .Item("@clasificacion").Value = Connection.GetValue(oData, "clasificacion")

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_especial", System.Data.SqlDbType.VarChar, 8000, "descripcion_especial"))
                .Item("@descripcion_especial").Value = Connection.GetValue(oData, "descripcion_especial")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "articulo", oCommand.Parameters.Item("@articulo").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = Connection.GetValue(oData, "articulo")
                .Add(New System.Data.SqlClient.SqlParameter("@codigo_barras", System.Data.SqlDbType.VarChar, 20, "codigo_barras"))
                .Item("@codigo_barras").Value = Connection.GetValue(oData, "codigo_barras")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_corta", System.Data.SqlDbType.VarChar, 50, "descripcion_corta"))
                .Item("@descripcion_corta").Value = Connection.GetValue(oData, "descripcion_corta")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 5000, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Connection.GetValue(oData, "departamento")
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = Connection.GetValue(oData, "grupo")
                .Add(New System.Data.SqlClient.SqlParameter("@unidad", System.Data.SqlDbType.VarChar, 3, "unidad"))
                .Item("@unidad").Value = Connection.GetValue(oData, "unidad")
                .Add(New System.Data.SqlClient.SqlParameter("@proveedor", System.Data.SqlDbType.Int, 4, "proveedor"))
                .Item("@proveedor").Value = Connection.GetValue(oData, "proveedor")
                .Add(New System.Data.SqlClient.SqlParameter("@ultimo_costo_sin_flete", System.Data.SqlDbType.Money, 8, "ultimo_costo_sin_flete"))
                .Item("@ultimo_costo_sin_flete").Value = Connection.GetValue(oData, "ultimo_costo_sin_flete")
                .Add(New System.Data.SqlClient.SqlParameter("@ultimo_costo", System.Data.SqlDbType.Money, 8, "ultimo_costo"))
                .Item("@ultimo_costo").Value = Connection.GetValue(oData, "ultimo_costo")
                .Add(New System.Data.SqlClient.SqlParameter("@ultima_compra", System.Data.SqlDbType.Datetime, 8, "ultima_compra"))
                .Item("@ultima_compra").Value = Connection.GetValue(oData, "ultima_compra")
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Decimal, 9, "impuesto"))
                .Item("@impuesto").Value = Connection.GetValue(oData, "impuesto")
                .Add(New System.Data.SqlClient.SqlParameter("@impuesto_suntuario", System.Data.SqlDbType.Decimal, 9, "impuesto_suntuario"))
                .Item("@impuesto_suntuario").Value = Connection.GetValue(oData, "impuesto_suntuario")
                .Add(New System.Data.SqlClient.SqlParameter("@importacion", System.Data.SqlDbType.Bit, 1, "importacion"))
                .Item("@importacion").Value = Connection.GetValue(oData, "importacion")
                .Add(New System.Data.SqlClient.SqlParameter("@descontinuado", System.Data.SqlDbType.Bit, 1, "descontinuado"))
                .Item("@descontinuado").Value = Connection.GetValue(oData, "descontinuado")
                .Add(New System.Data.SqlClient.SqlParameter("@maneja_series", System.Data.SqlDbType.Bit, 1, "maneja_series"))
                .Item("@maneja_series").Value = Connection.GetValue(oData, "maneja_series")
                .Add(New System.Data.SqlClient.SqlParameter("@entrega_domicilio", System.Data.SqlDbType.Bit, 1, "entrega_domicilio"))
                .Item("@entrega_domicilio").Value = Connection.GetValue(oData, "entrega_domicilio")
                .Add(New System.Data.SqlClient.SqlParameter("@precio_lista", System.Data.SqlDbType.Money, 8, "precio_lista"))
                .Item("@precio_lista").Value = Connection.GetValue(oData, "precio_lista")
                .Add(New System.Data.SqlClient.SqlParameter("@cajas", System.Data.SqlDbType.Int, 4, "cajas"))
                .Item("@cajas").Value = Connection.GetValue(oData, "cajas")
                .Add(New System.Data.SqlClient.SqlParameter("@no_resurtir", System.Data.SqlDbType.Bit, 1, "no_resurtir"))
                .Item("@no_resurtir").Value = Connection.GetValue(oData, "no_resurtir")
                .Add(New System.Data.SqlClient.SqlParameter("@requiere_armado", System.Data.SqlDbType.Bit, 1, "requiere_armado"))
                .Item("@requiere_armado").Value = Connection.GetValue(oData, "requiere_armado")
                .Add(New System.Data.SqlClient.SqlParameter("@requiere_instalacion", System.Data.SqlDbType.Bit, 1, "requiere_instalacion"))
                .Item("@requiere_instalacion").Value = Connection.GetValue(oData, "requiere_instalacion")
                .Add(New System.Data.SqlClient.SqlParameter("@articulo_regalo", System.Data.SqlDbType.Bit, 1, "articulo_regalo"))
                .Item("@articulo_regalo").Value = Connection.GetValue(oData, "articulo_regalo")
                .Add(New System.Data.SqlClient.SqlParameter("@meses_garantia", System.Data.SqlDbType.Int, 4, "meses_garantia"))
                .Item("@meses_garantia").Value = Connection.GetValue(oData, "meses_garantia")
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio_etiqueta_precios", System.Data.SqlDbType.VarChar, 30, "tamanio_etiqueta_precios"))
                .Item("@tamanio_etiqueta_precios").Value = Connection.GetValue(oData, "tamanio_etiqueta_precios")
                .Add(New System.Data.SqlClient.SqlParameter("@capacidad", System.Data.SqlDbType.VarChar, 6, "capacidad"))
                .Item("@capacidad").Value = Connection.GetValue(oData, "capacidad")
                .Add(New System.Data.SqlClient.SqlParameter("@modelo", System.Data.SqlDbType.VarChar, 18, "modelo"))
                .Item("@modelo").Value = Connection.GetValue(oData, "modelo")
                .Add(New System.Data.SqlClient.SqlParameter("@stock_minimo", System.Data.SqlDbType.Int, 4, "stock_minimo"))
                .Item("@stock_minimo").Value = Connection.GetValue(oData, "stock_minimo")
                .Add(New System.Data.SqlClient.SqlParameter("@stock_maximo", System.Data.SqlDbType.Int, 4, "stock_maximo"))
                .Item("@stock_maximo").Value = Connection.GetValue(oData, "stock_maximo")

                .Add(New System.Data.SqlClient.SqlParameter("@ligero_uso", System.Data.SqlDbType.Bit, 1, "ligero_uso"))
                .Item("@ligero_uso").Value = Connection.GetValue(oData, "ligero_uso")

                .Add(New System.Data.SqlClient.SqlParameter("@pedido_fabrica", System.Data.SqlDbType.Bit, 1, "pedido_fabrica"))
                .Item("@pedido_fabrica").Value = Connection.GetValue(oData, "pedido_fabrica")
                .Add(New System.Data.SqlClient.SqlParameter("@costo_pedido_fabrica", System.Data.SqlDbType.Money, 8, "costo_pedido_fabrica"))
                .Item("@costo_pedido_fabrica").Value = Connection.GetValue(oData, "costo_pedido_fabrica")
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_pedido_fabrica", System.Data.SqlDbType.VarChar, 5, "bodega_pedido_fabrica"))
                .Item("@bodega_pedido_fabrica").Value = Connection.GetValue(oData, "bodega_pedido_fabrica")

                .Add(New System.Data.SqlClient.SqlParameter("@marca", System.Data.SqlDbType.VarChar, 25, "marca"))
                .Item("@marca").Value = Connection.GetValue(oData, "marca")
                .Add(New System.Data.SqlClient.SqlParameter("@clasificacion", System.Data.SqlDbType.VarChar, 25, "clasificacion"))
                .Item("@clasificacion").Value = Connection.GetValue(oData, "clasificacion")

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion_especial", System.Data.SqlDbType.VarChar, 8000, "descripcion_especial"))
                .Item("@descripcion_especial").Value = Connection.GetValue(oData, "descripcion_especial")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_del(ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_exs(ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_articulos_sel(ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_grl(ByVal departamento As Long, ByVal grupo As Long, ByVal no_resurtir As Long, ByVal cadena As String, ByVal articulo As Long, ByVal articulo_regalo As Long, ByVal ligero_uso As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
                .Add(New System.Data.SqlClient.SqlParameter("@no_resurtir", System.Data.SqlDbType.Int, 4, "no_resurtir"))
                .Item("@no_resurtir").Value = no_resurtir
                .Add(New System.Data.SqlClient.SqlParameter("@cadena", System.Data.SqlDbType.VarChar, 100, "cadena"))
                .Item("@cadena").Value = cadena
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@articulo_regalo", System.Data.SqlDbType.Int, 4, "articulo_regalo"))
                .Item("@articulo_regalo").Value = articulo_regalo
                .Add(New System.Data.SqlClient.SqlParameter("@ligero_uso", System.Data.SqlDbType.Int, 4, "ligero_uso"))
                .Item("@ligero_uso").Value = ligero_uso
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_ligero_uso_grl(ByVal departamento As Long, ByVal grupo As Long, ByVal ligero_uso As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_ligero_uso_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
                '.Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                '.Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@ligero_uso", System.Data.SqlDbType.Bit, 1, "ligero_uso"))
                .Item("@ligero_uso").Value = ligero_uso
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_factura_grl(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_factura_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@serie", System.Data.SqlDbType.Char, 3, "serie"))
                .Item("@serie").Value = serie
                .Add(New System.Data.SqlClient.SqlParameter("@folio", System.Data.SqlDbType.Int, 4, "folio"))
                .Item("@folio").Value = folio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_actualiza_pedimento_articulos_upd(ByVal articulo As Long, ByVal pedimento As String, ByVal aduana As String, ByVal fecha_importacion As Object) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_pedimento_articulos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@pedimento", System.Data.SqlDbType.VarChar, 30, "pedimento"))
                .Item("@pedimento").Value = pedimento
                .Add(New System.Data.SqlClient.SqlParameter("@aduana", System.Data.SqlDbType.VarChar, 50, "aduana"))
                .Item("@aduana").Value = aduana
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_importacion", System.Data.SqlDbType.DateTime, 8, "fecha_importacion"))
                .Item("@fecha_importacion").Value = fecha_importacion
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_articulos_departamentos_grupos_grl(ByVal departamento As String, ByVal grupo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_departamentos_grupos_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Text, 0, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Text, 0, "GRUPO"))
                .Item("@GRUPO").Value = grupo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_departamentos_grupos_movtos_grl(ByVal departamento As Long, ByVal grupo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_departamentos_grupos_movtos_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@DEPARTAMENTO", System.Data.SqlDbType.Int, 4, "DEPARTAMENTO"))
                .Item("@DEPARTAMENTO").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@GRUPO", System.Data.SqlDbType.Int, 4, "GRUPO"))
                .Item("@GRUPO").Value = grupo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_ubicaciones_grl(ByVal ubicacion As String, ByVal bodega As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_ubicaciones_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@ubicacion", System.Data.SqlDbType.Text, 0, "ubicacion"))
                .Item("@ubicacion").Value = ubicacion
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    Public Function sp_articulos_folio() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_folio]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_precioventa(ByVal articulo As Long, ByVal precio As Long, ByVal sucursal_dependencia As Boolean, ByVal fechapedido As DateTime, ByRef precio_venta As Double, ByVal TienePlan As Boolean) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_precioventa]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
                .Item("@precio").Value = precio
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Bit, 1, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = sucursal_dependencia
                .Add(New System.Data.SqlClient.SqlParameter("@fechapedido", System.Data.SqlDbType.DateTime, 8, "fechapedido"))
                .Item("@fechapedido").Value = fechapedido
                .Add(New System.Data.SqlClient.SqlParameter("@TienePlan", System.Data.SqlDbType.Bit, 1, "TienePlan"))
                .Item("@TienePlan").Value = TienePlan

                .Add(New System.Data.SqlClient.SqlParameter("@precio_venta", System.Data.SqlDbType.Money, 8, "precio_venta"))
                .Item("@precio_venta").Value = precio_venta
                .Item("@precio_venta").Direction = ParameterDirection.InputOutput
            End With
            oEvent.Value = Connection.Execute(oCommand)
            precio_venta = oCommand.Parameters.Item("@precio_venta").Value
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try
        Return oEvent
    End Function

    Public Function sp_articulos_actualiza_ultimo_costo_fecha(ByVal articulo As Long, ByVal fecha_ultima_compra As String, ByVal ultimo_costo As Double, ByVal ultimo_costo_sin_flete As Double, ByVal precio_lista As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_actualiza_ultimo_costo_fecha]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ultima_compra", System.Data.SqlDbType.DateTime, 8, "fecha_ultima_compra"))
                .Item("@fecha_ultima_compra").Value = fecha_ultima_compra
                .Add(New System.Data.SqlClient.SqlParameter("@ultimo_costo", System.Data.SqlDbType.Money, 8, "ultimo_costo"))
                .Item("@ultimo_costo").Value = ultimo_costo
                .Add(New System.Data.SqlClient.SqlParameter("@ultimo_costo_sin_flete", System.Data.SqlDbType.Money, 8, "ultimo_costo_sin_flete"))
                .Item("@ultimo_costo_sin_flete").Value = ultimo_costo_sin_flete
                .Add(New System.Data.SqlClient.SqlParameter("@precio_lista", System.Data.SqlDbType.Money, 8, "precio_lista"))
                .Item("@precio_lista").Value = precio_lista
                '.Item("@precio_lista").Direction = ParameterDirection.InputOutput

            End With
            Connection.Execute(oCommand)
            'precio_lista = oCommand.Parameters.Item("@precio_lista").Value

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_estadisticas(ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_estadisticas]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_articulos_movimientos_inventario_grs(ByVal articulo As Long, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_movimientos_inventario_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_series_grs(ByVal articulo As Long, ByVal tipo_movimiento As Char, ByVal bodega As String, ByVal entrada As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_series_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_movimiento", System.Data.SqlDbType.Char, 1, "tipo_movimiento"))
                .Item("@tipo_movimiento").Value = tipo_movimiento
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@entrada", System.Data.SqlDbType.Int, 4, "entrada"))
                .Item("@entrada").Value = entrada


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    '@ACH-27/06/07: Se agregó un método para validar si existe una registro con Departamento-Grupo-Modelo
    Public Function sp_articulos_modelo_exs(ByVal departamento As Long, ByVal grupo As Long, ByVal modelo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_modelo_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
                .Add(New System.Data.SqlClient.SqlParameter("@grupo", System.Data.SqlDbType.Int, 4, "grupo"))
                .Item("@grupo").Value = grupo
                .Add(New System.Data.SqlClient.SqlParameter("@modelo", System.Data.SqlDbType.VarChar, 18, "modelo"))
                .Item("@modelo").Value = modelo
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
    '@ACH-27/06/07

    Public Function sp_articulos_ObtenerOC_Bodega_sel(ByVal articulo As Long, ByVal bodega As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_ObtenerOC_Bodega_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

    Public Function sp_articulos_valida_eliminar(ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_valida_eliminar]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_actualiza_costos_articulo_movimiento(ByVal articulo As Long, ByVal costo_movimiento As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_costos_articulo_movimiento]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@costo_movimiento", System.Data.SqlDbType.Money, 8, "costo_movimiento"))
                .Item("@costo_movimiento").Value = costo_movimiento

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function sp_obtener_bodega_pedido_fabrica(ByVal articulo As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_obtener_bodega_pedido_fabrica]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent

    End Function

    Public Function sp_articulos_costo_pedido_fabrica_ins(ByVal articulo As Long, ByVal bodega_pedido_fabrica As String, ByVal costo_pedido_fabrica As Double) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_articulos_costo_pedido_fabrica_ins]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@articulo", System.Data.SqlDbType.Int, 4, "articulo"))
                .Item("@articulo").Value = articulo
                .Add(New System.Data.SqlClient.SqlParameter("@bodega_pedido_fabrica", System.Data.SqlDbType.VarChar, 5, "bodega_pedido_fabrica"))
                .Item("@bodega_pedido_fabrica").Value = bodega_pedido_fabrica

                .Add(New System.Data.SqlClient.SqlParameter("@costo_pedido_fabrica", System.Data.SqlDbType.Money, 8, "costo_pedido_fabrica"))
                .Item("@costo_pedido_fabrica").Value = costo_pedido_fabrica
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
End Class
#End Region


