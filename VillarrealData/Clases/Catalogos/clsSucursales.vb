'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsSucursales
'DATE:		06/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Sucursales"
Public Class clsSucursales
	Public Function sp_sucursales_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_sucursales_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
				.Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
				.Item("@sucursal").Direction = ParameterDirection.InputOutput
				.Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 50, "nombre"))
				.Item("@nombre").Value = Connection.GetValue(oData, "nombre")
				.Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 30, "direccion"))
				.Item("@direccion").Value = Connection.GetValue(oData, "direccion")
				.Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 50, "colonia"))
				.Item("@colonia").Value = Connection.GetValue(oData, "colonia")
				.Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
				.Item("@cp").Value = Connection.GetValue(oData, "cp")
				.Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
				.Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
				.Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
				.Item("@estado").Value = Connection.GetValue(oData, "estado")
				.Add(New System.Data.SqlClient.SqlParameter("@telefono", System.Data.SqlDbType.VarChar, 13, "telefono"))
				.Item("@telefono").Value = Connection.GetValue(oData, "telefono")
				.Add(New System.Data.SqlClient.SqlParameter("@gerente", System.Data.SqlDbType.VarChar, 80, "gerente"))
                .Item("@gerente").Value = Connection.GetValue(oData, "gerente")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_salida_traspaso", System.Data.SqlDbType.VarChar, 5, "concepto_salida_traspaso"))
                .Item("@concepto_salida_traspaso").Value = Connection.GetValue(oData, "concepto_salida_traspaso")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_entrada_traspaso", System.Data.SqlDbType.VarChar, 5, "concepto_entrada_traspaso"))
                .Item("@concepto_entrada_traspaso").Value = Connection.GetValue(oData, "concepto_entrada_traspaso")


                .Add(New System.Data.SqlClient.SqlParameter("@serie_intereses", System.Data.SqlDbType.Char, 3, "serie_intereses"))
                .Item("@serie_intereses").Value = Connection.GetValue(oData, "serie_intereses")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_intereses", System.Data.SqlDbType.Int, 4, "folio_intereses"))
                .Item("@folio_intereses").Value = Connection.GetValue(oData, "folio_intereses")

                .Add(New System.Data.SqlClient.SqlParameter("@precio_venta", System.Data.SqlDbType.Int, 4, "precio_venta"))
                .Item("@precio_venta").Value = Connection.GetValue(oData, "precio_venta")
                .Add(New System.Data.SqlClient.SqlParameter("@precio_minimo_venta", System.Data.SqlDbType.Int, 4, "precio_minimo_venta"))
                .Item("@precio_minimo_venta").Value = Connection.GetValue(oData, "precio_minimo_venta")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Bit, 1, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = Connection.GetValue(oData, "sucursal_dependencia")

                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = Connection.GetValue(oData, "quincena_envio")
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = Connection.GetValue(oData, "anio_envio")

                .Add(New System.Data.SqlClient.SqlParameter("@factura_electronica", System.Data.SqlDbType.Bit, 1, "factura_electronica"))
                .Item("@factura_electronica").Value = Connection.GetValue(oData, "factura_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_electronica", System.Data.SqlDbType.Char, 3, "serie_factura_electronica"))
                .Item("@serie_factura_electronica").Value = Connection.GetValue(oData, "serie_factura_electronica")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_electronica", System.Data.SqlDbType.Int, 4, "folio_factura_electronica"))
                .Item("@folio_factura_electronica").Value = Connection.GetValue(oData, "folio_factura_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_nota_cargo_electronica", System.Data.SqlDbType.Char, 3, "serie_nota_cargo_electronica"))
                .Item("@serie_nota_cargo_electronica").Value = Connection.GetValue(oData, "serie_nota_cargo_electronica")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_nota_cargo_electronica", System.Data.SqlDbType.Int, 4, "folio_nota_cargo_electronica"))
                .Item("@folio_nota_cargo_electronica").Value = Connection.GetValue(oData, "folio_nota_cargo_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_nota_credito_electronica", System.Data.SqlDbType.Char, 3, "serie_nota_credito_electronica"))
                .Item("@serie_nota_credito_electronica").Value = Connection.GetValue(oData, "serie_nota_credito_electronica")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_nota_credito_electronica", System.Data.SqlDbType.Int, 4, "folio_nota_credito_electronica"))
                .Item("@folio_nota_credito_electronica").Value = Connection.GetValue(oData, "folio_nota_credito_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibos_electronica", System.Data.SqlDbType.Char, 3, "serie_recibos_electronica"))
                .Item("@serie_recibos_electronica").Value = Connection.GetValue(oData, "serie_recibos_electronica")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibos_electronica", System.Data.SqlDbType.Int, 4, "folio_recibos_electronica"))
                .Item("@folio_recibos_electronica").Value = Connection.GetValue(oData, "folio_recibos_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@puerto_impresion", System.Data.SqlDbType.VarChar, 100, "puerto_impresion"))
                .Item("@puerto_impresion").Value = Connection.GetValue(oData, "puerto_impresion")

     

				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)
			Connection.SetValue(oData, "sucursal", oCommand.Parameters.Item("@sucursal").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_sucursales_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_sucursales_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
				.Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
				.Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 50, "nombre"))
				.Item("@nombre").Value = Connection.GetValue(oData, "nombre")
				.Add(New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 30, "direccion"))
				.Item("@direccion").Value = Connection.GetValue(oData, "direccion")
				.Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.VarChar, 50, "colonia"))
				.Item("@colonia").Value = Connection.GetValue(oData, "colonia")
				.Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
				.Item("@cp").Value = Connection.GetValue(oData, "cp")
				.Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.VarChar, 50, "ciudad"))
				.Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
				.Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.VarChar, 50, "estado"))
				.Item("@estado").Value = Connection.GetValue(oData, "estado")
				.Add(New System.Data.SqlClient.SqlParameter("@telefono", System.Data.SqlDbType.VarChar, 13, "telefono"))
				.Item("@telefono").Value = Connection.GetValue(oData, "telefono")
				.Add(New System.Data.SqlClient.SqlParameter("@gerente", System.Data.SqlDbType.VarChar, 80, "gerente"))
                .Item("@gerente").Value = Connection.GetValue(oData, "gerente")

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_salida_traspaso", System.Data.SqlDbType.VarChar, 5, "concepto_salida_traspaso"))
                .Item("@concepto_salida_traspaso").Value = Connection.GetValue(oData, "concepto_salida_traspaso")
                .Add(New System.Data.SqlClient.SqlParameter("@concepto_entrada_traspaso", System.Data.SqlDbType.VarChar, 5, "concepto_entrada_traspaso"))
                .Item("@concepto_entrada_traspaso").Value = Connection.GetValue(oData, "concepto_entrada_traspaso")


                .Add(New System.Data.SqlClient.SqlParameter("@serie_intereses", System.Data.SqlDbType.Char, 3, "serie_intereses"))
                .Item("@serie_intereses").Value = Connection.GetValue(oData, "serie_intereses")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_intereses", System.Data.SqlDbType.Int, 4, "folio_intereses"))
                .Item("@folio_intereses").Value = Connection.GetValue(oData, "folio_intereses")

                .Add(New System.Data.SqlClient.SqlParameter("@precio_venta", System.Data.SqlDbType.Int, 4, "precio_venta"))
                .Item("@precio_venta").Value = Connection.GetValue(oData, "precio_venta")
                .Add(New System.Data.SqlClient.SqlParameter("@precio_minimo_venta", System.Data.SqlDbType.Int, 4, "precio_minimo_venta"))
                .Item("@precio_minimo_venta").Value = Connection.GetValue(oData, "precio_minimo_venta")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Bit, 1, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = Connection.GetValue(oData, "sucursal_dependencia")

                .Add(New System.Data.SqlClient.SqlParameter("@quincena_envio", System.Data.SqlDbType.Int, 4, "quincena_envio"))
                .Item("@quincena_envio").Value = Connection.GetValue(oData, "quincena_envio")
                .Add(New System.Data.SqlClient.SqlParameter("@anio_envio", System.Data.SqlDbType.Int, 4, "anio_envio"))
                .Item("@anio_envio").Value = Connection.GetValue(oData, "anio_envio")

                .Add(New System.Data.SqlClient.SqlParameter("@factura_electronica", System.Data.SqlDbType.Bit, 1, "factura_electronica"))
                .Item("@factura_electronica").Value = Connection.GetValue(oData, "factura_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_factura_electronica", System.Data.SqlDbType.Char, 3, "serie_factura_electronica"))
                .Item("@serie_factura_electronica").Value = Connection.GetValue(oData, "serie_factura_electronica")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_factura_electronica", System.Data.SqlDbType.Int, 4, "folio_factura_electronica"))
                .Item("@folio_factura_electronica").Value = Connection.GetValue(oData, "folio_factura_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_nota_cargo_electronica", System.Data.SqlDbType.Char, 3, "serie_nota_cargo_electronica"))
                .Item("@serie_nota_cargo_electronica").Value = Connection.GetValue(oData, "serie_nota_cargo_electronica")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_nota_cargo_electronica", System.Data.SqlDbType.Int, 4, "folio_nota_cargo_electronica"))
                .Item("@folio_nota_cargo_electronica").Value = Connection.GetValue(oData, "folio_nota_cargo_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_nota_credito_electronica", System.Data.SqlDbType.Char, 3, "serie_nota_credito_electronica"))
                .Item("@serie_nota_credito_electronica").Value = Connection.GetValue(oData, "serie_nota_credito_electronica")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_nota_credito_electronica", System.Data.SqlDbType.Int, 4, "folio_nota_credito_electronica"))
                .Item("@folio_nota_credito_electronica").Value = Connection.GetValue(oData, "folio_nota_credito_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibos_electronica", System.Data.SqlDbType.Char, 3, "serie_recibos_electronica"))
                .Item("@serie_recibos_electronica").Value = Connection.GetValue(oData, "serie_recibos_electronica")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibos_electronica", System.Data.SqlDbType.Int, 4, "folio_recibos_electronica"))
                .Item("@folio_recibos_electronica").Value = Connection.GetValue(oData, "folio_recibos_electronica")

                .Add(New System.Data.SqlClient.SqlParameter("@puerto_impresion", System.Data.SqlDbType.VarChar, 100, "puerto_impresion"))
                .Item("@puerto_impresion").Value = Connection.GetValue(oData, "puerto_impresion")


				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_sucursales_del(ByVal sucursal as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_sucursales_del]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
				.Item("@sucursal").Value = sucursal

			End With
			Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
	
		Return oEvent
	End Function

	Public Function sp_sucursales_exs(ByVal sucursal as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_sucursales_exs]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
				.Item("@sucursal").Value = sucursal

			End With
			oEvent.Value = Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

	Public Function sp_sucursales_sel(ByVal sucursal as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_sucursales_sel]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
				.Item("@sucursal").Value = sucursal

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_sucursales_grs() As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_sucursales_grs]"
			With oCommand.Parameters

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

    Public Function sp_sucursales_grl(sucursal_dependencia AS Long ) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_sucursales_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Int, 4, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = sucursal_dependencia
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_sucursales_existencias_grl(ByVal sucursal_dependencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_sucursales_existencias_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_dependencia", System.Data.SqlDbType.Int, 4, "sucursal_dependencia"))
                .Item("@sucursal_dependencia").Value = sucursal_dependencia
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_sucursales_folio_promocion_upd(ByVal sucursal As Long, ByVal folio_promocion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_sucursales_folio_promocion_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@folio_promocion", System.Data.SqlDbType.Int, 4, "folio_promocion"))
                .Item("@folio_promocion").Value = folio_promocion

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


