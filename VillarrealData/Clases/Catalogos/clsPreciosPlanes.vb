'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPreciosPlanes
'DATE:		26/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - PreciosPlanes"
Public Class clsPreciosPlanes
	Public Function sp_precios_planes_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_precios_planes_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
				.Item("@precio").Value = Connection.GetValue(oData, "precio")
				.Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
				.Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
				.Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
				.Item("@orden").Value = Connection.GetValue(oData, "orden")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_precios_planes_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_precios_planes_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
				.Item("@precio").Value = Connection.GetValue(oData, "precio")
				.Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
				.Item("@plan_credito").Value = Connection.GetValue(oData, "plan_credito")
				.Add(New System.Data.SqlClient.SqlParameter("@orden", System.Data.SqlDbType.Int, 4, "orden"))
				.Item("@orden").Value = Connection.GetValue(oData, "orden")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_precios_planes_del(ByVal precio as long, ByVal plan_credito as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_precios_planes_del]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
				.Item("@precio").Value = precio
				.Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
				.Item("@plan_credito").Value = plan_credito

			End With
			Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
	
		Return oEvent
	End Function

	Public Function sp_precios_planes_exs(ByVal precio as long, ByVal plan_credito as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_precios_planes_exs]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
				.Item("@precio").Value = precio
				.Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
				.Item("@plan_credito").Value = plan_credito

			End With
			oEvent.Value = Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

	Public Function sp_precios_planes_sel(ByVal precio as long, ByVal plan_credito as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_precios_planes_sel]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@precio", System.Data.SqlDbType.Int, 4, "precio"))
				.Item("@precio").Value = precio
				.Add(New System.Data.SqlClient.SqlParameter("@plan_credito", System.Data.SqlDbType.Int, 4, "plan_credito"))
				.Item("@plan_credito").Value = plan_credito

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_precios_planes_grs() As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_precios_planes_grs]"
			With oCommand.Parameters

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function



End Class
#End Region


