'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVendedores
'DATE:		24/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Vendedores"
Public Class clsVendedores
    Public Function sp_vendedores_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")
                .Item("@vendedor").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 60, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@sueldo", System.Data.SqlDbType.Money, 8, "sueldo"))
                .Item("@sueldo").Value = Connection.GetValue(oData, "sueldo")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")

                .Add(New System.Data.SqlClient.SqlParameter("@meta", System.Data.SqlDbType.Money, 8, "meta"))
                .Item("@meta").Value = Connection.GetValue(oData, "meta")
                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = Connection.GetValue(oData, "activo")

                .Add(New System.Data.SqlClient.SqlParameter("@codigo_nomipaq", System.Data.SqlDbType.VarChar, 5, "codigo_nomipaq"))
                .Item("@codigo_nomipaq").Value = Connection.GetValue(oData, "codigo_nomipaq")

                .Add(New System.Data.SqlClient.SqlParameter("@esquema_comisiones", System.Data.SqlDbType.Int, 4, "esquema_comisiones"))
                .Item("@esquema_comisiones").Value = Connection.GetValue(oData, "esquema_comisiones")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "vendedor", oCommand.Parameters.Item("@vendedor").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vendedores_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = Connection.GetValue(oData, "vendedor")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 60, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@sueldo", System.Data.SqlDbType.Money, 8, "sueldo"))
                .Item("@sueldo").Value = Connection.GetValue(oData, "sueldo")
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = Connection.GetValue(oData, "sucursal")
                .Add(New System.Data.SqlClient.SqlParameter("@meta", System.Data.SqlDbType.Money, 8, "meta"))
                .Item("@meta").Value = Connection.GetValue(oData, "meta")
                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = Connection.GetValue(oData, "activo")

                .Add(New System.Data.SqlClient.SqlParameter("@codigo_nomipaq", System.Data.SqlDbType.VarChar, 5, "codigo_nomipaq"))
                .Item("@codigo_nomipaq").Value = Connection.GetValue(oData, "codigo_nomipaq")
                .Add(New System.Data.SqlClient.SqlParameter("@esquema_comisiones", System.Data.SqlDbType.Int, 4, "esquema_comisiones"))
                .Item("@esquema_comisiones").Value = Connection.GetValue(oData, "esquema_comisiones")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vendedores_upd(ByVal vendedor As Long, ByVal nombre As String, ByVal sueldo As Double, ByVal sucursal As Long, ByVal meta As Double, ByVal activo As Boolean, ByVal codigo_nomipaq As String, ByVal esquema_comisiones As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 60, "nombre"))
                .Item("@nombre").Value = nombre
                .Add(New System.Data.SqlClient.SqlParameter("@sueldo", System.Data.SqlDbType.Money, 8, "sueldo"))
                .Item("@sueldo").Value = sueldo
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal
                .Add(New System.Data.SqlClient.SqlParameter("@meta", System.Data.SqlDbType.Money, 8, "meta"))
                .Item("@meta").Value = meta
                .Add(New System.Data.SqlClient.SqlParameter("@activo", System.Data.SqlDbType.Bit, 1, "activo"))
                .Item("@activo").Value = activo

                .Add(New System.Data.SqlClient.SqlParameter("@codigo_nomipaq", System.Data.SqlDbType.VarChar, 5, "codigo_nomipaq"))
                .Item("@codigo_nomipaq").Value = codigo_nomipaq
                .Add(New System.Data.SqlClient.SqlParameter("@esquema_comisiones", System.Data.SqlDbType.Int, 4, "esquema_comisiones"))
                .Item("@esquema_comisiones").Value = esquema_comisiones


                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vendedores_del(ByVal vendedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vendedores_exs(ByVal vendedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vendedores_sel(ByVal vendedor As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@vendedor", System.Data.SqlDbType.Int, 4, "vendedor"))
                .Item("@vendedor").Value = vendedor

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vendedores_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_vendedores_grl(ByVal sucursal_actual As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_vendedores_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal_actual
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_guardar_comisiones(ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal menor_13 As Double, ByVal mayor_13 As Double, ByVal sucursal As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events

        Try
            oCommand.CommandText = "[sp_guardar_comisiones]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_ini", System.Data.SqlDbType.DateTime, 8, "fecha_ini"))
                .Item("@fecha_ini").Value = fecha_ini
                .Add(New System.Data.SqlClient.SqlParameter("@fecha_fin", System.Data.SqlDbType.DateTime, 8, "fecha_fin"))
                .Item("@fecha_fin").Value = fecha_fin
                .Add(New System.Data.SqlClient.SqlParameter("@menor_13", System.Data.SqlDbType.Decimal, 18, "menor_13"))
                .Item("@menor_13").Value = menor_13
                .Add(New System.Data.SqlClient.SqlParameter("@mayor_13", System.Data.SqlDbType.Decimal, 18, "mayor_13"))
                .Item("@mayor_13").Value = mayor_13
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


