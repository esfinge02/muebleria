'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCiudades
'DATE:		15/06/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Ciudades"
Public Class clsCiudades
	Public Function sp_ciudades_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_ciudades_ins]"
			With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")

                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")

                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
				.Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
                '.Item("@ciudad").Direction = ParameterDirection.InputOutput

                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
			End With
			Connection.Execute(oCommand)
			Connection.SetValue(oData, "ciudad", oCommand.Parameters.Item("@ciudad").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_ciudades_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_ciudades_upd]"
			With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = Connection.GetValue(oData, "estado")
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = Connection.GetValue(oData, "municipio")
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
				.Item("@ciudad").Value = Connection.GetValue(oData, "ciudad")
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                '.Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                '.Item("@municipio").Value = Connection.GetValue(oData, "municipio")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
    Public Function sp_ciudades_del(ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ciudades_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = ciudad

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ciudades_exs(ByVal ciudad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ciudades_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = ciudad

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ciudades_sel(ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ciudades_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = ciudad

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ciudades_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ciudades_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ciudades_grl(ByVal estado As Long, ByVal municipio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ciudades_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class
#End Region


