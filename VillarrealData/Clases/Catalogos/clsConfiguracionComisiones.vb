Imports Dipros.Utils
Public Class clsConfiguracionComisiones

    Public Function sp_configuracion_comisiones_ins(ByVal partida As Long, ByVal limite_inferior_ventas As Double, ByVal limite_superior_ventas As Double, ByVal porcentaje_comision As Decimal) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_configuracion_comisiones_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@partida", System.Data.SqlDbType.Int, 4, "partida"))
                .Item("@partida").Value = partida
                .Add(New System.Data.SqlClient.SqlParameter("@limite_inferior_ventas", System.Data.SqlDbType.Money, 8, "limite_inferior_ventas"))
                .Item("@limite_inferior_ventas").Value = limite_inferior_ventas
                .Add(New System.Data.SqlClient.SqlParameter("@limite_superior_ventas", System.Data.SqlDbType.Money, 8, "limite_superior_ventas"))
                .Item("@limite_superior_ventas").Value = limite_superior_ventas
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_comision", System.Data.SqlDbType.Decimal, 9, "porcentaje_comision"))
                .Item("@porcentaje_comision").Value = porcentaje_comision
                .Add(New System.Data.SqlClient.SqlParameter("@quien", System.Data.SqlDbType.VarChar, 5, "quien"))
                .Item("@quien").Value = Connection.User.ToUpper()

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_configuracion_comisiones_del() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_configuracion_comisiones_del]"
            With oCommand.Parameters


            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_configuracion_comisiones_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_configuracion_comisiones_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

End Class

