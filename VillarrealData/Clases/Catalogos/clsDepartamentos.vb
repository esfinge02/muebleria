'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDepartamentos
'DATE:		24/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Departamentos"
Public Class clsDepartamentos
    Public Function sp_departamentos_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Connection.GetValue(oData, "departamento")
                .Item("@departamento").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 50, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_incremento_convenios", System.Data.SqlDbType.Decimal, 9, "porcentaje_incremento_convenios"))
                .Item("@porcentaje_incremento_convenios").Value = Connection.GetValue(oData, "porcentaje_incremento_convenios")
                .Add(New System.Data.SqlClient.SqlParameter("@no_identificable", System.Data.SqlDbType.Bit, 1, "no_identificable"))
                .Item("@no_identificable").Value = Connection.GetValue(oData, "no_identificable")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "departamento", oCommand.Parameters.Item("@departamento").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_departamentos_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = Connection.GetValue(oData, "departamento")
                .Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 50, "nombre"))
                .Item("@nombre").Value = Connection.GetValue(oData, "nombre")
                .Add(New System.Data.SqlClient.SqlParameter("@porcentaje_incremento_convenios", System.Data.SqlDbType.Decimal, 9, "porcentaje_incremento_convenios"))
                .Item("@porcentaje_incremento_convenios").Value = Connection.GetValue(oData, "porcentaje_incremento_convenios")
                .Add(New System.Data.SqlClient.SqlParameter("@no_identificable", System.Data.SqlDbType.Bit, 1, "no_identificable"))
                .Item("@no_identificable").Value = Connection.GetValue(oData, "no_identificable")

                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User
            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_departamentos_del(ByVal departamento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_departamentos_exs(ByVal departamento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_departamentos_sel(ByVal departamento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_departamentos_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_departamentos_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_grl]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_departamentos_existencias_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_existencias_grl]"
            With oCommand.Parameters
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_departamentos_fijo_grl(ByVal departamento As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_departamentos_fijo_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@departamento", System.Data.SqlDbType.Int, 4, "departamento"))
                .Item("@departamento").Value = departamento
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


