'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsTamañosEtiquetasDetalle
'DATE:		26/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - TamañosEtiquetasDetalle"
Public Class clsTamaniosEtiquetasDetalle
    Public Function sp_tamanios_etiquetas_detalle_ins(ByRef oData As DataSet, ByVal tamanio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tamanios_etiquetas_detalle_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio", System.Data.SqlDbType.Int, 4, "tamanio"))
                .Item("@tamanio").Value = tamanio
                .Add(New System.Data.SqlClient.SqlParameter("@plantilla", System.Data.SqlDbType.VarChar, 30, "plantilla"))
                .Item("@plantilla").Value = Connection.GetValue(oData, "plantilla")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tamanios_etiquetas_detalle_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tamanios_etiquetas_detalle_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio", System.Data.SqlDbType.Int, 4, "tamanio"))
                .Item("@tamanio").Value = Connection.GetValue(oData, "tamanio")
                .Add(New System.Data.SqlClient.SqlParameter("@plantilla", System.Data.SqlDbType.VarChar, 30, "plantilla"))
                .Item("@plantilla").Value = Connection.GetValue(oData, "plantilla")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tamanios_etiquetas_detalle_del(ByVal tamanio As Long, ByVal plantilla As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tamanios_etiquetas_detalle_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio", System.Data.SqlDbType.Int, 4, "tamanio"))
                .Item("@tamanio").Value = tamanio
                .Add(New System.Data.SqlClient.SqlParameter("@plantilla", System.Data.SqlDbType.VarChar, 30, "plantilla"))
                .Item("@plantilla").Value = plantilla

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tamanios_etiquetas_detalle_exs(ByVal tamanio As Long, ByVal plantilla As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tamanios_etiquetas_detalle_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio", System.Data.SqlDbType.Int, 4, "tamanio"))
                .Item("@tamanio").Value = tamanio
                .Add(New System.Data.SqlClient.SqlParameter("@plantilla", System.Data.SqlDbType.VarChar, 30, "plantilla"))
                .Item("@plantilla").Value = plantilla

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tamanios_etiquetas_detalle_sel(ByVal tamanio As Long, ByVal plantilla As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tamanios_etiquetas_detalle_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio", System.Data.SqlDbType.Int, 4, "tamanio"))
                .Item("@tamanio").Value = tamanio
                .Add(New System.Data.SqlClient.SqlParameter("@plantilla", System.Data.SqlDbType.VarChar, 30, "plantilla"))
                .Item("@plantilla").Value = plantilla

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_tamanios_etiquetas_detalle_grs(ByVal tamanio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_tamanios_etiquetas_detalle_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@tamanio", System.Data.SqlDbType.Int, 4, "tamanio"))
                .Item("@tamanio").Value = tamanio

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


