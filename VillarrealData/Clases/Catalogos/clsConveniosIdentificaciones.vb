'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsConveniosIdentificaciones
'DATE:		09/11/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - ConveniosIdentificaciones"
Public Class clsConveniosIdentificaciones

    Public Function sp_convenios_identificaciones_ins(ByVal convenio As Long, ByVal campo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_convenios_identificaciones_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 50, "campo"))
                .Item("@campo").Value = campo
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_convenios_identificaciones_del(ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_convenios_identificaciones_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio


            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_convenios_identificaciones_grs(ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_convenios_identificaciones_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    'Public Function sp_convenios_identificaciones_upd(ByVal convenio As Long, ByVal campo As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_convenios_identificaciones_upd]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
    '            .Item("@convenio").Value = convenio
    '            .Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 50, "campo"))
    '            .Item("@campo").Value = campo
    '            .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
    '            .Item("@QUIEN").Value = Connection.User

    '        End With
    '        Connection.Execute(oCommand)

    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function
    'Public Function sp_convenios_identificaciones_exs(ByVal convenio As Long, ByVal campo As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_convenios_identificaciones_exs]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
    '            .Item("@convenio").Value = convenio
    '            .Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 50, "campo"))
    '            .Item("@campo").Value = campo

    '        End With
    '        oEvent.Value = Connection.Execute(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    'Public Function sp_convenios_identificaciones_sel(ByVal convenio As Long, ByVal campo As String) As Events
    '    Dim oCommand As New System.Data.SqlClient.SqlCommand
    '    Dim oEvent As New Events
    '    Try
    '        oCommand.CommandText = "[sp_convenios_identificaciones_sel]"
    '        With oCommand.Parameters
    '            .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
    '            .Item("@convenio").Value = convenio
    '            .Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 50, "campo"))
    '            .Item("@campo").Value = campo

    '        End With
    '        oEvent.Value = Connection.GetDataSet(oCommand)
    '    Catch ex As Exception
    '        oEvent.Ex = ex
    '        oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
    '        oEvent.Layer = Events.ErrorLayer.DataLayer
    '    Finally
    '        oCommand = Nothing
    '    End Try

    '    Return oEvent
    'End Function

    Public Function sp_convenios_campos_plantilla_grs(ByVal convenio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_convenios_campos_plantilla_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


