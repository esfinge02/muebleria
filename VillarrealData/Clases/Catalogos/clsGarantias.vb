'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsGarantias
'DATE:		31/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Garantias"
Public Class clsGarantias
	Public Function sp_garantias_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_garantias_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@garantia", System.Data.SqlDbType.Int, 4, "garantia"))
				.Item("@garantia").Value = Connection.GetValue(oData, "garantia")
				.Item("@garantia").Direction = ParameterDirection.InputOutput
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 30, "descripcion"))
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)
			Connection.SetValue(oData, "garantia", oCommand.Parameters.Item("@garantia").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_garantias_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_garantias_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@garantia", System.Data.SqlDbType.Int, 4, "garantia"))
				.Item("@garantia").Value = Connection.GetValue(oData, "garantia")
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 30, "descripcion"))
				.Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_garantias_del(ByVal garantia as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_garantias_del]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@garantia", System.Data.SqlDbType.Int, 4, "garantia"))
				.Item("@garantia").Value = garantia

			End With
			Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
	
		Return oEvent
	End Function

	Public Function sp_garantias_exs(ByVal garantia as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_garantias_exs]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@garantia", System.Data.SqlDbType.Int, 4, "garantia"))
				.Item("@garantia").Value = garantia

			End With
			oEvent.Value = Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

	Public Function sp_garantias_sel(ByVal garantia as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_garantias_sel]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@garantia", System.Data.SqlDbType.Int, 4, "garantia"))
				.Item("@garantia").Value = garantia

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_garantias_grs() As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_garantias_grs]"
			With oCommand.Parameters

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function



End Class
#End Region


