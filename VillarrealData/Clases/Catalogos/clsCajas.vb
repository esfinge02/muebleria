'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCajas
'DATE:		04/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Cajas"
Public Class clsCajas
	Public Function sp_cajas_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_cajas_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
				.Item("@caja").Value = Connection.GetValue(oData, "caja")
				.Item("@caja").Direction = ParameterDirection.InputOutput
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = Connection.GetValue(oData, "serie_recibo")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = Connection.GetValue(oData, "folio_recibo")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_nota_credito", System.Data.SqlDbType.Char, 3, "serie_nota_credito"))
                .Item("@serie_nota_credito").Value = Connection.GetValue(oData, "serie_nota_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_nota_credito", System.Data.SqlDbType.Int, 4, "folio_nota_credito"))
                .Item("@folio_nota_credito").Value = Connection.GetValue(oData, "folio_nota_credito")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_nota_cargo", System.Data.SqlDbType.Char, 3, "serie_nota_cargo"))
                .Item("@serie_nota_cargo").Value = Connection.GetValue(oData, "serie_nota_cargo")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_nota_cargo", System.Data.SqlDbType.Int, 4, "folio_nota_cargo"))
                .Item("@folio_nota_cargo").Value = Connection.GetValue(oData, "folio_nota_cargo")

                .Add(New System.Data.SqlClient.SqlParameter("@puerto_impresion", System.Data.SqlDbType.VarChar, 100, "puerto_impresion"))
                .Item("@puerto_impresion").Value = Connection.GetValue(oData, "puerto_impresion")


                .Add(New System.Data.SqlClient.SqlParameter("@ruta_impresion_ticket", System.Data.SqlDbType.VarChar, 100, "ruta_impresion_ticket"))
                .Item("@ruta_impresion_ticket").Value = Connection.GetValue(oData, "ruta_impresion_ticket")


				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)
			Connection.SetValue(oData, "caja", oCommand.Parameters.Item("@caja").Value)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_cajas_upd(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_cajas_upd]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
				.Item("@caja").Value = Connection.GetValue(oData, "caja")
				.Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_recibo", System.Data.SqlDbType.Char, 3, "serie_recibo"))
                .Item("@serie_recibo").Value = Connection.GetValue(oData, "serie_recibo")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_recibo", System.Data.SqlDbType.Int, 4, "folio_recibo"))
                .Item("@folio_recibo").Value = Connection.GetValue(oData, "folio_recibo")
                .Add(New System.Data.SqlClient.SqlParameter("@serie_nota_credito", System.Data.SqlDbType.Char, 3, "serie_nota_credito"))
                .Item("@serie_nota_credito").Value = Connection.GetValue(oData, "serie_nota_credito")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_nota_credito", System.Data.SqlDbType.Int, 4, "folio_nota_credito"))
                .Item("@folio_nota_credito").Value = Connection.GetValue(oData, "folio_nota_credito")

                .Add(New System.Data.SqlClient.SqlParameter("@serie_nota_cargo", System.Data.SqlDbType.Char, 3, "serie_nota_cargo"))
                .Item("@serie_nota_cargo").Value = Connection.GetValue(oData, "serie_nota_cargo")
                .Add(New System.Data.SqlClient.SqlParameter("@folio_nota_cargo", System.Data.SqlDbType.Int, 4, "folio_nota_cargo"))
                .Item("@folio_nota_cargo").Value = Connection.GetValue(oData, "folio_nota_cargo")

                .Add(New System.Data.SqlClient.SqlParameter("@puerto_impresion", System.Data.SqlDbType.VarChar, 100, "puerto_impresion"))
                .Item("@puerto_impresion").Value = Connection.GetValue(oData, "puerto_impresion")

                .Add(New System.Data.SqlClient.SqlParameter("@ruta_impresion_ticket", System.Data.SqlDbType.VarChar, 100, "ruta_impresion_ticket"))
                .Item("@ruta_impresion_ticket").Value = Connection.GetValue(oData, "ruta_impresion_ticket")

				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function
		
	Public Function sp_cajas_del(ByVal caja as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_cajas_del]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
				.Item("@caja").Value = caja

			End With
			Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
	
		Return oEvent
	End Function

	Public Function sp_cajas_exs(ByVal caja as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_cajas_exs]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
				.Item("@caja").Value = caja

			End With
			oEvent.Value = Connection.Execute(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

	Public Function sp_cajas_sel(ByVal caja as long) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_cajas_sel]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@caja", System.Data.SqlDbType.Int, 4, "caja"))
				.Item("@caja").Value = caja

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

	Public Function sp_cajas_grs() As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_cajas_grs]"
			With oCommand.Parameters

			End With
			oEvent.Value = Connection.GetDataSet(oCommand)
		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try
		
		Return oEvent
	End Function

    Public Function sp_cajas_grl() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_cajas_grl]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


