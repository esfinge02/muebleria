'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDependenciasDetalleExportacion
'DATE:		26/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - DependenciasDetalleExportacion"
Public Class clsDependenciasDetalleExportacion
	Public Function sp_dependencias_detalle_exportacion_ins(ByRef oData As DataSet) As Events
		Dim oCommand As New System.Data.SqlClient.SqlCommand
		Dim oEvent As New Events
		Try
			oCommand.CommandText = "[sp_dependencias_detalle_exportacion_ins]"
			With oCommand.Parameters
				.Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
				.Item("@dependencia").Value = Connection.GetValue(oData, "dependencia")
				.Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 30, "campo"))
				.Item("@campo").Value = Connection.GetValue(oData, "campo")
				.Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.VarChar, 50, "tipo"))
				.Item("@tipo").Value = Connection.GetValue(oData, "tipo")
				.Add(New System.Data.SqlClient.SqlParameter("@longitud", System.Data.SqlDbType.Int, 4, "longitud"))
				.Item("@longitud").Value = Connection.GetValue(oData, "longitud")
				.Add(New System.Data.SqlClient.SqlParameter("@decimales", System.Data.SqlDbType.Int, 4, "decimales"))
				.Item("@decimales").Value = Connection.GetValue(oData, "decimales")
				.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
				.Item("@QUIEN").Value = Connection.User

			End With
			Connection.Execute(oCommand)

		Catch ex As Exception
			oEvent.Ex = ex
			oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
			oEvent.Layer = Events.ErrorLayer.DataLayer
		Finally
			oCommand = Nothing
		End Try

		Return oEvent
	End Function

    Public Function sp_dependencias_detalle_exportacion_ins(ByVal dependencia As Long, ByVal campo As String, ByVal tipo As String, ByVal longitud As Long, ByVal decimales As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_dependencias_detalle_exportacion_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia
                .Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 30, "campo"))
                .Item("@campo").Value = campo
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.VarChar, 50, "tipo"))
                .Item("@tipo").Value = tipo
                .Add(New System.Data.SqlClient.SqlParameter("@longitud", System.Data.SqlDbType.Int, 4, "longitud"))
                .Item("@longitud").Value = longitud
                .Add(New System.Data.SqlClient.SqlParameter("@decimales", System.Data.SqlDbType.Int, 4, "decimales"))
                .Item("@decimales").Value = decimales
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_dependencias_detalle_exportacion_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_dependencias_detalle_exportacion_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = Connection.GetValue(oData, "dependencia")
                .Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 30, "campo"))
                .Item("@campo").Value = Connection.GetValue(oData, "campo")
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.VarChar, 50, "tipo"))
                .Item("@tipo").Value = Connection.GetValue(oData, "tipo")
                .Add(New System.Data.SqlClient.SqlParameter("@longitud", System.Data.SqlDbType.Int, 4, "longitud"))
                .Item("@longitud").Value = Connection.GetValue(oData, "longitud")
                .Add(New System.Data.SqlClient.SqlParameter("@decimales", System.Data.SqlDbType.Int, 4, "decimales"))
                .Item("@decimales").Value = Connection.GetValue(oData, "decimales")
                .Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                .Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_dependencias_detalle_exportacion_del(ByVal dependencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_dependencias_detalle_exportacion_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia
    
            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_dependencias_detalle_exportacion_exs(ByVal dependencia As Long, ByVal campo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_dependencias_detalle_exportacion_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia
                .Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 30, "campo"))
                .Item("@campo").Value = campo

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_dependencias_detalle_exportacion_sel(ByVal dependencia As Long, ByVal campo As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_dependencias_detalle_exportacion_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia
                .Add(New System.Data.SqlClient.SqlParameter("@campo", System.Data.SqlDbType.VarChar, 30, "campo"))
                .Item("@campo").Value = campo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_dependencias_detalle_exportacion_grs(ByVal dependencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_dependencias_detalle_exportacion_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



    Public Function sp_traer_movimientos_para_envio_dependencia(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal tipo As Char) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traer_movimientos_para_envio_dependencia]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena", System.Data.SqlDbType.Int, 4, "quincena"))
                .Item("@quincena").Value = quincena
                .Add(New System.Data.SqlClient.SqlParameter("@anio", System.Data.SqlDbType.Int, 4, "anio"))
                .Item("@anio").Value = anio
                .Add(New System.Data.SqlClient.SqlParameter("@tipo", System.Data.SqlDbType.Char, 1, "tipo"))
                .Item("@tipo").Value = tipo

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_traer_movimientos_para_envio_dependencia_documentos(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_traer_movimientos_para_envio_dependencia_documentos]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@convenio", System.Data.SqlDbType.Int, 4, "convenio"))
                .Item("@convenio").Value = convenio
                .Add(New System.Data.SqlClient.SqlParameter("@quincena", System.Data.SqlDbType.Int, 4, "quincena"))
                .Item("@quincena").Value = quincena
                .Add(New System.Data.SqlClient.SqlParameter("@anio", System.Data.SqlDbType.Int, 4, "anio"))
                .Item("@anio").Value = anio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_movimientos_cobrar_cliente_grl(ByVal cliente As Long, ByVal sucursal As Long, ByVal quincena As Long, ByVal anio As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_movimientos_cobrar_cliente_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@sucursal", System.Data.SqlDbType.Int, 4, "sucursal"))
                .Item("@sucursal").Value = sucursal

                .Add(New System.Data.SqlClient.SqlParameter("@quincena", System.Data.SqlDbType.Int, 4, "quincena"))
                .Item("@quincena").Value = quincena
                .Add(New System.Data.SqlClient.SqlParameter("@anio", System.Data.SqlDbType.Int, 4, "anio"))
                .Item("@anio").Value = anio


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


    Public Function sp_actualiza_envio_cancelado(ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal serie_referencia As String, ByVal folio_referencia As Long, ByVal cliente_referencia As Long, ByVal documento_referencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_actualiza_envio_cancelado]"
            With oCommand.Parameters

                .Add(New System.Data.SqlClient.SqlParameter("@sucursal_referencia", System.Data.SqlDbType.Int, 4, "sucursal_referencia"))
                .Item("@sucursal_referencia").Value = sucursal_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@concepto_referencia", System.Data.SqlDbType.VarChar, 5, "concepto_referencia"))
                .Item("@concepto_referencia").Value = concepto_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@serie_referencia", System.Data.SqlDbType.Char, 3, "serie_referencia"))
                .Item("@serie_referencia").Value = serie_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@folio_referencia", System.Data.SqlDbType.Int, 4, "folio_referencia"))
                .Item("@folio_referencia").Value = folio_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@cliente_referencia", System.Data.SqlDbType.Int, 4, "cliente_referencia"))
                .Item("@cliente_referencia").Value = cliente_referencia

                .Add(New System.Data.SqlClient.SqlParameter("@documento_referencia", System.Data.SqlDbType.Int, 4, "documento_referencia"))
                .Item("@documento_referencia").Value = documento_referencia


            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function


End Class
#End Region


