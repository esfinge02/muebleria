'GENERATED CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsUbicaciones
'DATE:		07/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
Imports Dipros.Utils

#Region "DIPROS Systems, DataEnvironment - Ubicaciones"
Public Class clsUbicaciones
    Public Function sp_ubicaciones_ins(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ubicaciones_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Item("@bodega").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@ubicacion", System.Data.SqlDbType.VarChar, 15, "ubicacion"))
                .Item("@ubicacion").Value = Connection.GetValue(oData, "ubicacion")
                .Item("@ubicacion").Direction = ParameterDirection.InputOutput
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 60, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

            End With
            Connection.Execute(oCommand)
            Connection.SetValue(oData, "bodega", oCommand.Parameters.Item("@bodega").Value)
            Connection.SetValue(oData, "ubicacion", oCommand.Parameters.Item("@ubicacion").Value)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ubicaciones_upd(ByRef oData As DataSet) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ubicaciones_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = Connection.GetValue(oData, "bodega")
                .Add(New System.Data.SqlClient.SqlParameter("@ubicacion", System.Data.SqlDbType.VarChar, 15, "ubicacion"))
                .Item("@ubicacion").Value = Connection.GetValue(oData, "ubicacion")
                .Add(New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 60, "descripcion"))
                .Item("@descripcion").Value = Connection.GetValue(oData, "descripcion")

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ubicaciones_del(ByVal bodega As String, ByVal ubicacion As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ubicaciones_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@ubicacion", System.Data.SqlDbType.VarChar, 15, "ubicacion"))
                .Item("@ubicacion").Value = ubicacion

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ubicaciones_exs(ByVal bodega As String, ByVal ubicacion As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ubicaciones_exs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@ubicacion", System.Data.SqlDbType.VarChar, 15, "ubicacion"))
                .Item("@ubicacion").Value = ubicacion

            End With
            oEvent.Value = Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ubicaciones_sel(ByVal bodega As String, ByVal ubicacion As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ubicaciones_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
                .Add(New System.Data.SqlClient.SqlParameter("@ubicacion", System.Data.SqlDbType.VarChar, 15, "ubicacion"))
                .Item("@ubicacion").Value = ubicacion

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ubicaciones_grs() As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ubicaciones_grs]"
            With oCommand.Parameters

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_ubicaciones_grl(ByVal bodega As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_ubicaciones_grl]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@bodega", System.Data.SqlDbType.VarChar, 5, "bodega"))
                .Item("@bodega").Value = bodega
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function



End Class
#End Region


