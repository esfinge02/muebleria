Imports Dipros.Utils

Public Class clsPlantilla

    Public Function sp_plantilla_ins(ByVal dependencia As Long, ByVal clave_identificacion As String, ByVal rfc As String, ByVal plaza As Long, ByVal nombre_completo As String, ByVal apellidos As String, ByVal nombres As String, ByVal clave_empleado As Long, ByVal centro_trabajo As String, ByVal municipio As Long, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_plantilla_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia

                .Add(New System.Data.SqlClient.SqlParameter("@clave_identificacion", System.Data.SqlDbType.VarChar, 30, "clave_identificacion"))
                .Item("@clave_identificacion").Value = clave_identificacion

                .Add(New System.Data.SqlClient.SqlParameter("@rfc", System.Data.SqlDbType.VarChar, 20, "rfc"))
                .Item("@rfc").Value = rfc
                .Add(New System.Data.SqlClient.SqlParameter("@plaza", System.Data.SqlDbType.Int, 4, "plaza"))
                .Item("@plaza").Value = plaza
                .Add(New System.Data.SqlClient.SqlParameter("@nombre_completo", System.Data.SqlDbType.VarChar, 100, "nombre_completo"))
                .Item("@nombre_completo").Value = nombre_completo
                .Add(New System.Data.SqlClient.SqlParameter("@apellidos", System.Data.SqlDbType.VarChar, 50, "apellidos"))
                .Item("@apellidos").Value = apellidos
                .Add(New System.Data.SqlClient.SqlParameter("@nombres", System.Data.SqlDbType.VarChar, 50, "nombres"))
                .Item("@nombres").Value = nombres
                .Add(New System.Data.SqlClient.SqlParameter("@clave_empleado", System.Data.SqlDbType.Int, 4, "clave_empleado"))
                .Item("@clave_empleado").Value = clave_empleado
                .Add(New System.Data.SqlClient.SqlParameter("@centro_trabajo", System.Data.SqlDbType.VarChar, 25, "centro_trabajo"))
                .Item("@centro_trabajo").Value = centro_trabajo
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_plantilla_grl_trabajadores(ByVal dependencia As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_plantilla_grl_trabajadores]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia
            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_plantilla_upd_cliente(ByVal dependencia As Long, ByVal clave_identificacion As String, ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_plantilla_upd_cliente]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@clave_identificacion", System.Data.SqlDbType.VarChar, 30, "clave_identificacion"))
                .Item("@clave_identificacion").Value = clave_identificacion
                .Add(New System.Data.SqlClient.SqlParameter("@dependencia", System.Data.SqlDbType.Int, 4, "dependencia"))
                .Item("@dependencia").Value = dependencia



            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
End Class
