Imports Dipros.Utils

Public Class clsClientesDirecciones


    Public Function sp_clientes_direcciones_ins(ByVal cliente As Long, ByVal tipo_direccion As Long, ByVal domicilio As String, ByVal numero_exterior As String, ByVal numero_interior As String, _
    ByVal entrecalle As String, ByVal ycalle As String, ByVal cp As Long, ByVal colonia As Long, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal telefono_casa As String, _
    ByVal telefono_celular As String, ByVal observaciones As String, ByVal latitud As String, ByVal longitud As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_direcciones_ins]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_direccion", System.Data.SqlDbType.Int, 4, "tipo_direccion"))
                .Item("@tipo_direccion").Value = tipo_direccion
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = domicilio
                .Add(New System.Data.SqlClient.SqlParameter("@numero_exterior", System.Data.SqlDbType.VarChar, 10, "numero_exterior"))
                .Item("@numero_exterior").Value = numero_exterior
                .Add(New System.Data.SqlClient.SqlParameter("@numero_interior", System.Data.SqlDbType.VarChar, 10, "numero_interior"))
                .Item("@numero_interior").Value = numero_interior
                .Add(New System.Data.SqlClient.SqlParameter("@entrecalle", System.Data.SqlDbType.VarChar, 30, "entrecalle"))
                .Item("@entrecalle").Value = entrecalle


                .Add(New System.Data.SqlClient.SqlParameter("@ycalle", System.Data.SqlDbType.VarChar, 30, "ycalle"))
                .Item("@ycalle").Value = ycalle
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = cp

                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = colonia
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = ciudad

                .Add(New System.Data.SqlClient.SqlParameter("@telefono_casa", System.Data.SqlDbType.VarChar, 13, "telefono_casa"))
                .Item("@telefono_casa").Value = telefono_casa
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_celular", System.Data.SqlDbType.VarChar, 13, "telefono_celular"))
                .Item("@telefono_celular").Value = telefono_celular
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 100, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@latitud", System.Data.SqlDbType.VarChar, 25, "latitud"))
                .Item("@latitud").Value = latitud
                .Add(New System.Data.SqlClient.SqlParameter("@longitud", System.Data.SqlDbType.VarChar, 25, "longitud"))
                .Item("@longitud").Value = longitud



                '.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                '.Item("@QUIEN").Value = Connection.User


            End With
            Connection.Execute(oCommand)


        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_direcciones_upd(ByVal cliente As Long, ByVal tipo_direccion As Long, ByVal domicilio As String, ByVal numero_exterior As String, ByVal numero_interior As String, _
    ByVal entrecalle As String, ByVal ycalle As String, ByVal cp As Long, ByVal colonia As Long, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal telefono_casa As String, _
    ByVal telefono_celular As String, ByVal observaciones As String, ByVal latitud As String, ByVal longitud As String) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_direcciones_upd]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_direccion", System.Data.SqlDbType.Int, 4, "tipo_direccion"))
                .Item("@tipo_direccion").Value = tipo_direccion
                .Add(New System.Data.SqlClient.SqlParameter("@domicilio", System.Data.SqlDbType.VarChar, 50, "domicilio"))
                .Item("@domicilio").Value = domicilio
                .Add(New System.Data.SqlClient.SqlParameter("@numero_exterior", System.Data.SqlDbType.VarChar, 10, "numero_exterior"))
                .Item("@numero_exterior").Value = numero_exterior
                .Add(New System.Data.SqlClient.SqlParameter("@numero_interior", System.Data.SqlDbType.VarChar, 10, "numero_interior"))
                .Item("@numero_interior").Value = numero_interior
                .Add(New System.Data.SqlClient.SqlParameter("@entrecalle", System.Data.SqlDbType.VarChar, 30, "entrecalle"))
                .Item("@entrecalle").Value = entrecalle


                .Add(New System.Data.SqlClient.SqlParameter("@ycalle", System.Data.SqlDbType.VarChar, 30, "ycalle"))
                .Item("@ycalle").Value = ycalle
                .Add(New System.Data.SqlClient.SqlParameter("@cp", System.Data.SqlDbType.Int, 4, "cp"))
                .Item("@cp").Value = cp

                .Add(New System.Data.SqlClient.SqlParameter("@colonia", System.Data.SqlDbType.Int, 4, "colonia"))
                .Item("@colonia").Value = colonia
                .Add(New System.Data.SqlClient.SqlParameter("@estado", System.Data.SqlDbType.Int, 4, "estado"))
                .Item("@estado").Value = estado
                .Add(New System.Data.SqlClient.SqlParameter("@municipio", System.Data.SqlDbType.Int, 4, "municipio"))
                .Item("@municipio").Value = municipio
                .Add(New System.Data.SqlClient.SqlParameter("@ciudad", System.Data.SqlDbType.Int, 4, "ciudad"))
                .Item("@ciudad").Value = ciudad

                .Add(New System.Data.SqlClient.SqlParameter("@telefono_casa", System.Data.SqlDbType.VarChar, 13, "telefono_casa"))
                .Item("@telefono_casa").Value = telefono_casa
                .Add(New System.Data.SqlClient.SqlParameter("@telefono_celular", System.Data.SqlDbType.VarChar, 13, "telefono_celular"))
                .Item("@telefono_celular").Value = telefono_celular
                .Add(New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 100, "observaciones"))
                .Item("@observaciones").Value = observaciones
                .Add(New System.Data.SqlClient.SqlParameter("@latitud", System.Data.SqlDbType.VarChar, 25, "latitud"))
                .Item("@latitud").Value = latitud
                .Add(New System.Data.SqlClient.SqlParameter("@longitud", System.Data.SqlDbType.VarChar, 25, "longitud"))
                .Item("@longitud").Value = longitud

                '.Add(New System.Data.SqlClient.SqlParameter("@QUIEN", System.Data.SqlDbType.VarChar, 15, "QUIEN"))
                '.Item("@QUIEN").Value = Connection.User

            End With
            Connection.Execute(oCommand)

        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_direcciones_del(ByVal cliente As Long, ByVal tipo_direccion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_direcciones_del]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_direccion", System.Data.SqlDbType.Int, 4, "tipo_direccion"))
                .Item("@tipo_direccion").Value = tipo_direccion

            End With
            Connection.Execute(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_direcciones_sel(ByVal cliente As Long, ByVal tipo_direccion As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_direcciones_sel]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente
                .Add(New System.Data.SqlClient.SqlParameter("@tipo_direccion", System.Data.SqlDbType.Int, 4, "tipo_direccion"))
                .Item("@tipo_direccion").Value = tipo_direccion

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function

    Public Function sp_clientes_direcciones_grs(ByVal cliente As Long) As Events
        Dim oCommand As New System.Data.SqlClient.SqlCommand
        Dim oEvent As New Events
        Try
            oCommand.CommandText = "[sp_clientes_direcciones_grs]"
            With oCommand.Parameters
                .Add(New System.Data.SqlClient.SqlParameter("@cliente", System.Data.SqlDbType.Int, 4, "cliente"))
                .Item("@cliente").Value = cliente

            End With
            oEvent.Value = Connection.GetDataSet(oCommand)
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar ejecutar el comando " & oCommand.CommandText & ". Revise que los parámetros hayan sido establecidos correctamente."
            oEvent.Layer = Events.ErrorLayer.DataLayer
        Finally
            oCommand = Nothing
        End Try

        Return oEvent
    End Function
End Class
