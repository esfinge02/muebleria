Imports Dipros.Windows.Forms

Module modFormatos

#Region "Polizas"

    Public Sub for_polizas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Sucursal", "nombre_sucursal", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "P�liza", "poliza", 0, DevExpress.Utils.FormatType.None, "", 60)
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Script", "script", 2, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

#Region "Cuentas Contables"
    Public Sub for_cuentas_contables_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Clave", "cuentacontable", 0, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Sucursal", "nombre_sucursal", 3, DevExpress.Utils.FormatType.None, "", 100)
    End Sub
#End Region

End Module
