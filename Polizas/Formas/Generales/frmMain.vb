Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms


Public Class frmMain
    Inherits Dipros.Windows.frmTINMain


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents mnuRepPolizas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProPolizas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatCuentasContables As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuRepPolizas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProPolizas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatCuentasContables = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista, True)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCuentasContables)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProPolizas)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepPolizas)})
        '
        'BarManager
        '
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuRepPolizas, Me.mnuProPolizas, Me.mnuCatCuentasContables})
        Me.BarManager.MaxItemId = 111
        '
        'mnuRepPolizas
        '
        Me.mnuRepPolizas.Caption = "P�lizas"
        Me.mnuRepPolizas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepPolizas.Id = 108
        Me.mnuRepPolizas.ImageIndex = 0
        Me.mnuRepPolizas.Name = "mnuRepPolizas"
        '
        'mnuProPolizas
        '
        Me.mnuProPolizas.Caption = "Configurar P�lizas"
        Me.mnuProPolizas.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProPolizas.Id = 109
        Me.mnuProPolizas.ImageIndex = 0
        Me.mnuProPolizas.Name = "mnuProPolizas"
        '
        'mnuCatCuentasContables
        '
        Me.mnuCatCuentasContables.Caption = "Cuentas Contables"
        Me.mnuCatCuentasContables.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCuentasContables.Id = 110
        Me.mnuCatCuentasContables.ImageIndex = 1
        Me.mnuCatCuentasContables.Name = "mnuCatCuentasContables"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "frmMain"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Public Sub mnuCatCuentasContables_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCuentasContables.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCuentasContables As New VillarrealBusiness.clsCuentasContables
        With oGrid
            .Title = "Cuentas Contables"
            .UpdateTitle = "Cuentas Contables"
            .DataSource = AddressOf oCuentasContables.Listado
            .UpdateForm = New frmCuentasContables
            .Format = AddressOf for_cuentas_contables_grs
            .MdiParent = Me
            .Show()
        End With
        oCuentasContables = Nothing
    End Sub

    Public Sub mnuProPolizas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProPolizas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oPolizas As New VillarrealBusiness.clsPolizas
        With oGrid
            .Title = "Polizas"
            .UpdateTitle = "Polizas"
            .DataSource = AddressOf oPolizas.Listado
            .UpdateForm = New frmConfiguradorPolizas
            .Format = AddressOf for_polizas_grs
            .MdiParent = Me
            .Show()
        End With
        oPolizas = Nothing
    End Sub

    Public Sub mnuRepPolizas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepPolizas.ItemClick
        Dim oForm As New frmRepPolizas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "P�lizas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

End Class
