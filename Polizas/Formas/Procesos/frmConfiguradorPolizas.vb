Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmConfiguradorPolizas
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcPoliza As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cboScript As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents grDatosPoliza As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDatosPoliza As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcCuentaContable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreColumna As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepLkpCuentasContables As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepcboTipo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents RepcboCampos As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents repTxtDescripcion As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcControl As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkFlete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grcOrden As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdArriba As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdAbajo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHastaAbajo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHastaArriba As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ilsFlechas As System.Windows.Forms.ImageList
    Friend WithEvents cmdEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkPolizaGeneral As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConfiguradorPolizas))
        Me.grDatosPoliza = New DevExpress.XtraGrid.GridControl
        Me.grvDatosPoliza = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcControl = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCuentaContable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepLkpCuentasContables = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.grcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepcboTipo = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
        Me.grcNombreColumna = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepcboCampos = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcOrden = New DevExpress.XtraGrid.Columns.GridColumn
        Me.repTxtDescripcion = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.clcPoliza = New DevExpress.XtraEditors.CalcEdit
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboScript = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkFlete = New DevExpress.XtraEditors.CheckEdit
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmdArriba = New DevExpress.XtraEditors.SimpleButton
        Me.ilsFlechas = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.cmdAbajo = New DevExpress.XtraEditors.SimpleButton
        Me.cmdHastaAbajo = New DevExpress.XtraEditors.SimpleButton
        Me.cmdHastaArriba = New DevExpress.XtraEditors.SimpleButton
        Me.cmdEliminar = New DevExpress.XtraEditors.SimpleButton
        Me.chkPolizaGeneral = New DevExpress.XtraEditors.CheckEdit
        CType(Me.grDatosPoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDatosPoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepLkpCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepcboTipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepcboCampos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repTxtDescripcion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPoliza.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboScript.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.chkPolizaGeneral.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(8492, 28)
        '
        'grDatosPoliza
        '
        '
        'grDatosPoliza.EmbeddedNavigator
        '
        Me.grDatosPoliza.EmbeddedNavigator.Name = ""
        Me.grDatosPoliza.Location = New System.Drawing.Point(8, 144)
        Me.grDatosPoliza.MainView = Me.grvDatosPoliza
        Me.grDatosPoliza.Name = "grDatosPoliza"
        Me.grDatosPoliza.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepLkpCuentasContables, Me.RepcboTipo, Me.RepcboCampos, Me.repTxtDescripcion})
        Me.grDatosPoliza.Size = New System.Drawing.Size(808, 352)
        Me.grDatosPoliza.Styles.AddReplace("Style2", New DevExpress.Utils.ViewStyleEx("Style2", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDatosPoliza.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDatosPoliza.TabIndex = 9
        Me.grDatosPoliza.TabStop = False
        Me.grDatosPoliza.Text = "GridControl1"
        '
        'grvDatosPoliza
        '
        Me.grvDatosPoliza.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcControl, Me.grcCuentaContable, Me.grcDescripcion, Me.grcTipo, Me.grcNombreColumna, Me.grcPartida, Me.grcOrden})
        Me.grvDatosPoliza.GridControl = Me.grDatosPoliza
        Me.grvDatosPoliza.Name = "grvDatosPoliza"
        Me.grvDatosPoliza.OptionsView.ShowGroupPanel = False
        '
        'grcControl
        '
        Me.grcControl.Caption = "control"
        Me.grcControl.FieldName = "control"
        Me.grcControl.Name = "grcControl"
        Me.grcControl.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCuentaContable
        '
        Me.grcCuentaContable.Caption = "Cuenta Contable"
        Me.grcCuentaContable.ColumnEdit = Me.RepLkpCuentasContables
        Me.grcCuentaContable.FieldName = "cuentacontable"
        Me.grcCuentaContable.HeaderStyleName = "Style1"
        Me.grcCuentaContable.Name = "grcCuentaContable"
        Me.grcCuentaContable.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCuentaContable.VisibleIndex = 1
        Me.grcCuentaContable.Width = 121
        '
        'RepLkpCuentasContables
        '
        Me.RepLkpCuentasContables.AutoHeight = False
        Me.RepLkpCuentasContables.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepLkpCuentasContables.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("cuentacontable", "Cuenta", 40, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Center), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripci�n", 50)})
        Me.RepLkpCuentasContables.DisplayMember = "cuentacontable"
        Me.RepLkpCuentasContables.Name = "RepLkpCuentasContables"
        Me.RepLkpCuentasContables.NullText = ""
        Me.RepLkpCuentasContables.PopupWidth = 200
        Me.RepLkpCuentasContables.ValueMember = "cuentacontable"
        '
        'grcDescripcion
        '
        Me.grcDescripcion.Caption = "Descripci�n"
        Me.grcDescripcion.FieldName = "descripcion"
        Me.grcDescripcion.HeaderStyleName = "Style1"
        Me.grcDescripcion.Name = "grcDescripcion"
        Me.grcDescripcion.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDescripcion.VisibleIndex = 2
        Me.grcDescripcion.Width = 275
        '
        'grcTipo
        '
        Me.grcTipo.Caption = "Tipo"
        Me.grcTipo.ColumnEdit = Me.RepcboTipo
        Me.grcTipo.FieldName = "tipo"
        Me.grcTipo.HeaderStyleName = "Style1"
        Me.grcTipo.Name = "grcTipo"
        Me.grcTipo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipo.VisibleIndex = 3
        Me.grcTipo.Width = 82
        '
        'RepcboTipo
        '
        Me.RepcboTipo.AutoHeight = False
        Me.RepcboTipo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepcboTipo.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Debe", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Haber", "H", -1)})
        Me.RepcboTipo.Name = "RepcboTipo"
        '
        'grcNombreColumna
        '
        Me.grcNombreColumna.Caption = "Nombre Columna"
        Me.grcNombreColumna.ColumnEdit = Me.RepcboCampos
        Me.grcNombreColumna.FieldName = "nombre_campo"
        Me.grcNombreColumna.HeaderStyleName = "Style1"
        Me.grcNombreColumna.Name = "grcNombreColumna"
        Me.grcNombreColumna.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreColumna.VisibleIndex = 4
        Me.grcNombreColumna.Width = 242
        '
        'RepcboCampos
        '
        Me.RepcboCampos.AutoHeight = False
        Me.RepcboCampos.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepcboCampos.Name = "RepcboCampos"
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcOrden
        '
        Me.grcOrden.Caption = "Orden"
        Me.grcOrden.FieldName = "orden"
        Me.grcOrden.Name = "grcOrden"
        Me.grcOrden.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcOrden.SortIndex = 0
        Me.grcOrden.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcOrden.VisibleIndex = 0
        Me.grcOrden.Width = 74
        '
        'repTxtDescripcion
        '
        Me.repTxtDescripcion.AutoHeight = False
        Me.repTxtDescripcion.Name = "repTxtDescripcion"
        '
        'clcPoliza
        '
        Me.clcPoliza.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPoliza.Location = New System.Drawing.Point(95, 40)
        Me.clcPoliza.Name = "clcPoliza"
        '
        'clcPoliza.Properties
        '
        Me.clcPoliza.Properties.Enabled = False
        Me.clcPoliza.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPoliza.Size = New System.Drawing.Size(88, 20)
        Me.clcPoliza.TabIndex = 1
        Me.clcPoliza.Tag = "poliza"
        Me.clcPoliza.ToolTip = "Folio de la Poliza"
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(95, 88)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 100
        Me.txtNombre.Size = New System.Drawing.Size(424, 20)
        Me.txtNombre.TabIndex = 5
        Me.txtNombre.Tag = "nombre"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(44, 112)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(41, 16)
        Me.lblSucursal.TabIndex = 6
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Script:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(44, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Tag = ""
        Me.Label2.Text = "P&oliza:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(32, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Nombre:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboScript
        '
        Me.cboScript.EditValue = ""
        Me.cboScript.Location = New System.Drawing.Point(95, 112)
        Me.cboScript.Name = "cboScript"
        '
        'cboScript.Properties
        '
        Me.cboScript.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboScript.Size = New System.Drawing.Size(424, 23)
        Me.cboScript.TabIndex = 7
        Me.cboScript.Tag = "script"
        '
        'chkFlete
        '
        Me.chkFlete.Location = New System.Drawing.Point(568, 112)
        Me.chkFlete.Name = "chkFlete"
        '
        'chkFlete.Properties
        '
        Me.chkFlete.Properties.Caption = "Poliza Flete"
        Me.chkFlete.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkFlete.Size = New System.Drawing.Size(104, 19)
        Me.chkFlete.TabIndex = 8
        Me.chkFlete.Tag = "flete"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(96, 64)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(287, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 3
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(32, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Tag = ""
        Me.Label4.Text = "S&ucursal:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdArriba
        '
        Me.cmdArriba.ImageIndex = 1
        Me.cmdArriba.ImageList = Me.ilsFlechas
        Me.cmdArriba.Location = New System.Drawing.Point(8, 152)
        Me.cmdArriba.Name = "cmdArriba"
        Me.cmdArriba.Size = New System.Drawing.Size(48, 24)
        Me.cmdArriba.TabIndex = 59
        Me.cmdArriba.ToolTip = "Mueve el Registro un Nivel Arriba"
        '
        'ilsFlechas
        '
        Me.ilsFlechas.ImageSize = New System.Drawing.Size(16, 16)
        Me.ilsFlechas.ImageStream = CType(resources.GetObject("ilsFlechas.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilsFlechas.TransparentColor = System.Drawing.Color.Transparent
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SimpleButton1)
        Me.GroupBox1.Controls.Add(Me.cmdArriba)
        Me.GroupBox1.Controls.Add(Me.cmdAbajo)
        Me.GroupBox1.Controls.Add(Me.cmdHastaAbajo)
        Me.GroupBox1.Controls.Add(Me.cmdHastaArriba)
        Me.GroupBox1.Controls.Add(Me.cmdEliminar)
        Me.GroupBox1.Location = New System.Drawing.Point(824, 144)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(64, 352)
        Me.GroupBox1.TabIndex = 60
        Me.GroupBox1.TabStop = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.ImageIndex = 5
        Me.SimpleButton1.ImageList = Me.ilsFlechas
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 32)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(48, 24)
        Me.SimpleButton1.TabIndex = 62
        Me.SimpleButton1.ToolTip = "Elimina un Registro"
        '
        'cmdAbajo
        '
        Me.cmdAbajo.ImageIndex = 2
        Me.cmdAbajo.ImageList = Me.ilsFlechas
        Me.cmdAbajo.Location = New System.Drawing.Point(8, 200)
        Me.cmdAbajo.Name = "cmdAbajo"
        Me.cmdAbajo.Size = New System.Drawing.Size(48, 24)
        Me.cmdAbajo.TabIndex = 59
        Me.cmdAbajo.ToolTip = "Mueve el Registro un Nivel Abajo"
        '
        'cmdHastaAbajo
        '
        Me.cmdHastaAbajo.ImageIndex = 3
        Me.cmdHastaAbajo.ImageList = Me.ilsFlechas
        Me.cmdHastaAbajo.Location = New System.Drawing.Point(8, 232)
        Me.cmdHastaAbajo.Name = "cmdHastaAbajo"
        Me.cmdHastaAbajo.Size = New System.Drawing.Size(48, 24)
        Me.cmdHastaAbajo.TabIndex = 59
        Me.cmdHastaAbajo.ToolTip = "Mueve el Registro al Final"
        '
        'cmdHastaArriba
        '
        Me.cmdHastaArriba.ImageIndex = 0
        Me.cmdHastaArriba.ImageList = Me.ilsFlechas
        Me.cmdHastaArriba.Location = New System.Drawing.Point(8, 120)
        Me.cmdHastaArriba.Name = "cmdHastaArriba"
        Me.cmdHastaArriba.Size = New System.Drawing.Size(48, 24)
        Me.cmdHastaArriba.TabIndex = 59
        Me.cmdHastaArriba.ToolTip = "Mueve el Registro Al Principio"
        '
        'cmdEliminar
        '
        Me.cmdEliminar.ImageIndex = 6
        Me.cmdEliminar.ImageList = Me.ilsFlechas
        Me.cmdEliminar.Location = New System.Drawing.Point(8, 56)
        Me.cmdEliminar.Name = "cmdEliminar"
        Me.cmdEliminar.Size = New System.Drawing.Size(48, 24)
        Me.cmdEliminar.TabIndex = 61
        Me.cmdEliminar.ToolTip = "Elimina un Registro"
        '
        'chkPolizaGeneral
        '
        Me.chkPolizaGeneral.Location = New System.Drawing.Point(680, 112)
        Me.chkPolizaGeneral.Name = "chkPolizaGeneral"
        '
        'chkPolizaGeneral.Properties
        '
        Me.chkPolizaGeneral.Properties.Caption = "Poliza General"
        Me.chkPolizaGeneral.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkPolizaGeneral.Size = New System.Drawing.Size(104, 19)
        Me.chkPolizaGeneral.TabIndex = 61
        Me.chkPolizaGeneral.Tag = "general"
        '
        'frmConfiguradorPolizas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(898, 508)
        Me.Controls.Add(Me.chkPolizaGeneral)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.chkFlete)
        Me.Controls.Add(Me.cboScript)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.grDatosPoliza)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.clcPoliza)
        Me.Name = "frmConfiguradorPolizas"
        Me.Text = "frmConfiguradorPolizas"
        Me.Controls.SetChildIndex(Me.clcPoliza, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.grDatosPoliza, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.cboScript, 0)
        Me.Controls.SetChildIndex(Me.chkFlete, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.chkPolizaGeneral, 0)
        CType(Me.grDatosPoliza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDatosPoliza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepLkpCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepcboTipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepcboCampos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repTxtDescripcion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPoliza.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboScript.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.chkPolizaGeneral.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oPolizas As VillarrealBusiness.clsPolizas
    Private oPolizasDetalle As VillarrealBusiness.clsPolizasDetalle
    Private oReportes As VillarrealBusiness.Reportes
    Private oCuentasContables As VillarrealBusiness.clsCuentasContables
        Private oSucursales As VillarrealBusiness.clsSucursales


    Private NumeroPartidas As Long = 0
    Private CantidadfilasValidos As Long = 0

    Private Property NumeroFilasValidas() As Long
        Get
            Return CantidadfilasValidos
        End Get
        Set(ByVal Value As Long)
            If Value = -1 Then
                CantidadfilasValidos = 0
            Else
                CantidadfilasValidos = CantidadfilasValidos + Value
            End If

        End Set
    End Property

    Private ReadOnly Property Sucursal() As Long
        Get           
            Return PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmConfiguradorPolizas_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmConfiguradorPolizas_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmConfiguradorPolizas_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmConfiguradorPolizas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oPolizas.Insertar(Me.DataSource)
                frmConfiguradorPolizas_Detail(Response)

            Case Actions.Update
                Response = oPolizas.Actualizar(Me.DataSource)
                frmConfiguradorPolizas_Detail(Response)

            Case Actions.Delete
                frmConfiguradorPolizas_Detail(Response)
                Response = oPolizas.Eliminar(clcPoliza.Value)
        End Select

    End Sub
    Private Sub frmConfiguradorPolizas_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        Dim i As Long
        Dim sCuentaContable As String
        Dim sTipo As Char
        Dim sNombreCampo As String
        Dim lpartida As Long
        Dim lcontrol As Long

        Me.grvDatosPoliza.CloseEditor()
        Me.grvDatosPoliza.UpdateCurrentRow()

        Response = Me.oPolizasDetalle.Eliminar(Sucursal, Me.clcPoliza.Value, -1)

        For i = 0 To Me.grvDatosPoliza.RowCount - 1
            If Not Me.grvDatosPoliza.GetRowCellValue(i, Me.grcCuentaContable) Is System.DBNull.Value And _
                    Not Me.grvDatosPoliza.GetRowCellValue(i, Me.grcNombreColumna) Is System.DBNull.Value Then

                sCuentaContable = Me.grvDatosPoliza.GetRowCellValue(i, Me.grcCuentaContable)
                sTipo = Me.grvDatosPoliza.GetRowCellValue(i, Me.grcTipo)
                sNombreCampo = Me.grvDatosPoliza.GetRowCellValue(i, Me.grcNombreColumna)
                lpartida = Me.grvDatosPoliza.GetRowCellValue(i, Me.grcOrden)
                'lcontrol = Me.grvDatosPoliza.GetRowCellValue(i, Me.grcControl)

                'Select Case lcontrol
                '    Case 1
                Response = Me.oPolizasDetalle.Insertar(Sucursal, Me.clcPoliza.Value, lpartida, sCuentaContable, sTipo, sNombreCampo)
                '    Case 0
                '        If Action = Actions.Update Then
                '            Response = Me.oPolizasDetalle.Actualizar(Sucursal, Me.clcPoliza.Value, lpartida, sCuentaContable, sTipo, sNombreCampo)
                '        End If
                '        If Action = Actions.Delete Then
                '            Response = Me.oPolizasDetalle.Eliminar(Me.clcPoliza.Value, lpartida)
                '        End If
                'End Select
            End If
        Next

    End Sub
    Private Sub frmConfiguradorPolizas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oPolizas.DespliegaDatos(OwnerForm.Value("sucursal"), OwnerForm.Value("poliza"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

        Response = oPolizasDetalle.Listado(OwnerForm.Value("sucursal"), Me.clcPoliza.Value)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.grDatosPoliza.DataSource = oDataSet.Tables(0)
            Me.NumeroFilasValidas = oDataSet.Tables(0).Rows.Count
            Me.NumeroPartidas = oDataSet.Tables(0).Rows.Count
        End If

    End Sub
    Private Sub frmConfiguradorPolizas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

        oPolizas = New VillarrealBusiness.clsPolizas
        oPolizasDetalle = New VillarrealBusiness.clsPolizasDetalle
        oReportes = New VillarrealBusiness.Reportes
        oCuentasContables = New VillarrealBusiness.clsCuentasContables
        oSucursales = New VillarrealBusiness.clsSucursales


        LlenCuentasContables()
        InsertaNewRow()
        LLenaScripts(Response)
        LlenaCamposScript()

        'If Action = Actions.Insert Then
        '    Me.chkPolizaGeneral.Enabled = True
        'Else
        '    Me.chkPolizaGeneral.Enabled = False

        'End If


    End Sub
    Private Sub frmConfiguradorPolizas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        ObtenerNumeroFilasValidas()
        Response = oPolizas.Validacion(Action, Sucursal, txtNombre.Text, Me.cboScript.Text, NumeroFilasValidas)
    End Sub
    Private Sub frmConfiguradorPolizas_Localize() Handles MyBase.Localize
        Find("poliza", Me.clcPoliza.Value)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de los Controles"

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        LlenCuentasContables()
    End Sub

    Private Sub grvDatosPoliza_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grvDatosPoliza.KeyDown
        If e.KeyCode = e.KeyCode.Enter Then

            AgregarFila()


        End If
    End Sub
    Private Sub RepLkpCuentasContables_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RepLkpCuentasContables.EditValueChanged
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = Me.grvDatosPoliza
        Dim lkp As Object = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        Dim row As Object = lkp.Properties.GetDataSourceRowByKeyValue(lkp.EditValue)
        view.SetRowCellValue(view.FocusedRowHandle, view.Columns("descripcion"), row("descripcion"))
    End Sub
    Private Sub cboScript_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboScript.SelectedIndexChanged
        LlenaCamposScript()
    End Sub

    Private Sub cmdArriba_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdArriba.Click
        Arriba()
    End Sub
    Private Sub cmdAbajo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAbajo.Click
        Abajo()
    End Sub
    Private Sub cmdHastaArriba_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHastaArriba.Click
        HastaArriba()
    End Sub
    Private Sub cmdHastaAbajo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHastaAbajo.Click
        HastaAbajo()
    End Sub
    Private Sub cmdEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEliminar.Click


        If Me.grvDatosPoliza.FocusedRowHandle >= 0 Then
            Dim row As Integer = Me.grvDatosPoliza.FocusedRowHandle
            If Not Me.grvDatosPoliza.GetRowCellValue(row, Me.grcCuentaContable) Is System.DBNull.Value And _
                Not Me.grvDatosPoliza.GetRowCellValue(row, Me.grcNombreColumna) Is System.DBNull.Value Then
                Me.grvDatosPoliza.DeleteRow(row)
                NumeroPartidas = NumeroPartidas - 1

                For i As Integer = 0 To CType(grvDatosPoliza.DataSource, DataView).Count - 1
                    grvDatosPoliza.BeginUpdate()
                    CType(grvDatosPoliza.DataSource, DataView).Item(i).Item("orden") = i + 1
                    grvDatosPoliza.EndUpdate()

                Next
            End If
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub LLenaScripts(ByRef response As Events)
        response = oPolizas.Scripts
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = response.Value


            Dim i As Long
            For i = 1 To odataset.Tables(0).Rows.Count
                Dim item_for As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                item_for.Description = odataset.Tables(0).Rows(i - 1).Item("nombre")
                item_for.Value = odataset.Tables(0).Rows(i - 1).Item("nombre")
                Me.cboScript.Properties.Items.Add(item_for)
            Next

            Me.cboScript.SelectedIndex = 0
            odataset = Nothing
        End If

    End Sub
    Private Sub LlenaCamposScript()
        Dim response As New Events
        Dim odataset As DataSet
        Dim bEntro As Boolean = False

        'Aki hay que meter el nombre de cada script que hay para polizas
        'ya que depende de cual selecione se ejecutara
        Select Case Me.cboScript.Text
            'Case "re_poliza_cobranza_intereses_enajenacion"
            '    ' ========================
            '    response = oReportes.PolizaCobranzaInteresesEnajenacion(-1, Now.Date, False, 0)
            '    bEntro = True
            '    ' ========================
        Case "re_poliza_cobranza_intereses_normales"
                ' ========================
                response = oReportes.PolizaCobranzaInteresesNormales(-1, Now.Date, False, 0)
                bEntro = True
                ' ========================
            Case "re_poliza_diario_ventas_enajenacion"
                ' ========================
                response = oReportes.PolizaDiarioVentasEnajenacion(-1, Now.Date, False, 0)
                bEntro = True
                ' ========================
            Case "re_poliza_diario_ventas_normales"
                ' ========================
                response = oReportes.PolizaDiarioVentasNormales(-1, Now.Date, False, 0)
                bEntro = True
                ' ========================
            Case "re_poliza_ingresos_intereses_normales"
                ' ========================
                response = oReportes.PolizaIngresosInteresesNormales(-1, Now.Date, False, 0)
                bEntro = True
                ' ========================
                'Case "re_poliza_ingresos_intereses_enajenacion"
                '    ' ========================
                '    response = oReportes.PolizaIngresosInteresesEnajenacion(-1, Now.Date, False, 0)
                '    bEntro = True
                '    ' ========================
            Case "re_poliza_descuento_por_pronto_pago_normales"
                response = oReportes.PolizaDescuentoProntoPagoNormales(-1, Now.Date, False, 0)
                bEntro = True
                ' ========================
            Case "re_poliza_descuento_por_pronto_pago_enajenacion"
                response = oReportes.PolizaDescuentoProntoPagoEnajenacion(-1, Now.Date, False, 0)
                bEntro = True
                ' ========================
            Case "re_poliza_abono_cliente_pago_enajenacion"
                response = oReportes.PolizaAbonoClientePagoEnajenacion(-1, Now.Date, False, 0)
                bEntro = True
                ' ========================
            Case "re_poliza_abono_cliente_pago_normales"
                response = oReportes.PolizaAbonoClientePagoPuntualNormales(-1, Now.Date, False, 0)
                bEntro = True

            Case "re_poliza_fletes"
                response = oReportes.EjecutarPolizaFlete(-1, Now.Date, False, 0)
                bEntro = True

            Case "re_poliza_compras_personas_fisicas"
                response = oReportes.PolizaComprasPersonasFisicas(-1, Now.Date, False, 0)
                bEntro = True

            Case "re_poliza_compras_personas_morales"
                response = oReportes.PolizaComprasPersonasMorales(-1, Now.Date, False, 0)
                bEntro = True

            Case "re_poliza_devoluciones_proveeedor"
                response = oReportes.PolizaDevolucionesProveedor(-1, Now.Date, False, 0)
                bEntro = True
            Case "re_poliza_traspasos"
                response = oReportes.PolizaTraspasos(-1, Now.Date, False, 0)
                bEntro = True
            Case "re_poliza_descuentos_ventas_volumen"
                response = oReportes.PolizaDescuentosVentasVolumen(-1, Now.Date, False, 0)
                bEntro = True
            Case "re_poliza_descuentos_ventas_convenio"
                response = oReportes.PolizaDescuentosVentasConvenio(-1, Now.Date, False, 0)
                bEntro = True
            Case "re_poliza_cancelacion_intereses_especiales"
                response = oReportes.PolizaCancelacionInteresesEspeciales(-1, Now.Date, False, 0)
                bEntro = True
            Case "re_poliza_nota_credito_bonificacion_precio_tasa_iva"
                response = oReportes.PolizaNotaCreditoBonificacion(-1, Now.Date, False, 0)
                bEntro = True
            Case "re_poliza_fletes_otros_gastos"
                response = oReportes.PolizaFletesOtrosGastos(-1, Now.Date, False, 0)
                bEntro = True
        End Select


        If Not response.ErrorFound And bEntro = True Then
            odataset = response.Value

            Me.RepcboCampos.Items.Clear()

            Dim i As Long
            For i = 1 To odataset.Tables(0).Rows.Count
                Dim item_for As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                item_for.Description = odataset.Tables(0).Rows(i - 1).Item("nombre")
                item_for.Value = odataset.Tables(0).Rows(i - 1).Item("nombre")
                Me.RepcboCampos.Properties.Items.Add(item_for)
            Next


            odataset = Nothing
        Else
            Me.RepcboCampos.Items.Clear()
        End If

    End Sub
    Private Sub InsertaNewRow()
        Dim response As Events
        Dim odataset As DataSet

        response = Me.oPolizasDetalle.Listado(Sucursal, 0)
        If response.ErrorFound Then Exit Sub

        odataset = response.Value
        Me.grDatosPoliza.DataSource = odataset.Tables(0)

        Me.grvDatosPoliza.AddNewRow()
        Me.grvDatosPoliza.FocusedRowHandle = 0
        Me.grvDatosPoliza.MoveLast()
        Me.grvDatosPoliza.SetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcTipo, "D")
        NumeroPartidas = NumeroPartidas + 1
        Me.grvDatosPoliza.SetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcPartida, NumeroPartidas)
        Me.grvDatosPoliza.SetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcOrden, NumeroPartidas)
        Me.grvDatosPoliza.SetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcControl, 1)

    End Sub
    Private Sub ObtenerNumeroFilasValidas()
        Dim i As Long
        NumeroFilasValidas = 0

        Me.grvDatosPoliza.UpdateCurrentRow()

        For i = 0 To Me.grvDatosPoliza.RowCount - 1
            If Not Me.grvDatosPoliza.GetRowCellValue(i, Me.grcCuentaContable) Is System.DBNull.Value And _
                    Not Me.grvDatosPoliza.GetRowCellValue(i, Me.grcNombreColumna) Is System.DBNull.Value Then
                NumeroFilasValidas = NumeroFilasValidas + 1
            End If
        Next
    End Sub
    Private Sub LlenCuentasContables()
        Dim response As Events
        Dim oDataSet As DataSet

        If Me.chkPolizaGeneral.Checked Then
            response = oCuentasContables.Lookup(-1, True)
        Else
            response = oCuentasContables.Lookup(Sucursal, True)
        End If


        If Not response.ErrorFound Then
            oDataSet = response.Value
            Me.RepLkpCuentasContables.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If

    End Sub

#Region "Mover Registros"
    Private Sub Abajo()
        If grvDatosPoliza.FocusedRowHandle = CType(grvDatosPoliza.DataSource, DataView).Count - 1 Then Exit Sub
        Dim i As Integer = grvDatosPoliza.FocusedRowHandle + 1
        Dim iFocus As Integer = grvDatosPoliza.FocusedRowHandle
        grvDatosPoliza.BeginUpdate()
        CType(grvDatosPoliza.DataSource, DataView).Item(iFocus).Item("orden") = i + 1
        CType(grvDatosPoliza.DataSource, DataView).Item(iFocus + 1).Item("orden") = i
        grvDatosPoliza.FocusedRowHandle = iFocus + 1
        grvDatosPoliza.EndUpdate()
        grDatosPoliza.RefreshDataSource()
        grDatosPoliza.Refresh()
    End Sub
    Private Sub Arriba()
        If Me.grvDatosPoliza.FocusedRowHandle = 0 Then Exit Sub
        Dim i As Integer = grvDatosPoliza.FocusedRowHandle + 1
        Dim iFocus As Integer = grvDatosPoliza.FocusedRowHandle
        grvDatosPoliza.BeginUpdate()
        CType(grvDatosPoliza.DataSource, DataView).Item(iFocus).Item("orden") = i - 1
        CType(grvDatosPoliza.DataSource, DataView).Item(iFocus - 1).Item("orden") = i
        grvDatosPoliza.FocusedRowHandle = grvDatosPoliza.FocusedRowHandle - 1
        grvDatosPoliza.EndUpdate()
        grDatosPoliza.RefreshDataSource()
        grDatosPoliza.Refresh()
    End Sub
    Private Sub HastaArriba()
        For i As Integer = grvDatosPoliza.FocusedRowHandle To 0 Step -1
            grvDatosPoliza.FocusedRowHandle = i
            Arriba()
        Next
    End Sub
    Private Sub HastaAbajo()
        For i As Integer = grvDatosPoliza.FocusedRowHandle To CType(grvDatosPoliza.DataSource, DataView).Count - 1
            grvDatosPoliza.FocusedRowHandle = i
            Abajo()
        Next
    End Sub


#End Region
#End Region



    Private Sub AgregarFila()
        ' Actualiza los Valores al Cambiar de fila
        Me.grvDatosPoliza.CloseEditor()
        Me.grvDatosPoliza.UpdateCurrentRow()

        ' Valida si no tiene cuenta O nombre de la columna no deja agregar mas filas al grid
        If Me.grvDatosPoliza.GetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcCuentaContable) Is System.DBNull.Value Or Me.grvDatosPoliza.GetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcNombreColumna) Is System.DBNull.Value Then Exit Sub

        If Me.grvDatosPoliza.RowCount - 1 = Me.grvDatosPoliza.FocusedRowHandle Then
            Me.grcOrden.SortOrder = DevExpress.Data.ColumnSortOrder.None
            Me.grvDatosPoliza.AddNewRow()
            Me.grvDatosPoliza.FocusedRowHandle = 0
            Me.grvDatosPoliza.MoveLast()
            Me.grvDatosPoliza.SetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcTipo, "D")

            NumeroPartidas = NumeroPartidas + 1

            'Me.grvDatosPoliza.SetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcPartida, NumeroPartidas)
            Me.grvDatosPoliza.SetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcOrden, NumeroPartidas)
            Me.grvDatosPoliza.SetRowCellValue(Me.grvDatosPoliza.FocusedRowHandle, Me.grcControl, 1)

            Me.grcOrden.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            Me.grvDatosPoliza.MoveFirst()
            Me.grvDatosPoliza.MoveLast()
        End If


        grDatosPoliza.RefreshDataSource()
        grDatosPoliza.Refresh()
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        AgregarFila()
    End Sub

   
    Private Sub chkPolizaGeneral_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPolizaGeneral.CheckedChanged
        LlenCuentasContables()
    End Sub
End Class
