Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmRepPolizas
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpPoliza As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodegaEntrada As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpBodegaSalida As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodega_Salida As System.Windows.Forms.Label
    Friend WithEvents lblBodega_Entrada As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepPolizas))
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpPoliza = New Dipros.Editors.TINMultiLookup
        Me.lkpBodegaEntrada = New Dipros.Editors.TINMultiLookup
        Me.lkpBodegaSalida = New Dipros.Editors.TINMultiLookup
        Me.lblBodega_Salida = New System.Windows.Forms.Label
        Me.lblBodega_Entrada = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(288, 28)
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(45, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(109, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(280, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(109, 88)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Size = New System.Drawing.Size(104, 23)
        Me.dteFecha.TabIndex = 5
        Me.dteFecha.ToolTip = "Fecha"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(60, 88)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 4
        Me.lblFecha.Text = "Fe&cha:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(60, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Tag = ""
        Me.Label2.Text = "&P�liza:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPoliza
        '
        Me.lkpPoliza.AllowAdd = False
        Me.lkpPoliza.AutoReaload = False
        Me.lkpPoliza.DataSource = Nothing
        Me.lkpPoliza.DefaultSearchField = ""
        Me.lkpPoliza.DisplayMember = "nombre"
        Me.lkpPoliza.EditValue = Nothing
        Me.lkpPoliza.Filtered = False
        Me.lkpPoliza.InitValue = Nothing
        Me.lkpPoliza.Location = New System.Drawing.Point(109, 64)
        Me.lkpPoliza.MultiSelect = False
        Me.lkpPoliza.Name = "lkpPoliza"
        Me.lkpPoliza.NullText = ""
        Me.lkpPoliza.PopupWidth = CType(400, Long)
        Me.lkpPoliza.ReadOnlyControl = False
        Me.lkpPoliza.Required = False
        Me.lkpPoliza.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPoliza.SearchMember = ""
        Me.lkpPoliza.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPoliza.SelectAll = False
        Me.lkpPoliza.Size = New System.Drawing.Size(280, 20)
        Me.lkpPoliza.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPoliza.TabIndex = 3
        Me.lkpPoliza.Tag = "Poliza"
        Me.lkpPoliza.ToolTip = Nothing
        Me.lkpPoliza.ValueMember = "Poliza"
        '
        'lkpBodegaEntrada
        '
        Me.lkpBodegaEntrada.AllowAdd = False
        Me.lkpBodegaEntrada.AutoReaload = False
        Me.lkpBodegaEntrada.DataSource = Nothing
        Me.lkpBodegaEntrada.DefaultSearchField = ""
        Me.lkpBodegaEntrada.DisplayMember = "Descripcion"
        Me.lkpBodegaEntrada.EditValue = Nothing
        Me.lkpBodegaEntrada.Filtered = False
        Me.lkpBodegaEntrada.InitValue = Nothing
        Me.lkpBodegaEntrada.Location = New System.Drawing.Point(112, 48)
        Me.lkpBodegaEntrada.MultiSelect = False
        Me.lkpBodegaEntrada.Name = "lkpBodegaEntrada"
        Me.lkpBodegaEntrada.NullText = ""
        Me.lkpBodegaEntrada.PopupWidth = CType(400, Long)
        Me.lkpBodegaEntrada.ReadOnlyControl = False
        Me.lkpBodegaEntrada.Required = False
        Me.lkpBodegaEntrada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaEntrada.SearchMember = ""
        Me.lkpBodegaEntrada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaEntrada.SelectAll = False
        Me.lkpBodegaEntrada.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodegaEntrada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaEntrada.TabIndex = 62
        Me.lkpBodegaEntrada.Tag = "bodega_entrada"
        Me.lkpBodegaEntrada.ToolTip = Nothing
        Me.lkpBodegaEntrada.ValueMember = "Bodega"
        '
        'lkpBodegaSalida
        '
        Me.lkpBodegaSalida.AllowAdd = False
        Me.lkpBodegaSalida.AutoReaload = False
        Me.lkpBodegaSalida.DataSource = Nothing
        Me.lkpBodegaSalida.DefaultSearchField = ""
        Me.lkpBodegaSalida.DisplayMember = "Descripcion"
        Me.lkpBodegaSalida.EditValue = Nothing
        Me.lkpBodegaSalida.Filtered = False
        Me.lkpBodegaSalida.InitValue = Nothing
        Me.lkpBodegaSalida.Location = New System.Drawing.Point(112, 24)
        Me.lkpBodegaSalida.MultiSelect = False
        Me.lkpBodegaSalida.Name = "lkpBodegaSalida"
        Me.lkpBodegaSalida.NullText = ""
        Me.lkpBodegaSalida.PopupWidth = CType(400, Long)
        Me.lkpBodegaSalida.ReadOnlyControl = False
        Me.lkpBodegaSalida.Required = True
        Me.lkpBodegaSalida.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodegaSalida.SearchMember = ""
        Me.lkpBodegaSalida.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodegaSalida.SelectAll = False
        Me.lkpBodegaSalida.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodegaSalida.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodegaSalida.TabIndex = 60
        Me.lkpBodegaSalida.Tag = "bodega_salida"
        Me.lkpBodegaSalida.ToolTip = Nothing
        Me.lkpBodegaSalida.ValueMember = "Bodega"
        '
        'lblBodega_Salida
        '
        Me.lblBodega_Salida.AutoSize = True
        Me.lblBodega_Salida.Location = New System.Drawing.Point(16, 24)
        Me.lblBodega_Salida.Name = "lblBodega_Salida"
        Me.lblBodega_Salida.Size = New System.Drawing.Size(91, 16)
        Me.lblBodega_Salida.TabIndex = 59
        Me.lblBodega_Salida.Tag = ""
        Me.lblBodega_Salida.Text = "&Bodega Origen:"
        Me.lblBodega_Salida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBodega_Entrada
        '
        Me.lblBodega_Entrada.AutoSize = True
        Me.lblBodega_Entrada.Location = New System.Drawing.Point(11, 48)
        Me.lblBodega_Entrada.Name = "lblBodega_Entrada"
        Me.lblBodega_Entrada.Size = New System.Drawing.Size(96, 16)
        Me.lblBodega_Entrada.TabIndex = 61
        Me.lblBodega_Entrada.Tag = ""
        Me.lblBodega_Entrada.Text = "Bodega &Destino:"
        Me.lblBodega_Entrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lkpBodegaSalida)
        Me.GroupBox1.Controls.Add(Me.lblBodega_Salida)
        Me.GroupBox1.Controls.Add(Me.lblBodega_Entrada)
        Me.GroupBox1.Controls.Add(Me.lkpBodegaEntrada)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 120)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 80)
        Me.GroupBox1.TabIndex = 63
        Me.GroupBox1.TabStop = False
        '
        'frmRepPolizas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(434, 120)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpPoliza)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblFecha)
        Me.Name = "frmRepPolizas"
        Me.Text = "frmRepPolizaCobranzaIntereses"
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpPoliza, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oBodegas As VillarrealBusiness.clsBodegas


    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property Poliza() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpPoliza)
        End Get
    End Property
    Private ReadOnly Property BodegaSalida() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaSalida)
        End Get
    End Property
    Private ReadOnly Property BodegaEntrada() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodegaEntrada)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepPolizas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oSucursales = New VillarrealBusiness.clsSucursales
        oBodegas = New VillarrealBusiness.clsBodegas

        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)

    End Sub
    Private Sub frmRepPolizas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oReportes.EjecutarPoliza(Sucursal, Me.dteFecha.EditValue, True, Poliza)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                Dim oReport As New rptPoliza

                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)
                oReport.nombre_sucursal.Text = Me.lkpSucursal.Text
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))

                TINApp.ShowReport(Me.MdiParent, "P�lizas", oReport)

                oDataSet = Nothing
                oReport = Nothing

            Else

                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If


    End Sub
    Private Sub frmRepPolizas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Sucursal, Poliza, Me.dteFecha.Text)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"


    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpPoliza_Format() Handles lkpPoliza.Format
        Comunes.clsFormato.for_polizas_grl(Me.lkpPoliza) 'Me.lkpSucursal)
    End Sub
    Private Sub lkpPoliza_LoadData(ByVal Initialize As Boolean) Handles lkpPoliza.LoadData
        Dim Response As New Events
        Response = oReportes.LookupPoliza(Sucursal, -1)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPoliza.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpPoliza_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpPoliza.EditValueChanged
        If Poliza > 0 Then
            Dim poliza_traspasos As String = "Traspasos"
            If Me.lkpPoliza.Text.ToUpper = poliza_traspasos.ToUpper Then
                Me.Size = New Size(440, 240)
                Me.GroupBox1.Visible = True
            Else
                Me.GroupBox1.Visible = False
                Me.Size = New Size(440, 152)
            End If
        End If
    End Sub

    Private Sub lkpBodegaSalida_Format() Handles lkpBodegaSalida.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaSalida)
    End Sub
    Private Sub lkpBodegaSalida_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaSalida.LoadData
        Dim Response As New Events
        Response = oBodegas.LookupBodegasUsuarios(TINApp.Connection.User)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaSalida.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpBodegaSalida_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaSalida.EditValueChanged
        Me.lkpBodegaEntrada.EditValue = Nothing
        CargarBodegasDestino()
    End Sub

    Private Sub lkpBodegaEntrada_Format() Handles lkpBodegaEntrada.Format
        Comunes.clsFormato.for_bodegas_traspasos_grl(Me.lkpBodegaEntrada)
    End Sub
    Private Sub lkpBodegaEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpBodegaEntrada.LoadData
    End Sub
    Private Sub lkpBodegaEntrada_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpBodegaEntrada.EditValueChanged

    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CargarBodegasDestino()
        Dim Response As New Events
        Response = oBodegas.BodegasDestino(BodegaSalida)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpBodegaEntrada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
#End Region






   
End Class
