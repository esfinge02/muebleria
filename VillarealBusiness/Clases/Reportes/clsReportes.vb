Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	Reportes
'DATE:		09/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------

#Region "DIPROS Systems, BusinessEnvironment - Reportes"
Public Class Reportes
    Private oReportes As VillarrealData.clsReportes

#Region "Constructores"
    Sub New()
        oReportes = New VillarrealData.clsReportes
    End Sub
#End Region

#Region "Métodos Públicos"
    Public Function OrdenesCompra(ByVal orden As Long) As Events
        OrdenesCompra = oReportes.re_ordenes_compra(orden)
    End Function
    Public Function PendientesArribar(ByVal PROVEEDOR As Long, ByVal SUCURSAL As Long) As Events
        PendientesArribar = oReportes.re_pendientes_arribar(PROVEEDOR, SUCURSAL)
    End Function
    Public Function EntradasAlmacen(ByVal BODEGA As String, ByVal PROVEEDOR As Long, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal mercancia As Integer, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        EntradasAlmacen = oReportes.re_entradas_almacen(BODEGA, PROVEEDOR, DEPARTAMENTO, GRUPO, mercancia, fecha_ini, fecha_fin)
    End Function

    Public Function ExistenciasNuevo(ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal Todo As Boolean, ByVal Proveedor As Long, ByVal Tipo As Int16, ByVal CorteProveedor As Boolean) As Events
        ExistenciasNuevo = oReportes.re_existencias_nuevo(DEPARTAMENTO, GRUPO, Todo, Proveedor, Tipo, CorteProveedor)
    End Function
    'Public Function ExistenciasMultiple(ByVal BODEGA As String, ByVal DEPARTAMENTO As String, ByVal GRUPO As String, ByVal PROVEEDOR As String, ByVal Todo As Long, ByVal incluir_precios As Boolean, ByVal incluir_costos As Boolean, ByVal incluir_existencias As Boolean, ByVal incluir_series As Boolean) As Events
    '    ExistenciasMultiple = oReportes.re_existencias_multiple(BODEGA, DEPARTAMENTO, GRUPO, PROVEEDOR, Todo, incluir_precios, incluir_costos, incluir_existencias, incluir_series)
    'End Function
    Public Function ExistenciasMultipleNuevo(ByVal sucursal As Long, ByVal BODEGA As String, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal PROVEEDOR As Long, ByVal Todo As Boolean, ByVal incluir_precios As Boolean, ByVal incluir_costos As Boolean, ByVal incluir_existencias As Boolean, ByVal incluir_series As Boolean, ByVal Vendedor As Long) As Events
        ExistenciasMultipleNuevo = oReportes.re_existencias_multiplenuevo(sucursal, BODEGA, DEPARTAMENTO, GRUPO, PROVEEDOR, Todo, incluir_precios, incluir_costos, incluir_existencias, incluir_series, Vendedor)
    End Function
    Public Function DatosEmpresa() As Events
        DatosEmpresa = oReportes.sp_datos_empresa_sel()
    End Function
    Public Function MovimientosReferencia(ByVal CONCEPTO As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal desglosado As Long) As Events
        MovimientosReferencia = oReportes.re_movimientos_referencia(CONCEPTO, fecha_ini, fecha_fin, desglosado)
    End Function

    Public Function MovimientosNuevo(ByVal bodega As String, ByVal departamento As Long, ByVal grupo As Long, ByVal articulo As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        MovimientosNuevo = oReportes.re_movimientos_nuevo(bodega, departamento, grupo, articulo, fecha_ini, fecha_fin)
    End Function
    Public Function TraerMovimientosParaEnvio(ByVal convenio As Integer, ByVal quincena As Integer, ByVal anio As Integer, ByVal tipo As Char) As Events
        TraerMovimientosParaEnvio = oReportes.re_traer_movimientos_para_envio_dependencia(convenio, quincena, anio, tipo)
    End Function
    Public Function CosteoInventario(ByVal sucursal As Long, ByVal bodega As String, ByVal departamento As Long, ByVal GRUPO As Long, ByVal fecha As Date, ByVal desglosado As Long, Optional ByVal mercancia As Integer = -1, Optional ByVal ordenamiento As Integer = 1) As Events
        CosteoInventario = oReportes.re_valor_del_inventario(sucursal, bodega, departamento, GRUPO, fecha, desglosado, mercancia, ordenamiento)
    End Function
    Public Function ResumenSalidas(ByVal sucursal As Long, ByVal BODEGA As String, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal CONCEPTO As String, ByVal fechai As Date, ByVal fechaf As Date, ByVal tipo As Long, ByVal proveedor As Long, ByVal titulo As String, ByVal TipoNormales As String, ByVal tipomercancia As Int32) As Events
        ResumenSalidas = oReportes.re_resumensalidas(sucursal, BODEGA, DEPARTAMENTO, GRUPO, CONCEPTO, fechai, fechaf, tipo, proveedor, titulo, TipoNormales, tipomercancia)
    End Function

    Public Function ConfirmacionSaldosCobrador(ByVal Sucursal As Long, ByVal Cobrador As Long) As Events
        ConfirmacionSaldosCobrador = oReportes.re_confirmacion_saldos_cobrador(Sucursal, Cobrador)
    End Function

    Public Function ArticulosQuedados(ByVal bodega As String, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal fecha_ini As Date) As Events
        ArticulosQuedados = oReportes.re_articulos_quedados(bodega, DEPARTAMENTO, GRUPO, fecha_ini)
    End Function
    Public Function ArticulosPedidosFabrica(ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal PROVEEDOR As Long, ByVal incluido As Integer, ByVal fecha_ini As Date) As Events
        ArticulosPedidosFabrica = oReportes.re_articulos_pedidos_fabrica(DEPARTAMENTO, GRUPO, PROVEEDOR, incluido, fecha_ini)
    End Function
    Public Function SalidasPorTienda(ByVal CONCEPTO_VENTA As String, ByVal BODEGA As String, ByVal DEPARTAMENTO As String, ByVal GRUPO As String, ByVal mercancia As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        SalidasPorTienda = oReportes.re_salidas_por_tienda(CONCEPTO_VENTA, BODEGA, DEPARTAMENTO, GRUPO, mercancia, fecha_ini, fecha_fin)
    End Function
    Public Function devolucionMercancias(ByVal bodega As String, ByVal devolucion As Integer) As Events
        devolucionMercancias = oReportes.re_devolucion_mercancia(bodega, devolucion)
    End Function
    Public Function DevolucionEtiqueta(ByVal devolucion As Integer) As Events
        DevolucionEtiqueta = oReportes.sp_imprimir_etiquetas_devolucion_proveedor(devolucion)
    End Function



    Public Function ResumenProveedor(ByVal BODEGA As String, ByVal PROVEEDOR As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal DESGLOSADO As Boolean, ByVal mercancia As Long, ByVal concepto As String) As Events
        ResumenProveedor = oReportes.re_resumen_proveedor(BODEGA, PROVEEDOR, fecha_ini, fecha_fin, DESGLOSADO, mercancia, concepto)
    End Function
    Public Function ValeDeSalidaDeTraspasos(ByVal traspaso As Long) As Events
        ValeDeSalidaDeTraspasos = oReportes.re_traspasos_salidas(traspaso)
    End Function
    Public Function ValeDeEntradaDeTraspasos(ByVal traspasos As String) As Events
        ValeDeEntradaDeTraspasos = oReportes.re_traspasos_vale_entrada(traspasos)
    End Function
    Public Function EntradasAlmacenProceso(ByVal entrada As Long) As Events
        EntradasAlmacenProceso = oReportes.re_entradas_almacen_proceso(entrada)
    End Function
    Public Function Repartos(ByVal CHOFER As String, ByVal CLIENTE As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo_reporte As Long, ByVal Sucursal As Long, ByVal Repartir As Int16, ByVal SoloChoferesSucursal As Boolean) As Events
        Repartos = oReportes.re_repartos(CHOFER, CLIENTE, fecha_ini, fecha_fin, tipo_reporte, Sucursal, Repartir, SoloChoferesSucursal)
    End Function

    Public Function EnvioRepartos(ByVal fecha As Date, ByVal Sucursal As Long, ByVal turno As Int16) As Events
        EnvioRepartos = oReportes.re_envio_reparto(fecha, Sucursal, turno)
    End Function
    Public Function ValeDeReparto(ByVal reparto As Long, ByVal chofer As Boolean) As Events
        ValeDeReparto = oReportes.re_repartos_vale(reparto, chofer)
    End Function
    Public Function ListadoNotasDeCredito(ByVal sucursal As Long, ByVal fecha As Date) As Events
        ListadoNotasDeCredito = oReportes.re_ventas_credito_por_fecha(sucursal, fecha)
    End Function
    Public Function ListadoNotasDeCargo(ByVal sucursal As Long, ByVal fecha As Date) As Events
        'ListadoNotasDeCargo = oReportes.re_ventas_credito_por_fecha(sucursal, fecha)
    End Function
    Public Function ImprimeNotaDeCredito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, Optional ByVal iva_desglosado As Boolean = True) As Events
        ImprimeNotaDeCredito = oReportes.re_nota_de_credito(sucursal, concepto, serie, folio, cliente, iva_desglosado)
    End Function

    Public Function ImprimeNotaDeCreditoMultiple(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal cadena_folios As String, ByVal cadena_clientes As String) As Events
        ImprimeNotaDeCreditoMultiple = oReportes.re_nota_de_credito_multiple(sucursal, concepto, serie, cadena_folios, cadena_clientes)
    End Function
    Public Function ImprimeNotaDeCargo(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal iva_desglosado As Boolean, Optional ByVal ReimpresionCFS As Boolean = False) As Events
        ImprimeNotaDeCargo = oReportes.re_nota_de_cargo(sucursal, concepto, serie, folio, cliente, iva_desglosado, ReimpresionCFS)
    End Function
    Public Function ArticulosMasVendidos(ByVal sucursal As Long, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal cantidad As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal Proveedor As Long, ByVal Orden As Long, ByVal TipoOrden As Long) As Events
        ArticulosMasVendidos = oReportes.re_articulos_mas_vendidos(sucursal, DEPARTAMENTO, GRUPO, cantidad, fecha_ini, fecha_fin, Proveedor, Orden, TipoOrden)
    End Function

    Public Function ArticulosMasVendidosGrupo(ByVal sucursal As Long, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal cantidad As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal Proveedor As Long) As Events
        ArticulosMasVendidosGrupo = oReportes.re_articulos_mas_vendidos_grupo(sucursal, DEPARTAMENTO, GRUPO, cantidad, fecha_ini, fecha_fin, Proveedor)
    End Function

    Public Function EstadisticasVentas(ByVal Sucursal As Long, ByVal Bodega As String, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal proveedor As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        EstadisticasVentas = oReportes.re_estadisticas_ventas(Sucursal, Bodega, DEPARTAMENTO, GRUPO, proveedor, fecha_ini, fecha_fin)
    End Function
    Public Function NotasCanceladas(ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        NotasCanceladas = oReportes.re_notas_canceladas(fecha_ini, fecha_fin)
    End Function
    Public Function DiferenciaDePrecios(ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        DiferenciaDePrecios = oReportes.re_diferencia_de_precios(DEPARTAMENTO, GRUPO, fecha_ini, fecha_fin)
    End Function
    Public Function IngresosxCobrador(ByVal COBRADOR As Long, ByVal tipo_venta As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal cajero As Long, ByVal sucursal As Long, ByVal concentrado As Boolean, ByVal convenio As Long) As Events
        IngresosxCobrador = oReportes.re_ingresosxcobrador(COBRADOR, tipo_venta, fecha_ini, fecha_fin, cajero, sucursal, concentrado, convenio)
    End Function

    Public Function SolicitudCliente(ByVal cliente As Long) As Events
        SolicitudCliente = oReportes.re_clientes_sel(cliente)
    End Function
    Public Function EstadoCuenta(ByVal cliente As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal ampliado As Boolean, ByVal modo_cliente As Boolean, ByVal SaldoActual As Boolean, Optional ByVal sucursal As Long = -1, Optional ByVal convenio As Long = -1) As Events
        'JMARTINEZ 26Oct2006
        'Se agrega el modo cliente
        'Se quita el tipo_venta
        EstadoCuenta = oReportes.re_estado_cuenta(cliente, fecha_ini, fecha_fin, ampliado, modo_cliente, sucursal, SaldoActual, convenio)
    End Function

    Public Function RecordatorioSaldo(ByVal cliente As Long) As Events
        RecordatorioSaldo = oReportes.re_recordatorio_saldo(cliente)
    End Function

    Public Function EstadoCuentaProveedor(ByVal Proveedor As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        EstadoCuentaProveedor = oReportes.re_estado_cuenta_proveedor(Proveedor, fecha_ini, fecha_fin)
    End Function
    Public Function IngresosxFormaPago(ByVal sucursal As Long, ByVal CAJA As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal convenio As Long) As Events
        IngresosxFormaPago = oReportes.re_ingresos_por_forma_pago(sucursal, CAJA, fecha_ini, fecha_fin, convenio)
    End Function
    Public Function DiarioVentas(ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal canceladas As Boolean, ByVal sucursal As Long, Optional ByVal ventas_expo As Boolean = False, Optional ByVal convenio As Long = -1) As Events
        DiarioVentas = oReportes.re_diario_ventas(fecha_inicial, fecha_final, canceladas, sucursal, ventas_expo, convenio)
    End Function
    Public Function DiarioVentasDetallado(ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal canceladas As Boolean, ByVal sucursal As Long, Optional ByVal ventas_expo As Boolean = False, Optional ByVal convenio As Long = -1) As Events
        DiarioVentasDetallado = oReportes.re_diario_ventas_detallado(fecha_inicial, fecha_final, canceladas, sucursal, ventas_expo, convenio)
    End Function

    Public Function DepositosBancarios(ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal sucursal As Long) As Events
        DepositosBancarios = oReportes.re_depositos_bancarios(fecha_inicial, fecha_final, sucursal)
    End Function

    Public Function ResumenDeIngresos(ByVal CAJERO As Long, ByVal tipo_venta As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal sucursal As Long, ByVal SucursalCobrado As Long, ByVal convenio As Long) As Events
        ResumenDeIngresos = oReportes.re_resumen_ingresos(CAJERO, tipo_venta, fecha_ini, fecha_fin, sucursal, SucursalCobrado, convenio)
    End Function
    Public Function ResumenDeContabilidad(ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal sucursal As Long, ByVal convenio As Long) As Events
        ResumenDeContabilidad = oReportes.re_resumen_contabilidad(fecha_ini, fecha_fin, sucursal, convenio)
    End Function
    Public Function ImprimirEngancheContadoNotaCargo(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long) As Events
        ImprimirEngancheContadoNotaCargo = oReportes.re_movimientos_cobrar_detalle_cajas_sel(sucursal, concepto, serie, folio, cliente, documento)
    End Function
    Public Function Imprimirabono(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal serie_referencia As String, ByVal folio_referencia As Integer, ByVal documento_referencia As Integer) As Events
        Imprimirabono = oReportes.re_recibo_abonos(sucursal, concepto, serie, folio, cliente, serie_referencia, folio_referencia, documento_referencia)
    End Function
    Public Function ImprimirabonoTicket(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal serie_referencia As String, ByVal folio_referencia As Integer, ByVal documento_referencia As Integer) As Events
        ImprimirabonoTicket = oReportes.re_recibo_ticket(sucursal, concepto, serie, folio, cliente, serie_referencia, folio_referencia, documento_referencia)
    End Function
    Public Function ImprimirabonoTicketPromocion(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        ImprimirabonoTicketPromocion = oReportes.re_recibo_ticket_promocion(sucursal, concepto, serie, folio, cliente)
    End Function
    Public Function ImprimirabonoMenosTicket(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal serie_referencia As String, ByVal folio_referencia As Integer, ByVal documento_referencia As Integer) As Events
        ImprimirabonoMenosTicket = oReportes.re_recibo_menos(sucursal, serie, folio, cliente, serie_referencia, folio_referencia, documento_referencia)
    End Function
    Public Function AcumuladoClientes(ByVal NumClientes As Long, ByVal Fechai As Date, ByVal Fechaf As Date) As Events
        AcumuladoClientes = oReportes.re_acumulado_clientes(NumClientes, Fechai, Fechaf)
    End Function
    Public Function ImpresionEtiquetasPrecios(ByVal DEPARTAMENTO As String, ByVal GRUPO As String, ByVal opcion As Long, ByVal con_existencias As Boolean, ByVal tamanio_etiqueta_precios As String, ByVal BODEGA As String, ByVal entrada As Long, ByVal todos As Boolean, Optional ByVal ARTICULOS As String = "<Raiz />") As Events
        ImpresionEtiquetasPrecios = oReportes.re_impresion_etiquetas_precios(DEPARTAMENTO, GRUPO, opcion, con_existencias, tamanio_etiqueta_precios, BODEGA, entrada, todos, ARTICULOS)
    End Function
    Public Function ImpresionEtiquetasPrecios2(ByVal Entrada As Long) As Events
        ImpresionEtiquetasPrecios2 = oReportes.re_impresion_etiquetas_precios2(Entrada)
    End Function
    Public Function ArticulosKardex(ByVal bodega As String, ByVal articulo As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        ArticulosKardex = oReportes.re_articulos_kardex(bodega, articulo, fecha_ini, fecha_fin)
    End Function

    Public Function KardexMegaCred(ByVal codigo_barras As String) As Events
        KardexMegaCred = oReportes.re_kardex_megacred(codigo_barras)
    End Function

    Public Function ValeDeOrdenDeServicio(ByVal orden_servicio As Long) As Events
        ValeDeOrdenDeServicio = oReportes.re_ordenes_servicio_vale(orden_servicio)
    End Function
    Public Function EtiquetaDeOrdenDeServicio(ByVal orden_servicio As Long) As Events
        EtiquetaDeOrdenDeServicio = oReportes.re_ordenes_servicio_etiqueta(orden_servicio)
    End Function
    Public Function StocksDeArticulos(ByVal DEPARTAMENTO As Long, ByVal proveedor As Long, ByVal stock_minimo As Boolean) As Events
        StocksDeArticulos = oReportes.re_stocks_de_articulos(DEPARTAMENTO, proveedor, stock_minimo)
    End Function
    Public Function ClientesEnJuridicoPorEstatus(ByVal sucursal As String, ByVal abogado As String, ByVal status As String) As Events
        ClientesEnJuridicoPorEstatus = oReportes.re_clientes_en_juridico_por_estatus(sucursal, abogado, status)
    End Function
    Public Function AbonosClientesJuridico(ByVal sucursal As String, ByVal abogado As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        AbonosClientesJuridico = oReportes.re_abonos_de_clientes_en_juridico(sucursal, abogado, fecha_ini, fecha_fin)
    End Function
    Public Function ComisionesAbogados(ByVal sucursal As String, ByVal abogado As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        ComisionesAbogados = oReportes.re_comisiones_a_abogados(sucursal, abogado, fecha_ini, fecha_fin)
    End Function

#Region "Inventario Fisico"

    Public Function InventarioFisicoConteo1(ByVal inventario As Long) As Events
        InventarioFisicoConteo1 = oReportes.re_inventariofisico_conteo1(inventario)
    End Function
    Public Function InventarioFisicoConteo2(ByVal inventario As Long) As Events
        InventarioFisicoConteo2 = oReportes.re_inventariofisico_conteo2(inventario)
    End Function
    Public Function InventarioFisicoConteo1xUbicacion(ByVal inventario As Long) As Events
        InventarioFisicoConteo1xUbicacion = oReportes.re_inventariofisico_conteo1_xubica(inventario)
    End Function
    Public Function InventarioFisicoConteo2xUbicacion(ByVal inventario As Long) As Events
        InventarioFisicoConteo2xUbicacion = oReportes.re_inventariofisico_conteo2_xubica(inventario)
    End Function
    Public Function InventarioFisicoDiferencias(ByVal bodega As String, ByVal fecha As DateTime, ByVal inventario As Long) As Events
        InventarioFisicoDiferencias = oReportes.re_inventariofisico_diferencias(bodega, fecha, inventario)
    End Function
    Public Function InventarioFisicoDiferenciasxUbicacion(ByVal bodega As String, ByVal fecha As DateTime, ByVal inventario As Long) As Events
        InventarioFisicoDiferenciasxUbicacion = oReportes.re_inventariofisico_diferencias_xubica(bodega, fecha, inventario)
    End Function
#End Region

    Public Function TraspasosEntradasSalidas(ByVal bodega_origen As String, ByVal bodega_destino As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal desglosado As Boolean, ByVal solo_diferencias As Long) As Events
        TraspasosEntradasSalidas = oReportes.re_traspasos_entradas_salidas(bodega_origen, bodega_destino, fecha_ini, fecha_fin, desglosado, solo_diferencias)
    End Function
    'JAMP:10/Sep/2007 Cambio en filtros de Resumen de Entrada
    Public Function ResumenEntradas(ByVal SUCURSAL As Long, ByVal BODEGA As String, ByVal DEPARTAMENTO As Long, ByVal GRUPO As Long, ByVal PROVEEDOR As Long, ByVal concepto As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo_agrupado As Long, ByVal desglosado As Boolean, ByVal TipoProveedor As Char) As Events
        ResumenEntradas = oReportes.re_resumen_entradas(SUCURSAL, BODEGA, DEPARTAMENTO, GRUPO, PROVEEDOR, concepto, fecha_ini, fecha_fin, tipo_agrupado, desglosado, TipoProveedor)
    End Function
    Public Function InformeDeVentas(ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal menor_13 As Double, ByVal mayor_13 As Double, ByVal incluir_devoluciones As Boolean, ByVal sucursal As Long) As Events
        InformeDeVentas = oReportes.re_informe_de_ventas(fecha_ini, fecha_fin, menor_13, mayor_13, incluir_devoluciones, sucursal)
    End Function

    Public Function InformeDeVentas2014(ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal sucursal As Long) As Events
        InformeDeVentas2014 = oReportes.re_informe_de_ventas_2014(fecha_ini, fecha_fin, sucursal)
    End Function

    Public Function ComisionesDeVentas2014(ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal sucursal As Long, ByVal aplica_comision As Boolean) As Events
        ComisionesDeVentas2014 = oReportes.re_comisiones_de_ventas_2014(fecha_ini, fecha_fin, sucursal, aplica_comision)
    End Function

    Public Function SolicitudTraspasos(ByVal solicitud As Long) As Events
        SolicitudTraspasos = oReportes.re_solicitud_traspasos(solicitud)
    End Function
    Public Function OrdenesServicio(ByVal sucursal As Long, ByVal tipo_reporte As String, ByVal FECHA_INI As Date, ByVal FECHA_FIN As Date, ByVal CLIENTE As String, ByVal PROVEEDOR As String, ByVal CENTRO_SERVICIO As String, ByVal RECIBE As String, ByVal tipo_servicio As String, ByVal estatus As Char, ByVal FechaPromesa As Boolean) As Events
        OrdenesServicio = oReportes.re_ordenes_servicio(sucursal, tipo_reporte, FECHA_INI, FECHA_FIN, CLIENTE, PROVEEDOR, CENTRO_SERVICIO, RECIBE, tipo_servicio, estatus, FechaPromesa)
    End Function
    Public Function ArticulosConMasFallas(ByVal GRUPO As Long, ByVal cantidad As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal desglosado As Boolean, ByVal Sucursal As Long) As Events
        ArticulosConMasFallas = oReportes.re_articulos_con_mas_fallas(GRUPO, cantidad, fecha_ini, fecha_fin, desglosado, Sucursal)
    End Function
    Public Function ArticulosConCambioFisico(ByVal GRUPO As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal desglosado As Boolean, ByVal Sucursal As Long) As Events
        ArticulosConCambioFisico = oReportes.re_articulos_cambio_fisico(GRUPO, fecha_ini, fecha_fin, desglosado, Sucursal)
    End Function
    Public Function OrdenesServicioSinSolucion(ByVal FECHA_INI As Date, ByVal FECHA_FIN As Date, ByVal tipo_servicio As String, ByVal centro_servicio As Long, ByVal Sucursal As Long) As Events
        OrdenesServicioSinSolucion = oReportes.re_ordenes_servicio_sin_solucion(FECHA_INI, FECHA_FIN, tipo_servicio, centro_servicio, Sucursal)
    End Function


#Region "Saldos Generales"
    Public Function SaldosGenerales(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha As Date, ByVal tipo_venta As Char, ByVal desglosado As Boolean, ByVal cobrador As Long, ByVal convenio As Long) As Events
        SaldosGenerales = oReportes.re_saldos_generales_cobrador(sucursal, cliente_inicio, cliente_fin, fecha, tipo_venta, desglosado, cobrador, convenio)
    End Function
    Public Function SaldosGeneralesAgrupadoCobradores(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha As Date, ByVal tipo_venta As Char, ByVal desglosado As Boolean, ByVal cobrador As Long, ByVal convenio As Long) As Events
        SaldosGeneralesAgrupadoCobradores = oReportes.re_saldos_generales_agrupado(sucursal, cliente_inicio, cliente_fin, fecha, tipo_venta, desglosado, cobrador, convenio)
    End Function
    Public Function SaldosGeneralesDesglosados(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha As Date, ByVal tipo_venta As Char, ByVal desglosado As Boolean, ByVal cobrador As Long, ByVal convenio As Long) As Events
        SaldosGeneralesDesglosados = oReportes.re_saldos_generales_desglosado(sucursal, cliente_inicio, cliente_fin, fecha, tipo_venta, desglosado, cobrador, convenio)
    End Function
#End Region

    Public Function MovimientosClientes(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha_inicio As Date, ByVal fecha_fin As Date, ByVal tipo_venta As Integer, ByVal cobrador As Long, ByVal convenio As Long)
        MovimientosClientes = oReportes.re_movimientos_clientes(sucursal, cliente_inicio, cliente_fin, fecha_inicio, fecha_fin, tipo_venta, cobrador, convenio)
    End Function

    Public Function DocumentosxAño(ByVal sucursal As Integer, ByVal año As Long, ByVal tipo_venta As Integer, ByVal alafecha As DateTime)
        DocumentosxAño = oReportes.re_documentos_por_anio(sucursal, año, tipo_venta, alafecha)
    End Function

    Public Function SaldosVencidos(ByVal sucursal As Integer, ByVal cliente_desde As Integer, ByVal cliente_hasta As Integer, ByVal fecha_vencimiento As Date, ByVal tipo As Integer, ByVal plan_credito As Integer, ByVal cobrador As String, ByVal sin_abono As Byte, ByVal falten_liquidar As Long, ByVal meses_sin_abonar As Long, ByVal para_cliente As Boolean, ByVal imprimir_direccion As Boolean, ByVal Convenio As Long)
        SaldosVencidos = oReportes.re_saldos_vencidos(sucursal, cliente_desde, cliente_hasta, fecha_vencimiento, tipo, plan_credito, cobrador, sin_abono, falten_liquidar, meses_sin_abonar, para_cliente, imprimir_direccion, Convenio)
    End Function
    Public Function ResumenMovimientos(ByVal sucursal As Integer, ByVal concepto As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo_venta As Integer, ByVal convenio As Long)
        ResumenMovimientos = oReportes.re_resumen_movimientos(sucursal, concepto, fecha_ini, fecha_fin, tipo_venta, convenio)
    End Function
    Public Function DocumentosVencer(ByVal sucursal As Long, ByVal cliente As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo_venta As Integer, ByVal cobrador As Long, ByVal tipo_vencimiento As Long, ByVal convenio As Long)
        DocumentosVencer = oReportes.re_documentos_vencer(sucursal, cliente, fecha_ini, fecha_fin, tipo_venta, cobrador, tipo_vencimiento, convenio)
    End Function
    Public Function DocumentosVencerEjcutivo(ByVal cliente As Long, ByVal tipo_vencimiento As Long)
        DocumentosVencerEjcutivo = oReportes.re_documentos_vencer_ejecutivo(cliente, tipo_vencimiento)
    End Function
    Public Function Consolidado(ByVal cliente As Long, ByVal tipo_vencimiento As Long)
        Consolidado = oReportes.re_consolidado(cliente, tipo_vencimiento)
    End Function


    Public Function AnualClientes(ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo_venta As Integer, ByVal sucursal_actual As Long) As Events
        AnualClientes = oReportes.re_reporte_anual(cliente_inicio, cliente_fin, fecha_ini, fecha_fin, tipo_venta, sucursal_actual)
    End Function
    Public Function AuxiliarFacturas(ByVal sucursal As Integer, ByVal cliente As Integer, ByVal convenio As Long) As Events
        AuxiliarFacturas = oReportes.re_auxiliar_factura(sucursal, cliente, convenio)
    End Function
    Public Function VentasConSaldo(ByVal sucursal As Long, ByVal cobrador As Long, ByVal tipo_venta As Integer, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal saldos_vencidos As Boolean, ByVal sin_abono As Boolean, ByVal numero_abonos As Long, ByVal desglosado As Boolean, ByVal convenio As Long) As Events
        VentasConSaldo = oReportes.re_ventas_con_saldos(sucursal, cobrador, tipo_venta, fecha_ini, fecha_fin, saldos_vencidos, sin_abono, numero_abonos, desglosado, convenio)
    End Function
    Public Function MercanciaEnTransito(ByVal bodega As String) As Events
        MercanciaEnTransito = oReportes.re_mercancia_en_transito(bodega)
    End Function
    Public Function PagosFechaVencimiento(ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal tipo_movimiento As Long, ByVal tipo_estatus As Long) As Events
        PagosFechaVencimiento = oReportes.re_pagos_fecha_vencimiento(fecha_ini, fecha_fin, tipo_movimiento, tipo_estatus)
    End Function
    Public Function VencimientosCxP(ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal tipo_movimiento As Long, ByVal proveedor As Long) As Events
        VencimientosCxP = oReportes.re_vencimientos_cxp(fecha_ini, fecha_fin, tipo_movimiento, proveedor)
    End Function

#Region "Movimientos para Impresion"

    Public Function MovimientosProgramacionPagos(ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime) As Events
        MovimientosProgramacionPagos = oReportes.sp_movimientos_programacion_pagos(fecha_ini, fecha_fin)
    End Function

    Public Function MovimientosImpresionCheque(ByVal banco As Long, ByVal cuentabancaria As String, ByVal Tipo As Long) As Events
        MovimientosImpresionCheque = oReportes.sp_tipos_movimientos(banco, cuentabancaria, Tipo)
    End Function

    Public Function ActualizaProgramacionChequeraTipoMovimiento(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_movimiento As Long, ByVal folio_cheque As Long, ByVal concepto As String, ByVal Tipo As Long) As Events
        ActualizaProgramacionChequeraTipoMovimiento = oReportes.sp_actualiza_programacion_chequera_tipo_movimientos(banco, cuentabancaria, folio_movimiento, concepto, Tipo)
    End Function

    Public Function ActualizaChequeTipoMovimiento(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_movimiento As Long, ByVal folio_cheque As Long, ByVal concepto As String, ByVal Tipo As Long) As Events
        ActualizaChequeTipoMovimiento = oReportes.sp_actualiza_cheque_tipo_movimientos(banco, cuentabancaria, folio_movimiento, folio_cheque, concepto, Tipo)
    End Function


#End Region

#Region "Impresion de Cheques"

    Public Function ImprimeChequeAcreedoresDiversos(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Integer) As Events
        ImprimeChequeAcreedoresDiversos = oReportes.re_imprime_cheque_acreedores_diversos(banco, cuentabancaria, folio)
    End Function
    Public Function ImprimeChequeMovimientosPagar(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Integer) As Events
        ImprimeChequeMovimientosPagar = oReportes.re_imprime_cheque_movimientos_pagar(banco, cuentabancaria, folio)
    End Function
    Public Function ImprimeChequeMovimientosChequera(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_cheque As Integer) As Events
        ImprimeChequeMovimientosChequera = oReportes.re_imprime_cheque_movimientos_chequera(banco, cuentabancaria, folio_cheque)
    End Function
    Public Function ImprimeChequePagoFacturasProveedores(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_cheque As Integer) As Events
        ImprimeChequePagoFacturasProveedores = oReportes.re_imprime_cheque_pago_facturas_proveedores(banco, cuentabancaria, folio_cheque)
    End Function

#End Region

#Region " Antiguedades de Saldos"

    Public Function AntiguedadesSaldosDesglosado(ByVal proveedor As Long, ByVal fecha_corte As Date) As Events
        AntiguedadesSaldosDesglosado = oReportes.re_antiguedades_saldos_desglosado(proveedor, fecha_corte)
    End Function
    Public Function AntiguedadesSaldos(ByVal proveedor As Long, ByVal fecha_corte As Date) As Events
        AntiguedadesSaldos = oReportes.re_antiguedades_saldos(proveedor, fecha_corte)
    End Function

#End Region

#Region " Antiguedades de Saldos Cliente"

    Public Function AntiguedadesSaldosClienteDesglosado(ByVal Sucursal As Long, ByVal cliente As Long, ByVal fecha_corte As Date, ByVal Cobrador As Long, ByVal Vencido As Long, ByVal SoloSaldosVencidos As Boolean, ByVal convenio As Long) As Events
        AntiguedadesSaldosClienteDesglosado = oReportes.re_antiguedades_saldos_cliente_desglosado(Sucursal, cliente, fecha_corte, Cobrador, Vencido, SoloSaldosVencidos, convenio)
    End Function
    Public Function AntiguedadesSaldosCliente(ByVal Sucursal As Long, ByVal cliente As Long, ByVal fecha_corte As Date, ByVal Cobrador As Long, ByVal Vencido As Long, ByVal SoloSaldosVencidos As Boolean, ByVal convenio As Long) As Events
        AntiguedadesSaldosCliente = oReportes.re_antiguedades_saldos_cliente(Sucursal, cliente, fecha_corte, Cobrador, Vencido, SoloSaldosVencidos, convenio)
    End Function

#End Region

    Public Function MercanciaPendienteEntrega(ByVal sucursal As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime) As Events
        MercanciaPendienteEntrega = oReportes.re_mercancia_pendiente_entrega(sucursal, fecha_ini, fecha_fin)
    End Function
    Public Function SugerenciaCobro(ByVal sucursal As Long, ByVal fecha_fin As DateTime, ByVal desglosado As Boolean, ByVal intTipoVenta As Integer, ByVal cobrador As Long) As Events
        SugerenciaCobro = oReportes.re_sugerencia_cobro(sucursal, fecha_fin, desglosado, intTipoVenta, cobrador)
    End Function
    Public Function SugerenciaCobroProximasVisitas(ByVal sucursal As Long, ByVal fecha_fin As DateTime, ByVal desglosado As Boolean, ByVal intTipoVenta As Integer, ByVal cobrador As Long) As Events
        SugerenciaCobroProximasVisitas = oReportes.re_sugerencia_cobro_proximas_visitas(sucursal, fecha_fin, desglosado, intTipoVenta, cobrador)
    End Function
    Public Function PagosEfectivo(ByVal sucursal As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime, ByVal cantidad As Long) As Events
        PagosEfectivo = oReportes.re_pagos_efectivo_clientes(sucursal, fecha_ini, fecha_fin, cantidad)
    End Function
    Public Function MovimientosChequera(ByVal chequera As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        MovimientosChequera = oReportes.re_movimientos_chequera(chequera, fecha_ini, fecha_fin)
    End Function

    Public Function Bonificaciones(ByVal sucursal As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal Tipo As Char) As Events
        Bonificaciones = oReportes.re_bonificaciones(sucursal, fecha_inicial, fecha_final, Tipo)
    End Function

#Region " Relacion de Pago a Proveedores"

    Public Function TraerRelacionPagoProveedores(ByVal fecha As Date) As Events
        TraerRelacionPagoProveedores = oReportes.sp_traer_relacion_pago_proveedores(fecha)
    End Function
    Public Function RelacionPagoProveedores(ByVal fecha As Date, ByVal desglosado As Boolean) As Events
        RelacionPagoProveedores = oReportes.re_relacion_pago_proveedores(fecha, desglosado)
    End Function

#End Region

#Region "Polizas de Contabilidad"


#Region " Póliza de Diario de Ventas "
    Public Function PolizaDiarioVentasEnajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Long) As Events
        PolizaDiarioVentasEnajenacion = oReportes.re_poliza_diario_ventas_enajenacion(sucursal, fecha, llena_poliza, poliza)
    End Function
    Public Function PolizaDiarioVentasNormales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaDiarioVentasNormales = oReportes.re_poliza_diario_ventas_normales(sucursal, fecha, llena_poliza, poliza)
    End Function

#End Region

#Region " Poliza de Cobranza de Intereses "
    Public Function PolizaCobranzaInteresesEnajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaCobranzaInteresesEnajenacion = oReportes.re_poliza_cobranza_intereses_enajenacion(sucursal, fecha, llena_poliza, poliza)
    End Function

    Public Function PolizaCobranzaInteresesNormales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaCobranzaInteresesNormales = oReportes.re_poliza_cobranza_intereses_normales(sucursal, fecha, llena_poliza, poliza)
    End Function

#End Region


#Region " Poliza de Ingresos por Intereses de Enajenación"
    Public Function PolizaIngresosInteresesEnajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaIngresosInteresesEnajenacion = oReportes.re_poliza_ingresos_intereses_enajenacion(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza de Ingresos por Intereses de Normales"
    Public Function PolizaIngresosInteresesNormales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaIngresosInteresesNormales = oReportes.re_poliza_ingresos_intereses_normales(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza de Descuento por Pronto Pago Normal"
    Public Function PolizaDescuentoProntoPagoNormales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaDescuentoProntoPagoNormales = oReportes.re_poliza_descuento_por_pronto_pago_normales(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza de Descuento por Pronto Pago Enajenación"
    Public Function PolizaDescuentoProntoPagoEnajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaDescuentoProntoPagoEnajenacion = oReportes.re_poliza_descuento_por_pronto_pago_enajenacion(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region


#Region " Poliza Abono Cliente Pago Puntual"
    Public Function PolizaAbonoClientePagoEnajenacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaAbonoClientePagoEnajenacion = oReportes.re_poliza_abono_cliente_pago_enajenacion(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza Abono Cliente Pago Puntual y Contado"
    Public Function PolizaAbonoClientePagoPuntualNormales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaAbonoClientePagoPuntualNormales = oReportes.re_poliza_abono_cliente_pago_normales(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza Compras Personas Morales"
    Public Function PolizaComprasPersonasMorales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaComprasPersonasMorales = oReportes.re_poliza_compras_personas_morales(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza Compras Personas Fisicas"
    Public Function PolizaComprasPersonasFisicas(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaComprasPersonasFisicas = oReportes.re_poliza_compras_personas_fisicas(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza Compras Personas Morales"
    Public Function PolizaDevolucionesProveedor(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaDevolucionesProveedor = oReportes.re_poliza_devoluciones_proveeedor(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza Traspasos"
    Public Function PolizaTraspasos(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaTraspasos = oReportes.re_poliza_traspasos(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region



#Region " Poliza DescuentosVentasVolumen"
    Public Function PolizaDescuentosVentasVolumen(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaDescuentosVentasVolumen = oReportes.re_poliza_descuentos_ventas_volumen(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region


#Region " Poliza DescuentosVentasConvenio"
    Public Function PolizaDescuentosVentasConvenio(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaDescuentosVentasConvenio = oReportes.re_poliza_descuentos_ventas_convenio(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region




#Region " Poliza CancelacionCargo"
    Public Function PolizaCancelacionInteresesEspeciales(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaCancelacionInteresesEspeciales = oReportes.re_poliza_cancelacion_intereses_especiales(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza PolizaNotaCreditoBonificacion"
    Public Function PolizaNotaCreditoBonificacion(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaNotaCreditoBonificacion = oReportes.re_poliza_nota_credito_bonificacion_precio_tasa_iva(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

#Region " Poliza Poliza Fletes Otros Gastos"
    Public Function PolizaFletesOtrosGastos(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        PolizaFletesOtrosGastos = oReportes.re_poliza_fletes_otros_gastos(sucursal, fecha, llena_poliza, poliza)
    End Function
#End Region

    Public Function EjecutarPoliza(ByVal sucursal As Long, ByVal fecha As Date, ByVal llena_poliza As Boolean, ByVal poliza As Integer) As Events
        EjecutarPoliza = oReportes.re_ejecuta_polizas(sucursal, fecha, llena_poliza, poliza)
    End Function
    Public Function EjecutarPolizaFlete(ByVal entrada As Long, ByVal fecha As DateTime, ByVal llena_poliza As Boolean, ByVal sucursal As Long) As Events
        EjecutarPolizaFlete = oReportes.re_ejecuta_polizasFlete(entrada, fecha, llena_poliza, sucursal)
    End Function
    Public Function LookupPoliza(ByVal sucursal As Long, Optional ByVal flete As Long = -1) As Events
        LookupPoliza = oReportes.sp_polizas_grl(flete, sucursal)
    End Function


#End Region




#Region "Validaciones"

    'Public Function Validacion(ByVal sucursal As Long, optional ByVal Cobrador As Long) As Events
    '    Validacion = ValidaSucursal(Sucursal)
    '    If Validacion.ErrorFound Then Exit Function
    '    Validacion = ValidaCobrador(Sucursal)
    '    If Validacion.ErrorFound Then Exit Function
    'End Function

    Public Function Validacion(ByVal sucursal As Long, ByVal poliza As Long, ByVal fecha As String) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPoliza(poliza)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal sucursal As Long, ByVal fecha As String) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal sucursal As Long, ByVal cliente_inicio As Long, ByVal cliente_fin As Long) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaClientes(cliente_inicio, cliente_fin)
        'If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal sucursal As Long, ByVal fecha As Date) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechaCorte(fecha)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal fecha As String) As Events
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal fecha As Date) As Events
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal sucursal As Long, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechas(CDate(fecha_ini), CDate(fecha_fin))
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal concepto As Events, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        Validacion = concepto
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function
    End Function
    '@ACH-25/06/07: Se modificó para que evaluar varios bancos y chequeras
    Public Function Validacion(ByVal banco As String, ByVal chequera As String, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        Validacion = ValidaBanco(banco)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaChequera(chequera)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechas(fecha_ini, fecha_fin)
        If Validacion.ErrorFound Then Exit Function
    End Function
    '/@ACH-25/06/07
    Public Function Validacion(ByVal Bodega As Events, ByVal Departamento As Events, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        Validacion = Bodega
        If Validacion.ErrorFound Then Exit Function
        Validacion = Departamento
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal Departamento As Events, ByVal Grupo As Events, ByVal fecha_ini As String) As Events
        Validacion = Departamento
        If Validacion.ErrorFound Then Exit Function
        Validacion = Grupo
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal Proveedor As Events) As Events
        Validacion = Proveedor
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal Departamento As Events, ByVal Grupo As Events) As Events
        Validacion = Departamento
        If Validacion.ErrorFound Then Exit Function
        Validacion = Grupo
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal Bodega As Events, ByVal Proveedor As Events, ByVal Grupo As Events, ByVal Departamento As Events, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        Validacion = Bodega
        If Validacion.ErrorFound Then Exit Function
        Validacion = Departamento
        If Validacion.ErrorFound Then Exit Function
        Validacion = Grupo
        If Validacion.ErrorFound Then Exit Function
        Validacion = Proveedor
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function

    End Function
    '*****************
    Public Function Validacion(ByVal Bodega As Events, ByVal Grupo As Events, ByVal Departamento As Events, ByVal ARTICULO As Events, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        Validacion = Bodega
        If Validacion.ErrorFound Then Exit Function
        Validacion = Departamento
        If Validacion.ErrorFound Then Exit Function
        Validacion = Grupo
        If Validacion.ErrorFound Then Exit Function
        Validacion = ARTICULO
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function

    End Function
    '*****************
    Public Function Validacion(ByVal Sucursal As Events, ByVal Bodega As Events, ByVal Departamento As Events, ByVal Grupo As Events, ByVal Concepto As Events, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        Validacion = Sucursal
        If Validacion.ErrorFound Then Exit Function
        Validacion = Bodega
        If Validacion.ErrorFound Then Exit Function
        Validacion = Departamento
        If Validacion.ErrorFound Then Exit Function
        Validacion = Grupo
        If Validacion.ErrorFound Then Exit Function
        Validacion = Concepto
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function Validacion(ByVal fecha_ini As String, ByVal fecha_fin As String, Optional ByVal ValidarMismoAño As Boolean = False) As Events
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function

        If ValidarMismoAño Then
            Validacion = ValidaMismoAño(fecha_ini, fecha_fin)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function



    Public Function Validacion(ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechas(fecha_ini, fecha_fin)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal cantidad As Long) As Events
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechas(fecha_ini, fecha_fin)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(cantidad)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal sucursal As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_ini, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechas(fecha_ini, fecha_fin)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal Proveedor As Long, ByVal Departamento As Events)
        Validacion = ValidaProveedor(Proveedor)
        If Validacion.ErrorFound Then Exit Function
        Validacion = Departamento
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal sucursal As Long, Optional ByVal proveedor As Long = 0) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function

        If proveedor <> 0 Then
            Validacion = ValidaProveedor(proveedor)
            If Validacion.ErrorFound Then Exit Function
        End If


    End Function
    Public Function Validacion(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal plan_credito As String) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaClientes(cliente_inicio, cliente_fin)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPlanCredito(plan_credito)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Public Function Validacion(ByVal sucursal As Long, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal fecha_inicio As Date, ByVal fecha_fin As Date) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaClientes(cliente_inicio, cliente_fin)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_inicio, False)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_fin, True)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechas(fecha_inicio, fecha_fin)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal concepto As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date) As Events
        'Validacion = ValidaSucursal(sucursal)
        'If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConcepto(concepto)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechas(fecha_ini, fecha_fin)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Public Function Validacion_etiquetas(ByVal tamanio_etiqueta_precios As Long) As Events
        Validacion_etiquetas = ValidaEtiquetas(tamanio_etiqueta_precios)
        If Validacion_etiquetas.ErrorFound Then Exit Function
    End Function
    Public Function ValidacionAntiguedadSaldos(ByVal fecha As String, ByVal cliente As Long) As Events

        ValidacionAntiguedadSaldos = ValidaCliente(cliente)
        If ValidacionAntiguedadSaldos.ErrorFound Then Exit Function
        ValidacionAntiguedadSaldos = ValidaFecha(fecha)
        If ValidacionAntiguedadSaldos.ErrorFound Then Exit Function

    End Function
    'Public Function ValidacionSucursalAbogado(ByVal sucursal As Long, ByVal abogado As Long) As Events

    '    ValidacionSucursalAbogado = ValidaSucursal(sucursal)
    '    If ValidacionSucursalAbogado.ErrorFound Then Exit Function
    '    ValidacionSucursalAbogado = ValidaAbogado(abogado)
    '    If ValidacionSucursalAbogado.ErrorFound Then Exit Function

    'End Function


    Public Function ValidaMismoAño(ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime) As Events
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If fecha_inicial.Year <> fecha_final.Year Then
            oEvent.Message = "El Rango de Fechas deben ser dentro del mismo Año"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidacionArticulosIncluidos(ByVal articulos_incluidos As Long) As Events
        ValidacionArticulosIncluidos = ValidaArticulosIncluidos(articulos_incluidos)
        If ValidacionArticulosIncluidos.ErrorFound Then Exit Function
    End Function
    Public Function ValidaArticulosIncluidos(ByVal articulos_incluidos As Long)
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If articulos_incluidos <= 0 Or IsNumeric(articulos_incluidos) = False Then
            oEvent.Message = "Debe incluir al menos un artículo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaEtiquetas(ByVal tamanio_etiqueta_precios As Long)
        Dim oEvent As New Events
        If tamanio_etiqueta_precios <= 0 Or IsNumeric(tamanio_etiqueta_precios) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Artículo No Tiene un Tamaño de Etiqueta Asignado"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function Validaciones(ByVal cliente_ini As Long, ByVal cliente_fin As Long, ByVal anio As String) As Events
        Validaciones = ValidaClientes(cliente_ini, cliente_fin)
        If Validaciones.ErrorFound Then Exit Function
        Validaciones = ValidaFechaAnual(anio)
        If Validaciones.ErrorFound Then Exit Function

    End Function
    Public Function Validacion(ByVal sucursal As Integer, ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer, ByVal plan_credito As String, ByVal falten_liquidar As Integer, ByVal meses_sin_abonar As Integer) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaClientes(cliente_inicio, cliente_fin)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPlanCredito(plan_credito)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFaltenLiquidar(falten_liquidar)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMesesSinAbonar(meses_sin_abonar)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Public Function ValidacionAuxiliarFacturas(ByVal sucursal As Long, ByVal cliente As Long) As Events
        ValidacionAuxiliarFacturas = ValidaSucursal(sucursal)
        If ValidacionAuxiliarFacturas.ErrorFound Then Exit Function
        ValidacionAuxiliarFacturas = ValidaCliente(cliente)
        If ValidacionAuxiliarFacturas.ErrorFound Then Exit Function

    End Function
    Public Function ValidacionVentaConSaldo(ByVal sucursal As Long, ByVal fecha As String) As Events
        ValidacionVentaConSaldo = ValidaSucursal(sucursal)
        If ValidacionVentaConSaldo.ErrorFound Then Exit Function
        ValidacionVentaConSaldo = ValidaFecha(fecha)
        If ValidacionVentaConSaldo.ErrorFound Then Exit Function
    End Function
    Public Function ValidaSucursal(ByVal sucursal As String)
        Dim oEvent As New Events
        If sucursal.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal sucursal As Long)
        Dim oEvent As New Events
        If sucursal <= 0 Or IsNumeric(sucursal) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCobrador(ByVal Cobrador As Long)
        Dim oEvent As New Events
        If Cobrador <= 0 Or IsNumeric(Cobrador) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cobrador es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNumeroAbonos(ByVal numero_abonos As Long)
        Dim oEvent As New Events
        If numero_abonos <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Número de Abonos es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConcepto(ByVal concepto As String)
        Dim oEvent As New Events
        If concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPlanCredito(ByVal plan_credito As String)
        Dim oEvent As New Events
        If plan_credito.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Plan de Credito es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFechaCorte(ByVal fecha As String) As Events
        Dim oEvent As New Events
        Dim tipo_fecha As String
        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + " es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + " esta fuera del rango válido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent


    End Function
    Public Function ValidaFecha(ByVal fecha As String, ByVal final As Boolean) As Events
        Dim oEvent As New Events
        Dim tipo_fecha As String

        If final Then
            tipo_fecha = "Final"
        Else
            tipo_fecha = "Inicial"
        End If

        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + tipo_fecha + " es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + tipo_fecha + " esta fuera del rango válido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Public Function ValidaTamañoEtiquetaPrecios(ByVal tamaño As String)
        Dim oEvent As New Events
        If tamaño.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Tamaño de la Etiqueta de Precios es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    'Public Function ValidaProveedor(ByVal Proveedor As String)
    '    Dim oEvent As New Events
    '    If Proveedor.Trim.Length <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "El Proveedor es Requerido"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    Public Function ValidaProveedor(ByVal Proveedor As Long)
        Dim oEvent As New Events
        If Proveedor <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCliente(ByVal cliente As Long)
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If cliente <= 0 Then
            oEvent.Message = "El Cliente Es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaClientes(ByVal cliente_inicio As Integer, ByVal cliente_fin As Integer)
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If cliente_inicio <= 0 Then
            oEvent.Message = "El Cliente Desde: Es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        If cliente_fin <= 0 Then
            oEvent.Message = "El Cliente Hasta: Es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        If cliente_inicio > cliente_fin Then
            oEvent.Message = "El Cliente Hasta: Debe Ser Mayor o Igual Que El Cliente Desde:"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFechas(ByVal fecha_inicio As Date, ByVal fecha_fin As Date)
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If fecha_inicio > fecha_fin Then
            oEvent.Message = "La Fecha de Fin Debe Ser Mayor o Igual Que La Fecha de Inicio"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaAño(ByVal año As Long)
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If año > 0 And (año < 999 Or año > 2020) Then
            oEvent.Message = "El año no es valido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaFechaAnual(ByVal inicial As String)
        Dim oEvent As New Events

        If Not IsNumeric(inicial) Then

            oEvent.Ex = Nothing
            oEvent.Message = "El año es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFaltenLiquidar(ByVal falten_liquidar As Long)
        Dim oEvent As New Events
        If falten_liquidar < 0 Or IsNumeric(falten_liquidar) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los documentos faltantes por liquidar son Requeridos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMesesSinAbonar(ByVal meses_sin_abonar As Long)
        Dim oEvent As New Events
        If meses_sin_abonar < 0 Or IsNumeric(meses_sin_abonar) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los Meses sin Abonar son Requeridos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBanco(ByVal banco As String)
        Dim oEvent As New Events
        If banco.Length <= 8 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Banco es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPoliza(ByVal poliza As Long)
        Dim oEvent As New Events
        If poliza < 0 Or IsNumeric(poliza) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Póliza es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaChequera(ByVal chequera As String)
        Dim oEvent As New Events
        If chequera.Length <= 8 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Chequera es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events
        'Dim fecha As String
        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + " es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + " esta fuera del rango válido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Public Function ImprimeReportePagoFacturas(ByVal fecha As DateTime, ByVal folio_cheque As Integer) As Events
        ImprimeReportePagoFacturas = oReportes.re_impresion_pago_facturas_proveedores(fecha, folio_cheque)
    End Function
    Public Function ValidaAbogado(ByVal Abogado As Long)
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If Abogado <= 0 Or IsNumeric(Abogado) = False Then
            oEvent.Message = "El Abogado Es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCantidad(ByVal cantidad As Long)
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If cantidad <= 0 Or IsNumeric(cantidad) = False Then
            oEvent.Message = "La Cantidad Es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaBodega(ByVal bodega As String) As Events
        Dim oEvent As New Events
        oEvent.Ex = Nothing
        If bodega = "-1" Then
            oEvent.Message = "La Bodega Es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region


    Public Function ResumenSalidasDetallado(ByVal bodega As String, ByVal departamento As Long, ByVal grupo As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo As Long) As Events
        ResumenSalidasDetallado = oReportes.re_resumensalidasdetallado(bodega, departamento, grupo, fecha_ini, fecha_fin, tipo)
    End Function
    Public Function ImprimeCorrespondenciaClientes(ByVal cliente As Long) As Events
        ImprimeCorrespondenciaClientes = oReportes.re_imprime_correspndencia(cliente)
    End Function
    Public Function FacturasCanceladas(ByVal sucursal As Long, ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal tipo As Char, ByVal convenio As Long, ByVal facturas_timbradas As Boolean) As Events
        FacturasCanceladas = oReportes.re_facturas_canceladas(sucursal, fecha_ini, fecha_fin, tipo, convenio, facturas_timbradas)
    End Function
    Public Function FacturasSurtidasSobrepedido(ByVal entrada As Long) As Events
        FacturasSurtidasSobrepedido = oReportes.re_facturas_surtidas_sobrepedido(entrada)
    End Function
    Public Function CuentasNoLocalizables(ByVal sucursal As Long) As Events
        CuentasNoLocalizables = oReportes.re_cuentas_no_localizables(sucursal)
    End Function
    Public Function AbonosCancelados(ByVal sucursal As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime) As Events
        AbonosCancelados = oReportes.re_abonos_cancelados(sucursal, fecha_ini, fecha_fin)
    End Function

    Public Function VentasCobradas(ByVal sucursal As Long, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime) As Events
        VentasCobradas = oReportes.re_ventas_cobradas(sucursal, fecha_ini, fecha_fin)
    End Function


    Public Function SeriesArticulos(ByVal departamento As Long, ByVal grupo As Long) As Events
        SeriesArticulos = oReportes.re_series_articulos(departamento, grupo)
    End Function
    Public Function Beneficiarios(ByVal beneficiario_inicial As String, ByVal beneficiario_final As String, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal Cuenta_Bancaria As String) As Events
        Beneficiarios = oReportes.re_beneficiarios(beneficiario_inicial, beneficiario_final, fecha_inicial, fecha_final, Cuenta_Bancaria)
    End Function
    Public Function MovimientosNoAplicados(ByVal proveedor As Long) As Events
        MovimientosNoAplicados = oReportes.re_movimientos_no_aplicados(proveedor)
    End Function
    Public Function MercanciasTransito(ByVal bodega As String, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime) As Events
        MercanciasTransito = oReportes.re_mercancias_transito(bodega, fecha_inicial, fecha_final)
    End Function
    Public Function MercanciasEntregar(ByVal sucursal As String, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime) As Events
        MercanciasEntregar = oReportes.re_mercancia_pendiente_entrega(sucursal, fecha_inicial, fecha_final)
    End Function
    Public Function CambiosPrecios(ByVal Sucursal As Long, ByVal Departamento As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal Desglosado As Boolean, ByVal Precio As Long, ByVal Proveedor As Long) As Events
        CambiosPrecios = oReportes.re_cambios_precios(Sucursal, Departamento, fecha_inicial, fecha_final, Desglosado, Precio, Proveedor)
    End Function
    Public Function Cotizaciones(ByVal cotizacion As Long) As Events
        Cotizaciones = oReportes.re_cotizaciones(cotizacion)
    End Function
    Public Function FletesOtrosCargos(ByVal sucursal As Long, ByVal incluido_factura As Boolean, ByVal desde As Date, ByVal hasta As Date) As Events
        FletesOtrosCargos = oReportes.re_fletes_otroscargos(sucursal, incluido_factura, desde, hasta)
    End Function
    Public Function VentasDiarias(ByVal mes As String, ByVal anio As String, ByVal desglosado As Boolean) As Events
        VentasDiarias = oReportes.re_ventas_diarias(mes, anio, desglosado)
    End Function

#Region "Vistas"

    Public Function SalidasVistas(ByVal folio_vista_salida As Long) As Events
        SalidasVistas = oReportes.re_vistas_salidas(folio_vista_salida)
    End Function
    Public Function EntradaVistas(ByVal folio_vista_entrada As Long) As Events
        EntradaVistas = oReportes.re_vistas_entradas(folio_vista_entrada)
    End Function
#End Region


    Public Function MercanciaEtiquetada(ByVal bodega As String, ByVal sucursal As Long) As Events
        MercanciaEtiquetada = oReportes.re_mercancia_etiquetada(bodega, sucursal)
    End Function

    Public Function VentasxFechaEntrega(ByVal sucursal As Long, ByVal fecha_entrega As DateTime, ByVal horario As Char) As Events
        VentasxFechaEntrega = oReportes.re_ventas_fecha_entrega(sucursal, fecha_entrega, horario)
    End Function

    Public Function OrdenesRecuperacion(ByVal sucursal As Long, ByVal status As Char) As Events
        OrdenesRecuperacion = oReportes.re_ordenes_recuperacion(sucursal, status)
    End Function
    Public Function ValeOrdenesRecuperacion(ByVal folio As Long) As Events
        ValeOrdenesRecuperacion = oReportes.re_ordenes_recuperacion_vale(folio)
    End Function
    Public Function MovimientoInventario(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long) As Events
        MovimientoInventario = oReportes.re_movimiento_inventario(bodega, concepto, folio)
    End Function

    Public Function MovimientoInventarioCancelacionVentas(ByVal sucursal As Long, ByVal concepto As String, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long) As Events
        MovimientoInventarioCancelacionVentas = oReportes.re_movimiento_inventario_cancelacion_venta(sucursal, concepto, sucursal_referencia, concepto_referencia, folio_referencia)
    End Function

    Public Function MercanciaVistasAlmacen(ByVal sucursal As Long, ByVal desglosado As Boolean, ByVal status As Int16, ByVal fechaini As DateTime, ByVal fechafin As DateTime) As Events
        MercanciaVistasAlmacen = oReportes.re_mercancia_vistas_almacen(sucursal, desglosado, status, fechaini, fechafin)
    End Function

    Public Function DiferenciaCostosDevolucionProveedor(ByVal TipoOrden As Int16, ByVal fechaini As DateTime, ByVal fechafin As DateTime) As Events
        DiferenciaCostosDevolucionProveedor = oReportes.re_diferencia_costos_devolucion_proveedor(TipoOrden, fechaini, fechafin)
    End Function


    Public Function ReporteEstadistico(ByVal Departamento As Long, ByVal Grupo As Long, ByVal Proveedor As Long, ByVal SoloExistencias As Boolean) As Events
        ReporteEstadistico = oReportes.re_reporte_estadistico(Departamento, Grupo, Proveedor, SoloExistencias)
    End Function


    Public Function UtilidadVentas(ByVal Departamento As Long, ByVal Grupo As Long, ByVal Proveedor As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal ConcentradoPor As Long) As Events
        UtilidadVentas = oReportes.re_utilidad_ventas(Departamento, Grupo, Proveedor, fecha_inicial, fecha_final, ConcentradoPor)
    End Function

    Public Function DiferenciaCostosPedidoFabrica(ByVal Sucursal As Long, ByVal Fecha_ini As DateTime, ByVal Fecha_fin As DateTime) As Events
        DiferenciaCostosPedidoFabrica = oReportes.re_diferencias_costos_pedido_fabrica(Sucursal, Fecha_ini, Fecha_fin)

    End Function

    Public Function VentasporVendedor(ByVal Sucursal As Long, ByVal Fecha_ini As DateTime, ByVal Fecha_fin As DateTime, ByVal Departamento As Long, ByVal Grupo As String, ByVal Proveedor As Long, ByVal Vendedor As Long) As Events
        VentasporVendedor = oReportes.re_ventas_vendedor(Sucursal, Fecha_ini, Fecha_fin, Departamento, Grupo, Proveedor, Vendedor)

    End Function

    Public Function DescuentosEspecialesProgramados(ByVal Departamento As Long, ByVal Grupo As Long, ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal SoloVigentes As Boolean, ByVal SoloPendientesAutorizar As Boolean) As Events
        DescuentosEspecialesProgramados = oReportes.re_descuentos_especiales_programados(Departamento, Grupo, fecha_inicial, fecha_final, SoloVigentes, SoloPendientesAutorizar)
    End Function

    Public Function ReporteSeguimientosOrdenServicio(ByVal orden_servicio As Long) As Events
        ReporteSeguimientosOrdenServicio = oReportes.re_ordenes_servicio_detalle_reporte_grs(orden_servicio)
    End Function

    Public Function ImprimeVale(ByVal folio As Long) As Events
        ImprimeVale = oReportes.re_imprime_vale(folio)
    End Function

    Public Function ReporteVales(ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal vencidos As Boolean, ByVal Estatus As Char, ByVal TipoVale As Long, ByVal fecha_corte As DateTime) As Events
        ReporteVales = oReportes.re_reporte_vales(fecha_inicial, fecha_final, vencidos, Estatus, TipoVale, fecha_corte)
    End Function

    Public Function ReporteVentasCancelaciones(ByVal sucursal As Long, ByVal fecha_inicial As Date, ByVal fecha_final As Date) As Events
        ReporteVentasCancelaciones = oReportes.re_ventas_cancelaciones(sucursal, fecha_inicial, fecha_final)
    End Function

    Public Function DevolucionesProveedor(ByVal bodega As String, ByVal Proveedor As Long, ByVal Desglosado As Boolean, ByVal fecha_inicial As Date, ByVal fecha_final As Date, ByVal Estatus As Long) As Events
        DevolucionesProveedor = oReportes.re_devoluciones_proveedor(bodega, Proveedor, Desglosado, fecha_inicial, fecha_final, Estatus)

    End Function

    Public Function FacturasPendientesTimbrar(ByVal Sucursal As Long, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime) As Events
        FacturasPendientesTimbrar = oReportes.re_facturas_pendientes_timbrar(Sucursal, FechaIni, FechaFin)
    End Function

    Public Function RecibosCancelados(ByVal Sucursal As Long, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime) As Events
        RecibosCancelados = oReportes.re_recibos_cancelados(Sucursal, FechaIni, FechaFin)
    End Function

    Public Function Anticipos(ByVal cliente As Long, ByVal tipo As Long, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal tipo_pago As Long) As Events
        Anticipos = oReportes.re_anticipos(cliente, tipo, FechaIni, FechaFin, tipo_pago)
    End Function

    Public Function ReporteMenos(ByVal sucursal As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal cajero As Long, ByVal cancelados As Boolean) As Events
        ReporteMenos = oReportes.re_menos(sucursal, fecha_inicial, fecha_final, cajero, cancelados)
    End Function

    Public Function ReportePedidosFabricaSurtidosFecha(ByVal sucursal As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime, ByVal proveedor As Long) As Events
        ReportePedidosFabricaSurtidosFecha = oReportes.re_pedidos_fabrica_surtidos_fecha(sucursal, fecha_inicial, fecha_final, proveedor)
    End Function

    Public Function EnvioInformacionTXT(ByVal sucursal As Long, ByVal convenio As Long, ByVal año As Long, ByVal no_quincena As Long, ByVal tipo As Char) As Events
        EnvioInformacionTXT = oReportes.sp_envio_informacion_txt(sucursal, convenio, año, no_quincena, tipo)
    End Function

    Public Function Llamadas(ByVal sucursal As Long, ByVal tipo_llamada As Long, ByVal fecha_inicial As DateTime, ByVal fecha_final As DateTime) As Events
        Llamadas = oReportes.re_llamadas(sucursal, tipo_llamada, fecha_inicial, fecha_final)
    End Function

#End Region

End Class
#End Region
