Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsConceptosCxc
'DATE:		20/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ConceptosCxc"
Public Class clsConceptosCxc
    Private oConceptosCxc As VillarrealData.clsConceptosCxc

#Region "Constructores"
    Sub New()
        oConceptosCxc = New VillarrealData.clsConceptosCxc
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oConceptosCxc.sp_conceptos_cxc_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oConceptosCxc.sp_conceptos_cxc_upd(oData)
    End Function

    Public Function Eliminar(ByVal concepto As String) As Events
        Eliminar = oConceptosCxc.sp_conceptos_cxc_del(concepto)
    End Function

    Public Function DespliegaDatos(ByVal concepto As String) As Events
        DespliegaDatos = oConceptosCxc.sp_conceptos_cxc_sel(concepto)
    End Function

    Public Function Listado() As Events
        Listado = oConceptosCxc.sp_conceptos_cxc_grs()
    End Function

    Public Function Lookup(Optional ByVal tipo As Char = "T") As Events
        Lookup = oConceptosCxc.sp_conceptos_cxc_grl(tipo)
    End Function
    Public Function Lookup(ByVal tipo As String, ByVal mostrarse_listado As Boolean) As Events
        Lookup = oConceptosCxc.sp_conceptos_cxc_tipo_grl(tipo, mostrarse_listado)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Concepto As String, ByVal Descripcion As String) As Events
        Validacion = ValidaConcepto(Concepto)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

        If Action <> Actions.Insert Then Exit Function

        Validacion = ValidaExisteConcepto(Concepto)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Private Function ValidaConcepto(ByVal Concepto As String) As Events
        Dim oEvent As New Events
        If Concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaExisteConcepto(ByVal concepto As String) As Events
        Dim oEvent As New Events

        oEvent = oConceptosCxc.sp_conceptos_cxc_exs(concepto.Trim)   'oConceptosCuentasCobrar.sp_conceptos_cuentas_cobrar_exs(concepto.Trim)
        If Not oEvent.ErrorFound Then
            If oEvent.Value > 0 Then
                oEvent.Message = "El Concepto ya existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


