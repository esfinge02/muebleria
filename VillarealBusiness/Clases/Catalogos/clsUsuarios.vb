Imports Dipros.Utils
Imports Dipros.Utils.Common


#Region "DIPROS Systems, BusinessEnvironment - Usuarios"


Public Class clsUsuarios
    Private oUsuarios As VillarrealData.clsUsuarios

#Region "Constructores"
    Sub New()
        oUsuarios = New VillarrealData.clsUsuarios
    End Sub
#End Region

#Region "Métodos"
    Public Function ActualizarUsuariosAlmacen(ByRef oData As DataSet, ByVal abogado As Integer) As Events
        ActualizarUsuariosAlmacen = oUsuarios.sp_usuarios_almacen_upd(oData, abogado)
    End Function

    Public Function ActualizaModificarPreciosCotizacion(ByVal usuario As String, ByVal modificar_precios_cotizacion As Boolean) As Events
        ActualizaModificarPreciosCotizacion = oUsuarios.sp_usuarios_modificar_precios_cotizacion_upd(usuario, modificar_precios_cotizacion)
    End Function

    Public Function Lookup() As Events
        Lookup = oUsuarios.sp_usuarios_grl()
    End Function

    Public Function SeleccionaUsuarioAlmacen(ByVal usuario As String) As Events
        SeleccionaUsuarioAlmacen = oUsuarios.sp_usuarios_almacen_sel(usuario)
    End Function

    Public Function DespliegaDatosUsuario() As Events
        DespliegaDatosUsuario = oUsuarios.tin_usuarios_sel
    End Function
    Public Function ListadoUsuariosAlmacen() As Events
        ListadoUsuariosAlmacen = oUsuarios.sp_usuarios_almacen_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Usuario As String, ByVal Bodega As String, ByVal PorcentajeCondonacionIntereses As Double) As Events
        Validacion = ValidaUsuario(Usuario)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodega(Bodega)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPorcentajeCondonacionIntereses(PorcentajeCondonacionIntereses)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Private Function ValidaPorcentajeCondonacionIntereses(ByVal PorcentajeCondonacionIntereses As Double) As Events
        Dim oEvent As New Events
        'If PorcentajeCondonacionIntereses <= 0 Then
        '    oEvent.Ex = Nothing
        '    oEvent.Message = "El Porcentaje de Condonación de Intereses es Requerido"
        '    oEvent.Layer = Events.ErrorLayer.BussinessLayer
        '    Return oEvent
        'End If
        If PorcentajeCondonacionIntereses > 100 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Porcentaje de Condonación de Intereses debe ser Menor o Igual al 100%"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaUsuario(ByVal Usuario As String) As Events
        Dim oEvent As New Events
        If Usuario.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Usuario es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBodega(ByVal Bodega As String) As Events
        Dim oEvent As New Events
        If Bodega.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region


End Class
#End Region