Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCuentasContables
'DATE:		10/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CuentasContables"
Public Class clsCuentasContables
    Private oCuentasContables As VillarrealData.clsCuentasContables

#Region "Constructores"
    Sub New()
        oCuentasContables = New VillarrealData.clsCuentasContables
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal subcuenta As Boolean) As Events
        Insertar = oCuentasContables.sp_cuentas_contables_ins(oData, sucursal, subcuenta)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal subcuenta As Boolean) As Events
        Actualizar = oCuentasContables.sp_cuentas_contables_upd(oData, sucursal, subcuenta)
    End Function

    Public Function Eliminar(ByVal clave As String) As Events
        Eliminar = oCuentasContables.sp_cuentas_contables_del(clave)
    End Function

    Public Function DespliegaDatos(ByVal clave As String) As Events
        DespliegaDatos = oCuentasContables.sp_cuentas_contables_sel(clave)
    End Function

    Public Function Listado() As Events
        Listado = oCuentasContables.sp_cuentas_contables_grs()
    End Function

    Public Function Lookup(ByVal Sucursal As Long, ByVal polizas As Boolean) As Events
        Lookup = oCuentasContables.sp_cuentas_contables_grl(Sucursal, polizas)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal clave As String, ByVal descripcion As String) As Events
        If Action = Actions.Delete Then Exit Function
        Validacion = ValidaClave(clave)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaClave(ByVal Clave As String) As Events
        Dim oEvent As New Events
        If Clave.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cuenta es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n de la Cuenta es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


