Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCobradores
'DATE:		24/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Cobradores"
Public Class clsCobradores
    Private oCobradores As VillarrealData.clsCobradores

#Region "Constructores"
    Sub New()
        oCobradores = New VillarrealData.clsCobradores
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oCobradores.sp_cobradores_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oCobradores.sp_cobradores_upd(oData)
    End Function

    Public Function Eliminar(ByVal cobrador As Long) As Events
        Eliminar = oCobradores.sp_cobradores_del(cobrador)
    End Function

    Public Function DespliegaDatos(ByVal cobrador As Long) As Events
        DespliegaDatos = oCobradores.sp_cobradores_sel(cobrador)
    End Function

    Public Function Listado() As Events
        Listado = oCobradores.sp_cobradores_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal Sucursal As Long) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Cobrador es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

#Region "Lookups"
    Public Function Lookup(Optional ByVal sucursal As Long = -1) As Events
        Lookup = oCobradores.sp_cobradores_grl(sucursal)
    End Function

#End Region

End Class
#End Region


