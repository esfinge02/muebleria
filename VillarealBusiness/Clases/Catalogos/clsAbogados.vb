
Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAbogados
'DATE:		16/01/2007 00:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Abogados"
Public Class clsAbogados
    Private oAbogados As VillarrealData.clsAbogados

#Region "Constructores"
    Sub New()
        oAbogados = New VillarrealData.clsAbogados
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oAbogados.sp_abogados_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oAbogados.sp_abogados_upd(oData)
    End Function

    Public Function Eliminar(ByVal abogado As Long) As Events
        Eliminar = oAbogados.sp_abogados_del(abogado)
    End Function

    Public Function DespliegaDatos(ByVal abogado As Long) As Events
        DespliegaDatos = oAbogados.sp_abogados_sel(abogado)
    End Function

    Public Function Listado() As Events
        Listado = oAbogados.sp_abogados_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oAbogados.sp_abogados_grl()
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal nombre As String, ByVal direccion As String, ByVal Estado As Integer, ByVal Municipio As Integer, ByVal Ciudad As Integer, ByVal Colonia As Integer, ByVal Comision As Long ) As Events

        Validacion = ValidaNombre(nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDireccion(direccion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaEstado(Estado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMunicipio(Municipio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCiudad(Ciudad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaColonia(Colonia)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaComision(Comision)
        'If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFormatoComision(Comision)
        If Validacion.ErrorFound Then Exit Function


    End Function
    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Abogado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDireccion(ByVal Direccion As String) As Events
        Dim oEvent As New Events
        If Direccion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Direcci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaEstado(ByVal Estado As Long) As Events
        Dim oEvent As New Events
        If Estado <= 0 Or IsDBNull(Estado) = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMunicipio(ByVal municipio As String) As Events
        Dim oEvent As New Events
        If municipio <= 0 Or IsDBNull(municipio) = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Municipio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCiudad(ByVal Ciudad As Long) As Events
        Dim oEvent As New Events
        If Ciudad <= 0 Or IsDBNull(Ciudad) = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ciudad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaColonia(ByVal Colonia As Long) As Events
        Dim oEvent As New Events
        If Colonia <= 0 Or IsDBNull(Colonia) = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Colonia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    'Public Function ValidaComision(ByVal Comision As Long) As Events
    '    Dim oEvent As New Events
    '    If Comision <= 0 Or IsDBNull(Comision) = True Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "La Comisi�n es Requerida"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    Public Function ValidaFormatoComision(ByVal Comision As Long) As Events
        Dim oEvent As New Events
        If Comision > 100 Or Comision < (-100) Then
            oEvent.Ex = Nothing
            oEvent.Message = "Revise el formato de la Comisi�n"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function





#End Region

End Class
#End Region


