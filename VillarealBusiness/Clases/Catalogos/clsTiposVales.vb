Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsTiposVales
    Private oTipos As VillarrealData.clsTiposVales

#Region "Constructores"
    Sub New()
        oTipos = New VillarrealData.clsTiposVales
    End Sub
#End Region

    Public Function Insertar(ByRef tipo As Long, ByVal nombre As String, ByVal concepto As String, ByVal forma As Char) As Events
        Insertar = oTipos.sp_tipos_vales_ins(tipo, nombre, concepto, forma)
    End Function

    Public Function Actualizar(ByVal tipo As Long, ByVal nombre As String, ByVal concepto As String, ByVal forma As Char) As Events
        Actualizar = oTipos.sp_tipos_vales_upd(tipo, nombre, concepto, forma)
    End Function

    Public Function Eliminar(ByVal tipo As Long) As Events
        Eliminar = oTipos.sp_tipos_vales_del(tipo)
    End Function

    Public Function DespliegaDatos(ByVal tipo As Long) As Events
        DespliegaDatos = oTipos.sp_tipos_vales_sel(tipo)
    End Function

    Public Function Listado() As Events
        Listado = oTipos.sp_tipos_vales_grs()
    End Function

    Public Function Lookup(Optional ByVal forma As Char = "X") As Events
        Lookup = oTipos.sp_tipos_vales_grl(forma)
    End Function

    Public Function ObtenerTipoValeCancelacion() As Events
        ObtenerTipoValeCancelacion = oTipos.sp_tipos_vales_cancelacion()
    End Function

    Public Function Validacion(ByVal action As Actions, ByVal Nombre As String) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Private Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Tipo de Vale es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
End Class
