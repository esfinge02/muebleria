Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsProveedoresCuentasBancarias
'DATE:		19/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ProveedoresCuentasBancarias"
Public Class clsProveedoresCuentasBancarias
	Private oProveedoresCuentasBancarias As VillarrealData.clsProveedoresCuentasBancarias

#Region "Constructores"
	Sub New()
		oProveedoresCuentasBancarias = New VillarrealData.clsProveedoresCuentasBancarias
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal proveedor As Long) As Events
        Insertar = oProveedoresCuentasBancarias.sp_proveedores_cuentas_bancarias_ins(oData, proveedor)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal proveedor As Long) As Events
        Actualizar = oProveedoresCuentasBancarias.sp_proveedores_cuentas_bancarias_upd(oData, proveedor)
    End Function

    Public Function Eliminar(ByVal proveedor As Long, ByVal banco As Long, ByVal cuenta As String) As Events
        Eliminar = oProveedoresCuentasBancarias.sp_proveedores_cuentas_bancarias_del(proveedor, banco, cuenta)
    End Function

    Public Function DespliegaDatos(ByVal proveedor As Long, ByVal banco As Long, ByVal cuenta As String) As Events
        DespliegaDatos = oProveedoresCuentasBancarias.sp_proveedores_cuentas_bancarias_sel(proveedor, banco, cuenta)
    End Function

    Public Function Listado(ByVal proveedor As Long) As Events
        Listado = oProveedoresCuentasBancarias.sp_proveedores_cuentas_bancarias_grs(proveedor)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Banco As Long, ByVal Sucursal As String, ByVal Plaza As String, ByVal Cuenta As String) As Events
        Validacion = ValidaBanco(Banco)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPlaza(Plaza)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCuenta(Cuenta)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaBanco(ByVal Banco As Long) As Events
        Dim oEvent As New Events
        If Banco <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Banco es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal Sucursal As String) As Events
        Dim oEvent As New Events
        If Sucursal.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPlaza(ByVal Plaza As String) As Events
        Dim oEvent As New Events
        If Plaza.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Plaza es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCuenta(ByVal Cuenta As String) As Events
        Dim oEvent As New Events
        If Cuenta.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cuenta es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


