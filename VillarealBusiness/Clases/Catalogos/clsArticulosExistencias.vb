Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsArticulosExistencias
'DATE:		03/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ArticulosExistencias"
Public Class clsArticulosExistencias
    Private oArticulosExistencias As VillarrealData.clsArticulosExistencias

#Region "Constructores"
    Sub New()
        oArticulosExistencias = New VillarrealData.clsArticulosExistencias
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef articulo As Long, ByVal bodega As String, ByVal fisica As Double, _
    ByVal por_surtir As Double, ByVal transito As Double, ByVal entregar As Double, ByVal reparto As Double, ByVal vistas As Double, ByVal garantia As Double) As Events
        Insertar = oArticulosExistencias.sp_articulos_existencias_ins(articulo, bodega, fisica, por_surtir, transito, entregar, reparto, vistas, garantia)
    End Function

    Public Function Actualizar(ByVal articulo As Long, ByVal bodega As String, ByVal columna As String, ByVal cantidad As Long, ByVal cantidad_anterior As Long) As Events
        Actualizar = oArticulosExistencias.sp_actualiza_existencias(articulo, bodega, columna, cantidad, cantidad_anterior)
    End Function

    Public Function Eliminar(ByVal articulo As Long, Optional ByVal bodega As String = "-1") As Events
        Eliminar = oArticulosExistencias.sp_articulos_existencias_del(articulo, bodega)
    End Function

    'Public Function DespliegaDatos(ByVal articulo As String, ByVal bodega As Long) As Events
    '    DespliegaDatos = oArticulosExistencias.sp_articulos_existencias_sel(articulo, bodega)
    'End Function

    Public Function Listado(ByVal articulo As Long, ByVal EsInsert As Boolean) As Events
        Listado = oArticulosExistencias.sp_articulos_existencias_grs(articulo, EsInsert)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function

    Public Function ArticulosExistencias_PorSurtir(ByVal bodega As String, ByVal articulo As Long) As Events
        ArticulosExistencias_PorSurtir = oArticulosExistencias.sp_articulos_existencias_porsurtir(bodega, articulo)
    End Function

    'Public Function ArticulosExistencias_PorSurtir_SobrePedido(ByVal bodega As String, ByVal articulo As Long) As Events
    '    ArticulosExistencias_PorSurtir_SobrePedido = oArticulosExistencias.sp_articulos_existencias_porsurtir_sobrepedido(bodega, articulo)
    'End Function

    Public Function ArticulosExistencias_Traspasos(ByVal bodega As String, ByVal articulo As Long) As Events
        ArticulosExistencias_Traspasos = oArticulosExistencias.sp_articulos_existencias_traspasos(bodega, articulo)
    End Function

    Public Function ArticulosExistencias_Repartiendose(ByVal bodega As String, ByVal articulo As Long) As Events
        ArticulosExistencias_Repartiendose = oArticulosExistencias.sp_articulos_existencias_repartiendose(bodega, articulo)
    End Function

    Public Function ArticulosExistencias_PorEntregar(ByVal bodega As String, ByVal articulo As Long, ByVal sobrepedido As Boolean) As Events
        ArticulosExistencias_PorEntregar = oArticulosExistencias.sp_articulos_existencias_porentregar(bodega, articulo, sobrepedido)
    End Function

    Public Function ArticulosExistencias_Garantias(ByVal bodega As String, ByVal articulo As Long) As Events
        ArticulosExistencias_Garantias = oArticulosExistencias.sp_articulos_existencias_garantias(bodega, articulo)
    End Function
    Public Function ArticulosExistencias_Vistas(ByVal bodega As String, ByVal articulo As Long) As Events
        ArticulosExistencias_Vistas = oArticulosExistencias.sp_articulos_existencias_vistas(bodega, articulo)
    End Function
    Public Function ArticulosExistencias_Prefacturas(ByVal bodega As String, ByVal articulo As Long) As Events
        ArticulosExistencias_Prefacturas = oArticulosExistencias.sp_articulos_existencias_prefacturas(bodega, articulo)
    End Function

    Public Function ValidaExistenciaArticuloBodega(ByVal articulo As Long, ByVal bodega As String, ByVal cantidad As Double) As Events
        Dim response As New Events
        Dim existencia_suficiente As Integer
        response = oArticulosExistencias.sp_articulos_existencias_bodega(bodega, articulo, cantidad)
        existencia_suficiente = CType(response.Value, DataSet).Tables(0).Rows(0).Item("existencia_suficiente")
        If existencia_suficiente = 0 Then
            response.Ex = Nothing
            response.Message = "Existencia insuficiente en Bodega: " + bodega + " para el Art�culo: " + articulo.ToString
            response.Layer = Events.ErrorLayer.BussinessLayer
            Return response
        End If
        Return response
    End Function
    Public Function ArticulosBodegaExistencias(ByVal bodega As String, ByVal articulo As Long) As Events
        ArticulosBodegaExistencias = oArticulosExistencias.sp_bodega_articulo_exs(bodega, articulo)
    End Function



#End Region

End Class
#End Region


