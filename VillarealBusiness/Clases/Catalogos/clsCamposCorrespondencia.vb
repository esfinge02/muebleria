Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCamposCorrespondencia
'DATE:		31/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CamposCorrespondencia"
Public Class clsCamposCorrespondencia
    Private oCamposCorrespondencia As VillarrealData.clsCamposCorrespondencia

#Region "Constructores"
    Sub New()
        oCamposCorrespondencia = New VillarrealData.clsCamposCorrespondencia
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef campo_correspondencia As String, ByVal nombre_tabla As String, ByVal campo_tabla As String, ByVal tipo_dato As String) As Events
        Insertar = oCamposCorrespondencia.sp_campos_correspondencia_ins(campo_correspondencia, nombre_tabla, campo_tabla, tipo_dato)
    End Function

    Public Function Actualizar(ByVal campo_correspondencia As String, ByVal nombre_tabla As String, ByVal campo_tabla As String, ByVal tipo_dato As String) As Events
        Actualizar = oCamposCorrespondencia.sp_campos_correspondencia_upd(campo_correspondencia, nombre_tabla, campo_tabla, tipo_dato)
    End Function

    Public Function Eliminar(ByVal campo_correspondencia As String) As Events
        Eliminar = oCamposCorrespondencia.sp_campos_correspondencia_del(campo_correspondencia)
    End Function

    Public Function DespliegaDatos(ByVal campo_correspondencia As String) As Events
        DespliegaDatos = oCamposCorrespondencia.sp_campos_correspondencia_sel(campo_correspondencia)
    End Function

    Public Function Listado() As Events
        Listado = oCamposCorrespondencia.sp_campos_correspondencia_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Campo_Correspondencia As String, ByVal Campo_Tabla As String, ByVal bSintaxisValida As Boolean) As Events


        Validacion = ValidaCampo_Correspondencia(Campo_Correspondencia)
        If Validacion.ErrorFound Then Exit Function

        ' SI LA ACCION ES ELIMINAR ENTONCES NO HAY MAS VALIDACIONES
        If Action = Actions.Delete Then Exit Function

        Validacion = ValidaCampo_Tabla(Campo_Tabla)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSintaxis(bSintaxisValida)

        If Action <> Actions.Insert Then Exit Function

        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaExisteCampoCorrespondencia(Campo_Correspondencia)


    End Function

    Private Function ValidaCampo_Correspondencia(ByVal Campo_Correspondencia As String) As Events
        Dim oEvent As New Events
        If Campo_Correspondencia <> Campo_Correspondencia Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Campo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaCampo_Tabla(ByVal Campo_Tabla As String) As Events
        Dim oEvent As New Events
        If Campo_Tabla <> Campo_Tabla Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Expresi�n del Campo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaSintaxis(ByVal sintaxis As Boolean) As Events
        Dim oEvent As New Events
        If Not sintaxis Then
            oEvent.Ex = Nothing
            oEvent.Message = "Compruebe la Sintaxis"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaExisteCampoCorrespondencia(ByVal Campo_Correspondencia As String) As Events
        Dim oEvent As New Events

        oEvent = oCamposCorrespondencia.sp_campos_correspondencia_exs(Campo_Correspondencia.Trim)
        If Not oEvent.ErrorFound Then
            If oEvent.Value > 0 Then
                oEvent.Message = "El Campo ya existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


