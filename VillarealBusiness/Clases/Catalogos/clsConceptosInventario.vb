Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsConceptosInventario
'DATE:		28/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ConceptosInventario"
Public Class clsConceptosInventario
    Private oConceptosInventario As VillarrealData.clsConceptosInventario

#Region "Constructores"
    Sub New()
        oConceptosInventario = New VillarrealData.clsConceptosInventario
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oConceptosInventario.sp_conceptos_inventario_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oConceptosInventario.sp_conceptos_inventario_upd(oData)
    End Function

    Public Function Eliminar(ByVal concepto As String) As Events
        Eliminar = oConceptosInventario.sp_conceptos_inventario_del(concepto)
    End Function

    Public Function DespliegaDatos(ByVal concepto As String) As Events
        DespliegaDatos = oConceptosInventario.sp_conceptos_inventario_sel(concepto)
    End Function

    Public Function Listado() As Events
        Listado = oConceptosInventario.sp_conceptos_inventario_grs()
    End Function

    Public Function Lookup(Optional ByVal tipo As Char = "") As Events
        Lookup = oConceptosInventario.sp_conceptos_inventario_grl(tipo)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal concepto As String, ByVal Descripcion As String) As Events
        Validacion = ValidaConcepto(concepto)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

        If Action = Actions.Insert Then
            Validacion = ValidaExisteConcepto(concepto)
            If Validacion.ErrorFound Then Exit Function
        End If

        If Action = Actions.Delete Then
            Validacion = ValidaEliminar(concepto)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function

    Public Function ValidaConcepto(ByVal Concepto As String) As Events
        Dim oEvent As New Events
        If Concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaExisteConcepto(ByVal concepto As String) As Events
        Dim oEvent As New Events

        oEvent = oConceptosInventario.sp_conceptos_inventario_exs(concepto.Trim)
        If Not oEvent.ErrorFound Then
            If oEvent.Value > 0 Then
                oEvent.Message = "El Concepto ya existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function

    Private Function ValidaEliminar(ByVal concepto As String) As Events
        Dim oEvent As New Events
        oEvent = oConceptosInventario.sp_conceptos_inventario_valida_eliminar_del(concepto)
        If oEvent.Value = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto No se puede Eliminar por que esta siendo Utilizando"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


