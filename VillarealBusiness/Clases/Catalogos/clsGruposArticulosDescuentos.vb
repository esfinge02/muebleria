Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsGruposArticulosDescuentos
'DATE:		23/05/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - GruposArticulosDescuentos"
Public Class clsGruposArticulosDescuentos
    Private oGruposArticulosDescuentos As VillarrealData.clsGruposArticulosDescuentos

#Region "Constructores"
    Sub New()
        oGruposArticulosDescuentos = New VillarrealData.clsGruposArticulosDescuentos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal grupo As Long, ByVal precio As Long, ByVal porcentaje_descuento As Double) As Events
        Insertar = oGruposArticulosDescuentos.sp_grupos_articulos_descuentos_ins(grupo, precio, porcentaje_descuento)
    End Function

    Public Function Actualizar(ByVal grupo As Long, ByVal precio As Long, ByVal porcentaje_descuento As Double) As Events
        Actualizar = oGruposArticulosDescuentos.sp_grupos_articulos_descuentos_upd(grupo, precio, porcentaje_descuento)
    End Function

    Public Function Eliminar(ByVal grupo As Long, ByVal precio As Long) As Events
        Eliminar = oGruposArticulosDescuentos.sp_grupos_articulos_descuentos_del(grupo, precio)
    End Function

    Public Function DespliegaDatos(ByVal grupo As Long, ByVal precio As Long) As Events
        DespliegaDatos = oGruposArticulosDescuentos.sp_grupos_articulos_descuentos_sel(grupo, precio)
    End Function

    Public Function Listado(Optional ByVal grupo As Long = -1) As Events
        Listado = oGruposArticulosDescuentos.sp_grupos_articulos_descuentos_grs(grupo)
    End Function

    Public Function calcula_precio_venta(ByVal grupo As Long, ByVal precio As Long, ByVal porcentaje_descuento As Double) As Events
        calcula_precio_venta = oGruposArticulosDescuentos.ca_calcula_precio_venta_grupo(grupo, precio, porcentaje_descuento)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


