Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDependencias
'DATE:		16/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Dependencias"
Public Class clsDependencias
	Private oDependencias As VillarrealData.clsDependencias

#Region "Constructores"
	Sub New()
		oDependencias = New VillarrealData.clsDependencias
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oDependencias.sp_dependencias_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oDependencias.sp_dependencias_upd(oData)
	End Function

	Public Function Eliminar(ByVal dependencia as long) As Events
		Eliminar = oDependencias.sp_dependencias_del(dependencia)
	End Function

	Public Function DespliegaDatos(ByVal dependencia as long) As Events
		DespliegaDatos = oDependencias.sp_dependencias_sel(dependencia)
	End Function

	Public Function Listado() As Events
		Listado = oDependencias.sp_dependencias_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oDependencias.sp_dependencias_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String, ByVal Concepto_Deduccion As String) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConcepto_Deduccion(Concepto_Deduccion)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function Validacion(ByVal sucursal As Long, ByVal dependencia As Long, ByVal filas As Long, ByVal quincena_envio_actual As Long, ByVal quincena_envio As Long, ByVal anio_envio_actual As Long, ByVal anio_envio As Long) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDependencia(dependencia)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaQuincenaEnvioActual(quincena_envio_actual, quincena_envio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaAnioEnvioActual(anio_envio_actual, anio_envio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFilas(filas)
        If Validacion.ErrorFound Then Exit Function

    End Function


    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConcepto_Deduccion(ByVal Concepto_Deduccion As String) As Events
        Dim oEvent As New Events
        If Concepto_Deduccion.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto de Deducción es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaClaveIdentificacion(ByVal clave_identificacíon As String) As Events
        Dim oEvent As New Events
        If clave_identificacíon.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Clave de Identificación es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaSucursal(ByVal sucursal As Long) As Events
        Dim oEvent As New Events
        If sucursal <= 0 Or IsNumeric(sucursal) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Private Function ValidaDependencia(ByVal dependencia As Long) As Events
        Dim oEvent As New Events
        If dependencia <= 0 Or IsNumeric(dependencia) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Convenio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Private Function ValidaFilas(ByVal filas As Long) As Events
        Dim oEvent As New Events
        If filas <= 0 Or IsNumeric(filas) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "Debe insertar al menos un registro"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

    Private Function ValidaQuincenaEnvioActual(ByVal quincena_envio_actual As Long, ByVal quincena_envio As Long) As Events
        Dim oEvent As New Events
        If quincena_envio_actual <> quincena_envio Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Quincena de Envio debe ser Igual a la Quincena actual de la Sucursal"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaAnioEnvioActual(ByVal anio_envio_actual As Long, ByVal anio_envio As Long) As Events
        Dim oEvent As New Events
        If anio_envio_actual <> anio_envio Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Año de Envio debe ser Igual al Año actual de la Sucursal"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


