Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsArticulosPrecios
'DATE:		02/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ArticulosPrecios"
Public Class clsArticulosPrecios
    Private oArticulosPrecios As VillarrealData.clsArticulosPrecios

#Region "Constructores"
    Sub New()
        oArticulosPrecios = New VillarrealData.clsArticulosPrecios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal articulo As Long) As Events
        Insertar = oArticulosPrecios.sp_articulos_precios_ins(oData, articulo)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal articulo As Long) As Events
        Actualizar = oArticulosPrecios.sp_articulos_precios_upd(oData, articulo)
    End Function
    Public Function Actualizar(ByVal articulo As Long, ByVal clave_precio As Long, ByVal utilidad As Double, ByVal precio_venta As Double) As Events
        Actualizar = oArticulosPrecios.sp_articulos_precios_upd(articulo, clave_precio, utilidad, precio_venta)
    End Function

    Public Function Eliminar(ByVal articulo As Long, ByVal precio As Long) As Events
        Eliminar = oArticulosPrecios.sp_articulos_precios_del(articulo, precio)
    End Function

    Public Function DespliegaDatos(ByVal articulo As Long, ByVal precio As Long) As Events
        DespliegaDatos = oArticulosPrecios.sp_articulos_precios_sel(articulo, precio)
    End Function

    Public Function Listado(ByVal articulo As Long) As Events
        Listado = oArticulosPrecios.sp_articulos_precios_grs(articulo)
    End Function

    Public Function Lookup(ByVal articulo As Long) As Events
        Lookup = oArticulosPrecios.sp_articulos_precios_grl(articulo)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Precio As Double, ByVal Utilidad As Double, ByVal PrecioLista As Double, ByVal precio_venta As Double) As Events
        Validacion = ValidaPrecioLkp(Precio)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaUtilidad(Utilidad)
        'If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPrecio(precio_venta, PrecioLista)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaPrecioLkp(ByVal Precio As Long) As Events
        Dim oEvent As New Events
        If Precio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Precio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaPrecio(ByVal Precio As Double, ByVal PrecioLista As Double) As Events
        Dim oEvent As New Events
        If Precio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Precio no debe ser Menor a 0"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    'Public Function ValidaUtilidad(ByVal Utilidad As Double) As Events
    '    Dim oEvent As New Events
    '    If Utilidad < 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "La Utilidad es Requerida"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function

    Public Function Listado_Cambio_Precios(ByVal articulo As Long) As Events
        Listado_Cambio_Precios = oArticulosPrecios.sp_articulos_cambios_precios_grs(articulo)
    End Function

#End Region

End Class
#End Region


