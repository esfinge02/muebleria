Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCajerosFondos
'DATE:		22/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CajerosFondos"
Public Class clsCajerosFondos
    Private oCajerosFondos As VillarrealData.clsCajerosFondos

#Region "Constructores"
    Sub New()
        oCajerosFondos = New VillarrealData.clsCajerosFondos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal cajero As Long, ByVal fecha As Date) As Events
        Insertar = oCajerosFondos.sp_cajeros_fondos_ins(oData, cajero, fecha)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oCajerosFondos.sp_cajeros_fondos_upd(oData)
    End Function

    Public Function Eliminar(ByVal cajero As Long, ByVal fecha As Date) As Events
        Eliminar = oCajerosFondos.sp_cajeros_fondos_del(cajero, fecha)
    End Function

    Public Function DespliegaDatos(ByVal cajero As Long, ByVal fecha As Date) As Events
        DespliegaDatos = oCajerosFondos.sp_cajeros_fondos_sel(cajero, fecha)
    End Function

    Public Function Listado() As Events
        Listado = oCajerosFondos.sp_cajeros_fondos_grs()
    End Function

    Public Function Existe(ByVal cajero As Long, ByVal fecha As Date) As Events
        Existe = oCajerosFondos.sp_cajeros_fondos_exs(cajero, fecha)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal tipo_cambio As Double, ByVal fondo As Double) As Events
        Validacion = ValidaTipoCambio(tipo_cambio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFondo(fondo)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaTipoCambio(ByVal tipo_cambio As Double) As Events
        Dim oEvent As New Events
        If tipo_cambio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Tipo de Cambio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaFondo(ByVal fondo As Double) As Events
        Dim oEvent As New Events
        If fondo < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Fondo no Puede ser Negativo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function



#End Region

End Class
#End Region


