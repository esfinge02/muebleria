Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsTamañosEtiquetas
'DATE:		26/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - TamañosEtiquetas"
Public Class clsTamaniosEtiquetas
    Private oTamañosEtiquetas As VillarrealData.clsTamaniosEtiquetas

#Region "Constructores"
    Sub New()
        oTamañosEtiquetas = New VillarrealData.clsTamaniosEtiquetas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oTamañosEtiquetas.sp_tamanios_etiquetas_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oTamañosEtiquetas.sp_tamanios_etiquetas_upd(oData)
    End Function

    Public Function Eliminar(ByVal tamaño As Long) As Events
        Eliminar = oTamañosEtiquetas.sp_tamanios_etiquetas_del(tamaño)
    End Function

    Public Function DespliegaDatos(ByVal tamaño As Long) As Events
        DespliegaDatos = oTamañosEtiquetas.sp_tamanios_etiquetas_sel(tamaño)
    End Function

    Public Function Listado() As Events
        Listado = oTamañosEtiquetas.sp_tamanios_etiquetas_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oTamañosEtiquetas.sp_tamanios_etiquetas_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal descripcion As String) As Events
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Private Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvents As New Events
        If descripcion.Length = 0 Then
            oEvents.Ex = Nothing
            oEvents.Message = "La Descripción es Requerida"
            oEvents.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvents
        End If

        Return oEvents
    End Function



#End Region

End Class
#End Region


