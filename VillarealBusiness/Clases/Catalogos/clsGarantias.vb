Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsGarantias
'DATE:		31/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Garantias"
Public Class clsGarantias
	Private oGarantias As VillarrealData.clsGarantias

#Region "Constructores"
	Sub New()
		oGarantias = New VillarrealData.clsGarantias
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oGarantias.sp_garantias_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oGarantias.sp_garantias_upd(oData)
	End Function

	Public Function Eliminar(ByVal garantia as long) As Events
		Eliminar = oGarantias.sp_garantias_del(garantia)
	End Function

	Public Function DespliegaDatos(ByVal garantia as long) As Events
		DespliegaDatos = oGarantias.sp_garantias_sel(garantia)
	End Function

	Public Function Listado() As Events
		Listado = oGarantias.sp_garantias_grs()
	End Function

    Public Function Validacion(ByVal Action As Actions, ByVal descripcion As String) As Events
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvent As New Events
        If descripcion.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

#End Region

End Class
#End Region


