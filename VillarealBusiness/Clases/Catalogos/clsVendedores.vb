Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVendedores
'DATE:		24/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Vendedores"
Public Class clsVendedores
    Private oVendedores As VillarrealData.clsVendedores

#Region "Constructores"
    Sub New()
        oVendedores = New VillarrealData.clsVendedores
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oVendedores.sp_vendedores_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oVendedores.sp_vendedores_upd(oData)
    End Function

    Public Function Actualizar(ByVal vendedor As Long, ByVal nombre As String, ByVal sueldo As Double, ByVal sucursal As Long, ByVal meta As Double, ByVal activo As Boolean, ByVal codigo_nomipaq As String, ByVal esquema_comisiones As Long) As Events
        Actualizar = oVendedores.sp_vendedores_upd(vendedor, nombre, sueldo, sucursal, meta, activo, codigo_nomipaq, esquema_comisiones)
    End Function

    Public Function Eliminar(ByVal vendedor As Long) As Events
        Eliminar = oVendedores.sp_vendedores_del(vendedor)
    End Function

    Public Function DespliegaDatos(ByVal vendedor As Long) As Events
        DespliegaDatos = oVendedores.sp_vendedores_sel(vendedor)
    End Function

    Public Function Listado() As Events
        Listado = oVendedores.sp_vendedores_grs()
    End Function

    Public Function Lookup(Optional ByVal sucursal_actual As Long = -1) As Events
        Lookup = oVendedores.sp_vendedores_grl(sucursal_actual)
    End Function

    Public Function GuardarComisiones(ByVal fecha_ini As Date, ByVal fecha_fin As Date, ByVal menor_13 As Double, ByVal mayor_13 As Double, ByVal sucursal As Long) As Events
        GuardarComisiones = oVendedores.sp_guardar_comisiones(fecha_ini, fecha_fin, menor_13, mayor_13, sucursal)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal Sueldo As Double, ByVal Sucursal As Long) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSueldo(Sueldo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSueldo(ByVal Sueldo As Double) As Events
        Dim oEvent As New Events
        If Sueldo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Sueldo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


