Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsProveedores
'DATE:		01/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Proveedores"
Public Class clsProveedores
    Private oProveedores As VillarrealData.clsProveedores

#Region "Constructores"
    Sub New()
        oProveedores = New VillarrealData.clsProveedores
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oProveedores.sp_proveedores_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oProveedores.sp_proveedores_upd(oData)
    End Function

    Public Function Eliminar(ByVal proveedor As Long) As Events
        Eliminar = oProveedores.sp_proveedores_del(proveedor)
    End Function

    Public Function DespliegaDatos(ByVal proveedor As Long) As Events
        DespliegaDatos = oProveedores.sp_proveedores_sel(proveedor)
    End Function

    Public Function Listado() As Events
        Listado = oProveedores.sp_proveedores_grs()
    End Function
    Public Function Lookup(Optional ByVal utilizar_filtro As Boolean = False, Optional ByVal activo As Boolean = True) As Events
        Lookup = oProveedores.sp_proveedores_grl(utilizar_filtro, activo)
    End Function

    Public Function LookupExistenciasProveedores() As Events
        LookupExistenciasProveedores = oProveedores.sp_proveedores_existencias_grl()
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal Cuenta As String, ByVal Rfc As String, ByVal domicilio As String, ByVal colonia As String, ByVal ciudad As String, _
    ByVal Estado As String, ByVal CodigoPostal As Long, ByVal Telefonos As String, ByVal Contactos As String, ByVal Curp As String, ByVal Persona As Char) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCuenta(Cuenta)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaRfc(Rfc, Persona)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDomicilio(domicilio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaColonia(colonia)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCiudad(ciudad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaEstado(Estado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCodigoPostal(CodigoPostal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaTelefonos(Telefonos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaContactos(Contactos)
        If Validacion.ErrorFound Then Exit Function
        If Persona = "F" Then
            Validacion = ValidaCurp(Curp)
            If Validacion.ErrorFound Then Exit Function
        End If

    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCuenta(ByVal Cuenta As String) As Events
        Dim oEvent As New Events
        If Cuenta.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cuenta es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaRfc(ByVal Rfc As String, ByVal persona As Char) As Events
        'Dim oEvent As New Events
        'If Rfc.Trim.Length = 0 Then
        '    oEvent.Ex = Nothing
        '    oEvent.Message = "El RFC es Requerido"
        '    oEvent.Layer = Events.ErrorLayer.BussinessLayer
        '    Return oEvent
        'End If
        'Return oEvent
        Dim oEvent As New Events
        Dim letras As String
        Dim numeros As String
        Dim fecha1 As Integer
        Dim fecha2 As Integer
        Dim fecha3 As Integer
        Dim homoclave As String
        Dim letra As Char

        If Rfc.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Rfc es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If persona = "F" Then  'VALIDACION DEL RFC PARA PERSONA FISICA

            If Rfc.Length <> 13 And Rfc.Length <> 10 Then
                oEvent.Ex = Nothing
                oEvent.Message = "La Longitud del RFC es Incorrecta"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If

            letras = Rfc.Substring(0, 4)
            numeros = Rfc.Substring(4, 6)
            For Each letra In letras
                If Not Char.IsLetter(letra) Then
                    oEvent.Ex = Nothing
                    oEvent.Message = "Los Primeros 4 Caracteres del RFC Deben ser Letras"
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            Next
            For Each letra In numeros
                If Not Char.IsNumber(letra) Then
                    oEvent.Ex = Nothing
                    oEvent.Message = "Incorrecto el RFC en los Datos de la Fecha"
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            Next

            fecha2 = Rfc.Substring(6, 2) 'mes
            fecha3 = Rfc.Substring(8, 2) 'dia
            If fecha2 > 12 Or fecha3 > 31 Then
                oEvent.Ex = Nothing
                oEvent.Message = "Incorrecto el RFC en los Datos de la Fecha"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If

            If Rfc.Length = 13 Then
                homoclave = Rfc.Substring(10, 3)
                For Each letra In homoclave
                    If Not Char.IsLetterOrDigit(letra) Then
                        oEvent.Ex = Nothing
                        oEvent.Message = "Los �ltimos 3 Caracteres del RFC Deben ser Letras o N�meros"
                        oEvent.Layer = Events.ErrorLayer.BussinessLayer
                        Return oEvent
                    End If
                Next
            End If

        Else 'VALIDACION DEL RFC PARA PERSONA MORAL

            If Rfc.Length <> 12 And Rfc.Length <> 9 Then
                oEvent.Ex = Nothing
                oEvent.Message = "La Longitud del RFC es Incorrecta"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If

            letras = Rfc.Substring(0, 3)
            numeros = Rfc.Substring(3, 6)
            For Each letra In letras
                If Not Char.IsLetter(letra) Then
                    oEvent.Ex = Nothing
                    oEvent.Message = "Los Primeros 3 Caracteres del RFC Deben ser Letras"
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            Next
            For Each letra In numeros
                If Not Char.IsNumber(letra) Then
                    oEvent.Ex = Nothing
                    oEvent.Message = "Incorrecto el RFC en los Datos de la Fecha"
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            Next

            'fecha1 = Rfc.Substring(3, 2) 'a�o
            fecha2 = Rfc.Substring(5, 2) 'mes
            fecha3 = Rfc.Substring(7, 2) 'dia
            If fecha2 > 12 Or fecha3 > 31 Then
                oEvent.Ex = Nothing
                oEvent.Message = "Incorrecto el RFC en los Datos de la Fecha"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If


            If Rfc.Length = 12 Then
                homoclave = Rfc.Substring(9, 3)
                For Each letra In homoclave
                    If Not Char.IsLetterOrDigit(letra) Then
                        oEvent.Ex = Nothing
                        oEvent.Message = "Los �ltimos 3 Caracteres del RFC Deben ser Letras o N�meros"
                        oEvent.Layer = Events.ErrorLayer.BussinessLayer
                        Return oEvent
                    End If
                Next
            End If

        End If


        Return oEvent
    End Function
    Public Function ValidaDomicilio(ByVal domicilio As String) As Events
        Dim oEvent As New Events
        If domicilio.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Domicilio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaColonia(ByVal Colonia As String) As Events
        Dim oEvent As New Events
        If Colonia.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Colonia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCiudad(ByVal ciudad As String) As Events
        Dim oEvent As New Events
        If ciudad.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ciudad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaEstado(ByVal Estado As String) As Events
        Dim oEvent As New Events
        If Estado.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCodigoPostal(ByVal CodigoPostal As Long) As Events
        Dim oEvent As New Events
        If CodigoPostal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El CodigoPostal es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaTelefonos(ByVal Telefonos As String) As Events
        Dim oEvent As New Events
        If Telefonos.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los Telefonos son Requeridos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaContactos(ByVal Contactos As String) As Events
        Dim oEvent As New Events
        If Contactos.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los Contactos es Requeridos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCurp(ByVal Curp As String) As Events
        Dim oEvent As New Events
        If Curp.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Curp del Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


