Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsClientesConvenios
    Private oClientesConvenios As VillarrealData.clsClientesConvenios

#Region "Constructores"
    Sub New()
        oClientesConvenios = New VillarrealData.clsClientesConvenios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal cliente As Long) As Events
        Insertar = oClientesConvenios.sp_clientes_convenios_ins(oData, cliente)
    End Function

    Public Function Insertar(ByVal cliente As Long, ByVal convenio As Long) As Events
        Insertar = oClientesConvenios.sp_clientes_convenios_ins(cliente, convenio)
    End Function


    Public Function Actualizar(ByRef oData As DataSet, ByVal cliente As Long) As Events
        Actualizar = oClientesConvenios.sp_clientes_convenios_upd(oData, cliente)
    End Function

    Public Function Eliminar(ByVal cliente As Long, ByVal convenio As Long) As Events
        Eliminar = oClientesConvenios.sp_clientes_convenios_del(cliente, convenio)
    End Function

    'Public Function DespliegaDatosCobradoresVentas(ByVal cliente As Long, ByVal sucursal As Long) As Events
    '    DespliegaDatosCobradoresVentas = oClientesCobradores.sp_clientes_cobradores_sel(cliente, sucursal)
    'End Function

    Public Function Listado(ByVal cliente As Long) As Events
        Listado = oClientesConvenios.sp_clientes_convenios_grs(cliente)
    End Function


    Public Function Lookup(ByVal convenio As Long) As Events
        Lookup = oClientesConvenios.sp_clientes_convenios_lkp(convenio)
    End Function


    'Public Function Traercobrador(ByVal sucursal As Long, ByVal cliente As Long) As Events
    '    Traercobrador = oClientesCobradores.sp_clientes_cobradores_movimientos_sel(sucursal, cliente)
    'End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Convenio As Long, ByVal odata As DataSet, ByVal convenio_cadena As String) As Events

        Select Case Action

            Case Actions.Insert
                Validacion = ValidaConvenio(Convenio)
                If Validacion.ErrorFound Then Exit Function
                Validacion = ValidaConvenios(odata, convenio_cadena)
                If Validacion.ErrorFound Then Exit Function
        End Select
    End Function

    Public Function ValidaConvenio(ByVal Convenio As Long) As Events
        Dim oEvent As New Events
        If Convenio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Convenio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function



    Public Function ValidaConvenios(ByVal odata As DataSet, ByVal convenio As String) As Events
        Dim oevent As New Events
        If odata.Tables(0).Select("convenio = " & convenio & " and control in(0,1,2)").Length >= 1 Then
            oevent.Ex = Nothing
            oevent.Message = "El convenio ya esta asignado"
            Return oevent
        End If

        Return oevent
    End Function


#End Region


End Class
