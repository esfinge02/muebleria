Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAutorizacionesVentasPedidoFabrica
'DATE:		30/08/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - AutorizacionesVentasPedidoFabrica"
Public Class clsAutorizacionesVentasPedidoFabrica
    Private oAutorizacionesVentasPedidoFabrica As VillarrealData.clsAutorizacionesVentasPedidoFabrica

#Region "Constructores"
	Sub New()
        oAutorizacionesVentasPedidoFabrica = New VillarrealData.clsAutorizacionesVentasPedidoFabrica
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oAutorizacionesVentasPedidoFabrica.sp_autorizaciones_ventas_pedido_fabrica_ins(oData)
	End Function
    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oAutorizacionesVentasPedidoFabrica.sp_autorizaciones_ventas_pedido_fabrica_upd(oData)
    End Function
    Public Function Eliminar(ByVal folio_autorizacion As Integer) As Events
        Eliminar = oAutorizacionesVentasPedidoFabrica.sp_autorizaciones_ventas_pedido_fabrica_del(folio_autorizacion)
    End Function
    Public Function DespliegaDatos(ByVal folio_autorizacion As Integer) As Events
        DespliegaDatos = oAutorizacionesVentasPedidoFabrica.sp_autorizaciones_ventas_pedido_fabrica_sel(folio_autorizacion)
    End Function
    Public Function Listado() As Events
        Listado = oAutorizacionesVentasPedidoFabrica.sp_autorizaciones_ventas_pedido_fabrica_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal cliente As Long, ByVal vendedor As Long, ByVal articulo As Long, ByVal costo As Double, ByVal fecha As String, ByVal Bodega As String) As Events
        Validacion = ValidaCliente(cliente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaVendedor(vendedor)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulo(articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCosto(costo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodega(Bodega)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Private Function ValidacionUtilizada(ByVal Action As Actions, ByVal utilizada As Boolean) As Events
        Dim response As New Events

        If Action = Actions.Update And utilizada = True Then
            response.Ex = Nothing
            response.Message = "Esta Autorizacion ya ha sido utilizada"
            response.Layer = Events.ErrorLayer.BussinessLayer
        End If

        Return response

    End Function
    Private Function ValidaCliente(ByVal cliente As Long) As Events
        Dim oEvent As New Events
        If cliente < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaVendedor(ByVal vendedor As Long) As Events
        Dim oEvent As New Events
        If vendedor < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Vendedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaCosto(ByVal costo As Double) As Events
        Dim oEvent As New Events
        If costo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Costo es Requerido o Debe Ser mayor a 0"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFecha(ByVal fecha As String)
        Dim oEvent As New Events
        'Dim fecha As String
        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + " es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + " esta fuera del rango válido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Private Function ValidaBodega(ByVal bodega As String) As Events
        Dim oEvent As New Events
        If bodega.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function AutorizaPedido_fabrica_puntoventa_sel(ByVal folio_autorizacion As Long, ByVal cliente As Long, ByVal vendedor As Long, ByVal articulo As Long, ByVal cantidad As Long) As Events
        AutorizaPedido_fabrica_puntoventa_sel = oAutorizacionesVentasPedidoFabrica.sp_autoriza_ventas_pedido_fabrica_puntoventa_sel(folio_autorizacion, cliente, vendedor, articulo, cantidad)
    End Function

    Public Function Actualizar_autorizaciones_pedido_fabrica_punto_venta(ByVal folio_autorizacion As Long, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long) As Events
        Actualizar_autorizaciones_pedido_fabrica_punto_venta = Me.oAutorizacionesVentasPedidoFabrica.sp_autorizaciones_ventas_pedido_fabrica_punto_venta_upd(folio_autorizacion, sucursal_factura, serie_factura, folio_factura)
    End Function
#End Region

End Class
#End Region


