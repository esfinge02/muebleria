Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsClientesDirecciones

    Private oClientesDirecciones As VillarrealData.clsClientesDirecciones



#Region "Constructores"
    Sub New()
        oClientesDirecciones = New VillarrealData.clsClientesDirecciones
    End Sub
#End Region


#Region "M�todos"
   

    Public Function Insertar(ByVal cliente As Long, ByVal tipo_direccion As Long, ByVal domicilio As String, ByVal numero_exterior As String, ByVal numero_interior As String, _
    ByVal entrecalle As String, ByVal ycalle As String, ByVal cp As Long, ByVal colonia As Long, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal telefono_casa As String, _
    ByVal telefono_celular As String, ByVal observaciones As String, ByVal latitud As String, ByVal longitud As String) As Events

        Insertar = oClientesDirecciones.sp_clientes_direcciones_ins(cliente, tipo_direccion, domicilio, numero_exterior, numero_interior, entrecalle, ycalle, cp, colonia, estado, _
        municipio, ciudad, telefono_casa, telefono_celular, observaciones, latitud, longitud)
    End Function

    Public Function Actualizar(ByVal cliente As Long, ByVal tipo_direccion As Long, ByVal domicilio As String, ByVal numero_exterior As String, ByVal numero_interior As String, _
   ByVal entrecalle As String, ByVal ycalle As String, ByVal cp As Long, ByVal colonia As Long, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal telefono_casa As String, _
   ByVal telefono_celular As String, ByVal observaciones As String, ByVal latitud As String, ByVal longitud As String) As Events

        Actualizar = oClientesDirecciones.sp_clientes_direcciones_upd(cliente, tipo_direccion, domicilio, numero_exterior, numero_interior, entrecalle, ycalle, cp, colonia, estado, _
        municipio, ciudad, telefono_casa, telefono_celular, observaciones, latitud, longitud)
    End Function

    Public Function Eliminar(ByVal cliente As Long, ByVal tipo_direccion As Long) As Events
        Eliminar = oClientesDirecciones.sp_clientes_direcciones_del(cliente, tipo_direccion)
    End Function


    Public Function DespliegaDatos(ByVal cliente As Long, ByVal tipo_direccion As Long) As Events
        DespliegaDatos = oClientesDirecciones.sp_clientes_direcciones_sel(cliente, tipo_direccion)
    End Function

    Public Function Listado(ByVal cliente As Long) As Events
        Listado = oClientesDirecciones.sp_clientes_direcciones_grs(cliente)
    End Function


    Public Function Validacion(ByVal cliente As Long, ByVal tipo_direccion As Long, ByVal domicilio As String, ByVal numero_exterior As String, ByVal numero_interior As String, ByVal entrecalles As String, ByVal ycalles As String, ByVal cp As Long, ByVal colonia As Long, ByVal estado As Long, _
           ByVal municipio As Long, ByVal ciudad As Long, ByVal telefono_casa As String, ByVal telefono_celular As String, ByVal observaciones As String) As Events

        If numero_exterior.Length = 0 Then
            numero_exterior = "0"
        End If



        Validacion = ValidaTipoDireccion(tipo_direccion)
        If Validacion.ErrorFound Then Exit Function


        Validacion = ValidaDomicilio(domicilio)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaNumeroExterior(numero_exterior)
        If Validacion.ErrorFound Then Exit Function

        'Validacion = ValidaNumeroInterior(numero_interior)
        'If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaEntrecalles(entrecalles)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaYcalles(ycalles)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaEstado(estado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMunicipio(municipio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCiudad(ciudad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaColonia(colonia)
        If Validacion.ErrorFound Then Exit Function


    End Function

    Public Function ValidaTipoDireccion(ByVal tipo_direccion As Long) As Events
        Dim oEvent As New Events
        If tipo_direccion <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Tipo de Direcci�n es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


    Public Function ValidaDomicilio(ByVal Domicilio As String) As Events
        Dim oEvent As New Events
        '        If Domicilio.Trim.Length = 0 Then
        If Domicilio.Trim.Length < 4 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Calle es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaEntrecalles(ByVal entrecalles As String) As Events
        Dim oEvent As New Events
        If entrecalles.Trim.Length = 0 Then
            '    If entrecalles.Trim.Length < 4 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Entre Calle es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaYcalles(ByVal ycalles As String) As Events
        Dim oEvent As New Events
        If ycalles.Trim.Length = 0 Then
            'If ycalles.Trim.Length < 4 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Y Calle es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


    Public Function ValidaEstado(ByVal Estado As Long) As Events
        Dim oEvent As New Events
        If Estado = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMunicipio(ByVal municipio As String) As Events
        Dim oEvent As New Events
        If municipio = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Municipio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCiudad(ByVal Ciudad As Long) As Events
        Dim oEvent As New Events
        If Ciudad = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ciudad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaColonia(ByVal Colonia As Long) As Events
        Dim oEvent As New Events
        If Colonia <= 0 Or IsDBNull(Colonia) = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Colonia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaNumeroExterior(ByVal numero_exterior As Long) As Events
        Dim oEvent As New Events
        If numero_exterior <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Numero Exterior es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaNumeroInterior(ByVal numero_interior As Long) As Events
        Dim oEvent As New Events
        If numero_interior <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Numero Exterior es Interior"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaTelefonoCasa(ByVal telefono_casa As String) As Events
        Dim oEvent As New Events
        '        If Domicilio.Trim.Length = 0 Then
        If telefono_casa.Trim.Length < 4 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Telefono es Requerida. Si no tiene S/N"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaTelefonoCelular(ByVal telefono_celular As String) As Events
        Dim oEvent As New Events
        '        If Domicilio.Trim.Length = 0 Then
        If telefono_celular.Trim.Length < 4 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Celular o Nextel es Requerida. Si no tiene S/N"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaObservaciones(ByVal observaciones As String) As Events
        Dim oEvent As New Events
        '        If Domicilio.Trim.Length = 0 Then
        If observaciones.Trim.Length < 4 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Observaciones es Requerida. Si no tiene S/N"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function



#End Region

End Class
