Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsConfiguracionComisiones

    Private oConfiguracionComisiones As VillarrealData.clsConfiguracionComisiones

#Region "Constructores"
    Sub New()
        oConfiguracionComisiones = New VillarrealData.clsConfiguracionComisiones
    End Sub
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal partida As Long, ByVal limite_inferior_ventas As Double, ByVal limite_superior_ventas As Double, ByVal porcentaje_comision As Decimal) As Events
        Insertar = oConfiguracionComisiones.sp_configuracion_comisiones_ins(partida, limite_inferior_ventas, limite_superior_ventas, porcentaje_comision)
    End Function

    Public Function Eliminar() As Events
        Eliminar = oConfiguracionComisiones.sp_configuracion_comisiones_del()
    End Function

    Public Function Desplega() As Events
        Desplega = oConfiguracionComisiones.sp_configuracion_comisiones_grs()
    End Function

    Public Function ValidacionRangos(ByVal TablaRangos As DataTable) As Events
        Dim Response As New Events
        Dim i As Integer


        For i = 0 To TablaRangos.Rows.Count - 1

            Dim limite_inferior_actual As Double = TablaRangos.Rows(i).Item("limite_inferior_ventas")
            Dim limite_superior_actual As Double = TablaRangos.Rows(i).Item("limite_superior_ventas")

            If i = 0 Then
                If limite_superior_actual < limite_inferior_actual Then
                    Response.Message = "El Limite Superior es Menor al Limite Inferior en el registro " + (i + 1).ToString()
                    Exit For
                End If
            End If

            If i > 0 Then
                Dim limite_inferior_anterior As Double = TablaRangos.Rows(i - 1).Item("limite_inferior_ventas")
                Dim limite_superior_anterior As Double = TablaRangos.Rows(i - 1).Item("limite_superior_ventas")

                If limite_inferior_actual <= limite_superior_anterior Then
                    Response.Message = "El Limite Inferior del registro " + (i + 1).ToString() + " es Menor al Limite Superior del registro " + (i).ToString()
                    Exit For
                End If

                If limite_superior_actual <= limite_inferior_actual Then
                    Response.Message = "El Limite Superior es Menor al Limite Inferior en el registro " + (i + 1).ToString()
                    Exit For
                End If
            End If
        Next

        Return Response

    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal limite_inferior As Double, ByVal limite_superior As Double, ByVal porcentaje_comision As Decimal) As Events
        Validacion = ValidaLimiteInferior(limite_inferior)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaLimiteSuperior(limite_superior)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPorcentajeComision(porcentaje_comision)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Private Function ValidaLimiteInferior(ByVal limite_inferior As Double) As Events
        Dim oEvent As New Events
        If limite_inferior <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Limite Inferior es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaLimiteSuperior(ByVal limite_superior As Double) As Events
        Dim oEvent As New Events
        If limite_superior <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Limite Superior es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaPorcentajeComision(ByVal porcentaje_comision As Double) As Events
        Dim oEvent As New Events
        If porcentaje_comision <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Porcentaje Comisi�n es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function



#End Region

End Class
