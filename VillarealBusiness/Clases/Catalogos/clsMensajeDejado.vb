Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsMensajeDejado
    Private oMensajesD As VillarrealData.clsMensajeDejado

#Region "Constructores"
    Sub New()
        oMensajesD = New VillarrealData.clsMensajeDejado
    End Sub
#End Region


    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oMensajesD.sp_mensaje_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oMensajesD.sp_mensaje_upd(oData)
    End Function

    Public Function Eliminar(ByVal mensaje_dejado As Integer) As Events
        Eliminar = oMensajesD.sp_mensaje_del(mensaje_dejado)
    End Function

    Public Function DespliegaDatos(ByVal mensaje As Integer) As Events
        DespliegaDatos = oMensajesD.sp_mensaje_sel(mensaje)
    End Function

    Public Function Listado() As Events
        Listado = oMensajesD.sp_mensaje_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oMensajesD.sp_mensaje_grl()
    End Function

    Public Function LookupTipos(ByVal tipo_llamada As Integer) As Events
        LookupTipos = oMensajesD.sp_lkpmensaje_grl(tipo_llamada)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal nombre As String) As Events

        Validacion = ValidaNombre(nombre)
        If Validacion.ErrorFound Then Exit Function


    End Function

    Public Function ValidaNombre(ByVal nombre As String) As Events
        Dim oEvent As New Events
        If nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

End Class
