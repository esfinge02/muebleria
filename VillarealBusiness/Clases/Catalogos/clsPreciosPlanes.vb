Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPreciosPlanes
'DATE:		26/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - PreciosPlanes"
Public Class clsPreciosPlanes
	Private oPreciosPlanes As VillarrealData.clsPreciosPlanes

#Region "Constructores"
	Sub New()
		oPreciosPlanes = New VillarrealData.clsPreciosPlanes
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oPreciosPlanes.sp_precios_planes_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oPreciosPlanes.sp_precios_planes_upd(oData)
	End Function

	Public Function Eliminar(ByVal precio as long, ByVal plan_credito as long) As Events
		Eliminar = oPreciosPlanes.sp_precios_planes_del(precio, plan_credito)
	End Function

	Public Function DespliegaDatos(ByVal precio as long, ByVal plan_credito as long) As Events
		DespliegaDatos = oPreciosPlanes.sp_precios_planes_sel(precio, plan_credito)
	End Function

	Public Function Listado() As Events
		Listado = oPreciosPlanes.sp_precios_planes_grs()
	End Function

	Public Function Validacion(ByVal Action As Actions) As Events

	End Function



#End Region

End Class
#End Region


