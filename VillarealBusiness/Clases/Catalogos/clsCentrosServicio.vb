Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCentrosServicio
'DATE:		24/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CentrosServicio"
Public Class clsCentrosServicio
    Private oCentrosServicio As VillarrealData.clsCentrosServicio

#Region "Constructores"
    Sub New()
        oCentrosServicio = New VillarrealData.clsCentrosServicio
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oCentrosServicio.sp_centros_servicio_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oCentrosServicio.sp_centros_servicio_upd(oData)
    End Function

    Public Function Eliminar(ByVal centro As Long) As Events
        Eliminar = oCentrosServicio.sp_centros_servicio_del(centro)
    End Function

    Public Function DespliegaDatos(ByVal centro As Long) As Events
        DespliegaDatos = oCentrosServicio.sp_centros_servicio_sel(centro)
    End Function

    Public Function Listado() As Events
        Listado = oCentrosServicio.sp_centros_servicio_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oCentrosServicio.sp_centros_servicio_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String, ByVal Domicilio As String) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDomicilio(Domicilio)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDomicilio(ByVal Domicilio As String) As Events
        Dim oEvent As New Events
        If Domicilio.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Domicilio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


