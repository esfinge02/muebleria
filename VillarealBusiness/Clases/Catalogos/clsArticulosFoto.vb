Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsArticulosFoto
    Private oArticulosFoto As VillarrealData.clsArticulosFoto


#Region "Constructores"
    Sub New()
        oArticulosFoto = New VillarrealData.clsArticulosFoto
    End Sub
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal articulo As Long, ByVal foto As Object) As Events
        Insertar = oArticulosFoto.sp_articulos_foto_ins(articulo, foto)
    End Function
    Public Function Actualizar(ByVal articulo As Long, ByVal foto() As Byte) As Events
        Actualizar = oArticulosFoto.sp_articulos_foto_upd(articulo, foto)
    End Function
    Public Function Eliminar(ByVal articulo As Long) As Events
        Eliminar = oArticulosFoto.sp_articulos_foto_del(articulo)
    End Function
    Public Function DespliegaDatos(ByVal articulo As Long) As Events
        DespliegaDatos = oArticulosFoto.sp_articulos_foto_sel(articulo)
    End Function
#End Region
End Class
