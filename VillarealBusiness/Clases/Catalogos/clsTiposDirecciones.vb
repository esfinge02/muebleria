Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsTiposDirecciones
    Private oTiposDirecciones As VillarrealData.clsTiposDirecciones

#Region "Constructores"
    Sub New()
        oTiposDirecciones = New VillarrealData.clsTiposDirecciones
    End Sub
#End Region


    Public Function Lookup() As Events
        Lookup = oTiposDirecciones.sp_tipos_direcciones_grl()
    End Function


End Class
