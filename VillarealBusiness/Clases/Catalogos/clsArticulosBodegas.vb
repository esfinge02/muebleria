Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsArticulosBodegas
'DATE:		03/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ArticulosBodegas"
Public Class clsArticulosBodegas
    Private oArticulosBodegas As VillarrealData.clsArticulosBodegas

#Region "Constructores"
    Sub New()
        oArticulosBodegas = New VillarrealData.clsArticulosBodegas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal articulo As String) As Events
        Insertar = oArticulosBodegas.sp_articulos_bodegas_ins(oData, articulo)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal articulo As String) As Events
        Actualizar = oArticulosBodegas.sp_articulos_bodegas_upd(oData, articulo)
    End Function

    Public Function Eliminar(ByVal articulo As String, ByVal bodega As String) As Events
        Eliminar = oArticulosBodegas.sp_articulos_bodegas_del(articulo, bodega)
    End Function

    Public Function DespliegaDatos(ByVal articulo As String, ByVal bodega As Long) As Events
        DespliegaDatos = oArticulosBodegas.sp_articulos_bodegas_sel(articulo, bodega)
    End Function

    Public Function Listado(ByVal articulo As String) As Events
        Listado = oArticulosBodegas.sp_articulos_bodegas_grs(articulo)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Bodega As String, ByVal Ubicacion As String) As Events
        Validacion = ValidaBodega(Bodega)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaUbicacion(Ubicacion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaBodega(ByVal Bodega As String) As Events
        Dim oEvent As New Events
        If Bodega.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaUbicacion(ByVal Ubicacion As String) As Events
        Dim oEvent As New Events
        If Ubicacion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ubicaci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function




#End Region

End Class
#End Region


