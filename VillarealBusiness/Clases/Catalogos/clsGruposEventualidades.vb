Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsGruposEventualidades
'DATE:		03/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - GruposEventualidades"
Public Class clsGruposEventualidades
	Private oGruposEventualidades As VillarrealData.clsGruposEventualidades

#Region "Constructores"
	Sub New()
		oGruposEventualidades = New VillarrealData.clsGruposEventualidades
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oGruposEventualidades.sp_grupos_eventualidades_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oGruposEventualidades.sp_grupos_eventualidades_upd(oData)
	End Function

	Public Function Eliminar(ByVal grupo as long) As Events
		Eliminar = oGruposEventualidades.sp_grupos_eventualidades_del(grupo)
	End Function

	Public Function DespliegaDatos(ByVal grupo as long) As Events
		DespliegaDatos = oGruposEventualidades.sp_grupos_eventualidades_sel(grupo)
	End Function

	Public Function Listado() As Events
		Listado = oGruposEventualidades.sp_grupos_eventualidades_grs()
	End Function

    Public Function Lookup() As Events
        Lookup = oGruposEventualidades.sp_grupos_eventualidades_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


