Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsSucursales
'DATE:		06/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Sucursales"
Public Class clsSucursales
	Private oSucursales As VillarrealData.clsSucursales

#Region "Constructores"
	Sub New()
		oSucursales = New VillarrealData.clsSucursales
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oSucursales.sp_sucursales_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oSucursales.sp_sucursales_upd(oData)
	End Function

	Public Function Eliminar(ByVal sucursal as long) As Events
		Eliminar = oSucursales.sp_sucursales_del(sucursal)
	End Function

	Public Function DespliegaDatos(ByVal sucursal as long) As Events
		DespliegaDatos = oSucursales.sp_sucursales_sel(sucursal)
    End Function

    Public Function ActualizarFolioPromocion(ByVal sucursal As Long, ByVal folio_promocion As Long) As Events
        ActualizarFolioPromocion = oSucursales.sp_sucursales_folio_promocion_upd(sucursal, folio_promocion)
    End Function

    Public Function Listado() As Events
        Listado = oSucursales.sp_sucursales_grs()
    End Function

    Public Function Lookup(Optional ByVal sucursal_dependencia As Long = -1) As Events
        Lookup = oSucursales.sp_sucursales_grl(sucursal_dependencia)
    End Function

    Public Function LookupExistenciasSucursales(Optional ByVal sucursal_dependencia As Long = -1) As Events
        LookupExistenciasSucursales = oSucursales.sp_sucursales_existencias_grl(sucursal_dependencia)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal Direccion As String, ByVal Colonia As String, ByVal Cp As Long, ByVal Ciudad As String, ByVal Estado As String, ByVal Gerente As String, ByVal serie_interes As String, ByVal precio_minimo_venta As Long, ByVal concepto_salida As String, ByVal concepto_entrada As String, ByVal serie_timbrado As String) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDireccion(Direccion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaColonia(Colonia)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCp(Cp)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCiudad(Ciudad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaEstado(Estado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaGerente(Gerente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerieInteres(serie_interes)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaPrecioMinimoVEnta(precio_minimo_venta)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaConceptoSalida(concepto_salida)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConceptoEntrada(concepto_entrada)
        If Validacion.ErrorFound Then Exit Function

        'Validacion = ValidaSerieTimbrado(serie_timbrado)
        'If Validacion.ErrorFound Then Exit Function


    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre de la Sucursal es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDireccion(ByVal Direccion As String) As Events
        Dim oEvent As New Events
        If Direccion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Dirección es Requerrida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaColonia(ByVal Colonia As String) As Events
        Dim oEvent As New Events
        If Colonia.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Colonia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCp(ByVal Cp As Long) As Events
        Dim oEvent As New Events
        If Cp <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El CP es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCiudad(ByVal Ciudad As String) As Events
        Dim oEvent As New Events
        If Ciudad.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ciudad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaEstado(ByVal Estado As String) As Events
        Dim oEvent As New Events
        If Estado.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaGerente(ByVal Gerente As String) As Events
        Dim oEvent As New Events
        If Gerente.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Gerente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSerieNotacargo(ByVal serie_recibo As String) As Events
        Dim oEvent As New Events
        If serie_recibo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie de las Notas de Cargo es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSerieInteres(ByVal serie_intereses As String) As Events
        Dim oEvent As New Events
        If serie_intereses.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie de los Intereses es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSerieTimbrado(ByVal serie_timbrado As String) As Events
        Dim oEvent As New Events
        If serie_timbrado.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie del timbrado es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaConceptoSalida(ByVal concepto_salida As String) As Events
        Dim oEvent As New Events
        If concepto_salida.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto de Salida de Traspaso es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConceptoEntrada(ByVal concepto_entrada As String) As Events
        Dim oEvent As New Events
        If concepto_entrada.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto de Entrada de Traspaso es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaPrecioMinimoVEnta(ByVal precio_minimo_venta As Long) As Events
        Dim oEvent As New Events
        If precio_minimo_venta < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Precio Mínimo Venta es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


