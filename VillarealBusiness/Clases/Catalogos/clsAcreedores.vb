Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAcreedores
'DATE:		16/10/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Acreedores"
Public Class clsAcreedores
    Private oAcreedores As VillarrealData.clsAcreedores

#Region "Constructores"
    Sub New()
        oAcreedores = New VillarrealData.clsAcreedores
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oAcreedores.sp_acreedores_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oAcreedores.sp_acreedores_upd(oData)
    End Function

    Public Function Eliminar(ByVal acreedor As Long) As Events
        Eliminar = oAcreedores.sp_acreedores_del(acreedor)
    End Function

    Public Function DespliegaDatos(ByVal acreedor As Long) As Events
        DespliegaDatos = oAcreedores.sp_acreedores_sel(acreedor)
    End Function

    Public Function Listado() As Events
        Listado = oAcreedores.sp_acreedores_grs()
    End Function

    Public Function Lookup(Optional ByVal tipo As Long = -1) As Events
        Lookup = oAcreedores.sp_acreedores_grl(tipo)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal nombre As String, ByVal domicilio As String, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal colonia As Long) As Events
        Validacion = ValidaNombre(nombre)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaDomicilio(domicilio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaEstado(estado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMunicipio(municipio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCiudad(ciudad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaColonia(colonia)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Acreedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDomicilio(ByVal Domicilio As String) As Events
        Dim oEvent As New Events
        If Domicilio.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Calle es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaEstado(ByVal Estado As Long) As Events
        Dim oEvent As New Events
        If Estado <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMunicipio(ByVal municipio As String) As Events
        Dim oEvent As New Events
        If municipio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Municipio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCiudad(ByVal Ciudad As Long) As Events
        Dim oEvent As New Events
        If Ciudad <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ciudad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaColonia(ByVal Colonia As Long) As Events
        Dim oEvent As New Events
        If Colonia <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Colonia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


