Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsIdentificadoresPermisos

    Private oIdentificadoresPermisos As VillarrealData.clsIdentificadoresPermisos


#Region "Constructores"
    Sub New()
        oIdentificadoresPermisos = New VillarrealData.clsIdentificadoresPermisos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oIdentificadoresPermisos.sp_identificadores_permisos_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oIdentificadoresPermisos.sp_identificadores_permisos_upd(oData)
    End Function

    Public Function Eliminar(ByVal sistema As String, ByVal identificador As String) As Events
        Eliminar = oIdentificadoresPermisos.sp_identificadores_permisos_del(sistema, identificador)
    End Function

    Public Function DespliegaDatos(ByVal sistema As String, ByVal identificador As String) As Events
        DespliegaDatos = oIdentificadoresPermisos.sp_identificadores_permisos_sel(sistema, identificador)
    End Function

    Public Function Listado(ByVal sistema As String) As Events
        Listado = oIdentificadoresPermisos.sp_identificadores_permisos_grs(sistema)
    End Function

    Public Function Lookup() As Events
        'Lookup = oIdentificadoresPermisos.sp_unidades_grl()
    End Function


#Region "Validaciones"

    Public Function Validacion(ByVal Action As Actions, ByVal sistema As String, ByVal identificador As String, ByVal Descripcion As String) As Events
        Validacion = ValidaIdentificador(identificador)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

        If Action <> Actions.Insert Then Exit Function

        Validacion = ValidaExisteIdentificador(sistema, identificador)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaIdentificador(ByVal identificador As String) As Events
        Dim oEvent As New Events
        If identificador.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El identificador es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaExisteIdentificador(ByVal sistema As String, ByVal identificador As String) As Events
        Dim oEvent As New Events

        oEvent = Me.oIdentificadoresPermisos.sp_identificadores_permisos_exs(sistema, identificador)
        If Not oEvent.ErrorFound Then
            If oEvent.Value > 0 Then
                oEvent.Message = "El identificador ya existe "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
#End Region


    Public Function InsertarPermisos(ByVal usuario As String, ByVal sistema As String, ByVal identificador As String, ByVal permitir As Boolean, ByVal denegar As Boolean) As Events
        InsertarPermisos = oIdentificadoresPermisos.sp_permisos_extendidos_ins(usuario, sistema, identificador, permitir, denegar)
    End Function

    Public Function DesplegarPermisos(ByVal usuario As String, ByVal sistema As String) As Events
        DesplegarPermisos = oIdentificadoresPermisos.sp_permisos_extendidos_grs(usuario, sistema)
    End Function

    Public Function ListadoPermisos(ByVal usuario As String, ByVal sistema As String) As Events
        ListadoPermisos = oIdentificadoresPermisos.sp_permisos_extendidos_usuario_grs(usuario, sistema)
    End Function

#End Region


End Class
