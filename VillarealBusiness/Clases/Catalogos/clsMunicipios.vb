Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMunicipios
'DATE:		02/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Municipios"
Public Class clsMunicipios
    Private oMunicipios As VillarrealData.clsMunicipios

#Region "Constructores"
    Sub New()
        oMunicipios = New VillarrealData.clsMunicipios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oMunicipios.sp_municipios_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oMunicipios.sp_municipios_upd(oData)
    End Function

    Public Function Eliminar(ByVal estado As String, ByVal municipio As Long) As Events
        Eliminar = oMunicipios.sp_municipios_del(estado, municipio)
    End Function

    Public Function DespliegaDatos(ByVal estado As Long, ByVal municipio As Long) As Events
        DespliegaDatos = oMunicipios.sp_municipios_sel(estado, municipio)
    End Function

    Public Function Listado() As Events
        Listado = oMunicipios.sp_municipios_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String, ByVal Estado As Long) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaEstado(Estado)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaEstado(ByVal Estado As Long) As Events
        Dim oEvent As New Events
        If Estado <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCiudad(ByVal ciudad As Long) As Events
        Dim oEvent As New Events
        If ciudad <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ciudad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvent As New Events
        If descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function Lookup(Optional ByVal estado As Long = -1) As Events
        Lookup = oMunicipios.sp_municipios_grl(estado)
    End Function

#End Region

End Class
#End Region


