Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsTiposDepositos
    Private otiposdepositos As VillarrealData.clsTiposDepositos

#Region "Constructores"
    Sub New()
        otiposdepositos = New VillarrealData.clsTiposDepositos
    End Sub
#End Region

    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = otiposdepositos.sp_tipos_depositos_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = otiposdepositos.sp_tipos_depositos_upd(oData)
    End Function

    Public Function Eliminar(ByVal Tipo_deposito As Long) As Events
        Eliminar = otiposdepositos.sp_tipos_depositos_del(Tipo_deposito)
    End Function

    Public Function DespliegaDatos(ByVal Tipo_deposito As Long) As Events
        DespliegaDatos = otiposdepositos.sp_tipos_depositos_sel(Tipo_deposito)
    End Function

    Public Function Listado() As Events
        Listado = otiposdepositos.sp_tipos_depositos_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = otiposdepositos.sp_tipos_depositos_grl()
    End Function

    Public Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvent As New Events
        If descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
End Class
