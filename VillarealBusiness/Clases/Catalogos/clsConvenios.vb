Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class clsConvenios
    Private oConvenios As VillarrealData.clsConvenios

#Region "Constructores"
    Sub New()
        oConvenios = New VillarrealData.clsConvenios
    End Sub
#End Region


    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oConvenios.sp_convenios_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oConvenios.sp_convenios_upd(oData)
    End Function

    Public Function Eliminar(ByVal unidad As String) As Events
        Eliminar = oConvenios.sp_convenios_del(unidad)
    End Function

    Public Function DespliegaDatos(ByVal unidad As String) As Events
        DespliegaDatos = oConvenios.sp_convenios_sel(unidad)
    End Function

    Public Function Listado() As Events
        Listado = oConvenios.sp_convenios_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oConvenios.sp_convenios_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal nombre As String) As Events

        Validacion = ValidaNombre(nombre)
        If Validacion.ErrorFound Then Exit Function


    End Function

    Public Function ValidaZona(ByVal zona As Integer) As Events
        Dim oEvent As New Events
        If zona <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Clave de la Zona es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNombre(ByVal nombre As String) As Events
        Dim oEvent As New Events
        If nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

End Class
