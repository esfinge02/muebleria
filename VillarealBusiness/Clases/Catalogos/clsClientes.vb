Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientes
'DATE:		04/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Clientes"
Public Class clsClientes
    Private oClientes As VillarrealData.clsClientes

#Region "Constructores"
    Sub New()
        oClientes = New VillarrealData.clsClientes
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal rfc_completo As Boolean, ByVal tipo_captura As Char) As Events
        ' Insertar = oClientes.sp_clientes_ins(oData, rfc_completo, tipo_captura)
    End Function


    Public Function Insertar(ByRef cliente As Long, ByVal nombre As String, ByVal nombres As String, ByVal paterno As String, ByVal materno As String, ByVal rfc As String, _
    ByVal curp As String, ByVal domicilio As String, ByVal numero_exterior As String, ByVal numero_interior As String, ByVal entrecalle As String, ByVal ycalle As String, ByVal colonia As Long, _
    ByVal estado As Long, ByVal ciudad As Long, ByVal municipio As Long, ByVal cp As Long, ByVal telefono1 As String, ByVal telefono2 As String, ByVal fax As String, ByVal ocupacion As String, ByVal ingresos As Double, _
    ByVal puesto As String, ByVal departamento As String, ByVal telefonos_laboral As String, ByVal antiguedad_laboral As String, ByVal notas As String, ByVal cliente_aval As Long, ByVal aval As String, _
    ByVal domicilio_aval As String, ByVal colonia_aval As String, ByVal telefono_aval As String, ByVal ciudad_aval As String, ByVal estado_aval As String, ByVal anios_aval As Long, ByVal parentesco_aval As String, _
    ByVal tipo_cobro As Char, ByVal fecha_alta As DateTime, ByVal persona As Char, ByVal sucursal As Long, ByVal no_paga_comision As Boolean, ByVal rfc_completo As Boolean, ByVal cliente_dependencias As Long, ByVal limite_credito As Double, _
    ByVal tipo_captura As Char, ByVal localizado As Boolean, ByVal nombre_conyuge As String, ByVal trabaja_en_conyuge As String, ByVal puesto_conyuge As String, ByVal departamento_conyuge As String, ByVal telefonos_laboral_conyuge As String, _
    ByVal motivos_no_localizable As String, ByVal usuario_no_localizable As String, ByVal activo As Boolean, ByVal estado_civil As Char, ByVal nombre_referencia As String, ByVal domicilio_referencia As String, ByVal colonia_referencia As String, _
    ByVal telefono_referencia As String, ByVal ciudad_referencia As String, ByVal estado_referencia As String, ByVal trabaja_en_referencia As String, ByVal parentesco_referencia As String, ByVal forma_pago As Long, _
    ByVal ultimos_digitos_cuenta As Long, ByVal email As String, ByVal facturacion_especial As Boolean, ByVal fecha_nacimiento As DateTime) As Events
        Insertar = oClientes.sp_clientes_ins(cliente, nombre, nombres, paterno, materno, rfc, curp, domicilio, numero_exterior, numero_interior, entrecalle, ycalle, colonia, _
     estado, ciudad, municipio, cp, telefono1, telefono2, fax, ocupacion, ingresos, puesto, departamento, telefonos_laboral, antiguedad_laboral, notas, cliente_aval, aval, _
     domicilio_aval, colonia_aval, telefono_aval, ciudad_aval, estado_aval, anios_aval, parentesco_aval, tipo_cobro, fecha_alta, persona, sucursal, no_paga_comision, _
     rfc_completo, cliente_dependencias, limite_credito, tipo_captura, localizado, nombre_conyuge, trabaja_en_conyuge, puesto_conyuge, departamento_conyuge, _
     telefonos_laboral_conyuge, motivos_no_localizable, usuario_no_localizable, activo, estado_civil, nombre_referencia, domicilio_referencia, colonia_referencia, _
     telefono_referencia, ciudad_referencia, estado_referencia, trabaja_en_referencia, parentesco_referencia, forma_pago, _
     ultimos_digitos_cuenta, email, facturacion_especial, fecha_nacimiento)
    End Function

    Public Function InsertarDePlantilla(ByRef oData As DataSet, ByVal nombre As String, ByVal rfc As String, ByVal nombres As String, ByVal paterno As String, ByVal materno As String, ByVal fecha_alta As DateTime, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal colonia As Long, ByVal sucursal As Long) As Events
        InsertarDePlantilla = oClientes.sp_clientes_ins_plantilla(oData, nombre, rfc, nombres, paterno, materno, fecha_alta, estado, municipio, ciudad, colonia, sucursal)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal rfc_completo As Boolean) As Events
        Actualizar = oClientes.sp_clientes_upd(oData, rfc_completo)
    End Function


    Public Function Actualizar(ByVal cliente As Long, ByVal nombre As String, ByVal nombres As String, ByVal paterno As String, ByVal materno As String, ByVal rfc As String, ByVal curp As String, ByVal domicilio As String, _
   ByVal numero_exterior As String, ByVal numero_interior As String, ByVal entrecalle As String, ByVal ycalle As String, ByVal colonia As Long, ByVal estado As Long, ByVal ciudad As Long, ByVal municipio As Long, ByVal cp As Long, _
  ByVal telefono1 As String, ByVal telefono2 As String, ByVal fax As String, ByVal ocupacion As String, ByVal ingresos As Double, ByVal puesto As String, ByVal departamento As String, ByVal telefonos_laboral As String, _
  ByVal antiguedad_laboral As String, ByVal notas As String, ByVal cliente_aval As Long, ByVal aval As String, ByVal domicilio_aval As String, ByVal colonia_aval As String, ByVal telefono_aval As String, ByVal ciudad_aval As String, _
  ByVal estado_aval As String, ByVal anios_aval As Long, ByVal parentesco_aval As String, ByVal tipo_cobro As Char, ByVal fecha_alta As DateTime, ByVal persona As Char, ByVal sucursal As Long, ByVal no_paga_comision As Boolean, ByVal rfc_completo As Boolean, _
  ByVal cliente_dependencias As Long, ByVal limite_credito As Double, ByVal localizado As Boolean, ByVal nombre_conyuge As String, ByVal trabaja_en_conyuge As String, ByVal puesto_conyuge As String, ByVal departamento_conyuge As String, _
  ByVal telefonos_laboral_conyuge As String, ByVal motivos_no_localizable As String, ByVal usuario_no_localizable As String, ByVal activo As Boolean, ByVal estado_civil As Char, ByVal nombre_referencia As String, ByVal domicilio_referencia As String, ByVal colonia_referencia As String, _
   ByVal telefono_referencia As String, ByVal ciudad_referencia As String, ByVal estado_referencia As String, ByVal trabaja_en_referencia As String, ByVal parentesco_referencia As String, ByVal forma_pago As Long, _
   ByVal ultimos_digitos_cuenta As Long, ByVal email As String, ByVal facturacion_especial As Boolean, ByVal fecha_nacimiento As DateTime) As Events
        Actualizar = oClientes.sp_clientes_upd(cliente, nombre, nombres, paterno, materno, rfc, curp, domicilio, numero_exterior, numero_interior, entrecalle, ycalle, colonia, _
            estado, ciudad, municipio, cp, telefono1, telefono2, fax, ocupacion, ingresos, puesto, departamento, telefonos_laboral, antiguedad_laboral, notas, cliente_aval, aval, _
            domicilio_aval, colonia_aval, telefono_aval, ciudad_aval, estado_aval, anios_aval, parentesco_aval, tipo_cobro, fecha_alta, persona, sucursal, no_paga_comision, _
            rfc_completo, cliente_dependencias, limite_credito, localizado, nombre_conyuge, trabaja_en_conyuge, puesto_conyuge, departamento_conyuge, _
            telefonos_laboral_conyuge, motivos_no_localizable, usuario_no_localizable, activo, estado_civil, nombre_referencia, domicilio_referencia, colonia_referencia, _
            telefono_referencia, ciudad_referencia, estado_referencia, trabaja_en_referencia, parentesco_referencia, forma_pago, _
            ultimos_digitos_cuenta, email, facturacion_especial, fecha_nacimiento)

    End Function

    Public Function Eliminar(ByVal cliente As Long) As Events
        Eliminar = oClientes.sp_clientes_del(cliente)
    End Function

    Public Function DespliegaDatos(ByVal cliente As Long, Optional ByVal sucursal_caja As Long = -1) As Events
        DespliegaDatos = oClientes.sp_clientes_sel(cliente, sucursal_caja)
    End Function
    Public Function DespliegaDatosJuridico(ByVal cliente As Long) As Events
        DespliegaDatosJuridico = oClientes.sp_clientes_datos_juridico(cliente)
    End Function

    Public Function Existe(ByVal cliente As Long) As Events
        Existe = oClientes.sp_clientes_exs(cliente)
    End Function


    Public Function ExisteEnPlantilla(ByVal cliente As Long) As Events
        ExisteEnPlantilla = oClientes.sp_clientes_exs_plantilla(cliente)
    End Function

    Public Function Listado(ByVal estado As Integer) As Events
        Listado = oClientes.sp_clientes_grs(estado)
    End Function

    Public Function Listado() As Events
        Listado = oClientes.sp_clientes_grs(-1)
    End Function

#Region "Datos Adicionales"
    Public Function InsertarDatosAdicionales(ByVal Cliente As Long, ByVal telefono_nextel As String, ByVal extension As Long, ByVal tipo_casa As Char, ByVal pago_casa As Double, _
       ByVal area_laboral As String, ByVal direccion_conyuge As String, ByVal email_conyuge As String, ByVal telefono_conyuge As String, ByVal direccion_trabajo_conyuge As String, ByVal cp_aval As Long, ByVal telefono_celular_aval As String, _
       ByVal telefono_nextel_aval As String, ByVal telefono_oficina_aval As String, ByVal extension_aval As Long, ByVal estado_civil_aval As Char, ByVal email_aval As String, ByVal observaciones_aval As String, ByVal cp_referencia As Long, _
       ByVal telefono_celular_referencia As String, ByVal telefono_nextel_referencia As String, ByVal telefono_oficina_referencia As String, ByVal extension_referencia As Long, ByVal otros_referencia As String, ByVal nombre_referencia2 As String, _
       ByVal domicilio_referencia2 As String, ByVal colonia_referencia2 As String, ByVal telefono_referencia2 As String, ByVal ciudad_referencia2 As String, ByVal estado_referencia2 As String, ByVal cp_referencia2 As String, ByVal trabaja_en_referencia2 As String, _
       ByVal parentesco_referencia2 As String, ByVal telefono_celular_referencia2 As String, ByVal telefono_nextel_referencia2 As String, ByVal telefono_oficina_referencia2 As String, ByVal extension_referencia2 As String, ByVal otros_referencia2 As String, _
       ByVal nombre_referencia3 As String, ByVal domicilio_referencia3 As String, ByVal colonia_referencia3 As String, ByVal telefono_referencia3 As String, ByVal ciudad_referencia3 As String, ByVal estado_referencia3 As String, ByVal cp_referencia3 As String, _
       ByVal trabaja_en_referencia3 As String, ByVal parentesco_referencia3 As String, ByVal telefono_celular_referencia3 As String, ByVal telefono_nextel_referencia3 As String, ByVal telefono_oficina_referencia3 As String, ByVal extension_referencia3 As String, _
       ByVal otros_referencia3 As String, ByVal persona_autoriza As String, ByVal fecha_actualizacion As DateTime, ByVal email_referencia As String, ByVal email_referencia2 As String, ByVal email_referencia3 As String, _
       ByVal direccion_laboral As String, ByVal estado_laboral As Long, ByVal ciudad_laboral As Long, ByVal municipio_laboral As Long, ByVal colonia_laboral As Long, ByVal cp_laboral As Long) As Events

        InsertarDatosAdicionales = oClientes.sp_clientes_datos_adicionales_ins(Cliente, telefono_nextel, extension, tipo_casa, pago_casa, _
        area_laboral, direccion_conyuge, email_conyuge, telefono_conyuge, direccion_trabajo_conyuge, cp_aval, telefono_celular_aval, _
        telefono_nextel_aval, telefono_oficina_aval, extension_aval, estado_civil_aval, email_aval, observaciones_aval, cp_referencia, _
        telefono_celular_referencia, telefono_nextel_referencia, telefono_oficina_referencia, extension_referencia, otros_referencia, nombre_referencia2, _
        domicilio_referencia2, colonia_referencia2, telefono_referencia2, ciudad_referencia2, estado_referencia2, cp_referencia2, trabaja_en_referencia2, _
        parentesco_referencia2, telefono_celular_referencia2, telefono_nextel_referencia2, telefono_oficina_referencia2, extension_referencia2, otros_referencia2, _
        nombre_referencia3, domicilio_referencia3, colonia_referencia3, telefono_referencia3, ciudad_referencia3, estado_referencia3, cp_referencia3, _
        trabaja_en_referencia3, parentesco_referencia3, telefono_celular_referencia3, telefono_nextel_referencia3, telefono_oficina_referencia3, extension_referencia3, _
        otros_referencia3, persona_autoriza, fecha_actualizacion, email_referencia, email_referencia2, email_referencia3, _
        direccion_laboral, estado_laboral, ciudad_laboral, municipio_laboral, colonia_laboral, cp_laboral)
    End Function
#End Region

#Region "Lookups"


    Public Function LookupRepartoClienteExcluido(Optional ByVal sucursal_dependencia As Boolean = False, Optional ByVal cliente_exluido As Long = 0) As Events
        LookupRepartoClienteExcluido = oClientes.sp_clientes_grl_reparto(sucursal_dependencia, cliente_exluido)
    End Function

    Public Function LookupAvalClienteExcluido(ByVal cliente_exluido As Long) As Events
        LookupAvalClienteExcluido = oClientes.sp_clientes_grl_avales(cliente_exluido)
    End Function

    Public Function LookupDisponiblesJuridicos() As Events
        LookupDisponiblesJuridicos = oClientes.sp_clientes_disponibles_juridico_grl()
    End Function


    Public Function LookupCaja() As Events
        LookupCaja = oClientes.sp_clientes_grl_caja()
    End Function

    'Public Function LookupCajaTelefonoLlamada() As Events
    '    LookupCajaTelefonoLlamada = oClientes.sp_clientes_grl_caja_telefono_llamada()
    'End Function

    Public Function LookupCancelacionAbonos() As Events
        LookupCancelacionAbonos = oClientes.sp_clientes_cancelacion_abonos_grl()
    End Function

    Public Function LookupCliente(Optional ByVal sucursal_actual As Long = -1, Optional ByVal anioactual As Boolean = False) As Events
        LookupCliente = oClientes.sp_clientes_grl_cliente(sucursal_actual, anioactual)
    End Function


    Public Function LookupClienteEjecutivo(ByVal sucursal As Long) As Events
        LookupClienteEjecutivo = oClientes.sp_clientes_grl_ejecutivo(sucursal)
    End Function


    Public Function LookupClientesConvenios() As Events
        LookupClientesConvenios = oClientes.sp_clientes_convenios_grl
    End Function

    Public Function LookupFechaVenta() As Events
        LookupFechaVenta = oClientes.sp_clientes_grl_fecha_venta()
    End Function

    Public Function LookupLlenado(Optional ByVal sucursal_dependencia As Boolean = False, Optional ByVal cliente As Long = 0) As Events
        LookupLlenado = oClientes.sp_clientes_grl_llenado(sucursal_dependencia, cliente)
    End Function

    Public Function LookupPlantilla(Optional ByVal dependencia As Long = -1) As Events
        LookupPlantilla = oClientes.sp_clientes_grl_plantilla(dependencia)
    End Function

#End Region

#Region "Funcionalidad"

    Public Function ClientesNoPaganComisiones(ByVal no_pagan_comision As Boolean) As Events
        ClientesNoPaganComisiones = oClientes.sp_clientes_no_pagan_comisiones(no_pagan_comision)
    End Function

    Public Function ActualizaNoPagaComision(ByVal cliente As Long, ByVal no_pagan_comision As Boolean) As Events
        ActualizaNoPagaComision = oClientes.sp_clientes_no_paga_comision_upd(cliente, no_pagan_comision)
    End Function

    Public Function RecalcularSaldosClientes(ByVal cliente As Long) As Events
        RecalcularSaldosClientes = oClientes.RecalcularSaldosClientes(cliente)
    End Function

    Public Function Recalcula_saldo_documentos(ByVal cliente As Long) As Events
        Recalcula_saldo_documentos = oClientes.recalcula_saldo_documentos(cliente)
    End Function

#End Region

#Region "Validaciones"


    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal Paterno As String, ByVal Rfc As String, ByVal persona As Char, ByRef rfc_completo As Boolean, _
    ByVal domicilio As String, ByVal cp As Long, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal colonia As Long, ByVal cobradores As Long, ByVal ingresos As Double, ByVal Tipo_Cobro As Char, ByVal sucursal As Long, ByVal UsuarioActual As String, ByVal NombreM As String, _
    ByVal estado_civil As Char, ByVal nombre_conyuge As String, ByVal telefonos_laboral_conyuge As String, ByVal solicita_ultimos_digitos As Boolean, ByVal ultimos_digitos As Long) As Events
        If persona = "F" Then
            Validacion = ValidaNombre(Nombre)
        Else
            Validacion = ValidaNombreM(NombreM)
        End If

        If Validacion.ErrorFound Then Exit Function
        If persona = "F" Then
            Validacion = ValidaApellidoPaterno(Paterno)
        End If
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaRfc(Rfc, persona, rfc_completo)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaDomicilio(domicilio)
        If Validacion.ErrorFound Then Exit Function

        'Validacion = ValidaCp(cp)
        'If Validacion.ErrorFound Then Exit Function


        Validacion = ValidaEstado(estado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMunicipio(municipio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCiudad(ciudad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaColonia(colonia)
        If Validacion.ErrorFound Then Exit Function

        If persona = "F" Then
            Validacion = ValidaIngresos(ingresos)
            If Validacion.ErrorFound Then Exit Function
        End If

        If Tipo_Cobro = "C" Then
            Validacion = ValidaCobradores(cobradores)
            If Validacion.ErrorFound Then Exit Function
        End If

        If estado_civil = "C" Then
            Validacion = ValidaNombreConyuge(nombre_conyuge)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaTelefonosConyuge(telefonos_laboral_conyuge)
            If Validacion.ErrorFound Then Exit Function
        End If

        If (Action = Actions.Insert Or Action = Actions.Update) And UsuarioActual.ToUpper.Equals("SUPER") Then
            Validacion = ValidaSucursal(sucursal)
            If Validacion.ErrorFound Then Exit Function
        End If

        If (Action = Actions.Insert Or Action = Actions.Update) And solicita_ultimos_digitos Then
            Validacion = ValidaUltimosDigitos(ultimos_digitos)
            If Validacion.ErrorFound Then Exit Function
        End If

    End Function

    Private Function ValidaUltimosDigitos(ByVal ultimos_digitos As Long) As Events
        Dim oEvent As New Events
        If ultimos_digitos <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los Últimos Dígitos son Requeridos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaNombreConyuge(ByVal nombre_conyuge As String) As Events
        Dim oEvent As New Events
        If nombre_conyuge.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Conyuge es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaTelefonosConyuge(ByVal telefono_conyuge As String) As Events
        Dim oEvent As New Events
        If telefono_conyuge.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Teléfono del Conyuge es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNombreM(ByVal NombreM As String) As Events
        Dim oEvent As New Events
        If NombreM.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaApellidoPaterno(ByVal Paterno As String) As Events
        Dim oEvent As New Events
        If Paterno.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Apellido Paterno del Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaRfc(ByVal Rfc As String, ByVal persona As Char, Optional ByRef rfc_completo As Boolean = False) As Events
        Dim oEvent As New Events
        Dim letras As String
        Dim numeros As String
        Dim fecha1 As Integer
        Dim fecha2 As Integer
        Dim fecha3 As Integer
        Dim homoclave As String
        Dim letra As Char

        If Rfc = "XAXX010101000" Then
            Return oEvent
        End If

        If Rfc.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Rfc es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If persona = "F" Then  'VALIDACION DEL RFC PARA PERSONA FISICA

            If Rfc.Length <> 13 And Rfc.Length <> 10 Then
                'oEvent.Ex = Nothing
                'oEvent.Message = "La Longitud del RFC es Incorrecta"
                'oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If

            letras = Rfc.Substring(0, 4)
            numeros = Rfc.Substring(4, 6)
            For Each letra In letras
                If letra <> "&" Then
                    If Not Char.IsLetter(letra) Then
                        oEvent.Ex = Nothing
                        oEvent.Message = "Los Primeros 4 Caracteres del RFC Deben ser Letras"
                        oEvent.Layer = Events.ErrorLayer.BussinessLayer
                        Return oEvent
                    End If
                End If
            Next
            For Each letra In numeros
                If Not Char.IsNumber(letra) Then
                    oEvent.Ex = Nothing
                    oEvent.Message = "Incorrecto el RFC en los Datos de la Fecha"
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            Next

            If Rfc.Length = 13 Then
                homoclave = Rfc.Substring(10, 3)
                For Each letra In homoclave
                    If Not Char.IsLetterOrDigit(letra) Then
                        oEvent.Ex = Nothing
                        oEvent.Message = "Los Últimos 3 Caracteres del RFC Deben ser Letras o Números"
                        oEvent.Layer = Events.ErrorLayer.BussinessLayer
                        Return oEvent
                    End If
                Next
            End If

            fecha2 = Rfc.Substring(6, 2) 'mes
            fecha3 = Rfc.Substring(8, 2) 'dia
            If fecha2 > 12 Or fecha3 > 31 Then
                oEvent.Ex = Nothing
                oEvent.Message = "Incorrecto el RFC en los Datos de la Fecha"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If

            If Rfc.Length = 13 Then
                rfc_completo = True
            Else
                rfc_completo = False
            End If

        Else 'VALIDACION DEL RFC PARA PERSONA MORAL

            If Rfc.Length <> 12 And Rfc.Length <> 9 Then
                'oEvent.Ex = Nothing
                'oEvent.Message = "La Longitud del RFC es Incorrecta"
                'oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If

            letras = Rfc.Substring(0, 3)
            numeros = Rfc.Substring(3, 6)
            For Each letra In letras
                If letra <> "&" Then
                    If Not Char.IsLetter(letra) Then
                        oEvent.Ex = Nothing
                        oEvent.Message = "Los Primeros 3 Caracteres del RFC Deben ser Letras"
                        oEvent.Layer = Events.ErrorLayer.BussinessLayer
                        Return oEvent
                    End If
                End If
            Next
            For Each letra In numeros
                If Not Char.IsNumber(letra) Then
                    oEvent.Ex = Nothing
                    oEvent.Message = "Incorrecto el RFC en los Datos de la Fecha"
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            Next

            'fecha1 = Rfc.Substring(3, 2) 'año
            fecha2 = Rfc.Substring(5, 2) 'mes
            fecha3 = Rfc.Substring(7, 2) 'dia
            If fecha2 > 12 Or fecha3 > 31 Then
                oEvent.Ex = Nothing
                oEvent.Message = "Incorrecto el RFC en los Datos de la Fecha"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If


            If Rfc.Length = 12 Then
                homoclave = Rfc.Substring(9, 3)
                For Each letra In homoclave
                    If Not Char.IsLetterOrDigit(letra) Then
                        oEvent.Ex = Nothing
                        oEvent.Message = "Los Últimos 3 Caracteres del RFC Deben ser Letras o Números"
                        oEvent.Layer = Events.ErrorLayer.BussinessLayer
                        Return oEvent
                    End If
                Next
            End If

            If Rfc.Length = 12 Then
                rfc_completo = True
            Else
                rfc_completo = False
            End If

        End If


        Return oEvent
    End Function
    Public Function ValidaDomicilio(ByVal Domicilio As String) As Events
        Dim oEvent As New Events
        If Domicilio.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Calle es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCp(ByVal Cp As Long) As Events
        Dim oEvent As New Events
        If Cp <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Código Postal es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaEstado(ByVal Estado As Long) As Events
        Dim oEvent As New Events
        If Estado = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMunicipio(ByVal municipio As String) As Events
        Dim oEvent As New Events
        If municipio = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Municipio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCiudad(ByVal Ciudad As Long) As Events
        Dim oEvent As New Events
        If Ciudad = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ciudad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaColonia(ByVal Colonia As Long) As Events
        Dim oEvent As New Events
        If Colonia <= 0 Or IsDBNull(Colonia) = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Colonia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaCobradores(ByVal cobradores As Long)
        Dim oEvent As New Events
        If cobradores <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Cobrador es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaIngresos(ByVal Ingresos As Double) As Events
        Dim oEvent As New Events
        If Ingresos <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Ingreso Mensual es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal sucursal As Long) As Events
        Dim oEvent As New Events
        If sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La sucursal es requerida."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaAval(ByVal aval As Long, ByVal nombre_aval As String) As Events
        Dim oEvent As New Events
        If aval <= 0 And nombre_aval.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Aval es requerido."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent

    End Function
    Public Function ValidarClienteConvenioMegaCred(ByRef cliente As Long, ByVal convenio As Long, ByVal clave_pago As String, ByVal no_empleado As String, ByVal rfc As String, ByVal importe_abono As Double) As Events
        ValidarClienteConvenioMegaCred = oClientes.sp_valida_cliente_convenio_MegaCred(cliente, convenio, clave_pago, no_empleado, rfc, importe_abono)
    End Function

#End Region

#End Region

End Class
#End Region


