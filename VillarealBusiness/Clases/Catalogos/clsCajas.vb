Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCajas
'DATE:		04/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Cajas"
Public Class clsCajas
	Private oCajas As VillarrealData.clsCajas

#Region "Constructores"
	Sub New()
		oCajas = New VillarrealData.clsCajas
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oCajas.sp_cajas_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oCajas.sp_cajas_upd(oData)
	End Function

	Public Function Eliminar(ByVal caja as long) As Events
		Eliminar = oCajas.sp_cajas_del(caja)
	End Function

	Public Function DespliegaDatos(ByVal caja as long) As Events
		DespliegaDatos = oCajas.sp_cajas_sel(caja)
	End Function

	Public Function Listado() As Events
		Listado = oCajas.sp_cajas_grs()
	End Function

    Public Function Lookup() As Events
        Lookup = oCajas.sp_cajas_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal descripcion As String, ByVal serie_recibo As String, ByVal serie_nota_credito As String, ByVal serie_nota_cargo As String) As Events
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerieRecibo(serie_recibo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerieNCR(serie_nota_credito)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerieNCA(serie_nota_cargo)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvent As New Events
        If descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSerieRecibo(ByVal serie_recibo As String) As Events
        Dim oEvent As New Events
        If serie_recibo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie del Recibo es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSerieNCR(ByVal serie_nota_credito As String) As Events
        Dim oEvent As New Events
        If serie_nota_credito.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie de la Nota de Crédito es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSerieNCA(ByVal serie_nota_cargo As String) As Events
        Dim oEvent As New Events
        If serie_nota_cargo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie de la Nota de Cargo es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaSerieDescAnt(ByVal serie_descuentos_anticipados As String) As Events
        Dim oEvent As New Events
        If serie_descuentos_anticipados.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie de los Descuentos Anticipados es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


