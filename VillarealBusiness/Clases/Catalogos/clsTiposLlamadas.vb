Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsTiposLlamadas
'DATE:		17/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - TiposLlamadas"
Public Class clsTiposLlamadas
    Private oTiposLlamadas As VillarrealData.clsTiposLlamadas

#Region "Constructores"
    Sub New()
        oTiposLlamadas = New VillarrealData.clsTiposLlamadas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oTiposLlamadas.sp_tipos_llamadas_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oTiposLlamadas.sp_tipos_llamadas_upd(oData)
    End Function

    Public Function Eliminar(ByVal tipo_llamada As Long) As Events
        Eliminar = oTiposLlamadas.sp_tipos_llamadas_del(tipo_llamada)
    End Function

    Public Function DespliegaDatos(ByVal tipo_llamada As Long) As Events
        DespliegaDatos = oTiposLlamadas.sp_tipos_llamadas_sel(tipo_llamada)
    End Function

    Public Function Listado() As Events
        Listado = oTiposLlamadas.sp_tipos_llamadas_grs()
    End Function
    Public Function Lookup() As Events
        Lookup = oTiposLlamadas.sp_tipos_llamadas_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripcion es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


