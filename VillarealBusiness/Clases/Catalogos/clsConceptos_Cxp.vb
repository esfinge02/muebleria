Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsConceptosCxp
'DATE:		19/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ConceptosCxp"
Public Class clsConceptosCxp
    Private oConceptosCxp As VillarrealData.clsConceptosCxp

#Region "Constructores"
    Sub New()
        oConceptosCxp = New VillarrealData.clsConceptosCxp
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oConceptosCxp.sp_conceptos_cxp_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oConceptosCxp.sp_conceptos_cxp_upd(oData)
    End Function

    Public Function Eliminar(ByVal concepto As String) As Events
        Eliminar = oConceptosCxp.sp_conceptos_cxp_del(concepto)
    End Function

    Public Function DespliegaDatos(ByVal concepto As String) As Events
        DespliegaDatos = oConceptosCxp.sp_conceptos_cxp_sel(concepto)
    End Function

    Public Function Listado() As Events
        Listado = oConceptosCxp.sp_conceptos_cxp_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oConceptosCxp.sp_conceptos_cxp_grl()
    End Function

    Public Function LookupConceptoReferencia(ByVal tipo As Char) As Events
        LookupConceptoReferencia = oConceptosCxp.sp_conceptos_cxp_referencia_grl(tipo)
    End Function
    Public Function LookupFolioReferencia(ByVal tipo As Char) As Events
        LookupFolioReferencia = oConceptosCxp.sp_conceptos_cxp_referencia_grl(tipo)
    End Function

    'Public Function Validacion(ByVal Action As Actions) As Events
    'End Function

    Public Function Validacion(ByVal concepto As String, ByVal descripcion As String, ByVal tipo As String) As Events
        Validacion = ValidaConcepto(concepto)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaTipo(tipo)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaConcepto(ByVal concepto As String)
        Dim oEvent As New Events
        If concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaDescripcion(ByVal descripcion As String)
        Dim oEvent As New Events
        If descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaTipo(ByVal tipo As String)
        Dim oEvent As New Events
        If tipo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Tipo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


