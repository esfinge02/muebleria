Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCajeros
'DATE:		04/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Cajeros"
Public Class clsCajeros
	Private oCajeros As VillarrealData.clsCajeros

#Region "Constructores"
	Sub New()
		oCajeros = New VillarrealData.clsCajeros
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oCajeros.sp_cajeros_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oCajeros.sp_cajeros_upd(oData)
	End Function

	Public Function Eliminar(ByVal cajero as long) As Events
		Eliminar = oCajeros.sp_cajeros_del(cajero)
	End Function

	Public Function DespliegaDatos(ByVal cajero as long) As Events
		DespliegaDatos = oCajeros.sp_cajeros_sel(cajero)
	End Function

    Public Function Listado() As Events
        Listado = oCajeros.sp_cajeros_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oCajeros.sp_cajeros_grl()
    End Function

    Public Function Lookup_usuarios() As Events
        Lookup_usuarios = oCajeros.sp_cajeros_usuarios_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal sucursal As Long) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


