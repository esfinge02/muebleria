Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDepartamentos
'DATE:		24/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Departamentos"
Public Class clsDepartamentos
    Private oDepartamentos As VillarrealData.clsDepartamentos

#Region "Constructores"
    Sub New()
        oDepartamentos = New VillarrealData.clsDepartamentos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oDepartamentos.sp_departamentos_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oDepartamentos.sp_departamentos_upd(oData)
    End Function

    Public Function Eliminar(ByVal departamento As Long) As Events
        Eliminar = oDepartamentos.sp_departamentos_del(departamento)
    End Function

    Public Function DespliegaDatos(ByVal departamento As Long) As Events
        DespliegaDatos = oDepartamentos.sp_departamentos_sel(departamento)
    End Function


    Public Function Listado() As Events
        Listado = oDepartamentos.sp_departamentos_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oDepartamentos.sp_departamentos_grl()
    End Function

    Public Function LookupDepartamentosExistencias() As Events
        LookupDepartamentosExistencias = oDepartamentos.sp_departamentos_existencias_grl()
    End Function

    Public Function LookupFijo(ByVal departamento As Long) As Events
        LookupFijo = oDepartamentos.sp_departamentos_fijo_grl(departamento)
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


