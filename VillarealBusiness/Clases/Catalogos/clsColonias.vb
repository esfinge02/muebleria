Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsColonias
'DATE:		04/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Colonias"
Public Class clsColonias
	Private oColonias As VillarrealData.clsColonias

#Region "Constructores"
	Sub New()
		oColonias = New VillarrealData.clsColonias
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oColonias.sp_colonias_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oColonias.sp_colonias_upd(oData)
	End Function

    Public Function Eliminar(ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal colonia As Long) As Events
        Eliminar = oColonias.sp_colonias_del(estado, municipio, ciudad, colonia)
    End Function

    Public Function DespliegaDatos(ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long, ByVal colonia As Long) As Events
        DespliegaDatos = oColonias.sp_colonias_sel(estado, municipio, ciudad, colonia)
    End Function

    Public Function Listado() As Events
        Listado = oColonias.sp_colonias_grs()
    End Function

    Public Function Lookup(Optional ByVal estado As Long = -1, Optional ByVal municipio As Long = -1, Optional ByVal ciudad As Long = -1) As Events
        Lookup = oColonias.sp_colonias_grl(estado, municipio, ciudad)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal descripcion As String, ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long) As Events
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaEstado(estado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMunicipio(municipio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCiudad(ciudad)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvents As New Events
        If descripcion.Trim.Length = 0 Then
            oEvents.Ex = Nothing
            oEvents.Message = "La Descripción es Requerida"
            oEvents.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvents
        End If

        Return oEvents
    End Function
    Public Function ValidaEstado(ByVal Estado As Long) As Events
        Dim oEvent As New Events
        If Estado <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMunicipio(ByVal municipio As String) As Events
        Dim oEvent As New Events
        If municipio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Municipio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCiudad(ByVal ciudad As Long) As Events
        Dim oEvents As New Events
        If ciudad <= 0 Then
            oEvents.Ex = Nothing
            oEvents.Message = "La Ciudad es Requerida"
            oEvents.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvents
        End If

        Return oEvents
    End Function

#End Region

End Class
#End Region


