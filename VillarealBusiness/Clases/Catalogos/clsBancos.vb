Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsBancos
'DATE:		19/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Bancos"
Public Class clsBancos
    Private oBancos As VillarrealData.clsBancos

#Region "Constructores"
    Sub New()
        oBancos = New VillarrealData.clsBancos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oBancos.sp_bancos_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oBancos.sp_bancos_upd(oData)
    End Function

    Public Function Eliminar(ByVal banco As Long) As Events
        Eliminar = oBancos.sp_bancos_del(banco)
    End Function

    Public Function DespliegaDatos(ByVal banco As Long) As Events
        DespliegaDatos = oBancos.sp_bancos_sel(banco)
    End Function

    Public Function Listado() As Events
        Listado = oBancos.sp_bancos_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oBancos.sp_bancos_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal sucursal As String, ByVal direccion As String, ByVal ejecutivo_cuenta As String, ByVal gerente As String, ByVal cuenta As Integer) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDireccion(direccion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaEjecutivo(ejecutivo_cuenta)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaGerente(gerente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCuenta(cuenta)
        If Validacion.ErrorFound Then Exit Function
    End Function
    ' Me.txtSucursal, Me.txtDireccion, Me.txtEjecutivo_Cuenta, Me.txtGerente, Me.clcCuenta

    Public Function ValidaNombre(ByVal Nombre As String)
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSucursal(ByVal sucursal As String)
        Dim oEvent As New Events
        If sucursal.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaDireccion(ByVal Direccion As String)
        Dim oEvent As New Events
        If Direccion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Direci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaEjecutivo(ByVal ejecutivo_cuenta As String)
        Dim oEvent As New Events
        If ejecutivo_cuenta.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Ejecutivo de Cuenta es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaGerente(ByVal gerente As String)
        Dim oEvent As New Events
        If gerente.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Gerente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCuenta(ByVal cuenta As Integer)
        Dim oEvent As New Events
        If cuenta <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cuenta es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


