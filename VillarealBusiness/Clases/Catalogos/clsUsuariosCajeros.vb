Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsUsuariosCajeros
'DATE:		21/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - UsuariosCajeros"
Public Class clsUsuariosCajeros
    Private oUsuariosCajeros As VillarrealData.clsUsuariosCajeros

#Region "Constructores"
    Sub New()
        oUsuariosCajeros = New VillarrealData.clsUsuariosCajeros
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal usuario As String, ByVal cajero As Long) As Events
        Insertar = oUsuariosCajeros.sp_usuarios_cajeros_ins(usuario, cajero)
    End Function

    Public Function Actualizar(ByVal usuario As String, ByVal cajero As Long) As Events
        Actualizar = oUsuariosCajeros.sp_usuarios_cajeros_upd(usuario, cajero)
    End Function

    Public Function Eliminar(ByVal usuario As String, ByVal cajero As Long) As Events
        Eliminar = oUsuariosCajeros.sp_usuarios_cajeros_del(usuario, cajero)
    End Function

    Public Function DespliegaDatos(ByVal usuario As String, ByVal cajero As Long) As Events
        DespliegaDatos = oUsuariosCajeros.sp_usuarios_cajeros_sel(usuario, cajero)
    End Function

    Public Function Listado() As Events
        Listado = oUsuariosCajeros.sp_usuarios_cajeros_grs()
    End Function
    Public Function existe(ByVal usuario As String, ByVal cajero As Long) As Events
        existe = oUsuariosCajeros.sp_usuarios_cajeros_exs(usuario, cajero)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


