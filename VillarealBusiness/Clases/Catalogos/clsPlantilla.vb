Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsPlantilla
    Private oPlantilla As VillarrealData.clsPlantilla

#Region "Constructores"
    Sub New()
        oPlantilla = New VillarrealData.clsPlantilla
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"

    Public Function Insertar(ByVal dependencia As Long, ByVal clave_identificacion As String, ByVal rfc As String, ByVal plaza As Long, Optional ByVal nombre_completo As String = "", Optional ByVal apellidos As String = "", Optional ByVal nombres As String = "", Optional ByVal clave_empleado As Long = -1, Optional ByVal centro_trabajo As String = "", Optional ByVal municipio As Long = -1, Optional ByVal cliente As Long = -1) As Events
        Insertar = oPlantilla.sp_plantilla_ins(dependencia, clave_identificacion, rfc, plaza, nombre_completo, apellidos, nombres, clave_empleado, centro_trabajo, municipio, cliente)
    End Function

    Public Function LookupTrabajadores(Optional ByVal dependencia As Long = -1) As Events
        LookupTrabajadores = oPlantilla.sp_plantilla_grl_trabajadores(dependencia)
    End Function

    Public Function ActualizaCliente(ByVal cliente As Long, ByVal dependencia As Long, ByVal clave_identificacion As String) As Events
        ActualizaCliente = oPlantilla.sp_plantilla_upd_cliente(dependencia, clave_identificacion, cliente)
    End Function

    Public Function ValidacionImportarPlantilla_falta_dependencia(ByVal bandera As Boolean) As Events
        ValidacionImportarPlantilla_falta_dependencia = ValidaDependencia(bandera)
        If ValidacionImportarPlantilla_falta_dependencia.ErrorFound Then Exit Function
    End Function
    Public Function ValidacionImportarPlantilla(ByVal Convenio As Long, ByVal directorio As String, ByVal archivo As String, ByVal filas_campos As Long) As Events
        ValidacionImportarPlantilla = Validaconvenio(Convenio)
        If ValidacionImportarPlantilla.ErrorFound Then Exit Function
        ValidacionImportarPlantilla = ValidaDirectorio(directorio)
        If ValidacionImportarPlantilla.ErrorFound Then Exit Function
        ValidacionImportarPlantilla = ValidaArchivo(archivo)
        If ValidacionImportarPlantilla.ErrorFound Then Exit Function

        ValidacionImportarPlantilla = ValidaFilas(filas_campos)
        If ValidacionImportarPlantilla.ErrorFound Then Exit Function
    End Function

    Private Function ValidaDependencia(ByVal bandera As Long) As Events
        Dim oEvent As New Events
        If bandera = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Dependencia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function Validaconvenio(ByVal Convenio As Long) As Events
        Dim oEvent As New Events
        If Convenio <= 0 Or IsNumeric(Convenio) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Dependencia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaDirectorio(ByVal directorio As String) As Events
        Dim oEvent As New Events
        If directorio.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Directorio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaArchivo(ByVal archivo As String) As Events
        Dim oEvent As New Events
        If archivo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Archivo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaFilas(ByVal filas_campos As Long) As Events
        Dim oEvent As New Events
        If filas_campos <= 0 Or IsNumeric(filas_campos) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un campo como identificador del convenio es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
