Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsZonas
    Private oZonas As VillarrealData.clsZonas

#Region "Constructores"
    Sub New()
        oZonas = New VillarrealData.clsZonas
    End Sub
#End Region


    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oZonas.sp_zonas_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oZonas.sp_zonas_upd(oData)
    End Function

    Public Function Eliminar(ByVal unidad As String) As Events
        Eliminar = oZonas.sp_zonas_del(unidad)
    End Function

    Public Function DespliegaDatos(ByVal unidad As String) As Events
        DespliegaDatos = oZonas.sp_zonas_sel(unidad)
    End Function

    Public Function Listado() As Events
        Listado = oZonas.sp_zonas_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oZonas.sp_zonas_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String) As Events

        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function


    End Function

    Public Function ValidaZona(ByVal zona As Integer) As Events
        Dim oEvent As New Events
        If zona <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Clave de la Zona es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


End Class
