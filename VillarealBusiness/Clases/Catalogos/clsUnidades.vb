Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsUnidades
'DATE:		06/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Unidades"
Public Class clsUnidades
    Private oUnidades As VillarrealData.clsUnidades

#Region "Constructores"
    Sub New()
        oUnidades = New VillarrealData.clsUnidades
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oUnidades.sp_unidades_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oUnidades.sp_unidades_upd(oData)
    End Function

    Public Function Eliminar(ByVal unidad As String) As Events
        Eliminar = oUnidades.sp_unidades_del(unidad)
    End Function

    Public Function DespliegaDatos(ByVal unidad As String) As Events
        DespliegaDatos = oUnidades.sp_unidades_sel(unidad)
    End Function

    Public Function Listado() As Events
        Listado = oUnidades.sp_unidades_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oUnidades.sp_unidades_grl()
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Unidad As String, ByVal Descripcion As String) As Events
        Validacion = ValidaUnidad(Unidad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

        If Action <> Actions.Insert Then Exit Function

        Validacion = ValidaExisteUnidad(Unidad)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaUnidad(ByVal Unidad As String) As Events
        Dim oEvent As New Events
        If Unidad.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Clave de la Unidad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaExisteUnidad(ByVal unidad As String) As Events
        Dim oEvent As New Events

        oEvent = oUnidades.sp_unidades_exs(unidad.Trim)
        If Not oEvent.ErrorFound Then
            If oEvent.Value > 0 Then
                oEvent.Message = "La Unidad ya existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


