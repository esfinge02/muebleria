Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCiudades
'DATE:		15/06/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Ciudades"
Public Class clsCiudades
	Private oCiudades As VillarrealData.clsCiudades

#Region "Constructores"
	Sub New()
		oCiudades = New VillarrealData.clsCiudades
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oCiudades.sp_ciudades_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oCiudades.sp_ciudades_upd(oData)
	End Function

    Public Function Eliminar(ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long) As Events
        Eliminar = oCiudades.sp_ciudades_del(estado, municipio, ciudad)
    End Function

    Public Function DespliegaDatos(ByVal estado As Long, ByVal municipio As Long, ByVal ciudad As Long) As Events
        DespliegaDatos = oCiudades.sp_ciudades_sel(estado, municipio, ciudad)
    End Function

    Public Function Listado() As Events
        Listado = oCiudades.sp_ciudades_grs()
    End Function

    Public Function Lookup(Optional ByVal estado As Long = -1, Optional ByVal municipio As Long = -1) As Events
        Lookup = oCiudades.sp_ciudades_grl(estado, municipio)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal descripcion As String, ByVal estado As Long, ByVal municipio As Long) As Events
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaEstado(estado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMunicipio(municipio)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvent As New Events
        If descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaEstado(ByVal Estado As Long) As Events
        Dim oEvent As New Events
        If Estado <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Estado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMunicipio(ByVal municipio As String) As Events
        Dim oEvent As New Events
        If municipio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Municipio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


