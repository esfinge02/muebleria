Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsUbicaciones
'DATE:		07/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Ubicaciones"
Public Class clsUbicaciones
    Private oUbicaciones As VillarrealData.clsUbicaciones

#Region "Constructores"
    Sub New()
        oUbicaciones = New VillarrealData.clsUbicaciones
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oUbicaciones.sp_ubicaciones_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oUbicaciones.sp_ubicaciones_upd(oData)
    End Function

    Public Function Eliminar(ByVal bodega As String, ByVal ubicacion As String) As Events
        Eliminar = oUbicaciones.sp_ubicaciones_del(bodega, ubicacion)
    End Function

    Public Function DespliegaDatos(ByVal bodega As String, ByVal ubicacion As String) As Events
        DespliegaDatos = oUbicaciones.sp_ubicaciones_sel(bodega, ubicacion)
    End Function

    Public Function Listado() As Events
        Listado = oUbicaciones.sp_ubicaciones_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Bodega As String, ByVal Ubicacion As String, ByVal Descripcion As String) As Events
        Validacion = ValidaBodega(Bodega)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaUbicacion(Ubicacion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaBodega(ByVal Bodega As String) As Events
        Dim oEvent As New Events
        If Bodega.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaUbicacion(ByVal Ubicacion As String) As Events
        Dim oEvent As New Events
        If Ubicacion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ubicaci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function Lookup(Optional ByVal bodega As String = "") As Events
        Lookup = oUbicaciones.sp_ubicaciones_grl(bodega)
    End Function


#End Region

End Class
#End Region


