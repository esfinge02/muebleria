Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientesCobradores
'DATE:		04/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ClientesCobradores"
Public Class clsClientesCobradores
	Private oClientesCobradores As VillarrealData.clsClientesCobradores

#Region "Constructores"
	Sub New()
		oClientesCobradores = New VillarrealData.clsClientesCobradores
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal cliente As Long) As Events
        Insertar = oClientesCobradores.sp_clientes_cobradores_ins(oData, cliente)
    End Function

    Public Function Insertar(ByVal cliente As Long, ByVal cobrador As Long, ByVal sucursal As Long) As Events
        Insertar = oClientesCobradores.sp_clientes_cobradores_ins(cliente, cobrador, sucursal)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal cliente As Long) As Events
        Actualizar = oClientesCobradores.sp_clientes_cobradores_upd(oData, cliente)
    End Function

    Public Function Eliminar(ByVal cliente As Long, ByVal cobrador As Long) As Events
        Eliminar = oClientesCobradores.sp_clientes_cobradores_del(cliente, cobrador)
    End Function

    Public Function DespliegaDatosCobradoresVentas(ByVal cliente As Long, ByVal sucursal As Long) As Events
        DespliegaDatosCobradoresVentas = oClientesCobradores.sp_clientes_cobradores_sel(cliente, sucursal)
    End Function

    Public Function Listado(ByVal cliente As Long) As Events
        Listado = oClientesCobradores.sp_clientes_cobradores_grs(cliente)
    End Function


    Public Function Lookup(ByVal Cobrador As Long, ByVal sucursal_actual As Long) As Events
        Lookup = oClientesCobradores.sp_clientes_cobradores_lkp(Cobrador, sucursal_actual)
    End Function

    Public Function Clientes_cobradores_valida_dias_cobrador(ByVal cliente As Long) As Events
        Clientes_cobradores_valida_dias_cobrador = oClientesCobradores.sp_clientes_cobradores_valida_dias_cobrador(cliente)
    End Function

    Public Function Traercobrador(ByVal sucursal As Long, ByVal cliente As Long) As Events
        Traercobrador = oClientesCobradores.sp_clientes_cobradores_movimientos_sel(sucursal, cliente)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal cobrador As Long, ByVal odata As DataSet, ByVal sucursal As String, ByVal nombre_sucursal As String) As Events

        Select Case Action
            Case Actions.Insert, Actions.Update
                Validacion = ValidaCobrador(cobrador)
                If Validacion.ErrorFound Then Exit Function
                Validacion = ValidaSucursal(sucursal)
                If Validacion.ErrorFound Then Exit Function
                Validacion = ValidaCobradoresSucursal(odata, sucursal, nombre_sucursal)
                If Validacion.ErrorFound Then Exit Function

        End Select
    End Function

    Public Function ValidaCobrador(ByVal Cobrador As Long) As Events
        Dim oEvent As New Events
        If Cobrador <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cobrador es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSucursal(ByVal Sucursal As String) As Events
        Dim oEvent As New Events
        If CInt(Sucursal) <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaCobradoresSucursal(ByVal odata As DataSet, ByVal sucursal As String, ByVal nombre_sucursal As String) As Events
        Dim oevent As New Events
        If odata.Tables(0).Select("sucursal = " & sucursal & " and control in(0,1,2)").Length >= 1 Then
            oevent.Ex = Nothing
            oevent.Message = "Ya Tiene Asignado un Cobrador en " & nombre_sucursal
            Return oevent
        End If

        Return oevent
    End Function


#End Region

End Class
#End Region


