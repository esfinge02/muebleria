Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsProveedoresDescuentos
'DATE:		12/04/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ProveedoresDescuentos"
Public Class clsProveedoresDescuentos
    Private oProveedoresDescuentos As VillarrealData.clsProveedoresDescuentos

#Region "Constructores"
    Sub New()
        oProveedoresDescuentos = New VillarrealData.clsProveedoresDescuentos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal proveedor As Long) As Events
        Insertar = oProveedoresDescuentos.sp_proveedores_descuentos_ins(oData, proveedor)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal proveedor As Long) As Events
        Actualizar = oProveedoresDescuentos.sp_proveedores_descuentos_upd(oData, proveedor)
    End Function

    Public Function Eliminar(ByVal proveedor As Long, ByVal descuento As Long) As Events
        Eliminar = oProveedoresDescuentos.sp_proveedores_descuentos_del(proveedor, descuento)
    End Function

    Public Function DespliegaDatos(ByVal proveedor As Long) As Events
        DespliegaDatos = oProveedoresDescuentos.sp_proveedores_descuentos_sel(proveedor)
    End Function
    Public Function DespliegaDatosParaInsercionDeEntradas(ByVal proveedor As Long) As Events
        DespliegaDatosParaInsercionDeEntradas = oProveedoresDescuentos.sp_proveedores_descuentos_para_insercion_de_entradas_sel(proveedor)
    End Function

   

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal Porcentaje As Double, ByVal proveedor As Long, ByVal antes_iva As Boolean, ByVal vista_descuentos As DataView, ByVal ExisteProntoPago As Boolean) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPorcentaje(Porcentaje)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescuentosAntesIva(proveedor, antes_iva, vista_descuentos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaExisteProntoPago(ExisteProntoPago)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Private Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Descuento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaPorcentaje(ByVal Porcentaje As Double) As Events
        Dim oEvent As New Events
        If Porcentaje <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Porcentaje del Descuento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        If Porcentaje > 100 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Porcentaje del Descuento debe ser Menor o Igual al 100%"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaDescuentosAntesIva(ByVal proveedor As Long, ByVal antes_iva As Boolean, ByVal vista_descuentos As DataView) As Events
        Dim oEvent As New Events
        Dim response As New Events

        Dim oView As New DataView(vista_descuentos.Table)

        oView.RowFilter = "control in (0,1,2) and antes_iva <> " + antes_iva.ToString

        If oView.Count > 1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Todos los Descuentos del Proveedor deben de estar con la misma opcion de antes de iva"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer

            Return oEvent
        End If



        Return oEvent
    End Function

    Private Function ValidaExisteProntoPago(ByVal ExisteProntoPago As Boolean) As Events
        Dim oEvent As New Events
        Dim response As New Events

        If ExisteProntoPago = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "Ya existe un descuento marcado como pronto pago."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

#End Region

End Class
#End Region


