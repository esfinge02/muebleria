Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDependenciasDetalleExportacion
'DATE:		26/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - DependenciasDetalleExportacion"
Public Class clsDependenciasDetalleExportacion
	Private oDependenciasDetalleExportacion As VillarrealData.clsDependenciasDetalleExportacion

#Region "Constructores"
	Sub New()
		oDependenciasDetalleExportacion = New VillarrealData.clsDependenciasDetalleExportacion
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
   


    Public Function ValidacionDetalle(ByVal quincena As Long, ByVal anio As Long, ByVal documento As Long, ByVal importe As Double, ByVal importe_original As Double) As Events

        ValidacionDetalle = ValidaQuincena(quincena)
        If ValidacionDetalle.ErrorFound Then Exit Function
        ValidacionDetalle = ValidaAnio(anio)
        If ValidacionDetalle.ErrorFound Then Exit Function
        ValidacionDetalle = ValidaDocumento(documento)
        If ValidacionDetalle.ErrorFound Then Exit Function
        ValidacionDetalle = ValidaImporte(importe, importe_original)
        If ValidacionDetalle.ErrorFound Then Exit Function

    End Function
    Public Function Validacion(ByVal importe As Double, ByVal cliente As Long, ByVal filas As Long) As Events

        Validacion = ValidaCliente(cliente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaImporte(importe, importe)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFilas(filas)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function TraerMovimientosParaEnvio(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal tipo As Char) As Events
        TraerMovimientosParaEnvio = oDependenciasDetalleExportacion.sp_traer_movimientos_para_envio_dependencia(convenio, quincena, anio, tipo)
    End Function
    Public Function TraerMovimientosParaEnvioDocumentos(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal cliente As Long) As Events
        TraerMovimientosParaEnvioDocumentos = oDependenciasDetalleExportacion.sp_traer_movimientos_para_envio_dependencia_documentos(convenio, quincena, anio, cliente)
    End Function

    Public Function MovimientosCobrarCliente_grl(ByVal cliente As Long, ByVal sucursal As Long, ByVal quincena As Long, ByVal anio As Long) As Events
        MovimientosCobrarCliente_grl = oDependenciasDetalleExportacion.sp_movimientos_cobrar_cliente_grl(cliente, sucursal, quincena, anio)
    End Function


    Private Function ValidaQuincena(ByVal quincena As Long) As Events
        Dim oEvent As New Events
        If quincena <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Quincena es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaAnio(ByVal anio As Long) As Events
        Dim oEvent As New Events
        If anio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Año es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaImporte(ByVal importe As Double, ByVal importe_original As Double) As Events
        Dim oEvent As New Events
        If importe <= 0 Or IsNumeric(importe) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If importe > importe_original Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe no puede ser mayor al valor del documento"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Private Function ValidaDocumento(ByVal documento As Long) As Events
        Dim oEvent As New Events
        If documento <= 0 Or IsNumeric(documento) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Documento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Private Function ValidaCliente(ByVal cliente As Long) As Events
        Dim oEvent As New Events
        If cliente <= 0 Or IsNumeric(cliente) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Private Function ValidaFilas(ByVal filas As Long) As Events
        Dim oEvent As New Events
        If filas <= 0 Or IsNumeric(filas) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "Debe insertar al menos un registro"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

    Public Function ValidaExisteFila(ByVal clave As String, ByVal Campo As String, ByVal oDetalle As DataSet, ByVal Validacliente As Boolean) As Events
        Return ValidaExistePartida(clave, Campo, oDetalle, Validacliente)
    End Function
    Private Function ValidaExistePartida(ByVal Clave As String, ByVal Campo As String, ByRef oDetalle As DataSet, ByVal Validacliente As Boolean) As Events
        Dim oEvent As New Events
        If oDetalle.Tables(0).Rows.Count > 0 Then
            Dim dv As New DataView
            dv.Table = oDetalle.Tables(0)
            dv.RowFilter = Campo & "='" & Clave.ToString & "' and control in (0,1,2)"
            If dv.Count > 0 Then
                If Validacliente Then
                    oEvent.Message = "El Cliente ya Existe"
                Else
                    oEvent.Message = "El documento ya Existe"
                End If
                oEvent.Layer = Dipros.Utils.Events.ErrorLayer.InterfaceLayer
            End If
            dv = Nothing
        End If

        Return oEvent
    End Function
    Public Function ActualizaEnvioCancelado(ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal serie_referencia As String, ByVal folio_referencia As Long, ByVal cliente_referencia As Long, ByVal documento_referencia As Long) As Events
        ActualizaEnvioCancelado = oDependenciasDetalleExportacion.sp_actualiza_envio_cancelado(sucursal_referencia, concepto_referencia, serie_referencia, folio_referencia, cliente_referencia, documento_referencia)
    End Function
#End Region

End Class
#End Region


