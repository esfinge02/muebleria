Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsBodegueros
'DATE:		23/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Bodegueros"
Public Class clsBodegueros
    Private oBodegueros As VillarrealData.clsBodegueros

#Region "Constructores"
    Sub New()
        oBodegueros = New VillarrealData.clsBodegueros
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oBodegueros.sp_bodegueros_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oBodegueros.sp_bodegueros_upd(oData)
    End Function

    Public Function Eliminar(ByVal bodeguero As Long) As Events
        Eliminar = oBodegueros.sp_bodegueros_del(bodeguero)
    End Function

    Public Function DespliegaDatos(ByVal bodeguero As Long) As Events
        DespliegaDatos = oBodegueros.sp_bodegueros_sel(bodeguero)
    End Function

    Public Function Listado() As Events
        Listado = oBodegueros.sp_bodegueros_grs()
    End Function

    Public Function Lookup(ByVal sucursal_actual As Long) As Events
        Lookup = oBodegueros.sp_bodegueros_grl(sucursal_actual)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal Sucursal As Integer) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Bodeguero es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSucursal(ByVal Sucursal As Integer) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


