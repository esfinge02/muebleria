Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEventualidades
'DATE:		03/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Eventualidades"
Public Class clsEventualidades
	Private oEventualidades As VillarrealData.clsEventualidades

#Region "Constructores"
	Sub New()
		oEventualidades = New VillarrealData.clsEventualidades
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oEventualidades.sp_eventualidades_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oEventualidades.sp_eventualidades_upd(oData)
	End Function

	Public Function Eliminar(ByVal eventualidad as long) As Events
		Eliminar = oEventualidades.sp_eventualidades_del(eventualidad)
	End Function

	Public Function DespliegaDatos(ByVal eventualidad as long) As Events
		DespliegaDatos = oEventualidades.sp_eventualidades_sel(eventualidad)
	End Function

	Public Function Listado() As Events
		Listado = oEventualidades.sp_eventualidades_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oEventualidades.sp_eventualidades_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Grupo As Long, ByVal Descripcion As String) As Events
        Validacion = ValidaGrupo(Grupo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaGrupo(ByVal Grupo As Long) As Events
        Dim oEvent As New Events
        If Grupo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Grupo de Eventualidad es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


