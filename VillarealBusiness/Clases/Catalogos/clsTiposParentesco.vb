Imports Dipros.Utils
Imports Dipros.Utils.Common

'DESCRIPTION:	clsTiposLlamadas
'DATE:		10/01/2014 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - TiposLlamadas"
Public Class clsTiposParentesco
    Private oTiposParentesco As VillarrealData.clsTipoParentesco

#Region "Constructores"
    Sub New()
        oTiposParentesco = New VillarrealData.clsTipoParentesco
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oTiposParentesco.sp_tipo_parentesco_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oTiposParentesco.sp_tipos_parentesco_upd(oData)
    End Function

    Public Function Eliminar(ByVal parentesco As Long) As Events
        Eliminar = oTiposParentesco.sp_tipos_parentesco_del(parentesco)
    End Function

    Public Function DespliegaDatos(ByVal id_parentesco As Long) As Events
        DespliegaDatos = oTiposParentesco.sp_tipos_parentesco_sel(id_parentesco)
    End Function

    Public Function Listado() As Events
        Listado = oTiposParentesco.sp_tipos_parentesco_grs()
    End Function
    Public Function Lookup() As Events
        Lookup = oTiposParentesco.sp_tipos_parentesco_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripcion es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


