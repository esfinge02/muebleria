Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsBodegas
'DATE:		06/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Bodegas"
Public Class clsBodegas
    Private oBodegas As VillarrealData.clsBodegas

#Region "Constructores"
    Sub New()
        oBodegas = New VillarrealData.clsBodegas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oBodegas.sp_bodegas_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oBodegas.sp_bodegas_upd(oData)
    End Function

    Public Function Eliminar(ByVal bodega As String) As Events
        Eliminar = oBodegas.sp_bodegas_del(bodega)
    End Function

    Public Function DespliegaDatos(ByVal bodega As String) As Events
        DespliegaDatos = oBodegas.sp_bodegas_sel(bodega)
    End Function

    Public Function Listado() As Events
        Listado = oBodegas.sp_bodegas_grs()
    End Function

    Public Function Lookup(Optional ByVal sucursal As Long = -1, Optional ByVal multisucursal As Boolean = False, Optional ByVal sucursales As String = "") As Events
        Lookup = oBodegas.sp_bodegas_grl(sucursal, multisucursal, sucursales)

    End Function
    'FUNCION ESPECIAL PARA REP DE EXISTENCIAS 21/AGO/2007

    Public Function LookupBodegasExistencias(Optional ByVal sucursal As Long = -1) As Events
        LookupBodegasExistencias = oBodegas.sp_bodegas_existencias_grl(sucursal)
    End Function

    Public Function LookupBodegasUsuarios(ByVal usuario As String, Optional ByVal sucursal As Long = -1) As Events
        LookupBodegasUsuarios = oBodegas.sp_bodegas_usuarios_lkp(sucursal, usuario)

    End Function
    'sp_bodegas_moviles_grl
    Public Function LookupBodegasMoviles(Optional ByVal sucursal As Long = -1, Optional ByVal multisucursal As Boolean = False, Optional ByVal sucursales As String = "") As Events
        LookupBodegasMoviles = oBodegas.sp_bodegas_moviles_grl(sucursal, multisucursal, sucursales)

    End Function

    Public Function MultiLookup(ByVal sucursal As String) As Events
        MultiLookup = oBodegas.sp_bodegas_sucursales_grl(sucursal)
    End Function

    Public Function LookupSinBodegaActual(ByVal bodega_actual As String, ByVal sucursal_convenio As Boolean, ByVal articulo As Long) As Events
        LookupSinBodegaActual = oBodegas.sp_bodegas_sin_bodega_actual_grl(bodega_actual, sucursal_convenio, articulo)
    End Function


    Public Function BodegasDestino(ByVal bodega As String) As Events
        BodegasDestino = oBodegas.sp_bodegas_destinos_traspasos(bodega)
    End Function

    Public Function BodegasSucursalesSel(ByVal bodega As String) As Events
        BodegasSucursalesSel = oBodegas.sp_bodegas_sucursales_sel(bodega)
    End Function

    Public Function BodegaPrincipalExiste() As Events
        BodegaPrincipalExiste = oBodegas.sp_bodega_principal_exs()
    End Function



    Public Function Validacion(ByVal Action As Actions, ByVal Bodega As String, ByVal Sucursal As Long, ByVal Descripcion As String, ByVal Contraseña As String, ByVal bodegaprincipalExiste As Boolean) As Events
        Validacion = ValidaBodega(Bodega)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaContraseña(Contraseña)
        'If Validacion.ErrorFound Then Exit Function

        If Action <> Actions.Insert Then Exit Function

        Validacion = ValidaExisteBodega(Bodega)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaBodegaPrincipalExiste(bodegaprincipalExiste)
        'If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaBodega(ByVal Bodega As String) As Events
        Dim oEvent As New Events
        If Bodega.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Clave de la Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripcion es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaContraseña(ByVal Contrasena As String) As Events
        Dim oEvent As New Events
        If Contrasena.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Contraseña es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaExisteBodega(ByVal bodega As String) As Events
        Dim oEvent As New Events

        oEvent = oBodegas.sp_bodegas_exs(bodega.Trim)
        If Not oEvent.ErrorFound Then
            If oEvent.Value > 0 Then
                oEvent.Message = "La Bodega ya existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function

    'Private Function ValidaBodegaPrincipalExiste(ByVal bodegaprincipalExiste As String) As Events
    '    Dim oEvent As New Events

    '    oEvent = oBodegas.sp_bodegas_exs(bodega.Trim)
    '    If Not oEvent.ErrorFound Then
    '        If oEvent.Value > 0 Then
    '            oEvent.Message = "La Bodega ya existe"
    '            oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '            Return oEvent
    '        End If
    '    End If
    '    Return oEvent
    'End Function

#End Region

End Class
#End Region


