Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsTamañosEtiquetasDetalle
'DATE:		26/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - TamañosEtiquetasDetalle"
Public Class clsTamaniosEtiquetasDetalle
    Private oTamañosEtiquetasDetalle As VillarrealData.clsTamaniosEtiquetasDetalle

#Region "Constructores"
    Sub New()
        oTamañosEtiquetasDetalle = New VillarrealData.clsTamaniosEtiquetasDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal tamaño As Long) As Events
        Insertar = oTamañosEtiquetasDetalle.sp_tamanios_etiquetas_detalle_ins(oData, tamaño)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oTamañosEtiquetasDetalle.sp_tamanios_etiquetas_detalle_upd(oData)
    End Function

    Public Function Eliminar(ByVal tamaño As Long, ByVal plantilla As String) As Events
        Eliminar = oTamañosEtiquetasDetalle.sp_tamanios_etiquetas_detalle_del(tamaño, plantilla)
    End Function

    Public Function DespliegaDatos(ByVal tamaño As Long, ByVal plantilla As String) As Events
        DespliegaDatos = oTamañosEtiquetasDetalle.sp_tamanios_etiquetas_detalle_sel(tamaño, plantilla)
    End Function

    Public Function Listado(ByVal tamaño As Long) As Events
        Listado = oTamañosEtiquetasDetalle.sp_tamanios_etiquetas_detalle_grs(tamaño)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal oDataSet As DataSet) As Events
        Validacion = ValidaPlantillas(oDataSet)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Private Function ValidaPlantillas(ByVal oDataSet As DataSet) As Events
        Dim oEvents As New Events
        Dim i As Integer
        Dim plantilla As String

        For i = 0 To oDataSet.Tables(0).Rows.Count - 1
            plantilla = CStr(oDataSet.Tables(0).Rows(i).Item("plantilla"))
            If oDataSet.Tables(0).Select("plantilla = '" + plantilla + "' and control in (0,1,2)").Length > 1 Then
                oEvents.Ex = Nothing
                oEvents.Message = "La Plantilla " + plantilla + " se Repite"
                oEvents.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvents
            End If
        Next
        Return oEvents
    End Function



#End Region

End Class
#End Region


