Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPlanesCredito
'DATE:		03/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - PlanesCredito"
Public Class clsPlanesCredito
	Private oPlanesCredito As VillarrealData.clsPlanesCredito

#Region "Constructores"
	Sub New()
		oPlanesCredito = New VillarrealData.clsPlanesCredito
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal tipo_venta As Char, ByVal tipo_plazo As Char) As Events
        Insertar = oPlanesCredito.sp_planes_credito_ins(oData, tipo_venta, tipo_plazo)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal tipo_venta As Char, ByVal tipo_plazo As Char) As Events
        Actualizar = oPlanesCredito.sp_planes_credito_upd(oData, tipo_venta, tipo_plazo)
    End Function

    Public Function Eliminar(ByVal plan_credito As Long) As Events
        Eliminar = oPlanesCredito.sp_planes_credito_del(plan_credito)
    End Function

    Public Function DespliegaDatos(ByVal plan_credito As Long) As Events
        DespliegaDatos = oPlanesCredito.sp_planes_credito_sel(plan_credito)
    End Function

    Public Function Listado() As Events
        Listado = oPlanesCredito.sp_planes_credito_grs()
    End Function

    Public Function Lookup(Optional ByVal sucursal_dependencia As Boolean = False, Optional ByVal cotizacion As Double = -1, Optional ByVal vigente As Long = -1, Optional ByVal plan_credito As Long = -1) As Events
        Lookup = oPlanesCredito.sp_planes_credito_grl(sucursal_dependencia, cotizacion, vigente, plan_credito)
    End Function

    Public Function LookupDescuentosProgamados(Optional ByVal sucursal_dependencia As Boolean = False, Optional ByVal cotizacion As Double = -1, Optional ByVal vigente As Long = -1, Optional ByVal plan_credito As Long = -1) As Events
        LookupDescuentosProgamados = oPlanesCredito.sp_planes_credito_descuentos_programados_grl(sucursal_dependencia, cotizacion, vigente, plan_credito)
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String, ByVal Plazo As Double, ByVal Interes As Double) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPlazo(Plazo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaInteres(Interes)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPlazo(ByVal Plazo As Double) As Events
        Dim oEvent As New Events
        If Plazo < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Plazo No Puede Ser Negativo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaInteres(ByVal Interes As Double) As Events
        Dim oEvent As New Events
        If Interes < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Interés No Puede Ser Negativo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


