Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsOpcionesCancelacion
    Private oOpciones As VillarrealData.clsOpcionesCancelacion

    Sub New()
        oOpciones = New VillarrealData.clsOpcionesCancelacion
    End Sub


#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oOpciones.sp_opciones_cancelacion_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oOpciones.sp_opciones_cancelacion_upd(oData)
    End Function

    'Public Function Actualizar(ByVal vendedor As Long, ByVal nombre As String, ByVal sueldo As Double, ByVal sucursal As Long, ByVal meta As Double, ByVal activo As Boolean) As Events
    '    Actualizar = oOpciones.sp_vendedores_upd(vendedor, nombre, sueldo, sucursal, meta, activo)
    'End Function

    Public Function Eliminar(ByVal opcion As Long) As Events
        Eliminar = oOpciones.sp_opciones_cancelacion_del(opcion)
    End Function

    Public Function DespliegaDatos(ByVal opcion As Long) As Events
        DespliegaDatos = oOpciones.sp_opciones_cancelacion_sel(opcion)
    End Function

    Public Function Listado() As Events
        Listado = oOpciones.sp_opciones_cancelacion_grs()
    End Function

    Public Function Lookup(Optional ByVal opcion As Long = -1) As Events
        Lookup = oOpciones.sp_opciones_cancelacion_grl(opcion)
    End Function



    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function



    End Function

    Private Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripcion es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region
End Class
