Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsVendedoresEsquemas
    Private oVendedoresEsquemas As VillarrealData.clsVendedoresEsquemas

#Region "Constructores"
    Sub New()
        oVendedoresEsquemas = New VillarrealData.clsVendedoresEsquemas
    End Sub
#End Region

#Region "Propiedades"
    Public Function Lookup(Optional ByVal esquema As Long = -1) As Events

    End Function
#End Region


    Public Function Insertar(ByVal vendedor As Long, ByVal esquema As Long, ByVal sucursal As Long) As Events
        Insertar = oVendedoresEsquemas.sp_vendedores_esquemas_ins(vendedor, esquema, sucursal)
    End Function
    Public Function Eliminar(ByVal vendedor As Long, ByVal esquema As Long, ByVal sucursal As Long) As Events
        Eliminar = oVendedoresEsquemas.sp_vendedores_esquemas_del(vendedor, esquema, sucursal)
    End Function

    Public Function DespliegaDatos(ByVal vendedor As Long) As Events
        DespliegaDatos = oVendedoresEsquemas.sp_vendedores_esquemas_grs(vendedor)
    End Function

    Public Function Validacion(ByVal action As Actions, ByVal sucursal As Long, ByVal esquemas As Long, ByVal TablaEsquema As DataTable) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaEsquemas(esquemas)
        If Validacion.ErrorFound Then Exit Function

        If action = Actions.Delete Then Exit Function
        Validacion = ValidaRepiteEsquema(TablaEsquema, sucursal)
        If Validacion.ErrorFound Then Exit Function


    End Function
    Public Function ValidaSucursal(ByVal sucursal As Long) As Events
        Dim oEvents As New Events
        If sucursal <= 0 Then
            oEvents.Ex = Nothing
            oEvents.Message = "La Sucursal es requerida..."
            oEvents.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvents
        End If
        Return oEvents
    End Function

    Public Function ValidaEsquemas(ByVal esquemas As Long) As Events
        Dim oEvents As New Events
        If esquemas <= 0 Then
            oEvents.Ex = Nothing
            oEvents.Message = "El Esquema de Comisiones es requerido..."
            oEvents.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvents
        End If
        Return oEvents
    End Function

    Public Function ValidaRepiteEsquema(ByVal TablaEsquema As DataTable, ByVal sucursal As Long) As Events
        Dim oEvents As New Events
        For i As Integer = 0 To TablaEsquema.Rows.Count - 1
            If TablaEsquema.Rows(i).Item("sucursal") = sucursal Then
                oEvents.Ex = Nothing
                oEvents.Message = "Sucursal ya existe..."
                oEvents.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvents
            End If
        Next
        Return oEvents
    End Function
End Class
