Imports Dipros.Data
Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports VillarrealBusiness

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsConceptosMovimientosChequera
'DATE:		26/06/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ConceptosMovimientosChequera"
Public Class clsConceptosMovimientosChequera
    Private oConceptosMovimientosChequera As New VillarrealData.clsConceptosMovimientosChequera

#Region "Constructores"
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oConceptosMovimientosChequera.sp_conceptos_movimientos_chequera_ins(oData)
    End Function
    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oConceptosMovimientosChequera.sp_conceptos_movimientos_chequera_upd(oData)
    End Function
    Public Function Eliminar(ByVal concepto As Long) As Events
        Eliminar = oConceptosMovimientosChequera.sp_conceptos_movimientos_chequera_del(concepto)
    End Function
    Public Function DespliegaDatos(ByVal concepto As Long) As Events
        DespliegaDatos = oConceptosMovimientosChequera.sp_conceptos_movimientos_chequera_sel(concepto)
    End Function
    Public Function Listado() As Events
        Listado = oConceptosMovimientosChequera.sp_conceptos_movimientos_chequera_grs()
    End Function
    Public Function Lookup() As Events
        Lookup = oConceptosMovimientosChequera.sp_conceptos_movimientos_chequera_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String, ByRef intControl As Long) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then
            intControl = 1
            Exit Function
        End If
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El campo Descripción es requerido. Por favor, ingrese una descripción para el concepto."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


