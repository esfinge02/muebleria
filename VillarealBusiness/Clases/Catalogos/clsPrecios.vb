Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPrecios
'DATE:		01/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Precios"
Public Class clsPrecios
    Private oPrecios As VillarrealData.clsPrecios

#Region "Constructores"
    Sub New()
        oPrecios = New VillarrealData.clsPrecios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oPrecios.sp_precios_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oPrecios.sp_precios_upd(oData)
    End Function

    Public Function Eliminar(ByVal precio As Long) As Events
        Eliminar = oPrecios.sp_precios_del(precio)
    End Function

    Public Function DespliegaDatos(ByVal precio As Long) As Events
        DespliegaDatos = oPrecios.sp_precios_sel(precio)
    End Function

    Public Function Listado() As Events
        Listado = oPrecios.sp_precios_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oPrecios.sp_precios_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal precio As Long, ByVal Nombre As String) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        If Action = Actions.Delete Then
            Validacion = ValidaEliminar(precio)
            If Validacion.ErrorFound Then Exit Function
        End If

    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Precio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaEliminar(ByVal precio As Long) As Events
        Dim oEvent As New Events
        oEvent = oPrecios.sp_precios_valida_eliminar_del(precio)
        If oEvent.Value = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Precio No se puede Eliminar por que esta siendo Utilizando"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


