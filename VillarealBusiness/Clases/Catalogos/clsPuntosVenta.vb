Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPuntosVenta
'DATE:		08/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - PuntosVenta"
Public Class clsPuntosVenta
	Private oPuntosVenta As VillarrealData.clsPuntosVenta

#Region "Constructores"
	Sub New()
		oPuntosVenta = New VillarrealData.clsPuntosVenta
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oPuntosVenta.sp_puntos_venta_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oPuntosVenta.sp_puntos_venta_upd(oData)
	End Function

	Public Function Eliminar(ByVal puntoventa as long) As Events
		Eliminar = oPuntosVenta.sp_puntos_venta_del(puntoventa)
	End Function

	Public Function DespliegaDatos(ByVal puntoventa as long) As Events
		DespliegaDatos = oPuntosVenta.sp_puntos_venta_sel(puntoventa)
	End Function

    Public Function Listado() As Events
        Listado = oPuntosVenta.sp_puntos_venta_grs()
    End Function

    Public Function LookUp() As Events
        LookUp = oPuntosVenta.sp_puntos_venta_grl()
    End Function

    Public Function ValidaExisteSerie(ByVal puntoventa As Long, ByVal serie As String) As Events
        Dim response As New Events
        Dim existe As Integer
        response = oPuntosVenta.sp_puntos_venta_serie_exs(puntoventa, serie)
        existe = response.Value
        If existe > 0 Then
            response.Ex = Nothing
            response.Message = "La Serie ya existe"
            response.Layer = Events.ErrorLayer.BussinessLayer
            Return response
        End If
        Return response
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String, ByVal Serie As String, ByVal Sucursal As Integer) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerie(Serie)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaFolio(Folio)
        'If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripción es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSerie(ByVal Serie As String) As Events
        Dim oEvent As New Events
        If Serie.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal Sucursal As Integer) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Ssucursal es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    'Public Function ValidaFolio(ByVal Folio As long) As Events
    '	Dim oEvent As New Events
    '       If Folio < 0 Then
    '           oEvent.Ex = Nothing
    '           oEvent.Message = "El Folio es requerido"
    '           oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '           Return oEvent
    '       End If
    '       Return oEvent
    'End Function


#End Region

End Class
#End Region


