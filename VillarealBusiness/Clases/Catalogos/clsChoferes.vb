Imports Dipros.Utils
Imports Dipros.Utils.Common


#Region "DIPROS Systems, BusinessEnvironment - Choferes"
Public Class clsChoferes
    Private oChoferes As VillarrealData.clsChoferes

#Region "Constructores"
    Sub New()
        oChoferes = New VillarrealData.clsChoferes
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oChoferes.sp_choferes_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oChoferes.sp_choferes_upd(oData)
    End Function

    Public Function Eliminar(ByVal chofer As Long) As Events
        Eliminar = oChoferes.sp_choferes_del(chofer)
    End Function

    Public Function DespliegaDatos(ByVal chofer As Long) As Events
        DespliegaDatos = oChoferes.sp_choferes_sel(chofer)
    End Function

    Public Function Listado() As Events
        Listado = oChoferes.sp_choferes_grs()
    End Function

    Public Function Lookup(ByVal activo As Long) As Events
        Lookup = oChoferes.sp_choferes_grl(activo)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal Vehiculo As String, ByVal Sucursal As Long) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaVehiculo(Vehiculo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Chofer es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaVehiculo(ByVal Vehiculo As String) As Events
        Dim oEvent As New Events
        If Vehiculo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Vehiculo es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


