Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsConveniosIdentificaciones
'DATE:		09/11/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ConveniosIdentificaciones"
Public Class clsConveniosIdentificaciones
    Private oConveniosIdentificaciones As VillarrealData.clsConveniosIdentificaciones

#Region "Constructores"
    Sub New()
        oConveniosIdentificaciones = New VillarrealData.clsConveniosIdentificaciones
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal convenio As Long, ByVal campo As String) As Events
        Insertar = oConveniosIdentificaciones.sp_convenios_identificaciones_ins(convenio, campo)
    End Function

    'Public Function Actualizar(ByVal convenio As Long, ByVal campo As String) As Events
    '    Actualizar = oConveniosIdentificaciones.sp_convenios_identificaciones_upd(convenio, campo)
    'End Function

    Public Function Eliminar(ByVal convenio As Long) As Events
        Eliminar = oConveniosIdentificaciones.sp_convenios_identificaciones_del(convenio)
    End Function

    'Public Function DespliegaDatos(ByVal convenio As Long, ByVal campo As String) As Events
    '    DespliegaDatos = oConveniosIdentificaciones.sp_convenios_identificaciones_sel(convenio, campo)
    'End Function

    Public Function Listado(ByVal convenio As Long) As Events
        Listado = oConveniosIdentificaciones.sp_convenios_identificaciones_grs(convenio)
    End Function

    'Public Function Validacion(ByVal Action As Actions) As Events

    'End Function

    Public Function dependencias_campos_plantilla(ByVal convenio As Long) As Events
        dependencias_campos_plantilla = oConveniosIdentificaciones.sp_convenios_campos_plantilla_grs(convenio)
    End Function


#End Region

End Class
#End Region


