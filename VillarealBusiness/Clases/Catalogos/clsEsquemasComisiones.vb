Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsEsquemasComisiones
    Private oEsquemasComisiones As VillarrealData.clsEsquemasComisiones



#Region "Constructores"
    Sub New()
        oEsquemasComisiones = New VillarrealData.clsEsquemasComisiones
    End Sub
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oEsquemasComisiones.sp_esquemas_comisiones_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oEsquemasComisiones.sp_esquemas_comisiones_upd(oData)
    End Function

    Public Function Eliminar(ByVal esquema As Long) As Events
        Eliminar = oEsquemasComisiones.sp_esquemas_comisiones_del(esquema)
    End Function

    Public Function DespliegaDatos(ByVal esquema As Long) As Events
        DespliegaDatos = oEsquemasComisiones.sp_esquemas_comisiones_sel(esquema)
    End Function

    Public Function Listado() As Events
        Listado = oEsquemasComisiones.sp_esquemas_comisiones_grs()
    End Function

    Public Function Lookup(Optional ByVal esquema As Long = -1) As Events
        Lookup = oEsquemasComisiones.sp_esquemas_comisiones_grl(esquema)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String, ByVal porcentaje_comisiones_contado As Decimal, ByVal porcentaje_comisiones_creditos As Decimal, ByVal porcentaje_comisiones_fonacot As Decimal, ByVal porcentaje_comisiones_general As Decimal, ByVal restar_sueldo As Boolean, ByVal usar_tabla_rangos_credito As Boolean) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaSerieRecibo(serie_recibo)
        'If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaSerieNCR(serie_nota_credito)
        'If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaSerieNCA(serie_nota_cargo)
        'If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaNombre(ByVal nombre As String) As Events
        Dim oEvent As New Events
        If nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del esquema es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    'Public Function ValidaSerieRecibo(ByVal serie_recibo As String) As Events
    '    Dim oEvent As New Events
    '    If serie_recibo.Trim.Length = 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "La Serie del Recibo es Requerida"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    'Public Function ValidaSerieNCR(ByVal serie_nota_credito As String) As Events
    '    Dim oEvent As New Events
    '    If serie_nota_credito.Trim.Length = 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "La Serie de la Nota de Cr�dito es Requerida"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    'Public Function ValidaSerieNCA(ByVal serie_nota_cargo As String) As Events
    '    Dim oEvent As New Events
    '    If serie_nota_cargo.Trim.Length = 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "La Serie de la Nota de Cargo es Requerida"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    'Private Function ValidaSerieDescAnt(ByVal serie_descuentos_anticipados As String) As Events
    '    Dim oEvent As New Events
    '    If serie_descuentos_anticipados.Trim.Length = 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "La Serie de los Descuentos Anticipados es Requerida"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function

#End Region

End Class
