Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsChequeras
'DATE:		19/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Chequeras"
Public Class clsChequeras
    Private oChequeras As VillarrealData.clsChequeras

#Region "Constructores"
    Sub New()
        oChequeras = New VillarrealData.clsChequeras
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oChequeras.sp_chequeras_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oChequeras.sp_chequeras_upd(oData)
    End Function

    Public Function Eliminar(ByVal cuenta_bancaria As String) As Events
        Eliminar = oChequeras.sp_chequeras_del(cuenta_bancaria)
    End Function

    Public Function DespliegaDatos(ByVal cuenta_bancaria As String) As Events
        DespliegaDatos = oChequeras.sp_chequeras_sel(cuenta_bancaria)
    End Function

    Public Function ExisteCuenta(ByVal cuenta_bancaria As String) As Events
        ExisteCuenta = oChequeras.sp_chequeras_exs(cuenta_bancaria)
    End Function

    Public Function Listado() As Events
        Listado = oChequeras.sp_chequeras_grs()
    End Function
    Public Function Lookup(Optional ByVal banco As Long = -1) As Events
        Lookup = oChequeras.sp_chequeras_grl(banco)
    End Function
    '@ACH-25/06/07: Se cre� este m�todo para usar multiples bancos
    Public Function MultiLookup(ByVal banco As String) As Events
        MultiLookup = oChequeras.sp_chequeras_multi_grl(banco)
    End Function
    '/@ACH-25/06/07
    Public Function TraerPredeterminada() As Events
        TraerPredeterminada = oChequeras.sp_chequeras_predeterminada()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal banco As Integer, ByVal cuenta_bancaria As String) As Events
        If Action = Action.Insert Then
            Validacion = ValidaBanco(banco)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaCuentaBancaria(cuenta_bancaria)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaExisteCuentaBancaria(cuenta_bancaria)
            If Validacion.ErrorFound Then Exit Function
        End If

        If Action = Action.Update Or Action = Action.Delete Then
            Validacion = ValidaBanco(banco)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaCuentaBancaria(cuenta_bancaria)
            If Validacion.ErrorFound Then Exit Function
        End If

    End Function

    Public Function ValidaBanco(ByVal banco As Integer)
        Dim oEvent As New Events
        If banco <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Banco es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaCuentaBancaria(ByVal cuenta_bancaria As String)
        Dim oEvent As New Events
        If cuenta_bancaria.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cuenta Bancaria es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaExisteCuentaBancaria(ByVal cuenta_bancaria As String) As Events
        Dim oEvent As New Events

        oEvent = oChequeras.sp_chequeras_exs(cuenta_bancaria.Trim)
        If Not oEvent.ErrorFound Then
            If oEvent.Value > 0 Then
                oEvent.Message = "La Cuenta ya existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region






