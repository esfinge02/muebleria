Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEtiquetas
'DATE:		19/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Etiquetas"
Public Class clsEtiquetas
	Private oEtiquetas As VillarrealData.clsEtiquetas

#Region "Constructores"
	Sub New()
		oEtiquetas = New VillarrealData.clsEtiquetas
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oEtiquetas.sp_etiquetas_ins(oData)
	End Function

    Public Function Insertar(ByVal desde As Double, ByVal hasta As Double, ByVal plan1 As Long, ByVal plan2 As Long, ByVal plan3 As Long) As Events
        Insertar = oEtiquetas.sp_etiquetas_ins(desde, hasta, plan1, plan2, plan3)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oEtiquetas.sp_etiquetas_upd(oData)
    End Function

    Public Function Eliminar() As Events
        Eliminar = oEtiquetas.sp_etiquetas_del()
    End Function

    Public Function DespliegaDatos() As Events
        DespliegaDatos = oEtiquetas.sp_etiquetas_sel()
    End Function

    Public Function Listado(ByVal numero_filas As Long) As Events
        Listado = oEtiquetas.sp_etiquetas_grs(numero_filas)
    End Function

    Public Function Validacion(ByVal numero_filas As Long, ByVal oDataTable As DataTable) As Events
        Dim i As Integer
        If oDataTable Is Nothing Then
            Validacion = ValidaObjetoNoExiste(1)
            If Validacion.ErrorFound Then Exit Function
        Else

            For i = 0 To numero_filas - 1
                Validacion = ValidaPlanes(oDataTable.Rows(i))
                If Validacion.ErrorFound Then Exit Function
            Next

            Validacion = ValidaDesdeHasta(oDataTable)
            If Validacion.ErrorFound Then Exit Function

        End If
    End Function


    Public Function ValidaPlanes(ByVal oDataRow As DataRow) As Events
        Dim oEvents As New Events
        If oDataRow.Item("plan1") = oDataRow.Item("plan2") Or oDataRow.Item("plan1") = oDataRow.Item("plan3") Or oDataRow.Item("plan2") = oDataRow.Item("plan3") Then
            oEvents.Ex = Nothing
            oEvents.Message = "No se Pueden Repetir los Planes en la Misma Fila"
            oEvents.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvents
        End If

        Return oEvents
    End Function
    Public Function ValidaDesdeHasta(ByVal oTable As DataTable) As Events
        Dim oEvents As New Events
        Dim i, anterior As Integer

        anterior = -1
        For i = 0 To oTable.Rows.Count - 1
            If CType(oTable.Rows(i).Item("desde"), Double) > CType(oTable.Rows(i).Item("hasta"), Double) Then
                oEvents.Ex = Nothing
                oEvents.Message = "El Valor 'Desde' es Mayor al Valor 'Hasta' en la Fila " + CStr(i + 1)
                oEvents.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvents
            End If

            If anterior >= 0 Then
                If CType(oTable.Rows(i).Item("desde"), Double) < CType(oTable.Rows(anterior).Item("hasta"), Double) Then
                    oEvents.Ex = Nothing
                    oEvents.Message = "Un Valor 'Desde' No Puede Ser Menor a un Valor 'Hasta' Anterior"
                    oEvents.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvents
                End If
            End If

            anterior = anterior + 1
        Next
        Return oEvents
    End Function
    Public Function ValidaObjetoNoExiste(ByVal numero As Long) As Events
        Dim oEvents As New Events
        oEvents.Ex = Nothing
        oEvents.Message = "Al menos una Fila es Requerida"
        oEvents.Layer = Events.ErrorLayer.BussinessLayer
        Return oEvents
    End Function


#End Region

End Class
#End Region


