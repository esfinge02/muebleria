Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEmpleados
'DATE:		04/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Empleados"
Public Class clsEmpleados
	Private oEmpleados As VillarrealData.clsEmpleados

#Region "Constructores"
	Sub New()
		oEmpleados = New VillarrealData.clsEmpleados
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oEmpleados.sp_empleados_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oEmpleados.sp_empleados_upd(oData)
	End Function

	Public Function Eliminar(ByVal empleado as long) As Events
		Eliminar = oEmpleados.sp_empleados_del(empleado)
	End Function

	Public Function DespliegaDatos(ByVal empleado as long) As Events
		DespliegaDatos = oEmpleados.sp_empleados_sel(empleado)
	End Function

	Public Function Listado() As Events
		Listado = oEmpleados.sp_empleados_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oEmpleados.sp_empleados_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Nombre As String) As Events
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre del Empleado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


