Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsArticulos
'DATE:		01/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Articulos"
Public Class clsArticulos
    Private oArticulos As VillarrealData.clsArticulos

#Region "Constructores"
    Sub New()
        oArticulos = New VillarrealData.clsArticulos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oArticulos.sp_articulos_ins(oData)
    End Function
    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oArticulos.sp_articulos_upd(oData)
    End Function
    Public Function Eliminar(ByVal articulo As Long) As Events
        Eliminar = oArticulos.sp_articulos_del(articulo)
    End Function
    Public Function DespliegaDatos(ByVal articulo As Long) As Events
        DespliegaDatos = oArticulos.sp_articulos_sel(articulo)
    End Function
    Public Function EstadisticasDeArticulo(ByVal articulo As Long) As Events
        EstadisticasDeArticulo = oArticulos.sp_articulos_estadisticas(articulo)
    End Function

    Public Function MovimientosDeArticuloPorFechas(ByVal articulo As Long, ByVal fecha_ini As String, ByVal fecha_fin As String) As Events
        MovimientosDeArticuloPorFechas = oArticulos.sp_articulos_movimientos_inventario_grs(articulo, fecha_ini, fecha_fin)
    End Function

    Public Function Listado() As Events
        Listado = oArticulos.sp_articulos_grs()
    End Function
    Public Function folio() As Events
        folio = oArticulos.sp_articulos_folio()
    End Function
    Public Function Lookup(Optional ByVal departamento As Long = -1, Optional ByVal grupo As Long = -1, Optional ByVal no_resurtir As Long = -1, Optional ByVal cadena As String = "", Optional ByVal articulo As Long = -1, Optional ByVal articulo_regalo As Long = -1, Optional ByVal ligero_uso As Long = -1) As Events
        Lookup = oArticulos.sp_articulos_grl(departamento, grupo, no_resurtir, cadena, articulo, articulo_regalo, ligero_uso)
    End Function

    Public Function LookupArticulosPorFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        LookupArticulosPorFactura = oArticulos.sp_articulos_factura_grl(sucursal, serie, folio)
    End Function

    Public Function LookupArticulosLigeroUso(ByVal Departamento As Long, ByVal grupo As Long, ByVal ligero_uso As Boolean) As Events
        LookupArticulosLigeroUso = oArticulos.sp_articulos_ligero_uso_grl(Departamento, grupo, ligero_uso)
    End Function

    Public Function ActualizaPedimento(ByVal articulo As Long, ByVal pedimento As String, ByVal aduana As String, ByVal fecha_importacion As Object) As Events
        ActualizaPedimento = oArticulos.sp_actualiza_pedimento_articulos_upd(articulo, pedimento, aduana, fecha_importacion)
    End Function
    Public Function ActualizaUltimoCosto(ByVal articulo As Long, ByVal fecha_ultima_compra As String, ByVal ultimo_costo As Double, ByVal ultimo_costo_sin_flete As Double, ByVal precio_lista As Double) As Events
        ActualizaUltimoCosto = oArticulos.sp_articulos_actualiza_ultimo_costo_fecha(articulo, fecha_ultima_compra, ultimo_costo, ultimo_costo_sin_flete, precio_lista)
    End Function
    Public Function MultiLookup(ByVal departamento As String, ByVal grupo As String) As Events
        MultiLookup = oArticulos.sp_articulos_departamentos_grupos_grl(departamento, grupo)
    End Function

    Public Function LookupArticulosMovtos(ByVal departamento As Long, ByVal grupo As Long) As Events
        LookupArticulosMovtos = oArticulos.sp_articulos_departamentos_grupos_movtos_grl(departamento, grupo)
    End Function



    Public Function ActualizarCostosArticuloMovimiento(ByVal articulo As Long, ByVal costo_movimiento As Double) As Events
        ActualizarCostosArticuloMovimiento = oArticulos.sp_actualiza_costos_articulo_movimiento(articulo, costo_movimiento)
    End Function


    Public Function MultiLookup_xUbicaciones(ByVal ubicacion As String, ByVal bodega As String) As Events
        MultiLookup_xUbicaciones = oArticulos.sp_articulos_ubicaciones_grl(ubicacion, bodega)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal articulo As Long, ByVal modelo As String, ByVal strModeloActual As String, ByVal Descripcioncorta As String, ByVal Descripcion As String, ByVal Departamento As Long, ByVal DepartamentoActual As Long, ByVal Grupo As Long, ByVal GrupoActual As Long, ByVal Unidad As String, ByVal Proveedor As Long, ByVal Tama�oEtiquetaPrecios As String, ByVal ArticuloRegalo As Boolean, ByVal Marcopedidofabrica As Boolean, ByVal costo_pedido_fabrica As Double, ByVal bodega_pedido_fabrica As String) As Events


        Validacion = ValidaDepartamento(Departamento)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaGrupo(Grupo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaModelo(modelo)
        If Validacion.ErrorFound Then Exit Function
        If Action = Actions.Insert Then
            Validacion = ValidaDepartamentoGrupoModelo(Departamento, Grupo, modelo)
            If Validacion.ErrorFound Then Exit Function
        End If
        If Action = Actions.Update And Not (Departamento = DepartamentoActual And Grupo = GrupoActual And modelo.Equals(strModeloActual)) Then
            Validacion = ValidaDepartamentoGrupoModelo(Departamento, Grupo, modelo)
            If Validacion.ErrorFound Then Exit Function
        End If
        Validacion = ValidaDescripcionCorta(Descripcioncorta)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaUnidad(Unidad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaProveedor(Proveedor)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaTama�oEtiquetaPrecios(Tama�oEtiquetaPrecios, ArticuloRegalo)
        If Validacion.ErrorFound Then Exit Function



        If Action = Actions.Delete Then
            Validacion = ValidaEliminaArticulo(articulo)
            If Validacion.ErrorFound Then Exit Function
        End If


        If Action <> Actions.Delete And Marcopedidofabrica = True Then
            Validacion = ValidaCostoPedidoFabrica(costo_pedido_fabrica)
            If Validacion.ErrorFound Then Exit Function

            Validacion = ValidaBodegaPedidoFabrica(bodega_pedido_fabrica)
            If Validacion.ErrorFound Then Exit Function
        End If
        ''No mover de lugar ni poner nada debajo de esta ultima validacion
        '' ya que por proceso esta debe ser la ultima validacion
        'If Action <> Actions.Insert Then Exit Function
        'Validacion = ValidaExisteArticulo(articulo)
        'If Validacion.ErrorFound Then Exit Function



    End Function

    Public Function ValidaCostoPedidoFabrica(ByVal costo_pedido_fabrica As Double) As Events
        Dim oEvent As New Events
        If costo_pedido_fabrica <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El costo del pedido a f�brica es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaBodegaPedidoFabrica(ByVal bodega_pedido_fabrica As String) As Events
        Dim oEvent As New Events
        If bodega_pedido_fabrica.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega del pedido a f�brica es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaDescripcionCorta(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n Corta  es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDepartamento(ByVal Departamento As Long) As Events
        Dim oEvent As New Events
        If Departamento < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Departamento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaGrupo(ByVal Grupo As Long) As Events
        Dim oEvent As New Events
        If Grupo < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Grupo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaModelo(ByVal Modelo As String) As Events
        Dim oEvent As New Events

        If Modelo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Modelo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDepartamentoGrupoModelo(ByVal Departamento As Long, ByVal Grupo As Long, ByVal Modelo As String) As Events
        Dim oEvent As New Events
        Dim oDataSet As New DataSet
        oEvent = oArticulos.sp_articulos_modelo_exs(Departamento, Grupo, Modelo)
        If Not oEvent.ErrorFound Then
            oDataSet = oEvent.Value
            If oDataSet.Tables(0).Rows(0).Item(0) > 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El art�culo que est� intentado agregar ya existe."
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
    Public Function ValidaUnidad(ByVal Unidad As String) As Events
        Dim oEvent As New Events
        If Unidad.Trim.Length < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Unidad de Medida es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaProveedor(ByVal Proveedor As Long) As Events
        Dim oEvent As New Events
        If Proveedor < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaExisteArticulo(ByVal articulo As String) As Events
        Dim oEvent As New Events

        oEvent = oArticulos.sp_articulos_exs(articulo.Trim)
        If Not oEvent.ErrorFound Then
            If oEvent.Value > 0 Then
                oEvent.Message = "El Art�culo ya existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
    Public Function ValidaTama�oEtiquetaPrecios(ByVal Tama�oEtiquetaPrecios As String, ByVal ArticuloRegalo As Boolean) As Events
        Dim oEvent As New Events
        If ArticuloRegalo = False Then
            If Tama�oEtiquetaPrecios.Trim.Length = 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El Tama�o de Etiqueta es Requerido"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaEliminaArticulo(ByVal articulo As String) As Events
        Dim oEvent As New Events

        oEvent = oArticulos.sp_articulos_valida_eliminar(articulo.Trim)
        If Not oEvent.ErrorFound Then
            If oEvent.Value = 0 Then
                oEvent.Message = "El Art�culo No se Puede Eliminar ya que tiene OC o Movimiento al Inventario"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function


    Public Function CalculaImporte(ByVal ganancia As Double, ByVal ganancia_expo As Double, ByVal descuento As Double, ByVal iva As Double, ByVal Costo As Double, ByVal expo As Boolean, ByVal terminacion As Long, ByVal bRecalcularPrecioLista As Boolean, ByRef precio_lista As Double) As Events
        Dim oEvent As New Events
        'Dim precio_lista As Double
        Dim precio As Double
        Dim precio_terminacion As Double
        Dim sprecio() As String
        Dim sprecio2 As String
        Dim ultimo_digito As String

        If bRecalcularPrecioLista Then
            precio_lista = 0
            precio_lista = Costo * ganancia
        End If

        Try
            If expo = False Then

                precio = System.Math.Round(precio_lista - (precio_lista * (descuento / 100)))

                sprecio = precio.ToString.Split(".")
                sprecio2 = sprecio(0)


                ultimo_digito = sprecio2.Trim.Substring(sprecio2.Length - 1, 1)
                If CType(ultimo_digito, Long) > 5 And CType(ultimo_digito, Long) > terminacion Then
                    precio = precio + 10
                End If


                sprecio = precio.ToString.Split(".")
                sprecio2 = sprecio(0)

                sprecio2 = sprecio2.Substring(0, sprecio2.Length - 1) + terminacion.ToString
                precio_terminacion = CType(sprecio2, Double)
                If precio_terminacion > precio Then
                    oEvent.Value = precio
                Else
                    oEvent.Value = precio_terminacion
                End If


            Else

                precio = Costo * (1 + (iva / 100)) * ganancia_expo
                'precio = System.Math.Round((Costo * (1 + (iva / 100))) + ((Costo * (1 + (iva / 100))) * (ganancia_expo)))
                sprecio = precio.ToString.Split(".")
                sprecio2 = sprecio(0)

                If terminacion = 0 Then
                    ultimo_digito = sprecio2.Trim.Substring(sprecio2.Length - 1, 1)
                    If CType(ultimo_digito, Long) > 5 Then
                        precio = precio + 10
                    End If
                End If

                sprecio = precio.ToString.Split(".")
                sprecio2 = sprecio(0)

                sprecio2 = sprecio2.Substring(0, sprecio2.Length - 1) + terminacion.ToString
                precio_terminacion = CType(sprecio2, Double)
                If precio_terminacion > precio Then
                    oEvent.Value = precio
                Else
                    oEvent.Value = precio_terminacion
                End If

            End If
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar calcular el importe"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End Try
        Return oEvent
    End Function
    Public Function CalculaDescuento(ByVal ganancia As Double, ByVal ganancia_expo As Double, ByVal precio_venta As Double, ByVal iva As Double, ByVal Costo As Double, ByVal expo As Boolean) As Events
        Dim oEvent As New Events
        Dim precio_lista As Double
        precio_lista = 0
        Try
            If expo = False Then
                precio_lista = Costo * ganancia
                oEvent.Value = ((precio_lista - precio_venta) / precio_lista) * 100               ' oEvent.Value = Costo * (1 + (Utilidad / 100))
            Else
                oEvent.Value = 0
            End If
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar calcular el importe"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End Try
        Return oEvent
    End Function
    Public Function CalculaUtilidad(ByVal importe As Double, ByVal costo As Double) As Events
        Dim oEvent As New Events

        Try

            If costo = 0 Then
                oEvent.Value = 0
            Else

                oEvent.Value = ((importe / costo) - 1) * 100
            End If
        Catch ex As Exception
            oEvent.Ex = ex
            oEvent.Message = "Ha ocurrido un error al intentar calcular la Utilidad"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End Try
        Return oEvent
    End Function

    Public Function ArticuloPrecioVenta(ByVal articulo As Long, ByVal precio As Long, ByVal sucursal_dependencia As Boolean, ByVal fechapedido As DateTime, ByRef precio_venta As Double, Optional ByVal TienePlan As Boolean = False) As Events
        ArticuloPrecioVenta = oArticulos.sp_articulos_precioventa(articulo, precio, sucursal_dependencia, fechapedido, precio_venta, TienePlan)
        ArticuloPrecioVenta.Value = precio_venta
    End Function
    Public Function SeriesArticulos(ByVal articulo As Long, ByVal tipo_movimiento As Char, ByVal bodega As String, ByVal entrada As Long) As Events
        SeriesArticulos = oArticulos.sp_series_grs(articulo, tipo_movimiento, bodega, entrada)
    End Function

    Public Function ObtenerOCPorBodega(ByVal articulo As Long, ByVal bodega As String) As Events
        ObtenerOCPorBodega = oArticulos.sp_articulos_ObtenerOC_Bodega_sel(articulo, bodega)

    End Function


    Public Function ObtenerUltimaBodegaPedidoFabrica(ByVal articulo As Long) As Events
        ObtenerUltimaBodegaPedidoFabrica = oArticulos.sp_obtener_bodega_pedido_fabrica(articulo)

    End Function


    Public Function InsertarArticulosPedidoFabrica(ByVal articulo As Long, ByVal bodega_pedido_fabrica As String, ByVal costo_pedido_fabrica As Double) As Events
        InsertarArticulosPedidoFabrica = oArticulos.sp_articulos_costo_pedido_fabrica_ins(articulo, bodega_pedido_fabrica, costo_pedido_fabrica)
    End Function
#End Region

End Class
#End Region


