Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsGruposArticulos
'DATE:		24/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - GruposArticulos"
Public Class clsGruposArticulos
    Private oGruposArticulos As VillarrealData.clsGruposArticulos

#Region "Constructores"
    Sub New()
        oGruposArticulos = New VillarrealData.clsGruposArticulos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oGruposArticulos.sp_grupos_articulos_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oGruposArticulos.sp_grupos_articulos_upd(oData)
    End Function

    Public Function Eliminar(ByVal grupo As Long) As Events
        Eliminar = oGruposArticulos.sp_grupos_articulos_del(grupo)
    End Function

    Public Function DespliegaDatos(ByVal grupo As Long) As Events
        DespliegaDatos = oGruposArticulos.sp_grupos_articulos_sel(grupo)
    End Function

    Public Function Lookup(ByVal departamento As Long) As Events
        Lookup = oGruposArticulos.sp_grupos_articulos_grl(departamento)
    End Function
    Public Function LookupFijo(ByVal departamento As Long, ByVal grupo As Long) As Events
        LookupFijo = oGruposArticulos.sp_grupos_articulos_fijo_grl(departamento, grupo)
    End Function
    Public Function MultiLookup(ByVal departamento As String) As Events
        MultiLookup = oGruposArticulos.sp_grupos_articulos_departamentos_grl(departamento)
    End Function

    Public Function LookupGruposExistencias(ByVal departamento As Long) As Events
        LookupGruposExistencias = oGruposArticulos.sp_gruposarticulos_departamentosexistencias_grl(departamento)
    End Function


    Public Function Listado() As Events
        Listado = oGruposArticulos.sp_grupos_articulos_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Departamento As Long, ByVal Descripcion As String, ByVal factor_ganancia As Double, ByVal factor_ganancia_expo As Double) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDepartamento(Departamento)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFactor_Ganancia(factor_ganancia)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFactor_Ganancia_Expo(factor_ganancia_expo)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaDepartamento(ByVal Departamento As Long) As Events
        Dim oEvent As New Events
        If Departamento = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Departamento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripcion es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFactor_Ganancia(ByVal Factor_Ganancia As Double) As Events
        Dim oEvent As New Events
        If Factor_Ganancia <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Factor de Ganancia es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFactor_Ganancia_Expo(ByVal Factor_Ganancia_Expo As Double) As Events
        Dim oEvent As New Events
        If Factor_Ganancia_Expo < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Factor de Ganancia Expo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ObtenerPreciosArticulosxGrupo(ByVal grupo As Long) As Events
        ObtenerPreciosArticulosxGrupo = oGruposArticulos.sp_grupos_articulos_precios_sel(grupo)
    End Function
    Public Function ActualizaPrecioListaGrupo(ByVal grupo As Long, ByVal factor_ganancia As Double) As Events
        ActualizaPrecioListaGrupo = oGruposArticulos.sp_actualiza_precio_lista_grupos(grupo, factor_ganancia)
    End Function
    Public Function ActualizaPrecioExpoGrupo(ByVal grupo As Long, ByVal ganancia_expo As Double) As Events
        ActualizaPrecioExpoGrupo = oGruposArticulos.sp_actualiza_precio_expo_grupo(grupo, ganancia_expo)
    End Function

#End Region

End Class
#End Region


