Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEstados
'DATE:		28/08/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Estados"
Public Class clsEstados
    Private oEstados As VillarrealData.clsEstados

#Region "Constructores"
    Sub New()
        oEstados = New VillarrealData.clsEstados
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oEstados.sp_estados_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oEstados.sp_estados_upd(oData)
    End Function

    Public Function Eliminar(ByVal estado As Long) As Events
        Eliminar = oEstados.sp_estados_del(estado)
    End Function

    Public Function DespliegaDatos(ByVal estado As Long) As Events
        DespliegaDatos = oEstados.sp_estados_sel(estado)
    End Function

    Public Function Listado() As Events
        Listado = oEstados.sp_estados_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal descripcion As String) As Events
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvent As New Events
        If descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function Lookup() As Events
        Lookup = oEstados.sp_estados_grl()
    End Function

    'Public Function Lookup(Optional ByVal estado As Boolean = False) As Events
    '    Lookup = oEstados.sp_estados_grl()
    'End Function




#End Region

End Class
#End Region


