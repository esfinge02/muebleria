Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesServicio
'DATE:		31/05/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - OrdenesServicio"
Public Class clsOrdenesServicio
	Private oOrdenesServicio As VillarrealData.clsOrdenesServicio

#Region "Constructores"
	Sub New()
		oOrdenesServicio = New VillarrealData.clsOrdenesServicio
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal partida_factura As Long, ByVal articulo As Long) As Events
        Insertar = oOrdenesServicio.sp_ordenes_servicio_ins(oData, sucursal_factura, serie_factura, folio_factura, partida_factura, articulo)
    End Function
    '@ACH-22/06/07: Modifiqué el método InsertarOSReparto para agregar el CAMPO 'juridico'
    Public Function InsertarOSReparto(ByVal orden_servicio As Long, ByVal sucursal As Long, _
    ByVal tipo_servicio As Char, ByVal fecha As DateTime, ByVal articulo As Long, ByVal numero_serie As String, _
    ByVal cliente As Long, ByVal direccion_cliente As String, _
    ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, _
    ByVal partida_factura As Long, ByVal fecha_factura As DateTime, Optional ByVal juridico As Long = 0) As Events
        InsertarOSReparto = oOrdenesServicio.sp_reparto_ordenes_servicio_ins(orden_servicio, sucursal, _
     tipo_servicio, fecha, articulo, numero_serie, cliente, direccion_cliente, _
    sucursal_factura, serie_factura, folio_factura, partida_factura, fecha_factura, juridico)
    End Function
    '/@ACH-22/06/07

    Public Function Actualizar(ByRef oData As DataSet, ByVal articulo As Long) As Events
        Actualizar = oOrdenesServicio.sp_ordenes_servicio_upd(oData, articulo)
    End Function

    Public Function ActualizarImpresion(ByVal orden_servicio As Long) As Events
        ActualizarImpresion = oOrdenesServicio.sp_ordenes_servicio_reimpresion_upd(orden_servicio)
    End Function

    Public Function Eliminar(ByVal orden_servicio As Long) As Events
        Eliminar = oOrdenesServicio.sp_ordenes_servicio_del(orden_servicio)
    End Function

    Public Function DespliegaDatos(ByVal orden_servicio As Long) As Events
        DespliegaDatos = oOrdenesServicio.sp_ordenes_servicio_sel(orden_servicio)
    End Function

    Public Function Listado() As Events
        Listado = oOrdenesServicio.sp_ordenes_servicio_grs()
    End Function

    Public Function Lookup(ByVal devoluciones_proveedor As Boolean, ByVal tipo_servicio As Char, ByVal estatus As Char, Optional ByVal Bodega_Devolucion As String = "") As Events
        Lookup = oOrdenesServicio.sp_ordenes_servicio_grl(tipo_servicio, estatus, devoluciones_proveedor, Bodega_Devolucion)
    End Function

    Public Function ActualizaFoliosMovimientos(ByVal orden_servicio As Long, ByVal folio_movimiento As Long, ByVal Tipo As Boolean) As Events
        ActualizaFoliosMovimientos = Me.oOrdenesServicio.sp_actualiza_orden_servicio_folio_movimientos(orden_servicio, folio_movimiento, Tipo)
    End Function

    Public Function ActualizaUsuarioTermino(ByVal orden_servicio As Long) As Events
        ActualizaUsuarioTermino = Me.oOrdenesServicio.sp_ordenes_servicio_actualiza_usuario_termino_upd(orden_servicio)
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Fecha As String, ByVal FechaPromesa As String, ByVal Articulo As Long, ByVal Recibe As Long, ByVal Centro_Servicio As Long, ByVal Falla As String, ByVal maneja_series As Boolean, ByVal serie As String, ByVal tipo_servicio As Char) As Events
        Validacion = ValidaFecha(Fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(FechaPromesa)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulo(Articulo)
        If Validacion.ErrorFound Then Exit Function

        'SE COMENTO ESTA PARTE POR QUE NO ESTAN UTILIZANDO SERIES
        'DAM - 22-OCT-2008

        'If maneja_series = True Then
        '    Validacion = ValidaSerie(serie)
        '    If Validacion.ErrorFound Then Exit Function
        'End If

        Validacion = ValidaRecibe(Recibe, tipo_servicio)
        If Validacion.ErrorFound Then Exit Function

        If Action <> Actions.Insert Then
            Validacion = ValidaCentro_Servicio(Centro_Servicio, tipo_servicio)
            If Validacion.ErrorFound Then Exit Function
        End If

        Validacion = ValidaFalla(Falla)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Public Function ValidaGarantia(ByVal fecha As Date, ByVal fecha_factura As Date, ByVal garantia As Long, ByRef garantia_valida As Boolean) As Events
        Dim oEvent As New Events
        Dim meses As Long

        Dim dias As Int32 = DateDiff(DateInterval.Day, fecha_factura, fecha)
        If dias < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha de la Orden de Servicio No Puede Ser Menor a la Fecha de la Factura"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If fecha.Year = fecha_factura.Year Then
            meses = fecha.Month - fecha_factura.Month
        Else
            meses = (fecha.Month - fecha_factura.Month) + (12 * (fecha.Year - fecha_factura.Year))
        End If

        If fecha.Day > fecha_factura.Day Then
            meses = meses + 1
        End If

        If meses <= garantia Then
            garantia_valida = True
        Else
            garantia_valida = False
        End If

        Return oEvent
    End Function

    Public Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events

        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha esta fuera del rango válido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

    Public Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Artículo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaRecibe(ByVal Recibe As Long, ByVal tipo_servicio As Char) As Events
        Dim oEvent As New Events
        If Recibe <= 0 Then  'And tipo_servicio <> "A" Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Personal que Recibe es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCentro_Servicio(ByVal Centro_Servicio As Long, ByVal tipo_servicio As Char) As Events
        Dim oEvent As New Events
        If Centro_Servicio <= 0 And tipo_servicio <> "R" Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Centro de Servicio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFalla(ByVal Falla As String) As Events
        Dim oEvent As New Events
        If Falla.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Falla es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSerie(ByVal serie As String) As Events
        Dim oEvent As New Events
        If serie.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Número de Serie es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaBodega(ByVal bodega As String) As Events
        Dim oEvent As New Events
        If bodega.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSolucion(ByVal solucion As String) As Events
        Dim oEvent As New Events
        If solucion.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Solución es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaDetalles(ByVal numero_detalles As Long) As Events
        Dim oEvent As New Events
        If numero_detalles = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al Menos un Detalle de Seguimiento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


