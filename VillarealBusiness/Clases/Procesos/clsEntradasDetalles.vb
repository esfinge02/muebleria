Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradasDetalles
'DATE:		09/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - EntradasDetalles"
Public Class clsEntradasDetalles
    Private oEntradasDetalles As VillarrealData.clsEntradasDetalles

#Region "Constructores"
    Sub New()
        oEntradasDetalles = New VillarrealData.clsEntradasDetalles
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal entrada As Long, ByVal bodega As String) As Events
        Insertar = oEntradasDetalles.sp_entradas_detalles_ins(oData, entrada, bodega)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal entrada As Long, ByVal bodega As String, ByVal costear As Boolean) As Events
        Actualizar = oEntradasDetalles.sp_entradas_detalles_upd(oData, entrada, bodega, costear)
    End Function

    Public Function Eliminar(ByVal entrada As Long, ByVal partida As Long, ByVal cantidad As Long, ByVal articulo As String, ByVal orden As Long, ByVal bodega As String, ByVal sobre_pedido As Boolean) As Events
        Eliminar = oEntradasDetalles.sp_entradas_detalles_del(entrada, partida, cantidad, articulo, orden, bodega, sobre_pedido)
    End Function

    Public Function DespliegaDatos(ByVal entrada As Long, ByVal partida As Long) As Events
        DespliegaDatos = oEntradasDetalles.sp_entradas_detalles_sel(entrada, partida)
    End Function

    Public Function Listado(ByVal entrada As Long) As Events
        Listado = oEntradasDetalles.sp_entradas_detalles_grs(entrada)
    End Function

    Public Function DetalleOC(ByVal entrada As Long) As Events
        DetalleOC = oEntradasDetalles.sp_entradas_detalle_orden_compra_grs(entrada)
    End Function

    Public Function ChecarSeries(ByVal entrada As Long, ByVal partida As Long, ByVal articulo As String, ByVal cantidad As Long) As Events
        ChecarSeries = oEntradasDetalles.sp_entradas_detalles_checar_series(entrada, partida, articulo, cantidad)
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal Articulo As String, ByVal Costo As Double, ByVal orden As Long, ByVal cantidad_pedida As Double, Optional ByVal Cantidad As Double = 1) As Events
        Validacion = ValidaArticulo(Articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(Cantidad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCosto(Costo)
        If Validacion.ErrorFound Then Exit Function

        If orden <= 0 Then Exit Function
        Validacion = ValidaCantidadRecibida(cantidad_pedida, Cantidad)
        If Validacion.ErrorFound Then Exit Function
    End Function


    Private Function ValidaArticulo(ByVal Articulo As String) As Events
        Dim oEvent As New Events
        If Articulo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaCantidad(ByVal Cantidad As Double) As Events
        Dim oEvent As New Events
        If Cantidad < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cantidad Recibida es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaCosto(ByVal Costo As Double) As Events
        Dim oEvent As New Events
        If Costo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Costo Recibido es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaCantidadRecibida(ByVal Cantidad_pedida As Double, ByVal cantidad_recibida As Double) As Events
        Dim oEvent As New Events
        If cantidad_recibida > Cantidad_pedida Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cantidad Recibida no Puede ser Mayor a la Cantidad Pedida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


