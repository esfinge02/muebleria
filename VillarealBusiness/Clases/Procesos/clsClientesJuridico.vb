Imports Dipros.Utils
Imports Dipros.Utils.Common


#Region "DIPROS Systems, BusinessEnvironment - ClientesJuridico"
Public Class clsClientesJuridico
    Private oClientesJuridico As VillarrealData.clsClientesJuridico

#Region "Constructores"
    Sub New()
        oClientesJuridico = New VillarrealData.clsClientesJuridico
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef folio_juridico As Long, ByVal abogado As Long, ByVal cliente As Long, ByVal saldo_asignar As Double, ByVal fecha_asignacion As Date, ByVal status As String, ByVal no_recibir_abonos As Boolean, ByVal observaciones As String, ByVal cliente_reformado As Boolean) As Events
        Insertar = oClientesJuridico.sp_clientes_juridico_ins(folio_juridico, abogado, cliente, saldo_asignar, fecha_asignacion, status, no_recibir_abonos, observaciones, cliente_reformado)
    End Function

    Public Function Asignar(ByVal abogado As Long, ByVal cliente As Long, ByVal saldo_asignar As Double, ByVal fecha_asignacion As Date, ByVal status As String, ByVal no_recibir_abonos As Boolean, ByVal observaciones As String, ByVal cliente_reformado As Boolean) As Events
        Asignar = oClientesJuridico.sp_asignar_clientes_juridico_ins(abogado, cliente, saldo_asignar, fecha_asignacion, status, no_recibir_abonos, observaciones, cliente_reformado)
    End Function
    Public Function Actualizar(ByRef folio_juridico As Long, ByVal abogado As Long, ByVal cliente As Long, ByVal saldo_asignar As Double, ByVal fecha_asignacion As Date, ByVal status As String, ByVal no_recibir_abonos As Boolean, ByVal observaciones As String, ByVal cliente_reformado As Boolean) As Events
        Actualizar = oClientesJuridico.sp_clientes_juridico_upd(folio_juridico, abogado, cliente, saldo_asignar, fecha_asignacion, status, no_recibir_abonos, observaciones, cliente_reformado)
    End Function

    Public Function Eliminar(ByVal folio_juridico As Long) As Events
        Eliminar = oClientesJuridico.sp_clientes_juridico_del(folio_juridico)
    End Function

    Public Function DespliegaDatos(ByVal folio_juridico As Long) As Events
        DespliegaDatos = oClientesJuridico.sp_clientes_juridico_sel(folio_juridico)
    End Function

    Public Function Listado(ByVal abogados As String, ByVal todos As Boolean) As Events
        Listado = oClientesJuridico.sp_clientes_juridico_grs(abogados, todos)
    End Function

    Public Function Listado() As Events
        Listado = oClientesJuridico.sp_clientes_juridico_grs("", True)
    End Function
    Public Function ClientesParaAsignarAJuridico(ByVal Sucursal As String) As Events
        ClientesParaAsignarAJuridico = oClientesJuridico.sp_abonos_de_clientes_para_asignar_a_juridico(Sucursal)
    End Function
    Public Function NotasDeCargoPendientesClienteExiste(ByVal cliente As Integer) As Events
        NotasDeCargoPendientesClienteExiste = oClientesJuridico.sp_notas_cargo_pendientes_asignar_clientes_exs(cliente)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal fecha As String, ByVal abogado As Long) As Events

        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaAbogado(abogado)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal abogado As Long, ByVal cliente As Long) As Events

        Validacion = ValidaAbogado(abogado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCliente(cliente)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacionbrw(ByVal Action As Actions, ByVal abogado As Long) As Events
        If Action <> Actions.Insert Then Exit Function
        Validacionbrw = ValidaAbogado(abogado)
        If Validacionbrw.ErrorFound Then Exit Function
    End Function

    Private Function ValidaAbogado(ByVal abogado As Long) As Events
        Dim oEvent As New Events
        If abogado <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Abogado  es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
    Private Function ValidaCliente(ByVal Cliente As Long) As Events
        Dim oEvent As New Events
        If Cliente <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
    Private Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events
        If fecha = Nothing Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function



#End Region

End Class
#End Region


