Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsPaquetes
    Private oPaquetes As VillarrealData.clsPaquetes


#Region "Constructores"
    Sub New()
        oPaquetes = New VillarrealData.clsPaquetes
    End Sub
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oPaquetes.sp_paquetes_ins(oData)
    End Function


    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oPaquetes.sp_paquetes_upd(oData)
    End Function

    Public Function Eliminar(ByVal paquete As Long) As Events
        Eliminar = oPaquetes.sp_paquetes_del(paquete)
    End Function

    Public Function Listado() As Events
        Listado = oPaquetes.sp_paquetes_grs()
    End Function

    Public Function DespliegaDatos(ByVal paquete As Long) As Events
        DespliegaDatos = oPaquetes.sp_paquetes_sel(paquete)
    End Function

    Public Function Validacion(ByVal action As Actions, ByVal plan_credito As Long) As Events
        If action <> Actions.Delete Then
            Validacion = ValidaPlanCredito(plan_credito)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function

    Private Function ValidaPlanCredito(ByVal Plan_credito As Long) As Events
        Dim oEvent As New Events
        If Plan_credito < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Plan de Cr�dito es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent

    End Function

    Public Function Lookup() As Events
        Lookup = oPaquetes.sp_paquetes_grl()
    End Function

#End Region
End Class
