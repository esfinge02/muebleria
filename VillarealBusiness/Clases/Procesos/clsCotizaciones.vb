Imports Dipros.Utils
Imports Dipros.Utils.Common


'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCotizaciones
'DATE:		01/08/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Cotizaciones"
Public Class clsCotizaciones
    Private oCotizaciones As VillarrealData.clsCotizaciones

#Region "Constructores"
    Sub New()
        oCotizaciones = New VillarrealData.clsCotizaciones
    End Sub
#End Region

#Region "Propiedades"
    Public Function Lookup() As Events
        Lookup = oCotizaciones.sp_cotizaciones_grl()
    End Function
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef cotizacion As Long, ByVal sucursal As Long, ByVal fecha As Date, ByVal cliente As Long, ByVal nombre As String, ByVal observaciones As String, ByVal direccion As String, ByVal titulo As String, ByVal precio_contado As Boolean, ByVal atencion As String, ByVal autorizo As String, ByVal puesto As String) As Events
        Insertar = oCotizaciones.sp_cotizaciones_ins(cotizacion, sucursal, fecha, cliente, nombre, observaciones, direccion, titulo, precio_contado, atencion, autorizo, puesto)
    End Function

    Public Function Actualizar(ByRef cotizacion As Long, ByVal sucursal As Long, ByVal fecha As Date, ByVal cliente As Long, ByVal nombre As String, ByVal observaciones As String, ByVal direccion As String, ByVal titulo As String, ByVal precio_contado As Boolean, ByVal atencion As String, ByVal autorizo As String, ByVal puesto As String) As Events
        Actualizar = oCotizaciones.sp_cotizaciones_upd(cotizacion, sucursal, fecha, cliente, nombre, observaciones, direccion, titulo, precio_contado, atencion, autorizo, puesto)
    End Function

    Public Function Eliminar(ByVal cotizacion As Long) As Events
        Eliminar = oCotizaciones.sp_cotizaciones_del(cotizacion)
    End Function

    Public Function DespliegaDatos(ByVal cotizacion As Long) As Events
        DespliegaDatos = oCotizaciones.sp_cotizaciones_sel(cotizacion)
    End Function

    Public Function Listado() As Events
        Listado = oCotizaciones.sp_cotizaciones_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal nombre_cliente As String, ByVal planes As DataView, ByVal numero_articulos As Long, ByVal tabla_articulos As DataTable) As Events
        If Action = Actions.Delete Then Exit Function
        Validacion = ValidaCliente(nombre_cliente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPlanes(planes)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulos(numero_articulos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDepartamentosIdentificables(tabla_articulos)
    End Function

    Public Function ValidaCliente(ByVal cliente As String) As Events
        Dim oEvent As New Events
        If cliente.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaPlanes(ByVal planes As DataView) As Events
        Dim oEvent As New Events
        planes.RowFilter = "control in (0,1,2)"
        If planes.Count = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Plan de Cr�dito es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
    Private Function ValidaArticulos(ByVal articulos As Long) As Events
        Dim oEvent As New Events
        If articulos = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
    Private Function ValidaDepartamentosIdentificables(ByVal grvArticulos As DataTable) As Events
        Dim oEvent As New Events
        Dim i As Long
        Dim bNoValido As Boolean
        Dim bClasificacion_anterior As Boolean
        For i = 0 To grvArticulos.Rows.Count - 1
            If i = 0 Then
                bClasificacion_anterior = grvArticulos.Rows(i).Item("no_identificable")
            Else
                If Not grvArticulos.Rows(i).Item("no_identificable") Is System.DBNull.Value Then
                    If bClasificacion_anterior <> grvArticulos.Rows(i).Item("no_identificable") Then
                        bNoValido = True
                        Exit For
                    End If
                End If
            End If
        Next

        If bNoValido = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "No se puede Cotizar Articulos con Diferente clasifificacion de Identificable"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


