Imports Dipros.Utils
Imports Dipros.Utils.Common

#Region "DIPROS Systems, BusinessEnvironment - LlamadasClientesDetalle"
Public Class clsLlamadasClientesDetalle
    Private oLlamadasClientesDetalle As VillarrealData.clsLlamadasClientesDetalle

#Region "Constructores"
    Sub New()
        oLlamadasClientesDetalle = New VillarrealData.clsLlamadasClientesDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"

    Public Function Insertar(ByRef oData As DataSet, ByVal orden As Long, ByVal bodega As String, ByVal sobre_pedido As Boolean) As Events
        'Insertar = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_ins(oData, orden, bodega, sobre_pedido)
    End Function
    Public Function Actualizar(ByRef oData As DataSet, ByVal orden As Long, ByVal bodega As String, ByVal sobre_pedido As Boolean) As Events
        'Actualizar = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_upd(oData, orden, bodega, sobre_pedido)
    End Function
    Public Function Eliminar(ByVal orden As Long, ByVal partida As Long, ByVal cantidad As Long, ByVal articulo As Long, ByVal bodega As String, ByVal sobre_pedido As Boolean) As Events
        'Eliminar = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_del(orden, partida, cantidad, articulo, bodega, sobre_pedido)
    End Function
    Public Function DespliegaDatos(ByVal orden As Long) As Events
        'DespliegaDatos = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_grs(orden)
    End Function
    Public Function Listado() As Events
        'Listado = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_grs()
    End Function
    Public Function CambiarBodega(ByVal bodega_anterior As String, ByVal bodega_actualizada As String, ByVal articulo As Long, ByVal cantidad_anterior As Long, ByVal orden As Long, ByVal sobre_pedido As Boolean) As Events
        'CambiarBodega = oOrdenesCompraDetalle.sp_ordenes_compra_bodegas(bodega_anterior, bodega_actualizada, articulo, cantidad_anterior, orden, sobre_pedido)
    End Function

    Public Function ConsultarNumeros(ByVal cliente As Long) As Events
        ConsultarNumeros = oLlamadasClientesDetalle.sp_consulta_telefono_grl(cliente)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal cliente As Long, ByVal Fecha As String, ByVal tipo_llamada As Long, ByVal PersonaContesta As String, ByVal MensajeRecibido As String, ByVal parentesco As Long, ByVal numero_telefonico As String, ByVal mensaje_dejado As Long) As Events
        Validacion = ValidaCliente(cliente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(Fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaTipoLlamada(tipo_llamada)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPersonaContesta(PersonaContesta)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMensajeRecibido(MensajeRecibido)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaParentesco(parentesco)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaTelefono(numero_telefonico)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMensaje(mensaje_dejado)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Public Function ValidaCliente(ByVal cliente As Long) As Events
        Dim oEvent As New Events
        If cliente <= 0 Or IsDBNull(cliente) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaParentesco(ByVal parentesco As Long) As Events
        Dim oEvent As New Events
        If parentesco <= 0 Or IsDBNull(parentesco) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Parentesco es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFecha(ByVal Fecha As String) As Events
        Dim oEvent As New Events
        If Fecha.Trim.Length = 0 Or IsDate(Fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaTipoLlamada(ByVal tipo_llamada As Long) As Events
        Dim oEvent As New Events
        If tipo_llamada <= 0 Or IsDBNull(tipo_llamada) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Tipo de Llamada es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaTelefono(ByVal numero_telefonico As String) As Events
        Dim oEvent As New Events
        If numero_telefonico.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Tel�fono es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPersonaContesta(ByVal PersonaContesta As String) As Events
        Dim oEvent As New Events
        If PersonaContesta.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Persona que Contesta es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMensajeDejado(ByVal MensajeDejado As String) As Events
        Dim oEvent As New Events
        If MensajeDejado.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Mensaje Dejado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaMensajeRecibido(ByVal MensajeRecibido As String) As Events
        Dim oEvent As New Events
        If MensajeRecibido.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Mensaje Recibido es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaMensaje(ByVal mensaje_dejado As Long) As Events
        Dim oEvent As New Events
        If mensaje_dejado <= 0 Or IsDBNull(mensaje_dejado) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Mensaje Dejado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region
