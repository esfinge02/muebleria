Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsFormasPagos
'DATE:		22/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - FormasPagos"
Public Class clsFormasPagos
    Private oFormasPagos As VillarrealData.clsFormasPagos

#Region "Constructores"
    Sub New()
        oFormasPagos = New VillarrealData.clsFormasPagos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByRef BitPrimerRegistro As Boolean, ByRef mensaje As Boolean) As Events
        Insertar = oFormasPagos.sp_formas_pagos_ins(oData, BitPrimerRegistro, mensaje)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByRef BitPrimerRegistro As Boolean, ByRef mensaje As Boolean) As Events
        Actualizar = oFormasPagos.sp_formas_pagos_upd(oData, BitPrimerRegistro, mensaje)
    End Function

    Public Function Eliminar(ByVal forma_pago As Long) As Events
        Eliminar = oFormasPagos.sp_formas_pagos_del(forma_pago)
    End Function
    Public Function PrimerRegistro(ByVal forma_pago As Integer, ByVal BitPrimerRegistro As Boolean) As Boolean
        PrimerRegistro = oFormasPagos.sp_formas_pagos_primer_registro(forma_pago, BitPrimerRegistro)

    End Function
    Public Function DespliegaDatos(ByVal forma_pago As Long) As Events
        DespliegaDatos = oFormasPagos.sp_formas_pagos_sel(forma_pago)
    End Function

    Public Function MovimientosCaja() As Events
        MovimientosCaja = oFormasPagos.sp_formas_pagos_movimientos_caja()
    End Function
    Public Function Listado() As Events
        Listado = oFormasPagos.sp_formas_pagos_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oFormasPagos.sp_formas_pagos_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion As String) As Events
        Validacion = ValidaDescripcion(Descripcion)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion(ByVal Descripcion As String) As Events
        Dim oEvent As New Events
        If Descripcion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function




#End Region

End Class
#End Region


