Imports Dipros.Utils
Imports Dipros.Utils.Common

#Region "DIPROS Systems, BusinessEnvironment - Dependencias Importacion"
Public Class clsDependenciasImportacion
    'Private oDependenciasImportacion As VillarrealData.clsDependenciasImportacion

#Region "Constructores"
    Sub New()
        'oDependenciasImportacion = New VillarrealData.clsDependenciasImportacion
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"


    
    Public Function Validacion(ByVal sucursal As Long, ByVal convenio As Long, ByVal anio As Long, ByVal quincena As Long) As Events
        Validacion = ValidaSucursal(sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConvenio(convenio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaAnio(anio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaQuincena(quincena)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Private Function ValidaSucursal(ByVal sucursal As Long) As Events
        Dim oEvent As New Events
        If sucursal <= 0 Or IsNumeric(sucursal) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaConvenio(ByVal convenio As Long) As Events
        Dim oEvent As New Events
        If convenio <= 0 Or IsNumeric(convenio) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Convenio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaAnio(ByVal anio As Long) As Events
        Dim oEvent As New Events
        If anio <= 0 Or IsNumeric(anio) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El A�o es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaQuincena(ByVal quincena As Long) As Events
        Dim oEvent As New Events
        If quincena <= 0 Or IsNumeric(quincena) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Quincena es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    'Public Function Insertar(ByRef oData As DataSet) As Events
    '    Insertar = oDevolucionesProveedor.sp_devoluciones_proveedor_ins(oData)
    'End Function

    'Public Function Actualizar(ByRef oData As DataSet) As Events
    '    Actualizar = oDevolucionesProveedor.sp_devoluciones_proveedor_upd(oData)
    'End Function

    'Public Function Eliminar(ByVal bodega As String, ByVal devolucion As Long) As Events
    '    Eliminar = oDevolucionesProveedor.sp_devoluciones_proveedor_del(bodega, devolucion)
    'End Function

    'Public Function DespliegaDatos(ByVal bodega As String, ByVal devolucion As Long) As Events
    '    DespliegaDatos = oDevolucionesProveedor.sp_devoluciones_proveedor_sel(bodega, devolucion)
    'End Function

    'Public Function Listado() As Events
    '    Listado = oDevolucionesProveedor.sp_devoluciones_proveedor_grs()
    'End Function

#End Region

End Class
#End Region