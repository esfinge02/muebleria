Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVistasSalidas
'DATE:		20/12/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - VistasSalidas"
Public Class clsVistasSalidas
    Private oVistasSalidas As VillarrealData.clsVistasSalidas

#Region "Constructores"
    Sub New()
        oVistasSalidas = New VillarrealData.clsVistasSalidas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oVistasSalidas.sp_vistas_salidas_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oVistasSalidas.sp_vistas_salidas_upd(oData)
    End Function

    Public Function Eliminar(ByVal folio_vista_salida As Long) As Events
        Eliminar = oVistasSalidas.sp_vistas_salidas_del(folio_vista_salida)
    End Function

    Public Function DespliegaDatos(ByVal folio_vista_salida As Long) As Events
        DespliegaDatos = oVistasSalidas.sp_vistas_salidas_sel(folio_vista_salida)
    End Function

    Public Function Listado() As Events
        Listado = oVistasSalidas.sp_vistas_salidas_grs()
    End Function
    Public Function Lookup(ByVal vista_entregada As Boolean) As Events
        Lookup = oVistasSalidas.sp_vistas_salidas_grl(vista_entregada)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Sucursal As Long, ByVal Cliente As Long, ByVal Persona_Recibe As String, ByVal Placas As String, ByVal Bodeguero As Long, ByVal Usuario_Autorizo As String) As Events
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCliente(Cliente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPersona_Recibe(Persona_Recibe)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPlacas(Placas)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodeguero(Bodeguero)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaUsuario_Autorizo(Usuario_Autorizo)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCliente(ByVal Cliente As Long) As Events
        Dim oEvent As New Events
        If Cliente <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPersona_Recibe(ByVal Persona_Recibe As String) As Events
        Dim oEvent As New Events
        If Persona_Recibe.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Persona que Recibe es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPlacas(ByVal Placas As String) As Events
        Dim oEvent As New Events
        If Placas.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Las Placas son Requeridas"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBodeguero(ByVal Bodeguero As Long) As Events
        Dim oEvent As New Events
        If Bodeguero <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Bodeguero es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaUsuario_Autorizo(ByVal Usuario_Autorizo As String) As Events
        Dim oEvent As New Events
        If Usuario_Autorizo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Persona que Autoriza es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


