Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsTraspasos
'DATE:		15/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------

#Region "DIPROS Systems, BusinessEnvironment - Traspasos"
Public Class clsTraspasos
    Private oTraspasos As VillarrealData.clsTraspasos

    Private Sucursal As Long = 0

#Region "Constructores"
    Sub New(ByVal Sucursal_actual As Long)
        oTraspasos = New VillarrealData.clsTraspasos
        Sucursal = Sucursal_actual
    End Sub
    Sub New()
        oTraspasos = New VillarrealData.clsTraspasos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal_salida As Long, ByVal folio_salida As Long, ByVal cancelado As Boolean) As Events
        Insertar = oTraspasos.sp_traspasos_ins(oData, sucursal_salida, folio_salida, cancelado)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal sucursal_salida As Long) As Events
        Actualizar = oTraspasos.sp_traspasos_upd(oData, sucursal_salida)
    End Function

    Public Function ActualizarEntrada(ByVal traspaso As Long, ByVal concepto_entrada As String, ByVal folio_entrada As Long, ByVal observaciones_entrada As String) As Events
        ActualizarEntrada = oTraspasos.sp_traspasos_entrada_upd(traspaso, concepto_entrada, folio_entrada, observaciones_entrada)
    End Function

    Public Function ActualizarFolioSalida(ByVal traspaso As Long, ByVal folio_salida As Long) As Events
        ActualizarFolioSalida = oTraspasos.sp_traspasos_folio_salida(traspaso, folio_salida)
    End Function

    Public Function Eliminar(ByVal traspaso As Long) As Events
        Eliminar = oTraspasos.sp_traspasos_del(traspaso)
    End Function

    Public Function DespliegaDatos(ByVal traspaso As Long) As Events
        DespliegaDatos = oTraspasos.sp_traspasos_sel(traspaso)
    End Function

    Public Function Listado() As Events
        Listado = oTraspasos.sp_traspasos_grs(Sucursal)
    End Function

    Public Function ListadoTraspasosDetalles(ByVal bodega_salida As String, ByVal bodega_entrada As String) As Events
        ListadoTraspasosDetalles = oTraspasos.sp_traspasos_detalles(bodega_salida, bodega_entrada)
    End Function

    Public Function TraspasosSalidas_ActualizaExistencias_Ins(ByVal bodega_entrada As String, ByVal articulo As Long, ByVal cantidad As Double) As Events
        TraspasosSalidas_ActualizaExistencias_Ins = oTraspasos.sp_traspasos_salidas_actualizaexistencias_ins(bodega_entrada, articulo, cantidad)
    End Function

    Public Function TraspasosSalidas_ActualizaExistencias_Del(ByVal bodega_entrada As String, ByVal articulo As Long, ByVal cantidad As Double) As Events
        TraspasosSalidas_ActualizaExistencias_Del = oTraspasos.sp_traspasos_salidas_actualizaexistencias_del(bodega_entrada, articulo, cantidad)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Sucursal_Salida As Long, ByVal Bodega_Salida As String, ByVal Concepto_Salida As String, ByVal Sucursal_Entrada As Long, ByVal Bodega_Entrada As String, ByVal Chofer As Long, ByVal Status As String, ByVal tableArticulos As DataView, ByVal Bodeguero As Long, ByVal UsarSeries As Boolean) As Events
        Validacion = ValidaSucursal_Salida(Sucursal_Salida)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodega_Salida(Bodega_Salida)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConcepto_Salida(Concepto_Salida)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSucursal_Entrada(Sucursal_Entrada)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodega_Entrada(Bodega_Entrada)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaStatus(Status)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaChofer(Chofer)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodeguero(Bodeguero)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulos(tableArticulos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaNumerosSerie(tableArticulos, UsarSeries)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Public Function ValidaSucursal_Salida(ByVal Sucursal_Salida As Long) As Events
        Dim oEvent As New Events
        If Sucursal_Salida <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBodega_Salida(ByVal Bodega_Salida As String) As Events
        Dim oEvent As New Events
        If Bodega_Salida.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega de Origen es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConcepto_Salida(ByVal Concepto_Salida As String) As Events
        Dim oEvent As New Events
        If Concepto_Salida.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFolio_Salida(ByVal Folio_Salida As Long) As Events
        Dim oEvent As New Events
        If Folio_Salida <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal_Entrada(ByVal Sucursal_Entrada As Long) As Events
        Dim oEvent As New Events
        If Sucursal_Entrada <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal Destino es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBodega_Entrada(ByVal Bodega_Entrada As String) As Events
        Dim oEvent As New Events
        If Bodega_Entrada.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega Destino es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaStatus(ByVal Status As String) As Events
        Dim oEvent As New Events
        If Status.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Status es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaChofer(ByVal Chofer As Long) As Events
        Dim oEvent As New Events
        If Chofer <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Chofer es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaBodeguero(ByVal Bodeguero As Long) As Events
        Dim oEvent As New Events
        If Bodeguero <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Bodeguero es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaArticulos(ByVal tableArticulos As DataView) As Events
        Dim oEvent As New Events
        If tableArticulos Is Nothing Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Artículos es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If tableArticulos.Count <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Artículos es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

    Public Function ValidaNumerosSerie(ByVal Data_Grid As DataView, ByVal UsarSeries As Boolean) As Events
        Dim oEvents As New Events
        Dim i As Integer = 0
        Dim strCondicion As String
        'si maneja series
        'DAM -- REVISA MANEJO SERIES OCT-08
        If UsarSeries = True Then
            While i < Data_Grid.Table.Rows.Count
                If Data_Grid.Table.Rows(i).Item("control") <> "3" And Data_Grid.Table.Rows(i).Item("control") <> "4" Then
                    If CType(Data_Grid.Table.Rows(i).Item("maneja_series"), Boolean) = True Then
                        If CStr(Data_Grid.Table.Rows(i).Item("numero_serie")).Trim.Length = 0 Then
                            oEvents.Ex = Nothing
                            oEvents.Message = "El Número de Serie del Artículo " + CStr(Data_Grid.Table.Rows(i).Item("articulo")) + " es Requerido"
                            Exit While
                        End If

                        strCondicion = "articulo = " + CStr(Data_Grid.Table.Rows(i).Item("articulo")) + " and numero_serie = '" + CStr(Data_Grid.Table.Rows(i).Item("numero_serie")) + "'"
                        If Data_Grid.Table.Select(strCondicion).Length > 1 Then
                            oEvents.Ex = Nothing
                            oEvents.Message = "El Número de Serie del Artículo " + CStr(Data_Grid.Table.Rows(i).Item("articulo")) + " Está Repetido"
                            Exit While
                        End If

                    End If
                End If
                i = i + 1
            End While
        End If

        Return oEvents

    End Function

    Public Function ValidaEntradaTraspaso(ByVal SalidasTable As DataTable) As Events


        Dim Traspaso As Long
        Dim ConceptoEntrada As String
        Dim FolioEntrada As Long

        Dim response As Events
        Dim oDataSet As DataSet


        For Each orow As DataRow In SalidasTable.Rows
            Traspaso = orow("traspaso")
          
            response = oTraspasos.sp_traspasos_sel(Traspaso)
            If Not response.ErrorFound Then
                oDataSet = response.Value

                ConceptoEntrada = oDataSet.Tables(0).Rows(0).Item("concepto_entrada")
                FolioEntrada = oDataSet.Tables(0).Rows(0).Item("folio_entrada")

                If ConceptoEntrada.Trim.Length > 0 And FolioEntrada > 0 Then
                    response.Message = "El Traspaso # " + Traspaso.ToString + " ya tiene una entrada con el folio # " + FolioEntrada.ToString
                End If
            End If
        Next



        Return response
    End Function


#End Region

End Class
#End Region


