Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientesJuridicoCorrespondencia
'DATE:		18/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ClientesJuridicoCorrespondencia"
Public Class clsClientesJuridicoCorrespondencia
    Private oClientesJuridicoCorrespondencia As VillarrealData.clsClientesJuridicoCorrespondencia

#Region "Constructores"
    Sub New()
        oClientesJuridicoCorrespondencia = New VillarrealData.clsClientesJuridicoCorrespondencia
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Insertar = oClientesJuridicoCorrespondencia.sp_clientes_juridico_correspondencia_ins(oData, folio_juridico)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Actualizar = oClientesJuridicoCorrespondencia.sp_clientes_juridico_correspondencia_upd(oData, folio_juridico)
    End Function

    Public Function Eliminar(ByVal folio_juridico As Long, ByVal folio_juridico_correspondencia As Long) As Events
        Eliminar = oClientesJuridicoCorrespondencia.sp_clientes_juridico_correspondencia_del(folio_juridico, folio_juridico_correspondencia)
    End Function

    Public Function DespliegaDatos(ByVal folio_juridico As Long, ByVal folio_juridico_correspondencia As Long) As Events
        DespliegaDatos = oClientesJuridicoCorrespondencia.sp_clientes_juridico_correspondencia_sel(folio_juridico, folio_juridico_correspondencia)
    End Function

    Public Function Listado(ByVal folio_juridico As Long) As Events
        Listado = oClientesJuridicoCorrespondencia.sp_clientes_juridico_correspondencia_grs(folio_juridico)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


