Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPolizas
'DATE:		20/10/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Polizas"
Public Class clsPolizas
    Private oPolizas As VillarrealData.clsPolizas

#Region "Constructores"
    Sub New()
        oPolizas = New VillarrealData.clsPolizas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oPolizas.sp_polizas_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oPolizas.sp_polizas_upd(oData)
    End Function

    Public Function Eliminar(ByVal poliza As Long) As Events
        Eliminar = oPolizas.sp_polizas_del(poliza)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal poliza As Long) As Events
        DespliegaDatos = oPolizas.sp_polizas_sel(sucursal, poliza)
    End Function

    Public Function Listado() As Events
        Listado = oPolizas.sp_polizas_grs()
    End Function

    Public Function Scripts() As Events
        Scripts = oPolizas.sp_polizas_scripts()
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Sucursal As Long, ByVal Nombre As String, ByVal Script As String, ByVal NumeroFilasValidas As Long) As Events
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaNombre(Nombre)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaScript(Script)
        If Validacion.ErrorFound Then Exit Function

        If Action <> Actions.Delete Then
            Validacion = ValidaNumeroFilasValidas(NumeroFilasValidas)
        End If

    End Function


    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaNombre(ByVal Nombre As String) As Events
        Dim oEvent As New Events
        If Nombre.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre de la Poliza es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaScript(ByVal Script As String) As Events
        Dim oEvent As New Events
        If Script.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Script para el llenado de la poliza es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNumeroFilasValidas(ByVal NumeroFilasValidas As Long) As Events
        Dim oEvent As New Events
        If NumeroFilasValidas <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos una cuenta es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


