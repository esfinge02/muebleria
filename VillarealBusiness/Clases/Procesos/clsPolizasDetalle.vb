Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPolizasDetalle
'DATE:		20/10/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - PolizasDetalle"
Public Class clsPolizasDetalle
    Private oPolizasDetalle As VillarrealData.clsPolizasDetalle

#Region "Constructores"
    Sub New()
        oPolizasDetalle = New VillarrealData.clsPolizasDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal sucursal As Long, ByVal poliza As Long, ByVal partida As Long, ByVal cuentacontable As String, ByVal tipo As String, ByVal nombre_campo As String) As Events
        Insertar = oPolizasDetalle.sp_polizas_detalle_ins(sucursal, poliza, partida, cuentacontable, tipo, nombre_campo)
    End Function

    Public Function Actualizar(ByVal sucursal As Long, ByVal poliza As Long, ByVal partida As Long, ByVal cuentacontable As String, ByVal tipo As String, ByVal nombre_campo As String) As Events
        Actualizar = oPolizasDetalle.sp_polizas_detalle_upd(sucursal, poliza, partida, cuentacontable, tipo, nombre_campo)
    End Function

    Public Function Eliminar(ByVal sucursal As Long, ByVal poliza As Long, ByVal partida As Long) As Events
        Eliminar = oPolizasDetalle.sp_polizas_detalle_del(sucursal, poliza, partida)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal poliza As Long, ByVal partida As Long) As Events
        DespliegaDatos = oPolizasDetalle.sp_polizas_detalle_sel(sucursal, poliza, partida)
    End Function

    Public Function Listado(ByVal sucursal As Long, ByVal poliza As Long) As Events
        Listado = oPolizasDetalle.sp_polizas_detalle_grs(sucursal, poliza)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


