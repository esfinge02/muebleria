Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsHisArticulosPrecios
'DATE:		09/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - HisArticulosPrecios"
Public Class clsHisArticulosPrecios
    Private oHisArticulosPrecios As VillarrealData.clsHisArticulosPrecios

#Region "Constructores"
    Sub New()
        oHisArticulosPrecios = New VillarrealData.clsHisArticulosPrecios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal folio As Long, ByVal articulo As Long, ByVal precio As Long, ByVal importe_original As Double) As Events
        Insertar = oHisArticulosPrecios.sp_his_articulos_precios_ins(folio, articulo, precio, importe_original)
    End Function

    'Public Function Actualizar(ByRef folio As Long, ByVal articulo As String, ByVal fecha_actualizado As Date, ByVal precio As Long, ByVal importe_original As Double, ByVal importe_actualizado As Double) As Events
    '    Actualizar = oHisArticulosPrecios.sp_his_articulos_precios_upd(folio, articulo, fecha_actualizado, precio, importe_original, importe_actualizado)
    'End Function

    Public Function Eliminar(ByVal folio As Long, ByVal articulo As Long, ByVal fecha_actualizado As Date) As Events
        Eliminar = oHisArticulosPrecios.sp_his_articulos_precios_del(folio, articulo, fecha_actualizado)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long, ByVal articulo As Long, ByVal fecha_actualizado As Date) As Events
        DespliegaDatos = oHisArticulosPrecios.sp_his_articulos_precios_sel(folio, articulo, fecha_actualizado)
    End Function

    'Public Function Existe(ByVal articulo As String, ByVal precio As Long) As Events
    '    Existe = oHisArticulosPrecios.sp_his_articulos_precios_del(articulo, precio)
    'End Function

    Public Function Listado() As Events
        Listado = oHisArticulosPrecios.sp_his_articulos_precios_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


