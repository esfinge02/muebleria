Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAcreedoresDiversos
'DATE:		20/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - AcreedoresDiversos"
Public Class clsAcreedoresDiversos
    Private oAcreedoresDiversos As VillarrealData.clsAcreedoresDiversos

#Region "Constructores"
    Sub New()
        oAcreedoresDiversos = New VillarrealData.clsAcreedoresDiversos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal actualizacheque As Boolean, ByVal subtotal As Double, ByVal iva As Double, ByVal rfc As String, ByVal Folio_Poliza_Cheque As Long) As Events
        Insertar = oAcreedoresDiversos.sp_acreedores_diversos_ins(oData, actualizacheque, subtotal, iva, rfc, Folio_Poliza_Cheque)
    End Function


    Public Function Insertar(ByRef folio_pago As Long, ByVal fecha_programacion As DateTime, ByVal numero_documento As Long, ByVal fecha_documento As DateTime, _
    ByVal importe As Double, ByVal acreedor As Long, ByVal beneficiario As String, ByVal tipo_pago As Char, ByVal referencia_transferencia As Long, _
    ByVal banco As Long, ByVal fecha_pago As DateTime, ByVal cuenta_bancaria As String, ByVal folio_cheque As Long, ByVal fecha_deposito As DateTime, _
    ByVal abono_cuenta As Boolean, ByVal concepto As String, ByVal actualizacheque As Boolean, ByVal subtotal As Double, ByVal iva As Double, ByVal rfc As String, ByVal Folio_Poliza_Cheque As Long, Optional ByVal ConceptoMovimiento As Long = 0) As Events

        Insertar = oAcreedoresDiversos.sp_acreedores_diversos_ins(folio_pago, fecha_programacion, numero_documento, fecha_documento, _
        importe, acreedor, beneficiario, tipo_pago, referencia_transferencia, banco, fecha_pago, cuenta_bancaria, ConceptoMovimiento, folio_cheque, fecha_deposito, _
         abono_cuenta, concepto, actualizacheque, subtotal, iva, rfc, Folio_Poliza_Cheque)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal actualizacheque As Boolean, ByVal importe_anterior As Double, ByVal subtotal As Double, ByVal iva As Double, ByVal rfc As String, ByVal Folio_Poliza_Cheque As Long) As Events
        Actualizar = oAcreedoresDiversos.sp_acreedores_diversos_upd(oData, actualizacheque, importe_anterior, subtotal, iva, rfc, Folio_Poliza_Cheque)
    End Function

    Public Function Eliminar(ByVal folio_pago As Long) As Events
        Eliminar = oAcreedoresDiversos.sp_acreedores_diversos_del(folio_pago)
    End Function

    Public Function DespliegaDatos(ByVal folio_pago As Long) As Events
        DespliegaDatos = oAcreedoresDiversos.sp_acreedores_diversos_sel(folio_pago)
    End Function

    Public Function Listado() As Events
        Listado = oAcreedoresDiversos.sp_acreedores_diversos_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Numero_Documento As Double, ByVal Importe As Double, ByVal Fecha_Documento As String, ByVal TipoPago As String, ByVal CuentaBancaria As String, ByVal Referencia_Transferencia As Double, ByVal beneficiario As String, ByVal rfc As String, ByVal subtotal As Double, ByVal iva As Double, ByVal ConceptoMovimiento As Long) As Events  'ByVal Folio_Cheque As Double,
        Validacion = ValidaNumeroDocumento(Numero_Documento)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechaPago(Fecha_Documento)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBeneficiario(beneficiario)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaRfc(rfc)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaImporte(Importe)
        If Validacion.ErrorFound Then Exit Function

        'TIPO DE PAGO CHEQUERA
        If TipoPago.Trim = "C" Then
            Validacion = ValidaCuentaBancaria(CuentaBancaria)
            If Validacion.ErrorFound Then Exit Function
            If Action = Actions.Insert Or Action = Actions.Update Then
                Validacion = ValidaConceptoMovimiento(ConceptoMovimiento)
                If Validacion.ErrorFound Then Exit Function
            End If
        End If
        'TIPO DE PAGO TRANSFERENCIA
        If TipoPago.Trim = "T" Then
            Validacion = ValidaCuentaBancaria(CuentaBancaria)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaReferenciaTransferencia(Referencia_Transferencia)
            If Validacion.ErrorFound Then Exit Function
        End If
        'TIPO DE PAGO TRANSFERENCIA
        If TipoPago.Trim = "" Then
            Exit Function
        End If

    End Function

    Public Function ValidaNumeroDocumento(ByVal Numero_Documento As Double)
        Dim oEvent As New Events
        If Numero_Documento <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El N�mero de Documento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaImporte(ByVal Importe As Double)
        Dim oEvent As New Events
        If Importe <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    'Public Function ValidaSubtotal(ByVal subtotal As Double)
    '    Dim oEvent As New Events
    '    If subtotal <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "El Subtotal es Requerido"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function

    Public Function ValidaIva(ByVal iva As Double)
        Dim oEvent As New Events
        If iva <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El IVA es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaFechaPago(ByVal Fecha_Documento As String)
        Dim oEvent As New Events
        If Fecha_Documento.Length = 0 Or IsDate(Fecha_Documento) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha del Documento es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaCuentaBancaria(ByVal CuentaBancaria As String)
        Dim oEvent As New Events
        If CuentaBancaria.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cuenta Bancaria es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConceptoMovimiento(ByVal ConceptoMovimiento As Long)
        Dim oEvent As New Events
        If ConceptoMovimiento <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto del Movimiento de Chequera es requerido. Por favor, seleccione un concepto."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    'Public Function ValidaFolio_Cheque(ByVal Folio_Cheque As Double)
    '    Dim oEvent As New Events
    '    If Folio_Cheque <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "El Folio del Cheque es Requerido"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    Public Function ValidaReferenciaTransferencia(ByVal Referencia_Transferencia As Double)
        Dim oEvent As New Events
        If Referencia_Transferencia <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Referencia de Transferencia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    'Public Function ValidaFolioPago(ByVal folio_pago As Integer)
    '    Dim oEvent As New Events
    '    If folio_pago <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "El Folio de Pago es Requerido"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function

    Public Function ValidaBeneficiario(ByVal beneficiario As String)
        Dim oEvent As New Events
        If beneficiario.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El beneficiario es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function



    Public Function ValidaRfc(ByVal rfc As String) As Events
        Dim oEvent As New Events


        If rfc.Length <> 13 And rfc.Length <> 10 And rfc.Length <> 0 And rfc.Length <> 12 And rfc.Length <> 9 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Longitud del RFC es Incorrecta"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

#End Region

End Class
#End Region


