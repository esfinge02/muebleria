Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsSolicitudTraspasos
'DATE:		23/06/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - SolicitudTraspasos"
Public Class clsSolicitudTraspasos
	Private oSolicitudTraspasos As VillarrealData.clsSolicitudTraspasos

#Region "Constructores"
	Sub New()
		oSolicitudTraspasos = New VillarrealData.clsSolicitudTraspasos
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal bodega_entrada As String, ByVal bodega_salida As String) As Events
        Insertar = oSolicitudTraspasos.sp_solicitud_traspasos_ins(oData, bodega_entrada, bodega_salida)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal bodega_entrada As String, ByVal bodega_salida As String) As Events
        Actualizar = oSolicitudTraspasos.sp_solicitud_traspasos_upd(oData, bodega_entrada, bodega_salida)
    End Function

    Public Function Eliminar(ByVal solicitud As Long) As Events
        Eliminar = oSolicitudTraspasos.sp_solicitud_traspasos_del(solicitud)
    End Function

    Public Function DespliegaDatos(ByVal solicitud As Long) As Events
        DespliegaDatos = oSolicitudTraspasos.sp_solicitud_traspasos_sel(solicitud)
    End Function

    Public Function Listado() As Events
        Listado = oSolicitudTraspasos.sp_solicitud_traspasos_grs()
    End Function

    Public Function Lookup(ByVal usuario As String) As Events
        Lookup = oSolicitudTraspasos.sp_solicitud_traspasos_grl(usuario)
    End Function

    Public Function ArticulosDeSolicitud(ByVal solicitud As Long) As Events
        ArticulosDeSolicitud = oSolicitudTraspasos.sp_solicitud_traspasos_articulos(solicitud)
    End Function

    Public Function ActualizarEstatus(ByVal solicitud As Long, ByVal estatus As String, ByVal traspaso As Long) As Events
        ActualizarEstatus = oSolicitudTraspasos.sp_solicitud_traspasos_cambiar_estatus(solicitud, estatus, traspaso)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal fecha As String, ByVal bodega_salida As String, ByVal bodega_entrada As String, ByVal solicita As Long, ByVal tableArticulos As DataView, ByVal Autorizo As Long) As Events
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodegaSalida(bodega_salida)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodegaEntrada(bodega_entrada)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSolicita(solicita)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaAutoriza(Autorizo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulos(tableArticulos)
        If Validacion.ErrorFound Then Exit Function
        
    End Function

    Public Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events

        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha esta fuera del rango válido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Public Function ValidaBodegaSalida(ByVal bodega_salida As String) As Events
        Dim oEvent As New Events
        If bodega_salida.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega de Origen es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBodegaEntrada(ByVal bodega_entrada As String) As Events
        Dim oEvent As New Events
        If bodega_entrada.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega Destino es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSolicita(ByVal solicita As Long) As Events
        Dim oEvent As New Events
        If solicita <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Personal que Solicita es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaAutoriza(ByVal autoriza As Long) As Events
        Dim oEvent As New Events
        If autoriza <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Personal que Autoriza es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaArticulos(ByVal tableArticulos As DataView) As Events
        Dim oEvent As New Events
        If tableArticulos Is Nothing Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Artículos es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If tableArticulos.Count <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Artículos es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

#End Region

End Class
#End Region


