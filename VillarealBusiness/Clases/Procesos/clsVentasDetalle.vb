Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVentasDetalle
'DATE:		10/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - VentasDetalle"
Public Class clsVentasDetalle
    Private oVentasDetalle As VillarrealData.clsVentasDetalle
    Private oAutorizaciones As VillarrealBusiness.clsAutorizacionesVentasPedidoFabrica
    Private oArticulos As VillarrealBusiness.clsArticulos

#Region "Constructores"
    Sub New()
        oVentasDetalle = New VillarrealData.clsVentasDetalle
        oAutorizaciones = New VillarrealBusiness.clsAutorizacionesVentasPedidoFabrica
        oArticulos = New VillarrealBusiness.clsArticulos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    '*/ csequera enero 2008
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long, ByVal folio_movimiento As Long, ByVal tipo_precio_venta As Long, ByVal folio_descuento_especial_programado As Long, ByVal precio_descuento_especial_programado As Double, ByVal precio_pactado As Double, Optional ByVal descripcion_anterior As String = "") As Events
        Insertar = oVentasDetalle.sp_ventas_detalle_ins(oData, sucursal, serie, folio, partida, folio_movimiento, tipo_precio_venta, folio_descuento_especial_programado, precio_descuento_especial_programado, precio_pactado, descripcion_anterior)
    End Function

    Public Function InsertarAnticipo(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long, ByVal articulo As Long, ByVal preciounitario As Double, _
    ByVal precio_contado As Double, ByVal sobrepedido As Boolean, ByVal costo As Double, ByVal surtido As Boolean, ByVal reparto As Boolean, ByVal bodega As String, ByVal folio_movimiento As Long, ByVal tipo_precio_venta As Long, ByVal partida_vistas As Long, _
    ByVal folio_descuento_especial_programado As Long, ByVal precio_descuento_especial_programado As Double, ByVal precio_pactado As Double, ByVal descripcion_anterior As String, ByVal impresa_etiqueta_vendido As Boolean) As Events
        InsertarAnticipo = oVentasDetalle.sp_ventas_detalle_ins(sucursal, serie, folio, partida, articulo, preciounitario, precio_contado, sobrepedido, costo, surtido, reparto, bodega, folio_movimiento, tipo_precio_venta, partida_vistas, folio_descuento_especial_programado, precio_descuento_especial_programado, precio_pactado, descripcion_anterior, impresa_etiqueta_vendido)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal precio_pactado As Double) As Events
        Actualizar = oVentasDetalle.sp_ventas_detalle_upd(oData, sucursal, serie, folio, precio_pactado)
    End Function
    '*/ csequera enero 2008
    Public Function Eliminar(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        Eliminar = oVentasDetalle.sp_ventas_detalle_del(sucursal, serie, folio, partida)
    End Function
    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        DespliegaDatos = oVentasDetalle.sp_ventas_detalle_sel(sucursal, serie, folio, partida)
    End Function
    Public Function DespliegaDatosParaOrdenesRecuperacion(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        DespliegaDatosParaOrdenesRecuperacion = oVentasDetalle.sp_ventas_detalle_ordenes_recuperacion_sel(sucursal, serie, folio, partida)
    End Function

    Public Function Listado(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Listado = oVentasDetalle.sp_ventas_detalle_grs(sucursal, serie, folio)
    End Function
    Public Function ListadoPendientesRepartir(ByVal sucursal As Long, ByVal bodega As String, ByVal serie As String, ByVal folio As Long) As Events
        ListadoPendientesRepartir = oVentasDetalle.sp_ventas_detalles_repartos_pendientes_grs(sucursal, bodega, serie, folio)
    End Function
    Public Function ListadoPendientesRepartir2(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        ListadoPendientesRepartir2 = oVentasDetalle.sp_ventas_detalles_repartos_pendientes_filtro_grs(sucursal, serie, folio)
    End Function
    Public Function ListadoExistenciasArticulosBodegas(ByVal articulo As Long) As Events
        ListadoExistenciasArticulosBodegas = oVentasDetalle.sp_ventas_detalle_articulos_existencias_grs(articulo)
    End Function
    Public Function DespliegaDatosxFolioVentaDetalle(ByVal folio_venta_detalle As Long) As Events
        DespliegaDatosxFolioVentaDetalle = Me.oVentasDetalle.sp_ventas_detalle_folio_venta_detalle_sel(folio_venta_detalle)
    End Function
    Public Function ActualizaImpresaEtiquetaVendido(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        ActualizaImpresaEtiquetaVendido = oVentasDetalle.sp_ventas_detalle_impresa_etiqueta_vendido_upd(sucursal, serie, folio, partida)
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal Articulo As Long, ByVal Cantidad As Double, ByVal Bodega As String, ByVal Precio As Double, ByVal ArticuloRegalo As Boolean, ByVal Departamento As Long, ByVal Grupo As Long, ByVal lCategoriasIdentificablesArticulo As Long, ByVal CategoriasIdentificables As Long, ByVal sobrepedido As Boolean, ByVal FolioDescuento As Long) As Events

        Validacion = ValidaArticulo(Articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(Cantidad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPrecio(Precio, ArticuloRegalo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodega(Bodega, sobrepedido)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDepartamento(Departamento)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaGrupo(Grupo)
        If Validacion.ErrorFound Then Exit Function

        If FolioDescuento <= 0 Then
            Validacion = ValidaCategoriasIdentificables(lCategoriasIdentificablesArticulo, CategoriasIdentificables)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function
    Public Function Validacion_SobrePedido(ByVal folio_autorizacion As Long, ByVal articulo As Long, ByVal bodega As String, ByVal cliente As Long, ByVal vendedor As Long, ByVal cantidad As Long) As Events

        Dim oEvent As New Events
        Dim OEVENT_AUTORIZACION As Events
        Dim OEVENT_OCVALIDA As Events
        Dim OEVENT_MARCADOPEDIDOFABRICA As Events


        ' Se Obtienen la  Informacion P�ra Validar Si el Articulo Puede ser Agregado en la Venta.
        OEVENT_AUTORIZACION = ValidaDatosAutorizacion(folio_autorizacion, cliente, vendedor, articulo, cantidad)
        OEVENT_OCVALIDA = ValidaOrdenCompraConEspacio(articulo, bodega)
        OEVENT_MARCADOPEDIDOFABRICA = ValidaArticuloMarcadoPedidoFabrica(articulo)

        'Se revisa la informacion y en caso de error, se manda el mensaje.
        If OEVENT_AUTORIZACION.ErrorFound And OEVENT_OCVALIDA.ErrorFound And OEVENT_MARCADOPEDIDOFABRICA.ErrorFound Then
            'oEvent.Message = "Art�culo: " & articulo.ToString & "  - " & OEVENT_AUTORIZACION.Message & ", " & OEVENT_OCVALIDA.Message & ", " & OEVENT_MARCADOPEDIDOFABRICA.Message
            oEvent.Message = "Art�culo: " & articulo.ToString & "  -  Requiere una autorizaci�n para venta con pedido a fabrica"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If

        'Aqui se Almacenara el Valor del Costo en el Caso del que Aplique
        oEvent.Value = 0

        If Not OEVENT_AUTORIZACION.ErrorFound Then
            oEvent.Value = OEVENT_AUTORIZACION.Value
        End If

        If Not OEVENT_OCVALIDA.ErrorFound Then
            oEvent.Value = OEVENT_OCVALIDA.Value
        End If

        If (Not OEVENT_MARCADOPEDIDOFABRICA.ErrorFound) And CType(oEvent.Value, Integer) = 0 Then
            oEvent.Value = OEVENT_MARCADOPEDIDOFABRICA.Value
        End If


        Validacion_SobrePedido = oEvent

    End Function

    Public Function CajaArticulosFactura(ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long) As Events
        CajaArticulosFactura = oVentasDetalle.sp_ventas_cajas_articulos_factura(sucursal_factura, serie_factura, folio_factura)
    End Function
    Public Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Art�culo es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDepartamento(ByVal Departamento As Long) As Events
        Dim oEvent As New Events
        If Departamento = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Departamento es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaGrupo(ByVal Grupo As Long) As Events
        Dim oEvent As New Events
        If Grupo = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Grupo es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCantidad(ByVal Cantidad As Double) As Events
        Dim oEvent As New Events
        If Cantidad < 1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cantidad es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPrecio(ByVal Precio As Double, ByVal ArticuloRegalo As Boolean) As Events
        Dim oEvent As New Events
        If Precio <= 0 And ArticuloRegalo = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Precio es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBodega(ByVal Bodega As String, ByVal SobrePedido As Boolean) As Events
        Dim oEvent As New Events
        If Bodega = "" And SobrePedido = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaCategoriasIdentificables(ByVal lCategoriasIdentificablesArticulo As Long, ByVal CategoriasIdentificables As Long) As Events
        Dim oEvent As New Events
        Dim categoria As String
        If lCategoriasIdentificablesArticulo <> CategoriasIdentificables And CategoriasIdentificables > -1 Then
            oEvent.Ex = Nothing
            Select Case CategoriasIdentificables
                Case 0
                    categoria = "Identificables"
                Case 1
                    categoria = "No Identificables"
            End Select
            oEvent.Message = "No se Pueden Facturar Articulos diferente a " + categoria
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent

    End Function

    Private Function ValidaDatosAutorizacion(ByVal folio_autorizacion As Long, ByVal cliente As Long, ByVal vendedor As Long, ByVal articulo As Long, ByVal cantidad As Long) As Events

        Dim oEvent As New Events
        Dim odataset As DataSet


        If folio_autorizacion > 0 Then
            oEvent = oAutorizaciones.AutorizaPedido_fabrica_puntoventa_sel(folio_autorizacion, cliente, vendedor, articulo, cantidad)
            If Not oEvent.ErrorFound Then
                odataset = oEvent.Value
                If odataset.Tables(0).Rows.Count = 0 Then
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    oEvent.Message = "Folio de Solicitud No Valido"
                Else

                    oEvent.Value = odataset.Tables(0).Rows(0).Item("costo")
                End If

            End If
        Else
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            oEvent.Message = "Folio de Solicitud No Valido"
        End If

        Return oEvent
    End Function
    Private Function ValidaOrdenCompraConEspacio(ByVal articulo As Long, ByVal bodega As String) As Events
        Dim oEvent As New Events
        Dim odataset As DataSet
        Dim OC As Long


        oEvent = oArticulos.ObtenerOCPorBodega(articulo, bodega)

        If Not oEvent.ErrorFound Then
            odataset = oEvent.Value
            If odataset.Tables(0).Rows.Count > 0 Then
                OC = odataset.Tables(0).Rows(0)("oc")
                If OC <= 0 Then
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    oEvent.Message = "Orden Compra No Valido"
                Else
                    oEvent.Value = 1

                End If
            Else
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                oEvent.Message = "Orden Compra No Valido"
            End If
        End If

        Return oEvent

    End Function
    Private Function ValidaArticuloMarcadoPedidoFabrica(ByVal articulo As Long) As Events
        Dim oEvent As New Events
        Dim odataset As DataSet
        Dim ChkPedidoFabrica As Boolean

        oEvent = oArticulos.DespliegaDatos(articulo)
        If Not oEvent.ErrorFound Then

            odataset = oEvent.Value
            If odataset.Tables(0).Rows.Count > 0 Then

                ChkPedidoFabrica = odataset.Tables(0).Rows(0).Item("pedido_fabrica")

                If Not ChkPedidoFabrica Then
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    oEvent.Message = "El Articulo No Esta Marcado en Catalogo como Pedido a Fabrica "
                Else
                    oEvent.Value = odataset.Tables(0).Rows(0)("costo_pedido_fabrica")
                End If
            End If
        End If

        Return oEvent

    End Function
    Public Function ActualizarBodegaVenta(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long, ByVal bodega_anterior As String, ByVal bodega_nueva As String) As Events
        ActualizarBodegaVenta = oVentasDetalle.sp_cambia_bodega_venta(sucursal, serie, folio, partida, bodega_anterior, bodega_nueva)
    End Function


#End Region

End Class
#End Region


