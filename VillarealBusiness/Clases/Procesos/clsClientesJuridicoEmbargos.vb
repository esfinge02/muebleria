Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientesJuridicoEmbargos
'DATE:		30/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ClientesJuridicoEmbargos"
Public Class clsClientesJuridicoEmbargos
    Private oClientesJuridicoEmbargos As VillarrealData.clsClientesJuridicoEmbargos

#Region "Constructores"
    Sub New()
        oClientesJuridicoEmbargos = New VillarrealData.clsClientesJuridicoEmbargos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Insertar = oClientesJuridicoEmbargos.sp_clientes_juridico_embargos_ins(oData, folio_juridico)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Actualizar = oClientesJuridicoEmbargos.sp_clientes_juridico_embargos_upd(oData, folio_juridico)
    End Function

    Public Function Eliminar(ByVal folio_juridico As Long, ByVal embargo As Long) As Events
        Eliminar = oClientesJuridicoEmbargos.sp_clientes_juridico_embargos_del(folio_juridico, embargo)
    End Function

    Public Function DespliegaDatos(ByVal folio_juridico As Long, ByVal embargo As Long) As Events
        DespliegaDatos = oClientesJuridicoEmbargos.sp_clientes_juridico_embargos_sel(folio_juridico, embargo)
    End Function

    Public Function Listado(ByVal folio_juridico As Long) As Events
        Listado = oClientesJuridicoEmbargos.sp_clientes_juridico_embargos_grs(folio_juridico)
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal Descripcion_Mercancia As String, ByVal Valor_Estimado As Double) As Events
        Validacion = ValidaDescripcion_Mercancia(Descripcion_Mercancia)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaValor_Estimado(Valor_Estimado)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaDescripcion_Mercancia(ByVal Descripcion_Mercancia As String) As Events
        Dim oEvent As New Events
        If Descripcion_Mercancia.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaValor_Estimado(ByVal Valor_Estimado As Double) As Events
        Dim oEvent As New Events
        If Valor_Estimado <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El valor estimado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


