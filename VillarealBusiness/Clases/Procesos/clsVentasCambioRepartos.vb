Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsVentasCambioReparto
    Private oVentasCambioReparto As VillarrealData.clsVentasCambioReparto

    Sub New()
        oVentasCambioReparto = New VillarrealData.clsVentasCambioReparto
    End Sub

    Public Function ObtenerDatosReparto(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        ObtenerDatosReparto = oVentasCambioReparto.sp_datos_reparto_cambio_sel(sucursal, serie, folio)
    End Function

    Public Function Insertar(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal fecha_reparto_original As DateTime, ByVal horario_original As String, ByVal domicilio_original As String, ByVal colonia_original As String, ByVal ciudad_original As String, ByVal estado_original As String, ByVal telefono_original As String, ByVal observaciones_original As String) As Events
        Insertar = oVentasCambioReparto.sp_ventas_cambio_reparto_ins(sucursal, serie, folio, fecha_reparto_original, horario_original, domicilio_original, colonia_original, ciudad_original, estado_original, telefono_original, observaciones_original)
    End Function

    Public Function ActualizarVenta(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal fecha_reparto As DateTime, ByVal horario_reparto As String, ByVal domicilio_reparto As String, ByVal colonia_reparto As String, ByVal ciudad_reparto As String, ByVal estado_reparto As String, ByVal telefono_reparto As String, ByVal observaciones_reparto As String) As Events
        ActualizarVenta = oVentasCambioReparto.sp_ventas_cambio_reparto_upd(sucursal, serie, folio, fecha_reparto, horario_reparto, domicilio_reparto, colonia_reparto, ciudad_reparto, estado_reparto, telefono_reparto, observaciones_reparto)
    End Function
End Class
