Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsProductos
    Private oProductos As VillarrealData.clsProductos

#Region "Constructores"
    Sub New()
        oProductos = New VillarrealData.clsProductos
    End Sub
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal fecha As DateTime, ByVal estatus As Char) As Events
        Insertar = oProductos.sp_productos_ins(convenio, quincena, anio, fecha, estatus)
    End Function

    Public Function Actualizar(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal fecha As DateTime, ByVal estatus As Char, ByVal fecha_aplicacion As Object) As Events
        Actualizar = oProductos.sp_productos_upd(convenio, quincena, anio, fecha, estatus, IIf(IsDate(fecha_aplicacion), fecha_aplicacion, System.DBNull.Value))
    End Function

    Public Function Eliminar(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long) As Events
        Eliminar = oProductos.sp_productos_del(convenio, quincena, anio)
    End Function

    Public Function ActualizarEstatus(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal estatus As Char, ByVal fecha_aplicacion As DateTime) As Events
        ActualizarEstatus = oProductos.sp_productos_estatus_upd(convenio, quincena, anio, estatus, fecha_aplicacion)
    End Function

    Public Function DespliegaDatos(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long) As Events
        DespliegaDatos = oProductos.sp_productos_sel(convenio, quincena, anio)
    End Function

    Public Function Listado(ByVal convenio As Long, ByVal anio As Long) As Events
        Listado = oProductos.sp_productos_grs(convenio, anio)
    End Function

    Public Function Existe(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long) As Events
        Existe = oProductos.sp_productos_exs(convenio, quincena, anio)
    End Function
#End Region

End Class
