Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesRecuperacion
'DATE:		11/10/2007 0:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - OrdenesRecuperacion"
Public Class clsOrdenesRecuperacion
    Private oOrdenesRecuperacion As VillarrealData.clsOrdenesRecuperacion

#Region "Constructores"
    Sub New()
        oOrdenesRecuperacion = New VillarrealData.clsOrdenesRecuperacion
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal elabora As String) As Events
        Insertar = oOrdenesRecuperacion.sp_ordenes_recuperacion_ins(oData, sucursal, elabora)
    End Function
    Public Function Actualizar(ByRef oData As DataSet, ByVal presona_recibe As String, ByVal persona_recoje As String, ByVal persona_inspecciona As String) As Events
        Actualizar = oOrdenesRecuperacion.sp_ordenes_recuperacion_upd(oData, presona_recibe, persona_recoje, persona_inspecciona)
    End Function

    Public Function Eliminar(ByVal folio As Long) As Events
        Eliminar = oOrdenesRecuperacion.sp_ordenes_recuperacion_del(folio)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oOrdenesRecuperacion.sp_ordenes_recuperacion_sel(folio)
    End Function

    Public Function Listado() As Events
        Listado = oOrdenesRecuperacion.sp_ordenes_recuperacion_grs()
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal Sucursal As Integer, ByVal Serie As String, ByVal Folio As Integer) As Events
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerie(Serie)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFolio(Folio)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSerie(ByVal Serie As String) As Events
        Dim oEvent As New Events
        If Serie Is Nothing Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        Else
            If Serie.Trim.Length = 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "La Serie es Requerida"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
    Public Function ValidaFolio(ByVal Folio As Long) As Events
        Dim oEvent As New Events
        If Folio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region




'Public Class clsOrdenesRecuperacion

'End Class
