Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsHisCostos
'DATE:		15/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - HisCostos"
Public Class clsHisCostos
    Private oHisCostos As VillarrealData.clsHisCostos

#Region "Constructores"
    Sub New()
        oHisCostos = New VillarrealData.clsHisCostos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef folio As Long, ByVal fecha As Date, ByVal articulo As Long, ByVal bodega As String, ByVal cantidad As Double, ByVal costo As Double, ByVal saldo As Double) As Events
        Insertar = oHisCostos.sp_his_costos_ins(folio, fecha, articulo, bodega, cantidad, costo, saldo)
    End Function

    Public Function Actualizar(ByRef folio As Long, ByVal fecha As Date, ByVal articulo As Long, ByVal bodega As String, ByVal cantidad As Double, ByVal costo As Double, ByVal saldo As Double) As Events
        Actualizar = oHisCostos.sp_his_costos_upd(folio, fecha, articulo, bodega, cantidad, costo, saldo)
    End Function

    Public Function ActualizaSaldo(ByVal folio As Long, ByVal cantidad As Long) As Events
        ActualizaSaldo = oHisCostos.sp_his_costos_actualiza_saldo(folio, cantidad)
    End Function

    Public Function ActualizaSaldoCantidad(ByVal folio As Long, ByVal saldo As Long, ByVal cantidad As Long) As Events
        ActualizaSaldoCantidad = oHisCostos.sp_his_costos_actualiza_saldo_cantidad(folio, saldo, cantidad)
    End Function

    Public Function ActualizaSaldoDisminuye(ByVal folio As Long, ByVal cantidad As Long) As Events
        ActualizaSaldoDisminuye = oHisCostos.sp_his_costos_actualiza_saldo_disminuye(folio, cantidad)
    End Function

    Public Function Eliminar(ByVal folio As Long) As Events
        Eliminar = oHisCostos.sp_his_costos_del(folio)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oHisCostos.sp_his_costos_sel(folio)
    End Function

    Public Function Listado() As Events
        Listado = oHisCostos.sp_his_costos_grs()
    End Function

    Public Function UltimoCostoArticulo(ByVal articulo As Long, ByVal bodega As String) As Events
        UltimoCostoArticulo = oHisCostos.sp_ultimo_costo_articulo(articulo, bodega)
    End Function

    Public Function ExisteFolioArticulo(ByVal folio As Long, ByVal articulo As Long) As Events
        ExisteFolioArticulo = oHisCostos.sp_his_costos_cancelacion_exs(folio, articulo)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function


#End Region

End Class
#End Region


