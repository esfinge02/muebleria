Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCorrespondenciaClientesDetalle
'DATE:		02/02/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CorrespondenciaClientesDetalle"
Public Class clsCorrespondenciaClientesDetalle
    Private oCorrespondenciaClientesDetalle As VillarrealData.clsCorrespondenciaClientesDetalle

#Region "Constructores"
    Sub New()
        oCorrespondenciaClientesDetalle = New VillarrealData.clsCorrespondenciaClientesDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal correspondencia As Long, ByVal partida As Long, ByVal campo_correspondencia As String) As Events
        Insertar = oCorrespondenciaClientesDetalle.sp_correspondencia_clientes_detalle_ins(correspondencia, partida, campo_correspondencia)
    End Function

    Public Function Actualizar(ByVal correspondencia As Long, ByVal partida As Long, ByVal campo_correspondencia As String) As Events
        Actualizar = oCorrespondenciaClientesDetalle.sp_correspondencia_clientes_detalle_upd(correspondencia, partida, campo_correspondencia)
    End Function

    Public Function Eliminar(ByVal correspondencia As Long) As Events
        Eliminar = oCorrespondenciaClientesDetalle.sp_correspondencia_clientes_detalle_del(correspondencia)
    End Function

    Public Function DespliegaDatos(ByVal correspondencia As Long, ByVal partida As Long) As Events
        DespliegaDatos = oCorrespondenciaClientesDetalle.sp_correspondencia_clientes_detalle_sel(correspondencia, partida)
    End Function

    Public Function Listado(ByVal correspondencia As Long) As Events
        Listado = oCorrespondenciaClientesDetalle.sp_correspondencia_clientes_detalle_grs(correspondencia)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region


End Class
#End Region


