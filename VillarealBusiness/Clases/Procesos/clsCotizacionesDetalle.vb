Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCotizacionesDetalle
'DATE:		31/08/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CotizacionesDetalle"
Public Class clsCotizacionesDetalle
    Private oCotizacionesDetalle As VillarrealData.clsCotizacionesDetalle

#Region "Constructores"
    Sub New()
        oCotizacionesDetalle = New VillarrealData.clsCotizacionesDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef cotizacion As Long, ByRef articulo As Long, ByVal cantidad As Double) As Events
        Insertar = oCotizacionesDetalle.sp_cotizaciones_detalle_ins(cotizacion, articulo, cantidad)
    End Function

    Public Function Actualizar(ByRef cotizacion As Long, ByRef articulo As Long, ByVal cantidad As Double) As Events
        Actualizar = oCotizacionesDetalle.sp_cotizaciones_detalle_upd(cotizacion, articulo, cantidad)
    End Function

    Public Function Eliminar(ByVal cotizacion As Long, Optional ByVal articulo As Long = -1) As Events
        Eliminar = oCotizacionesDetalle.sp_cotizaciones_detalle_del(cotizacion, articulo)
    End Function

    Public Function DespliegaDatos(ByVal cotizacion As Long, ByVal articulo As Long) As Events
        DespliegaDatos = oCotizacionesDetalle.sp_cotizaciones_detalle_sel(cotizacion, articulo)
    End Function

    Public Function Listado(ByVal cotizacion As Long) As Events
        Listado = oCotizacionesDetalle.sp_cotizaciones_detalle_grs(cotizacion)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


