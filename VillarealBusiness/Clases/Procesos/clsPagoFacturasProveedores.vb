Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPagoFacturasProveedores
'DATE:		04/10/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - PagoFacturasProveedores"
Public Class clsPagoFacturasProveedores
    Private oPagoFacturasProveedores As VillarrealData.clsPagoFacturasProveedores

#Region "Constructores"
    Sub New()
        oPagoFacturasProveedores = New VillarrealData.clsPagoFacturasProveedores
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef folio As Long, ByVal concepto As String, ByVal fecha As Date, ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio_cheque As Long, ByVal importe As Double, ByVal proveedor As Long, ByVal cancelado As Boolean, ByVal abono_cuenta As Boolean) As Events
        Insertar = oPagoFacturasProveedores.sp_pago_facturas_proveedores_ins(folio, concepto, fecha, banco, cuentabancaria, folio_cheque, importe, proveedor, cancelado, abono_cuenta)
    End Function

    Public Function Actualizar(ByRef folio As Long, ByVal concepto As String, ByVal fecha As Date, ByVal banco As Date, ByVal cuentabancaria As String, ByVal folio_cheque As Long, ByVal importe As Double, ByVal proveedor As Long, ByVal cancelado As Boolean, ByVal abono_cuenta As Boolean) As Events
        Actualizar = oPagoFacturasProveedores.sp_pago_facturas_proveedores_upd(folio, concepto, fecha, banco, cuentabancaria, folio_cheque, importe, proveedor, cancelado, abono_cuenta)
    End Function

    Public Function Eliminar(ByVal folio As Long, ByVal concepto As String) As Events
        Eliminar = oPagoFacturasProveedores.sp_pago_facturas_proveedores_del(folio, concepto)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long, ByVal concepto As String) As Events
        DespliegaDatos = oPagoFacturasProveedores.sp_pago_facturas_proveedores_sel(folio, concepto)
    End Function

    Public Function Listado() As Events
        Listado = oPagoFacturasProveedores.sp_pago_facturas_proveedores_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal chequera As String, ByVal proveedor As Long, ByVal RegistrosSeleecionados As Long) As Events
        Validacion = ValidaCuentaBancaria(chequera)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaProveedor(proveedor)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidafilasSeleccionadas(RegistrosSeleecionados)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function Validacion_PagosVariasFacturas(ByVal Action As Actions, ByVal chequera As String, ByVal proveedor As Long, ByVal RegistrosSeleecionados As Long, ByVal ConceptoMovimiento As Long, ByRef intControl As Long) As Events
        Validacion_PagosVariasFacturas = ValidaCuentaBancaria(chequera)
        If Validacion_PagosVariasFacturas.ErrorFound Then
            intControl = 1
            Exit Function
        End If
        Validacion_PagosVariasFacturas = ValidaProveedor(proveedor)
        If Validacion_PagosVariasFacturas.ErrorFound Then
            intControl = 2
            Exit Function
        End If
        Validacion_PagosVariasFacturas = ValidafilasSeleccionadas(RegistrosSeleecionados)
        If Validacion_PagosVariasFacturas.ErrorFound Then Exit Function
        If Action = Actions.Insert Or Action = Actions.Update Then
            Validacion_PagosVariasFacturas = ValidaConceptoMovimiento(ConceptoMovimiento)
            If Validacion_PagosVariasFacturas.ErrorFound Then
                intControl = 3
                Exit Function
            End If
        End If
    End Function

    Private Function ValidaCuentaBancaria(ByVal chequera As String) As Events
        Dim oEvent As New Events
        If chequera.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Chequera es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaProveedor(ByVal Proveedor As Long) As Events
        Dim oEvent As New Events
        If Proveedor <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidafilasSeleccionadas(ByVal RegistrosSeleecionados As Long) As Events
        Dim oEvent As New Events
        If RegistrosSeleecionados = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos una Factura es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaConceptoMovimiento(ByVal ConceptoMovimiento As Long) As Events
        Dim oEvent As New Events
        If ConceptoMovimiento <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto del Movimiento es requerido. Por favor, seleccione un concepto."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


