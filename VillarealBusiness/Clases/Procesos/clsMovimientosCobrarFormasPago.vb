Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosCobrarFormasPago
'DATE:		24/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - MovimientosCobrarFormasPago"
Public Class clsMovimientosCobrarFormasPago
    Private oMovimientosCobrarFormasPago As VillarrealData.clsMovimientosCobrarFormasPago

#Region "Constructores"
    Sub New()
        oMovimientosCobrarFormasPago = New VillarrealData.clsMovimientosCobrarFormasPago
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long, ByVal sucursal_pago As Long, ByVal fecha As Date, ByVal caja As Long, ByVal cajero As Long, ByVal monto As Double, ByVal tipo_cambio As Double, ByVal dolares As Long, ByVal ultimos_digitos_cuenta As String) As Events
        Insertar = oMovimientosCobrarFormasPago.sp_movimientos_cobrar_formas_pago_ins(sucursal_documento, concepto_documento, serie_documento, folio_documento, cliente_documento, forma_pago, sucursal_pago, fecha, caja, cajero, monto, tipo_cambio, dolares, ultimos_digitos_cuenta)
    End Function

    'Public Function Actualizar(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long, ByVal sucursal_pago As Long, ByVal fecha As Date, ByVal caja As Long, ByVal cajero As Long, ByVal monto As Double, ByVal tipo_cambio As Double, ByVal dolares As Long, ByVal ultimos_digitos_cuenta As Long) As Events
    '    Actualizar = oMovimientosCobrarFormasPago.sp_movimientos_cobrar_formas_pago_upd(sucursal_documento, concepto_documento, serie_documento, folio_documento, cliente_documento, forma_pago, sucursal_pago, fecha, caja, cajero, monto, tipo_cambio, dolares)
    'End Function

    Public Function Eliminar(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long) As Events
        Eliminar = oMovimientosCobrarFormasPago.sp_movimientos_cobrar_formas_pago_del(sucursal_documento, concepto_documento, serie_documento, folio_documento, cliente_documento, forma_pago)
    End Function

    Public Function DespliegaDatos(ByVal sucursal_documento As Long, ByVal concepto_documento As String, ByVal serie_documento As String, ByVal folio_documento As Long, ByVal cliente_documento As Long, ByVal forma_pago As Long) As Events
        DespliegaDatos = oMovimientosCobrarFormasPago.sp_movimientos_cobrar_formas_pago_sel(sucursal_documento, concepto_documento, serie_documento, folio_documento, cliente_documento, forma_pago)
    End Function

    Public Function Listado() As Events
        Listado = oMovimientosCobrarFormasPago.sp_movimientos_cobrar_formas_pago_grs()
    End Function

    Private Function ValidaUltimosDigitos(ByVal UltimosDigitos As String) As Events
        Dim response As New Events

        If UltimosDigitos.Trim.Length = 0 Then
            response.Message = "Los ultimos digitos son requeridos"
        Else
            If UltimosDigitos.Trim.Length < 4 Then
                response.Message = "Los cantidad de digitos son insuficientes"
            End If
        End If

        Return response
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal forma_pago As Long, ByVal chkdolares As Boolean, ByVal importe As Double, ByVal dolares As Double, ByVal SolictaUltimosDigitos As Boolean, ByVal UltimosDigitos As String) As Events
        Validacion = Validaformapago(forma_pago)
        If Validacion.ErrorFound Then Exit Function
        If chkdolares = False Then
            Validacion = Validaimporte(importe)
            If Validacion.ErrorFound Then Exit Function

            If SolictaUltimosDigitos Then
                Validacion = ValidaUltimosDigitos(UltimosDigitos)
            End If

        Else
            Validacion = Validadolares(dolares)
            If Validacion.ErrorFound Then Exit Function
            Validacion = Validaimporte(importe)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function


    Public Function Validaformapago(ByVal formapago As Long) As Events
        Dim oEvent As New Events
        If formapago <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La forma de pago es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


    Public Function Validaimporte(ByVal importe As Long) As Events
        Dim oEvent As New Events
        If importe <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El importe es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function Validadolares(ByVal dolares As Long) As Events
        Dim oEvent As New Events
        If dolares <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los d�lares son requeridos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


