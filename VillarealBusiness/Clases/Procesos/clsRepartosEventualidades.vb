Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsRepartosEventualidades
'DATE:		17/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - RepartosEventualidades"
Public Class clsRepartosEventualidades
    Private oRepartosEventualidades As VillarrealData.clsRepartosEventualidades

#Region "Constructores"
    Sub New()
        oRepartosEventualidades = New VillarrealData.clsRepartosEventualidades
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal reparto As Long) As Events
        Insertar = oRepartosEventualidades.sp_repartos_eventualidades_ins(oData, sucursal, reparto)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal reparto As Long) As Events
        Actualizar = oRepartosEventualidades.sp_repartos_eventualidades_upd(oData, sucursal, reparto)
    End Function

    Public Function Eliminar(ByVal reparto As Long, ByVal eventualidad As Long) As Events
        Eliminar = oRepartosEventualidades.sp_repartos_eventualidades_del(reparto, eventualidad)
    End Function

    Public Function DespliegaDatos(ByVal reparto As Long, ByVal eventualidad As Long) As Events
        DespliegaDatos = oRepartosEventualidades.sp_repartos_eventualidades_sel(reparto, eventualidad)
    End Function

    Public Function Listado(ByVal reparto As Long) As Events
        Listado = oRepartosEventualidades.sp_repartos_eventualidades_grs(reparto)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Eventualidad As Long, ByVal observaciones As String) As Events
        Validacion = ValidaEventualidad(Eventualidad)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaObservaciones(observaciones)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Private Function ValidaEventualidad(ByVal eventualidad As Long) As Events
        Dim oEvent As New Events
        If eventualidad <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Eventualidad es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
    Private Function ValidaObservaciones(ByVal Observaciones As String) As Events
        Dim oEvent As New Events
        If Observaciones.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Observacion es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


