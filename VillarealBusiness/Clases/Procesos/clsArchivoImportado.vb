Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class clsArchivoImportado
    Private oArchivoImportado As VillarrealData.clsArchivoImportado


#Region "Constructores"
    Sub New()
        oArchivoImportado = New VillarrealData.clsArchivoImportado
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"

    Public Function TraerMovimientosImportados(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal nombre_tabla As String) As Events
        TraerMovimientosImportados = oArchivoImportado.sp_traer_movimientos_importados(sucursal, convenio, quincena, anio, nombre_tabla)
    End Function
    Public Function ActualizaMovimientosImportados(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal sucursal As Long, ByVal cliente As Long, ByVal importe As Long) As Events
        ActualizaMovimientosImportados = oArchivoImportado.ca_actualiza_movimientos_importados(convenio, quincena, anio, sucursal, cliente, importe)
    End Function

#End Region

End Class
