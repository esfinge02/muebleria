Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDevolucionesProveedorDetalle
'DATE:		22/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - DevolucionesProveedorDetalle"
Public Class clsDevolucionesProveedorDetalle
    Private oDevolucionesProveedorDetalle As VillarrealData.clsDevolucionesProveedorDetalle



#Region "Constructores"
    Sub New()
        oDevolucionesProveedorDetalle = New VillarrealData.clsDevolucionesProveedorDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal bodega As String, ByVal devolucion As Long) As Events
        Insertar = oDevolucionesProveedorDetalle.sp_devoluciones_proveedor_detalle_ins(oData, bodega, devolucion)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal bodega As String, ByVal devolucion As Long) As Events
        Actualizar = oDevolucionesProveedorDetalle.sp_devoluciones_proveedor_detalle_upd(oData, bodega, devolucion)
    End Function

    Public Function Eliminar(ByVal bodega As String, ByVal devolucion As Long, ByVal partida As Long) As Events
        Eliminar = oDevolucionesProveedorDetalle.sp_devoluciones_proveedor_detalle_del(bodega, devolucion, partida)
    End Function

    Public Function DespliegaDatos(ByVal bodega As String, ByVal devolucion As Long) As Events
        DespliegaDatos = oDevolucionesProveedorDetalle.sp_devoluciones_proveedor_detalle_sel(bodega, devolucion)
    End Function


    Public Function Listado() As Events
        Listado = oDevolucionesProveedorDetalle.sp_devoluciones_proveedor_detalle_grs()
    End Function

    'Public Function ArticuloExistenciasEntradaActual(ByVal entrada As Long, ByVal articulo As Long) As Long
    '    ArticuloExistenciasEntradaActual = oDevolucionesProveedorDetalle.sp_articulo_existencias_entrada_actual(entrada, articulo)
    'End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Articulo As String, ByVal Cantidad As Double, ByVal maneja_Series As Boolean, ByVal serie As String, ByVal insertarhis_series As Boolean) As Events
        Validacion = ValidaArticulo(Articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(Cantidad)
        If Validacion.ErrorFound Then Exit Function

        If maneja_Series = False Then Exit Function
        Validacion = ValidaSerie(serie)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerieHisSeries(insertarhis_series)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal serie As String) As Events
        Validacion = ValidaSerie(serie)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function ValidaExistenciasArticulos(ByVal existencia_articulo As Long, ByVal cantidad_articulo_en_grv As Long, ByVal serie_ocupada As Boolean) As Events
        ValidaExistenciasArticulos = ValidaExistenciaArticulo(existencia_articulo)
        If ValidaExistenciasArticulos.ErrorFound Then Exit Function
        ValidaExistenciasArticulos = ValidaExistenciaArticuloEnGRV(existencia_articulo, cantidad_articulo_en_grv)
        If ValidaExistenciasArticulos.ErrorFound Then Exit Function
        ValidaExistenciasArticulos = ValidaSerieOcupada(serie_ocupada)
        If ValidaExistenciasArticulos.ErrorFound Then Exit Function
    End Function

    Public Function Validacion(ByVal Entrada As Long, ByVal Departamento As Long, ByVal Grupo As Long, ByVal Articulo As Long, ByVal cantidad As Long, ByVal orden_servicio As Long, ByVal Accion As Actions) As Events
        'Validacion = ValidaEntrada(Entrada)
        'If Validacion.ErrorFound Then Exit Function
        If Accion = Actions.Insert Or Accion = Actions.Update Then
            Validacion = ValidaOrdenServicio(orden_servicio)
            If Validacion.ErrorFound Then Exit Function
        End If
        
        Validacion = ValidaDepartamento(Departamento)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaGrupo(Grupo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulo(Articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(cantidad)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Private Function ValidaArticulo(ByVal Articulo As String) As Events
        Dim oEvent As New Events
        If Articulo.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaCantidad(ByVal Cantidad As Double) As Events
        Dim oEvent As New Events
        If Cantidad <= 0 Or IsNumeric(Cantidad) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cantidad Recibida es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCantidadDevolucion(ByVal Cantidad_devuelta As Double, ByVal cantidad_recibida As Double) As Events
        Dim oEvent As New Events
        If cantidad_recibida < Cantidad_devuelta Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cantidad Devuelta (" + Cantidad_devuelta.ToString + ") es Mayor que la Cantidad Recibida (" + cantidad_recibida.ToString + ")"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent

    End Function

    Private Function ValidaSerie(ByVal serie As String) As Events
        Dim oEvent As New Events
        If serie.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El N�mero de Serie es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaSerieHisSeries(ByVal insertarhis_series As Boolean) As Events

        Dim oEvent As New Events

        If insertarhis_series = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Movimiento del  Numero de Serie no se puede Insertar porque no hay una Entrada"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaEntrada(ByVal Entrada As Long) As Events
        Dim oEvent As New Events
        If Entrada <= 0 Or IsNumeric(Entrada) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Entrada es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaOrdenServicio(ByVal OrdenServicio As Long) As Events
        Dim oEvent As New Events
        If OrdenServicio <= 0 Or IsNumeric(OrdenServicio) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Orden de Servicio es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaDepartamento(ByVal Departamento As Long) As Events
        Dim oEvent As New Events
        If Departamento <= 0 Or IsNumeric(Departamento) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Departamento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaGrupo(ByVal Grupo As Long) As Events
        Dim oEvent As New Events
        If Grupo <= 0 Or IsNumeric(Grupo) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Grupo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo <= 0 Or IsNumeric(Articulo) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Art�culo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaExistenciaArticulo(ByVal existencia_articulo As Long) As Events
        Dim oEvent As New Events
        If existencia_articulo <= 0 Or IsNumeric(existencia_articulo) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Art�culo no tiene Existencias"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaExistenciaArticuloEnGRV(ByVal existencia_articulo As Long, ByVal cantidad_articulo_en_grv As Long) As Events
        Dim oEvent As New Events
        If (cantidad_articulo_en_grv < 0 Or IsNumeric(cantidad_articulo_en_grv) = False) Or (cantidad_articulo_en_grv >= existencia_articulo) Then
            oEvent.Ex = Nothing
            oEvent.Message = "La cantidad del articulo debe ser Mayor a los articulos a devolver"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaSerieOcupada(ByVal serie_ocupada As Boolean) As Events
        Dim oEvent As New Events
        If (serie_ocupada = True) Then
            oEvent.Ex = Nothing
            oEvent.Message = "La serie del art�culo ya ha sido seleccionada"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function





#End Region

End Class
#End Region


