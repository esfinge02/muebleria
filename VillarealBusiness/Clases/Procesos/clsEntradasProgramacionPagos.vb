Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradasProgramacionPagos
'DATE:		02/10/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - EntradasProgramacionPagos"
Public Class clsEntradasProgramacionPagos
    Private oEntradasProgramacionPagos As VillarrealData.clsEntradasProgramacionPagos

#Region "Constructores"
    Sub New()
        oEntradasProgramacionPagos = New VillarrealData.clsEntradasProgramacionPagos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal entrada As Long, ByVal partida As Long, ByVal fecha As Date, ByVal importe As Double, ByVal descuento As Double, ByVal total As Double) As Events
        Insertar = oEntradasProgramacionPagos.sp_entradas_programacion_pagos_ins(entrada, partida, fecha, importe, descuento, total)
    End Function

    Public Function Actualizar(ByVal entrada As Long, ByVal partida As Long, ByVal fecha As Date, ByVal importe As Double, ByVal descuento As Double, ByVal total As Double) As Events
        Actualizar = oEntradasProgramacionPagos.sp_entradas_programacion_pagos_upd(entrada, partida, fecha, importe, descuento, total)
    End Function

    Public Function Eliminar(ByVal entrada As Long, ByVal partida As Long) As Events
        Eliminar = oEntradasProgramacionPagos.sp_entradas_programacion_pagos_del(entrada, partida)
    End Function

    Public Function DespliegaDatos(ByVal entrada As Long) As Events
        DespliegaDatos = oEntradasProgramacionPagos.sp_entradas_programacion_pagos_sel(entrada)
    End Function

    Public Function Listado() As Events
        Listado = oEntradasProgramacionPagos.sp_entradas_programacion_pagos_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


