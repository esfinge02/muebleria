Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsPaquetesDetalle
    Private oPaquetesDetalle As VillarrealData.clsPaquetesDetalle

#Region "Constructores"
    Sub New()
        oPaquetesDetalle = New VillarrealData.clsPaquetesDetalle
    End Sub
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal paquete As Long, ByRef oData As DataSet) As Events
        Insertar = oPaquetesDetalle.sp_paquetes_detalle_ins(paquete, oData)
    End Function

    Public Function Actualizar(ByVal paquete As Long, ByRef oData As DataSet) As Events
        Actualizar = oPaquetesDetalle.sp_paquetes_detalle_upd(paquete, oData)
    End Function

    Public Function Eliminar(ByVal paquete As Long, ByVal partida As Long) As Events
        Eliminar = oPaquetesDetalle.sp_paquetes_detalle_del(paquete, partida)
    End Function

    Public Function DespliegaDatos(ByVal paquete As Long, ByVal partida As Long) As Events
        DespliegaDatos = oPaquetesDetalle.sp_paquetes_detalle_grs(paquete, partida)
    End Function

    Public Function Validacion(ByVal articulo As Long, ByVal cantidad As Long, ByVal contado As Double, ByVal credito As Double) As Events
        Validacion = ValidaArticulo(articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(cantidad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPrecioContado(contado)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPrecioCredito(credito)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaArticulo(ByVal articulo As Long) As Events
        Dim oevents As New Events
        If articulo = -1 Then
            oevents.Message = "El Art�culo es requerido"
        End If
        Return oevents
    End Function
    Public Function ValidaCantidad(ByVal cantidad As Integer) As Events
        Dim oevents As New Events
        If cantidad < 1 Then
            oevents.Message = "La Cantidad debe ser mayor a 0"
        End If
        Return oevents

    End Function
    Public Function ValidaPrecioContado(ByVal contado As Double) As Events
        Dim oevents As New Events
        If contado < 1 Then
            oevents.Message = "El Precio de Contado debe ser mayor a 0"
        End If
        Return oevents
    End Function
    Public Function ValidaPrecioCredito(ByVal credito As Double) As Events
        Dim oevents As New Events
        If credito < 1 Then
            oevents.Message = "El Precio de Cr�dito debe ser mayor a 0"
        End If
        Return oevents
    End Function


#End Region

End Class
