Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosInventarios
'DATE:		13/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - MovimientosInventarios"
Public Class clsMovimientosInventarios
    Private oMovimientosInventarios As VillarrealData.clsMovimientosInventarios

#Region "Constructores"
    Sub New()
        oMovimientosInventarios = New VillarrealData.clsMovimientosInventarios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha As DateTime, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String) As Events
        Insertar = oMovimientosInventarios.sp_movimientos_inventarios_ins(sucursal, bodega, concepto, folio, fecha, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
    End Function

    Public Function Actualizar(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByRef folio As Long, ByVal fecha As DateTime, ByVal sucursal_referencia As Long, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal observaciones As String) As Events
        Actualizar = oMovimientosInventarios.sp_movimientos_inventarios_upd(sucursal, bodega, concepto, folio, fecha, sucursal_referencia, concepto_referencia, folio_referencia, observaciones)
    End Function

    Public Function Eliminar(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long) As Events
        Eliminar = oMovimientosInventarios.sp_movimientos_inventarios_del(bodega, concepto, folio)
    End Function

    Public Function DespliegaDatos(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long) As Events
        DespliegaDatos = oMovimientosInventarios.sp_movimientos_inventarios_sel(bodega, concepto, folio)
    End Function

    Public Function Listado(Optional ByVal bodega As String = "-1", Optional ByVal concepto As String = "-1", Optional ByVal fechai As String = "01/01/1900", Optional ByVal fechaf As String = "01/01/1900") As Events
        Listado = oMovimientosInventarios.sp_movimientos_inventarios_grs(bodega, concepto, CDate(fechai), CDate(fechaf))
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal concepto As String, ByVal detalle As Long) As Events
        Validacion = ValidaConcepto(concepto)
        If Validacion.ErrorFound Then Exit Function
        Validacion = Validadetalle(detalle)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaConcepto(ByVal concepto As String) As Events
        Dim oevents As New Events
        If concepto.Length = 0 Then
            oevents.Message = "El Concepto es requerido"
        End If
        Return oevents
    End Function
    Public Function Validadetalle(ByVal detalle As Long) As Events
        Dim oevents As New Events
        If detalle = 0 Then
            oevents.Message = "El Movimiento requiere partidas"
        End If
        Return oevents
    End Function
#End Region

End Class
#End Region


