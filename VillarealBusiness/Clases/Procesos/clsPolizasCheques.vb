Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPolizasCheques
'DATE:		11/04/2008 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - PolizasCheques"
Public Class clsPolizasCheques
    Private oPolizasCheques As VillarrealData.clsPolizasCheques

#Region "Constructores"
    Sub New()
        oPolizasCheques = New VillarrealData.clsPolizasCheques
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal folio_poliza_cheque As Long, ByVal tipo_pago As Long) As Events
        Insertar = oPolizasCheques.sp_polizas_cheques_ins(oData, folio_poliza_cheque, tipo_pago)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal folio_poliza_cheque As Long) As Events
        Actualizar = oPolizasCheques.sp_polizas_cheques_upd(oData, folio_poliza_cheque)
    End Function

    Public Function Eliminar(ByVal folio_poliza_cheque As Long, ByVal partida As Long) As Events
        Eliminar = oPolizasCheques.sp_polizas_cheques_del(folio_poliza_cheque, partida)
    End Function

    Public Function DespliegaDatos(ByVal folio_poliza_cheque As Long, Optional ByVal partida As Long = -1) As Events
        DespliegaDatos = oPolizasCheques.sp_polizas_cheques_sel(folio_poliza_cheque, partida)
    End Function

    'Public Function Listado() As Events
    '    Listado = oPolizasCheques.sp_polizas_cheques_grs()
    'End Function

    Public Function ObtenerSiguienteFolio() As Events
        ObtenerSiguienteFolio = oPolizasCheques.sp_polizas_cheques_siguiente_folio
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal cuenta_contable As String, ByVal RevisaImporte As Boolean, ByVal Concepto As String) As Events

        Validacion = ValidaCuentaContable(cuenta_contable)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCargoAbono(RevisaImporte)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConcepto(Concepto)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Private Function ValidaCuentaContable(ByVal cuenta_contable As String) As Events
        Dim oEvent As New Events
        If cuenta_contable.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La cuenta contable es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaCargoAbono(ByVal RevisaImporte As Boolean) As Events
        Dim oEvent As New Events
        If RevisaImporte = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cargo o Abono es Requerido(a)"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaConcepto(ByVal concepto As String) As Events
        Dim oEvent As New Events
        If concepto.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


