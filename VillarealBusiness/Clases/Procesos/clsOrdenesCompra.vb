Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesCompra
'DATE:		06/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - OrdenesCompra"
Public Class clsOrdenesCompra
	Private oOrdenesCompra As VillarrealData.clsOrdenesCompra

#Region "Constructores"
	Sub New()
		oOrdenesCompra = New VillarrealData.clsOrdenesCompra
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oOrdenesCompra.sp_ordenes_compra_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oOrdenesCompra.sp_ordenes_compra_upd(oData)
	End Function

	Public Function Eliminar(ByVal orden as long) As Events
		Eliminar = oOrdenesCompra.sp_ordenes_compra_del(orden)
	End Function

	Public Function DespliegaDatos(ByVal orden as long) As Events
		DespliegaDatos = oOrdenesCompra.sp_ordenes_compra_sel(orden)
	End Function

    Public Function Listado(ByVal tipo_status As Integer) As Events
        Listado = oOrdenesCompra.sp_ordenes_compra_grs(tipo_status)
    End Function

    Public Function Lookup(Optional ByVal Proveedor As Long = -1) As Events
        Lookup = oOrdenesCompra.sp_ordenes_compra_grl(Proveedor)
    End Function

    Public Function LookupOrdenesCerradas() As Events
        LookupOrdenesCerradas = oOrdenesCompra.sp_ordenes_compra_cerradas_grl()
    End Function

    Public Function CerrarOrden(ByVal orden As Long) As Events
        CerrarOrden = oOrdenesCompra.sp_ordenes_compra_cerrar(orden)
    End Function

    Public Function AbrirOrden(ByVal orden As Long) As Events
        AbrirOrden = oOrdenesCompra.sp_ordenes_compra_abrir(orden)
    End Function

    Public Function CambiarStatus(ByVal orden As Long) As Events
        CambiarStatus = oOrdenesCompra.sp_ordenes_compra_cambiar_status(orden)
    End Function

    Public Function DespliegaSurtimientos(ByVal orden As Long) As Events
        DespliegaSurtimientos = oOrdenesCompra.ca_despliega_surtimientos(orden)
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Sucursal As Long, ByVal Fecha As String, ByVal Fecha_Promesa As String, ByVal Bodega As String, ByVal Proveedor As Long, ByVal folio_proveedor As String, ByVal status As String, ByVal NumArticulos As Long, ByVal odata_articulos As DataSet) As Events
        Validacion = ValidaSucursal(Sucursal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(Fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha_Promesa(Fecha_Promesa)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodega(Bodega)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaProveedor(Proveedor)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaNumeroArticulos(NumArticulos)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaCantidades(odata_articulos)
        If Validacion.ErrorFound Then Exit Function

        If Action = Actions.Delete Then
            Validacion = ValidaStatusPS(status)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal orden As Long) As Events
        Validacion = ValidaOrden(orden)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaStatusPS(ByVal status As String) As Events
        Dim oEvent As New Events
        If status.Trim <> "PS" Then
            oEvent.Ex = Nothing
            oEvent.Message = "No se puede eliminar porque el Status es distinto de 'Por Surtir'"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaOrden(ByVal orden As Long) As Events
        Dim oEvent As New Events
        If orden <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Orden es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFecha(ByVal Fecha As String) As Events
        Dim oEvent As New Events
        If Fecha = Nothing Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFecha_Promesa(ByVal Fecha_Promesa As String) As Events
        Dim oEvent As New Events
        If Fecha_Promesa = Nothing Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha Promesa es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBodega(ByVal Bodega As String) As Events
        Dim oEvent As New Events
        If Bodega.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaProveedor(ByVal Proveedor As Long) As Events
        Dim oEvent As New Events
        If Proveedor <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFolio_Proveedor(ByVal folio_proveedor As String) As Events
        Dim oEvent As New Events
        If folio_proveedor.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio del Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNumeroArticulos(ByVal num_articulos As Long) As Events
        Dim oEvent As New Events
        If num_articulos < 1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Por lo menos debe existir un Artículo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaCantidades(ByVal odata As DataSet) As Events
        Dim oEvent As New Events
        Dim row() As DataRow

        row = odata.Tables(0).Select("cantidad > 0")

        If row.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Articulo con Cantidad Pedida es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


