Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsRepartosDetalle
'DATE:		12/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - RepartosDetalle"
Public Class clsRepartosDetalle
	Private oRepartosDetalle As VillarrealData.clsRepartosDetalle

#Region "Constructores"
	Sub New()
		oRepartosDetalle = New VillarrealData.clsRepartosDetalle
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByVal sucursal As Long, ByVal reparto As Long, ByRef partida As Long, ByVal articulo As Long, ByVal cantidad As Long, ByVal bodega As String, ByVal serie As String, ByVal folio_factura As Long, ByVal partida_factura As Long, ByVal numero_serie As String) As Events
        Insertar = oRepartosDetalle.sp_repartos_detalle_ins(sucursal, reparto, partida, articulo, cantidad, bodega, serie, folio_factura, partida_factura, numero_serie)
    End Function

    'Public Function Actualizar(ByRef oData As DataSet) As Events
    '    Actualizar = oRepartosDetalle.sp_repartos_detalle_upd(oData)
    'End Function

    Public Function Eliminar(ByVal reparto As Long, ByVal partida As Long) As Events
        Eliminar = oRepartosDetalle.sp_repartos_detalle_del(reparto, partida)
    End Function

    Public Function DespliegaDatos(ByVal reparto As Long, ByVal partida As Long) As Events
        DespliegaDatos = oRepartosDetalle.sp_repartos_detalle_sel(reparto, partida)
    End Function

    Public Function Listado() As Events
        Listado = oRepartosDetalle.sp_repartos_detalle_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal banSerie As Boolean, ByVal serie As String, ByVal articulo As Long, ByVal insertarhis_series As Boolean) As Events
        If banSerie = False Then Exit Function

        Validacion = ValidaSerie(banSerie, serie, articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerieHisSeries(insertarhis_series)
        If Validacion.ErrorFound Then Exit Function

    End Function


    Public Function Validacion(ByVal banSerie As Boolean, ByVal serie As String, ByVal articulo As Long, ByVal bSerieValida As Boolean) As Events
        If banSerie = False Then Exit Function

        Validacion = ValidaSerie(banSerie, serie, articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerieCapturada(bSerieValida)
        If Validacion.ErrorFound Then Exit Function

    End Function




    Public Function ValidaSerie(ByVal banserie As Boolean, ByVal serie As String, ByVal articulo As Long) As Events
        Dim oevents As New Events
        If banserie Then
            If serie.Length = 0 Then
                oevents.Message = "El Número de Serie del Artículo " & articulo & " es Requerido"
            End If
        End If
        Return oevents
    End Function

    Private Function ValidaSerieCapturada(ByVal bSerieValida As Boolean) As Events
        Dim oEvent As New Events

        If bSerieValida = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Número de Serie no se puede Insertar porque no es Válido"  '"El Movimiento del  Numero de Serie no se puede Insertar "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaSerieHisSeries(ByVal insertarhis_series As Boolean) As Events
        Dim oEvent As New Events

        If insertarhis_series = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Movimiento del  Numero de Serie no se puede Insertar porque no hay una Entrada"  '"El Movimiento del  Numero de Serie no se puede Insertar "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


    Public Function MostrarProductos(ByVal intReparto As Integer) As Events
        MostrarProductos = oRepartosDetalle.sp_repartos_productos_grs(intReparto)
    End Function

    '@ACH-22/06/07: Cree el método de RepartosServicio para obtener los artículos de la orden de reparto.
    Public Function RepartosServicio(ByVal intReparto As Integer) As Events
        RepartosServicio = oRepartosDetalle.sp_repartos_servicio_grs(intReparto)
    End Function
    '/@ACH-22/06/07
#End Region

End Class
#End Region


