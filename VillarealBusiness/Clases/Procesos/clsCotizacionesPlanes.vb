Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCotizacionesPlanes
'DATE:		31/08/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CotizacionesPlanes"
Public Class clsCotizacionesPlanes
    Private oCotizacionesPlanes As VillarrealData.clsCotizacionesPlanes

#Region "Constructores"
    Sub New()
        oCotizacionesPlanes = New VillarrealData.clsCotizacionesPlanes
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef cotizacion As Long, ByVal plan_credito As Long, ByVal enganche As Double, ByVal plazo As Double, ByVal precio_venta As Long) As Events
        Insertar = oCotizacionesPlanes.sp_cotizaciones_planes_ins(cotizacion, plan_credito, enganche, plazo, precio_venta)
    End Function

    Public Function Actualizar(ByRef cotizacion As Long, ByVal plan_credito As Long, ByVal enganche As Double, ByVal plazo As Double, ByVal precio_venta As Long) As Events
        Actualizar = oCotizacionesPlanes.sp_cotizaciones_planes_upd(cotizacion, plan_credito, enganche, plazo, precio_venta)
    End Function

    Public Function Eliminar(ByVal cotizacion As Long, ByVal plan_credito As Long) As Events
        Eliminar = oCotizacionesPlanes.sp_cotizaciones_planes_del(cotizacion, plan_credito)
    End Function

    Public Function DespliegaDatos(ByVal cotizacion As Long, ByVal plan_credito As Long) As Events
        DespliegaDatos = oCotizacionesPlanes.sp_cotizaciones_planes_sel(cotizacion, plan_credito)
    End Function

    Public Function Listado(ByVal cotizacion As Long) As Events
        Listado = oCotizacionesPlanes.sp_cotizaciones_planes_grs(cotizacion)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Plan_Credito As Long, ByVal oDetalle As DataSet) As Events
        Validacion = ValidaPlanCredito(Plan_Credito)
        If Validacion.ErrorFound Then Exit Function
        If Action = Actions.Insert Then
            Validacion = ValidaExistePlan(CStr(Plan_Credito), "plan_credito", oDetalle)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function

    Public Function ValidaPlanCredito(ByVal Plan As Long) As Events
        Dim oEvent As New Events
        If Plan <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Plan de Cr�dito es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaExistePlan(ByVal clave As String, ByVal Campo As String, ByVal oDetalle As DataSet) As Events
        Return ValidaExistePartida(clave, Campo, oDetalle)
    End Function

    Private Function ValidaExistePartida(ByVal Clave As String, ByVal Campo As String, ByRef oDetalle As DataSet) As Events
        Dim oEvent As New Events
        If oDetalle.Tables(0).Rows.Count > 0 Then
            Dim dv As New DataView
            dv.Table = oDetalle.Tables(0)
            dv.RowFilter = Campo & "='" & Clave.ToString & "' and control in (0,1,2)"
            If dv.Count > 0 Then
                oEvent.Message = "El Plan ya Existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
            dv = Nothing
        End If

        Return oEvent
    End Function
#End Region

End Class
#End Region


