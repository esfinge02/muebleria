Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsAnticiposFormasPago

    Private oAnticiposFormasPago As VillarrealData.clsAnticiposFormasPago

    Sub New()
        oAnticiposFormasPago = New VillarrealData.clsAnticiposFormasPago
    End Sub

    Public Function Insertar(ByVal folio As Long, ByRef partida As Long, ByVal forma_pago As Long, ByVal tipo_cambio As Double, ByVal dolares As Long, ByVal importe As Double, ByVal ultimos_digitos_cuenta As String) As Events
        Insertar = oAnticiposFormasPago.sp_anticipos_formas_pago_ins(folio, partida, forma_pago, tipo_cambio, dolares, importe, ultimos_digitos_cuenta)
    End Function

    Public Function Actualizar(ByVal folio As Long, ByVal partida As Long, ByVal forma_pago As Long, ByVal tipo_cambio As Double, ByVal dolares As Long, ByVal importe As Double, ByVal ultimos_digitos_cuenta As String) As Events
        Actualizar = oAnticiposFormasPago.sp_anticipos_formas_pago_upd(folio, partida, forma_pago, tipo_cambio, dolares, importe, ultimos_digitos_cuenta)
    End Function

    Public Function Eliminar(ByVal folio As Long, ByVal partida As Long) As Events
        Eliminar = oAnticiposFormasPago.sp_anticipos_formas_pago_del(folio, partida)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oAnticiposFormasPago.sp_anticipos_formas_pago_sel(folio)
    End Function
End Class
