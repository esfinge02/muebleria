Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientesJuridicoGastosCostas
'DATE:		30/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ClientesJuridicoGastosCostas"
Public Class clsClientesJuridicoGastosCostas
    Private oClientesJuridicoGastosCostas As VillarrealData.clsClientesJuridicoGastosCostas

#Region "Constructores"
    Sub New()
        oClientesJuridicoGastosCostas = New VillarrealData.clsClientesJuridicoGastosCostas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Insertar = oClientesJuridicoGastosCostas.sp_clientes_juridico_gastos_costas_ins(oData, folio_juridico)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Actualizar = oClientesJuridicoGastosCostas.sp_clientes_juridico_gastos_costas_upd(oData, folio_juridico)
    End Function

    Public Function Eliminar(ByVal folio_juridico As Long, ByVal fecha As Date) As Events
        Eliminar = oClientesJuridicoGastosCostas.sp_clientes_juridico_gastos_costas_del(folio_juridico, fecha)
    End Function

    Public Function DespliegaDatos(ByVal folio_juridico As Long, ByVal fecha As Date) As Events
        DespliegaDatos = oClientesJuridicoGastosCostas.sp_clientes_juridico_gastos_costas_sel(folio_juridico, fecha)
    End Function

    Public Function Listado(ByVal folio_juridico As Long) As Events
        Listado = oClientesJuridicoGastosCostas.sp_clientes_juridico_gastos_costas_grs(folio_juridico)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Concepto As String, ByVal importe As Double) As Events
        Validacion = ValidaConcepto(Concepto)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaImporte(importe)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Private Function ValidaConcepto(ByVal Concepto As String) As Events
        Dim oEvent As New Events
        If Concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaImporte(ByVal Importe As Double) As Events
        Dim oEvent As New Events
        If Importe <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


