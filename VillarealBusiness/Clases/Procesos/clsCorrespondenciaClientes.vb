Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCorrespondenciaClientes
'DATE:		01/02/2007 00:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CorrespondenciaClientes"
Public Class clsCorrespondenciaClientes
    Private oCorrespondenciaClientes As VillarrealData.clsCorrespondenciaClientes

#Region "Constructores"
    Sub New()
        oCorrespondenciaClientes = New VillarrealData.clsCorrespondenciaClientes
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal texto_correspondencia As String) As Events
        Insertar = oCorrespondenciaClientes.sp_correspondencia_clientes_ins(oData, texto_correspondencia)
    End Function
    Public Function Actualizar(ByRef oData As DataSet, ByVal texto_correspondencia As String) As Events
        Actualizar = oCorrespondenciaClientes.sp_correspondencia_clientes_upd(oData, texto_correspondencia)
    End Function
    Public Function Eliminar(ByVal correspondencia As Long) As Events
        Eliminar = oCorrespondenciaClientes.sp_correspondencia_clientes_del(correspondencia)
    End Function
    Public Function DespliegaDatos(ByVal correspondencia As Long) As Events
        DespliegaDatos = oCorrespondenciaClientes.sp_correspondencia_clientes_sel(correspondencia)
    End Function
    Public Function Listado() As Events
        Listado = oCorrespondenciaClientes.sp_correspondencia_clientes_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oCorrespondenciaClientes.sp_correspondencia_clientes_lkp()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal descripcion As String, ByVal texto_correspondencia As String) As Events
        Validacion = ValidaDescripcion(descripcion)
        If Validacion.ErrorFound = True Then Exit Function
        Validacion = ValidaTextoCorrespondencia(texto_correspondencia)
        If Validacion.ErrorFound = True Then Exit Function
    End Function

    Public Function Validacion(ByVal correspondencia As Long) As Events
        Validacion = ValidaCorrespondencia(correspondencia)
        If Validacion.ErrorFound = True Then Exit Function
    End Function

    Private Function ValidaCorrespondencia(ByVal correspondencia As Long) As Events
        Dim oEvent As New Events
        If correspondencia <= 0 Or IsNumeric(correspondencia) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Correspondencia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaDescripcion(ByVal descripcion As String) As Events
        Dim oEvent As New Events
        If descripcion.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Descripci�n es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaTextoCorrespondencia(ByVal texto_correspondencia As String) As Events
        Dim oEvent As New Events
        If texto_correspondencia.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Texto de la Correspondencia es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


