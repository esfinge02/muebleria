Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsSolicitudTraspasosDetalle
'DATE:		23/06/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - SolicitudTraspasosDetalle"
Public Class clsSolicitudTraspasosDetalle
	Private oSolicitudTraspasosDetalle As VillarrealData.clsSolicitudTraspasosDetalle

#Region "Constructores"
	Sub New()
		oSolicitudTraspasosDetalle = New VillarrealData.clsSolicitudTraspasosDetalle
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal solicitud As Long) As Events
        Insertar = oSolicitudTraspasosDetalle.sp_solicitud_traspasos_detalle_ins(oData, solicitud)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal solicitud As Long) As Events
        Actualizar = oSolicitudTraspasosDetalle.sp_solicitud_traspasos_detalle_upd(oData, solicitud)
    End Function

    Public Function Eliminar(ByVal solicitud As Long, ByVal articulo As Long) As Events
        Eliminar = oSolicitudTraspasosDetalle.sp_solicitud_traspasos_detalle_del(solicitud, articulo)
    End Function

    Public Function DespliegaDatos(ByVal solicitud As Long, ByVal articulo As Long) As Events
        DespliegaDatos = oSolicitudTraspasosDetalle.sp_solicitud_traspasos_detalle_sel(solicitud, articulo)
    End Function

    Public Function Listado(ByVal solicitud As Long) As Events
        Listado = oSolicitudTraspasosDetalle.sp_solicitud_traspasos_detalle_grs(solicitud)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal articulo As Long, ByVal cantidad As Long, ByVal oDetalle As DataSet) As Events
        Validacion = ValidaArticulo(articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(cantidad)
        If Validacion.ErrorFound Then Exit Function

        If Action = Actions.Insert Then
            Validacion = ValidaExisteArticulo(CStr(articulo), "articulo", oDetalle)
            If Validacion.ErrorFound Then Exit Function
        End If

    End Function

    Public Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Artículo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCantidad(ByVal Cantidad As Long) As Events
        Dim oEvent As New Events
        If Cantidad <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cantidad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaExisteArticulo(ByVal clave As String, ByVal Campo As String, ByVal oDetalle As DataSet) As Events
        Return ValidaExistePartida(clave, Campo, oDetalle)
    End Function

    Private Function ValidaExistePartida(ByVal Clave As String, ByVal Campo As String, ByRef oDetalle As DataSet) As Events
        Dim oEvent As New Events
        If oDetalle.Tables(0).Rows.Count > 0 Then
            Dim dv As New DataView
            dv.Table = oDetalle.Tables(0)
            dv.RowFilter = Campo & "='" & Clave.ToString & "' and control in (0,1,2)"
            If dv.Count > 0 Then
                oEvent.Message = "El Articulo ya Existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
            dv = Nothing
        End If

        Return oEvent
    End Function

#End Region

End Class
#End Region


