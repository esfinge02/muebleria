Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEnviosConvenios
'DATE:		06/11/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - EnviosConvenios"
Public Class clsEnviosConvenios
    Private oEnviosConvenios As VillarrealData.clsEnviosConvenios

#Region "Constructores"
    Sub New()
        oEnviosConvenios = New VillarrealData.clsEnviosConvenios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef sucursal As Long, ByRef dependencia As Long, ByRef quincena As Long, ByRef anio As Long, ByRef cliente As Long, ByRef importe As Long, ByRef tipo As Char, ByRef cancelado As Boolean, ByRef fecha_cancelacion As DateTime, ByRef usuario_cancelacion As Integer) As Events
        Insertar = oEnviosConvenios.sp_envios_convenios_ins(sucursal, dependencia, quincena, anio, cliente, importe, tipo, cancelado, fecha_cancelacion, usuario_cancelacion)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oEnviosConvenios.sp_envios_convenios_upd(oData)
    End Function

    Public Function Eliminar(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long) As Events
        Eliminar = oEnviosConvenios.sp_envios_convenios_del(sucursal, convenio, quincena_envio, anio_envio, cliente)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long) As Events
        DespliegaDatos = oEnviosConvenios.sp_envios_convenios_sel(sucursal, convenio, quincena_envio, anio_envio, cliente)
    End Function

    Public Function Listado() As Events
        Listado = oEnviosConvenios.sp_envios_convenios_grs()
    End Function

    Public Function ActualizaFechaEnvios(ByVal sucursal As Long, ByVal anio_envio As Long) As Events
        ActualizaFechaEnvios = oEnviosConvenios.sp_actualiza_fechas_envios(sucursal, anio_envio)
    End Function


    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


