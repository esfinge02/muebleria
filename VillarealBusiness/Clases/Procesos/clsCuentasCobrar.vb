Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCuentasCobrar
'DATE:		10/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CuentasCobrar"
Public Class clsCuentasCobrar
    '    Private oCuentasCobrar As VillarrealData.clsCuentasCobrar

    '#Region "Constructores"
    '    Sub New()
    '        oCuentasCobrar = New VillarrealData.clsCuentasCobrar
    '    End Sub
    '#End Region

    '#Region "Propiedades"
    '#End Region

    '#Region "M�todos"
    '    Public Function Insertar(ByRef oData As DataSet) As Events
    '        Insertar = oCuentasCobrar.sp_cuentas_cobrar_ins(oData)
    '    End Function
    '    Public Function Insertar(ByVal sucursal As Long, ByRef folio As Long, ByVal serie As String, ByVal cliente As Long, ByVal concepto As String, ByVal fecha As Date, ByVal tipo_pago As String, ByVal monto As Double, ByVal saldo As Double, ByVal numero_documentos As Long, ByVal fecha_cancelacion As Date, ByVal observaciones As String, ByVal status As String) As Events
    '        Insertar = oCuentasCobrar.sp_cuentas_cobrar_ins(sucursal, folio, serie, cliente, concepto, fecha, tipo_pago, monto, saldo, numero_documentos, fecha_cancelacion, observaciones, status)
    '    End Function

    '    Public Function Actualizar(ByVal sucursal As Long, ByRef folio As Long, ByVal serie As String, ByVal cliente As Long, ByVal concepto As String, ByVal fecha As Date, ByVal tipo_pago As String, ByVal monto As Double, ByVal saldo As Double, ByVal numero_documentos As Long, ByVal fecha_cancelacion As Date, ByVal observaciones As String, ByVal status As String) As Events
    '        Actualizar = oCuentasCobrar.sp_cuentas_cobrar_upd(sucursal, folio, serie, cliente, concepto, fecha, tipo_pago, monto, saldo, numero_documentos, fecha_cancelacion, observaciones, status)
    '    End Function

    '    Public Function Actualizar(ByRef oData As DataSet) As Events
    '        Actualizar = oCuentasCobrar.sp_cuentas_cobrar_upd(oData)
    '    End Function

    '    Public Function Eliminar(ByVal sucursal As Long, ByVal folio As Long, ByVal serie As String, ByVal cliente As Long) As Events
    '        Eliminar = oCuentasCobrar.sp_cuentas_cobrar_del(sucursal, folio, serie, cliente)
    '    End Function

    '    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal folio As Long, ByVal serie As String, ByVal cliente As Long) As Events
    '        DespliegaDatos = oCuentasCobrar.sp_cuentas_cobrar_sel(sucursal, folio, serie, cliente)
    '    End Function

    '    Public Function Listado() As Events
    '        Listado = oCuentasCobrar.sp_cuentas_cobrar_grs()
    '    End Function

    '    Public Function Validacion(ByVal Action As Actions) As Events

    '    End Function



    '#End Region

End Class
#End Region


