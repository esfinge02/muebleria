Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsDescuentosEspecialesProgramados
    Private oDescuentosEspecialesProgramados As VillarrealData.clsDescuentosEspecialesProgramados

#Region "Constructores"
    Sub New()
        oDescuentosEspecialesProgramados = New VillarrealData.clsDescuentosEspecialesProgramados
    End Sub
#End Region


#Region "M�todos"


    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oDescuentosEspecialesProgramados.sp_descuentos_especiales_programados_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oDescuentosEspecialesProgramados.sp_descuentos_especiales_programados_upd(oData)
    End Function

    Public Function Eliminar(ByVal oData As DataSet) As Events
        Eliminar = oDescuentosEspecialesProgramados.sp_descuentos_especiales_programados_del(oData)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oDescuentosEspecialesProgramados.sp_descuentos_especiales_programados_sel(folio)
    End Function

    Public Function Listado() As Events
        Listado = oDescuentosEspecialesProgramados.sp_descuentos_especiales_programados_grs
    End Function

    Public Function DespliegaDatosVentas(ByVal articulo As Long, ByVal plan_credito As Long, ByVal fecha_venta As DateTime) As DataSet
        DespliegaDatosVentas = oDescuentosEspecialesProgramados.sp_descuentos_especiales_programados_ventas_sel(articulo, plan_credito, fecha_venta).Value
    End Function


    Public Function Validacion(ByVal action As Actions, ByVal articulo As Long, ByVal Plan_Credito As Long, ByVal fecha_inicio As String, ByVal fecha_final As String, ByVal odata As DataSet) As Events
        Validacion = ValidaArticulo(articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPlanCredito(Plan_Credito)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFechas(fecha_inicio, fecha_final)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = validaDescuento(descuento)
        'If Validacion.ErrorFound Then Exit Function

        If action = Actions.Insert Then
            Validacion = ValidaRegistro(odata)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function
    Private Function validaDescuento(ByVal descuento As Double) As Events
        Dim oEvent As New Events
        If descuento = 0 Then
            oEvent.Message = "El Descuento es Requerido"
        Else
            If descuento > 100 Then
                oEvent.Message = "El Descuento no puede ser mayor al 100 %"
            End If
        End If

        Return oEvent
    End Function
    Private Function ValidaFechas(ByVal fecha_inicio As String, ByVal fecha_final As String) As Events
        Dim oEvent As New Events
        If Not IsDate(fecha_inicio) Then
            oEvent.Message = "La Fecha Inicio es Requerida"
        Else
            If Not IsDate(fecha_final) Then
                oEvent.Message = "La Fecha Final es Requerida"
            Else
                If CDate(fecha_final) < CDate(fecha_inicio) Then
                    oEvent.Message = "La Fecha Final es Menor que la Inicial"
                End If
            End If
        End If
        Return oEvent

    End Function
    Private Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaPlanCredito(ByVal Plan_credito As Long) As Events
        Dim oEvent As New Events
        If Plan_credito < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Plan de Cr�dito es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaRegistro(ByVal odata As DataSet) As Events
        Dim response As Events = Me.oDescuentosEspecialesProgramados.sp_descuentos_especiales_programados_valida_exs(odata)

        If Not response.ErrorFound Then
            Dim resultado As Object = response.Value

            If resultado > 0 Then
                response.Message = "Existe una promoci�n vigente para el articulo a ese plan de cr�dito"
            End If
        End If

        Return response
    End Function

#End Region

End Class
