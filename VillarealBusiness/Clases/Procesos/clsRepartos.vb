Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsRepartos
'DATE:		03/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Repartos"
Public Class clsRepartos
	Private oRepartos As VillarrealData.clsRepartos

#Region "Constructores"
	Sub New()
		oRepartos = New VillarrealData.clsRepartos
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByVal oData As DataSet, ByVal fecha As String, ByRef reparto As Long, ByVal sucursal_actual As Long, ByVal status As String, ByVal sucursal_factura As Long, ByVal factura As Long) As Events
        Insertar = oRepartos.sp_repartos_ins(oData, fecha, reparto, sucursal_actual, status, sucursal_factura, factura)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oRepartos.sp_repartos_upd(oData)
    End Function

    Public Function Eliminar(ByVal sucursal As Long, ByVal reparto As Long) As Events
        Eliminar = oRepartos.sp_repartos_del(sucursal, reparto)
    End Function

    Public Function DespliegaDatos(ByVal reparto As Long) As Events
        DespliegaDatos = oRepartos.sp_repartos_sel(reparto)
    End Function

    Public Function Listado() As Events
        Listado = oRepartos.sp_repartos_grs()
    End Function

    Public Function Lookup(ByVal sucursal_actual As Long, Optional ByVal reparto As Long = -1, Optional ByVal cliente As Long = -1, Optional ByVal AnioActual As Boolean = True) As Events
        Lookup = oRepartos.sp_repartos_grl(reparto, cliente, AnioActual, sucursal_actual)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal SucursalFactura As Long, ByVal Fecha As String, ByVal Factura As String, ByVal Repartir As Long, ByVal Chofer As Long, ByVal bodeguero As Long, ByVal oArticulos As DataTable, ByVal bodegaEntrega As String) As Events
        Validacion = ValidaSucursal(SucursalFactura)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(Fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFactura(Factura)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaRepartir(Repartir)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaChofer(Chofer)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodeguero(bodeguero)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulos(oArticulos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodegaEntrega(bodegaEntrega)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal SucursalFactura As Long, ByVal Fecha As String, ByVal Factura As String, ByVal Repartir As Long, ByVal bodeguero As Long, ByVal oArticulos As DataTable, ByVal bodegaEntrega As String) As Events
        Validacion = ValidaSucursal(SucursalFactura)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(Fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFactura(Factura)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaRepartir(Repartir)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodeguero(bodeguero)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulos(oArticulos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodegaEntrega(bodegaEntrega)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaSucursal(ByVal SucursalFactura As Long) As Events
        Dim oEvent As New Events
        If SucursalFactura <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFecha(ByVal Fecha As String) As Events
        Dim oEvent As New Events
        If Fecha.Trim.Length = 0 Or IsDate(Fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(Fecha).Year < 1753 Or CDate(Fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha esta fuera del rango válido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Public Function ValidaFactura(ByVal Factura As String) As Events
        Dim oEvent As New Events
        If Factura.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Factura es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaRepartir(ByVal Repartir As Long) As Events
        Dim oEvent As New Events
        If Repartir <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Campo Repartir es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaChofer(ByVal Chofer As Long) As Events
        Dim oEvent As New Events
        If Chofer <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Chofer es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaBodeguero(ByVal Bodeguero As Long) As Events
        Dim oEvent As New Events
        If Bodeguero <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Bodeguero es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaArticulos(ByVal odata As DataTable) As Events
        Dim oEvent As New Events
        Dim row() As DataRow


        row = odata.Select("repartir = True")

        If row.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Articulo se debe Repartir"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function

    Public Function ValidaBodegaEntrega(ByVal BodegaEntrega As String) As Events
        Dim oEvent As New Events
        If BodegaEntrega.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega de Entrega es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    'Private Function ValidaNumerosSerie(ByVal odata As DataTable) As Events
    '    Dim oEvent As New Events

    '    If row.Length = 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "Al menos un Articulo se debe Repartir"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '    End If
    '    Return oEvent
    'End Function

    Public Function EliminarReparto(ByVal reparto As Long) As Events
        EliminarReparto = oRepartos.sp_elimina_reparto(reparto)

    End Function

    Public Function CerrarReparto(ByVal reparto As Long, ByVal fecha_entrega As Date, ByVal recibio As String, ByVal sucursal_actual As Long) As Events
        CerrarReparto = oRepartos.sp_cerrar_reparto(reparto, fecha_entrega, recibio, sucursal_actual)
    End Function


    Public Function CierreRepartos(ByVal sucursal As Long, ByVal fecha As DateTime) As Events
        CierreRepartos = oRepartos.sp_repartos_cierre_grs(sucursal, fecha)
    End Function

#End Region

End Class
#End Region


