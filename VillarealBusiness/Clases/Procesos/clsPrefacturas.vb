
Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsPrefacturas
    Private oPrefacturas As VillarrealData.clsPrefacturas

#Region "Constructores"
    Sub New()
        oPrefacturas = New VillarrealData.clsPrefacturas
    End Sub
#End Region


#Region "M�todos"

    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal fecha As DateTime, ByVal punto_venta As Long, Optional ByVal importe As Double = 0, Optional ByVal menos As Double = 0, Optional ByVal enganche As Double = 0, _
Optional ByVal interes As Double = 0, Optional ByVal plan As Long = 0, Optional ByVal fecha_primer_documento As String = "01/01/1900", Optional ByVal numero_documentos As Long = 0, Optional ByVal importe_documentos As Double = 0, Optional ByVal importe_ultimo_documento As Double = 0, _
Optional ByVal enganche_en_documento As Boolean = False, Optional ByVal fecha_enganche_documento As String = "01/01/1900", Optional ByVal importe_enganche_documento As Double = 0, Optional ByVal liquida_vencimiento As Boolean = False, Optional ByVal monto_liquida_vencimiento As Double = 0, _
Optional ByVal enganche_en_menos As Boolean = False, Optional ByVal sucursal_dependencia As Boolean = False, Optional ByVal folio_plantilla As Long = 0, Optional ByVal quincena_inicio As Long = -1, Optional ByVal ano_inicio As Long = -1, Optional ByVal cotizacion As Long = 0, Optional ByVal FolioDescuentoCliente As Long = -1, _
Optional ByVal aplica_enganche As Boolean = False, Optional ByVal paquete As Long = -1, Optional ByVal convenio As Long = -1, Optional ByVal cancelacion_automatica As Boolean = True) As Events
        Insertar = oPrefacturas.sp_prefacturas_ins(oData, sucursal, fecha, importe, menos, enganche, interes, plan, CDate(fecha_primer_documento).Date, numero_documentos, importe_documentos, importe_ultimo_documento, enganche_en_documento, CDate(fecha_enganche_documento).Date, importe_enganche_documento, liquida_vencimiento, monto_liquida_vencimiento, enganche_en_menos, sucursal_dependencia, folio_plantilla, quincena_inicio, ano_inicio, punto_venta, FolioDescuentoCliente, aplica_enganche, paquete, convenio, cancelacion_automatica)
    End Function


    Public Function DespliegaDatos(ByVal folio_prefactura As Long) As Events
        DespliegaDatos = oPrefacturas.sp_prefacturas_sel(folio_prefactura)
    End Function

    Public Function Listado(ByVal sucursal As Long, ByVal fecha As DateTime) As Events
        'Listado = oprefacturas.
    End Function

    Public Function TraerArticulosDescuentosClienteVentas(ByVal folio_descuento As Long) As Events
        Dim oVentas As New VillarrealBusiness.clsVentas
        TraerArticulosDescuentosClienteVentas = oVentas.TraerArticulosDescuentosClienteVentas(folio_descuento)
        oVentas = Nothing

    End Function

    Public Function TraerArticulosPaquetesVentas(ByVal paquete As Long) As Events
        Dim oVentas As New VillarrealBusiness.clsVentas
        TraerArticulosPaquetesVentas = oVentas.TraerArticulosPaquetesVentas(paquete)
        oVentas = Nothing

    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Vendedor As Long, ByVal Cliente As Long, ByVal rfc As String, ByVal NumArticulos As Long, ByVal sucursal_dependencia As Boolean, ByVal folio_pedido As Long) As Events


        Validacion = ValidaVendedor(Vendedor)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCliente(Cliente)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaRFC(rfc)
        'If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaNumeroArticulos(NumArticulos)
        If Validacion.ErrorFound Then Exit Function

        If sucursal_dependencia = False Then Exit Function
        Validacion = ValidaFolioPedido(folio_pedido)
        If Validacion.ErrorFound Then Exit Function


    End Function

    Public Function ValidaFolioPedido(ByVal folio_pedido As Long) As Events
        Dim oEvent As New Events
        If folio_pedido <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio de Pedido es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaNumeroArticulos(ByVal NumArticulos As Long) As Events
        Dim oEvent As New Events
        If NumArticulos < 1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Por lo menos debe existir un Art�culo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaVendedor(ByVal Vendedor As Long) As Events
        Dim oEvent As New Events
        If Vendedor = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Vendedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaCliente(ByVal Cliente As Long) As Events
        Dim oEvent As New Events
        If Cliente = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region





End Class
