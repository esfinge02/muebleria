Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports VillarrealBusiness.clsUtilerias

Public Class clsDescuentosMasivosMegaCred

    Private oDescuentosMasivosMegaCred As VillarrealData.clsDescuentosMasivosMegaCred

#Region "Constructores"
    Sub New()
        oDescuentosMasivosMegaCred = New VillarrealData.clsDescuentosMasivosMegaCred
    End Sub
#End Region

    Public Function Insertar(ByVal convenio As Long, ByVal cliente As Long, ByVal sucursal As Long, ByVal importe As Double) As Events
        Insertar = oDescuentosMasivosMegaCred.sp_procesar_abonos_megacred_ins(convenio, cliente, sucursal, importe)
    End Function

    Public Function Eliminar() As Events
        Eliminar = oDescuentosMasivosMegaCred.sp_procesar_abonos_megacred_del()
    End Function

    Public Function ProcesarAbonosMegaCred(ByVal caja As Long, ByVal cajero As Long, ByVal forma_pago As Long, ByVal digitos As String) As Events
        ProcesarAbonosMegaCred = Me.oDescuentosMasivosMegaCred.sp_procesar_abonos_mega_cred(caja, cajero, forma_pago, digitos)
    End Function


    Public Function Validacion(ByVal FormaPago As Long, ByVal solicita_ultimos_digitos As Boolean, ByVal ultimos_digitos As String, ByVal ArchivoValido As Boolean, ByVal Convenio As Long, ByVal Cajero As Long) As Events
        Validacion = ValidaFormaPago(FormaPago)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaCajero(Cajero)
        If Validacion.ErrorFound Then Exit Function

        If solicita_ultimos_digitos Then
            Validacion = ValidaUltimosDigitos(ultimos_digitos)
            If Validacion.ErrorFound Then Exit Function
        End If

        Validacion = ValidaArchivoValido(ArchivoValido)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaConvenio(ByVal convenio As Long) As Events
        Dim oEvent As New Events
        If convenio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Convenio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaCajero(ByVal Cajero As Long) As Events
        Dim oEvent As New Events
        If Cajero <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cajero es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


    Private Function ValidaArchivoValido(ByVal ArchivoValido As Boolean) As Events
        Dim oEvent As New Events
        If Not ArchivoValido Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Archivo no cumple con las Validaciones Correspondientes"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
End Class
