Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesServicioDetalle
'DATE:		02/06/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - OrdenesServicioDetalle"
Public Class clsOrdenesServicioDetalle
	Private oOrdenesServicioDetalle As VillarrealData.clsOrdenesServicioDetalle

#Region "Constructores"
	Sub New()
		oOrdenesServicioDetalle = New VillarrealData.clsOrdenesServicioDetalle
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet, orden_servicio as Long ) As Events
        Insertar = oOrdenesServicioDetalle.sp_ordenes_servicio_detalle_ins(oData, orden_servicio)
	End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal orden_servicio As Long) As Events
        Actualizar = oOrdenesServicioDetalle.sp_ordenes_servicio_detalle_upd(oData, orden_servicio)
    End Function

    Public Function Eliminar(ByVal orden_servicio As Long, ByVal partida As Long) As Events
        Eliminar = oOrdenesServicioDetalle.sp_ordenes_servicio_detalle_del(orden_servicio, partida)
    End Function

    Public Function DespliegaDatos(ByVal orden_servicio As Long, ByVal partida As Long) As Events
        DespliegaDatos = oOrdenesServicioDetalle.sp_ordenes_servicio_detalle_sel(orden_servicio, partida)
    End Function

    Public Function Listado(ByVal orden_servicio As Long) As Events
        Listado = oOrdenesServicioDetalle.sp_ordenes_servicio_detalle_grs(orden_servicio)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal fecha As String, ByVal fecha_promesa As String, ByVal capturo As String, ByVal persona As String, ByVal observaciones As String) As Events
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha_promesa)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCapturo(capturo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPersona(persona)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaObservaciones(observaciones)
        If Validacion.ErrorFound Then Exit Function

    End Function

    Public Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events

        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha esta fuera del rango válido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function

    Public Function ValidaCapturo(ByVal capturo As String) As Events
        Dim oEvent As New Events

        If capturo.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Persona que Capturó es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaPersona(ByVal persona As String) As Events
        Dim oEvent As New Events

        If persona.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Persona es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaObservaciones(ByVal observaciones As String) As Events
        Dim oEvent As New Events

        If observaciones.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Las Observaciones son Requeridas"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


