Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVistasEntradasDetalle
'DATE:		28/12/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - VistasEntradasDetalle"
Public Class clsVistasEntradasDetalle
    Private oVistasEntradasDetalle As VillarrealData.clsVistasEntradasDetalle

#Region "Constructores"
    Sub New()
        oVistasEntradasDetalle = New VillarrealData.clsVistasEntradasDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal folio_vista_entrada As Long, ByVal partida As Long, ByVal articulo As Long, ByVal numero_serie As String, ByVal bodega_entrada As String, ByVal concepto_inventario_entrada As String, ByVal folio_inventario_entrada As Long) As Events
        Insertar = oVistasEntradasDetalle.sp_vistas_entradas_detalle_ins(folio_vista_entrada, partida, articulo, numero_serie, bodega_entrada, concepto_inventario_entrada, folio_inventario_entrada)
    End Function

    Public Function Actualizar(ByVal folio_vista_entrada As Long, ByVal partida As Long, ByVal articulo As Long, ByVal numero_serie As String, ByVal bodega_entrada As String, ByVal concepto_inventario_entrada As String, ByVal folio_inventario_entrada As Long) As Events
        Actualizar = oVistasEntradasDetalle.sp_vistas_entradas_detalle_upd(folio_vista_entrada, partida, articulo, numero_serie, bodega_entrada, concepto_inventario_entrada, folio_inventario_entrada)
    End Function

    Public Function Eliminar(ByVal folio_vista_entrada As Long, ByVal partida As Long) As Events
        Eliminar = oVistasEntradasDetalle.sp_vistas_entradas_detalle_del(folio_vista_entrada, partida)
    End Function

    Public Function DespliegaDatos(ByVal folio_vista_entrada As Long) As Events
        DespliegaDatos = oVistasEntradasDetalle.sp_vistas_entradas_detalle_sel(folio_vista_entrada)
    End Function

    Public Function Listado() As Events
        Listado = oVistasEntradasDetalle.sp_vistas_entradas_detalle_grs()
    End Function

    Public Function ArticulosSinEntregarFacturarVistas(ByVal folio_vista_salida As Long) As Events
        ArticulosSinEntregarFacturarVistas = oVistasEntradasDetalle.sp_vistas_entradas_articulos_sin_entregar_facturar(folio_vista_salida)
    End Function

    Public Function ArticulosSinEntregarFacturarPtoVenta(ByVal folio_vista_salida As Long) As Events
        ArticulosSinEntregarFacturarPtoVenta = oVistasEntradasDetalle.sp_vistas_entradas_articulos_sin_entregar_facturar_pto_venta(folio_vista_salida)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


