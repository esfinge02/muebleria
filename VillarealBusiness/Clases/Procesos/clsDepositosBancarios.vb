Imports Dipros.Data
Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports VillarrealBusiness

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDepositosBancarios
'DATE:		30/06/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - DepositosBancarios"
Public Class clsDepositosBancarios
    Private oDepositosBancarios As New VillarrealData.clsDepositosBancarios

#Region "Constructores"
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oDepositosBancarios.sp_depositos_bancarios_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oDepositosBancarios.sp_depositos_bancarios_upd(oData)
    End Function
    Public Function Actualizar_Aplicado(ByVal sucursal As Long, ByVal fecha As DateTime, ByVal consecutivo As Long, ByVal folio As Long) As Events
        Actualizar_Aplicado = oDepositosBancarios.sp_depositos_bancarios_aplicado_upd(sucursal, fecha, consecutivo, folio)
    End Function

    Public Function Eliminar(ByVal sucursal As Long, ByVal fecha As Date, ByVal consecutivo As Long) As Events
        Eliminar = oDepositosBancarios.sp_depositos_bancarios_del(sucursal, fecha, consecutivo)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal fecha As Date, ByVal consecutivo As Long) As Events
        DespliegaDatos = oDepositosBancarios.sp_depositos_bancarios_sel(sucursal, fecha, consecutivo)
    End Function
    Public Function Listado(ByVal fecha As DateTime, ByVal intAplicados As Long, Optional ByVal sucursal As Long = -1) As Events
        Listado = oDepositosBancarios.sp_depositos_bancarios_grs(sucursal, fecha, intAplicados)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal UsuarioActual As String, ByVal UsuarioInserto As String, ByVal Banco As Long, ByVal Chequera As String, ByVal Depositante As String, ByVal Importe As Double, ByRef intControl As Long) As Events
        If Action = Actions.Update Or Action = Actions.Delete Then
            Validacion = ValidaUsuario(UsuarioActual, UsuarioInserto)
            If Validacion.ErrorFound Then Exit Function
        End If
        Validacion = ValidaBanco(Banco)
        If Validacion.ErrorFound Then
            intControl = 1
            Exit Function
        End If
        Validacion = ValidaChequera(Chequera)
        If Validacion.ErrorFound Then
            intControl = 2
            Exit Function
        End If
        Validacion = ValidaDepositante(Depositante)
        If Validacion.ErrorFound Then
            intControl = 3
            Exit Function
        End If
        Validacion = ValidaImporte(Importe)
        If Validacion.ErrorFound Then
            intControl = 4
            Exit Function
        End If
    End Function
    Public Function ValidaUsuario(ByVal UsuarioActual As String, ByVal UsuarioInserto As String) As Events
        Dim oEvent As New Events

        If Not UsuarioActual.ToUpper.Equals(UsuarioInserto.ToUpper) Then
            oEvent.Ex = Nothing
            oEvent.Message = UsuarioActual + ": Usted no es el usuario quien ingresó este depósito. Por lo tanto, no puede modificarlo, ni eliminarlo."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaBanco(ByVal Banco As Long) As Events
        Dim oEvent As New Events
        If Banco <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El nombre del Banco es requerido. Por favor seleccione un banco."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaChequera(ByVal Chequera As String) As Events
        Dim oEvent As New Events
        If Chequera.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El número de la Cuenta Bancaria de la Chequera es requerido. Por favor seleccione una chequera."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDepositante(ByVal Depositante As String) As Events
        Dim oEvent As New Events
        If Depositante.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El nombre del Depositante es requerido. Por favor, escriba el nombre de la persona que realizó el depósito."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaImporte(ByVal Importe As Double) As Events
        Dim oEvent As New Events
        If Importe <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe del Depósito es requerido. Por favor, ingrese un valor válido en dicho campo."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


