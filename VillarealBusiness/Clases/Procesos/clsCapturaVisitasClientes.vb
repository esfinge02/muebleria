Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCapturaVisitasClientes
'DATE:		29/05/2008 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CapturaVisitasClientes"
Public Class clsCapturaVisitasClientes
    Private oCapturaVisitasClientes As VillarrealData.clsCapturaVisitasClientes

#Region "Constructores"
    Sub New()
        oCapturaVisitasClientes = New VillarrealData.clsCapturaVisitasClientes
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oCapturaVisitasClientes.sp_captura_visitas_clientes_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oCapturaVisitasClientes.sp_captura_visitas_clientes_upd(oData)
    End Function

    Public Function Eliminar(ByVal folio_captura As Long) As Events
        Eliminar = oCapturaVisitasClientes.sp_captura_visitas_clientes_del(folio_captura)
    End Function

    Public Function DespliegaDatos(ByVal folio_captura As Long) As Events
        DespliegaDatos = oCapturaVisitasClientes.sp_captura_visitas_clientes_sel(folio_captura)
    End Function

    'Private Function Existe(ByVal oData As DataSet) As Events
    '    Existe = oCapturaVisitasClientes.sp_captura_visitas_clientes_exs(oData)
    'End Function


    Public Function Listado(ByVal cobrador As Long, ByVal fecha_corte As DateTime, ByVal sucursal As Long) As Events
        Listado = oCapturaVisitasClientes.sp_captura_visitas_clientes_grs(cobrador, fecha_corte, sucursal)
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Cobrador As Long, ByVal Cliente As Long, ByVal fecha As String, ByVal odata As DataSet) As Events
        Validacion = ValidaCobrador(Cobrador)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCliente(Cliente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function

        If Action <> Actions.Delete Then
            Validacion = ValidaRegistro(odata)
            If Validacion.ErrorFound Then Exit Function
        End If

    End Function

    Public Function ValidaCobrador(ByVal Cobrador As Long) As Events
        Dim oEvent As New Events
        If Cobrador < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cobrador es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCliente(ByVal Cliente As Long) As Events
        Dim oEvent As New Events
        If Cliente < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events
        Dim tipo_fecha As String
        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + " es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha " + " esta fuera del rango v�lido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaRegistro(ByVal oData As DataSet) As Events
        Dim response As Events

        response = oCapturaVisitasClientes.sp_captura_visitas_clientes_exs(oData)
        If Not response.ErrorFound Then

            Dim orow As DataRow

            orow = CType(response.Value, DataSet).Tables(0).Rows(0)
            If orow.Item(0) > 0 Then
                response.Message = "El Registro ya existe para esa fecha de visita"
                response.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If

        Return response
    End Function

#End Region

End Class
#End Region


