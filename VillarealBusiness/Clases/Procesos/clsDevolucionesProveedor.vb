Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsDevolucionesProveedor
'DATE:		23/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - DevolucionesProveedor"
Public Class clsDevolucionesProveedor
    Private oDevolucionesProveedor As VillarrealData.clsDevolucionesProveedor

#Region "Constructores"
    Sub New()
        oDevolucionesProveedor = New VillarrealData.clsDevolucionesProveedor
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oDevolucionesProveedor.sp_devoluciones_proveedor_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oDevolucionesProveedor.sp_devoluciones_proveedor_upd(oData)
    End Function

    Public Function Eliminar(ByVal bodega As String, ByVal devolucion As Long) As Events
        Eliminar = oDevolucionesProveedor.sp_devoluciones_proveedor_del(bodega, devolucion)
    End Function

    Public Function DespliegaDatos(ByVal bodega As String, ByVal devolucion As Long) As Events
        DespliegaDatos = oDevolucionesProveedor.sp_devoluciones_proveedor_sel(bodega, devolucion)
    End Function

    Public Function Listado() As Events
        Listado = oDevolucionesProveedor.sp_devoluciones_proveedor_grs()
    End Function

    Public Function ActualizaOrdenServicio(ByVal orden_servicio As Long, ByVal folio_movimiento_entrada As Long) As Events
        ActualizaOrdenServicio = oDevolucionesProveedor.sp_devoluciones_proveedor_actualiza_orden_servicio(orden_servicio, folio_movimiento_entrada)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal bodega As String, ByVal Proveedor As Long, ByVal cantidad_articulos As Long) As Events
        Validacion = ValidaBodega(bodega)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaProveedor(Proveedor)
        If Validacion.ErrorFound Then Exit Function

        Validacion = ValidaArticulos(cantidad_articulos)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaBodega(ByVal Bodega As String) As Events
        Dim oEvent As New Events
        If Bodega.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaProveedor(ByVal Proveedor As Long) As Events
        Dim oEvent As New Events
        If Proveedor < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaArticulos(ByVal articulos As Long) As Events
        Dim oEvent As New Events
        If articulos = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function


    Public Function ActualizaEnviado(ByVal bodega As String, ByVal devolucion As Long) As Events
        ActualizaEnviado = oDevolucionesProveedor.sp_actualiza_enviado_devoluciones_proveedor(bodega, devolucion)

    End Function

    Public Function ValidaOrdenServicio(ByVal orden_servicio As Integer) As Events
        ValidaOrdenServicio = oDevolucionesProveedor.sp_orden_servicio_asignada_devolucion_ex(orden_servicio)
    End Function


    Public Function ActualizaNotaAplicada(ByVal Bodega As String, ByVal devolucion As Long, ByVal nota_aplicada As Boolean) As Events
        ActualizaNotaAplicada = oDevolucionesProveedor.sp_devoluciones_proveedor_actualiza_nota_aplicada(Bodega, devolucion, nota_aplicada)
    End Function

    Public Function ActualizaAplicadoInventario(ByVal Bodega As String, ByVal devolucion As Long, ByVal nota_aplicada As Boolean) As Events
        ActualizaAplicadoInventario = oDevolucionesProveedor.sp_devoluciones_proveedor_actualiza_aplicado_inventario(Bodega, devolucion, nota_aplicada)
    End Function

    Public Function ActualizaEnviadoProveedor(ByVal Bodega As String, ByVal devolucion As Long, ByVal enviado_proveedor As Boolean) As Events
        ActualizaEnviadoProveedor = oDevolucionesProveedor.sp_devoluciones_proveedor_actualiza_enviado_proveedor(Bodega, devolucion, enviado_proveedor)
    End Function

#End Region

End Class
#End Region



