Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosCobrar
'DATE:		19/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - MovimientosCobrar"
Public Class clsMovimientosCobrar
    Private oMovimientosCobrar As VillarrealData.clsMovimientosCobrar

#Region "Constructores"
    Sub New()
        oMovimientosCobrar = New VillarrealData.clsMovimientosCobrar
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    'Public Function Insertar(ByRef oData As DataSet, ByVal genera_comision_a_cobrador As Boolean, ByVal convenio As Long) As Events
    '    Insertar = oMovimientosCobrar.sp_movimientos_cobrar_ins(oData, genera_comision_a_cobrador, convenio)
    'End Function

    Public Function Insertar(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal partida As Long, _
    ByVal fecha As Date, ByVal caja As Long, ByVal cajero As Long, ByVal cobrador As Long, ByVal documentos As Long, ByVal cargo As Double, ByVal abono As Double, ByVal subtotal As Double, ByVal impuesto As Double, ByVal total As Double, ByVal saldo As Double, _
    ByVal fecha_cancelacion As Date, ByVal observaciones As String, ByVal estatus As String, ByVal genera_comision_a_cobrador As Boolean, ByVal convenio As Long) As Events
        Insertar = oMovimientosCobrar.sp_movimientos_cobrar_ins(sucursal, concepto, serie, folio, cliente, partida, _
    fecha, caja, cajero, cobrador, documentos, cargo, abono, subtotal, impuesto, total, saldo, _
    fecha_cancelacion, observaciones, estatus, genera_comision_a_cobrador, convenio)
    End Function

    Public Function InsertarCajas(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal cliente As Long, ByVal partida As Long, _
       ByVal fecha As Date, ByVal caja As Long, ByVal cajero As Object, ByVal cobrador As Object, ByVal documentos As Long, ByVal cargo As Double, ByVal abono As Double, ByVal subtotal As Double, ByVal impuesto As Double, ByVal total As Double, ByVal saldo As Double, _
       ByVal fecha_cancelacion As Object, ByVal observaciones As String, ByVal estatus As String, ByVal sucursal_actual As Long, ByRef folio As Long, Optional ByVal TieneIvaDesglosadoFacturaNotaCargo As Boolean = False, Optional ByVal convenio As Long = 0) As Events
        InsertarCajas = oMovimientosCobrar.sp_movimientos_cobrar_cajas_ins(sucursal, concepto, serie, cliente, partida, _
    fecha, caja, cajero, cobrador, documentos, cargo, abono, subtotal, impuesto, total, saldo, _
    fecha_cancelacion, observaciones, estatus, folio, sucursal_actual, TieneIvaDesglosadoFacturaNotaCargo, Convenio)
    End Function

    Public Function InsertarBonificacion(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As DateTime, ByVal cliente As Long, ByVal tipo As Char, ByVal importe As Long, ByVal observaciones As String, ByVal serie_recibo As String, ByVal folio_recibo As Long) As Events
        InsertarBonificacion = oMovimientosCobrar.sp_bonificaciones_ins(sucursal, caja, fecha, cliente, tipo, importe, observaciones, serie_recibo, folio_recibo)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal genera_comision_a_cobrador As Boolean) As Events
        Actualizar = oMovimientosCobrar.sp_movimientos_cobrar_upd(oData, genera_comision_a_cobrador)
    End Function

    Public Function Eliminar(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        Eliminar = oMovimientosCobrar.sp_movimientos_cobrar_del(sucursal, concepto, serie, folio, cliente)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        DespliegaDatos = oMovimientosCobrar.sp_movimientos_cobrar_sel(sucursal, concepto, serie, folio, cliente)
    End Function

    'Public Function DespliegaSaldoConMovReferencia(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
    '    DespliegaSaldoConMovReferencia = oMovimientosCobrar.sp_movimientos_cobrar_saldos_con_mov_referencia(sucursal, concepto, serie, folio, cliente)
    'End Function

    Public Function Existe(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        Existe = oMovimientosCobrar.sp_movimientos_cobrar_exs(sucursal, concepto, serie, folio, cliente)
    End Function

    Public Function DespliegaDatosVentas(ByVal sucursal As Long, ByVal cliente As Long, ByVal tipomovimiento As Char, ByVal fecha As Date, ByVal abono_proximo As Boolean) As Events
        DespliegaDatosVentas = oMovimientosCobrar.sp_movimientos_cobrar_ventas_sel(sucursal, cliente, tipomovimiento, fecha, abono_proximo)
    End Function

    Public Function MovimientosVentaCliente(ByVal cliente As Long) As Events
        MovimientosVentaCliente = oMovimientosCobrar.sp_ventas_movimientos_sel(cliente)
    End Function


    Public Function MovimientosVentaClienteFiltro(ByVal cliente As Long, ByVal saldo As Boolean, ByVal sucursal As Boolean, ByVal numero_sucursal As Long) As Events
        MovimientosVentaClienteFiltro = oMovimientosCobrar.sp_ventas_movimientos_filtro_sel(cliente, saldo, sucursal, numero_sucursal)
    End Function

    Public Function SaldosMovimientosCliente(ByVal cliente As Long, ByVal SaldoMayorCero As Boolean) As Events
        SaldosMovimientosCliente = oMovimientosCobrar.sp_saldos_movimientos_cliente(cliente, SaldoMayorCero)
    End Function

    Public Function MovimientosVentaClienteDetalle(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal sucursal_actual As Long) As Events
        MovimientosVentaClienteDetalle = oMovimientosCobrar.sp_ventas_movimientos_detalle_sel(sucursal, concepto, serie, folio, cliente, sucursal_actual)
    End Function

    Public Function Listado() As Events
        Listado = oMovimientosCobrar.sp_movimientos_cobrar_grs()
    End Function

    Public Function LookupNotaCredito(ByVal serie As String, ByVal concepto_nota_credito As String) As Events
        LookupNotaCredito = oMovimientosCobrar.sp_notas_credito_grl(serie, concepto_nota_credito)
    End Function

    Public Function LookupNotaCargo(ByVal serie As String, ByVal concepto As String) As Events
        LookupNotaCargo = oMovimientosCobrar.sp_notas_cargo_grl(serie, concepto)
    End Function

    Public Function NotasCargoCliente(ByVal cliente As Long) As Events
        NotasCargoCliente = oMovimientosCobrar.sp_notas_cargo_por_cliente_grs(cliente)
    End Function


    Public Function MovimientosCliente(ByVal cliente As Long) As Events
        MovimientosCliente = oMovimientosCobrar.sp_movimientos_cliente_sel(cliente)
    End Function


    Public Function ExisteEnganchesVencidos(ByVal cliente As Long) As Events
        ExisteEnganchesVencidos = oMovimientosCobrar.sp_existe_enganches_vencidos_exs(cliente)
    End Function

    Public Function ExisteNotaCargoGeneral(ByVal sucursal As Long, ByVal fecha As DateTime) As Events
        ExisteNotaCargoGeneral = oMovimientosCobrar.sp_notas_cargo_generales_exs(sucursal, fecha)
    End Function

    Public Function GeneraNotaCargoGeneral(ByVal sucursal As Long, ByVal fecha As DateTime) As Events
        GeneraNotaCargoGeneral = oMovimientosCobrar.sp_notas_cargo_generales_ins(sucursal, fecha)
    End Function

    Public Function MovimientosCondonacionIVa(ByVal sucursal As Long, ByVal mes As Int32, ByVal anio As Long) As Events
        MovimientosCondonacionIVa = oMovimientosCobrar.sp_movimientos_condonacion_iva_sel(sucursal, mes, anio)
    End Function


    Public Function ActualizarObservaciones(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal observaciones As String) As Events
        ActualizarObservaciones = oMovimientosCobrar.sp_movimientos_cobrar_actualizar_observaciones(sucursal, concepto, serie, folio, cliente, observaciones)
    End Function




#Region "Cancelar Abonos"

    'DAM - 16-06-2007
    'Utilizo el mismo Sp para la cancelacion de la nota de credito y el abono.. OJO si el SP tambien afecta a esta funcion

    Public Function CancelarNotaCredito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal fecha_cancelacion As String) As Events
        CancelarNotaCredito = oMovimientosCobrar.sp_movimientos_cobrar_cancelar_nota_credito(sucursal, concepto, serie, folio, cliente, fecha_cancelacion)
    End Function

    Public Function CancelarNotaCargo(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal fecha_cancelacion As String) As Events
        CancelarNotaCargo = oMovimientosCobrar.sp_movimientos_cobrar_cancelar_nota_cargo(sucursal, concepto, serie, folio, cliente, fecha_cancelacion)
    End Function

    Public Function CancelarAbono(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal fecha_cancelacion As String) As Events
        CancelarAbono = oMovimientosCobrar.sp_movimientos_cobrar_cancelar_nota_credito(sucursal, concepto, serie, folio, cliente, fecha_cancelacion)
    End Function

#End Region

    Public Function BuscaAbono(ByVal sucursal As Long, ByVal cliente As Long, ByVal serie As String, ByVal folio As Long) As Events
        BuscaAbono = oMovimientosCobrar.sp_movimientos_cobrar_buscar_abono(sucursal, cliente, serie, folio)
    End Function
    Public Function BuscaAbonoReimpresion(ByVal sucursal As Long, ByVal cliente As Long, ByVal serie As String, ByVal folio As Long) As Events
        BuscaAbonoReimpresion = oMovimientosCobrar.sp_movimientos_cobrar_buscar_abono_reimpresion(sucursal, cliente, serie, folio)
    End Function

#Region "Validaciones"


    Public Function Validacion(ByVal Action As Actions, ByVal cobrador As Long, ByVal importe As Double, ByVal intereses As Double, ByVal totalformapago As Double, ByVal bonificar As Double, ByVal importe_documentos As Double, ByVal no_recibir_abonos As Boolean, ByVal PorcentajeCondonacionIntereses As Double, ByVal Folio_Bonificacion As Long) As Events
        Validacion = ValidaCobrador(cobrador)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaMonto(importe)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaIntereses(intereses, bonificar)
        If Validacion.ErrorFound Then Exit Function
        Validacion = Validapago(importe, intereses, totalformapago, bonificar)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPagodocumentos(importe, intereses, bonificar, importe_documentos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaNoRecibirAbonos(no_recibir_abonos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPorcentajeCondonacionIntereses(intereses, bonificar, PorcentajeCondonacionIntereses, Folio_Bonificacion)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaCargos(NotaCargo, BonificacionCargo)
        'If Validacion.ErrorFound Then Exit Function





    End Function
    Public Function Validacion(ByVal ImporteAPagar As Long, ByVal ImporteRecibido As String, ByVal Cambio As String) As Events
        Validacion = ValidaImporteAPagar(ImporteAPagar)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaImporteRecibido(ImporteRecibido)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaImportePagarRecibido(ImporteAPagar, ImporteRecibido)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCambio(Cambio)
        If Validacion.ErrorFound Then Exit Function


    End Function

    Public Function ValidacionUltimosDigitos(ByVal solicita_ultimos_digitos As Boolean, ByVal ultimos_digitos As String) As Events
        If solicita_ultimos_digitos Then
            ValidacionUltimosDigitos = ValidaUltimosDigitos(ultimos_digitos)
            If ValidacionUltimosDigitos.ErrorFound Then Exit Function
        Else
            ValidacionUltimosDigitos = New Events

        End If

    End Function

    Private Function ValidaUltimosDigitos(ByVal ultimos_digitos As String) As Events
        Dim oEvent As New Events
        If ultimos_digitos.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los Ultimos d�gitos son Requeridos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        Else
            If ultimos_digitos.ToString().Length < 4 Then
                oEvent.Ex = Nothing
                oEvent.Message = "Los Ultimos 4 d�gitos son Requeridos"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent

    End Function

    Private Function ValidaFolioVale(ByVal FolioVale As Long) As Events
        Dim oEvent As New Events
        If FolioVale <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El folio del vale es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaFormaPago(ByVal FormaPago As Long) As Events
        Dim oEvent As New Events
        If FormaPago <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Forma de Pago es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaCliente(ByVal cliente As Long) As Events
        Dim oEvent As New Events
        If cliente < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaImportePagarRecibido(ByVal ImporteAPagar As Long, ByVal ImporteRecibido As String) As Events
        Dim oEvent As New Events
        If IsNumeric(ImporteAPagar) And IsNumeric(ImporteRecibido) Then
            If ImporteAPagar > CType(ImporteRecibido, Long) Then
                oEvent.Ex = Nothing
                oEvent.Message = "El importe recibido es menor que el importe a pagar"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
    Public Function ValidaImporteAPagar(ByVal ImporteAPagar As Long) As Events
        Dim oEvent As New Events
        If ImporteAPagar <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El importe a pagar es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaImporteRecibido(ByVal ImporteRecibido As String) As Events
        Dim oEvent As New Events
        If ImporteRecibido.Trim.Length = 0 Or Not IsNumeric(ImporteRecibido) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El importe recibido es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If IsNumeric(ImporteRecibido) Then
            If CType(ImporteRecibido, Long) <= 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El importe recibido es requerido"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
    Public Function ValidaCambio(ByVal Cambio As String) As Events
        Dim oEvent As New Events
        If Cambio.Trim.Length = 0 Or Not IsNumeric(Cambio) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El cambio recibido es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If IsNumeric(Cambio) Then
            If CType(Cambio, Long) < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El cambio recibido es requerido"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
    Public Function ValidacionTraspasoCuenta(ByVal Action As Actions, ByVal Delcliente As Long, ByVal Alcliente As Long, ByVal letras As Long, ByVal importe_letra As Double) As Events
        ValidacionTraspasoCuenta = ValidaCliente(Delcliente)
        If ValidacionTraspasoCuenta.ErrorFound Then Exit Function
        ValidacionTraspasoCuenta = ValidaCliente(Alcliente)
        If ValidacionTraspasoCuenta.ErrorFound Then Exit Function
        ValidacionTraspasoCuenta = ValidaNumeroLetras(letras)
        If ValidacionTraspasoCuenta.ErrorFound Then Exit Function

        ValidacionTraspasoCuenta = ValidaImporteTraspasosSaldos(importe_letra)
        If ValidacionTraspasoCuenta.ErrorFound Then Exit Function


    End Function
    Public Function ValidacionNotaCargo(ByVal Action As Actions, ByVal sucursal As Long, ByVal cliente As Long, ByVal concepto As String, ByVal importe As Double, ByVal notas As String, ByVal factura_electronica As Boolean, ByVal OpcionPago As Char, ByVal FormaPago As Long, ByVal solicita_ultimos_digitos As Boolean, ByVal ultimos_digitos As String, ByVal SucursalDependencia As Boolean, ByVal Convenio As Long, ByVal solicita_folio_vale As Boolean, ByVal folio_vale As Long) As Events
        ValidacionNotaCargo = ValidaSucursal(sucursal)
        If ValidacionNotaCargo.ErrorFound Then Exit Function
        If SucursalDependencia Then
            ValidacionNotaCargo = ValidaConvenio(Convenio)
            If ValidacionNotaCargo.ErrorFound Then Exit Function
        End If

        ValidacionNotaCargo = ValidaCliente(cliente)
        If ValidacionNotaCargo.ErrorFound Then Exit Function
        ValidacionNotaCargo = ValidaConcepto(concepto)
        If ValidacionNotaCargo.ErrorFound Then Exit Function
        ValidacionNotaCargo = ValidaImporte(importe)
        If ValidacionNotaCargo.ErrorFound Then Exit Function
        ValidacionNotaCargo = ValidaNotas(notas)
        If ValidacionNotaCargo.ErrorFound Then Exit Function

        If factura_electronica = True And OpcionPago = "D" Then
            ValidacionNotaCargo = ValidaFormaPago(FormaPago)
            If ValidacionNotaCargo.ErrorFound Then Exit Function

            If solicita_ultimos_digitos Then
                ValidacionNotaCargo = ValidaUltimosDigitos(ultimos_digitos)
                If ValidacionNotaCargo.ErrorFound Then Exit Function
            End If

            If solicita_folio_vale Then
                ValidacionNotaCargo = ValidaFolioVale(folio_vale)
                If ValidacionNotaCargo.ErrorFound Then Exit Function
            End If
        End If

    End Function
    Public Function ValidacionNotaCredito(ByVal cliente As Long, ByVal concepto As String, ByVal fecha As String, ByVal letras As Long, ByVal importe As Double, ByVal cargo As Double) As Events
        ValidacionNotaCredito = ValidaCliente(cliente)
        If ValidacionNotaCredito.ErrorFound Then Exit Function
        ValidacionNotaCredito = ValidaConcepto(concepto)
        If ValidacionNotaCredito.ErrorFound Then Exit Function
        ValidacionNotaCredito = ValidaFecha(fecha)
        If ValidacionNotaCredito.ErrorFound Then Exit Function
        ValidacionNotaCredito = ValidaImporte(importe)
        If ValidacionNotaCredito.ErrorFound Then Exit Function
        ValidacionNotaCredito = ValidaLetrasSinSaldar(letras)
        If ValidacionNotaCredito.ErrorFound Then Exit Function
        ValidacionNotaCredito = ValidaImportevsCargo(importe, cargo)
        If ValidacionNotaCredito.ErrorFound Then Exit Function
    End Function
    Public Function ValidaConcepto(ByVal concepto As String) As Events
        Dim oEvent As New Events
        If concepto.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El movimiento es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConvenio(ByVal convenio As Long) As Events
        Dim oEvent As New Events
        If convenio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Convenio es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaNotas(ByVal nota As String) As Events
        Dim oEvent As New Events
        If nota.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Las Observaciones son requeridas"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCobrador(ByVal cobrador As Long) As Events
        Dim oEvent As New Events
        If cobrador < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El cobrador es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaIntereses(ByVal intereses As Double, ByVal bonificar As Double) As Events
        Dim oEvent As New Events
        If intereses < bonificar Then
            oEvent.Ex = Nothing
            oEvent.Message = "La bonificaci�n no puede ser mayor a los intereses"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPorcentajeCondonacionIntereses(ByVal intereses As Double, ByVal bonificar As Double, ByVal PorcentajeCondonacionIntereses As Double, ByVal Folio_Bonificacion As Long) As Events
        Dim oEvent As New Events
        If Folio_Bonificacion = 0 Then
            If bonificar > (intereses * PorcentajeCondonacionIntereses / 100) Then
                oEvent.Ex = Nothing
                oEvent.Message = "Usted Tiene Permiso de Condonar Hasta un " + CType(PorcentajeCondonacionIntereses, String) + " % del Monto de los Intereses"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function
    Public Function ValidaMonto(ByVal monto As Double) As Events
        Dim oEvent As New Events
        If monto <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El monto a pagar es requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal Sucursal As Long) As Events
        Dim oEvent As New Events
        If Sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal es requerida "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function Validapago(ByVal importe As Double, ByVal intereses As Double, ByVal totalformapago As Double, ByVal bonificar As Double) As Events
        Dim oEvent As New Events
        'If totalformapago > (importe + 5) Then
        '    oEvent.Ex = Nothing
        '    oEvent.Message = "Las formas de pago exceden el total del monto a pagar"
        '    oEvent.Layer = Events.ErrorLayer.BussinessLayer
        '    Return oEvent
        'Elseif
        If totalformapago < importe Then
            oEvent.Ex = Nothing
            oEvent.Message = "Las formas de pago no exceden el total del monto a pagar"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent

        End If
        Return oEvent
    End Function
    Public Function ValidaPagodocumentos(ByVal importe As Double, ByVal intereses As Double, ByVal bonificar As Double, ByVal importe_documentos As Double) As Events
        Dim oEvent As New Events
        If importe_documentos < (importe - intereses - bonificar) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El monto a pagar excede el total del importe de los documentos "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent

        End If
        Return oEvent
    End Function
    Public Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events

        If fecha.Trim.Length = 0 Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        If CDate(fecha).Year < 1753 Or CDate(fecha).Year > 9999 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha esta fuera del rango v�lido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Public Function ValidaLetrasSinSaldar(ByVal letras As Long) As Events
        Dim oEvent As New Events
        If letras < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "No Hay Letras Sin Saldar"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaImporte(ByVal importe As Long) As Events
        Dim oEvent As New Events
        If importe <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe Debe Ser Mayor a 0"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaImporteTraspasosSaldos(ByVal importe As Double) As Events
        Dim oEvent As New Events
        If importe <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe de las Letras deben ser Mayor a 0"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNumeroLetras(ByVal letras As Long) As Events
        Dim oEvent As New Events
        If letras <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Numero de Letras Debe Ser Mayor a 0"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNoRecibirAbonos(ByVal no_recibir_abonos As Boolean) As Events
        Dim oEvent As New Events
        If no_recibir_abonos = True Then
            oEvent.Ex = Nothing
            oEvent.Message = "No recibir pagos, el cliente tiene un proceso jur�dico."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCargos(ByVal NotaCargo As Long, ByVal bonificacionCargo As Long) As Events
        Dim oEvent As New Events
        If NotaCargo < bonificacionCargo Then
            oEvent.Ex = Nothing
            oEvent.Message = "La bonificaci�n no puede ser mayor a las notas de cargo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaImportevsCargo(ByVal importe As Double, ByVal cargo As Double) As Events
        Dim oEvent As New Events
        If cargo < importe Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe de Cr�dito no puede ser mayor al Cargo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
    End Function

#End Region


#Region "Movimientos DETECNO"
    Public Function LlenaAbonoCFDI(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal tipo As String, ByVal sucursal_actual As Long) As Events
        LlenaAbonoCFDI = Me.oMovimientosCobrar.sp_recibo_abono_cfdi_ins(sucursal, concepto, serie, folio, cliente, tipo, sucursal_actual)
    End Function
    Public Function LlenaNotaCreditoCFDI(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal iva_desglosado As Boolean, ByVal sucursal_actual As Long, ByVal imprimir_nota As Boolean) As Events
        LlenaNotaCreditoCFDI = Me.oMovimientosCobrar.sp_nota_de_credito_cfdi_ins(sucursal, concepto, serie, folio, cliente, iva_desglosado, sucursal_actual, imprimir_nota)
    End Function

    Public Function LlenaNotaCreditoValeCFDI(ByVal sucursal As Long, ByVal folio_vale As Long) As Events
        LlenaNotaCreditoValeCFDI = Me.oMovimientosCobrar.sp_nota_de_credito_vale_cfdi_ins(sucursal, folio_vale)
    End Function
    Public Function LllenarNotaCargoCFDI(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal iva_desglosado As Boolean, ByVal sucursal_actual As Long) As Events
        LllenarNotaCargoCFDI = Me.oMovimientosCobrar.sp_nota_de_cargo_cfdi_ins(sucursal, concepto, serie, folio, cliente, iva_desglosado, sucursal_actual)
    End Function

    Public Function ReimprimeAbono(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        ReimprimeAbono = Me.oMovimientosCobrar.sp_reimprime_recibos(sucursal, concepto, serie, folio, cliente)
    End Function

    Public Function CancelarTimbrado(ByVal serie As String, ByVal folio As Long, ByVal folio_electronico As Long) As Events
        CancelarTimbrado = oMovimientosCobrar.sp_cancela_cfdi(serie, folio, folio_electronico)
    End Function

#End Region

#End Region

End Class
#End Region


