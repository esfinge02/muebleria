Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradasDetallesSeries
'DATE:		19/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - EntradasDetallesSeries"
Public Class clsEntradasDetallesSeries
    Private oEntradasDetallesSeries As VillarrealData.clsEntradasDetallesSeries

#Region "Constructores"
    Sub New()
        oEntradasDetallesSeries = New VillarrealData.clsEntradasDetallesSeries
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal entrada As Long, ByVal partida As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
        Insertar = oEntradasDetallesSeries.sp_entradas_detalles_series_ins(entrada, partida, articulo, numero_serie)
    End Function

    Public Function Actualizar(ByVal entrada As Long, ByVal partida As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
        Actualizar = oEntradasDetallesSeries.sp_entradas_detalles_series_upd(entrada, partida, articulo, numero_serie)
    End Function

    Public Function Eliminar(ByVal entrada As Long, ByVal partida As Long, ByVal articulo As Long) As Events
        Eliminar = oEntradasDetallesSeries.sp_entradas_detalles_series_del(entrada, partida, articulo)
    End Function

    Public Function DespliegaDatos(ByVal entrada As Long, ByVal partida As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
        DespliegaDatos = oEntradasDetallesSeries.sp_entradas_detalles_series_sel(entrada, partida, articulo, numero_serie)
    End Function

    Public Function Listado() As Events
        Listado = oEntradasDetallesSeries.sp_entradas_detalles_series_grs()
    End Function

    Public Function Validacion(ByVal entrada As Long) As Events
        Validacion = ValidaEntrada(entrada)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal numero_serie As String, ByVal articulo As Long) As Events
        'Validacion = ValidaSerie(numero_serie, articulo)

    End Function

    Public Function ValidaEntrada(ByVal entrada As Long) As Events
        Dim oevents As New Events
        If entrada <= 0 Then
            oevents.Ex = Nothing
            oevents.Message = "La Entrada es Requerida"
            oevents.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oevents
    End Function

    Public Function ValidaSerie(ByVal serie As String, ByVal articulo As Long) As Events
        Dim oevents As New Events
        If serie.Length = 0 Then
            oevents.Ex = Nothing
            oevents.Message = "Falta un N�mero de Serie del Art�culo " & articulo
            oevents.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oevents
    End Function



#End Region

End Class
#End Region


