Imports Dipros.Data
Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports VillarrealBusiness

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCambioCheques
'DATE:		22/06/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CambioCheques"
Public Class clsCambioCheques
    'Private csCambioCheques As ClassStack = BusinessEnvironment.Factory.NewStack(GetType(VillarrealCapacitacionData.clsCambioCheques))
    Private oCambioCheques As New VillarrealData.clsCambioCheques

#Region "Constructores"
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByVal sucursal_actual As Long, ByVal caja_actual As Long, ByRef oData As DataSet) As Events
        Insertar = oCambioCheques.sp_cambio_cheques_ins(sucursal_actual, caja_actual, oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oCambioCheques.sp_cambio_cheques_upd(oData)
    End Function

    Public Function Eliminar(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As Date, ByVal partida As Long) As Events
        Eliminar = oCambioCheques.sp_cambio_cheques_del(sucursal, caja, fecha, partida)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As Date, ByVal partida As Long) As Events
        DespliegaDatos = oCambioCheques.sp_cambio_cheques_sel(sucursal, caja, fecha, partida)
    End Function

    Public Function Listado(ByVal sucursal As Long, ByVal caja As Long, ByVal fecha As DateTime) As Events
        Listado = oCambioCheques.sp_cambio_cheques_grs(sucursal, caja, fecha)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal banco As String, ByVal numero_cheque As String, ByVal monto As Double, ByVal quien As String, ByRef intNumeroControl As Long) As Events
        Validacion = ValidaBanco(banco)
        If Validacion.ErrorFound Then
            intNumeroControl = 1
            Exit Function
        End If
        Validacion = ValidaNumeroCheque(numero_cheque)
        If Validacion.ErrorFound Then
            intNumeroControl = 2
            Exit Function
        End If
        Validacion = ValidaMonto(monto)
        If Validacion.ErrorFound Then
            intNumeroControl = 3
            Exit Function
        End If
        If Action = Actions.Delete Then
            Validacion = ValidaUsuario(quien)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function

    Private Function ValidaBanco(ByVal banco As String)
        Dim oEvent As New Events

        If banco.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El campo Banco es requerido. Por favor, ingrese el nombre del banco."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaNumeroCheque(ByVal numero_cheque As String)
        Dim oEvent As New Events

        If numero_cheque.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El campo Número de Cheque es requerido. Por favor, ingrese un valor para este campo."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaMonto(ByVal monto As String)
        Dim oEvent As New Events

        If monto <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El campo Monto es requerido. Por favor, ingrese el monto del cheque."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaUsuario(ByVal usuario As String)
        Dim oEvent As New Events
        Dim strUsuario As String

        strUsuario = BusinessEnvironment.Connection.User.ToUpper
        If Not usuario.ToUpper.Equals(strUsuario) Then
            oEvent.Ex = Nothing
            oEvent.Message = strUsuario + ": No puede eliminar el registro de este cheque, ya que fue generado por otro usuario."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


