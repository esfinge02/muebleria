Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradasDetallePrecios
'DATE:		10/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - EntradasDetallePrecios"
Public Class clsEntradasDetallePrecios
    Private oEntradasDetallePrecios As VillarrealData.clsEntradasDetallePrecios

#Region "Constructores"
    Sub New()
        oEntradasDetallePrecios = New VillarrealData.clsEntradasDetallePrecios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataRow, ByVal entrada As Long, ByVal partida As Long, ByVal articulo As Long) As Events
        Insertar = oEntradasDetallePrecios.sp_entradas_detalle_precios_ins(oData, entrada, partida, articulo)
    End Function

    Public Function Actualizar(ByRef oData As DataRow, ByVal entrada As Long, ByVal partida As Long, ByVal articulo As Long) As Events
        Actualizar = oEntradasDetallePrecios.sp_entradas_detalle_precios_upd(oData, entrada, partida, articulo)
    End Function

    Public Function Eliminar(ByVal entrada As Long, ByVal partida As Long, ByVal articulo As Long, ByVal precio As Long) As Events
        Eliminar = oEntradasDetallePrecios.sp_entradas_detalle_precios_del(entrada, partida, articulo, precio)
    End Function

    Public Function DespliegaDatos(ByVal entrada As Long, ByVal partida As Long, ByVal articulo As Long) As Events
        DespliegaDatos = oEntradasDetallePrecios.sp_entradas_detalle_precios_sel(entrada, partida, articulo)
    End Function

    Public Function Listado() As Events
        Listado = oEntradasDetallePrecios.sp_entradas_detalle_precios_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


