Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosChequera
'DATE:		08/09/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - MovimientosChequera"
Public Class clsMovimientosChequera
    Private oMovimientosChequera As VillarrealData.clsMovimientosChequera

#Region "Constructores"
    Sub New()
        oMovimientosChequera = New VillarrealData.clsMovimientosChequera
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByRef saldo_actual As Double, ByVal actualizacheque As Boolean, ByVal Folio_Poliza_Cheque As Long) As Events
        Insertar = oMovimientosChequera.sp_movimientos_chequera_ins(oData, saldo_actual, actualizacheque, Folio_Poliza_Cheque)
    End Function

    Public Function Insertar(ByVal banco As Long, ByVal cuentabancaria As String, ByRef folio As Long, ByVal fecha As DateTime, ByVal factura As String, _
    ByVal importe As Double, ByVal tipo As Char, ByVal generar_cheque As Boolean, ByVal beneficiario As String, ByVal cheque As Long, ByVal abono_a_cuenta As Boolean, _
    ByVal formulo As String, ByVal autorizo As String, ByVal concepto As String, ByRef saldo_actual As Double, ByVal actualizacheque As Boolean, ByVal folio_pago_proveedores As Long, ByVal ConceptoMovimiento As Long, ByVal Folio_Poliza_Cheque As Long) As Events
        Insertar = oMovimientosChequera.sp_movimientos_chequera_ins(banco, cuentabancaria, folio, fecha, factura, _
    importe, tipo, generar_cheque, beneficiario, cheque, abono_a_cuenta, _
    formulo, autorizo, concepto, saldo_actual, actualizacheque, folio_pago_proveedores, ConceptoMovimiento, Folio_Poliza_Cheque)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal importe_anterior As Double, ByRef saldo_actual As Double, ByVal actualizacheque As Boolean, ByVal Folio_Poliza_Cheque As Long) As Events
        Actualizar = oMovimientosChequera.sp_movimientos_chequera_upd(oData, importe_anterior, saldo_actual, actualizacheque, Folio_Poliza_Cheque)
    End Function

    Public Function Eliminar(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Long, ByRef saldo_actual As Double) As Events
        Eliminar = oMovimientosChequera.sp_movimientos_chequera_del(banco, cuentabancaria, folio, saldo_actual)
    End Function

    Public Function DespliegaDatos(ByVal banco As Long, ByVal cuentabancaria As String, ByVal folio As Long) As Events
        DespliegaDatos = oMovimientosChequera.sp_movimientos_chequera_sel(banco, cuentabancaria, folio)
    End Function

    Public Function Listado(Optional ByVal banco As Long = -1, Optional ByVal cuentabancaria As String = "") As Events
        Listado = oMovimientosChequera.sp_movimientos_chequera_grs(banco, cuentabancaria)
    End Function
    Public Function ListadoBeneficiarios() As Events
        ListadoBeneficiarios = oMovimientosChequera.sp_movimientos_chequera_beneficiario_grl()
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal ConceptoMovimiento As Long, ByVal Importe As Double, ByVal genera_cheque As Boolean, ByVal Beneficiario As String, ByRef intControl As Long) As Events
        Validacion = ValidaConceptoMovimiento(ConceptoMovimiento)
        If Validacion.ErrorFound Then
            intControl = 1
            Exit Function
        End If

        Validacion = ValidaImporte(Importe)
        If Validacion.ErrorFound Then
            intControl = 2
            Exit Function
        End If

        If genera_cheque = False Then Exit Function
        Validacion = ValidaBeneficiario(Beneficiario)
        If Validacion.ErrorFound Then
            intControl = 3
            Exit Function
        End If

    End Function

    Public Function ValidacionBrowse(ByVal banco As Long, ByVal cuentabancaria As String) As Events
        ValidacionBrowse = ValidaBanco(banco)
        If ValidacionBrowse.ErrorFound Then Exit Function
        ValidacionBrowse = ValidaCuentaBancaria(cuentabancaria)
        If ValidacionBrowse.ErrorFound Then Exit Function

    End Function
    Public Function ValidacionProgramacionPagos(ByVal banco As Long, ByVal cuentabancaria As String, ByVal importe As Double, ByVal saldo As Double) As Events
        ValidacionProgramacionPagos = ValidaBanco(banco)
        If ValidacionProgramacionPagos.ErrorFound Then Exit Function
        ValidacionProgramacionPagos = ValidaCuentaBancaria(cuentabancaria)
        If ValidacionProgramacionPagos.ErrorFound Then Exit Function
        ValidacionProgramacionPagos = ValidaImporteSaldo(importe, saldo)
        If ValidacionProgramacionPagos.ErrorFound Then Exit Function

    End Function

    Private Function ValidaConceptoMovimiento(ByVal ConceptoMovimiento As Long) As Events
        Dim oEvent As New Events
        If ConceptoMovimiento <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto del Movimiento es requerido. Por favor, seleccione un concepto."
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaImporte(ByVal Importe As Double) As Events
        Dim oEvent As New Events
        If Importe <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaBeneficiario(ByVal Beneficiario As String) As Events
        Dim oEvent As New Events
        If Beneficiario.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Beneficiario es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaBanco(ByVal banco As Long) As Events
        Dim oEvent As New Events
        If banco <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Banco es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaCuentaBancaria(ByVal cuentabancaria As String) As Events
        Dim oEvent As New Events
        If cuentabancaria.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cuenta Bancaria es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


    Private Function ValidaImporteSaldo(ByVal importe As Double, ByVal saldo As Double) As Events
        Dim oEvent As New Events
        If importe > saldo Then
            Select Case ShowMessage(MessageType.MsgQuestion, "La Cuenta Bancaria estara en Sobregiro �Deseas continuar ?", "Programaci�n de Pagos")
                Case Answer.MsgNo, Answer.MsgCancel
                    oEvent.Ex = Nothing
                    oEvent.Message = "La Cuenta Bancaria estaria Sobregirada"
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
            End Select

        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


