Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVistasSalidasDetalle
'DATE:		20/12/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - VistasSalidasDetalle"
Public Class clsVistasSalidasDetalle
    Private oVistasSalidasDetalle As VillarrealData.clsVistasSalidasDetalle

#Region "Constructores"
    Sub New()
        oVistasSalidasDetalle = New VillarrealData.clsVistasSalidasDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    '/* csequera enero 2008
    Public Function Insertar(ByRef oData As DataSet, ByRef folio_vista_salida As Long, ByVal facturada As Boolean, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal concepto_inventario_salida As String, ByVal folio_inventario_salida As Long, ByVal partida As Long, ByVal numero_serie As String) As Events
        Insertar = oVistasSalidasDetalle.sp_vistas_salidas_detalle_ins(oData, folio_vista_salida, facturada, sucursal_factura, serie_factura, folio_factura, concepto_inventario_salida, folio_inventario_salida, partida, numero_serie.Trim)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByRef folio_vista_salida As Long, ByVal facturada As Boolean, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal concepto_inventario_salida As String, ByVal folio_inventario_salida As Long) As Events
        Actualizar = oVistasSalidasDetalle.sp_vistas_salidas_detalle_upd(oData, folio_vista_salida, facturada, sucursal_factura, serie_factura, folio_factura, concepto_inventario_salida, folio_inventario_salida)
    End Function
    '*/ csequera enero 2008
    Public Function Eliminar(ByVal folio_vista_salida As Long, ByVal partida As Long) As Events
        Eliminar = oVistasSalidasDetalle.sp_vistas_salidas_detalle_del(folio_vista_salida, partida)
    End Function

    'Public Function DespliegaDatos(ByVal folio_vista_salida As Long, ByVal partida As Long) As Events
    '    DespliegaDatos = oVistasSalidasDetalle.sp_vistas_salidas_detalle_sel(folio_vista_salida, partida)
    'End Function

    Public Function Listado(ByVal folio_vista_salida As Long) As Events
        Listado = oVistasSalidasDetalle.sp_vistas_salidas_detalle_grs(folio_vista_salida)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Articulo As Long, ByVal cantidad As Long, ByVal Numero_Serie As String, ByVal Bodega_Salida As String, ByVal maneja_series As Boolean, ByVal UsarSeries As Boolean, ByVal CantidadSeries As Long) As Events
        Validacion = ValidaArticulo(Articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodega_Salida(Bodega_Salida)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaExistencia(Articulo, Bodega_Salida, cantidad)
        If Validacion.ErrorFound Then Exit Function

        Select Case Action
            Case Actions.Insert
                If CantidadSeries > 0 Then
                    Validacion = ValidaCantidadSeries(cantidad, CantidadSeries)
                    If Validacion.ErrorFound Then Exit Function
                End If
        End Select
     


        'DAM REVISA MANEJO SERIES
        If UsarSeries And maneja_series And Action <> Actions.Delete Then
            'Validacion = ValidaNumero_Serie(Numero_Serie)
            'If Validacion.ErrorFound Then Exit Function
        End If

    End Function

    Private Function ValidaCantidadSeries(ByVal cantidad As Long, ByVal Series As Long) As Events
        Dim oEvent As New Events
        If Series < cantidad Then
            oEvent.Ex = Nothing
            oEvent.Message = "La cantidad de Series es Menor a la cantidad de articulos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer

        End If
        If Series > cantidad Then
            oEvent.Ex = Nothing
            oEvent.Message = "La cantidad de Series es Mayor a la cantidad de articulos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function


    Private Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaNumero_Serie(ByVal Numero_Serie As String) As Events
        Dim oEvent As New Events
        If Numero_Serie.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Numero de Serie es Requerdo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaBodega_Salida(ByVal Bodega_Salida As String) As Events
        Dim oEvent As New Events
        If Bodega_Salida.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaExistencia(ByVal articulo As Long, ByVal bodega As String, ByVal cantidad As Long) As Events
        Dim oEvent As New Events
        Dim odatatable As DataTable
        Dim oMovimientosInventarioDetalle As New VillarrealBusiness.clsMovimientosInventariosDetalle
        oEvent = oMovimientosInventarioDetalle.ValidacantidadArticulos(-1, articulo, cantidad, bodega, "", -1, "I", "Salida Vista")

        Return oEvent
    End Function

    '/* csequera enero 2008
    Public Function ActualizarFacturada(ByVal folio_vista_salida As Long, ByVal partida As Long, ByVal articulo As Long, ByVal facturada As Boolean, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long) As Events
        ActualizarFacturada = oVistasSalidasDetalle.sp_vistas_salidas_detalle_factura_upd(folio_vista_salida, partida, articulo, facturada, sucursal_factura, serie_factura, folio_factura)
    End Function
    '*/ csequera enero 2008
    ''nuevo csequera 19 en 08
    'Public Function BuscaFolioSalidaVista(ByVal folio As Long, ByVal partida As Long) As Events
    '    BuscaFolioSalidaVista = oVistasSalidasDetalle.sp_vistas_salidas_busca_folio_sel(folio, partida)
    'End Function


#End Region

End Class
#End Region


