Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class clsEntradasSalidasReparacion
    Private oEntradasSalidasReparacion As VillarrealData.clsEntradasSalidasReparacion


    Sub New()
        oEntradasSalidasReparacion = New VillarrealData.clsEntradasSalidasReparacion
    End Sub

    Public Function ActualizaSalidaReparacion(ByVal orden_servicio As Long, ByVal fecha_salida_reparacion As DateTime, ByVal observaciones_salida_reparacion As String, ByVal BodegueroSalida As Long) As Events
        ActualizaSalidaReparacion = oEntradasSalidasReparacion.sp_entradas_salidas_GeneraSalida_upd(orden_servicio, fecha_salida_reparacion, observaciones_salida_reparacion, BodegueroSalida)
    End Function

    Public Function ActualizaEntradaReparacion(ByVal orden_servicio As Long, ByVal fecha_entrada_reparacion As DateTime, ByVal observaciones_entrada_reparacion As String, ByVal BodegueroEntrada As Long) As Events
        ActualizaEntradaReparacion = oEntradasSalidasReparacion.sp_entradas_salidas_GeneraEntrada_upd(orden_servicio, fecha_entrada_reparacion, observaciones_entrada_reparacion, BodegueroEntrada)
    End Function

    Public Function DespliegaDatos(ByVal orden_servicio As Long) As Events
        DespliegaDatos = oEntradasSalidasReparacion.sp_entradas_salidas_reparacion_sel(orden_servicio)
    End Function
    Public Function Listado() As Events
        Listado = oEntradasSalidasReparacion.sp_ordenes_servicio_reparaciones_grs()
    End Function


End Class
