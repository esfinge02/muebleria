Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsCotizacionesArticulosPlanes
'DATE:		31/08/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - CotizacionesArticulosPlanes"
Public Class clsCotizacionesArticulosPlanes
    Private oCotizacionesArticulosPlanes As VillarrealData.clsCotizacionesArticulosPlanes

#Region "Constructores"
    Sub New()
        oCotizacionesArticulosPlanes = New VillarrealData.clsCotizacionesArticulosPlanes
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal cotizacion As Long, ByVal articulo As Long, ByVal plan_credito As Long, ByVal importe_venta As Double, ByVal total As Double) As Events
        Insertar = oCotizacionesArticulosPlanes.sp_cotizaciones_articulos_planes_ins(cotizacion, articulo, plan_credito, importe_venta, total)
    End Function

    Public Function Actualizar(ByVal cotizacion As Long, ByVal articulo As Long, ByVal plan_credito As Long, ByVal importe_venta As Double, ByVal total As Double) As Events
        Actualizar = oCotizacionesArticulosPlanes.sp_cotizaciones_articulos_planes_upd(cotizacion, articulo, plan_credito, importe_venta, total)
    End Function

    Public Function Eliminar(ByVal cotizacion As Long, Optional ByVal articulo As Long = -1, Optional ByVal plan_credito As Long = -1) As Events
        Eliminar = oCotizacionesArticulosPlanes.sp_cotizaciones_articulos_planes_del(cotizacion, articulo, plan_credito)
    End Function

    Public Function DespliegaDatos(ByVal cotizacion As Long, ByVal articulo As Long, ByVal plan_credito As Long) As Events
        DespliegaDatos = oCotizacionesArticulosPlanes.sp_cotizaciones_articulos_planes_sel(cotizacion, articulo, plan_credito)
    End Function

    Public Function Listado(ByVal cotizacion As Long) As Events
        Listado = oCotizacionesArticulosPlanes.sp_cotizaciones_articulos_planes_grs(cotizacion)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


