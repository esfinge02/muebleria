Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsAutorizacionesBonificacionesInteres
'DATE:		02/06/2008 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - AutorizacionesBonificacionesInteres"
Public Class clsAutorizacionesBonificacionesInteres
    Private oAutorizacionesBonificacionesInteres As VillarrealData.clsAutorizacionesBonificacionesInteres

#Region "Constructores"
    Sub New()
        oAutorizacionesBonificacionesInteres = New VillarrealData.clsAutorizacionesBonificacionesInteres
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oAutorizacionesBonificacionesInteres.sp_autorizaciones_bonificaciones_interes_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oAutorizacionesBonificacionesInteres.sp_autorizaciones_bonificaciones_interes_upd(oData)
    End Function

    Public Function ActualizarCaja(ByVal folio As Long, ByVal utilizada As Boolean) As Events
        ActualizarCaja = oAutorizacionesBonificacionesInteres.sp_autorizaciones_bonificaciones_interes_caja_upd(folio, utilizada)
    End Function

    Public Function Eliminar(ByVal folio As Long) As Events
        Eliminar = oAutorizacionesBonificacionesInteres.sp_autorizaciones_bonificaciones_interes_del(folio)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oAutorizacionesBonificacionesInteres.sp_autorizaciones_bonificaciones_interes_sel(folio)
    End Function

    Public Function DespliegaDatosCaja(ByVal cliente As Long, ByVal caja As Long, ByVal importe As Double) As Events
        DespliegaDatosCaja = oAutorizacionesBonificacionesInteres.sp_autorizaciones_bonificaciones_interes_caja_sel(cliente, caja, importe)
    End Function

    Public Function Listado() As Events
        Listado = oAutorizacionesBonificacionesInteres.sp_autorizaciones_bonificaciones_interes_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal cliente As Long, ByVal caja As Long, ByVal interes As Double, ByVal bonificacion As Double, ByVal fecha As String) As Events

        Validacion = ValidaCliente(cliente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCaja(caja)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function

        If Action = Actions.Insert Then
            Validacion = ValidaBonificacion(interes, bonificacion)
            If Validacion.ErrorFound Then Exit Function
        End If
    End Function

    Private Function ValidaCliente(ByVal Cliente As Long) As Events
        Dim oEvent As New Events
        If Cliente <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function

    Private Function ValidaCaja(ByVal Caja As Long) As Events
        Dim oEvent As New Events
        If Caja <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Caja es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
    Private Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events
        If fecha = Nothing Or IsDate(fecha) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function

    Private Function ValidaBonificacion(ByVal Interes As Double, ByVal bonificacion As Double) As Events
        Dim oEvent As New Events

        If bonificacion = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bonificación es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer

        ElseIf bonificacion > Interes Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bonificación es mayor que los intereses"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer

        End If

        Return oEvent
    End Function

#End Region

End Class
#End Region


