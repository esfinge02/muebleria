Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesRecuperacionDetalle
'DATE:		11/10/2007 0:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - OrdenesRecuperacionDetalle"
Public Class clsOrdenesRecuperacionDetalle
    Private oOrdenesRecuperacionDetalle As VillarrealData.clsOrdenesRecuperacionDetalle

#Region "Constructores"
    Sub New()
        oOrdenesRecuperacionDetalle = New VillarrealData.clsOrdenesRecuperacionDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    'Public Function Insertar(ByRef oData As DataSet, ByVal Folio As Long) As Events
    '    Insertar = oOrdenesRecuperacionDetalle.sp_ordenes_recuperacion_detalle_ins(oData, folio)
    'End Function
    Public Function Insertar(ByRef oData As DataRow, ByVal Folio As Long) As Events
        Insertar = oOrdenesRecuperacionDetalle.sp_ordenes_recuperacion_detalle_ins(oData, Folio)
    End Function

    Public Function Actualizar(ByRef oData As DataRow, ByVal Folio As Long) As Events
        Actualizar = oOrdenesRecuperacionDetalle.sp_ordenes_recuperacion_detalle_upd(oData, Folio)
    End Function

    'Public Function Actualizar(ByRef oData As DataSet, ByVal Folio As Long) As Events
    '    Actualizar = oOrdenesRecuperacionDetalle.sp_ordenes_recuperacion_detalle_upd(oData, FOLIO)
    'End Function

    Public Function Eliminar(ByVal folio As Long) As Events
        Eliminar = oOrdenesRecuperacionDetalle.sp_ordenes_recuperacion_detalle_del(folio)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oOrdenesRecuperacionDetalle.sp_ordenes_recuperacion_detalle_sel(folio)
    End Function

    Public Function Listado() As Events
        Listado = oOrdenesRecuperacionDetalle.sp_ordenes_recuperacion_detalle_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


