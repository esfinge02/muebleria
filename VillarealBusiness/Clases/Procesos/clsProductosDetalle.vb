Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class clsProductosDetalle
    Private oProductosDetalle As VillarrealData.clsProductosDetalle

#Region "Constructores"
    Sub New()
        oProductosDetalle = New VillarrealData.clsProductosDetalle

    End Sub
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal cliente As Long, ByVal convenio_aplica As Object, ByVal importe As Double) As Events
        Insertar = oProductosDetalle.sp_productos_detalle_ins(convenio, quincena, anio, cliente, convenio_aplica, importe)
    End Function

    Public Function Actualizar(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal cliente As Long, ByVal convenio_aplica As Long, ByVal importe As Double) As Events
        Actualizar = oProductosDetalle.sp_productos_detalle_upd(convenio, quincena, anio, cliente, convenio_aplica, importe)
    End Function

    Public Function Eliminar(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal cliente As Long) As Events
        Eliminar = oProductosDetalle.sp_productos_detalle_del(convenio, quincena, anio, cliente)
    End Function

    Public Function DespliegaDatos(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long, ByVal cliente As Long) As Events
        DespliegaDatos = oProductosDetalle.sp_productos_detalle_sel(convenio, quincena, anio, cliente)
    End Function

    Public Function Listado(ByVal convenio As Long, ByVal quincena As Long, ByVal anio As Long) As Events
        Listado = oProductosDetalle.sp_productos_detalle_grs(convenio, quincena, anio)
    End Function

    Public Function Validacion(ByVal Cliente As Long, ByVal ConvenioAplica As Long) As Events
        Validacion = ValidaCliente(Cliente)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Private Function ValidaCliente(ByVal Cliente As Long) As Events
        Dim oEvent As New Events
        If Cliente = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaConvenioAplica(ByVal ConvenioAplica As Long) As Events
        Dim oEvent As New Events
        If ConvenioAplica = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Convenio es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region
End Class
