Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosInventariosDetalleSeries
'DATE:		15/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - MovimientosInventariosDetalleSeries"
Public Class clsMovimientosInventariosDetalleSeries
    Private oMovimientosInventariosDetalleSeries As VillarrealData.clsMovimientosInventariosDetalleSeries

#Region "Constructores"
    Sub New()
        oMovimientosInventariosDetalleSeries = New VillarrealData.clsMovimientosInventariosDetalleSeries
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal articulo As Long) As Events
        Insertar = oMovimientosInventariosDetalleSeries.sp_movimientos_inventarios_detalle_series_ins(oData, sucursal, bodega, concepto, folio, partida, articulo)
    End Function

    Public Function Insertar(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal numero_serie As String, ByVal articulo As Long) As Events
        Insertar = oMovimientosInventariosDetalleSeries.sp_movimientos_inventarios_detalle_series_ins(sucursal, bodega, concepto, folio, partida, numero_serie, articulo)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal articulo As Long) As Events
        Actualizar = oMovimientosInventariosDetalleSeries.sp_movimientos_inventarios_detalle_series_upd(oData, sucursal, bodega, concepto, folio, partida, articulo)
    End Function

    Public Function Eliminar(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal numero_serie As String) As Events
        Eliminar = oMovimientosInventariosDetalleSeries.sp_movimientos_inventarios_detalle_series_del(bodega, concepto, folio, partida, numero_serie)
    End Function

    Public Function DespliegaDatos(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long, ByVal numero_serie As String) As Events
        DespliegaDatos = oMovimientosInventariosDetalleSeries.sp_movimientos_inventarios_detalle_series_sel(bodega, concepto, folio, partida, numero_serie)
    End Function

    Public Function Listado() As Events
        Listado = oMovimientosInventariosDetalleSeries.sp_movimientos_inventarios_detalle_series_grs()
    End Function

    Public Function InsertarSerieArticuloReparto(ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal numero_serie As String, ByVal articulo As Long) As Events
        InsertarSerieArticuloReparto = oMovimientosInventariosDetalleSeries.sp_movimientos_detalle_series_repartos(sucursal_factura, serie_factura, folio_factura, numero_serie, articulo)
    End Function
    Public Function InsertarSeriesEntradas(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida_movimiento As Long, ByVal articulo As Long, ByVal entrada As Long, ByVal partida_entrada As Long) As Events
        InsertarSeriesEntradas = oMovimientosInventariosDetalleSeries.sp_movimientos_inventarios_detalle_entradas_series_ins(sucursal, bodega, concepto, folio, partida_movimiento, articulo, entrada, partida_entrada)
    End Function
    Public Function CambiarBodegaHisSeries(ByVal articulo As Long, ByVal numero_serie As String, ByVal bodega As String) As Events
        CambiarBodegaHisSeries = oMovimientosInventariosDetalleSeries.sp_his_series_cambiar_bodega(articulo, numero_serie, bodega)
    End Function

    Public Function EliminarSerieMVC(ByVal articulo As Long, ByVal numero_serie As String) As Events
        EliminarSerieMVC = oMovimientosInventariosDetalleSeries.sp_movimientos_inventarios_detalle_series_elimina_mvc_del(articulo, numero_serie)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function

#End Region

End Class
#End Region
