Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosCobrarDetalle
'DATE:		19/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - MovimientosCobrarDetalle"
Public Class clsMovimientosCobrarDetalle
    Private oMovimientosCobrarDetalle As VillarrealData.clsMovimientosCobrarDetalle

#Region "Constructores"
    Sub New()
        oMovimientosCobrarDetalle = New VillarrealData.clsMovimientosCobrarDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal observaciones As String, ByVal importe_proporcional_costo As Double) As Events
        Insertar = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_ins(oData, observaciones, importe_proporcional_costo)
    End Function

    Public Function Insertar(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long, ByVal partida As Long, _
    ByVal fecha As Date, ByVal importe As Double, ByVal sucursal_referencia As Object, ByVal concepto_referencia As Object, ByVal serie_referencia As Object, ByVal folio_referencia As Object, ByVal cliente_referencia As Object, ByVal documento_referencia As Object, _
    ByVal plazo As Long, ByVal fecha_vencimiento As Object, ByVal saldo As Double, ByVal tipo_abono As String, ByVal observaciones As String, ByVal importe_proporcional_costo As Double, Optional ByVal quincena_envio As Long = -1, Optional ByVal ano_envio As Long = -1, Optional ByVal sucursal_dependencia As Boolean = False) As Events
        Insertar = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_ins(sucursal, concepto, serie, folio, cliente, documento, fecha, importe, sucursal_referencia, concepto_referencia, serie_referencia, folio_referencia, cliente_referencia, documento_referencia, plazo, fecha_vencimiento, saldo, tipo_abono, observaciones, importe_proporcional_costo, quincena_envio, ano_envio, sucursal_dependencia)
    End Function



    Public Function Eliminar(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long) As Events
        Eliminar = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_del(sucursal, concepto, serie, folio, cliente, documento)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long) As Events
        DespliegaDatos = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_sel(sucursal, concepto, serie, folio, cliente, documento)
    End Function

    Public Function DocumentosMovimiento(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        DocumentosMovimiento = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_documentos_movimiento(sucursal, concepto, serie, folio, cliente)
    End Function

    Public Function Listado() As Events
        Listado = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_grs()
    End Function

    Public Function ExisteDocumento(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long) As Events
        ExisteDocumento = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_exs(sucursal, concepto, serie, folio, cliente, documento)
    End Function
    Public Function ActualizarInteres(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long, ByVal saldo_interes As Double, ByVal fecha_ultimo_abono As DateTime) As Events
        ActualizarInteres = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_saldo_interes_upd(sucursal, concepto, serie, folio, cliente, documento, saldo_interes, fecha_ultimo_abono)
    End Function

    Public Function LetrasDeFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal UsarEnganche As Boolean) As Events
        LetrasDeFactura = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_letras_factura_grs(sucursal, serie, folio, cliente, UsarEnganche)
    End Function

    Public Function LetraDeFacturaTraspasos(ByVal sucursal As Long, ByVal concepto_tipo_cargo As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        LetraDeFacturaTraspasos = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_letras_factura_traspasos_grs(sucursal, concepto_tipo_cargo, serie, folio, cliente)
    End Function

    Public Function CambiaFechaVencimiento(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long, ByVal fecha_vencimiento As Date, ByVal quincena_envio As Long, ByVal anio_envio As Long) As Events
        CambiaFechaVencimiento = oMovimientosCobrarDetalle.sp_cambia_fechas_vencimiento(sucursal, serie, folio, cliente, documento, fecha_vencimiento, quincena_envio, anio_envio)
    End Function

    Public Function ConsultaDocumentosSaldados(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        ConsultaDocumentosSaldados = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_documentos_saldados_grs(sucursal, concepto, serie, folio, cliente)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function

    Public Function MovimientosClientesFacturaDetalle(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long) As Events
        MovimientosClientesFacturaDetalle = oMovimientosCobrarDetalle.sp_movimientos_cobrar_detalle_documentos_facturas_grs(sucursal, concepto, serie, folio, cliente)
    End Function

    Public Function DescuentaInteresesBonificados(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, ByVal documento As Long, ByVal bonificar As Double) As Events
        DescuentaInteresesBonificados = oMovimientosCobrarDetalle.sp_descuenta_intereses_bonificados(sucursal, concepto, serie, folio, cliente, documento, bonificar)
    End Function

#End Region

End Class
#End Region


