Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVentasCambios
'DATE:		06/03/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - VentasCambios"
Public Class clsVentasCambios
    Private oVentasCambios As VillarrealData.clsVentasCambios

#Region "Constructores"
    Sub New()
        oVentasCambios = New VillarrealData.clsVentasCambios
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Insertar = oVentasCambios.sp_ventas_cambios_ins(oData, sucursal, serie, folio)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Actualizar = oVentasCambios.sp_ventas_cambios_upd(oData, sucursal, serie, folio)
    End Function

    Public Function Eliminar(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        Eliminar = oVentasCambios.sp_ventas_cambios_del(sucursal, serie, folio, partida)
    End Function

    Public Function EliminarxFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        EliminarxFactura = oVentasCambios.sp_ventas_cambios_factura_del(sucursal, serie, folio)
    End Function

    Public Function ActualizaFacturaCambioFisicoArticulos(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        ActualizaFacturaCambioFisicoArticulos = oVentasCambios.sp_ventas_cambio_fisico_articulos_upd(sucursal, serie, folio)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal partida As Long) As Events
        DespliegaDatos = oVentasCambios.sp_ventas_cambios_sel(sucursal, serie, folio, partida)
    End Function

    Public Function Listado() As Events
        Listado = oVentasCambios.sp_ventas_cambios_grs()
    End Function

    Public Function Validacion(ByVal articulo As Long, ByVal bodega As String) As Events
        Validacion = ValidaArticulo(articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaBodega(bodega)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function Validacion(ByVal factura As String, ByVal concepto_salida As String, ByVal concepto_entrada As String) As Events

        Validacion = ValidaFactura(factura)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConceptoSalida(concepto_salida)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConceptoEntrada(concepto_entrada)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Private Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Art�culo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaBodega(ByVal bodega As String) As Events
        Dim oEvent As New Events
        If bodega.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


    Private Function ValidaFactura(ByVal Factura As String) As Events
        Dim oEvent As New Events
        If Factura.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Factura es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaConceptoSalida(ByVal concepto As String)
        Dim oEvent As New Events
        If concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto Salida es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaConceptoEntrada(ByVal concepto As String)
        Dim oEvent As New Events
        If concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto Entrada es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function




#End Region

End Class
#End Region


