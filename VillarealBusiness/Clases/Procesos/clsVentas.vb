Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports VillarrealBusiness.clsUtilerias


'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVentas
'DATE:		10/04/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Ventas"
Public Class clsVentas
    Private oVentas As VillarrealData.clsVentas

#Region "Constructores"
    Sub New()
        oVentas = New VillarrealData.clsVentas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal serie As String, ByVal fecha As DateTime, ByVal punto_venta As Long, Optional ByVal importe As Double = 0, Optional ByVal menos As Double = 0, Optional ByVal enganche As Double = 0, _
    Optional ByVal interes As Double = 0, Optional ByVal plan As Long = 0, Optional ByVal fecha_primer_documento As String = "01/01/1900", Optional ByVal numero_documentos As Long = 0, Optional ByVal importe_documentos As Double = 0, Optional ByVal importe_ultimo_documento As Double = 0, _
    Optional ByVal enganche_en_documento As Boolean = False, Optional ByVal fecha_enganche_documento As String = "01/01/1900", Optional ByVal importe_enganche_documento As Double = 0, Optional ByVal liquida_vencimiento As Boolean = False, Optional ByVal monto_liquida_vencimiento As Double = 0, _
    Optional ByVal enganche_en_menos As Boolean = False, Optional ByVal sucursal_dependencia As Boolean = False, Optional ByVal folio_plantilla As Long = 0, Optional ByVal quincena_inicio As Long = -1, Optional ByVal ano_inicio As Long = -1, Optional ByVal cotizacion As Long = 0, Optional ByVal FolioDescuentoCliente As Long = -1, _
    Optional ByVal aplica_enganche As Boolean = False, Optional ByVal paquete As Long = -1, Optional ByVal convenio As Long = -1, Optional ByVal TotalAnticipos As Double = 0, Optional ByVal Apartado As Boolean = False) As Events
        Insertar = oVentas.sp_ventas_ins(oData, sucursal, serie, fecha, importe, menos, enganche, interes, plan, CDate(fecha_primer_documento).Date, numero_documentos, importe_documentos, importe_ultimo_documento, enganche_en_documento, CDate(fecha_enganche_documento).Date, importe_enganche_documento, liquida_vencimiento, monto_liquida_vencimiento, enganche_en_menos, sucursal_dependencia, folio_plantilla, quincena_inicio, ano_inicio, punto_venta, FolioDescuentoCliente, aplica_enganche, paquete, convenio, TotalAnticipos, Apartado)
    End Function
    Public Function InsertarAnticipo(ByVal sucursal As Long, ByVal serie As String, ByRef folio As Long, ByVal fecha As DateTime, ByVal vendedor As Long, ByVal cliente As Long, ByVal domicilio As String, ByVal colonia As String, ByVal ciudad As String, ByVal municipio As String, _
           ByVal cp As Long, ByVal estado As String, ByVal telefono1 As String, ByVal telefono2 As String, ByVal fax As String, ByVal curp As String, ByVal rfc As String, ByVal tipoventa As Char, ByVal ivadesglosado As Boolean, ByVal pedimento As String, ByVal aduana As String, _
           ByVal importe As Double, ByVal menos As Double, ByVal enganche As Double, ByVal subtotal As Double, ByVal impuesto As Double, ByVal total As Double, _
           ByVal interes As Double, ByVal plan As Long, ByVal fecha_primer_documento As Date, ByVal numero_documentos As Long, ByVal importe_documentos As Double, ByVal importe_ultimo_documento As Double, _
           ByVal enganche_en_documento As Boolean, ByVal fecha_enganche_documento As Date, ByVal importe_enganche_documento As Double, ByVal liquida_vencimiento As Boolean, ByVal monto_liquida_vencimiento As Double, _
           ByVal enganche_en_menos As Boolean, ByVal sucursal_dependencia As Boolean, ByVal folio_plantilla As Long, ByVal quincena_inicio As Long, ByVal ano_inicio As Long, ByVal punto_venta As Long, ByVal FolioDescuentoCliente As Long, _
           ByVal aplica_enganche As Boolean, ByVal paquete As Long, ByVal convenio As Long) As Events
        InsertarAnticipo = oVentas.sp_ventas_ins(sucursal, serie, folio, fecha, vendedor, cliente, domicilio, colonia, ciudad, municipio, _
            cp, estado, telefono1, telefono2, fax, curp, rfc, tipoventa, ivadesglosado, pedimento, aduana, importe, menos, enganche, subtotal, impuesto, total, _
            interes, plan, fecha_primer_documento, numero_documentos, importe_documentos, importe_ultimo_documento, enganche_en_documento, fecha_enganche_documento, _
            importe_enganche_documento, liquida_vencimiento, monto_liquida_vencimiento, _
            enganche_en_menos, sucursal_dependencia, folio_plantilla, quincena_inicio, ano_inicio, punto_venta, FolioDescuentoCliente, _
            aplica_enganche, paquete, convenio, False)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        DespliegaDatos = oVentas.sp_ventas_sel(sucursal, serie, folio)
    End Function
    Public Function DespliegaDatosEjecutivo(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        DespliegaDatosEjecutivo = oVentas.sp_ventas_ejecutivo_sel(sucursal, serie, folio)
    End Function


    Public Function Listado(ByVal puntoventa As Long, ByVal fecha As Date, Optional ByVal sucursal As Long = -1) As Events
        Listado = oVentas.sp_ventas_grs(puntoventa, fecha, sucursal)
    End Function

    Private Function ExisteFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        ExisteFactura = oVentas.sp_ventas_exs(sucursal, serie, folio)
    End Function

    Public Function ListadoEjecutivo(ByVal sucursal As Long, ByVal serie As String, ByVal fecha As Date) As Events
        ListadoEjecutivo = oVentas.sp_ventas_grs_ejecutivo(sucursal, serie, fecha)
    End Function

    Public Function LookupFacturas(Optional ByVal sucursal As Long = -1, Optional ByVal bodega As String = "-1") As Events
        LookupFacturas = oVentas.sp_ventas_facturas_grl(sucursal, bodega)
    End Function
    Public Function LookupFacturasCanceladas(Optional ByVal sucursal As Long = -1) As Events
        LookupFacturasCanceladas = oVentas.sp_ventas_facturas_canceladas_grl(sucursal)
    End Function

    Public Function LookupFacturasPorCliente(ByVal cliente As Long) As Events
        LookupFacturasPorCliente = oVentas.sp_ventas_facturas_cliente_grl(cliente)
    End Function

    Public Function VentasCreditoPorFecha(ByVal sucursal As Long, ByVal fecha As Date) As Events
        VentasCreditoPorFecha = oVentas.sp_ventas_credito_por_fecha_grs(sucursal, fecha)
    End Function


    Public Function VentasCreditoVencidasSinNotaCargo(ByVal sucursal As Long, ByVal cliente As Long, ByVal fecha As Date) As Events
        VentasCreditoVencidasSinNotaCargo = oVentas.sp_ventas_credito_vencidas_sin_nota_cargo_grs(sucursal, cliente, fecha)
    End Function

    Public Function ActualizaImpresaNotaCredito(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal importe_ncr As Double) As Events
        ActualizaImpresaNotaCredito = oVentas.sp_ventas_impresa_nota_credito_upd(sucursal, serie, folio, importe_ncr)
    End Function

    Public Function ActualizaNotaCargoGenerada(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal importe_ncargo As Double, ByVal fecha As DateTime) As Events
        ActualizaNotaCargoGenerada = oVentas.sp_ventas_impresa_nota_cargo_upd(sucursal, serie, folio, importe_ncargo, fecha)
    End Function
    Public Function EtiquetasPorImprimir(ByVal bodega As String, ByVal fecha As Date, ByVal todas As Boolean) As Events
        EtiquetasPorImprimir = oVentas.sp_ventas_etiquetas_por_imprimir_grs(bodega, fecha, todas)
    End Function

    Public Function ActualizaFechaVentas(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal fecha As Date) As Events
        ActualizaFechaVentas = oVentas.sp_ventas_fecha_upd(sucursal, concepto, serie, folio, fecha)
    End Function

    Public Function ActualizaQuincenaAnioEnvio(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal quincena_incio As Long, ByVal anio_inicio As Long) As Events
        ActualizaQuincenaAnioEnvio = oVentas.sp_ventas_cambia_quincena_anio_envio(sucursal, serie, folio, quincena_incio, anio_inicio)
    End Function

    Public Function FacturasCreditoPorCliente(ByVal cliente As Long) As Events
        FacturasCreditoPorCliente = oVentas.sp_ventas_credito_por_cliente_grs(cliente)
    End Function

    Public Function FacturasyNotasCreditoPorCliente(ByVal cliente As Long) As Events
        FacturasyNotasCreditoPorCliente = oVentas.sp_ventas_credito_por_cliente_traspasos_grs(cliente)
    End Function

    Public Function CargosPorCliente(ByVal cliente As Long) As Events
        CargosPorCliente = oVentas.sp_cargos_por_cliente_grs(cliente)
    End Function

    Public Function LookupSucursalVentas(ByVal cancelada As Boolean, Optional ByVal megacred As Boolean = False) As Events
        LookupSucursalVentas = oVentas.sp_ventas_sucursal_grl(cancelada, megacred)

    End Function
    Public Function LookupSerieVentas(ByVal sucursal As Long, ByVal cancelada As Boolean) As Events
        LookupSerieVentas = oVentas.sp_ventas_serie_grl(sucursal, cancelada)
    End Function
    Public Function LookupFolioVentas(ByVal sucursal As Long, ByVal serie As String, ByVal cancelada As Boolean) As Events
        LookupFolioVentas = oVentas.sp_ventas_folio_grl(sucursal, serie, cancelada)
    End Function

    Public Function ActualizaVendedorVentas(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal vendedor As Long) As Events
        ActualizaVendedorVentas = oVentas.sp_ventas_vendedor_upd(sucursal, serie, folio, vendedor)
    End Function

    Public Function ActualizaConvenioVentas(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal convenio As Long) As Events
        ActualizaConvenioVentas = oVentas.sp_ventas_convenio_upd(sucursal, serie, folio, convenio)
    End Function


    Public Function ActualizaEntradaEnVentasSobrePedido(ByVal orden_compra As Long, ByVal entrada As Long, ByVal articulo As Long, ByVal bodega As String, ByVal cantidad As Long, ByVal costo As Double, ByVal folio_historico_costo As Long) As Events
        ActualizaEntradaEnVentasSobrePedido = oVentas.sp_ventas_sobrepedido_actualizar_entrada(orden_compra, entrada, articulo, bodega, cantidad, costo, folio_historico_costo)
    End Function



    Public Function Validacion(ByVal Action As Actions, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal Vendedor As Long, ByVal Cliente As Long, ByVal rfc As String, ByVal NumArticulos As Long, ByVal sucursal_dependencia As Boolean, ByVal folio_pedido As Long, ByVal Refacturado As Boolean, ByVal Sucursal_factura_cancelada As Long, ByVal serie_factura_cancelada As String, ByVal folio_factura_cancelada As Long) As Events

        'Validacion = ValidaFactura(sucursal_factura, serie_factura, folio_factura)
        'If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaVendedor(Vendedor)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCliente(Cliente)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaRFC(rfc)
        'If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaNumeroArticulos(NumArticulos)
        If Validacion.ErrorFound Then Exit Function

        If Refacturado = True Then
            Validacion = ValidaSucursalFacturaCancelada(Sucursal_factura_cancelada)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaSerieFacturaCancelada(serie_factura_cancelada)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaFolioFacturaCancelada(folio_factura_cancelada)
            If Validacion.ErrorFound Then Exit Function
        End If

        If sucursal_dependencia = False Then Exit Function
        Validacion = ValidaFolioPedido(folio_pedido)
        If Validacion.ErrorFound Then Exit Function


    End Function


    Private Function ValidaSucursalFacturaCancelada(ByVal sucursal As Long) As Events
        Dim oEvent As New Events
        If sucursal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Sucursal de la Factura Cancelada es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaSerieFacturaCancelada(ByVal serie As String) As Events
        Dim oEvent As New Events
        If serie.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Serie de la Factura Cancelada es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaFolioFacturaCancelada(ByVal Folio As String) As Events
        Dim oEvent As New Events
        If Folio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio de la Factura Cancelada es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


    Public Function ValidaFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        Dim oEvent As New Events
        oEvent = Me.ExisteFactura(sucursal, serie, folio)
        If Not oEvent.ErrorFound Then
            If oEvent.Value = 1 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El Folio: " + serie + "-" + folio.ToString + " de Factura ya Existe"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent
    End Function

    Public Function ValidaCancelacionVenta(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal ConRegistro As Boolean, ByVal Fecha_Venta As DateTime, ByVal Fecha_Cancelacion As DateTime, ByVal opcion_cancelacion As Long, ByVal tipo_vale_cancelacion As Long) As Events
        ValidaCancelacionVenta = ValidaSucursal(sucursal)
        If ValidaCancelacionVenta.ErrorFound Then Exit Function
        ValidaCancelacionVenta = ValidaSerie(serie)
        If ValidaCancelacionVenta.ErrorFound Then Exit Function
        ValidaCancelacionVenta = Validafolio(folio)
        If ValidaCancelacionVenta.ErrorFound Then Exit Function
        ValidaCancelacionVenta = ValidaOpcionCancelacion(opcion_cancelacion)
        If ValidaCancelacionVenta.ErrorFound Then Exit Function
        ValidaCancelacionVenta = ValidaTipoValeCancelacion(tipo_vale_cancelacion)
        If ValidaCancelacionVenta.ErrorFound Then Exit Function

        If ConRegistro = True Then
            ValidaCancelacionVenta = ValidaFechaVenta_vs_FechaCancelacion(Fecha_Venta, Fecha_Cancelacion)
            If ValidaCancelacionVenta.ErrorFound Then Exit Function
        End If


    End Function


    Public Function ValidaFechaVenta_vs_FechaCancelacion(ByVal Fecha_Venta As DateTime, ByVal Fecha_Cancelacion As DateTime) As Events
        Dim oEvent As New Events
        If Fecha_Venta.Day = Fecha_Cancelacion.Day And Fecha_Venta.Month = Fecha_Cancelacion.Month And Fecha_Venta.Year = Fecha_Cancelacion.Year Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha de la Venta no debe ser Igual a la Fecha de Cancelaci�n"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaFolioPedido(ByVal folio_pedido As Long) As Events
        Dim oEvent As New Events
        If folio_pedido <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio de Pedido es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaTipoValeCancelacion(ByVal tipo_vale_cancelacion As Long) As Events
        Dim oEvent As New Events
        If tipo_vale_cancelacion <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El tipo de vale de cancelaci�n no esta en el catalogo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidacionCambiarVendedor(ByVal Action As Actions, ByVal Sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal Vendedor As Long) As Events
        ValidacionCambiarVendedor = ValidaSucursal(Sucursal)
        If ValidacionCambiarVendedor.ErrorFound Then Exit Function
        ValidacionCambiarVendedor = ValidaSerie(serie)
        If ValidacionCambiarVendedor.ErrorFound Then Exit Function
        ValidacionCambiarVendedor = Validafolio(folio)
        If ValidacionCambiarVendedor.ErrorFound Then Exit Function
        ValidacionCambiarVendedor = ValidaVendedor(Vendedor)
        If ValidacionCambiarVendedor.ErrorFound Then Exit Function
    End Function
    Public Function ValidacionCambiarConvenio(ByVal Action As Actions, ByVal Sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal Convenio As Long) As Events
        ValidacionCambiarConvenio = ValidaSucursal(Sucursal)
        If ValidacionCambiarConvenio.ErrorFound Then Exit Function
        ValidacionCambiarConvenio = ValidaSerie(serie)
        If ValidacionCambiarConvenio.ErrorFound Then Exit Function
        ValidacionCambiarConvenio = Validafolio(folio)
        If ValidacionCambiarConvenio.ErrorFound Then Exit Function
        ValidacionCambiarConvenio = ValidaConvenio(Convenio)
        If ValidacionCambiarConvenio.ErrorFound Then Exit Function
    End Function
    Public Function ValidaVendedor(ByVal Vendedor As Long) As Events
        Dim oEvent As New Events
        If Vendedor = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Vendedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConvenio(ByVal Convenio As Long) As Events
        Dim oEvent As New Events
        If Convenio = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Convenio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCliente(ByVal Cliente As Long) As Events
        Dim oEvent As New Events
        If Cliente = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSucursal(ByVal sucursal As Long) As Events
        Dim oEvent As New Events
        If sucursal = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La sucursal es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function Validafolio(ByVal folio As Long) As Events
        Dim oEvent As New Events
        If folio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El folio es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaOpcionCancelacion(ByVal opcion_cancelacion As Long) As Events
        Dim oEvent As New Events
        If opcion_cancelacion <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La opci�n de cancelaci�n es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaRFC(ByVal rfc As String) As Events
        Dim oEvent As New Events
        If rfc.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El RFC es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSerie(ByVal serie As String) As Events
        Dim oEvent As New Events
        If serie.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La serie es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNumeroArticulos(ByVal NumArticulos As Long) As Events
        Dim oEvent As New Events
        If NumArticulos < 1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Por lo menos debe existir un Art�culo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#Region "Datos Credito"
    Public Function ValidacionDatosCredito(ByVal Action As Actions, ByVal Plan As Long, ByVal fecha_1doc As String, ByVal enganche_en_documento As Boolean, ByVal fecha_enganche As String) As Events
        ValidacionDatosCredito = ValidaPlan(Plan)
        If ValidacionDatosCredito.ErrorFound Then Exit Function
        ValidacionDatosCredito = ValidaFecha_1doc(fecha_1doc)
        If ValidacionDatosCredito.ErrorFound Then Exit Function
        ValidacionDatosCredito = ValidaFecha_Enganche(enganche_en_documento, fecha_enganche)
        If ValidacionDatosCredito.ErrorFound Then Exit Function

    End Function

    Public Function ValidaPlan(ByVal Plan As Long) As Events
        Dim oEvent As New Events
        If Plan = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Plan de Cr�dito es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaFecha_1doc(ByVal fecha_1doc As String) As Events
        Dim oEvent As New Events
        If fecha_1doc = Nothing Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha del primer documento es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function

    Public Function ValidaFecha_Enganche(ByVal enganche_en_documento As Boolean, ByVal fecha_enganche As String) As Events
        Dim oEvent As New Events
        If enganche_en_documento Then
            If fecha_enganche = Nothing Then
                oEvent.Ex = Nothing
                oEvent.Message = "La Fecha de enganche en documento es requerida"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function

#End Region

    Public Function ValidaCancelar(ByVal folio_venta As Long, ByVal sucursal As Long, ByVal serie As String) As Events
        ValidaCancelar = oVentas.sp_ventas_validacancelar(folio_venta, sucursal, serie)
    End Function

    Public Function EliminarVenta(ByVal sucursal As Long, ByVal serie As String, ByVal folio_venta As Long, ByVal cliente As Long, ByVal concepto As String, ByVal observaciones As String) As Events
        EliminarVenta = oVentas.sp_ventas_cobrar_cancelacion_eliminada(sucursal, serie, folio_venta, concepto, cliente, observaciones)
    End Function

    Public Function CancelarVenta(ByVal sucursal As Long, ByVal serie As String, ByVal folio_venta As Long, ByVal cliente As Long, ByVal concepto As String, ByVal observaciones As String, ByVal importe_ncr As Double, ByVal opcion_cancelacion As Long) As Events
        CancelarVenta = oVentas.sp_ventas_cobrar_cancelacion_cancelada(sucursal, serie, folio_venta, concepto, cliente, observaciones, importe_ncr, opcion_cancelacion)
    End Function

    Public Function EsEnajenacion(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal factor_enajenacion As Double, ByVal ConceptoFactura As String, ByVal porcentaje_iva As Double) As Events
        EsEnajenacion = oVentas.sp_ventas_esenajenacion(sucursal, serie, folio, factor_enajenacion, ConceptoFactura, porcentaje_iva)
    End Function

    Public Function ImpresionFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal concepto As String, ByVal ivadesglosado As Boolean) As Events
        ImpresionFactura = oVentas.re_factura(sucursal, serie, folio, concepto, ivadesglosado)
    End Function

    Public Function ImpresionFacturaMagisterio(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal concepto As String, ByVal ivadesglosado As Boolean) As Events
        ImpresionFacturaMagisterio = oVentas.re_factura_magisterio(sucursal, serie, folio, concepto, ivadesglosado)
    End Function


    Public Function TraerArticulosVentas(ByVal cotizacion As Long) As Events
        TraerArticulosVentas = Me.oVentas.sp_traerarticulos_ventas(cotizacion)
    End Function


    Public Function TraerArticulosDescuentosClienteVentas(ByVal folio_descuento As Long) As Events
        TraerArticulosDescuentosClienteVentas = Me.oVentas.sp_traerarticulos_descuentos_clientes_ventas(folio_descuento)
    End Function

    Public Function TraerArticulosPaquetesVentas(ByVal paquete As Long) As Events
        TraerArticulosPaquetesVentas = Me.oVentas.sp_traerarticulos_paquetes_ventas(paquete)
    End Function

    Public Function ActualizaCotizacion(ByVal cotizacion As Long, ByVal sucursal As Long, ByVal serie As Char, ByVal Folio As Long) As Events
        ActualizaCotizacion = Me.oVentas.sp_actualiza_venta_cotizacion(cotizacion, sucursal, serie, Folio)
    End Function


    Public Function ValidaRecuperacionesRepartos(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long) As Events
        ValidaRecuperacionesRepartos = oVentas.sp_valida_recuperacion_repartos_venta(sucursal, serie, folio)
    End Function

#Region "Nuevo Cliente Dependencia"
    Public Function ValidacionNuevoClienteDependencia(ByVal Action As Actions, ByVal trabajador As Long) As Events

        ValidacionNuevoClienteDependencia = ValidaTrabajador(trabajador)
        If ValidacionNuevoClienteDependencia.ErrorFound Then Exit Function

    End Function

    Public Function ValidaTrabajador(ByVal trabajador As Long) As Events
        Dim oEvent As New Events
        If trabajador = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Trabajador es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region


    Public Function ObtenerAbonosFactura(ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long) As Events
        ObtenerAbonosFactura = oVentas.sp_obtiene_abonos_factura_sel(sucursal_factura, serie_factura, folio_factura)
    End Function

    Public Function VentasCancelaciones(ByVal fecha As DateTime, ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal observaciones As String, ByVal importe_gastos As Double) As Events
        VentasCancelaciones = oVentas.sp_ventas_cancelaciones_ins(fecha, sucursal, serie, folio, observaciones, importe_gastos)
    End Function
    'Public Function RevisionxFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByRef articulos_no_entregados As Integer) As Events
    '    RevisionxFactura = oVentas.sp_revision_por_factura(sucursal, serie, folio, articulos_no_entregados)
    'End Function


#Region "Timbrado"

    'Private Function ValidaUltimosDigitos(ByVal ultimos_digitos As String) As Events
    '    Dim oEvent As New Events
    '    If CLng(ultimos_digitos) <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "Los Ultimos d�gitos son Requeridos"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    Else
    '        If ultimos_digitos.Length < 4 Then
    '            oEvent.Ex = Nothing
    '            oEvent.Message = "Los Ultimos 4 d�gitos son Requeridos"
    '            oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '            Return oEvent
    '        End If
    '    End If
    '    Return oEvent

    'End Function

    'Private Function ValidaFormaPago(ByVal FormaPago As Long) As Events
    '    Dim oEvent As New Events
    '    If FormaPago <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "La Forma de Pago es requerida"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function

    Public Function ValidaTimbradoFacturas(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal TipoVenta As Char, ByVal FormaPago As Long, ByVal solicita_ultimos_digitos As Boolean, ByVal ultimos_digitos As String) As Events
        ValidaTimbradoFacturas = ValidaSucursal(sucursal)
        If ValidaTimbradoFacturas.ErrorFound Then Exit Function
        ValidaTimbradoFacturas = ValidaSerie(serie)
        If ValidaTimbradoFacturas.ErrorFound Then Exit Function
        ValidaTimbradoFacturas = Validafolio(folio)
        If ValidaTimbradoFacturas.ErrorFound Then Exit Function
        If TipoVenta = "D" Then
            ValidaTimbradoFacturas = ValidaFormaPago(FormaPago)
            If ValidaTimbradoFacturas.ErrorFound Then Exit Function

            If solicita_ultimos_digitos Then
                ValidaTimbradoFacturas = ValidaUltimosDigitos(ultimos_digitos)
                If ValidaTimbradoFacturas.ErrorFound Then Exit Function
            End If
        End If

    End Function
    Public Function ObtenerFacturasTimbrar(ByVal obtiene_serie As Boolean, ByVal sucursal As Long, Optional ByVal serie_filtrar As String = "") As Events
        ObtenerFacturasTimbrar = oVentas.sp_obtener_facturas_timbrar(obtiene_serie, sucursal, serie_filtrar)
    End Function

    Public Function TimbrarFactura(ByVal sucursal As Long, ByVal serie As String, ByVal folio As Long, ByVal concepto_factura As String, ByVal ivadesglosado As Boolean, ByVal sucursal_actual As Long) As Events
        TimbrarFactura = oVentas.sp_factura_cfdi_ins(sucursal, serie, folio, concepto_factura, ivadesglosado, sucursal_actual)
    End Function
    Public Function TimbrarComprobantesSimplificados(ByVal Sucursal As Long, ByVal Fecha As DateTime) As Events
        TimbrarComprobantesSimplificados = oVentas.sp_nota_de_cargo_moratorios_cfdi_ins(Sucursal, Fecha)
    End Function

#End Region


    Public Function BuscaAbonoMenosReimpresion(ByVal sucursal As Long, ByVal cliente As Long, ByVal serie As String, ByVal folio As Long) As Events
        BuscaAbonoMenosReimpresion = oVentas.sp_facturas_buscar_abono_menos_reimpresion(sucursal, cliente, serie, folio)
    End Function

#End Region

End Class
#End Region


