Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsInventarioFisicoDetalle
'DATE:		08/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - InventarioFisicoDetalle"
Public Class clsInventarioFisicoDetalle
    Private oInventarioFisicoDetalle As VillarrealData.clsInventarioFisicoDetalle

#Region "Constructores"
    Sub New()
        oInventarioFisicoDetalle = New VillarrealData.clsInventarioFisicoDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal inventario As Long, ByVal articulo As Long, ByVal conteo_1 As Double, ByVal conteo_2 As Double, ByVal contador As Long, ByVal supervisor As Long, ByVal ubicacion As String) As Events
        Insertar = oInventarioFisicoDetalle.sp_inventario_fisico_detalle_ins(inventario, articulo, conteo_1, conteo_2, contador, supervisor, ubicacion)
    End Function

    'Public Function Actualizar(ByVal inventario As Long, ByVal articulo As Long, ByVal conteo_1 As Double, ByVal conteo_2 As Double) As Events
    '    Actualizar = oInventarioFisicoDetalle.sp_inventario_fisico_detalle_upd(inventario, articulo, conteo_1, conteo_2)
    'End Function

    'Public Function Eliminar(ByVal inventario As Long, ByVal articulo As Long) As Events
    '    Eliminar = oInventarioFisicoDetalle.sp_inventario_fisico_detalle_del(inventario, articulo)
    'End Function

    'Public Function DespliegaDatos(ByVal inventario As Long, ByVal articulo As Long) As Events
    '    DespliegaDatos = oInventarioFisicoDetalle.sp_inventario_fisico_detalle_sel(inventario, articulo)
    'End Function

    Public Function Listado(ByVal inventario As Long) As Events
        Listado = oInventarioFisicoDetalle.sp_inventario_fisico_detalle_grs(inventario)
    End Function

    Public Function ListadoxUbicacion(ByVal inventario As Long) As Events
        ListadoxUbicacion = oInventarioFisicoDetalle.sp_inventario_fisico_detalle_grs_xubica(inventario)
    End Function

    Public Function ListadoArticulos(ByVal articulos As String, ByVal conteo1 As Double, ByVal conteo2 As Double, ByVal contador As Long, ByVal supervisor As Long) As Events
        ListadoArticulos = oInventarioFisicoDetalle.sp_inventario_fisico_detalle_listadoarticulos(articulos, conteo1, conteo2, contador, supervisor)
    End Function

    Public Function ListadoArticulosXUbicacion(ByVal articulos As String, ByVal conteo1 As Double, ByVal conteo2 As Double, ByVal contador As Long, ByVal supervisor As Long, ByVal ubicacion As String, ByVal bodega As String) As Events
        ListadoArticulosXUbicacion = oInventarioFisicoDetalle.sp_inventario_fisico_detalle_listadoarticulos_xubica(articulos, conteo1, conteo2, contador, supervisor, ubicacion, bodega)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Articulo As Long, ByVal Contador As Long, ByVal Supervisor As Long) As Events
        Validacion = ValidaArticulo(Articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaContador(Contador)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSupervisor(Supervisor)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Art�culo es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaContador(ByVal Contador As Long) As Events
        Dim oEvent As New Events
        If Contador = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Contador es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSupervisor(ByVal Supervisor As Long) As Events
        Dim oEvent As New Events
        If Supervisor = -1 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Supervisor es requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


