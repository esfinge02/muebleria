Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradasProveedorDescuentos
'DATE:		14/04/2007 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - EntradasProveedorDescuentos"
Public Class clsEntradasProveedorDescuentos
    Private oEntradasProveedorDescuentos As VillarrealData.clsEntradasProveedorDescuentos

#Region "Constructores"
    Sub New()
        oEntradasProveedorDescuentos = New VillarrealData.clsEntradasProveedorDescuentos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal entrada As Long, ByRef oData As DataSet) As Events
        'Public Function Insertar(ByVal entrada As Long, ByRef descuento As Long, ByVal nombre As String, ByVal porcentaje As Double) As Events
        Insertar = oEntradasProveedorDescuentos.sp_entradas_proveedor_descuentos_ins(entrada, oData)
    End Function

    Public Function Actualizar(ByVal entrada As Long, ByVal oData As DataSet) As Events
        Actualizar = oEntradasProveedorDescuentos.sp_entradas_proveedor_descuentos_upd(entrada, oData)
    End Function

    Public Function Eliminar(ByVal entrada As Long, ByVal oData As DataSet) As Events
        Eliminar = oEntradasProveedorDescuentos.sp_entradas_proveedor_descuentos_del(entrada, oData)
    End Function

    Public Function DespliegaDatos(ByVal entrada As Long) As Events
        DespliegaDatos = oEntradasProveedorDescuentos.sp_entradas_proveedor_descuentos_sel(entrada)
    End Function

    Public Function Listado() As Events
        Listado = oEntradasProveedorDescuentos.sp_entradas_proveedor_descuentos_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


