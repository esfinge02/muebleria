Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsDescuentosEspecialesClientes
    Private oDescuentosEspecialesClientes As VillarrealData.clsDescuentosEspecialesClientes

#Region "Constructores"
    Sub New()
        oDescuentosEspecialesClientes = New VillarrealData.clsDescuentosEspecialesClientes
    End Sub
#End Region

#Region "M�todos"


    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oDescuentosEspecialesClientes.sp_descuentos_especiales_clientes_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oDescuentosEspecialesClientes.sp_descuentos_especiales_clientes_upd(oData)
    End Function

    Public Function Eliminar(ByVal oData As DataSet) As Events
        Eliminar = oDescuentosEspecialesClientes.sp_descuentos_especiales_clientes_del(oData)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oDescuentosEspecialesClientes.sp_descuentos_especiales_clientes_sel(folio)
    End Function

    Public Function Listado() As Events
        Listado = oDescuentosEspecialesClientes.sp_descuentos_especiales_clientes_grs
    End Function

    Public Function Lookup(ByVal Cliente As Long, ByVal utilizada As Boolean) As Events
        Lookup = oDescuentosEspecialesClientes.sp_descuentos_especiales_clientes_lkp(Cliente, utilizada)
    End Function

    Public Function ActualizarVentas(ByVal folio_descuento As Long, ByVal utilizada As Boolean) As Events
        ActualizarVentas = oDescuentosEspecialesClientes.sp_descuentos_especiales_clientes_actualiza_ventas_upd(folio_descuento, utilizada)
    End Function

    Public Function Validacion(ByVal action As Actions, ByVal cliente As Long, ByVal tipo As Long, ByVal monto As Double, ByVal porcentaje As Double, ByVal plan As Long, ByVal importe_total_con_descuento As Double) As Events
        If action <> Actions.Delete Then
            Validacion = ValidaCliente(cliente)
            If Validacion.ErrorFound Then Exit Function

            If tipo = 0 Then
                Validacion = ValidaMonto(monto, importe_total_con_descuento)
                If Validacion.ErrorFound Then Exit Function
            Else

                Validacion = validaDescuento(porcentaje)
                If Validacion.ErrorFound Then Exit Function
                Validacion = ValidaPlanCredito(plan)
                If Validacion.ErrorFound Then Exit Function
            End If


        End If
    End Function

    Private Function ValidaCliente(ByVal cliente As Long) As Events
        Dim oEvent As New Events
        If cliente < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaMonto(ByVal monto As Double, ByVal importe_total_con_descuento As Double) As Events
        Dim oEvent As New Events
        If monto = 0 Then
            oEvent.Message = "El Monto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            'Else
            '    If importe_total_con_descuento < monto Then
            '        oEvent.Message = "El Monto del Descuento es Mayor al Importe Total"
            '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
            '    End If
        End If

        Return oEvent

    End Function

    Private Function ValidaPlanCredito(ByVal Plan_credito As Long) As Events
        Dim oEvent As New Events
        If Plan_credito < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Plan de Cr�dito es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function validaDescuento(ByVal descuento As Double) As Events
        Dim oEvent As New Events
        If descuento = 0 Then
            oEvent.Message = "El Descuento es Requerido"
        Else
            If descuento > 100 Then
                oEvent.Message = "El Descuento no puede ser mayor al 100 %"
            End If
        End If

        Return oEvent
    End Function
#End Region



End Class