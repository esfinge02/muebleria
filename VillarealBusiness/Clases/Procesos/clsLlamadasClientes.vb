Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsLlamadasClientes
'DATE:		17/01/2007 00:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - LlamadasClientes"
Public Class clsLlamadasClientes
    Private oLlamadasClientes As VillarrealData.clsLlamadasClientes

#Region "Constructores"
    Sub New()
        oLlamadasClientes = New VillarrealData.clsLlamadasClientes
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oLlamadasClientes.sp_llamadas_clientes_ins(oData)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oLlamadasClientes.sp_llamadas_clientes_upd(oData)
    End Function

    Public Function Eliminar(ByVal cliente As Long, ByVal fecha_hora As Date) As Events
        Eliminar = oLlamadasClientes.sp_llamadas_clientes_del(cliente, fecha_hora)
    End Function

    Public Function DespliegaDatos(ByVal cliente As Long) As Events
        DespliegaDatos = oLlamadasClientes.sp_llamadas_clientes_sel(cliente)
    End Function

    Public Function Listado() As Events
        Listado = oLlamadasClientes.sp_llamadas_clientes_grs()
    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal cliente As Long) As Events
        Validacion = ValidaCliente(cliente)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Public Function ValidaCliente(ByVal cliente As Long) As Events
        Dim oEvent As New Events
        If cliente <= 0 Or IsDBNull(cliente) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


