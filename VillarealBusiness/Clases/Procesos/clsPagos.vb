Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPagos
'DATE:		10/04/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Pagos"
Public Class clsPagos
    Private oPagos As VillarrealData.clsPagos

#Region "Constructores"
    Sub New()
        oPagos = New VillarrealData.clsPagos
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oPagos.sp_pagos_ins(oData)
    End Function
    Public Function Insertar(ByVal sucursal_abono As Long, ByRef folio_recibo As Long, ByVal serie_recibo As String, ByVal cajero As Long, ByVal cobrador As Long, ByVal cliente As Long, ByVal fecha_cancelacion As Date, ByVal status As String) As Events
        Insertar = oPagos.sp_pagos_ins(sucursal_abono, folio_recibo, serie_recibo, cajero, cobrador, cliente, fecha_cancelacion, status)
    End Function

    Public Function Actualizar(ByVal sucursal_abono As Long, ByRef folio_recibo As Long, ByVal serie_recibo As String, ByVal cajero As Long, ByVal cobrador As Long, ByVal cliente As Long, ByVal fecha_cancelacion As Date, ByVal status As String) As Events
        Actualizar = oPagos.sp_pagos_upd(sucursal_abono, folio_recibo, serie_recibo, cajero, cobrador, cliente, fecha_cancelacion, status)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oPagos.sp_pagos_upd(oData)
    End Function

    Public Function Eliminar(ByVal sucursal_abono As Long, ByVal folio_recibo As Long, ByVal serie_recibo As String) As Events
        Eliminar = oPagos.sp_pagos_del(sucursal_abono, folio_recibo, serie_recibo)
    End Function

    Public Function DespliegaDatos(ByVal sucursal_abono As Long, ByVal folio_recibo As Long, ByVal serie_recibo As String) As Events
        DespliegaDatos = oPagos.sp_pagos_sel(sucursal_abono, folio_recibo, serie_recibo)
    End Function

    Public Function Listado() As Events
        Listado = oPagos.sp_pagos_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


