Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEnviosConveniosDetalle
'DATE:		06/11/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - EnviosConveniosDetalle"
Public Class clsEnviosConveniosDetalle
    Private oEnviosConveniosDetalle As VillarrealData.clsEnviosConveniosDetalle

#Region "Constructores"
    Sub New()
        oEnviosConveniosDetalle = New VillarrealData.clsEnviosConveniosDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef sucursal As Long, ByRef convenio As Long, ByRef quincena_envio As Long, ByRef anio_envio As Long, ByRef cliente As Long, ByRef partida As Long, ByVal sucursal_referencia As Long, ByRef concepto_referencia As String, ByRef serie_referencia As String, ByRef folio_referencia As Long, ByRef cliente_referencia As Long, ByRef documento_referencia As Long, ByRef importe As Long, ByRef afectado As Boolean, ByRef cancelado As Boolean) As Events
        Insertar = oEnviosConveniosDetalle.sp_envios_convenios_detalle_ins(sucursal, convenio, quincena_envio, anio_envio, cliente, partida, sucursal_referencia, concepto_referencia, serie_referencia, folio_referencia, cliente_referencia, documento_referencia, importe, afectado, cancelado)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oEnviosConveniosDetalle.sp_envios_convenios_detalle_upd(oData)
    End Function

    Public Function Eliminar(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long, ByVal partida As Long) As Events
        Eliminar = oEnviosConveniosDetalle.sp_envios_convenios_detalle_del(sucursal, convenio, quincena_envio, anio_envio, cliente, partida)
    End Function

    Public Function DespliegaDatos(ByVal sucursal As Long, ByVal convenio As Long, ByVal quincena_envio As Long, ByVal anio_envio As Long, ByVal cliente As Long, ByVal partida As Long) As Events
        DespliegaDatos = oEnviosConveniosDetalle.sp_envios_convenios_detalle_sel(sucursal, convenio, quincena_envio, anio_envio, cliente, partida)
    End Function

    Public Function Listado() As Events
        Listado = oEnviosConveniosDetalle.sp_envios_convenios_detalle_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


