Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsInventarioFisicoDetalleSeries
'DATE:		08/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - InventarioFisicoDetalleSeries"
Public Class clsInventarioFisicoDetalleSeries
    Private oInventarioFisicoDetalleSeries As VillarrealData.clsInventarioFisicoDetalleSeries

#Region "Constructores"
    Sub New()
        oInventarioFisicoDetalleSeries = New VillarrealData.clsInventarioFisicoDetalleSeries
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
        Insertar = oInventarioFisicoDetalleSeries.sp_inventario_fisico_detalle_series_ins(inventario, articulo, numero_serie)
    End Function

    'Public Function Actualizar(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
    '    Actualizar = oInventarioFisicoDetalleSeries.sp_inventario_fisico_detalle_series_upd(inventario, articulo, numero_serie)
    'End Function

    'Public Function Eliminar(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
    '    Eliminar = oInventarioFisicoDetalleSeries.sp_inventario_fisico_detalle_series_del(inventario, articulo, numero_serie)
    'End Function

    'Public Function DespliegaDatos(ByVal inventario As Long, ByVal articulo As Long, ByVal numero_serie As String) As Events
    '    DespliegaDatos = oInventarioFisicoDetalleSeries.sp_inventario_fisico_detalle_series_sel(inventario, articulo, numero_serie)
    'End Function

    Public Function Listado(ByVal inventario As Long, ByVal articulo As Long) As Events
        Listado = oInventarioFisicoDetalleSeries.sp_inventario_fisico_detalle_series_grs(inventario, articulo)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


