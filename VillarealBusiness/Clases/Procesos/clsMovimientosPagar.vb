Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosPagar
'DATE:		20/07/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - MovimientosPagar"
Public Class clsMovimientosPagar
	Private oMovimientosPagar As VillarrealData.clsMovimientosPagar

#Region "Constructores"
	Sub New()
		oMovimientosPagar = New VillarrealData.clsMovimientosPagar
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"



    Public Function Insertar(ByRef oData As DataSet, ByVal tipo_pago As Object, ByVal cargo As Object, ByVal abono As Object, ByVal banco As Object, ByVal status As String, ByVal abono_cuenta As Boolean, ByVal folio_poliza_cheque As Long) As Events ', ByVal abono_cuenta As Boolean) As Events
        Insertar = oMovimientosPagar.sp_movimientos_pagar_ins(oData, tipo_pago, cargo, abono, banco, status, abono_cuenta, folio_poliza_cheque)
    End Function

    Public Function Insertar(ByVal concepto As String, ByRef folio As Long, ByVal proveedor As Long, ByVal entrada As Long, ByVal fecha As Date, ByVal fecha_vencimiento As Date, _
                            ByVal plazo As Integer, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal es_padre As Boolean, ByVal cargo As Double, ByVal abono As Double, ByVal subtotal As Double, ByVal iva As Double, _
                            ByVal total As Double, ByVal saldo As Double, ByVal tipo_pago As Char, ByVal banco As Long, ByVal cuenta_bancaria As Object, ByVal cheque As Long, ByVal observaciones As String, ByVal formulo As String, ByVal autorizo As String, ByVal status As String, ByVal folio_factura As String, ByVal abono_cuenta As Boolean, ByVal folio_pago_proveedores As Long, ByVal folio_poliza_cheque As Long) As Events
        Insertar = oMovimientosPagar.sp_movimientos_pagar_ins(concepto, folio, proveedor, entrada, fecha, fecha_vencimiento, plazo, concepto_referencia, folio_referencia, _
                                                                es_padre, cargo, abono, subtotal, iva, total, saldo, tipo_pago, banco, cuenta_bancaria, cheque, observaciones, formulo, autorizo, status, folio_factura, abono_cuenta, folio_pago_proveedores, folio_poliza_cheque)
    End Function
    Public Function InsertarAbonoPorComision(ByVal concepto As String, ByRef folio As Long, ByVal proveedor As Long, ByVal entrada As Long, ByVal fecha As Date, ByVal fecha_vencimiento As Date, _
                            ByVal plazo As Integer, ByVal concepto_referencia As String, ByVal folio_referencia As Long, ByVal es_padre As Boolean, ByVal cargo As Double, ByVal abono As Double, ByVal subtotal As Double, ByVal iva As Double, _
                            ByVal total As Double, ByVal saldo As Double, ByVal observaciones As String, ByVal formulo As String, ByVal autorizo As String, ByVal status As String, ByVal folio_factura As String, ByVal abono_cuenta As Boolean, ByVal folio_pago_proveedores As Long) As Events
        InsertarAbonoPorComision = oMovimientosPagar.sp_movimientos_pagar_abono_por_comision_ins(concepto, folio, proveedor, entrada, fecha, fecha_vencimiento, plazo, concepto_referencia, folio_referencia, _
                                                                es_padre, cargo, abono, subtotal, iva, total, saldo, observaciones, formulo, autorizo, status, folio_factura, abono_cuenta, folio_pago_proveedores)
    End Function


    Public Function Actualizar(ByRef oData As DataSet, ByVal tipo_pago As Object, ByVal cargo As Object, ByVal abono As Object, ByVal banco As Object, ByVal status As String, ByVal ActualizarCheque As Boolean, ByVal folio_poliza_cheque As Long) As Events ', ByVal abono_cuenta As Boolean) As Events
        Actualizar = oMovimientosPagar.sp_movimientos_pagar_upd(oData, tipo_pago, cargo, abono, banco, status, ActualizarCheque, folio_poliza_cheque)  ' abono_cuenta
    End Function


    Public Function Eliminar(ByVal concepto As String, ByVal folio As Long) As Events
        Eliminar = oMovimientosPagar.sp_movimientos_pagar_del(concepto, folio)
    End Function

    Public Function DespliegaDatos(ByVal concepto As String, ByVal folio As Long) As Events
        DespliegaDatos = oMovimientosPagar.sp_movimientos_pagar_sel(concepto, folio)
    End Function

    Public Function Listado(ByVal proveedor As Long) As Events
        Listado = oMovimientosPagar.sp_movimientos_pagar_grs(proveedor)
    End Function

    Public Function ActualizaFolioReferencia(ByVal concepto As String, ByVal folio As Long) As Events
        ActualizaFolioReferencia = oMovimientosPagar.sp_movimientos_pagar_actualiza_folio_referencia(concepto, folio)
    End Function

    Public Function Lookup(ByVal concepto As String, ByVal proveedor As Long) As Events
        Lookup = oMovimientosPagar.sp_movimientos_pagar_grl(concepto, proveedor)
    End Function

    Public Function Existe(ByVal concepto As String, ByVal folio As Long) As Events
        Existe = oMovimientosPagar.sp_movimientos_pagar_exs(concepto, folio)
    End Function

    Public Function llena_pago_varias_facturas(ByVal proveedor As Long) As Events
        llena_pago_varias_facturas = oMovimientosPagar.sp_llena_pago_varias_facturas(proveedor)
    End Function


    Public Function AsignarAbonoPorComision(ByVal concepto As String, ByRef folio As Long, ByVal proveedor As Long, ByVal entrada As Long, _
                            ByVal concepto_referencia As String, ByVal folio_referencia As Long) As Events
        AsignarAbonoPorComision = oMovimientosPagar.sp_movimientos_pagar_asignar_abono_por_comision(concepto, folio, proveedor, entrada, concepto_referencia, folio_referencia)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal proveedor As Long) As Events
        Validacion = ValidaProveedor(proveedor)
        If Validacion.ErrorFound Then Exit Function

    End Function
    Public Function Validacion(ByVal ConceptoReferencia As String, ByVal FolioReferencia As Long, ByVal Concepto As String, ByVal Folio As Long, ByVal entrada As Long) As Events
        Validacion = ValidaConcepto(Concepto)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFolio(Folio)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConceptoReferencia(ConceptoReferencia)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFolioReferencia(FolioReferencia)
        If Validacion.ErrorFound Then Exit Function
        'Validacion = ValidaEntrada(entrada)
        'If Validacion.ErrorFound Then Exit Function

    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal proveedor As Long, ByVal concepto As String, ByVal fecha As String, ByVal subtotal As Double, ByVal iva As Double, ByVal total As Double, ByVal tipo_pago As String, ByVal banco As Object, ByVal referencia_transferencia As String, ByVal cheque As Long) As Events
        Validacion = ValidaProveedor(proveedor)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaConcepto(concepto)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFecha(fecha)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSubTotal(subtotal)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaIva(iva)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaTotal(total)
        If Validacion.ErrorFound Then Exit Function

        If tipo_pago = "C" Then
            Validacion = ValidaBanco(banco)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaCheque(cheque)
            If Validacion.ErrorFound Then Exit Function
        ElseIf tipo_pago = "T" Then
            Validacion = ValidaBanco(banco)
            If Validacion.ErrorFound Then Exit Function
            Validacion = ValidaReferenciaTransferencia(referencia_transferencia)
            If Validacion.ErrorFound Then Exit Function
        End If


    End Function

    Public Function ValidaProveedor(ByVal proveedor As Long) As Events
        Dim oEvent As New Events
        If proveedor <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaEntrada(ByVal entrada As Long) As Events
        Dim oEvent As New Events
        If entrada <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Entrada es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFolio(ByVal Folio As Long) As Events
        Dim oEvent As New Events
        If Folio <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConcepto(ByVal concepto As String) As Events
        Dim oEvent As New Events
        If concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFolioReferencia(ByVal FolioReferencia As Long) As Events
        Dim oEvent As New Events
        If FolioReferencia <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio de Referencia es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaConceptoReferencia(ByVal ConceptoReferencia As String) As Events
        Dim oEvent As New Events
        If ConceptoReferencia.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Concepto de Referencia es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events
        If fecha.Trim.Length = 0 Or Not IsDate(fecha) Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSubTotal(ByVal subtotal As Double) As Events
        Dim oEvent As New Events
        If subtotal <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El SubTotal es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaTotal(ByVal total As Double) As Events
        Dim oEvent As New Events
        If total <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Total es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaIva(ByVal iva As Double) As Events
        Dim oEvent As New Events
        If iva <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Iva es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaSaldo(ByVal saldo As Double) As Events
        Dim oEvent As New Events
        If saldo <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Saldo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaBanco(ByVal banco As Object) As Events
        Dim oEvent As New Events
        If IsDBNull(banco) Or banco Is Nothing Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Banco es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
            Exit Function
        End If

        If CLng(banco) <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Banco es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If

        Return oEvent
    End Function
    Public Function ValidaCheque(ByVal cheque As Long) As Events
        Dim oEvent As New Events
        If cheque < 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cheque es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaReferenciaTransferencia(ByVal referencia_transferencia As String) As Events
        Dim oEvent As New Events
        If referencia_transferencia.Trim.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Referencia de Transferencia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region

End Class
#End Region


