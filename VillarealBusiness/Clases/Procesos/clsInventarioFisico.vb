Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsInventarioFisico
'DATE:		08/06/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - InventarioFisico"
Public Class clsInventarioFisico
    Private oInventarioFisico As VillarrealData.clsInventarioFisico

#Region "Constructores"
    Sub New()
        oInventarioFisico = New VillarrealData.clsInventarioFisico
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef inventario As Long, ByVal bodega As String, ByVal fecha As Date, ByVal status As String, ByVal observaciones As String, ByVal tipo_inventario As Char) As Events
        Insertar = oInventarioFisico.sp_inventario_fisico_ins(inventario, bodega, fecha, status, observaciones, tipo_inventario)
    End Function

    Public Function Actualizar(ByRef inventario As Long, ByVal bodega As String, ByVal fecha As Date, ByVal status As String, ByVal observaciones As String, ByVal tipo_inventario As Char) As Events
        Actualizar = oInventarioFisico.sp_inventario_fisico_upd(inventario, bodega, fecha, status, observaciones, tipo_inventario)
    End Function

    Public Function Eliminar(ByVal inventario As Long) As Events
        Eliminar = oInventarioFisico.sp_inventario_fisico_del(inventario)
    End Function

    Public Function DespliegaDatos(ByVal inventario As Long) As Events
        DespliegaDatos = oInventarioFisico.sp_inventario_fisico_sel(inventario)
    End Function

    Public Function Listado(ByVal tipo_inventario As Char, ByVal bodega As String) As Events
        Listado = oInventarioFisico.sp_inventario_fisico_grs(tipo_inventario, bodega)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal brw As Boolean, ByVal bodega As String, ByVal NumArticulos As Long, ByVal fecha As DateTime, ByVal fecha_cierre As DateTime) As Events
        Validacion = ValidaBodega(bodega)
        If Validacion.ErrorFound Then Exit Function

        If brw Then Exit Function

        Validacion = ValidaFecha(fecha, fecha_cierre)
        If Validacion.ErrorFound Then Exit Function

        If Action = Actions.Delete Then Exit Function

        Validacion = ValidaNumeroArticulos(NumArticulos)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaBodega(ByVal Bodega As String) As Events
        Dim oEvent As New Events
        If Bodega.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaNumeroArticulos(ByVal NumArticulos As Long) As Events
        Dim oEvent As New Events
        If NumArticulos = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Debe existir por lo menos un art�culo"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaFecha(ByVal fecha As DateTime, ByVal fecha_cierre As DateTime) As Events
        Dim oEvent As New Events
        'si no hay fecha de cierre definida en variables del sistema, pasa la validacion
        If fecha_cierre = Nothing Then Return oEvent

        If fecha <= fecha_cierre Then
            oEvent.Ex = Nothing
            oEvent.Message = "No se pueden capturar movimientos anteriores a la Fecha de Cierre"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#Region "Accion Ajustar"
    Public Function Ajustar(ByVal sucursal As Long, ByVal bodega As String, ByVal inventario As Long, ByVal fecha As DateTime, ByVal conceptoentrada As String, ByVal conceptosalida As String) As Events
        Ajustar = oInventarioFisico.sp_inventario_fisico_ajustar(sucursal, bodega, inventario, fecha, conceptoentrada, conceptosalida)
    End Function

    Public Function ValidacionAjustar(ByVal conceptoentrada As String, ByVal conceptosalida As String) As Events
        ValidacionAjustar = ValidaConcepto(conceptoentrada, "E")
        If ValidacionAjustar.ErrorFound Then Exit Function
        ValidacionAjustar = ValidaConcepto(conceptosalida, "S")
        If ValidacionAjustar.ErrorFound Then Exit Function
    End Function

    Public Function ValidaConcepto(ByVal concepto As String, ByVal tipo As String) As Events
        Dim oEvent As New Events
        If concepto.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            If tipo = "E" Then
                oEvent.Message = "El Concepto de Entrada es requerido"
            End If
            If tipo = "S" Then
                oEvent.Message = "El Concepto de Salida es requerido"
            End If
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

#End Region

End Class

#End Region

