Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsClientesJuridicoSeguimiento
'DATE:		29/01/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - ClientesJuridicoSeguimiento"
Public Class clsClientesJuridicoSeguimiento
    Private oClientesJuridicoSeguimiento As VillarrealData.clsClientesJuridicoSeguimiento

#Region "Constructores"
    Sub New()
        oClientesJuridicoSeguimiento = New VillarrealData.clsClientesJuridicoSeguimiento
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Insertar = oClientesJuridicoSeguimiento.sp_clientes_juridico_seguimiento_ins(oData, folio_juridico)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal folio_juridico As Long) As Events
        Actualizar = oClientesJuridicoSeguimiento.sp_clientes_juridico_seguimiento_upd(oData, folio_juridico)
    End Function

    Public Function Eliminar(ByVal folio_juridico As Long, ByVal fecha As Date) As Events
        Eliminar = oClientesJuridicoSeguimiento.sp_clientes_juridico_seguimiento_del(folio_juridico, fecha)
    End Function

    Public Function DespliegaDatos(ByVal folio_juridico As Long, ByVal fecha As Date) As Events
        DespliegaDatos = oClientesJuridicoSeguimiento.sp_clientes_juridico_seguimiento_sel(folio_juridico, fecha)
    End Function

    Public Function Listado(ByVal folio_juridico As Long) As Events
        Listado = oClientesJuridicoSeguimiento.sp_clientes_juridico_seguimiento_grs(folio_juridico)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal seguimiento As String) As Events
        Validacion = ValidaSeguimiento(seguimiento)
        If Validacion.ErrorFound Then Exit Function
    End Function
    Private Function ValidaSeguimiento(ByVal Seguimiento As String) As Events
        Dim oEvent As New Events
        If Seguimiento.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Error Message"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function


#End Region

End Class
#End Region


