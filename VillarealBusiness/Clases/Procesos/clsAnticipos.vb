Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsAnticipos

    Private oAnticipos As VillarrealData.clsAnticipos

    Sub New()
        oAnticipos = New VillarrealData.clsAnticipos
    End Sub


#Region "M�todos"

    Public Function Insertar(ByRef folio As Long, ByVal fecha As DateTime, ByVal cliente As Long, ByVal concepto As String, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal importe As Double, ByVal saldo As Double, ByVal estatus As Char, ByVal vigencia As DateTime, ByVal tipo_venta As Char) As Events
        Insertar = oAnticipos.sp_anticipos_ins(folio, fecha, cliente, concepto, sucursal_factura, serie_factura, folio_factura, importe, saldo, estatus, vigencia, tipo_venta)
    End Function

    Public Function Actualizar(ByVal folio As Long, ByVal fecha As DateTime, ByVal cliente As Long, ByVal concepto As String, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal importe As Double, ByVal saldo As Double, ByVal estatus As Char, ByVal vigencia As DateTime, ByVal tipo_venta As Char) As Events
        Actualizar = oAnticipos.sp_anticipos_upd(folio, fecha, cliente, concepto, sucursal_factura, serie_factura, folio_factura, importe, saldo, estatus, vigencia, tipo_venta)
    End Function

    Public Function Eliminar(ByVal folio As Long) As Events
        Eliminar = oAnticipos.sp_anticipos_del(folio)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oAnticipos.sp_anticipos_sel(folio)
    End Function

    Public Function Listado() As Events
        Listado = oAnticipos.sp_anticipos_grs()
    End Function

    Public Function Lookup(ByVal cliente As Long) As Events
        Lookup = oAnticipos.sp_anticipos_grl(cliente)
    End Function

    Public Function ValidaVale(ByVal folio As Long, ByVal cliente As Long, ByVal importe_pago As Double) As Events
        ValidaVale = oAnticipos.sp_anticipos_validacion_sel(folio, cliente, importe_pago)
    End Function

    Public Function ActualizaFacturaGenerada(ByVal folio As Long, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long) As Events
        ActualizaFacturaGenerada = oAnticipos.sp_anticipos_actualiza_factura_generada_upd(folio, sucursal_factura, serie_factura, folio_factura)
    End Function

    Public Function CancelaAnticipo(ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long) As Events
        CancelaAnticipo = oAnticipos.sp_anticipos_cancelacion_factura_upd(sucursal_factura, serie_factura, folio_factura)
    End Function


#End Region

End Class
