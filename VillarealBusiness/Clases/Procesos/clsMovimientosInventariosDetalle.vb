Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsMovimientosInventariosDetalle
'DATE:		13/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - MovimientosInventariosDetalle"
Public Class clsMovimientosInventariosDetalle
    Private oMovimientosInventariosDetalle As VillarrealData.clsMovimientosInventariosDetalle

#Region "Constructores"
    Sub New()
        oMovimientosInventariosDetalle = New VillarrealData.clsMovimientosInventariosDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    '*/ csequera enero 2008
    Public Function Insertar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal fecha As Date, Optional ByVal costo_flete As Object = 0, Optional ByRef folio_movimiento As Long = 0) As Events
        Insertar = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_ins(oData, sucursal, bodega, concepto, folio, fecha, folio_movimiento, costo_flete)
    End Function
    Public Function Insertar(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal fecha As DateTime, ByVal partida As Long, ByVal articulo As Long, ByVal cantidad As Double, ByVal costo As Double, ByVal importe As Double, ByVal folio_historico_costo As Long, ByRef folio_movimiento As Long, Optional ByVal costo_flete As Object = 0) As Events
        Insertar = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_ins(sucursal, bodega, concepto, folio, fecha, partida, articulo, cantidad, costo, importe, folio_historico_costo, folio_movimiento, costo_flete)
    End Function
    Public Function Actualizar(ByRef oData As DataSet, ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal fecha As Date) As Events
        Actualizar = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_upd(oData, sucursal, bodega, concepto, folio, fecha)
    End Function
    Public Function Actualizar(ByVal sucursal As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal fecha As Date, ByVal partida As Long, ByVal articulo As Long, ByVal cantidad As Double, ByVal costo As Double, ByVal importe As Double, ByVal costo_flete As Double) As Events
        Actualizar = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_upd(sucursal, bodega, concepto, folio, fecha, partida, articulo, cantidad, costo, importe, costo_flete)
    End Function
    '*/ csequera enero 2008
    Public Function Eliminar(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long) As Events
        Eliminar = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_del(bodega, concepto, folio, partida)
    End Function
    Public Function DespliegaDatos(ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal partida As Long) As Events
        DespliegaDatos = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_sel(bodega, concepto, folio, partida)
    End Function
    Public Function Listado(ByVal folio_movimiento As Long, ByVal bodega As String, ByVal concepto As String) As Events
        Listado = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_grs(folio_movimiento, bodega, concepto)
    End Function
    Public Function ListadoMovimientosInventarioAlmacen(ByVal folio_movimiento As Long, ByVal bodega As String, ByVal concepto As String) As Events
        ListadoMovimientosInventarioAlmacen = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_almacen_grs(folio_movimiento, bodega, concepto)
    End Function
    Public Function Validacion(ByVal articulo As Long, ByVal cantidad As Integer, ByVal banSerie As Boolean, ByVal serie As String, ByVal costo As Double, Optional ByVal tipo_concepto As Char = "E") As Events
        Validacion = ValidaArticulo(articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(cantidad)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaSerie(banSerie, serie)
        If Validacion.ErrorFound Then Exit Function

        If tipo_concepto = "E" Then
            Validacion = ValidaCosto(costo)
            If Validacion.ErrorFound Then Exit Function
        End If

    End Function
    Public Function Validacion(ByVal Action As Actions, ByVal articulo As Long, ByVal cantidad As Integer, ByVal banSerie As Boolean, ByVal serie As String, ByVal insertarhis_series As Boolean) As Events
        Validacion = ValidaArticulo(articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(cantidad)
        If Validacion.ErrorFound Then Exit Function

        If Action = Actions.Insert Then
            Validacion = ValidaSerie(banSerie, serie)
            If Validacion.ErrorFound Then Exit Function

            If banSerie = False Then Exit Function

            Validacion = ValidaSerieHisSeries(insertarhis_series)
            If Validacion.ErrorFound Then Exit Function
        End If

    End Function

    Public Function ValidaCosto(ByVal costo As Double) As Events
        Dim oevents As New Events
        If costo < 1 Then
            oevents.Message = "El Costo debe ser mayor a 0"
        End If
        Return oevents
    End Function
    Public Function ValidaArticulo(ByVal articulo As Long) As Events
        Dim oevents As New Events
        If articulo = -1 Then
            oevents.Message = "El Art�culo es requerido"
        End If
        Return oevents
    End Function
    Public Function ValidaCantidad(ByVal cantidad As Integer) As Events
        Dim oevents As New Events
        If cantidad < 1 Then
            oevents.Message = "La Cantidad debe ser mayor a 0"
        End If
        Return oevents
    End Function
    Private Function ValidaSerie(ByVal banserie As Boolean, ByVal serie As String) As Events
        Dim oevents As New Events
        If banserie Then
            If serie.Length = 0 Then
                oevents.Message = "La Serie es requerida"
            End If
        End If
        Return oevents
    End Function
    Public Function ValidaSerieHisSeries(ByVal insertarhis_series As Boolean) As Events
        Dim oEvent As New Events

        If insertarhis_series = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Movimiento del  Numero de Serie no se puede Insertar "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidacantidadArticulos(ByRef oData As DataSet, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal accion As Char, ByVal proceso As String) As Events
        Dim oEvent As New Events
        Dim Response As New Events
        Dim dataset As New DataSet
        Response = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_validar_cantidad(oData, bodega, concepto, folio, accion)
        If Not Response.ErrorFound Then
            dataset = Response.Value
            If accion = "I" Then

                If dataset.Tables(0).Rows(0).Item("fisica") < oData.Tables(0).Rows(0).Item("cantidad") Then

                    Dim cantidad_cadena As String

                    If oData.Tables(0).Rows(0).Item("cantidad").GetType().ToString() = "System.Int32" Then
                        cantidad_cadena = Str(oData.Tables(0).Rows(0).Item("cantidad"))
                    Else
                        cantidad_cadena = oData.Tables(0).Rows(0).Item("cantidad")
                    End If

                    oEvent.Message = "La existencia del art�culo " + CType(oData.Tables(0).Rows(0).Item("articulo"), String) + " es de " + Str(dataset.Tables(0).Rows(0).Item("fisica")) + ", menor que la cantidad de " + proceso + " que es de" + cantidad_cadena
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            ElseIf accion = "U" Then
                If dataset.Tables(0).Rows(0).Item("fisica") < (oData.Tables(0).Rows(0).Item("cantidad") - dataset.Tables(0).Rows(0).Item("cantidad_movimiento")) Then

                    oEvent.Message = "La existencia del art�culo " + oData.Tables(0).Rows(0).Item("articulo") + " es de " + Str(dataset.Tables(0).Rows(0).Item("fisica") + dataset.Tables(0).Rows(0).Item("cantidad_movimiento")) + ", menor que la cantidad de " + proceso + " que es de" + Str(oData.Tables(0).Rows(0).Item("cantidad"))
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            End If
        Else
            oEvent = Response
        End If
        Return oEvent
    End Function


    Public Function ValidacantidadArticulos(ByVal partida As Long, ByVal articulo As Long, ByVal cantidad As Long, ByVal bodega As String, ByVal concepto As String, ByVal folio As Long, ByVal accion As Char, ByVal proceso As String) As Events
        Dim oEvent As New Events
        Dim Response As New Events
        Dim dataset As New DataSet
        Response = oMovimientosInventariosDetalle.sp_movimientos_inventarios_detalle_validar_cantidad(partida, articulo, cantidad, bodega, concepto, folio, accion)
        If Not Response.ErrorFound Then
            dataset = Response.Value
            If accion = "I" Then
                If proceso.ToUpper <> "ENTRADA VISTAS" Then
                    If dataset.Tables(0).Rows(0).Item("fisica") < cantidad Then
                        oEvent.Ex = Nothing
                        oEvent.Message = "La existencia del art�culo " + CType(articulo, String) + " es de " + Str(dataset.Tables(0).Rows(0).Item("fisica")) + ", menor que la cantidad de " + proceso + " que es de" + Str(cantidad)
                        oEvent.Layer = Events.ErrorLayer.BussinessLayer
                        Return oEvent
                    End If
                Else
                    If dataset.Tables(0).Rows(0).Item("vistas") < cantidad Then
                        oEvent.Ex = Nothing
                        oEvent.Message = "La cantidad de vistas del art�culo " + CType(articulo, String) + " es de " + Str(dataset.Tables(0).Rows(0).Item("fisica")) + ", menor que la cantidad de " + proceso + " que es de" + Str(cantidad)
                        oEvent.Layer = Events.ErrorLayer.BussinessLayer
                        Return oEvent
                    End If
                End If
            ElseIf accion = "U" Then
                If dataset.Tables(0).Rows(0).Item("fisica") < (cantidad - dataset.Tables(0).Rows(0).Item("cantidad_movimiento")) Then
                    oEvent.Ex = Nothing
                    oEvent.Message = "La existencia del art�culo " + CType(articulo, String) + " es de " + Str(dataset.Tables(0).Rows(0).Item("fisica") + dataset.Tables(0).Rows(0).Item("cantidad_movimiento")) + ", menor que la cantidad de " + proceso + " que es de" + Str(cantidad)
                    oEvent.Layer = Events.ErrorLayer.BussinessLayer
                    Return oEvent
                End If
            End If
        Else
            oEvent = Response
        End If
        Return oEvent
    End Function


    Public Function ValidaExisteSerie(ByVal clave As String, ByVal Campo As String, ByVal oDetalle As DataSet) As Events
        Return ValidaExistePartida(clave, Campo, oDetalle)
    End Function
    Private Function ValidaExistePartida(ByVal Clave As String, ByVal Campo As String, ByRef oDetalle As DataSet) As Events
        Dim oEvent As New Events
        If oDetalle.Tables(0).Rows.Count > 0 Then
            Dim dv As New DataView
            dv.Table = oDetalle.Tables(0)
            dv.RowFilter = Campo & "='" & Clave.ToString & "' and control in (0,1,2)"
            If dv.Count > 0 Then
                oEvent.Message = "La Serie ya fue Utilizada"
                oEvent.Layer = Dipros.Utils.Events.ErrorLayer.InterfaceLayer
            End If
            dv = Nothing
        End If

        Return oEvent
    End Function

    Public Function RecalculaExistenciasCancelacion(ByVal articulo As Long) As Events
        RecalculaExistenciasCancelacion = oMovimientosInventariosDetalle.util_recalcular_existencias(articulo)
    End Function

#End Region


End Class
#End Region

