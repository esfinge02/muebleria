Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVistasEntradas
'DATE:		27/12/2007 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - VistasEntradas"
Public Class clsVistasEntradas
    Private oVistasEntradas As VillarrealData.clsVistasEntradas

#Region "Constructores"
    Sub New()
        oVistasEntradas = New VillarrealData.clsVistasEntradas
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByRef oData As DataSet) As Events
        Insertar = oVistasEntradas.sp_vistas_entradas_ins(oData)
    End Function
    Public Function Insertar(ByRef folio_vista_entrada As Integer, ByVal folio_vista_salida As Integer, ByVal sucursal As Integer, ByVal fecha As DateTime, ByVal cliente As Integer, ByVal persona_devuelve As String, ByVal persona_captura As String, ByVal observaciones As String) As Events
        Insertar = oVistasEntradas.sp_vistas_entradas_ins(folio_vista_entrada, folio_vista_salida, sucursal, fecha, cliente, persona_devuelve, persona_captura, observaciones)
    End Function

    Public Function Actualizar(ByRef oData As DataSet) As Events
        Actualizar = oVistasEntradas.sp_vistas_entradas_upd(oData)
    End Function

    Public Function Eliminar(ByVal folio_vista_entrada As Long) As Events
        Eliminar = oVistasEntradas.sp_vistas_entradas_del(folio_vista_entrada)
    End Function

    Public Function DespliegaDatos(ByVal folio_vista_entrada As Long) As Events
        DespliegaDatos = oVistasEntradas.sp_vistas_entradas_sel(folio_vista_entrada)
    End Function

    Public Function Listado() As Events
        Listado = oVistasEntradas.sp_vistas_entradas_grs()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Folio_Vista_Salida As Long, ByVal Persona_Devuelve As String, ByVal Articulos As DataTable) As Events
        Validacion = ValidaFolio_Vista_Salida(Folio_Vista_Salida)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaPersona_Devuelve(Persona_Devuelve)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulosSeleccionados(Articulos)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaFolio_Vista_Salida(ByVal Folio_Vista_Salida As Long) As Events
        Dim oEvent As New Events
        If Folio_Vista_Salida <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio de Salida es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaPersona_Devuelve(ByVal Persona_Devuelve As String) As Events
        Dim oEvent As New Events
        If Persona_Devuelve.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Persona que Devuelve es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaArticulosSeleccionados(ByVal Articulos As DataTable) As Events
        Dim oEvent As New Events
        If Articulos.Select("seleccionar = True ").Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

#End Region

End Class
#End Region


