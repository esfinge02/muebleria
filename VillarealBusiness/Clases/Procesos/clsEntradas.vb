Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsEntradas
'DATE:		04/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Entradas"
Public Class clsEntradas
	Private oEntradas As VillarrealData.clsEntradas

#Region "Constructores"
	Sub New()
		oEntradas = New VillarrealData.clsEntradas
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
	Public Function Insertar(ByRef oData As DataSet) As Events
		Insertar = oEntradas.sp_entradas_ins(oData)
	End Function

	Public Function Actualizar(ByRef oData As DataSet) As Events
		Actualizar = oEntradas.sp_entradas_upd(oData)
	End Function

	Public Function Eliminar(ByVal folio as long) As Events
		Eliminar = oEntradas.sp_entradas_del(folio)
    End Function

    Public Function EliminarCosteada(ByVal folio As Long) As Events
        EliminarCosteada = oEntradas.sp_eliminar_entrada_costeada_del(folio)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oEntradas.sp_entradas_sel(folio)
    End Function

    Public Function Listado() As Events
        Listado = oEntradas.sp_entradas_grs()
    End Function

    Public Function Lookup(Optional ByVal bodega As String = "-1", Optional ByVal costeada As Boolean = False, Optional ByVal proveedor As Long = -1) As Events
        Lookup = oEntradas.sp_entradas_grl(bodega, costeada, proveedor)
    End Function

    Public Function DespliegaDatosArticulos(ByVal entrada As Long, ByVal departamento As Long, ByVal grupo As Long, ByVal bodega As String) As Events
        DespliegaDatosArticulos = oEntradas.sp_articulos_entrada_sel(entrada, departamento, grupo, bodega)
    End Function

    Public Function DespliegaDatosDepartamentoArticulos(ByVal entrada As Long) As Events
        DespliegaDatosDepartamentoArticulos = oEntradas.sp_departamento_articulos_entrada_sel(entrada)
    End Function
    Public Function DespliegaDatosGrupoArticulos(ByVal entrada As Long, ByVal departamento As Long) As Events
        DespliegaDatosGrupoArticulos = oEntradas.sp_grupo_articulos_entrada_sel(entrada, departamento)
    End Function
    Public Function DespliegaArticulosParaSurtimiento(ByVal entrada As Long) As Events
        DespliegaArticulosParaSurtimiento = oEntradas.sp_entradas_articulos_surtimiento(entrada)
    End Function



    'Public Function TraerCondicionesPagoFechas(ByVal fecha As Date, ByVal total As Long, ByVal plazo1 As Long, ByVal plazo2 As Long, ByVal plazo3 As Long, ByVal plazo4 As Long, ByVal plazo5 As Long, ByVal descuentoProntoPago1 As Long, ByVal descuentoProntoPago2 As Long, ByVal descuentoProntoPago3 As Long, ByVal descuentoProntoPago4 As Long, ByVal descuentoProntoPago5 As Long) As Events
    '    TraerCondicionesPagoFechas = oEntradas.sp_entradas_traer_condiciones_pago_fechas(fecha, total, plazo1, plazo2, plazo3, plazo4, plazo5, descuentoProntoPago1, descuentoProntoPago2, descuentoProntoPago3, descuentoProntoPago4, descuentoProntoPago5)
    'End Function




    Public Function Validacion(ByVal Action As Actions, ByVal Proveedor As Long, ByVal orden_compra As Long, ByVal Bodega As String, ByVal cantidad_articulos As Long, ByVal odata_articulos As DataSet, ByVal fecha As String, ByVal referencia As String, ByVal FolioFactura As Double, ByVal FechaFactura As String, ByVal Flete As Object, ByVal Seguro As Object, ByVal Maniobras As Object, ByVal OtrosGastos As Object, ByVal SubtotalFlete As Object, ByVal IVAFlete As String, ByVal IvaRetenido As Object, ByVal TotalFlete As Object) As Events

        Select Case Action
            Case Actions.Insert, Actions.Update
                Validacion = ValidaProveedor(Proveedor)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaOrdenCompra(orden_compra)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaBodega(Bodega)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaReferencia(referencia)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaArticulos(cantidad_articulos)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaCantidades(odata_articulos)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaFecha(fecha)
                If Validacion.ErrorFound Then Exit Function


                'Validacion = ValidaDescuentosProntoPago(descuentoProntoPago1)
                'If Validacion.ErrorFound Then Exit Function

                'Validacion = ValidaDescuentosProntoPago(descuentoProntoPago2)
                'If Validacion.ErrorFound Then Exit Function

                'Validacion = ValidaDescuentosProntoPago(descuentoProntoPago3)
                'If Validacion.ErrorFound Then Exit Function

                'Validacion = ValidaDescuentosProntoPago(descuentoProntoPago4)
                'If Validacion.ErrorFound Then Exit Function

                'Validacion = ValidaDescuentosProntoPago(descuentoProntoPago5)
                'If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaFolioFactura(FolioFactura)
                If Validacion.ErrorFound Then Exit Function


                Validacion = ValidaFechaFactura(FechaFactura)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaFlete(Flete)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaSeguro(Seguro)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaManiobras(Maniobras)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaOtrosGastos(OtrosGastos)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaSubtotalFlete(SubtotalFlete)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaIVAFlete(IVAFlete)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaIvaRetenido(IvaRetenido)
                If Validacion.ErrorFound Then Exit Function

                Validacion = ValidaTotalFlete(TotalFlete)
                If Validacion.ErrorFound Then Exit Function

            Case Actions.Delete
                Validacion = New Events

        End Select

    End Function
    'Public Function Validacion(ByVal ConceptoEntrada As String, ByVal ConceptoCancelacionEntrada As String, ByVal Concepto_cxp As String) As Events
    '    Validacion = ValidaConceptoEntrada(ConceptoEntrada)
    '    If Validacion.ErrorFound Then Exit Function
    '    Validacion = ValidaConceptoCancelacionEntrada(ConceptoCancelacionEntrada)
    '    If Validacion.ErrorFound Then Exit Function
    '    Validacion = ValidaConcepto_cxp(Concepto_cxp)
    '    If Validacion.ErrorFound Then Exit Function
    'End Function
    'Public Function ValidaConceptoEntrada(ByVal ConceptoEntrada As String) As Events
    '    Dim oEvent As New Events
    '    If ConceptoEntrada.Length <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "El Concepto de Entrada es Requerido"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    'Public Function ValidaConceptoCancelacionEntrada(ByVal CancelacionEntrada As String) As Events
    '    Dim oEvent As New Events
    '    If CancelacionEntrada.Length <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "El Concepto de Cancelacion de Entrada es Requerido"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    'Public Function ValidaConcepto_cxp(ByVal Concepto_cxp As String) As Events
    '    Dim oEvent As New Events
    '    If Concepto_cxp.Length <= 0 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "El Concepto de CXP es Requerido"
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '        Return oEvent
    '    End If
    '    Return oEvent
    'End Function
    Public Function ValidaProveedor(ByVal Proveedor As Long) As Events
        Dim oEvent As New Events
        If Proveedor <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Proveedor es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaOrdenCompra(ByVal orden_compra As Long) As Events
        Dim oEvent As New Events
        If orden_compra <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Orden de Compra es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaReferencia(ByVal referencia As String) As Events
        Dim oEvent As New Events
        If referencia.Length <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Referencia es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Public Function ValidaBodega(ByVal Bodega As String) As Events
        Dim oEvent As New Events
        If Bodega.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Bodega es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Private Function ValidaArticulos(ByVal articulos As Long) As Events
        Dim oEvent As New Events
        If articulos = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Al menos un Articulo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
    Private Function ValidaCantidades(ByVal odata As DataSet) As Events
        Dim oEvent As New Events
        Dim row() As DataRow

        'NUEVA VALIDACION AL 17/AGO/2007
        row = odata.Tables(0).Select("cantidad=0")
        If odata.Tables(0).Rows.Count = row.Length Then
            oEvent.Ex = Nothing
            oEvent.Message = "Es necesario capturar por lo menos una partida para generar la Entrada al Almacén"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent

        'ESTE ES LA VALIDACION ANTERIOR AL 17/AGO/2007
        'row = odata.Tables(0).Select("cantidad > 0")

        '    If row.Length = 0 Then
        '        oEvent.Ex = Nothing
        '        oEvent.Message = "Al menos un Articulo con Cantidad Recibida es Requerido"
        '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
        '    End If
        '    Return oEvent
    End Function

    Private Function ValidaFecha(ByVal fecha As String) As Events
        Dim oEvent As New Events
        If fecha = Nothing Then
            oEvent.Ex = Nothing
            oEvent.Message = "La fecha es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function

    'Private Function ValidaDescuentosProntoPago(ByVal descuentoProntoPago As Long) As Events
    '    Dim oEvent As New Events
    '    If descuentoProntoPago > 100 Then
    '        oEvent.Ex = Nothing
    '        oEvent.Message = "El Descuento por Pronto Pago debe ser Menor o Igual a 100% "
    '        oEvent.Layer = Events.ErrorLayer.BussinessLayer
    '    End If
    '    Return oEvent
    'End Function

    Private Function ValidaFolioFactura(ByVal FolioFactura As Long) As Events
        Dim oEvent As New Events
        If IsNumeric(FolioFactura) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Folio de la Factura es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If FolioFactura < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El Folio de la Factura es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaFechaFactura(ByVal FechaFactura As String) As Events
        Dim oEvent As New Events
        If FechaFactura = Nothing Or IsDate(FechaFactura) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Fecha de la Factura es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If
        Return oEvent
    End Function
    Private Function ValidaFlete(ByVal Flete As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(Flete) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Flete es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If Flete < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El Flete es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaSeguro(ByVal Seguro As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(Seguro) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Seguro es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If Seguro < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El Seguro es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaManiobras(ByVal Maniobras As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(Maniobras) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El campo Maniobras es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If Maniobras < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El campo Maniobras es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaOtrosGastos(ByVal OtrosGastos As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(OtrosGastos) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El campo Otros Gastos es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If OtrosGastos < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El campo Otros Gastos es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaSubtotalFlete(ByVal SubtotalFlete As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(SubtotalFlete) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Subtotal del Flete es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If SubtotalFlete < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El Subtotal del Flete es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaIVAFlete(ByVal IVAFlete As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(IVAFlete) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El IVA del Flete es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If IVAFlete < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El IVA del Flete es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaIvaRetenido(ByVal IvaRetenido As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(IvaRetenido) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Iva Retenido es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If IvaRetenido < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El Iva Retenido es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function
    Private Function ValidaTotalFlete(ByVal TotalFlete As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(TotalFlete) = False Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Total del Flete es Requerido "
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        Else
            If TotalFlete < 0 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El Total del Flete es Requerido "
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
            End If
        End If
        Return oEvent
    End Function



#End Region

End Class
#End Region


