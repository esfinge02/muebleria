Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsPagoFacturasProveedoresDetalle
'DATE:		04/10/2006 12:00:00 a.m.
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - PagoFacturasProveedoresDetalle"
Public Class clsPagoFacturasProveedoresDetalle
    Private oPagoFacturasProveedoresDetalle As VillarrealData.clsPagoFacturasProveedoresDetalle

#Region "Constructores"
    Sub New()
        oPagoFacturasProveedoresDetalle = New VillarrealData.clsPagoFacturasProveedoresDetalle
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function Insertar(ByVal folio As Long, ByVal concepto As String, ByRef partida As Long, ByVal folio_factura As Long, ByVal concepto_factura As String, ByVal importe As Double) As Events
        Insertar = oPagoFacturasProveedoresDetalle.sp_pago_facturas_proveedores_detalle_ins(folio, concepto, partida, folio_factura, concepto_factura, importe)
    End Function

    Public Function Actualizar(ByVal folio As Long, ByVal concepto As String, ByRef partida As Long, ByVal folio_factura As Long, ByVal concepto_factura As String, ByVal importe As Double) As Events
        Actualizar = oPagoFacturasProveedoresDetalle.sp_pago_facturas_proveedores_detalle_upd(folio, concepto, partida, folio_factura, concepto_factura, importe)
    End Function

    Public Function Eliminar(ByVal folio As Long, ByVal concepto As String, ByVal partida As Long) As Events
        Eliminar = oPagoFacturasProveedoresDetalle.sp_pago_facturas_proveedores_detalle_del(folio, concepto, partida)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long, ByVal concepto As String, ByVal partida As Long) As Events
        DespliegaDatos = oPagoFacturasProveedoresDetalle.sp_pago_facturas_proveedores_detalle_sel(folio, concepto, partida)
    End Function

    Public Function Listado(ByVal folio As Long, ByVal concepto As String) As Events
        Listado = oPagoFacturasProveedoresDetalle.sp_pago_facturas_proveedores_detalle_grs(folio, concepto)
    End Function

    Public Function Validacion(ByVal Action As Actions) As Events

    End Function



#End Region

End Class
#End Region


