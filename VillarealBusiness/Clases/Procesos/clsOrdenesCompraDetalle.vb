Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsOrdenesCompraDetalle
'DATE:		06/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - OrdenesCompraDetalle"
Public Class clsOrdenesCompraDetalle
	Private oOrdenesCompraDetalle As VillarrealData.clsOrdenesCompraDetalle

#Region "Constructores"
	Sub New()
		oOrdenesCompraDetalle = New VillarrealData.clsOrdenesCompraDetalle
	End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Insertar(ByRef oData As DataSet, ByVal orden As Long, ByVal bodega As String, ByVal sobre_pedido As Boolean) As Events
        Insertar = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_ins(oData, orden, bodega, sobre_pedido)
    End Function

    Public Function Actualizar(ByRef oData As DataSet, ByVal orden As Long, ByVal bodega As String, ByVal sobre_pedido As Boolean) As Events
        Actualizar = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_upd(oData, orden, bodega, sobre_pedido)
    End Function

    Public Function Eliminar(ByVal orden As Long, ByVal partida As Long, ByVal cantidad As Long, ByVal articulo As Long, ByVal bodega As String, ByVal sobre_pedido As Boolean) As Events
        Eliminar = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_del(orden, partida, cantidad, articulo, bodega, sobre_pedido)
    End Function

    Public Function DespliegaDatos(ByVal orden As Long) As Events
        DespliegaDatos = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_grs(orden)
    End Function

    Public Function Listado() As Events
        'Listado = oOrdenesCompraDetalle.sp_ordenes_compra_detalle_grs()
    End Function

    Public Function CambiarBodega(ByVal bodega_anterior As String, ByVal bodega_actualizada As String, ByVal articulo As Long, ByVal cantidad_anterior As Long, ByVal orden As Long, ByVal sobre_pedido As Boolean) As Events
        CambiarBodega = oOrdenesCompraDetalle.sp_ordenes_compra_bodegas(bodega_anterior, bodega_actualizada, articulo, cantidad_anterior, orden, sobre_pedido)
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Departamento As Long, ByVal Grupo As Long, ByVal Articulo As Long, ByVal Cantidad As Double) As Events
        Validacion = ValidaDepartamento(Departamento)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaGrupo(Grupo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaArticulo(Articulo)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaCantidad(Cantidad)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaDepartamento(ByVal Departamento As Long) As Events
        Dim oEvent As New Events
        If Departamento <= 0 Or IsDBNull(Departamento) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Departamento es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaGrupo(ByVal Grupo As Long) As Events
        Dim oEvent As New Events
        If Grupo <= 0 Or IsDBNull(Grupo) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Grupo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaArticulo(ByVal Articulo As Long) As Events
        Dim oEvent As New Events
        If Articulo <= 0 Or IsDBNull(Articulo) Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Artículo es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaCantidad(ByVal Cantidad As Double) As Events
        Dim oEvent As New Events
        If Cantidad <= 0 Or IsDBNull(Cantidad) Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Cantidad es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaExisteArticulo(ByVal clave As String, ByVal Campo As String, ByVal oDetalle As DataSet) As Events
        Return ValidaExistePartida(clave, Campo, oDetalle)
    End Function
    Private Function ValidaExistePartida(ByVal Clave As String, ByVal Campo As String, ByRef oDetalle As DataSet) As Events
        Dim oEvent As New Events
        If oDetalle.Tables(0).Rows.Count > 0 Then
            Dim dv As New DataView
            dv.Table = oDetalle.Tables(0)
            dv.RowFilter = Campo & "='" & Clave.ToString & "' and control in (0,1,2)"
            If dv.Count > 0 Then
                oEvent.Message = "El Articulo ya Existe"
                oEvent.Layer = Dipros.Utils.Events.ErrorLayer.InterfaceLayer
            End If
            dv = Nothing
        End If

        Return oEvent
    End Function
    Public Function TraerArticulosOC(ByVal proveedor As Long, ByVal sucursal As Long) As Events
        TraerArticulosOC = Me.oOrdenesCompraDetalle.sp_traerarticulos_oc(proveedor, sucursal)
    End Function
    Public Function ArticulosSobrePedidoOC(ByVal proveedor As Long, ByVal sucursal As Long) As Events
        ArticulosSobrePedidoOC = Me.oOrdenesCompraDetalle.sp_articulos_sobre_pedido_oc(proveedor, sucursal)
    End Function
    Public Function TraerDatosVentaArticulo(ByVal articulo As Long) As Events
        TraerDatosVentaArticulo = Me.oOrdenesCompraDetalle.sp_ventas_articulo_oc(articulo)
    End Function

    Public Function ActualizarVentaDetalleSobrePedidoOC(ByVal orden As Long, ByVal folios_venta_Detalle As String) As Events
        ActualizarVentaDetalleSobrePedidoOC = oOrdenesCompraDetalle.sp_actualiza_venta_detalle_sobre_pedido_oc(orden, folios_venta_Detalle)
    End Function

#End Region

End Class
#End Region


