Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsValesDetalle

    Private oValesDetalle As VillarrealData.clsValesDetalle

    Sub New()
        oValesDetalle = New VillarrealData.clsValesDetalle
    End Sub

#Region "M�todos"


    Public Function Insertar(ByVal folio As Long, ByVal sucursal_recibo As String, ByVal concepto_recibo As String, ByVal serie_recibo As String, ByVal folio_recibo As Long, ByVal fecha As DateTime, ByVal importe As Double) As Events
        Insertar = oValesDetalle.sp_vales_detalle_ins(folio, sucursal_recibo, concepto_recibo, serie_recibo, folio_recibo, fecha, importe)
    End Function
    Public Function Actualizar(ByVal folio As Long, ByVal sucursal_recibo As String, ByVal concepto_recibo As String, ByVal serie_recibo As String, ByVal folio_recibo As Long, ByVal fecha As DateTime, ByVal importe As Double) As Events
        Actualizar = oValesDetalle.sp_vales_detalle_upd(folio, sucursal_recibo, concepto_recibo, serie_recibo, folio_recibo, fecha, importe)
    End Function
    Public Function Eliminar(ByVal folio As Long, ByVal sucursal_recibo As String, ByVal concepto_recibo As String, ByVal serie_recibo As String, ByVal folio_recibo As Long) As Events
        Eliminar = oValesDetalle.sp_vales_detalle_del(folio, sucursal_recibo, concepto_recibo, serie_recibo, folio_recibo)
    End Function
    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oValesDetalle.sp_vales_detalle_grs(folio)
    End Function

#End Region

End Class
