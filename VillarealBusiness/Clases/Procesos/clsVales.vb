Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsVales

    Private oVales As VillarrealData.clsVales

    Sub New()
        oVales = New VillarrealData.clsVales
    End Sub

#Region "M�todos"

    Public Function Insertar(ByRef folio As Long, ByVal fecha As DateTime, ByVal cliente As Long, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal importe As Double, ByVal saldo As Double, ByVal estatus As Char, ByVal vigencia As DateTime, ByVal tipo_vale As Long, ByVal convenio As Long, ByVal anio_convenio As Long, ByVal observaciones As String, ByVal nombre_titular As String, ByVal nombre_autoriza_canjear As String) As Events
        Insertar = oVales.sp_vales_ins(folio, fecha, cliente, sucursal_factura, serie_factura, folio_factura, importe, saldo, estatus, vigencia, tipo_vale, convenio, anio_convenio, observaciones, nombre_titular, nombre_autoriza_canjear)
    End Function

    Public Function Actualizar(ByVal folio As Long, ByVal fecha As DateTime, ByVal cliente As Long, ByVal sucursal_factura As Long, ByVal serie_factura As String, ByVal folio_factura As Long, ByVal importe As Double, ByVal saldo As Double, ByVal estatus As Char, ByVal vigencia As DateTime, ByVal tipo_vale As Long, ByVal convenio As Long, ByVal anio_convenio As Long, ByVal observaciones As String, ByVal nombre_titular As String, ByVal nombre_autoriza_canjear As String) As Events
        Actualizar = oVales.sp_vales_upd(folio, fecha, cliente, sucursal_factura, serie_factura, folio_factura, importe, saldo, estatus, vigencia, tipo_vale, convenio, anio_convenio, observaciones, nombre_titular, nombre_autoriza_canjear)
    End Function

    Public Function Eliminar(ByVal folio As Long) As Events
        Eliminar = oVales.sp_vales_del(folio)
    End Function

    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oVales.sp_vales_sel(folio)
    End Function

    Public Function Listado() As Events
        Listado = oVales.sp_vales_grs()
    End Function

    Public Function Lookup() As Events
        Lookup = oVales.sp_vales_grl()
    End Function

    Public Function Validacion(ByVal Action As Actions, ByVal Cliente As Long, ByVal TipoVale As Long, ByVal ConceptoVale As String, ByVal NombreTitular As String, ByVal SucursalVenta As Long, _
    ByVal SerieVenta As String, ByVal FolioVenta As Long, ByVal Convenio As Long, ByVal AnioConvenio As Long, ByVal Estatus As Char, ByVal CantidadValesDetalleAbonos As Long, _
    ByVal UsuarioEdita As String, ByVal Importe As Double) As Events
        Validacion = ValidaCliente(Cliente)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaTipoVale(TipoVale)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaImporte(Importe)
        If Validacion.ErrorFound Then Exit Function

        Select Case Action
            Case Actions.Insert

                Select Case ConceptoVale
                    Case "DE"
                        Validacion = ValidaNombreTitular(NombreTitular)
                        If Validacion.ErrorFound Then Exit Function
                        Validacion = ValidaDatosVenta(SucursalVenta, SerieVenta, FolioVenta, Cliente)
                        If Validacion.ErrorFound Then Exit Function
                    Case "CO"
                        Validacion = ValidaNombreTitular(NombreTitular)
                        If Validacion.ErrorFound Then Exit Function
                        Validacion = ValidaDatosConvenio(Convenio, AnioConvenio)
                        If Validacion.ErrorFound Then Exit Function
                    Case "VE"
                        Validacion = ValidaDatosVenta(SucursalVenta, SerieVenta, FolioVenta, Cliente)
                        If Validacion.ErrorFound Then Exit Function
                End Select


            Case Actions.Update
                Validacion = ValidaCancelacion(Estatus, CantidadValesDetalleAbonos, UsuarioEdita)
        End Select
    End Function

    Private Function ValidaNombreTitular(ByVal NombreTitular As String) As Events
        Dim oEvent As New Events
        If NombreTitular.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El nombre del Titular es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If

        Return oEvent
    End Function

    Private Function ValidaImporte(ByVal Importe As Double) As Events
        Dim oEvent As New Events
        If Importe <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Importe  es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
        End If

        Return oEvent
    End Function
    Private Function ValidaDatosVenta(ByVal SucursalVenta As Long, ByVal SerieVenta As String, ByVal FolioVenta As Long, ByVal Cliente As Long) As Events
        Dim oEvent As New Events

        If SucursalVenta < 1 Then
            oEvent.Message = "La Sucursal de la Venta es Requerida"
        Else
            If SerieVenta.Trim.Length = 0 Then
                oEvent.Message = "La Serie de la Venta es Requerida"
            Else
                If FolioVenta < 1 Then
                    oEvent.Message = "El Folio de la Venta es Requerido"
                Else
                    oEvent = Me.oVales.sp_valida_factura_cliente_sel(Cliente, SucursalVenta, SerieVenta, FolioVenta)
                    If oEvent.Value = 0 Then
                        oEvent.Message = "La factura no es V�lida para el Cliente"
                    End If
                End If
            End If
        End If

        Return oEvent
    End Function
    Private Function ValidaDatosConvenio(ByVal Convenio As Long, ByVal Anio_Convenio As Long) As Events
        Dim oEvent As New Events
        If Convenio < 1 Then
            oEvent.Message = "El Convenio es Requerido"
        Else
            If Anio_Convenio < 0 Then
                oEvent.Message = "El a�o del Convenio es Requerido"
            End If
        End If


        Return oEvent
    End Function
    Private Function ValidaCancelacion(ByVal estatus As Char, ByVal CantidadValesDetalle As Long, ByVal UsuarioEdicion As String) As Events
        Dim oEvent As New Events
        If estatus = "C" Then
            If UsuarioEdicion = "SUPER" Then
                If CantidadValesDetalle > 0 Then
                    oEvent.Message = "No se puede Cancelar el Vale por que tiene Abonos Relacionados"
                End If
            Else
                oEvent.Message = "Solo el Administrador del Sistema puede Cancelar el Vale"
            End If
        End If
        Return oEvent
    End Function
    Public Function ValidaVale(ByVal folio As Long, ByVal cliente As Long, ByVal importe_pago As Double) As Events
        ValidaVale = oVales.sp_vales_validacion_sel(folio, cliente, importe_pago)
    End Function
    Private Function ValidaCliente(ByVal cliente As Long) As Events
        Dim oEvent As New Events
        If cliente <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Cliente es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function

    Private Function ValidaTipoVale(ByVal TipoVale As Long) As Events
        Dim oEvent As New Events
        If TipoVale <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Tipo de Vale es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function



#End Region

End Class
