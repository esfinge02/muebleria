Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsAnticiposDetalle

    Private oAnticiposDetalle As VillarrealData.clsAnticiposDetalle

    Sub New()
        oAnticiposDetalle = New VillarrealData.clsAnticiposDetalle
    End Sub

#Region "M�todos"


    Public Function Insertar(ByVal folio As Long, ByVal sucursal_factura_aplicado As Long, ByVal serie_factura_aplicado As String, ByVal folio_factura_aplicado As Long, ByVal fecha As DateTime, ByVal importe As Double) As Events
        Insertar = oAnticiposDetalle.sp_anticipos_detalle_ins(folio, sucursal_factura_aplicado, serie_factura_aplicado, folio_factura_aplicado, fecha, importe)
    End Function
    Public Function Actualizar(ByVal folio As Long, ByVal sucursal_factura_aplicado As String, ByVal serie_factura_aplicado As String, ByVal folio_factura_aplicado As Long, ByVal fecha As DateTime, ByVal importe As Double) As Events
        Actualizar = oAnticiposDetalle.sp_anticipos_detalle_upd(folio, sucursal_factura_aplicado, serie_factura_aplicado, folio_factura_aplicado, fecha, importe)
    End Function
    Public Function Eliminar(ByVal folio As Long, ByVal sucursal_factura_aplicado As String, ByVal serie_factura_aplicado As String, ByVal folio_factura_aplicado As Long) As Events
        Eliminar = oAnticiposDetalle.sp_anticipos_detalle_del(folio, sucursal_factura_aplicado, serie_factura_aplicado, folio_factura_aplicado)
    End Function
    Public Function DespliegaDatos(ByVal folio As Long) As Events
        DespliegaDatos = oAnticiposDetalle.sp_anticipos_detalle_grs(folio)
    End Function

    Public Function DespliegaDatosVentas(ByVal sucursal_factura_aplicado As String, ByVal serie_factura_aplicado As String, ByVal folio_factura_aplicado As Long) As Events
        DespliegaDatosVentas = oAnticiposDetalle.sp_anticipos_detalle_ventas_grs(sucursal_factura_aplicado, serie_factura_aplicado, folio_factura_aplicado)
    End Function
#End Region
End Class
