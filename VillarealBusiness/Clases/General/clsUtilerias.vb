Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class clsUtilerias
    Private oUtilerias As VillarrealData.clsUtilerias

#Region "Constructores"
    Sub New()
        oUtilerias = New VillarrealData.clsUtilerias
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "M�todos"
    Public Function ChecarHis_Series(ByVal articulo As Long, ByVal numeroserie As String, ByVal tipo_movimiento As Char, ByVal bodega As String) As Events
        ChecarHis_Series = oUtilerias.checarHis_Series(articulo, numeroserie, tipo_movimiento, bodega)
    End Function

    Public Function obtenerSaldosVencidosInteresesCliente(ByVal cliente As Long, ByVal fecha As DateTime) As Events
        obtenerSaldosVencidosInteresesCliente = oUtilerias.sp_obtener_saldos_vencidos_interese_cliente_grs(cliente, fecha)
    End Function

    Public Function Obtener_Domicilios_Ventas_Repartos_Cliente(ByVal cliente As Long) As Events
        Obtener_Domicilios_Ventas_Repartos_Cliente = oUtilerias.sp_domicilios_ventas_repartos_cliente_grs(cliente)
    End Function
    Public Function AbonosClientesJuridico(ByVal abogado As Long, ByVal cliente As Long) As Events
        AbonosClientesJuridico = oUtilerias.sp_abonos_de_cliente_en_juridico(abogado, cliente)
    End Function
    Public Function Tablas(ByVal tipo As String) As Events
        Tablas = oUtilerias.sp_tablas(tipo)
    End Function

    Public Function Campos_Tablas(ByVal id_tabla As Long) As Events
        Campos_Tablas = oUtilerias.sp_campos_tablas(id_tabla)
    End Function


    Public Function FechaServidor() As Events
        FechaServidor = oUtilerias.FechaServidor
    End Function

    Public Shared Function ValidaUltimosDigitos(ByVal ultimos_digitos As String) As Events
        Dim oEvent As New Events
        If CLng(ultimos_digitos) <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "Los Ultimos d�gitos son Requeridos"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        Else
            If ultimos_digitos.Length < 4 Then
                oEvent.Ex = Nothing
                oEvent.Message = "Los Ultimos 4 d�gitos son Requeridos"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
        End If
        Return oEvent

    End Function

    Public Shared Function ValidaFormaPago(ByVal FormaPago As Long) As Events
        Dim oEvent As New Events
        If FormaPago <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Forma de Pago es requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
#End Region
End Class
