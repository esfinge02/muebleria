Imports Dipros.Utils
Imports Dipros.Utils.Common

'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:	clsVariables
'DATE:		09/03/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
#Region "DIPROS Systems, BusinessEnvironment - Variables"
Public Class clsVariables
    Private oVariables As VillarrealData.clsVariables

    Enum tipo_dato As Short
        Varchar = 1
        Entero = 2
        Float = 3
        Bit = 4
    End Enum

#Region "Constructores"
    Sub New()
        oVariables = New VillarrealData.clsVariables
    End Sub
#End Region

#Region "Propiedades"
#End Region

#Region "Métodos"
    Public Function Actualizar(ByRef oData As DataSet, ByVal InicioComisionesCobrador As DateTime, ByVal FinComisionesCobrador As DateTime) As Events
        Actualizar = oVariables.sp_variables_upd(oData, InicioComisionesCobrador, FinComisionesCobrador)
    End Function

    Public Function DespliegaDatos() As Events
        DespliegaDatos = oVariables.sp_variables_sel()
    End Function

    Public Function LookupPlantillasReportesEtiquetasPrecios() As Events
        LookupPlantillasReportesEtiquetasPrecios = oVariables.sp_reportes_ventas_etiquetas_grs()
    End Function

    Public Function LookupPlantillasReportesEtiquetasPreciosSinAsignar() As Events
        LookupPlantillasReportesEtiquetasPreciosSinAsignar = oVariables.sp_reportes_ventas_etiquetas_grs()
    End Function


    Public Function TraeDatos(ByVal datafield As String, ByVal type As tipo_dato) As Object

        If CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows.Count > 0 Then

            Select Case type
                Case tipo_dato.Entero, tipo_dato.Float
                    TraeDatos = CType(IIf(IsDBNull(CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0)), 0, CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0)), Long)
                Case tipo_dato.Varchar
                    'TraeDatos = CType(IIf(IsDBNull(CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0)), 0, CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0)), Long)
                    TraeDatos = CType(IIf(IsDBNull(CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0)), "", CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0)), String)
                    'TraeDatos = CType(CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0), String)
                Case tipo_dato.Bit
                    TraeDatos = CType(IIf(IsDBNull(CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0)), False, CType(oVariables.sp_variables_lkp(datafield).Value, DataSet).Tables(0).Rows(0).Item(0)), Boolean)
            End Select

        Else
            Select Case type
                Case tipo_dato.Entero, tipo_dato.Float
                    TraeDatos = -1
                Case tipo_dato.Varchar
                    TraeDatos = ""
            End Select

        End If
    End Function


    Public Function Validacion(ByVal Action As Actions, ByVal Empresa As String, ByVal Direccion As String, ByVal Telefonos As String, ByVal enganche_en_menos As Object, ByVal hora_inicio_comisiones_cobrador As DateTime, ByVal hora_fin_comisiones_cobrador As DateTime, ByVal formapago As Long) As Events
        Validacion = ValidaEmpresa(Empresa)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaDireccion(Direccion)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaTelefonos(Telefonos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaEngancheEnMenos(enganche_en_menos)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaHorarioComisionesACobradores(hora_inicio_comisiones_cobrador, hora_fin_comisiones_cobrador)
        If Validacion.ErrorFound Then Exit Function
        Validacion = ValidaFormaPagoContado(formapago)
        If Validacion.ErrorFound Then Exit Function
    End Function

    Public Function ValidaEmpresa(ByVal Empresa As String) As Events
        Dim oEvent As New Events
        If Empresa.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Nombre de la Empresa es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaDireccion(ByVal Direccion As String) As Events
        Dim oEvent As New Events
        If Direccion.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Dirección de la Empresa es Requerida"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaTelefonos(ByVal Telefono As String) As Events
        Dim oEvent As New Events
        If Telefono.Trim.Length = 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "El Teléfono de la Empresa es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaEngancheEnMenos(ByVal enganche_en_menos As Object) As Events
        Dim oEvent As New Events
        If IsNumeric(enganche_en_menos) Then
            If CType(enganche_en_menos, Double) > 100 Then
                oEvent.Ex = Nothing
                oEvent.Message = "El valor de enganche en menos debe ser menor  que 100"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaHorarioComisionesACobradores(ByVal hora_inicio_comisiones_cobrador As DateTime, ByVal hora_fin_comisiones_cobrador As DateTime) As Events
        Dim oEvent As New Events
        If IsDate(hora_inicio_comisiones_cobrador) And IsDate(hora_fin_comisiones_cobrador) Then
            If CType(hora_inicio_comisiones_cobrador, DateTime) > CType(hora_fin_comisiones_cobrador, DateTime) Then
                oEvent.Ex = Nothing
                oEvent.Message = "La hora de cierre de comisiones al cobrador debe ser mayor a la hora de inicio de comisiones a cobrador"
                oEvent.Layer = Events.ErrorLayer.BussinessLayer
                Return oEvent
            End If
            Return oEvent
        End If
        Return oEvent
    End Function
    Public Function ValidaFormaPagoContado(ByVal FormaPago As Long) As Events
        Dim oEvent As New Events
        If FormaPago <= 0 Then
            oEvent.Ex = Nothing
            oEvent.Message = "La Forma de Pago de Contado es Requerido"
            oEvent.Layer = Events.ErrorLayer.BussinessLayer
            Return oEvent
        End If
        Return oEvent

    End Function


    Public Function ActualizarComisionContados(ByVal porcentaje_comision_contados As Decimal) As Events
        ActualizarComisionContados = oVariables.sp_variables_porcentaje_comision_contados_upd(porcentaje_comision_contados)
    End Function

#End Region

End Class
#End Region


