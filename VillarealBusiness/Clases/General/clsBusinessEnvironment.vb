'GENERATE CODE BY TINGeneraNet Ver. 3.0
'DESCRIPTION:   BusinessEnvironment
'DATE:         18/02/2006 00:00:00
'----------------------------------------------------------------------------------------------------------------
Public Class BusinessEnvironment
    Public oConnection As Dipros.Data.Data
    Shared Property Connection() As Dipros.Data.Data
        Get
            Connection = VillarrealData.DataEnvironment.Connection
        End Get
        Set(ByVal Value As Dipros.Data.Data)
            villarrealData.DataEnvironment.Connection = Value
        End Set
    End Property
End Class
