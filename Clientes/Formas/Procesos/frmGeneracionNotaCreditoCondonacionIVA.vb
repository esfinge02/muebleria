Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmGeneracionNotaCreditoCondonacionIVA
    Inherits Dipros.Windows.frmTINForm


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolioFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcEnajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfactor_enajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClaveCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcIvaDesglosado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents tlbNotasCredito As System.Windows.Forms.ToolBar
    Friend WithEvents btnLlenar As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnVaciar As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbnImprimir As System.Windows.Forms.ToolBarButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGeneracionNotaCreditoCondonacionIVA))
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolioFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEnajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfactor_enajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClaveCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcIvaDesglosado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.tlbNotasCredito = New System.Windows.Forms.ToolBar
        Me.btnLlenar = New System.Windows.Forms.ToolBarButton
        Me.btnVaciar = New System.Windows.Forms.ToolBarButton
        Me.tbnImprimir = New System.Windows.Forms.ToolBarButton
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbnImprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(669, 28)
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(8, 120)
        Me.grFacturas.MainView = Me.grvFacturas
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grFacturas.Size = New System.Drawing.Size(480, 312)
        Me.grFacturas.TabIndex = 69
        Me.grFacturas.Text = "Articulos"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcPartida, Me.grcFactura, Me.grcCliente, Me.grcImporte, Me.grcFolioFactura, Me.grcEnajenacion, Me.grcfactor_enajenacion, Me.grcClaveCliente, Me.grcIvaDesglosado})
        Me.grvFacturas.GridControl = Me.grFacturas
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsCustomization.AllowFilter = False
        Me.grvFacturas.OptionsCustomization.AllowGroup = False
        Me.grvFacturas.OptionsCustomization.AllowSort = False
        Me.grvFacturas.OptionsMenu.EnableFooterMenu = False
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        Me.grvFacturas.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 50
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "clave_compuesta"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 1
        Me.grcFactura.Width = 76
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "nombre_cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.VisibleIndex = 3
        Me.grcCliente.Width = 267
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "$###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 85
        '
        'grcFolioFactura
        '
        Me.grcFolioFactura.Caption = "Folio Factura"
        Me.grcFolioFactura.FieldName = "folio_factura"
        Me.grcFolioFactura.Name = "grcFolioFactura"
        Me.grcFolioFactura.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcEnajenacion
        '
        Me.grcEnajenacion.Caption = "Enajenacion"
        Me.grcEnajenacion.FieldName = "enajenacion"
        Me.grcEnajenacion.Name = "grcEnajenacion"
        Me.grcEnajenacion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcfactor_enajenacion
        '
        Me.grcfactor_enajenacion.Caption = "Factor Enajenacion"
        Me.grcfactor_enajenacion.FieldName = "factor_enajenacion"
        Me.grcfactor_enajenacion.Name = "grcfactor_enajenacion"
        Me.grcfactor_enajenacion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcClaveCliente
        '
        Me.grcClaveCliente.Caption = "Cliente"
        Me.grcClaveCliente.FieldName = "cliente"
        Me.grcClaveCliente.Name = "grcClaveCliente"
        Me.grcClaveCliente.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcClaveCliente.VisibleIndex = 2
        '
        'grcIvaDesglosado
        '
        Me.grcIvaDesglosado.Caption = "IvaDesglosado"
        Me.grcIvaDesglosado.ColumnEdit = Me.chkSeleccionar
        Me.grcIvaDesglosado.FieldName = "ivadesglosado"
        Me.grcIvaDesglosado.Name = "grcIvaDesglosado"
        Me.grcIvaDesglosado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcIvaDesglosado.VisibleIndex = 5
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(16, 64)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 67
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 6, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(64, 64)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha.TabIndex = 68
        Me.dteFecha.Tag = "fecha"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(8, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 65
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(64, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(264, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 66
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'tlbNotasCredito
        '
        Me.tlbNotasCredito.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tlbNotasCredito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tlbNotasCredito.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnLlenar, Me.btnVaciar})
        Me.tlbNotasCredito.ButtonSize = New System.Drawing.Size(77, 22)
        Me.tlbNotasCredito.Dock = System.Windows.Forms.DockStyle.None
        Me.tlbNotasCredito.DropDownArrows = True
        Me.tlbNotasCredito.ImageList = Me.ilsToolbar
        Me.tlbNotasCredito.Location = New System.Drawing.Point(8, 91)
        Me.tlbNotasCredito.Name = "tlbNotasCredito"
        Me.tlbNotasCredito.ShowToolTips = True
        Me.tlbNotasCredito.Size = New System.Drawing.Size(480, 29)
        Me.tlbNotasCredito.TabIndex = 70
        Me.tlbNotasCredito.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right
        '
        'btnLlenar
        '
        Me.btnLlenar.ImageIndex = 4
        Me.btnLlenar.Text = "Todos"
        '
        'btnVaciar
        '
        Me.btnVaciar.ImageIndex = 1
        Me.btnVaciar.Text = "Ninguno"
        '
        'tbnImprimir
        '
        Me.tbnImprimir.Text = "Imprimir"
        Me.tbnImprimir.Visible = False
        '
        'frmGeneracionNotaCreditoCondonacionIVA
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(498, 440)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.tlbNotasCredito)
        Me.Name = "frmGeneracionNotaCreditoCondonacionIVA"
        Me.Text = "frmGeneracionNotaCreditoCondonacionIVA"
        Me.Controls.SetChildIndex(Me.tlbNotasCredito, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Public oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar

    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oVariables As VillarrealBusiness.clsVariables
    Private intCajero As Long

    'Private sConceptoDescuentosAnticipados As String = ""
    'Private sConceptoFacturaCredito As String = ""
    Private sConceptoCondonacionIVA As String = "61"
    Private Tabla_Documentos As DataTable


    Private cadena_folios As String = ""
    Private cadena_clientes As String = ""

    Private Property Folios() As String
        Get
            Return cadena_folios
        End Get
        Set(ByVal Value As String)
            If cadena_folios.Length = 0 Then
                cadena_folios = Value
            Else
                cadena_folios = cadena_folios + "," + Value
            End If
        End Set
    End Property
    Private Property Clientes() As String
        Get
            Return cadena_clientes
        End Get
        Set(ByVal Value As String)
            If cadena_clientes.Length = 0 Then
                cadena_clientes = Value
            Else
                cadena_clientes = cadena_clientes + "," + Value
            End If
        End Set
    End Property


    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Cajero() As Long
        Get
            If Comunes.clsUtilerias.uti_Usuariocajero(CStr(TINApp.Connection.User), intCajero) = True Then
                Return intCajero
            Else
                Return -1
            End If
        End Get
    End Property


    Private factura_electronica As Boolean = False
    Private SerieNC As String = ""
    Private folio_movimiento As Long = 0

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmGeneracionNotaCreditoCondonacionIVA_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim impuesto As Double
        Dim subtotal As Double
        Dim iva As Double
        Dim dIVa As Double

        If Not factura_electronica Then
            SerieNC = Comunes.clsUtilerias.uti_SerieCajasNotaCredito(Comunes.Common.Caja_Actual)
        End If

        impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)

        Dim orow As DataRow
        Dim orow_documentos As DataRow

        For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows
            If Not Response.ErrorFound And orow("seleccionar") = True Then

                If impuesto <> -1 Then

                    subtotal = orow("importe") '/ (1 + (impuesto / 100))
                    iva = 0 'orow("importe") - subtotal

                    dIVa = 0 '1 + (impuesto / 100)
                Else
                    Response.Message = "El Porcentaje de Impuesto no esta definido"
                    Exit Sub
                End If


                If Not Response.ErrorFound Then
                    Dim observaciones As String



                    observaciones = IIf(orow("ivadesglosado"), "1", "0")
                    Response = oMovimientosCobrar.InsertarCajas(Sucursal, sConceptoCondonacionIVA, SerieNC, orow("cliente"), 0, _
                                                      Me.dteFecha.DateTime, Comunes.Common.Caja_Actual, Cajero, System.DBNull.Value, 1, 0, orow("importe"), subtotal, iva, _
                                                     orow("importe"), 0, System.DBNull.Value, observaciones + "Condonaci�n de IVA ", "", Sucursal, orow("folio_movimiento"), False)

                    'DAM 17/05/2006 -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO

                    observaciones = "Nota de Cr�dito generada para la Venta " + CType(orow("serie"), String) + "-" + CType(orow("folio"), String)



                    Dim filter As String = " sucursal_referencia = " + orow("sucursal").ToString + " and concepto_referencia = " + orow("concepto").ToString + " and serie_referencia = '" + orow("serie").ToString + "' and folio_referencia = " + orow("folio").ToString + " and cliente_referencia = " + orow("cliente").ToString

                    Dim documentos_filtrados As DataRow() = Tabla_Documentos.Select(filter)


                    Dim docto As Integer = 0
                    For Each orow_documentos In documentos_filtrados

                        docto = docto + 1
                        If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.Insertar(Sucursal, sConceptoCondonacionIVA, SerieNC, orow("folio_movimiento"), orow("cliente"), _
                                                                                docto, 0, Me.dteFecha.DateTime, orow_documentos("importe"), Sucursal, orow("concepto"), orow("serie"), orow("folio"), orow("cliente"), orow_documentos("documento_referencia"), 0, System.DBNull.Value, 0, "", observaciones, 0)

                    Next



                    'Se verifica si usa factura electronica y no hubo un error anterior
                    If Me.factura_electronica And Not Response.ErrorFound Then
                        'Se llena la nota de credito Electronica
                        Response = Me.oMovimientosCobrar.LlenaNotaCreditoCFDI(Sucursal, sConceptoCondonacionIVA, SerieNC, orow("folio_movimiento"), orow("cliente"), orow("ivadesglosado"), Comunes.Common.Sucursal_Actual, False)
                    End If

                    Tabla_Documentos.Select("")

                End If
            End If
        Next

      

    End Sub
    Private Sub frmGeneracionNotaCreditoCondonacionIVA_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oVariables = New VillarrealBusiness.clsVariables

        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)

        ' Sucursales
        '-----------------------------
        Dim oDataSet As DataSet
        Response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        oDataSet = CType(Response.Value, DataSet)
        factura_electronica = CType(oDataSet.Tables(0).Rows(0).Item("factura_electronica"), Boolean)
        SerieNC = CType(oDataSet.Tables(0).Rows(0).Item("serie_nota_credito_electronica"), String)
        '-----------------------------


        If sConceptoCondonacionIVA.Length = 0 Then
            Me.tbrTools.Buttons.Item(0).Visible = False
            Me.tbrTools.Buttons.Item(0).Enabled = False
            ShowMessage(MessageType.MsgInformation, "El Concepto de  CXC de Condonacion de IVa no esta definido", "Variables del Sistema", Nothing, False)
        End If
    End Sub
    Private Sub frmGeneracionNotaCreditoCondonacionIVA_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

    Private Sub frmGeneracionNotaCreditoCondonacionIVA_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = ValidaGrid()
    End Sub


    Private Sub frmGeneracionNotaCreditoCondonacionIVA_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmGeneracionNotaCreditoCondonacionIVA_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        Dim Response As New Events
        Dim orow As DataRow
        For Each orow In CType(Me.grFacturas.DataSource, DataTable).Rows

            If orow("seleccionar") = True Then

                Me.Clientes = CType(orow("cliente"), String)
                Me.Folios = CType(orow("folio_movimiento"), String)

            End If
        Next

        If Not Me.factura_electronica Then
            Me.ImprimeNotaDeCredito(Sucursal, sConceptoCondonacionIVA, SerieNC, Response)
        End If

    End Sub
    Private Sub frmGeneracionNotaCreditoCondonacionIVA_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        CargarFacturas()
    End Sub
    Private Sub dteFecha_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha.EditValueChanged

        CargarFacturas()
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button.Text = "Imprimir" Then
            'ImprimeListadoNotasCredito()
        End If

    End Sub
    Private Sub tlbNotasCredito_ButtonClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tlbNotasCredito.ButtonClick
        If e.Button Is btnLlenar Then
            Dim oRow As DataRow
            For Each oRow In CType(grvFacturas.DataSource, DataView).Table.Rows
                oRow.Item("seleccionar") = True
            Next
        ElseIf e.Button Is btnVaciar Then
            Dim oRow As DataRow
            For Each oRow In CType(grvFacturas.DataSource, DataView).Table.Rows
                oRow.Item("seleccionar") = False
            Next
        End If

    End Sub

#End Region




#Region "DIPROS Systems, Funcionalidad"

    Private Sub CargarFacturas()
        Dim Response As Dipros.Utils.Events
        Try
            Response = Me.oMovimientosCobrar.MovimientosCondonacionIVa(Sucursal, Me.dteFecha.DateTime.Month, Me.dteFecha.DateTime.Year)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grFacturas.DataSource = oDataSet.Tables(0)
                Tabla_Documentos = oDataSet.Tables(1)


                If Me.grFacturas.DataSource Is Nothing Then
                    tlbNotasCredito.Enabled = False
                Else
                    If CType(Me.grvFacturas.DataSource, DataView).Table.Rows.Count > 0 Then
                        tlbNotasCredito.Enabled = True
                    Else
                        tlbNotasCredito.Enabled = False
                    End If

                End If

                oDataSet = Nothing
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Function ValidaGrid() As Events
        Dim oevent As New Events

        Me.grvFacturas.CloseEditor()
        Me.grvFacturas.UpdateCurrentRow()

        If CType(Me.grFacturas.DataSource, DataTable).Select("seleccionar = 1").Length <= 0 Then
            oevent.Ex = Nothing
            oevent.Message = "Se Debe Seleccionar Al Menos Una Factura"
        End If
        Return oevent
    End Function
    Private Function ValidaCajero() As Events
        Dim oevent As New Events
        If Cajero <= 0 Then
            oevent.Ex = Nothing
            oevent.Message = "El Usuario No Tiene un Cajero Asignado"
        End If
        Return oevent
    End Function

    Private Sub ImprimeNotaDeCredito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByRef response As Dipros.Utils.Events)

        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            'response = oReportes.ImprimeNotaDeCredito(sucursal, concepto, serie, folio, cliente, iva_desglosado)

            response = oReportes.ImprimeNotaDeCreditoMultiple(sucursal, concepto, serie, Me.Folios, Me.Clientes)

            If response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Las Notas de Cr�dito no pueden Mostrarse")
            Else
                Dim oDataSet As DataSet
                oDataSet = response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New Comunes.rptNotaDeCreditoCondonacionIVA
                    oReport.DataSource = oDataSet.Tables(0)



                    If TINApp.Connection.User.ToUpper = "SUPER" Then
                        TINApp.ShowReport(Me.MdiParent, "Impresi�n de Notas de Cr�ditos ", oReport, , , , True)
                    Else
                        TINApp.PrintReport(oReport)
                    End If


                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")

                End If

                oDataSet = Nothing
            End If
        Catch ex As Exception
            response.Ex = ex
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub

#End Region


End Class

