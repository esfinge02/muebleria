Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias


Public Class frmNotasCreditoANotasCargo
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursalNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents grcAbonos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblconcepto As System.Windows.Forms.Label
    Friend WithEvents lkpconcepto As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcEnajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfactor_enajenacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmNotasCreditoANotasCargo))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursalNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAbonos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcEnajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfactor_enajenacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.lblconcepto = New System.Windows.Forms.Label
        Me.lkpconcepto = New Dipros.Editors.TINMultiLookup
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3227, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(50, 40)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cl&iente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(104, 40)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(360, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 1
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(56, 88)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 4
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "F&echa:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(104, 88)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha.TabIndex = 5
        Me.dteFecha.Tag = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 200)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 17)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "&NotasCargo"
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(8, 216)
        Me.grFacturas.MainView = Me.grvFacturas
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grFacturas.Size = New System.Drawing.Size(640, 216)
        Me.grFacturas.TabIndex = 12
        Me.grFacturas.Text = "Facturas"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcSucursal, Me.grcSucursalNombre, Me.grcFactura, Me.grcFecha, Me.grcAbonos, Me.grcImporte, Me.grcEnajenacion, Me.grcfactor_enajenacion, Me.grcConcepto})
        Me.grvFacturas.GridControl = Me.grFacturas
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsCustomization.AllowFilter = False
        Me.grvFacturas.OptionsCustomization.AllowGroup = False
        Me.grvFacturas.OptionsCustomization.AllowSort = False
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        Me.grvFacturas.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 30
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSucursalNombre
        '
        Me.grcSucursalNombre.Caption = "Sucursal"
        Me.grcSucursalNombre.FieldName = "sucursal_nombre"
        Me.grcSucursalNombre.Name = "grcSucursalNombre"
        Me.grcSucursalNombre.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursalNombre.VisibleIndex = 1
        Me.grcSucursalNombre.Width = 106
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 3
        Me.grcFactura.Width = 124
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha_factura"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 4
        Me.grcFecha.Width = 100
        '
        'grcAbonos
        '
        Me.grcAbonos.Caption = "importe"
        Me.grcAbonos.DisplayFormat.FormatString = "c2"
        Me.grcAbonos.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcAbonos.FieldName = "abonos"
        Me.grcAbonos.Name = "grcAbonos"
        Me.grcAbonos.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcAbonos.VisibleIndex = 5
        Me.grcAbonos.Width = 91
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "saldo"
        Me.grcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 6
        Me.grcImporte.Width = 88
        '
        'grcEnajenacion
        '
        Me.grcEnajenacion.Caption = "Enajenacion"
        Me.grcEnajenacion.FieldName = "enajenacion"
        Me.grcEnajenacion.Name = "grcEnajenacion"
        Me.grcEnajenacion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcfactor_enajenacion
        '
        Me.grcfactor_enajenacion.Caption = "Factor Enajenacion"
        Me.grcfactor_enajenacion.FieldName = "factor_enajenacion"
        Me.grcfactor_enajenacion.Name = "grcfactor_enajenacion"
        Me.grcfactor_enajenacion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Cargo"
        Me.grcConcepto.FieldName = "n_concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.VisibleIndex = 2
        Me.grcConcepto.Width = 99
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(44, 112)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 6
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(104, 112)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "n2"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(96, 19)
        Me.clcImporte.TabIndex = 7
        Me.clcImporte.Tag = "importe"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(8, 136)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 8
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "&Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(104, 136)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(360, 38)
        Me.txtObservaciones.TabIndex = 9
        Me.txtObservaciones.Tag = "observaciones"
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(104, 176)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "I.&V.A. Desglosado"
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(128, 20)
        Me.chkDesglosado.TabIndex = 10
        Me.chkDesglosado.Tag = ""
        '
        'lblconcepto
        '
        Me.lblconcepto.AutoSize = True
        Me.lblconcepto.Location = New System.Drawing.Point(24, 64)
        Me.lblconcepto.Name = "lblconcepto"
        Me.lblconcepto.Size = New System.Drawing.Size(73, 16)
        Me.lblconcepto.TabIndex = 2
        Me.lblconcepto.Tag = ""
        Me.lblconcepto.Text = "&Movimiento:"
        Me.lblconcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpconcepto
        '
        Me.lkpconcepto.AllowAdd = False
        Me.lkpconcepto.AutoReaload = True
        Me.lkpconcepto.DataSource = Nothing
        Me.lkpconcepto.DefaultSearchField = ""
        Me.lkpconcepto.DisplayMember = "descripcion"
        Me.lkpconcepto.EditValue = Nothing
        Me.lkpconcepto.Filtered = False
        Me.lkpconcepto.InitValue = Nothing
        Me.lkpconcepto.Location = New System.Drawing.Point(104, 64)
        Me.lkpconcepto.MultiSelect = False
        Me.lkpconcepto.Name = "lkpconcepto"
        Me.lkpconcepto.NullText = ""
        Me.lkpconcepto.PopupWidth = CType(400, Long)
        Me.lkpconcepto.ReadOnlyControl = False
        Me.lkpconcepto.Required = False
        Me.lkpconcepto.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpconcepto.SearchMember = ""
        Me.lkpconcepto.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpconcepto.SelectAll = False
        Me.lkpconcepto.Size = New System.Drawing.Size(360, 20)
        Me.lkpconcepto.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpconcepto.TabIndex = 3
        Me.lkpconcepto.Tag = ""
        Me.lkpconcepto.ToolTip = Nothing
        Me.lkpconcepto.ValueMember = "concepto"
        '
        'frmNotasCreditoANotasCargo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(658, 440)
        Me.Controls.Add(Me.lblconcepto)
        Me.Controls.Add(Me.lkpconcepto)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmNotasCreditoANotasCargo"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.Controls.SetChildIndex(Me.lkpconcepto, 0)
        Me.Controls.SetChildIndex(Me.lblconcepto, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oVariables As VillarrealBusiness.clsVariables
    Private oConceptos_cxc As VillarrealBusiness.clsConceptosCxc
    Private oSucursales As VillarrealBusiness.clsSucursales


    Private Sucursal As Long = -1
    Private Serie As String = ""
    Private Folio As Long = -1
    Private enajenacion_factura As Boolean
    Private factor_enajenacion As Double
    Private Cargo_a_Bonificar As Double


    Private Folio_Movimiento As Long = -1
    Private ImportePagar As Double
    Private factura_electronica As Boolean = False
    Private SerieNC As String = ""

    'Private ConceptoNotaCredito As String
    'Private banConceptoNotaCredito As Boolean = False
    Private fila_seleccionada As Integer
    Private tableLetrasSinSaldar As DataTable


    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property Concepto() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpconcepto)
        End Get
    End Property
    'Private ReadOnly Property ConceptoFacturaCredito() As String
    '    Get
    '        Return oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
    '    End Get
    'End Property
    Private ReadOnly Property NumeroLetrasSinSaldar() As Long
        Get
            If Me.tableLetrasSinSaldar Is Nothing Then
                Return -1
            Else
                Return Me.tableLetrasSinSaldar.Rows.Count
            End If
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmNotasCredito_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmNotasCredito_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmNotasCredito_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        If Not Me.factura_electronica Then
            Me.ImprimeNotaDeCredito(Sucursal, Concepto, SerieNC, Folio_Movimiento, Cliente, Me.chkDesglosado.Checked)
        End If
    End Sub

    Private Sub frmNotasCredito_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Dim i As Integer = 0
        Dim importe_documento As Double
        Dim numero_documentos As Long
        Dim impuesto As Double
        Dim subtotal As Double
        Dim iva As Double
        Dim observaciones As String
        Dim sConceptoNotaCredito As String

        Dim dImporte_proporcional_costo As Double
        Dim dIVa As Double

        If Not factura_electronica Then
            SerieNC = Comunes.clsUtilerias.uti_SerieCajasNotaCredito(Comunes.Common.Caja_Actual)
        End If

        impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)
        If impuesto <> -1 Then

            subtotal = Me.clcImporte.Value / (1 + (impuesto / 100))
            iva = Me.clcImporte.Value - subtotal
            dIVa = 1 + (impuesto / 100)
        Else
            ShowMessage(MessageType.MsgInformation, "El Porcentaje de Impuesto no esta definido", "Variables del Sistema", Nothing, False)
            Exit Sub
        End If


        numero_documentos = Me.NumeroDocumentos(ImportePagar)



        Response = oMovimientosCobrar.InsertarCajas(Sucursal, Concepto, SerieNC, Cliente, 0, _
                                           Me.dteFecha.DateTime, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, numero_documentos, 0, ImportePagar, subtotal, iva, _
                                           ImportePagar, 0, System.DBNull.Value, Me.txtObservaciones.Text, "", Comunes.Common.Sucursal_Actual, Folio_Movimiento)

        If Not Response.ErrorFound Then
            For i = 0 To Me.tableLetrasSinSaldar.Rows.Count - 1
                If ImportePagar > Me.tableLetrasSinSaldar.Rows(i).Item("saldo") Then
                    importe_documento = Me.tableLetrasSinSaldar.Rows(i).Item("saldo")
                Else
                    importe_documento = ImportePagar
                End If

                'DAM 17/05/2006 -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO

                observaciones = "Nota de Credito por Pronto Pago " + Me.txtObservaciones.Text

                If enajenacion_factura = True Then
                    'DAM 16/ABR/07 SE AGREGO ESTE CALCULO PARA EL IMPORTE PROPORCIONAL DE LOS DOCUMENTOS DE LA NOTA DE CREDITO
                    dImporte_proporcional_costo = (importe_documento / dIVa) * factor_enajenacion
                    dImporte_proporcional_costo = dImporte_proporcional_costo * -1
                Else
                    dImporte_proporcional_costo = 0

                End If
                If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.Insertar(Sucursal, Concepto, SerieNC, Folio_Movimiento, Cliente, _
                                                         i + 1, 0, Me.dteFecha.DateTime, importe_documento, Sucursal, tableLetrasSinSaldar.Rows(i).Item("concepto"), Serie, Folio, Cliente, tableLetrasSinSaldar.Rows(i).Item("documento"), 0, System.DBNull.Value, 0, "P", observaciones, dImporte_proporcional_costo)

                ImportePagar = ImportePagar - importe_documento
                If ImportePagar = 0 Then Exit For

            Next
        End If

        'Se verifica si usa factura electronica y no hubo un error anterior
        If Me.factura_electronica And Not Response.ErrorFound Then
            'Se llena la nota de credito Electronica
            Response = Me.oMovimientosCobrar.LlenaNotaCreditoCFDI(Comunes.Common.Sucursal_Actual, Concepto, SerieNC, Folio_Movimiento, Cliente, Me.chkDesglosado.Checked, Comunes.Common.Sucursal_Actual, True)
        End If


        ' If Not Response.ErrorFound Then Me.ImprimeNotaDeCredito(Sucursal, Concepto, SerieNC, Folio_Movimiento, Cliente, Me.chkDesglosado.Checked)

    End Sub
    Private Sub frmNotasCredito_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oVariables = New VillarrealBusiness.clsVariables
        oConceptos_cxc = New VillarrealBusiness.clsConceptosCxc
        oSucursales = New VillarrealBusiness.clsSucursales



        'Me.TraeConceptoNotaCredito()

        'If Me.banConceptoNotaCredito = False Then
        '    Me.tbrTools.Buttons.Item(2).Visible = False
        '    Me.tbrTools.Buttons.Item(2).Enabled = False
        'End If

        ' Sucursales
        '-----------------------------
        Dim oDataSet As DataSet
        Response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        oDataSet = CType(Response.Value, DataSet)
        factura_electronica = CType(oDataSet.Tables(0).Rows(0).Item("factura_electronica"), Boolean)
        SerieNC = CType(oDataSet.Tables(0).Rows(0).Item("serie_nota_credito_electronica"), String)
        '-----------------------------

        Me.Location = New Point(0, 0)
        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
    End Sub
    Private Sub frmNotasCredito_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Try
            Response = oMovimientosCobrar.ValidacionNotaCredito(Cliente, Me.Concepto, Me.dteFecha.Text, NumeroLetrasSinSaldar, ImportePagar, Cargo_a_Bonificar)
        Catch ex As Exception

        End Try

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        CargarNotasCargo()
    End Sub

    Private Sub lkpConcepto_LoadData(ByVal Initialize As Boolean) Handles lkpconcepto.LoadData
        Dim Response As New Events
        ' es true para que solo muestre los conceptos que se pueden hacer en este modulo
        Response = oConceptos_cxc.Lookup("A", True)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpconcepto.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpConcepto_Format() Handles lkpconcepto.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpconcepto)
    End Sub


    Private Sub grvFacturas_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvFacturas.CellValueChanging
        'Dim j As Integer = Me.grvFacturas.FocusedRowHandle

        fila_seleccionada = Me.grvFacturas.FocusedRowHandle
        Dim i As Integer = 0
        For i = 0 To Me.grvFacturas.RowCount - 1
            If i <> fila_seleccionada Then
                Me.grvFacturas.SetRowCellValue(i, Me.grcSeleccionar, False)
            Else
                Me.grvFacturas.SetRowCellValue(i, Me.grcSeleccionar, True)
            End If
        Next
        Me.grvFacturas.UpdateCurrentRow()
    End Sub
    Private Sub grvFacturas_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvFacturas.CellValueChanged
        If e.Column Is Me.grcSeleccionar Then
            If CType(e.Value, Boolean) = True Then
                Sucursal = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("sucursal"), Long)
                Serie = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("serie"), String)
                Folio = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("folio"), Long)
                enajenacion_factura = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("enajenacion"), Boolean)
                factor_enajenacion = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("factor_enajenacion"), Double)
                Cargo_a_Bonificar = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("importe"), Double)
                Me.CargarLetrasSinSaldar()
            End If
        End If
    End Sub

    Private Sub clcImporte_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcImporte.EditValueChanged
        If Not IsNumeric(Me.clcImporte.EditValue) Then Exit Sub
        ImportePagar = Me.clcImporte.EditValue
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CargarNotasCargo()
        Dim Response As Dipros.Utils.Events
        Try
            ' Response = oVentas.FacturasCreditoPorCliente(Cliente)
            Response = oMovimientosCobrar.NotasCargoCliente(Cliente)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grFacturas.DataSource = oDataSet.Tables(0)

                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub CargarLetrasSinSaldar()

        Dim Response As Dipros.Utils.Events
        Try
            Response = oMovimientosCobrarDetalle.LetrasDeFactura(Sucursal, Serie, Folio, Cliente, False)

            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                'CType(Me.grLetras.DataSource, DataTable).Rows.Add(oDataSet.Tables(0).Rows(0))
                tableLetrasSinSaldar = oDataSet.Tables(0)
                oDataSet = Nothing
            End If

        Catch ex As Exception
        End Try
    End Sub
    Private Function NumeroDocumentos(ByVal importe As Double) As Long
        Dim i As Long
        Dim importe_documento As Double

        For i = 0 To Me.tableLetrasSinSaldar.Rows.Count - 1
            If importe > Me.tableLetrasSinSaldar.Rows(i).Item("saldo") Then
                importe_documento = Me.tableLetrasSinSaldar.Rows(i).Item("saldo")
            Else
                importe_documento = importe
            End If

            importe = importe - importe_documento
            If importe = 0 Then Exit For
        Next

        Return i + 1

    End Function
    Private Sub ImprimeNotaDeCredito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, Optional ByVal iva_desglosado As Boolean = True)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            Response = oReportes.ImprimeNotaDeCredito(sucursal, concepto, serie, folio, cliente, iva_desglosado)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Las Notas de Cr�dito no pueden Mostrarse")
            Else
                If Response.Value.Tables(0).Rows.Count > 0 Then
                    Dim oDataSet As DataSet
                    Dim oReport As New Comunes.rptNotaDeCredito

                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)


                    TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cr�dito " & CType(folio, String), oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub


#End Region

    Private Sub frmNotasCredito_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If VerificaPermisoExtendido(Me.MenuOption.Name, "FECHA_NCR_A_NC") Then
            Me.dteFecha.Enabled = True
        Else
            Me.dteFecha.Enabled = False
        End If
    End Sub


    
End Class
