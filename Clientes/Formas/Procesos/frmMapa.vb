Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Public Class frmMapa
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents wwweb As AxSHDocVw.AxWebBrowser
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtconsulta As System.Windows.Forms.TextBox
    Friend WithEvents directorio As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents buscar As System.Windows.Forms.Button
    Friend WithEvents ComboSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents txtnombre As System.Windows.Forms.TextBox
    Friend WithEvents txtclave As System.Windows.Forms.TextBox
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents idc As System.Windows.Forms.ColumnHeader
    Friend WithEvents id As System.Windows.Forms.ColumnHeader
    Friend WithEvents nombre As System.Windows.Forms.ColumnHeader
    Friend WithEvents dir As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMapa))
        Me.wwweb = New AxSHDocVw.AxWebBrowser
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtconsulta = New System.Windows.Forms.TextBox
        Me.directorio = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.buscar = New System.Windows.Forms.Button
        Me.ComboSucursal = New System.Windows.Forms.ComboBox
        Me.txtnombre = New System.Windows.Forms.TextBox
        Me.txtclave = New System.Windows.Forms.TextBox
        Me.ListView1 = New System.Windows.Forms.ListView
        Me.idc = New System.Windows.Forms.ColumnHeader
        Me.id = New System.Windows.Forms.ColumnHeader
        Me.nombre = New System.Windows.Forms.ColumnHeader
        Me.dir = New System.Windows.Forms.ColumnHeader
        CType(Me.wwweb, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'wwweb
        '
        Me.wwweb.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.wwweb.Enabled = True
        Me.wwweb.Location = New System.Drawing.Point(0, 0)
        Me.wwweb.OcxState = CType(resources.GetObject("wwweb.OcxState"), System.Windows.Forms.AxHost.State)
        Me.wwweb.Size = New System.Drawing.Size(900, 600)
        Me.wwweb.TabIndex = 24
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ListView1)
        Me.GroupBox1.Controls.Add(Me.txtconsulta)
        Me.GroupBox1.Controls.Add(Me.directorio)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.buscar)
        Me.GroupBox1.Controls.Add(Me.ComboSucursal)
        Me.GroupBox1.Controls.Add(Me.txtnombre)
        Me.GroupBox1.Controls.Add(Me.txtclave)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(216, 536)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Busqueda de Direcciones"
        '
        'txtconsulta
        '
        Me.txtconsulta.AutoSize = False
        Me.txtconsulta.Location = New System.Drawing.Point(8, 456)
        Me.txtconsulta.Multiline = True
        Me.txtconsulta.Name = "txtconsulta"
        Me.txtconsulta.Size = New System.Drawing.Size(192, 24)
        Me.txtconsulta.TabIndex = 32
        Me.txtconsulta.Text = ""
        '
        'directorio
        '
        Me.directorio.Location = New System.Drawing.Point(8, 424)
        Me.directorio.Name = "directorio"
        Me.directorio.Size = New System.Drawing.Size(192, 23)
        Me.directorio.TabIndex = 31
        Me.directorio.Text = "Buscar Direcciones"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(152, 23)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Nombre del cliente"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 23)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Clave"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 23)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Sucursal"
        '
        'buscar
        '
        Me.buscar.Location = New System.Drawing.Point(8, 184)
        Me.buscar.Name = "buscar"
        Me.buscar.Size = New System.Drawing.Size(192, 23)
        Me.buscar.TabIndex = 27
        Me.buscar.Text = "Buscar Clientes"
        '
        'ComboSucursal
        '
        Me.ComboSucursal.DisplayMember = "0"
        Me.ComboSucursal.Location = New System.Drawing.Point(8, 48)
        Me.ComboSucursal.Name = "ComboSucursal"
        Me.ComboSucursal.Size = New System.Drawing.Size(192, 21)
        Me.ComboSucursal.TabIndex = 26
        Me.ComboSucursal.Text = "0 - Seleccione una Sucursal"
        '
        'txtnombre
        '
        Me.txtnombre.Location = New System.Drawing.Point(8, 152)
        Me.txtnombre.Name = "txtnombre"
        Me.txtnombre.Size = New System.Drawing.Size(192, 20)
        Me.txtnombre.TabIndex = 25
        Me.txtnombre.Text = ""
        '
        'txtclave
        '
        Me.txtclave.Location = New System.Drawing.Point(64, 96)
        Me.txtclave.Name = "txtclave"
        Me.txtclave.Size = New System.Drawing.Size(136, 20)
        Me.txtclave.TabIndex = 24
        Me.txtclave.Text = ""
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idc, Me.id, Me.nombre, Me.dir})
        Me.ListView1.Location = New System.Drawing.Point(8, 216)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(192, 200)
        Me.ListView1.TabIndex = 33
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'idc
        '
        Me.idc.Text = "Clave"
        Me.idc.Width = 40
        '
        'id
        '
        Me.id.Text = "id"
        Me.id.Width = 0
        '
        'nombre
        '
        Me.nombre.Text = "Nombre"
        Me.nombre.Width = 105
        '
        'dir
        '
        Me.dir.Text = "Direcciones"
        Me.dir.Width = 40
        '
        'frmMapa
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(884, 561)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.wwweb)
        Me.Name = "frmMapa"
        Me.Text = "frmMapa"
        CType(Me.wwweb, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmMapa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        wwweb.Navigate("http://192.168.20.43/trabajos/mapa.html")


    End Sub

    

End Class
