Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmCambioFechaVentas
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcNombreSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tlbrCambiar As System.Windows.Forms.ToolBarButton
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambioFechaVentas))
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcNombreSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.tlbrCambiar = New System.Windows.Forms.ToolBarButton
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tlbrCambiar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1896, 28)
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(32, 77)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(165, 16)
        Me.lblFecha.TabIndex = 66
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "Cambiar Fecha de Venta  al :"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(200, 75)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy "
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy "
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha.TabIndex = 67
        Me.dteFecha.Tag = ""
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(24, 109)
        Me.grFacturas.MainView = Me.grvFacturas
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grFacturas.Size = New System.Drawing.Size(440, 312)
        Me.grFacturas.TabIndex = 65
        Me.grFacturas.Text = "Facturas"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcNombreSucursal, Me.grcFactura, Me.grcFecha, Me.grcImporte, Me.grcSucursal, Me.grcFolio, Me.grcSerie})
        Me.grvFacturas.GridControl = Me.grFacturas
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsCustomization.AllowFilter = False
        Me.grvFacturas.OptionsCustomization.AllowGroup = False
        Me.grvFacturas.OptionsCustomization.AllowSort = False
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        Me.grvFacturas.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 35
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcNombreSucursal
        '
        Me.grcNombreSucursal.Caption = "Sucursal"
        Me.grcNombreSucursal.FieldName = "sucursal_nombre"
        Me.grcNombreSucursal.Name = "grcNombreSucursal"
        Me.grcNombreSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreSucursal.VisibleIndex = 1
        Me.grcNombreSucursal.Width = 195
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 2
        Me.grcFactura.Width = 164
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha_factura"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 131
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 137
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "folio"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(96, 51)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.Required = True
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(241, 20)
        Me.lkpCliente.TabIndex = 64
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(40, 48)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 68
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tlbrCambiar
        '
        Me.tlbrCambiar.ImageIndex = 0
        Me.tlbrCambiar.Text = "Cambiar"
        '
        'frmCambioFechaVentas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(498, 448)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lkpCliente)
        Me.Name = "frmCambioFechaVentas"
        Me.Text = "frmCambioFechaVentas"
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas
    Private otabla As DataTable

    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCambioFechaVentas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas

        Me.Location = New Point(0, 0)
        Me.tbrTools.Buttons(0).Visible = False
        Me.dteFecha.EditValue = CType(TINApp.FechaServidor, Date)
    End Sub
    Private Sub frmCambioFechaVentas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub
    Private Sub frmCambioFechaVentas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

    End Sub
    Private Sub frmCambioFechaVentas_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Validated

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupFechaVenta()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
        CargarFacturas()
    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button.Text = "Cambiar" Then
            Dim i As Long
            Dim response As New Events
            Me.grvFacturas.UpdateCurrentRow()
            Me.grvFacturas.CloseEditor()
            TINApp.Connection.Begin()
            For i = 0 To Me.grvFacturas.RowCount - 1
                If otabla.Rows(i).Item("seleccionar") Then
                    response = oVentas.ActualizaFechaVentas(otabla.Rows(i).Item("sucursal"), otabla.Rows(i).Item("concepto"), otabla.Rows(i).Item("serie"), otabla.Rows(i).Item("folio"), Me.dteFecha.EditValue)
                    If response.ErrorFound Then
                        TINApp.Connection.Rollback()
                        response.ShowError()
                        Exit Sub
                    End If
                End If
            Next
            TINApp.Connection.Commit()
            CargarFacturas()
        End If
    End Sub

#End Region


#Region "DIPROS Systems, Funcionalidad"
    Private Sub CargarFacturas()
        Dim Response As Dipros.Utils.Events
        Try
            Response = oVentas.FacturasCreditoPorCliente(Cliente)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                otabla = oDataSet.Tables(0)
                Me.grFacturas.DataSource = otabla
                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try
    End Sub

#End Region

End Class
