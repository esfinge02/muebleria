Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes
Imports Comunes.clsUtilerias

Public Class frmTraspasoSaldos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpClienteTraspaso As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblClienteTraspaso As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents lSaldo As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents chkTodaCuenta As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grSaldos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvSaldos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcIncluir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkIncluir As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents lblComision As System.Windows.Forms.Label
    Friend WithEvents clcDocumentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grLetras As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvLetras As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaVencimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaVencimientoNueva As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteLetra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblTipo_Plazo As System.Windows.Forms.Label
    Friend WithEvents cboTipo_Plazo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtefechadocumento As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dtefecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClaveSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblIntereses As System.Windows.Forms.Label
    Friend WithEvents clcIntereses As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTraspasoSaldos))
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpClienteTraspaso = New Dipros.Editors.TINMultiLookup
        Me.lblClienteTraspaso = New System.Windows.Forms.Label
        Me.lblSaldo = New System.Windows.Forms.Label
        Me.lSaldo = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.chkTodaCuenta = New DevExpress.XtraEditors.CheckEdit
        Me.grSaldos = New DevExpress.XtraGrid.GridControl
        Me.grvSaldos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcIncluir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkIncluir = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcClaveSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblComision = New System.Windows.Forms.Label
        Me.clcDocumentos = New Dipros.Editors.TINCalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.grLetras = New DevExpress.XtraGrid.GridControl
        Me.grvLetras = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaVencimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaVencimientoNueva = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteLetra = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblTipo_Plazo = New System.Windows.Forms.Label
        Me.cboTipo_Plazo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.dtefechadocumento = New DevExpress.XtraEditors.DateEdit
        Me.dtefecha = New DevExpress.XtraEditors.DateEdit
        Me.lblIntereses = New System.Windows.Forms.Label
        Me.clcIntereses = New Dipros.Editors.TINCalcEdit
        CType(Me.chkTodaCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grSaldos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvSaldos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIncluir, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDocumentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grLetras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvLetras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo_Plazo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtefechadocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtefecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(9207, 28)
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(104, 35)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(264, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 1
        Me.lkpCliente.Tag = "cliente"
        Me.lkpCliente.ToolTip = "Seleccione un cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(24, 39)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(69, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Del &Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lkpClienteTraspaso
        '
        Me.lkpClienteTraspaso.AllowAdd = False
        Me.lkpClienteTraspaso.AutoReaload = False
        Me.lkpClienteTraspaso.DataSource = Nothing
        Me.lkpClienteTraspaso.DefaultSearchField = ""
        Me.lkpClienteTraspaso.DisplayMember = "nombre"
        Me.lkpClienteTraspaso.EditValue = Nothing
        Me.lkpClienteTraspaso.Filtered = False
        Me.lkpClienteTraspaso.InitValue = Nothing
        Me.lkpClienteTraspaso.Location = New System.Drawing.Point(104, 58)
        Me.lkpClienteTraspaso.MultiSelect = False
        Me.lkpClienteTraspaso.Name = "lkpClienteTraspaso"
        Me.lkpClienteTraspaso.NullText = ""
        Me.lkpClienteTraspaso.PopupWidth = CType(400, Long)
        Me.lkpClienteTraspaso.ReadOnlyControl = False
        Me.lkpClienteTraspaso.Required = False
        Me.lkpClienteTraspaso.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteTraspaso.SearchMember = ""
        Me.lkpClienteTraspaso.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteTraspaso.SelectAll = True
        Me.lkpClienteTraspaso.Size = New System.Drawing.Size(264, 20)
        Me.lkpClienteTraspaso.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteTraspaso.TabIndex = 3
        Me.lkpClienteTraspaso.Tag = "clientetraspaso"
        Me.lkpClienteTraspaso.ToolTip = "Seleccione un cliente"
        Me.lkpClienteTraspaso.ValueMember = "cliente"
        '
        'lblClienteTraspaso
        '
        Me.lblClienteTraspaso.AutoSize = True
        Me.lblClienteTraspaso.Location = New System.Drawing.Point(32, 61)
        Me.lblClienteTraspaso.Name = "lblClienteTraspaso"
        Me.lblClienteTraspaso.Size = New System.Drawing.Size(62, 16)
        Me.lblClienteTraspaso.TabIndex = 2
        Me.lblClienteTraspaso.Text = "Al &Cliente:"
        Me.lblClienteTraspaso.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(456, 40)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(40, 16)
        Me.lblSaldo.TabIndex = 5
        Me.lblSaldo.Text = "Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lSaldo
        '
        Me.lSaldo.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lSaldo.Location = New System.Drawing.Point(504, 40)
        Me.lSaldo.Name = "lSaldo"
        Me.lSaldo.Size = New System.Drawing.Size(104, 16)
        Me.lSaldo.TabIndex = 6
        Me.lSaldo.Text = "0.00"
        Me.lSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(695, 39)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 11
        Me.lblFecha.Text = "Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkTodaCuenta
        '
        Me.chkTodaCuenta.Location = New System.Drawing.Point(104, 82)
        Me.chkTodaCuenta.Name = "chkTodaCuenta"
        '
        'chkTodaCuenta.Properties
        '
        Me.chkTodaCuenta.Properties.Caption = "Toda la cuenta"
        Me.chkTodaCuenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlLightLight, System.Drawing.SystemColors.ControlText)
        Me.chkTodaCuenta.Size = New System.Drawing.Size(104, 19)
        Me.chkTodaCuenta.TabIndex = 4
        Me.chkTodaCuenta.Tag = "todacuenta"
        '
        'grSaldos
        '
        '
        'grSaldos.EmbeddedNavigator
        '
        Me.grSaldos.EmbeddedNavigator.Name = ""
        Me.grSaldos.Location = New System.Drawing.Point(6, 128)
        Me.grSaldos.MainView = Me.grvSaldos
        Me.grSaldos.Name = "grSaldos"
        Me.grSaldos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkIncluir})
        Me.grSaldos.Size = New System.Drawing.Size(444, 295)
        Me.grSaldos.TabIndex = 18
        Me.grSaldos.Text = "Saldos"
        '
        'grvSaldos
        '
        Me.grvSaldos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcIncluir, Me.grcSucursal, Me.grcFactura, Me.grcFecha, Me.grcImporte, Me.grcConcepto, Me.grcClaveSucursal})
        Me.grvSaldos.GridControl = Me.grSaldos
        Me.grvSaldos.Name = "grvSaldos"
        Me.grvSaldos.OptionsView.ShowGroupPanel = False
        Me.grvSaldos.OptionsView.ShowIndicator = False
        '
        'grcIncluir
        '
        Me.grcIncluir.ColumnEdit = Me.chkIncluir
        Me.grcIncluir.FieldName = "seleccionar"
        Me.grcIncluir.Name = "grcIncluir"
        Me.grcIncluir.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcIncluir.VisibleIndex = 0
        Me.grcIncluir.Width = 58
        '
        'chkIncluir
        '
        Me.chkIncluir.AutoHeight = False
        Me.chkIncluir.Name = "chkIncluir"
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal_nombre"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursal.VisibleIndex = 1
        Me.grcSucursal.Width = 266
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Cargo"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 2
        Me.grcFactura.Width = 153
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha_factura"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 165
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 171
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcClaveSucursal
        '
        Me.grcClaveSucursal.Caption = "Clave Sucursal"
        Me.grcClaveSucursal.FieldName = "sucursal"
        Me.grcClaveSucursal.Name = "grcClaveSucursal"
        Me.grcClaveSucursal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'lblComision
        '
        Me.lblComision.AutoSize = True
        Me.lblComision.Location = New System.Drawing.Point(376, 60)
        Me.lblComision.Name = "lblComision"
        Me.lblComision.Size = New System.Drawing.Size(101, 16)
        Me.lblComision.TabIndex = 7
        Me.lblComision.Tag = ""
        Me.lblComision.Text = "No. Documentos:"
        Me.lblComision.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDocumentos
        '
        Me.clcDocumentos.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.clcDocumentos.Location = New System.Drawing.Point(488, 59)
        Me.clcDocumentos.MaxValue = 0
        Me.clcDocumentos.MinValue = 0
        Me.clcDocumentos.Name = "clcDocumentos"
        '
        'clcDocumentos.Properties
        '
        Me.clcDocumentos.Properties.DisplayFormat.FormatString = "n0"
        Me.clcDocumentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDocumentos.Properties.EditFormat.FormatString = "n0"
        Me.clcDocumentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDocumentos.Properties.MaskData.EditMask = "########0.00"
        Me.clcDocumentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDocumentos.Size = New System.Drawing.Size(64, 19)
        Me.clcDocumentos.TabIndex = 8
        Me.clcDocumentos.Tag = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(454, 108)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 17)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "LETRAS"
        '
        'grLetras
        '
        '
        'grLetras.EmbeddedNavigator
        '
        Me.grLetras.EmbeddedNavigator.Name = ""
        Me.grLetras.Location = New System.Drawing.Point(456, 128)
        Me.grLetras.MainView = Me.grvLetras
        Me.grLetras.Name = "grLetras"
        Me.grLetras.Size = New System.Drawing.Size(386, 295)
        Me.grLetras.TabIndex = 20
        Me.grLetras.TabStop = False
        Me.grLetras.Text = "Letras"
        '
        'grvLetras
        '
        Me.grvLetras.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcDocumento, Me.grcFechaVencimiento, Me.grcFechaVencimientoNueva, Me.grcImporteLetra})
        Me.grvLetras.GridControl = Me.grLetras
        Me.grvLetras.Name = "grvLetras"
        Me.grvLetras.OptionsBehavior.Editable = False
        Me.grvLetras.OptionsCustomization.AllowFilter = False
        Me.grvLetras.OptionsCustomization.AllowGroup = False
        Me.grvLetras.OptionsCustomization.AllowSort = False
        Me.grvLetras.OptionsView.ShowGroupPanel = False
        Me.grvLetras.OptionsView.ShowIndicator = False
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Documento"
        Me.grcDocumento.FieldName = "documento"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.VisibleIndex = 0
        Me.grcDocumento.Width = 70
        '
        'grcFechaVencimiento
        '
        Me.grcFechaVencimiento.Caption = "Fecha"
        Me.grcFechaVencimiento.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaVencimiento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaVencimiento.FieldName = "fecha"
        Me.grcFechaVencimiento.Name = "grcFechaVencimiento"
        Me.grcFechaVencimiento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaVencimiento.VisibleIndex = 1
        Me.grcFechaVencimiento.Width = 103
        '
        'grcFechaVencimientoNueva
        '
        Me.grcFechaVencimientoNueva.Caption = "Fecha Vencimiento"
        Me.grcFechaVencimientoNueva.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaVencimientoNueva.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaVencimientoNueva.FieldName = "fecha_vencimiento"
        Me.grcFechaVencimientoNueva.Name = "grcFechaVencimientoNueva"
        Me.grcFechaVencimientoNueva.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaVencimientoNueva.VisibleIndex = 2
        Me.grcFechaVencimientoNueva.Width = 108
        '
        'grcImporteLetra
        '
        Me.grcImporteLetra.Caption = "Importe"
        Me.grcImporteLetra.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcImporteLetra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteLetra.FieldName = "importe"
        Me.grcImporteLetra.Name = "grcImporteLetra"
        Me.grcImporteLetra.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteLetra.VisibleIndex = 3
        Me.grcImporteLetra.Width = 103
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 17)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "FACTURAS"
        '
        'lblTipo_Plazo
        '
        Me.lblTipo_Plazo.AutoSize = True
        Me.lblTipo_Plazo.Location = New System.Drawing.Point(648, 60)
        Me.lblTipo_Plazo.Name = "lblTipo_Plazo"
        Me.lblTipo_Plazo.Size = New System.Drawing.Size(82, 16)
        Me.lblTipo_Plazo.TabIndex = 13
        Me.lblTipo_Plazo.Tag = ""
        Me.lblTipo_Plazo.Text = "&Tipo de plazo:"
        Me.lblTipo_Plazo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo_Plazo
        '
        Me.cboTipo_Plazo.EditValue = 2
        Me.cboTipo_Plazo.Location = New System.Drawing.Point(744, 58)
        Me.cboTipo_Plazo.Name = "cboTipo_Plazo"
        '
        'cboTipo_Plazo.Properties
        '
        Me.cboTipo_Plazo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo_Plazo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Semanal", 0, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Quincenal", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mensual", 2, -1)})
        Me.cboTipo_Plazo.Size = New System.Drawing.Size(96, 20)
        Me.cboTipo_Plazo.TabIndex = 14
        Me.cboTipo_Plazo.Tag = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(568, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(172, 16)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Fecha del Primer Documento :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'dtefechadocumento
        '
        Me.dtefechadocumento.EditValue = New Date(2006, 5, 11, 0, 0, 0, 0)
        Me.dtefechadocumento.Location = New System.Drawing.Point(744, 81)
        Me.dtefechadocumento.Name = "dtefechadocumento"
        '
        'dtefechadocumento.Properties
        '
        Me.dtefechadocumento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtefechadocumento.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dtefechadocumento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dtefechadocumento.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dtefechadocumento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dtefechadocumento.Size = New System.Drawing.Size(96, 20)
        Me.dtefechadocumento.TabIndex = 16
        '
        'dtefecha
        '
        Me.dtefecha.EditValue = New Date(2006, 5, 11, 0, 0, 0, 0)
        Me.dtefecha.Location = New System.Drawing.Point(744, 35)
        Me.dtefecha.Name = "dtefecha"
        '
        'dtefecha.Properties
        '
        Me.dtefecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtefecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dtefecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dtefecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dtefecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dtefecha.Size = New System.Drawing.Size(96, 20)
        Me.dtefecha.TabIndex = 12
        '
        'lblIntereses
        '
        Me.lblIntereses.AutoSize = True
        Me.lblIntereses.Location = New System.Drawing.Point(416, 85)
        Me.lblIntereses.Name = "lblIntereses"
        Me.lblIntereses.Size = New System.Drawing.Size(61, 16)
        Me.lblIntereses.TabIndex = 9
        Me.lblIntereses.Text = "Intereses:"
        '
        'clcIntereses
        '
        Me.clcIntereses.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcIntereses.Location = New System.Drawing.Point(488, 84)
        Me.clcIntereses.MaxValue = 0
        Me.clcIntereses.MinValue = 0
        Me.clcIntereses.Name = "clcIntereses"
        '
        'clcIntereses.Properties
        '
        Me.clcIntereses.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcIntereses.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.EditFormat.FormatString = "###,###,##0.00"
        Me.clcIntereses.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcIntereses.Properties.MaskData.EditMask = "########0.00"
        Me.clcIntereses.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcIntereses.Size = New System.Drawing.Size(64, 19)
        Me.clcIntereses.TabIndex = 10
        Me.clcIntereses.Tag = ""
        '
        'frmTraspasoSaldos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(850, 432)
        Me.Controls.Add(Me.clcIntereses)
        Me.Controls.Add(Me.lblIntereses)
        Me.Controls.Add(Me.dtefecha)
        Me.Controls.Add(Me.dtefechadocumento)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblTipo_Plazo)
        Me.Controls.Add(Me.cboTipo_Plazo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.grSaldos)
        Me.Controls.Add(Me.grLetras)
        Me.Controls.Add(Me.lblComision)
        Me.Controls.Add(Me.clcDocumentos)
        Me.Controls.Add(Me.chkTodaCuenta)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lSaldo)
        Me.Controls.Add(Me.lblSaldo)
        Me.Controls.Add(Me.lkpClienteTraspaso)
        Me.Controls.Add(Me.lblClienteTraspaso)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblCliente)
        Me.Name = "frmTraspasoSaldos"
        Me.Text = "frmTraspasoSaldos"
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblClienteTraspaso, 0)
        Me.Controls.SetChildIndex(Me.lkpClienteTraspaso, 0)
        Me.Controls.SetChildIndex(Me.lblSaldo, 0)
        Me.Controls.SetChildIndex(Me.lSaldo, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.chkTodaCuenta, 0)
        Me.Controls.SetChildIndex(Me.clcDocumentos, 0)
        Me.Controls.SetChildIndex(Me.lblComision, 0)
        Me.Controls.SetChildIndex(Me.grLetras, 0)
        Me.Controls.SetChildIndex(Me.grSaldos, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.cboTipo_Plazo, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Plazo, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.dtefechadocumento, 0)
        Me.Controls.SetChildIndex(Me.dtefecha, 0)
        Me.Controls.SetChildIndex(Me.lblIntereses, 0)
        Me.Controls.SetChildIndex(Me.clcIntereses, 0)
        CType(Me.chkTodaCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grSaldos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvSaldos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIncluir, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDocumentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grLetras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvLetras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo_Plazo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtefechadocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtefecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcIntereses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Dim oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas

    Private Entro_Grid As Boolean = False
    Dim fila_seleccionada As Integer
    Private SaldoTotalCliente As Double = 0
    Private i As Long = 0
    Private y As Long = 0
    Private Saldo_Factura As Double = 0
    Private ovariables As New VillarrealBusiness.clsVariables

    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oReportes As VillarrealBusiness.Reportes
  
    '********** NOTA DE CREDITO ****************************
    Private tableLetrasSinSaldar As DataTable

    Private Sucursal_Factura As Long = -1
    Private Serie_Factura As String = ""
    Private Folio_Factura As Long = -1
    Private concepto_Factura As String = ""
    '****************************************

    Private Folio_Movimiento As Long = -1
    Private plazo_vencimiento As Long

    Private FolioNotaCredito As Long = 0
    Private FolioNotaCargo As Long = 0


    Private ReadOnly Property DelCliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property AlCliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteTraspaso)
        End Get
    End Property

    Private ReadOnly Property ConceptoNotaCargo() As String
        Get
            Return ovariables.TraeDatos("concepto_cxc_nota_cargo", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property SerieNotaCargo() As String
        Get
            Return Comunes.clsUtilerias.uti_SerieCajaNotaCargo(Comunes.Common.Caja_Actual)
            'Return Comunes.clsUtilerias.uti_SerieSucursalNotacargo(Comunes.Common.Sucursal_Actual)
        End Get
    End Property
    Private ReadOnly Property ConceptoNotaCredito() As String
        Get
            Return oVariables.TraeDatos("concepto_cxc_nota_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar)
        End Get
    End Property
    Private ReadOnly Property SerieNotaCredito() As String
        Get
            Return Comunes.clsUtilerias.uti_SerieCajasNotaCredito(Comunes.Common.Caja_Actual)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmNotasCargo_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmNotasCargo_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmNotasCargo_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        Me.ImprimirNotaCargo(Comunes.Common.Sucursal_Actual, ConceptoNotaCargo, Me.SerieNotaCargo, 0, FolioNotaCargo, AlCliente, True)
        Me.ImprimeNotaDeCredito(Comunes.Common.Sucursal_Actual, ConceptoNotaCredito, SerieNotaCredito, FolioNotaCredito, DelCliente, True)
    End Sub

    Private Sub frmTraspasoSaldos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        'ShowMessage(MessageType.MsgInformation, "El proceso no esta completo", , , True)
        Dim folio As Long = 0
        'Dim dimporte As Double = 0
        Dim impuesto As Double

        Dim subtotal_credito As Double = 0
        Dim subtotal_cargo As Double = 0

        Dim iva_credito As Double = 0
        Dim iva_cargo As Double = 0

        Dim importe_documento As Double
        Dim importe_nota_credito As Double
        Dim importe_nota_cargo As Double
        Dim numero_documentos_a_pagar As Long = 0
        Dim observaciones As String


        If Me.chkTodaCuenta.Checked = True Then
            numero_documentos_a_pagar = Me.grvSaldos.RowCount
            importe_nota_credito = SaldoTotalCliente
            importe_nota_cargo = SaldoTotalCliente + Me.clcIntereses.Value
        Else
            numero_documentos_a_pagar = 1
            importe_nota_credito = Saldo_Factura
            importe_nota_cargo = Saldo_Factura + Me.clcIntereses.Value
        End If


        impuesto = CType(ovariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)
        If impuesto <> -1 Then

            subtotal_credito = importe_nota_credito / (1 + (impuesto / 100))
            subtotal_cargo = importe_nota_cargo / (1 + (impuesto / 100))

            iva_credito = importe_nota_credito - subtotal_credito
            iva_cargo = importe_nota_cargo - subtotal_cargo

        Else
            ShowMessage(MessageType.MsgInformation, "El Porcentaje de Impuesto no esta definido", "Variables del Sistema", Nothing, False)
            Exit Sub
        End If

        ' Generacion de la Nota de Cargo
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Response = oMovimientosCobrar.InsertarCajas(Comunes.Common.Sucursal_Actual, Me.ConceptoNotaCargo, Me.SerieNotaCargo, Me.AlCliente, 0, Me.dtefecha.DateTime.Date, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, Me.clcDocumentos.EditValue, importe_nota_cargo, 0, subtotal_cargo, iva_cargo, importe_nota_cargo, importe_nota_cargo, System.DBNull.Value, "Nota de Cargo Generada desde Traspasos de Cuentas ", "", Comunes.Common.Sucursal_Actual, folio)
        If Response.ErrorFound Then
            Response.ShowError()
            Exit Sub
        End If

        FolioNotaCargo = folio


        'DAM 17/05/2006 -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO
        observaciones = "Nota de Cargo Generada para la cuenta " + AlCliente.ToString + " desde la cuenta " + DelCliente.ToString + ", desde Traspasos de Cuentas "
        Dim orow As DataRow
        For Each orow In CType(Me.grLetras.DataSource, DataTable).Rows
            Response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoNotaCargo, Me.SerieNotaCargo, folio, AlCliente, orow("documento"), 0, orow("fecha"), orow("importe"), System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, plazo_vencimiento, orow("fecha_vencimiento"), orow("importe"), "", observaciones, 0)
            If Response.ErrorFound Then
                Response.ShowError()
                Exit For
            End If
        Next

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Generacion de la Nota de Credito
        If Not Response.ErrorFound Then
            For y = 0 To numero_documentos_a_pagar - 1
                Response = oMovimientosCobrar.InsertarCajas(Comunes.Common.Sucursal_Actual, ConceptoNotaCredito, SerieNotaCredito, DelCliente, 0, Me.dtefecha.EditValue, Comunes.Common.Caja_Actual, -1, System.DBNull.Value, Me.clcDocumentos.EditValue, 0, importe_nota_credito, subtotal_credito, iva_credito, importe_nota_credito, 0, System.DBNull.Value, "Nota de Credito Generada desde Traspasos de Cuentas", "", Comunes.Common.Sucursal_Actual, folio)

                If Not Response.ErrorFound Then
                    Me.FolioNotaCredito = folio
                End If
                If numero_documentos_a_pagar > 1 Then Me.grvSaldos.SetRowCellValue(y, Me.grcIncluir, True)

                Me.CargarLetrasSinSaldar(Me.Sucursal_Factura, Me.concepto_Factura, Me.Serie_Factura, Me.Folio_Factura)
                For i = 0 To Me.tableLetrasSinSaldar.Rows.Count - 1
                    If importe_nota_credito > Me.tableLetrasSinSaldar.Rows(i).Item("saldo") Then
                        importe_documento = Me.tableLetrasSinSaldar.Rows(i).Item("saldo")
                    Else
                        importe_documento = importe_nota_credito
                    End If

                    'DAM 17/05/2006 -  SE AGREGO LAS OBSERVACIONES DEL DETALLE DEL MOVIMIENTO
                    observaciones = "Nota de Cr�dito Generada para la cuenta " + DelCliente.ToString + ", desde Traspasos de Cuentas "
                    If Not Response.ErrorFound Then Response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoNotaCredito, SerieNotaCredito, folio, DelCliente, _
                                                             i + 1, 0, Me.dtefecha.EditValue, importe_documento, Sucursal_Factura, concepto_Factura, Serie_Factura, Folio_Factura, DelCliente, tableLetrasSinSaldar.Rows(i).Item("documento"), 0, System.DBNull.Value, 0, "P", observaciones, 0)

                    importe_nota_credito = importe_nota_credito - importe_documento
                    If importe_nota_credito = 0 Then Exit For

                Next i
            Next y
        End If

    End Sub

    Private Sub frmTraspasoSaldos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas

        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        ovariables = New VillarrealBusiness.clsVariables
        oReportes = New VillarrealBusiness.Reportes

        Me.dtefecha.EditValue = CDate(TINApp.FechaServidor)
        Me.dtefechadocumento.EditValue = CDate(TINApp.FechaServidor)

    End Sub

    Private Sub frmTraspasoSaldos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Dim importe As Double = 0
        If Me.grvLetras.RowCount > 0 Then
            importe = Me.grvLetras.GetRowCellValue(0, Me.grcImporteLetra)
        End If
        Response = oMovimientosCobrar.ValidacionTraspasoCuenta(Action, Me.DelCliente, Me.AlCliente, Me.grvLetras.RowCount, importe)
    End Sub

    Private Sub frmTraspasoSaldos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If VerificaPermisoExtendido(Me.MenuOption.Name, "FECHA_TRA_SALDO") Then
            Me.dtefecha.Enabled = True
        Else
            Me.dtefecha.Enabled = False
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData

        Dim Response As New Events
        Response = oClientes.LookupCliente
        If Not Response.ErrorFound Then
            Me.lkpCliente.DataSource = CType(Response.Value, DataSet).Tables(0)
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Me.lkpCliente.DataSource = CType(Response.Value, DataSet).Tables(0)
        'End If
        'Response = Nothing

        CargarFacturas()
        Me.lSaldo.Text = Format(Me.grvSaldos.Columns("importe").SummaryItem.SummaryValue, "c2")
        SaldoTotalCliente = Me.grvSaldos.Columns("importe").SummaryItem.SummaryValue
        GeneraDocumentos()
        'IIf(IsNumeric(Me.grcImporte.SummaryText), CType(Me.grcImporte.SummaryText, Double), 0)
    End Sub

    Private Sub lkpClienteTraspaso_LoadData(ByVal Initialize As Boolean) Handles lkpClienteTraspaso.LoadData
        Dim oClientes As New VillarrealBusiness.clsClientes
        Dim Response As New Events
        Response = oClientes.LookupCliente
        If Not Response.ErrorFound Then
            Me.lkpClienteTraspaso.DataSource = CType(Response.Value, DataSet).Tables(0)
        End If
        Response = Nothing
    End Sub
    Private Sub lkpClienteTraspaso_Format() Handles lkpClienteTraspaso.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteTraspaso)
    End Sub

    Private Sub lkpClienteTraspaso_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpClienteTraspaso.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpClienteTraspaso.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Me.lkpClienteTraspaso.DataSource = CType(Response.Value, DataSet).Tables(0)
        'End If
        'Response = Nothing
    End Sub

    Private Sub chkTodaCuenta_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodaCuenta.CheckedChanged
        GeneraDocumentos()
    End Sub

    Private Sub clcDocumentos_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcDocumentos.EditValueChanged
        If Me.clcDocumentos.IsLoading = True Then Exit Sub
        GeneraDocumentos()
    End Sub

    Private Sub cboTipo_Plazo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipo_Plazo.SelectedIndexChanged
        If Me.cboTipo_Plazo.IsLoading = True Then Exit Sub
        GeneraDocumentos()
    End Sub

    Private Sub dtefechadocumento_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtefechadocumento.EditValueChanged
        If Me.dtefechadocumento.IsLoading = True Then Exit Sub
        GeneraDocumentos()
    End Sub

    Private Sub grvSaldos_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvSaldos.CellValueChanged
        If e.Column Is Me.grcIncluir Then
            If CType(e.Value, Boolean) = True Then
                Sucursal_Factura = CType(CType(Me.grvSaldos.DataSource, DataView).Table.Rows(e.RowHandle).Item("sucursal"), Long)
                concepto_Factura = CType(CType(Me.grvSaldos.DataSource, DataView).Table.Rows(e.RowHandle).Item("concepto"), String)
                Serie_Factura = CType(CType(Me.grvSaldos.DataSource, DataView).Table.Rows(e.RowHandle).Item("serie"), String)
                Folio_Factura = CType(CType(Me.grvSaldos.DataSource, DataView).Table.Rows(e.RowHandle).Item("folio"), Long)

            End If
        End If
    End Sub
    Private Sub grvSaldos_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvSaldos.CellValueChanging
        fila_seleccionada = Me.grvSaldos.FocusedRowHandle
        Saldo_Factura = Me.grvSaldos.GetRowCellValue(fila_seleccionada, Me.grcImporte)

        Dim i As Integer = 0
        For i = 0 To Me.grvSaldos.RowCount - 1
            If i <> fila_seleccionada Then
                Me.grvSaldos.SetRowCellValue(i, Me.grcIncluir, False)
            Else
                Me.grvSaldos.SetRowCellValue(i, Me.grcIncluir, True)
            End If
        Next
        GeneraDocumentos()
    End Sub
    Private Sub clcIntereses_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcIntereses.EditValueChanged
        If Me.clcIntereses.IsLoading = True Then Exit Sub
        GeneraDocumentos()
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CargarFacturas()
        Dim Response As Dipros.Utils.Events
        Try
            Response = oVentas.FacturasyNotasCreditoPorCliente(DelCliente)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grSaldos.DataSource = oDataSet.Tables(0)
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Me.grvSaldos.SetRowCellValue(0, Me.grcIncluir, True)
                    grvSaldos_CellValueChanging(Nothing, Nothing)
                Else
                    Saldo_Factura = 0
                End If
                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub GeneraDocumentos()

        Dim fecha_anterior As DateTime

        If IsNumeric(Me.clcDocumentos.EditValue) = False Then Exit Sub


        Dim i As Long = 0

        Dim orow As DataRow
        Dim odatatable As New DataTable
        Dim importe As Double
        '@ACH-25/06/07: Agregu� en el c�lculo del importe los intereses por el traspaso de saldos
        If Me.clcIntereses.EditValue = Nothing Then clcIntereses.EditValue = 0
        If Me.chkTodaCuenta.Checked = True Then
            importe = (SaldoTotalCliente + Me.clcIntereses.EditValue) / Me.clcDocumentos.EditValue
            'importe = SaldoTotalCliente / Me.clcDocumentos.EditValue
        Else
            importe = (Saldo_Factura + Me.clcIntereses.EditValue) / Me.clcDocumentos.EditValue
            'importe = Saldo_Factura / Me.clcDocumentos.EditValue
        End If

        importe = System.Math.Round(importe, 0)
        '/@ACH-25/06/07: Agregu� en el c�lculo del importe los intereses por el traspaso de saldos

        odatatable.Columns.Add("documento").DataType = System.Type.GetType("System.Int32")
        odatatable.Columns.Add("fecha").DataType = System.Type.GetType("System.DateTime")
        odatatable.Columns.Add("fecha_vencimiento").DataType = System.Type.GetType("System.DateTime")
        odatatable.Columns.Add("importe").DataType = System.Type.GetType("System.Double")

        Select Case Me.cboTipo_Plazo.Value
            Case 0
                plazo_vencimiento = 7
            Case 1
                plazo_vencimiento = 15
            Case 2
                plazo_vencimiento = 30
        End Select


        fecha_anterior = ValidaFecha()

        For i = 1 To Me.clcDocumentos.EditValue
            orow = odatatable.NewRow
            orow("documento") = i
            orow("fecha") = Me.dtefecha.EditValue
            orow("importe") = importe

            If plazo_vencimiento = 30 Then

                If i > 1 Then
                    'If fecha_anterior.Day = 15 Then
                    fecha_anterior = fecha_anterior.AddMonths(1)
                    'Else
                    '    fecha_anterior = "01/" + CStr(fecha_anterior.Month) + "/" + CStr(fecha_anterior.Year)
                    '    fecha_anterior = fecha_anterior.AddMonths(2)
                    '    fecha_anterior = fecha_anterior.AddDays(-1)
                    'End If
                End If
            Else
                fecha_anterior = fecha_anterior.AddDays(plazo_vencimiento)
            End If

            orow("fecha_vencimiento") = fecha_anterior

            odatatable.Rows.Add(orow)
        Next i
        Me.grLetras.DataSource = odatatable

    End Sub
    Private Function ValidaFecha() As DateTime
        Dim fecha_regreso As DateTime

        If IsDate(Me.dtefechadocumento.Text) Then
            If Me.dtefechadocumento.DateTime.Day <= 15 Then
                fecha_regreso = "15/" + Me.dtefechadocumento.DateTime.Month.ToString + "/" + Me.dtefechadocumento.DateTime.Year.ToString
            Else
                fecha_regreso = Me.dtefechadocumento.DateTime.Date.DaysInMonth(dtefechadocumento.DateTime.Date.Year, dtefechadocumento.DateTime.Date.Month).ToString + "/" + Me.dtefechadocumento.DateTime.Month.ToString + "/" + Me.dtefechadocumento.DateTime.Year.ToString
            End If
        Else
            fecha_regreso = "15/" + CDate(TINApp.FechaServidor).Date.Month.ToString + "/" + CDate(TINApp.FechaServidor).Date.Year.ToString
        End If

        'Me.fecha_vencimiento = fecha_regreso
        Return fecha_regreso

    End Function

    Private Sub CargarLetrasSinSaldar(ByVal Sucursal_F As Long, ByVal Concepto_F As String, ByVal serie_F As String, ByVal Folio_F As Long)
        Dim Response As Dipros.Utils.Events
        Try
            Response = oMovimientosCobrarDetalle.LetraDeFacturaTraspasos(Sucursal_F, Concepto_F, serie_F, Folio_F, DelCliente)

            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                tableLetrasSinSaldar = oDataSet.Tables(0)
                oDataSet = Nothing
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub ImprimeNotaDeCredito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, Optional ByVal iva_desglosado As Boolean = True)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            Response = oReportes.ImprimeNotaDeCredito(sucursal, concepto, serie, folio, cliente, iva_desglosado)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Las Notas de Cr�dito no pueden Mostrarse")
            Else
                If Response.Value.Tables(0).Rows.Count > 0 Then
                    Dim oDataSet As DataSet
                    Dim oReport As New Comunes.rptNotaDeCredito

                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                    TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cr�dito " & CType(folio, String), oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub
    Private Sub ImprimirNotaCargo(ByVal Sucursal_Actual As Long, ByVal conceptointereses As String, ByVal serieinteres As String, ByVal saldo_intereses As Double, ByVal foliointeres As Long, ByVal cliente As Long, ByVal Iva_Desglosado As Boolean)
        Dim response As New Events

        response = oReportes.ImprimeNotaDeCargo(Sucursal_Actual, conceptointereses, serieinteres, foliointeres, cliente, Iva_Desglosado)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La Nota de Cargo no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptNotaCargo   'Clientes.rptNotaCargo

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cargo ", oReport)
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub


#End Region

    
End Class
