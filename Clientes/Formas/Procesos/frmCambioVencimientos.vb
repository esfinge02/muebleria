Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCambioVencimientos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaVencimiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteLetra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFacturas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grvLetras As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grLetras As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcSucursalNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFechaVencimientoNueva As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdCambiarFechas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents clcAnioEnvio As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents clcQuincenaEnvio As Dipros.Editors.TINCalcEdit
    Friend WithEvents gpEnvios As System.Windows.Forms.GroupBox
    Friend WithEvents grcQuincenaEnvio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAnioEnvio As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambioVencimientos))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.grFacturas = New DevExpress.XtraGrid.GridControl
        Me.grvFacturas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursalNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.grLetras = New DevExpress.XtraGrid.GridControl
        Me.grvLetras = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaVencimiento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFechaVencimientoNueva = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteLetra = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcQuincenaEnvio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAnioEnvio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.cmdCambiarFechas = New DevExpress.XtraEditors.SimpleButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.gpEnvios = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.clcAnioEnvio = New Dipros.Editors.TINCalcEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.clcQuincenaEnvio = New Dipros.Editors.TINCalcEdit
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grLetras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvLetras, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpEnvios.SuspendLayout()
        CType(Me.clcAnioEnvio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcQuincenaEnvio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5277, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(16, 40)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cl&iente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(72, 38)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.Required = True
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(241, 20)
        Me.lkpCliente.TabIndex = 1
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'grFacturas
        '
        '
        'grFacturas.EmbeddedNavigator
        '
        Me.grFacturas.EmbeddedNavigator.Name = ""
        Me.grFacturas.Location = New System.Drawing.Point(8, 113)
        Me.grFacturas.MainView = Me.grvFacturas
        Me.grFacturas.Name = "grFacturas"
        Me.grFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grFacturas.Size = New System.Drawing.Size(413, 295)
        Me.grFacturas.TabIndex = 6
        Me.grFacturas.Text = "Facturas"
        '
        'grvFacturas
        '
        Me.grvFacturas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcSucursal, Me.grcSucursalNombre, Me.grcFactura, Me.grcFecha, Me.grcImporte})
        Me.grvFacturas.GridControl = Me.grFacturas
        Me.grvFacturas.Name = "grvFacturas"
        Me.grvFacturas.OptionsCustomization.AllowFilter = False
        Me.grvFacturas.OptionsCustomization.AllowGroup = False
        Me.grvFacturas.OptionsCustomization.AllowSort = False
        Me.grvFacturas.OptionsView.ShowGroupPanel = False
        Me.grvFacturas.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 35
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSucursalNombre
        '
        Me.grcSucursalNombre.Caption = "Sucursal"
        Me.grcSucursalNombre.FieldName = "sucursal_nombre"
        Me.grcSucursalNombre.Name = "grcSucursalNombre"
        Me.grcSucursalNombre.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursalNombre.VisibleIndex = 1
        Me.grcSucursalNombre.Width = 195
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 2
        Me.grcFactura.Width = 164
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha_factura"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 131
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 137
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(16, 64)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(191, 16)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "Cambiar vencimiento a partir &del:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(216, 62)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha.TabIndex = 3
        Me.dteFecha.Tag = ""
        '
        'grLetras
        '
        '
        'grLetras.EmbeddedNavigator
        '
        Me.grLetras.EmbeddedNavigator.Name = ""
        Me.grLetras.Location = New System.Drawing.Point(428, 113)
        Me.grLetras.MainView = Me.grvLetras
        Me.grLetras.Name = "grLetras"
        Me.grLetras.Size = New System.Drawing.Size(414, 295)
        Me.grLetras.TabIndex = 9
        Me.grLetras.Text = "Letras"
        '
        'grvLetras
        '
        Me.grvLetras.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcDocumento, Me.grcFechaVencimiento, Me.grcFechaVencimientoNueva, Me.grcImporteLetra, Me.grcQuincenaEnvio, Me.grcAnioEnvio})
        Me.grvLetras.GridControl = Me.grLetras
        Me.grvLetras.Name = "grvLetras"
        Me.grvLetras.OptionsBehavior.Editable = False
        Me.grvLetras.OptionsCustomization.AllowFilter = False
        Me.grvLetras.OptionsCustomization.AllowGroup = False
        Me.grvLetras.OptionsCustomization.AllowSort = False
        Me.grvLetras.OptionsView.ShowGroupPanel = False
        Me.grvLetras.OptionsView.ShowIndicator = False
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Documento"
        Me.grcDocumento.FieldName = "documento"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.VisibleIndex = 0
        Me.grcDocumento.Width = 70
        '
        'grcFechaVencimiento
        '
        Me.grcFechaVencimiento.Caption = "Fecha"
        Me.grcFechaVencimiento.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaVencimiento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaVencimiento.FieldName = "fecha_vencimiento"
        Me.grcFechaVencimiento.Name = "grcFechaVencimiento"
        Me.grcFechaVencimiento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaVencimiento.VisibleIndex = 1
        Me.grcFechaVencimiento.Width = 103
        '
        'grcFechaVencimientoNueva
        '
        Me.grcFechaVencimientoNueva.Caption = "Fecha Nueva"
        Me.grcFechaVencimientoNueva.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFechaVencimientoNueva.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFechaVencimientoNueva.FieldName = "fecha_vencimiento_nueva"
        Me.grcFechaVencimientoNueva.Name = "grcFechaVencimientoNueva"
        Me.grcFechaVencimientoNueva.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFechaVencimientoNueva.VisibleIndex = 2
        Me.grcFechaVencimientoNueva.Width = 95
        '
        'grcImporteLetra
        '
        Me.grcImporteLetra.Caption = "Importe"
        Me.grcImporteLetra.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcImporteLetra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteLetra.FieldName = "importe"
        Me.grcImporteLetra.Name = "grcImporteLetra"
        Me.grcImporteLetra.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteLetra.VisibleIndex = 3
        Me.grcImporteLetra.Width = 116
        '
        'grcQuincenaEnvio
        '
        Me.grcQuincenaEnvio.Caption = "Quincena"
        Me.grcQuincenaEnvio.FieldName = "quincena_envio"
        Me.grcQuincenaEnvio.Name = "grcQuincenaEnvio"
        Me.grcQuincenaEnvio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcAnioEnvio
        '
        Me.grcAnioEnvio.Caption = "A�o"
        Me.grcAnioEnvio.FieldName = "anio_envio"
        Me.grcAnioEnvio.Name = "grcAnioEnvio"
        Me.grcAnioEnvio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'cmdCambiarFechas
        '
        Me.cmdCambiarFechas.Location = New System.Drawing.Point(320, 61)
        Me.cmdCambiarFechas.Name = "cmdCambiarFechas"
        Me.cmdCambiarFechas.Size = New System.Drawing.Size(98, 23)
        Me.cmdCambiarFechas.TabIndex = 4
        Me.cmdCambiarFechas.Text = "Ca&mbiar Fechas"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 17)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "&FACTURAS"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(428, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 17)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "&LETRAS"
        '
        'gpEnvios
        '
        Me.gpEnvios.Controls.Add(Me.Label9)
        Me.gpEnvios.Controls.Add(Me.clcAnioEnvio)
        Me.gpEnvios.Controls.Add(Me.Label8)
        Me.gpEnvios.Controls.Add(Me.clcQuincenaEnvio)
        Me.gpEnvios.Enabled = False
        Me.gpEnvios.Location = New System.Drawing.Point(428, 36)
        Me.gpEnvios.Name = "gpEnvios"
        Me.gpEnvios.Size = New System.Drawing.Size(308, 48)
        Me.gpEnvios.TabIndex = 7
        Me.gpEnvios.TabStop = False
        Me.gpEnvios.Text = "Envios"
        Me.gpEnvios.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(200, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(30, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Tag = ""
        Me.Label9.Text = "A�o:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAnioEnvio
        '
        Me.clcAnioEnvio.EditValue = "0"
        Me.clcAnioEnvio.Location = New System.Drawing.Point(240, 19)
        Me.clcAnioEnvio.MaxValue = 0
        Me.clcAnioEnvio.MinValue = 0
        Me.clcAnioEnvio.Name = "clcAnioEnvio"
        '
        'clcAnioEnvio.Properties
        '
        Me.clcAnioEnvio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnioEnvio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnioEnvio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcAnioEnvio.Properties.MaxLength = 4
        Me.clcAnioEnvio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAnioEnvio.Size = New System.Drawing.Size(54, 19)
        Me.clcAnioEnvio.TabIndex = 7
        Me.clcAnioEnvio.Tag = "anio_envio"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(40, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Tag = ""
        Me.Label8.Text = "Quincena:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcQuincenaEnvio
        '
        Me.clcQuincenaEnvio.EditValue = "0"
        Me.clcQuincenaEnvio.Location = New System.Drawing.Point(104, 19)
        Me.clcQuincenaEnvio.MaxValue = 0
        Me.clcQuincenaEnvio.MinValue = 0
        Me.clcQuincenaEnvio.Name = "clcQuincenaEnvio"
        '
        'clcQuincenaEnvio.Properties
        '
        Me.clcQuincenaEnvio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincenaEnvio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincenaEnvio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcQuincenaEnvio.Properties.MaxLength = 2
        Me.clcQuincenaEnvio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcQuincenaEnvio.Size = New System.Drawing.Size(54, 19)
        Me.clcQuincenaEnvio.TabIndex = 5
        Me.clcQuincenaEnvio.Tag = "quincena_envio"
        '
        'frmCambioVencimientos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(852, 416)
        Me.Controls.Add(Me.gpEnvios)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grLetras)
        Me.Controls.Add(Me.cmdCambiarFechas)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.grFacturas)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Name = "frmCambioVencimientos"
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.grFacturas, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.cmdCambiarFechas, 0)
        Me.Controls.SetChildIndex(Me.grLetras, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.gpEnvios, 0)
        CType(Me.grFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grLetras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvLetras, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpEnvios.ResumeLayout(False)
        CType(Me.clcAnioEnvio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcQuincenaEnvio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVentas As VillarrealBusiness.clsVentas
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private j As Integer

    Private Sucursal As Long = -1
    Private Serie As String = ""
    Private Folio As Long = -1
    Private fecha_vencimiento As Date

    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property SucursalDependencia() As Boolean
        Get
            Return Comunes.clsUtilerias.uti_SucursalDependencia(Comunes.Common.Sucursal_Actual)
        End Get
    End Property
    Private ReadOnly Property QuincenaEnvioDeSucursal() As Long
        Get
            Return Comunes.clsUtilerias.uti_SucursalQuincenaEnvio(Comunes.Common.Sucursal_Actual)
        End Get
    End Property




#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCambioVencimientos_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmCambioVencimientos_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub
    Private Sub frmCambioVencimientos_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmCambioVencimientos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientes = New VillarrealBusiness.clsClientes
        oVentas = New VillarrealBusiness.clsVentas
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle

        Me.Location = New Point(0, 0)
        Me.dteFecha.EditValue = CDate(TINApp.FechaServidor)
    End Sub
    Private Sub frmCambioVencimientos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Dim orow As DataRow
        For Each orow In CType(Me.grLetras.DataSource, DataTable).Rows
            If Not Response.ErrorFound Then

                Response = oMovimientosCobrarDetalle.CambiaFechaVencimiento(Sucursal, Serie, Folio, Cliente, orow("documento"), orow("fecha_vencimiento_nueva"), orow("quincena_envio"), orow("anio_envio"))

            End If
        Next

        If Not Response.ErrorFound Then
            oVentas.ActualizaQuincenaAnioEnvio(Sucursal, Serie, Folio, Me.clcQuincenaEnvio.EditValue, Me.clcAnioEnvio.EditValue)
        End If
    End Sub
    Private Sub frmCambioVencimientos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields

    End Sub
    Private Sub frmCambioVencimientos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        ValidaFecha()
        Response = Validacion()
    End Sub


#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupFechaVenta()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(Comunes.clsUtilerias.uti_SucursalDependencia(Comunes.Common.Sucursal_Actual), lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing

        Me.grLetras.DataSource = Nothing
        CargarFacturas()
    End Sub

    Private Sub grvFacturas_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvFacturas.CellValueChanged
        If e.Column Is Me.grcSeleccionar Then
            If CType(e.Value, Boolean) = True Then
                Sucursal = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("sucursal"), Long)
                Serie = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("serie"), String)
                Folio = CType(CType(Me.grvFacturas.DataSource, DataView).Table.Rows(e.RowHandle).Item("folio"), Long)

                Me.CargarLetras()

                If SucursalDependencia = True Then
                    Me.gpEnvios.Enabled = True
                    Me.gpEnvios.Visible = True
                    Me.gpEnvios.Focus()
                    Me.grcQuincenaEnvio.VisibleIndex = 4
                    Me.grcAnioEnvio.VisibleIndex = 5
                    Me.grvLetras.BestFitColumns()
                Else
                    Me.gpEnvios.Enabled = False
                    Me.gpEnvios.Visible = False
                    Me.grcQuincenaEnvio.VisibleIndex = -1
                    Me.grcAnioEnvio.VisibleIndex = -1

                End If
            End If
        End If
    End Sub
    Private Sub grvFacturas_CellValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles grvFacturas.CellValueChanging
        'Dim j As Integer = Me.grvFacturas.FocusedRowHandle

        j = Me.grvFacturas.FocusedRowHandle
        Dim i As Integer = 0
        For i = 0 To Me.grvFacturas.RowCount - 1
            If i <> j Then
                Me.grvFacturas.SetRowCellValue(i, Me.grcSeleccionar, False)
            Else
                Me.grvFacturas.SetRowCellValue(i, Me.grcSeleccionar, True)
            End If
        Next
        Me.grvFacturas.UpdateCurrentRow()
    End Sub

    Private Sub cmdCambiarFechas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCambiarFechas.Click
        Dim oevent As New Events
        ValidaFecha()
        oevent = Validacion()
        If Not oevent.ErrorFound Then
            CargarFechasVencimientoNuevas()
        Else
            oevent.ShowError()
        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub CargarFacturas()
        Dim Response As Dipros.Utils.Events
        Try
            Response = oVentas.FacturasCreditoPorCliente(Cliente)
            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                Me.grFacturas.DataSource = oDataSet.Tables(0)

                oDataSet = Nothing
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub CargarLetras()

        Dim Response As Dipros.Utils.Events
        Try
            Response = oMovimientosCobrarDetalle.LetrasDeFactura(Sucursal, Serie, Folio, Cliente, False)

            If Not Response.ErrorFound Then
                Dim oDataSet As DataSet
                oDataSet = Response.Value
                'CType(Me.grLetras.DataSource, DataTable).Rows.Add(oDataSet.Tables(0).Rows(0))
                Me.grLetras.DataSource = oDataSet.Tables(0)
                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Me.clcQuincenaEnvio.EditValue = CType(oDataSet.Tables(0).Rows(0).Item("quincena_envio"), Long)
                    Me.clcAnioEnvio.EditValue = CType(oDataSet.Tables(0).Rows(0).Item("anio_envio"), Long)
                End If
                oDataSet = Nothing
            End If

        Catch ex As Exception
        End Try
    End Sub
    Private Sub CargarFechasVencimientoNuevas()
        If Me.grvLetras.RowCount < 1 Then Exit Sub
        Dim Response As New Dipros.Utils.Events
        Dim quincena_envio As Long
        Dim anio_envio As Long

        Try

            Dim orow As DataRow

            quincena_envio = CLng(Me.clcQuincenaEnvio.EditValue)
            anio_envio = CLng(Me.clcAnioEnvio.EditValue)

            For Each orow In CType(Me.grLetras.DataSource, DataTable).Rows
                orow("fecha_vencimiento_nueva") = fecha_vencimiento
                orow("quincena_envio") = quincena_envio
                orow("anio_envio") = anio_envio

                If CType(orow("plazo"), Long) = 30 Then  '''PLAZO MENSUAL
                    If fecha_vencimiento.Day = 15 Then
                        fecha_vencimiento = fecha_vencimiento.AddMonths(1)
                    Else
                        fecha_vencimiento = "01/" + CStr(fecha_vencimiento.Month) + "/" + CStr(fecha_vencimiento.Year)
                        fecha_vencimiento = fecha_vencimiento.AddMonths(2)
                        fecha_vencimiento = fecha_vencimiento.AddDays(-1)
                    End If

                Else                                    '''OTRO PLAZO
                    If fecha_vencimiento.Day = 15 Then
                        fecha_vencimiento = "01/" + CStr(fecha_vencimiento.Month) + "/" + CStr(fecha_vencimiento.Year)
                        fecha_vencimiento = fecha_vencimiento.AddMonths(1)
                        fecha_vencimiento = fecha_vencimiento.AddDays(-1)

                    Else
                        fecha_vencimiento = fecha_vencimiento.AddDays(orow("plazo"))
                    End If
                End If

                If SucursalDependencia = True Then
                    quincena_envio = quincena_envio + 1
                    If quincena_envio > 24 Then
                        quincena_envio = 1
                        anio_envio = anio_envio + 1
                    End If
                End If
            Next

        Catch ex As Exception
        End Try

    End Sub

    Private Function Validacion() As Events
        Dim oevent As New Events
        If CDate(fecha_vencimiento.ToShortDateString) < CDate(CType(Me.grvFacturas.GetRowCellValue(j, Me.grcFecha), Date).ToShortDateString) Then
            oevent.Message = "La Fecha de las Letras No Pueden ser Menores a la Fecha de Factura"
            Return oevent
        End If

        If Me.grvLetras.RowCount < 1 Then
            oevent.Message = "No hay Letras para Cambiar Fecha de Vencimiento"
            Return oevent
        End If

        If SucursalDependencia = True Then
            If Me.clcQuincenaEnvio.EditValue < QuincenaEnvioDeSucursal Then
                oevent.Message = "La Quincena de Env�o No Puede Ser Menor de " + CStr(QuincenaEnvioDeSucursal)
                Return oevent
            End If

            If Me.clcQuincenaEnvio.EditValue > 24 Then
                oevent.Message = "La Quincena de Env�o No Puede Ser Mayor de 24"
                Return oevent
            End If

            If Me.clcAnioEnvio.EditValue < CType(Me.grvFacturas.GetRowCellValue(j, Me.grcFecha), Date).Year Then
                oevent.Message = "El A�o de Env�o No Puede Ser Menor Que el A�o de la Factura"
                Return oevent
            End If
        End If

        Return oevent
    End Function
    Private Sub ValidaFecha()
        If IsDate(Me.dteFecha.Text) Then
            If Me.dteFecha.DateTime.Day <= 15 Then
                Me.dteFecha.DateTime = "15/" + Me.dteFecha.DateTime.Month.ToString + "/" + Me.dteFecha.DateTime.Year.ToString
            Else
                Me.dteFecha.DateTime = Me.dteFecha.DateTime.Date.DaysInMonth(dteFecha.DateTime.Date.Year, dteFecha.DateTime.Date.Month).ToString + "/" + Me.dteFecha.DateTime.Month.ToString + "/" + Me.dteFecha.DateTime.Year.ToString
            End If
        Else
            Me.dteFecha.DateTime = "15/" + CDate(TINApp.FechaServidor).Date.Month.ToString + "/" + CDate(TINApp.FechaServidor).Date.Year.ToString
        End If

        Me.fecha_vencimiento = Me.dteFecha.DateTime
    End Sub

#End Region



End Class
