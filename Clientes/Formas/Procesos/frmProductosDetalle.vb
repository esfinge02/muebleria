Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmProductosDetalle
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Public WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConvenioAplica As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    Public WithEvents lblNombreCliente As System.Windows.Forms.Label
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Public WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmProductosDetalle))
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblConvenioAplica = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        Me.lblNombreCliente = New System.Windows.Forms.Label
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(606, 28)
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(56, 40)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(112, 40)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(520, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(360, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 1
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblConvenioAplica
        '
        Me.lblConvenioAplica.AutoSize = True
        Me.lblConvenioAplica.Location = New System.Drawing.Point(8, 64)
        Me.lblConvenioAplica.Name = "lblConvenioAplica"
        Me.lblConvenioAplica.Size = New System.Drawing.Size(97, 16)
        Me.lblConvenioAplica.TabIndex = 2
        Me.lblConvenioAplica.Tag = ""
        Me.lblConvenioAplica.Text = "Convenio Aplica:"
        Me.lblConvenioAplica.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(112, 64)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = ""
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(360, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 3
        Me.lkpConvenio.Tag = "convenio_aplica"
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'lblNombreCliente
        '
        Me.lblNombreCliente.AutoSize = True
        Me.lblNombreCliente.Location = New System.Drawing.Point(112, 123)
        Me.lblNombreCliente.Name = "lblNombreCliente"
        Me.lblNombreCliente.Size = New System.Drawing.Size(46, 16)
        Me.lblNombreCliente.TabIndex = 6
        Me.lblNombreCliente.Tag = "nombre_cliente"
        Me.lblNombreCliente.Text = "nombre"
        Me.lblNombreCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(52, 88)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 4
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(112, 88)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcImporte.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte.TabIndex = 5
        Me.clcImporte.TabStop = False
        Me.clcImporte.Tag = "importe"
        '
        'frmProductosDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(482, 116)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lblConvenioAplica)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblNombreCliente)
        Me.Name = "frmProductosDetalle"
        Me.Text = "frmProductosDetalle"
        Me.Controls.SetChildIndex(Me.lblNombreCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioAplica, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oProductosDetalle As New VillarrealBusiness.clsProductosDetalle
    Private oConvenios As New VillarrealBusiness.clsConvenios
    Private oClientes As New VillarrealBusiness.clsClientes

    ReadOnly Property Cliente() As Long
        Get
            Return PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property

    ReadOnly Property ConvenioAplica() As Long
        Get
            Return PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property
    Private Sub frmProductosDetalle_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl

            Select Case Action

                Case Actions.Insert
                    .AddRow(Me.DataSource)

                Case Actions.Update
                    .UpdateRow(Me.DataSource)

                Case Actions.Delete
                    .DeleteRow()

            End Select

        End With
    End Sub
    Private Sub frmProductosDetalle_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete

        End Select

    End Sub
    Private Sub frmProductosDetalle_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow

    End Sub
    Private Sub frmProductosDetalle_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = Me.oProductosDetalle.Validacion(Me.Cliente, Me.ConvenioAplica)
    End Sub


    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        If Me.Cliente > 0 Then
            Me.lblNombreCliente.Text = Me.lkpCliente.GetValue("nombre")
        Else
            Me.lblNombreCliente.Text = ""
        End If
    End Sub

    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub


End Class
