Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmProductos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblTipo_Plazo As System.Windows.Forms.Label
    Friend WithEvents cboEstatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents gbxQuincenaAnio As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents clcAnio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblQuincena As System.Windows.Forms.Label
    Friend WithEvents clcQuincena As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblConvenioCambiado As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    Friend WithEvents dteFechaAplicacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConvenioAplica As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaProductosDetalle As Dipros.Windows.TINMaster
    Friend WithEvents grProductoDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvProductoDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmProductos))
        Me.lblTipo_Plazo = New System.Windows.Forms.Label
        Me.cboEstatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.gbxQuincenaAnio = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.clcAnio = New Dipros.Editors.TINCalcEdit
        Me.lblQuincena = New System.Windows.Forms.Label
        Me.clcQuincena = New Dipros.Editors.TINCalcEdit
        Me.lblConvenioCambiado = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        Me.dteFechaAplicacion = New DevExpress.XtraEditors.DateEdit
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.grProductoDetalle = New DevExpress.XtraGrid.GridControl
        Me.grvProductoDetalle = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConvenioAplica = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaProductosDetalle = New Dipros.Windows.TINMaster
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxQuincenaAnio.SuspendLayout()
        CType(Me.clcAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcQuincena.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaAplicacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grProductoDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvProductoDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1950, 28)
        '
        'lblTipo_Plazo
        '
        Me.lblTipo_Plazo.AutoSize = True
        Me.lblTipo_Plazo.Location = New System.Drawing.Point(494, 41)
        Me.lblTipo_Plazo.Name = "lblTipo_Plazo"
        Me.lblTipo_Plazo.Size = New System.Drawing.Size(50, 16)
        Me.lblTipo_Plazo.TabIndex = 59
        Me.lblTipo_Plazo.Tag = ""
        Me.lblTipo_Plazo.Text = "Estatus:"
        Me.lblTipo_Plazo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstatus
        '
        Me.cboEstatus.EditValue = "P"
        Me.cboEstatus.Location = New System.Drawing.Point(552, 40)
        Me.cboEstatus.Name = "cboEstatus"
        '
        'cboEstatus.Properties
        '
        Me.cboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstatus.Properties.Enabled = False
        Me.cboEstatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pendiente", "P", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Aplicado", "A", -1)})
        Me.cboEstatus.Size = New System.Drawing.Size(96, 23)
        Me.cboEstatus.TabIndex = 60
        Me.cboEstatus.Tag = "estatus"
        '
        'gbxQuincenaAnio
        '
        Me.gbxQuincenaAnio.Controls.Add(Me.Label7)
        Me.gbxQuincenaAnio.Controls.Add(Me.clcAnio)
        Me.gbxQuincenaAnio.Controls.Add(Me.lblQuincena)
        Me.gbxQuincenaAnio.Controls.Add(Me.clcQuincena)
        Me.gbxQuincenaAnio.Location = New System.Drawing.Point(88, 64)
        Me.gbxQuincenaAnio.Name = "gbxQuincenaAnio"
        Me.gbxQuincenaAnio.Size = New System.Drawing.Size(360, 56)
        Me.gbxQuincenaAnio.TabIndex = 61
        Me.gbxQuincenaAnio.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(203, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(30, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Tag = ""
        Me.Label7.Text = "A�o:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAnio
        '
        Me.clcAnio.EditValue = "0"
        Me.clcAnio.Location = New System.Drawing.Point(243, 18)
        Me.clcAnio.MaxValue = 0
        Me.clcAnio.MinValue = 0
        Me.clcAnio.Name = "clcAnio"
        '
        'clcAnio.Properties
        '
        Me.clcAnio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnio.Properties.Enabled = False
        Me.clcAnio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcAnio.Properties.MaxLength = 4
        Me.clcAnio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAnio.Size = New System.Drawing.Size(48, 19)
        Me.clcAnio.TabIndex = 5
        Me.clcAnio.Tag = "anio"
        '
        'lblQuincena
        '
        Me.lblQuincena.AutoSize = True
        Me.lblQuincena.Location = New System.Drawing.Point(51, 21)
        Me.lblQuincena.Name = "lblQuincena"
        Me.lblQuincena.Size = New System.Drawing.Size(60, 16)
        Me.lblQuincena.TabIndex = 2
        Me.lblQuincena.Tag = ""
        Me.lblQuincena.Text = "Quincena:"
        Me.lblQuincena.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcQuincena
        '
        Me.clcQuincena.EditValue = "0"
        Me.clcQuincena.Location = New System.Drawing.Point(115, 18)
        Me.clcQuincena.MaxValue = 0
        Me.clcQuincena.MinValue = 0
        Me.clcQuincena.Name = "clcQuincena"
        '
        'clcQuincena.Properties
        '
        Me.clcQuincena.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincena.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincena.Properties.Enabled = False
        Me.clcQuincena.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcQuincena.Properties.MaxLength = 2
        Me.clcQuincena.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcQuincena.Size = New System.Drawing.Size(48, 19)
        Me.clcQuincena.TabIndex = 3
        Me.clcQuincena.Tag = "quincena"
        '
        'lblConvenioCambiado
        '
        Me.lblConvenioCambiado.AutoSize = True
        Me.lblConvenioCambiado.Location = New System.Drawing.Point(24, 41)
        Me.lblConvenioCambiado.Name = "lblConvenioCambiado"
        Me.lblConvenioCambiado.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenioCambiado.TabIndex = 63
        Me.lblConvenioCambiado.Tag = ""
        Me.lblConvenioCambiado.Text = "Convenio:"
        Me.lblConvenioCambiado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Enabled = False
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(88, 40)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = ""
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(360, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 64
        Me.lkpConvenio.Tag = "convenio"
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'dteFechaAplicacion
        '
        Me.dteFechaAplicacion.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFechaAplicacion.Location = New System.Drawing.Point(552, 98)
        Me.dteFechaAplicacion.Name = "dteFechaAplicacion"
        '
        'dteFechaAplicacion.Properties
        '
        Me.dteFechaAplicacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaAplicacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaAplicacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaAplicacion.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaAplicacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaAplicacion.Properties.Enabled = False
        Me.dteFechaAplicacion.Size = New System.Drawing.Size(96, 23)
        Me.dteFechaAplicacion.TabIndex = 68
        Me.dteFechaAplicacion.Tag = "fecha_aplicacion"
        Me.dteFechaAplicacion.ToolTip = "Fecha Aplicaci�n del Producto"
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(552, 70)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.Enabled = False
        Me.dteFecha_Ini.Size = New System.Drawing.Size(96, 23)
        Me.dteFecha_Ini.TabIndex = 66
        Me.dteFecha_Ini.Tag = "fecha"
        Me.dteFecha_Ini.ToolTip = "Fecha del Producto"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(480, 101)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 16)
        Me.Label5.TabIndex = 67
        Me.Label5.Text = "Aplicaci�n:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(503, 75)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 65
        Me.lblFecha.Text = "Fecha:"
        '
        'grProductoDetalle
        '
        '
        'grProductoDetalle.EmbeddedNavigator
        '
        Me.grProductoDetalle.EmbeddedNavigator.Name = ""
        Me.grProductoDetalle.Location = New System.Drawing.Point(9, 160)
        Me.grProductoDetalle.MainView = Me.grvProductoDetalle
        Me.grProductoDetalle.Name = "grProductoDetalle"
        Me.grProductoDetalle.Size = New System.Drawing.Size(688, 288)
        Me.grProductoDetalle.TabIndex = 69
        Me.grProductoDetalle.Text = "GridControl1"
        '
        'grvProductoDetalle
        '
        Me.grvProductoDetalle.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcCliente, Me.grcNombre, Me.grcImporte, Me.grcConvenioAplica})
        Me.grvProductoDetalle.GridControl = Me.grProductoDetalle
        Me.grvProductoDetalle.Name = "grvProductoDetalle"
        Me.grvProductoDetalle.OptionsView.ShowFooter = True
        Me.grvProductoDetalle.OptionsView.ShowGroupPanel = False
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.VisibleIndex = 0
        Me.grcCliente.Width = 69
        '
        'grcNombre
        '
        Me.grcNombre.Caption = "Nombre"
        Me.grcNombre.FieldName = "nombre_cliente"
        Me.grcNombre.Name = "grcNombre"
        Me.grcNombre.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombre.VisibleIndex = 1
        Me.grcNombre.Width = 397
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporte.VisibleIndex = 3
        Me.grcImporte.Width = 117
        '
        'grcConvenioAplica
        '
        Me.grcConvenioAplica.Caption = "Convenio Aplica"
        Me.grcConvenioAplica.FieldName = "convenio_aplica"
        Me.grcConvenioAplica.Name = "grcConvenioAplica"
        Me.grcConvenioAplica.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConvenioAplica.VisibleIndex = 2
        Me.grcConvenioAplica.Width = 91
        '
        'tmaProductosDetalle
        '
        Me.tmaProductosDetalle.BackColor = System.Drawing.Color.White
        Me.tmaProductosDetalle.CanDelete = True
        Me.tmaProductosDetalle.CanInsert = False
        Me.tmaProductosDetalle.CanUpdate = True
        Me.tmaProductosDetalle.Grid = Me.grProductoDetalle
        Me.tmaProductosDetalle.Location = New System.Drawing.Point(9, 136)
        Me.tmaProductosDetalle.Name = "tmaProductosDetalle"
        Me.tmaProductosDetalle.Size = New System.Drawing.Size(688, 23)
        Me.tmaProductosDetalle.TabIndex = 70
        Me.tmaProductosDetalle.TabStop = False
        Me.tmaProductosDetalle.Title = "Detalle"
        Me.tmaProductosDetalle.UpdateTitle = "un Detalle"
        '
        'frmProductos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(706, 460)
        Me.Controls.Add(Me.grProductoDetalle)
        Me.Controls.Add(Me.tmaProductosDetalle)
        Me.Controls.Add(Me.dteFechaAplicacion)
        Me.Controls.Add(Me.dteFecha_Ini)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblConvenioCambiado)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.gbxQuincenaAnio)
        Me.Controls.Add(Me.lblTipo_Plazo)
        Me.Controls.Add(Me.cboEstatus)
        Me.Name = "frmProductos"
        Me.Text = "frmProductos"
        Me.Controls.SetChildIndex(Me.cboEstatus, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Plazo, 0)
        Me.Controls.SetChildIndex(Me.gbxQuincenaAnio, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioCambiado, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Ini, 0)
        Me.Controls.SetChildIndex(Me.dteFechaAplicacion, 0)
        Me.Controls.SetChildIndex(Me.tmaProductosDetalle, 0)
        Me.Controls.SetChildIndex(Me.grProductoDetalle, 0)
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxQuincenaAnio.ResumeLayout(False)
        CType(Me.clcAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcQuincena.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaAplicacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grProductoDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvProductoDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oProductos As New VillarrealBusiness.clsProductos
    Private oProductosDetalle As New VillarrealBusiness.clsProductosDetalle
    Private oConvenios As New VillarrealBusiness.clsConvenios



    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property


    Private Sub frmProductos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept


        Select Case Action
            'Case Actions.Insert
            '    Response = Me.oPaquetes.Insertar(Me.DataSource)
        Case Actions.Update
                Response = Me.oProductos.Actualizar(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue, Me.dteFecha_Ini.DateTime, Me.cboEstatus.EditValue, Me.dteFechaAplicacion.EditValue)
            Case Actions.Delete
                Response = Me.oProductos.Eliminar(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue)
        End Select
    End Sub
    Private Sub frmProductos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize

        With tmaProductosDetalle
            .UpdateTitle = "un Producto"
            .UpdateForm = New frmProductosDetalle
            .AddColumn("cliente", "System.Int32")
            .AddColumn("nombre_cliente")
            .AddColumn("convenio_aplica", "System.Int32")
            .AddColumn("importe", "System.Double")
        End With

    End Sub
    Private Sub frmProductos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

    End Sub
    Private Sub frmProductos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oProductos.DespliegaDatos(OwnerForm.Value("convenio"), OwnerForm.Value("quincena"), OwnerForm.Value("anio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            Response = oProductosDetalle.Listado(OwnerForm.Value("convenio"), OwnerForm.Value("quincena"), OwnerForm.Value("anio"))
            If Not Response.ErrorFound Then
                oDataSet = Response.Value
                Me.tmaProductosDetalle.DataSource = oDataSet
            End If
        End If

    End Sub
    Private Sub frmProductos_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail
        With tmaProductosDetalle
            .MoveFirst()
            Do While Not tmaProductosDetalle.EOF

                Select Case tmaProductosDetalle.CurrentAction
                    'Case Actions.Insert
                    'Response = Me.oProductosDetalle.Insertar(Me.clcPaquete.Value, .SelectedRow)
                Case Actions.Update
                        Response = Me.oProductosDetalle.Actualizar(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue, .SelectedRow.Tables(0).Rows(0).Item("cliente"), .SelectedRow.Tables(0).Rows(0).Item("convenio_aplica"), .SelectedRow.Tables(0).Rows(0).Item("importe"))
                    Case Actions.Delete
                        Response = Me.oProductosDetalle.Eliminar(Me.Convenio, Me.clcQuincena.EditValue, Me.clcAnio.EditValue, .SelectedRow.Tables(0).Rows(0).Item("cliente"))
                End Select


                tmaProductosDetalle.MoveNext()
            Loop
        End With
    End Sub

    Private Sub frmProductos_Localize() Handles MyBase.Localize

    End Sub

    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub





    Private Sub frmProductos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
    End Sub
End Class
