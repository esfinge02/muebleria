Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports System.Windows.Forms
Imports Comunes.clsUtilerias

Public Class frmAcualizacionClientesCobro   
    Inherits Dipros.Windows.frmTINForm

#Region "DIPROS Systems, Declaraciones"
    Private oClientesDirecciones As VillarrealBusiness.clsClientesDirecciones
    Private oColonias As New VillarrealBusiness.clsColonias
    Private oEstados As VillarrealBusiness.clsEstados
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private ociudades As VillarrealBusiness.clsCiudades
    Private oClientes As VillarrealBusiness.clsClientes

    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property
    Private ReadOnly Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCiudad)
        End Get
    End Property
    Private ReadOnly Property Colonia() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpColonia)
        End Get
    End Property
    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblFecha_Alta As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtycalle As DevExpress.XtraEditors.TextEdit
    Public WithEvents clcCliente As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblNotas As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Alta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtentrecalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lblCp As System.Windows.Forms.Label
    Friend WithEvents clcCp As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblTelefono1 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelefono2 As System.Windows.Forms.Label
    Friend WithEvents txtFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNombres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPaterno As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPaterno As System.Windows.Forms.Label
    Friend WithEvents txtMaterno As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblMaterno As System.Windows.Forms.Label
    Friend WithEvents txtNotas As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lkpColonia As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroExterior As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroInterior As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpCiudad As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtLatitud As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLongitud As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAcualizacionClientesCobro))
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblFecha_Alta = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtycalle = New DevExpress.XtraEditors.TextEdit
        Me.clcCliente = New Dipros.Editors.TINCalcEdit
        Me.lblNotas = New System.Windows.Forms.Label
        Me.dteFecha_Alta = New DevExpress.XtraEditors.DateEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.txtentrecalle = New DevExpress.XtraEditors.TextEdit
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtDomicilio = New DevExpress.XtraEditors.TextEdit
        Me.lblColonia = New System.Windows.Forms.Label
        Me.lblCp = New System.Windows.Forms.Label
        Me.clcCp = New Dipros.Editors.TINCalcEdit
        Me.lblTelefono1 = New System.Windows.Forms.Label
        Me.txtTelefono1 = New DevExpress.XtraEditors.TextEdit
        Me.lblTelefono2 = New System.Windows.Forms.Label
        Me.txtFax = New DevExpress.XtraEditors.TextEdit
        Me.txtNombres = New DevExpress.XtraEditors.TextEdit
        Me.txtPaterno = New DevExpress.XtraEditors.TextEdit
        Me.lblPaterno = New System.Windows.Forms.Label
        Me.txtMaterno = New DevExpress.XtraEditors.TextEdit
        Me.lblMaterno = New System.Windows.Forms.Label
        Me.txtNotas = New DevExpress.XtraEditors.MemoEdit
        Me.lkpColonia = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNumeroExterior = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtNumeroInterior = New DevExpress.XtraEditors.TextEdit
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.lkpCiudad = New Dipros.Editors.TINMultiLookup
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.lblEstado = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtLatitud = New DevExpress.XtraEditors.TextEdit
        Me.txtLongitud = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtycalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Alta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtentrecalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPaterno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMaterno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroExterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroInterior.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLatitud.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLongitud.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4175, 28)
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(19, 192)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(39, 16)
        Me.Label10.TabIndex = 154
        Me.Label10.Tag = ""
        Me.Label10.Text = "Entre:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha_Alta
        '
        Me.lblFecha_Alta.AutoSize = True
        Me.lblFecha_Alta.Location = New System.Drawing.Point(643, 40)
        Me.lblFecha_Alta.Name = "lblFecha_Alta"
        Me.lblFecha_Alta.Size = New System.Drawing.Size(83, 16)
        Me.lblFecha_Alta.TabIndex = 141
        Me.lblFecha_Alta.Tag = ""
        Me.lblFecha_Alta.Text = "Fec&ha de alta:"
        Me.lblFecha_Alta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(432, 200)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(11, 16)
        Me.Label11.TabIndex = 156
        Me.Label11.Tag = ""
        Me.Label11.Text = "y"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtycalle
        '
        Me.txtycalle.EditValue = ""
        Me.txtycalle.Location = New System.Drawing.Point(461, 200)
        Me.txtycalle.Name = "txtycalle"
        '
        'txtycalle.Properties
        '
        Me.txtycalle.Properties.MaxLength = 30
        Me.txtycalle.Size = New System.Drawing.Size(403, 20)
        Me.txtycalle.TabIndex = 13
        Me.txtycalle.Tag = "ycalle"
        '
        'clcCliente
        '
        Me.clcCliente.EditValue = "0"
        Me.clcCliente.Location = New System.Drawing.Point(96, 40)
        Me.clcCliente.MaxValue = 0
        Me.clcCliente.MinValue = 0
        Me.clcCliente.Name = "clcCliente"
        '
        'clcCliente.Properties
        '
        Me.clcCliente.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCliente.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCliente.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCliente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCliente.Size = New System.Drawing.Size(114, 19)
        Me.clcCliente.TabIndex = 0
        Me.clcCliente.Tag = "cliente"
        '
        'lblNotas
        '
        Me.lblNotas.AutoSize = True
        Me.lblNotas.Location = New System.Drawing.Point(19, 264)
        Me.lblNotas.Name = "lblNotas"
        Me.lblNotas.Size = New System.Drawing.Size(93, 16)
        Me.lblNotas.TabIndex = 143
        Me.lblNotas.Tag = ""
        Me.lblNotas.Text = "Observaciones :"
        Me.lblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Alta
        '
        Me.dteFecha_Alta.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha_Alta.Location = New System.Drawing.Point(758, 40)
        Me.dteFecha_Alta.Name = "dteFecha_Alta"
        '
        'dteFecha_Alta.Properties
        '
        Me.dteFecha_Alta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Alta.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Alta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Alta.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Alta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Alta.Size = New System.Drawing.Size(116, 23)
        Me.dteFecha_Alta.TabIndex = 142
        Me.dteFecha_Alta.TabStop = False
        Me.dteFecha_Alta.Tag = "fecha_alta"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(19, 40)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 139
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtentrecalle
        '
        Me.txtentrecalle.EditValue = ""
        Me.txtentrecalle.Location = New System.Drawing.Point(96, 200)
        Me.txtentrecalle.Name = "txtentrecalle"
        '
        'txtentrecalle.Properties
        '
        Me.txtentrecalle.Properties.MaxLength = 30
        Me.txtentrecalle.Size = New System.Drawing.Size(317, 20)
        Me.txtentrecalle.TabIndex = 12
        Me.txtentrecalle.Tag = "entrecalle"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(19, 72)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(72, 16)
        Me.lblNombre.TabIndex = 145
        Me.lblNombre.Tag = ""
        Me.lblNombre.Text = "&Nombre (s):"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicilio
        '
        Me.txtDomicilio.EditValue = ""
        Me.txtDomicilio.Location = New System.Drawing.Point(96, 168)
        Me.txtDomicilio.Name = "txtDomicilio"
        '
        'txtDomicilio.Properties
        '
        Me.txtDomicilio.Properties.MaxLength = 50
        Me.txtDomicilio.Size = New System.Drawing.Size(374, 20)
        Me.txtDomicilio.TabIndex = 11
        Me.txtDomicilio.Tag = "domicilio"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(24, 136)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 151
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "C&olonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCp
        '
        Me.lblCp.AutoSize = True
        Me.lblCp.Location = New System.Drawing.Point(288, 136)
        Me.lblCp.Name = "lblCp"
        Me.lblCp.Size = New System.Drawing.Size(39, 16)
        Me.lblCp.TabIndex = 158
        Me.lblCp.Tag = ""
        Me.lblCp.Text = "C. P. &:"
        Me.lblCp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCp
        '
        Me.clcCp.EditValue = "0"
        Me.clcCp.Location = New System.Drawing.Point(376, 136)
        Me.clcCp.MaxValue = 0
        Me.clcCp.MinValue = 0
        Me.clcCp.Name = "clcCp"
        '
        'clcCp.Properties
        '
        Me.clcCp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCp.Properties.Enabled = False
        Me.clcCp.Properties.MaskData.EditMask = "#"
        Me.clcCp.Properties.MaxLength = 5
        Me.clcCp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCp.Size = New System.Drawing.Size(96, 19)
        Me.clcCp.TabIndex = 8
        Me.clcCp.Tag = "cp"
        '
        'lblTelefono1
        '
        Me.lblTelefono1.AutoSize = True
        Me.lblTelefono1.Location = New System.Drawing.Point(19, 232)
        Me.lblTelefono1.Name = "lblTelefono1"
        Me.lblTelefono1.Size = New System.Drawing.Size(140, 16)
        Me.lblTelefono1.TabIndex = 160
        Me.lblTelefono1.Tag = ""
        Me.lblTelefono1.Text = "Telefonos :    Casa Fijo :"
        Me.lblTelefono1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTelefono1
        '
        Me.txtTelefono1.EditValue = ""
        Me.txtTelefono1.Location = New System.Drawing.Point(168, 232)
        Me.txtTelefono1.Name = "txtTelefono1"
        '
        'txtTelefono1.Properties
        '
        Me.txtTelefono1.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtTelefono1.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtTelefono1.Properties.MaxLength = 13
        Me.txtTelefono1.Size = New System.Drawing.Size(173, 20)
        Me.txtTelefono1.TabIndex = 14
        Me.txtTelefono1.Tag = "telefono1"
        '
        'lblTelefono2
        '
        Me.lblTelefono2.AutoSize = True
        Me.lblTelefono2.Location = New System.Drawing.Point(384, 232)
        Me.lblTelefono2.Name = "lblTelefono2"
        Me.lblTelefono2.Size = New System.Drawing.Size(101, 16)
        Me.lblTelefono2.TabIndex = 162
        Me.lblTelefono2.Tag = ""
        Me.lblTelefono2.Text = "Celular o Nextel :"
        Me.lblTelefono2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFax
        '
        Me.txtFax.EditValue = ""
        Me.txtFax.Location = New System.Drawing.Point(496, 232)
        Me.txtFax.Name = "txtFax"
        '
        'txtFax.Properties
        '
        Me.txtFax.Properties.MaskData.EditMask = "(999)000-0000"
        Me.txtFax.Properties.MaskData.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtFax.Properties.MaxLength = 13
        Me.txtFax.Size = New System.Drawing.Size(125, 20)
        Me.txtFax.TabIndex = 16
        Me.txtFax.Tag = "fax"
        '
        'txtNombres
        '
        Me.txtNombres.EditValue = ""
        Me.txtNombres.Location = New System.Drawing.Point(96, 72)
        Me.txtNombres.Name = "txtNombres"
        '
        'txtNombres.Properties
        '
        Me.txtNombres.Properties.Enabled = False
        Me.txtNombres.Properties.MaxLength = 40
        Me.txtNombres.Size = New System.Drawing.Size(173, 20)
        Me.txtNombres.TabIndex = 1
        Me.txtNombres.Tag = "nombres"
        '
        'txtPaterno
        '
        Me.txtPaterno.EditValue = ""
        Me.txtPaterno.Location = New System.Drawing.Point(374, 72)
        Me.txtPaterno.Name = "txtPaterno"
        '
        'txtPaterno.Properties
        '
        Me.txtPaterno.Properties.Enabled = False
        Me.txtPaterno.Properties.MaxLength = 30
        Me.txtPaterno.Size = New System.Drawing.Size(173, 20)
        Me.txtPaterno.TabIndex = 2
        Me.txtPaterno.Tag = "paterno"
        '
        'lblPaterno
        '
        Me.lblPaterno.AutoSize = True
        Me.lblPaterno.Location = New System.Drawing.Point(288, 72)
        Me.lblPaterno.Name = "lblPaterno"
        Me.lblPaterno.Size = New System.Drawing.Size(67, 16)
        Me.lblPaterno.TabIndex = 147
        Me.lblPaterno.Tag = ""
        Me.lblPaterno.Text = "A. Pater&no:"
        Me.lblPaterno.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMaterno
        '
        Me.txtMaterno.EditValue = ""
        Me.txtMaterno.Location = New System.Drawing.Point(653, 72)
        Me.txtMaterno.Name = "txtMaterno"
        '
        'txtMaterno.Properties
        '
        Me.txtMaterno.Properties.Enabled = False
        Me.txtMaterno.Properties.MaxLength = 30
        Me.txtMaterno.Size = New System.Drawing.Size(173, 20)
        Me.txtMaterno.TabIndex = 3
        Me.txtMaterno.Tag = "materno"
        '
        'lblMaterno
        '
        Me.lblMaterno.AutoSize = True
        Me.lblMaterno.Location = New System.Drawing.Point(566, 72)
        Me.lblMaterno.Name = "lblMaterno"
        Me.lblMaterno.Size = New System.Drawing.Size(69, 16)
        Me.lblMaterno.TabIndex = 149
        Me.lblMaterno.Tag = ""
        Me.lblMaterno.Text = "A. Matern&o:"
        Me.lblMaterno.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNotas
        '
        Me.txtNotas.EditValue = ""
        Me.txtNotas.Location = New System.Drawing.Point(125, 264)
        Me.txtNotas.Name = "txtNotas"
        '
        'txtNotas.Properties
        '
        Me.txtNotas.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtNotas.Size = New System.Drawing.Size(547, 43)
        Me.txtNotas.TabIndex = 17
        Me.txtNotas.Tag = "notas"
        '
        'lkpColonia
        '
        Me.lkpColonia.AllowAdd = False
        Me.lkpColonia.AutoReaload = True
        Me.lkpColonia.DataSource = Nothing
        Me.lkpColonia.DefaultSearchField = ""
        Me.lkpColonia.DisplayMember = "descripcion"
        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia.Filtered = False
        Me.lkpColonia.InitValue = Nothing
        Me.lkpColonia.Location = New System.Drawing.Point(96, 136)
        Me.lkpColonia.MultiSelect = False
        Me.lkpColonia.Name = "lkpColonia"
        Me.lkpColonia.NullText = ""
        Me.lkpColonia.PopupWidth = CType(360, Long)
        Me.lkpColonia.ReadOnlyControl = False
        Me.lkpColonia.Required = False
        Me.lkpColonia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpColonia.SearchMember = ""
        Me.lkpColonia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpColonia.SelectAll = False
        Me.lkpColonia.Size = New System.Drawing.Size(173, 20)
        Me.lkpColonia.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpColonia.TabIndex = 7
        Me.lkpColonia.Tag = "colonia"
        Me.lkpColonia.ToolTip = Nothing
        Me.lkpColonia.ValueMember = "colonia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 168)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 16)
        Me.Label2.TabIndex = 166
        Me.Label2.Tag = ""
        Me.Label2.Text = "Calle   :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(496, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 16)
        Me.Label3.TabIndex = 167
        Me.Label3.Tag = ""
        Me.Label3.Text = "No. Ex&t.:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumeroExterior
        '
        Me.txtNumeroExterior.EditValue = ""
        Me.txtNumeroExterior.Location = New System.Drawing.Point(560, 136)
        Me.txtNumeroExterior.Name = "txtNumeroExterior"
        '
        'txtNumeroExterior.Properties
        '
        Me.txtNumeroExterior.Properties.MaxLength = 10
        Me.txtNumeroExterior.Size = New System.Drawing.Size(96, 20)
        Me.txtNumeroExterior.TabIndex = 9
        Me.txtNumeroExterior.Tag = "numero_exterior"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(696, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 16)
        Me.Label4.TabIndex = 169
        Me.Label4.Tag = ""
        Me.Label4.Text = "No. Inte. :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumeroInterior
        '
        Me.txtNumeroInterior.EditValue = ""
        Me.txtNumeroInterior.Location = New System.Drawing.Point(768, 136)
        Me.txtNumeroInterior.Name = "txtNumeroInterior"
        '
        'txtNumeroInterior.Properties
        '
        Me.txtNumeroInterior.Properties.MaxLength = 10
        Me.txtNumeroInterior.Size = New System.Drawing.Size(93, 20)
        Me.txtNumeroInterior.TabIndex = 10
        Me.txtNumeroInterior.Tag = "numero_interior"
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(96, 104)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.ReadOnlyControl = False
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(176, 20)
        Me.lkpEstado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEstado.TabIndex = 4
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'lkpCiudad
        '
        Me.lkpCiudad.AllowAdd = False
        Me.lkpCiudad.AutoReaload = True
        Me.lkpCiudad.DataSource = Nothing
        Me.lkpCiudad.DefaultSearchField = ""
        Me.lkpCiudad.DisplayMember = "descripcion"
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad.Filtered = False
        Me.lkpCiudad.InitValue = Nothing
        Me.lkpCiudad.Location = New System.Drawing.Point(648, 104)
        Me.lkpCiudad.MultiSelect = False
        Me.lkpCiudad.Name = "lkpCiudad"
        Me.lkpCiudad.NullText = ""
        Me.lkpCiudad.PopupWidth = CType(300, Long)
        Me.lkpCiudad.ReadOnlyControl = False
        Me.lkpCiudad.Required = False
        Me.lkpCiudad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCiudad.SearchMember = ""
        Me.lkpCiudad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCiudad.SelectAll = False
        Me.lkpCiudad.Size = New System.Drawing.Size(168, 20)
        Me.lkpCiudad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCiudad.TabIndex = 6
        Me.lkpCiudad.Tag = "ciudad"
        Me.lkpCiudad.ToolTip = Nothing
        Me.lkpCiudad.ValueMember = "ciudad"
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = True
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(368, 104)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(300, Long)
        Me.lkpMunicipio.ReadOnlyControl = False
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(168, 20)
        Me.lkpMunicipio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMunicipio.TabIndex = 5
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(560, 104)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(60, 16)
        Me.lblCiudad.TabIndex = 174
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "Locali&dad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(24, 104)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 170
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "E&stado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(288, 104)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 16)
        Me.Label7.TabIndex = 171
        Me.Label7.Tag = ""
        Me.Label7.Text = "Municipio:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtLatitud
        '
        Me.txtLatitud.EditValue = ""
        Me.txtLatitud.Location = New System.Drawing.Point(712, 232)
        Me.txtLatitud.Name = "txtLatitud"
        '
        'txtLatitud.Properties
        '
        Me.txtLatitud.Properties.MaxLength = 40
        Me.txtLatitud.Size = New System.Drawing.Size(173, 20)
        Me.txtLatitud.TabIndex = 175
        Me.txtLatitud.Tag = "Latitud"
        Me.txtLatitud.Visible = False
        '
        'txtLongitud
        '
        Me.txtLongitud.EditValue = ""
        Me.txtLongitud.Location = New System.Drawing.Point(712, 272)
        Me.txtLongitud.Name = "txtLongitud"
        '
        'txtLongitud.Properties
        '
        Me.txtLongitud.Properties.MaxLength = 40
        Me.txtLongitud.Size = New System.Drawing.Size(173, 20)
        Me.txtLongitud.TabIndex = 176
        Me.txtLongitud.Tag = "Longitud"
        Me.txtLongitud.Visible = False
        '
        'frmCenso
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(921, 316)
        Me.Controls.Add(Me.txtLongitud)
        Me.Controls.Add(Me.txtLatitud)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lkpCiudad)
        Me.Controls.Add(Me.lkpMunicipio)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtNumeroExterior)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtNumeroInterior)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblFecha_Alta)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtycalle)
        Me.Controls.Add(Me.clcCliente)
        Me.Controls.Add(Me.lblNotas)
        Me.Controls.Add(Me.dteFecha_Alta)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.txtentrecalle)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtDomicilio)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.lblCp)
        Me.Controls.Add(Me.clcCp)
        Me.Controls.Add(Me.lblTelefono1)
        Me.Controls.Add(Me.txtTelefono1)
        Me.Controls.Add(Me.lblTelefono2)
        Me.Controls.Add(Me.txtFax)
        Me.Controls.Add(Me.txtNombres)
        Me.Controls.Add(Me.txtPaterno)
        Me.Controls.Add(Me.lblPaterno)
        Me.Controls.Add(Me.txtMaterno)
        Me.Controls.Add(Me.lblMaterno)
        Me.Controls.Add(Me.txtNotas)
        Me.Controls.Add(Me.lkpColonia)
        Me.Controls.Add(Me.Label7)
        Me.Name = "frmCenso"
        Me.Text = "Censo Clientes"
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.lkpColonia, 0)
        Me.Controls.SetChildIndex(Me.txtNotas, 0)
        Me.Controls.SetChildIndex(Me.lblMaterno, 0)
        Me.Controls.SetChildIndex(Me.txtMaterno, 0)
        Me.Controls.SetChildIndex(Me.lblPaterno, 0)
        Me.Controls.SetChildIndex(Me.txtPaterno, 0)
        Me.Controls.SetChildIndex(Me.txtNombres, 0)
        Me.Controls.SetChildIndex(Me.txtFax, 0)
        Me.Controls.SetChildIndex(Me.lblTelefono2, 0)
        Me.Controls.SetChildIndex(Me.txtTelefono1, 0)
        Me.Controls.SetChildIndex(Me.lblTelefono1, 0)
        Me.Controls.SetChildIndex(Me.clcCp, 0)
        Me.Controls.SetChildIndex(Me.lblCp, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.txtDomicilio, 0)
        Me.Controls.SetChildIndex(Me.lblNombre, 0)
        Me.Controls.SetChildIndex(Me.txtentrecalle, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Alta, 0)
        Me.Controls.SetChildIndex(Me.lblNotas, 0)
        Me.Controls.SetChildIndex(Me.clcCliente, 0)
        Me.Controls.SetChildIndex(Me.txtycalle, 0)
        Me.Controls.SetChildIndex(Me.Label11, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Alta, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtNumeroInterior, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.txtNumeroExterior, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.lblCiudad, 0)
        Me.Controls.SetChildIndex(Me.lkpMunicipio, 0)
        Me.Controls.SetChildIndex(Me.lkpCiudad, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        Me.Controls.SetChildIndex(Me.txtLatitud, 0)
        Me.Controls.SetChildIndex(Me.txtLongitud, 0)
        CType(Me.txtycalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Alta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtentrecalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPaterno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMaterno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNotas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroExterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroInterior.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLatitud.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLongitud.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmCenso_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oClientesDirecciones = New VillarrealBusiness.clsClientesDirecciones
        oColonias = New VillarrealBusiness.clsColonias
        oEstados = New VillarrealBusiness.clsEstados
        oMunicipios = New VillarrealBusiness.clsMunicipios
        ociudades = New VillarrealBusiness.clsCiudades
        oClientes = New VillarrealBusiness.clsClientes
    End Sub


    Private Sub frmCenso_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oClientesDirecciones.Validacion(Me.clcCliente.Value, 1, Me.txtDomicilio.Text, Me.txtNumeroExterior.Text, Me.txtNumeroInterior.Text, Me.txtentrecalle.Text, _
                            Me.txtycalle.Text, Me.clcCp.EditValue, Colonia, Estado, Municipio, Ciudad, Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtTelefono1.Text), _
                            Comunes.clsUtilerias.EliminaFormatoTelefonico(Me.txtFax.Text), Me.txtNotas.Text)


    End Sub

    Private Sub frmCenso_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Response = oClientesDirecciones.Insertar(Me.clcCliente.EditValue, 1, Me.txtDomicilio.Text, Me.txtNumeroExterior.Text, Me.txtNumeroInterior.Text, Me.txtentrecalle.Text, _
                            Me.txtycalle.Text, Me.clcCp.EditValue, Colonia, Estado, Municipio, Ciudad, EliminaFormatoTelefonico(Me.txtTelefono1.Text), EliminaFormatoTelefonico(Me.txtFax.Text), Me.txtNotas.Text, "", "")
    End Sub

    Private Sub frmCenso_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmCenso_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub

    Private Sub frmCenso_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de los Controles"
    Private Sub clcCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcCliente.Validated
        VerificaCliente()
    End Sub

    Private Sub lkpEstado_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub
    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        Comunes.clsFormato.for_estados_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData
        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

    Private Sub lkpMunicipio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        '        If Entro_Despliega = True Then Exit Sub
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad_LoadData(True)
    End Sub
    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData
        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCiudad_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCiudad.EditValueChanged
        If Me.lkpCiudad.EditValue Is Nothing Then Exit Sub

        Me.lkpColonia.EditValue = Nothing
        Me.lkpColonia_LoadData(True)
    End Sub
    Private Sub lkpCiudad_Format() Handles lkpCiudad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpCiudad)
    End Sub
    Private Sub lkpCiudad_LoadData(ByVal Initialize As Boolean) Handles lkpCiudad.LoadData
        Dim Response As New Events
        Response = ociudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCiudad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpColonia_LoadData(ByVal Initialize As Boolean) Handles lkpColonia.LoadData
        Dim Response As New Events
        Response = oColonias.Lookup(Estado, Municipio, Ciudad)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpColonia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpColonia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpColonia.EditValueChanged
        clcCp.EditValue = lkpColonia.GetValue("codigo_postal")
    End Sub
    Private Sub lkpColonia_Format() Handles lkpColonia.Format
        Comunes.clsFormato.for_colonias_grl(Me.lkpColonia)
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub VerificaCliente()

        Dim response As Events
        Dim responseDos As Events
        Dim oDataSetDos As DataSet
        Dim oDataSet As DataSet
        response = oClientes.Existe(Me.clcCliente.Value)
        If response.ErrorFound Then
            response.ShowMessage()
            Exit Sub
        Else
            If CType(response.Value, Int32) > 0 Then
                responseDos = oClientes.DespliegaDatos(Me.clcCliente.Value)
                oDataSetDos = responseDos.Value
                Me.txtNombres.Text = oDataSetDos.Tables(0).Rows(0).Item("nombres")
                Me.txtPaterno.Text = oDataSetDos.Tables(0).Rows(0).Item("paterno")
                Me.txtMaterno.Text = oDataSetDos.Tables(0).Rows(0).Item("materno")
                Me.lkpEstado.EditValue = oDataSetDos.Tables(0).Rows(0).Item("estado")
                Me.lkpMunicipio.EditValue = oDataSetDos.Tables(0).Rows(0).Item("municipio")
                Me.lkpCiudad.EditValue = oDataSetDos.Tables(0).Rows(0).Item("ciudad")
                Me.lkpColonia.EditValue = oDataSetDos.Tables(0).Rows(0).Item("colonia")

                Me.txtNumeroExterior.Text = oDataSetDos.Tables(0).Rows(0).Item("numero_exterior")
                Me.txtNumeroInterior.Text = oDataSetDos.Tables(0).Rows(0).Item("numero_interior")
                Me.txtentrecalle.Text = oDataSetDos.Tables(0).Rows(0).Item("entrecalle")
                Me.txtycalle.Text = oDataSetDos.Tables(0).Rows(0).Item("ycalle")
                Me.txtTelefono1.Text = oDataSetDos.Tables(0).Rows(0).Item("telefono1")
                Me.txtFax.Text = oDataSetDos.Tables(0).Rows(0).Item("fax")

            End If

            End If
    End Sub
#End Region


End Class
