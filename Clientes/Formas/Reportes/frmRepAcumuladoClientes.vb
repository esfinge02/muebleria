Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmRepAcumuladoClientes
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents gpbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents clcClientes As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepAcumuladoClientes))
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.gpbFechas = New System.Windows.Forms.GroupBox
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.clcClientes = New Dipros.Editors.TINCalcEdit
        Me.gpbFechas.SuspendLayout()
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcClientes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(73, 48)
        Me.lkpCliente.MultiSelect = True
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(272, 22)
        Me.lkpCliente.TabIndex = 60
        Me.lkpCliente.ToolTip = "Seleccione un cliente"
        Me.lkpCliente.ValueMember = "cliente"
        Me.lkpCliente.Visible = False
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(17, 50)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'gpbFechas
        '
        Me.gpbFechas.Controls.Add(Me.dteFecha_Ini)
        Me.gpbFechas.Controls.Add(Me.Label4)
        Me.gpbFechas.Controls.Add(Me.dteFecha_Fin)
        Me.gpbFechas.Controls.Add(Me.Label5)
        Me.gpbFechas.Location = New System.Drawing.Point(17, 80)
        Me.gpbFechas.Name = "gpbFechas"
        Me.gpbFechas.Size = New System.Drawing.Size(328, 56)
        Me.gpbFechas.TabIndex = 2
        Me.gpbFechas.TabStop = False
        Me.gpbFechas.Text = "Fechas:  "
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 8, 30, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(70, 22)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(90, 22)
        Me.dteFecha_Ini.TabIndex = 1
        Me.dteFecha_Ini.ToolTip = "Fecha Inicial"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Desde: "
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 8, 30, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(216, 22)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(96, 22)
        Me.dteFecha_Fin.TabIndex = 3
        Me.dteFecha_Fin.ToolTip = "Fecha Final"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(176, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Ha&sta: "
        '
        'clcClientes
        '
        Me.clcClientes.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcClientes.Location = New System.Drawing.Point(73, 48)
        Me.clcClientes.MaxValue = 0
        Me.clcClientes.MinValue = 0
        Me.clcClientes.Name = "clcClientes"
        '
        'clcClientes.Properties
        '
        Me.clcClientes.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcClientes.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcClientes.Properties.MaskData.EditMask = "########0"
        Me.clcClientes.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcClientes.Size = New System.Drawing.Size(70, 21)
        Me.clcClientes.TabIndex = 1
        Me.clcClientes.Tag = ""
        '
        'frmRepAcumuladoClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 152)
        Me.Controls.Add(Me.clcClientes)
        Me.Controls.Add(Me.gpbFechas)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblCliente)
        Me.Name = "frmRepAcumuladoClientes"
        Me.Text = "frmRepAcumuladoClientes"
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.gpbFechas, 0)
        Me.Controls.SetChildIndex(Me.clcClientes, 0)
        Me.gpbFechas.ResumeLayout(False)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcClientes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepAcumuladoClientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)
    End Sub

    Private Sub frmRepAcumuladoClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        'ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
        'Response = oReportes.AcumuladoClientes(Me.lkpCliente.ToXML)
        Response = oReportes.AcumuladoClientes(Me.clcClientes.EditValue, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)
        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Acumulado de Clientes no se puede Mostrar")
        Else
            If Response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptAcumuladoClientes
                oDataSet = Response.Value
                oReport.DataSource = oDataSet.Tables(0)
                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Acumulado de Clientes", oReport)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub

    Private Sub frmRepEstadoDeCuenta_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Me.dteFecha_Ini.Text, Me.dteFecha_Fin.Text)
        'Response = Comunes.clsUtilerias.ValidaMultiSeleccion(Me.lkpCliente.ToXML, "Clientes")
        If Me.clcClientes.EditValue < 0 Then
            Response.Message = "La Cantidad de Clientes debe ser un Numero Positivo"
        End If

    End Sub

    Private Sub frmRepAcumuladoClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim oClientes As New VillarrealBusiness.clsClientes
        Dim Response As New Events
        Response = oClientes.LookupCliente
        If Not Response.ErrorFound Then
            Me.lkpCliente.DataSource = CType(Response.Value, DataSet).Tables(0)
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub

    Private Sub clcClientes_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcClientes.EditValueChanged
        If Me.clcClientes.IsLoading Then Exit Sub

        If Not IsNumeric(Me.clcClientes.EditValue) Then
            Me.clcClientes.EditValue = 0
        Else
            Exit Sub
        End If

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region




End Class
