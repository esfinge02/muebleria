Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmRepAnualClientes
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblTipoVenta As System.Windows.Forms.Label
    Friend WithEvents cboTipoVenta As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblClienteFin As System.Windows.Forms.Label
    Friend WithEvents lkpClienteFin As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente1 As System.Windows.Forms.Label
    Friend WithEvents lkpClienteInicio As Dipros.Editors.TINMultiLookup
    Friend WithEvents seFecha_Ini As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents lblanio As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepAnualClientes))
        Me.lblanio = New System.Windows.Forms.Label
        Me.lblTipoVenta = New System.Windows.Forms.Label
        Me.cboTipoVenta = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblClienteFin = New System.Windows.Forms.Label
        Me.lkpClienteFin = New Dipros.Editors.TINMultiLookup
        Me.lblCliente1 = New System.Windows.Forms.Label
        Me.lkpClienteInicio = New Dipros.Editors.TINMultiLookup
        Me.seFecha_Ini = New DevExpress.XtraEditors.SpinEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(638, 28)
        Me.tbrTools.TabIndex = 9
        '
        'lblanio
        '
        Me.lblanio.AutoSize = True
        Me.lblanio.Location = New System.Drawing.Point(60, 43)
        Me.lblanio.Name = "lblanio"
        Me.lblanio.Size = New System.Drawing.Size(30, 16)
        Me.lblanio.TabIndex = 0
        Me.lblanio.Text = "&A�o:"
        '
        'lblTipoVenta
        '
        Me.lblTipoVenta.AutoSize = True
        Me.lblTipoVenta.Location = New System.Drawing.Point(22, 67)
        Me.lblTipoVenta.Name = "lblTipoVenta"
        Me.lblTipoVenta.Size = New System.Drawing.Size(68, 16)
        Me.lblTipoVenta.TabIndex = 2
        Me.lblTipoVenta.Tag = ""
        Me.lblTipoVenta.Text = "&Tipo Venta:"
        Me.lblTipoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoVenta
        '
        Me.cboTipoVenta.EditValue = ""
        Me.cboTipoVenta.Location = New System.Drawing.Point(98, 64)
        Me.cboTipoVenta.Name = "cboTipoVenta"
        '
        'cboTipoVenta.Properties
        '
        Me.cboTipoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoVenta.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enajenaci�n", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Normales", 3, -1)})
        Me.cboTipoVenta.Size = New System.Drawing.Size(136, 20)
        Me.cboTipoVenta.TabIndex = 3
        Me.cboTipoVenta.Tag = "tipo"
        Me.cboTipoVenta.ToolTip = "Tipo de Venta"
        '
        'lblClienteFin
        '
        Me.lblClienteFin.AutoSize = True
        Me.lblClienteFin.BackColor = System.Drawing.SystemColors.Window
        Me.lblClienteFin.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClienteFin.ForeColor = System.Drawing.Color.Black
        Me.lblClienteFin.Location = New System.Drawing.Point(219, 26)
        Me.lblClienteFin.Name = "lblClienteFin"
        Me.lblClienteFin.Size = New System.Drawing.Size(41, 16)
        Me.lblClienteFin.TabIndex = 2
        Me.lblClienteFin.Tag = ""
        Me.lblClienteFin.Text = "&Hasta:"
        Me.lblClienteFin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpClienteFin
        '
        Me.lkpClienteFin.AllowAdd = False
        Me.lkpClienteFin.AutoReaload = False
        Me.lkpClienteFin.BackColor = System.Drawing.SystemColors.Window
        Me.lkpClienteFin.DataSource = Nothing
        Me.lkpClienteFin.DefaultSearchField = ""
        Me.lkpClienteFin.DisplayMember = "cliente"
        Me.lkpClienteFin.EditValue = Nothing
        Me.lkpClienteFin.Filtered = False
        Me.lkpClienteFin.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpClienteFin.ForeColor = System.Drawing.Color.Black
        Me.lkpClienteFin.InitValue = Nothing
        Me.lkpClienteFin.Location = New System.Drawing.Point(267, 24)
        Me.lkpClienteFin.MultiSelect = False
        Me.lkpClienteFin.Name = "lkpClienteFin"
        Me.lkpClienteFin.NullText = ""
        Me.lkpClienteFin.PopupWidth = CType(300, Long)
        Me.lkpClienteFin.Required = False
        Me.lkpClienteFin.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteFin.SearchMember = ""
        Me.lkpClienteFin.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteFin.SelectAll = False
        Me.lkpClienteFin.Size = New System.Drawing.Size(136, 20)
        Me.lkpClienteFin.TabIndex = 3
        Me.lkpClienteFin.Tag = ""
        Me.lkpClienteFin.ToolTip = "Cliente Hasta"
        Me.lkpClienteFin.ValueMember = "cliente"
        '
        'lblCliente1
        '
        Me.lblCliente1.AutoSize = True
        Me.lblCliente1.BackColor = System.Drawing.SystemColors.Window
        Me.lblCliente1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente1.ForeColor = System.Drawing.Color.Black
        Me.lblCliente1.Location = New System.Drawing.Point(19, 26)
        Me.lblCliente1.Name = "lblCliente1"
        Me.lblCliente1.Size = New System.Drawing.Size(43, 16)
        Me.lblCliente1.TabIndex = 0
        Me.lblCliente1.Tag = ""
        Me.lblCliente1.Text = "&Desde:"
        Me.lblCliente1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpClienteInicio
        '
        Me.lkpClienteInicio.AllowAdd = False
        Me.lkpClienteInicio.AutoReaload = False
        Me.lkpClienteInicio.BackColor = System.Drawing.SystemColors.Window
        Me.lkpClienteInicio.DataSource = Nothing
        Me.lkpClienteInicio.DefaultSearchField = ""
        Me.lkpClienteInicio.DisplayMember = "cliente"
        Me.lkpClienteInicio.EditValue = Nothing
        Me.lkpClienteInicio.Filtered = False
        Me.lkpClienteInicio.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpClienteInicio.ForeColor = System.Drawing.Color.Black
        Me.lkpClienteInicio.InitValue = Nothing
        Me.lkpClienteInicio.Location = New System.Drawing.Point(67, 24)
        Me.lkpClienteInicio.MultiSelect = False
        Me.lkpClienteInicio.Name = "lkpClienteInicio"
        Me.lkpClienteInicio.NullText = ""
        Me.lkpClienteInicio.PopupWidth = CType(300, Long)
        Me.lkpClienteInicio.Required = False
        Me.lkpClienteInicio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteInicio.SearchMember = ""
        Me.lkpClienteInicio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteInicio.SelectAll = False
        Me.lkpClienteInicio.Size = New System.Drawing.Size(136, 20)
        Me.lkpClienteInicio.TabIndex = 1
        Me.lkpClienteInicio.Tag = ""
        Me.lkpClienteInicio.ToolTip = "Cliente Desde"
        Me.lkpClienteInicio.ValueMember = "cliente"
        '
        'seFecha_Ini
        '
        Me.seFecha_Ini.EditValue = New Decimal(New Integer() {1990, 0, 0, 0})
        Me.seFecha_Ini.Location = New System.Drawing.Point(98, 40)
        Me.seFecha_Ini.Name = "seFecha_Ini"
        '
        'seFecha_Ini.Properties
        '
        Me.seFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seFecha_Ini.Properties.MaxValue = New Decimal(New Integer() {2020, 0, 0, 0})
        Me.seFecha_Ini.Properties.MinValue = New Decimal(New Integer() {1990, 0, 0, 0})
        Me.seFecha_Ini.Properties.UseCtrlIncrement = False
        Me.seFecha_Ini.Size = New System.Drawing.Size(75, 20)
        Me.seFecha_Ini.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblClienteFin)
        Me.GroupBox1.Controls.Add(Me.lkpClienteFin)
        Me.GroupBox1.Controls.Add(Me.lkpClienteInicio)
        Me.GroupBox1.Controls.Add(Me.lblCliente1)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 89)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(423, 56)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clientes:"
        '
        'frmRepAnualClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(458, 152)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.seFecha_Ini)
        Me.Controls.Add(Me.lblanio)
        Me.Controls.Add(Me.lblTipoVenta)
        Me.Controls.Add(Me.cboTipoVenta)
        Me.Name = "frmRepAnualClientes"
        Me.Text = "frmRepAnualClientes"
        Me.Controls.SetChildIndex(Me.cboTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lblanio, 0)
        Me.Controls.SetChildIndex(Me.seFecha_Ini, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oClienteInicio As VillarrealBusiness.clsClientes
    Private oClienteFin As VillarrealBusiness.clsClientes

    Private ReadOnly Property ClienteInicio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteInicio)

        End Get
    End Property
    Private ReadOnly Property ClienteFin() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteFin)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepAnualClientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub frmRepAnualClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Dim dfecha_ini As DateTime
        Dim dfecha_fin As DateTime

        dfecha_ini = CDate("01-01-" + Me.seFecha_Ini.Text)
        dfecha_fin = CDate("31-12-" + Me.seFecha_Ini.Text)

        Response = oReportes.AnualClientes(Me.ClienteInicio, Me.ClienteFin, dfecha_ini, dfecha_fin, Me.cboTipoVenta.EditValue, Comunes.Common.Sucursal_Actual)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte Anual de Clientes no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oReport As New rptAnualClientes
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Anual de clientes", oReport)
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If




    End Sub

    Private Sub frmRepAnualClientes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.SelectNextControl(Me.lkpClienteFin, True, False, False, True)
        Response = oReportes.Validaciones(Me.ClienteInicio, Me.ClienteFin, Me.seFecha_Ini.Text)
    End Sub

    Private Sub frmRepAnualClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oClienteInicio = New VillarrealBusiness.clsClientes
        oClienteFin = New VillarrealBusiness.clsClientes
        Me.cboTipoVenta.SelectedIndex = 0 'TODOS
        Me.seFecha_Ini.Value = CDate(TINApp.FechaServidor).Year
        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"


    Private Sub lkpClienteInicio_LoadData(ByVal Initialize As Boolean) Handles lkpClienteInicio.LoadData
        Dim Response As New Events
        Response = oClienteInicio.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteInicio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpClienteInicio_Format() Handles lkpClienteInicio.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteInicio)
    End Sub

    Private Sub lkpClienteFin_LoaData(ByVal Initialize As Boolean) Handles lkpClienteFin.LoadData
        Dim Response As New Events
        Response = oClienteFin.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteFin.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpClienteFin_Format() Handles lkpClienteFin.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteFin)
    End Sub

    'Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
    '    Dim oClientes As New VillarrealBusiness.clsClientes
    '    Dim Response As New Events
    '    Response = oClientes.Lookup
    '    If Not Response.ErrorFound Then
    '        Me.lkpCliente.DataSource = CType(Response.Value, DataSet).Tables(0)
    '    End If
    '    Response = Nothing
    'End Sub

    'Private Sub lkpCliente_Format() Handles lkpCliente.Format
    '    Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    'End Sub

    'Private Sub clcClientes_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcClientes.EditValueChanged
    '    If Me.clcClientes.IsLoading Then Exit Sub

    '    If Not IsNumeric(Me.clcClientes.EditValue) Then
    '        Me.clcClientes.EditValue = 0
    '    Else
    '        Exit Sub
    '    End If

    'End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

#End Region

 
End Class
