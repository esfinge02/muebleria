Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmRepMovimientosClientes
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblClienteFin As System.Windows.Forms.Label
    Friend WithEvents lkpClienteFin As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente1 As System.Windows.Forms.Label
    Friend WithEvents lkpClienteInicio As Dipros.Editors.TINMultiLookup
    Friend WithEvents dteFecha_Ini As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblTipoVenta As System.Windows.Forms.Label
    Friend WithEvents cboTipoVenta As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents dteFecha_Fin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblConvenioCambiado As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepMovimientosClientes))
        Me.lblClienteFin = New System.Windows.Forms.Label
        Me.lkpClienteFin = New Dipros.Editors.TINMultiLookup
        Me.lblCliente1 = New System.Windows.Forms.Label
        Me.lkpClienteInicio = New Dipros.Editors.TINMultiLookup
        Me.dteFecha_Ini = New DevExpress.XtraEditors.DateEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.lblTipoVenta = New System.Windows.Forms.Label
        Me.cboTipoVenta = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.dteFecha_Fin = New DevExpress.XtraEditors.DateEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblConvenioCambiado = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(936, 28)
        '
        'lblClienteFin
        '
        Me.lblClienteFin.AutoSize = True
        Me.lblClienteFin.BackColor = System.Drawing.SystemColors.Window
        Me.lblClienteFin.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClienteFin.ForeColor = System.Drawing.Color.Black
        Me.lblClienteFin.Location = New System.Drawing.Point(266, 67)
        Me.lblClienteFin.Name = "lblClienteFin"
        Me.lblClienteFin.Size = New System.Drawing.Size(82, 16)
        Me.lblClienteFin.TabIndex = 4
        Me.lblClienteFin.Tag = ""
        Me.lblClienteFin.Text = "C&liente Hasta:"
        Me.lblClienteFin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpClienteFin
        '
        Me.lkpClienteFin.AllowAdd = False
        Me.lkpClienteFin.AutoReaload = True
        Me.lkpClienteFin.BackColor = System.Drawing.SystemColors.Window
        Me.lkpClienteFin.DataSource = Nothing
        Me.lkpClienteFin.DefaultSearchField = ""
        Me.lkpClienteFin.DisplayMember = "cliente"
        Me.lkpClienteFin.EditValue = Nothing
        Me.lkpClienteFin.Filtered = False
        Me.lkpClienteFin.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpClienteFin.ForeColor = System.Drawing.Color.Black
        Me.lkpClienteFin.InitValue = Nothing
        Me.lkpClienteFin.Location = New System.Drawing.Point(353, 64)
        Me.lkpClienteFin.MultiSelect = False
        Me.lkpClienteFin.Name = "lkpClienteFin"
        Me.lkpClienteFin.NullText = ""
        Me.lkpClienteFin.PopupWidth = CType(300, Long)
        Me.lkpClienteFin.ReadOnlyControl = False
        Me.lkpClienteFin.Required = False
        Me.lkpClienteFin.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteFin.SearchMember = ""
        Me.lkpClienteFin.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteFin.SelectAll = False
        Me.lkpClienteFin.Size = New System.Drawing.Size(136, 20)
        Me.lkpClienteFin.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteFin.TabIndex = 5
        Me.lkpClienteFin.Tag = "cliente"
        Me.lkpClienteFin.ToolTip = "Cliente Hasta"
        Me.lkpClienteFin.ValueMember = "cliente"
        '
        'lblCliente1
        '
        Me.lblCliente1.AutoSize = True
        Me.lblCliente1.BackColor = System.Drawing.SystemColors.Window
        Me.lblCliente1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente1.ForeColor = System.Drawing.Color.Black
        Me.lblCliente1.Location = New System.Drawing.Point(18, 67)
        Me.lblCliente1.Name = "lblCliente1"
        Me.lblCliente1.Size = New System.Drawing.Size(85, 16)
        Me.lblCliente1.TabIndex = 2
        Me.lblCliente1.Tag = ""
        Me.lblCliente1.Text = "&Cliente Desde:"
        Me.lblCliente1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpClienteInicio
        '
        Me.lkpClienteInicio.AllowAdd = False
        Me.lkpClienteInicio.AutoReaload = True
        Me.lkpClienteInicio.BackColor = System.Drawing.SystemColors.Window
        Me.lkpClienteInicio.DataSource = Nothing
        Me.lkpClienteInicio.DefaultSearchField = ""
        Me.lkpClienteInicio.DisplayMember = "cliente"
        Me.lkpClienteInicio.EditValue = Nothing
        Me.lkpClienteInicio.Filtered = False
        Me.lkpClienteInicio.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpClienteInicio.ForeColor = System.Drawing.Color.Black
        Me.lkpClienteInicio.InitValue = Nothing
        Me.lkpClienteInicio.Location = New System.Drawing.Point(108, 64)
        Me.lkpClienteInicio.MultiSelect = False
        Me.lkpClienteInicio.Name = "lkpClienteInicio"
        Me.lkpClienteInicio.NullText = ""
        Me.lkpClienteInicio.PopupWidth = CType(300, Long)
        Me.lkpClienteInicio.ReadOnlyControl = False
        Me.lkpClienteInicio.Required = False
        Me.lkpClienteInicio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpClienteInicio.SearchMember = ""
        Me.lkpClienteInicio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpClienteInicio.SelectAll = False
        Me.lkpClienteInicio.Size = New System.Drawing.Size(136, 20)
        Me.lkpClienteInicio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpClienteInicio.TabIndex = 3
        Me.lkpClienteInicio.Tag = ""
        Me.lkpClienteInicio.ToolTip = "Cliente Desde"
        Me.lkpClienteInicio.ValueMember = "cliente"
        '
        'dteFecha_Ini
        '
        Me.dteFecha_Ini.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Ini.Location = New System.Drawing.Point(108, 88)
        Me.dteFecha_Ini.Name = "dteFecha_Ini"
        '
        'dteFecha_Ini.Properties
        '
        Me.dteFecha_Ini.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Ini.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Ini.Size = New System.Drawing.Size(136, 23)
        Me.dteFecha_Ini.TabIndex = 7
        Me.dteFecha_Ini.ToolTip = "Fecha Inicial"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(60, 91)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "&Desde:"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(43, 139)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 12
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = False
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(108, 136)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = "(Todos)"
        Me.lkpCobrador.PopupWidth = CType(300, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = True
        Me.lkpCobrador.Size = New System.Drawing.Size(381, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 13
        Me.lkpCobrador.Tag = "cobrador"
        Me.lkpCobrador.ToolTip = "Cobrador"
        Me.lkpCobrador.ValueMember = "cobrador"
        '
        'lblTipoVenta
        '
        Me.lblTipoVenta.AutoSize = True
        Me.lblTipoVenta.Location = New System.Drawing.Point(35, 115)
        Me.lblTipoVenta.Name = "lblTipoVenta"
        Me.lblTipoVenta.Size = New System.Drawing.Size(68, 16)
        Me.lblTipoVenta.TabIndex = 10
        Me.lblTipoVenta.Tag = ""
        Me.lblTipoVenta.Text = "&Tipo Venta:"
        Me.lblTipoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoVenta
        '
        Me.cboTipoVenta.EditValue = ""
        Me.cboTipoVenta.Location = New System.Drawing.Point(108, 112)
        Me.cboTipoVenta.Name = "cboTipoVenta"
        '
        'cboTipoVenta.Properties
        '
        Me.cboTipoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoVenta.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Normales", 3, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enajenaci�n", 2, -1)})
        Me.cboTipoVenta.Size = New System.Drawing.Size(136, 23)
        Me.cboTipoVenta.TabIndex = 11
        Me.cboTipoVenta.Tag = "tipo"
        Me.cboTipoVenta.ToolTip = "Tipo de Venta"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lblSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSucursal.ForeColor = System.Drawing.Color.Black
        Me.lblSucursal.Location = New System.Drawing.Point(47, 43)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(108, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(300, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(381, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = "Sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'dteFecha_Fin
        '
        Me.dteFecha_Fin.EditValue = New Date(2006, 3, 14, 0, 0, 0, 0)
        Me.dteFecha_Fin.Location = New System.Drawing.Point(353, 88)
        Me.dteFecha_Fin.Name = "dteFecha_Fin"
        '
        'dteFecha_Fin.Properties
        '
        Me.dteFecha_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Fin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Fin.Size = New System.Drawing.Size(136, 23)
        Me.dteFecha_Fin.TabIndex = 9
        Me.dteFecha_Fin.ToolTip = "Fecha Final"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(307, 91)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 16)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Ha&sta:"
        '
        'lblConvenioCambiado
        '
        Me.lblConvenioCambiado.AutoSize = True
        Me.lblConvenioCambiado.Location = New System.Drawing.Point(43, 160)
        Me.lblConvenioCambiado.Name = "lblConvenioCambiado"
        Me.lblConvenioCambiado.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenioCambiado.TabIndex = 14
        Me.lblConvenioCambiado.Tag = ""
        Me.lblConvenioCambiado.Text = "Convenio:"
        Me.lblConvenioCambiado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(108, 160)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = "(Todos)"
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(381, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 15
        Me.lkpConvenio.Tag = ""
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'frmRepMovimientosClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(506, 188)
        Me.Controls.Add(Me.lblConvenioCambiado)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.dteFecha_Fin)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.lkpCobrador)
        Me.Controls.Add(Me.lblTipoVenta)
        Me.Controls.Add(Me.cboTipoVenta)
        Me.Controls.Add(Me.lblClienteFin)
        Me.Controls.Add(Me.lkpClienteFin)
        Me.Controls.Add(Me.lblCliente1)
        Me.Controls.Add(Me.lkpClienteInicio)
        Me.Controls.Add(Me.dteFecha_Ini)
        Me.Controls.Add(Me.Label4)
        Me.Name = "frmRepMovimientosClientes"
        Me.Text = "frmRepMovimientosClientes"
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Ini, 0)
        Me.Controls.SetChildIndex(Me.lkpClienteInicio, 0)
        Me.Controls.SetChildIndex(Me.lblCliente1, 0)
        Me.Controls.SetChildIndex(Me.lkpClienteFin, 0)
        Me.Controls.SetChildIndex(Me.lblClienteFin, 0)
        Me.Controls.SetChildIndex(Me.cboTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Fin, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioCambiado, 0)
        CType(Me.dteFecha_Ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oReportes As VillarrealBusiness.Reportes
    Private oClienteInicio As VillarrealBusiness.clsClientes
    Private oClienteFin As VillarrealBusiness.clsClientes
    Private oCobrador As VillarrealBusiness.clsCobradores
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oConvenios As VillarrealBusiness.clsConvenios


    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property ClienteInicio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteInicio)

        End Get
    End Property
    Private ReadOnly Property ClienteFin() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpClienteFin)
        End Get
    End Property
    Private ReadOnly Property Cobrador() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCobrador)
        End Get
    End Property
    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepMovimientosClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        Response = oReportes.MovimientosClientes(Me.Sucursal, Me.ClienteInicio, Me.ClienteFin, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue, Me.cboTipoVenta.EditValue, Me.Cobrador, Me.Convenio)

        If Response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Movimientos de Clientes no se puede Mostrar")
        Else
            Dim oDataSet As DataSet
            oDataSet = Response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Dim oReport As New rptMovimientosClientes
                oReport.DataSource = oDataSet.Tables(0)
                TINApp.ShowReport(Me.MdiParent, "Movimientos Clientes", oReport)
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If

            oDataSet = Nothing
        End If

    End Sub

    Private Sub frmfrmRepMovimientosClientes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oReportes.Validacion(Me.Sucursal, Me.ClienteInicio, Me.ClienteFin, Me.dteFecha_Ini.EditValue, Me.dteFecha_Fin.EditValue)
    End Sub

    Private Sub frmRepMovimientosClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oReportes = New VillarrealBusiness.Reportes
        oClienteInicio = New VillarrealBusiness.clsClientes
        oClienteFin = New VillarrealBusiness.clsClientes
        oSucursales = New VillarrealBusiness.clsSucursales
        oCobrador = New VillarrealBusiness.clsCobradores
        oConvenios = New VillarrealBusiness.clsConvenios

        Me.cboTipoVenta.SelectedIndex = 0 'TODOS
        Me.dteFecha_Ini.EditValue = CDate("01" + TINApp.FechaServidor.Substring(2, TINApp.FechaServidor.Length - 2))
        Me.dteFecha_Fin.EditValue = CDate(TINApp.FechaServidor)

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

#End Region

#Region " DIPROS Systems,Eventos de Controles "

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpClienteInicio_LoadData(ByVal Initialize As Boolean) Handles lkpClienteInicio.LoadData
        Dim Response As New Events
        Response = oClienteInicio.LookupCliente
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteInicio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpClienteInicio_Format() Handles lkpClienteInicio.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteInicio)
    End Sub

    Private Sub lkpClienteInicio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpClienteInicio.EditValueChanged
        'Dim Response As New Events
        'Response = oClienteInicio.LookupLlenado(False, lkpClienteInicio.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpClienteInicio.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub

    Private Sub lkpClienteFin_LoaData(ByVal Initialize As Boolean) Handles lkpClienteFin.LoadData
        Dim Response As New Events
        Response = oClienteFin.LookupCliente
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpClienteFin.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpClienteFin_Format() Handles lkpClienteFin.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpClienteFin)
    End Sub

    Private Sub lkpClienteFin_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpClienteFin.EditValueChanged
        'Dim Response As New Events
        'Response = oClienteFin.LookupLlenado(False, lkpClienteFin.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpClienteFin.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub

    Private Sub lkpCobrador_LoaData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData
        Dim Response As New Events
        Response = oCobrador.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub


    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub


#End Region



End Class
