Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmRepAntiguedadesSaldosCliente
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents chkDesglosado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblFechaCorte As System.Windows.Forms.Label
    Friend WithEvents dteFechaCorte As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lkpCobrador As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblTipoVenta As System.Windows.Forms.Label
    Friend WithEvents cboTipoVenta As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkSaldosVencidos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblConvenioCambiado As System.Windows.Forms.Label
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRepAntiguedadesSaldosCliente))
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.chkDesglosado = New DevExpress.XtraEditors.CheckEdit
        Me.lblFechaCorte = New System.Windows.Forms.Label
        Me.dteFechaCorte = New DevExpress.XtraEditors.DateEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblCobrador = New System.Windows.Forms.Label
        Me.lkpCobrador = New Dipros.Editors.TINMultiLookup
        Me.lblTipoVenta = New System.Windows.Forms.Label
        Me.cboTipoVenta = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.chkSaldosVencidos = New DevExpress.XtraEditors.CheckEdit
        Me.lblConvenioCambiado = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSaldosVencidos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(88, 64)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = True
        Me.lkpCliente.Size = New System.Drawing.Size(264, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 3
        Me.lkpCliente.ToolTip = "Seleccione un cliente"
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(36, 64)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 2
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkDesglosado
        '
        Me.chkDesglosado.Location = New System.Drawing.Point(224, 136)
        Me.chkDesglosado.Name = "chkDesglosado"
        '
        'chkDesglosado.Properties
        '
        Me.chkDesglosado.Properties.Caption = "Des&glosado"
        Me.chkDesglosado.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkDesglosado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkDesglosado.Size = New System.Drawing.Size(110, 22)
        Me.chkDesglosado.TabIndex = 12
        Me.chkDesglosado.Tag = "desglosado"
        Me.chkDesglosado.ToolTip = "desglosado"
        '
        'lblFechaCorte
        '
        Me.lblFechaCorte.AutoSize = True
        Me.lblFechaCorte.Location = New System.Drawing.Point(8, 163)
        Me.lblFechaCorte.Name = "lblFechaCorte"
        Me.lblFechaCorte.Size = New System.Drawing.Size(75, 16)
        Me.lblFechaCorte.TabIndex = 10
        Me.lblFechaCorte.Tag = ""
        Me.lblFechaCorte.Text = "&Fecha Corte:"
        Me.lblFechaCorte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFechaCorte
        '
        Me.dteFechaCorte.EditValue = "31/05/2006"
        Me.dteFechaCorte.Location = New System.Drawing.Point(88, 163)
        Me.dteFechaCorte.Name = "dteFechaCorte"
        '
        'dteFechaCorte.Properties
        '
        Me.dteFechaCorte.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCorte.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCorte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaCorte.Size = New System.Drawing.Size(95, 23)
        Me.dteFechaCorte.TabIndex = 11
        Me.dteFechaCorte.Tag = "fechaCorte"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lblSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSucursal.ForeColor = System.Drawing.Color.Black
        Me.lblSucursal.Location = New System.Drawing.Point(27, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(88, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = "(Todos)"
        Me.lkpSucursal.PopupWidth = CType(360, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(264, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal"
        Me.lkpSucursal.ToolTip = "Sucursal"
        Me.lkpSucursal.ValueMember = "sucursal"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.Location = New System.Drawing.Point(23, 88)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(60, 16)
        Me.lblCobrador.TabIndex = 4
        Me.lblCobrador.Tag = ""
        Me.lblCobrador.Text = "C&obrador:"
        Me.lblCobrador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCobrador
        '
        Me.lkpCobrador.AllowAdd = False
        Me.lkpCobrador.AutoReaload = True
        Me.lkpCobrador.DataSource = Nothing
        Me.lkpCobrador.DefaultSearchField = ""
        Me.lkpCobrador.DisplayMember = "nombre"
        Me.lkpCobrador.EditValue = Nothing
        Me.lkpCobrador.Filtered = False
        Me.lkpCobrador.InitValue = Nothing
        Me.lkpCobrador.Location = New System.Drawing.Point(88, 88)
        Me.lkpCobrador.MultiSelect = False
        Me.lkpCobrador.Name = "lkpCobrador"
        Me.lkpCobrador.NullText = ""
        Me.lkpCobrador.PopupWidth = CType(300, Long)
        Me.lkpCobrador.ReadOnlyControl = False
        Me.lkpCobrador.Required = False
        Me.lkpCobrador.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCobrador.SearchMember = ""
        Me.lkpCobrador.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCobrador.SelectAll = True
        Me.lkpCobrador.Size = New System.Drawing.Size(264, 20)
        Me.lkpCobrador.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCobrador.TabIndex = 5
        Me.lkpCobrador.Tag = "cobrador"
        Me.lkpCobrador.ToolTip = "Cobrador"
        Me.lkpCobrador.ValueMember = "cobrador"
        '
        'lblTipoVenta
        '
        Me.lblTipoVenta.AutoSize = True
        Me.lblTipoVenta.Location = New System.Drawing.Point(31, 136)
        Me.lblTipoVenta.Name = "lblTipoVenta"
        Me.lblTipoVenta.Size = New System.Drawing.Size(52, 16)
        Me.lblTipoVenta.TabIndex = 8
        Me.lblTipoVenta.Tag = ""
        Me.lblTipoVenta.Text = "Vencido:"
        Me.lblTipoVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoVenta
        '
        Me.cboTipoVenta.EditValue = 0
        Me.cboTipoVenta.Location = New System.Drawing.Point(88, 136)
        Me.cboTipoVenta.Name = "cboTipoVenta"
        '
        'cboTipoVenta.Properties
        '
        Me.cboTipoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoVenta.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todas", 0, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("1-30 Dias", 1, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("31-60 Dias", 2, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("61-90 Dias", 3, -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("M�s de 90", 4, -1)})
        Me.cboTipoVenta.Size = New System.Drawing.Size(128, 23)
        Me.cboTipoVenta.TabIndex = 9
        Me.cboTipoVenta.Tag = "tipo"
        Me.cboTipoVenta.ToolTip = "Tipo de Venta"
        '
        'chkSaldosVencidos
        '
        Me.chkSaldosVencidos.Location = New System.Drawing.Point(224, 163)
        Me.chkSaldosVencidos.Name = "chkSaldosVencidos"
        '
        'chkSaldosVencidos.Properties
        '
        Me.chkSaldosVencidos.Properties.Caption = "Solo Saldos Vencidos"
        Me.chkSaldosVencidos.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkSaldosVencidos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkSaldosVencidos.Size = New System.Drawing.Size(136, 22)
        Me.chkSaldosVencidos.TabIndex = 13
        Me.chkSaldosVencidos.Tag = "SaldosVencidos"
        '
        'lblConvenioCambiado
        '
        Me.lblConvenioCambiado.AutoSize = True
        Me.lblConvenioCambiado.Location = New System.Drawing.Point(23, 112)
        Me.lblConvenioCambiado.Name = "lblConvenioCambiado"
        Me.lblConvenioCambiado.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenioCambiado.TabIndex = 6
        Me.lblConvenioCambiado.Tag = ""
        Me.lblConvenioCambiado.Text = "Convenio:"
        Me.lblConvenioCambiado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(88, 112)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = "(Todos)"
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(264, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 7
        Me.lkpConvenio.Tag = ""
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'frmRepAntiguedadesSaldosCliente
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(362, 196)
        Me.Controls.Add(Me.lblConvenioCambiado)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.chkSaldosVencidos)
        Me.Controls.Add(Me.lblTipoVenta)
        Me.Controls.Add(Me.cboTipoVenta)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.lkpCobrador)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.chkDesglosado)
        Me.Controls.Add(Me.lblFechaCorte)
        Me.Controls.Add(Me.dteFechaCorte)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblCliente)
        Me.Name = "frmRepAntiguedadesSaldosCliente"
        Me.Text = "frmRepAntiguedadesSaldosCliente"
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFechaCorte, 0)
        Me.Controls.SetChildIndex(Me.lblFechaCorte, 0)
        Me.Controls.SetChildIndex(Me.chkDesglosado, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpCobrador, 0)
        Me.Controls.SetChildIndex(Me.lblCobrador, 0)
        Me.Controls.SetChildIndex(Me.cboTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVenta, 0)
        Me.Controls.SetChildIndex(Me.chkSaldosVencidos, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioCambiado, 0)
        CType(Me.chkDesglosado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFechaCorte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSaldosVencidos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oReportes As VillarrealBusiness.Reportes
    Private oClientes As VillarrealBusiness.clsClientes
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oCobrador As VillarrealBusiness.clsCobradores
    Private oConvenios As VillarrealBusiness.clsConvenios



    Private ReadOnly Property Sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)

        End Get
    End Property

    Friend ReadOnly Property NombreCliente() As String
        Get
            Return IIf(Me.lkpCliente.EditValue Is Nothing, "", CStr(Me.lkpCliente.GetValue("nombre")))
        End Get
    End Property

    Private ReadOnly Property Cobrador() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCobrador)
        End Get
    End Property

    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmRepAntiguedadesSaldosCliente_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept


        If Me.chkDesglosado.Checked = True Then
            'ANTIGUEDADES DE SALDOS CLIENTE DESGLOSADO
            Response = oReportes.AntiguedadesSaldosClienteDesglosado(Sucursal, Me.Cliente, Me.dteFechaCorte.EditValue, Me.Cobrador, Me.cboTipoVenta.Value, Me.chkSaldosVencidos.Checked, Me.Convenio)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte de Antiguedades de Saldos de Cliente Desglosado no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptAntiguedadesSaldosClienteDesglosado
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Antiguedades de Saldos de Cliente Desglosado", oReport, , , , True)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If


        Else
            'ANTIGUEDADES DE SALDOS 
            Response = oReportes.AntiguedadesSaldosCliente(Sucursal, Me.Cliente, Me.dteFechaCorte.EditValue, Me.Cobrador, Me.cboTipoVenta.Value, Me.chkSaldosVencidos.Checked, Me.Convenio)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "El Reporte de Antiguedades de Saldos de Cliente no se puede Mostrar")
            Else
                Dim oDataSet As DataSet
                oDataSet = Response.Value

                If oDataSet.Tables(0).Rows.Count > 0 Then
                    Dim oReport As New rptAntiguedadesSaldosCliente
                    oReport.DataSource = oDataSet.Tables(0)
                    TINApp.ShowReport(Me.MdiParent, "Antiguedades de Saldos de Cliente", oReport, , , , True)
                    oReport = Nothing
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If

                oDataSet = Nothing
            End If
        End If



    End Sub
    Private Sub frmRepAntiguedadesSaldosCliente_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oReportes = New VillarrealBusiness.Reportes
        oClientes = New VillarrealBusiness.clsClientes
        oCobrador = New VillarrealBusiness.clsCobradores
        oConvenios = New VillarrealBusiness.clsConvenios

        Me.dteFechaCorte.EditValue = CDate(TINApp.FechaServidor)

    End Sub
    Private Sub frmRepAntiguedadesSaldosCliente_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        If Me.chkDesglosado.Checked = True Then
            Response = oReportes.ValidacionAntiguedadSaldos(Me.dteFechaCorte.EditValue, Me.Cliente)
        Else
            Response = oReportes.Validacion(Me.dteFechaCorte.EditValue)
        End If

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub



    Private Sub lkpCobrador_LoaData(ByVal Initialize As Boolean) Handles lkpCobrador.LoadData
        Dim Response As New Events
        Response = oCobrador.Lookup(Me.Sucursal)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCobrador.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
            Me.lkpCobrador.SelectAll = True

        End If
        Response = Nothing
    End Sub
    Private Sub lkpCobrador_Format() Handles lkpCobrador.Format
        Comunes.clsFormato.for_cobradores_grl(Me.lkpCobrador)
    End Sub


    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub

#End Region


End Class
