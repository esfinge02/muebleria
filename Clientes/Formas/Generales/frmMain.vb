Imports Dipros.Utils.Common
Imports Dipros.Windows.Forms

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuCatCobradores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatConceptosCuentasCobrar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProGenerarNotaCredito As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCambiarVencimientos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepEstadosCuentaClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProCambiarFechaVentas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProTraspasoSaldos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProNotasCredito As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProNotasCargo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepAcumuladoClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatMunicipios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatCiudades As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatColonias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSaldosGenerales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSaldosVencidos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMovimientosClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenMovimientos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDocumentosVencer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepAnualClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepAuxiliarFacturas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepVentasConSaldo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatEstados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuRepAntiguedadesSaldosCliente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepSugerenciaCobro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepCuentasNoLocalizables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepPagosEfectivo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProOrdenesRecuperacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProNotasCargoDescuentosAnticipados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepResumenContabilidad As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepOrdenesRecuperacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarEditItem1 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents mnuProNotasCreditoNotasCargo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuCatAltasClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatZonas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepDocumentosxA�o As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProNotaCreditoCondonacionIVA As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatConvenios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProAbonosMegaCred As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProProductos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProEnvioInformacionConvenios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepLlamadas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProModificacionCobro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProMapa As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatParentesco As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatMensaje As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatCobradores = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatConceptosCuentasCobrar = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProGenerarNotaCredito = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCambiarVencimientos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepEstadosCuentaClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProCambiarFechaVentas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProTraspasoSaldos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProNotasCredito = New DevExpress.XtraBars.BarButtonItem
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProNotasCargo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepAcumuladoClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatMunicipios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatCiudades = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatColonias = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSaldosGenerales = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSaldosVencidos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMovimientosClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenMovimientos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDocumentosVencer = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepAnualClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepAuxiliarFacturas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepVentasConSaldo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatEstados = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuRepAntiguedadesSaldosCliente = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepSugerenciaCobro = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepCuentasNoLocalizables = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepPagosEfectivo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProOrdenesRecuperacion = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProNotasCargoDescuentosAnticipados = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepResumenContabilidad = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepOrdenesRecuperacion = New DevExpress.XtraBars.BarButtonItem
        Me.BarEditItem1 = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.mnuProNotasCreditoNotasCargo = New DevExpress.XtraBars.BarButtonItem
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
        Me.mnuCatAltasClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatZonas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepDocumentosxA�o = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProNotaCreditoCondonacionIVA = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatConvenios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProAbonosMegaCred = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProProductos = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProEnvioInformacionConvenios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepLlamadas = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProModificacionCobro = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProMapa = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatParentesco = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatMensaje = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatCobradores, Me.mnuCatConceptosCuentasCobrar, Me.mnuCatClientes, Me.mnuProGenerarNotaCredito, Me.mnuProCambiarVencimientos, Me.mnuRepEstadosCuentaClientes, Me.mnuProCambiarFechaVentas, Me.mnuProTraspasoSaldos, Me.mnuProNotasCredito, Me.BarButtonItem1, Me.mnuProNotasCargo, Me.mnuRepAcumuladoClientes, Me.mnuCatMunicipios, Me.mnuCatCiudades, Me.mnuCatColonias, Me.mnuRepSaldosGenerales, Me.mnuRepSaldosVencidos, Me.mnuRepMovimientosClientes, Me.mnuRepResumenMovimientos, Me.mnuRepDocumentosVencer, Me.mnuRepAnualClientes, Me.mnuRepAuxiliarFacturas, Me.mnuRepVentasConSaldo, Me.mnuCatEstados, Me.BarStaticItem1, Me.mnuRepAntiguedadesSaldosCliente, Me.mnuRepSugerenciaCobro, Me.mnuRepCuentasNoLocalizables, Me.mnuRepPagosEfectivo, Me.mnuProOrdenesRecuperacion, Me.mnuProNotasCargoDescuentosAnticipados, Me.mnuRepResumenContabilidad, Me.mnuRepOrdenesRecuperacion, Me.BarEditItem1, Me.mnuProNotasCreditoNotasCargo, Me.BarSubItem1, Me.mnuCatAltasClientes, Me.mnuCatZonas, Me.mnuRepDocumentosxA�o, Me.mnuProNotaCreditoCondonacionIVA, Me.mnuCatConvenios, Me.mnuProAbonosMegaCred, Me.mnuProProductos, Me.mnuProEnvioInformacionConvenios, Me.mnuRepLlamadas, Me.mnuProModificacionCobro, Me.mnuProMapa, Me.mnuCatParentesco, Me.mnuCatMensaje})
        Me.BarManager.MaxItemId = 157
        Me.BarManager.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1})
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'barMainMenu
        '
        Me.barMainMenu.FloatLocation = New System.Drawing.Point(22, 152)
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True)})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatAltasClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCobradores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatConceptosCuentasCobrar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatEstados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatMunicipios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatCiudades), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatColonias), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatZonas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatConvenios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatParentesco), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatMensaje)})
        '
        'mnuProcesos
        '
        Me.mnuProcesos.ImageIndex = 27
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProGenerarNotaCredito), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProNotasCargoDescuentosAnticipados), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCambiarVencimientos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProCambiarFechaVentas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProTraspasoSaldos), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProNotasCargo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProOrdenesRecuperacion), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProNotaCreditoCondonacionIVA), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProAbonosMegaCred), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProProductos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProEnvioInformacionConvenios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProModificacionCobro), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProMapa)})
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepEstadosCuentaClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepAcumuladoClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSaldosGenerales), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSaldosVencidos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMovimientosClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenMovimientos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDocumentosVencer), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepAnualClientes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepAuxiliarFacturas), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepVentasConSaldo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepAntiguedadesSaldosCliente), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepSugerenciaCobro), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepCuentasNoLocalizables), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepDocumentosxA�o), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepOrdenesRecuperacion), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepResumenContabilidad), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepPagosEfectivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepLlamadas)})
        '
        'mnuCatCobradores
        '
        Me.mnuCatCobradores.Caption = "C&obradores"
        Me.mnuCatCobradores.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCobradores.Id = 105
        Me.mnuCatCobradores.ImageIndex = 1
        Me.mnuCatCobradores.Name = "mnuCatCobradores"
        '
        'mnuCatConceptosCuentasCobrar
        '
        Me.mnuCatConceptosCuentasCobrar.Caption = "Conceptos de C&uentas por Cobrar"
        Me.mnuCatConceptosCuentasCobrar.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatConceptosCuentasCobrar.Id = 106
        Me.mnuCatConceptosCuentasCobrar.ImageIndex = 2
        Me.mnuCatConceptosCuentasCobrar.Name = "mnuCatConceptosCuentasCobrar"
        '
        'mnuCatClientes
        '
        Me.mnuCatClientes.Caption = "Clien&tes"
        Me.mnuCatClientes.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatClientes.Id = 107
        Me.mnuCatClientes.ImageIndex = 0
        Me.mnuCatClientes.Name = "mnuCatClientes"
        '
        'mnuProGenerarNotaCredito
        '
        Me.mnuProGenerarNotaCredito.Caption = "&Generar Notas de Cr�dito por Descuentos Anticipados"
        Me.mnuProGenerarNotaCredito.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProGenerarNotaCredito.Id = 108
        Me.mnuProGenerarNotaCredito.ImageIndex = 3
        Me.mnuProGenerarNotaCredito.Name = "mnuProGenerarNotaCredito"
        '
        'mnuProCambiarVencimientos
        '
        Me.mnuProCambiarVencimientos.Caption = "Ca&mbiar Fechas de Vencimiento"
        Me.mnuProCambiarVencimientos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCambiarVencimientos.Id = 110
        Me.mnuProCambiarVencimientos.ImageIndex = 4
        Me.mnuProCambiarVencimientos.Name = "mnuProCambiarVencimientos"
        '
        'mnuRepEstadosCuentaClientes
        '
        Me.mnuRepEstadosCuentaClientes.Caption = "&Estados de Cuenta"
        Me.mnuRepEstadosCuentaClientes.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepEstadosCuentaClientes.Id = 111
        Me.mnuRepEstadosCuentaClientes.ImageIndex = 9
        Me.mnuRepEstadosCuentaClientes.Name = "mnuRepEstadosCuentaClientes"
        '
        'mnuProCambiarFechaVentas
        '
        Me.mnuProCambiarFechaVentas.Caption = "Cambiar &Fecha de Ventas"
        Me.mnuProCambiarFechaVentas.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProCambiarFechaVentas.Id = 112
        Me.mnuProCambiarFechaVentas.ImageIndex = 5
        Me.mnuProCambiarFechaVentas.Name = "mnuProCambiarFechaVentas"
        '
        'mnuProTraspasoSaldos
        '
        Me.mnuProTraspasoSaldos.Caption = "&Traspaso de Saldos"
        Me.mnuProTraspasoSaldos.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProTraspasoSaldos.Id = 113
        Me.mnuProTraspasoSaldos.ImageIndex = 6
        Me.mnuProTraspasoSaldos.Name = "mnuProTraspasoSaldos"
        '
        'mnuProNotasCredito
        '
        Me.mnuProNotasCredito.Caption = "&Notas de Cr�dito"
        Me.mnuProNotasCredito.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProNotasCredito.Id = 114
        Me.mnuProNotasCredito.ImageIndex = 7
        Me.mnuProNotasCredito.Name = "mnuProNotasCredito"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "BarButtonItem1"
        Me.BarButtonItem1.Id = 115
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'mnuProNotasCargo
        '
        Me.mnuProNotasCargo.Caption = "N&otas de Cargo"
        Me.mnuProNotasCargo.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProNotasCargo.Id = 116
        Me.mnuProNotasCargo.ImageIndex = 8
        Me.mnuProNotasCargo.Name = "mnuProNotasCargo"
        '
        'mnuRepAcumuladoClientes
        '
        Me.mnuRepAcumuladoClientes.Caption = "Ac&umulado de Clientes"
        Me.mnuRepAcumuladoClientes.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepAcumuladoClientes.Id = 117
        Me.mnuRepAcumuladoClientes.ImageIndex = 10
        Me.mnuRepAcumuladoClientes.Name = "mnuRepAcumuladoClientes"
        '
        'mnuCatMunicipios
        '
        Me.mnuCatMunicipios.Caption = "&Municipios"
        Me.mnuCatMunicipios.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatMunicipios.Id = 118
        Me.mnuCatMunicipios.ImageIndex = 11
        Me.mnuCatMunicipios.Name = "mnuCatMunicipios"
        '
        'mnuCatCiudades
        '
        Me.mnuCatCiudades.Caption = "&Localidades"
        Me.mnuCatCiudades.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatCiudades.GroupIndex = 12
        Me.mnuCatCiudades.Id = 119
        Me.mnuCatCiudades.ImageIndex = 12
        Me.mnuCatCiudades.Name = "mnuCatCiudades"
        '
        'mnuCatColonias
        '
        Me.mnuCatColonias.Caption = "Colo&nias"
        Me.mnuCatColonias.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatColonias.Id = 120
        Me.mnuCatColonias.ImageIndex = 13
        Me.mnuCatColonias.Name = "mnuCatColonias"
        '
        'mnuRepSaldosGenerales
        '
        Me.mnuRepSaldosGenerales.Caption = "&Saldos Generales"
        Me.mnuRepSaldosGenerales.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSaldosGenerales.Id = 122
        Me.mnuRepSaldosGenerales.ImageIndex = 21
        Me.mnuRepSaldosGenerales.Name = "mnuRepSaldosGenerales"
        '
        'mnuRepSaldosVencidos
        '
        Me.mnuRepSaldosVencidos.Caption = "Sa&ldos Vencidos"
        Me.mnuRepSaldosVencidos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSaldosVencidos.Id = 123
        Me.mnuRepSaldosVencidos.ImageIndex = 15
        Me.mnuRepSaldosVencidos.Name = "mnuRepSaldosVencidos"
        '
        'mnuRepMovimientosClientes
        '
        Me.mnuRepMovimientosClientes.Caption = "&Movimientos Clientes"
        Me.mnuRepMovimientosClientes.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMovimientosClientes.Id = 124
        Me.mnuRepMovimientosClientes.ImageIndex = 22
        Me.mnuRepMovimientosClientes.Name = "mnuRepMovimientosClientes"
        '
        'mnuRepResumenMovimientos
        '
        Me.mnuRepResumenMovimientos.Caption = "Resume&n  de Movimientos"
        Me.mnuRepResumenMovimientos.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenMovimientos.Id = 125
        Me.mnuRepResumenMovimientos.ImageIndex = 16
        Me.mnuRepResumenMovimientos.Name = "mnuRepResumenMovimientos"
        '
        'mnuRepDocumentosVencer
        '
        Me.mnuRepDocumentosVencer.Caption = "&Documentos por Vencer"
        Me.mnuRepDocumentosVencer.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDocumentosVencer.Id = 126
        Me.mnuRepDocumentosVencer.ImageIndex = 17
        Me.mnuRepDocumentosVencer.Name = "mnuRepDocumentosVencer"
        '
        'mnuRepAnualClientes
        '
        Me.mnuRepAnualClientes.Caption = "Anual de Cl&ientes"
        Me.mnuRepAnualClientes.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepAnualClientes.Id = 127
        Me.mnuRepAnualClientes.ImageIndex = 23
        Me.mnuRepAnualClientes.Name = "mnuRepAnualClientes"
        '
        'mnuRepAuxiliarFacturas
        '
        Me.mnuRepAuxiliarFacturas.Caption = "Au&xiliar de Facturas"
        Me.mnuRepAuxiliarFacturas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepAuxiliarFacturas.Id = 128
        Me.mnuRepAuxiliarFacturas.ImageIndex = 18
        Me.mnuRepAuxiliarFacturas.Name = "mnuRepAuxiliarFacturas"
        '
        'mnuRepVentasConSaldo
        '
        Me.mnuRepVentasConSaldo.Caption = "Ven&tas con Saldo"
        Me.mnuRepVentasConSaldo.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepVentasConSaldo.Id = 129
        Me.mnuRepVentasConSaldo.ImageIndex = 19
        Me.mnuRepVentasConSaldo.Name = "mnuRepVentasConSaldo"
        '
        'mnuCatEstados
        '
        Me.mnuCatEstados.Caption = "&Estados"
        Me.mnuCatEstados.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatEstados.Id = 131
        Me.mnuCatEstados.ImageIndex = 14
        Me.mnuCatEstados.Name = "mnuCatEstados"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(43, 437)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 132
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'mnuRepAntiguedadesSaldosCliente
        '
        Me.mnuRepAntiguedadesSaldosCliente.Caption = "Antig�edad de Sald&os"
        Me.mnuRepAntiguedadesSaldosCliente.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepAntiguedadesSaldosCliente.Id = 133
        Me.mnuRepAntiguedadesSaldosCliente.ImageIndex = 24
        Me.mnuRepAntiguedadesSaldosCliente.Name = "mnuRepAntiguedadesSaldosCliente"
        '
        'mnuRepSugerenciaCobro
        '
        Me.mnuRepSugerenciaCobro.Caption = "Su&gerencia de Cobro"
        Me.mnuRepSugerenciaCobro.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepSugerenciaCobro.Id = 134
        Me.mnuRepSugerenciaCobro.ImageIndex = 25
        Me.mnuRepSugerenciaCobro.Name = "mnuRepSugerenciaCobro"
        '
        'mnuRepCuentasNoLocalizables
        '
        Me.mnuRepCuentasNoLocalizables.Caption = "Cuentas &No Localizables"
        Me.mnuRepCuentasNoLocalizables.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepCuentasNoLocalizables.Id = 135
        Me.mnuRepCuentasNoLocalizables.ImageIndex = 26
        Me.mnuRepCuentasNoLocalizables.Name = "mnuRepCuentasNoLocalizables"
        '
        'mnuRepPagosEfectivo
        '
        Me.mnuRepPagosEfectivo.Caption = "Pa&gos en Efectivo"
        Me.mnuRepPagosEfectivo.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepPagosEfectivo.Id = 136
        Me.mnuRepPagosEfectivo.ImageIndex = 28
        Me.mnuRepPagosEfectivo.Name = "mnuRepPagosEfectivo"
        '
        'mnuProOrdenesRecuperacion
        '
        Me.mnuProOrdenesRecuperacion.Caption = "&Ordenes de Recuperaci�n"
        Me.mnuProOrdenesRecuperacion.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProOrdenesRecuperacion.Id = 137
        Me.mnuProOrdenesRecuperacion.ImageIndex = 30
        Me.mnuProOrdenesRecuperacion.Name = "mnuProOrdenesRecuperacion"
        '
        'mnuProNotasCargoDescuentosAnticipados
        '
        Me.mnuProNotasCargoDescuentosAnticipados.Caption = "G&enerar Notas de Cargo por Descuentos Anticipados"
        Me.mnuProNotasCargoDescuentosAnticipados.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProNotasCargoDescuentosAnticipados.Id = 138
        Me.mnuProNotasCargoDescuentosAnticipados.ImageIndex = 29
        Me.mnuProNotasCargoDescuentosAnticipados.Name = "mnuProNotasCargoDescuentosAnticipados"
        '
        'mnuRepResumenContabilidad
        '
        Me.mnuRepResumenContabilidad.Caption = "Resumen de Conta&bilidad"
        Me.mnuRepResumenContabilidad.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepResumenContabilidad.Id = 139
        Me.mnuRepResumenContabilidad.ImageIndex = 31
        Me.mnuRepResumenContabilidad.Name = "mnuRepResumenContabilidad"
        '
        'mnuRepOrdenesRecuperacion
        '
        Me.mnuRepOrdenesRecuperacion.Caption = "Ordenes de Recuperaci�n"
        Me.mnuRepOrdenesRecuperacion.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepOrdenesRecuperacion.Id = 140
        Me.mnuRepOrdenesRecuperacion.Name = "mnuRepOrdenesRecuperacion"
        '
        'BarEditItem1
        '
        Me.BarEditItem1.Caption = "Notas de Cr�dito"
        Me.BarEditItem1.Edit = Me.RepositoryItemTextEdit1
        Me.BarEditItem1.Id = 141
        Me.BarEditItem1.Name = "BarEditItem1"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'mnuProNotasCreditoNotasCargo
        '
        Me.mnuProNotasCreditoNotasCargo.Caption = "Notas de Cr�dito a Notas de Cargo"
        Me.mnuProNotasCreditoNotasCargo.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProNotasCreditoNotasCargo.Id = 142
        Me.mnuProNotasCreditoNotasCargo.ImageIndex = 7
        Me.mnuProNotasCreditoNotasCargo.Name = "mnuProNotasCreditoNotasCargo"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "Notas de Credito"
        Me.BarSubItem1.Id = 143
        Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProNotasCredito), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProNotasCreditoNotasCargo)})
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'mnuCatAltasClientes
        '
        Me.mnuCatAltasClientes.Caption = "Altas de Clientes"
        Me.mnuCatAltasClientes.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatAltasClientes.Id = 144
        Me.mnuCatAltasClientes.Name = "mnuCatAltasClientes"
        '
        'mnuCatZonas
        '
        Me.mnuCatZonas.Caption = "Zonas"
        Me.mnuCatZonas.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatZonas.Id = 145
        Me.mnuCatZonas.Name = "mnuCatZonas"
        '
        'mnuRepDocumentosxA�o
        '
        Me.mnuRepDocumentosxA�o.Caption = "Documentos por A�o"
        Me.mnuRepDocumentosxA�o.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepDocumentosxA�o.Id = 146
        Me.mnuRepDocumentosxA�o.Name = "mnuRepDocumentosxA�o"
        '
        'mnuProNotaCreditoCondonacionIVA
        '
        Me.mnuProNotaCreditoCondonacionIVA.Caption = "Nota de Cr�dito por Condonacion de IVA"
        Me.mnuProNotaCreditoCondonacionIVA.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProNotaCreditoCondonacionIVA.Id = 147
        Me.mnuProNotaCreditoCondonacionIVA.Name = "mnuProNotaCreditoCondonacionIVA"
        '
        'mnuCatConvenios
        '
        Me.mnuCatConvenios.Caption = "Convenios"
        Me.mnuCatConvenios.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatConvenios.Id = 148
        Me.mnuCatConvenios.Name = "mnuCatConvenios"
        '
        'mnuProAbonosMegaCred
        '
        Me.mnuProAbonosMegaCred.Caption = "Subir Abonos MegaCred"
        Me.mnuProAbonosMegaCred.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProAbonosMegaCred.Id = 149
        Me.mnuProAbonosMegaCred.Name = "mnuProAbonosMegaCred"
        '
        'mnuProProductos
        '
        Me.mnuProProductos.Caption = "Productos"
        Me.mnuProProductos.CategoryGuid = New System.Guid("e4f571e0-2013-4e73-908e-71dd259d36ad")
        Me.mnuProProductos.Id = 150
        Me.mnuProProductos.Name = "mnuProProductos"
        '
        'mnuProEnvioInformacionConvenios
        '
        Me.mnuProEnvioInformacionConvenios.Caption = "Env�o de Informaci�n de Convenios"
        Me.mnuProEnvioInformacionConvenios.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProEnvioInformacionConvenios.Id = 151
        Me.mnuProEnvioInformacionConvenios.Name = "mnuProEnvioInformacionConvenios"
        '
        'mnuRepLlamadas
        '
        Me.mnuRepLlamadas.Caption = "Llamadas"
        Me.mnuRepLlamadas.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepLlamadas.Id = 152
        Me.mnuRepLlamadas.Name = "mnuRepLlamadas"
        '
        'mnuProModificacionCobro
        '
        Me.mnuProModificacionCobro.Caption = "Agregar Direcciones de Cobro"
        Me.mnuProModificacionCobro.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProModificacionCobro.Id = 153
        Me.mnuProModificacionCobro.Name = "mnuProModificacionCobro"
        '
        'mnuProMapa
        '
        Me.mnuProMapa.Caption = "Mapa de Clientes"
        Me.mnuProMapa.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProMapa.Id = 154
        Me.mnuProMapa.Name = "mnuProMapa"
        '
        'mnuCatParentesco
        '
        Me.mnuCatParentesco.Caption = "Parentesco"
        Me.mnuCatParentesco.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatParentesco.Id = 155
        Me.mnuCatParentesco.Name = "mnuCatParentesco"
        '
        'mnuCatMensaje
        '
        Me.mnuCatMensaje.Caption = "Mensaje Dejado"
        Me.mnuCatMensaje.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatMensaje.Id = 156
        Me.mnuCatMensaje.Name = "mnuCatMensaje"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"


    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim response As Dipros.Utils.Events
        response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value

            Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
        End If
    End Sub

    Private Sub frmMain_LoadOptions() Handles MyBase.LoadOptions
        With TINApp.Options.Security
            '---------------------------------------------------------------------------------------------
            .Options("mnuProNotasCargoDescuentosAnticipados").AddAction("FECHA_N_C_DESC", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProTraspasoSaldos").AddAction("FECHA_TRA_SALDO", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProNotasCargo").AddAction("FECHA_N_C", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProNotasCredito").AddAction("FECHA_N_CR", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)
            '---------------------------------------------------------------------------------------------
            .Options("mnuProNotasCreditoNotasCargo").AddAction("FECHA_NCR_A_NC", "Modificar Fecha", Dipros.Utils.MenuAction.ActionTypes.Extended)

            '.Options("mnuCatClientes").AddAction("Ins_f", "Ins Abonos a Fletes", Dipros.Utils.MenuAction.ActionTypes.Extended)

            '.Options("mnuCatClientes").AddAction("Upd_f", "Upd Abonos a Fletes", Dipros.Utils.MenuAction.ActionTypes.Extended)

            '.Options("mnuCatClientes").AddAction("Del_f", "Del Abonos a Fletes", Dipros.Utils.MenuAction.ActionTypes.Extended)

        End With
    End Sub
#End Region

#Region "Catalogos"

    Private Sub mnuCatAltasClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatAltasClientes.ItemClick
        Dim oForm As New Comunes.frmSolicitudesClienteActual

        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Alta de Clientes"

            .Show()
        End With
    End Sub

    Public Sub mnuCatCobradores_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCobradores.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCobradores As New VillarrealBusiness.clsCobradores
        With oGrid
            .Title = "Cobradores"
            .UpdateTitle = "Cobradores"
            .DataSource = AddressOf oCobradores.Listado
            .UpdateForm = New Comunes.frmCobradores
            .Format = AddressOf Comunes.clsFormato.for_cobradores_grs
            .MdiParent = Me
            .Show()
            .Width = 680
        End With
        oCobradores = Nothing
        oGrid = Nothing


    End Sub
    Public Sub mnuCatConceptosCuentasCobrar_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatConceptosCuentasCobrar.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oConceptosCuentasCobrar As New VillarrealBusiness.clsConceptosCxc   'VillarrealBusiness.clsConceptosCuentasCobrar
        With oGrid
            .Title = "Conceptos de Cuentas por Cobrar"
            .UpdateTitle = "Conceptos de Cuentas por Cobrar"
            .DataSource = AddressOf oConceptosCuentasCobrar.Listado
            .UpdateForm = New frmConceptosCuentasCobrar
            .Format = AddressOf for_conceptos_cuentas_cobrar_grs
            .MdiParent = Me
            .Show()
            .Width = 430
        End With
        oConceptosCuentasCobrar = Nothing
    End Sub
    Public Sub mnuCatClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatClientes.ItemClick
        Dim oGrid As New Comunes.brwClientes

        With oGrid
            .MenuOption = e.Item
            .Title = "Clientes"
            .UpdateTitle = "un Cliente"
            '.UpdateForm = New Comunes.frmClientes
            .Format = AddressOf Comunes.clsFormato.for_clientes_grs
            .MdiParent = Me
            .MenuOption = e.Item            
            .Show()
        End With


    End Sub
    Public Sub mnuCatEstados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatEstados.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oEstados As New VillarrealBusiness.clsEstados
        With oGrid
            .CanDelete = False
            .CanInsert = False
            .Title = "Estados"
            .UpdateTitle = "Estados"
            .DataSource = AddressOf oEstados.Listado
            .UpdateForm = New frmEstados
            .Format = AddressOf for_estados_grs
            .MdiParent = Me
            .Show()
        End With
        oEstados = Nothing
    End Sub
    Public Sub mnuCatMunicipios_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatMunicipios.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oMunicipios As New VillarrealBusiness.clsMunicipios
        With oGrid
            .Title = "Municipios"
            .UpdateTitle = "Municipio"
            .DataSource = AddressOf oMunicipios.Listado
            .UpdateForm = New Comunes.frmMunicipios
            .Format = AddressOf Comunes.clsFormato.for_municipios_grs
            .MdiParent = Me
            .Show()
            .Width = 430
        End With
        oMunicipios = Nothing
    End Sub
    Public Sub mnuCatCiudades_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatCiudades.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oCiudades As New VillarrealBusiness.clsCiudades
        With oGrid
            .Title = "Localidades"
            .UpdateTitle = "Localidad"
            .DataSource = AddressOf oCiudades.Listado
            .UpdateForm = New frmCiudades
            .Format = AddressOf for_ciudades_grs
            .MdiParent = Me
            .Show()
            .Width = 500
        End With
        oCiudades = Nothing
    End Sub
    Public Sub mnuCatColonias_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatColonias.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oColonias As New VillarrealBusiness.clsColonias
        With oGrid
            .Title = "Colonias"
            .UpdateTitle = "Colonia"
            .DataSource = AddressOf oColonias.Listado
            .UpdateForm = New frmColonias
            .Format = AddressOf for_colonias_grs
            .MdiParent = Me
            .Show()
            .Width = 600
        End With
        oColonias = Nothing
    End Sub

    Private Sub mnuCatZonas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatZonas.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oZonas As New VillarrealBusiness.clsZonas
        With oGrid
            .Title = "Zonas"
            .UpdateTitle = "Zona"
            .DataSource = AddressOf oZonas.Listado
            .UpdateForm = New frmZonas
            .Format = AddressOf for_zonas_grs
            .MdiParent = Me
            .Show()
            .Width = 600
        End With
        oZonas = Nothing
    End Sub


    Public Sub mnuCatConvenios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatConvenios.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oConvenios As New VillarrealBusiness.clsConvenios
        With oGrid
            .Title = "Convenios"
            .UpdateTitle = "Convenio"
            .DataSource = AddressOf oConvenios.Listado
            .UpdateForm = New frmConvenios
            .Format = AddressOf for_convenios_grs
            .MdiParent = Me
            .Show()
            .Width = 600
        End With
        oConvenios = Nothing
    End Sub

    Public Sub mnuCatParentesco_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatParentesco.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oParentesco As New VillarrealBusiness.clsTiposParentesco
        With oGrid
            .Title = "Parentesco"
            .UpdateTitle = "Parentesco"
            .DataSource = AddressOf oParentesco.Listado
            .UpdateForm = New frmParentesco
            .Format = AddressOf for_parentesco_grs
            .MdiParent = Me
            .Show()
            .Width = 600
        End With
        oParentesco = Nothing
    End Sub

    Public Sub mnuCatMensajeDejado_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatMensaje.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oMensaje As New VillarrealBusiness.clsMensajeDejado
        With oGrid
            .Title = "Mensaje Dejado"
            .UpdateTitle = "Mensaje Dejado"
            .DataSource = AddressOf oMensaje.Listado
            .UpdateForm = New frmMensajeDejado
            .Format = AddressOf for_mensaje_dejado_grs
            .MdiParent = Me
            .Show()
            .Width = 600
        End With
        oMensaje = Nothing
    End Sub


#End Region

#Region "Procesos"
    Public Sub mnuProGenerarNotaCredito_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProGenerarNotaCredito.ItemClick
        Dim oForm As New Comunes.frmGeneracionNotasCredito
        oForm.MenuOption = e.Item
        oForm.MdiParent = Me
        oForm.Title = "Generar Notas de Cr�dito por Descuentos Anticipados"
        oForm.Show()
    End Sub

    Public Sub mnuProCambiarVencimientos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCambiarVencimientos.ItemClick
        Dim oForm As New frmCambioVencimientos
        oForm.MenuOption = e.Item
        oForm.MdiParent = Me
        oForm.Title = "Cambiar Fechas de Vencimiento"
        oForm.Show()
    End Sub
    Public Sub mnuProCambiarFechaVentas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProCambiarFechaVentas.ItemClick
        Dim oForm As New frmCambioFechaVentas
        oForm.MenuOption = e.Item
        oForm.MdiParent = Me
        oForm.Title = "Cambiar Fecha de Venta"
        oForm.Show()
    End Sub
    Public Sub mnuProTraspasoSaldos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProTraspasoSaldos.ItemClick
        Dim oForm As New frmTraspasoSaldos
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Traspaso de Saldos"
            .Show()
        End With
    End Sub

    Public Sub mnuProNotasCargoDescuentosAnticipados_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProNotasCargoDescuentosAnticipados.ItemClick
        Dim oForm As New Comunes.frmGeneracionNotasCargoDescuentosAnticipados
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Notas de Cargo por Descuentos Anticipados"
            .Show()
        End With
    End Sub
    Public Sub mnuProNotasCredito_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProNotasCredito.ItemClick
        Dim oForm As New Comunes.frmNotasCredito
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Notas de Cr�dito"
            .Show()
        End With
    End Sub
    Public Sub mnuProNotasCreditoNotasCargo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProNotasCreditoNotasCargo.ItemClick
        Dim oForm As New frmNotasCreditoANotasCargo
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Notas de Cr�dito a Notas de Cargo"
            .Show()
        End With
    End Sub
    Public Sub mnuProNotasCargo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProNotasCargo.ItemClick
        Dim oForm As New Comunes.frmNotasCargo
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Notas de Cargo"
            .Show()
        End With
    End Sub

    Public Sub mnuProOrdenesRecuperacion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProOrdenesRecuperacion.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oOrdenesRecuperacion As New VillarrealBusiness.clsOrdenesRecuperacion
        With oGrid
            .Title = "Ordenes de Recuperaci�n"
            .UpdateTitle = "Ordenes de Recuperaci�n"
            .DataSource = AddressOf oOrdenesRecuperacion.Listado
            .UpdateForm = New Comunes.frmOrdenesRecuperacion
            .Format = AddressOf for_ordenes_recuperacion_grs
            .MdiParent = Me
            .Show()
        End With
        oOrdenesRecuperacion = Nothing
    End Sub

    Private Sub mnuProNotaCreditoCondonacionIVA_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProNotaCreditoCondonacionIVA.ItemClick
        Dim oForm As New frmGeneracionNotaCreditoCondonacionIVA
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Notas de Cr�dito por Condonaci�n de IVA"
            .Show()
        End With
    End Sub

    Public Sub mnuProAbonosMegaCred_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProAbonosMegaCred.ItemClick
        Dim oForm As New Comunes.DescuentosMasivosMegaCred
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Abonos Masivos de MegaCred"
            .Show()
        End With
    End Sub
    'Public Sub mnuProOrdenesCompra_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProOrdenesCompra.ItemClick
    '     Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
    '     Dim oOrdenesCompra As New VillarrealBusiness.clsOrdenesCompra
    '     With oGrid
    '         .Title = "�rdenes de Compra"
    '         .UpdateTitle = "una Orden de Compra"
    '         .DataSource = AddressOf oOrdenesCompra.Listado
    '         .UpdateForm = New frmOrdenesCompra
    '         .Format = AddressOf for_ordenes_compra_grs
    '         .MdiParent = Me
    '         .Show()
    '     End With
    '     oOrdenesCompra = Nothing
    'End Sub
    Public Sub mnuProProductos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProProductos.ItemClick
        Dim oGrid As New brwProductos

        With oGrid
            .MenuOption = e.Item
            .CanInsert = False
            .Title = "Productos"
            .UpdateTitle = "un Producto"
            .UpdateForm = New frmProductos
            .Format = AddressOf ModFormatos.for_productos_grs
            .MdiParent = Me
            .Show()
        End With

        oGrid = Nothing

    End Sub

    Public Sub mnuProEnvioInformacionConvenios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProEnvioInformacionConvenios.ItemClick
        Dim oForm As New Comunes.frmEnvioInformacionConvenios
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Envio de Informaci�n de Convenios"
            .Show()
        End With
    End Sub

    Private Sub mnuProModificacionCobro_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProModificacionCobro.ItemClick
        Dim oForm As New frmAcualizacionClientesCobro
        With oForm
            .MenuOption = e.Item
            .MdiParent = Me
            .Title = "Modificacion de Clientes Datos de Cobro"
            .Show()
        End With
    End Sub

    Public Sub mnuProMapa_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProMapa.ItemClick
        Dim oForm As New frmMapa
        With oForm
            '.MenuOption = e.Item
            .MdiParent = Me
            '.Title = "Mapa de Direcciones de Clientes"
            .Show()
        End With
    End Sub
#End Region

#Region "Reportes"

    Public Sub mnuRepEstadosCuentaClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepEstadosCuentaClientes.ItemClick
        Dim oForm As New Comunes.frmRepEstadoDeCuenta
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Estado de Cuenta"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepAcumuladoClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepAcumuladoClientes.ItemClick
        Dim oForm As New frmRepAcumuladoClientes
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Acumulado de Clientes"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepSaldosGenerales_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSaldosGenerales.ItemClick
        Dim oForm As New Comunes.frmRepSaldosGenerales
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Saldos Generales"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepSaldosVencidos_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSaldosVencidos.ItemClick
        Dim oForm As New Comunes.frmRepSaldosVencidos
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Saldos Vencidos"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepMovimientosClientes_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMovimientosClientes.ItemClick
        Dim oForm As New frmRepMovimientosClientes
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Movimientos de Clientes"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepResumenMovimientos_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenMovimientos.ItemClick
        Dim oForm As New frmRepResumenMovimientos
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Resumen de Movimientos"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepDocumentosVencer_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDocumentosVencer.ItemClick
        Dim oForm As New frmRepDocumentosVencer
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Documentos por Vencer"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepAnualClientes_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepAnualClientes.ItemClick
        Dim oForm As New frmRepAnualClientes
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Anual de Clientes"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepAuxiliarFacturas_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepAuxiliarFacturas.ItemClick
        Dim oForm As New frmRepAuxiliarFacturas
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Auxiliar de Facturas"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
    Public Sub mnuRepVentasConSaldo_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepVentasConSaldo.ItemClick
        Dim oForm As New frmRepVentasConSaldo
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Ventas con Saldo"
            .Action = Actions.Report
            .Show()
        End With
    End Sub

    Public Sub mnuRepPagosEfectivo_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepPagosEfectivo.ItemClick
        Dim oForm As New frmRepPagosEfectivo
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Pagos en Efectivo"
            .Action = Actions.Report
            .Show()
        End With
    End Sub

    Public Sub mnuRepAntiguedadesSaldosCliente_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepAntiguedadesSaldosCliente.ItemClick
        Dim oForm As New frmRepAntiguedadesSaldosCliente
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Antiguedades de Saldos de Cliente"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
    Public Sub mnuRepSugerenciaCobro_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepSugerenciaCobro.ItemClick
        Dim oForm As New Comunes.frmRepSugerenciaCobro
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Sugerencia de Cobro"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepCuentasNoLocalizables_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepCuentasNoLocalizables.ItemClick
        Dim oForm As New frmRepCuentasNoLocalizables
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Cuentas No Localizables"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepResumenContabilidad_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepResumenContabilidad.ItemClick
        Dim oForm As New Comunes.frmResumenContabilidad
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Resumen de Contabilidad"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepOrdenesRecuperacion_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepOrdenesRecuperacion.ItemClick
        Dim oForm As New Comunes.frmRepOrdenesRecuperacion
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Ordenes de Recuperaci�n"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub

    Public Sub mnuRepDocumentosxA�o_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepDocumentosxA�o.ItemClick
        Dim oForm As New frmDocumentosxA�o
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Documentos por A�o"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub


    Public Sub mnuRepLlamadas_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepLlamadas.ItemClick
        Dim oForm As New frmReporteLlamadas
        oForm.MdiParent = Me
        oForm.MenuOption = e.Item
        oForm.Title = "Llamadas"
        oForm.Action = Actions.Report
        oForm.Show()
    End Sub
#End Region



  
  
End Class
