Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmEstados
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    'Dim KS As Keys
#End Region


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents clcEstado As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEstados))
        Me.lblEstado = New System.Windows.Forms.Label
        Me.clcEstado = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(27, 50)
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(40, 40)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 63
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "&Estado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcEstado
        '
        Me.clcEstado.EditValue = "0"
        Me.clcEstado.Location = New System.Drawing.Point(97, 40)
        Me.clcEstado.MaxValue = 0
        Me.clcEstado.MinValue = 0
        Me.clcEstado.Name = "clcEstado"
        '
        'clcEstado.Properties
        '
        Me.clcEstado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEstado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcEstado.Properties.Enabled = False
        Me.clcEstado.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcEstado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcEstado.Size = New System.Drawing.Size(52, 19)
        Me.clcEstado.TabIndex = 64
        Me.clcEstado.Tag = "estado"
        Me.clcEstado.ToolTip = "Estado"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(17, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 65
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(97, 64)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(208, 20)
        Me.txtDescripcion.TabIndex = 66
        Me.txtDescripcion.Tag = "descripcion"
        Me.txtDescripcion.ToolTip = "Descripci�n"
        '
        'frmEstados
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(322, 103)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.clcEstado)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmEstados"
        Me.Text = "frmEstados"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcEstado, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        CType(Me.clcEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"
    Private oEstados As VillarrealBusiness.clsEstados
#End Region


#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmEstados_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            'Case Actions.Insert
            '    Response = oEstados.Insertar(Me.DataSource)

        Case Actions.Update
                Response = oEstados.Actualizar(Me.DataSource)

                'Case Actions.Delete
                '    Response = oEstados.Eliminar(clcEstado.Value)

        End Select
    End Sub

    Private Sub frmEstados_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oEstados.DespliegaDatos(OwnerForm.Value("estado"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmEstados_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oEstados = New VillarrealBusiness.clsEstados

        Select Case Action

            'Case Actions.Insert
        Case Actions.Update
                'Case Actions.Delete

        End Select
    End Sub

    Private Sub frmEstados_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oEstados.Validacion(Action, Me.txtDescripcion.Text)
    End Sub

    Private Sub frmCiudades_Localize() Handles MyBase.Localize
        Find("estado", CType(Me.clcEstado.EditValue, Object))
    End Sub

#End Region


#Region "DIPROS Systems, Eventos de Controles"

#End Region


#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
