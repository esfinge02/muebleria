Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmConceptosCuentasCobrar
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents txtdescripcion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents chkMostrarse_Listado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboTipoConcepto As DevExpress.XtraEditors.ImageComboBoxEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConceptosCuentasCobrar))
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.lblTipo = New System.Windows.Forms.Label
        Me.cboTipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.txtdescripcion = New DevExpress.XtraEditors.MemoEdit
        Me.chkMostrarse_Listado = New DevExpress.XtraEditors.CheckEdit
        Me.cboTipoConcepto = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtdescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMostrarse_Listado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(237, 28)
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(48, 40)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 0
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "&Concepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(113, 40)
        Me.txtConcepto.Name = "txtConcepto"
        '
        'txtConcepto.Properties
        '
        Me.txtConcepto.Properties.MaxLength = 5
        Me.txtConcepto.Size = New System.Drawing.Size(60, 20)
        Me.txtConcepto.TabIndex = 1
        Me.txtConcepto.Tag = "concepto"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(36, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(76, 136)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(32, 16)
        Me.lblTipo.TabIndex = 4
        Me.lblTipo.Tag = ""
        Me.lblTipo.Text = "&Tipo:"
        Me.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = "C"
        Me.cboTipo.Location = New System.Drawing.Point(113, 134)
        Me.cboTipo.Name = "cboTipo"
        '
        'cboTipo.Properties
        '
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cargo", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Abono", "A", -1)})
        Me.cboTipo.Size = New System.Drawing.Size(68, 20)
        Me.cboTipo.TabIndex = 5
        Me.cboTipo.Tag = "tipo"
        '
        'txtdescripcion
        '
        Me.txtdescripcion.EditValue = ""
        Me.txtdescripcion.Location = New System.Drawing.Point(113, 65)
        Me.txtdescripcion.Name = "txtdescripcion"
        Me.txtdescripcion.Size = New System.Drawing.Size(180, 64)
        Me.txtdescripcion.TabIndex = 3
        Me.txtdescripcion.Tag = "descripcion"
        '
        'chkMostrarse_Listado
        '
        Me.chkMostrarse_Listado.Location = New System.Drawing.Point(112, 184)
        Me.chkMostrarse_Listado.Name = "chkMostrarse_Listado"
        '
        'chkMostrarse_Listado.Properties
        '
        Me.chkMostrarse_Listado.Properties.Caption = "Mostrarse en el listado"
        Me.chkMostrarse_Listado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkMostrarse_Listado.Size = New System.Drawing.Size(184, 19)
        Me.chkMostrarse_Listado.TabIndex = 8
        Me.chkMostrarse_Listado.Tag = "mostrarse_listado"
        '
        'cboTipoConcepto
        '
        Me.cboTipoConcepto.EditValue = ""
        Me.cboTipoConcepto.Location = New System.Drawing.Point(113, 160)
        Me.cboTipoConcepto.Name = "cboTipoConcepto"
        '
        'cboTipoConcepto.Properties
        '
        Me.cboTipoConcepto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoConcepto.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ninguno", "", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Abono Letras", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Contados", "C", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Enganches", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Descuento Anticipado", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Intereses", "I", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nota de Cargo", "N", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pronto Pago", "P", -1)})
        Me.cboTipoConcepto.Size = New System.Drawing.Size(152, 20)
        Me.cboTipoConcepto.TabIndex = 7
        Me.cboTipoConcepto.Tag = "tipo_concepto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 160)
        Me.Label2.Name = "Label2"
        Me.Label2.TabIndex = 6
        Me.Label2.Tag = ""
        Me.Label2.Text = "Ti&po Movimiento:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmConceptosCuentasCobrar
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(306, 208)
        Me.Controls.Add(Me.chkMostrarse_Listado)
        Me.Controls.Add(Me.txtdescripcion)
        Me.Controls.Add(Me.lblConcepto)
        Me.Controls.Add(Me.txtConcepto)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.cboTipo)
        Me.Controls.Add(Me.cboTipoConcepto)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmConceptosCuentasCobrar"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.cboTipoConcepto, 0)
        Me.Controls.SetChildIndex(Me.cboTipo, 0)
        Me.Controls.SetChildIndex(Me.lblTipo, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.txtConcepto, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto, 0)
        Me.Controls.SetChildIndex(Me.txtdescripcion, 0)
        Me.Controls.SetChildIndex(Me.chkMostrarse_Listado, 0)
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtdescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMostrarse_Listado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oConceptosCuentasCobrar As VillarrealBusiness.clsConceptosCxc    'VillarrealBusiness.clsConceptosCuentasCobrar
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmConceptosCuentasCobrar_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oConceptosCuentasCobrar.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oConceptosCuentasCobrar.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oConceptosCuentasCobrar.Eliminar(txtConcepto.Text)

        End Select
    End Sub

    Private Sub frmConceptosCuentasCobrar_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oConceptosCuentasCobrar.DespliegaDatos(OwnerForm.Value("concepto"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmConceptosCuentasCobrar_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oConceptosCuentasCobrar = New VillarrealBusiness.clsConceptosCxc   'VillarrealBusiness.clsConceptosCuentasCobrar

        Me.chkMostrarse_Listado.Text = "Mostrarse en el listado de " + Me.cboTipo.Text + "(s)"

        cboTipoConcepto.SelectedIndex = 0

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.txtConcepto.Enabled = False
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmConceptosCuentasCobrar_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oConceptosCuentasCobrar.Validacion(Action, txtConcepto.Text, txtdescripcion.Text)
    End Sub

    Private Sub frmConceptosCuentasCobrar_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub cboTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipo.SelectedIndexChanged
        If Me.cboTipo.SelectedIndex >= 0 Then

            Me.chkMostrarse_Listado.Text = "Mostrarse en el listado de " + Me.cboTipo.Text + "(s)"
        End If

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region


End Class
