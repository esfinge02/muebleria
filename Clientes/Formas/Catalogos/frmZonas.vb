Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmZonas
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblZona As System.Windows.Forms.Label
    Friend WithEvents clcZona As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmZonas))
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblZona = New System.Windows.Forms.Label
        Me.clcZona = New Dipros.Editors.TINCalcEdit
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcZona.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(8, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 59
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(80, 64)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(344, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'lblZona
        '
        Me.lblZona.AutoSize = True
        Me.lblZona.Location = New System.Drawing.Point(40, 40)
        Me.lblZona.Name = "lblZona"
        Me.lblZona.Size = New System.Drawing.Size(37, 16)
        Me.lblZona.TabIndex = 60
        Me.lblZona.Tag = ""
        Me.lblZona.Text = "&Zona:"
        Me.lblZona.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcZona
        '
        Me.clcZona.EditValue = "0"
        Me.clcZona.Location = New System.Drawing.Point(80, 40)
        Me.clcZona.MaxValue = 0
        Me.clcZona.MinValue = 0
        Me.clcZona.Name = "clcZona"
        '
        'clcZona.Properties
        '
        Me.clcZona.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcZona.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcZona.Properties.Enabled = False
        Me.clcZona.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcZona.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcZona.Size = New System.Drawing.Size(72, 19)
        Me.clcZona.TabIndex = 61
        Me.clcZona.Tag = "zona"
        '
        'frmZonas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(449, 96)
        Me.Controls.Add(Me.lblZona)
        Me.Controls.Add(Me.clcZona)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmZonas"
        Me.Text = "frmZonas"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcZona, 0)
        Me.Controls.SetChildIndex(Me.lblZona, 0)
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcZona.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

   


#Region "DIPROS Systems, Declaraciones"
    Private oZonas As VillarrealBusiness.clsZonas
#End Region

#Region "DIPROS Systems, Eventos de la Forma"


    Private Sub frmZonas_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oZonas.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oZonas.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oZonas.Eliminar(Me.clcZona.Value)

        End Select
    End Sub

    Private Sub frmZonas_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oZonas.DespliegaDatos(OwnerForm.Value("zona"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmZonas_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oZonas.Validacion(Action, txtDescripcion.Text)
    End Sub

    Private Sub frmZonas_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oZonas = New VillarrealBusiness.clsZonas

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmZonas_Localize() Handles MyBase.Localize
        Find("zona", Me.clcZona.Text)
    End Sub



#End Region

#Region "DIPROS Systems, Eventos de Controles"
#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

   
   
End Class
