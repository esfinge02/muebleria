Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmParentesco
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcId_Parentesco As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtParentesco As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParentesco))
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcId_Parentesco = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtParentesco = New DevExpress.XtraEditors.TextEdit
        CType(Me.clcId_Parentesco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtParentesco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(344, 28)
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Enabled = False
        Me.lblFolio.Location = New System.Drawing.Point(38, 40)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 64
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcId_Parentesco
        '
        Me.clcId_Parentesco.EditValue = "0"
        Me.clcId_Parentesco.Location = New System.Drawing.Point(86, 40)
        Me.clcId_Parentesco.MaxValue = 0
        Me.clcId_Parentesco.MinValue = 0
        Me.clcId_Parentesco.Name = "clcId_Parentesco"
        '
        'clcId_Parentesco.Properties
        '
        Me.clcId_Parentesco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcId_Parentesco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcId_Parentesco.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcId_Parentesco.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcId_Parentesco.Size = New System.Drawing.Size(87, 19)
        Me.clcId_Parentesco.TabIndex = 65
        Me.clcId_Parentesco.Tag = "parentesco"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(0, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(69, 16)
        Me.lblDescripcion.TabIndex = 63
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "Parentesco:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtParentesco
        '
        Me.txtParentesco.EditValue = ""
        Me.txtParentesco.Location = New System.Drawing.Point(86, 64)
        Me.txtParentesco.Name = "txtParentesco"
        '
        'txtParentesco.Properties
        '
        Me.txtParentesco.Properties.MaxLength = 50
        Me.txtParentesco.Size = New System.Drawing.Size(242, 20)
        Me.txtParentesco.TabIndex = 62
        Me.txtParentesco.Tag = "descripcion"
        '
        'frmParentesco
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(394, 131)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.clcId_Parentesco)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtParentesco)
        Me.Name = "frmParentesco"
        Me.Text = "frmParentesco"
        Me.Controls.SetChildIndex(Me.txtParentesco, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcId_Parentesco, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        CType(Me.clcId_Parentesco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtParentesco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region




#Region "DIPROS Systems, Declaraciones"
    Private oParentesco As VillarrealBusiness.clsTiposParentesco
#End Region

#Region "DIPROS Systems, Eventos de la Forma"


    Private Sub frmParentesco_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oParentesco.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oParentesco.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oParentesco.Eliminar(Me.clcId_Parentesco.Value)

        End Select
    End Sub

    Private Sub frmParentesco_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oParentesco.DespliegaDatos(OwnerForm.Value("parentesco"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmParentesco_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oParentesco.Validacion(Action, txtParentesco.Text)
    End Sub

    Private Sub frmParentesco_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oParentesco = New VillarrealBusiness.clsTiposParentesco

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub


#End Region


End Class
