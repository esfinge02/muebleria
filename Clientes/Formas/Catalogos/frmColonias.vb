Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmColonias
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblColonia As System.Windows.Forms.Label
		Friend WithEvents clcColonia As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblCiudad As System.Windows.Forms.Label
		Friend WithEvents lkpCiudad As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TinCalcEdit1 As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lkpZonas As Dipros.Editors.TINMultiLookup

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmColonias))
        Me.lblColonia = New System.Windows.Forms.Label
        Me.clcColonia = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.lkpCiudad = New Dipros.Editors.TINMultiLookup
        Me.lblEstado = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.TinCalcEdit1 = New Dipros.Editors.TINCalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.lkpZonas = New Dipros.Editors.TINMultiLookup
        CType(Me.clcColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TinCalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(119, 50)
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(37, 40)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 0
        Me.lblColonia.Tag = ""
        Me.lblColonia.Text = "C&olonia:"
        Me.lblColonia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcColonia
        '
        Me.clcColonia.EditValue = "0"
        Me.clcColonia.Location = New System.Drawing.Point(92, 39)
        Me.clcColonia.MaxValue = 0
        Me.clcColonia.MinValue = 0
        Me.clcColonia.Name = "clcColonia"
        '
        'clcColonia.Properties
        '
        Me.clcColonia.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcColonia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcColonia.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcColonia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcColonia.Properties.Enabled = False
        Me.clcColonia.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcColonia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcColonia.Size = New System.Drawing.Size(52, 19)
        Me.clcColonia.TabIndex = 1
        Me.clcColonia.Tag = "colonia"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(15, 88)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 4
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(92, 88)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(300, 20)
        Me.txtDescripcion.TabIndex = 5
        Me.txtDescripcion.Tag = "descripcion"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(27, 160)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(60, 16)
        Me.lblCiudad.TabIndex = 10
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "&Localidad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCiudad
        '
        Me.lkpCiudad.AllowAdd = False
        Me.lkpCiudad.AutoReaload = False
        Me.lkpCiudad.DataSource = Nothing
        Me.lkpCiudad.DefaultSearchField = ""
        Me.lkpCiudad.DisplayMember = "descripcion"
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad.Filtered = False
        Me.lkpCiudad.InitValue = Nothing
        Me.lkpCiudad.Location = New System.Drawing.Point(92, 160)
        Me.lkpCiudad.MultiSelect = False
        Me.lkpCiudad.Name = "lkpCiudad"
        Me.lkpCiudad.NullText = ""
        Me.lkpCiudad.PopupWidth = CType(400, Long)
        Me.lkpCiudad.ReadOnlyControl = False
        Me.lkpCiudad.Required = False
        Me.lkpCiudad.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCiudad.SearchMember = ""
        Me.lkpCiudad.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCiudad.SelectAll = False
        Me.lkpCiudad.Size = New System.Drawing.Size(300, 20)
        Me.lkpCiudad.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCiudad.TabIndex = 11
        Me.lkpCiudad.Tag = "Ciudad"
        Me.lkpCiudad.ToolTip = Nothing
        Me.lkpCiudad.ValueMember = "Ciudad"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(41, 112)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 6
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "&Estado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Tag = ""
        Me.Label2.Text = "M&unicipio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = False
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(92, 136)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(400, Long)
        Me.lkpMunicipio.ReadOnlyControl = False
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(300, 20)
        Me.lkpMunicipio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpMunicipio.TabIndex = 9
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(92, 112)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.ReadOnlyControl = False
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(300, 20)
        Me.lkpEstado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpEstado.TabIndex = 7
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Tag = ""
        Me.Label3.Text = "Codigo Postal:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TinCalcEdit1
        '
        Me.TinCalcEdit1.EditValue = "0"
        Me.TinCalcEdit1.Location = New System.Drawing.Point(92, 64)
        Me.TinCalcEdit1.MaxValue = 0
        Me.TinCalcEdit1.MinValue = 0
        Me.TinCalcEdit1.Name = "TinCalcEdit1"
        '
        'TinCalcEdit1.Properties
        '
        Me.TinCalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TinCalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TinCalcEdit1.Properties.MaskData.EditMask = "###,###,##0"
        Me.TinCalcEdit1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.TinCalcEdit1.Size = New System.Drawing.Size(52, 19)
        Me.TinCalcEdit1.TabIndex = 3
        Me.TinCalcEdit1.Tag = "codigo_postal"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(50, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 16)
        Me.Label4.TabIndex = 12
        Me.Label4.Tag = ""
        Me.Label4.Text = "&Zona:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpZonas
        '
        Me.lkpZonas.AllowAdd = False
        Me.lkpZonas.AutoReaload = False
        Me.lkpZonas.DataSource = Nothing
        Me.lkpZonas.DefaultSearchField = ""
        Me.lkpZonas.DisplayMember = "descripcion"
        Me.lkpZonas.EditValue = Nothing
        Me.lkpZonas.Filtered = False
        Me.lkpZonas.InitValue = Nothing
        Me.lkpZonas.Location = New System.Drawing.Point(92, 184)
        Me.lkpZonas.MultiSelect = False
        Me.lkpZonas.Name = "lkpZonas"
        Me.lkpZonas.NullText = ""
        Me.lkpZonas.PopupWidth = CType(400, Long)
        Me.lkpZonas.ReadOnlyControl = False
        Me.lkpZonas.Required = False
        Me.lkpZonas.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpZonas.SearchMember = ""
        Me.lkpZonas.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpZonas.SelectAll = False
        Me.lkpZonas.Size = New System.Drawing.Size(300, 20)
        Me.lkpZonas.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpZonas.TabIndex = 13
        Me.lkpZonas.Tag = "zona"
        Me.lkpZonas.ToolTip = Nothing
        Me.lkpZonas.ValueMember = "zona"
        '
        'frmColonias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(402, 216)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lkpZonas)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TinCalcEdit1)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpMunicipio)
        Me.Controls.Add(Me.lblColonia)
        Me.Controls.Add(Me.clcColonia)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.lkpCiudad)
        Me.Name = "frmColonias"
        Me.Controls.SetChildIndex(Me.lkpCiudad, 0)
        Me.Controls.SetChildIndex(Me.lblCiudad, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcColonia, 0)
        Me.Controls.SetChildIndex(Me.lblColonia, 0)
        Me.Controls.SetChildIndex(Me.lkpMunicipio, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        Me.Controls.SetChildIndex(Me.TinCalcEdit1, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lkpZonas, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        CType(Me.clcColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TinCalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"

    Private oEstados As VillarrealBusiness.clsEstados
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oCiudades As VillarrealBusiness.clsCiudades
    Private oColonias As VillarrealBusiness.clsColonias
    Private oZonas As VillarrealBusiness.clsZonas


    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property
    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property
    Private ReadOnly Property Ciudad() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCiudad)
        End Get
    End Property
    Private ReadOnly Property Zona() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpZonas)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmColonias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    Private Sub frmColonias_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oColonias.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oColonias.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oColonias.Eliminar(Estado, Municipio, Ciudad, clcColonia.Value)

        End Select
    End Sub
    Private Sub frmColonias_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oColonias.DespliegaDatos(OwnerForm.Value("estado"), OwnerForm.Value("municipio"), OwnerForm.Value("ciudad"), OwnerForm.Value("colonia"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub
    Private Sub frmColonias_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oColonias = New VillarrealBusiness.clsColonias
        oCiudades = New VillarrealBusiness.clsCiudades
        oMunicipios = New VillarrealBusiness.clsMunicipios
        oEstados = New VillarrealBusiness.clsEstados
        oZonas = New VillarrealBusiness.clsZonas

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpCiudad.Enabled = False
                Me.lkpMunicipio.Enabled = False
                Me.lkpEstado.Enabled = False
            Case Actions.Delete
        End Select
    End Sub
    Private Sub frmColonias_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oColonias.Validacion(Action, Me.txtDescripcion.Text, Estado, Municipio, Ciudad)
    End Sub

    Private Sub frmColonias_Localize() Handles MyBase.Localize
        Find("colonia", CType(Me.clcColonia.EditValue, Object))
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        ModFormatos.for_estado_grl(Me.lkpEstado)
        'Comunes.clsFormato.for_estado_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpEstado_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        Comunes.clsFormato.for_municipios_grl(Me.lkpMunicipio)
    End Sub
    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData
        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpMunicipio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpMunicipio.EditValueChanged
        Me.lkpCiudad.EditValue = Nothing
        Me.lkpCiudad_LoadData(True)
    End Sub

    Private Sub lkpCiudad_Format() Handles lkpCiudad.Format
        Comunes.clsFormato.for_ciudades_grl(Me.lkpCiudad)
    End Sub
    Private Sub lkpCiudad_LoadData(ByVal Initialize As Boolean) Handles lkpCiudad.LoadData
        Dim Response As New Events
        Response = oCiudades.Lookup(Estado, Municipio)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCiudad.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


    Private Sub lkpZonas_LoadData(ByVal Initialize As Boolean) Handles lkpZonas.LoadData
        Dim Response As New Events
        Response = oZonas.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpZonas.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpZonas_Format() Handles lkpZonas.Format
        Comunes.clsFormato.for_zonas_grl(Me.lkpZonas)
    End Sub


#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region






End Class
