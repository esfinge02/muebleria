Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmCiudades
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblCiudad As System.Windows.Forms.Label
		Friend WithEvents clcCiudad As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpMunicipio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lkpEstado As Dipros.Editors.TINMultiLookup

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCiudades))
        Me.lblCiudad = New System.Windows.Forms.Label
        Me.clcCiudad = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpMunicipio = New Dipros.Editors.TINMultiLookup
        Me.lblEstado = New System.Windows.Forms.Label
        Me.lkpEstado = New Dipros.Editors.TINMultiLookup
        CType(Me.clcCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(27, 40)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(60, 16)
        Me.lblCiudad.TabIndex = 0
        Me.lblCiudad.Tag = ""
        Me.lblCiudad.Text = "&Localidad:"
        Me.lblCiudad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCiudad
        '
        Me.clcCiudad.EditValue = "0"
        Me.clcCiudad.Location = New System.Drawing.Point(92, 39)
        Me.clcCiudad.MaxValue = 0
        Me.clcCiudad.MinValue = 0
        Me.clcCiudad.Name = "clcCiudad"
        '
        'clcCiudad.Properties
        '
        Me.clcCiudad.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcCiudad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCiudad.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcCiudad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCiudad.Properties.Enabled = False
        Me.clcCiudad.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcCiudad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCiudad.Size = New System.Drawing.Size(52, 21)
        Me.clcCiudad.TabIndex = 1
        Me.clcCiudad.Tag = "ciudad"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(15, 63)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(92, 61)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(300, 22)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 109)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Tag = ""
        Me.Label2.Text = "M&unicipio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpMunicipio
        '
        Me.lkpMunicipio.AllowAdd = False
        Me.lkpMunicipio.AutoReaload = False
        Me.lkpMunicipio.DataSource = Nothing
        Me.lkpMunicipio.DefaultSearchField = ""
        Me.lkpMunicipio.DisplayMember = "descripcion"
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio.Filtered = False
        Me.lkpMunicipio.InitValue = Nothing
        Me.lkpMunicipio.Location = New System.Drawing.Point(92, 109)
        Me.lkpMunicipio.MultiSelect = False
        Me.lkpMunicipio.Name = "lkpMunicipio"
        Me.lkpMunicipio.NullText = ""
        Me.lkpMunicipio.PopupWidth = CType(400, Long)
        Me.lkpMunicipio.Required = False
        Me.lkpMunicipio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpMunicipio.SearchMember = ""
        Me.lkpMunicipio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpMunicipio.SelectAll = False
        Me.lkpMunicipio.Size = New System.Drawing.Size(300, 22)
        Me.lkpMunicipio.TabIndex = 7
        Me.lkpMunicipio.Tag = "municipio"
        Me.lkpMunicipio.ToolTip = Nothing
        Me.lkpMunicipio.ValueMember = "municipio"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(41, 85)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 16)
        Me.lblEstado.TabIndex = 4
        Me.lblEstado.Tag = ""
        Me.lblEstado.Text = "&Estado:"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpEstado
        '
        Me.lkpEstado.AllowAdd = False
        Me.lkpEstado.AutoReaload = False
        Me.lkpEstado.DataSource = Nothing
        Me.lkpEstado.DefaultSearchField = ""
        Me.lkpEstado.DisplayMember = "descripcion"
        Me.lkpEstado.EditValue = Nothing
        Me.lkpEstado.Filtered = False
        Me.lkpEstado.InitValue = Nothing
        Me.lkpEstado.Location = New System.Drawing.Point(92, 85)
        Me.lkpEstado.MultiSelect = False
        Me.lkpEstado.Name = "lkpEstado"
        Me.lkpEstado.NullText = ""
        Me.lkpEstado.PopupWidth = CType(400, Long)
        Me.lkpEstado.Required = False
        Me.lkpEstado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpEstado.SearchMember = ""
        Me.lkpEstado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpEstado.SelectAll = False
        Me.lkpEstado.Size = New System.Drawing.Size(300, 22)
        Me.lkpEstado.TabIndex = 5
        Me.lkpEstado.Tag = "estado"
        Me.lkpEstado.ToolTip = Nothing
        Me.lkpEstado.ValueMember = "estado"
        '
        'frmCiudades
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(402, 136)
        Me.Controls.Add(Me.lkpEstado)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lkpMunicipio)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.clcCiudad)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmCiudades"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcCiudad, 0)
        Me.Controls.SetChildIndex(Me.lblCiudad, 0)
        Me.Controls.SetChildIndex(Me.lkpMunicipio, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblEstado, 0)
        Me.Controls.SetChildIndex(Me.lkpEstado, 0)
        CType(Me.clcCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oCiudades As VillarrealBusiness.clsCiudades
    Private oMunicipios As VillarrealBusiness.clsMunicipios
    Private oEstados As VillarrealBusiness.clsEstados

    Private ReadOnly Property Municipio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpMunicipio)
        End Get
    End Property
    Private ReadOnly Property Estado() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpEstado)
        End Get
    End Property


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
	Private Sub frmCiudades_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
		Select Case Action
		Case Actions.Insert
						Response = oCiudades.Insertar(Me.DataSource)

            Case Actions.Update

                Response = oCiudades.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oCiudades.Eliminar(lkpEstado.EditValue, lkpMunicipio.EditValue, clcCiudad.Value)

        End Select
    End Sub

    Private Sub frmCiudades_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oCiudades.DespliegaDatos(OwnerForm.Value("estado"), OwnerForm.Value("municipio"), OwnerForm.Value("ciudad"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If

    End Sub

    Private Sub frmCiudades_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oCiudades = New VillarrealBusiness.clsCiudades
        oMunicipios = New VillarrealBusiness.clsMunicipios
        oEstados = New VillarrealBusiness.clsEstados

        'Me.lkpEstado_LoadData(True)
        'Me.lkpMunicipio_LoadData(True)

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
                Me.lkpEstado.Enabled = False
                Me.lkpMunicipio.Enabled = False
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmCiudades_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oCiudades.Validacion(Action, Me.txtDescripcion.Text, Estado, Municipio)
    End Sub

    Private Sub frmCiudades_Localize() Handles MyBase.Localize
        Find("ciudad", CType(Me.clcCiudad.EditValue, Object))
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpEstado_Format() Handles lkpEstado.Format
        ModFormatos.for_estado_grl(Me.lkpEstado)
        'Comunes.clsFormato.for_estado_grl(Me.lkpEstado)
    End Sub
    Private Sub lkpEstado_LoadData(ByVal Initialize As Boolean) Handles lkpEstado.LoadData

        Dim Response As New Events
        Response = oEstados.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpEstado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

    Private Sub lkpEstado_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpEstado.EditValueChanged
        Me.lkpMunicipio.EditValue = Nothing
        Me.lkpMunicipio_LoadData(True)
    End Sub

    Private Sub lkpMunicipio_Format() Handles lkpMunicipio.Format
        ModFormatos.for_municipio_grl(Me.lkpMunicipio)
        'Comunes.clsFormato.for_estado_grl(Me.lkpEstado)
    End Sub

    Private Sub lkpMunicipio_LoadData(ByVal Initialize As Boolean) Handles lkpMunicipio.LoadData

        Dim Response As New Events
        Response = oMunicipios.Lookup(Estado)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpMunicipio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"


#End Region

    
End Class
