Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmMensajeDejado
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblDescTipoLLamada As System.Windows.Forms.Label
    Friend WithEvents lblTipo_Llamada As System.Windows.Forms.Label
    Friend WithEvents lkpTipo_Llamada As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcMensaje As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMensajeDejado))
        Me.txtDescripcion = New DevExpress.XtraEditors.MemoEdit
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.clcMensaje = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.lblDescTipoLLamada = New System.Windows.Forms.Label
        Me.lblTipo_Llamada = New System.Windows.Forms.Label
        Me.lkpTipo_Llamada = New Dipros.Editors.TINMultiLookup
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMensaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(171, 28)
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(88, 112)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 200
        Me.txtDescripcion.Size = New System.Drawing.Size(264, 88)
        Me.txtDescripcion.TabIndex = 7
        Me.txtDescripcion.Tag = "descripcion"
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(24, 48)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenio.TabIndex = 4
        Me.lblConvenio.Tag = ""
        Me.lblConvenio.Text = "Convenio:"
        Me.lblConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcMensaje
        '
        Me.clcMensaje.EditValue = "0"
        Me.clcMensaje.Location = New System.Drawing.Point(88, 48)
        Me.clcMensaje.MaxValue = 0
        Me.clcMensaje.MinValue = 0
        Me.clcMensaje.Name = "clcMensaje"
        '
        'clcMensaje.Properties
        '
        Me.clcMensaje.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMensaje.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMensaje.Properties.Enabled = False
        Me.clcMensaje.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcMensaje.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcMensaje.Size = New System.Drawing.Size(264, 19)
        Me.clcMensaje.TabIndex = 5
        Me.clcMensaje.Tag = "mensaje"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(16, 112)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 6
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "Descripcion:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescTipoLLamada
        '
        Me.lblDescTipoLLamada.Location = New System.Drawing.Point(8, 136)
        Me.lblDescTipoLLamada.Name = "lblDescTipoLLamada"
        Me.lblDescTipoLLamada.Size = New System.Drawing.Size(264, 24)
        Me.lblDescTipoLLamada.TabIndex = 63
        Me.lblDescTipoLLamada.Tag = "descripcion_tipo_llamada"
        Me.lblDescTipoLLamada.Text = "descripcion_tipo_llamada"
        Me.lblDescTipoLLamada.Visible = False
        '
        'lblTipo_Llamada
        '
        Me.lblTipo_Llamada.AutoSize = True
        Me.lblTipo_Llamada.Location = New System.Drawing.Point(8, 80)
        Me.lblTipo_Llamada.Name = "lblTipo_Llamada"
        Me.lblTipo_Llamada.Size = New System.Drawing.Size(80, 16)
        Me.lblTipo_Llamada.TabIndex = 61
        Me.lblTipo_Llamada.Tag = ""
        Me.lblTipo_Llamada.Text = "&Tipo llamada:"
        Me.lblTipo_Llamada.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpTipo_Llamada
        '
        Me.lkpTipo_Llamada.AllowAdd = False
        Me.lkpTipo_Llamada.AutoReaload = False
        Me.lkpTipo_Llamada.DataSource = Nothing
        Me.lkpTipo_Llamada.DefaultSearchField = ""
        Me.lkpTipo_Llamada.DisplayMember = "descripcion"
        Me.lkpTipo_Llamada.EditValue = Nothing
        Me.lkpTipo_Llamada.Filtered = False
        Me.lkpTipo_Llamada.InitValue = Nothing
        Me.lkpTipo_Llamada.Location = New System.Drawing.Point(88, 80)
        Me.lkpTipo_Llamada.MultiSelect = False
        Me.lkpTipo_Llamada.Name = "lkpTipo_Llamada"
        Me.lkpTipo_Llamada.NullText = ""
        Me.lkpTipo_Llamada.PopupWidth = CType(400, Long)
        Me.lkpTipo_Llamada.ReadOnlyControl = False
        Me.lkpTipo_Llamada.Required = False
        Me.lkpTipo_Llamada.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpTipo_Llamada.SearchMember = ""
        Me.lkpTipo_Llamada.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpTipo_Llamada.SelectAll = False
        Me.lkpTipo_Llamada.Size = New System.Drawing.Size(264, 20)
        Me.lkpTipo_Llamada.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpTipo_Llamada.TabIndex = 62
        Me.lkpTipo_Llamada.Tag = "llamada"
        Me.lkpTipo_Llamada.ToolTip = "tipo de llamada"
        Me.lkpTipo_Llamada.ValueMember = "tipo_llamada"
        '
        'frmMensajeDejado
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(370, 211)
        Me.Controls.Add(Me.lblTipo_Llamada)
        Me.Controls.Add(Me.lkpTipo_Llamada)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblConvenio)
        Me.Controls.Add(Me.clcMensaje)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblDescTipoLLamada)
        Me.Name = "frmMensajeDejado"
        Me.Text = "frmMensajeDejado"
        Me.Controls.SetChildIndex(Me.lblDescTipoLLamada, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcMensaje, 0)
        Me.Controls.SetChildIndex(Me.lblConvenio, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lkpTipo_Llamada, 0)
        Me.Controls.SetChildIndex(Me.lblTipo_Llamada, 0)
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMensaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oMensajeD As VillarrealBusiness.clsMensajeDejado
    Private oTipoLlamada As New VillarrealBusiness.clsTiposLlamadas

    Public ReadOnly Property tipo_llamada() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpTipo_Llamada)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    

    Private Sub frmMensajes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oMensajeD.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oMensajeD.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oMensajeD.Eliminar(Me.clcMensaje.Value)

        End Select
    End Sub

    Private Sub frmMensajes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oMensajeD.DespliegaDatos(OwnerForm.Value("mensaje_dejado"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If
    End Sub


    Private Sub frmMensajes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oMensajeD.Validacion(Action, txtDescripcion.Text)
    End Sub

    Private Sub frmMensajes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oMensajeD = New VillarrealBusiness.clsMensajeDejado

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de la Controles"
    Private Sub lkpTipo_Llamada_Format() Handles lkpTipo_Llamada.Format
        Comunes.clsFormato.for_tipos_llamadas_grl(Me.lkpTipo_Llamada)
    End Sub
    Private Sub lkpTipo_Llamada_LoadData(ByVal Initialize As Boolean) Handles lkpTipo_Llamada.LoadData
        Dim Response As New Events
        Response = oTipoLlamada.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpTipo_Llamada.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpTipo_Llamada_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpTipo_Llamada.EditValueChanged
        If tipo_llamada > 0 Then
            Me.lblDescTipoLLamada.Text = Me.lkpTipo_Llamada.DisplayText
        End If
    End Sub
#End Region

End Class
