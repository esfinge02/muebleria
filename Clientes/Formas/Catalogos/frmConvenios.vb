Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmConvenios
    Inherits Dipros.Windows.frmTINForm


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents clcConvenio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtNombre As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConvenios))
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.clcConvenio = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtNombre = New DevExpress.XtraEditors.MemoEdit
        CType(Me.clcConvenio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(385, 28)
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(25, 40)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenio.TabIndex = 0
        Me.lblConvenio.Tag = ""
        Me.lblConvenio.Text = "Convenio:"
        Me.lblConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcConvenio
        '
        Me.clcConvenio.EditValue = "0"
        Me.clcConvenio.Location = New System.Drawing.Point(88, 40)
        Me.clcConvenio.MaxValue = 0
        Me.clcConvenio.MinValue = 0
        Me.clcConvenio.Name = "clcConvenio"
        '
        'clcConvenio.Properties
        '
        Me.clcConvenio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcConvenio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcConvenio.Properties.Enabled = False
        Me.clcConvenio.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcConvenio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcConvenio.Size = New System.Drawing.Size(72, 19)
        Me.clcConvenio.TabIndex = 1
        Me.clcConvenio.Tag = "convenio"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(32, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(53, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "Nombre:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(88, 64)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 200
        Me.txtNombre.Size = New System.Drawing.Size(344, 96)
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.Tag = "nombre"
        '
        'frmConvenios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(442, 172)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblConvenio)
        Me.Controls.Add(Me.clcConvenio)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Name = "frmConvenios"
        Me.Text = "frmConvenios"
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenio, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        CType(Me.clcConvenio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region



#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oConvenios As VillarrealBusiness.clsConvenios
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmConvenios_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oConvenios.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oConvenios.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oConvenios.Eliminar(Me.clcConvenio.Value)

        End Select
    End Sub

    Private Sub frmConvenios_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oConvenios.DespliegaDatos(OwnerForm.Value("convenio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
        End If
    End Sub


    Private Sub frmConvenios_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oConvenios.Validacion(Action, txtNombre.Text)
    End Sub

    Private Sub frmConvenios_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oConvenios = New VillarrealBusiness.clsConvenios

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub



    Private Sub frmConvenios_Localize() Handles MyBase.Localize
        Find("convenio", Me.clcConvenio.EditValue)
    End Sub

#End Region

End Class
