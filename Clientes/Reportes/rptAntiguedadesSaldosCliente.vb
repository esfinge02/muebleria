Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptAntiguedadesSaldosCliente
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphTipo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfTipo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblOrdenado As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoActual As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoVencido As DataDynamics.ActiveReports.Label = Nothing
	Private lbl1_30 As DataDynamics.ActiveReports.Label = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblNombre As DataDynamics.ActiveReports.Label = Nothing
	Private lbl_46_60 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private lbl_31_45 As DataDynamics.ActiveReports.Label = Nothing
	Private lbl61 As DataDynamics.ActiveReports.Label = Nothing
    Private lblultimo_abono As DataDynamics.ActiveReports.Label = Nothing
	Private ultima_llamada As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNombreArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoVencido As DataDynamics.ActiveReports.TextBox = Nothing
	Private txt1_30 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txt_46_60 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txt_31_45 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s46_601 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Ultimo_abono As DataDynamics.ActiveReports.TextBox = Nothing
	Private ultimo_llamada As DataDynamics.ActiveReports.TextBox = Nothing
	Private por_aplicar As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private saldo_actual2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo_vencido2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s1_302 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s31_452 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s46_603 As DataDynamics.ActiveReports.TextBox = Nothing
	Private s61_mas2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptAntiguedadesSaldosCliente.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphTipo = CType(Me.Sections("gphTipo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfTipo = CType(Me.Sections("gpfTipo"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape1 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblOrdenado = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblSaldoActual = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblSaldoVencido = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lbl1_30 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblCliente = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblNombre = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lbl_46_60 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Line)
		Me.lbl_31_45 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lbl61 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
        Me.lblultimo_abono = CType(Me.PageHeader.Controls(17), DataDynamics.ActiveReports.Label)
		Me.ultima_llamada = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.txtNombreArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoVencido = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txt1_30 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtCliente = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtFecha = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txt_46_60 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txt_31_45 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.s46_601 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Ultimo_abono = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.ultimo_llamada = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.por_aplicar = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.Label28 = CType(Me.gpfTipo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.saldo_actual2 = CType(Me.gpfTipo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.saldo_vencido2 = CType(Me.gpfTipo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.s1_302 = CType(Me.gpfTipo.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.s31_452 = CType(Me.gpfTipo.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.s46_603 = CType(Me.gpfTipo.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.s61_mas2 = CType(Me.gpfTipo.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptAntiguedadesSaldosCliente_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

    Private Sub PageHeader_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Format

    End Sub
End Class
