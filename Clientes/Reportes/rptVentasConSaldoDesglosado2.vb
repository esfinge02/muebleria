Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptVentasConSaldoDesglosado
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader3 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader4 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader5 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader6 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter6 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter5 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter4 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter3 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCobrador As DataDynamics.ActiveReports.Label = Nothing
	Private lblNumeroAbonos As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldosVencidos As DataDynamics.ActiveReports.Label = Nothing
	Private txtSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private cobrador1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtsucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private txtnombre_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private serie2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private tiene_saldos_vencidos1 As DataDynamics.ActiveReports.CheckBox = Nothing
	Private factor_enajenacion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe_proporcional_costo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private serie1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private folio1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line11 As DataDynamics.ActiveReports.Line = Nothing
	Private numero_abonos1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_cliente2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line14 As DataDynamics.ActiveReports.Line = Nothing
	Private cliente3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cobrador2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private documento_referencia3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_vencimiento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe_documento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo_documento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private documento_referencia4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line13 As DataDynamics.ActiveReports.Line = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line15 As DataDynamics.ActiveReports.Line = Nothing
	Private saldo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line6 As DataDynamics.ActiveReports.Line = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox9 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo_documento2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line16 As DataDynamics.ActiveReports.Line = Nothing
	Private vencer2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line8 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptVentasConSaldoDesglosado.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader3 = CType(Me.Sections("GroupHeader3"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader4 = CType(Me.Sections("GroupHeader4"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader5 = CType(Me.Sections("GroupHeader5"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader6 = CType(Me.Sections("GroupHeader6"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter6 = CType(Me.Sections("GroupFooter6"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter5 = CType(Me.Sections("GroupFooter5"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter4 = CType(Me.Sections("GroupFooter4"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter3 = CType(Me.Sections("GroupFooter3"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.lblVenta = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblCobrador = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblNumeroAbonos = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblSaldosVencidos = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtSaldo = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label26 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblCliente = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label32 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label33 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Label34 = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.cobrador1 = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtsucursal = CType(Me.GroupHeader3.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line3 = CType(Me.GroupHeader3.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtnombre_sucursal = CType(Me.GroupHeader3.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.serie2 = CType(Me.GroupHeader5.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.TextBox5 = CType(Me.GroupHeader5.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.tiene_saldos_vencidos1 = CType(Me.GroupHeader6.Controls(0),DataDynamics.ActiveReports.CheckBox)
		Me.factor_enajenacion1 = CType(Me.GroupHeader6.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.importe_proporcional_costo1 = CType(Me.GroupHeader6.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.saldo8 = CType(Me.GroupHeader6.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.serie1 = CType(Me.GroupHeader6.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.folio1 = CType(Me.GroupHeader6.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Line11 = CType(Me.GroupHeader6.Controls(6),DataDynamics.ActiveReports.Line)
		Me.numero_abonos1 = CType(Me.GroupHeader6.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.nombre_cliente2 = CType(Me.GroupHeader6.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.Line14 = CType(Me.GroupHeader6.Controls(9),DataDynamics.ActiveReports.Line)
		Me.cliente3 = CType(Me.GroupHeader6.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.cobrador2 = CType(Me.GroupHeader6.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.documento_referencia3 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.fecha_vencimiento1 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.importe_documento1 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.saldo_documento1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.documento_referencia4 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.TextBox6 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.TextBox7 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.TextBox8 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Line13 = CType(Me.GroupFooter4.Controls(0),DataDynamics.ActiveReports.Line)
		Me.TextBox3 = CType(Me.GroupFooter4.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.saldo6 = CType(Me.GroupFooter4.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.TextBox4 = CType(Me.GroupFooter4.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.saldo7 = CType(Me.GroupFooter4.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line15 = CType(Me.GroupFooter4.Controls(5),DataDynamics.ActiveReports.Line)
		Me.saldo1 = CType(Me.GroupFooter3.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line6 = CType(Me.GroupFooter3.Controls(1),DataDynamics.ActiveReports.Line)
		Me.TextBox1 = CType(Me.GroupFooter3.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.TextBox9 = CType(Me.GroupFooter3.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.saldo_documento2 = CType(Me.GroupFooter3.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line16 = CType(Me.GroupFooter3.Controls(5),DataDynamics.ActiveReports.Line)
		Me.vencer2 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.saldo2 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line8 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region


    Private Sub rptVentasConSaldoDesglosado_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
