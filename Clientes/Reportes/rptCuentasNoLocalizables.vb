Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptCuentasNoLocalizables
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As PageHeader = Nothing
    Private WithEvents GroupHeader1 As GroupHeader = Nothing
    Private WithEvents GroupHeader3 As GroupHeader = Nothing
    Private WithEvents Detail As Detail = Nothing
    Private WithEvents GroupFooter3 As GroupFooter = Nothing
    Private WithEvents GroupFooter1 As GroupFooter = Nothing
    Private WithEvents PageFooter As PageFooter = Nothing
	Private txtEmpresa As TextBox = Nothing
	Private txtDireccionEmpresa As TextBox = Nothing
	Private txtTelefonosEmpresa As TextBox = Nothing
	Private lblTitulo As Label = Nothing
	Private picLogotipo As Picture = Nothing
	Private lblCliente As Label = Nothing
	Private lblMotivosNoLocalizable As Label = Nothing
	Private lblUsuarioNoLocalizable As Label = Nothing
	Private txtSaldo As Label = Nothing
	Private lblFechas As Label = Nothing
	Private Label2 As Label = Nothing
	Private cobrador1 As TextBox = Nothing
	Private txtsucursal As TextBox = Nothing
	Private Line3 As Line = Nothing
	Private txtnombre_sucursal As TextBox = Nothing
	Private txtcliente As TextBox = Nothing
	Private txtSaldoActual As TextBox = Nothing
	Private txtNombreCliente As TextBox = Nothing
	Private txtMotivosNoLocalizable As TextBox = Nothing
	Private Line7 As Line = Nothing
	Private txtusuario_no_localizable As TextBox = Nothing
	Private fecha_actual1 As Label = Nothing
	Private Label25 As Label = Nothing
	Private txtPaginasReporte As TextBox = Nothing
	Private lblDeReporte As Label = Nothing
	Private txtPaginaReporte As TextBox = Nothing
	Private Line As Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptCuentasNoLocalizables.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader3 = CType(Me.Sections("GroupHeader3"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter3 = CType(Me.Sections("GroupFooter3"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.lblCliente = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblMotivosNoLocalizable = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblUsuarioNoLocalizable = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtSaldo = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.cobrador1 = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtsucursal = CType(Me.GroupHeader3.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line3 = CType(Me.GroupHeader3.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtnombre_sucursal = CType(Me.GroupHeader3.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtcliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoActual = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreCliente = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtMotivosNoLocalizable = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line7 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Line)
		Me.txtusuario_no_localizable = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

    Private Sub rptCuentasNoLocalizables_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        Dim RESPONSE As Dipros.Utils.Events

        RESPONSE = oReportes.DatosEmpresa()

        If RESPONSE.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = RESPONSE.Value
            Me.txtEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = oDataSet.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(oDataSet.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
        RESPONSE = Nothing
        oReportes = Nothing
    End Sub

End Class
