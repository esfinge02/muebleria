Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptVentasConSaldo
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader3 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter3 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblCobrador As DataDynamics.ActiveReports.Label = Nothing
	Private lblNumeroAbonos As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldosVencidos As DataDynamics.ActiveReports.Label = Nothing
	Private txtSaldo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private cobrador1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtsucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private txtnombre_sucursal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtserie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcobrador As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoActual As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreCobrador As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNumeroAbonos As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line7 As DataDynamics.ActiveReports.Line = Nothing
	Public txtTieneSaldosVencidos As DataDynamics.ActiveReports.CheckBox = Nothing
	Private txtFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line9 As DataDynamics.ActiveReports.Line = Nothing
	Private txtFactorCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private cobrador2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line6 As DataDynamics.ActiveReports.Line = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private vencer2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line8 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptVentasConSaldo.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.GroupHeader3 = CType(Me.Sections("GroupHeader3"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter3 = CType(Me.Sections("GroupFooter3"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.lblVenta = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblCobrador = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblNumeroAbonos = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblSaldosVencidos = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtSaldo = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label26 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.cobrador1 = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtsucursal = CType(Me.GroupHeader3.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line3 = CType(Me.GroupHeader3.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtnombre_sucursal = CType(Me.GroupHeader3.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtserie = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtcobrador = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoActual = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreCobrador = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtNumeroAbonos = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line7 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Line)
		Me.txtTieneSaldosVencidos = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.CheckBox)
		Me.txtFolio = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.Line9 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Line)
		Me.txtFactorCosto = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.cobrador2 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.saldo1 = CType(Me.GroupFooter3.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line6 = CType(Me.GroupFooter3.Controls(1),DataDynamics.ActiveReports.Line)
		Me.TextBox1 = CType(Me.GroupFooter3.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.vencer2 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.saldo2 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line8 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptVentasConSaldo_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
