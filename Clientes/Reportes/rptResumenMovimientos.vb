Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptResumenMovimientos
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolio As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblObservaciones As DataDynamics.ActiveReports.Label = Nothing
	Private lblCosto As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private txtconcepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtdescripcion As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line9 As DataDynamics.ActiveReports.Line = Nothing
	Private txtNoCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonos As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtObservaciones As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private anio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSerie As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalImporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line8 As DataDynamics.ActiveReports.Line = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line10 As DataDynamics.ActiveReports.Line = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptResumenMovimientos.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader2 = CType(Me.Sections("GroupHeader2"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter2 = CType(Me.Sections("GroupFooter2"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.lblCliente = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblFecha = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblFolio = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblObservaciones = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblCosto = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label26 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.txtconcepto = CType(Me.GroupHeader2.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtdescripcion = CType(Me.GroupHeader2.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line9 = CType(Me.GroupHeader2.Controls(2),DataDynamics.ActiveReports.Line)
		Me.txtNoCliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreCliente = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonos = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtImporte = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtObservaciones = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.anio = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtSerie = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.GroupFooter2.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalImporte = CType(Me.GroupFooter2.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line8 = CType(Me.GroupFooter2.Controls(2),DataDynamics.ActiveReports.Line)
		Me.importe1 = CType(Me.GroupFooter2.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line10 = CType(Me.GroupFooter2.Controls(4),DataDynamics.ActiveReports.Line)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region


    Private Sub rptResumenMovimientos_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub

    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format

        If CType(Me.Fields("estatus").Value, String).ToUpper = "C" Then

            Detail.BackColor = System.Drawing.Color.FromName("Control")
        Else
            Detail.BackColor = System.Drawing.Color.White
        End If

    End Sub
End Class
