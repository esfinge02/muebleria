Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptAcumuladoClientes
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblDomicilio As DataDynamics.ActiveReports.Label = Nothing
	Private lblRFC As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private lblColonia As DataDynamics.ActiveReports.Label = Nothing
	Private lblCiudad As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private lblEstado As DataDynamics.ActiveReports.Label = Nothing
	Private lblNombre As DataDynamics.ActiveReports.Label = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private lblApellidoPaterno As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private lblCurp As DataDynamics.ActiveReports.Label = Nothing
	Private txtNoCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtRFC As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private txtDomicilio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtColonia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCiudad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtNombreEstado As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCp As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtApellidoPaterno As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtApellidoMaterno As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCurp As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtSaldoFinal As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblGranTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptAcumuladoClientes.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblCliente = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblDomicilio = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblRFC = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblColonia = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblCiudad = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblEstado = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblNombre = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label27 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblApellidoPaterno = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblCurp = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.txtNoCliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreCliente = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtRFC = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Line)
		Me.txtDomicilio = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtColonia = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtCiudad = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtNombreEstado = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtCp = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtApellidoPaterno = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtApellidoMaterno = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.txtCurp = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Line)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.txtSaldoFinal = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblGranTotal = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format

    End Sub

    Private Sub rptAcumuladoClientes_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
