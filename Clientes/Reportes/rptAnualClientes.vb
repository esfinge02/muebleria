Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptAnualClientes
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghPagina As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotal As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblVence As DataDynamics.ActiveReports.Label = Nothing
	Private lblCobrador As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_4 As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_3 As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_2 As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_5 As DataDynamics.ActiveReports.Label = Nothing
	Private n_col_6 As DataDynamics.ActiveReports.Label = Nothing
	Private txtNoCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private txtNombreCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVence As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private ttotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line9 As DataDynamics.ActiveReports.Line = Nothing
	Private Label26 As DataDynamics.ActiveReports.Label = Nothing
	Private v_col_11 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_31 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_21 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_41 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_51 As DataDynamics.ActiveReports.TextBox = Nothing
	Private v_col_61 As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_vencimiento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line10 As DataDynamics.ActiveReports.Line = Nothing
	Private Line11 As DataDynamics.ActiveReports.Line = Nothing
	Private Line12 As DataDynamics.ActiveReports.Line = Nothing
	Private Line13 As DataDynamics.ActiveReports.Line = Nothing
	Private Line14 As DataDynamics.ActiveReports.Line = Nothing
	Private Line15 As DataDynamics.ActiveReports.Line = Nothing
	Private Line16 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptAnualClientes.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghPagina = CType(Me.Sections("ghPagina"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblTitulo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.picLogotipo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Picture)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.ghPagina.Controls(0),DataDynamics.ActiveReports.Label)
		Me.lblTotal = CType(Me.ghPagina.Controls(1),DataDynamics.ActiveReports.Label)
		Me.n_col_1 = CType(Me.ghPagina.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblVence = CType(Me.ghPagina.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblCobrador = CType(Me.ghPagina.Controls(4),DataDynamics.ActiveReports.Label)
		Me.n_col_4 = CType(Me.ghPagina.Controls(5),DataDynamics.ActiveReports.Label)
		Me.n_col_3 = CType(Me.ghPagina.Controls(6),DataDynamics.ActiveReports.Label)
		Me.n_col_2 = CType(Me.ghPagina.Controls(7),DataDynamics.ActiveReports.Label)
		Me.n_col_5 = CType(Me.ghPagina.Controls(8),DataDynamics.ActiveReports.Label)
		Me.n_col_6 = CType(Me.ghPagina.Controls(9),DataDynamics.ActiveReports.Label)
		Me.txtNoCliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Line)
		Me.txtNombreCliente = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.v_col_1 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtVence = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.v_col_4 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.v_col_2 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.v_col_3 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.v_col_6 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.v_col_5 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.TextBox3 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label25 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.ttotal = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Line9 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.Line)
		Me.Label26 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.v_col_11 = CType(Me.ReportFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.v_col_31 = CType(Me.ReportFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.v_col_21 = CType(Me.ReportFooter.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.v_col_41 = CType(Me.ReportFooter.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.v_col_51 = CType(Me.ReportFooter.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.v_col_61 = CType(Me.ReportFooter.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.fecha_vencimiento1 = CType(Me.ReportFooter.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Line10 = CType(Me.ReportFooter.Controls(10),DataDynamics.ActiveReports.Line)
		Me.Line11 = CType(Me.ReportFooter.Controls(11),DataDynamics.ActiveReports.Line)
		Me.Line12 = CType(Me.ReportFooter.Controls(12),DataDynamics.ActiveReports.Line)
		Me.Line13 = CType(Me.ReportFooter.Controls(13),DataDynamics.ActiveReports.Line)
		Me.Line14 = CType(Me.ReportFooter.Controls(14),DataDynamics.ActiveReports.Line)
		Me.Line15 = CType(Me.ReportFooter.Controls(15),DataDynamics.ActiveReports.Line)
		Me.Line16 = CType(Me.ReportFooter.Controls(16),DataDynamics.ActiveReports.Line)
	End Sub

#End Region


    Private Sub rptAnualClientes_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
