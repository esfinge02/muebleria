Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptMovimientosClientes
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghCliente As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfCliente As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Shape6 As DataDynamics.ActiveReports.Shape = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private nombre_cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private domicilio_cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label32 As DataDynamics.ActiveReports.Label = Nothing
	Private ciudad_cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label33 As DataDynamics.ActiveReports.Label = Nothing
	Private telefonos_cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label34 As DataDynamics.ActiveReports.Label = Nothing
	Private rfc_cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label35 As DataDynamics.ActiveReports.Label = Nothing
	Private colonia_cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label36 As DataDynamics.ActiveReports.Label = Nothing
	Private estado_cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label38 As DataDynamics.ActiveReports.Label = Nothing
	Private Label39 As DataDynamics.ActiveReports.Label = Nothing
	Private Label40 As DataDynamics.ActiveReports.Label = Nothing
	Private Label41 As DataDynamics.ActiveReports.Label = Nothing
	Private Label42 As DataDynamics.ActiveReports.Label = Nothing
	Private Label45 As DataDynamics.ActiveReports.Label = Nothing
	Private Label46 As DataDynamics.ActiveReports.Label = Nothing
	Private cliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcionCorta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCosto As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe As DataDynamics.ActiveReports.TextBox = Nothing
	Private cargo_documento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cargo_documento2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cobrador As DataDynamics.ActiveReports.TextBox = Nothing
	Private saldo_actual1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label37 As DataDynamics.ActiveReports.Label = Nothing
	Private saldo_actual2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label43 As DataDynamics.ActiveReports.Label = Nothing
	Private vencido1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label44 As DataDynamics.ActiveReports.Label = Nothing
	Private Line18 As DataDynamics.ActiveReports.Line = Nothing
	Private Line19 As DataDynamics.ActiveReports.Line = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label25 As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptMovimientosClientes.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghCliente = CType(Me.Sections("ghCliente"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfCliente = CType(Me.Sections("gfCliente"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Shape6 = CType(Me.ghCliente.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Label30 = CType(Me.ghCliente.Controls(1),DataDynamics.ActiveReports.Label)
		Me.nombre_cliente1 = CType(Me.ghCliente.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label31 = CType(Me.ghCliente.Controls(3),DataDynamics.ActiveReports.Label)
		Me.domicilio_cliente1 = CType(Me.ghCliente.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label32 = CType(Me.ghCliente.Controls(5),DataDynamics.ActiveReports.Label)
		Me.ciudad_cliente1 = CType(Me.ghCliente.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label33 = CType(Me.ghCliente.Controls(7),DataDynamics.ActiveReports.Label)
		Me.telefonos_cliente1 = CType(Me.ghCliente.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.Label34 = CType(Me.ghCliente.Controls(9),DataDynamics.ActiveReports.Label)
		Me.rfc_cliente1 = CType(Me.ghCliente.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.Label35 = CType(Me.ghCliente.Controls(11),DataDynamics.ActiveReports.Label)
		Me.colonia_cliente1 = CType(Me.ghCliente.Controls(12),DataDynamics.ActiveReports.TextBox)
		Me.Label36 = CType(Me.ghCliente.Controls(13),DataDynamics.ActiveReports.Label)
		Me.estado_cliente1 = CType(Me.ghCliente.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.Label38 = CType(Me.ghCliente.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label39 = CType(Me.ghCliente.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label40 = CType(Me.ghCliente.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label41 = CType(Me.ghCliente.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label42 = CType(Me.ghCliente.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Label45 = CType(Me.ghCliente.Controls(20),DataDynamics.ActiveReports.Label)
		Me.Label46 = CType(Me.ghCliente.Controls(21),DataDynamics.ActiveReports.Label)
		Me.cliente = CType(Me.ghCliente.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcionCorta = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtCantidad = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtCosto = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.importe = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.cargo_documento1 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.cargo_documento2 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.cobrador = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.saldo_actual1 = CType(Me.gfCliente.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label37 = CType(Me.gfCliente.Controls(1),DataDynamics.ActiveReports.Label)
		Me.saldo_actual2 = CType(Me.gfCliente.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label43 = CType(Me.gfCliente.Controls(3),DataDynamics.ActiveReports.Label)
		Me.vencido1 = CType(Me.gfCliente.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label44 = CType(Me.gfCliente.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Line18 = CType(Me.gfCliente.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Line19 = CType(Me.gfCliente.Controls(7),DataDynamics.ActiveReports.Line)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label25 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
	End Sub

	#End Region

   
    Private Sub rptMovimientosClientes_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
