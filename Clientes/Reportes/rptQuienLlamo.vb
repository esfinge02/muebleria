Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptQuienLlamo
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghPersonaLlama As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfPersonaLlama As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblSaldoVencidoActual As DataDynamics.ActiveReports.Label = Nothing
	Private lblSaldoVencidoLlamada As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporteUltimoAbono As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaUltimoAbono As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechaPromesaPago As DataDynamics.ActiveReports.Label = Nothing
	Private lblUltimoAbonoAnterior As DataDynamics.ActiveReports.Label = Nothing
	Private lblCliente As DataDynamics.ActiveReports.Label = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private filtro2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblPersonaLlama As DataDynamics.ActiveReports.Label = Nothing
	Private fecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtClienteFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre_cliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaPromesaPago As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoVencidoLlamada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSaldoVencidoActual As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtUltimoAbonoAnterior As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFechaUltimoAbono As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteUltimoAbono As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private lblTotalClientes As DataDynamics.ActiveReports.Label = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalSaldoVenciodoLlamada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalSaldoVencidoActual As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private Line4 As DataDynamics.ActiveReports.Line = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalLlamada As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalVencido As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Clientes.rptQuienLlamo.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghPersonaLlama = CType(Me.Sections("ghPersonaLlama"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfPersonaLlama = CType(Me.Sections("gfPersonaLlama"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape1 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.lblSaldoVencidoActual = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblSaldoVencidoLlamada = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblImporteUltimoAbono = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblFechaUltimoAbono = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblFechaPromesaPago = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblUltimoAbonoAnterior = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblCliente = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblFecha = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.filtro2 = CType(Me.ghPersonaLlama.Controls(0),DataDynamics.ActiveReports.Label)
		Me.lblPersonaLlama = CType(Me.ghPersonaLlama.Controls(1),DataDynamics.ActiveReports.Label)
		Me.fecha = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtClienteFolio = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.nombre_cliente = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaPromesaPago = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoVencidoLlamada = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtSaldoVencidoActual = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtUltimoAbonoAnterior = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtFechaUltimoAbono = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteUltimoAbono = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.gfPersonaLlama.Controls(0),DataDynamics.ActiveReports.Line)
		Me.Label6 = CType(Me.gfPersonaLlama.Controls(1),DataDynamics.ActiveReports.Label)
		Me.lblTotalClientes = CType(Me.gfPersonaLlama.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.gfPersonaLlama.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtTotalSaldoVenciodoLlamada = CType(Me.gfPersonaLlama.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalSaldoVencidoActual = CType(Me.gfPersonaLlama.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Line)
		Me.Line3 = CType(Me.PageFooter.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Line4 = CType(Me.PageFooter.Controls(7),DataDynamics.ActiveReports.Line)
		Me.Label9 = CType(Me.PageFooter.Controls(8),DataDynamics.ActiveReports.Label)
		Me.txtTotalLlamada = CType(Me.PageFooter.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalVencido = CType(Me.PageFooter.Controls(10),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region


    Private Sub rptQuienLlamo_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub


End Class
