Imports Dipros.Windows.Forms

Module ModFormatos



#Region "ConceptosCuentasCobrar"

    Public Sub for_conceptos_cuentas_cobrar_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)

        AddColumns(oGrid, "Tipo", "tipo", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Concepto", "concepto", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 250)
        AddColumns(oGrid, "Tipo", "tipo_concepto", 2, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region

#Region "Ciudades"

    Public Sub for_ciudades_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Localidad", "ciudad", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Municipio", "nombre_municipio", 2, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Municipio", "municipio", -1, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Estado", "nombre_estado", 3, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oGrid, "Estado", "estado", -1, DevExpress.Utils.FormatType.None, "", 100)

    End Sub

#End Region

#Region "Colonias"

    Public Sub for_colonias_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Colonia", "colonia", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 190)
        AddColumns(oGrid, "Estado", "nombre_estado", 2, DevExpress.Utils.FormatType.None, "", 190)
        AddColumns(oGrid, "Municipio", "nombre_municipio", 3, DevExpress.Utils.FormatType.None, "", 190)
        AddColumns(oGrid, "Localidad", "nombre_ciudad", 4, DevExpress.Utils.FormatType.None, "", 190)
        AddColumns(oGrid, "C�digo Postal", "codigo_postal", 5, DevExpress.Utils.FormatType.None, "", 100)
    End Sub

#End Region

#Region "Colonias"

    Public Sub for_zonas_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Zona", "zona", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 190)

    End Sub

#End Region

#Region "Estados"

    Public Sub for_estados_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Estado", "estado", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 190)

    End Sub

    Public Sub for_estado_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Estado", "estado", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 240)

    End Sub
#End Region

#Region "Municipios"
    Public Sub for_municipio_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Municipio", "municipio", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 190)
    End Sub
#End Region



#Region "Convenios"

    Public Sub for_convenios_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Convenio", "convenio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Nombre", "nombre", 1, DevExpress.Utils.FormatType.None, "", 280)

    End Sub

#End Region

#Region "Parentesco"

    Public Sub for_parentesco_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Parentesco", "parentesco", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Descripcion", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 280)

    End Sub

#End Region


#Region "mensaje dejado"

    Public Sub for_mensaje_dejado_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "mensaje_dejado", 0, DevExpress.Utils.FormatType.Numeric, "")
        AddColumns(oGrid, "Tipo de Llamada", "desc_llamda", 1, DevExpress.Utils.FormatType.None, "", 280)
        AddColumns(oGrid, "Mensaje", "descripcion", 2, DevExpress.Utils.FormatType.None, "", 280)
    End Sub

#End Region


    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	OrdenesRecuperacion
    'DATE:		12/10/2007 0:00:00
    '----------------------------------------------------------------------------------------------------------------
#Region "OrdenesRecuperacion"

    Public Sub for_ordenes_recuperacion_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal", "sucursal", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Elabora", "elabora", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Status", "status", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Status", "descripcion_status", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodega Recibe", "bodega_recibe", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Bodega Recibe", "nombre_bodega_recibe", 5, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Persona Recibe", "persona_recibe", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Persona Recibe", "nombre_persona_recibe", 6, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha Recibe", "fecha_recibe", 7, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal Factura", "sucursal_factura", -1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Sucursal Factura", "nombre_sucursal_factura", 8, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Serie Factura", "serie_factura", 9, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Folio Factura", "folio_factura", 10, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Persona Recoje", "persona_recoje", 11, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Persona Inspecciona", "persona_inspecciona", 12, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Fecha Inspecciona", "fecha_inspecciona", 13, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Observaciones Inspecci�n", "observaciones_inspeccion", 14, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Observaciones", "observaciones", 15, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region
    'GENERATE CODE BY TINGeneraNet Ver. 3.0
    'DESCRIPTION:	OrdenesRecuperacionDetalle
    'DATE:		11/10/2007 0:00:00
    '----------------------------------------------------------------------------------------------------------------
#Region "OrdenesRecuperacionDetalle"

    Public Sub for_ordenes_recuperacion_detalle_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Folio", "folio", 0, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Art�culo", "articulo", 1, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Partida", "partida", 2, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Cantidad", "cantidad", 3, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Serie", "serie", 4, DevExpress.Utils.FormatType.None, "")
        AddColumns(oGrid, "Costo", "costo", 5, DevExpress.Utils.FormatType.None, "")

    End Sub

#End Region


#Region "Productos"

    Public Sub for_productos_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Quincena", "quincena", 0, DevExpress.Utils.FormatType.Numeric, "n0", 100)
        AddColumns(oGrid, "A�o", "anio", 1, DevExpress.Utils.FormatType.Numeric, "", 100)
        AddColumns(oGrid, "Fecha", "fecha", 2, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 200)
        AddColumns(oGrid, "Estatus", "estatus", 3, DevExpress.Utils.FormatType.None, "", 100)
        AddColumns(oGrid, "Fecha Aplicaci�n", "fecha_aplicacion", 4, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 200)


    End Sub

#End Region


End Module
