Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptSubExportarArchivoQuincenal
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private cliente1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private cliente2 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Magisterio.rptSubExportarArchivoQuincenal.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Label11 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.cliente1 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.cliente2 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region
End Class
