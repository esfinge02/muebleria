Imports Dipros.Utils.Common

Public Class frmMain
    Inherits Dipros.Windows.frmTINMain

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents mnuCatDependencias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProImportarPlantillaDependencias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuProExportarEnvios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProExportearArchivoLetrasDetalle As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuProImportarArchivo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCatClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRepMercanciaEnTransito As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.mnuCatDependencias = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProImportarPlantillaDependencias = New DevExpress.XtraBars.BarButtonItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.mnuProExportarEnvios = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProExportearArchivoLetrasDetalle = New DevExpress.XtraBars.BarButtonItem
        Me.mnuProImportarArchivo = New DevExpress.XtraBars.BarButtonItem
        Me.mnuCatClientes = New DevExpress.XtraBars.BarButtonItem
        Me.mnuRepMercanciaEnTransito = New DevExpress.XtraBars.BarButtonItem
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatDependencias), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatClientes)})
        '
        'mnuHerramientas
        '
        Me.mnuHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiCambiarUsuario), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPermisos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBitacora), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiBarra), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiTamanoIconos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiColores), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiPreferencias, True)})
        '
        'mnuArchivo
        '
        Me.mnuArchivo.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcEspecificar), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArcSalir)})
        '
        'mnuAyuda
        '
        Me.mnuAyuda.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuAcerca), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuContenido), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuInformacion, True)})
        '
        'barMainMenu
        '
        Me.barMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuArchivo), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCatalogos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProcesos), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportes), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHerramientas, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVentana), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAyuda)})
        Me.barMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.barMainMenu.OptionsBar.DisableClose = True
        Me.barMainMenu.OptionsBar.UseWholeRow = True
        '
        'mnuReportes
        '
        Me.mnuReportes.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRepMercanciaEnTransito)})
        '
        'BarManager
        '
        Me.BarManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuCatDependencias, Me.mnuProImportarPlantillaDependencias, Me.BarStaticItem1, Me.mnuProExportarEnvios, Me.mnuProExportearArchivoLetrasDetalle, Me.mnuProImportarArchivo, Me.mnuCatClientes, Me.mnuRepMercanciaEnTransito})
        Me.BarManager.MaxItemId = 114
        '
        'mnuUtiFondos
        '
        Me.mnuUtiFondos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiSinGrafico), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUtiFondo)})
        '
        'mnuVentana
        '
        Me.mnuVentana.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenCascada), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenHorizontal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenVertical), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuVenLista)})
        '
        'ilsIcons
        '
        Me.ilsIcons.ImageStream = CType(resources.GetObject("ilsIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'mnuProcesos
        '
        Me.mnuProcesos.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProImportarPlantillaDependencias), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProExportarEnvios), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuProImportarArchivo)})
        '
        'mnuCatDependencias
        '
        Me.mnuCatDependencias.Caption = "Convenios"
        Me.mnuCatDependencias.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatDependencias.Id = 105
        Me.mnuCatDependencias.ImageIndex = 0
        Me.mnuCatDependencias.Name = "mnuCatDependencias"
        '
        'mnuProImportarPlantillaDependencias
        '
        Me.mnuProImportarPlantillaDependencias.Caption = "I&mportar Plantilla"
        Me.mnuProImportarPlantillaDependencias.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProImportarPlantillaDependencias.Id = 106
        Me.mnuProImportarPlantillaDependencias.ImageIndex = 1
        Me.mnuProImportarPlantillaDependencias.Name = "mnuProImportarPlantillaDependencias"
        '
        'Bar1
        '
        Me.Bar1.BarName = "sucursal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.Bar1.Text = "sucursal"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Id = 107
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Center
        Me.BarStaticItem1.Width = 32
        '
        'mnuProExportarEnvios
        '
        Me.mnuProExportarEnvios.Caption = "&Exportar Env�os"
        Me.mnuProExportarEnvios.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProExportarEnvios.Id = 109
        Me.mnuProExportarEnvios.Name = "mnuProExportarEnvios"
        '
        'mnuProExportearArchivoLetrasDetalle
        '
        Me.mnuProExportearArchivoLetrasDetalle.Caption = "Exportar Archivo Letras Detalle"
        Me.mnuProExportearArchivoLetrasDetalle.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProExportearArchivoLetrasDetalle.Id = 110
        Me.mnuProExportearArchivoLetrasDetalle.Name = "mnuProExportearArchivoLetrasDetalle"
        '
        'mnuProImportarArchivo
        '
        Me.mnuProImportarArchivo.Caption = "&Importar Archivo"
        Me.mnuProImportarArchivo.CategoryGuid = New System.Guid("f3dd9d83-241e-4a9e-ac7e-1e96e60ea9c8")
        Me.mnuProImportarArchivo.Id = 111
        Me.mnuProImportarArchivo.Name = "mnuProImportarArchivo"
        '
        'mnuCatClientes
        '
        Me.mnuCatClientes.Caption = "C&lientes"
        Me.mnuCatClientes.CategoryGuid = New System.Guid("3142c208-de4c-4701-a0d3-16d2209f30e6")
        Me.mnuCatClientes.Id = 112
        Me.mnuCatClientes.ImageIndex = 2
        Me.mnuCatClientes.Name = "mnuCatClientes"
        '
        'mnuRepMercanciaEnTransito
        '
        Me.mnuRepMercanciaEnTransito.Caption = "Mercancia en Tr�nsito"
        Me.mnuRepMercanciaEnTransito.CategoryGuid = New System.Guid("80c64971-9814-40b9-a374-10b5798b14a4")
        Me.mnuRepMercanciaEnTransito.Id = 113
        Me.mnuRepMercanciaEnTransito.ImageIndex = 3
        Me.mnuRepMercanciaEnTransito.Name = "mnuRepMercanciaEnTransito"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(617, 366)
        Me.Company = "DIPROS Systems"
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.Text = "DIPROS Systems - Tecnolog�a .NET"
        CType(Me.BarManager, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region "Eventos de la forma"

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim osucursales As New VillarrealBusiness.clsSucursales
        Dim response As Dipros.Utils.Events
        response = osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)

        If Not response.ErrorFound Then
            Dim odataset As DataSet

            odataset = response.Value

            Me.BarStaticItem1.Caption = "Sucursal Actual: " + odataset.Tables(0).Rows(0).Item("nombre")
        End If
    End Sub

#End Region

#Region "Catalogos"
    Public Sub mnuCatDependencias_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatDependencias.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oDependencias As New VillarrealBusiness.clsDependencias
        With oGrid
            .Title = "Convenios"
            .UpdateTitle = "Un Convenio"
            .DataSource = AddressOf oDependencias.Listado
            .UpdateForm = New frmDependencias
            .Format = AddressOf for_dependencias_grs
            .MdiParent = Me
            .Width = 750
            .Show()
        End With
        oDependencias = Nothing
    End Sub


    Public Sub mnuCatClientes_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCatClientes.ItemClick
        Dim oGrid As New Dipros.Windows.frmTINGridNet(e.Item)
        Dim oClientes As New VillarrealBusiness.clsClientes
        With oGrid
            .Title = "Clientes"
            .UpdateTitle = "un Cliente"
            '.DataSource = AddressOf oClientes.Listado()
            .DataSource = AddressOf oClientes.Listado
            .UpdateForm = New Comunes.frmClientes
            .Format = AddressOf Comunes.clsFormato.for_clientes_grs
            .MdiParent = Me
            .Show()
        End With
        oClientes = Nothing
    End Sub
#End Region

#Region "Procesos"
    Public Sub mnuProImportarPlantillaDependencias_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProImportarPlantillaDependencias.ItemClick
        Dim oform As New frmImportarPlantilla
        With oform
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Importar Plantilla"
            .Action = Dipros.Utils.Common.Actions.Accept
            .Show()
        End With
    End Sub

    Public Sub mnuProExportarEnvios_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProExportarEnvios.ItemClick
        Dim oform As New frmExportarArchivo
        With oform
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Exportar Archivo"
            .Action = Dipros.Utils.Common.Actions.Accept
            .Show()
        End With
    End Sub
    Public Sub mnuProImportarArchivo_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuProImportarArchivo.ItemClick
        Dim oform As New frmImportarArchivo
        With oform
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Importar Archivo"
            .Action = Dipros.Utils.Common.Actions.Accept
            .Show()
        End With
    End Sub

#End Region

#Region "Reportes"
    Public Sub mnuRepMercanciaEnTransito_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRepMercanciaEnTransito.ItemClick
        Dim oForm As New frmRepMercanciaEnTransito
        With oForm
            .MdiParent = Me
            .MenuOption = e.Item
            .Title = "Mercancia en Transito"
            .Action = Actions.Report
            .Show()
        End With
    End Sub
#End Region


End Class
