Imports Dipros.Utils.Common
Imports Dipros.Utils

Public Class frmDependencias
	Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
	Dim KS As Keys
#End Region

#Region " Código generado por el Diseñador de Windows Forms "
	Public Sub New()
		MyBase.New()
		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()
		'Agregar cualquier inicialización después de la llamada a InitializeComponent()
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
        	MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer
	'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Diseñador de Windows Forms. 
	'No lo modifique con el editor de código. 
		Friend WithEvents lblDependencia As System.Windows.Forms.Label
		Friend WithEvents clcDependencia As Dipros.Editors.TINCalcEdit 
		Friend WithEvents lblDescripcion As System.Windows.Forms.Label
		Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
		Friend WithEvents lblConcepto_Deduccion As System.Windows.Forms.Label
		Friend WithEvents txtConcepto_Deduccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtConsulta As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grvCampos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcCampo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcLongitud As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cboTipo As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents grCampos As DevExpress.XtraGrid.GridControl
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcLongitud As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents grcSeleccionar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit

		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDependencias))
        Me.lblDependencia = New System.Windows.Forms.Label
        Me.clcDependencia = New Dipros.Editors.TINCalcEdit
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.lblConcepto_Deduccion = New System.Windows.Forms.Label
        Me.txtConcepto_Deduccion = New DevExpress.XtraEditors.TextEdit
        Me.txtConsulta = New DevExpress.XtraEditors.MemoEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.grCampos = New DevExpress.XtraGrid.GridControl
        Me.grvCampos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSeleccionar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcCampo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.cboTipo = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.grcLongitud = New DevExpress.XtraGrid.Columns.GridColumn
        Me.clcLongitud = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.clcDependencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto_Deduccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConsulta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grCampos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvCampos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcLongitud, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(790, 28)
        '
        'lblDependencia
        '
        Me.lblDependencia.AutoSize = True
        Me.lblDependencia.Location = New System.Drawing.Point(82, 40)
        Me.lblDependencia.Name = "lblDependencia"
        Me.lblDependencia.Size = New System.Drawing.Size(66, 16)
        Me.lblDependencia.TabIndex = 0
        Me.lblDependencia.Tag = ""
        Me.lblDependencia.Text = "Con&venios:"
        Me.lblDependencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDependencia
        '
        Me.clcDependencia.EditValue = "0"
        Me.clcDependencia.Location = New System.Drawing.Point(152, 39)
        Me.clcDependencia.MaxValue = 0
        Me.clcDependencia.MinValue = 0
        Me.clcDependencia.Name = "clcDependencia"
        '
        'clcDependencia.Properties
        '
        Me.clcDependencia.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcDependencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDependencia.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcDependencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDependencia.Properties.Enabled = False
        Me.clcDependencia.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcDependencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDependencia.Size = New System.Drawing.Size(64, 21)
        Me.clcDependencia.TabIndex = 1
        Me.clcDependencia.Tag = "dependencia"
        Me.clcDependencia.ToolTip = "Clave de Dependencia"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(76, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "D&escripción:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(152, 62)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 50
        Me.txtDescripcion.Size = New System.Drawing.Size(328, 22)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "descripcion"
        Me.txtDescripcion.ToolTip = "Descripción de la Dependencia"
        '
        'lblConcepto_Deduccion
        '
        Me.lblConcepto_Deduccion.AutoSize = True
        Me.lblConcepto_Deduccion.Location = New System.Drawing.Point(10, 88)
        Me.lblConcepto_Deduccion.Name = "lblConcepto_Deduccion"
        Me.lblConcepto_Deduccion.Size = New System.Drawing.Size(138, 16)
        Me.lblConcepto_Deduccion.TabIndex = 4
        Me.lblConcepto_Deduccion.Tag = ""
        Me.lblConcepto_Deduccion.Text = "C&oncepto de Deducción:"
        Me.lblConcepto_Deduccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConcepto_Deduccion
        '
        Me.txtConcepto_Deduccion.EditValue = ""
        Me.txtConcepto_Deduccion.Location = New System.Drawing.Point(152, 86)
        Me.txtConcepto_Deduccion.Name = "txtConcepto_Deduccion"
        '
        'txtConcepto_Deduccion.Properties
        '
        Me.txtConcepto_Deduccion.Properties.MaxLength = 20
        Me.txtConcepto_Deduccion.Size = New System.Drawing.Size(128, 22)
        Me.txtConcepto_Deduccion.TabIndex = 5
        Me.txtConcepto_Deduccion.Tag = "concepto_deduccion"
        Me.txtConcepto_Deduccion.ToolTip = "Concepto de Deducción de la Dependencia"
        '
        'txtConsulta
        '
        Me.txtConsulta.EditValue = ""
        Me.txtConsulta.Location = New System.Drawing.Point(152, 110)
        Me.txtConsulta.Name = "txtConsulta"
        '
        'txtConsulta.Properties
        '
        Me.txtConsulta.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtConsulta.Size = New System.Drawing.Size(328, 38)
        Me.txtConsulta.TabIndex = 7
        Me.txtConsulta.Tag = "consulta"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(91, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Tag = ""
        Me.Label2.Text = "Co&nsulta:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grCampos
        '
        '
        'grCampos.EmbeddedNavigator
        '
        Me.grCampos.EmbeddedNavigator.Name = ""
        Me.grCampos.Location = New System.Drawing.Point(8, 176)
        Me.grCampos.MainView = Me.grvCampos
        Me.grCampos.Name = "grCampos"
        Me.grCampos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.cboTipo, Me.clcLongitud, Me.chkSeleccionar})
        Me.grCampos.Size = New System.Drawing.Size(472, 144)
        Me.grCampos.TabIndex = 9
        Me.grCampos.Text = "Campos"
        '
        'grvCampos
        '
        Me.grvCampos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSeleccionar, Me.grcCampo, Me.grcTipo, Me.grcLongitud})
        Me.grvCampos.GridControl = Me.grCampos
        Me.grvCampos.Name = "grvCampos"
        Me.grvCampos.OptionsCustomization.AllowFilter = False
        Me.grvCampos.OptionsCustomization.AllowGroup = False
        Me.grvCampos.OptionsCustomization.AllowSort = False
        Me.grvCampos.OptionsView.ShowGroupPanel = False
        Me.grvCampos.OptionsView.ShowIndicator = False
        '
        'grcSeleccionar
        '
        Me.grcSeleccionar.Caption = "Seleccionar"
        Me.grcSeleccionar.ColumnEdit = Me.chkSeleccionar
        Me.grcSeleccionar.FieldName = "seleccionar"
        Me.grcSeleccionar.Name = "grcSeleccionar"
        Me.grcSeleccionar.VisibleIndex = 0
        Me.grcSeleccionar.Width = 67
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'grcCampo
        '
        Me.grcCampo.Caption = "Campo"
        Me.grcCampo.FieldName = "campo"
        Me.grcCampo.Name = "grcCampo"
        Me.grcCampo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCampo.VisibleIndex = 1
        Me.grcCampo.Width = 100
        '
        'grcTipo
        '
        Me.grcTipo.Caption = "Tipo"
        Me.grcTipo.ColumnEdit = Me.cboTipo
        Me.grcTipo.FieldName = "tipo"
        Me.grcTipo.Name = "grcTipo"
        Me.grcTipo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipo.VisibleIndex = 2
        Me.grcTipo.Width = 153
        '
        'cboTipo
        '
        Me.cboTipo.AutoHeight = False
        Me.cboTipo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Items.AddRange(New Object() {"bit", "char", "datetime", "int", "money", "numeric", "text", "varchar"})
        Me.cboTipo.Name = "cboTipo"
        Me.cboTipo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'grcLongitud
        '
        Me.grcLongitud.Caption = "Longitud"
        Me.grcLongitud.ColumnEdit = Me.clcLongitud
        Me.grcLongitud.FieldName = "longitud"
        Me.grcLongitud.Name = "grcLongitud"
        Me.grcLongitud.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcLongitud.VisibleIndex = 3
        '
        'clcLongitud
        '
        Me.clcLongitud.AutoHeight = False
        Me.clcLongitud.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, False, False, False, DevExpress.Utils.HorzAlignment.Center, Nothing)})
        Me.clcLongitud.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLongitud.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcLongitud.Name = "clcLongitud"
        Me.clcLongitud.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never
        Me.clcLongitud.ShowPopupShadow = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 160)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(270, 17)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Seleccione los campos de Indentificación:"
        '
        'frmDependencias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(490, 327)
        Me.Controls.Add(Me.grCampos)
        Me.Controls.Add(Me.txtConsulta)
        Me.Controls.Add(Me.lblDependencia)
        Me.Controls.Add(Me.clcDependencia)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblConcepto_Deduccion)
        Me.Controls.Add(Me.txtConcepto_Deduccion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Name = "frmDependencias"
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtConcepto_Deduccion, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto_Deduccion, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcDependencia, 0)
        Me.Controls.SetChildIndex(Me.lblDependencia, 0)
        Me.Controls.SetChildIndex(Me.txtConsulta, 0)
        Me.Controls.SetChildIndex(Me.grCampos, 0)
        CType(Me.clcDependencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto_Deduccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConsulta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grCampos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvCampos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcLongitud, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oDependencias As VillarrealBusiness.clsDependencias
    Private oConveniosIdentificadores As VillarrealBusiness.clsConveniosIdentificaciones


#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmDependencias_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmDependencias_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub
    Private Sub frmDependencias_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmDependencias_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Me.grvCampos.CloseEditor()
        Me.grvCampos.UpdateCurrentRow()


        Select Case Action
            Case Actions.Insert
                Response = oDependencias.Insertar(Me.DataSource)

               


            Case Actions.Update
                Response = oDependencias.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oConveniosIdentificadores.Eliminar(Me.clcDependencia.Value)
                If Not Response.ErrorFound Then Response = oDependencias.Eliminar(clcDependencia.Value)

        End Select

        If Not Response.ErrorFound Then


            If Action = Actions.Delete Then Exit Sub

            Dim orow As DataRow
            For Each orow In CType(Me.grvCampos.DataSource, DataView).Table.Rows
                If orow("seleccionar") = True Then
                    If Not Response.ErrorFound Then Response = oConveniosIdentificadores.Insertar(Me.clcDependencia.Value, orow("campo"))
                End If
            Next

        End If
    End Sub
    Private Sub frmDependencias_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oDependencias.DespliegaDatos(OwnerForm.Value("dependencia"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            oDataSet = Nothing
        End If

        LLENAGRID()

    End Sub
    Private Sub frmDependencias_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
        oDependencias = New VillarrealBusiness.clsDependencias
        oConveniosIdentificadores = New VillarrealBusiness.clsConveniosIdentificaciones
        LLENAGRID()

    End Sub
    Private Sub frmDependencias_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Me.grvCampos.CloseEditor()
        Me.grvCampos.UpdateCurrentRow()

        Response = oDependencias.Validacion(Action, txtDescripcion.Text, txtConcepto_Deduccion.Text)

    End Sub
    Private Sub frmDependencias_Localize() Handles MyBase.Localize
        Find("dependencia", CType(Me.clcDependencia.EditValue, Object))
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
	Private Sub txtDescripcion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oDependencias.ValidaDescripcion(txtDescripcion.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
	End Sub
	Private Sub txtConcepto_Deduccion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtConcepto_Deduccion.Validating
		Dim oEvent As Dipros.Utils.Events
		If Action <> Actions.Insert Or MyBase.KeyEsc = Keys.Escape Then Exit Sub
		oEvent = oDependencias.ValidaConcepto_Deduccion(txtConcepto_Deduccion.Text)
		If oEvent.ErrorFound Then
			oEvent.ShowError()
			e.Cancel = True
		End If
    End Sub

    'Private Sub grvCampos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grvCampos.KeyDown
    '    If e.KeyCode = e.KeyCode.Enter Then
    '        ' Valida si no tiene campo no deja agregar mas filas al grid
    '        If Me.grvCampos.GetRowCellValue(Me.grvCampos.FocusedRowHandle, Me.grcCampo) Is System.DBNull.Value Then Exit Sub
    '        If CType(Me.grvCampos.GetRowCellValue(Me.grvCampos.FocusedRowHandle, Me.grcCampo), String).Length = 0 Then Exit Sub

    '        ' Actualiza los Valores al Cambiar de fila
    '        Me.grvCampos.CloseEditor()
    '        Me.grvCampos.UpdateCurrentRow()


    '        If Me.grvCampos.RowCount - 1 = Me.grvCampos.FocusedRowHandle Then
    '            Me.grvCampos.AddNewRow()
    '            Me.grvCampos.FocusedRowHandle = 0
    '            Me.grvCampos.MoveLast()
    '            Me.grvCampos.SetRowCellValue(Me.grvCampos.FocusedRowHandle, Me.grcCampo, "")
    '            Me.grvCampos.SetRowCellValue(Me.grvCampos.FocusedRowHandle, Me.grcTipo, "bit")
    '            Me.grvCampos.SetRowCellValue(Me.grvCampos.FocusedRowHandle, Me.grcLongitud, 0)

    '            Me.grcCampo = Me.grvCampos.FocusedColumn
    '        End If


    '        Dim Response As Dipros.Utils.Events


    '    End If

    'End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub LLENAGRID()
        Dim response As Events
        response = oConveniosIdentificadores.dependencias_campos_plantilla(Me.clcDependencia.Value)
        If Not response.ErrorFound Then
            Dim odataset As DataSet
            odataset = response.Value

            Me.grCampos.DataSource = odataset.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub

#End Region



    
End Class
