Imports System.Data.Odbc
Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmImportarPlantilla
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lkpDependencia As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtArchivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDirectorio As System.Windows.Forms.Label
    Friend WithEvents lkpDirectorio As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmImportarPlantilla))
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.lkpDependencia = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtArchivo = New DevExpress.XtraEditors.TextEdit
        Me.lblDirectorio = New System.Windows.Forms.Label
        Me.lkpDirectorio = New DevExpress.XtraEditors.ButtonEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        CType(Me.txtArchivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkpDirectorio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(293, 28)
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(5, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(448, 48)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "ESTE PROCESO REALIZAR� LA IMPORTACI�N DE LOS EMPLEADOS SEGUN EL CONVENIO SELECCIO" & _
        "NADO"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(31, 93)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(60, 16)
        Me.lblVendedor.TabIndex = 1
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "&Convenio:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDependencia
        '
        Me.lkpDependencia.AllowAdd = False
        Me.lkpDependencia.AutoReaload = False
        Me.lkpDependencia.DataSource = Nothing
        Me.lkpDependencia.DefaultSearchField = ""
        Me.lkpDependencia.DisplayMember = "descripcion"
        Me.lkpDependencia.EditValue = Nothing
        Me.lkpDependencia.Filtered = False
        Me.lkpDependencia.InitValue = Nothing
        Me.lkpDependencia.Location = New System.Drawing.Point(91, 90)
        Me.lkpDependencia.MultiSelect = False
        Me.lkpDependencia.Name = "lkpDependencia"
        Me.lkpDependencia.NullText = ""
        Me.lkpDependencia.PopupWidth = CType(310, Long)
        Me.lkpDependencia.Required = False
        Me.lkpDependencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDependencia.SearchMember = ""
        Me.lkpDependencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDependencia.SelectAll = False
        Me.lkpDependencia.Size = New System.Drawing.Size(336, 22)
        Me.lkpDependencia.TabIndex = 2
        Me.lkpDependencia.Tag = ""
        Me.lkpDependencia.ToolTip = "convenio"
        Me.lkpDependencia.ValueMember = "dependencia"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(84, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Arch&ivo:"
        '
        'txtArchivo
        '
        Me.txtArchivo.EditValue = "FORMATO_IMPORTACION"
        Me.txtArchivo.Location = New System.Drawing.Point(137, 38)
        Me.txtArchivo.Name = "txtArchivo"
        Me.txtArchivo.Size = New System.Drawing.Size(175, 22)
        Me.txtArchivo.TabIndex = 6
        Me.txtArchivo.ToolTip = "Archivo"
        '
        'lblDirectorio
        '
        Me.lblDirectorio.AutoSize = True
        Me.lblDirectorio.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDirectorio.Location = New System.Drawing.Point(9, 17)
        Me.lblDirectorio.Name = "lblDirectorio"
        Me.lblDirectorio.Size = New System.Drawing.Size(125, 16)
        Me.lblDirectorio.TabIndex = 3
        Me.lblDirectorio.Text = "&Seleccione Directorio:"
        Me.lblDirectorio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDirectorio
        '
        Me.lkpDirectorio.EditValue = ""
        Me.lkpDirectorio.Location = New System.Drawing.Point(137, 14)
        Me.lkpDirectorio.Name = "lkpDirectorio"
        '
        'lkpDirectorio.Properties
        '
        Me.lkpDirectorio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.lkpDirectorio.Size = New System.Drawing.Size(296, 22)
        Me.lkpDirectorio.TabIndex = 4
        Me.lkpDirectorio.ToolTip = "Ruta del Directorio en donde se guardar� el archivo o archivos a generar"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtArchivo)
        Me.GroupBox1.Controls.Add(Me.lkpDirectorio)
        Me.GroupBox1.Controls.Add(Me.lblDirectorio)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 118)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(443, 72)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'frmImportarPlantilla
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(458, 199)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lkpDependencia)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmImportarPlantilla"
        Me.Text = "frmImportarPlantilla"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lkpDependencia, 0)
        Me.Controls.SetChildIndex(Me.lblVendedor, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.txtArchivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkpDirectorio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oPlantilla As New VillarrealBusiness.clsPlantilla
    Private oDependencias As New VillarrealBusiness.clsDependencias
    Private oConveniosIdentificadores As New VillarrealBusiness.clsConveniosIdentificaciones
    Private Consulta_Dependencia As String = ""
    Private odatatable As DataTable
    Private oFormWait As Comunes.frmImportar
    Private response_proceso As Events
    Private clave_identificacion As String

    Private ReadOnly Property Convenio() As Long
        Get
            Return PreparaValorLookup(Me.lkpDependencia)
        End Get
    End Property
    Private ReadOnly Property Directorio() As String
        Get
            Return PreparaValorLookupStr(Me.lkpDirectorio)
        End Get
    End Property

#Region "Eventos de la Forma"

    Private Sub frmImportarPlantilla_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
    End Sub
    Private Sub frmImportarPlantilla_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        oFormWait = New Comunes.frmImportar
        Dim oThread As New Threading.Thread(AddressOf ImportarPlantilla)
        oThread.Start()
        oFormWait.ShowDialog()

    End Sub
    Private Sub frmImportarPlantilla_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

        If odatatable Is Nothing Then
            Response = oPlantilla.ValidacionImportarPlantilla_falta_dependencia(True)
        Else
            Response = oPlantilla.ValidacionImportarPlantilla(Me.Convenio, Me.Directorio, Me.txtArchivo.Text, odatatable.Rows.Count)
        End If
    End Sub
#End Region

#Region "Eventos de los Controles"

    Private Sub lkpDependencia_Format() Handles lkpDependencia.Format
        Comunes.clsFormato.for_dependencias_grl(Me.lkpDependencia)
    End Sub
    Private Sub lkpDependencia_LoadData(ByVal Initialize As Boolean) Handles lkpDependencia.LoadData
        Dim Response As New Events
        Response = oDependencias.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDependencia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDependencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDependencia.EditValueChanged
        If Me.lkpDependencia.DataSource Is Nothing Or Me.lkpDependencia.EditValue Is Nothing Then Exit Sub
        Consulta_Dependencia = Me.lkpDependencia.GetValue("consulta")

        If Convenio > 0 Then
            Dim response As Events
            response = oConveniosIdentificadores.Listado(Convenio)
            If Not response.ErrorFound Then
                odatatable = CType(response.Value, DataSet).Tables(0)

            End If

        End If


    End Sub
    Private Sub lkpDirectorio_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkpDirectorio.Click
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            lkpDirectorio.EditValue = FolderBrowserDialog1.SelectedPath
        End If
    End Sub
#End Region

#Region "Funcionalidad"
    Private Sub ImportarPlantilla()
        Dim bEntro As Boolean = False
        Dim oImporta As New Comunes.clsImportaDbfs
        Dim oLector As OdbcDataReader
        Dim conteo As Integer = 0
        Dim municipio As Long
        Dim clave_empleado As Long
        Dim nombres As String = ""
        Dim apellidos As String = ""
        Dim i As Long = 0


        Try
            TINApp.Connection.Begin()

            oImporta.Directorio = lkpDirectorio.EditValue
            oImporta.Archivo = Me.txtArchivo.Text

            oImporta.Consulta = Consulta_Dependencia

            oLector = oImporta.RealizaConsulta
            Do While oLector.Read
                bEntro = True
                clave_identificacion = ""

                If CType(oLector("municipio"), String).Trim.Length = 0 Then
                    municipio = -1
                Else

                    municipio = CType(oLector("municipio"), Long)
                End If

                If CType(oLector("clave_emp"), String).Trim.Length = 0 Then
                    clave_empleado = -1
                Else
                    clave_empleado = CType(oLector("clave_emp"), Long)
                End If


                apellidos = oLector("apellidos")
                nombres = oLector("nombres")

                If nombres.Trim(" ").Length = 0 Then
                    nombres = CType(oLector("nom_comp"), String).Substring(0, CType(oLector("nom_comp"), String).IndexOf(" "))
                    If ((CType(oLector("nom_comp"), String).Length) - (CType(oLector("nom_comp"), String).IndexOf(" ") + 1)) > 0 Then apellidos = CType(oLector("nom_comp"), String).Substring(CType(oLector("nom_comp"), String).IndexOf(" ") + 1, ((CType(oLector("nom_comp"), String).Length) - (CType(oLector("nom_comp"), String).IndexOf(" ") + 1)))
                End If



                For i = 0 To Me.odatatable.Rows.Count - 1

                    Select Case CType(Me.odatatable.Rows(i).Item("campo"), String).ToUpper
                        Case "RFC"
                            clave_identificacion = clave_identificacion + CType(oLector("rfc"), String)

                        Case "PLAZA"
                            CType(oLector("plaza"), Integer).ToString()
                            clave_identificacion = clave_identificacion + CType(oLector("plaza"), Integer).ToString()

                        Case "NOMBRE_COMPLETO"
                            clave_identificacion = clave_identificacion + CType(oLector("nom_comp"), String)

                        Case "APELLIDOS"
                            clave_identificacion = clave_identificacion + apellidos

                        Case "NOMBRES"
                            clave_identificacion = clave_identificacion + nombres

                        Case "CLAVE_EMPLEADO"
                            clave_identificacion = clave_identificacion + clave_empleado.ToString

                        Case "CENTRO_TRABAJO"
                            clave_identificacion = clave_identificacion + CType(oLector("centro_tra"), String)

                        Case "MUNICIPIO"
                            clave_identificacion = clave_identificacion + municipio.ToString

                    End Select
                Next


                response_proceso = oPlantilla.Insertar(Convenio, clave_identificacion, oLector("rfc"), oLector("plaza"), oLector("nom_comp"), apellidos, _
                                                    nombres, clave_empleado, oLector("centro_tra"), municipio)
                conteo += 1

                If response_proceso.ErrorFound Then
                    Throw New Exception("Error al Insertar el registro " & conteo.ToString + " - " + response_proceso.Message)
                End If
            Loop

            oImporta.Terminar()
            oFormWait.Close()
            TINApp.Connection.Commit()
        Catch ex As Exception
            TINApp.Connection.Rollback()
            oImporta.Terminar()
            oFormWait.Close()
            oLector = Nothing
            oImporta = Nothing
            If Not ex Is Nothing Then
                ShowMessage(MessageType.MsgInformation, "No se encuentra el Archivo en el Directorio Especificado", Me.Title)
            End If
            'ShowMessage(MessageType.MsgError, ex.Message, Me.Title)
        Finally

        End Try
    End Sub


#End Region

End Class
