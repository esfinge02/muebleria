Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmExportarArchivoDocumentos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grEnviosDependencias As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvEnviosDependenciasdocumentos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepcboTipo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents tmaEnviosDependenciasDocumentos As Dipros.Windows.TINMaster
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcimporte As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblnombre_cliente As System.Windows.Forms.Label
    Friend WithEvents lbltipo As System.Windows.Forms.Label
    Friend WithEvents grcQuincena As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcAnio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents clcMensual As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcMunicipio As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcQna_ini As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcQna_fin As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblRFC As System.Windows.Forms.Label
    Friend WithEvents lblDesc_municipio As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcImporteOriginal As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblTipoOriginal As System.Windows.Forms.Label
    Friend WithEvents txtTipoOriginal As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExportarArchivoDocumentos))
        Me.grEnviosDependencias = New DevExpress.XtraGrid.GridControl
        Me.grvEnviosDependenciasdocumentos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSerie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcQuincena = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcAnio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepcboTipo = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
        Me.tmaEnviosDependenciasDocumentos = New Dipros.Windows.TINMaster
        Me.clcimporte = New DevExpress.XtraEditors.CalcEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblPartida = New System.Windows.Forms.Label
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        Me.lblnombre_cliente = New System.Windows.Forms.Label
        Me.lbltipo = New System.Windows.Forms.Label
        Me.clcMensual = New DevExpress.XtraEditors.CalcEdit
        Me.clcMunicipio = New DevExpress.XtraEditors.CalcEdit
        Me.clcQna_ini = New DevExpress.XtraEditors.CalcEdit
        Me.clcQna_fin = New DevExpress.XtraEditors.CalcEdit
        Me.lblRFC = New System.Windows.Forms.Label
        Me.lblDesc_municipio = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcImporteOriginal = New DevExpress.XtraEditors.CalcEdit
        Me.lblTipoOriginal = New System.Windows.Forms.Label
        Me.txtTipoOriginal = New DevExpress.XtraEditors.TextEdit
        CType(Me.grEnviosDependencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvEnviosDependenciasdocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepcboTipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcimporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMensual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcQna_ini.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcQna_fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporteOriginal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTipoOriginal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5270, 28)
        '
        'grEnviosDependencias
        '
        '
        'grEnviosDependencias.EmbeddedNavigator
        '
        Me.grEnviosDependencias.EmbeddedNavigator.Name = ""
        Me.grEnviosDependencias.Location = New System.Drawing.Point(8, 144)
        Me.grEnviosDependencias.MainView = Me.grvEnviosDependenciasdocumentos
        Me.grEnviosDependencias.Name = "grEnviosDependencias"
        Me.grEnviosDependencias.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepcboTipo})
        Me.grEnviosDependencias.Size = New System.Drawing.Size(640, 272)
        Me.grEnviosDependencias.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grEnviosDependencias.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.TabIndex = 72
        Me.grEnviosDependencias.Text = "EnviosDependencias"
        '
        'grvEnviosDependenciasdocumentos
        '
        Me.grvEnviosDependenciasdocumentos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSucursal, Me.grcConcepto, Me.grcSerie, Me.grcFolio, Me.grcCliente, Me.grcDocumento, Me.grcImporte, Me.grcQuincena, Me.grcAnio})
        Me.grvEnviosDependenciasdocumentos.GridControl = Me.grEnviosDependencias
        Me.grvEnviosDependenciasdocumentos.Name = "grvEnviosDependenciasdocumentos"
        Me.grvEnviosDependenciasdocumentos.OptionsCustomization.AllowFilter = False
        Me.grvEnviosDependenciasdocumentos.OptionsCustomization.AllowGroup = False
        Me.grvEnviosDependenciasdocumentos.OptionsView.ShowGroupPanel = False
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursal.VisibleIndex = 0
        Me.grcSucursal.Width = 76
        '
        'grcConcepto
        '
        Me.grcConcepto.Caption = "Concepto"
        Me.grcConcepto.FieldName = "concepto"
        Me.grcConcepto.Name = "grcConcepto"
        Me.grcConcepto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcConcepto.VisibleIndex = 1
        Me.grcConcepto.Width = 84
        '
        'grcSerie
        '
        Me.grcSerie.Caption = "Serie"
        Me.grcSerie.FieldName = "serie"
        Me.grcSerie.Name = "grcSerie"
        Me.grcSerie.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSerie.VisibleIndex = 2
        Me.grcSerie.Width = 61
        '
        'grcFolio
        '
        Me.grcFolio.Caption = "Folio"
        Me.grcFolio.FieldName = "folio"
        Me.grcFolio.Name = "grcFolio"
        Me.grcFolio.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFolio.VisibleIndex = 3
        Me.grcFolio.Width = 68
        '
        'grcCliente
        '
        Me.grcCliente.Caption = "Cliente"
        Me.grcCliente.FieldName = "cliente"
        Me.grcCliente.Name = "grcCliente"
        Me.grcCliente.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCliente.VisibleIndex = 4
        Me.grcCliente.Width = 130
        '
        'grcDocumento
        '
        Me.grcDocumento.Caption = "Documento"
        Me.grcDocumento.FieldName = "documento"
        Me.grcDocumento.Name = "grcDocumento"
        Me.grcDocumento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDocumento.VisibleIndex = 5
        Me.grcDocumento.Width = 110
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.SummaryItem.DisplayFormat = "$###,###,##0.00"
        Me.grcImporte.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporte.VisibleIndex = 6
        Me.grcImporte.Width = 97
        '
        'grcQuincena
        '
        Me.grcQuincena.Caption = "Quincena"
        Me.grcQuincena.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcQuincena.FieldName = "quincena"
        Me.grcQuincena.Name = "grcQuincena"
        '
        'grcAnio
        '
        Me.grcAnio.Caption = "Anio"
        Me.grcAnio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcAnio.FieldName = "anio"
        Me.grcAnio.Name = "grcAnio"
        '
        'RepcboTipo
        '
        Me.RepcboTipo.AutoHeight = False
        Me.RepcboTipo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepcboTipo.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Alta", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Baja", "B", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Modificacion", "M", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("None", "N", -1)})
        Me.RepcboTipo.Name = "RepcboTipo"
        '
        'tmaEnviosDependenciasDocumentos
        '
        Me.tmaEnviosDependenciasDocumentos.BackColor = System.Drawing.Color.White
        Me.tmaEnviosDependenciasDocumentos.CanDelete = True
        Me.tmaEnviosDependenciasDocumentos.CanInsert = True
        Me.tmaEnviosDependenciasDocumentos.CanUpdate = True
        Me.tmaEnviosDependenciasDocumentos.Grid = Me.grEnviosDependencias
        Me.tmaEnviosDependenciasDocumentos.Location = New System.Drawing.Point(11, 120)
        Me.tmaEnviosDependenciasDocumentos.Name = "tmaEnviosDependenciasDocumentos"
        Me.tmaEnviosDependenciasDocumentos.Size = New System.Drawing.Size(640, 23)
        Me.tmaEnviosDependenciasDocumentos.TabIndex = 7
        Me.tmaEnviosDependenciasDocumentos.Title = ""
        Me.tmaEnviosDependenciasDocumentos.UpdateTitle = "un Registro"
        '
        'clcimporte
        '
        Me.clcimporte.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcimporte.Location = New System.Drawing.Point(85, 87)
        Me.clcimporte.Name = "clcimporte"
        '
        'clcimporte.Properties
        '
        Me.clcimporte.Properties.DisplayFormat.FormatString = "$###,###0.00"
        Me.clcimporte.Properties.Enabled = False
        Me.clcimporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcimporte.Size = New System.Drawing.Size(96, 22)
        Me.clcimporte.TabIndex = 6
        Me.clcimporte.Tag = "importe"
        Me.clcimporte.ToolTip = "importe"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(35, 66)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 3
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = True
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(85, 63)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(520, Long)
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(382, 22)
        Me.lkpCliente.TabIndex = 4
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = "cliente"
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 16)
        Me.Label2.TabIndex = 5
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Importe:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(34, 42)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 1
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "&Partida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(85, 40)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.Enabled = False
        Me.clcPartida.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(72, 21)
        Me.clcPartida.TabIndex = 2
        Me.clcPartida.Tag = "partida"
        Me.clcPartida.ToolTip = "partida"
        '
        'lblnombre_cliente
        '
        Me.lblnombre_cliente.Location = New System.Drawing.Point(496, 40)
        Me.lblnombre_cliente.Name = "lblnombre_cliente"
        Me.lblnombre_cliente.Size = New System.Drawing.Size(312, 23)
        Me.lblnombre_cliente.TabIndex = 79
        Me.lblnombre_cliente.Tag = "nombre_cliente"
        Me.lblnombre_cliente.Visible = False
        '
        'lbltipo
        '
        Me.lbltipo.Location = New System.Drawing.Point(619, 64)
        Me.lbltipo.Name = "lbltipo"
        Me.lbltipo.Size = New System.Drawing.Size(32, 23)
        Me.lbltipo.TabIndex = 80
        Me.lbltipo.Tag = "tipo"
        Me.lbltipo.Visible = False
        '
        'clcMensual
        '
        Me.clcMensual.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcMensual.Location = New System.Drawing.Point(219, 96)
        Me.clcMensual.Name = "clcMensual"
        Me.clcMensual.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcMensual.Size = New System.Drawing.Size(75, 22)
        Me.clcMensual.TabIndex = 81
        Me.clcMensual.Tag = "mensual"
        Me.clcMensual.Visible = False
        '
        'clcMunicipio
        '
        Me.clcMunicipio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcMunicipio.Location = New System.Drawing.Point(299, 96)
        Me.clcMunicipio.Name = "clcMunicipio"
        Me.clcMunicipio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcMunicipio.Size = New System.Drawing.Size(75, 22)
        Me.clcMunicipio.TabIndex = 82
        Me.clcMunicipio.Tag = "municpio"
        Me.clcMunicipio.Visible = False
        '
        'clcQna_ini
        '
        Me.clcQna_ini.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcQna_ini.Location = New System.Drawing.Point(379, 96)
        Me.clcQna_ini.Name = "clcQna_ini"
        Me.clcQna_ini.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcQna_ini.Size = New System.Drawing.Size(75, 22)
        Me.clcQna_ini.TabIndex = 83
        Me.clcQna_ini.Tag = "qna_ini"
        Me.clcQna_ini.Visible = False
        '
        'clcQna_fin
        '
        Me.clcQna_fin.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcQna_fin.Location = New System.Drawing.Point(459, 96)
        Me.clcQna_fin.Name = "clcQna_fin"
        Me.clcQna_fin.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcQna_fin.Size = New System.Drawing.Size(75, 22)
        Me.clcQna_fin.TabIndex = 84
        Me.clcQna_fin.Tag = "qna_fin"
        Me.clcQna_fin.Visible = False
        '
        'lblRFC
        '
        Me.lblRFC.Location = New System.Drawing.Point(475, 64)
        Me.lblRFC.Name = "lblRFC"
        Me.lblRFC.TabIndex = 85
        Me.lblRFC.Visible = False
        '
        'lblDesc_municipio
        '
        Me.lblDesc_municipio.Location = New System.Drawing.Point(539, 93)
        Me.lblDesc_municipio.Name = "lblDesc_municipio"
        Me.lblDesc_municipio.TabIndex = 86
        Me.lblDesc_municipio.Tag = "desc_municipio"
        Me.lblDesc_municipio.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(161, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.TabIndex = 88
        Me.Label3.Tag = ""
        Me.Label3.Text = "I&mporte Original:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label3.Visible = False
        '
        'clcImporteOriginal
        '
        Me.clcImporteOriginal.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImporteOriginal.Location = New System.Drawing.Point(261, 39)
        Me.clcImporteOriginal.Name = "clcImporteOriginal"
        '
        'clcImporteOriginal.Properties
        '
        Me.clcImporteOriginal.Properties.DisplayFormat.FormatString = "$###,###0.00"
        Me.clcImporteOriginal.Properties.Enabled = False
        Me.clcImporteOriginal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImporteOriginal.Size = New System.Drawing.Size(78, 22)
        Me.clcImporteOriginal.TabIndex = 87
        Me.clcImporteOriginal.Tag = "importe_original"
        Me.clcImporteOriginal.Visible = False
        '
        'lblTipoOriginal
        '
        Me.lblTipoOriginal.AutoSize = True
        Me.lblTipoOriginal.Location = New System.Drawing.Point(347, 42)
        Me.lblTipoOriginal.Name = "lblTipoOriginal"
        Me.lblTipoOriginal.Size = New System.Drawing.Size(79, 16)
        Me.lblTipoOriginal.TabIndex = 89
        Me.lblTipoOriginal.Tag = ""
        Me.lblTipoOriginal.Text = "&Tipo Original:"
        Me.lblTipoOriginal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTipoOriginal.Visible = False
        '
        'txtTipoOriginal
        '
        Me.txtTipoOriginal.EditValue = ""
        Me.txtTipoOriginal.Location = New System.Drawing.Point(426, 39)
        Me.txtTipoOriginal.Name = "txtTipoOriginal"
        '
        'txtTipoOriginal.Properties
        '
        Me.txtTipoOriginal.Properties.Enabled = False
        Me.txtTipoOriginal.Size = New System.Drawing.Size(32, 22)
        Me.txtTipoOriginal.TabIndex = 90
        Me.txtTipoOriginal.Tag = "tipo_original"
        Me.txtTipoOriginal.Visible = False
        '
        'frmExportarArchivoDocumentos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(658, 424)
        Me.Controls.Add(Me.txtTipoOriginal)
        Me.Controls.Add(Me.lblTipoOriginal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.clcImporteOriginal)
        Me.Controls.Add(Me.lblDesc_municipio)
        Me.Controls.Add(Me.lblRFC)
        Me.Controls.Add(Me.clcQna_fin)
        Me.Controls.Add(Me.clcQna_ini)
        Me.Controls.Add(Me.clcMunicipio)
        Me.Controls.Add(Me.clcMensual)
        Me.Controls.Add(Me.lbltipo)
        Me.Controls.Add(Me.lblnombre_cliente)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.grEnviosDependencias)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.clcimporte)
        Me.Controls.Add(Me.tmaEnviosDependenciasDocumentos)
        Me.Name = "frmExportarArchivoDocumentos"
        Me.Text = "frmExportarArchivoDocumentos"
        Me.Controls.SetChildIndex(Me.tmaEnviosDependenciasDocumentos, 0)
        Me.Controls.SetChildIndex(Me.clcimporte, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.grEnviosDependencias, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lblnombre_cliente, 0)
        Me.Controls.SetChildIndex(Me.lbltipo, 0)
        Me.Controls.SetChildIndex(Me.clcMensual, 0)
        Me.Controls.SetChildIndex(Me.clcMunicipio, 0)
        Me.Controls.SetChildIndex(Me.clcQna_ini, 0)
        Me.Controls.SetChildIndex(Me.clcQna_fin, 0)
        Me.Controls.SetChildIndex(Me.lblRFC, 0)
        Me.Controls.SetChildIndex(Me.lblDesc_municipio, 0)
        Me.Controls.SetChildIndex(Me.clcImporteOriginal, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.lblTipoOriginal, 0)
        Me.Controls.SetChildIndex(Me.txtTipoOriginal, 0)
        CType(Me.grEnviosDependencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvEnviosDependenciasdocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepcboTipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcimporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMensual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcQna_ini.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcQna_fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporteOriginal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTipoOriginal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oClientes As New VillarrealBusiness.clsClientes
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oDocumentos As New VillarrealBusiness.clsDependenciasDetalleExportacion
    Public sucursal_dependencia As Boolean = False
    Private ImporteTotal As Double
    Private ImporteOriginal As Double


    Public ReadOnly Property Cliente() As Long
        Get
            Return PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Public ReadOnly Property Quincena_envio() As Long
        Get
            Return CType(OwnerForm, frmExportarArchivo).Quincena_envio
        End Get
    End Property
    Public ReadOnly Property Anio_envio() As Long
        Get
            Return CType(OwnerForm, frmExportarArchivo).Anio_envio
        End Get
    End Property
    Public ReadOnly Property Partida() As Long
        Get
            Return Me.clcPartida.EditValue
        End Get
    End Property
    Private Property Tipo() As Char
        Get
            Return lbltipo.Text.ToUpper
        End Get
        Set(ByVal Value As Char)
            lbltipo.Text = Value
        End Set
    End Property
#End Region

#Region "Eventos de la Forma"

    Private Sub frmExportarArchivoDocumentos_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oDocumentos = New VillarrealBusiness.clsDependenciasDetalleExportacion
        sucursal_dependencia = CType(CType(oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)


        With Me.tmaEnviosDependenciasDocumentos
            .UpdateForm = New frmExportarArchivoLetrasDetalle
            .AddColumn("sucursal", "System.Int32")
            .AddColumn("concepto")
            .AddColumn("serie")
            .AddColumn("folio", "System.Int32")
            .AddColumn("cliente", "System.Int32")
            .AddColumn("documento", "System.Int32")
            .AddColumn("importe", "System.Double")
            .AddColumn("quincena", "System.Int32")
            .AddColumn("anio", "System.Int32")
            .AddColumn("importe_original", "System.Double")
            .AddColumn("tipo_original")
        End With

        Select Case Action
            Case Actions.Insert
                Tipo = "A"
            Case Actions.Update, Actions.Delete

                Me.clcimporte.Enabled = False
                Me.lkpCliente.Enabled = False
        End Select
    End Sub
    Private Sub frmExportarArchivoDocumentos_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        ActualizaTipo()
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
        Me.ActualizaLetras()
    End Sub
    Private Sub frmExportarArchivoDocumentos_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        'Select Case Action
        '    Case Actions.Update
        '        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        '        'me.importe = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("importe")
        '    Case Actions.Delete
        '        Me.DataSource = OwnerForm.MasterControl.SelectedRow

        'End Select

        Me.lkpCliente.EditValue = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("cliente")
        Me.clcPartida.Value = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("partida")
        Tipo = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("tipo")
        'tipo_original = Tipo

        ImporteOriginal = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("importe")

        Me.clcImporteOriginal.EditValue = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("importe_original")
        Me.txtTipoOriginal.EditValue = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("tipo_original")
        ObtenerPrecios()
        CalculaTotal()
    End Sub
    Private Sub frmExportarArchivoDocumentos_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oDocumentos.Validacion(Me.clcimporte.EditValue, Me.lkpCliente.EditValue, Me.grvEnviosDependenciasdocumentos.DataRowCount)
        If Response.ErrorFound Then Exit Sub
        If Action = Actions.Insert Then Response = oDocumentos.ValidaExisteFila(Cliente, "cliente", CType(OwnerForm, frmExportarArchivo).tmaEnviosDependencias.DataSource, True)
    End Sub

#End Region

#Region "Eventos de los Controles"

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData

        Dim Response As New Events
        Response = oClientes.LookupLlenado(sucursal_dependencia)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        If Cliente <> -1 Then   'And Action = Actions.Insert
            Me.lblnombre_cliente.Text = Me.lkpCliente.GetValue("nombre")
            Me.lblRFC.Text = Me.lkpCliente.GetValue("rfc")
            Me.clcMunicipio.Value = Me.lkpCliente.GetValue("municipio")
            Me.lblDesc_municipio.Text = Me.lkpCliente.GetValue("desc_municipio")
            Me.clcQna_ini.Value = Me.lkpCliente.GetValue("qna_ini")
            Me.clcQna_fin.Value = Me.lkpCliente.GetValue("qna_fin")
        End If
    End Sub
    Private Sub tmaEnviosDependenciasDocumentos_ValidateFields(ByRef Cancel As Boolean) Handles tmaEnviosDependenciasDocumentos.ValidateFields
        If Me.Cliente <= 0 Then
            Cancel = True
            ShowMessage(MessageType.MsgError, "El Cliente es Requerido")
        End If
    End Sub
#End Region

#Region "Funcionalidad"

    Private Sub ObtenerPrecios()
        Dim Indice As Integer
        'Indice = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("partida")
        Indice = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("cliente")
        Select Case Action
            Case Actions.Insert
                CalculaTotal() ''
            Case Actions.Update
                Me.tmaEnviosDependenciasDocumentos.DataSource = CType(OwnerForm, frmExportarArchivo).ClientesLetras(Indice).Letras
            Case Actions.Delete
                tmaEnviosDependenciasDocumentos.DataSource = CType(OwnerForm, frmExportarArchivo).ClientesLetras(Indice).Letras
        End Select
    End Sub
    Public Sub CalculaTotal()
        Me.clcimporte.Value = Me.grcImporte.SummaryItem.SummaryValue
        Me.clcMensual.Value = Me.clcimporte.Value * 2
    End Sub
    Private Sub ActualizaLetras()
        Dim Aux As New ClientesDeudasLetras
        Aux.Indice = Cliente
        Aux.Letras = Me.tmaEnviosDependenciasDocumentos.DataSource
        CType(OwnerForm, frmExportarArchivo).ClientesLetras(Aux.Indice) = Aux
    End Sub
    Private Sub ActualizaTipo()
        If Me.clcimporte.EditValue = Me.clcImporteOriginal.EditValue Then
            Tipo = Me.txtTipoOriginal.EditValue
        Else

            Select Case Tipo
                Case "A"
                    ' Si es alta siempre sera A
                    Tipo = "A"
                Case "B"
                    'Si es Baja 
                    If Me.clcimporte.Value > 0 Then
                        Tipo = "M"
                    End If
                Case "N", "M"
                    'si es None o Modificacion
                    If Me.clcimporte.Value > 0 And ImporteOriginal <> Me.clcimporte.Value Then
                        Tipo = "M"
                    End If
                    If Me.clcimporte.Value = 0 Then
                        Tipo = "B"
                    End If
            End Select

        End If

    End Sub

#End Region

 
End Class

