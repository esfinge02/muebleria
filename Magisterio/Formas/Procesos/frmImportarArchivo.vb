Imports System.Data.Odbc
Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias
Public Class frmImportarArchivo
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents spinAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents spinQuincena As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtArchivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lkpDirectorio As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents lblDirectorio As System.Windows.Forms.Label
    Friend WithEvents grcClaveCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepcboTipo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents grcImporteEnviado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteDescontado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDiferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbrExtraer As System.Windows.Forms.ToolBarButton
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents grArchivosRecibidos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvArchivosRecibidos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcclave_identificacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcdependencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcdescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcsucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcconvenio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcquincena_envio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcanio_envio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grctipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcconcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcserie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfolio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcdocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcpartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcsaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmImportarArchivo))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.spinAnio = New DevExpress.XtraEditors.SpinEdit
        Me.spinQuincena = New DevExpress.XtraEditors.SpinEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtArchivo = New DevExpress.XtraEditors.TextEdit
        Me.lkpDirectorio = New DevExpress.XtraEditors.ButtonEdit
        Me.lblDirectorio = New System.Windows.Forms.Label
        Me.grArchivosRecibidos = New DevExpress.XtraGrid.GridControl
        Me.grvArchivosRecibidos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteEnviado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteDescontado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDiferencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcclave_identificacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcdependencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcdescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcsucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcconvenio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcquincena_envio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcanio_envio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grctipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcconcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcserie = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfolio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcdocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcpartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcsaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepcboTipo = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.tbrExtraer = New System.Windows.Forms.ToolBarButton
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.GroupBox1.SuspendLayout()
        CType(Me.spinAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spinQuincena.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArchivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkpDirectorio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grArchivosRecibidos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvArchivosRecibidos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepcboTipo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbrExtraer})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(3557, 28)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.spinAnio)
        Me.GroupBox1.Controls.Add(Me.spinQuincena)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(221, 112)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(264, 48)
        Me.GroupBox1.TabIndex = 73
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "&Env�o"
        '
        'spinAnio
        '
        Me.spinAnio.EditValue = New Decimal(New Integer() {2006, 0, 0, 0})
        Me.spinAnio.Location = New System.Drawing.Point(192, 16)
        Me.spinAnio.Name = "spinAnio"
        '
        'spinAnio.Properties
        '
        Me.spinAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spinAnio.Properties.MaxValue = New Decimal(New Integer() {2015, 0, 0, 0})
        Me.spinAnio.Properties.MinValue = New Decimal(New Integer() {1990, 0, 0, 0})
        Me.spinAnio.Properties.UseCtrlIncrement = False
        Me.spinAnio.Size = New System.Drawing.Size(64, 22)
        Me.spinAnio.TabIndex = 8
        Me.spinAnio.ToolTip = "a�o"
        '
        'spinQuincena
        '
        Me.spinQuincena.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.spinQuincena.Location = New System.Drawing.Point(75, 16)
        Me.spinQuincena.Name = "spinQuincena"
        '
        'spinQuincena.Properties
        '
        Me.spinQuincena.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spinQuincena.Properties.MaxValue = New Decimal(New Integer() {24, 0, 0, 0})
        Me.spinQuincena.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.spinQuincena.Properties.UseCtrlIncrement = False
        Me.spinQuincena.Size = New System.Drawing.Size(64, 22)
        Me.spinQuincena.TabIndex = 6
        Me.spinQuincena.ToolTip = "quincena"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 16)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "&Quincena:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(160, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 16)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "A&�o:"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Enabled = False
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(70, 16)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(257, 22)
        Me.lkpSucursal.TabIndex = 2
        Me.lkpSucursal.Tag = "sucursal_entrada"
        Me.lkpSucursal.ToolTip = "sucursal"
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(14, 19)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(10, 43)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenio.TabIndex = 3
        Me.lblConvenio.Tag = ""
        Me.lblConvenio.Text = "&Convenio:"
        Me.lblConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "descripcion"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(70, 40)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = ""
        Me.lkpConvenio.PopupWidth = CType(310, Long)
        Me.lkpConvenio.Required = False
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(257, 22)
        Me.lkpConvenio.TabIndex = 4
        Me.lkpConvenio.Tag = ""
        Me.lkpConvenio.ToolTip = "convenio"
        Me.lkpConvenio.ValueMember = "dependencia"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(87, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 16)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Arch&ivo:"
        '
        'txtArchivo
        '
        Me.txtArchivo.EditValue = "FORMATO_IMPORTACION"
        Me.txtArchivo.Location = New System.Drawing.Point(140, 40)
        Me.txtArchivo.Name = "txtArchivo"
        Me.txtArchivo.Size = New System.Drawing.Size(185, 22)
        Me.txtArchivo.TabIndex = 12
        Me.txtArchivo.ToolTip = "archivo"
        '
        'lkpDirectorio
        '
        Me.lkpDirectorio.EditValue = "C:\"
        Me.lkpDirectorio.Location = New System.Drawing.Point(140, 16)
        Me.lkpDirectorio.Name = "lkpDirectorio"
        '
        'lkpDirectorio.Properties
        '
        Me.lkpDirectorio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.lkpDirectorio.Size = New System.Drawing.Size(185, 22)
        Me.lkpDirectorio.TabIndex = 10
        Me.lkpDirectorio.ToolTip = "Ruta del Directorio en donde se guardar� el archivo o archivos a generar"
        '
        'lblDirectorio
        '
        Me.lblDirectorio.AutoSize = True
        Me.lblDirectorio.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDirectorio.Location = New System.Drawing.Point(12, 19)
        Me.lblDirectorio.Name = "lblDirectorio"
        Me.lblDirectorio.Size = New System.Drawing.Size(125, 16)
        Me.lblDirectorio.TabIndex = 9
        Me.lblDirectorio.Text = "Seleccione &Directorio:"
        Me.lblDirectorio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grArchivosRecibidos
        '
        '
        'grArchivosRecibidos.EmbeddedNavigator
        '
        Me.grArchivosRecibidos.EmbeddedNavigator.Name = ""
        Me.grArchivosRecibidos.Location = New System.Drawing.Point(13, 168)
        Me.grArchivosRecibidos.MainView = Me.grvArchivosRecibidos
        Me.grArchivosRecibidos.Name = "grArchivosRecibidos"
        Me.grArchivosRecibidos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepcboTipo})
        Me.grArchivosRecibidos.Size = New System.Drawing.Size(683, 272)
        Me.grArchivosRecibidos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grArchivosRecibidos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArchivosRecibidos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArchivosRecibidos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArchivosRecibidos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArchivosRecibidos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grArchivosRecibidos.TabIndex = 90
        Me.grArchivosRecibidos.Text = "ArchivosRecibidos"
        '
        'grvArchivosRecibidos
        '
        Me.grvArchivosRecibidos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveCliente, Me.grcNombreCliente, Me.grcImporteEnviado, Me.grcImporteDescontado, Me.grcDiferencia, Me.grcclave_identificacion, Me.grcdependencia, Me.grcdescripcion, Me.grcsucursal, Me.grcconvenio, Me.grcquincena_envio, Me.grcanio_envio, Me.grctipo, Me.grcconcepto, Me.grcserie, Me.grcfolio, Me.grcdocumento, Me.grcpartida, Me.grcsaldo})
        Me.grvArchivosRecibidos.GridControl = Me.grArchivosRecibidos
        Me.grvArchivosRecibidos.GroupSummary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "importe_enviado", Nothing, "")})
        Me.grvArchivosRecibidos.Name = "grvArchivosRecibidos"
        Me.grvArchivosRecibidos.OptionsCustomization.AllowFilter = False
        Me.grvArchivosRecibidos.OptionsCustomization.AllowGroup = False
        Me.grvArchivosRecibidos.OptionsCustomization.AllowSort = False
        Me.grvArchivosRecibidos.OptionsView.ShowGroupPanel = False
        '
        'grcClaveCliente
        '
        Me.grcClaveCliente.Caption = "Cuenta"
        Me.grcClaveCliente.FieldName = "cliente"
        Me.grcClaveCliente.Name = "grcClaveCliente"
        Me.grcClaveCliente.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcClaveCliente.SortIndex = 0
        Me.grcClaveCliente.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcClaveCliente.VisibleIndex = 0
        Me.grcClaveCliente.Width = 112
        '
        'grcNombreCliente
        '
        Me.grcNombreCliente.Caption = "Nombre"
        Me.grcNombreCliente.FieldName = "nombre_cliente"
        Me.grcNombreCliente.Name = "grcNombreCliente"
        Me.grcNombreCliente.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreCliente.VisibleIndex = 1
        Me.grcNombreCliente.Width = 205
        '
        'grcImporteEnviado
        '
        Me.grcImporteEnviado.Caption = "Importe Enviado"
        Me.grcImporteEnviado.DisplayFormat.FormatString = "c2"
        Me.grcImporteEnviado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteEnviado.FieldName = "importe_enviado"
        Me.grcImporteEnviado.Name = "grcImporteEnviado"
        Me.grcImporteEnviado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteEnviado.SummaryItem.FieldName = "importe"
        Me.grcImporteEnviado.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcImporteEnviado.VisibleIndex = 2
        Me.grcImporteEnviado.Width = 115
        '
        'grcImporteDescontado
        '
        Me.grcImporteDescontado.Caption = "Importe Descontado"
        Me.grcImporteDescontado.DisplayFormat.FormatString = "c2"
        Me.grcImporteDescontado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteDescontado.FieldName = "importe_descontado"
        Me.grcImporteDescontado.Name = "grcImporteDescontado"
        Me.grcImporteDescontado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteDescontado.VisibleIndex = 3
        Me.grcImporteDescontado.Width = 119
        '
        'grcDiferencia
        '
        Me.grcDiferencia.Caption = "Diferencia"
        Me.grcDiferencia.DisplayFormat.FormatString = "c2"
        Me.grcDiferencia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDiferencia.FieldName = "diferencia"
        Me.grcDiferencia.Name = "grcDiferencia"
        Me.grcDiferencia.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDiferencia.VisibleIndex = 4
        Me.grcDiferencia.Width = 115
        '
        'grcclave_identificacion
        '
        Me.grcclave_identificacion.Caption = "clave_identificacion"
        Me.grcclave_identificacion.FieldName = "clave_identificacion"
        Me.grcclave_identificacion.Name = "grcclave_identificacion"
        '
        'grcdependencia
        '
        Me.grcdependencia.Caption = "dependencia"
        Me.grcdependencia.FieldName = "dependencia"
        Me.grcdependencia.Name = "grcdependencia"
        '
        'grcdescripcion
        '
        Me.grcdescripcion.Caption = "descripcion"
        Me.grcdescripcion.FieldName = "descripcion"
        Me.grcdescripcion.Name = "grcdescripcion"
        '
        'grcsucursal
        '
        Me.grcsucursal.Caption = "sucursal"
        Me.grcsucursal.FieldName = "sucursal"
        Me.grcsucursal.Name = "grcsucursal"
        '
        'grcconvenio
        '
        Me.grcconvenio.Caption = "convenio"
        Me.grcconvenio.FieldName = "convenio"
        Me.grcconvenio.Name = "grcconvenio"
        '
        'grcquincena_envio
        '
        Me.grcquincena_envio.Caption = "quincena_envio"
        Me.grcquincena_envio.FieldName = "quincena_envio"
        Me.grcquincena_envio.Name = "grcquincena_envio"
        '
        'grcanio_envio
        '
        Me.grcanio_envio.Caption = "anio_envio"
        Me.grcanio_envio.FieldName = "anio_envio"
        Me.grcanio_envio.Name = "grcanio_envio"
        '
        'grctipo
        '
        Me.grctipo.Caption = "tipo"
        Me.grctipo.FieldName = "tipo"
        Me.grctipo.Name = "grctipo"
        '
        'grcconcepto
        '
        Me.grcconcepto.Caption = "concepto"
        Me.grcconcepto.FieldName = "concepto"
        Me.grcconcepto.Name = "grcconcepto"
        '
        'grcserie
        '
        Me.grcserie.Caption = "serie"
        Me.grcserie.FieldName = "serie"
        Me.grcserie.Name = "grcserie"
        '
        'grcfolio
        '
        Me.grcfolio.Caption = "folio"
        Me.grcfolio.FieldName = "folio"
        Me.grcfolio.Name = "grcfolio"
        '
        'grcdocumento
        '
        Me.grcdocumento.Caption = "documento"
        Me.grcdocumento.FieldName = "documento"
        Me.grcdocumento.Name = "grcdocumento"
        '
        'grcpartida
        '
        Me.grcpartida.Caption = "partida"
        Me.grcpartida.FieldName = "partida"
        Me.grcpartida.Name = "grcpartida"
        '
        'grcsaldo
        '
        Me.grcsaldo.Caption = "saldo"
        Me.grcsaldo.FieldName = "saldo"
        Me.grcsaldo.Name = "grcsaldo"
        '
        'RepcboTipo
        '
        Me.RepcboTipo.AutoHeight = False
        Me.RepcboTipo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepcboTipo.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Alta", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Baja", "B", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Modificacion", "M", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("None", "N", -1)})
        Me.RepcboTipo.Name = "RepcboTipo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lkpDirectorio)
        Me.GroupBox2.Controls.Add(Me.lblDirectorio)
        Me.GroupBox2.Controls.Add(Me.txtArchivo)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(357, 34)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(336, 72)
        Me.GroupBox2.TabIndex = 91
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Documento"
        '
        'tbrExtraer
        '
        Me.tbrExtraer.Text = "Extraer Informaci�n"
        Me.tbrExtraer.ToolTipText = "Extraer Informaci�n"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lkpSucursal)
        Me.GroupBox3.Controls.Add(Me.lblSucursal)
        Me.GroupBox3.Controls.Add(Me.lblConvenio)
        Me.GroupBox3.Controls.Add(Me.lkpConvenio)
        Me.GroupBox3.Location = New System.Drawing.Point(13, 34)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(336, 72)
        Me.GroupBox3.TabIndex = 92
        Me.GroupBox3.TabStop = False
        '
        'frmImportarArchivo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(706, 455)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.grArchivosRecibidos)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmImportarArchivo"
        Me.Text = "frmImportarArchivo"
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.grArchivosRecibidos, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.spinAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spinQuincena.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArchivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkpDirectorio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grArchivosRecibidos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvArchivosRecibidos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepcboTipo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "Declaraciones"
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oDependencias As New VillarrealBusiness.clsDependencias
    Private oDependenciasImportacion As New VillarrealBusiness.clsDependenciasImportacion
    Private EstructuraLetras As ArrayList
    Private oArchivoImportado As New VillarrealBusiness.clsArchivoImportado
    Private oConveniosIdentificadores As New VillarrealBusiness.clsConveniosIdentificaciones


    Private Consulta_Dependencia As String = ""
    Private odatatable As DataTable
    Private oFormWait As Comunes.frmImportar
    Private response_proceso As Events
    Private clave_identificacion As String



    Private ReadOnly Property Sucursal() As Long
        Get
            Return PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Private ReadOnly Property convenio() As Long
        Get
            Return PreparaValorLookup(Me.lkpConvenio)
        End Get
    End Property
    Private ReadOnly Property Directorio() As String
        Get
            Return PreparaValorLookupStr(Me.lkpDirectorio)
        End Get
    End Property
    Public ReadOnly Property Quincena_envio() As Long
        Get
            Return Me.spinQuincena.Value
        End Get
    End Property
    Public ReadOnly Property Anio_envio() As Long
        Get
            Return Me.spinAnio.Value
        End Get
    End Property



#End Region

#Region "Eventos de la Forma"

    Private Sub frmExportarArchivo_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmExportarArchivo_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
        'ImprimirReporte()
    End Sub
    Private Sub frmExportarArchivo_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmImportarArchivo_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        '    oDocumentos = New VillarrealBusiness.clsDependenciasDetalleExportacion
        'sucursal_dependencia = CType(CType(oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)

        'Me.spinAnio.EditValue = Year(CDate(TINApp.FechaServidor))
        Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual
        Me.lkpSucursal.Enabled = False
        Me.spinQuincena.EditValue = Me.lkpSucursal.GetValue("quincena_envio")
        Me.spinAnio.EditValue = Me.lkpSucursal.GetValue("anio_envio")


        'Try
        '    Me.spinQuincena.EditValue = Me.lkpSucursal.GetValue("quincena_envio")
        '    Me.spinQuincena.Text = Me.lkpSucursal.GetValue("quincena_envio")
        '    Me.spinAnio.EditValue = Me.lkpSucursal.GetValue("anio_envio")
        '    Me.spinAnio.Text = Me.lkpSucursal.GetValue("anio_envio")
        'Catch ex As Exception
        'End Try
        'With Me.tmaEnviosDependencias
        '    .UpdateForm = New frmExportarArchivoLetrasDetalle
        '    .AddColumn("sucursal", "System.Int32")
        '    .AddColumn("concepto")
        '    .AddColumn("serie")
        '    .AddColumn("folio", "System.Int32")
        '    .AddColumn("cliente", "System.Int32")
        '    .AddColumn("documento", "System.Int32")
        '    .AddColumn("importe", "System.Double")
        '    .AddColumn("importe_descontado", "System.Double")
        '    .AddColumn("diferencia", "System.Double")
        '    .AddColumn("quincena", "System.Int32")
        '    .AddColumn("anio", "System.Int32")
        'End With

        '    Select Case Action
        '        Case Actions.Insert
        '            Tipo = "A"
        '        Case Actions.Update, Actions.Delete
        '            Me.clcimporte.Enabled = False
        '            Me.lkpCliente.Enabled = False
        '    End Select
    End Sub
    Private Sub frmImportarArchivo_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        GuardaMovimientos()
        'Me.Close()
    End Sub
    Private Sub frmImportarArchivo_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        '    Me.lkpCliente.EditValue = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("cliente")
        '    Me.clcPartida.Value = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("partida")
        '    Tipo = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("tipo")
        '    ImporteOriginal = CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("importe")
    End Sub
    Private Sub frmImportarArchivo_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oDependenciasImportacion.Validacion(Me.lkpSucursal.EditValue, convenio, Me.spinAnio.EditValue, Me.spinQuincena.EditValue)
        If Response.ErrorFound Then Exit Sub
        '    If Action = Actions.Insert Then Response = oDocumentos.ValidaExisteFila(Cliente, "cliente", CType(OwnerForm, frmExportarArchivo).tmaEnviosDependencias.DataSource, True)
    End Sub

#End Region

#Region "Eventos de los Controles"

    Private Sub lkpSucursalEntrada_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursalEntrada_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_dependencias_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim Response As New Events
        Response = oDependencias.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpDependencia_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpConvenio.EditValueChanged
        If Me.lkpConvenio.DataSource Is Nothing Or Me.lkpConvenio.EditValue Is Nothing Then Exit Sub
        Consulta_Dependencia = Me.lkpConvenio.GetValue("consulta")

        If convenio > 0 Then
            Dim response As Events
            response = oConveniosIdentificadores.Listado(convenio)
            If Not response.ErrorFound Then
                odatatable = CType(response.Value, DataSet).Tables(0)

            End If

        End If
    End Sub
    Private Sub lkpDirectorio_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkpDirectorio.Click
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            lkpDirectorio.EditValue = FolderBrowserDialog1.SelectedPath
        End If
    End Sub
    Private Sub spinAnio_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spinAnio.EditValueChanged
        'ObtieneDatos()
    End Sub
    Private Sub spinQuincena_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spinQuincena.EditValueChanged
        If CType(sender, DevExpress.XtraEditors.SpinEdit).IsLoading = True Then Exit Sub
        'ObtieneDatos()
    End Sub
    Private Sub frmImportarArchivo_ToolBarClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles MyBase.ToolBarClick
        If e.Button Is Me.tbrExtraer Then

            oFormWait = New Comunes.frmImportar
            Dim oThread As New Threading.Thread(AddressOf ImportarArchivoDBF)
            oThread.Start()
            oFormWait.ShowDialog()

            'If ValidaDirectorio() Then Exit Sub
            'Try
            '    ImportarArchivo()
            '    ShowMessage(MessageType.MsgInformation, "�El Archivo fue importado exitosamente!", "Exportar a Excel")
            'Catch ex As Exception
            '    ShowMessage(MessageType.MsgError, "Error al generar archivo", "Atenci�n", ex)
            'End Try

        End If
    End Sub
   
#End Region

#Region "Funcionalidad"
    Public Sub ExtraerArchivoExcel()

        Dim Nombre_archivo As String
        Nombre_archivo = "-{}-data source=" + Me.lkpDirectorio.EditValue + Me.txtArchivo.EditValue + ".XLS; "
        MsgBox(Nombre_archivo)

        Dim DS As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim MyConnection As System.Data.OleDb.OleDbConnection

        MyConnection = New System.Data.OleDb.OleDbConnection( _
              "provider=Microsoft.Jet.OLEDB.4.0; " & _
              Nombre_archivo & _
              "Extended Properties=Excel 8.0;")
        ' Select the data from Sheet1 of the workbook.
        MyCommand = New System.Data.OleDb.OleDbDataAdapter( _
              "select * from [Sheet1$]", MyConnection)

        DS = New System.Data.DataSet
        Try
            MyCommand.Fill(DS)
        Catch ex As Exception
        End Try

        MyConnection.Close()

    End Sub
    Public Sub ImportarArchivoDBF()

        Dim bEntro As Boolean = False
        Dim oImporta As New Comunes.clsImportaDbfs
        Dim oLector As OdbcDataReader
        Dim conteo As Integer = 0
        Dim municipio As Long
        Dim clave_empleado As Long
        Dim nombres As String = ""
        Dim apellidos As String = ""
        Dim i As Long = 0

        Dim Claves As String = ""
        Dim importes As String = ""
        Dim sDatosImportados As String = ""
        Dim sSQL_Tabla As String
        Dim sSQL_Insert As String

        Try

            'Creo la tabla que se va utilizar con los datos importados
            sDatosImportados = "tmp_datos_importados" & Format(Now, "hhmmss")
            sSQL_Tabla = "create table " + sDatosImportados + "( clave_identificacion varchar(30), importe money ) "

            TINApp.Connection.RunQuery(sSQL_Tabla)

            oImporta.Directorio = lkpDirectorio.EditValue
            oImporta.Archivo = Me.txtArchivo.Text

            oImporta.Consulta = "SELECT rfc, nom_comp, clave_emp,  plaza, centro_tra,  municipio, importe FROM " ' Consulta_Dependencia

            oLector = oImporta.RealizaConsulta
            Do While oLector.Read
                bEntro = True
                clave_identificacion = ""

                If CType(oLector("municipio"), String).Trim.Length = 0 Then
                    municipio = -1
                Else

                    municipio = CType(oLector("municipio"), Long)
                End If

                If CType(oLector("clave_emp"), String).Trim.Length = 0 Then
                    clave_empleado = -1
                Else
                    clave_empleado = CType(oLector("clave_emp"), Long)
                End If
                'MsgBox(CType((Me.odatatable.Rows.Count - 1), String))

                For i = 0 To Me.odatatable.Rows.Count - 1

                    Select Case CType(Me.odatatable.Rows(i).Item("campo"), String).ToUpper
                        Case "RFC"
                            clave_identificacion = clave_identificacion + CType(oLector("rfc"), String)

                        Case "PLAZA"
                            CType(oLector("plaza"), Integer).ToString()
                            clave_identificacion = clave_identificacion + CType(oLector("plaza"), Integer).ToString()

                        Case "NOMBRE_COMPLETO"
                            clave_identificacion = clave_identificacion + CType(oLector("nom_comp"), String)

                        Case "CLAVE_EMPLEADO"
                            clave_identificacion = clave_identificacion + clave_empleado.ToString

                        Case "CENTRO_TRABAJO"
                            clave_identificacion = clave_identificacion + CType(oLector("centro_tra"), String)

                        Case "MUNICIPIO"
                            clave_identificacion = clave_identificacion + municipio.ToString

                    End Select
                    'MsgBox(clave_identificacion)
                Next

                conteo += 1

                sSQL_Insert = "insert into " + sDatosImportados + " values ('" + clave_identificacion + "'," + CType(CType(oLector("importe"), Double), String) + ")"
                TINApp.Connection.RunQuery(sSQL_Insert)


            Loop

            oImporta.Terminar()
            oFormWait.Close()

            response_proceso = oArchivoImportado.TraerMovimientosImportados(Sucursal, convenio, Quincena_envio, Anio_envio, sDatosImportados)
            If Not response_proceso.ErrorFound Then
                Me.grArchivosRecibidos.DataSource = CType(response_proceso.Value, DataSet).Tables(0)
            End If


        Catch ex As Exception
            oImporta.Terminar()
            oLector = Nothing
            oImporta = Nothing
            oFormWait.Close()
            ShowMessage(MessageType.MsgInformation, "No se encuentra el Archivo en el Directorio Especificado", Me.Title)

        Finally

        End Try



    End Sub
    'Public Sub ObtieneDatos()
    '    Dim response As Events
    '    Dim data_articulos As DataSet

    '    response = oDependenciasImportacion.TraerMovimientosImportados(Me.convenio, Me.spinQuincena.Value, Me.spinAnio.Value) ', Me.cbotipo.Value)

    '    If Not response.ErrorFound Then
    '        data_articulos = response.Value
    '        response = DespliegaLetras(data_articulos)
    '    End If
    'End Sub
    Private Function DespliegaLetras(ByVal Data As DataSet) As Events
        Dim odataset As New DataSet
        Dim response As Events

        odataset = Data
        Me.grArchivosRecibidos.DataSource = odataset.Tables(0)

        Return response
    End Function
    Private Function GuardaMovimientos()
        Dim response As Events
        Dim i As Integer

        For i = 0 To Me.grvArchivosRecibidos.RowCount - 1
            Try
                response = oArchivoImportado.ActualizaMovimientosImportados(convenio, Me.spinQuincena.EditValue, Me.spinAnio.EditValue, Me.lkpSucursal.EditValue, Me.grvArchivosRecibidos.GetRowCellValue(i, Me.grcClaveCliente), Me.grvArchivosRecibidos.GetRowCellValue(i, Me.grcImporteDescontado))
            Catch ex As Exception
            End Try
            If response.ErrorFound Then
                response.ShowError()
                MsgBox("No se Guardaron los Movimientos")
            End If


        Next


    End Function

#End Region

End Class
