Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Structure ClientesDeudasLetras
    Dim Indice As Integer
    Dim Letras As DataSet
End Structure

Public Class frmExportarArchivo
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents lkpDependencia As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbotipo As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents spinQuincena As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents spinAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents grvEnviosDependencias As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tmaEnviosDependencias As Dipros.Windows.TINMaster
    Friend WithEvents grEnviosDependencias As DevExpress.XtraGrid.GridControl
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcClaveCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNombreCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepcboTipo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents tbrExportar As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblDirectorio As System.Windows.Forms.Label
    Friend WithEvents lkpDirectorio As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporteOriginal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcTipoOriginal As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExportarArchivo))
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.lkpDependencia = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.cbotipo = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.spinAnio = New DevExpress.XtraEditors.SpinEdit
        Me.spinQuincena = New DevExpress.XtraEditors.SpinEdit
        Me.grEnviosDependencias = New DevExpress.XtraGrid.GridControl
        Me.grvEnviosDependencias = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNombreCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepcboTipo = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporteOriginal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcTipoOriginal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaEnviosDependencias = New Dipros.Windows.TINMaster
        Me.tbrExportar = New System.Windows.Forms.ToolBarButton
        Me.lblDirectorio = New System.Windows.Forms.Label
        Me.lkpDirectorio = New DevExpress.XtraEditors.ButtonEdit
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        CType(Me.cbotipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.spinAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spinQuincena.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grEnviosDependencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvEnviosDependencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepcboTipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkpDirectorio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbrExportar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(5608, 28)
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(24, 67)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenio.TabIndex = 2
        Me.lblConvenio.Tag = ""
        Me.lblConvenio.Text = "&Convenio:"
        Me.lblConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDependencia
        '
        Me.lkpDependencia.AllowAdd = False
        Me.lkpDependencia.AutoReaload = False
        Me.lkpDependencia.DataSource = Nothing
        Me.lkpDependencia.DefaultSearchField = ""
        Me.lkpDependencia.DisplayMember = "descripcion"
        Me.lkpDependencia.EditValue = Nothing
        Me.lkpDependencia.Filtered = False
        Me.lkpDependencia.InitValue = Nothing
        Me.lkpDependencia.Location = New System.Drawing.Point(88, 64)
        Me.lkpDependencia.MultiSelect = False
        Me.lkpDependencia.Name = "lkpDependencia"
        Me.lkpDependencia.NullText = ""
        Me.lkpDependencia.PopupWidth = CType(310, Long)
        Me.lkpDependencia.Required = False
        Me.lkpDependencia.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDependencia.SearchMember = ""
        Me.lkpDependencia.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDependencia.SelectAll = False
        Me.lkpDependencia.Size = New System.Drawing.Size(376, 22)
        Me.lkpDependencia.TabIndex = 3
        Me.lkpDependencia.Tag = ""
        Me.lkpDependencia.ToolTip = "convenio"
        Me.lkpDependencia.ValueMember = "dependencia"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.BackColor = System.Drawing.SystemColors.Window
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Enabled = False
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpSucursal.ForeColor = System.Drawing.Color.Black
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(88, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(376, 22)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal_entrada"
        Me.lkpSucursal.ToolTip = "sucursal"
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(28, 43)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbotipo
        '
        Me.cbotipo.EditValue = "T"
        Me.cbotipo.Location = New System.Drawing.Point(544, 40)
        Me.cbotipo.Name = "cbotipo"
        '
        'cbotipo.Properties
        '
        Me.cbotipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbotipo.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Todos", "T", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Altas", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Bajas", "B", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Modificaciones", "M", -1)})
        Me.cbotipo.Size = New System.Drawing.Size(104, 22)
        Me.cbotipo.TabIndex = 11
        Me.cbotipo.ToolTip = "tipo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(504, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "&Tipo:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(44, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "&Quincena:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(233, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "A&�o:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.spinAnio)
        Me.GroupBox1.Controls.Add(Me.spinQuincena)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(88, 89)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(376, 48)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Env�o"
        '
        'spinAnio
        '
        Me.spinAnio.EditValue = New Decimal(New Integer() {2006, 0, 0, 0})
        Me.spinAnio.Location = New System.Drawing.Point(268, 16)
        Me.spinAnio.Name = "spinAnio"
        '
        'spinAnio.Properties
        '
        Me.spinAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spinAnio.Properties.MaxValue = New Decimal(New Integer() {2015, 0, 0, 0})
        Me.spinAnio.Properties.MinValue = New Decimal(New Integer() {1990, 0, 0, 0})
        Me.spinAnio.Properties.UseCtrlIncrement = False
        Me.spinAnio.Size = New System.Drawing.Size(64, 22)
        Me.spinAnio.TabIndex = 7
        Me.spinAnio.ToolTip = "a�o"
        '
        'spinQuincena
        '
        Me.spinQuincena.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.spinQuincena.Location = New System.Drawing.Point(108, 16)
        Me.spinQuincena.Name = "spinQuincena"
        '
        'spinQuincena.Properties
        '
        Me.spinQuincena.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spinQuincena.Properties.MaxValue = New Decimal(New Integer() {24, 0, 0, 0})
        Me.spinQuincena.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.spinQuincena.Properties.UseCtrlIncrement = False
        Me.spinQuincena.Size = New System.Drawing.Size(64, 22)
        Me.spinQuincena.TabIndex = 5
        Me.spinQuincena.ToolTip = "quincena"
        '
        'grEnviosDependencias
        '
        '
        'grEnviosDependencias.EmbeddedNavigator
        '
        Me.grEnviosDependencias.EmbeddedNavigator.Name = ""
        Me.grEnviosDependencias.Location = New System.Drawing.Point(9, 200)
        Me.grEnviosDependencias.MainView = Me.grvEnviosDependencias
        Me.grEnviosDependencias.Name = "grEnviosDependencias"
        Me.grEnviosDependencias.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepcboTipo})
        Me.grEnviosDependencias.Size = New System.Drawing.Size(640, 272)
        Me.grEnviosDependencias.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grEnviosDependencias.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grEnviosDependencias.TabIndex = 8
        Me.grEnviosDependencias.Text = "EnviosDependencias"
        '
        'grvEnviosDependencias
        '
        Me.grvEnviosDependencias.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveCliente, Me.grcNombreCliente, Me.grcImporte, Me.grcTipo, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.grcImporteOriginal, Me.grcTipoOriginal})
        Me.grvEnviosDependencias.GridControl = Me.grEnviosDependencias
        Me.grvEnviosDependencias.Name = "grvEnviosDependencias"
        Me.grvEnviosDependencias.OptionsCustomization.AllowFilter = False
        Me.grvEnviosDependencias.OptionsCustomization.AllowGroup = False
        Me.grvEnviosDependencias.OptionsCustomization.AllowSort = False
        Me.grvEnviosDependencias.OptionsView.ShowGroupPanel = False
        '
        'grcClaveCliente
        '
        Me.grcClaveCliente.Caption = "Cuenta"
        Me.grcClaveCliente.FieldName = "cliente"
        Me.grcClaveCliente.Name = "grcClaveCliente"
        Me.grcClaveCliente.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcClaveCliente.SortIndex = 0
        Me.grcClaveCliente.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcClaveCliente.VisibleIndex = 0
        Me.grcClaveCliente.Width = 77
        '
        'grcNombreCliente
        '
        Me.grcNombreCliente.Caption = "Nombre"
        Me.grcNombreCliente.FieldName = "nombre_cliente"
        Me.grcNombreCliente.Name = "grcNombreCliente"
        Me.grcNombreCliente.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNombreCliente.VisibleIndex = 1
        Me.grcNombreCliente.Width = 197
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "c2"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 3
        Me.grcImporte.Width = 153
        '
        'grcTipo
        '
        Me.grcTipo.Caption = "Tipo"
        Me.grcTipo.ColumnEdit = Me.RepcboTipo
        Me.grcTipo.FieldName = "tipo"
        Me.grcTipo.Name = "grcTipo"
        Me.grcTipo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipo.SortIndex = 1
        Me.grcTipo.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.grcTipo.VisibleIndex = 2
        Me.grcTipo.Width = 98
        '
        'RepcboTipo
        '
        Me.RepcboTipo.AutoHeight = False
        Me.RepcboTipo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepcboTipo.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Alta", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Baja", "B", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Modificacion", "M", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("None", "N", -1)})
        Me.RepcboTipo.Name = "RepcboTipo"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "rfc"
        Me.GridColumn1.FieldName = "rfc"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "qna_ini"
        Me.GridColumn2.FieldName = "qna_ini"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "qna_fin"
        Me.GridColumn3.FieldName = "qna_fin"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "mensual"
        Me.GridColumn4.FieldName = "mensual"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "municipio"
        Me.GridColumn5.FieldName = "municipio"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "desc_municipio"
        Me.GridColumn6.FieldName = "desc_municipio"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcImporteOriginal
        '
        Me.grcImporteOriginal.Caption = "Importe Original"
        Me.grcImporteOriginal.DisplayFormat.FormatString = "c2"
        Me.grcImporteOriginal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporteOriginal.FieldName = "importe_original"
        Me.grcImporteOriginal.Name = "grcImporteOriginal"
        Me.grcImporteOriginal.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporteOriginal.Width = 98
        '
        'grcTipoOriginal
        '
        Me.grcTipoOriginal.Caption = "Tipo Original"
        Me.grcTipoOriginal.FieldName = "tipo_original"
        Me.grcTipoOriginal.Name = "grcTipoOriginal"
        Me.grcTipoOriginal.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'tmaEnviosDependencias
        '
        Me.tmaEnviosDependencias.BackColor = System.Drawing.Color.White
        Me.tmaEnviosDependencias.CanDelete = True
        Me.tmaEnviosDependencias.CanInsert = True
        Me.tmaEnviosDependencias.CanUpdate = True
        Me.tmaEnviosDependencias.Grid = Me.grEnviosDependencias
        Me.tmaEnviosDependencias.Location = New System.Drawing.Point(9, 176)
        Me.tmaEnviosDependencias.Name = "tmaEnviosDependencias"
        Me.tmaEnviosDependencias.Size = New System.Drawing.Size(640, 23)
        Me.tmaEnviosDependencias.TabIndex = 12
        Me.tmaEnviosDependencias.Title = ""
        Me.tmaEnviosDependencias.UpdateTitle = "un Registro"
        '
        'tbrExportar
        '
        Me.tbrExportar.Text = "Exportar Informaci�n"
        Me.tbrExportar.ToolTipText = "Exporta a un archivo la informaci�n desplegada"
        '
        'lblDirectorio
        '
        Me.lblDirectorio.AutoSize = True
        Me.lblDirectorio.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDirectorio.Location = New System.Drawing.Point(16, 147)
        Me.lblDirectorio.Name = "lblDirectorio"
        Me.lblDirectorio.Size = New System.Drawing.Size(125, 16)
        Me.lblDirectorio.TabIndex = 8
        Me.lblDirectorio.Text = "Seleccione &Directorio:"
        Me.lblDirectorio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDirectorio
        '
        Me.lkpDirectorio.EditValue = ""
        Me.lkpDirectorio.Location = New System.Drawing.Point(144, 144)
        Me.lkpDirectorio.Name = "lkpDirectorio"
        '
        'lkpDirectorio.Properties
        '
        Me.lkpDirectorio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.lkpDirectorio.Size = New System.Drawing.Size(320, 22)
        Me.lkpDirectorio.TabIndex = 9
        Me.lkpDirectorio.ToolTip = "Ruta del Directorio en donde se guardar� el archivo o archivos a generar"
        '
        'frmExportarArchivo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(658, 480)
        Me.Controls.Add(Me.lblDirectorio)
        Me.Controls.Add(Me.grEnviosDependencias)
        Me.Controls.Add(Me.lkpDirectorio)
        Me.Controls.Add(Me.tmaEnviosDependencias)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbotipo)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblConvenio)
        Me.Controls.Add(Me.lkpDependencia)
        Me.Name = "frmExportarArchivo"
        Me.Text = "frmExportarArchivo"
        Me.Controls.SetChildIndex(Me.lkpDependencia, 0)
        Me.Controls.SetChildIndex(Me.lblConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.cbotipo, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.tmaEnviosDependencias, 0)
        Me.Controls.SetChildIndex(Me.lkpDirectorio, 0)
        Me.Controls.SetChildIndex(Me.grEnviosDependencias, 0)
        Me.Controls.SetChildIndex(Me.lblDirectorio, 0)
        CType(Me.cbotipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.spinAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spinQuincena.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grEnviosDependencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvEnviosDependencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepcboTipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkpDirectorio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Dipros Systems, Declaraciones "

    Private oDependencias As New VillarrealBusiness.clsDependencias
    Private odependenciasdetalle As New VillarrealBusiness.clsDependenciasDetalleExportacion
    Private oReportes As New VillarrealBusiness.Reportes
    Private oSucursales As New VillarrealBusiness.clsSucursales
    Private oEnviosConvenios As New VillarrealBusiness.clsEnviosConvenios
    Private oEnviosConveniosDetalle As New VillarrealBusiness.clsEnviosConveniosDetalle

    Private EstructuraLetras As ArrayList
    Private InfoGuardada As Boolean = False
    Private aceptar As Boolean = False
    Private lquincena_envio_actual As Long = 0
    Private lanio_envio_actual As Long = 0

#End Region

#Region "Dipros Systems, Propiedades"
    Private ReadOnly Property convenio() As Long
        Get
            Return PreparaValorLookup(Me.lkpDependencia)
        End Get
    End Property
    Public ReadOnly Property Quincena_envio() As Long
        Get
            Return Me.spinQuincena.Value
        End Get
    End Property
    Public ReadOnly Property Anio_envio() As Long
        Get
            Return Me.spinAnio.Value
        End Get
    End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private Property Quincena_Envio_Actual() As Long
        Get
            Return lquincena_envio_actual
        End Get
        Set(ByVal Value As Long)
            lquincena_envio_actual = Value
        End Set
    End Property
    Private Property Anio_Envio_Actual() As Long
        Get
            Return lanio_envio_actual
        End Get
        Set(ByVal Value As Long)
            lanio_envio_actual = Value
        End Set
    End Property
    Public Property ClientesLetras(ByVal Indice As Integer) As ClientesDeudasLetras
        Get
            Dim AuxArticulos As ClientesDeudasLetras
            For i As Integer = 0 To EstructuraLetras.Count - 1
                AuxArticulos = EstructuraLetras(i)
                'revisa si existe
                If AuxArticulos.Indice = Indice Then
                    Return AuxArticulos
                    Exit For
                End If
            Next
        End Get
        Set(ByVal Value As ClientesDeudasLetras)
            If Not ExisteCliente(Value.Indice) Then
                EstructuraLetras.Add(Value)
            Else
                EstructuraLetras(IndexArticulo(Value.Indice)) = Value
            End If
        End Set
    End Property
#End Region

#Region "Dipros Systems, Eventos de la Forma"

    Private Sub frmExportarArchivo_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub
    Private Sub frmExportarArchivo_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
        ImprimirReporte()
    End Sub
    Private Sub frmExportarArchivo_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmExportarArchivo_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oDependencias.Validacion(Me.lkpSucursal.EditValue, Me.lkpDependencia.EditValue, Me.grvEnviosDependencias.DataRowCount, Me.Quincena_Envio_Actual, Me.Quincena_envio, Me.Anio_Envio_Actual, Me.Anio_envio)
    End Sub
    Private Sub frmExportarArchivo_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.Location = New Point(0, 0)
        VerficaGuardado()

        Me.lkpSucursal.EditValue = Comunes.Common.Sucursal_Actual
        Me.lkpSucursal.Enabled = False
        Try
            Me.spinQuincena.EditValue = Me.lkpSucursal.GetValue("quincena_envio")
            Me.spinAnio.EditValue = Me.lkpSucursal.GetValue("anio_envio")
        Catch ex As Exception
        End Try

        With Me.tmaEnviosDependencias
            .UpdateForm = New frmExportarArchivoDocumentos
            .AddColumn("cliente", "System.Int32")
            .AddColumn("nombre_cliente")
            .AddColumn("importe", "System.Double")
            .AddColumn("tipo")
            .AddColumn("importe_original", "System.Double")
            .AddColumn("tipo_original")

            .AddColumn("rfc")
            .AddColumn("qna_ini")
            .AddColumn("qna_fin")
            .AddColumn("mensual", "System.Double")
            .AddColumn("municipio")
            .AddColumn("desc_municipio")

        End With

        'Try

        '    'Me.spinQuincena.ResetText()
        '    Me.spinQuincena.Text = Me.lkpSucursal.GetValue("quincena_envio")
        '    Me.spinAnio.Text = Me.lkpSucursal.GetValue("anio_envio")
        '    'Me.spinAnio.ResetText()
        '    'Me.spinQuincena.EditValue = Me.lkpSucursal.GetValue("quincena_envio")
        '    'Me.spinAnio.EditValue = Me.lkpSucursal.GetValue("anio_envio")
        'Catch ex As Exception
        'End Try

        EstructuraLetras = New ArrayList

    End Sub
    'Private Sub frmExportarArchivo_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
    '    Me.spinQuincena.EditValue = Me.lkpSucursal.GetValue("quincena_envio")
    '    Me.spinQuincena.Text = Me.lkpSucursal.GetValue("quincena_envio")
    '    Me.spinAnio.EditValue = Me.lkpSucursal.GetValue("anio_envio")
    '    Me.spinAnio.Text = Me.lkpSucursal.GetValue("anio_envio")
    'End Sub
    Private Sub frmExportarArchivo_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

        aceptar = True

        Dim i As Integer

        For i = 0 To Me.grvEnviosDependencias.RowCount - 1

            Response = oEnviosConvenios.Insertar(Sucursal, Me.lkpDependencia.EditValue, Me.spinQuincena.EditValue, Me.spinAnio.EditValue, Me.grvEnviosDependencias.GetRowCellValue(i, Me.grcClaveCliente), Me.grvEnviosDependencias.GetRowCellValue(i, Me.grcImporte), Me.grvEnviosDependencias.GetRowCellValue(i, Me.grcTipo), False, CDate("01/01/1900").Date, 0)

            If Not Response.ErrorFound Then
                Dim Aux As ClientesDeudasLetras
                Dim iRow As Integer
                ' Inserto el Detalle de Precios de las Entradas
                Aux = Me.ClientesLetras(Me.grvEnviosDependencias.GetRowCellValue(i, Me.grcClaveCliente))
                For Each oRow As DataRow In Aux.Letras.Tables(0).Rows
                    Response = oEnviosConveniosDetalle.Insertar(Sucursal, Me.lkpDependencia.EditValue, Me.spinQuincena.EditValue, Me.spinAnio.EditValue, oRow.Item("cliente"), oRow.Item("partida"), oRow.Item("sucursal"), oRow.Item("concepto"), oRow.Item("serie"), oRow.Item("folio"), oRow.Item("cliente"), oRow.Item("documento"), oRow.Item("importe"), 0, 0)
                Next
            End If
        Next

        If Not Response.ErrorFound Then
            InfoGuardada = True
            aceptar = False
            VerficaGuardado()

        End If

    End Sub
#End Region

#Region "Dipros Systems, Eventos de los Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        If Sucursal > 0 Then
            Quincena_Envio_Actual = Me.lkpSucursal.GetValue("quincena_envio")
            Anio_Envio_Actual = Me.lkpSucursal.GetValue("anio_envio")
        End If
    End Sub

    Private Sub lkpDependencia_Format() Handles lkpDependencia.Format
        Comunes.clsFormato.for_dependencias_grl(Me.lkpDependencia)
    End Sub
    Private Sub lkpDependencia_LoadData(ByVal Initialize As Boolean) Handles lkpDependencia.LoadData
        Dim Response As New Events
        Response = oDependencias.Lookup()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDependencia.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpDependencia_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDependencia.EditValueChanged
        If aceptar = False Then ObtieneDatos()
    End Sub
    Private Sub spinQuincena_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spinQuincena.EditValueChanged, spinAnio.EditValueChanged
        If CType(sender, DevExpress.XtraEditors.SpinEdit).IsLoading = True Or aceptar = True Then Exit Sub
        ObtieneDatos()
    End Sub
    Private Sub cbotipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbotipo.SelectedIndexChanged
        If Me.cbotipo.IsLoading = True Then Exit Sub

        'Me.tmaEnviosDependencias.DataSource.Tables(0).DefaultView.RowFilter = "tipo = '" + Me.cbotipo.Value + "'"
        If Me.cbotipo.Value <> "T" Then
            CType(Me.grEnviosDependencias.DataSource, DataView).RowFilter = "tipo = '" + Me.cbotipo.Value + "'"
        Else
            CType(Me.grEnviosDependencias.DataSource, DataView).RowFilter = ""
        End If

    End Sub

    Private Sub tmaEnviosDependencias_ValidateFields(ByRef Cancel As Boolean) Handles tmaEnviosDependencias.ValidateFields

        Select Case tmaEnviosDependencias.Action
            Case Actions.Delete
                If Me.tmaEnviosDependencias.SelectedRow.Tables(0).Rows(0).Item("tipo") = "M" Or Me.tmaEnviosDependencias.SelectedRow.Tables(0).Rows(0).Item("tipo") = "A" Then
                    Cancel = True
                    ShowMessage(MessageType.MsgInformation, "No se puede Eliminar")
                End If

        End Select

        'If Me.tmaEnviosDependencias.SelectedRow.Tables(0).Rows(0).Item("tipo") = "M" And Me.tmaEnviosDependencias.Action = Actions.Delete Then
        '    Cancel = True
        'End If

    End Sub
    Private Sub lkpDirectorio_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkpDirectorio.Click
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            lkpDirectorio.EditValue = FolderBrowserDialog1.SelectedPath
        End If
    End Sub
    Private Sub frmExportarArchivo_ToolBarClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles MyBase.ToolBarClick
        If e.Button Is Me.tbrExportar Then
            If ValidaDirectorio() Then Exit Sub
            Try
                If ExportarArchivo() Then
                    ShowMessage(MessageType.MsgInformation, "�El Archivo fue generado exitosamente!", "Exportar a Excel")

                    Dim response As Events
                    response = oEnviosConvenios.ActualizaFechaEnvios(Sucursal, Me.spinAnio.Value)
                    If response.ErrorFound Then
                        ShowMessage(MessageType.MsgError, "Error al generar actualizar fechas de envio", "Atenci�n", response.Ex)
                    End If

                End If
            Catch ex As Exception
                ShowMessage(MessageType.MsgError, "Error al generar archivo", "Atenci�n", ex)
            End Try

        End If
    End Sub

#End Region

#Region "Dipros Systems, funcionalidad"
    Public Sub ObtieneDatos()
        Dim response As Events
        Dim data_articulos As DataSet

        response = odependenciasdetalle.TraerMovimientosParaEnvio(Me.convenio, Me.spinQuincena.Value, Me.spinAnio.Value, "T")

        If Not response.ErrorFound Then
            data_articulos = response.Value
            response = DespliegaLetras(data_articulos)
        End If


    End Sub
    Private Function DespliegaLetras(ByVal Data As DataSet) As Events
        Dim odataset As New DataSet
        Dim response As Events
        Dim AuxPrecios As ClientesDeudasLetras

        EstructuraLetras.Clear()
        odataset = Data
        Me.tmaEnviosDependencias.DataSource = odataset

        Me.tmaEnviosDependencias.MoveFirst()

        Do While Not Me.tmaEnviosDependencias.EOF
            AuxPrecios = New ClientesDeudasLetras

            response = odependenciasdetalle.TraerMovimientosParaEnvioDocumentos(convenio, Me.spinQuincena.Value, Me.spinAnio.Value, tmaEnviosDependencias.Item("cliente"))

            If Not response.ErrorFound Then
                odataset = response.Value
                AuxPrecios.Indice = tmaEnviosDependencias.Item("cliente")  'tmaEnviosDependencias.Item("partida")
                AuxPrecios.Letras = odataset
                Me.EstructuraLetras.Add(AuxPrecios)
            End If
            Me.tmaEnviosDependencias.MoveNext()
        Loop

        Return response
    End Function
    Private Sub VerficaGuardado()
        Me.tbrExportar.Enabled = InfoGuardada

        Me.lkpDependencia.Enabled = Not InfoGuardada
        Me.tbrTools.Buttons(0).Enabled = Not InfoGuardada
        Me.spinAnio.Enabled = Not InfoGuardada
        Me.spinQuincena.Enabled = Not InfoGuardada
        Me.tmaEnviosDependencias.Enabled = Not InfoGuardada
        Me.grEnviosDependencias.Enabled = Not InfoGuardada

    End Sub
    Private Sub ImprimirReporte()
        Dim response As New Events
        response = oReportes.TraerMovimientosParaEnvio(Me.lkpDependencia.EditValue, Me.spinQuincena.EditValue, Me.spinAnio.EditValue, Me.cbotipo.EditValue)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Reporte de Exportar Archivo no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New rptExportarArchivoQuincenal
                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)
                oReport.oDatatable = oDataSet.Tables(1)
                TINApp.ShowReport(Me.MdiParent, "Exportar Archivo", oReport)
                oDataSet = Nothing
                oReport = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If
    End Sub
    Private Function ValidaDirectorio() As Boolean
        If lkpDirectorio.Text.Length = 0 Then
            ShowMessage(MessageType.MsgInformation, "El Directorio es Requerido", "Mensaje")
            ValidaDirectorio = True
        Else
            ValidaDirectorio = False
        End If
    End Function
    Private Function ExportarArchivo() As Boolean
        'Create a new workbook in Excel.
        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object
        Dim iRow As Integer
        Dim iCol As Integer
        Dim i As Integer
        Dim Table As DataTable

        Dim dFechainicial As Date
        Dim dFechafinal As Date
        Dim File As String

        ExportarArchivo = False

        Try
            Table = CType(Me.grEnviosDependencias.DataSource, DataView).Table

            If lkpDirectorio.Text.LastIndexOf("\") = lkpDirectorio.Text.Length - 1 Then
                File = CType(lkpDirectorio.EditValue, String) & "EXPORTAR_ENVIOS.XLS"
            Else
                File = CType(lkpDirectorio.EditValue, String) & "\EXPORTAR_ENVIOS.XLS"
            End If
            oExcel = CreateObject("Excel.Application")
            oBook = oExcel.Workbooks.Add

            oSheet = oBook.Worksheets(1)
            oSheet.name = "PRCAJTRI"

            oSheet.Cells(1, 1).Value = "RFC"
            oSheet.Cells(1, 2).Value = "NOMBRE"
            oSheet.Cells(1, 3).Value = "MUNICIPIO"
            oSheet.Cells(1, 4).Value = "DESC_MUNICIPIO"
            oSheet.Cells(1, 5).Value = "QNA_INI"
            oSheet.Cells(1, 6).Value = "QNA_FIN"
            oSheet.Cells(1, 7).Value = "MENSUAL"
            oSheet.Cells(1, 8).Value = "IMPORTE"
            oSheet.Cells(1, 9).Value = "TIPOMOV"
            oSheet.Cells(1, 10).Value = "TIPAG"


            With Table
                For iRow = 0 To .Rows.Count - 1
                    For iCol = 0 To 9

                        Select Case iCol
                            Case 0
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("rfc")
                            Case 1
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("nombre_cliente")
                            Case 2
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("municipio")
                            Case 3
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("desc_municipio")
                            Case 4
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("qna_ini")
                            Case 5
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("qna_fin")
                            Case 6
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("mensual")
                            Case 7
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("importe")
                            Case 8
                                oSheet.Cells(iRow + 2, iCol + 1).value = Table.Rows(iRow)("tipo")
                                'Case 9
                                'oSheet.Cells(iRow + 2, iCol + 1).value = Data.Tables(0).Rows(iRow)("tipo_pago")
                        End Select

                    Next
                Next
            End With

            oSheet.Range("A1").AutoFormat()
            oBook.Worksheets(2).visible = False
            oBook.Worksheets(3).visible = False

            oBook.SaveAs(File)

            'Bandera para saber si fue genereado exitosamente
            ExportarArchivo = True
        Catch ex As Exception
            ExportarArchivo = False
        Finally
            oSheet = Nothing
            oBook = Nothing
            oExcel.Quit()
            oExcel = Nothing
        End Try



    End Function
    Private Function ExisteCliente(ByVal Indice As Integer) As Boolean
        Dim AuxArticulosPrecios As ClientesDeudasLetras
        For i As Integer = 0 To EstructuraLetras.Count - 1
            AuxArticulosPrecios = EstructuraLetras(i)
            'revisa si existe el cliente
            If AuxArticulosPrecios.Indice = Indice Then
                ExisteCliente = True
                Exit Function
            End If
        Next
    End Function
    Private Function IndexArticulo(ByVal Indice As Integer) As Integer
        Dim AuxExamenes As ClientesDeudasLetras
        For i As Integer = 0 To EstructuraLetras.Count - 1
            AuxExamenes = EstructuraLetras(i)

            If AuxExamenes.Indice = Indice Then
                IndexArticulo = i
                Exit Function
            End If
        Next
    End Function

#End Region

End Class
