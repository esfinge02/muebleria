Imports Dipros.Utils
Imports Dipros.Utils.Common
Public Class frmExportarArchivoLetrasDetalle
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents clcConcepto As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents clcSucursal As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents clcFolio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDocumento_ As System.Windows.Forms.Label
    Friend WithEvents clcDocumento As Dipros.Editors.TINCalcEdit
    Friend WithEvents clcSerie As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcQuincena As Dipros.Editors.TINCalcEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clcAnio As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents clcCliente As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents lkpDocumento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDocumento2 As System.Windows.Forms.Label
    Friend WithEvents clcPartida As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExportarArchivoLetrasDetalle))
        Me.lblPartida = New System.Windows.Forms.Label
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblConcepto = New System.Windows.Forms.Label
        Me.clcConcepto = New Dipros.Editors.TINCalcEdit
        Me.lblSerie = New System.Windows.Forms.Label
        Me.clcSerie = New Dipros.Editors.TINCalcEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.clcSucursal = New Dipros.Editors.TINCalcEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblFolio = New System.Windows.Forms.Label
        Me.clcFolio = New Dipros.Editors.TINCalcEdit
        Me.lblDocumento_ = New System.Windows.Forms.Label
        Me.clcDocumento = New Dipros.Editors.TINCalcEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcQuincena = New Dipros.Editors.TINCalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.clcAnio = New Dipros.Editors.TINCalcEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.clcCliente = New Dipros.Editors.TINCalcEdit
        Me.lkpDocumento = New Dipros.Editors.TINMultiLookup
        Me.lblDocumento2 = New System.Windows.Forms.Label
        Me.clcPartida = New Dipros.Editors.TINCalcEdit
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcQuincena.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(0, 50)
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(112, 296)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(48, 16)
        Me.lblPartida.TabIndex = 64
        Me.lblPartida.Tag = ""
        Me.lblPartida.Text = "Partida:"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(33, 66)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 3
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(89, 64)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "n2"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(80, 21)
        Me.clcImporte.TabIndex = 4
        Me.clcImporte.Tag = "importe"
        Me.clcImporte.ToolTip = "importe"
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Location = New System.Drawing.Point(96, 200)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(60, 16)
        Me.lblConcepto.TabIndex = 68
        Me.lblConcepto.Tag = ""
        Me.lblConcepto.Text = "&Concepto:"
        Me.lblConcepto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcConcepto
        '
        Me.clcConcepto.EditValue = "0"
        Me.clcConcepto.Location = New System.Drawing.Point(160, 200)
        Me.clcConcepto.MaxValue = 0
        Me.clcConcepto.MinValue = 0
        Me.clcConcepto.Name = "clcConcepto"
        '
        'clcConcepto.Properties
        '
        Me.clcConcepto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcConcepto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcConcepto.Properties.Enabled = False
        Me.clcConcepto.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcConcepto.Properties.Precision = 2
        Me.clcConcepto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcConcepto.Size = New System.Drawing.Size(80, 21)
        Me.clcConcepto.TabIndex = 69
        Me.clcConcepto.Tag = "concepto"
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(120, 224)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie.TabIndex = 70
        Me.lblSerie.Tag = ""
        Me.lblSerie.Text = "&Serie:"
        Me.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSerie
        '
        Me.clcSerie.EditValue = "0"
        Me.clcSerie.Location = New System.Drawing.Point(160, 224)
        Me.clcSerie.MaxValue = 0
        Me.clcSerie.MinValue = 0
        Me.clcSerie.Name = "clcSerie"
        '
        'clcSerie.Properties
        '
        Me.clcSerie.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSerie.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSerie.Properties.Enabled = False
        Me.clcSerie.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSerie.Properties.Precision = 2
        Me.clcSerie.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSerie.Size = New System.Drawing.Size(80, 21)
        Me.clcSerie.TabIndex = 71
        Me.clcSerie.Tag = "serie"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(96, 176)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 72
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "&Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSucursal
        '
        Me.clcSucursal.EditValue = "0"
        Me.clcSucursal.Location = New System.Drawing.Point(160, 176)
        Me.clcSucursal.MaxValue = 0
        Me.clcSucursal.MinValue = 0
        Me.clcSucursal.Name = "clcSucursal"
        '
        'clcSucursal.Properties
        '
        Me.clcSucursal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSucursal.Properties.Enabled = False
        Me.clcSucursal.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcSucursal.Properties.Precision = 2
        Me.clcSucursal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcSucursal.Size = New System.Drawing.Size(80, 21)
        Me.clcSucursal.TabIndex = 73
        Me.clcSucursal.Tag = "sucursal"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 16)
        Me.Label5.TabIndex = 66
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Importe:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(120, 248)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio.TabIndex = 74
        Me.lblFolio.Tag = ""
        Me.lblFolio.Text = "&Folio:"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = "0"
        Me.clcFolio.Location = New System.Drawing.Point(160, 248)
        Me.clcFolio.MaxValue = 0
        Me.clcFolio.MinValue = 0
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcFolio.Properties.Precision = 2
        Me.clcFolio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio.Size = New System.Drawing.Size(80, 21)
        Me.clcFolio.TabIndex = 75
        Me.clcFolio.Tag = "folio"
        '
        'lblDocumento_
        '
        Me.lblDocumento_.AutoSize = True
        Me.lblDocumento_.Location = New System.Drawing.Point(80, 272)
        Me.lblDocumento_.Name = "lblDocumento_"
        Me.lblDocumento_.Size = New System.Drawing.Size(72, 16)
        Me.lblDocumento_.TabIndex = 76
        Me.lblDocumento_.Tag = ""
        Me.lblDocumento_.Text = "&Documento:"
        Me.lblDocumento_.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcDocumento
        '
        Me.clcDocumento.EditValue = "0"
        Me.clcDocumento.Location = New System.Drawing.Point(160, 272)
        Me.clcDocumento.MaxValue = 0
        Me.clcDocumento.MinValue = 0
        Me.clcDocumento.Name = "clcDocumento"
        '
        'clcDocumento.Properties
        '
        Me.clcDocumento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDocumento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDocumento.Properties.Enabled = False
        Me.clcDocumento.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcDocumento.Properties.Precision = 2
        Me.clcDocumento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDocumento.Size = New System.Drawing.Size(80, 21)
        Me.clcDocumento.TabIndex = 77
        Me.clcDocumento.Tag = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(96, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 16)
        Me.Label2.TabIndex = 79
        Me.Label2.Tag = ""
        Me.Label2.Text = "Quincena:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcQuincena
        '
        Me.clcQuincena.EditValue = "0"
        Me.clcQuincena.Location = New System.Drawing.Point(160, 104)
        Me.clcQuincena.MaxValue = 0
        Me.clcQuincena.MinValue = 0
        Me.clcQuincena.Name = "clcQuincena"
        '
        'clcQuincena.Properties
        '
        Me.clcQuincena.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincena.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcQuincena.Properties.Enabled = False
        Me.clcQuincena.Properties.MaskData.EditMask = "###,###"
        Me.clcQuincena.Properties.Precision = 2
        Me.clcQuincena.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcQuincena.Size = New System.Drawing.Size(80, 21)
        Me.clcQuincena.TabIndex = 80
        Me.clcQuincena.Tag = "quincena"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(128, 128)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 16)
        Me.Label4.TabIndex = 82
        Me.Label4.Tag = ""
        Me.Label4.Text = "A�o:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcAnio
        '
        Me.clcAnio.EditValue = "0"
        Me.clcAnio.Location = New System.Drawing.Point(160, 128)
        Me.clcAnio.MaxValue = 0
        Me.clcAnio.MinValue = 0
        Me.clcAnio.Name = "clcAnio"
        '
        'clcAnio.Properties
        '
        Me.clcAnio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcAnio.Properties.Enabled = False
        Me.clcAnio.Properties.MaskData.Blank = " "
        Me.clcAnio.Properties.Precision = 2
        Me.clcAnio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcAnio.Size = New System.Drawing.Size(80, 21)
        Me.clcAnio.TabIndex = 83
        Me.clcAnio.Tag = "anio"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(104, 152)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 84
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "&Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCliente
        '
        Me.clcCliente.EditValue = "0"
        Me.clcCliente.Location = New System.Drawing.Point(160, 152)
        Me.clcCliente.MaxValue = 0
        Me.clcCliente.MinValue = 0
        Me.clcCliente.Name = "clcCliente"
        '
        'clcCliente.Properties
        '
        Me.clcCliente.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCliente.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCliente.Properties.Enabled = False
        Me.clcCliente.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcCliente.Properties.Precision = 2
        Me.clcCliente.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCliente.Size = New System.Drawing.Size(80, 21)
        Me.clcCliente.TabIndex = 85
        Me.clcCliente.Tag = "cliente"
        '
        'lkpDocumento
        '
        Me.lkpDocumento.AllowAdd = False
        Me.lkpDocumento.AutoReaload = False
        Me.lkpDocumento.BackColor = System.Drawing.SystemColors.Window
        Me.lkpDocumento.DataSource = Nothing
        Me.lkpDocumento.DefaultSearchField = ""
        Me.lkpDocumento.DisplayMember = "documento_despliega"
        Me.lkpDocumento.EditValue = Nothing
        Me.lkpDocumento.Enabled = False
        Me.lkpDocumento.Filtered = False
        Me.lkpDocumento.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkpDocumento.ForeColor = System.Drawing.Color.Black
        Me.lkpDocumento.InitValue = Nothing
        Me.lkpDocumento.Location = New System.Drawing.Point(89, 40)
        Me.lkpDocumento.MultiSelect = False
        Me.lkpDocumento.Name = "lkpDocumento"
        Me.lkpDocumento.NullText = ""
        Me.lkpDocumento.PopupWidth = CType(400, Long)
        Me.lkpDocumento.Required = False
        Me.lkpDocumento.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDocumento.SearchMember = ""
        Me.lkpDocumento.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDocumento.SelectAll = False
        Me.lkpDocumento.Size = New System.Drawing.Size(211, 22)
        Me.lkpDocumento.TabIndex = 2
        Me.lkpDocumento.Tag = "documento"
        Me.lkpDocumento.ToolTip = "documento"
        Me.lkpDocumento.ValueMember = "documento"
        '
        'lblDocumento2
        '
        Me.lblDocumento2.AutoSize = True
        Me.lblDocumento2.Location = New System.Drawing.Point(14, 43)
        Me.lblDocumento2.Name = "lblDocumento2"
        Me.lblDocumento2.Size = New System.Drawing.Size(72, 16)
        Me.lblDocumento2.TabIndex = 1
        Me.lblDocumento2.Tag = ""
        Me.lblDocumento2.Text = "&Documento:"
        Me.lblDocumento2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPartida
        '
        Me.clcPartida.EditValue = "0"
        Me.clcPartida.Location = New System.Drawing.Point(160, 296)
        Me.clcPartida.MaxValue = 0
        Me.clcPartida.MinValue = 0
        Me.clcPartida.Name = "clcPartida"
        '
        'clcPartida.Properties
        '
        Me.clcPartida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPartida.Properties.Enabled = False
        Me.clcPartida.Properties.MaskData.EditMask = "###,###"
        Me.clcPartida.Properties.Precision = 2
        Me.clcPartida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcPartida.Size = New System.Drawing.Size(80, 21)
        Me.clcPartida.TabIndex = 88
        Me.clcPartida.Tag = "partida"
        '
        'frmExportarArchivoLetrasDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(314, 95)
        Me.Controls.Add(Me.clcPartida)
        Me.Controls.Add(Me.lkpDocumento)
        Me.Controls.Add(Me.lblDocumento2)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.clcCliente)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.clcAnio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.clcQuincena)
        Me.Controls.Add(Me.lblDocumento_)
        Me.Controls.Add(Me.clcDocumento)
        Me.Controls.Add(Me.lblFolio)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.clcSucursal)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.clcSerie)
        Me.Controls.Add(Me.lblConcepto)
        Me.Controls.Add(Me.clcConcepto)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.Label5)
        Me.Name = "frmExportarArchivoLetrasDetalle"
        Me.Text = "frmExportarArchivoLetrasDetalle"
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.clcConcepto, 0)
        Me.Controls.SetChildIndex(Me.lblConcepto, 0)
        Me.Controls.SetChildIndex(Me.clcSerie, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.clcSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.lblFolio, 0)
        Me.Controls.SetChildIndex(Me.clcDocumento, 0)
        Me.Controls.SetChildIndex(Me.lblDocumento_, 0)
        Me.Controls.SetChildIndex(Me.clcQuincena, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.clcAnio, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.clcCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblDocumento2, 0)
        Me.Controls.SetChildIndex(Me.lkpDocumento, 0)
        Me.Controls.SetChildIndex(Me.clcPartida, 0)
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcQuincena.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private importe_original As Double = 0
    Private oDocumentos As New VillarrealBusiness.clsDependenciasDetalleExportacion

    Private ReadOnly Property Documento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDocumento)
        End Get
    End Property

#Region "Eventos de la Forma"

    Private Sub frmExportarArchivoLetrasDetalle_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    .AddRow(Me.DataSource)
                Case Actions.Update
                    .UpdateRow(Me.DataSource)
                Case Actions.Delete
                    .DeleteRow()
            End Select
        End With
        OwnerForm.CalculaTotal()
    End Sub
    Private Sub frmExportarArchivoLetrasDetalle_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        Me.clcQuincena.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("quincena")), Long)
        Me.clcAnio.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("anio")), Long)
        Me.clcDocumento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("documento")), Long)
        Me.lkpDocumento_LoadData(True)
        Me.lkpDocumento.EditValue = Me.clcDocumento.EditValue
        If Action = Actions.Delete Then
            Me.lkpDocumento.Enabled() = False
            Me.clcImporte.Enabled = False
        End If
    End Sub
    Private Sub frmExportarArchivoLetrasDetalle_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oDocumentos = New VillarrealBusiness.clsDependenciasDetalleExportacion

        Me.clcQuincena.EditValue = OwnerForm.Quincena_envio
        Me.clcAnio.EditValue = OwnerForm.Anio_envio
        Me.clcCliente.EditValue = OwnerForm.Cliente
        Me.clcSucursal.EditValue = Comunes.Common.Sucursal_Actual
        Me.clcPartida.EditValue = OwnerForm.Partida

        Me.lkpDocumento_LoadData(True)

        Select Case Action
            Case Actions.Insert
                OwnerForm.calculatotal()

            Case Actions.Update
                Me.lkpDocumento.Enabled = False
                cambiaDatos()

            Case Actions.Delete
                Me.lkpDocumento.Enabled = False
                Me.clcImporte.Enabled = False

        End Select
    End Sub
    Private Sub frmMovimientosPagar_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        If Not Action = Actions.Delete Then
            Response = oDocumentos.ValidacionDetalle(Me.clcQuincena.EditValue, Me.clcAnio.EditValue, Documento, Me.clcImporte.EditValue, Me.importe_original)
            If Response.ErrorFound Then Exit Sub
            If Action = Actions.Insert Then Response = oDocumentos.ValidaExisteFila(Documento, "documento", CType(OwnerForm, frmExportarArchivoDocumentos).tmaEnviosDependenciasDocumentos.DataSource, False)
        End If

    End Sub

#End Region

#Region "Eventos de los Controles"

    Private Sub lkpDocumento_LoadData(ByVal Initialize As Boolean)

                Dim Response As New Events
                If Me.clcQuincena.EditValue = Nothing Then
                    Me.clcQuincena.EditValue = 0
                End If
                If Me.clcAnio.EditValue = Nothing Then
                    Me.clcAnio.EditValue = 0
                End If

                Response = oDocumentos.MovimientosCobrarCliente_grl(Me.clcCliente.EditValue, Me.clcSucursal.EditValue, Me.clcQuincena.EditValue, Me.clcAnio.EditValue)
                If Not Response.ErrorFound Then
                    Dim oDataSet As DataSet
                    oDataSet = Response.Value
                    Me.lkpDocumento.DataSource = oDataSet.Tables(0)
                    oDataSet = Nothing
                End If
                Response = Nothing
    End Sub
    Private Sub lkpDocumento_Format() Handles lkpDocumento.Format
        modFormatos.for_documentos_grl(Me.lkpDocumento)
    End Sub
    Private Sub lkpDocumento_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpDocumento.EditValueChanged
        If Me.clcQuincena.EditValue = Nothing Then
            Me.clcQuincena.EditValue = 0
        End If
        If Me.clcAnio.EditValue = Nothing Then
            Me.clcAnio.EditValue = 0
        End If

        Me.clcSucursal.EditValue = Me.lkpDocumento.GetValue("sucursal")
        Me.clcConcepto.EditValue = Me.lkpDocumento.GetValue("concepto")
        Me.clcSerie.EditValue = Me.lkpDocumento.GetValue("serie")
        Me.clcFolio.EditValue = Me.lkpDocumento.GetValue("folio")
        Me.clcDocumento.EditValue = Me.lkpDocumento.GetValue("Documento")

        importe_original = Me.lkpDocumento.GetValue("importe")
        Me.clcImporte.EditValue = Me.lkpDocumento.GetValue("importe")


    End Sub
    Private Sub clcAnio_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcAnio.EditValueChanged
        If Me.clcQuincena.EditValue > 0 And Me.clcAnio.EditValue > 0 Then
            Me.lkpDocumento.Enabled = True
        End If
        If Action = Actions.Delete Then
            Me.lkpDocumento.Enabled = False
        End If

        cambiaDatos()
    End Sub
    Private Sub clcQuincena_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clcQuincena.EditValueChanged
        If Me.clcQuincena.EditValue > 0 And Me.clcAnio.EditValue > 0 Then
            Me.lkpDocumento.Enabled = True
        End If
        If Action = Actions.Delete Then
            Me.lkpDocumento.Enabled = False
        End If
        cambiaDatos()
    End Sub

#End Region

#Region "Funcionalidad"

    Private Sub cambiaDatos()

        Dim Response As New Events
        If Me.clcAnio.EditValue = Nothing Then
            Me.clcAnio.EditValue = 0
        End If
        Response = oDocumentos.MovimientosCobrarCliente_grl(Me.clcCliente.EditValue, Me.clcSucursal.EditValue, Me.clcQuincena.EditValue, Me.clcAnio.EditValue)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDocumento.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
        If Me.lkpDocumento.EditValue > 0 Then
            Me.clcSucursal.EditValue = Me.lkpDocumento.GetValue("sucursal")
            Me.clcConcepto.EditValue = Me.lkpDocumento.GetValue("concepto")
            Me.clcSerie.EditValue = Me.lkpDocumento.GetValue("serie")
            Me.clcFolio.EditValue = Me.lkpDocumento.GetValue("folio")
            Me.clcDocumento.EditValue = Me.lkpDocumento.GetValue("Documento")
            If Me.clcImporte.EditValue <= 0 Then
                Me.clcImporte.EditValue = Me.lkpDocumento.GetValue("importe")
            End If

        End If

    End Sub

#End Region

End Class


