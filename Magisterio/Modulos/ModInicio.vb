Imports System.Globalization
Imports System.Threading

Module ModInicio
    Public TINApp As New Dipros.Windows.Application
    Private Convenios As Boolean = True

    'DESCRIPCION: Funci�n de inicio del sistema
    'DESARROLLO: DIPROS SYSTEMS
    'FECHA: 18/02/2006 00:00:00
    Public Sub Main()
        TINApp.Connection = New Dipros.Data.Data
        TINApp.Application = "Magisterio"
        TINApp.Prefix = "MAG"

        If Not TINApp.Connection.Trusted Then End
        If Not TINApp.Login() Then End

        'Inicializa la capa de negocios
        VillarrealBusiness.BusinessEnvironment.Connection = TINApp.Connection

        'Paso de conexion y frmMain a Comunes
        Comunes.Common.Aplicacion = TINApp

        ''----------------------------------------------------------------------------------
        'Selecciono por default la cultura de M�xico como configuracion del programa
        Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")

        Dim oMicultura As New CultureInfo("es-MX")
        Dim instance As New DateTimeFormatInfo
        Dim value As String

        value = "hh:mm:ss:tt"
        instance.LongTimePattern = value
        oMicultura.DateTimeFormat.LongTimePattern = instance.LongTimePattern

        Thread.CurrentThread.CurrentCulture = oMicultura

        ''----------------------------------------------------------------------------------

        Dim oMain As New frmMain



        If Comunes.clsUtilerias.AbrirArchivo(Comunes.Common.Sucursal_Actual) = False Then
            If TINApp.Connection.User = "SUPER" Then

                Dim oForm As New Comunes.frmSucursalActual
                oForm.Convenios = Convenios

                Application.Run(oForm)

                If oForm.EntrarMain = True Then

                    Application.Run(oMain)
                Else
                    MsgBox("No hay una Sucursal Definida", MsgBoxStyle.Information, "Mensaje del Sistema")

                End If
            Else

                MsgBox("No hay una Sucursal Definida y S�lo el Supervisor tiene acceso", MsgBoxStyle.Information, "Mensaje del Sistema")
            End If
        Else

            Application.Run(oMain)

        End If
    End Sub

End Module
