Imports Dipros.Windows.Forms

Module modFormatos

#Region "Dependencias"

    Public Sub for_dependencias_grs(ByVal oGrid As DevExpress.XtraGrid.GridControl)
        AddColumns(oGrid, "Convenio", "dependencia", 0, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oGrid, "Descripci�n", "descripcion", 1, DevExpress.Utils.FormatType.None, "", 300)
        AddColumns(oGrid, "Concepto de Deducci�n", "concepto_deduccion", 2, DevExpress.Utils.FormatType.None, "", 135)
        AddColumns(oGrid, "Clave de Identificaci�n", "clave_identificacion", 3, DevExpress.Utils.FormatType.None, "", 200)
    End Sub

#End Region
    'estatus tipo_abono quincena_envio anio_envio  despliega                                                                                    documento_despliega                                           envio_despliega
#Region "Documentos"

    Public Sub for_documentos_grl(ByVal oLookup As Dipros.Editors.TINMultiLookup)
        AddColumns(oLookup, "Documento", "documento", 7, DevExpress.Utils.FormatType.None, "", 70)
        AddColumns(oLookup, "Importe", "importe", 10, DevExpress.Utils.FormatType.Numeric, "$##,##0.00", 70)
        AddColumns(oLookup, "Fecha Vencimiento", "fecha_vencimiento", 17, DevExpress.Utils.FormatType.DateTime, "dd/MMM/yyyy", 120)

        AddColumns(oLookup, "Sucursal", "sucursal", -1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Nombre", "nombre", -1, DevExpress.Utils.FormatType.None, "", 330)
        AddColumns(oLookup, "Concepto", "concepto", -1, DevExpress.Utils.FormatType.None, "", 140)
        AddColumns(oLookup, "Serie", "serie", -1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Folio", "folio", -1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Cliente", "cliente", -1, DevExpress.Utils.FormatType.None, "", 330)
        AddColumns(oLookup, "Partida", "partida", -1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Fecha", "fecha", -1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Sucursal Referencia", "sucursal_referencia", -1, DevExpress.Utils.FormatType.None, "", 140)
        AddColumns(oLookup, "Serie Referencia", "serie_referencia", -1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Folio Referencia", "folio_referencia", -1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Cliente Referencia", "cliente_referencia", -1, DevExpress.Utils.FormatType.None, "", 330)
        AddColumns(oLookup, "Documento Referencia", "documento_referencia", -1, DevExpress.Utils.FormatType.None, "", 140)
        AddColumns(oLookup, "Plazo", "plazo", -1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Saldo", "saldo", -1, DevExpress.Utils.FormatType.None, "", 330)
        AddColumns(oLookup, "Saldo Inter�s", "saldo_interes", -1, DevExpress.Utils.FormatType.None, "", 140)
        AddColumns(oLookup, "Fecha �ltimo Abono", "fecha_ultimo_abono", -1, DevExpress.Utils.FormatType.None, "", 200)
        AddColumns(oLookup, "Estatus", "estatus", -1, DevExpress.Utils.FormatType.None, "", 80)
        AddColumns(oLookup, "Tipo Abono", "tipo_abono", -1, DevExpress.Utils.FormatType.None, "", 330)
        AddColumns(oLookup, "Quincena Env�o", "quincena_envio", -1, DevExpress.Utils.FormatType.None, "", 140)
        AddColumns(oLookup, "Anio Env�o", "anio_envio", -1, DevExpress.Utils.FormatType.None, "", 200)

    End Sub

#End Region

End Module
