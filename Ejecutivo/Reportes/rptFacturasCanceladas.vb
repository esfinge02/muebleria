Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptFacturasCanceladas
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghFecha As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfBodega As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Public picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblEntrada As DataDynamics.ActiveReports.Label = Nothing
	Private lblFecha As DataDynamics.ActiveReports.Label = Nothing
	Private lblReferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblImporte As DataDynamics.ActiveReports.Label = Nothing
	Private lblSucursal As DataDynamics.ActiveReports.Label = Nothing
	Private Label17 As DataDynamics.ActiveReports.Label = Nothing
	Private lblOC As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private Label28 As DataDynamics.ActiveReports.Label = Nothing
	Private Label29 As DataDynamics.ActiveReports.Label = Nothing
	Private Label30 As DataDynamics.ActiveReports.Label = Nothing
	Private Label31 As DataDynamics.ActiveReports.Label = Nothing
	Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTipo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtImporteNCR As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescuento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEnganche As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtInteres As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_factura As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_cancelacion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private observaciones_cancelacion As DataDynamics.ActiveReports.TextBox = Nothing
	Private folio_fiscal As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteDia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescuentoDia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEngancheDia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtInteresDia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalDia As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label19 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label27 As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private txtFechaActual As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private txtImporteNCRGran As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtMenosGran As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEngancheGran As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtInteresGran As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalGran As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptFacturasCanceladas.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghFecha = CType(Me.Sections("ghFecha"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfBodega = CType(Me.Sections("gpfBodega"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.Shape1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.picLogotipo = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.lblEntrada = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblFecha = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblReferencia = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblImporte = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblSucursal = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label17 = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblOC = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Line)
		Me.Label18 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label28 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label29 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Label30 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Label31 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.txtFecha = CType(Me.ghFecha.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtCliente = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtTipo = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtFolio = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtImporteNCR = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDescuento = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtEnganche = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtInteres = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.fecha_factura = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.fecha_cancelacion1 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.observaciones_cancelacion = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.folio_fiscal = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.Label12 = CType(Me.gpfBodega.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteDia = CType(Me.gpfBodega.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDescuentoDia = CType(Me.gpfBodega.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtEngancheDia = CType(Me.gpfBodega.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtInteresDia = CType(Me.gpfBodega.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalDia = CType(Me.gpfBodega.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Label19 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.TextBox2 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label27 = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Line)
		Me.txtFechaActual = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtImporteNCRGran = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtMenosGran = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtEngancheGran = CType(Me.ReportFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtInteresGran = CType(Me.ReportFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalGran = CType(Me.ReportFooter.Controls(5),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptFacturasCanceladas_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
