Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptDiarioVentasExpo
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghEnajenacion As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents ghTipoventa As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolio As DataDynamics.ActiveReports.Label = Nothing
	Private lblDiferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioLista As DataDynamics.ActiveReports.Label = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private lblenganche As DataDynamics.ActiveReports.Label = Nothing
	Private costo2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private tipoventa2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private tipoventa1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfolio As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion_corta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioLista As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDiferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtEnganche As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtIntereses As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSinIntereses As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private descuento1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private enganche1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private intereses1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private sin_intereses As DataDynamics.ActiveReports.TextBox = Nothing
	Private importe2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private descuento2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private enganche2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private sin_intereses1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private intereses2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_normales1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_normales_credito1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_normales_contado1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total_enajenacion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private importe3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private descuento3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private enganche3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private total3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private sin_intereses2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private intereses3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label15 As DataDynamics.ActiveReports.Label = Nothing
	Private Label16 As DataDynamics.ActiveReports.Label = Nothing
	Private Label17 As DataDynamics.ActiveReports.Label = Nothing
	Private Label18 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptDiarioVentasExpo.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghEnajenacion = CType(Me.Sections("ghEnajenacion"),DataDynamics.ActiveReports.GroupHeader)
		Me.ghTipoventa = CType(Me.Sections("ghTipoventa"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter2 = CType(Me.Sections("GroupFooter2"),DataDynamics.ActiveReports.GroupFooter)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.lblFolio = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.lblDiferencia = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblFactura = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblPrecioVenta = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblPrecioLista = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label1 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblenganche = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.costo2 = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Shape)
		Me.tipoventa2 = CType(Me.ghEnajenacion.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.tipoventa1 = CType(Me.ghTipoventa.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtfolio = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion_corta = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioLista = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioVenta = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDiferencia = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtFactura = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtEnganche = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtTotal = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtIntereses = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtSinIntereses = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.importe1 = CType(Me.GroupFooter2.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.descuento1 = CType(Me.GroupFooter2.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.costo3 = CType(Me.GroupFooter2.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.enganche1 = CType(Me.GroupFooter2.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.total1 = CType(Me.GroupFooter2.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label10 = CType(Me.GroupFooter2.Controls(5),DataDynamics.ActiveReports.Label)
		Me.intereses1 = CType(Me.GroupFooter2.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.sin_intereses = CType(Me.GroupFooter2.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.importe2 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.descuento2 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.costo4 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.enganche2 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.total2 = CType(Me.GroupFooter1.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label11 = CType(Me.GroupFooter1.Controls(5),DataDynamics.ActiveReports.Label)
		Me.sin_intereses1 = CType(Me.GroupFooter1.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.intereses2 = CType(Me.GroupFooter1.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.total_normales1 = CType(Me.GroupFooter1.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.total_normales_credito1 = CType(Me.GroupFooter1.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.total_normales_contado1 = CType(Me.GroupFooter1.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.total_enajenacion1 = CType(Me.GroupFooter1.Controls(11),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Line)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.importe3 = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.descuento3 = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.costo5 = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.enganche3 = CType(Me.ReportFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.total3 = CType(Me.ReportFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label12 = CType(Me.ReportFooter.Controls(5),DataDynamics.ActiveReports.Label)
		Me.sin_intereses2 = CType(Me.ReportFooter.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.intereses3 = CType(Me.ReportFooter.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.ReportFooter.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.ReportFooter.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.TextBox3 = CType(Me.ReportFooter.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.Label15 = CType(Me.ReportFooter.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.ReportFooter.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label17 = CType(Me.ReportFooter.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.ReportFooter.Controls(14),DataDynamics.ActiveReports.Label)
		Me.TextBox4 = CType(Me.ReportFooter.Controls(15),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptDiarioVentasExpo_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
