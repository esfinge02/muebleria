Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptRecordatorioSaldo
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private VENCIDO As DataDynamics.ActiveReports.TextBox = Nothing
	Private POR_VENCER As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptRecordatorioSaldo.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Label1 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.TextBox1 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.VENCIDO = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.POR_VENCER = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region
End Class
