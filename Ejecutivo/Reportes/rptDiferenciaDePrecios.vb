Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptDiferenciaDePrecios
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents gphDepartamento As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents gphGrupo As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gpfGrupo As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents gpfDepartamento As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private lblFechas As DataDynamics.ActiveReports.Label = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private lblFolioReferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblDiferencia As DataDynamics.ActiveReports.Label = Nothing
	Private lblFactura As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioVenta As DataDynamics.ActiveReports.Label = Nothing
	Private lblPrecioLista As DataDynamics.ActiveReports.Label = Nothing
	Private txtDepartamento As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtnombre_concepto As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDescripcion_corta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioLista As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPrecioVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDiferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtFactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalGrupoDiferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalGrupoPrecioVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalGrupoPrecioLista As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTotalDptoDiferencia As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalDptoPrecioVenta As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTotalDptoPrecioLista As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptDiferenciaDePrecios.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.gphDepartamento = CType(Me.Sections("gphDepartamento"),DataDynamics.ActiveReports.GroupHeader)
		Me.gphGrupo = CType(Me.Sections("gphGrupo"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gpfGrupo = CType(Me.Sections("gpfGrupo"),DataDynamics.ActiveReports.GroupFooter)
		Me.gpfDepartamento = CType(Me.Sections("gpfDepartamento"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.lblFechas = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Shape8 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.Label6 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.lblFolioReferencia = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblDiferencia = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblFactura = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblPrecioVenta = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblPrecioLista = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.txtDepartamento = CType(Me.gphDepartamento.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtnombre_concepto = CType(Me.gphGrupo.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDescripcion_corta = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioLista = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtPrecioVenta = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtDiferencia = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtFactura = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.Line2 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Line)
		Me.Label10 = CType(Me.gpfGrupo.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalGrupoDiferencia = CType(Me.gpfGrupo.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalGrupoPrecioVenta = CType(Me.gpfGrupo.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalGrupoPrecioLista = CType(Me.gpfGrupo.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label4 = CType(Me.gpfDepartamento.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtTotalDptoDiferencia = CType(Me.gpfDepartamento.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalDptoPrecioVenta = CType(Me.gpfDepartamento.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTotalDptoPrecioLista = CType(Me.gpfDepartamento.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Line)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region

    Private Sub rptDiferenciaDePrecios_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
