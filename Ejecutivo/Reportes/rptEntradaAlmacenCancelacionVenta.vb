Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptEntradaAlmacenCancelacionVenta
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private lblTitulo As DataDynamics.ActiveReports.Label = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Label6 As DataDynamics.ActiveReports.Label = Nothing
	Private TextBox11 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Public txtNomCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Public txtNoCliente As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtfactura As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private Label4 As DataDynamics.ActiveReports.Label = Nothing
	Private Label5 As DataDynamics.ActiveReports.Label = Nothing
	Private sucursal_nombre1 As DataDynamics.ActiveReports.Label = Nothing
	Private CANTIDAD As DataDynamics.ActiveReports.Label = Nothing
	Private CONCEPTO As DataDynamics.ActiveReports.Label = Nothing
	Private sucursal_factura3 As DataDynamics.ActiveReports.Label = Nothing
	Private articulo As DataDynamics.ActiveReports.Label = Nothing
	Private Costo_sin_iva As DataDynamics.ActiveReports.Label = Nothing
	Private Label8 As DataDynamics.ActiveReports.Label = Nothing
	Private Label9 As DataDynamics.ActiveReports.Label = Nothing
	Private Label10 As DataDynamics.ActiveReports.Label = Nothing
	Private Label11 As DataDynamics.ActiveReports.Label = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Public TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Public TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox9 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox10 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtsubtotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtiva As DataDynamics.ActiveReports.TextBox = Nothing
	Private txttotal As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo_unitario_sin_iva1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private costo_total_sin_iva1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private precio_publico_iva_incluido1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private precio_contado_iva_incluido1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label7 As DataDynamics.ActiveReports.Label = Nothing
	Private Label12 As DataDynamics.ActiveReports.Label = Nothing
	Private Label13 As DataDynamics.ActiveReports.Label = Nothing
	Private Label14 As DataDynamics.ActiveReports.Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptEntradaAlmacenCancelacionVenta.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.txtEmpresa = CType(Me.ReportHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.ReportHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.ReportHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.picLogotipo = CType(Me.ReportHeader.Controls(3),DataDynamics.ActiveReports.Picture)
		Me.lblTitulo = CType(Me.ReportHeader.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label2 = CType(Me.ReportHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.ReportHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.TextBox11 = CType(Me.ReportHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtNomCliente = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtNoCliente = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtfactura = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Label3 = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label4 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.Label)
		Me.sucursal_nombre1 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.CANTIDAD = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.CONCEPTO = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.sucursal_factura3 = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.articulo = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Costo_sin_iva = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.Label9 = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.Label10 = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.Line1 = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Line)
		Me.TextBox3 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.TextBox4 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.TextBox5 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.TextBox6 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.TextBox7 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.TextBox8 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.TextBox9 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.TextBox10 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtsubtotal = CType(Me.ReportFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.txtiva = CType(Me.ReportFooter.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txttotal = CType(Me.ReportFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.costo_unitario_sin_iva1 = CType(Me.ReportFooter.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.costo_total_sin_iva1 = CType(Me.ReportFooter.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.precio_publico_iva_incluido1 = CType(Me.ReportFooter.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.precio_contado_iva_incluido1 = CType(Me.ReportFooter.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.Label7 = CType(Me.ReportFooter.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.ReportFooter.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.ReportFooter.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.ReportFooter.Controls(10),DataDynamics.ActiveReports.Label)
	End Sub

	#End Region



    Private Sub PageHeader_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Format
        Me.txtsubtotal.Value = Me.Fields("subtotal").Value
        Me.txtiva.Value = Me.Fields("iva").Value
        Me.txttotal.Value = Me.Fields("total").Value
    End Sub

    Private Sub rptEntradaAlmacenCancelacionVenta_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
