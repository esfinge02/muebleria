Imports System
Imports System.Drawing
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class rptCotizaciones
Inherits ActiveReport
	Public Sub New()
	MyBase.New()
		InitializeReport()
	End Sub
	#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private picLogotipo As DataDynamics.ActiveReports.Picture = Nothing
	Private txtEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDireccionEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTelefonosEmpresa As DataDynamics.ActiveReports.TextBox = Nothing
	Private direccion_empresa1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Encabezado As DataDynamics.ActiveReports.TextBox = Nothing
	Private atencion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private atencion2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private domicilio1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtcotizacion As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtArticulo As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private TextBox5 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox8 As DataDynamics.ActiveReports.TextBox = Nothing
	Private TextBox9 As DataDynamics.ActiveReports.TextBox = Nothing
	Private encabezado_cotizacion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private nombre1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private autorizo1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private pie_cotizacion1 As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtPaginaReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblDeReporte As DataDynamics.ActiveReports.Label = Nothing
	Private txtPaginasReporte As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label As DataDynamics.ActiveReports.Label = Nothing
	Private fecha_actual1 As DataDynamics.ActiveReports.Label = Nothing
	Private autorizo2 As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "Ejecutivo.rptCotizaciones.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.GroupHeader1 = CType(Me.Sections("GroupHeader1"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.GroupFooter1 = CType(Me.Sections("GroupFooter1"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.picLogotipo = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.txtEmpresa = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDireccionEmpresa = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtTelefonosEmpresa = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.direccion_empresa1 = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Encabezado = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.atencion1 = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.atencion2 = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.nombre2 = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.domicilio1 = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtcotizacion = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.txtArticulo = CType(Me.GroupHeader1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.GroupHeader1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.TextBox2 = CType(Me.GroupHeader1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.TextBox3 = CType(Me.GroupHeader1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.TextBox4 = CType(Me.GroupHeader1.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.GroupHeader1.Controls(5),DataDynamics.ActiveReports.Line)
		Me.TextBox5 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.TextBox6 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.TextBox7 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.TextBox8 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.TextBox9 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.encabezado_cotizacion1 = CType(Me.GroupFooter1.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.nombre1 = CType(Me.GroupFooter1.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.autorizo1 = CType(Me.GroupFooter1.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.pie_cotizacion1 = CType(Me.GroupFooter1.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtPaginaReporte = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.lblDeReporte = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.txtPaginasReporte = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Label = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.fecha_actual1 = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.autorizo2 = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.TextBox)
	End Sub

	#End Region

    Private Sub rptCotizaciones_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim oReportes As New VillarrealBusiness.Reportes
        Dim dibujo As Image
        sender = oReportes.DatosEmpresa()

        If sender.Value.Tables(0).Rows.Count > 0 Then
            Dim oDataSet As DataSet
            oDataSet = sender.Value
            Me.txtEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("nombre_empresa")
            Me.txtDireccionEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("direccion_empresa")
            Me.txtTelefonosEmpresa.Value = sender.Value.Tables(0).Rows.Item(0).Item("telefonos_empresa")
            dibujo = Dipros.Utils.Draw.ByteToImage(sender.Value.Tables(0).Rows.Item(0).Item("logotipo_empresa"))
            Me.picLogotipo.Image = dibujo

            oDataSet = Nothing
        End If
    End Sub
End Class
