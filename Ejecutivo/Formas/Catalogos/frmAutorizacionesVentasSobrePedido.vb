Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmAutorizacionesVentasPedidoFabrica
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo. 
    Friend WithEvents lblFolio_Autorizacion As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Autorizacion As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha_Autorizacion As System.Windows.Forms.Label
    Friend WithEvents dteFecha_Autorizacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents clcCosto As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkUtilizada As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblSucursal_Factura As System.Windows.Forms.Label
    Friend WithEvents lblSerie_Factura As System.Windows.Forms.Label
    Friend WithEvents txtSerie_Factura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFolio_Factura As System.Windows.Forms.Label
    Friend WithEvents clcFolio_Factura As Dipros.Editors.TINCalcEdit
    Friend WithEvents lkpSucursal_Factura As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpVendedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents gbxDatosFactura As System.Windows.Forms.GroupBox
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblBodega As System.Windows.Forms.Label
    Friend WithEvents lkpBodega As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAutorizacionesVentasPedidoFabrica))
        Me.lblFolio_Autorizacion = New System.Windows.Forms.Label
        Me.clcFolio_Autorizacion = New Dipros.Editors.TINCalcEdit
        Me.lblFecha_Autorizacion = New System.Windows.Forms.Label
        Me.dteFecha_Autorizacion = New DevExpress.XtraEditors.DateEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.lblCosto = New System.Windows.Forms.Label
        Me.clcCosto = New Dipros.Editors.TINCalcEdit
        Me.chkUtilizada = New DevExpress.XtraEditors.CheckEdit
        Me.lblSucursal_Factura = New System.Windows.Forms.Label
        Me.lblSerie_Factura = New System.Windows.Forms.Label
        Me.txtSerie_Factura = New DevExpress.XtraEditors.TextEdit
        Me.lblFolio_Factura = New System.Windows.Forms.Label
        Me.clcFolio_Factura = New Dipros.Editors.TINCalcEdit
        Me.lkpSucursal_Factura = New Dipros.Editors.TINMultiLookup
        Me.lkpVendedor = New Dipros.Editors.TINMultiLookup
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.gbxDatosFactura = New System.Windows.Forms.GroupBox
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.lblBodega = New System.Windows.Forms.Label
        Me.lkpBodega = New Dipros.Editors.TINMultiLookup
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        CType(Me.clcFolio_Autorizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha_Autorizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUtilizada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDatosFactura.SuspendLayout()
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1500, 28)
        '
        'lblFolio_Autorizacion
        '
        Me.lblFolio_Autorizacion.AutoSize = True
        Me.lblFolio_Autorizacion.Location = New System.Drawing.Point(68, 40)
        Me.lblFolio_Autorizacion.Name = "lblFolio_Autorizacion"
        Me.lblFolio_Autorizacion.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio_Autorizacion.TabIndex = 0
        Me.lblFolio_Autorizacion.Tag = ""
        Me.lblFolio_Autorizacion.Text = "Folio:"
        Me.lblFolio_Autorizacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Autorizacion
        '
        Me.clcFolio_Autorizacion.EditValue = "0"
        Me.clcFolio_Autorizacion.Location = New System.Drawing.Point(111, 40)
        Me.clcFolio_Autorizacion.MaxValue = 0
        Me.clcFolio_Autorizacion.MinValue = 0
        Me.clcFolio_Autorizacion.Name = "clcFolio_Autorizacion"
        '
        'clcFolio_Autorizacion.Properties
        '
        Me.clcFolio_Autorizacion.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Autorizacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Autorizacion.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Autorizacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Autorizacion.Properties.Enabled = False
        Me.clcFolio_Autorizacion.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Autorizacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Autorizacion.Size = New System.Drawing.Size(74, 19)
        Me.clcFolio_Autorizacion.TabIndex = 1
        Me.clcFolio_Autorizacion.Tag = "folio_autorizacion"
        '
        'lblFecha_Autorizacion
        '
        Me.lblFecha_Autorizacion.AutoSize = True
        Me.lblFecha_Autorizacion.Location = New System.Drawing.Point(344, 40)
        Me.lblFecha_Autorizacion.Name = "lblFecha_Autorizacion"
        Me.lblFecha_Autorizacion.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha_Autorizacion.TabIndex = 1
        Me.lblFecha_Autorizacion.Tag = ""
        Me.lblFecha_Autorizacion.Text = "Fec&ha:"
        Me.lblFecha_Autorizacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha_Autorizacion
        '
        Me.dteFecha_Autorizacion.EditValue = "30/08/2007"
        Me.dteFecha_Autorizacion.Location = New System.Drawing.Point(392, 40)
        Me.dteFecha_Autorizacion.Name = "dteFecha_Autorizacion"
        '
        'dteFecha_Autorizacion.Properties
        '
        Me.dteFecha_Autorizacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha_Autorizacion.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Autorizacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha_Autorizacion.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha_Autorizacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha_Autorizacion.Size = New System.Drawing.Size(95, 20)
        Me.dteFecha_Autorizacion.TabIndex = 2
        Me.dteFecha_Autorizacion.Tag = "fecha_autorizacion"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(56, 64)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 3
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(41, 88)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(62, 16)
        Me.lblVendedor.TabIndex = 5
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "Ve&ndedor:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(63, 184)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(40, 16)
        Me.lblCosto.TabIndex = 13
        Me.lblCosto.Tag = ""
        Me.lblCosto.Text = "Cos&to:"
        Me.lblCosto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCosto
        '
        Me.clcCosto.EditValue = "0"
        Me.clcCosto.Location = New System.Drawing.Point(111, 184)
        Me.clcCosto.MaxValue = 0
        Me.clcCosto.MinValue = 0
        Me.clcCosto.Name = "clcCosto"
        '
        'clcCosto.Properties
        '
        Me.clcCosto.Properties.DisplayFormat.FormatString = "c2"
        Me.clcCosto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCosto.Properties.EditFormat.FormatString = "n2"
        Me.clcCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCosto.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcCosto.Properties.MaxLength = 9
        Me.clcCosto.Properties.Precision = 2
        Me.clcCosto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCosto.Size = New System.Drawing.Size(90, 19)
        Me.clcCosto.TabIndex = 14
        Me.clcCosto.Tag = "costo"
        '
        'chkUtilizada
        '
        Me.chkUtilizada.EditValue = "False"
        Me.chkUtilizada.Location = New System.Drawing.Point(56, 264)
        Me.chkUtilizada.Name = "chkUtilizada"
        '
        'chkUtilizada.Properties
        '
        Me.chkUtilizada.Properties.Caption = "Utilizada"
        Me.chkUtilizada.Properties.Enabled = False
        Me.chkUtilizada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.SystemColors.ControlText)
        Me.chkUtilizada.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.Highlight)
        Me.chkUtilizada.Size = New System.Drawing.Size(80, 19)
        Me.chkUtilizada.TabIndex = 19
        Me.chkUtilizada.Tag = "utilizada"
        '
        'lblSucursal_Factura
        '
        Me.lblSucursal_Factura.AutoSize = True
        Me.lblSucursal_Factura.Location = New System.Drawing.Point(42, 24)
        Me.lblSucursal_Factura.Name = "lblSucursal_Factura"
        Me.lblSucursal_Factura.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal_Factura.TabIndex = 0
        Me.lblSucursal_Factura.Tag = ""
        Me.lblSucursal_Factura.Text = "&Sucursal:"
        Me.lblSucursal_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSerie_Factura
        '
        Me.lblSerie_Factura.AutoSize = True
        Me.lblSerie_Factura.Location = New System.Drawing.Point(58, 48)
        Me.lblSerie_Factura.Name = "lblSerie_Factura"
        Me.lblSerie_Factura.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie_Factura.TabIndex = 2
        Me.lblSerie_Factura.Tag = ""
        Me.lblSerie_Factura.Text = "S&erie:"
        Me.lblSerie_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSerie_Factura
        '
        Me.txtSerie_Factura.EditValue = ""
        Me.txtSerie_Factura.Location = New System.Drawing.Point(98, 48)
        Me.txtSerie_Factura.Name = "txtSerie_Factura"
        '
        'txtSerie_Factura.Properties
        '
        Me.txtSerie_Factura.Properties.Enabled = False
        Me.txtSerie_Factura.Properties.MaxLength = 3
        Me.txtSerie_Factura.Size = New System.Drawing.Size(34, 20)
        Me.txtSerie_Factura.TabIndex = 3
        Me.txtSerie_Factura.Tag = "serie_factura"
        '
        'lblFolio_Factura
        '
        Me.lblFolio_Factura.AutoSize = True
        Me.lblFolio_Factura.Location = New System.Drawing.Point(58, 72)
        Me.lblFolio_Factura.Name = "lblFolio_Factura"
        Me.lblFolio_Factura.Size = New System.Drawing.Size(35, 16)
        Me.lblFolio_Factura.TabIndex = 4
        Me.lblFolio_Factura.Tag = ""
        Me.lblFolio_Factura.Text = "&Folio:"
        Me.lblFolio_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio_Factura
        '
        Me.clcFolio_Factura.EditValue = "0"
        Me.clcFolio_Factura.Location = New System.Drawing.Point(98, 72)
        Me.clcFolio_Factura.MaxValue = 0
        Me.clcFolio_Factura.MinValue = 0
        Me.clcFolio_Factura.Name = "clcFolio_Factura"
        '
        'clcFolio_Factura.Properties
        '
        Me.clcFolio_Factura.Properties.DisplayFormat.FormatString = "###,###,##0"
        Me.clcFolio_Factura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Factura.Properties.EditFormat.FormatString = "###,###,##0"
        Me.clcFolio_Factura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcFolio_Factura.Properties.Enabled = False
        Me.clcFolio_Factura.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcFolio_Factura.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcFolio_Factura.Size = New System.Drawing.Size(74, 19)
        Me.clcFolio_Factura.TabIndex = 5
        Me.clcFolio_Factura.Tag = "folio_factura"
        '
        'lkpSucursal_Factura
        '
        Me.lkpSucursal_Factura.AllowAdd = False
        Me.lkpSucursal_Factura.AutoReaload = False
        Me.lkpSucursal_Factura.DataSource = Nothing
        Me.lkpSucursal_Factura.DefaultSearchField = ""
        Me.lkpSucursal_Factura.DisplayMember = "nombre"
        Me.lkpSucursal_Factura.EditValue = Nothing
        Me.lkpSucursal_Factura.Enabled = False
        Me.lkpSucursal_Factura.Filtered = False
        Me.lkpSucursal_Factura.InitValue = Nothing
        Me.lkpSucursal_Factura.Location = New System.Drawing.Point(98, 24)
        Me.lkpSucursal_Factura.MultiSelect = False
        Me.lkpSucursal_Factura.Name = "lkpSucursal_Factura"
        Me.lkpSucursal_Factura.NullText = ""
        Me.lkpSucursal_Factura.PopupWidth = CType(400, Long)
        Me.lkpSucursal_Factura.ReadOnlyControl = False
        Me.lkpSucursal_Factura.Required = False
        Me.lkpSucursal_Factura.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal_Factura.SearchMember = ""
        Me.lkpSucursal_Factura.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal_Factura.SelectAll = False
        Me.lkpSucursal_Factura.Size = New System.Drawing.Size(280, 20)
        Me.lkpSucursal_Factura.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal_Factura.TabIndex = 1
        Me.lkpSucursal_Factura.Tag = "sucursal_factura"
        Me.lkpSucursal_Factura.ToolTip = "sucursal"
        Me.lkpSucursal_Factura.ValueMember = "Sucursal"
        '
        'lkpVendedor
        '
        Me.lkpVendedor.AllowAdd = False
        Me.lkpVendedor.AutoReaload = False
        Me.lkpVendedor.DataSource = Nothing
        Me.lkpVendedor.DefaultSearchField = ""
        Me.lkpVendedor.DisplayMember = "nombre"
        Me.lkpVendedor.EditValue = Nothing
        Me.lkpVendedor.Filtered = False
        Me.lkpVendedor.InitValue = Nothing
        Me.lkpVendedor.Location = New System.Drawing.Point(111, 87)
        Me.lkpVendedor.MultiSelect = False
        Me.lkpVendedor.Name = "lkpVendedor"
        Me.lkpVendedor.NullText = ""
        Me.lkpVendedor.PopupWidth = CType(420, Long)
        Me.lkpVendedor.ReadOnlyControl = False
        Me.lkpVendedor.Required = True
        Me.lkpVendedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpVendedor.SearchMember = ""
        Me.lkpVendedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpVendedor.SelectAll = False
        Me.lkpVendedor.Size = New System.Drawing.Size(280, 20)
        Me.lkpVendedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpVendedor.TabIndex = 6
        Me.lkpVendedor.Tag = "vendedor"
        Me.lkpVendedor.ToolTip = Nothing
        Me.lkpVendedor.ValueMember = "Vendedor"
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(111, 63)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = True
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(280, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 4
        Me.lkpCliente.Tag = "cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'gbxDatosFactura
        '
        Me.gbxDatosFactura.Controls.Add(Me.txtSerie_Factura)
        Me.gbxDatosFactura.Controls.Add(Me.clcFolio_Factura)
        Me.gbxDatosFactura.Controls.Add(Me.lblFolio_Factura)
        Me.gbxDatosFactura.Controls.Add(Me.lblSucursal_Factura)
        Me.gbxDatosFactura.Controls.Add(Me.lblSerie_Factura)
        Me.gbxDatosFactura.Controls.Add(Me.lkpSucursal_Factura)
        Me.gbxDatosFactura.Location = New System.Drawing.Point(40, 264)
        Me.gbxDatosFactura.Name = "gbxDatosFactura"
        Me.gbxDatosFactura.Size = New System.Drawing.Size(400, 104)
        Me.gbxDatosFactura.TabIndex = 20
        Me.gbxDatosFactura.TabStop = False
        Me.gbxDatosFactura.Text = "                         Datos de la Factura Donde se Utiliz� el Folio"
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = True
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(111, 135)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(280, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 10
        Me.lkpGrupo.Tag = ""
        Me.lkpGrupo.ToolTip = "grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(60, 136)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 9
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = True
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(111, 111)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(280, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 8
        Me.lkpDepartamento.Tag = ""
        Me.lkpDepartamento.ToolTip = "departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(15, 112)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 7
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(52, 160)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 16)
        Me.Label2.TabIndex = 11
        Me.Label2.Tag = ""
        Me.Label2.Text = "Art�c&ulo:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = True
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(111, 159)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(400, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = ""
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(108, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 12
        Me.lkpArticulo.Tag = "articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'lblBodega
        '
        Me.lblBodega.AutoSize = True
        Me.lblBodega.Location = New System.Drawing.Point(53, 208)
        Me.lblBodega.Name = "lblBodega"
        Me.lblBodega.Size = New System.Drawing.Size(50, 16)
        Me.lblBodega.TabIndex = 15
        Me.lblBodega.Tag = ""
        Me.lblBodega.Text = "Bodega:"
        Me.lblBodega.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpBodega
        '
        Me.lkpBodega.AllowAdd = False
        Me.lkpBodega.AutoReaload = False
        Me.lkpBodega.DataSource = Nothing
        Me.lkpBodega.DefaultSearchField = ""
        Me.lkpBodega.DisplayMember = "descripcion"
        Me.lkpBodega.EditValue = Nothing
        Me.lkpBodega.Filtered = False
        Me.lkpBodega.InitValue = Nothing
        Me.lkpBodega.Location = New System.Drawing.Point(112, 208)
        Me.lkpBodega.MultiSelect = False
        Me.lkpBodega.Name = "lkpBodega"
        Me.lkpBodega.NullText = ""
        Me.lkpBodega.PopupWidth = CType(400, Long)
        Me.lkpBodega.ReadOnlyControl = False
        Me.lkpBodega.Required = False
        Me.lkpBodega.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpBodega.SearchMember = ""
        Me.lkpBodega.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpBodega.SelectAll = False
        Me.lkpBodega.Size = New System.Drawing.Size(280, 20)
        Me.lkpBodega.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpBodega.TabIndex = 16
        Me.lkpBodega.Tag = "bodega_pedido_fabrica"
        Me.lkpBodega.ToolTip = Nothing
        Me.lkpBodega.ValueMember = "Bodega"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 232)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 16)
        Me.Label3.TabIndex = 17
        Me.Label3.Tag = ""
        Me.Label3.Text = "Cantidad:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = "0"
        Me.clcCantidad.Location = New System.Drawing.Point(112, 232)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcCantidad.Properties.MaxLength = 9
        Me.clcCantidad.Properties.Precision = 2
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Size = New System.Drawing.Size(90, 19)
        Me.clcCantidad.TabIndex = 18
        Me.clcCantidad.Tag = "cantidad"
        '
        'frmAutorizacionesVentasPedidoFabrica
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(506, 376)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.clcCantidad)
        Me.Controls.Add(Me.lblBodega)
        Me.Controls.Add(Me.lkpBodega)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblFolio_Autorizacion)
        Me.Controls.Add(Me.lblFecha_Autorizacion)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.chkUtilizada)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lkpVendedor)
        Me.Controls.Add(Me.clcFolio_Autorizacion)
        Me.Controls.Add(Me.dteFecha_Autorizacion)
        Me.Controls.Add(Me.clcCosto)
        Me.Controls.Add(Me.gbxDatosFactura)
        Me.Name = "frmAutorizacionesVentasPedidoFabrica"
        Me.Controls.SetChildIndex(Me.gbxDatosFactura, 0)
        Me.Controls.SetChildIndex(Me.clcCosto, 0)
        Me.Controls.SetChildIndex(Me.dteFecha_Autorizacion, 0)
        Me.Controls.SetChildIndex(Me.clcFolio_Autorizacion, 0)
        Me.Controls.SetChildIndex(Me.lkpVendedor, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.chkUtilizada, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.lblCosto, 0)
        Me.Controls.SetChildIndex(Me.lblVendedor, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblFecha_Autorizacion, 0)
        Me.Controls.SetChildIndex(Me.lblFolio_Autorizacion, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpBodega, 0)
        Me.Controls.SetChildIndex(Me.lblBodega, 0)
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        CType(Me.clcFolio_Autorizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha_Autorizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUtilizada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDatosFactura.ResumeLayout(False)
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVendedores As VillarrealBusiness.clsVendedores
    Private oDepartamentos As VillarrealBusiness.clsDepartamentos
    Private oGrupos As VillarrealBusiness.clsGruposArticulos
    Private oArticulos As VillarrealBusiness.clsArticulos
    Private oAutorizacionesVentasPedidoFabrica As VillarrealBusiness.clsAutorizacionesVentasPedidoFabrica
    Private oBodegas As VillarrealBusiness.clsBodegas

    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property vendedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpVendedor)
        End Get
    End Property
    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property
    Private ReadOnly Property Bodega() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpBodega)
        End Get
    End Property


    Private FECHA_ANTERIOR As DateTime

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmAutorizacionesVentasPedidoFabrica_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oAutorizacionesVentasPedidoFabrica.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oAutorizacionesVentasPedidoFabrica.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oAutorizacionesVentasPedidoFabrica.Eliminar(Me.clcFolio_Autorizacion.Value)

        End Select
    End Sub

    Private Sub frmAutorizacionesVentasPedidoFabrica_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oAutorizacionesVentasPedidoFabrica.DespliegaDatos(OwnerForm.Value("folio_autorizacion"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet


            'DEPARTAMENTO
            If IsDBNull(oDataSet.Tables(0).Rows(0)("departamento")) Then

                Me.lkpDepartamento.EditValue = -1

            Else

                Me.lkpDepartamento.EditValue = oDataSet.Tables(0).Rows(0)("departamento")

            End If
            'GRUPO
            If IsDBNull(oDataSet.Tables(0).Rows(0)("grupo")) Then

                Me.lkpGrupo.EditValue = -1

            Else

                Me.lkpGrupo.EditValue = oDataSet.Tables(0).Rows(0)("grupo")

            End If
            'ARTICULO
            If IsDBNull(oDataSet.Tables(0).Rows(0)("articulo")) Then

                Me.lkpArticulo.EditValue = -1

            Else

                Me.lkpArticulo.EditValue = oDataSet.Tables(0).Rows(0)("articulo")
                If Me.lkpArticulo.EditValue > 0 Then

                    If Departamento <= 0 Or Grupo <= 0 Then

                        Actualiza_Lookups()

                    End If

                End If

            End If


            FECHA_ANTERIOR = Me.dteFecha_Autorizacion.DateTime

            'Response = oAutorizacionesVentasPedidoFabrica.ValidacionUtilizada(Actions.Update, Me.chkUtilizada.Checked)
            'If Response.ErrorFound Then
            '    Me.tbrTools.Buttons(0).Enabled = False
            'End If
        End If

    End Sub

    Private Sub frmAutorizacionesVentasPedidoFabrica_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oAutorizacionesVentasPedidoFabrica = New VillarrealBusiness.clsAutorizacionesVentasPedidoFabrica
        oSucursales = New VillarrealBusiness.clsSucursales
        oClientes = New VillarrealBusiness.clsClientes
        oVendedores = New VillarrealBusiness.clsVendedores
        oDepartamentos = New VillarrealBusiness.clsDepartamentos
        oGrupos = New VillarrealBusiness.clsGruposArticulos
        oArticulos = New VillarrealBusiness.clsArticulos
        oBodegas = New VillarrealBusiness.clsBodegas

        Me.chkUtilizada.Checked = False

        Select Case Action
            Case Actions.Insert
            Case Actions.Update
            Case Actions.Delete
        End Select
    End Sub

    Private Sub frmAutorizacionesVentasPedidoFabrica_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oAutorizacionesVentasPedidoFabrica.Validacion(Action, Cliente, vendedor, Articulo, Me.clcCosto.Value, Me.dteFecha_Autorizacion.Text, Bodega)
    End Sub

    Private Sub frmAutorizacionesVentasPedidoFabrica_Localize() Handles MyBase.Localize
        Find("Unknow", CType("Replace by a control", Object))

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub

    Private Sub lkpVendedor_Format() Handles lkpVendedor.Format
        Comunes.clsFormato.for_vendedores_grl(Me.lkpVendedor)
    End Sub
    Private Sub lkpVendedor_LoadData(ByVal Initialize As Boolean) Handles lkpVendedor.LoadData
        Dim Response As New Events
        Response = oVendedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpVendedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub


    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData

        Dim Response As New Events

        Response = oDepartamentos.Lookup()

        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")
        Me.lkpGrupo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpGrupo.Text = Me.lkpGrupo.GetValue("descripcion")
        Me.lkpArticulo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData

        Dim Response As New Events

        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData

        Dim Response As New Events
        Response = oArticulos.Lookup(Me.lkpDepartamento.EditValue, Me.lkpGrupo.EditValue)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged
        If Articulo <> -1 Then
            Me.clcCosto.Value = Me.lkpArticulo.GetValue("ultimo_costo")
        End If
    End Sub


    Private Sub lkpSucursal_Format() Handles lkpSucursal_Factura.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal_Factura)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal_Factura.LoadData
        Dim Response As New Events
        Response = oSucursales.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal_Factura.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpBodega_LoadData(ByVal Initialize As Boolean) Handles lkpBodega.LoadData
        Dim response As Events
        response = oBodegas.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpBodega.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpBodega_Format() Handles lkpBodega.Format
        Comunes.clsFormato.for_bodegas_grl(Me.lkpBodega)
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub Actualiza_Lookups()

        Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        Me.lkpDepartamento_LoadData(True)
        If Me.lkpDepartamento.EditValue > 0 Then

            Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")

        End If

        Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")

    End Sub
#End Region


    'Private Sub dteFecha_Autorizacion_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteFecha_Autorizacion.Validated
    '    If IsDate(Me.dteFecha_Autorizacion.EditValue) = False Then
    '        Me.dteFecha_Autorizacion.EditValue = Me.FECHA_ANTERIOR
    '    End If
    'End Sub

    'Private Sub dteFecha_Autorizacion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dteFecha_Autorizacion.Validating
    '    If IsDate(Me.dteFecha_Autorizacion.EditValue) = False Then
    '        Me.dteFecha_Autorizacion.EditValue = Me.FECHA_ANTERIOR
    '    End If
    'End Sub

End Class
