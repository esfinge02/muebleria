Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmTiposVales
    Inherits Dipros.Windows.frmTINForm

#Region "Declaraciones de Variables"
    Dim KS As Keys
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cboConcepto As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents cboForma As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents lblTipoVale As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents clcTipo As Dipros.Editors.TINCalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTiposVales))
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.cboConcepto = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.cboForma = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.lblTipoVale = New System.Windows.Forms.Label
        Me.clcTipo = New Dipros.Editors.TINCalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboForma.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(346, 28)
        '
        'txtNombre
        '
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(79, 62)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.MaxLength = 50
        Me.txtNombre.Size = New System.Drawing.Size(329, 20)
        Me.txtNombre.TabIndex = 3
        '
        'cboConcepto
        '
        Me.cboConcepto.EditValue = "CA"
        Me.cboConcepto.Location = New System.Drawing.Point(79, 85)
        Me.cboConcepto.Name = "cboConcepto"
        '
        'cboConcepto.Properties
        '
        Me.cboConcepto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboConcepto.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelaci�n", "CA", 0), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Descuento", "DE", 1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Convenio", "CO", 2), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Venta", "VE", 3)})
        Me.cboConcepto.Properties.SmallImages = Me.ImageList2
        Me.cboConcepto.Size = New System.Drawing.Size(112, 23)
        Me.cboConcepto.TabIndex = 5
        '
        'ImageList2
        '
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'cboForma
        '
        Me.cboForma.EditValue = "A"
        Me.cboForma.Location = New System.Drawing.Point(79, 111)
        Me.cboForma.Name = "cboForma"
        '
        'cboForma.Properties
        '
        Me.cboForma.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboForma.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Automatico", "A", 0), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Manual", "M", 1)})
        Me.cboForma.Properties.SmallImages = Me.ImageList1
        Me.cboForma.Size = New System.Drawing.Size(112, 23)
        Me.cboForma.TabIndex = 7
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'lblTipoVale
        '
        Me.lblTipoVale.AutoSize = True
        Me.lblTipoVale.Location = New System.Drawing.Point(39, 41)
        Me.lblTipoVale.Name = "lblTipoVale"
        Me.lblTipoVale.Size = New System.Drawing.Size(32, 16)
        Me.lblTipoVale.TabIndex = 0
        Me.lblTipoVale.Tag = ""
        Me.lblTipoVale.Text = "Tipo:"
        Me.lblTipoVale.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcTipo
        '
        Me.clcTipo.EditValue = "0"
        Me.clcTipo.Location = New System.Drawing.Point(79, 40)
        Me.clcTipo.MaxValue = 0
        Me.clcTipo.MinValue = 0
        Me.clcTipo.Name = "clcTipo"
        '
        'clcTipo.Properties
        '
        Me.clcTipo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcTipo.Properties.Enabled = False
        Me.clcTipo.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcTipo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcTipo.Size = New System.Drawing.Size(65, 19)
        Me.clcTipo.TabIndex = 1
        Me.clcTipo.Tag = ""
        Me.clcTipo.ToolTip = "Tipo de Vale"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Tag = ""
        Me.Label3.Text = "Nombre:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Tag = ""
        Me.Label4.Text = "Concepto:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Tag = ""
        Me.Label5.Text = "Forma:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmTiposVales
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(418, 148)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblTipoVale)
        Me.Controls.Add(Me.clcTipo)
        Me.Controls.Add(Me.cboForma)
        Me.Controls.Add(Me.cboConcepto)
        Me.Controls.Add(Me.txtNombre)
        Me.Name = "frmTiposVales"
        Me.Text = "frmTiposVales"
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.cboConcepto, 0)
        Me.Controls.SetChildIndex(Me.cboForma, 0)
        Me.Controls.SetChildIndex(Me.clcTipo, 0)
        Me.Controls.SetChildIndex(Me.lblTipoVale, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboForma.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oTiposVales As VillarrealBusiness.clsTiposVales
#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmTiposVales_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        Me.oTiposVales = New VillarrealBusiness.clsTiposVales
    End Sub

    Private Sub frmTiposVales_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oTiposVales.Insertar(Me.clcTipo.EditValue, Me.txtNombre.Text, Me.cboConcepto.EditValue, Me.cboForma.EditValue)

            Case Actions.Update
                Response = oTiposVales.Actualizar(Me.clcTipo.EditValue, Me.txtNombre.Text, Me.cboConcepto.EditValue, Me.cboForma.EditValue)

            Case Actions.Delete
                Response = oTiposVales.Eliminar(clcTipo.Value)
        End Select
    End Sub

    Private Sub frmTiposVales_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = Me.oTiposVales.Validacion(Action, Me.txtNombre.Text)
    End Sub

    Private Sub frmTiposVales_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oTiposVales.DespliegaDatos(OwnerForm.Value("tipo"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            If oDataSet.Tables(0).Rows.Count > 0 Then
                Me.clcTipo.EditValue = oDataSet.Tables(0).Rows(0).Item("tipo")
                Me.txtNombre.Text = oDataSet.Tables(0).Rows(0).Item("nombre")
                Me.cboConcepto.EditValue = oDataSet.Tables(0).Rows(0).Item("concepto")
                Me.cboForma.EditValue = oDataSet.Tables(0).Rows(0).Item("forma")
            End If
        End If

    End Sub

    Private Sub frmTiposVales_Localize() Handles MyBase.Localize
        Find("tipo", Me.clcTipo.EditValue)
    End Sub
#End Region

#Region "DIPROS Systems, Eventos de Controles"

#End Region

#Region "DIPROS Systems, Funcionalidad"
#End Region

   
End Class
