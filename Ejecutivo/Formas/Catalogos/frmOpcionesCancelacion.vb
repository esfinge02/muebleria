Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmOpcionesCancelacion
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblPlan_Credito As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcOpcionCancelacion As Dipros.Editors.TINCalcEdit
    Friend WithEvents chkActivo As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOpcionesCancelacion))
        Me.lblPlan_Credito = New System.Windows.Forms.Label
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.clcOpcionCancelacion = New Dipros.Editors.TINCalcEdit
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.chkActivo = New DevExpress.XtraEditors.CheckEdit
        CType(Me.clcOpcionCancelacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(394, 28)
        '
        'lblPlan_Credito
        '
        Me.lblPlan_Credito.AutoSize = True
        Me.lblPlan_Credito.Location = New System.Drawing.Point(37, 43)
        Me.lblPlan_Credito.Name = "lblPlan_Credito"
        Me.lblPlan_Credito.Size = New System.Drawing.Size(47, 16)
        Me.lblPlan_Credito.TabIndex = 59
        Me.lblPlan_Credito.Tag = ""
        Me.lblPlan_Credito.Text = "Opci�n:"
        Me.lblPlan_Credito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(13, 64)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(72, 16)
        Me.lblDescripcion.TabIndex = 61
        Me.lblDescripcion.Tag = ""
        Me.lblDescripcion.Text = "&Descripci�n:"
        Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcOpcionCancelacion
        '
        Me.clcOpcionCancelacion.EditValue = "0"
        Me.clcOpcionCancelacion.Location = New System.Drawing.Point(93, 40)
        Me.clcOpcionCancelacion.MaxValue = 0
        Me.clcOpcionCancelacion.MinValue = 0
        Me.clcOpcionCancelacion.Name = "clcOpcionCancelacion"
        '
        'clcOpcionCancelacion.Properties
        '
        Me.clcOpcionCancelacion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOpcionCancelacion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcOpcionCancelacion.Properties.Enabled = False
        Me.clcOpcionCancelacion.Properties.MaskData.EditMask = "###,###,##0"
        Me.clcOpcionCancelacion.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcOpcionCancelacion.Size = New System.Drawing.Size(59, 19)
        Me.clcOpcionCancelacion.TabIndex = 60
        Me.clcOpcionCancelacion.Tag = "opcion"
        Me.clcOpcionCancelacion.ToolTip = "Opci�n de Cancelaci�n"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(93, 63)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.MaxLength = 60
        Me.txtDescripcion.Size = New System.Drawing.Size(336, 20)
        Me.txtDescripcion.TabIndex = 62
        Me.txtDescripcion.Tag = "descripcion"
        Me.txtDescripcion.ToolTip = "descripci�n"
        '
        'chkActivo
        '
        Me.chkActivo.Location = New System.Drawing.Point(368, 87)
        Me.chkActivo.Name = "chkActivo"
        '
        'chkActivo.Properties
        '
        Me.chkActivo.Properties.Caption = "Activo"
        Me.chkActivo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkActivo.Size = New System.Drawing.Size(64, 19)
        Me.chkActivo.TabIndex = 63
        Me.chkActivo.Tag = "activo"
        Me.chkActivo.ToolTip = "Activo"
        '
        'frmOpcionesCancelacion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(442, 108)
        Me.Controls.Add(Me.chkActivo)
        Me.Controls.Add(Me.lblPlan_Credito)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.clcOpcionCancelacion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Name = "frmOpcionesCancelacion"
        Me.Text = "frmOpcionesCancelacion"
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.clcOpcionCancelacion, 0)
        Me.Controls.SetChildIndex(Me.lblDescripcion, 0)
        Me.Controls.SetChildIndex(Me.lblPlan_Credito, 0)
        Me.Controls.SetChildIndex(Me.chkActivo, 0)
        CType(Me.clcOpcionCancelacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Transaccion"
    Private Sub frmOpcionesCancelacion_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmOpcionesCancelacion_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmOpcionesCancelacion_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
#End Region

    Private oOpciones As VillarrealBusiness.clsOpcionesCancelacion

    Private Sub frmOpcionesCancelacion_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oOpciones = New VillarrealBusiness.clsOpcionesCancelacion

    End Sub

    Private Sub frmOpcionesCancelacion_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oOpciones.Validacion(Me.Action, Me.txtDescripcion.Text)
    End Sub

    Private Sub frmOpcionesCancelacion_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oOpciones.DespliegaDatos(OwnerForm.Value("opcion"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

        End If
    End Sub

    Private Sub frmOpcionesCancelacion_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oOpciones.Insertar(Me.DataSource)

            Case Actions.Update
                Response = oOpciones.Actualizar(Me.DataSource)

            Case Actions.Delete
                Response = oOpciones.Eliminar(Me.clcOpcionCancelacion.EditValue)

        End Select
    End Sub



    Private Sub frmOpcionesCancelacion_Localize() Handles MyBase.Localize
        Find("opcion", CType(Me.clcOpcionCancelacion.EditValue, Object))
    End Sub


End Class
