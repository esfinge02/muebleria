Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmAnticipos
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents grcsucursal_recibo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfolio_recibo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcfecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcimporte As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gpbFactura As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Public WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Public WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents clcFolio As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcImporte As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcSaldo As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents dteVigencia As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cboEstatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents grValesDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvValesDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents repClcFolio_Recibo As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tblReimprimirVale As System.Windows.Forms.ToolBarButton
    Friend WithEvents clcFolioFactura As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents txtSerieFactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grFormaspago As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFormaspago As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcClaveFormapago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFormapago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcManejaDolares As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkManejadolares As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcTipocambio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcDolares As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rclcImporte As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPredeterminada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkPredeterminada As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcsolicita_ultimos_digitos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaFormasPago As Dipros.Windows.TINMaster
    Public WithEvents lblTipoventa As System.Windows.Forms.Label
    Public WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboTipoventa As DevExpress.XtraEditors.ImageComboBoxEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAnticipos))
        Me.grValesDetalle = New DevExpress.XtraGrid.GridControl
        Me.grvValesDetalle = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcsucursal_recibo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcfolio_recibo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.repClcFolio_Recibo = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcfecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.grcimporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.gpbFactura = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.clcFolioFactura = New DevExpress.XtraEditors.CalcEdit
        Me.txtSerieFactura = New DevExpress.XtraEditors.TextEdit
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.clcFolio = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.clcImporte = New DevExpress.XtraEditors.CalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.clcSaldo = New DevExpress.XtraEditors.CalcEdit
        Me.dteVigencia = New DevExpress.XtraEditors.DateEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblTipoventa = New System.Windows.Forms.Label
        Me.cboEstatus = New DevExpress.XtraEditors.ImageComboBoxEdit
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tblReimprimirVale = New System.Windows.Forms.ToolBarButton
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.grFormaspago = New DevExpress.XtraGrid.GridControl
        Me.grvFormaspago = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcClaveFormapago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFormapago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcManejaDolares = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkManejadolares = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcTipocambio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcDolares = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rclcImporte = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPredeterminada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkPredeterminada = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grcsolicita_ultimos_digitos = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tmaFormasPago = New Dipros.Windows.TINMaster
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.Label10 = New System.Windows.Forms.Label
        Me.cboTipoventa = New DevExpress.XtraEditors.ImageComboBoxEdit
        CType(Me.grValesDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvValesDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repClcFolio_Recibo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbFactura.SuspendLayout()
        CType(Me.clcFolioFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerieFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteVigencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.grFormaspago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFormaspago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkManejadolares, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rclcImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPredeterminada, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tblReimprimirVale})
        Me.tbrTools.Location = New System.Drawing.Point(23, 0)
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(4510, 28)
        '
        'grValesDetalle
        '
        Me.grValesDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grValesDetalle.EmbeddedNavigator
        '
        Me.grValesDetalle.EmbeddedNavigator.Name = ""
        Me.grValesDetalle.Location = New System.Drawing.Point(0, 0)
        Me.grValesDetalle.MainView = Me.grvValesDetalle
        Me.grValesDetalle.Name = "grValesDetalle"
        Me.grValesDetalle.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1, Me.RepositoryItemDateEdit1, Me.repClcFolio_Recibo})
        Me.grValesDetalle.Size = New System.Drawing.Size(600, 206)
        Me.grValesDetalle.TabIndex = 15
        Me.grValesDetalle.Text = "GridControl1"
        '
        'grvValesDetalle
        '
        Me.grvValesDetalle.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcsucursal_recibo, Me.grcfolio_recibo, Me.grcfecha, Me.grcimporte, Me.grcFactura})
        Me.grvValesDetalle.GridControl = Me.grValesDetalle
        Me.grvValesDetalle.Name = "grvValesDetalle"
        Me.grvValesDetalle.OptionsView.ShowGroupPanel = False
        '
        'grcsucursal_recibo
        '
        Me.grcsucursal_recibo.Caption = "Sucursal Recibo"
        Me.grcsucursal_recibo.FieldName = "sucursal_recibo"
        Me.grcsucursal_recibo.Name = "grcsucursal_recibo"
        Me.grcsucursal_recibo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcsucursal_recibo.VisibleIndex = 0
        Me.grcsucursal_recibo.Width = 129
        '
        'grcfolio_recibo
        '
        Me.grcfolio_recibo.Caption = "Uso en Factura"
        Me.grcfolio_recibo.ColumnEdit = Me.repClcFolio_Recibo
        Me.grcfolio_recibo.FieldName = "usado_factura"
        Me.grcfolio_recibo.Name = "grcfolio_recibo"
        Me.grcfolio_recibo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcfolio_recibo.VisibleIndex = 1
        Me.grcfolio_recibo.Width = 86
        '
        'repClcFolio_Recibo
        '
        Me.repClcFolio_Recibo.AutoHeight = False
        Me.repClcFolio_Recibo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.repClcFolio_Recibo.DisplayFormat.FormatString = "n0"
        Me.repClcFolio_Recibo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.repClcFolio_Recibo.Name = "repClcFolio_Recibo"
        '
        'grcfecha
        '
        Me.grcfecha.Caption = "Fecha"
        Me.grcfecha.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.grcfecha.FieldName = "fecha"
        Me.grcfecha.Name = "grcfecha"
        Me.grcfecha.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcfecha.VisibleIndex = 2
        Me.grcfecha.Width = 105
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'grcimporte
        '
        Me.grcimporte.Caption = "Importe"
        Me.grcimporte.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.grcimporte.DisplayFormat.FormatString = "c2"
        Me.grcimporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcimporte.FieldName = "importe"
        Me.grcimporte.Name = "grcimporte"
        Me.grcimporte.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcimporte.VisibleIndex = 3
        Me.grcimporte.Width = 99
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatString = "c2"
        Me.RepositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura Anticipo"
        Me.grcFactura.FieldName = "Factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.Width = 87
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = "10/04/2006"
        Me.dteFecha.Location = New System.Drawing.Point(529, 40)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFecha.Size = New System.Drawing.Size(95, 23)
        Me.dteFecha.TabIndex = 5
        Me.dteFecha.Tag = "fecha"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(483, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "Fecha:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbFactura
        '
        Me.gpbFactura.Controls.Add(Me.Label5)
        Me.gpbFactura.Controls.Add(Me.lblSucursal)
        Me.gpbFactura.Controls.Add(Me.lblOrden)
        Me.gpbFactura.Controls.Add(Me.lkpSucursal)
        Me.gpbFactura.Controls.Add(Me.clcFolioFactura)
        Me.gpbFactura.Controls.Add(Me.txtSerieFactura)
        Me.gpbFactura.Enabled = False
        Me.gpbFactura.Location = New System.Drawing.Point(16, 151)
        Me.gpbFactura.Name = "gpbFactura"
        Me.gpbFactura.Size = New System.Drawing.Size(291, 96)
        Me.gpbFactura.TabIndex = 14
        Me.gpbFactura.TabStop = False
        Me.gpbFactura.Text = "Factura:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(58, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 17)
        Me.Label5.TabIndex = 4
        Me.Label5.Tag = ""
        Me.Label5.Text = "&Folio:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(38, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(55, 40)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 2
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(99, 14)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(173, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "sucursal_factura"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'clcFolioFactura
        '
        Me.clcFolioFactura.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolioFactura.Location = New System.Drawing.Point(99, 64)
        Me.clcFolioFactura.Name = "clcFolioFactura"
        '
        'clcFolioFactura.Properties
        '
        Me.clcFolioFactura.Properties.Enabled = False
        Me.clcFolioFactura.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolioFactura.Size = New System.Drawing.Size(80, 20)
        Me.clcFolioFactura.TabIndex = 59
        Me.clcFolioFactura.Tag = "folio_factura"
        '
        'txtSerieFactura
        '
        Me.txtSerieFactura.EditValue = ""
        Me.txtSerieFactura.Location = New System.Drawing.Point(99, 39)
        Me.txtSerieFactura.Name = "txtSerieFactura"
        '
        'txtSerieFactura.Properties
        '
        Me.txtSerieFactura.Properties.Enabled = False
        Me.txtSerieFactura.Properties.MaxLength = 50
        Me.txtSerieFactura.Size = New System.Drawing.Size(80, 20)
        Me.txtSerieFactura.TabIndex = 60
        Me.txtSerieFactura.Tag = "serie_factura"
        Me.txtSerieFactura.ToolTip = "descripci�n"
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(64, 64)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(520, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(344, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 7
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(16, 64)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 6
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcFolio
        '
        Me.clcFolio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolio.Location = New System.Drawing.Point(64, 40)
        Me.clcFolio.Name = "clcFolio"
        '
        'clcFolio.Properties
        '
        Me.clcFolio.Properties.Enabled = False
        Me.clcFolio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolio.Size = New System.Drawing.Size(80, 20)
        Me.clcFolio.TabIndex = 1
        Me.clcFolio.Tag = "folio"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(24, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "Folio:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcImporte.Location = New System.Drawing.Point(120, 40)
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "c2"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcImporte.Size = New System.Drawing.Size(96, 20)
        Me.clcImporte.TabIndex = 3
        Me.clcImporte.Tag = "importe"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(61, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Tag = ""
        Me.Label4.Text = "Importe:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(74, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Tag = ""
        Me.Label6.Text = "Saldo:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcSaldo
        '
        Me.clcSaldo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcSaldo.Location = New System.Drawing.Point(120, 64)
        Me.clcSaldo.Name = "clcSaldo"
        '
        'clcSaldo.Properties
        '
        Me.clcSaldo.Properties.DisplayFormat.FormatString = "c2"
        Me.clcSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.EditFormat.FormatString = "c2"
        Me.clcSaldo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcSaldo.Properties.Enabled = False
        Me.clcSaldo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcSaldo.Size = New System.Drawing.Size(96, 20)
        Me.clcSaldo.TabIndex = 5
        Me.clcSaldo.Tag = "saldo"
        '
        'dteVigencia
        '
        Me.dteVigencia.EditValue = "10/04/2006"
        Me.dteVigencia.Location = New System.Drawing.Point(120, 16)
        Me.dteVigencia.Name = "dteVigencia"
        '
        'dteVigencia.Properties
        '
        Me.dteVigencia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteVigencia.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteVigencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteVigencia.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteVigencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteVigencia.Properties.Enabled = False
        Me.dteVigencia.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteVigencia.Size = New System.Drawing.Size(95, 23)
        Me.dteVigencia.TabIndex = 1
        Me.dteVigencia.Tag = "vigencia"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(59, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(55, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Tag = ""
        Me.Label7.Text = "Vigencia:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(192, 40)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(95, 16)
        Me.Label8.TabIndex = 2
        Me.Label8.Tag = ""
        Me.Label8.Text = "Usuario Genero:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(288, 40)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(0, 16)
        Me.Label9.TabIndex = 3
        Me.Label9.Tag = "usuario_genera"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTipoventa
        '
        Me.lblTipoventa.AutoSize = True
        Me.lblTipoventa.Location = New System.Drawing.Point(458, 64)
        Me.lblTipoventa.Name = "lblTipoventa"
        Me.lblTipoventa.Size = New System.Drawing.Size(50, 16)
        Me.lblTipoventa.TabIndex = 8
        Me.lblTipoventa.Tag = ""
        Me.lblTipoventa.Text = "Estatus:"
        Me.lblTipoventa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEstatus
        '
        Me.cboEstatus.EditValue = "A"
        Me.cboEstatus.Location = New System.Drawing.Point(512, 64)
        Me.cboEstatus.Name = "cboEstatus"
        '
        'cboEstatus.Properties
        '
        Me.cboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEstatus.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Activo", "A", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Canjeado", "J", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cambiado", "E", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelado", "C", -1)})
        Me.cboEstatus.Size = New System.Drawing.Size(112, 23)
        Me.cboEstatus.TabIndex = 9
        Me.cboEstatus.Tag = "estatus"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dteVigencia)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.clcImporte)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.clcSaldo)
        Me.GroupBox1.Location = New System.Drawing.Point(336, 151)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(291, 96)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        '
        'tblReimprimirVale
        '
        Me.tblReimprimirVale.Enabled = False
        Me.tblReimprimirVale.Text = "Reimprimir"
        Me.tblReimprimirVale.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 88)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 16)
        Me.Label11.TabIndex = 10
        Me.Label11.Tag = ""
        Me.Label11.Text = "Concepto:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtConcepto
        '
        Me.txtConcepto.EditValue = ""
        Me.txtConcepto.Location = New System.Drawing.Point(64, 88)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(344, 56)
        Me.txtConcepto.TabIndex = 11
        Me.txtConcepto.Tag = "concepto"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(16, 255)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(608, 232)
        Me.TabControl1.TabIndex = 16
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.Transparent
        Me.TabPage1.Controls.Add(Me.grFormaspago)
        Me.TabPage1.Controls.Add(Me.tmaFormasPago)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(600, 206)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Formas de Pago"
        '
        'grFormaspago
        '
        Me.grFormaspago.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'grFormaspago.EmbeddedNavigator
        '
        Me.grFormaspago.EmbeddedNavigator.Name = ""
        Me.grFormaspago.Location = New System.Drawing.Point(0, 23)
        Me.grFormaspago.MainView = Me.grvFormaspago
        Me.grFormaspago.Name = "grFormaspago"
        Me.grFormaspago.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkManejadolares, Me.rclcImporte, Me.chkPredeterminada})
        Me.grFormaspago.Size = New System.Drawing.Size(600, 183)
        Me.grFormaspago.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Info, System.Drawing.SystemColors.ControlText, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grFormaspago.TabIndex = 63
        Me.grFormaspago.TabStop = False
        '
        'grvFormaspago
        '
        Me.grvFormaspago.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcClaveFormapago, Me.grcFormapago, Me.grcManejaDolares, Me.grcTipocambio, Me.grcDolares, Me.GridColumn1, Me.grcPredeterminada, Me.grcsolicita_ultimos_digitos})
        Me.grvFormaspago.GridControl = Me.grFormaspago
        Me.grvFormaspago.GroupPanelText = "Precios"
        Me.grvFormaspago.Name = "grvFormaspago"
        Me.grvFormaspago.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvFormaspago.OptionsView.ShowFooter = True
        Me.grvFormaspago.OptionsView.ShowGroupPanel = False
        Me.grvFormaspago.OptionsView.ShowIndicator = False
        '
        'grcClaveFormapago
        '
        Me.grcClaveFormapago.Caption = "Clave Bodega"
        Me.grcClaveFormapago.FieldName = "forma_pago"
        Me.grcClaveFormapago.Name = "grcClaveFormapago"
        Me.grcClaveFormapago.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcFormapago
        '
        Me.grcFormapago.Caption = "Forma pago"
        Me.grcFormapago.FieldName = "descripcion"
        Me.grcFormapago.Name = "grcFormapago"
        Me.grcFormapago.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFormapago.VisibleIndex = 0
        Me.grcFormapago.Width = 105
        '
        'grcManejaDolares
        '
        Me.grcManejaDolares.Caption = "Maneja d�lares"
        Me.grcManejaDolares.ColumnEdit = Me.chkManejadolares
        Me.grcManejaDolares.FieldName = "maneja_dolares"
        Me.grcManejaDolares.Name = "grcManejaDolares"
        Me.grcManejaDolares.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcManejaDolares.Width = 90
        '
        'chkManejadolares
        '
        Me.chkManejadolares.AutoHeight = False
        Me.chkManejadolares.Name = "chkManejadolares"
        '
        'grcTipocambio
        '
        Me.grcTipocambio.Caption = "Tipo de Cambio"
        Me.grcTipocambio.DisplayFormat.FormatString = "c2"
        Me.grcTipocambio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcTipocambio.FieldName = "tipo_cambio"
        Me.grcTipocambio.Name = "grcTipocambio"
        Me.grcTipocambio.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcTipocambio.VisibleIndex = 1
        Me.grcTipocambio.Width = 89
        '
        'grcDolares
        '
        Me.grcDolares.Caption = "D�lares"
        Me.grcDolares.ColumnEdit = Me.rclcImporte
        Me.grcDolares.DisplayFormat.FormatString = "c2"
        Me.grcDolares.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcDolares.FieldName = "dolares"
        Me.grcDolares.Name = "grcDolares"
        Me.grcDolares.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcDolares.VisibleIndex = 2
        Me.grcDolares.Width = 66
        '
        'rclcImporte
        '
        Me.rclcImporte.AutoHeight = False
        Me.rclcImporte.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, False, False, DevExpress.Utils.HorzAlignment.Center, Nothing)})
        Me.rclcImporte.DisplayFormat.FormatString = "c2"
        Me.rclcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rclcImporte.EditFormat.FormatString = "n2"
        Me.rclcImporte.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.rclcImporte.Name = "rclcImporte"
        Me.rclcImporte.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never
        Me.rclcImporte.ShowPopupShadow = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Importe"
        Me.GridColumn1.ColumnEdit = Me.rclcImporte
        Me.GridColumn1.DisplayFormat.FormatString = "c2"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn1.FieldName = "importe"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.SummaryItem.DisplayFormat = "{0:c2}"
        Me.GridColumn1.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.GridColumn1.SummaryItem.Tag = "importe"
        Me.GridColumn1.VisibleIndex = 3
        Me.GridColumn1.Width = 67
        '
        'grcPredeterminada
        '
        Me.grcPredeterminada.Caption = "Predeterminada"
        Me.grcPredeterminada.ColumnEdit = Me.chkPredeterminada
        Me.grcPredeterminada.FieldName = "predeterminada"
        Me.grcPredeterminada.Name = "grcPredeterminada"
        Me.grcPredeterminada.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPredeterminada.Width = 91
        '
        'chkPredeterminada
        '
        Me.chkPredeterminada.AutoHeight = False
        Me.chkPredeterminada.Name = "chkPredeterminada"
        '
        'grcsolicita_ultimos_digitos
        '
        Me.grcsolicita_ultimos_digitos.Caption = "solicita_ultimos_digitos"
        Me.grcsolicita_ultimos_digitos.ColumnEdit = Me.chkPredeterminada
        Me.grcsolicita_ultimos_digitos.FieldName = "solicitar_ulitmos_digitos"
        Me.grcsolicita_ultimos_digitos.Name = "grcsolicita_ultimos_digitos"
        Me.grcsolicita_ultimos_digitos.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'tmaFormasPago
        '
        Me.tmaFormasPago.BackColor = System.Drawing.Color.White
        Me.tmaFormasPago.CanDelete = True
        Me.tmaFormasPago.CanInsert = True
        Me.tmaFormasPago.CanUpdate = True
        Me.tmaFormasPago.Dock = System.Windows.Forms.DockStyle.Top
        Me.tmaFormasPago.Grid = Me.grFormaspago
        Me.tmaFormasPago.Location = New System.Drawing.Point(0, 0)
        Me.tmaFormasPago.Name = "tmaFormasPago"
        Me.tmaFormasPago.Size = New System.Drawing.Size(600, 23)
        Me.tmaFormasPago.TabIndex = 62
        Me.tmaFormasPago.TabStop = False
        Me.tmaFormasPago.Title = "Formas de Pago"
        Me.tmaFormasPago.UpdateTitle = "una forma de pago"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grValesDetalle)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(600, 206)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Utilizado en los Documentos"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(424, 91)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(84, 16)
        Me.Label10.TabIndex = 12
        Me.Label10.Tag = ""
        Me.Label10.Text = "&Tipo de venta:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTipoventa
        '
        Me.cboTipoventa.EditValue = "C"
        Me.cboTipoventa.Location = New System.Drawing.Point(512, 90)
        Me.cboTipoventa.Name = "cboTipoventa"
        '
        'cboTipoventa.Properties
        '
        Me.cboTipoventa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoventa.Properties.Items.AddRange(New Object() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Contado", "D", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cr�dito", "C", -1)})
        Me.cboTipoventa.Size = New System.Drawing.Size(112, 23)
        Me.cboTipoventa.TabIndex = 13
        Me.cboTipoventa.Tag = "tipo_venta"
        '
        'frmAnticipos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(634, 500)
        Me.Controls.Add(Me.cboEstatus)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cboTipoventa)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.txtConcepto)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblTipoventa)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.clcFolio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.gpbFactura)
        Me.Name = "frmAnticipos"
        Me.Text = "frmVales"
        Me.Controls.SetChildIndex(Me.gpbFactura, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.clcFolio, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        Me.Controls.SetChildIndex(Me.lblTipoventa, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.Label11, 0)
        Me.Controls.SetChildIndex(Me.txtConcepto, 0)
        Me.Controls.SetChildIndex(Me.TabControl1, 0)
        Me.Controls.SetChildIndex(Me.cboTipoventa, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.cboEstatus, 0)
        CType(Me.grValesDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvValesDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repClcFolio_Recibo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbFactura.ResumeLayout(False)
        CType(Me.clcFolioFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerieFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteVigencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.grFormaspago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFormaspago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkManejadolares, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rclcImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPredeterminada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.cboTipoventa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Declaraciones"
    Private oAnticipos As VillarrealBusiness.clsAnticipos
    Private oAnticiposDetalle As VillarrealBusiness.clsAnticiposDetalle
    Private oClientes As VillarrealBusiness.clsClientes
    Private oReportes As VillarrealBusiness.Reportes
    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Private oAnticiposFormasPago As VillarrealBusiness.clsAnticiposFormasPago
    Private oVariables As VillarrealBusiness.clsVariables

    Private oVentasDetalle As VillarrealBusiness.clsVentasDetalle
    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oMovimientosCobrarFormasPago As VillarrealBusiness.clsMovimientosCobrarFormasPago

    Private CantidadValesDetalleAbonos As Long = 0
    Private impuesto As Double
    Private impuesto_porcentaje As Double = 0

    Private clcSubtotal As Double = 0
    Private clcImpuesto As Double = 0

    Private folio_factura As Long = 0
    Private SerieFactura As String = ""
    Private SerieRecibos As String = ""
    Private FolioRecibos As Long = 0
    Private factura_electronica As Boolean


    Private ConceptoFacturaContado As String
    Private ConceptoFacturaCredito As String
    Private ConceptoAbono As String
    Private banConceptoAbono As Boolean = False


    Private clcCp As Long
    Private txtDomicilio As String
    Private txtColonia As String
    Private txtCiudad As String
    Private txtMunicipio As String
    Private txtCurp As String
    Private txtEstado As String
    Private txtFax As String
    Private txtRfc As String
    Private txtTelefono1 As String
    Private txtTelefono2 As String
    Private txtNumInterior As String
    Private txtNumExterior As String
    Private dLimiteCredito As Double
    Private chkRfcCompleto As Boolean
    Private chkIvadesglosado As Boolean




    Public ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property
    Public ReadOnly Property FolioVenta() As Long
        Get
            Return Me.clcFolioFactura.Value
            'Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolio)
        End Get
    End Property
    Public ReadOnly Property serie() As String
        Get
            Return Me.txtSerieFactura.Text
            'Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie)
        End Get
    End Property
    Public ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Private ReadOnly Property ArticuloAnticipos() As Long
        Get
            Return oVariables.TraeDatos("articulo_anticipos", VillarrealBusiness.clsVariables.tipo_dato.Entero)
        End Get
    End Property


#End Region

#Region "Eventos de la Forma"

    Private Sub frmAnticipos_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub
    Private Sub frmAnticipos_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()

        ImprimeFactura(SerieFactura, folio_factura, Me.chkIvadesglosado, factura_electronica)
    End Sub
    Private Sub frmAnticipos_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub


    Private Sub frmVales_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = oAnticipos.Insertar(Me.clcFolio.Value, Me.dteFecha.DateTime, Me.Cliente, Me.txtConcepto.Text, Me.sucursal, Me.serie, Me.FolioVenta, Me.clcImporte.Value, Me.clcSaldo.Value, Me.cboEstatus.Value, Me.dteVigencia.DateTime, Me.cboTipoventa.EditValue)
            Case Actions.Update
                Response = oAnticipos.Actualizar(Me.clcFolio.Value, Me.dteFecha.DateTime, Me.Cliente, Me.txtConcepto.Text, Me.sucursal, Me.serie, Me.FolioVenta, Me.clcImporte.Value, Me.clcSaldo.Value, Me.cboEstatus.Value, Me.dteVigencia.DateTime, Me.cboTipoventa.EditValue)

        End Select

    End Sub

    Private Sub frmVales_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oAnticipos = New VillarrealBusiness.clsAnticipos
        oAnticiposDetalle = New VillarrealBusiness.clsAnticiposDetalle
        oClientes = New VillarrealBusiness.clsClientes
        oReportes = New VillarrealBusiness.Reportes
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oAnticiposFormasPago = New VillarrealBusiness.clsAnticiposFormasPago
        oVariables = New VillarrealBusiness.clsVariables
        oVentasDetalle = New VillarrealBusiness.clsVentasDetalle
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oMovimientosCobrarFormasPago = New VillarrealBusiness.clsMovimientosCobrarFormasPago


        Me.dteFecha.DateTime = CDate(TINApp.FechaServidor)
        Me.dteVigencia.DateTime = DateAdd(DateInterval.Year, 1, Me.dteFecha.DateTime)

        Select Case Me.Action
            Case Actions.Insert
                Me.cboEstatus.Enabled = False
            Case Actions.Update
                Me.cboEstatus.Enabled = True
                Me.EnabledEdit(False)

        End Select
        ConfigurarMaster()
        TraeConceptoFacturaContado()
        TraeConceptoFacturaCredito()
        TraeConceptoAbono()


    End Sub

    Private Sub frmVales_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields

        If clcImporte.EditValue <= 0 Then
            Response = New Events
            Response.Message = "El importe es Requerido"
            Exit Sub
        End If


        If Me.cboTipoventa.EditValue = "D" Then
            Dim TotalFormaPago As Double = ObtenerTotalFormaPago()

            If Me.clcImporte.EditValue > TotalFormaPago Then
                Response = New Events
                Response.Message = "La suma de las Formas de pago es menor al importe del anticipo"
                Exit Sub
            Else
                If Me.clcImporte.EditValue < TotalFormaPago Then
                    Response = New Events
                    Response.Message = "La suma de las Formas de pago es mayor al importe del anticipo"
                    Exit Sub
                End If
            End If
        End If


        Select Case Me.Action
            Case Actions.Insert

                If Me.ArticuloAnticipos < 0 Then
                    Response = New Events
                    Response.Message = "No esta configurado el articulo para la factura del anticipo"
                    Exit Sub
                End If


                impuesto = CType(oVariables.TraeDatos("impuesto", VillarrealBusiness.clsVariables.tipo_dato.Float), Double)

                If impuesto <> -1 Then
                    impuesto_porcentaje = 1 + (impuesto / 100)
                    Me.clcSubtotal = System.Math.Round(Me.clcImporte.EditValue / impuesto_porcentaje, 2)
                    Me.clcImpuesto = Me.clcImporte.EditValue - Me.clcSubtotal
                Else
                    Response.Message = "El Porcentaje de Impuesto no esta definido"
                End If
        End Select

        If Me.cboEstatus.Value = "C" Then
            If TINApp.Connection.User.ToUpper() = "SUPER" Then
                If CantidadValesDetalleAbonos > 0 Then
                    Response = New Events
                    Response.Message = "No se puede Cancelar el Anticipo por que tiene Facturas Relacionados"
                End If
            Else
                Response = New Events
                Response.Message = "Solo el Administrador del Sistema puede Cancelar el Anticipo"
            End If
        End If


    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tblReimprimirVale Then
            ImprimirVale(clcFolio.Value)
        End If
    End Sub

    Private Sub frmVales_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oAnticipos.DespliegaDatos(OwnerForm.Value("folio"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet
            TraerDatosFormasPago()
            TraerDatosDetalle()
            Me.cboEstatus.Enabled = False
            Me.cboTipoventa.Enabled = False
        End If
    End Sub

    Private Sub frmVales_Localize() Handles MyBase.Localize
        Find("folio", Me.clcFolio.EditValue)
    End Sub

    Private Sub frmAnticipos_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With tmaFormasPago
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oAnticiposFormasPago.Insertar(Me.clcFolio.EditValue, CType(tmaFormasPago.Item("partida"), Long), CType(tmaFormasPago.Item("forma_pago"), Long), CType(tmaFormasPago.Item("tipo_cambio"), Double), CType(tmaFormasPago.Item("dolares"), Long), CType(tmaFormasPago.Item("importe"), Double), CType(tmaFormasPago.Item("ultimos_digitos_cuenta"), String))
                    Case Actions.Update
                        Response = oAnticiposFormasPago.Actualizar(Me.clcFolio.EditValue, CType(tmaFormasPago.Item("partida"), Long), CType(tmaFormasPago.Item("forma_pago"), Long), CType(tmaFormasPago.Item("tipo_cambio"), Double), CType(tmaFormasPago.Item("dolares"), Long), CType(tmaFormasPago.Item("importe"), Double), CType(tmaFormasPago.Item("ultimos_digitos_cuenta"), String))
                    Case Actions.Delete
                        Response = oAnticiposFormasPago.Eliminar(Me.clcFolio.EditValue, CType(tmaFormasPago.Item("partida"), Long))
                End Select
                If Response.ErrorFound Then Exit Sub
                .MoveNext()
            Loop
        End With


        Select Case Me.Action
            Case Actions.Insert
                If Me.cboTipoventa.EditValue = "D" Then
                    Response = GenerarFacturaContado()
                End If
                If Me.cboTipoventa.EditValue = "C" Then
                    Response = GenerarFacturaCredito()
                End If

        End Select
    End Sub

#End Region

#Region "Eventos de los Controles"
    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        'lkpSerie_LoadData(True)
    End Sub



    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData

        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If
        Response = Nothing

    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        If Cliente <> -1 Then
            LlenarDatosCliente()
        Else
            LimpiarDatosCliente()
        End If

    End Sub
#End Region

#Region "Funcionalidad"

    Private Sub TraerDatosFormasPago()
        Dim response As Events
        Dim odataset As DataSet

        response = Me.oAnticiposFormasPago.DespliegaDatos(clcFolio.Value)
        If Not response.ErrorFound Then
            odataset = response.Value
            Me.tmaFormasPago.DataSource = odataset

        End If
    End Sub
    Private Sub TraerDatosDetalle()
        Dim response As Events
        Dim odataset As DataSet

        response = oAnticiposDetalle.DespliegaDatos(clcFolio.Value)
        If Not response.ErrorFound Then
            odataset = response.Value
            CantidadValesDetalleAbonos = IIf(odataset.Tables.Count > 0, odataset.Tables(0).Rows.Count, 0)
            Me.grValesDetalle.DataSource = odataset.Tables(0)

        End If
    End Sub
    Private Sub ImprimirVale(ByVal folio As Long)
        Dim response As New Events

        response = oReportes.ImprimeVale(folio)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "El Vale no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New Comunes.rptValeFormato

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)


                TINApp.ShowReport(Me.MdiParent, "Impresi�n del Vale ", oReport, , , , True)
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub
    Private Sub ImprimeFactura(ByVal serie As String, ByVal folio As Long, ByVal ivadesglosado As Boolean, ByVal factura_electronica As Boolean)
        Dim response As New Events
        Dim concepto_fac As String


        concepto_fac = CType(oVariables.TraeDatos("concepto_cxc_factura_contado", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)

        response = oVentas.ImpresionFactura(Comunes.Common.Sucursal_Actual, serie, folio, concepto_fac, ivadesglosado)

        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La Impresi�n de la Factura no se puede Mostrar")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then

                Dim oDataSet As DataSet
                If Not factura_electronica Then
                    Dim oReport As New Comunes.rptFactura
                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    If TINApp.Connection.User.ToUpper = "SUPER" Then
                        TINApp.ShowReport(Me.MdiParent, "Factura", oReport, False, Nothing, True, True)
                    Else
                        TINApp.PrintReport(oReport, False, True)
                    End If
                    oReport = Nothing

                Else
                    Dim oReport As New Comunes.rptFacturaFormato
                    oDataSet = response.Value
                    oReport.DataSource = oDataSet.Tables(0)
                    oreport.txtLeyendaReparto.Visible = True
                    oreport.txtLeyendaReparto.Text = "CLIENTE"
                    oreport.txtDescripcionArticulo.MultiLine = CType(oDataSet.Tables(0).Rows(0).Item("EsMultiline"), Boolean)


                    If TINApp.Connection.User.ToUpper = "SUPER" Then
                        TINApp.ShowReport(Me.MdiParent, "Factura Electronica", oReport, False, Nothing, True, True)
                    Else
                        TINApp.PrintReport(oReport, False, True)
                    End If
                    oReport = Nothing
                End If

                oDataSet = Nothing
            Else
                ShowMessage(MessageType.MsgInformation, "La Factura no tiene Informaci�n")
            End If

        End If

    End Sub
    Private Function GenerarFacturaContado() As Events

        Dim response As New Events
    
        ' Sucursales
        '-----------------------------
        Dim oDataSet As DataSet


        response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        oDataSet = CType(response.Value, DataSet)
        factura_electronica = CType(oDataSet.Tables(0).Rows(0).Item("factura_electronica"), Boolean)
        SerieFactura = CType(oDataSet.Tables(0).Rows(0).Item("serie_factura_electronica"), String)
        SerieRecibos = CType(oDataSet.Tables(0).Rows(0).Item("serie_recibos_electronica"), String)
        folio_factura = CType(CType(oDataSet.Tables(0).Rows(0).Item("folio_factura_electronica"), Integer) + 1, String)

        '-----------------------------

        If Not factura_electronica Then
            SerieFactura = Comunes.clsUtilerias.uti_SeriePV(Comunes.Common.Caja_Actual)
            folio_factura = uti_FolioPV(Comunes.Common.PuntoVenta_Actual)
        End If

        impuesto_porcentaje = 1 + (impuesto / 100)
        Me.clcSubtotal = System.Math.Round(Me.clcImporte.EditValue / impuesto_porcentaje, 2)
        Me.clcImpuesto = Me.clcImporte.EditValue - Me.clcSubtotal

        response = oVentas.InsertarAnticipo(Comunes.Common.Sucursal_Actual, SerieFactura, folio_factura, Me.dteFecha.DateTime, 0, Me.Cliente, Me.txtDomicilio, Me.txtColonia, Me.txtCiudad, Me.txtMunicipio, Me.clcCp, Me.txtEstado, Me.txtTelefono1, Me.txtTelefono2, Me.txtFax, Me.txtCurp, Me.txtRfc, Me.cboTipoventa.EditValue, Me.chkIvadesglosado, "", "", Me.clcImporte.EditValue, 0, 0, Me.clcSubtotal, Me.clcImpuesto, Me.clcImporte.EditValue, 0, 0, _
                    "01/01/1900", 0, 0, 0.0, False, "01/01/1900", 0.0, False, 0.0, False, False, 0, -1, -1, Comunes.Common.PuntoVenta_Actual, -1, False, -1, -1)

        If Not response.ErrorFound Then

            response = oVentasDetalle.InsertarAnticipo(Comunes.Common.Sucursal_Actual, SerieFactura, folio_factura, 1, Me.ArticuloAnticipos, Me.clcImporte.EditValue, Me.clcImporte.EditValue, False, 0, False, False, "01", -1, 0, -1, -1, -1, Me.clcImporte.EditValue, Me.txtConcepto.Text, True)
        Else
            response.Message = "Error, al Insertar el Encabezado de la Factura de Contado"
        End If


        '----------------------------------------------------------------------------------------------------
        '----------------------------------------------------------------------------------------------------


        Dim subtotal_cargo_abono As Double = 0
        Dim iva_cargo_abono As Double = 0


        If impuesto <> -1 Then
            subtotal_cargo_abono = Me.clcImporte.EditValue / (1 + (impuesto / 100))
            iva_cargo_abono = Me.clcImporte.EditValue - subtotal_cargo_abono
        Else
            response.Message = "El Porcentaje de Impuesto no esta definido"
            Exit Function
        End If
        If Not response.ErrorFound Then
            response = oMovimientosCobrar.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFacturaContado, SerieFactura, folio_factura, Cliente, 0, Me.dteFecha.DateTime.Date, -1, -1, -1, 1, Me.clcImporte.EditValue, 0, subtotal_cargo_abono, iva_cargo_abono, Me.clcImporte.EditValue, Me.clcImporte.EditValue, CDate("01/01/1900").Date, "", "", False, -1)

            If Not response.ErrorFound Then
                response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFacturaContado, SerieFactura, folio_factura, Cliente, 1, 0, Me.dteFecha.DateTime.Date, Me.clcImporte.EditValue, -1, -1, -1, -1, -1, -1, 0, Me.dteFecha.DateTime.Date, Me.clcImporte.EditValue, "", "", 0)
            End If
        End If


        '----------------------------------------------------------------------------------------------
        If Not response.ErrorFound Then
            response = oMovimientosCobrar.InsertarCajas(Comunes.Common.Sucursal_Actual, Me.ConceptoAbono, Me.SerieRecibos, Me.Cliente, 0, Me.dteFecha.EditValue, Comunes.Common.Caja_Actual, Comunes.Common.Cajero, -1, 1, 0, Me.clcImporte.EditValue, subtotal_cargo_abono, iva_cargo_abono, Me.clcImporte.EditValue, 0, System.DBNull.Value, "", "", Comunes.Common.Sucursal_Actual, Me.FolioRecibos)
            If Not response.ErrorFound Then
                Dim sObservaciones As String
                sObservaciones = "Abono a la Referencia " + CType(Me.SerieFactura, String) + "-" + CType(Me.folio_factura, String) + " al documento " + CType(1, String)
                response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoAbono, SerieRecibos, Me.FolioRecibos, Me.Cliente, 1, 0, Me.dteFecha.DateTime.Date, Me.clcImporte.EditValue, Comunes.Common.Sucursal_Actual, ConceptoFacturaContado, Me.SerieFactura, Me.folio_factura, Me.Cliente, 1, 0, Me.dteFecha.DateTime.Date, 0, "C", sObservaciones, 0)

                'If Not response.ErrorFound Then
                '    Dim dolares As Double = 0
                '    Dim TipoCambio As Double = Me.Tipo_Cambio

                '    If Me.UcSolicitaFormaPago2.ManejaDolares Then
                '        dolares = Me.Total / TipoCambio
                '    End If
                '    response = Me.oMovimientosCobrarFormasPago.Insertar(sucursal, ConceptoAbono, SerieAbono, Me.FolioAbono, Me.ClienteFactura, Me.UcSolicitaFormaPago2.FormaPago, sucursal, Me.dteFecha.DateTime, Common.Caja_Actual, Common.Cajero, Me.Total, TipoCambio, dolares, Me.UcSolicitaFormaPago2.UltimosDigitos)
                'End If
            End If
        End If

        ' --------------------------------------------------------------------------------------------
        With tmaFormasPago
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        response = Me.oMovimientosCobrarFormasPago.Insertar(Comunes.Common.Sucursal_Actual, Me.ConceptoAbono, Me.SerieRecibos, Me.FolioRecibos, Me.Cliente, CType(tmaFormasPago.Item("forma_pago"), Long), Comunes.Common.Sucursal_Actual, Me.dteFecha.DateTime, Comunes.Common.Caja_Actual, Comunes.Common.Cajero, CType(tmaFormasPago.Item("importe"), Double), CType(tmaFormasPago.Item("tipo_cambio"), Double), CType(tmaFormasPago.Item("dolares"), Long), CType(tmaFormasPago.Item("ultimos_digitos_cuenta"), String))
                        'Case Actions.Update
                        '    response = oMovimientosCobrarFormasPago.Actualizar(Me.clcFolio.EditValue, CType(tmaFormasPago.Item("partida"), Long), CType(tmaFormasPago.Item("forma_pago"), Long), CType(tmaFormasPago.Item("tipo_cambio"), Double), CType(tmaFormasPago.Item("dolares"), Long), CType(tmaFormasPago.Item("importe"), Double), CType(tmaFormasPago.Item("ultimos_digitos_cuenta"), String))
                    Case Actions.Delete
                        response = oMovimientosCobrarFormasPago.Eliminar(Comunes.Common.Sucursal_Actual, Me.ConceptoAbono, Me.SerieRecibos, Me.FolioRecibos, Me.Cliente, CType(tmaFormasPago.Item("forma_pago"), Long))
                End Select
                If response.ErrorFound Then Exit Function
                .MoveNext()
            Loop
        End With
        ' --------------------------------------------------------------------------------------------

        If Not response.ErrorFound Then
            Dim ConceptoVenta As String
            ConceptoVenta = CType(oVariables.TraeDatos("concepto_venta", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
            If ConceptoVenta = "" Then
                ShowMessage(MessageType.MsgInformation, "El Concepto de Venta no esta definido", "Variables del Sistema", Nothing, False)
            End If
            response = oVentas.TimbrarFactura(Comunes.Common.Sucursal_Actual, SerieFactura, folio_factura, ConceptoVenta, Me.chkIvadesglosado, Comunes.Common.Sucursal_Actual)


            If Not response.ErrorFound Then
                response = Me.oAnticipos.ActualizaFacturaGenerada(Me.clcFolio.EditValue, Comunes.Common.Sucursal_Actual, SerieFactura, folio_factura)
            End If
        End If

        Return response

    End Function
    Private Function GenerarFacturaCredito() As Events
        Dim response As New Events

        Dim oDataSet As DataSet


        response = oSucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual)
        oDataSet = CType(response.Value, DataSet)
        factura_electronica = CType(oDataSet.Tables(0).Rows(0).Item("factura_electronica"), Boolean)
        SerieFactura = CType(oDataSet.Tables(0).Rows(0).Item("serie_factura_electronica"), String)
        folio_factura = CType(CType(oDataSet.Tables(0).Rows(0).Item("folio_factura_electronica"), Integer) + 1, String)

        '-----------------------------

        If Not factura_electronica Then
            SerieFactura = Comunes.clsUtilerias.uti_SeriePV(Comunes.Common.Caja_Actual)
            folio_factura = uti_FolioPV(Comunes.Common.PuntoVenta_Actual)
        End If

        response = oVentas.InsertarAnticipo(Comunes.Common.Sucursal_Actual, SerieFactura, folio_factura, Me.dteFecha.DateTime, 0, Me.Cliente, Me.txtDomicilio, Me.txtColonia, Me.txtCiudad, Me.txtMunicipio, Me.clcCp, Me.txtEstado, Me.txtTelefono1, Me.txtTelefono2, Me.txtFax, Me.txtCurp, Me.txtRfc, Me.cboTipoventa.EditValue, Me.chkIvadesglosado, "", "", Me.clcImporte.EditValue, 0, 0, Me.clcSubtotal, Me.clcImpuesto, Me.clcImporte.EditValue, 0, 1, _
                 Me.dteFecha.DateTime, 1, Me.clcImporte.EditValue, 0.0, False, "01/01/1900", 0.0, False, 0.0, False, False, 0, -1, -1, Comunes.Common.PuntoVenta_Actual, -1, False, -1, -1)

        If Not response.ErrorFound Then

            response = oVentasDetalle.InsertarAnticipo(Comunes.Common.Sucursal_Actual, SerieFactura, folio_factura, 1, Me.ArticuloAnticipos, Me.clcImporte.EditValue, Me.clcImporte.EditValue, False, 0, False, False, "01", -1, 0, -1, -1, -1, Me.clcImporte.EditValue, Me.txtConcepto.Text, True)
        Else
            response.Message = "Error, al Insertar el Encabezado de la Factura"
        End If

        Dim subtotal_cargo_abono As Double = 0
        Dim iva_cargo_abono As Double = 0


        If impuesto <> -1 Then
            subtotal_cargo_abono = Me.clcImporte.EditValue / (1 + (impuesto / 100))
            iva_cargo_abono = Me.clcImporte.EditValue - subtotal_cargo_abono
        Else
            response.Message = "El Porcentaje de Impuesto no esta definido"
            Exit Function
        End If
        If Not response.ErrorFound Then
            response = oMovimientosCobrar.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFacturaCredito, SerieFactura, folio_factura, Cliente, 0, Me.dteFecha.DateTime.Date, -1, -1, -1, 1, Me.clcImporte.EditValue, 0, subtotal_cargo_abono, iva_cargo_abono, Me.clcImporte.EditValue, Me.clcImporte.EditValue, CDate("01/01/1900").Date, "", "", False, -1)

            If Not response.ErrorFound Then
                response = oMovimientosCobrarDetalle.Insertar(Comunes.Common.Sucursal_Actual, ConceptoFacturaCredito, SerieFactura, folio_factura, Cliente, 1, 0, Me.dteFecha.DateTime.Date, Me.clcImporte.EditValue, -1, -1, -1, -1, -1, -1, 1, Me.dteFecha.DateTime.Date, Me.clcImporte.EditValue, "", "", 0)
            End If
        End If

        If Not response.ErrorFound Then
            Dim ConceptoVenta As String
            ConceptoVenta = CType(oVariables.TraeDatos("concepto_venta", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
            If ConceptoVenta = "" Then
                ShowMessage(MessageType.MsgInformation, "El Concepto de Venta no esta definido", "Variables del Sistema", Nothing, False)
            End If
            response = oVentas.TimbrarFactura(Comunes.Common.Sucursal_Actual, SerieFactura, folio_factura, ConceptoVenta, Me.chkIvadesglosado, Comunes.Common.Sucursal_Actual)


            If Not response.ErrorFound Then
                response = Me.oAnticipos.ActualizaFacturaGenerada(Me.clcFolio.EditValue, Comunes.Common.Sucursal_Actual, SerieFactura, folio_factura)
            End If
        End If

        Return response
    End Function
    Private Sub ConfigurarMaster()
        With Me.tmaFormasPago
            .UpdateForm = New frmAnticiposFormaPago
            ' .AddColumn("partida", "System.Int32")
            .AddColumn("forma_pago", "System.Int32")
            .AddColumn("descripcion")
            .AddColumn("tipo_cambio", "System.Double")
            .AddColumn("dolares", "System.Int32")
            .AddColumn("importe", "System.Double")
            .AddColumn("solicitar_ulitmos_digitos", "System.Boolean")
            .AddColumn("maneja_dolares", "System.Boolean")
            .AddColumn("ultimos_digitos_cuenta", "System.String")

        End With
    End Sub
    Private Function ObtenerTotalFormaPago() As Double
        Dim dTotalFormaPago As Double = 0
        With tmaFormasPago
            .MoveFirst()
            Do While Not .EOF
                Select Case Action
                    Case Actions.Insert, Actions.Update, Actions.None
                        dTotalFormaPago = dTotalFormaPago + CType(.Item("Importe"), Double)
                End Select


                .MoveNext()
            Loop
        End With

        Return dTotalFormaPago
    End Function

    Private Sub LlenarDatosCliente()

        Dim response As New Events
        Dim oData As DataSet
        response = oClientes.DespliegaDatos(Cliente)
        If Not response.ErrorFound Then

            oData = response.Value
            With oData.Tables(0).Rows(0)
                Me.clcCp = .Item("cp")
                Me.txtCiudad = .Item("nombre_ciudad")
                Me.txtColonia = .Item("nombre_colonia")
                Me.txtMunicipio = .Item("nombre_municipio")
                Me.txtCurp = .Item("curp")
                Me.txtNumExterior = .Item("numero_exterior")
                Me.txtNumInterior = .Item("numero_interior")
                Me.txtDomicilio = .Item("domicilio_numeros")

                ' .Item("domicilio") + "  " + Me.txtNumExterior.Text  '.Item("domicilio_numeros") '.Item("domicilio") + " Ext." + Me.txtNumExterior.Text + " Int." + Me.txtNumInterior.Text
                Me.txtEstado = .Item("nombre_estado")
                Me.txtFax = .Item("fax")

                Me.txtTelefono1 = .Item("telefono1")
                Me.txtTelefono2 = .Item("telefono2")
                Me.dLimiteCredito = .Item("limite_credito")
                Me.chkRfcCompleto = .Item("rfc_completo")


                Me.chkIvadesglosado = True
                If Me.chkRfcCompleto Then
                    Me.txtRfc = .Item("rfc")
                Else
                    Me.txtRfc = "XAXX010101000" ' Se pone un RFC Generico                  
                End If




            End With



            'response = oClientesCobradores.DespliegaDatosCobradoresVentas(Me.Cliente, Comunes.Common.Sucursal_Actual)
            'If Not response.ErrorFound Then

            '    oData = response.Value
            '    If oData.Tables(0).Rows.Count > 0 Then Me.Cobrador = oData.Tables(0).Rows(0).Item("cobrador")

            'End If

        Else

            LimpiarDatosCliente()

        End If

    End Sub
    Private Sub LimpiarDatosCliente()

        Me.clcCp = 0
        Me.txtDomicilio = ""
        Me.txtColonia = ""
        Me.txtCiudad = ""
        Me.txtMunicipio = ""
        Me.txtCurp = ""
        Me.txtEstado = ""
        Me.txtFax = ""
        Me.txtRfc = ""
        Me.txtTelefono1 = ""
        Me.txtTelefono2 = ""
        Me.txtNumInterior = ""
        Me.txtNumExterior = ""
        Me.dLimiteCredito = 0

    End Sub

    Private Sub TraeConceptoFacturaContado()

        ConceptoFacturaContado = CType(oVariables.TraeDatos("concepto_cxc_factura_contado", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoFacturaContado = "" Then
            ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Contado no esta definido", "Variables del Sistema", Nothing, False)

        End If

    End Sub
    Private Sub TraeConceptoFacturaCredito()

        ConceptoFacturaCredito = CType(oVariables.TraeDatos("concepto_cxc_factura_credito", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoFacturaCredito = "" Then

            ShowMessage(MessageType.MsgInformation, "El Concepto de Factura de Cr�dito no esta definido", "Variables del Sistema", Nothing, False)

        End If

    End Sub


    Private Sub TraeConceptoAbono()
        ConceptoAbono = CType(oVariables.TraeDatos("concepto_cxc_abono", VillarrealBusiness.clsVariables.tipo_dato.Varchar), String)
        If ConceptoAbono <> "" Then
            banConceptoAbono = True
        Else
            ShowMessage(MessageType.MsgInformation, "El Concepto de CXC de Abono no esta definido", "Variables del Sistema", Nothing, False)
        End If
    End Sub


#End Region





End Class

