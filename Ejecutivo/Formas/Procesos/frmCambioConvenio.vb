Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmCambioConvenio
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpFolio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSerie As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpConvenioCambiado As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpConvenio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblConvenioCambiado As System.Windows.Forms.Label
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.ToolBarButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambioConvenio))
        Me.lblImporte = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.lblCliente = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lkpFolio = New Dipros.Editors.TINMultiLookup
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.lkpSerie = New Dipros.Editors.TINMultiLookup
        Me.lblConvenioCambiado = New System.Windows.Forms.Label
        Me.lkpConvenioCambiado = New Dipros.Editors.TINMultiLookup
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.lkpConvenio = New Dipros.Editors.TINMultiLookup
        Me.btnAceptar = New System.Windows.Forms.ToolBarButton
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnAceptar})
        Me.tbrTools.Name = "tbrTools"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(15, 136)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 67
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(27, 112)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 65
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(21, 160)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 69
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(32, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 17)
        Me.Label2.TabIndex = 63
        Me.Label2.Tag = ""
        Me.Label2.Text = "F&olio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(12, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 59
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(29, 64)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 61
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(72, 135)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte.TabIndex = 68
        Me.clcImporte.Tag = ""
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(72, 109)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy "
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy  hh:mm:ss"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(96, 23)
        Me.dteFecha.TabIndex = 66
        Me.dteFecha.Tag = ""
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(72, 157)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = True
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(344, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 70
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lkpFolio
        '
        Me.lkpFolio.AllowAdd = False
        Me.lkpFolio.AutoReaload = False
        Me.lkpFolio.DataSource = Nothing
        Me.lkpFolio.DefaultSearchField = ""
        Me.lkpFolio.DisplayMember = "folio"
        Me.lkpFolio.EditValue = Nothing
        Me.lkpFolio.Filtered = False
        Me.lkpFolio.InitValue = Nothing
        Me.lkpFolio.Location = New System.Drawing.Point(72, 86)
        Me.lkpFolio.MultiSelect = False
        Me.lkpFolio.Name = "lkpFolio"
        Me.lkpFolio.NullText = ""
        Me.lkpFolio.PopupWidth = CType(350, Long)
        Me.lkpFolio.ReadOnlyControl = False
        Me.lkpFolio.Required = False
        Me.lkpFolio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolio.SearchMember = ""
        Me.lkpFolio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolio.SelectAll = False
        Me.lkpFolio.Size = New System.Drawing.Size(136, 20)
        Me.lkpFolio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolio.TabIndex = 64
        Me.lkpFolio.Tag = ""
        Me.lkpFolio.ToolTip = Nothing
        Me.lkpFolio.ValueMember = "folio"
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(72, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(280, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 60
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'lkpSerie
        '
        Me.lkpSerie.AllowAdd = False
        Me.lkpSerie.AutoReaload = False
        Me.lkpSerie.DataSource = Nothing
        Me.lkpSerie.DefaultSearchField = ""
        Me.lkpSerie.DisplayMember = "serie"
        Me.lkpSerie.EditValue = Nothing
        Me.lkpSerie.Filtered = False
        Me.lkpSerie.InitValue = Nothing
        Me.lkpSerie.Location = New System.Drawing.Point(72, 63)
        Me.lkpSerie.MultiSelect = False
        Me.lkpSerie.Name = "lkpSerie"
        Me.lkpSerie.NullText = ""
        Me.lkpSerie.PopupWidth = CType(350, Long)
        Me.lkpSerie.ReadOnlyControl = False
        Me.lkpSerie.Required = False
        Me.lkpSerie.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSerie.SearchMember = ""
        Me.lkpSerie.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSerie.SelectAll = False
        Me.lkpSerie.Size = New System.Drawing.Size(136, 20)
        Me.lkpSerie.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSerie.TabIndex = 62
        Me.lkpSerie.Tag = ""
        Me.lkpSerie.ToolTip = Nothing
        Me.lkpSerie.ValueMember = "serie"
        '
        'lblConvenioCambiado
        '
        Me.lblConvenioCambiado.AutoSize = True
        Me.lblConvenioCambiado.Location = New System.Drawing.Point(6, 208)
        Me.lblConvenioCambiado.Name = "lblConvenioCambiado"
        Me.lblConvenioCambiado.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenioCambiado.TabIndex = 73
        Me.lblConvenioCambiado.Tag = ""
        Me.lblConvenioCambiado.Text = "Convenio:"
        Me.lblConvenioCambiado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenioCambiado
        '
        Me.lkpConvenioCambiado.AllowAdd = False
        Me.lkpConvenioCambiado.AutoReaload = False
        Me.lkpConvenioCambiado.DataSource = Nothing
        Me.lkpConvenioCambiado.DefaultSearchField = ""
        Me.lkpConvenioCambiado.DisplayMember = "nombre_convenio"
        Me.lkpConvenioCambiado.EditValue = Nothing
        Me.lkpConvenioCambiado.Filtered = False
        Me.lkpConvenioCambiado.InitValue = Nothing
        Me.lkpConvenioCambiado.Location = New System.Drawing.Point(72, 207)
        Me.lkpConvenioCambiado.MultiSelect = False
        Me.lkpConvenioCambiado.Name = "lkpConvenioCambiado"
        Me.lkpConvenioCambiado.NullText = ""
        Me.lkpConvenioCambiado.PopupWidth = CType(420, Long)
        Me.lkpConvenioCambiado.ReadOnlyControl = False
        Me.lkpConvenioCambiado.Required = True
        Me.lkpConvenioCambiado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenioCambiado.SearchMember = ""
        Me.lkpConvenioCambiado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenioCambiado.SelectAll = False
        Me.lkpConvenioCambiado.Size = New System.Drawing.Size(345, 20)
        Me.lkpConvenioCambiado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenioCambiado.TabIndex = 74
        Me.lkpConvenioCambiado.Tag = ""
        Me.lkpConvenioCambiado.ToolTip = Nothing
        Me.lkpConvenioCambiado.ValueMember = "convenio"
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(6, 184)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(60, 16)
        Me.lblConvenio.TabIndex = 71
        Me.lblConvenio.Tag = ""
        Me.lblConvenio.Text = "Convenio:"
        Me.lblConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConvenio
        '
        Me.lkpConvenio.AllowAdd = False
        Me.lkpConvenio.AutoReaload = False
        Me.lkpConvenio.DataSource = Nothing
        Me.lkpConvenio.DefaultSearchField = ""
        Me.lkpConvenio.DisplayMember = "nombre"
        Me.lkpConvenio.EditValue = Nothing
        Me.lkpConvenio.Enabled = False
        Me.lkpConvenio.Filtered = False
        Me.lkpConvenio.InitValue = Nothing
        Me.lkpConvenio.Location = New System.Drawing.Point(72, 182)
        Me.lkpConvenio.MultiSelect = False
        Me.lkpConvenio.Name = "lkpConvenio"
        Me.lkpConvenio.NullText = ""
        Me.lkpConvenio.PopupWidth = CType(420, Long)
        Me.lkpConvenio.ReadOnlyControl = False
        Me.lkpConvenio.Required = True
        Me.lkpConvenio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConvenio.SearchMember = ""
        Me.lkpConvenio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConvenio.SelectAll = False
        Me.lkpConvenio.Size = New System.Drawing.Size(345, 20)
        Me.lkpConvenio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConvenio.TabIndex = 72
        Me.lkpConvenio.Tag = ""
        Me.lkpConvenio.ToolTip = Nothing
        Me.lkpConvenio.ValueMember = "convenio"
        '
        'btnAceptar
        '
        Me.btnAceptar.Text = "Aceptar"
        '
        'frmCambioConvenio
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(449, 244)
        Me.Controls.Add(Me.lblConvenioCambiado)
        Me.Controls.Add(Me.lkpConvenioCambiado)
        Me.Controls.Add(Me.lblConvenio)
        Me.Controls.Add(Me.lkpConvenio)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblOrden)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lkpFolio)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lkpSerie)
        Me.Name = "frmCambioConvenio"
        Me.Text = "Cambio de Convenio"
        Me.Controls.SetChildIndex(Me.lkpSerie, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpFolio, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblOrden, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenio, 0)
        Me.Controls.SetChildIndex(Me.lblConvenio, 0)
        Me.Controls.SetChildIndex(Me.lkpConvenioCambiado, 0)
        Me.Controls.SetChildIndex(Me.lblConvenioCambiado, 0)
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"

    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVendedores As VillarrealBusiness.clsVendedores
    Private oClientesConvenios As VillarrealBusiness.clsClientesConvenios
    Private oConvenios As VillarrealBusiness.clsConvenios


    Private ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property folio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolio)
        End Get
    End Property
    Private ReadOnly Property Convenio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpConvenioCambiado)
        End Get
    End Property
    Private ReadOnly Property serie() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie)
        End Get
    End Property
    Private ReadOnly Property Cliente() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property

#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCambiarVendedor_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oClientes = New VillarrealBusiness.clsClientes
        oVendedores = New VillarrealBusiness.clsVendedores
        oClientesConvenios = New VillarrealBusiness.clsClientesConvenios
        oConvenios = New VillarrealBusiness.clsConvenios
        Me.tbrTools.Buttons(0).Visible = False
    End Sub

    Private Sub frmCambiarVendedor_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept

    End Sub

    Private Sub frmCambiarVendedor_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oVentas.ValidacionCambiarVendedor(Action, sucursal, serie, folio, vendedor)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub frmCambiarVendedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub
    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False, True)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing

    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        lkpSerie_LoadData(True)
    End Sub

    Private Sub lkpSerie_LoadData(ByVal Initialize As Boolean) Handles lkpSerie.LoadData
        Dim response As Events
        response = oVentas.LookupSerieVentas(sucursal, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSerie.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpSerie_Format() Handles lkpSerie.Format
        Comunes.clsFormato.for_series_ventas_grl(Me.lkpSerie)
    End Sub
    Private Sub lkpSerie_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSerie.EditValueChanged
        lkpFolio_LoadData(True)


    End Sub

    Private Sub lkpFolio_LoadData(ByVal Initialize As Boolean) Handles lkpFolio.LoadData
        Dim response As Events
        response = oVentas.LookupFolioVentas(sucursal, serie, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolio.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpfolio_Format() Handles lkpFolio.Format
        Comunes.clsFormato.for_folio_ventas_grl(Me.lkpFolio)
    End Sub
    Private Sub lkpFolio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolio.EditValueChanged
        traerDatos()
    End Sub


    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub



    Private Sub lkpConvenioCambiado_Format() Handles lkpConvenioCambiado.Format
        Comunes.clsFormato.for_convenios_ventas_grl(Me.lkpConvenioCambiado)
    End Sub
    Private Sub lkpConvenioCambiado_LoadData(ByVal Initialize As Boolean) Handles lkpConvenioCambiado.LoadData
        Dim response As Events

        response = Me.oClientesConvenios.Listado(Cliente)

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenioCambiado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub

    Private Sub lkpConvenio_Format() Handles lkpConvenio.Format
        Comunes.clsFormato.for_convenios_ventas_grl(Me.lkpConvenio)
    End Sub
    Private Sub lkpConvenio_LoadData(ByVal Initialize As Boolean) Handles lkpConvenio.LoadData
        Dim response As Events

        response = Me.oConvenios.Lookup()

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = CType(response.Value, DataSet)
            Me.lkpConvenio.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

        If e.Button.Text = "Aceptar" Then
            Dim response As New Events

            response = oVentas.ValidacionCambiarConvenio(Action, sucursal, serie, folio, Convenio)
            If response.ErrorFound Then
                response.ShowError()
                Exit Sub
            End If

            TINApp.Connection.Begin()

            response = oVentas.ActualizaConvenioVentas(sucursal, serie, folio, Convenio)
            If response.ErrorFound Then
                TINApp.Connection.Rollback()
                response.ShowError()
                Exit Sub
            End If

            Me.Close()
            'traerDatos()

            TINApp.Connection.Commit()
            response = Nothing
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub traerDatos()
        Dim response As Events
        response = oVentas.DespliegaDatos(sucursal, serie, folio)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Me.dteFecha.EditValue = oDataSet.Tables(0).Rows(0).Item("fecha")
                Me.clcImporte.EditValue = oDataSet.Tables(0).Rows(0).Item("total")
                Me.lkpCliente.EditValue = oDataSet.Tables(0).Rows(0).Item("cliente")
                Me.lkpConvenio.EditValue = oDataSet.Tables(0).Rows(0).Item("convenio")
            Else
                Me.dteFecha.EditValue = CDate("01/01/1900")
                Me.clcImporte.EditValue = 0
                Me.lkpCliente.EditValue = Nothing
                Me.lkpConvenio.EditValue = Nothing
                Me.lkpConvenioCambiado.EditValue = Nothing


            End If
        Else

            response.ShowError()
        End If
    End Sub
#End Region


   
    
    
End Class
