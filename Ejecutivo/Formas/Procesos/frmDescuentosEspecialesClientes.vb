Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmDescuentosEspecialesClientes
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents clcFolioDescuento As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clcPorcentaje As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents lblPlan As System.Windows.Forms.Label
    Friend WithEvents lkpPlan As Dipros.Editors.TINMultiLookup
    Friend WithEvents clcMonto As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkutilizada As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grDescuentos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDescuentos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcArticulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSobrePedido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grchkReparto As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grchkSurtido As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcModelo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tmaDescuentos As Dipros.Windows.TINMaster
    Public WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents grcPrecioLista As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcPrecioListaUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Public WithEvents lblSerieSucursal As System.Windows.Forms.Label
    Public WithEvents lblFolio As System.Windows.Forms.Label
    Friend WithEvents lkpUsuarioCapturo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpUsuario As Dipros.Editors.TINMultiLookup
    Friend WithEvents dteFechaCaptura As DevExpress.XtraEditors.DateEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDescuentosEspecialesClientes))
        Me.clcFolioDescuento = New DevExpress.XtraEditors.CalcEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.clcPorcentaje = New DevExpress.XtraEditors.CalcEdit
        Me.lblPlan = New System.Windows.Forms.Label
        Me.lkpPlan = New Dipros.Editors.TINMultiLookup
        Me.clcMonto = New DevExpress.XtraEditors.CalcEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkutilizada = New DevExpress.XtraEditors.CheckEdit
        Me.grDescuentos = New DevExpress.XtraGrid.GridControl
        Me.grvDescuentos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcPartida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcArticulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcModelo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioLista = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioDescuento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcPrecioListaUnitario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grchkSobrePedido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grchkReparto = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.grchkSurtido = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.tmaDescuentos = New Dipros.Windows.TINMaster
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lkpUsuarioCapturo = New Dipros.Editors.TINMultiLookup
        Me.dteFechaCaptura = New DevExpress.XtraEditors.DateEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblFolio = New System.Windows.Forms.Label
        Me.lblSerieSucursal = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lkpUsuario = New Dipros.Editors.TINMultiLookup
        CType(Me.clcFolioDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcMonto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.chkutilizada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grDescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dteFechaCaptura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(2940, 28)
        '
        'clcFolioDescuento
        '
        Me.clcFolioDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcFolioDescuento.Location = New System.Drawing.Point(65, 40)
        Me.clcFolioDescuento.Name = "clcFolioDescuento"
        '
        'clcFolioDescuento.Properties
        '
        Me.clcFolioDescuento.Properties.Enabled = False
        Me.clcFolioDescuento.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcFolioDescuento.Size = New System.Drawing.Size(72, 20)
        Me.clcFolioDescuento.TabIndex = 1
        Me.clcFolioDescuento.Tag = "folio_descuento"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(25, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Tag = ""
        Me.Label3.Text = "Folio:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "Descuento:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcPorcentaje
        '
        Me.clcPorcentaje.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPorcentaje.Location = New System.Drawing.Point(104, 62)
        Me.clcPorcentaje.Name = "clcPorcentaje"
        '
        'clcPorcentaje.Properties
        '
        Me.clcPorcentaje.Properties.DisplayFormat.FormatString = "n2"
        Me.clcPorcentaje.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.Properties.EditFormat.FormatString = "n2"
        Me.clcPorcentaje.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcPorcentaje.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPorcentaje.Size = New System.Drawing.Size(75, 20)
        Me.clcPorcentaje.TabIndex = 5
        Me.clcPorcentaje.Tag = "porcentaje_descuento"
        '
        'lblPlan
        '
        Me.lblPlan.AutoSize = True
        Me.lblPlan.Location = New System.Drawing.Point(8, 41)
        Me.lblPlan.Name = "lblPlan"
        Me.lblPlan.Size = New System.Drawing.Size(91, 16)
        Me.lblPlan.TabIndex = 2
        Me.lblPlan.Tag = ""
        Me.lblPlan.Text = "&Plan de cr�dito:"
        Me.lblPlan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpPlan
        '
        Me.lkpPlan.AllowAdd = False
        Me.lkpPlan.AutoReaload = False
        Me.lkpPlan.DataSource = Nothing
        Me.lkpPlan.DefaultSearchField = ""
        Me.lkpPlan.DisplayMember = "descripcion"
        Me.lkpPlan.EditValue = Nothing
        Me.lkpPlan.Filtered = False
        Me.lkpPlan.InitValue = Nothing
        Me.lkpPlan.Location = New System.Drawing.Point(104, 37)
        Me.lkpPlan.MultiSelect = False
        Me.lkpPlan.Name = "lkpPlan"
        Me.lkpPlan.NullText = ""
        Me.lkpPlan.PopupWidth = CType(450, Long)
        Me.lkpPlan.ReadOnlyControl = False
        Me.lkpPlan.Required = False
        Me.lkpPlan.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpPlan.SearchMember = ""
        Me.lkpPlan.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpPlan.SelectAll = False
        Me.lkpPlan.Size = New System.Drawing.Size(240, 20)
        Me.lkpPlan.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpPlan.TabIndex = 3
        Me.lkpPlan.Tag = "plan_credito"
        Me.lkpPlan.ToolTip = Nothing
        Me.lkpPlan.ValueMember = "plan_credito"
        '
        'clcMonto
        '
        Me.clcMonto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcMonto.Location = New System.Drawing.Point(104, 12)
        Me.clcMonto.Name = "clcMonto"
        '
        'clcMonto.Properties
        '
        Me.clcMonto.Properties.DisplayFormat.FormatString = "c2"
        Me.clcMonto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto.Properties.EditFormat.FormatString = "n2"
        Me.clcMonto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcMonto.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcMonto.Size = New System.Drawing.Size(75, 20)
        Me.clcMonto.TabIndex = 1
        Me.clcMonto.Tag = "monto"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(56, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Tag = ""
        Me.Label4.Text = "Monto:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'RadioGroup1
        '
        Me.RadioGroup1.EditValue = 1
        Me.RadioGroup1.Location = New System.Drawing.Point(31, 131)
        Me.RadioGroup1.Name = "RadioGroup1"
        '
        'RadioGroup1.Properties
        '
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Contado"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Credito")})
        Me.RadioGroup1.Size = New System.Drawing.Size(88, 90)
        Me.RadioGroup1.TabIndex = 8
        Me.RadioGroup1.Tag = "opcion"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(31, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Tag = ""
        Me.Label5.Text = "Tipo Descuento:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.clcMonto)
        Me.GroupBox1.Controls.Add(Me.lkpPlan)
        Me.GroupBox1.Controls.Add(Me.clcPorcentaje)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblPlan)
        Me.GroupBox1.Location = New System.Drawing.Point(127, 124)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(353, 96)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        '
        'chkutilizada
        '
        Me.chkutilizada.Location = New System.Drawing.Point(344, 41)
        Me.chkutilizada.Name = "chkutilizada"
        '
        'chkutilizada.Properties
        '
        Me.chkutilizada.Properties.Caption = "Utilizada"
        Me.chkutilizada.Properties.Enabled = False
        Me.chkutilizada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.chkutilizada.Size = New System.Drawing.Size(72, 19)
        Me.chkutilizada.TabIndex = 2
        Me.chkutilizada.Tag = "utilizada"
        '
        'grDescuentos
        '
        '
        'grDescuentos.EmbeddedNavigator
        '
        Me.grDescuentos.EmbeddedNavigator.Name = ""
        Me.grDescuentos.Location = New System.Drawing.Point(8, 309)
        Me.grDescuentos.MainView = Me.grvDescuentos
        Me.grDescuentos.Name = "grDescuentos"
        Me.grDescuentos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.grchkSobrePedido, Me.grchkReparto, Me.grchkSurtido})
        Me.grDescuentos.Size = New System.Drawing.Size(632, 160)
        Me.grDescuentos.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.LightSkyBlue, System.Drawing.SystemColors.WindowText, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.grDescuentos.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText, System.Drawing.SystemColors.Highlight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.SystemColors.WindowText, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.grDescuentos.TabIndex = 13
        Me.grDescuentos.TabStop = False
        Me.grDescuentos.Text = "Ventas"
        '
        'grvDescuentos
        '
        Me.grvDescuentos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcPartida, Me.grcNArticulo, Me.grcArticulo, Me.grcCantidad, Me.grcModelo, Me.grcPrecioLista, Me.grcPrecioDescuento, Me.grcPrecioListaUnitario})
        Me.grvDescuentos.GridControl = Me.grDescuentos
        Me.grvDescuentos.Name = "grvDescuentos"
        Me.grvDescuentos.OptionsCustomization.AllowFilter = False
        Me.grvDescuentos.OptionsCustomization.AllowGroup = False
        Me.grvDescuentos.OptionsCustomization.AllowSort = False
        Me.grvDescuentos.OptionsView.ShowFooter = True
        Me.grvDescuentos.OptionsView.ShowGroupPanel = False
        '
        'grcPartida
        '
        Me.grcPartida.Caption = "Partida"
        Me.grcPartida.FieldName = "Partida"
        Me.grcPartida.Name = "grcPartida"
        Me.grcPartida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPartida.Width = 52
        '
        'grcNArticulo
        '
        Me.grcNArticulo.Caption = "Art�culo"
        Me.grcNArticulo.FieldName = "n_articulo"
        Me.grcNArticulo.Name = "grcNArticulo"
        Me.grcNArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNArticulo.VisibleIndex = 1
        Me.grcNArticulo.Width = 229
        '
        'grcArticulo
        '
        Me.grcArticulo.Caption = "Art�culo"
        Me.grcArticulo.FieldName = "articulo"
        Me.grcArticulo.Name = "grcArticulo"
        Me.grcArticulo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcCantidad
        '
        Me.grcCantidad.Caption = "Cantidad"
        Me.grcCantidad.FieldName = "cantidad"
        Me.grcCantidad.Name = "grcCantidad"
        Me.grcCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcCantidad.VisibleIndex = 2
        Me.grcCantidad.Width = 57
        '
        'grcModelo
        '
        Me.grcModelo.Caption = "Modelo"
        Me.grcModelo.FieldName = "modelo"
        Me.grcModelo.Name = "grcModelo"
        Me.grcModelo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcModelo.VisibleIndex = 0
        Me.grcModelo.Width = 107
        '
        'grcPrecioLista
        '
        Me.grcPrecioLista.Caption = "Precio Lista x Cantidad"
        Me.grcPrecioLista.DisplayFormat.FormatString = "c2"
        Me.grcPrecioLista.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioLista.FieldName = "precio_lista_cantidad"
        Me.grcPrecioLista.Name = "grcPrecioLista"
        Me.grcPrecioLista.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioLista.SummaryItem.DisplayFormat = "{0:C2}"
        Me.grcPrecioLista.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcPrecioLista.VisibleIndex = 4
        Me.grcPrecioLista.Width = 110
        '
        'grcPrecioDescuento
        '
        Me.grcPrecioDescuento.Caption = "Precio C/Descuento"
        Me.grcPrecioDescuento.DisplayFormat.FormatString = "c2"
        Me.grcPrecioDescuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioDescuento.FieldName = "precio_descuento"
        Me.grcPrecioDescuento.Name = "grcPrecioDescuento"
        Me.grcPrecioDescuento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioDescuento.SummaryItem.DisplayFormat = "{0:C2}"
        Me.grcPrecioDescuento.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.grcPrecioDescuento.VisibleIndex = 5
        Me.grcPrecioDescuento.Width = 115
        '
        'grcPrecioListaUnitario
        '
        Me.grcPrecioListaUnitario.Caption = "Precio Lista"
        Me.grcPrecioListaUnitario.DisplayFormat.FormatString = "c2"
        Me.grcPrecioListaUnitario.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcPrecioListaUnitario.FieldName = "precio_lista"
        Me.grcPrecioListaUnitario.Name = "grcPrecioListaUnitario"
        Me.grcPrecioListaUnitario.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcPrecioListaUnitario.VisibleIndex = 3
        '
        'grchkSobrePedido
        '
        Me.grchkSobrePedido.AutoHeight = False
        Me.grchkSobrePedido.Name = "grchkSobrePedido"
        Me.grchkSobrePedido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grchkReparto
        '
        Me.grchkReparto.AutoHeight = False
        Me.grchkReparto.Name = "grchkReparto"
        Me.grchkReparto.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'grchkSurtido
        '
        Me.grchkSurtido.AutoHeight = False
        Me.grchkSurtido.Name = "grchkSurtido"
        Me.grchkSurtido.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'tmaDescuentos
        '
        Me.tmaDescuentos.BackColor = System.Drawing.Color.White
        Me.tmaDescuentos.CanDelete = True
        Me.tmaDescuentos.CanInsert = True
        Me.tmaDescuentos.CanUpdate = True
        Me.tmaDescuentos.Grid = Me.grDescuentos
        Me.tmaDescuentos.Location = New System.Drawing.Point(8, 285)
        Me.tmaDescuentos.Name = "tmaDescuentos"
        Me.tmaDescuentos.Size = New System.Drawing.Size(611, 23)
        Me.tmaDescuentos.TabIndex = 12
        Me.tmaDescuentos.TabStop = False
        Me.tmaDescuentos.Title = "Art�culos"
        Me.tmaDescuentos.UpdateTitle = "un Art�culo"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(17, 64)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 3
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(65, 64)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(520, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = False
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(352, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 4
        Me.lkpCliente.Tag = "Cliente"
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "Cliente"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lkpUsuarioCapturo)
        Me.GroupBox2.Controls.Add(Me.dteFechaCaptura)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Location = New System.Drawing.Point(32, 229)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(592, 48)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos de Captura"
        '
        'lkpUsuarioCapturo
        '
        Me.lkpUsuarioCapturo.AllowAdd = False
        Me.lkpUsuarioCapturo.AutoReaload = False
        Me.lkpUsuarioCapturo.DataSource = Nothing
        Me.lkpUsuarioCapturo.DefaultSearchField = ""
        Me.lkpUsuarioCapturo.DisplayMember = "nombre"
        Me.lkpUsuarioCapturo.EditValue = Nothing
        Me.lkpUsuarioCapturo.Enabled = False
        Me.lkpUsuarioCapturo.Filtered = False
        Me.lkpUsuarioCapturo.InitValue = Nothing
        Me.lkpUsuarioCapturo.Location = New System.Drawing.Point(319, 18)
        Me.lkpUsuarioCapturo.MultiSelect = False
        Me.lkpUsuarioCapturo.Name = "lkpUsuarioCapturo"
        Me.lkpUsuarioCapturo.NullText = ""
        Me.lkpUsuarioCapturo.PopupWidth = CType(400, Long)
        Me.lkpUsuarioCapturo.ReadOnlyControl = False
        Me.lkpUsuarioCapturo.Required = False
        Me.lkpUsuarioCapturo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpUsuarioCapturo.SearchMember = ""
        Me.lkpUsuarioCapturo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpUsuarioCapturo.SelectAll = False
        Me.lkpUsuarioCapturo.Size = New System.Drawing.Size(204, 20)
        Me.lkpUsuarioCapturo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpUsuarioCapturo.TabIndex = 3
        Me.lkpUsuarioCapturo.Tag = "usuario_captura"
        Me.lkpUsuarioCapturo.ToolTip = "usuario"
        Me.lkpUsuarioCapturo.ValueMember = "usuario"
        '
        'dteFechaCaptura
        '
        Me.dteFechaCaptura.EditValue = "10/04/2006"
        Me.dteFechaCaptura.Location = New System.Drawing.Point(120, 19)
        Me.dteFechaCaptura.Name = "dteFechaCaptura"
        '
        'dteFechaCaptura.Properties
        '
        Me.dteFechaCaptura.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFechaCaptura.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCaptura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFechaCaptura.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFechaCaptura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteFechaCaptura.Properties.Enabled = False
        Me.dteFechaCaptura.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.dteFechaCaptura.Size = New System.Drawing.Size(95, 20)
        Me.dteFechaCaptura.TabIndex = 1
        Me.dteFechaCaptura.Tag = "fecha_captura"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(263, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 16)
        Me.Label8.TabIndex = 2
        Me.Label8.Tag = ""
        Me.Label8.Text = "Usuario:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(69, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Tag = ""
        Me.Label7.Text = "Fecha:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.lblFolio)
        Me.GroupBox3.Controls.Add(Me.lblSerieSucursal)
        Me.GroupBox3.Location = New System.Drawing.Point(488, 124)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(136, 96)
        Me.GroupBox3.TabIndex = 10
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Factura"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 56)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 16)
        Me.Label10.TabIndex = 2
        Me.Label10.Tag = ""
        Me.Label10.Text = "Folio:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Tag = ""
        Me.Label9.Text = "Serie:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFolio
        '
        Me.lblFolio.AutoSize = True
        Me.lblFolio.Location = New System.Drawing.Point(56, 56)
        Me.lblFolio.Name = "lblFolio"
        Me.lblFolio.Size = New System.Drawing.Size(11, 16)
        Me.lblFolio.TabIndex = 3
        Me.lblFolio.Tag = "folio_factura"
        Me.lblFolio.Text = "0"
        Me.lblFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSerieSucursal
        '
        Me.lblSerieSucursal.AutoSize = True
        Me.lblSerieSucursal.Location = New System.Drawing.Point(56, 24)
        Me.lblSerieSucursal.Name = "lblSerieSucursal"
        Me.lblSerieSucursal.Size = New System.Drawing.Size(0, 16)
        Me.lblSerieSucursal.TabIndex = 1
        Me.lblSerieSucursal.Tag = "serie_factura"
        Me.lblSerieSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 16)
        Me.Label6.TabIndex = 5
        Me.Label6.Tag = ""
        Me.Label6.Text = "Autoriza:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpUsuario
        '
        Me.lkpUsuario.AllowAdd = False
        Me.lkpUsuario.AutoReaload = False
        Me.lkpUsuario.DataSource = Nothing
        Me.lkpUsuario.DefaultSearchField = ""
        Me.lkpUsuario.DisplayMember = "nombre"
        Me.lkpUsuario.EditValue = Nothing
        Me.lkpUsuario.Filtered = False
        Me.lkpUsuario.InitValue = Nothing
        Me.lkpUsuario.Location = New System.Drawing.Point(65, 88)
        Me.lkpUsuario.MultiSelect = False
        Me.lkpUsuario.Name = "lkpUsuario"
        Me.lkpUsuario.NullText = ""
        Me.lkpUsuario.PopupWidth = CType(400, Long)
        Me.lkpUsuario.ReadOnlyControl = False
        Me.lkpUsuario.Required = False
        Me.lkpUsuario.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpUsuario.SearchMember = ""
        Me.lkpUsuario.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpUsuario.SelectAll = False
        Me.lkpUsuario.Size = New System.Drawing.Size(351, 20)
        Me.lkpUsuario.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpUsuario.TabIndex = 6
        Me.lkpUsuario.Tag = "usuario_autoriza"
        Me.lkpUsuario.ToolTip = "usuario"
        Me.lkpUsuario.ValueMember = "usuario"
        '
        'frmDescuentosEspecialesClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(650, 472)
        Me.Controls.Add(Me.lkpUsuario)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.grDescuentos)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.tmaDescuentos)
        Me.Controls.Add(Me.chkutilizada)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.RadioGroup1)
        Me.Controls.Add(Me.clcFolioDescuento)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label6)
        Me.Name = "frmDescuentosEspecialesClientes"
        Me.Text = "frmDescuentosEspecialesClientes"
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.clcFolioDescuento, 0)
        Me.Controls.SetChildIndex(Me.RadioGroup1, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.chkutilizada, 0)
        Me.Controls.SetChildIndex(Me.tmaDescuentos, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.grDescuentos, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.lkpUsuario, 0)
        CType(Me.clcFolioDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcMonto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.chkutilizada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grDescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSobrePedido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkReparto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grchkSurtido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dteFechaCaptura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oPlanesCredito As New VillarrealBusiness.clsPlanesCredito
    Private osucursales As New VillarrealBusiness.clsSucursales
    Private oClientes As New VillarrealBusiness.clsClientes
    Private oDescuentosEspecialesClientes As New VillarrealBusiness.clsDescuentosEspecialesClientes
    Private oDescuentosEspecialesClientesDetalles As New VillarrealBusiness.clsDescuentosEspecialesClientesDetalle
    Private oUsuarios As New VillarrealBusiness.clsUsuarios

    Private ReadOnly Property PlanCredito() As Long
        Get
            Return PreparaValorLookup(Me.lkpPlan)
        End Get
    End Property
    Public ReadOnly Property Cliente() As Long
        Get
            Return PreparaValorLookup(Me.lkpCliente)
        End Get
    End Property
    Public ReadOnly Property Usuario_Autoriza() As String
        Get
            Return PreparaValorLookupStr(Me.lkpUsuario)
        End Get
    End Property
    Public ReadOnly Property Usuario_Captura() As String
        Get
            Return PreparaValorLookupStr(Me.lkpUsuarioCapturo)
        End Get
    End Property

    Private sucursal_dependencia As Boolean


#Region "Eventos de la Forma"

    Private Sub frmDescuentosEspecialesClientes_BeginUpdate() Handles MyBase.BeginUpdate
        TINApp.Connection.Begin()
    End Sub

    Private Sub frmDescuentosEspecialesClientes_EndUpdate() Handles MyBase.EndUpdate
        TINApp.Connection.Commit()
    End Sub

    Private Sub frmDescuentosEspecialesClientes_AbortUpdate() Handles MyBase.AbortUpdate
        TINApp.Connection.Rollback()
    End Sub

    Private Sub frmDescuentosEspecialesClientes_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        ConfiguraMaster()
        sucursal_dependencia = CType(CType(osucursales.DespliegaDatos(Comunes.Common.Sucursal_Actual).Value, DataSet).Tables(0).Rows(0).Item("sucursal_dependencia"), Boolean)
        RadioGroup1.SelectedIndex = 1
        RadioGroup1.SelectedIndex = 0
        Me.dteFechaCaptura.DateTime = CDate(TINApp.FechaServidor).Date
        Me.lkpUsuarioCapturo.EditValue = TINApp.Connection.User



    End Sub
    Private Sub frmDescuentosEspecialesClientes_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        Select Case Action
            Case Actions.Insert
                Response = Me.oDescuentosEspecialesClientes.Insertar(Me.DataSource)
            Case Actions.Update
                Response = Me.oDescuentosEspecialesClientes.Actualizar(Me.DataSource)
            Case Actions.Delete
                Response = Me.oDescuentosEspecialesClientes.Eliminar(Me.DataSource)
        End Select
    End Sub
    Private Sub frmDescuentosEspecialesClientes_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = Me.oDescuentosEspecialesClientes.Validacion(Me.Action, Cliente, Me.RadioGroup1.SelectedIndex, Me.clcMonto.Value, Me.clcPorcentaje.Value, Me.PlanCredito, Me.grcPrecioDescuento.SummaryItem.SummaryValue)
    End Sub
    Private Sub frmDescuentosEspecialesClientes_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Response = oDescuentosEspecialesClientes.DespliegaDatos(OwnerForm.Value("folio_descuento"))
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.DataSource = oDataSet

            If Me.chkutilizada.Checked = True Then
                Me.tbrTools.Buttons(0).Enabled = False
            End If

            If Me.clcMonto.Value > 0 Then
                Me.RadioGroup1.SelectedIndex = 0
            Else
                Me.RadioGroup1.SelectedIndex = 1
            End If



            Response = oDescuentosEspecialesClientesDetalles.Listado(Me.clcFolioDescuento.Value)
            If Not Response.ErrorFound Then
                oDataSet = CType(Response.Value, DataSet)
                Me.tmaDescuentos.DataSource = oDataSet
            End If

            If Action = Actions.Delete Then
                Me.RadioGroup1.Enabled = False
                Me.GroupBox1.Enabled = False
            End If

            CalculaPrecioDescuento()

        End If
    End Sub
    Private Sub frmDescuentosEspecialesClientes_Detail(ByRef Response As Dipros.Utils.Events) Handles MyBase.Detail

        With Me.tmaDescuentos
            .MoveFirst()
            Do While Not .EOF
                Select Case .CurrentAction
                    Case Actions.Insert
                        Response = oDescuentosEspecialesClientesDetalles.Insertar(Me.clcFolioDescuento.Value, .SelectedRow)   ', clcOrden.Text, lkpBodega.EditValue, Me.chkSobrePedido.Checked)
                    Case Actions.Update
                        Response = oDescuentosEspecialesClientesDetalles.Actualizar(Me.clcFolioDescuento.Value, .SelectedRow)
                    Case Actions.Delete
                        Response = oDescuentosEspecialesClientesDetalles.Eliminar(Me.clcFolioDescuento.Value, .SelectedRow.Tables(0).Rows(0).Item("partida"))
                End Select
                .MoveNext()
            Loop
        End With
    End Sub
    Private Sub frmDescuentosEspecialesClientes_Localize() Handles MyBase.Localize
        Find("folio_descuento", Me.clcFolioDescuento.Value)
    End Sub

#End Region

#Region "Eventos de los Controles"
    Private Sub lkpUsuario_Format() Handles lkpUsuario.Format
        Comunes.clsFormato.for_usuarios_grl(Me.lkpUsuario)
    End Sub
    Private Sub lkpUsuario_LoadData(ByVal Initialize As Boolean) Handles lkpUsuario.LoadData
        Dim response As Events

        response = oUsuarios.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpUsuario.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing

    End Sub
    Private Sub lkpUsuarioCapturo_Format() Handles lkpUsuarioCapturo.Format
        Comunes.clsFormato.for_usuarios_grl(Me.lkpUsuarioCapturo)
    End Sub

    Private Sub lkpUsuarioCapturo_LoadData(ByVal Initialize As Boolean) Handles lkpUsuarioCapturo.LoadData
        Dim response As Events

        response = oUsuarios.Lookup()
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpUsuarioCapturo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        response = Nothing
    End Sub

    Private Sub lkpPlan_LoadData(ByVal Initialize As Boolean) Handles lkpPlan.LoadData

        Dim Response As New Events
        Response = oPlanesCredito.Lookup(sucursal_dependencia, , 1)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpPlan.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If

        Response = Nothing

    End Sub
    Private Sub lkpPlan_Format() Handles lkpPlan.Format
        Comunes.clsFormato.for_planes_credito_grl(Me.lkpPlan)
    End Sub
    Private Sub RadioGroup1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
        Select Case Me.RadioGroup1.SelectedIndex
            Case 0
                Me.lkpPlan.Enabled = False
                Me.lkpPlan.EditValue = Nothing
                Me.clcPorcentaje.Value = 0
                Me.clcPorcentaje.Enabled = False
                Me.clcMonto.Enabled = True
            Case 1

                Me.lkpPlan.Enabled = True
                Me.clcPorcentaje.Enabled = True

                Me.clcMonto.Value = 0
                Me.clcMonto.Enabled = False
        End Select

        CalculaPrecioDescuento()
    End Sub
    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub
    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData

        Dim Response As New Events
        Response = oClientes.LookupCliente() '(sucursal_dependencia)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing

        End If
        Response = Nothing

    End Sub
    Private Sub clcPorcentaje_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcPorcentaje.EditValueChanged
        If Me.clcPorcentaje.IsLoading Then Exit Sub
        CalculaPrecioDescuento()
    End Sub
    Private Sub clcMonto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcMonto.EditValueChanged
        If Me.clcMonto.IsLoading Then Exit Sub
        CalculaPrecioDescuento()
    End Sub


#End Region

#Region "Funcionalidad"
    Private Sub ConfiguraMaster()
        With Me.tmaDescuentos
            .UpdateTitle = "un Art�culo"
            .UpdateForm = New frmDescuentosEspecialesClientesDetalle
            .AddColumn("departamento")
            .AddColumn("grupo")
            .AddColumn("articulo", "System.Int32")
            .AddColumn("n_articulo")
            .AddColumn("cantidad", "System.Int32")
            .AddColumn("modelo")
            .AddColumn("precio_lista", "System.Double")
            .AddColumn("precio_descuento", "System.Double")
            .AddColumn("precio_lista_cantidad", "System.Double")

        End With
    End Sub

    Public Sub CalculaPrecioDescuento()
        Dim precio_cantidad As Double = 0
        With Me.tmaDescuentos
            .MoveFirst()
            Do While Not .EOF

                precio_cantidad = .Item("precio_lista_cantidad")
                '.Item("precio_lista_cantidad") = precio_cantidad
                'Me.grvDescuentos.UpdateSummary()

                If Me.grcPrecioLista.SummaryItem.SummaryValue = 0 Then Exit Do

                Select Case Me.RadioGroup1.SelectedIndex
                    Case 0
                        Dim porcentaje_descuento As Double = (Me.grcPrecioLista.SummaryItem.SummaryValue - Me.clcMonto.Value) / Me.grcPrecioLista.SummaryItem.SummaryValue
                        .Item("precio_descuento") = System.Math.Round(precio_cantidad * porcentaje_descuento)     '.Item("precio_lista") * porcentaje_descuento
                    Case 1
                        .Item("precio_descuento") = System.Math.Round(precio_cantidad - (precio_cantidad * (Me.clcPorcentaje.Value / 100)))
                        'System.Math.Round(.Item("precio_lista") - (.Item("precio_lista") * (Me.clcPorcentaje.Value / 100)))
                End Select
                .MoveNext()
            Loop
        End With
    End Sub

#End Region





End Class
