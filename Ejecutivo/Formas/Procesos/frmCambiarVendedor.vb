Imports Dipros.Utils.Common
Imports Dipros.Utils
Public Class frmCambiarVendedor
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lkpSucursal As Dipros.Editors.TINMultiLookup
    Friend WithEvents lkpSerie As Dipros.Editors.TINMultiLookup
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lkpFolio As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lkpCliente As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lkpVendedor As Dipros.Editors.TINMultiLookup
    Friend WithEvents tlbrAceptar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lkpvendedorCambiado As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCambiarVendedor))
        Me.lblOrden = New System.Windows.Forms.Label
        Me.lkpSerie = New Dipros.Editors.TINMultiLookup
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.lkpSucursal = New Dipros.Editors.TINMultiLookup
        Me.Label2 = New System.Windows.Forms.Label
        Me.lkpFolio = New Dipros.Editors.TINMultiLookup
        Me.lblCliente = New System.Windows.Forms.Label
        Me.lkpCliente = New Dipros.Editors.TINMultiLookup
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblVendedor = New System.Windows.Forms.Label
        Me.lkpVendedor = New Dipros.Editors.TINMultiLookup
        Me.tlbrAceptar = New System.Windows.Forms.ToolBarButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.lkpvendedorCambiado = New Dipros.Editors.TINMultiLookup
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tlbrAceptar})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(504, 28)
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrden.Location = New System.Drawing.Point(34, 65)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 17)
        Me.lblOrden.TabIndex = 2
        Me.lblOrden.Tag = ""
        Me.lblOrden.Text = "&Serie:"
        Me.lblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSerie
        '
        Me.lkpSerie.AllowAdd = False
        Me.lkpSerie.AutoReaload = False
        Me.lkpSerie.DataSource = Nothing
        Me.lkpSerie.DefaultSearchField = ""
        Me.lkpSerie.DisplayMember = "serie"
        Me.lkpSerie.EditValue = Nothing
        Me.lkpSerie.Filtered = False
        Me.lkpSerie.InitValue = Nothing
        Me.lkpSerie.Location = New System.Drawing.Point(80, 64)
        Me.lkpSerie.MultiSelect = False
        Me.lkpSerie.Name = "lkpSerie"
        Me.lkpSerie.NullText = ""
        Me.lkpSerie.PopupWidth = CType(350, Long)
        Me.lkpSerie.ReadOnlyControl = False
        Me.lkpSerie.Required = False
        Me.lkpSerie.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSerie.SearchMember = ""
        Me.lkpSerie.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSerie.SelectAll = False
        Me.lkpSerie.Size = New System.Drawing.Size(136, 20)
        Me.lkpSerie.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSerie.TabIndex = 3
        Me.lkpSerie.Tag = ""
        Me.lkpSerie.ToolTip = Nothing
        Me.lkpSerie.ValueMember = "serie"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(17, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "S&ucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpSucursal
        '
        Me.lkpSucursal.AllowAdd = False
        Me.lkpSucursal.AutoReaload = False
        Me.lkpSucursal.DataSource = Nothing
        Me.lkpSucursal.DefaultSearchField = ""
        Me.lkpSucursal.DisplayMember = "nombre"
        Me.lkpSucursal.EditValue = Nothing
        Me.lkpSucursal.Filtered = False
        Me.lkpSucursal.InitValue = Nothing
        Me.lkpSucursal.Location = New System.Drawing.Point(80, 40)
        Me.lkpSucursal.MultiSelect = False
        Me.lkpSucursal.Name = "lkpSucursal"
        Me.lkpSucursal.NullText = ""
        Me.lkpSucursal.PopupWidth = CType(400, Long)
        Me.lkpSucursal.ReadOnlyControl = False
        Me.lkpSucursal.Required = False
        Me.lkpSucursal.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpSucursal.SearchMember = ""
        Me.lkpSucursal.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpSucursal.SelectAll = False
        Me.lkpSucursal.Size = New System.Drawing.Size(280, 20)
        Me.lkpSucursal.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpSucursal.TabIndex = 1
        Me.lkpSucursal.Tag = "Sucursal"
        Me.lkpSucursal.ToolTip = Nothing
        Me.lkpSucursal.ValueMember = "Sucursal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(37, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Tag = ""
        Me.Label2.Text = "F&olio:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpFolio
        '
        Me.lkpFolio.AllowAdd = False
        Me.lkpFolio.AutoReaload = False
        Me.lkpFolio.DataSource = Nothing
        Me.lkpFolio.DefaultSearchField = ""
        Me.lkpFolio.DisplayMember = "folio"
        Me.lkpFolio.EditValue = Nothing
        Me.lkpFolio.Filtered = False
        Me.lkpFolio.InitValue = Nothing
        Me.lkpFolio.Location = New System.Drawing.Point(80, 88)
        Me.lkpFolio.MultiSelect = False
        Me.lkpFolio.Name = "lkpFolio"
        Me.lkpFolio.NullText = ""
        Me.lkpFolio.PopupWidth = CType(350, Long)
        Me.lkpFolio.ReadOnlyControl = False
        Me.lkpFolio.Required = False
        Me.lkpFolio.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpFolio.SearchMember = ""
        Me.lkpFolio.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpFolio.SelectAll = False
        Me.lkpFolio.Size = New System.Drawing.Size(136, 20)
        Me.lkpFolio.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpFolio.TabIndex = 5
        Me.lkpFolio.Tag = ""
        Me.lkpFolio.ToolTip = Nothing
        Me.lkpFolio.ValueMember = "folio"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(26, 163)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 10
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "C&liente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpCliente
        '
        Me.lkpCliente.AllowAdd = False
        Me.lkpCliente.AutoReaload = False
        Me.lkpCliente.DataSource = Nothing
        Me.lkpCliente.DefaultSearchField = ""
        Me.lkpCliente.DisplayMember = "nombre"
        Me.lkpCliente.EditValue = Nothing
        Me.lkpCliente.Enabled = False
        Me.lkpCliente.Filtered = False
        Me.lkpCliente.InitValue = Nothing
        Me.lkpCliente.Location = New System.Drawing.Point(80, 162)
        Me.lkpCliente.MultiSelect = False
        Me.lkpCliente.Name = "lkpCliente"
        Me.lkpCliente.NullText = ""
        Me.lkpCliente.PopupWidth = CType(400, Long)
        Me.lkpCliente.ReadOnlyControl = False
        Me.lkpCliente.Required = True
        Me.lkpCliente.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpCliente.SearchMember = ""
        Me.lkpCliente.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpCliente.SelectAll = False
        Me.lkpCliente.Size = New System.Drawing.Size(344, 20)
        Me.lkpCliente.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpCliente.TabIndex = 11
        Me.lkpCliente.Tag = ""
        Me.lkpCliente.ToolTip = Nothing
        Me.lkpCliente.ValueMember = "cliente"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(20, 139)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 8
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "&Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(80, 139)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "c2"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(80, 19)
        Me.clcImporte.TabIndex = 9
        Me.clcImporte.Tag = ""
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(32, 115)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "&Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(80, 112)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy "
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy  hh:mm:ss"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(96, 23)
        Me.dteFecha.TabIndex = 7
        Me.dteFecha.Tag = ""
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(11, 187)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(62, 16)
        Me.lblVendedor.TabIndex = 12
        Me.lblVendedor.Tag = ""
        Me.lblVendedor.Text = "Vendedor:"
        Me.lblVendedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpVendedor
        '
        Me.lkpVendedor.AllowAdd = False
        Me.lkpVendedor.AutoReaload = False
        Me.lkpVendedor.DataSource = Nothing
        Me.lkpVendedor.DefaultSearchField = ""
        Me.lkpVendedor.DisplayMember = "nombre"
        Me.lkpVendedor.EditValue = Nothing
        Me.lkpVendedor.Enabled = False
        Me.lkpVendedor.Filtered = False
        Me.lkpVendedor.InitValue = Nothing
        Me.lkpVendedor.Location = New System.Drawing.Point(79, 186)
        Me.lkpVendedor.MultiSelect = False
        Me.lkpVendedor.Name = "lkpVendedor"
        Me.lkpVendedor.NullText = ""
        Me.lkpVendedor.PopupWidth = CType(420, Long)
        Me.lkpVendedor.ReadOnlyControl = False
        Me.lkpVendedor.Required = True
        Me.lkpVendedor.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpVendedor.SearchMember = ""
        Me.lkpVendedor.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpVendedor.SelectAll = False
        Me.lkpVendedor.Size = New System.Drawing.Size(345, 20)
        Me.lkpVendedor.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpVendedor.TabIndex = 13
        Me.lkpVendedor.Tag = "Vendedor"
        Me.lkpVendedor.ToolTip = Nothing
        Me.lkpVendedor.ValueMember = "Vendedor"
        '
        'tlbrAceptar
        '
        Me.tlbrAceptar.ImageIndex = 0
        Me.tlbrAceptar.Text = "Aceptar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 213)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 16)
        Me.Label3.TabIndex = 14
        Me.Label3.Tag = ""
        Me.Label3.Text = "V&endedor:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpvendedorCambiado
        '
        Me.lkpvendedorCambiado.AllowAdd = False
        Me.lkpvendedorCambiado.AutoReaload = False
        Me.lkpvendedorCambiado.DataSource = Nothing
        Me.lkpvendedorCambiado.DefaultSearchField = ""
        Me.lkpvendedorCambiado.DisplayMember = "nombre"
        Me.lkpvendedorCambiado.EditValue = Nothing
        Me.lkpvendedorCambiado.Filtered = False
        Me.lkpvendedorCambiado.InitValue = Nothing
        Me.lkpvendedorCambiado.Location = New System.Drawing.Point(80, 210)
        Me.lkpvendedorCambiado.MultiSelect = False
        Me.lkpvendedorCambiado.Name = "lkpvendedorCambiado"
        Me.lkpvendedorCambiado.NullText = ""
        Me.lkpvendedorCambiado.PopupWidth = CType(420, Long)
        Me.lkpvendedorCambiado.ReadOnlyControl = False
        Me.lkpvendedorCambiado.Required = True
        Me.lkpvendedorCambiado.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpvendedorCambiado.SearchMember = ""
        Me.lkpvendedorCambiado.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpvendedorCambiado.SelectAll = False
        Me.lkpvendedorCambiado.Size = New System.Drawing.Size(345, 20)
        Me.lkpvendedorCambiado.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpvendedorCambiado.TabIndex = 15
        Me.lkpvendedorCambiado.Tag = ""
        Me.lkpvendedorCambiado.ToolTip = Nothing
        Me.lkpvendedorCambiado.ValueMember = "Vendedor"
        '
        'frmCambiarVendedor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(442, 244)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lkpvendedorCambiado)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lkpVendedor)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lblOrden)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lkpCliente)
        Me.Controls.Add(Me.lkpFolio)
        Me.Controls.Add(Me.lkpSucursal)
        Me.Controls.Add(Me.lkpSerie)
        Me.Name = "frmCambiarVendedor"
        Me.Text = "frmCambiarVendedor"
        Me.Controls.SetChildIndex(Me.lkpSerie, 0)
        Me.Controls.SetChildIndex(Me.lkpSucursal, 0)
        Me.Controls.SetChildIndex(Me.lkpFolio, 0)
        Me.Controls.SetChildIndex(Me.lkpCliente, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblOrden, 0)
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.lkpVendedor, 0)
        Me.Controls.SetChildIndex(Me.lblVendedor, 0)
        Me.Controls.SetChildIndex(Me.lkpvendedorCambiado, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DIPROS Systems, Declaraciones"

    Private oSucursales As VillarrealBusiness.clsSucursales
    Private oVentas As VillarrealBusiness.clsVentas
    Private oClientes As VillarrealBusiness.clsClientes
    Private oVendedores As VillarrealBusiness.clsVendedores

    Private ReadOnly Property sucursal() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpSucursal)
        End Get
    End Property

    Private ReadOnly Property folio() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpFolio)
        End Get
    End Property
    Private ReadOnly Property vendedor() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpvendedorCambiado)
        End Get
    End Property
    Private ReadOnly Property serie() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpSerie)
        End Get
    End Property
#End Region

#Region "DIPROS Systems, Eventos de la Forma"
    Private Sub frmCambiarVendedor_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oSucursales = New VillarrealBusiness.clsSucursales
        oVentas = New VillarrealBusiness.clsVentas
        oClientes = New VillarrealBusiness.clsClientes
        oVendedores = New VillarrealBusiness.clsVendedores

        Me.tbrTools.Buttons(0).Visible = False
    End Sub

    Private Sub frmCambiarVendedor_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
      
    End Sub

    Private Sub frmCambiarVendedor_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        'Response = oVentas.ValidacionCambiarVendedor(Action, sucursal, serie, folio, vendedor)
    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"
    Private Sub frmCambiarVendedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = 0
        Me.Left = 0
    End Sub

    Private Sub lkpSucursal_Format() Handles lkpSucursal.Format
        Comunes.clsFormato.for_sucursales_grl(Me.lkpSucursal)
    End Sub

    Private Sub lkpSucursal_LoadData(ByVal Initialize As Boolean) Handles lkpSucursal.LoadData
        Dim Response As New Events
        Response = oVentas.LookupSucursalVentas(False)
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpSucursal.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub
    Private Sub lkpSucursal_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpSucursal.EditValueChanged
        lkpSerie_LoadData(True)
    End Sub

    Private Sub lkpSerie_LoadData(ByVal Initialize As Boolean) Handles lkpSerie.LoadData
        Dim response As Events
        response = oVentas.LookupSerieVentas(sucursal, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpSerie.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub

    Private Sub lkpSerie_Format() Handles lkpSerie.Format
        Comunes.clsFormato.for_series_ventas_grl(Me.lkpSerie)
    End Sub
    Private Sub lkpSerie_EditValueChanged(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles lkpSerie.EditValueChanged
        lkpFolio_LoadData(True)

       
    End Sub
    Private Sub lkpFolio_LoadData(ByVal Initialize As Boolean) Handles lkpFolio.LoadData
        Dim response As Events
        response = oVentas.LookupFolioVentas(sucursal, serie, False)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpFolio.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If
    End Sub
    Private Sub lkpfolio_Format() Handles lkpFolio.Format
        Comunes.clsFormato.for_folio_ventas_grl(Me.lkpFolio)
    End Sub

    Private Sub lkpCliente_LoadData(ByVal Initialize As Boolean) Handles lkpCliente.LoadData
        Dim Response As New Events
        Response = oClientes.LookupCliente()
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpCliente.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpCliente_Format() Handles lkpCliente.Format
        Comunes.clsFormato.for_clientes_grl(Me.lkpCliente)
    End Sub

    Private Sub lkpCliente_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpCliente.EditValueChanged
        'Dim Response As New Events
        'Response = oClientes.LookupLlenado(False, lkpCliente.GetValue("cliente"))
        'If Not Response.ErrorFound Then
        '    Dim oDataSet As DataSet
        '    oDataSet = Response.Value
        '    Me.lkpCliente.DataSource = oDataSet.Tables(0)
        '    oDataSet = Nothing
        'End If
        'Response = Nothing
    End Sub

    Private Sub lkpVendedor_Format() Handles lkpVendedor.Format
        Comunes.clsFormato.for_vendedores_grl(Me.lkpVendedor)
    End Sub

    Private Sub lkpVendedor_LoadData(ByVal Initialize As Boolean) Handles lkpVendedor.LoadData
        Dim Response As New Events
        Response = oVendedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpVendedor.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpvendedorCambiado_Format() Handles lkpvendedorCambiado.Format
        Comunes.clsFormato.for_vendedores_grl(Me.lkpvendedorCambiado)
    End Sub

    Private Sub lkpvendedorCambiado_LoadData(ByVal Initialize As Boolean) Handles lkpvendedorCambiado.LoadData
        Dim Response As New Events
        Response = oVendedores.Lookup
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpvendedorCambiado.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
        Response = Nothing
    End Sub

    Private Sub lkpFolio_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpFolio.EditValueChanged
        traerDatos()
    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick

        If e.Button.Text = "Aceptar" Then
            Dim response As New Events

            response = oVentas.ValidacionCambiarVendedor(Action, sucursal, serie, folio, vendedor)
            If response.ErrorFound Then
                response.ShowError()
                Exit Sub
            End If

            response = oVentas.ActualizaVendedorVentas(sucursal, serie, folio, vendedor)
            If response.ErrorFound Then
                response.ShowError()
                Exit Sub
            End If
            traerDatos()

            response = Nothing
        End If
    End Sub
#End Region

#Region "DIPROS Systems, Funcionalidad"
    Private Sub traerDatos()
        Dim response As Events
        response = oVentas.DespliegaDatos(sucursal, serie, folio)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value

            If oDataSet.Tables(0).Rows.Count > 0 Then
                Me.dteFecha.EditValue = oDataSet.Tables(0).Rows(0).Item("fecha")
                Me.clcImporte.EditValue = oDataSet.Tables(0).Rows(0).Item("total")
                Me.lkpCliente.EditValue = oDataSet.Tables(0).Rows(0).Item("cliente")
                Me.lkpVendedor.EditValue = oDataSet.Tables(0).Rows(0).Item("vendedor")
            Else
                Me.dteFecha.EditValue = CDate("01/01/1900")
                Me.clcImporte.EditValue = 0
                Me.lkpCliente.EditValue = Nothing
                Me.lkpVendedor.EditValue = Nothing
                Me.lkpvendedorCambiado.EditValue = Nothing


            End If
        Else

            response.ShowError()
        End If
    End Sub
#End Region

   
End Class
