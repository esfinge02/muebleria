Imports Dipros.Utils
Imports Dipros.Utils.Common

Public Class frmConsultaNotaCredito
    Inherits Dipros.Windows.frmTINForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents lkpNotaCredito As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblNotaCredito As System.Windows.Forms.Label
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcDocumentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDocumentos As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents grDocumentos As DevExpress.XtraGrid.GridControl
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursalNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grvDocumentos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtSucursal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grcNumeroDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbCancelarNotaCredito As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents lkpConceptoNotaCredito As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConsultaNotaCredito))
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.lblSerie = New System.Windows.Forms.Label
        Me.lkpNotaCredito = New Dipros.Editors.TINMultiLookup
        Me.lblNotaCredito = New System.Windows.Forms.Label
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lblCliente = New System.Windows.Forms.Label
        Me.txtCliente = New DevExpress.XtraEditors.TextEdit
        Me.clcDocumentos = New Dipros.Editors.TINCalcEdit
        Me.lblDocumentos = New System.Windows.Forms.Label
        Me.txtSucursal = New DevExpress.XtraEditors.TextEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.grDocumentos = New DevExpress.XtraGrid.GridControl
        Me.grvDocumentos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursalNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbCancelarNotaCredito = New System.Windows.Forms.ToolBarButton
        Me.tbReimprimir = New System.Windows.Forms.ToolBarButton
        Me.Label25 = New System.Windows.Forms.Label
        Me.lkpConceptoNotaCredito = New Dipros.Editors.TINMultiLookup
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDocumentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbCancelarNotaCredito, Me.tbReimprimir})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1659, 28)
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(117, 64)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie.Properties.MaxLength = 3
        Me.txtSerie.Size = New System.Drawing.Size(32, 20)
        Me.txtSerie.TabIndex = 3
        Me.txtSerie.Tag = ""
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(75, 64)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie.TabIndex = 2
        Me.lblSerie.Tag = ""
        Me.lblSerie.Text = "&Serie:"
        Me.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpNotaCredito
        '
        Me.lkpNotaCredito.AllowAdd = False
        Me.lkpNotaCredito.AutoReaload = False
        Me.lkpNotaCredito.DataSource = Nothing
        Me.lkpNotaCredito.DefaultSearchField = ""
        Me.lkpNotaCredito.DisplayMember = "folio"
        Me.lkpNotaCredito.EditValue = Nothing
        Me.lkpNotaCredito.Filtered = False
        Me.lkpNotaCredito.InitValue = Nothing
        Me.lkpNotaCredito.Location = New System.Drawing.Point(117, 87)
        Me.lkpNotaCredito.MultiSelect = False
        Me.lkpNotaCredito.Name = "lkpNotaCredito"
        Me.lkpNotaCredito.NullText = ""
        Me.lkpNotaCredito.PopupWidth = CType(350, Long)
        Me.lkpNotaCredito.ReadOnlyControl = False
        Me.lkpNotaCredito.Required = False
        Me.lkpNotaCredito.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpNotaCredito.SearchMember = ""
        Me.lkpNotaCredito.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpNotaCredito.SelectAll = False
        Me.lkpNotaCredito.Size = New System.Drawing.Size(136, 20)
        Me.lkpNotaCredito.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpNotaCredito.TabIndex = 5
        Me.lkpNotaCredito.Tag = ""
        Me.lkpNotaCredito.ToolTip = Nothing
        Me.lkpNotaCredito.ValueMember = "campos_llave"
        '
        'lblNotaCredito
        '
        Me.lblNotaCredito.AutoSize = True
        Me.lblNotaCredito.Location = New System.Drawing.Point(16, 87)
        Me.lblNotaCredito.Name = "lblNotaCredito"
        Me.lblNotaCredito.Size = New System.Drawing.Size(96, 16)
        Me.lblNotaCredito.TabIndex = 4
        Me.lblNotaCredito.Tag = ""
        Me.lblNotaCredito.Text = "&Nota de Cr�dito:"
        Me.lblNotaCredito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(23, 207)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 16
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(117, 207)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.Enabled = False
        Me.txtObservaciones.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservaciones.Size = New System.Drawing.Size(360, 38)
        Me.txtObservaciones.TabIndex = 17
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(59, 183)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 12
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(117, 183)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatString = "$###,###,##0.00"
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(96, 19)
        Me.clcImporte.TabIndex = 13
        Me.clcImporte.Tag = "importe"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(71, 110)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(117, 110)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(96, 20)
        Me.dteFecha.TabIndex = 7
        Me.dteFecha.Tag = ""
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(65, 159)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 10
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCliente
        '
        Me.txtCliente.EditValue = ""
        Me.txtCliente.Location = New System.Drawing.Point(117, 159)
        Me.txtCliente.Name = "txtCliente"
        '
        'txtCliente.Properties
        '
        Me.txtCliente.Properties.Enabled = False
        Me.txtCliente.Properties.MaxLength = 3
        Me.txtCliente.Size = New System.Drawing.Size(360, 20)
        Me.txtCliente.TabIndex = 11
        Me.txtCliente.Tag = ""
        '
        'clcDocumentos
        '
        Me.clcDocumentos.EditValue = "0"
        Me.clcDocumentos.Location = New System.Drawing.Point(320, 183)
        Me.clcDocumentos.MaxValue = 0
        Me.clcDocumentos.MinValue = 0
        Me.clcDocumentos.Name = "clcDocumentos"
        '
        'clcDocumentos.Properties
        '
        Me.clcDocumentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDocumentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDocumentos.Properties.Enabled = False
        Me.clcDocumentos.Properties.MaskData.EditMask = "0"
        Me.clcDocumentos.Properties.Precision = 2
        Me.clcDocumentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDocumentos.Size = New System.Drawing.Size(96, 19)
        Me.clcDocumentos.TabIndex = 15
        Me.clcDocumentos.Tag = "documentos"
        '
        'lblDocumentos
        '
        Me.lblDocumentos.AutoSize = True
        Me.lblDocumentos.Location = New System.Drawing.Point(232, 183)
        Me.lblDocumentos.Name = "lblDocumentos"
        Me.lblDocumentos.Size = New System.Drawing.Size(78, 16)
        Me.lblDocumentos.TabIndex = 14
        Me.lblDocumentos.Tag = ""
        Me.lblDocumentos.Text = "Documentos:"
        Me.lblDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSucursal
        '
        Me.txtSucursal.EditValue = ""
        Me.txtSucursal.Location = New System.Drawing.Point(117, 135)
        Me.txtSucursal.Name = "txtSucursal"
        '
        'txtSucursal.Properties
        '
        Me.txtSucursal.Properties.Enabled = False
        Me.txtSucursal.Properties.MaxLength = 3
        Me.txtSucursal.Size = New System.Drawing.Size(360, 20)
        Me.txtSucursal.TabIndex = 9
        Me.txtSucursal.Tag = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(56, 135)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 8
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grDocumentos
        '
        '
        'grDocumentos.EmbeddedNavigator
        '
        Me.grDocumentos.EmbeddedNavigator.Name = ""
        Me.grDocumentos.Location = New System.Drawing.Point(8, 272)
        Me.grDocumentos.MainView = Me.grvDocumentos
        Me.grDocumentos.Name = "grDocumentos"
        Me.grDocumentos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grDocumentos.Size = New System.Drawing.Size(552, 168)
        Me.grDocumentos.TabIndex = 19
        Me.grDocumentos.Text = "Documentos"
        '
        'grvDocumentos
        '
        Me.grvDocumentos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSucursal, Me.grcSucursalNombre, Me.grcFactura, Me.grcNumeroDocumento, Me.grcFecha, Me.grcImporte})
        Me.grvDocumentos.GridControl = Me.grDocumentos
        Me.grvDocumentos.Name = "grvDocumentos"
        Me.grvDocumentos.OptionsCustomization.AllowFilter = False
        Me.grvDocumentos.OptionsCustomization.AllowGroup = False
        Me.grvDocumentos.OptionsCustomization.AllowSort = False
        Me.grvDocumentos.OptionsView.ShowGroupPanel = False
        Me.grvDocumentos.OptionsView.ShowIndicator = False
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSucursalNombre
        '
        Me.grcSucursalNombre.Caption = "Sucursal"
        Me.grcSucursalNombre.FieldName = "sucursal_nombre"
        Me.grcSucursalNombre.Name = "grcSucursalNombre"
        Me.grcSucursalNombre.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursalNombre.VisibleIndex = 0
        Me.grcSucursalNombre.Width = 152
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 1
        Me.grcFactura.Width = 80
        '
        'grcNumeroDocumento
        '
        Me.grcNumeroDocumento.Caption = "Documento"
        Me.grcNumeroDocumento.FieldName = "documento"
        Me.grcNumeroDocumento.Name = "grcNumeroDocumento"
        Me.grcNumeroDocumento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumeroDocumento.VisibleIndex = 2
        Me.grcNumeroDocumento.Width = 70
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha Vencimiento"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha_vencimiento"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 109
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 139
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 256)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 17)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "&DOCUMENTOS"
        '
        'tbCancelarNotaCredito
        '
        Me.tbCancelarNotaCredito.Text = "Cancelar Nota"
        '
        'tbReimprimir
        '
        Me.tbReimprimir.Text = "Reimpresi�n"
        Me.tbReimprimir.ToolTipText = "Imprime la Nota de Cr�dito"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(52, 40)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(60, 16)
        Me.Label25.TabIndex = 0
        Me.Label25.Tag = ""
        Me.Label25.Text = "Concepto:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoNotaCredito
        '
        Me.lkpConceptoNotaCredito.AllowAdd = False
        Me.lkpConceptoNotaCredito.AutoReaload = False
        Me.lkpConceptoNotaCredito.DataSource = Nothing
        Me.lkpConceptoNotaCredito.DefaultSearchField = ""
        Me.lkpConceptoNotaCredito.DisplayMember = "descripcion"
        Me.lkpConceptoNotaCredito.EditValue = Nothing
        Me.lkpConceptoNotaCredito.Filtered = False
        Me.lkpConceptoNotaCredito.InitValue = Nothing
        Me.lkpConceptoNotaCredito.Location = New System.Drawing.Point(117, 40)
        Me.lkpConceptoNotaCredito.MultiSelect = False
        Me.lkpConceptoNotaCredito.Name = "lkpConceptoNotaCredito"
        Me.lkpConceptoNotaCredito.NullText = ""
        Me.lkpConceptoNotaCredito.PopupWidth = CType(400, Long)
        Me.lkpConceptoNotaCredito.ReadOnlyControl = False
        Me.lkpConceptoNotaCredito.Required = False
        Me.lkpConceptoNotaCredito.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoNotaCredito.SearchMember = ""
        Me.lkpConceptoNotaCredito.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoNotaCredito.SelectAll = False
        Me.lkpConceptoNotaCredito.Size = New System.Drawing.Size(211, 20)
        Me.lkpConceptoNotaCredito.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoNotaCredito.TabIndex = 1
        Me.lkpConceptoNotaCredito.Tag = "concepto_cxp_nota_credito"
        Me.lkpConceptoNotaCredito.ToolTip = "Concepto de la Nota de Cr�dito en CxP"
        Me.lkpConceptoNotaCredito.ValueMember = "concepto"
        '
        'frmConsultaNotaCredito
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(570, 448)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.lkpConceptoNotaCredito)
        Me.Controls.Add(Me.grDocumentos)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lkpNotaCredito)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.lblNotaCredito)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.clcDocumentos)
        Me.Controls.Add(Me.lblDocumentos)
        Me.Controls.Add(Me.txtSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Name = "frmConsultaNotaCredito"
        Me.Text = "frmConsultaNotaCredito"
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.txtSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblDocumentos, 0)
        Me.Controls.SetChildIndex(Me.clcDocumentos, 0)
        Me.Controls.SetChildIndex(Me.txtCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblNotaCredito, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.txtSerie, 0)
        Me.Controls.SetChildIndex(Me.lkpNotaCredito, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.grDocumentos, 0)
        Me.Controls.SetChildIndex(Me.lkpConceptoNotaCredito, 0)
        Me.Controls.SetChildIndex(Me.Label25, 0)
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDocumentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "DIPROS Systems, Declaraciones"
    Private oMovimientosCobrar As New VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As New VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oConceptosCXC As New VillarrealBusiness.clsConceptosCxc

    Private ReadOnly Property Serie() As String
        Get
            Return Me.txtSerie.Text
        End Get
    End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return CLng(Me.lkpNotaCredito.GetValue("sucursal"))
        End Get
    End Property
    'Private ReadOnly Property Concepto() As String
    '    Get
    '        Return CStr(Me.lkpNotaCredito.GetValue("concepto"))
    '    End Get
    'End Property
    Private ReadOnly Property Folio() As Long
        Get
            Return CLng(Me.lkpNotaCredito.GetValue("folio"))
        End Get
    End Property

    Private ReadOnly Property Cliente() As Long
        Get
            Return CLng(Me.lkpNotaCredito.GetValue("cliente"))
        End Get
    End Property

    Private ReadOnly Property concepto_cxc_nota_credito() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoNotaCredito)
        End Get
    End Property

    Private FolioElectronico As Long = 0

#End Region

#Region "DIPROS Systems, Eventos de la Forma"

    Private Sub frmConsultaNotaCredito_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle

        Me.Location = New Point(0, 0)
        Me.tbrTools.Buttons(0).Visible = False
        Me.tbrTools.Buttons(0).Enabled = False
        Me.tbrTools.Buttons(2).Enabled = False
        Me.tbrTools.Buttons(3).Enabled = False

    End Sub

#End Region

#Region "DIPROS Systems, Eventos de Controles"

    Private Sub lkpConceptoNotaCredito_Format() Handles lkpConceptoNotaCredito.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpConceptoNotaCredito)
    End Sub
    Private Sub lkpConceptoNotaCredito_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoNotaCredito.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("A")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoNotaCredito.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub lkpNotaCredito_LoadData(ByVal Initialize As Boolean) Handles lkpNotaCredito.LoadData
        Dim response As Events
        response = oMovimientosCobrar.LookupNotaCredito(Serie, Me.concepto_cxc_nota_credito)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpNotaCredito.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpNotaCredito_Format() Handles lkpNotaCredito.Format
        Comunes.clsFormato.for_notas_credito_grl(Me.lkpNotaCredito)
    End Sub
    Private Sub lkpNotaCredito_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkpNotaCredito.Validated
        If Me.lkpNotaCredito.Text = "" Then
            Me.lkpNotaCredito.Focus()
            Me.tbrTools.Buttons(2).Enabled = False
            Me.tbrTools.Buttons(3).Enabled = False


        Else
            Me.tbrTools.Buttons(2).Enabled = True
            Me.tbrTools.Buttons(3).Enabled = True
        End If
    End Sub
    Private Sub lkpNotaCredito_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpNotaCredito.EditValueChanged
        Dim oEvents As New Events

        If Me.lkpNotaCredito.EditValue Is Nothing Then
            LimpiarDatosNotaCredito()
            Me.tbrTools.Buttons(2).Enabled = False
            Exit Sub
        End If

        DesplegarDatosDeNotaCredito()
        Me.tbrTools.Buttons(2).Enabled = True
        DesplegarDocumentosDeNotaCredito(oEvents)
        If oEvents.ErrorFound Then
            oEvents.ShowError()
            Exit Sub
        End If
    End Sub

    Private Sub txtSerie_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerie.EditValueChanged
        If Me.txtSerie.Text.Length < 1 Then Exit Sub

        Me.lkpNotaCredito.EditValue = Nothing
        Me.lkpNotaCredito_LoadData(True)

    End Sub
    Private Sub txtSerie_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerie.Validated
        If Me.lkpNotaCredito.DataSource Is Nothing Then
            Me.txtSerie.Text = ""
            Me.txtSerie.Focus()
            Exit Sub
        End If

        If CType(Me.lkpNotaCredito.DataSource, DataTable).Rows.Count < 1 Then
            Me.txtSerie.Text = ""
            Me.txtSerie.Focus()
        End If
    End Sub

    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tbCancelarNotaCredito Then
            Dim oEvents As Events

            oEvents = oMovimientosCobrar.CancelarNotaCredito(Sucursal, Me.concepto_cxc_nota_credito, Serie, Folio, Cliente, CDate(TINApp.FechaServidor).ToString)
            If Not oEvents.ErrorFound Then
                'Me.Close()

                If Not oEvents.ErrorFound Then
                    oEvents = Me.oMovimientosCobrar.CancelarTimbrado(Serie, Folio, Me.FolioElectronico)
                End If

                Me.lkpNotaCredito.EditValue = Nothing
                Me.lkpNotaCredito_LoadData(True)
                Me.txtSerie.Focus()
            Else
                oEvents.ShowError()
            End If

        End If

        If e.Button Is Me.tbReimprimir Then
            Dim pregunta As Boolean
            Select Case ShowMessage(MessageType.MsgQuestion, "�Con iva desglosado?")
                Case Answer.MsgYes
                    pregunta = True
                Case Answer.MsgNo
                    pregunta = False
                Case Answer.MsgCancel
                    Exit Sub
            End Select
            Me.ImprimeNotaDeCredito(Sucursal, Me.concepto_cxc_nota_credito, Serie, Folio, Cliente, pregunta)

        End If
    End Sub

#End Region

#Region "DIPROS Systems, Funcionalidad"

    Private Sub DesplegarDatosDeNotaCredito()
        Me.dteFecha.EditValue = CDate(Me.lkpNotaCredito.GetValue("fecha"))
        Me.txtCliente.Text = CStr(Me.lkpNotaCredito.GetValue("nombre_cliente"))
        Me.txtSucursal.Text = CStr(Me.lkpNotaCredito.GetValue("nombre_sucursal"))
        Me.clcImporte.EditValue = CDbl(Me.lkpNotaCredito.GetValue("total"))
        Me.clcDocumentos.EditValue = CLng(Me.lkpNotaCredito.GetValue("documentos"))
        Me.txtObservaciones.Text = CStr(Me.lkpNotaCredito.GetValue("observaciones"))
        FolioElectronico = CLng(Me.lkpNotaCredito.GetValue("folio_factura_electronica"))
    End Sub
    Private Sub LimpiarDatosNotaCredito()
        Me.dteFecha.EditValue = Nothing
        Me.txtCliente.Text = ""
        Me.txtSucursal.Text = ""
        Me.clcImporte.EditValue = 0
        Me.clcDocumentos.EditValue = 0
        Me.txtObservaciones.Text = ""
        Me.grDocumentos.DataSource = Nothing
        FolioElectronico = 0

    End Sub
    Private Sub DesplegarDocumentosDeNotaCredito(ByRef response As Events)
        response = oMovimientosCobrarDetalle.ConsultaDocumentosSaldados(Sucursal, Me.concepto_cxc_nota_credito, Serie, Folio, Cliente)

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.grDocumentos.DataSource = oDataSet.Tables(0)
        End If

    End Sub
    Private Sub ImprimeNotaDeCredito(ByVal sucursal As Long, ByVal concepto As String, ByVal serie As String, ByVal folio As Long, ByVal cliente As Long, Optional ByVal iva_desglosado As Boolean = True)
        Dim Response As Events
        Dim oReportes As New VillarrealBusiness.Reportes

        Try
            Response = oReportes.ImprimeNotaDeCredito(sucursal, concepto, serie, folio, cliente, iva_desglosado)

            If Response.ErrorFound Then
                ShowMessage(MessageType.MsgInformation, "Las Notas de Cr�dito no pueden Mostrarse")
            Else
                If Response.Value.Tables(0).Rows.Count > 0 Then
                    Dim oDataSet As DataSet
                    Dim oReport As New Comunes.rptNotaDeCredito

                    oDataSet = Response.Value
                    oReport.DataSource = oDataSet.Tables(0)

                    'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                    TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cr�dito " & CType(folio, String), oReport)
                Else
                    ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
                End If
            End If
        Catch ex As Exception
            ShowMessage(MessageType.MsgError, ex.ToString, )
        End Try
    End Sub


#End Region



End Class
