Imports Dipros.Utils
Imports Dipros.Utils.Common
Imports Comunes.clsUtilerias

Public Class frmDescuentosEspecialesClientesDetalle
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lkpGrupo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents lkpDepartamento As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents txtNArticulo As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbl_ePartida As System.Windows.Forms.Label
    Friend WithEvents lblPartida As System.Windows.Forms.Label
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lkpArticulo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents clcCantidad As Dipros.Editors.TINCalcEdit
    Friend WithEvents txtmodelo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcPrecioLista As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcPrecioDescuento As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents clcPrecioListaCantidad As DevExpress.XtraEditors.CalcEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDescuentosEspecialesClientesDetalle))
        Me.lkpGrupo = New Dipros.Editors.TINMultiLookup
        Me.lblGrupo = New System.Windows.Forms.Label
        Me.lkpDepartamento = New Dipros.Editors.TINMultiLookup
        Me.lblDepartamento = New System.Windows.Forms.Label
        Me.txtNArticulo = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lbl_ePartida = New System.Windows.Forms.Label
        Me.lblPartida = New System.Windows.Forms.Label
        Me.lblArticulo = New System.Windows.Forms.Label
        Me.lkpArticulo = New Dipros.Editors.TINMultiLookup
        Me.lblCantidad = New System.Windows.Forms.Label
        Me.clcCantidad = New Dipros.Editors.TINCalcEdit
        Me.txtmodelo = New DevExpress.XtraEditors.TextEdit
        Me.clcPrecioLista = New DevExpress.XtraEditors.CalcEdit
        Me.clcPrecioDescuento = New DevExpress.XtraEditors.CalcEdit
        Me.clcPrecioListaCantidad = New DevExpress.XtraEditors.CalcEdit
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioLista.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcPrecioListaCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Name = "tbrTools"
        '
        'lkpGrupo
        '
        Me.lkpGrupo.AllowAdd = False
        Me.lkpGrupo.AutoReaload = True
        Me.lkpGrupo.DataSource = Nothing
        Me.lkpGrupo.DefaultSearchField = ""
        Me.lkpGrupo.DisplayMember = "descripcion"
        Me.lkpGrupo.EditValue = Nothing
        Me.lkpGrupo.Filtered = False
        Me.lkpGrupo.InitValue = Nothing
        Me.lkpGrupo.Location = New System.Drawing.Point(104, 88)
        Me.lkpGrupo.MultiSelect = False
        Me.lkpGrupo.Name = "lkpGrupo"
        Me.lkpGrupo.NullText = ""
        Me.lkpGrupo.PopupWidth = CType(470, Long)
        Me.lkpGrupo.ReadOnlyControl = False
        Me.lkpGrupo.Required = False
        Me.lkpGrupo.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpGrupo.SearchMember = ""
        Me.lkpGrupo.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpGrupo.SelectAll = False
        Me.lkpGrupo.Size = New System.Drawing.Size(274, 20)
        Me.lkpGrupo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpGrupo.TabIndex = 64
        Me.lkpGrupo.Tag = "grupo"
        Me.lkpGrupo.ToolTip = "grupo"
        Me.lkpGrupo.ValueMember = "grupo"
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(56, 88)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(43, 16)
        Me.lblGrupo.TabIndex = 63
        Me.lblGrupo.Tag = ""
        Me.lblGrupo.Text = "&Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpDepartamento
        '
        Me.lkpDepartamento.AllowAdd = False
        Me.lkpDepartamento.AutoReaload = True
        Me.lkpDepartamento.DataSource = Nothing
        Me.lkpDepartamento.DefaultSearchField = ""
        Me.lkpDepartamento.DisplayMember = "nombre"
        Me.lkpDepartamento.EditValue = Nothing
        Me.lkpDepartamento.Filtered = False
        Me.lkpDepartamento.InitValue = Nothing
        Me.lkpDepartamento.Location = New System.Drawing.Point(104, 64)
        Me.lkpDepartamento.MultiSelect = False
        Me.lkpDepartamento.Name = "lkpDepartamento"
        Me.lkpDepartamento.NullText = ""
        Me.lkpDepartamento.PopupWidth = CType(470, Long)
        Me.lkpDepartamento.ReadOnlyControl = False
        Me.lkpDepartamento.Required = False
        Me.lkpDepartamento.SearchFirstBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpDepartamento.SearchMember = ""
        Me.lkpDepartamento.SearchSecondBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpDepartamento.SelectAll = False
        Me.lkpDepartamento.Size = New System.Drawing.Size(274, 20)
        Me.lkpDepartamento.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpDepartamento.TabIndex = 62
        Me.lkpDepartamento.Tag = "departamento"
        Me.lkpDepartamento.ToolTip = "departamento"
        Me.lkpDepartamento.ValueMember = "departamento"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(8, 64)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(88, 16)
        Me.lblDepartamento.TabIndex = 61
        Me.lblDepartamento.Tag = ""
        Me.lblDepartamento.Text = "&Departamento:"
        Me.lblDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNArticulo
        '
        Me.txtNArticulo.Location = New System.Drawing.Point(104, 136)
        Me.txtNArticulo.Name = "txtNArticulo"
        Me.txtNArticulo.Size = New System.Drawing.Size(320, 16)
        Me.txtNArticulo.TabIndex = 68
        Me.txtNArticulo.Tag = "n_articulo"
        Me.txtNArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 67
        Me.Label2.Tag = ""
        Me.Label2.Text = "&Descripci�n:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_ePartida
        '
        Me.lbl_ePartida.AutoSize = True
        Me.lbl_ePartida.Location = New System.Drawing.Point(48, 40)
        Me.lbl_ePartida.Name = "lbl_ePartida"
        Me.lbl_ePartida.Size = New System.Drawing.Size(48, 16)
        Me.lbl_ePartida.TabIndex = 59
        Me.lbl_ePartida.Tag = ""
        Me.lbl_ePartida.Text = "Part&ida:"
        Me.lbl_ePartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPartida
        '
        Me.lblPartida.AutoSize = True
        Me.lblPartida.Location = New System.Drawing.Point(104, 40)
        Me.lblPartida.Name = "lblPartida"
        Me.lblPartida.Size = New System.Drawing.Size(11, 16)
        Me.lblPartida.TabIndex = 60
        Me.lblPartida.Tag = "partida"
        Me.lblPartida.Text = "0"
        Me.lblPartida.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(48, 112)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(51, 16)
        Me.lblArticulo.TabIndex = 65
        Me.lblArticulo.Tag = ""
        Me.lblArticulo.Text = "Art�c&ulo:"
        Me.lblArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpArticulo
        '
        Me.lkpArticulo.AllowAdd = False
        Me.lkpArticulo.AutoReaload = True
        Me.lkpArticulo.DataSource = Nothing
        Me.lkpArticulo.DefaultSearchField = "modelo"
        Me.lkpArticulo.DisplayMember = "modelo"
        Me.lkpArticulo.EditValue = Nothing
        Me.lkpArticulo.Filtered = False
        Me.lkpArticulo.InitValue = Nothing
        Me.lkpArticulo.Location = New System.Drawing.Point(104, 112)
        Me.lkpArticulo.MultiSelect = False
        Me.lkpArticulo.Name = "lkpArticulo"
        Me.lkpArticulo.NullText = ""
        Me.lkpArticulo.PopupWidth = CType(400, Long)
        Me.lkpArticulo.ReadOnlyControl = False
        Me.lkpArticulo.Required = False
        Me.lkpArticulo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpArticulo.SearchMember = "modelo"
        Me.lkpArticulo.SearchSecondBy = Dipros.Editors.eSearchPriority.SearchMember
        Me.lkpArticulo.SelectAll = False
        Me.lkpArticulo.Size = New System.Drawing.Size(152, 20)
        Me.lkpArticulo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpArticulo.TabIndex = 66
        Me.lkpArticulo.Tag = "Articulo"
        Me.lkpArticulo.ToolTip = Nothing
        Me.lkpArticulo.ValueMember = "Articulo"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(40, 160)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(58, 16)
        Me.lblCantidad.TabIndex = 69
        Me.lblCantidad.Tag = ""
        Me.lblCantidad.Text = "Ca&ntidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcCantidad
        '
        Me.clcCantidad.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.clcCantidad.Location = New System.Drawing.Point(104, 160)
        Me.clcCantidad.MaxValue = 0
        Me.clcCantidad.MinValue = 0
        Me.clcCantidad.Name = "clcCantidad"
        '
        'clcCantidad.Properties
        '
        Me.clcCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcCantidad.Properties.MaskData.EditMask = "########0.00"
        Me.clcCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcCantidad.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.clcCantidad.Size = New System.Drawing.Size(48, 19)
        Me.clcCantidad.TabIndex = 70
        Me.clcCantidad.Tag = "cantidad"
        '
        'txtmodelo
        '
        Me.txtmodelo.EditValue = ""
        Me.txtmodelo.Location = New System.Drawing.Point(352, 40)
        Me.txtmodelo.Name = "txtmodelo"
        '
        'txtmodelo.Properties
        '
        Me.txtmodelo.Properties.Enabled = False
        Me.txtmodelo.Size = New System.Drawing.Size(75, 20)
        Me.txtmodelo.TabIndex = 90
        Me.txtmodelo.TabStop = False
        Me.txtmodelo.Tag = "modelo"
        Me.txtmodelo.Visible = False
        '
        'clcPrecioLista
        '
        Me.clcPrecioLista.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPrecioLista.Location = New System.Drawing.Point(256, 40)
        Me.clcPrecioLista.Name = "clcPrecioLista"
        Me.clcPrecioLista.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPrecioLista.Size = New System.Drawing.Size(75, 20)
        Me.clcPrecioLista.TabIndex = 91
        Me.clcPrecioLista.TabStop = False
        Me.clcPrecioLista.Tag = "precio_lista"
        Me.clcPrecioLista.Visible = False
        '
        'clcPrecioDescuento
        '
        Me.clcPrecioDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPrecioDescuento.Location = New System.Drawing.Point(352, 160)
        Me.clcPrecioDescuento.Name = "clcPrecioDescuento"
        Me.clcPrecioDescuento.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPrecioDescuento.Size = New System.Drawing.Size(75, 20)
        Me.clcPrecioDescuento.TabIndex = 92
        Me.clcPrecioDescuento.TabStop = False
        Me.clcPrecioDescuento.Tag = "precio_descuento"
        Me.clcPrecioDescuento.Visible = False
        '
        'clcPrecioListaCantidad
        '
        Me.clcPrecioListaCantidad.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.clcPrecioListaCantidad.Location = New System.Drawing.Point(256, 160)
        Me.clcPrecioListaCantidad.Name = "clcPrecioListaCantidad"
        Me.clcPrecioListaCantidad.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.clcPrecioListaCantidad.Size = New System.Drawing.Size(75, 20)
        Me.clcPrecioListaCantidad.TabIndex = 93
        Me.clcPrecioListaCantidad.TabStop = False
        Me.clcPrecioListaCantidad.Tag = "precio_lista_cantidad"
        Me.clcPrecioListaCantidad.Visible = False
        '
        'frmDescuentosEspecialesClientesDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(449, 200)
        Me.Controls.Add(Me.clcPrecioListaCantidad)
        Me.Controls.Add(Me.clcPrecioDescuento)
        Me.Controls.Add(Me.clcPrecioLista)
        Me.Controls.Add(Me.txtmodelo)
        Me.Controls.Add(Me.lkpGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.lkpDepartamento)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.txtNArticulo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lbl_ePartida)
        Me.Controls.Add(Me.lblPartida)
        Me.Controls.Add(Me.lblArticulo)
        Me.Controls.Add(Me.lkpArticulo)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.clcCantidad)
        Me.Name = "frmDescuentosEspecialesClientesDetalle"
        Me.Text = "frmDescuentosEspecialesClientesDetalle"
        Me.Controls.SetChildIndex(Me.clcCantidad, 0)
        Me.Controls.SetChildIndex(Me.lblCantidad, 0)
        Me.Controls.SetChildIndex(Me.lkpArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblPartida, 0)
        Me.Controls.SetChildIndex(Me.lbl_ePartida, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtNArticulo, 0)
        Me.Controls.SetChildIndex(Me.lblDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lkpDepartamento, 0)
        Me.Controls.SetChildIndex(Me.lblGrupo, 0)
        Me.Controls.SetChildIndex(Me.lkpGrupo, 0)
        Me.Controls.SetChildIndex(Me.txtmodelo, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioLista, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioDescuento, 0)
        Me.Controls.SetChildIndex(Me.clcPrecioListaCantidad, 0)
        CType(Me.clcCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtmodelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioLista.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcPrecioListaCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oDepartamentos As New VillarrealBusiness.clsDepartamentos
    Private oGrupos As New VillarrealBusiness.clsGruposArticulos
    Private oArticulos As New VillarrealBusiness.clsArticulos
    Private oDescuentosEspecialesClientesDetalles As New VillarrealBusiness.clsDescuentosEspecialesClientesDetalle

    Private ReadOnly Property Departamento() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpDepartamento)
        End Get
    End Property
    Private ReadOnly Property Grupo() As Long
        Get
            Return Comunes.clsUtilerias.PreparaValorLookup(Me.lkpGrupo)
        End Get
    End Property
    Private ReadOnly Property Articulo() As Long
        Get
            Return PreparaValorLookup(Me.lkpArticulo)
        End Get
    End Property

    Private articulo_temporal As Long = 0

#Region "Eventos de la Forma"
    Private Sub frmDescuentosEspecialesClientesDetalle_Accept(ByRef Response As Dipros.Utils.Events) Handles MyBase.Accept
        With OwnerForm.MasterControl
            Select Case Action
                Case Actions.Insert
                    'CargarCostosArticulos() 
                    .AddRow(Me.DataSource)
                    'OwnerForm.dteFechaPedido.Enabled = False
                    'CType(OwnerForm, frmVentas).CategoriasIdentificables = Me.lCategoriasIdentificablesArticulo

                Case Actions.Update

                    'CargarCostosArticulos() 
                    .UpdateRow(Me.DataSource)

                Case Actions.Delete

                    .DeleteRow()
                    'CType(OwnerForm, frmVentas).CategoriasIdentificables = -1
            End Select
        End With

        CType(Me.OwnerForm, frmDescuentosEspecialesClientes).CalculaPrecioDescuento()
    End Sub
    Private Sub frmDescuentosEspecialesClientesDetalle_ValidateFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.ValidateFields
        Response = oDescuentosEspecialesClientesDetalles.Validacion(Articulo, Me.clcPrecioLista.Value)
    End Sub
    Private Sub frmDescuentosEspecialesClientesDetalle_DisplayFields(ByRef Response As Dipros.Utils.Events) Handles MyBase.DisplayFields
        Me.DataSource = OwnerForm.MasterControl.SelectedRow
        'DEPARTAMENTO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("departamento")) Then

            Me.lkpDepartamento.EditValue = -1

        Else

            Me.lkpDepartamento.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("departamento")), Long)

        End If
        'GRUPO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("grupo")) Then

            Me.lkpGrupo.EditValue = -1

        Else

            Me.lkpGrupo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("grupo")), Long)

        End If
        'ARTICULO
        If IsDBNull(OwnerForm.MasterControl.SelectedRow.Tables(0).Rows(0)("articulo")) Then

            Me.lkpArticulo.EditValue = -1

        Else

            Me.lkpArticulo.EditValue = CType((CType(OwnerForm.MasterControl.SelectedRow, DataSet).Tables(0).Rows(0)("articulo")), Long)
            If Me.lkpArticulo.EditValue > 0 Then

                If Departamento <= 0 Or Grupo <= 0 Then

                    Actualiza_Lookups()

                End If

            End If

        End If

    End Sub

#End Region

#Region "Eventos de los Controles"

#End Region

#Region "Lookup"
    Private Sub lkpDepartamento_LoadData(ByVal Initialize As Boolean) Handles lkpDepartamento.LoadData

        Dim Response As New Events

        Response = oDepartamentos.Lookup()

        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpDepartamento.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpDepartamento_Format() Handles lkpDepartamento.Format
        Comunes.clsFormato.for_departamentos_grl(Me.lkpDepartamento)
    End Sub
    Private Sub lkpDepartamento_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpDepartamento.EditValueChanged

        Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")
        Me.lkpGrupo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpGrupo.EditValueChanged

        Me.lkpGrupo.Text = Me.lkpGrupo.GetValue("descripcion")
        Me.lkpArticulo.EditValue = Nothing

    End Sub
    Private Sub lkpGrupo_LoadData(ByVal Initialize As Boolean) Handles lkpGrupo.LoadData

        Dim Response As New Events

        Response = oGrupos.Lookup(Departamento)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpGrupo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpGrupo_Format() Handles lkpGrupo.Format
        Comunes.clsFormato.for_grupos_articulos_grl(Me.lkpGrupo)
    End Sub

    Private Sub lkpArticulo_Format() Handles lkpArticulo.Format
        Comunes.clsFormato.for_articulos_grl(Me.lkpArticulo)
    End Sub
    Private Sub lkpArticulo_LoadData(ByVal Initialize As Boolean) Handles lkpArticulo.LoadData

        Dim Response As New Events
        Response = oArticulos.Lookup(Departamento, Grupo)
        If Not Response.ErrorFound Then

            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpArticulo.DataSource = oDataSet.Tables(0)

        End If

    End Sub
    Private Sub lkpArticulo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpArticulo.EditValueChanged

        If Articulo <> -1 Then

            If articulo_temporal <> Articulo Then articulo_temporal = Articulo
            Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
            Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")
            Me.lkpArticulo.EditValue = articulo_temporal
            Me.txtNArticulo.Text = Me.lkpArticulo.GetValue("descripcion_corta")
            Me.txtmodelo.Text = Me.lkpArticulo.GetValue("modelo")
            Me.clcPrecioLista.Value = Me.lkpArticulo.GetValue("precio_lista")
        End If

    End Sub
#End Region

#Region "Funcionalidad"
    Private Sub Actualiza_Lookups()

        Me.lkpDepartamento.EditValue = Me.lkpArticulo.GetValue("departamento")
        Me.lkpDepartamento_LoadData(True)
        If Me.lkpDepartamento.EditValue > 0 Then

            Me.lkpDepartamento.Text = Me.lkpDepartamento.GetValue("nombre")

        End If

        Me.lkpGrupo.EditValue = Me.lkpArticulo.GetValue("grupo")

    End Sub
#End Region

    

    
    Private Sub clcCantidad_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clcCantidad.EditValueChanged
        If IsNumeric(Me.clcCantidad.EditValue) Then
            Me.clcPrecioListaCantidad.Value = Me.clcCantidad.Value * Me.clcPrecioLista.Value
        End If
    End Sub
End Class
