Imports Dipros.Utils
Imports Dipros.Utils.Common


Public Class frmConsultaNotaCargo
    Inherits Dipros.Windows.frmTINForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grDocumentos As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDocumentos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcSucursalNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcNumeroDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grcImporte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents clcImporte As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents dteFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents clcDocumentos As Dipros.Editors.TINCalcEdit
    Friend WithEvents lblDocumentos As System.Windows.Forms.Label
    Friend WithEvents txtSucursal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents tbReimprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents lkpNotaCargo As Dipros.Editors.TINMultiLookup
    Friend WithEvents lblNotaCargo As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents tbCancelarNota As System.Windows.Forms.ToolBarButton
    Friend WithEvents lkpConceptoNotaCargo As Dipros.Editors.TINMultiLookup
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConsultaNotaCargo))
        Me.grDocumentos = New DevExpress.XtraGrid.GridControl
        Me.grvDocumentos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcSucursalNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcNumeroDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.grcImporte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblObservaciones = New System.Windows.Forms.Label
        Me.txtObservaciones = New DevExpress.XtraEditors.MemoEdit
        Me.lblImporte = New System.Windows.Forms.Label
        Me.clcImporte = New Dipros.Editors.TINCalcEdit
        Me.lblFecha = New System.Windows.Forms.Label
        Me.dteFecha = New DevExpress.XtraEditors.DateEdit
        Me.lkpNotaCargo = New Dipros.Editors.TINMultiLookup
        Me.txtSerie = New DevExpress.XtraEditors.TextEdit
        Me.lblSerie = New System.Windows.Forms.Label
        Me.lblNotaCargo = New System.Windows.Forms.Label
        Me.lblCliente = New System.Windows.Forms.Label
        Me.txtCliente = New DevExpress.XtraEditors.TextEdit
        Me.clcDocumentos = New Dipros.Editors.TINCalcEdit
        Me.lblDocumentos = New System.Windows.Forms.Label
        Me.txtSucursal = New DevExpress.XtraEditors.TextEdit
        Me.lblSucursal = New System.Windows.Forms.Label
        Me.tbReimprimir = New System.Windows.Forms.ToolBarButton
        Me.Label25 = New System.Windows.Forms.Label
        Me.lkpConceptoNotaCargo = New Dipros.Editors.TINMultiLookup
        Me.tbCancelarNota = New System.Windows.Forms.ToolBarButton
        CType(Me.grDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.clcDocumentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ilsToolbar
        '
        Me.ilsToolbar.ImageStream = CType(resources.GetObject("ilsToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'tbrTools
        '
        Me.tbrTools.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbReimprimir, Me.tbCancelarNota})
        Me.tbrTools.Name = "tbrTools"
        Me.tbrTools.Size = New System.Drawing.Size(1053, 28)
        '
        'grDocumentos
        '
        '
        'grDocumentos.EmbeddedNavigator
        '
        Me.grDocumentos.EmbeddedNavigator.Name = ""
        Me.grDocumentos.Location = New System.Drawing.Point(8, 272)
        Me.grDocumentos.MainView = Me.grvDocumentos
        Me.grDocumentos.Name = "grDocumentos"
        Me.grDocumentos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar})
        Me.grDocumentos.Size = New System.Drawing.Size(552, 168)
        Me.grDocumentos.TabIndex = 87
        Me.grDocumentos.Text = "Documentos"
        Me.grDocumentos.Visible = False
        '
        'grvDocumentos
        '
        Me.grvDocumentos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.grcSucursal, Me.grcSucursalNombre, Me.grcFactura, Me.grcNumeroDocumento, Me.grcFecha, Me.grcImporte})
        Me.grvDocumentos.GridControl = Me.grDocumentos
        Me.grvDocumentos.Name = "grvDocumentos"
        Me.grvDocumentos.OptionsCustomization.AllowFilter = False
        Me.grvDocumentos.OptionsCustomization.AllowGroup = False
        Me.grvDocumentos.OptionsCustomization.AllowSort = False
        Me.grvDocumentos.OptionsView.ShowGroupPanel = False
        Me.grvDocumentos.OptionsView.ShowIndicator = False
        '
        'grcSucursal
        '
        Me.grcSucursal.Caption = "Sucursal"
        Me.grcSucursal.FieldName = "sucursal"
        Me.grcSucursal.Name = "grcSucursal"
        Me.grcSucursal.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'grcSucursalNombre
        '
        Me.grcSucursalNombre.Caption = "Sucursal"
        Me.grcSucursalNombre.FieldName = "sucursal_nombre"
        Me.grcSucursalNombre.Name = "grcSucursalNombre"
        Me.grcSucursalNombre.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcSucursalNombre.VisibleIndex = 0
        Me.grcSucursalNombre.Width = 152
        '
        'grcFactura
        '
        Me.grcFactura.Caption = "Factura"
        Me.grcFactura.FieldName = "factura"
        Me.grcFactura.Name = "grcFactura"
        Me.grcFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFactura.VisibleIndex = 1
        Me.grcFactura.Width = 80
        '
        'grcNumeroDocumento
        '
        Me.grcNumeroDocumento.Caption = "Documento"
        Me.grcNumeroDocumento.FieldName = "documento"
        Me.grcNumeroDocumento.Name = "grcNumeroDocumento"
        Me.grcNumeroDocumento.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcNumeroDocumento.VisibleIndex = 2
        Me.grcNumeroDocumento.Width = 70
        '
        'grcFecha
        '
        Me.grcFecha.Caption = "Fecha Vencimiento"
        Me.grcFecha.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.grcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.grcFecha.FieldName = "fecha_vencimiento"
        Me.grcFecha.Name = "grcFecha"
        Me.grcFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcFecha.VisibleIndex = 3
        Me.grcFecha.Width = 109
        '
        'grcImporte
        '
        Me.grcImporte.Caption = "Importe"
        Me.grcImporte.DisplayFormat.FormatString = "$###,###,##0.00"
        Me.grcImporte.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.grcImporte.FieldName = "importe"
        Me.grcImporte.Name = "grcImporte"
        Me.grcImporte.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.grcImporte.VisibleIndex = 4
        Me.grcImporte.Width = 139
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 256)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 17)
        Me.Label2.TabIndex = 86
        Me.Label2.Text = "&DOCUMENTOS"
        Me.Label2.Visible = False
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(16, 208)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(89, 16)
        Me.lblObservaciones.TabIndex = 16
        Me.lblObservaciones.Tag = ""
        Me.lblObservaciones.Text = "Observaciones:"
        Me.lblObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtObservaciones
        '
        Me.txtObservaciones.EditValue = ""
        Me.txtObservaciones.Location = New System.Drawing.Point(112, 208)
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtObservaciones.Properties
        '
        Me.txtObservaciones.Properties.Enabled = False
        Me.txtObservaciones.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtObservaciones.Size = New System.Drawing.Size(360, 38)
        Me.txtObservaciones.TabIndex = 17
        Me.txtObservaciones.Tag = "observaciones"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(56, 184)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(53, 16)
        Me.lblImporte.TabIndex = 12
        Me.lblImporte.Tag = ""
        Me.lblImporte.Text = "Importe:"
        Me.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clcImporte
        '
        Me.clcImporte.EditValue = "0"
        Me.clcImporte.Location = New System.Drawing.Point(112, 184)
        Me.clcImporte.MaxValue = 0
        Me.clcImporte.MinValue = 0
        Me.clcImporte.Name = "clcImporte"
        '
        'clcImporte.Properties
        '
        Me.clcImporte.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcImporte.Properties.Enabled = False
        Me.clcImporte.Properties.MaskData.EditMask = "$###,###,##0.00"
        Me.clcImporte.Properties.Precision = 2
        Me.clcImporte.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcImporte.Size = New System.Drawing.Size(96, 19)
        Me.clcImporte.TabIndex = 13
        Me.clcImporte.Tag = "importe"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(64, 112)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 16)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Tag = ""
        Me.lblFecha.Text = "Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dteFecha
        '
        Me.dteFecha.EditValue = New Date(2006, 3, 3, 0, 0, 0, 0)
        Me.dteFecha.Location = New System.Drawing.Point(112, 112)
        Me.dteFecha.Name = "dteFecha"
        '
        'dteFecha.Properties
        '
        Me.dteFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteFecha.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.EditFormat.FormatString = "dd/MMM/yyyy"
        Me.dteFecha.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.dteFecha.Properties.Enabled = False
        Me.dteFecha.Size = New System.Drawing.Size(96, 23)
        Me.dteFecha.TabIndex = 7
        Me.dteFecha.Tag = ""
        '
        'lkpNotaCargo
        '
        Me.lkpNotaCargo.AllowAdd = False
        Me.lkpNotaCargo.AutoReaload = False
        Me.lkpNotaCargo.DataSource = Nothing
        Me.lkpNotaCargo.DefaultSearchField = ""
        Me.lkpNotaCargo.DisplayMember = "folio"
        Me.lkpNotaCargo.EditValue = Nothing
        Me.lkpNotaCargo.Filtered = False
        Me.lkpNotaCargo.InitValue = Nothing
        Me.lkpNotaCargo.Location = New System.Drawing.Point(112, 88)
        Me.lkpNotaCargo.MultiSelect = False
        Me.lkpNotaCargo.Name = "lkpNotaCargo"
        Me.lkpNotaCargo.NullText = ""
        Me.lkpNotaCargo.PopupWidth = CType(350, Long)
        Me.lkpNotaCargo.ReadOnlyControl = False
        Me.lkpNotaCargo.Required = False
        Me.lkpNotaCargo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpNotaCargo.SearchMember = ""
        Me.lkpNotaCargo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpNotaCargo.SelectAll = False
        Me.lkpNotaCargo.Size = New System.Drawing.Size(136, 20)
        Me.lkpNotaCargo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpNotaCargo.TabIndex = 5
        Me.lkpNotaCargo.Tag = ""
        Me.lkpNotaCargo.ToolTip = Nothing
        Me.lkpNotaCargo.ValueMember = "campos_llave"
        '
        'txtSerie
        '
        Me.txtSerie.EditValue = ""
        Me.txtSerie.Location = New System.Drawing.Point(112, 64)
        Me.txtSerie.Name = "txtSerie"
        '
        'txtSerie.Properties
        '
        Me.txtSerie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie.Properties.MaxLength = 3
        Me.txtSerie.Size = New System.Drawing.Size(32, 20)
        Me.txtSerie.TabIndex = 3
        Me.txtSerie.Tag = ""
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(72, 64)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(37, 16)
        Me.lblSerie.TabIndex = 2
        Me.lblSerie.Tag = ""
        Me.lblSerie.Text = "&Serie:"
        Me.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNotaCargo
        '
        Me.lblNotaCargo.AutoSize = True
        Me.lblNotaCargo.Location = New System.Drawing.Point(24, 88)
        Me.lblNotaCargo.Name = "lblNotaCargo"
        Me.lblNotaCargo.Size = New System.Drawing.Size(88, 16)
        Me.lblNotaCargo.TabIndex = 4
        Me.lblNotaCargo.Tag = ""
        Me.lblNotaCargo.Text = "&Nota de Cargo:"
        Me.lblNotaCargo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(64, 160)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(47, 16)
        Me.lblCliente.TabIndex = 10
        Me.lblCliente.Tag = ""
        Me.lblCliente.Text = "Cliente:"
        Me.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCliente
        '
        Me.txtCliente.EditValue = ""
        Me.txtCliente.Location = New System.Drawing.Point(112, 160)
        Me.txtCliente.Name = "txtCliente"
        '
        'txtCliente.Properties
        '
        Me.txtCliente.Properties.Enabled = False
        Me.txtCliente.Properties.MaxLength = 3
        Me.txtCliente.Size = New System.Drawing.Size(360, 20)
        Me.txtCliente.TabIndex = 11
        Me.txtCliente.Tag = ""
        '
        'clcDocumentos
        '
        Me.clcDocumentos.EditValue = "0"
        Me.clcDocumentos.Location = New System.Drawing.Point(320, 184)
        Me.clcDocumentos.MaxValue = 0
        Me.clcDocumentos.MinValue = 0
        Me.clcDocumentos.Name = "clcDocumentos"
        '
        'clcDocumentos.Properties
        '
        Me.clcDocumentos.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDocumentos.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.clcDocumentos.Properties.Enabled = False
        Me.clcDocumentos.Properties.MaskData.EditMask = "0"
        Me.clcDocumentos.Properties.Precision = 2
        Me.clcDocumentos.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                        Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                        Or DevExpress.Utils.StyleOptions.UseFont) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor) _
                        Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                        Or DevExpress.Utils.StyleOptions.UseImage) _
                        Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                        Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.clcDocumentos.Size = New System.Drawing.Size(96, 19)
        Me.clcDocumentos.TabIndex = 15
        Me.clcDocumentos.Tag = "documentos"
        '
        'lblDocumentos
        '
        Me.lblDocumentos.AutoSize = True
        Me.lblDocumentos.Location = New System.Drawing.Point(232, 184)
        Me.lblDocumentos.Name = "lblDocumentos"
        Me.lblDocumentos.Size = New System.Drawing.Size(78, 16)
        Me.lblDocumentos.TabIndex = 14
        Me.lblDocumentos.Tag = ""
        Me.lblDocumentos.Text = "Documentos:"
        Me.lblDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSucursal
        '
        Me.txtSucursal.EditValue = ""
        Me.txtSucursal.Location = New System.Drawing.Point(112, 136)
        Me.txtSucursal.Name = "txtSucursal"
        '
        'txtSucursal.Properties
        '
        Me.txtSucursal.Properties.Enabled = False
        Me.txtSucursal.Properties.MaxLength = 3
        Me.txtSucursal.Size = New System.Drawing.Size(360, 20)
        Me.txtSucursal.TabIndex = 9
        Me.txtSucursal.Tag = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(56, 136)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(56, 16)
        Me.lblSucursal.TabIndex = 8
        Me.lblSucursal.Tag = ""
        Me.lblSucursal.Text = "Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbReimprimir
        '
        Me.tbReimprimir.Text = "Reimprimir"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(48, 40)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(60, 16)
        Me.Label25.TabIndex = 0
        Me.Label25.Tag = ""
        Me.Label25.Text = "Concepto:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lkpConceptoNotaCargo
        '
        Me.lkpConceptoNotaCargo.AllowAdd = False
        Me.lkpConceptoNotaCargo.AutoReaload = False
        Me.lkpConceptoNotaCargo.DataSource = Nothing
        Me.lkpConceptoNotaCargo.DefaultSearchField = ""
        Me.lkpConceptoNotaCargo.DisplayMember = "descripcion"
        Me.lkpConceptoNotaCargo.EditValue = Nothing
        Me.lkpConceptoNotaCargo.Filtered = False
        Me.lkpConceptoNotaCargo.InitValue = Nothing
        Me.lkpConceptoNotaCargo.Location = New System.Drawing.Point(112, 40)
        Me.lkpConceptoNotaCargo.MultiSelect = False
        Me.lkpConceptoNotaCargo.Name = "lkpConceptoNotaCargo"
        Me.lkpConceptoNotaCargo.NullText = ""
        Me.lkpConceptoNotaCargo.PopupWidth = CType(400, Long)
        Me.lkpConceptoNotaCargo.ReadOnlyControl = False
        Me.lkpConceptoNotaCargo.Required = False
        Me.lkpConceptoNotaCargo.SearchFirstBy = Dipros.Editors.eSearchPriority.DisplayMember
        Me.lkpConceptoNotaCargo.SearchMember = ""
        Me.lkpConceptoNotaCargo.SearchSecondBy = Dipros.Editors.eSearchPriority.ValueMember
        Me.lkpConceptoNotaCargo.SelectAll = False
        Me.lkpConceptoNotaCargo.Size = New System.Drawing.Size(211, 20)
        Me.lkpConceptoNotaCargo.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlDark)
        Me.lkpConceptoNotaCargo.TabIndex = 1
        Me.lkpConceptoNotaCargo.Tag = "concepto_cxp_nota_credito"
        Me.lkpConceptoNotaCargo.ToolTip = "Concepto de la Nota de Cr�dito en CxP"
        Me.lkpConceptoNotaCargo.ValueMember = "concepto"
        '
        'tbCancelarNota
        '
        Me.tbCancelarNota.Text = "Cancela Nota"
        Me.tbCancelarNota.ToolTipText = "Cancela la nota de cargo visualizada"
        '
        'frmConsultaNotaCargo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(570, 260)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.lkpConceptoNotaCargo)
        Me.Controls.Add(Me.grDocumentos)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.clcImporte)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.dteFecha)
        Me.Controls.Add(Me.lkpNotaCargo)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.lblSerie)
        Me.Controls.Add(Me.lblNotaCargo)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.clcDocumentos)
        Me.Controls.Add(Me.lblDocumentos)
        Me.Controls.Add(Me.txtSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Name = "frmConsultaNotaCargo"
        Me.Text = "Consulta de Nota de Cargo"
        Me.Controls.SetChildIndex(Me.lblSucursal, 0)
        Me.Controls.SetChildIndex(Me.txtSucursal, 0)
        Me.Controls.SetChildIndex(Me.lblDocumentos, 0)
        Me.Controls.SetChildIndex(Me.clcDocumentos, 0)
        Me.Controls.SetChildIndex(Me.txtCliente, 0)
        Me.Controls.SetChildIndex(Me.lblCliente, 0)
        Me.Controls.SetChildIndex(Me.lblNotaCargo, 0)
        Me.Controls.SetChildIndex(Me.lblSerie, 0)
        Me.Controls.SetChildIndex(Me.txtSerie, 0)
        Me.Controls.SetChildIndex(Me.lkpNotaCargo, 0)
        Me.Controls.SetChildIndex(Me.dteFecha, 0)
        Me.Controls.SetChildIndex(Me.lblFecha, 0)
        Me.Controls.SetChildIndex(Me.clcImporte, 0)
        Me.Controls.SetChildIndex(Me.lblImporte, 0)
        Me.Controls.SetChildIndex(Me.txtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.lblObservaciones, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.grDocumentos, 0)
        Me.Controls.SetChildIndex(Me.lkpConceptoNotaCargo, 0)
        Me.Controls.SetChildIndex(Me.Label25, 0)
        CType(Me.grDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcImporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.clcDocumentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private oMovimientosCobrar As VillarrealBusiness.clsMovimientosCobrar
    Private oMovimientosCobrarDetalle As VillarrealBusiness.clsMovimientosCobrarDetalle
    Private oReportes As VillarrealBusiness.Reportes
    Private oConceptosCXC As VillarrealBusiness.clsConceptosCxc


    Private ReadOnly Property Serie() As String
        Get
            Return Me.txtSerie.Text
        End Get
    End Property
    Private ReadOnly Property Sucursal() As Long
        Get
            Return CLng(Me.lkpNotaCargo.GetValue("sucursal"))
        End Get
    End Property
    'Private ReadOnly Property Concepto() As String
    '    Get
    '        Return CStr(Me.lkpNotaCargo.GetValue("concepto"))
    '    End Get
    'End Property
    Private ReadOnly Property Folio() As Long
        Get
            Return CLng(Me.lkpNotaCargo.GetValue("folio"))
        End Get
    End Property

    Private ReadOnly Property Cliente() As Long
        Get
            Return CLng(Me.lkpNotaCargo.GetValue("cliente"))
        End Get
    End Property

    Private ReadOnly Property concepto_cxc_nota_cargo() As String
        Get
            Return Comunes.clsUtilerias.PreparaValorLookupStr(Me.lkpConceptoNotaCargo)
        End Get
    End Property

    Private FolioElectronico As Long = 0


    Private Sub frmConsultaNotaCargo_Initialize(ByRef Response As Dipros.Utils.Events) Handles MyBase.Initialize
        oMovimientosCobrar = New VillarrealBusiness.clsMovimientosCobrar
        oMovimientosCobrarDetalle = New VillarrealBusiness.clsMovimientosCobrarDetalle
        oReportes = New VillarrealBusiness.Reportes
        oConceptosCXC = New VillarrealBusiness.clsConceptosCxc


        Me.Location = New Point(0, 0)
        Me.tbrTools.Buttons(0).Visible = False
        Me.tbrTools.Buttons(0).Enabled = False
        Me.tbrTools.Buttons(2).Enabled = False


    End Sub
    Private Sub tbrTools_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbrTools.ButtonClick
        If e.Button Is Me.tbCancelarNota Then
            Dim oEvents As Events

            TINApp.Connection.Begin()

            oEvents = oMovimientosCobrar.CancelarNotaCargo(Sucursal, Me.concepto_cxc_nota_cargo, Serie, Folio, Cliente, CDate(TINApp.FechaServidor).ToString)
            If Not oEvents.ErrorFound Then
                'Me.Close()


                oEvents = Me.oMovimientosCobrar.CancelarTimbrado(Serie, Folio, Me.FolioElectronico)
                If Not oEvents.ErrorFound Then
                    TINApp.Connection.Commit()

                    Me.lkpNotaCargo.EditValue = Nothing
                    Me.lkpNotaCargo_LoadData(True)
                    Me.txtSerie.Focus()

                Else
                    TINApp.Connection.Rollback()
                End If


            Else
                TINApp.Connection.Rollback()
                oEvents.ShowError()
            End If

        End If

        If e.Button Is Me.tbReimprimir Then
            Dim pregunta As Boolean
            Select Case ShowMessage(MessageType.MsgQuestion, "�Con iva desglosado?")
                Case Answer.MsgYes
                    pregunta = True
                Case Answer.MsgNo
                    pregunta = False
                Case Answer.MsgCancel
                    Exit Sub
            End Select
            Me.ImprimirNotaCargo(Me.concepto_cxc_nota_cargo, Serie, Folio)

        End If
    End Sub

    Private Sub llkpConceptoNotaCargo_Format() Handles lkpConceptoNotaCargo.Format
        Comunes.clsFormato.for_conceptos_cxc_grl(Me.lkpConceptoNotaCargo)
    End Sub
    Private Sub lkpConceptoNotaCargo_LoadData(ByVal Initialize As Boolean) Handles lkpConceptoNotaCargo.LoadData
        Dim Response As New Events
        Response = oConceptosCXC.Lookup("C")
        If Not Response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = Response.Value
            Me.lkpConceptoNotaCargo.DataSource = oDataSet.Tables(0)
            oDataSet = Nothing
        End If
    End Sub

    Private Sub lkpNotaCargo_LoadData(ByVal Initialize As Boolean) Handles lkpNotaCargo.LoadData
        Dim response As Events
        response = oMovimientosCobrar.LookupNotaCargo(Serie, concepto_cxc_nota_cargo)
        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.lkpNotaCargo.DataSource = oDataSet.Tables(0)
        Else
            response.ShowError()
        End If

    End Sub
    Private Sub lkpNotaCargo_Format() Handles lkpNotaCargo.Format
        Comunes.clsFormato.for_notas_cargo_grl(Me.lkpNotaCargo)
    End Sub
    Private Sub lkpNotaCargo_EditValueChanged(ByVal Sender As Object, ByVal e As System.EventArgs) Handles lkpNotaCargo.EditValueChanged

        Dim oEvents As New Events

        If Me.lkpNotaCargo.EditValue Is Nothing Then
            LimpiarDatosNotaCargo()
            Me.tbrTools.Buttons(2).Enabled = False
            Exit Sub
        End If

        DesplegarDatosDeNotaCargo()
        Me.tbrTools.Buttons(2).Enabled = True
        DesplegarDocumentosDeNotaCargo(oEvents)
        If oEvents.ErrorFound Then
            oEvents.ShowError()
            Exit Sub
        End If

    End Sub
    Private Sub lkpNotaCargo_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkpNotaCargo.Validated
        If Me.lkpNotaCargo.Text = "" Then
            Me.lkpNotaCargo.Focus()
            Me.tbrTools.Buttons(2).Enabled = False
            Me.tbrTools.Buttons(3).Enabled = False


        Else
            Me.tbrTools.Buttons(2).Enabled = True
            Me.tbrTools.Buttons(3).Enabled = True
        End If

    End Sub
    Private Sub txtSerie_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerie.EditValueChanged
        If Me.txtSerie.Text.Length < 1 Then Exit Sub

        Me.lkpNotaCargo.EditValue = Nothing
        Me.lkpNotaCargo_LoadData(True)

    End Sub
    Private Sub txtSerie_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerie.Validated
        If Me.lkpNotaCargo.DataSource Is Nothing Then
            Me.txtSerie.Text = ""
            Me.txtSerie.Focus()
            Exit Sub
        End If

        If CType(Me.lkpNotaCargo.DataSource, DataTable).Rows.Count < 1 Then
            Me.txtSerie.Text = ""
            Me.txtSerie.Focus()
        End If
    End Sub


    Private Sub DesplegarDatosDeNotaCargo()
        Me.dteFecha.EditValue = CDate(Me.lkpNotaCargo.GetValue("fecha"))
        Me.txtCliente.Text = CStr(Me.lkpNotaCargo.GetValue("nombre_cliente"))
        Me.txtSucursal.Text = CStr(Me.lkpNotaCargo.GetValue("nombre_sucursal"))
        Me.clcImporte.EditValue = CDbl(Me.lkpNotaCargo.GetValue("total"))
        Me.clcDocumentos.EditValue = CLng(Me.lkpNotaCargo.GetValue("documentos"))
        Me.txtObservaciones.Text = CStr(Me.lkpNotaCargo.GetValue("observaciones"))
        FolioElectronico = CLng(Me.lkpNotaCargo.GetValue("folio_factura_electronica"))
    End Sub
    Private Sub LimpiarDatosNotaCargo()
        Me.dteFecha.EditValue = Nothing
        Me.txtCliente.Text = ""
        Me.txtSucursal.Text = ""
        Me.clcImporte.EditValue = 0
        Me.clcDocumentos.EditValue = 0
        Me.txtObservaciones.Text = ""
        Me.grDocumentos.DataSource = Nothing
        FolioElectronico = 0
    End Sub
    Private Sub DesplegarDocumentosDeNotaCargo(ByRef response As Events)
        response = oMovimientosCobrarDetalle.ConsultaDocumentosSaldados(Sucursal, Me.concepto_cxc_nota_cargo, Serie, Folio, Cliente)

        If Not response.ErrorFound Then
            Dim oDataSet As DataSet
            oDataSet = response.Value
            Me.grDocumentos.DataSource = oDataSet.Tables(0)
        End If

    End Sub
    Private Sub ImprimirNotaCargo(ByVal conceptointereses As String, ByVal serieinteres As String, ByVal foliointeres As Long)
        Dim response As New Events

        response = oReportes.ImprimeNotaDeCargo(Sucursal, conceptointereses, serieinteres, foliointeres, Cliente, True)
        If response.ErrorFound Then
            ShowMessage(MessageType.MsgInformation, "La Nota de Cargo no pueden Mostrarse")
        Else
            If response.Value.Tables(0).Rows.Count > 0 Then
                Dim oDataSet As DataSet
                Dim oReport As New Comunes.rptNotaCargo   'Clientes.rptNotaCargo

                oDataSet = response.Value
                oReport.DataSource = oDataSet.Tables(0)

                'oReport.picLogotipo.Image = Comunes.clsUtilerias.uti_ByteToImage(oDataSet.Tables(0).Rows(0).Item("logotipo"))
                TINApp.ShowReport(Me.MdiParent, "Impresi�n de la Nota de Cargo ", oReport)
            Else
                ShowMessage(MessageType.MsgInformation, "El Reporte no tiene Informaci�n")
            End If
        End If

    End Sub




End Class
